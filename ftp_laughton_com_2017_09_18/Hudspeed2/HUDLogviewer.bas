REM Start of BASIC! Program
Myname$="hudlogviewer.bas"
Debug.off
FN.DEF getstaticmap$(lat,lon,zoom,sizex,sizey,fname$,maptype$) 
Url$="http://maps.google.com/maps/api/staticmap?"
Url$=url$+"center="+format$("##%.######",lat)+","+format$("##%.######",lon)
Url$=url$+"&zoom="+format$("##",zoom)+"&scale=1"
Url$=url$+"&size="+format$("####",sizex)+"x"+format $("####",sizey)
Url$=url$+"&maptype="+maptype$
Url$=url$+"&sensor=false"
Url$=replace$(url$," ","") 
Byte.open r,xx,url$
Byte.copy xx,fname$
FN.RTN url$
! Print url$
FN.END 
Fn.def datecheat$(i$) 
Mths$="JanFebMarAprMayJunJulAugSepOctNovDec" 
Q$=mid$(i$,7,2) +" "+mid$(mths$,(3*val(mid$(i$,5,2)))-2,3)+" "+left$(i$,4)+"@"+mid$(i$,9,2)+":"+right$(i$,2)
FN.RTN q$
Fn.end







FN.DEF ltrim$(a$) 
While left$(a$,1)=" "
A$=right$(a$,len(a$)-1)
REPEAT 
Fn.rtn a$
FN.END 
FN.DEF trimto$(a$,n) 
While left$(a$,1)=" "
A$=right$(a$,len(a$)-1)
REPEAT 
While len(a$)<n
 A$=" "+a$
REPEAT 
Fn.rtn a$
FN.END 
Maptype$="roadmap"
File.root fr$
Notapk=is_in("RFO-BASIC",UPPER$(FR$)) 

DIM scobjs[10]
DIM kks[91]
Kks[1]=110.574
Kks[16]=110.649
Kks[31]=110.852
Kks[46]=111.132
Kks[61]=111.142
Kks[76]=111.618
Kks[91]=111.694
FOR x=1 TO 90 STEP 15
 Poop=(kks[x+15]-kks[x])/15
 Parp=kks[x]
 FOR y= 1 TO 14
  Kks[x+y]=poop+parp
  Parp=parp+poop
 NEXT y
NEXT x




ARRAY.LOAD pointy[],1,2,5.625,11.25,22.5,45,90,180,360,720,1440,2880,5760,11520,23040,46080,92160,184320,368640
ARRAY.LOAD Days$[],"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"
ARRAY.LOAD mths$[],"January","February","March","April","May","June","July","August","September","October","November","December"

GR.OPEN 255,0,0,0, 0,0
Topfile=1
Bakin:
Gr.Screen w, h 
GR.TEXT.SIZE h/20
GR.TEXT.ALIGN 1 
GR.COLOR 255,0,255,0,1 
GR.PAINT.GET grircle
GR.COLOR 255,0,0,255,1 
GR.PAINT.GET blircle
GR.COLOR 255,255 ,255,255 ,1 
!! 
If notapk
  Fprefix1$=""
Else
  Fprefix1$="../../HUDSpeed/"
ENDIF 
!! 
Fprefix1$="../../"
FILE.EXISTS fe, fprefix1$+"hudlogs" 
If!fe
 alogct=0
ELSE 
 File.dir fprefix1$+"hudlogs", all$[] 
 ARRAY.LENGTH alogct,all$[] 
 If all$[1]="" then alogct=0
ENDIF 
Dlogct=0





IF Alogct+dlogct=0
  Dialog.message "There are no Logs Available","",ok,"Close Viewer" 
  If notapk then end else exit 
ENDIF 
!! 
If alogct>0&dlogct>0
 If alogct=1 then areis$="is" else areis$="are"
 If alogct=1 then e$=" " else e$="s" 
 If dlogct=1 then de$="" else de$="s"
  Dialog.message "there"+areis$+" "+int$(alogct)+" Alec Log"+e$,"and "+int$(dlogct)+" Dalec Log"+de$+" available",ok,"Alec","Dalec"
 If ok=1 then from$=" from Alec " else from$="  From Dalec "
ENDIF

IF alogct=0 then From$="from Dalec "
IF dlogct=0 then From$="from Alec "

From$="from HUDSpeed" 
!! 
GR.TEXT.DRAW h1,w/24,h/16,"LOG VIEWER"
GR.TEXT.SIZE h/24
GR.TEXT.DRAW h2,w/24,h/8,"These Logs are available:-"
GR.TEXT.DRAW h3,w/24,h*3/16,"Touch to select, Long Press to Delete"
Gr.circle hl1,w*0.95,h/50,w/200
Gr.circle hl2,w*0.95,h/25,w/200
Gr.circle hl3,w*0.95,h*3/50,w/200
Gr.color 255,255 ,255,0,1 


ldr$=fprefix1$+"/hudlogs/"



File.dir ldr$,ll$[] 
ARRAY.LENGTH l, ll$[]



Dim them[36]
Dim those$[36]
! 4 columns of 9
! Calculate text size
Twaddle=20
GR.TEXT.SIZE h/twaddle
Do
Gr.text.width twiddle,"77 mmm 9999@99.99"
Twaddle++
UNTIL twiddle>w/4
Twaddle--
GR.TEXT.SIZE h/twaddle


Col=w/32
! There is 13/16 of h left so each is 13/16*10
J=h*13/160
Row=h/4
Y=1 


For x=topfile TO topfile+35
If x<=l
   
     I$=left$(ll$[x],len(ll$[x]) -4)
     Oi$=i$
   I$=datecheat$(i$) 
ELSE 
   I$=" "
   Oi$=" "
   
ENDIF 

GR.TEXT.DRAW them[y],col,row,left$(i$,len(is$)-6)
Gr.text.width ctw, left$(i$,len(is$)-6)
GR.TEXT.SIZE h/(twaddle*2)
GR.TEXT.DRAW they,col+ctw,row, right$(i$,6)
GR.TEXT.SIZE h/twaddle
Those$[y]=oi$ 
 
Y=y+1
Row=row+j
If row>=h*9.5/10 then 
   Row=h/4
   Col=col+w/4
ENDIF 
Next x


If l>36+topfile
  Gr.text.width tw," More > "
  GR.TEXT.DRAW more,w-tw,h," More > "
  Morevis=1
ELSE 
!  Gr.hide more
  GR.RENDER 
ENDIF 
If topfile>1
   GR.TEXT.width tw," < Prev "
   GR.TEXT.DRAW prev, 0,h," <Prev "
Endif


GR.RENDER 
Hover:
Do
Gr.touch tch, tchx, tchy
UNTIL (tch&(tchy>h*4/16-j))|(tch&tchx>w*0.94&tchy<h*3/50)
If tchx>w*0.94
  Dialog.message,, ok,"Exit Viewer","Help","Cancel" 
  If ok=2
  GOSUB helpusall
  Do
  Gr.touch tch,tchx,tchy
  Until !tch
  GOTO hover
  Elseif ok=1
   If notapk then end else exit
  Endif
Endif

Ttt=time() 
If tchy>h*15/16&tchx>w*18/20
!     POPUP"more"+int$(topfile) 
     Topfile=topfile+36
      ARRAY.DELETE ll$[] 
       Undim them[] 
       Undim those$[] 
       Gr.cls
       Goto bakin
Endif
If tchx<w/20&tchy>h*15/16
   If topfile>36
     Topfile=topfile-36
   Else
     Topfile=1
   ENDIF 
   Undim them[] 
   Undim those$[] 
   ARRAY.DELETE ll$[] 
   Gr.cls
   GOTO bakin
ENDIF 




Hitun=(tchy-h/4+j) %how far down
Hitun=hitun/(h*3/4)  %frac down
Hitun=hitun*9+1 %got the row
Glop=int(tchx/w*4) 
Hitun=hitun+9*glop
Do
Gr.touch tch,tchx, tchy
UNTIL  !tch|time()-ttt>500
If time()-ttt>500 
!       GOSUB killfile

       Gr.get.value them[hitun],"text", hq$
 
 
!       Dialog.message "Permanently Delete",hq$ +"?",ok,"Yes","Cancel"

       Fd$=datecheat$(those$[hitun]) 
       Dialog.message "Permanently Delete",fd$ +"?",ok,"Yes","Cancel" 
       If ok=1 THEN File.Delete fq,ldr$+those$[hitun]+".log" 

       ARRAY.DELETE ll$[] 
       Undim them[] 
       Undim those$[] 
       Gr.cls
       Goto bakin
ENDIF 
! POPUP int$(hitun) 
Gr.get.value them[hitun] ,"text",hq$

Hq$=those$[hitun] 
 
If ltrim$(hq$) =""
 ARRAY.DELETE ll$[] 
 Undim them[] 
 Undim those$[] 
 Gr.cls
  Goto bakin
ENDIF 
! The file is chosen, let's process it
!! 
GR.TEXT.SIZE h/20
Logfile record types
R Event Start; td/la/lo
L Leg Start;td/la/lo/rate(c)/time(c,secs),dist(c) 
E Leg End td/la/lo/dist(leg a)/time(leg a)/ave leg
X Event End td/la/lo/tot inlegs dist/totall dist/totinlegs time/totallt
M Met/Imp td/mi
V Rate change td/newrate
! Point ti/la/lo diffs (each 10 secs) 


Don't 	think these matter anymore
GR.COLOR 255,0,255,0,1
GR.PAINT.GET grircle
GR.COLOR 255,0,0,255,1
GR.PAINT.GET blircle
!! 
GR.COLOR 255,0,0,0,1
If !lisx
 List.create s,els
 List.create n,lats
 List.create n,lons
 List.create n,tims
 List.create n,leggo %nlats when leg changes
 List.create n,circobjs
 List.create n,linobjs
 Dim ea[24]
 Lisx=1
Else
 List.clear els
 List.clear lats
 List.clear lons
 List.clear tims
 List.clear leggo
 ARRAY.DELETE ea[] 
ENDIF 
GR.TEXT.SIZE h/30
Vs=1
Legno=1
! Popup ldr$+hq$
Text.open r, f,ldr$+hq$+".log"

nvar=0
Gosub variance
! Creates vlaterr, vlonerr
Cumvlaterr=0
Cumvlonerr=0
Text.open r, f,ldr$+hq$+".log"
Text.readln f, a$
Split bb$[],a$, ":" 
Time val(bb$[2]),y$,m$,d$,h$,mm$,ss$,a,b
L$="Trip on "+days$[a]+" "+d$+" "+mths$[val(m$)] +" "+y$
! GR.TEXT.DRAWrl1, w/32,h/24,l$
List.add els,l$+"0"
Vs=vs+1
L$=h$+":"+mm$+" Trip Started"
! GR.TEXT.DRAW rl1, w/32,h/24+h/24*vs,l$
List.add els, l$+"0"
Latseed=val(bb$[3])
Lonseed=val(bb$[4])
Timseed=val(bb$[2])
ARRAY.DELETE bb$[]
Vs=vs+1 
DO 
Text.readln f,a$
If a$="EOF" THEN d_u.break
SW.BEGIN left$(a$,1)
SW.CASE "L" 
  GOSUB legstart
  SW.BREAK 
SW.CASE "E" 
  GOSUB legend
  SW.BREAK 
SW.CASE "X" 
  GOSUB eventend
  SW.BREAK 
SW.CASE "M" 
  GOSUB metimp
  SW.BREAK 
SW.CASE "V" 
  GOSUB ratech
  SW.BREAK 
SW.CASE "!" 
  GOSUB point
SW.DEFAULT 
SW.END 
UNTIL asleep
Text.close f
GOTO points


! Here are the record handling routines
! An extra ch is appended to each line in els
! This is to allow colours for legs
! The code is 0=white,1=red,2=green,3=blue,4=yellow

Eventend:
Split bb$[],a$,":"
Il=val(bb$[7])
Tl=val(bb$[8])
Hms=il
GOSUB chms
Il$=hms$
Hms=tl
GOSUB chms
Tl$=hms$
Time val(bb$[2]),y$,m$,d$,h$,mm$,ss$,a,b
L$=h$+":"+mm$+" Trip Ended"
! GR.TEXT.DRAW l, w/32,h/24+h/24*vs,l$
List.add els,l$+"0"
Vs=vs+1
! Skip inleg distance if -1 (Alec) 
If val(bb$[5])<>-1
! GR.TEXT.DRAWl,w/32, h/24+h/24*vs,"Inleg distance "+ltrim$(format$("##%.##",val(bb$[5]))) 
List.add els, "Inleg distance "+ltrim$(format$("##%.##",val(bb$[5])))+"0"
Vs=vs+1 
ENDIF 
! GR.TEXT.DRAW l,w/32,h/24+vs*h/24,"Overall distance "+ltrim$(format$("##%.##",val(bb$[6])))
List.add els, "Overall distance "+ltrim$(format$("##%.##",val(bb$[6])))+"km/"+ltrim$(format$("##%.##",val(bb$[6])*0.625))+"m"+"0" 

Vs=vs+1
! Skip inleg time if -1 (Alec) 
If val(bb$[7])<>-1
! GR.TEXT.DRAW l,w/32,h/24+h/24*vs,"Inleg time "+il$
List.add els, "Inleg time "+il$+"0"
Vs=vs+1
ENDIF 
! GR.TEXT.DRAW l,w/32,h/24+vs*h/24,"Overall time "+tl$
List.add els, "Overall time "+tl$+"0"
Vs=vs+1
If val(bb$[7])=-1 then goto legavskip1
If val(bb$[7])=0 THEN bb$[7]="1"
Avl=val(bb$[5])/((val(bb$[7])/3600000)) 
Legavskip1:
If val(bb$[8])=0 THEN bb$[8]="1"
Avt=val(bb$[6])/((val(bb$[8])/3600000)) 
If val(bb$[7]) =-1 then goto legavskip2
! GR.TEXT.DRAW l,w/32,h/24+vs*h/24,"Inleg average speed "+ltrim$(format$("###.##",avl)) 
List.add els, "Inleg average speed "+ltrim$(format$("###.##",avl))+"0" 
Vs=vs+1
Legavskip2:
! GR.TEXT.DRAW l, w/32,h/24+vs*h/24,"Overall avg.speed "+ltrim$(format$("###.##",avt*))+"kmh/"++ltrim$(format$("###",avt))
List.add els, "Overall avge speed "+ltrim$(format$("###",avt))+"kph/"+ltrim$(format$("###",avt*0.625))+"mph0"



Vs=vs+1
L$="Max Speed all Trip "+format$("##%",val(bb$[9])*3.6)+"kph/"+ltrim$(format$("##%",val(bb$[9])*3.6*0.625)+"mph") 
List.add els,l$+"0"
!!
L$="Altitude all Rally Min "+bb$[10]+" Max "+bb$[11]
List.add els,l$+"0"

!! 
ARRAY.DELETE bb$[] 
List.add leggo,99999
GR.RENDER 
RETURN 



Legstart:
SPLIT bb$[],a$,":" 
Time val(bb$[2]),y$,m$,d$,h$,mm$,ss$,a,b
L$=h$+":"+mm$+" Leg "+int$(legno)+" Start"
! GR.TEXT.DRAW l,w/32,h/24+vs*h/24,l$
Elscol=elscol+1
If elscol=5 THEN elscol=1
List.add els,l$+int$(elscol) 
Vs=vs+1
Hms=val(bb$[6]) *1000
GOSUB chms
If val(bb$[5])=0
   Sq$="???"
ELSE 
   Sq$= ltrim$(format$("##%",val(bb$[5])))
ENDIF 
If val(bb$[6])=0 THEN hms$="???"
If val(bb$[7])=0
   Dq$="???"
ELSE 
   Dq$=ltrim$(format$("##%.##",val(bb$[7])))
ENDIF 

L$="  S="+sq$ +",T="+hms$+",D="+dq$ 
! GR.TEXT.DRAW l,w/32,h/24+vs*h/24,l$
List.add els,l$+int$(elscol)
Vs=vs+1
ARRAY.DELETE bb$[] 
List.size lats,u
List.add leggo,u
RETURN 




Legend:

Split bb$[],a$,":"
! ARRAY.LENGTH al, bb$[] 

Time val(bb$[2]),y$,m$,d$,h$,mm$,ss$,a,b
L$=h$+":"+mm$+" Leg "+int$(legno)+" End"
! GR.TEXT.DRAW l,w/32,h/24+vs*h/24,l$
List.add els, l$+int$(elscol)
Vs=vs+1
Legno=legno+1
Hms=val(bb$[6])
GOSUB chms
L$=" Actual T="+hms$+",S="+ltrim$(format$("##%.##",val(bb$[7])))+",D="+ltrim$(format$("##%.##",val(bb$[5])))

! GR.TEXT.DRAW l,w/32,h/24+vs*h/24,l$
List.add els,l$+int$(elscol)
Vs=vs+1
L$="Max speed this Leg "+format$("##%",val(bb$[8]))
List.add els,l$+int$(elscol)
L$="Altitude this Leg Min "+bb$[9]+" Max "+bb$[10]
List.add els,l$+int$(elscol)
ARRAY.DELETE bb$[] 

RETURN 


Metimp:
SPLIT bb$[],a$,":" 
If bb$[3]="i" then im$="Imperial" else im$="Metric"
Time val(bb$[2]),y$,m$,d$,h$,mm$,ss$,a,b
L$=h$+":"+mm$+" System Units "+im$
! GR.TEXT.DRAW l,w/32,h/24+vs*h/24,l$
List.add els,l$+int$(elscol) 
Vs=vs+1
ARRAY.DELETE bb$[] 
RETURN 
Ratech:
Split bb$[], a$,":" 
Time val(bb$[2]),y$,m$,d$,h$,mm$,ss$,a,b
L$=h$+":"+mm$+" Speed change to "+ltrim$(format$("##%",val(bb$[3]))) 
List.add els,l$+int$(elscol) 
Vs=vs+1
ARRAY.DELETE bb$[] 
RETURN 


Point:
Split bb$[],a$,":" 
Nt=timseed+val(bb$[2]) 
List.add tims,nt
Timseed=nt
Nlat=latseed+val(bb$[3])/100000
If val(bb$[3])|val(bb$[4]) 
  Nlat=nlat+vlaterr
  Cumvlaterr=cumvlaterr+vlaterr
Endif



List.add lats,nlat
Latseed=nlat
Nlon=lonseed+val(bb$[4])/100000

If val(bb$[3])|val(bb$[4]) 
  Nlon=nlon+vlonerr
  Cumvlonerr=cumvlonerr+vlonerr
Endif

List.add lons,nlon
Lonseed=nlon
ARRAY.DELETE bb$[] 
RETURN 


! Get here after all records processed and lists built
! We could make this the cls point:we did and it is
Points:
! Debug.echo.on
Gr.cls
If mloaded 
 Gr.bitmap.delete mappy2
 Mappy2s--

Mloaded=0
ENDIF 
Gr.color 255,255,255,255,1 

List.size lons,lotz
! POPUP int$(lotz) 
List.size els,esz
Elsz=esz
! POPUP int$(esz) 


 !Make 24 holders
 For x=1 TO 24
 GR.TEXT.DRAW ea[x],w/64,h/24*x,""
NEXT x 
GR.PAINT.GET elswhite
GR.COLOR 255,255,0,0,1 
GR.PAINT.GET elsred
GR.COLOR 255,0,255,0,1 
GR.PAINT.GET elsgreen
GR.COLOR 255,0,0,255,1 
GR.PAINT.GET elsblue
GR.COLOR 255,255,255,0,1 
GR.PAINT.GET elsyellow
GR.COLOR 255,255,255,255,1 
Elscol=0
Gr.text.width tmw,"88.88"
Gr.line tmwl, Tmw+w/64, 0,tmw+w/64,h
Paddy$=""
Do
Paddy$=paddy$+" "
Gr.text.width pdw,paddy$
UNTIL pdw>=tmw
! Show/scroll 24 items from els
Eltp=1
If esz>24
  Elbt=24
Else
  Elbt=esz
ENDIF 
For x=eltp to elbt
List.get els,x,a$
Gosub cleg
La$=LEFT $(a$,1)
IF ! (la$>="0"&la$<="9") then a$ = paddy$+a$

GR.MODIFY ea[x],"text",a$,"paint",colg
Next x 
 
GR.RENDER 
If esz>24
  Popup "Swipe ^ v to Scroll Log", -w/4
ENDIF 
Zolly:

GOSUB drawmap

List.clear circobjs
List.clear linobjs
Oy=0
Egg=1

For x=1 TO lotz
List.get lats,x,nlat
List.get lons,x,nlon
Ppd=pointy[zm] 

List.get leggo,egg,yolk

If x>yolk
 Egg=egg+1
 Lgcol=mod(egg, 4)
 SW.BEGIN lgcol
 SW.CASE 0
   GR.COLOR 255,255,0,0,1 

   SW.BREAK 
 SW.CASE 1
   GR.COLOR 255,0,255,0,1 
 
   SW.BREAK 
 SW.CASE 2
   GR.COLOR 255,0,0,255,1 
 
   SW.BREAK 
 SW.CASE 3
   GR.COLOR 255,255,255,0,1 
 
 SW.END 
!! 
  If egg/2=int(egg/2)
    Gr.color 255,0,255,0,1 
  Else
    GR.COLOR 255,0,0,255,1
  ENDIF 
!! 
ENDIF 




GOSUB plotring
NEXT x 
If zm>4 THEN GR.SHOW zmoutxt
If zm < 18 then GR.SHOW zmintxt
If zm > 4 THEN Gr.show zmoutboxx
If zm < 18 then Gr.show zminboxx
Gr.show mtro
Gr.show mtte
Gr.show mthy
GR.COLOR 255,0,0,0,1 
! The index dots
Gr.circle md1,w-w/128,w/64,7
Gr.circle md2,w-w/128,w/64+w/128,7
Gr.circle md3,w-w/128,w/64+w/64,7

GR.RENDER 

Scrolljump:




Do
GOSUB scrolly
UNTIL now






chms:
X=round(hms/1000)
H77=int(x/3600)
H77$=int$(h77)
! If len(h77$)=1 then h77$="0"+h77$
X=x-(h77*3600)
M77=int(x/60)
M77$=int$(m77)
If len(m77$)=1 then m77$="0"+m77$
X=x-(m77*60)
S77$=int$(x) 
If len(s77$)=1 then s77$="0"+s77$
Hms$=h77$+":"+m77$+":"+s77$
RETURN



Scrolly:

Gr.touch tch,tchx,tchy
If  !tch then RETURN 
! Doth he zoom? 
Backgammon=0
If tchx >w/2
   Zozo=0
   If tchy>h*13/16&tchy<h*15/16
      If tchx>w*9/16&tchx<w*11/16&zm<18
       Zozo=1
       Lastlx=0
       GOTO points
     ENDIF 
     If tchx>w*13/16&tchx<w*15/16&zm>4
       Zozo=-1
       Lastlx=0
       GOTO points
     ENDIF 
   ENDIF 
   If tchy>h*15/16
     Maptype$="hybrid"
     If tchx<w*5/6 then maptype$="terrain"
     If tchx<w*4/6 then maptype$="roadmap"
     Lastlx=0
     GOTO points
   ENDIF 
ENDIF 

! Then menueth he? 
If tchx>w/2
  If tchx>(w-w/120)
    If tchy<h/16
! Touched menu dots
    Gosub mpanel
!    If gm=4 then popup "juju" 
    If gm=4 then goto scrolljump
      Lastlx=0
      GOSUB mapmenu
      If recentred 
!         Recentred=0
          goto points
          RETURN 
      ENDIF 
    ENDIF 
   ENDIF 
ENDIF 
! If he neither zoom nor menu and on the Right resume
! Whoa! If he's in map then maybe wants recentre
Qnc2=0
If tchx>w/2
 Qnewcentrex=tchx
 Qnewcentrey=tchy
 Qnewcentre=1
 Lastlx=0
 Qnc2=1
 Gosub mapmenu
Endif
If qnc2 then goto points




If tchx>w/2 THEN RETURN 
Oy=tchy
Do
Gr.touch tch,tchx,tchy
If tchy<>oy
 If tchy>oy then gosub scrup else gosub scrdown
ENDIF 
 Oy=tchy


UNTIL !tch
RETURN 
!! 
If xy=oy then RETURN 
If xy>oy 
   gosub scrdown
ELSE 
   GOSUB scrup
ENDIF 
RETURN
!! 
Scrup:

If eltp=1 THEN RETURN 
Eltp=eltp-1
If eltp+24>elsz 
   Elbt=elsz
ELSE 
   Elbt=eltp+24
ENDIF 
Y=eltp
 For x=1 to 24



If y<=elsz
   List.get els,y,a$
   GOSUB cleg
   Gosub talign
ELSE 
   A$=""
ENDIF 


GR.MODIFY ea[x],"text",a$,"paint",colg
Y=y+1
Next x 

GR.RENDER 
RETURN 
Scrdown:

 If elsz<elbt then RETURN 
Eltp=eltp+1

If eltp+24>elsz
   Elbt=elsz
ELSE 
   Elbt=eltp+24
ENDIF 
Y=eltp
For x=1 TO 24
If y<=elsz
  List.get els, y, a$
  GOSUB cleg
  GOSUB talign
ELSE 
  A$=""
ENDIF 
GR.MODIFY ea[x],"text",a$,"paint",colg
Y=y+1
Next x
GR.RENDER 
RETURN 


RETURN 


Talign:
La$=LEFT $(a$,1)
IF ! (la$>="0"&la$<="9") then a$ = paddy$+a$

RETURN 
Cleg:
Cleg$=right$(a$,1)
A$=LEFT$(a$,len(a$)-1)
SW.BEGIN cleg$
SW.CASE "0"
  Colg=elswhite
  SW.BREAK 
SW.CASE "1"
  Colg=elsred
  SW.BREAK 
SW.CASE "2"
  Colg=elsgreen
  SW.BREAK 
SW.CASE "3"
  Colg=elsblue
  SW.BREAK 
SW.CASE "4"
  Colg=elsyellow
SW.END 


RETURN 



Drawmap:

! DEBUG.ECHO.On
GR.TEXT.ALIGN 2
Socket.myip web$
If web$=""
   GR.TEXT.DRAW noweb, w*3/4,h/2,"No Internet Connection ;so  no maps"
   GR.RENDER 
   GR.TEXT.ALIGN 1
   GOTO scrolljump
ENDIF 
GR.TEXT.DRAW wewe, w*3/4,h/2,"Waiting for Google Map"
GOSUB zoomboxes
GR.RENDER 
GR.TEXT.ALIGN 1
 
! Get max & min lat&lon
If!maxlon&!maxlat

 Maxlat=-99
 Minlat=99
 List.size lats,latz
 For x=1 TO latz
 List.get lats,x, y
 If y<minlat then minlat=y
 If y>maxlat then maxlat=y
 NEXT x
 Maxlon=-180
 Minlon=180
 List.size lons,lonz
 For x=1 TO lonz
 List.get lons,x, y
 
 If y<minlon then minlon=y
 If y>maxlon then maxlon=y
 NEXT x



  

! Find next zoom size up
ENDIF 


! Get map centre






! Recentred means new centre this pass
! Sized means that current size is set







If (!recentred)

  If!sized
     Mlat=(maxlat+minlat) /2
     Mlon=(maxlon+minlon) /2
     Zm=19
     Do
     Zm=zm-1
     GOSUB getzoom
     UNTIL(mapwide>abs(maxlon-minlon))&~
      (maphigh>abs(maxlat-minlat))   


     Zm-- 
     Sized=1
   Endif

 Zm=zm+zozo




 If zm<1 then zm=2
 Mapwide=360/2^(zm-2)
 Maphigh=180/2^(zm-2)
Else
 Mlat=newmlat
 Mlon=newmlon
 Recentred=0


ENDIF 


! Popup int$(zm) 
Getstaticmap$(mlat,mlon,zm,640,640,"damap.map",maptype$)

Gr.bitmap.load mappyz,"damap.map"
Mappyzs++
File.delete mdel, "damap.map"

If mappyz=-1
 Popup "Error Loading Map\n"+geterror$(),, 1 
 If !notapk then exit else end
Endif
Gr.bitmap.crop bmaa,mappyz,120,0,520,640
Bmaas++

If bmaa=-1
 Popup "Error Cropping Map\n"+geterror$(),, 1 
 If !notapk then exit else end
Endif
Gr.bitmap.delete mappyz
Mappyzs--


!cropped bitmap is bmaa
Gr.bitmap.scale mappy2,bmaa,w/2,w/2
Mappy2s++

If mappy2=-1
 Popup "Error Scaling Map\n"+geterror$(),, 1 
 If !notapk then exit else end
Endif
! Scaled bm is mappy2
Mloaded=1
Gr.bitmap.delete bmaa
Bmaas--



Gr.bitmap.draw mappydz,mappy2,w/2,0
! Gr.bitmap.delete mappy2
GR.PAINT.GET psave
GR.TEXT.ALIGN 2
GR.COLOR 128,0,0,255
GR.TEXT.DRAW Cenmess,w*3/4,h*0.05,"Touch Map to recentre"
Gr.Paint.copy psave



! GR.RENDER 
RETURN 
Getzoom:
! Mapkm= (cos(toradians(mlat))*2*pi()*6378)/(256*2^(zm))*w/2
Mapwide=360/2^(zm-2)
Maphigh=180/2^(zm-2)









RetuRN 

Plotring:
! POPUP "pr" 
! At zoom 9 360 basis

!Plot nlat,nlon given center mlat,mlon
!Given center x=3/4w,y=w/4




!latitude
Dlat=nlat-mlat
Q=dlat/maphigh*w/2


Ly=w/4-q

! *(1/(cos(toradians(mlat)))) 
!Long.
Dlon=nlon-mlon
Q=dlon/mapwide*w

Lx=w*3/4+q-w*3/52



If lastlx=0
   Lastlx=lx
   Lastly=ly
ENDIF 

Gr.color 128,255,0,0,0 
If (lx>w/2)&(lx<w)&(ly>0)&(ly<w/2) then circlit=1 else circlit=0
Gr.circle cf,lx, ly,3

If !circlit then Gr.hide cf

List.add circobjs, cf

! Even if we didn't draw the circl we want the j line to it
If ((lastlx>w/2)&(lastlx<w)&(lastly<w/2)&(lastly>0)) 
  Lastjlx=lastlx
  Lastjly=lastly
Else
  Gosub chopp
Endif

Gr.line cg,lastjlx,lastjly,lx,ly


If !circlit then Gr.hide cg
List.add linobjs,cg

Lastlx=lx
Lastly=ly
! GR.RENDER 
RETURN 
Chopp:
If lastlx<w/2 then lastjlx=w/2
If lastlx>w then lastjlx=w
If lastly<0 then lastjly=0
If lastly>w/2 then lastjly=w/2
RETURN 
Zoomboxes:
GR.COLOR 255,255,255,255,1 
GR.TEXT.DRAW zmintxt, w*5/8,h*14.5/16, "Zoom In" 

GR.TEXT.DRAW zmoutxt, w*7/8,h*14.5/16, "Zoom Out" 
GR.COLOR 255,255,255,255,0
GR.TEXT.ALIGN 1
Zg=w/100
gr.text.height,zh
Gr.color 255,255,255,255,0
gr.text.width zw," Zoom Out "
Gr.rect zminboxx,w*5/8-zw/2-zg,h*14.5/16-zg+zh,w*5/8+zw/2+zg,h*14.5/16+zg
Gr.rect zmoutboxx,w*7/8-zw/2-zg,h*14.5/16-zg+zh,w*7/8+zw/2+zg,h*14.5/16+zg
GR.HIDE zmintxt
GR.HIDE zmoutxt
GR.HIDE zminboxx
GR.HIDE zmoutboxx
Gr.color 255,255,255 ,255 ,1   

! Maptype selection 
GR.TEXT.ALIGN 2
Gr.color 255,0 ,0 ,255 ,1   
GR.TEXT.DRAW mtro,w*3.5/6,h-h/36,"Roadmap"
Gr.color 255,0 ,255 ,0 ,1   
GR.TEXT.DRAW mtte,w*4.5/6,h-h/36,"Terrain"
Gr.color 255,165 ,42 ,42 ,1   
GR.TEXT.DRAW mthy,w*5.5/6,h-h/36,"Hybrid"

Gr.color 255,255 ,255 ,255 ,1   
Gr.hide mtro
GR.HIDE mtte
Gr.hide mthy
Gr.Text.align 1
Gr.text.typeface 2
GR.TEXT.SIZE h/36
GR.TEXT.DRAW botline,w/2,h-h/32,""

Gr.text.typeface 1
GR.TEXT.SIZE h/30
RETURN 
Mapmenu:


! Hide showing track
List.size circobjs,cz


For x=1 to cz
List.get circobjs,x, y
Gr.hide y
List.get linobjs,x, y
Gr.Hide y
Next x
GR.RENDER 
Gm=4
If !qnewcentre then GOSUB mpanel
! Gm 1=rapid,2=1/10,3=1/100,4=recentre,5=exit,6=help
If gm=6
  GR.HIDE mpgroup:GR.RENDER 
  GOSUB helpusall
  RETURN 
ENDIF 
If gm=1
 List.size circobjs,x
 For z=1 TO x
 List.get circobjs,z, y
 Gr.Show y
 List.get linobjs,z, y
 Gr.Show y
 NEXT z
 GR.RENDER 
 RETURN 
ENDIF 
If gm=4
   
   Gosub newcentre
   RETURN 
ENDIF 
If gm=5
 Dialog.message "Exit","Really Exit?",ok,"Ok","Cancel" 
! If ok=3 THEN run myname$
 If ok=1 

   If notapk then end
   Exit
 ENDIF 
 RETURN 
ENDIF 
If gm=2 THEN delay=1000 else delay=100
!common display routine
Plotting=1
Papa=7
Gr.show zmintxt
Gr.show zminboxx
 Gr.modify zmoutxt,"text","Quit"
! Gr.hide zmoutxt
! Gr.hide zmoutboxx
Gr.hide mtro
Gr.hide mtte
Gr.Hide mthy
Gr.show botline
GR.MODIFY zmintxt,"text","Pause"
List.size circobjs,x
For z=1 TO x
List.get circobjs,z, y
Gr.show y
List.get linobjs,z, y
Gr.show y
List.get tims, z, ty

If z=1 THEN t1=ty
Time ty,yr$,m$,d$,h$,mm$,ss$,j, k
! GR.MODIFY zmoutxt,"text",h$+":"+mm$+":"+ss$

List.get lats,z, thislat
If z=1 THEN qz=2 else qz=z
List.get lats,qz-1, thatlat
Latdiff=abs(thislat-thatlat) 
List.get lons,z, thislon
If z=1 THEN qz=2 else qz=z
List.get lons,qz-1,thatlon
Londiff=abs(thislon-thatlon)
!1 degree lat=kks(lat) km
Latkas=abs(latdiff*kks[int(abs(thislat) )]) 
! 1 degree lon is the same *cos lat
Lonkas=abs(londiff*kks[int(abs(thislat))])*cos(toradians(thislat)) 

Pyths=sqr(latkas^2+lonkas^2)
Kumquats=kumquats+pyths
If ty-t1=0 THEN ty=1
Avy=kumquats/(ty-t1)*3600000
!we did pyth k in 10 secs, so pyth*360 kph
If im$="Imperial" then pyths=pyths*5/8
L$="Speed:"+trimto$(format$("##%",pyths*360),3) +" Dist:"+trimto$(format$("##%.##",kumquats),6) +" Time:"+h$+":"+mm$+":"+ss$+" Avg:"+trimto$(format$("##%.##",avy),6) 

! GR.MODIFY zmintxt,"text",format$("##%",pyths*360)
Gr.modify botline,"text",l$








GR.RENDER 
Pause delay
If pawpaw=1
  GR.MODIFY zmintxt,"text","Resume"
  GR.RENDER 
  Pawpaw=0
  Do
  A=a
  UNTIL pawpaw
  GR.MODIFY zmintxt,"text","Pause"
  Pawpaw=0
  GR.RENDER 
Elseif Pawpaw=9
  GOTO points
ENDIF 
NEXT z
Papa=0
Kumquats=0
! Now put everything back 
GR.MODIFY zmintxt, "text", "Next"
GR.RENDER 
Do 
Gr.touch tch, tchx, tchy
UNTIL  !tch
Do 
Gr.touch tch,tchx,tchy
UNTIL tch&tchx>w*9/16&tchx<w*11/16&tchy>h*13/16&tchy<h*15/16
GR.MODIFY zmintxt,"text","Zoom In"
GR.HIDE botline
Gr.Show zmoutxt
GR.SHOW zmoutboxx
GR.SHOW mtro
GR.SHOW mtte
Gr.show mthy
GR.RENDER 
Do
Gr.touch tch,tchx,tchy
UNTIL  !tch
GR.RENDER 

RETURN 
Mpanel:
Btdta=0  %Been There Done That
If btdt
   Btdt=0
   Btdta=1
   Gm=btdtgm
Endif
If btdta then return
! Hides itself once selection made
GR.COLOR 255,255,128,128,1 
Gr.rect mpnl,w*5/8,h*3/8,w*7/8,h*6.5/8
GR.COLOR 255,0,0,0,1 
GR.TEXT.ALIGN 1
GR.TEXT.DRAW op1,w*11/16,h*13/32,"Rapid"
GR.TEXT.DRAW op2,w*11/16,h*15/32,"10 times Speed"
GR.TEXT.DRAW op3,w*11/16,h*17/32,"100 times Speed"
GR.TEXT.DRAW op4,w*11/16,h*19/32,"Close Menu"
GR.TEXT.DRAW op5,w*11/16,h*21/32,"Exit Viewer"
GR.TEXT.DRAW op6,w*11/16,h*23/32,"Help"
Gr.group mpgroup,mpnl,op1,op2,op3,op4, op5,op6
GR.RENDER 

Do
Gr.touch tch,tchx, tchy
UNTIL ! Tch
Do 
Gr.touch tch,tchx,tchy
UNTIL tch&tchx>w*5/8&tchx<w*7/8&tchy>h*3/8&tchy<h*6.5/8 

Gm=int(4*((tchy-h*3/8)/(h/4)))+1
Do
Gr.touch tch,tchx, tchy
UNTIL !Tch
GR.HIDE mpgroup
Gr.render
Btdt=1
Btdtgm=gm
RETURN 
Newcentre:
! GR.HIDE MPGROUP
GR.RENDER 


Qfrig=0
If qnewcentre
   Tchx=qnewcentrex
   Tchy=qnewcentrey
   Qnewcentre=0
   Qfrig=1
   Recentred=1
Endif
If qfrig then goto qfrigged

Gr.text.width tw,"Touch New Centre"
Popup "Touch New Centre",tw
Recentred=1
! DPP=1/PPD
Do
Gr.touch tch,tchx,tchy
UNTIL !tch
Do
Gr.touch tch,tchx,tchy
UNTIL tch

Qfrigged:
! Popup "qfr" 
Londiff=tchx-w*3/4
!thats pixels, but we want degrees
! W/2 pixels=mapwide degrees
! So we want londiff(w/2) *mapwide
! Popup str$(mapwide) 
Scfrac=520/640
Newmlon=mlon+(londiff/(w))*mapwide

Latdiff=tchy-w/4
Newmlat=mlat-(latdiff/(w/2))*maphigh




RETURN 


Ongrtouch:
Pawpaw=0
If papa<>7 THEN gr.ongrtouch.resume %not plotting

Gr.touch tch,tchx,tchy
! 
If tchy<h*7/8 THEN gr.Ongrtouch.resume
If tchx<w*9/16|tchx>w*15/16 THEN gr.ongrtouch.resume

If tchx>w*13/16
 pawpaw=9
! So he touched pause/resume
Else
Pawpaw=1
Endif
DO 
GR.TOUCH tch,tchx,tchy
UNTIL  !Tch
Gr.ongrtouch.resume

ONBACKKEY:
If backgammon 
!  Console.save "debug.txt"
  If notapk then end else exit
ENDIF 
Backgammon =1
Popup "Touch Back Key Again to Quit" 
Back.resume


Helpusall:
FILE.EXISTS fe,"hudviewhelp.html"
If fe then goto feefi
! It didnt exist & wasnt apk
If notakp 
   POPUP "Helpless" 
   Return
Endif
! Didnt exist in apk
! So open it in assets
 Text.open r, hfl,"hudviewhelp.html"


    If hfl<0
     Popup "Helplesss" 
     Return
    Endif
! It opened ok
Text.open w, nhlp, "hudviewhelp.htm"
   Do
    Text.readln hfl, bg$
    Text.writeln nhlp,bg$
   Until bg$="EOF" 
   Text.close hfl
   Text.close nhlp
    


    

 

Feefi:
HTML.OPEN 1, 0
HTML.LOAD.URL "hudviewhelp.html"

xnextUserAction:

! loop until data$ is not ""

DO
 HTML.GET.DATALINK data$
UNTIL data$ <> ""
! PRINT data$
! The first four characters of data$
! identify the type of data returned.
! Extract those three characters to
! use in a switch statement

type$ = LEFT$(data$, 4)

! Trim the first four characters from
! data$

data$ = MID$(data$,5)

! Act on the data type
! Shown are all the current data types

SW.BEGIN type$

 ! Back Key hit.
 ! if we can go back then do it
 SW.CASE "BAK:"

  IF data$ = "1" THEN
   HTML.GO.BACK
  ELSE
   HTML.CLOSE
   GOTO nomorehelp 
  ENDIF
  SW.BREAK


 ! A hyperlink was clicked on
 SW.CASE "LNK:"
 !PRINT "Hyperlink selected: "+ data$
 HTML.LOAD.URL data$
 SW.BREAK

! An error occured
SW.CASE "ERR:"

 HTML.CLOSE
 GOTO nomorehelp
 SW.BREAK


! Check for Exit
IF data$ = "Exit"

 HTML.CLOSE
 GOTO nomorehelp
ENDIF



SW.DEFAULT

  HTML.CLOSE
  GOTO nomorehelp
  END

SW.END

GOTO xnextUserAction
nomorehelp:
! PRINT    "nomo" 
ENDIF 
RETURN 

VARIANCE:
Vcount=1
Text.readln f, a$
Split v$[], a$,":"
Vlat=val(v$[3])
Vlon=val(v$[4])
Do
 Text.readln f,a$
 If left$(a$,1)="!"
   Array.delete v$[] 
   Split v$[],a$,":"
   If (val(v$[3])|val(v$[4])) then vcount++
   Vlat=vlat+val(v$[3])/100000
   Vlon=vlon+val(v$[4])/100000
 Elseif left$(a$,1)="X"
   Array.Delete v$[] 
   Split v$[],a$,":" 
   Vlaterr=(val(v$[3])-vlat)/vcount
   Vlonerr=(val(v$[4])-vlon)/vcount
 Endif
!vlaterr&vlonerr are the Degrees by which every moved plot
! Must be incremented, but CUMULATIVELY
Until left$(a$,1)="X"
text.close f
Return



