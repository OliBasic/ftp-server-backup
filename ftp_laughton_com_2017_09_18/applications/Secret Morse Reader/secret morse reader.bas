!!

===========================
    Secret Morse Reader
       ver. 1.32
           2016
  program size 84.5Kbytes
    programmed by Sedge
===========================

The Secret Morse Reader is
a program  that allows you  
to read text secretly with 
Morse  code  by  vibration

!!


!***********************
!#1   constants
!***********************

! Boolean
let True = 1 
let False = 0

Tggle  = 1 % toggle
Choice = 2 % three choices
Volsam = 3 % volume with sample
Dlog   = 4 % dialog message

Numst = 11 % number of settings
           ! including Reset

Inertia = 985 % less than 1000

! lengths of key press
Minspress = 3  % in milliseconds
Maxspress = 250 % short press
Lpress    = 800 % long press
Longer    = 1200 
 !long press for vibration mode

Samptxt$ = ~ % sample text
 "rats and cats"

Maxfile = 50
 ! maximum number of files

Stroke = 0
Fill = 1

Ed  = 1  % edit mode
Mv  = 2  % move mode
Rd  = 3  % read mode
St  = 4  % settings mode
Ts  = 5  % top switch
Bg  = 6  % background
Mlk = 7  % mode lock
Ccl = 8  % concealer
Ds  = 9  % dark screen
Cbx = 10 % choice box
Vls = 11 % volume with sample 
dim ob[11] % object numbers


! mode names
array.load Mnm$[]~
 "EDIT","MOVE","READ","SETTING"

Slt = 1 % silent mode
Ton = 2 % tone mode
Vib = 3 % vibration mode


Outpsh = 0 % outside area pushed
Tspsh  = 1 % top switch pushed
Extpsh = 2 % exit button pushed

Frmpsh = 3 % frame pushed
Txtpsh = 4 % text pushed
Addpsh = 5 % +Add button pushed
Setpsh = 6 % setting pushed
Mlkpsh = 7 % mode lock pushed
Ppbpsh = 8 % play pause button
Lkpsh  = 9 % left key pushed
Rkpsh = 10 % right key pushed
Cntpsh = 11 % contents pushed
Blnkpsh = 12 % blank pushed
Bgnpsh = 13 % begin(top) pushed
Txtsigpsh = 14 
! text signal change button

! text status
None   = 0
Plain  = 1 
Redng  = 2 % reading-green frame
Chosen = 3 % orange colored
Rd_cs  = 4 % reading & chosen


Barmvint = 1/3
! bar move animation interval 
! 1/3 is maximum


dim Sname$[Numst], ~% setng name
 Skind[Numst],~% kind of setting
 Sep$[Numst - 1],~%set each path
 Dflt[Numst - 1],~% default
 Mins[Numst - 1],~% min of setng
 Maxs[Numst - 1]  % max of setng
 

dim Ssts$[1, 3]

Ssts$[1, 1] = "Silent"
Ssts$[1, 2] = "Tone"
Ssts$[1, 3] = "Vibration"


Cp$ = "smorsereader/"
 ! common path
Pspt$ = Cp$ + "textfile/pos/"
 ! reading position path
Txtpt$ = Cp$ + "textfile/text/"
 ! text path
Fopt$ = Cp$ + "fileorder"
Drpt$ = Cp$ + "draft"
Crpt$ = Cp$ + "current/"
Stpt$ = Cp$ + "settings/"

file.mkdir Pspt$
file.mkdir Txtpt$
file.mkdir Crpt$
file.mkdir Stpt$


Minv = 5 % minimum velocity

Spcunit = 200 
! length of space(silence) unit
! in milliseconds




!******************************
!#2 definition of functions ***

fn.def lrkeytxtcolor()
gr.color 230, 30, 100, 200, 1
fn.end

fn.def safewrite(aa$, bb$)
! string names (fn$, s$) 
! didn't work

obck = 0 % backkey
pending = 0
 ! pending backkey
 
saving = 1 % True

text.open w, ftn, aa$
text.writeln ftn, bb$
text.close ftn

saving = 0 % False

if pending then
 gosub Backkeyexecution
 pending = 0
endif

fn.end



! improved drawinto.start
fn.def bdstart(bmp, flag, index)

if flag then

 print "overlap of " + ~
  "bitmap.drawinto.start"
 print "index : ";index
 END
 
endif

gr.bitmap.drawinto.start bmp

flag = 1 % True

fn.end



! improved drawinto.end
fn.def bdend(flag, index)

if !flag then

 print "bitmap.drawinto." + ~
  " not started"
 print "can't end"
 print "index : ";index
 END
 
endif

gr.bitmap.drawinto.end

flag = 0 % False

fn.end



! limits index for array
fn.def within(n, r)

while n < 1
 n += r
repeat 

while n > r
 n -= r
repeat

fn.rtn n

fn.end


! text color in move mode
fn.def mvtxtcolor() 
gr.color 255, 0, 30, 60, 1
fn.end


! background color in read mode
fn.def rdcolor(rdtxt, numfl)

if mod(rdtxt, 2) = 1 ~
 | numfl = 0 then % odd number
 
 edcolor()
 
else % text number is even

 gr.color 255, 150, 180, 220, 1
 
endif

fn.end



! background color
fn.def bgcolor()
gr.color 255, 50, 50, 80, 1
fn.end


! top switch color
fn.def tpswcolor()
gr.color 200, 255, 150, 150, 1
fn.end


!texts are changeable 
!if there are more than one text
fn.def changbl(numfl)

if numfl = 0 then

 popup "There's no text", 0,0,0
 f = 0 % False
  
elseif numfl = 1 then 
   
 popup"This is the only text", ~
  0, 0, 0
 f = 0 % False
  
else
   
 f = 1 % True
   
endif 
   
fn.rtn f

fn.end


! response vibration
! to key touch
fn.def respvib(long)

undim v[]
dim v[2]
v[1] = 1

if long then
 v[2] = 400
else
 v[2] = 100
endif

vibrate v[], -1 % -1 = play once

fn.end


!calculates x from read position
fn.def redx(rpos, Rdlts)
rx = mod((rpos - 1), Rdlts) + 1
fn.rtn rx
fn.end

fn.def opaque()
gr.color 255
fn.end

fn.def nearop() % near opaque
gr.color 247
fn.end

fn.def white()
gr.color 255, 255, 255, 255, 1
fn.end


!fills bitmap with certain color
fn.def bmfill(bmp, bdf)
   ! bdf: bitmap drawinto flag
gr.bitmap.size bmp, bw, bh  
bdstart(bmp, &bdf, 0)
gr.rect iv, 0, 0, bw, bh

fn.end


! fills the bitmap with black
fn.def bmclear(bmp, bdf)

gr.color 255, 0, 0, 0, 1 % Fill
bmfill(bmp, &bdf)

fn.end



! creates and fills a bitmap
fn.def bmcreatefill ~
 (xsize, ysize, bdf)

gr.bitmap.create bmp, xsize, ~
 ysize

bdstart(bmp, &bdf, 0)

gr.rect iv, 0, 0, xsize, ysize

fn.rtn bmp

fn.end



! calcultes x position on slide
fn.def stvtosldx(stv, min, max,~
 Sldmin, Sldmax)

wk = (stv - min) / (max - min)
sldx = Sldmin + ~
 (Sldmax - Sldmin) * wk

fn.rtn sldx

fn.end


! fades dark screen
!  True  > fades in to half dark
!  False > fades out
fn.def fade(op, flg)

if flg then

 for i = 30 to 150 step 30

  gr.modify op, "alpha", i
  gr.render
       
 next

else

 for i = 120 to 0 step -30

  gr.modify op, "alpha", i
  gr.render
       
 next

endif

fn.end



! color of the sample button
fn.def sambtncolor()
gr.color 255, 190, 240, 150, 1
fn.end


fn.def rstcolor() % reset color
gr.color 200, 150, 0, 0, 1
fn.end


! color of settings name
fn.def stnamecolor()

gr.color 255, 30, 30, 30, 1
gr.text.align 1 % left
gr.text.setfont "sans serif","B"

fn.end


! dark line color of settings
fn.def stlinecolord() 
gr.color 20, 50, 50, 70, 1
fn.end


! bright line color of settings
fn.def stlinecolorb() 
gr.color 80, 220, 255, 220, 1
fn.end



! color of edit and other modes
fn.def edcolor()
gr.color 255,150,150,180,1 %Fill
fn.end

! color of settings mode
fn.def stcolor()
gr.color 255, 150, 170, 180, 1
fn.end

! toggle slit color
fn.def tglslitcolor()
gr.color 255, 120, 120, 130, 1
fn.end

fn.def tglslitcontour()
gr.color 255, 200, 255, 200, 0
fn.end


fn.def tglslitbright()
gr.color 255, 0, 100, 200, 1
fn.end


fn.def tgllampcolor()
gr.color 255, 0, 150, 255, 1
fn.end


! color of left right keys
fn.def lrkeycolor()
gr.color 255, 200, 230, 240, 1
fn.end



! prints "LONG PRESS" on keys
fn.def longpress(x, y, sz)

Lp$ = "LONG PRESS"
gr.text.size sz
gr.text.align 2 % center
gr.text.setfont "sans serif"

gr.color 100,0,0,0,1 % Fill
gr.text.draw iv, x, y, Lp$

fn.end


! value function avoids error
fn.def value(vs$)

l$ = left$(vs$, 1)

if "0" <= l$ & l$ <= "9" then
 v = val(vs$)
else
 v = 0
endif

fn.rtn v

fn.end




! reads one data from a file
fn.def red(fn$, def, min, max)
 !  filename default min max

text.open r, ftn, fn$

if ftn <> -1 then % if exists

 text.readln ftn, wk$
 text.close ftn
 v = value(wk$)

 if v < min | max < v then
  r = def
 else
  r = v
 endif

else % the file doesn't exist

 r = def % takes default value

endif
 
fn.rtn r

fn.end



fn.def whattime$()

time year$, month$, ~
 day$, hour$, minute$, second$
t$ = year$ + month$ + day$ + ~
 hour$ + minute$ + second$

fn.rtn t$

fn.end


! calculates text number from y
fn.def ytotxt(y, Topmged, ~
 Inted, Htfed)

txt = floor((y - Topmged + ~
 Inted / 2) / ~
 (Inted + Htfed) + 1)

fn.rtn txt

fn.end


! box bottom y 
fn.def calc_bxbtmy(htbx, H)

bxbtmy = htbx - H * 1.2
fn.rtn bxbtmy

fn.end



!**********************
! Go To Initialization
!vvvvvvvvvvvvvvvvvvvvvv

goto Initialization



! ========================

! *******************
!#3 subroutines   ***
! *******************

Conceal :

bdstart(bpccl, &bdf, 0)
bgcolor()
gr.rect iv, 0, Topclip + Lrkeyt~
 , Width, Height
rdcolor(rdtxt, numfl)
nearop()
gr.rect iv, 0, Topclip + Lrkeyt~
 , Width, Height
bdend(&bdf, 0)

if grdal then

 for i = 63 to 255 step 64
  gr.modify ob[Ccl], "alpha", i
  gr.render
 next
 
else

 gr.modify ob[Ccl], "alpha", 255
 gr.render
  
endif

return



Appear :

for i = 192 to 0 step -64
 gr.modify ob[Ccl], "alpha", i
 gr.render
next

return



OnError:

err$ = geterror$()

if is_in~
 ("Invalid Bitmap Pointer",err$)~
 > 0 then

 err$ += "\nThe program failed"~
  + " to create a new bitmap\n"~
  + "Please close other apps to"~
  + " reduce memory usage or\n"~
  + "Restart your phone " ~ 
  + "if necessary"

endif

dialog.message "Error", err$, ~
 n, "Exit"
 
! Exit command here in apk ver.
END % ********************* END


Wheremv : 
! where pushed in move mode

opsh = psh
 ! first value is invalid

if ty < Topclip then
 gosub Modepush

elseif Extleft < tx & ~
 tx < Extright & ~
 Exttop + Topclip <ty & ~
 ty < Extbottom + Topclip then

 psh = Extpsh % exit pushed
 
elseif Bgnl < tx & tx < Bgnr & ~
 Bgnt + Topclip < ty & ~
 ty < Bgnb + Topclip then
 
 psh = Bgnpsh %top button pushed
 
elseif Txtsigl < tx & ~
 tx < Txtsigr & ~
 Txtsigt + Topclip < ty & ~
 ty < Txtsigb + Topclip then
 
 psh = Txtsigpsh % change pushed

! text-signal window pushed
elseif Mvwinl < tx & ~
 tx < Mvwinr & ~
 Mvwint + Topclip < ty & ~
 ty < Mvwinb + Topclip then
 
 txx = tx - Mvwinl
 tyy = ty - Topclip - Mvwint

 if sigf then % when signal
 
  psh = Blnkpsh % interim value
  pshpos = ceil((tyy - ~
   (mvsy - Mvsiglf / 2)) / ~
   Mvsiglf)
 
  if 1 <= pshpos & ~
   pshpos <= len(rdtxt$) & ~
   Mvsiglf / 2 < tyy & ~
   tyy < Mvwinh - Mvsiglf / 2 ~
   then
   
   wk = csrtop + ~
    (pshpos - topletter)
   wk = within(wk, Csrnum)
   cursorr = csr[wk]
   if Csl - W < txx & ~
    txx < cursorr + W then
    psh = Cntpsh
   endif
   
  endif
 
 else
 
  pshlin = ceil((tyy - (mvty + ~
   (Mvtxtlf - Mvd_u) / 2)) / ~
   Mvtxtlf) + 1
  
  pshltr = ~
   ceil((txx - Mvtxtlftmgn) / ~
   Mvtxtw) 
 
  pshpos = (pshlin - 1) * Mvlts~
   + pshltr
 
  if 1 <= pshpos & ~
   pshpos <= len(rdtxt$) & ~
   1 <= pshltr & ~
   pshltr <= Mvlts then   
   
   psh = Cntpsh   

  else  
   psh = Blnkpsh
  endif
   
 endif

else

 psh = Outpsh

endif

return
 



! draws frame and gradation
! for left right keys
Lrkeyframe : 

gr.set.stroke 1
gr.color , 0, 50, 120

al = 0
for ii = 0 to Lrkeyh
 gr.color al
 
 if ii < Lrkeyh * 0.6 then
  al += 60 / Lrkeyh
 else
  al += 100 / Lrkeyh
 endif
 
 gr.line iv, 0, ii, Lrkeyw, ii
next

gr.color 50, 0, 120, 120, Stroke
gr.set.stroke W / 12
gr.rect iv, 0, 0, Lrkeyw, Lrkeyh

return



! initialization for reading
Iniforread :

rtxt$ = rdtxt$
! rtxt$ is reading text for
! Tread subroutine

drsig = True
skipspc = False
jstwspc = False

jstc = False % just touched
ltc = False % last touch

jstplyd = False 
 ! just played
  
plyend = False

drawf = True

return


Putml : % puts mode lock

for itr = -Topclip to 0 ~
 step H / 4

 gr.modify ob[Mlk], "y", itr
 gr.render

next
  
mlocked = True
 
return
 
 
 

Rmoveml : % removes mode lock

for itr = 0 to -Topclip ~
 step -H / 2

 gr.modify ob[Mlk], "y", itr
 gr.render

next
  
mlocked = False
 
return
 


Longprs : % awaits long press

lpflag = True
inipsh = psh % initial push
clk = clock()

do

 gr.touch touched, tx, ty
 if touched then
 
  gosub Whererd

  if psh <> inipsh then
   lpflag = False
  endif
  
 else
 
  lpflag = False
  
 endif

until clock() - clk > Longer | ~
 (!lpflag)

return



% draws signal in move mode
Drmvsig :

bdstart(bpmvwin, &bdf, 0)
white()
gr.rect iv, 0, starty, ~
 Mvwinw, eendy

! calculates start y position
wk = floor((mvsy - ~
 (starty - Mvsigbrdt))/ Mvsiglf)
sigy = mvsy - Mvsiglf * wk
mvchr = 1 - wk

if tobtm then

 csri = ~
  csrtop + (mvchr - topletter)
 csri = within(csri, Csrnum)

else

 csrtop -= topletter - mvchr
 csrtop = ~
  within(csrtop, Csrnum)
 topletter = mvchr
 csri = csrtop

endif

while sigy < eendy + Mvsigbrdt

 if mvchr > 0 then

  sigx = Mvsiglftmgn
  sigpos = mvchr
  gosub Tread
  
  if cursorr < Csl+Mvsiglf then
   cursorr = Csl + Mvsiglf
  elseif cursorr > Mvwinw then
   cursorr = Mvwinw
  endif
 
  csr[csri] = cursorr
    
  if mvchr = rpos then
   gr.color 100, 255, 0, 0,Fill 
   gr.rect iv, Csl, ~
    sigy - Mvsiglf / 2, ~
    cursorr, sigy + Mvsiglf / 2
   opaque() % restores opacity
   gosub Drsignal % again
  endif
  
 endif
 
 sigy += Mvsiglf
 mvchr++
 csri++
 if csri > Csrnum then
  csri = 1
 endif
 
repeat

bdend(&bdf, 0)

return




% draws text in move mode
Drmvtxt : 

bdstart(bpmvwin, &bdf, 0)
white()
gr.rect iv, 0, starty, ~
 Mvwinw, eendy

! calculates start y position
wk = floor((mvty-(starty-Mvd)) ~
 / Mvtxtlf) 
y = mvty - Mvtxtlf * wk
mvlin = 1 - wk

gr.text.align 1 % left
gr.text.size Mvtxth
gr.text.setfont "monospace"

mvtxtcolor()
while y < eendy - Mvu

 mvchr = (mvlin - 1) * Mvlts + 1

 if mvlin > 0 then

  x = Mvtxtlftmgn
 
  for i = 1 to Mvlts
  
   if mvchr = rpos then
    gr.color 100, 255, 0, 0,Fill 
    gr.rect iv, x, y + Mvu, ~
     x + Mvtxtw, y + Mvd
    mvtxtcolor()
   endif
  
   gr.text.draw iv, x, y,~
    mid$(rdtxt$, mvchr++, 1)
   x += Mvtxtw
  next
 
 endif
 
 y += Mvtxtlf
 mvlin++

repeat

bdend(&bdf, 0)

return




% draws all move mode 
Drawmvall :

starty = 0
eendy = Mvwinh

if sigf then

 csrtop = 1
 topletter = top
 tobtm = False
 
 gosub Drmvsig
 
else

 gosub Drmvtxt
 
endif

gosub Copytxtsig

return



! copies text or signal
! to the main bitmap
Copytxtsig :

edcolor()
bmfill(bpmv, &bdf)

gr.bitmap.draw iv, bpbgn, ~
 Bgnl, Bgnt
gr.bitmap.draw iv, bpext, ~
 Extleft, Exttop

opaque()
gr.bitmap.draw iv, bpmvwin, ~
 Mvwinl, Mvwint

if sigf then

 gr.bitmap.draw iv, bptotxt, ~
  Txtsigl, Txtsigt
  
else

 gr.bitmap.draw iv, bptosig, ~
  Txtsigl, Txtsigt
  
endif

bdend(&bdf, 0)

return



! where in read mode
Whererd :

tyy = ty - Topclip

if ty < Topclip then

 if rmode = Vib then
 
  if black then
   psh = Mlkpsh
  elseif ccld then
   psh = Outpsh
  elseif mlocked then
   psh = Mlkpsh
  else 
   gosub Modepush
  endif
  
 else
  
  gosub Modepush
  
 endif
 
elseif Ppbl < tx & tx < Ppbr & ~
 Ppbt < tyy & tyy < Ppbb then
 
 psh = Ppbpsh

elseif Lrkeyt < tyy & ~
 tyy < Lrkeyb then
 
 if ccld then
 
  psh = Outpsh
  
 else
 
  if Lkeyl < tx & tx < Lkeyr ~
   then
 
   psh = Lkpsh
 
  elseif Rkeyl < tx & ~
   tx < Rkeyr then
 
   psh = Rkpsh
 
  else

   psh = Outpsh
   
  endif
  
 endif
 
else

 psh = Outpsh

endif

return




! draws a triangle for play
Drplay :

bdstart(bpreado, &bdf, 0)

gr.color 50, 50, 200, 200, Fill
gr.circle iv, Ppbcx, Ppbcy, ~
 Ppbrd
gr.color 100, 150, 50, 50, Fill
gr.poly iv, btrp, Ppbcx, Ppbcy
 
if rmode = Vib then 
 longpress(Ppbcx, ~
  Ppbcy - H * 0.2, H / 3)
endif

bdend(&bdf, 0)

return



! draws two rectangles for pause
Drpaus :

bdstart(bpreado, &bdf, 0)

gr.color 40, 180, 180, 0, Fill
gr.circle iv, Ppbcx, Ppbcy, ~
 Ppbrd
gr.color 80, 50, 100, 0, Fill
gr.rect iv, Ppbcx - W, ~
 Ppbcy - H * 1.1, ~
 Ppbcx - W * 0.4, ~
 Ppbcy + H * 1.1
gr.rect iv, Ppbcx + W * 0.4, ~
 Ppbcy - H * 1.1, Ppbcx + W, ~
 Ppbcy + H * 1.1
 
if rmode = Vib then 
 longpress(Ppbcx, ~
  Ppbcy - H * 0.2, H / 3)
endif

bdend(&bdf, 0)

return



! draws play or pause symbol
Plyorpaus :

if jstplyd then

 gosub Drplay
 
else

 gosub Drpaus
 
endif

return



! gets string and read position
Rdtxttotxtdata :

if numfl > 0 & rdtxt > 0 then

 list.get tptr, rdtxt, rdtxt$
 list.get pospt, rdtxt, rpos
 
else

 rdtxt$ = ""
 rpos = 1
 
endif

return



!initializes move mode variables
Inimvvar : 

mvlstlin = int((len(Rdtxt$) ~
 - 1) / Mvlts) + 1
  
if Mvtxttpmgn + ~
 Mvtxtlf * (mvlstlin - 1) ~
 + Mvd_u + Mvtxtbtmmgn > ~
 Mvwinh then
 
 minty = Mvwinh - Mvtxtbtmmgn ~
  - Mvd - ~
  Mvtxtlf * (mvlstlin - 1) 
  
else

 minty = Maxty
 
endif


if Mvsigtpmgn + ~
 Mvsiglf * (len(Rdtxt$) - 1) ~
 + Mvsigbtmmgn > Mvwinh then
 
 minsy = Mvwinh - Mvsigbtmmgn ~
  - Mvsiglf * (len(Rdtxt$) - 1)
  
else

 minsy = Maxsy
 
endif

return



! gets y for the signal screen
! from text screen top position
Toptomvsy :

if top < 1 then 
 top = 1
endif
   
if Mvsigtpmgn + ~
 Mvsiglf * (len(rdtxt$) - top) ~
 < Mvwinh - Mvsigbtmmgn then
 
 if Mvsigtpmgn + ~
  Mvsiglf * (len(rdtxt$) - 1) ~
  + Mvsigbtmmgn > Mvwinh then
 
  neartp = False
  nearbtm = True
  
 else
 
  neartp = True
  nearbtm = False
   
 endif
 
else

 neartp = False
 nearbtm = False
 
endif


if neartp then 

 mvsy = Mvsigtpmgn
 wk = 1
 
else

 if nearbtm then

  mvsy = Mvwinh - Mvsigbtmmgn
  wk = len(rdtxt$)
 
 else
  
  mvsy = Mvsigtpmgn
  wk = top
 
 endif

 mvsy -= (wk - 1) * Mvsiglf
 !y position of the first letter
 
endif

return 




! calculates y for text screen
Toptomvty : 


if top < 1 then 
 top = 1
endif

tlin = int((top - 1) / Mvlts) ~
 + 1
tpx = redx(top, Mvlts)
 
  
if Mvtxttpmgn + Mvd_u + ~
 Mvtxtlf * (mvlstlin - tlin) < ~
 Mvwinh - Mvtxtbtmmgn then
 
 if Mvtxttpmgn + Mvd_u + ~
  Mvtxtlf * (mvlstlin - 1) ~
  + Mvtxtbtmmgn > Mvwinh then
 
  neartp = False
  nearbtm = True
   
 else
 
  neartp = True
  nearbtm = False
   
 endif
 
else

 neartp = False
 nearbtm = False
 
endif


if neartp then 

 mvty = Mvtxttpmgn - Mvu
 wklin = 1
 
else

 if nearbtm then

  mvty = Mvwinh - ~
   Mvtxtbtmmgn - Mvd
  wklin = mvlstlin
 
 else
  
  mvty = Mvtxttpmgn - Mvu
  wklin = tlin
 
 endif

 mvty -= (wklin - 1) * Mvtxtlf
 ! y position of the first line
 
endif


return



! gets y for text screen
! from the center position
Centomvty : 


if cen < 1 then 
 cen = 1
endif

clin = int((cen - 1) / Mvlts) ~
 + 1
cx = redx(cen, Mvlts)
 
 
if Mvcen + Mvd - Mvtxtlf * ~
 (clin - 1) - Mvd_u > Mvtxttpmgn~
 then
 
 neartp = True
 nearbtm = False
 
elseif Mvcen + Mvd + ~
 Mvtxtlf * (mvlstlin - clin) < ~
 Mvwinh - Mvtxtbtmmgn then
 
 if Mvtxttpmgn + Mvd_u + ~
  Mvtxtlf * (mvlstlin - 1) ~
  + Mvtxtbtmmgn > Mvwinh then
 
  neartp = False
  nearbtm = True
   
 else
 
  neartp = True
  nearbtm = False
   
 endif
 
else

 neartp = False
 nearbtm = False
 
endif



if neartp then 

 mvty = Mvtxttpmgn - Mvu
 wklin = 1
 
else

 if nearbtm then

  mvty = Mvwinh - ~
   Mvtxtbtmmgn - Mvd
  wklin = mvlstlin
 
 else
  
  mvty = Mvcen
  wklin = clin
 
 endif

 mvty -= (wklin - 1) * Mvtxtlf
 ! y position of the first line

endif

return 



! position for read mode window2
Poswin2 :

if rpos < 1 then 
 rpos = 1
endif

rlin = int((rpos - 1) / Rdlts) ~
 + 1
rx = redx(rpos, Rdlts)
 
return 



!draws window2 text in read mode
Drwin2txt :
 
if Win2cen + Win2d - ~
 Win2d_u * rlin > Win2tpmgn then
 
 neartp = True
 nearbtm = False
 
elseif Win2cen + Win2d + ~
 Win2d_u * (lstlin - rlin) < ~
 Win2h - Win2btmmgn then
 
 if Win2tpmgn + ~
  Win2d_u * lstlin ~
  + Win2btmmgn > Win2h then
 
  neartp = False
  nearbtm = True
  
 else
 
  neartp = True
  nearbtm = False
  
 endif
 
else

 neartp = False
 nearbtm = False
 
endif


gr.color 255, 0, 0, 0, Fill
gr.text.size Win2txth
gr.text.setfont "monospace"
gr.text.align 1 % left

if neartp then 

 y = Win2tpmgn - Win2u
 drlin = 1
 
else

 if nearbtm then

  y = Win2h - Win2btmmgn - Win2d
  drlin = lstlin
 
 else
  
  y = Win2cen
  drlin = rlin
 
 endif

 y -= Win2d_u
 while y > -Win2d

  drlin--
  y -= Win2d_u
  
 repeat
 y += Win2d_u

endif

gr.bitmap.delete bpwin2txt
gr.bitmap.create bpwin2txt, ~
 Win2w, Win2h
bdstart(bpwin2txt, &bdf, 0)


drchr = (drlin - 1) * Rdlts + 1

do

 x = Win2lftmgn
 
 for i = 1 to Rdlts
  gr.text.draw iv, x, y, ~
   mid$(rdtxt$, drchr++, 1)
  x += Win2txtw
 next
 
 if drlin = rlin then
  csy = y + Win2d
 endif
 
 y += Win2d_u
 drlin++

until y > Win2h - Win2u | ~
 drlin > lstlin


bdend(&bdf, 0)

return



! unites window2 in read mode
! (background and text)
Unitewin2 :

white()
bmfill(bpwin2, &bdf)

rx = redx(rpos, Rdlts)
csx = Win2txtw * (rx - 1) + ~
 Win2lftmgn

gr.color 100, 255, 0, 0, Fill
gr.rect iv, csx, csy - Win2d_u,~
 csx + Win2txtw, csy

opaque()
gr.bitmap.draw iv, bpwin2txt, ~
 0, 0

bdend(&bdf, 0)

bdstart(bpreado, &bdf, 0)
opaque()
gr.bitmap.draw iv, bpwin2, ~
 Win2l, Win2t
bdend(&bdf, 0)

return



! draws all window2 in read mode
Drwin2all : 

gosub Poswin2
gosub Drwin2txt
gosub Unitewin2

return



Drwin2con : % conditional

gosub Poswin2

if rx = 1 then
 gosub Drwin2txt
endif

gosub Unitewin2

return



! draws top window in read mode
Drwin1 :

bdstart(bpreado, &bdf, 0)
opaque()
gr.bitmap.draw iv, bpwin1, ~
 Win1l, Win1t
bdend(&bdf, 0)

return


! clears window1 in read mode
Clwin1 :

white()
bmfill(bpwin1, &bdf)
bdend(&bdf, 0)

gosub Drwin1


return



! draws read mode, copies lrkeys
Drawrdcp :

rdcolor(rdtxt, numfl)
bmfill(bpreado, &bdf)
bdend(&bdf, 0)
gosub Drwin1
gosub Drwin2con
gosub Plyorpaus
gosub Copybpread
gosub Copylrkey

return



! draws read mode
Drawrd :

rdcolor(rdtxt, numfl)
bmfill(bpreado, &bdf)
bdend(&bdf, 0)
gosub Drwin1
gosub Drwin2con
gosub Plyorpaus
gosub Copybpread
gosub Drawlrkey

return



Inireado : %initializes original

lstlin = int((len(Rdtxt$) - 1) ~
 / Rdlts) + 1
rdcolor(rdtxt, numfl)
bmfill(bpreado, &bdf)
bdend(&bdf, 0)
gosub Clwin1
gosub Drwin2all
gosub Drplay

return



Inicopy : % initializes & copies

gosub Inireado
gosub Copybpread
gosub Drawlrkey

return



! copies left right keys
Copylrkey : 

bdstart(bpreadc, &bdf, 0)
nearop()
gr.bitmap.draw iv, bplk, ~
 Lkeyl, Lrkeyt
gr.bitmap.draw iv, bprk, ~
 Rkeyl, Lrkeyt
bdend(&bdf, 0)
 
return



! draws left and right keys
Drawlrkey :

! left key
lrkeycolor()
bmfill(bplk, &bdf)
gosub Lrkeyframe
gr.color 200,255,100,100, Fill
gr.poly iv, trp, Lrkeyw / 2 , ~
 H * 0.7

if rmode = Vib then 
 longpress(Lrkeyw / 2, H * 1.5,~
  H / 3.5)
endif

lrkeytxtcolor()
gr.text.size H * 0.4
gr.text.setfont "sans serif","B"
gr.text.align 2 % center
gr.text.draw iv, Lrkeyw / 2, ~
 H * 0.55, "Previous"

bdend(&bdf, 0)

! right key
lrkeycolor()
bmfill(bprk, &bdf)
gosub Lrkeyframe
gr.color 200, 30, 170, 120, Fill
gr.poly iv, rtrp, Lrkeyw / 2 , ~
 H * 0.75

if rmode = Vib then 
 longpress(Lrkeyw / 2, H * 1.5,~
  H / 3.5)
endif

lrkeytxtcolor()
gr.text.size H * 0.4
gr.text.setfont "sans serif","B"
gr.text.align 2 % center
gr.text.draw iv, Lrkeyw / 2, ~
 H * 0.55, "Next"

bdend(&bdf, 0)

gosub Copylrkey



return
 
 


! copies bitmap in read mode
Copybpread :

opaque()
bdstart(bpreadc, &bdf, 0)
gr.bitmap.draw iv, bpreado, 0, 0
bdend(&bdf, 0)

return



Vibptn :
! prepares constants for 
! vibration & tone

if rmode = Vib then

 undim vdot[], vdash[]
 dim vdot[2], vdash[2]
 
 vdot[1] = 1
 vdot[2] = vlen * ldot
  
 vdash[1] = 1
 vdash[2] = vlen * ldash

 sspc = lsspace * vlen
 lspc = llspace * vlen
 wspc = lwspace * vlen

else

 tdot = tlen * ldot
 tdash = tlen * ldash

 if tdot  < 1 then 
  tdot  = 1
 endif
 if tdash < 1 then
  tdash = 1
 endif


 sspc = lsspace * tlen
 lspc = llspace * tlen
 wspc = lwspace * tlen

endif

return


Samtxtcl :
! clears screen for sample text

bdstart(bpvls, &bdf, 0)
stcolor()

gr.rect iv, 0, ~
 Sldcy + Sldrd + H / 2 , ~
 Wdbx, Samt - H / 4

bdend(&bdf, 0)

return



Seginc : % increases segment

rseg++
seghead = True

if rseg > len(sig$) then

 drawf = True
 rseg = 1
 

 rpos++ % end reading
 if rpos > len(rtxt$) then

  rpos = 1
  plyend = True

 endif
 
endif

return




Paus : % pause

if skipspc then

 gosub Seginc
 
else

 if spclen > spcunit then

  pause spcunit

  spclen -= spcunit

 elseif spclen > 0

  pause spclen
  
  gosub Seginc

 else

  gosub Seginc

 endif

endif

return




Tread : % text read

! converts letters to signal

if rseg = 1 & seghead | ~
 mode = Mv then

 m$ = mid$(rtxt$, sigpos, 1)

 list.search alp, m$, alpi
 
 if alpi >= 1 then
  list.get sigp, alpi, sig$

  l$ = left$(sig$, 1)
  if l$ <> "." & l$ <> "-" & ~
   l$ <> "_" then % not signal

   wk$ = sig$
   sig$ = ""
   l = len(wk$)

   for i = 1 to l

    md$ = mid$(wk$, i, 1)

    list.search alp, md$, alpi

    if alpi >= 1 then

     list.get sigp, alpi, s$
     sig$ += s$

     if i < l then
      sig$ += "/"
     endif

    else

     wk2$ = upper$(md$)

     list.search alp, wk2$, alpi
 
     if alpi >= 1 then

      list.get sigp, alpi, s$
      sig$ += s$

      if i < l then
       sig$ += "/"
      endif
     
     endif  

    endif

   next

   if l > 1 then % e.g. Yen
    m$ = wk$
   endif

  endif

 else % lower-case or unknown

  wk$ = upper$(m$)

  list.search alp, wk$, alpi
 
  if alpi >= 1 then
   list.get sigp, alpi, sig$
   
   l$ = left$(sig$, 1)
   if l$ <> "." & l$ <> "-" & ~
    l$ <> "_" then % not signal
   
    list.search alp, sig$, alpi
 
    if alpi >= 1 then
     list.get sigp, alpi, sig$
    else
     sig$ = ""
    endif
    
   endif
    
  else
   sig$ = ""
  endif

 endif

endif


if mode = Rd | mode = St then 
 
 if rseg = 1 & seghead & drawf ~
  then 
 
  if mode = St then
 
   bdstart(bpvls, &bdf, 0)
  
  elseif mode = Rd then
   ! scrolls morse signals up
  
   bdstart(bpwin1scr, &bdf, 0)
   opaque()
   gr.bitmap.draw iv, bpwin1, ~
    0, - Win1linh    
   bdend(&bdf, 0)
  
   bdstart(bpwin1, &bdf, 0)
   white()
   gr.rect iv, 0, ~
    Win1scrh, Win1w, Win1h
   gr.bitmap.draw iv, bpwin1scr,~
    0, 0
 
  endif

  gr.text.align 1 % left
  gr.text.setfont "monospace"
  
  if mode = St then
   gr.color 255, 0, 0, 0, Fill
   gr.text.size H / 2
  elseif mode = Rd
   gr.color 200, 0, 0, 50, Fill
   gr.text.size Win1txth
  endif

  gr.text.draw iv, morsex, ~
   morsey, m$

  gr.text.width wk, m$
  gr.text.height ht

  sigx = morsex + wk + W / 2
  sigy = morsey - ht * 0.3
 
  gosub Drsignal
 
  bdend(&bdf, 0)

  if mode = Rd then 
   gosub Drawrdcp
  endif

  gr.render
  drawf = False

 endif

 gosub Treadsub
 
elseif mode = Mv then
 
 gosub Drsignal
 
endif

return




Drsignal : % draws signal

x = sigx
y = sigy

if mode = St then

 brdt = H / 8
 wdth = W / 8
 gr.color 220, 0, 50, 0, Fill
  
elseif mode = Rd then

 brdt = H / 5
 wdth = W / 5

elseif mode = Mv then

 brdt = Mvsigbrdt 
 wdth = Mvsigwdth 

endif

gr.set.stroke brdt

for i = 1 to len(sig$)

 sw.begin mid$(sig$, i, 1)

 sw.case "."

  if i = 1 then % aligns left
   x += brdt / 2
  endif
  
  if mode = Mv then
   gr.color 255, 170, 0, 0, Fill
  endif
  
  gr.circle iv, x, y, brdt / 2
  x += wdth * 1.5

 sw.break

 sw.case "-"
 
  if mode = Mv then
   gr.color 255, 0, 70, 120,Fill
  endif
  
  gr.line iv, x, y, x + ~
   wdth * 3, y
  x += wdth * 4.5

 sw.break

 sw.case "/"

  x += wdth * 2

 sw.break

 sw.end

next

cursorr = x
 
return



Treadsub :
!subroutine of text read(Tread:)

if seghead then

 seg$ = mid$(sig$, rseg, 1)

 if rseg = len(sig$) then
  spclen = lspc
 else 
  spclen = sspc
 endif

 !seghead = False

 if seg$ = "." then

  if rmode = Vib then

   vibrate vdot[], -1

  elseif rmode = Ton then

   tone pitch * pitch, tdot, ~
    False

  elseif rmode = Slt

   spclen += tdot

  endif

  skipspc = False
  jstwspc = False

 elseif seg$ = "-" then

  if rmode = Vib then

   vibrate vdash[], -1

  elseif rmode = Ton then

   tone pitch * pitch, tdash, ~
    False

  elseif rmode = Slt

   spclen += tdash

  endif

  skipspc = False
  jstwspc = False

 elseif seg$ = "/" then
 ! long space (between letters)

  spclen = lspc - sspc

 elseif seg$ = "_" then
 ! white space (between words)

  spclen = wspc - lspc

  if jstwspc then
   skipspc = True
  endif
  jstwspc = True

 else

  spclen = 1 % pass

 endif

 seghead = False

else

 gosub paus

endif

return




Drcbx : % draws the choise box

htbx = Htcbx
btitle$ = Sname$[sti]
gosub Drbox

cbxint = (bxbtmy - Bxtopy) / 3.5
 ! line interval of choice box

gr.text.size H * 0.425

for i = 1 to 3

 wk = Bxtopy + cbxint * i 
 stnamecolor()
 !gr.text.setfont ,"N"
 gr.text.draw iv, W, ~
  wk - H * 0.5, Ssts$[sti, i]

 if i < 3 then

  stlinecolorb()
  gr.set.stroke H / 32
  gr.line iv, 0, ~
   Bxtopy + cbxint * i, Wdbx, ~
   Bxtopy + cbxint * i

 endif


next

return




Drsld : % draws the slide

bdstart(bpvls, &bdf, 0)

stcolor()

gr.rect iv, 0, ~
 Sldcy - Sldrd - H / 8, Wdbx, ~
 Sldcy + Sldrd + H / 8

tglslitcolor()
gr.rect iv, Sldl + Sldrd, ~
 Sldcy - Sldrd, ~
 Sldr - Sldrd, Sldcy + Sldrd
gr.circle iv, Sldr - Sldrd, ~
 Sldcy, Sldrd

tglslitbright()
gr.rect iv, Sldl + Sldrd, ~
 Sldcy - Sldrd, sldx, ~
 Sldcy + Sldrd
gr.circle iv, Sldl + Sldrd, ~
 Sldcy, Sldrd

gr.color 255, 200, 200, 180,Fill

gr.rect iv, sldx - Prjhalf, ~
 Sldcy - Sldrd, sldx + Prjhalf,~
 Sldcy + Sldrd
gr.circle iv, sldx - Prjhalf, ~
 Sldcy, Sldrd
gr.circle iv, sldx + Prjhalf, ~
 Sldcy, Sldrd

gr.color 150, 50, 180, 50
gr.set.stroke W / 8
gr.line iv, sldx, ~
 Sldcy - Sldrd, sldx, ~
 Sldcy + Sldrd

tglslitcontour()
gr.set.stroke W / 16
gr.line iv, sldx, ~
 Sldcy - Sldrd, sldx, ~
 Sldcy + Sldrd

gr.set.stroke H / 32
tglslitcontour()
gr.line iv, sldx - Prjhalf, ~
 Sldcy - Sldrd, sldx + Prjhalf,~
 Sldcy - Sldrd
gr.line iv, sldx - Prjhalf, ~
 Sldcy + Sldrd, sldx + Prjhalf,~
 Sldcy + Sldrd
gr.arc iv, ~
 sldx - Prjhalf - Sldrd, ~
 Sldcy - Sldrd, ~
 sldx - Prjhalf + Sldrd, ~
 Sldcy + Sldrd,~
 90, 180, 0
gr.arc iv, ~
 sldx + Prjhalf - Sldrd, ~
 Sldcy - Sldrd, ~
 sldx + Prjhalf + Sldrd, ~
 Sldcy + Sldrd,~
 -90, 180, 0

bdend(&bdf, 0)

return



! puts each setting value to
! the variable
Stvtoeach :

sw.begin sti

sw.case 1
 rmode = stv
sw.break
sw.case 2
 black = stv
sw.break
sw.case 3
 vlen = stv
sw.break
sw.case 4
 tlen = stv
sw.break
sw.case 5
 ldot = stv
sw.break
sw.case 6
 ldash = stv
sw.break
sw.case 7
 lsspace = stv
sw.break
sw.case 8
 llspace = stv
sw.break
sw.case 9
 lwspace = stv
sw.break
sw.case 10
 pitch = stv
sw.break


sw.end

return



! gets each setting value
Stitostv :

sw.begin sti

sw.case 1
 stv = rmode
sw.break
sw.case 2
 stv = black
sw.break
sw.case 3
 stv = vlen
sw.break
sw.case 4
 stv = tlen
sw.break
sw.case 5
 stv = ldot
sw.break
sw.case 6
 stv = ldash
sw.break
sw.case 7
 stv = lsspace
sw.break
sw.case 8
 stv = llspace
sw.break
sw.case 9
 stv = lwspace
sw.break
sw.case 10
 stv = pitch
sw.break


sw.end

return



! draws play or stop symbols
Playorstop :

bdstart(bpvls, &bdf, 0)

sambtncolor()

gr.circle iv, Samcx, Samcy, ~
 Samrd - H / 4

if Skind[sti] = Volsam then

 if jstplyd then

  gr.color 100, 255, 0, 100,Fill
  gr.poly iv, plysam, ~
   Samcx - W * 0.4, Samcy

 else

  gr.color 100, 0, 100, 150,Fill
  gr.rect iv, Samcx - W * 0.6, ~
   Samcy - H * 0.6, ~
   Samcx + W * 0.6, ~
   Samcy + H * 0.6
  
 endif

endif

stnamecolor()
gr.text.align 2 % center
gr.text.size H * 0.5
gr.text.draw iv, Samcx, ~
 Samcy + H * 0.15, "Sample"

bdend(&bdf, 0)

return


! draws sample button
Drsambtn :

sambtncolor()
gr.circle iv, Samcx,Samcy, Samrd
stlinecolord()
gr.color ,,,, Stroke
gr.set.stroke W / 8
gr.circle iv, Samcx,Samcy, Samrd

bdend(&bdf, 0)

gosub Playorstop % draws play

return



Chsts : % choice status

bdstart(bpcbx, &bdf, 0)

bxbtmy = calc_bxbtmy(Htcbx, H)
cbxint = (bxbtmy - Bxtopy) / 3.5

for i = 1 to 3

 wk = Bxtopy + cbxint * i 
 
 stcolor()

 gr.circle iv, Cbuttonx, ~
  wk - H * 0.65, H * 0.3

 stlinecolorb()
 gr.circle iv, Cbuttonx, ~
  wk - H * 0.65, H * 0.3

 if i = stv then
  
  tgllampcolor()
  gr.circle iv, Cbuttonx, ~
   wk - H * 0.65, H * 0.25

 endif

next

bdend(&bdf, 0)


return



! draws a box for settings
Drbox :

rdc = W / 8 % radius of corners

stcolor()

gr.rect iv, rdc, 0, Wdbx - rdc,~
 htbx
gr.rect iv, 0, rdc, rdc, ~
 htbx - rdc
gr.rect iv, Wdbx - rdc, rdc, ~
 Wdbx, htbx - rdc
gr.circle iv, rdc, rdc, rdc
gr.circle iv, Wdbx - rdc, rdc, ~
 rdc
gr.circle iv, rdc, htbx - rdc, ~
 rdc
gr.circle iv, Wdbx - rdc, ~
 htbx - rdc, rdc


gr.set.stroke W / 16
stlinecolord()


bxbtmy = calc_bxbtmy(htbx, H)

gr.line iv, 0, Bxtopy - H/16 , ~
 Wdbx, Bxtopy - H/16
gr.line iv, 0, bxbtmy - H/32 ~
 , Wdbx, bxbtmy - H/32

gr.set.stroke W / 8
gr.color ,,,, Stroke
gr.rect iv, Rstl, ~
 htbx - H * 0.75, ~
 Rstr, htbx - H * 0.25
gr.rect iv, Okl, ~
 htbx - H * 1.05, ~
 Okr, htbx - H * 0.15

gr.set.stroke W / 16



stlinecolorb()

gr.line iv, 0, Bxtopy, Wdbx, ~
 Bxtopy
gr.line iv, 0, bxbtmy , Wdbx, ~
 bxbtmy

gr.color ,,,, Fill
gr.rect iv, Rstl, ~
 htbx - H * 0.75, ~
 Rstr, htbx - H * 0.25
gr.rect iv, Okl, ~
 htbx - H * 1.05, ~
 Okr, htbx - H * 0.15

stnamecolor()
gr.text.size H * 0.45
gr.text.draw iv, W / 4, ~
 W * 0.75, btitle$

gr.text.size H * 0.65
gr.text.setfont "sans serif","N"
gr.text.align 2 % center
gr.text.draw iv, ~
 (Okl + Okr) / 2, ~
 htbx - H * 0.36, "OK"

rstcolor()
gr.text.size H * 0.4
gr.text.setfont "sans serif","B"
gr.text.draw iv, ~
 (Rstl + Rstr) / 2, ~
 htbx - H * 0.35, "RESET"


return



Wherest : 
! where pushed in settings mode

opsh = psh
 ! first value is invalid

if ty < Topclip then
 gosub Modepush

elseif Extleft < tx & ~
 tx < Extright & ~
 Exttop + Topclip <ty & ~
 ty < Extbottom + Topclip then

 psh = Extpsh

else

 psh = Setpsh % interim

 by = ty + posst - Topclip 

 sti = ceil((by - Topmgst) ~
  / Htst)

 if sti < 1 | Numst < sti then

  psh = Frmpsh

 else

  if Skind[sti] = Tggle & ~
   (tx < Tglleft | ~
   Tglleft + Tglw < tx) then

   psh = Frmpsh

  endif

 endif

endif

return
 


Tgsts : % toggle status

sti = 2
cstsy = Topmgst + Htst*(sti - 1)
opaque()
if black then
 bp = tglon
else
 bp = tgloff
endif

gr.bitmap.draw iv, bp, ~
 Tglleft, cstsy + H * 0.2

return



Csts : % choice status

stcolor()
cstsy = Topmgst + Htst*(sti - 1)

gr.rect iv, W*5, cstsy + H/3,~
 Width, cstsy + H
gr.color 255, 50, 50, 100, 1
gr.text.size H * 0.45
gr.text.align 2 % center
gr.text.setfont "sans serif","B"

if sti = 1 then
 s$ = Ssts$[1, rmode]

endif

gr.text.draw iv,  ~
 W * 7, cstsy + H * 0.85 ,S$

return



Drawst : % draws settings

bmclear(stclip, &bdf)
opaque()

gr.bitmap.draw iv, bpst, ~
 0, -posst

gr.bitmap.draw iv, bpext, ~
 Extleft, Exttop

bdend(&bdf, 0)

return



Draw2modes :

if sn = 1 then % mode Ed --> St
 mback = floor(barmode)
else           % mode Ed <-- St
 mback = ceil(barmode)
endif

mforth = mback + sn

if mback <> omback then

 gr.hide ob[omback]

 if Ed <= mforth & ~
  mforth <= St then

  gr.show ob[mforth]

 endif

endif

mbackx = -(barmode - mback) * ~
 Width
mforthx = mbackx + Width * sn

gr.modify ob[mback], "x", ~
 mbackx

if Ed <= mforth & ~
 mforth <= St then

 gr.modify ob[mforth], "x", ~
  mforthx

endif

gr.render

omback = mback

return



Modechange : 

gosub Rdtxttotxtdata

if mode <> Ed then
 gosub Rdintosc
 gosub Drawed
endif

if mode <> Mv then

 gosub Inimvvar
 
 cen = rpos 
 gosub Centomvty 
 
 sigf = False
 gosub Drawmvall
 
endif

if mode <> Rd then
 gosub Inicopy
endif

gosub Drawst

gosub Savpos_st

sn = sgn(pshmode - mode)
! sign (direction)
bmint = Barmvint * sn
wk = bmint

barinertia = 2
! inertia of the moving bar

bslowspan = 0
for i = 1 to barinertia
 wk /= 2
 bslowspan += wk
next   

bfastbgn = mode + bslowspan
bfastend = pshmode - bslowspan
q = round(((bfastend-bfastbgn) ~
 / bmint),, "U") %away from zero
bmi = (bfastend - bfastbgn) / q


barmode = mode
mode = pshmode

gosub Savcr

chang = True
gosub Drawtpsw   
chang = False

omback = barmode
gr.show ob[barmode + sn]

! begins slowly

for i = 1 to barinertia

 barmode += wk
 wk *= 2
 gosub Drawtpsw
 gosub Draw2modes

next

for i = 1 to q

 barmode += bmi
 gosub Drawtpsw
 gosub Draw2modes

next

wk = bmint / 2
 
for i = 1 to barinertia - 1

 barmode += wk
 wk /= 2
 gosub Drawtpsw
 gosub Draw2modes

next

barmode = mode
gosub Drawtpsw
gosub Draw2modes

return



Modepush : 

psh = Outpsh

if H < ty & ty < Topclip then
 
 for i = Ed to St
 
  mbarx = Mnmcenter + ~
   (i - 1) * Mnmint - Msww / 2
  if mbarx < tx & ~
   tx < mbarx + Msww then
   psh = Tspsh
   pshmode = i
  endif

 next

endif
  
return
 



! ending routine

Askend : 

dialog.message , "Exit?", ~
  ans, "Resume" ,"Exit"

if ans = 2 then 

 if outoffunction then
 
  if arng then
   rdtxt = rdtxtkp
  endif

  if vsflag then
   rmode = rmodekeep
   rpos = rposkeep
  endif

  gosub Savcr

  gosub Savpos_st
  
 endif

 ! Exit command here in apk ver.
 END %****************** END

endif

return



Redst : % reads settings
 
stv = red(Sep$[sti], Dflt[sti],~
 Mins[sti], Maxs[sti])

return


Wrtst : % writes setting

safewrite(Sep$[sti], str$(stv))

return



Savpos_st : 
! saves reading position or
! settings depending on 
! the current mode

if (mode = Mv | mode = Rd) ~
 & numfl > 0 & rdtxt > 0 then
 
 svtxt = rdtxt
 svpos = rpos
 gosub Savpos
 
endif
   
if mode = St then

 gosub Savst

endif

return


! saves reading position

Savpos :

if svtxt <= numfl then

 list.replace pospt, svtxt,svpos
 list.get fnpt, svtxt, fn$

 safewrite ~
  (Pspt$ + fn$, str$(svpos))

endif

return


! saves current data
Savcr :
 
safewrite ~
 (Crpt$ + "mode", str$(mode))

gosub Savrdtxt

return


! saves reading text number
Savrdtxt :

safewrite ~
 (Crpt$ + "rdtxt", str$(rdtxt))

return


! saves settings data
Savst :

for sti = 1 to Numst - 1

 gosub Stitostv
 gosub Wrtst

next

Return




Savfo :
! saves file order

list.size fnpt, numfl
          ! number of files
 

!(1) at first wrtites draft
text.open w, ftn, Drpt$
 ! file table num, draft path
text.writeln ftn, str$(numfl)


for i = 1 to numfl

 list.get fnpt,i,wk$ % file name
 text.writeln ftn, wk$

next

text.close ftn

!(2) copies into file order list
grabfile wk$, Drpt$

safewrite(Fopt$, wk$)

return




Ongrtouch:

jstc = True

gr.ongrtouch.resume



! draws top switch bar
Drawtpsw:

gr.text.align 2 % center
 
if chang then 
! 1.when mode is changed
!   draws mode names

 bmclear(tpsw, &bdf)
   
 tpswcolor()
 gr.rect iv, 0, 0, Width, H

 gr.text.size H * 0.5
 gr.text.setfont "monospace"

 x = Mnmcenter
 for i = Ed to St 

  if i = mode then
   gr.text.bold True
   white()
  else
   gr.text.bold False
   gr.color 200, 0, 0, 0, Fill
  endif

  gr.text.draw iv, x, H * 0.6,~
  Mnm$[i]
  x += Mnmint
 next

 gr.text.bold False

else

 bdstart(tpsw, &bdf, 0)
 gr.color 255, 0, 0, 0, Fill
 gr.rect iv, 0 ,H - Mbarh, ~
  Width, H   
 tpswcolor()
 gr.rect iv, 0 ,H - Mbarh, ~
  Width, H

endif

! 2.draws mode indication bar
white()

mbarx = Mnmcenter + ~
 (barmode-1) * Mnmint - Mbarw/2

gr.rect iv, mbarx,~
 H - Mbarh, mbarx + Mbarw, H

bdend(&bdf, 0)

return



! moves reading text into screen
Rdintosc :

if rdtxt > 0 then
 
 by = (rdtxt - 1) * ~
 (Htfed + Inted) + Topmged
   ! y on bitmap

 rdtxty = by - posed
   ! y on screen - Topclip

 if rdtxty < Topmged then

  posed = by - Topmged

 elseif rdtxty > ~
  Htclp - Btmmged - Htfed ~
  then

  posed = by + Htfed + Btmmged ~
   - Htclp

 endif

endif

return



! draws a green text frame
Drawtxtframe:

gr.color 255, 10, 100, 50, Fill

brd = W / 4 % breadth
inr = W / 10 % innner breadth
gr.rect iv, ~
 Lftmged - brd, by - brd, ~
 Width - Ritmged + brd, by - inr
gr.rect iv, ~
 Lftmged - brd, ~
 by + Htfed + inr, ~
 Width - Ritmged + brd, ~
 by + Htfed + brd
gr.rect iv, ~
 Lftmged - brd, by - inr, ~
 Lftmged - inr, by + Htfed + inr
gr.rect iv, ~
 Width - Ritmged + inr, ~
 by - inr, ~
 Width - Ritmged + brd, ~
 by + Htfed + inr

return



! creates & draws edit mode
Createdrawed:

gosub Createed

gosub Drawed

return


Createed:
! draws only a rectangle

heighted = Topmged + ~
 Htfed * numfl + ~
 Inted * (numfl - 1) + Btmmged

if heighted < Htclp then
 heighted = Htclp
endif

gr.bitmap.delete bped

gr.bitmap.create bped, Width, ~
 heighted

edcolor()
bmfill(bped, &bdf)
bdend(&bdf, 0)

return



Drawed: % draws edit mode

! text number for drawing
txtd = floor ~
 ((posed - Topmged) / ~
 (Inted + Htfed) + 1)
if txtd < 1 then txtd = 1

by = (txtd-1) * (Htfed+Inted) +~
 Topmged  % y on bitmap
bdstart(bped, &bdf, 0)


for i=1 to Maxedtxt

 if txtd <= numfl then

  if txtd = rdtxt then
   if txtd = chosntxt then
    tsts[i] = Rd_cs
   else
    tsts[i] = Redng
   endif
  else 
   if txtd = chosntxt then
    tsts[i] = Chosen 
   else
    tsts[i] = Plain
   endif
  endif

  if tsts[i] <> None then

   txty[i] = by-posed+Topclip
   ! y on screen

   list.get drwnpt, txtd, flg

   if !flg then

    if txtd = chosntxt then
     gr.color 255,255,150,50, ~
      Fill
    else
     white()
    endif

    gr.rect iv, Lftmged, by, ~
     Width-Ritmged, by + Htfed

    gr.color 255,0,0,0,Fill
    gr.text.align 1 % left
    gr.text.size H / 2
    gr.text.setfont "sans serif"

    
    list.get tptr, txtd, s$

    lt = len(s$)
    num = 1
    l$ = left$(s$,num)
    gr.text.width wk,l$
    while wk <= ~
     Width - Lftmged - Ritmged ~
     - W / 6 & num <= lt
      
     num++
     l$ = left$(s$,num)
     gr.text.width wk,l$
    repeat
    num--
    l$ = left$(s$,num)

    gr.text.draw iv, ~
     Lftmged + W / 8,~
     by + H * 2/3, l$

    if txtd = rdtxt then
     gosub Drawtxtframe   
    endif

    list.replace drwnpt,txtd, ~
     True

   endif

  endif

 else

  tsts[i] = None

 endif

 by += Htfed + Inted
 txtd++

next i

bdend(&bdf, 0)


bmclear(edclip, &bdf)
opaque()

gr.bitmap.draw iv, bped, ~
 0, -posed

gr.bitmap.draw iv, bpext, ~
 Extleft, Exttop

gr.bitmap.draw iv, bpadd, ~
 Addleft, Addtop


bdend(&bdf, 0)

return



Onbackkey:

if saving then
 pending = 1 % True
else
 gosub Backkeyexecution
endif

back.resume



Backkeyexecution :

if !obck then 

 obck = 1 % True

 gosub Askend

endif

obck = 0 % False

return



Exit: % when "exit" pushed

obck = True
gosub Askend
obck = False

return




Whereed :
! where is pushed in edit mode

opsh = psh

if ty < Topclip then
 gosub Modepush

elseif Extleft < tx & ~
 tx < Extright & ~
 Exttop + Topclip < ty & ~
 ty < Extbottom + Topclip then

 psh = Extpsh

elseif ~
 Addleft < tx & tx < Addright ~
 & Addtop + Topclip < ty & ~
 ty < Addbottom + Topclip then

 psh = Addpsh

else

 psh = Frmpsh % interim
 if Lftmged < tx & ~
  tx < Width - Ritmged then

  for i=1 to Maxedtxt
   if tsts[i] <> None & ~
    (txty[i] < ty & ~
    ty < txty[i] + Htfed) then

    psh = Txtpsh 
    pshsts = tsts[i] 
    ! status of pushed text

   endif
  next
 endif
endif

return


Erasetxt: 

eby = (ertxt - 1) * ~
 (Htfed + Inted) + Topmged
edcolor()
gr.rect iv, 0, eby - Inted / 2,~
 Width, eby + Htfed + Inted / 2
list.replace drwnpt,ertxt,False

return




!*****************************
!*****************************
!#4 edit mode


Editmode :

arng = False % text arranging
bchang = False % button change
getout = False

stay = True % touch staying
staya = True % staying almost

v = 0 % velocity
jstc = False % just touched
ltc = False % last touch


ipos = -1

do

 opos = ipos

 ipos = int(posed)
 if ipos <> opos then

  gosub Drawed
  gr.render

 endif 

 gr.touch touched, tx, ty

 if touched then

  if jstc | ltc then

   ltc = True

   if jstc then

    !v = 0
    clk = clock()
    gosub Whereed
      
   else

    gosub Whereed

    if psh <> opsh then
     bchang = True
     ! button changed
    endif

    if psh = Extpsh & !bchang ~
     then

     gosub Exit
     jstc = False
     ltc = False
    
    elseif psh = Tspsh & !bchang~
     & pshmode <> mode then

     gosub Modechange
     getout = True
    
  
    ! moves only when
    ! text scroll pushed
    elseif psh = Txtpsh | ~
     psh = Frmpsh then

     v = ty - oy

    endif 

   endif



   if abs(tx-ox) + abs(ty-oy) ~
    < W / 12 | jstc & !getout ~
    then
   
      ! long press ***********
    ct = clock() - clk
    if ct >= Lpress then

     ! long press on text
     if psh = Txtpsh & staya ~
      then

      if pshsts = Plain | ~
       pshsts = Redng then

       bdstart(bped, &bdf, 0)

       if rdtxt > 0 then
        ertxt = rdtxt
        gosub Erasetxt
       endif

       if chosntxt > 0 then
        ertxt = chosntxt
        gosub Erasetxt
       endif

       txt = ytotxt( ~
        posed + ty - Topclip,~
        Topmged, Inted, Htfed)
    
       ertxt = txt      
       gosub Erasetxt

       bdend(&bdf, 0)

       if !arng then
        rdtxtkp = rdtxt
        ! keep rdtxt
        rdtxt = 0
       endif
       arng = False
 
       chosntxt = txt
             
       gosub Drawed
       gr.render
                 
       dialog.message ,, ~
        ans, "Delete", ~
        "Arrange", "Edit"

       if ans = 1 then %delete
          
        dialog.message , ~
         "Delete This Text?", ~
         ans, "Delete", ~
         "Cancel"

        if ans <> 1 then
         ans = 0 % default
        endif

       endif

! choises for long pressed text
sw.begin ans
sw.case 1 % delete ------------

 list.get fnpt, txt, fn$
 
 list.remove fnpt, txt
 list.remove pospt, txt
 list.remove tptr, txt
    
 list.remove drwnpt, txt    

 numfl--   

 for i=1 to numfl
  list.replace drwnpt, i, False
 next

 gosub Savfo

 file.delete flg, Pspt$ + fn$

 file.delete flg, ~
  Txtpt$ + fn$ + ".txt"

 if rdtxtkp = txt then
  rdtxtkp = 1
 elseif rdtxtkp > txt then
  rdtxtkp--
  if rdtxtkp < 1 then
   rdtxtkp = 1
  endif
 endif

 rdtxt = rdtxtkp
 gosub Savrdtxt
 chosntxt = 0
 gosub Createed
    

sw.break

sw.case 2 % arrange ----------

 popup ~
  "Tap the Destination" ~
  , 0, H * 3, 0

 arng = True

sw.break

sw.case 3 % edit --------------

 list.get tptr, txt, t$
 text.input s$, t$, "Edit Text"
 list.replace tptr, txt, s$
 list.replace drwnpt, txt, False

 rdtxt = txt
 svtxt = txt
 svpos = 1
 gosub Savpos
 gosub Savrdtxt
 chosntxt = 0

 list.get fnpt, txt, fn$

 safewrite ~
 (Txtpt$ + fn$ + ".txt", s$)


sw.break

sw.default

 list.replace drwnpt, txt, False


 chosntxt = 0
  
 rdtxt = rdtxtkp
 if rdtxt > 0 then
  list.replace drwnpt, rdtxt, ~
   False
 endif


sw.end
  
       gosub Drawed
       gr.render   
  
      endif

      ltc = False

     elseif psh = Addpsh then

      if numfl >= Maxfile then

       popup "Number of files" ~
        + " can't exceed " + ~
        using$(,"%d", ~
        int(Maxfile)), 0, 0, 1

      else 

       if arng then      

        bdstart(bped, &bdf, 0)   

        ertxt = chosntxt
        gosub Erasetxt
        chosntxt = 0

        rdtxt = rdtxtkp
        ertxt = rdtxt
        gosub Erasetxt
          
        bdend(&bdf, 0)

        arng = False
        gosub Drawed
        gr.render

       endif

       text.input s$,, ~
        "Enter New Text"

       if s$ <> "" then

          ! in case time zone
          ! changed & same date
          ! & time comes twice

        fn$ = whattime$()
        list.search fnpt,fn$,flg 

        while flg

         fn$ = whattime$()
         list.search fnpt, ~
          fn$, flg

        repeat

        list.insert fnpt, 1, fn$
        list.insert pospt, 1, 1
        list.insert tptr, 1, s$        
      
        list.insert drwnpt,1, ~
         False
        numfl++
        for i = 2 to numfl
         list.replace drwnpt, ~
          i, False
        next
             

        rdtxt = 1
        svtxt = 1
        svpos = 1
        gosub Savpos       

        safewrite ~
         (Txtpt$+fn$+".txt", s$)
        
        gosub Savfo
        gosub Savrdtxt

        posed = 0     

        gosub Createdrawed
        gr.render

       endif
         
      endif
   
     endif

     ltc = False
     v = 0
    
    endif  
    
   else

    ! touched and moving
    stay = False
    staya = False

   endif
    
   jstc = False

  endif

   ! ignores touch 
   ! if jstc&ltc flgs are false


 else % not touched ----------

  if ltc then
      
   ct = clock() - clk
   if Minspress < ct & ~
    ct <= Maxspress & stay then 
    ! short press --------------

    tx = ox
    ty = oy
    gosub Whereed
 
    if psh = Txtpsh then

     txt = ytotxt( ~
      posed + ty - Topclip, ~
      Topmged, Inted, Htfed)
     
     if arng then 

      rdtxt = rdtxtkp
         
      if txt <> chosntxt then
         
       list.get fnpt, ~
        chosntxt,fn$
       list.get pospt, ~
        chosntxt,ps
       list.get tptr, ~
        chosntxt,s$   

       ! move up
       if txt < chosntxt then
         
        for i = chosntxt - 1 ~
         to txt step -1

         list.get fnpt, i,wkf$
         list.get pospt, i, wk
         list.get tptr, i, wk$

         list.replace fnpt, ~
          i + 1, wkf$
         list.replace pospt, ~
          i + 1, wk
         list.replace tptr, ~  
          i + 1, wk$
         list.replace drwnpt,~
          i + 1, False

        next

        if txt <= rdtxt & ~
         rdtxt < chosntxt then
         rdtxt++
        elseif rdtxt = chosntxt~
         then
         rdtxt = txt
        endif   
  
       else % move down

        for i = chosntxt + 1 ~
         to txt
  
         list.get fnpt, i,wkf$
         list.get pospt, i, wk
         list.get tptr, i, wk$

         list.replace fnpt, ~
          i - 1, wkf$
         list.replace pospt, ~
          i - 1, wk
         list.replace tptr, ~  
          i - 1, wk$
         list.replace drwnpt, ~
          i - 1, False

        next

        if chosntxt < rdtxt & ~
         rdtxt <= txt then
         rdtxt--
        elseif rdtxt = chosntxt~
         then
         rdtxt = txt
        endif   
        
       endif

       list.replace fnpt, ~
        txt, fn$
       list.replace pospt, ~
        txt, ps
       list.replace tptr, ~  
        txt, s$
    
       gosub Savfo
       gosub Savrdtxt
  
      endif

      list.replace drwnpt, ~
       txt, False

      if rdtxt > 0 then
       list.replace drwnpt, ~
       rdtxt, False
      endif

      chosntxt = 0
          
      gosub Drawed
      gr.render

      arng = False

     else 
       ! changes frame ---------
       ! for reading text

      bdstart(bped, &bdf, 0)

  ! short press to select text 

      if pshsts = Plain then

       if rdtxt > 0 then
        ! erases old frame 
        ertxt = rdtxt
        gosub Erasetxt
       endif

       rdtxt = txt
       ertxt = rdtxt
       gosub Erasetxt
       gosub Savrdtxt

      endif

      bdend(&bdf, 0) 

      gosub Drawed
      gr.render

              
     endif
     
     jstc = False
     v = 0

    endif
   
   endif

  endif

 
  if abs(v) < W / 8 then 
   stay = True
  endif

  if abs(v) < W * 0.8 then
   staya = True
  endif

  ltc = False
  bchang = False

 endif

   
 ox = tx
 oy = ty
 posed -= v

 if posed < 0 then
  posed = 0
 elseif posed > heighted-Htclp ~
  then
  posed = heighted-Htclp
 endif
  
 v *= Inertia / 1000 % 0.985

 if abs(v) < Height / 10 then
  v *= (Inertia / 1000) * 0.9
 endif

 if abs(v) < Minv then
  v = 0
 endif


until getout

return % End of Edit Mode



!*****************************
!*****************************
!#5 move mode

Movemode :


bchang = False % button change
getout = False

stay = True % touch staying

v = 0 % velocity
jstc = False % just touched
ltc = False % last touch

cmvty = ceil(mvty)
cmvsy = ceil(mvsy)

rtxt$ = rdtxt$ %Tread needs this


do

 if sigf then % signal

  omvsy = cmvsy
  ! old value for mvsy
  cmvsy = ceil(mvsy)
    
  if cmvsy > omvsy then
   ! move toward top

   bdstart(bpmvscr, &bdf, 0)
   gr.bitmap.draw iv, bpmvwin, ~
    0, 0
   bdend(&bdf, 0)
   
   starty = 0
   eendy = cmvsy - omvsy
   tobtm = False
   
   gosub Drmvsig
   
   bdstart(bpmvwin, &bdf, 0)
   gr.bitmap.draw iv, bpmvscr, ~
    0, cmvsy - omvsy
   bdend(&bdf, 0)
     
   gosub Copytxtsig
   gr.render
   
  elseif cmvsy < omvsy then
   ! move toward bottom 

   bdstart(bpmvscr, &bdf, 0)
   gr.bitmap.draw iv, bpmvwin, ~
    0, 0
   bdend(&bdf, 0)
   
   starty = Mvwinh + ~
    cmvsy - omvsy
   eendy = Mvwinh
   tobtm = True
   
   gosub Drmvsig
   csrtop = csri
   topletter = mvchr - Csrnum
   
   bdstart(bpmvwin, &bdf, 0)
   gr.bitmap.draw iv, bpmvscr, ~
    0, cmvsy - omvsy
   bdend(&bdf, 0)
     
   gosub Copytxtsig
   gr.render

  endif

 else         % text

  omvty = cmvty
  ! old value for mvty

  cmvty = ceil(mvty)

  if cmvty > omvty then
   ! move toward top

   bdstart(bpmvscr, &bdf, 0)
   gr.bitmap.draw iv, bpmvwin, ~
    0, 0
   bdend(&bdf, 0)
   
   starty = 0
   eendy = cmvty - omvty
   
   gosub Drmvtxt
   
   bdstart(bpmvwin, &bdf, 0)
   gr.bitmap.draw iv, bpmvscr, ~
    0, cmvty - omvty
   bdend(&bdf, 0)
     
   gosub Copytxtsig
   gr.render
   
  elseif cmvty < omvty then
   ! move toward bottom 

   bdstart(bpmvscr, &bdf, 0)
   gr.bitmap.draw iv, bpmvwin, ~
    0, 0
   bdend(&bdf, 0)
   
   starty = Mvwinh + ~
    cmvty - omvty
   eendy = Mvwinh
   
   gosub Drmvtxt
   
   bdstart(bpmvwin, &bdf, 0)
   gr.bitmap.draw iv, bpmvscr, ~
    0, cmvty - omvty
   bdend(&bdf, 0)
     
   gosub Copytxtsig
   gr.render


  endif

 endif



 gr.touch touched, tx, ty

 if touched then

  if jstc | ltc then
   
   ltc = True

   if jstc then

    clk = clock()
    gosub Wheremv
     ! decides first psh that
     ! will be opsh next time 
         
   else

    gosub Wheremv

    if psh <> opsh then
     bchang = True
     ! button changed
    endif

    if psh = Extpsh & !bchang ~
     then

     gosub Exit
     jstc = False
     ltc = False
    
    elseif psh = Tspsh & !bchang & ~
     pshmode <> mode then
   
     if numfl > 0 then
      list.replace pospt, ~
       rdtxt, rpos
     endif
     gosub Modechange
     getout = True
    
    ! moves only when
    ! pushed insise the window
    elseif psh = Cntpsh | ~
     psh = Blnkpsh then

     v = ty - oy

    endif 

   endif

   if abs(tx-ox) + abs(ty-oy) ~
    < W / 12 | jstc then

   else
     ! touched and moving
    stay = False
 
   endif
    
   jstc = False

  endif

   ! ignores touch 
   ! if jstc&ltc flgs are false

 else % not touched----------

  if ltc then

   ct = clock() - clk
   if Minspress < ct & ~
    ct <= Maxspress then
    ! short press --------------
 
    tx = ox
    ty = oy
    gosub Wheremv
 
    if psh = Cntpsh & stay then
     rpos = pshpos

     svpos = rpos
     svtxt = rdtxt
     gosub Savpos

     top = rpos % for topletter
     gosub Drawmvall
     gr.render
     
    elseif psh = Bgnpsh then
     
     rpos = 1
     top = 1
     
     svpos = rpos
     svtxt = rdtxt
     gosub Savpos
     
     if sigf then
         
      gosub Toptomvsy
      gosub Drawmvall
      gr.render
      
      cmvsy = ceil(mvsy) 
      v = 0
     
     else
      
      gosub Toptomvty 
      gosub Drawmvall
      gr.render
      
      cmvty = ceil(mvty)
      v = 0
     
     endif

    elseif psh = Txtsigpsh then
     
     sigf = !sigf
  
     if sigf then
      
      tlin = ~
       ceil((Mvtxttpmgn - Mvty)~
       / Mvtxtlf) 
      top = tlin * Mvlts
      gosub Toptomvsy
      gosub Drawmvall
      gr.render
      
      cmvsy = ceil(mvsy) 
      v = 0
     
     else
     
      top = ceil((Mvcen - Mvsy ~
       ) / Mvsiglf)       
      gosub Toptomvty 
 
      gosub Drawmvall
      gr.render
      
      cmvty = ceil(mvty)
      v = 0
     
     endif

    endif

   endif

  endif
 
  if abs(v) < W / 8 then 
   stay = True
  endif

  ltc = False
  bchang = False

 endif
   
 ox = tx
 oy = ty
 
 if sigf then
  mvsy += v
  if mvsy < minsy then
   mvsy = minsy
  elseif mvsy > Maxsy then
   mvsy = Maxsy
  endif
 else
  mvty += v
  if mvty < minty then
   mvty = minty
  elseif mvty > Maxty then
   mvty = Maxty
  endif
 endif

 v *= Inertia / 1000

 if abs(v) < Height/10 then
  v *= (Inertia / 1000) * 0.9
 endif

 if abs(v) < Minv then
  v = 0
 endif

until getout

return % End of Move Mode


!*****************************
!*****************************
!#6 read mode

Readmode :

! mode lock & blackout

mlocked = False % mode locked

ccld = False % concealed

if black & rmode = Vib then

 for i = 7 to 255 step 8

  gr.modify ob[Ds], "alpha", i
  gr.render

 next
 
 dialog.message ~
  "Blackout Started", ~
  "Long Press the top of the" +~
  " screen to exit blackout", ~
  ans, "Ok", "Cancel"
  
 if ans <> 1 then
 
  black = False
  sti = 2
  stv = False % black
  gosub Wrtst
  bdstart(bpst, &bdf, 0)
  gosub Tgsts
  bdend(&bdf, 0)
  
  gr.modify ob[Ds], "alpha", 0
  gr.render
  
 endif
 
elseif rmode = Vib then

 gosub Putml
 
endif


! starts reading

getout = False
morsex = W * 0.3
morsey = Win1txty

gosub Iniforread
      
do % read text loop

 if !jstplyd | plyend then

  jstc = False
  do
  until jstc

  do
   gr.touch touched, tx,ty
  until touched
  
 endif

 gosub Whererd


 if psh = Ppbpsh then
 
  if jstplyd then

   jstplyd = False
    
  else 
   
   if rmode = Vib then
    gosub Longprs
   endif
   
   if rmode <> Vib | lpflag then
  
    gosub Drawrdcp
    gr.render 
   
    if rmode = Vib then
     
     if !black then 
     
      if !mlocked then
       gosub Putml  
      endif
    
      grdal = True % gradual
      gosub Conceal
      
     endif
   
     ccld = True
     
    endif

    
    gosub Vibptn
          
    skipspc = True
    jstwspc = False
    rseg = 1
    seghead = True
    lifted = False
    ctr = 0
    
  ! text reading routine -------

    do  
   
     sigpos = rpos
     gosub Tread

   ! occasionally saves position   
     if ++ctr > 500 then
      svtxt = rdtxt
      svpos = rpos
      gosub Savpos
      ctr = 0
     endif
       
     gr.touch touched, tx, ty
     gosub Whererd   

     if !touched then
      lifted = True
     endif
     
     if lifted & touched & ~
      rmode = Vib then
           
      if psh <> Outpsh then
      
       gosub Longprs
  
       if !lpflag then
        lifted = False
       endif
       
      else     
       lifted = False
      endif
      
     endif
   
    until (lifted & touched & ~
     psh <> Outpsh) | plyend
     
    svtxt = rdtxt
    svpos = rpos
    gosub Savpos

    jstplyd = True
    plyend = False
 
    gosub Drawrdcp
    gr.render
 
    if rmode = Vib then
    
     if !black then
      gosub Appear
     endif
     
     ccld = False
     
    endif
      
   endif    

  endif
   
 
 elseif psh = Tspsh & ~
  pshmode <> mode then

  if numfl > 0 then
   list.replace pospt, ~
    rdtxt, rpos
  endif
  gosub Modechange
  getout = True

 elseif psh = Mlkpsh then
 
  if rmode = Vib & !jstplyd then
   gosub Longprs     
  endif
   
  if rmode <> Vib | lpflag then
  
   respvib(True)
 
   if black then
   
    black = False
    sti = 2
    stv = False % black
    gosub Wrtst
    bdstart(bpst, &bdf, 0)
    gosub Tgsts
    bdend(&bdf, 0)
    
    gr.modify ob[Ds], "alpha", 0
      
    if jstplyd then
    
     grdal = False
     gosub Conceal
     pause 200
     gosub Appear
     ccld = False
     
    endif
    
    gr.render
         
   else
  
    gosub Rmoveml
   
   endif
   
  endif
  
  jstplyd = False
  
 elseif psh = Lkpsh then
 
  if rmode = Vib & !jstplyd then
   gosub Longprs     
  endif
   
  if rmode <> Vib | lpflag then
  
   respvib(False)
 
   if rmode = Vib & !mlocked ~
    & !black then   
    gosub Putml
   endif
 
   if changbl(numfl) then
   
    svtxt = rdtxt
    svpos = rpos
    gosub Savpos
 
    bdstart(tall, &bdf, 0)  
    opaque()
    gr.bitmap.draw iv, bpreado,~
     0, Htclp
    bdend(&bdf, 0)
 
    ertxt = rdtxt
    bdstart(bped, &bdf, 0)
    gosub Erasetxt
    bdend(&bdf, 0)
    
    rdtxt--
    if rdtxt = 0 then
     rdtxt = numfl
    endif
    gosub Savrdtxt
       
    list.replace drwnpt, ~
     rdtxt, False
    
    gosub Rdtxttotxtdata
    gosub Inireado
    
    bdstart(tall, &bdf, 0)
    opaque()
    gr.bitmap.draw iv, bpreado,~
     0, 0    
    bdend(&bdf, 0)

    for i = 1 to vscrlsize
     
     list.get vscrl, i, wk
     bdstart(bpreadc, &bdf, 0)
     opaque()  
     gr.bitmap.draw iv, tall, ~
      0, Htclp * (wk - 1)
     bdend(&bdf, 0)
     gosub Copylrkey
     gr.render  
     pause 30
     
    next
    
    gosub Iniforread
  
   endif
  
  endif
  
  jstplyd = False
  
 elseif psh = Rkpsh then
 
  if rmode = Vib & !jstplyd then
   gosub Longprs     
  endif
   
  if rmode <> Vib | lpflag then
  
   respvib(False)
 
   if rmode = Vib & !mlocked ~
    & !black then   
    gosub Putml
   endif
 
   if changbl(numfl) then
   
    svtxt = rdtxt
    svpos = rpos
    gosub Savpos
 
    bdstart(tall, &bdf, 0) 
    opaque()
    gr.bitmap.draw iv, bpreado,~
     0, 0
    bdend(&bdf, 0)
    
    ertxt = rdtxt
    bdstart(bped, &bdf, 0)
    gosub Erasetxt
    bdend(&bdf, 0)
         
    rdtxt++
    if rdtxt > numfl then
     rdtxt = 1
    endif
    gosub Savrdtxt
    
    list.replace drwnpt,~
     rdtxt, False    
    
    gosub Rdtxttotxtdata
    gosub Inireado
    
    bdstart(tall, &bdf, 0) 
    opaque()
    gr.bitmap.draw iv, bpreado,~
     0, Htclp  
    bdend(&bdf, 0)

    for i = 1 to vscrlsize
     
     list.get vscrl, i, wk
     bdstart(bpreadc, &bdf, 0)
     opaque()  
     gr.bitmap.draw iv, tall, ~
      0, -Htclp * wk
     bdend(&bdf, 0)
     gosub Copylrkey
     gr.render  
     pause 30
     
    next
    
    gosub Iniforread
  
   endif
  
  endif
  
  jstplyd = False
  

 else
 
  jstplyd = False

 endif

until getout

return % End of Read Mode



!*****************************
!*****************************
!#7 settings mode
  
Settingsmode :


bchang = False % button change
getout = False

stay = True % touch staying

v = 0 % velocity
jstc = False % just touched
ltc = False % last touch

ipos = -1 % int(posst)


do

 opos = ipos

 ipos = int(posst)
 if ipos <> opos then

  gosub Drawst
  gr.render 

 endif

 gr.touch touched, tx, ty

 if touched then

  if jstc | ltc then
   
   ltc = True

   if jstc then

    clk = clock()
    gosub Wherest 
     ! decides first psh that
     ! will be opsh next time 
         
   else

    gosub Wherest

    if psh <> opsh then
     bchang = True
     ! button changed
    endif

    if psh = Extpsh & !bchang~
     then

     gosub Exit
     jstc = False
     ltc = False
    
    elseif psh = Tspsh & !bchang & ~
     pshmode <> mode then

     gosub Modechange
     getout = True
  

    ! moves only when
    ! settings sections pushed
    elseif psh = Setpsh | ~
     psh = Frmpsh then

     v = ty - oy

    endif 

   endif
     
   if abs(tx-ox) + abs(ty-oy) ~
    < W / 12 | jstc then
 
   else
     ! touched and moving
    stay = False

   endif
    
   jstc = False

  endif

   ! ignores touch 
   ! if jstc&ltc flgs are false

 else % not touched----------

  if ltc then

   ct = clock() - clk
   if Minspress < ct & ~
    ct <= Maxspress & stay then
    ! short press --------------

    tx = ox
    ty = oy
    gosub Wherest
 
    if psh = Setpsh then
         
     if Skind[sti] = Tggle ~
      then
     
      black = !black
      stv = black
      gosub Wrtst
      bdstart(bpst, &bdf, 0)
      gosub Tgsts
      bdend(&bdf, 0)
      gosub Drawst
      gr.render

     elseif Skind[sti] = Choice~
      then

      bdstart(bpcbx, &bdf, 0)

      htbx = Htcbx
      btitle$ = Sname$[sti]
      gosub Drbox
      gosub Drcbx

      d = Dflt[sti]

      cbxint = ~ 
       (bxbtmy - Bxtopy) / 3.5
      wk = Bxtopy + cbxint * d

      rstcolor()

      gr.circle iv, ~
       Cbuttonx + W * 0.6, ~
       wk - H * 0.65, H * 0.05  

      bdend(&bdf, 0)
      
      gosub Stitostv

      gosub Chsts

      gr.show ob[Cbx]

      fade(ob[Ds], True) 
     
      bxbtmy = ~ 
       calc_bxbtmy(Htcbx, H)
      cbxint = ~
       (bxbtmy - Bxtopy) / 3.5

      do

       jstc = False
       do
       until jstc

       do
        gr.touch touched, tx, ty
       until touched

       tx -= Cbxx
       ty -= Cbxy

       for i = 1 to 3

        if Bxtopy + cbxint * ~
         (i - 1) < ty & ty < ~
         Bxtopy + cbxint * i ~
         then
    
         stv = i
         gosub Stvtoeach
         gosub Wrtst

         gosub Chsts
         gr.render

         i = 3 % out of for-next

        endif

       next

       if Rstl < tx & ~
        tx < Rstr & ~
        bxbtmy < ty & ~
        ty < Htcbx then

        stv = d
        gosub Stvtoeach
        gosub Wrtst

        gosub Chsts
        gr.render

       endif

      until Okl < tx & tx < Okr~
       & bxbtmy < ty ~
       & ty < Htcbx

      bdstart(bpst, &bdf, 0)
      gosub Csts
      bdend(&bdf, 0)

      gosub Drawst
      gr.hide ob[Cbx]
      fade(ob[Ds], False)

     
     elseif ~ % ---------------
      Skind[sti] = Volsam then
           
      rposkeep = rpos
       ! keep reading position
 
      rtxt$ = Samptxt$

      rmodekeep = rmode

      if sti = 3 then 
       !general vibration length
       rmode = Vib
      elseif sti = 4 & ~
       rmode = Vib then   
       ! general tone length 
       rmode = Slt
      elseif sti = 10 & ~
       rmode = Vib then   
       ! pitch
       rmode = Ton 
      endif 

      rseg = 1 % reading segment
      seghead = True
      
      plyend = False

      morsex = W * 3
      morsey = Samt - H * 0.5  

   
      vsflag = True
       ! flag for recovery
       ! of variables

      bdstart(bpvls, &bdf, 0)

      htbx = Htvls
      btitle$ = Sname$[sti]
      gosub Drbox

      rstx = stvtosldx ~ 
       (Dflt[sti], Mins[sti], ~
       Maxs[sti], Sldmin, ~
       Sldmax)

      rstcolor()
      gr.circle iv, rstx, ~
       Sldcy + Sldrd + H / 4, ~
       H * 0.05

      jstplyd = True
       ! to depict triangle
      gosub Drsambtn

      jstplyd = False 
       ! just played
      
      gosub Stitostv

      sldx = stvtosldx ~ 
       (stv, Mins[sti], ~
       Maxs[sti], Sldmin, ~
       Sldmax)

      gosub Drsld 

      gr.show ob[Vls]

      fade(ob[Ds], True) 
     
      bxbtmy = ~ 
       calc_bxbtmy(Htvls, H)


      do

       if !jstplyd | plyend then

        jstc = False
        do
        until jstc

        do
         gr.touch touched, tx,ty
        until touched

        tx -= Vlsx
        ty -= Vlsy
        starttx = tx
        startty = ty

        plyend = False
 
       endif
       
       chflg = False % changed   

       ! if slide touched
       if Bxtopy < ty & ~
        ty < Sldcy * 2 - Bxtopy ~
        then

        gosub Samtxtcl

        do
   
         sldx = tx

         if sldx < Sldmin then

          sldx = Sldmin

         elseif sldx > Sldmax ~
          then

          sldx = Sldmax

         endif

         gosub Drsld
         gr.render

         gr.touch touched, tx,ty

         tx -= Vlsx
         ty -= Vlsy

        until !touched | ~
         (!(Bxtopy < ty & ~
         ty < Sldcy*2 - Bxtopy))  

        stv = round~
         ((sldx - Sldmin) / ~
         (Sldmax - Sldmin) * ~
         (Maxs[sti] - Mins[sti])~
         + Mins[sti])

        chflg = True % changed

       ! reset touched
       elseif Rstl < tx & ~
        tx < Rstr & ~
        bxbtmy < ty & ~
        ty < Htvls then

        stv = Dflt[sti]
        chflg = True
        gosub Samtxtcl

       endif


       if chflg then 
       ! various conditions
        
        pp$ = ""

        sw.begin sti

        sw.case 5 % dot length

         if stv > ldash then

          pp$ = "Length of Dot" ~
           + " can't exceed " + ~
           "Lengh of Dash!"
          stv = ldash

         endif

        sw.break
        
        sw.case 6 % dash length
       
         if stv < ldot then

          pp$ = "Length of Dash"~
           + " can't be less " ~
           + "than Lengh of " ~
           + "Dot!"
          stv = ldot

         endif

        sw.break

        sw.case 7 % short space

         if stv > llspace then

          pp$ = "Length of " + ~
           "Short Space "+ ~
           "can't exceed " + ~
           "Lengh of Long Space!"
          stv = llspace

         endif

        sw.break

        sw.case 8 % long space

         if stv < lsspace then

          pp$ = "Length of " + ~
           "Long Space "+ ~  
           "can't be less " ~
           + "than Lengh of " ~
           + "Short Space!"
          stv = lsspace

         endif

         if stv > lwspace then

          pp$ = "Length of " + ~
           "Long Space "+ ~
           "can't exceed " + ~
           "Lengh of White " + ~
           "Space!"
          stv = lwspace

         endif

        sw.break

        sw.case 9 % white space

         if stv < llspace then

          pp$ = "Length of " + ~
           "White Space "+ ~  
           "can't be less " ~
           + "than Lengh of " ~
           + "Long Space!"
          stv = llspace

         endif

        sw.break

        sw.end

        if pp$ <> "" then

         popup pp$, 0, 0, 0

        endif

        gosub Stvtoeach
        gosub Wrtst
        
        sldx = stvtosldx(stv, ~
         Mins[sti], Maxs[sti], ~
         Sldmin, Sldmax)

        gosub Drsld
        gr.render

       endif % chflg

       ! --------------------
       if Saml < tx & tx < Samr~
        & Samt < ty & ty < Samb~
        then % sample pushed
       
        gosub Samtxtcl
        gr.render

        if jstplyd then

         jstplyd = False

        else     

         gosub Playorstop
         ! draws stop
    
         gosub Vibptn
          
         rpos = 1

         drawf = True
         skipspc = False
         jstwspc = False
         rseg = 1
         seghead = True
         lifted = False

! sample reading routine -------

         do  

          sigpos = rpos
          gosub Tread
        
          if rseg = 1 & seghead~
           then

           gosub Samtxtcl
 
          endif

          gr.touch touched, ~
           tx, ty

          tx -= Vlsx
          ty -= Vlsy

          if !touched then
           lifted = True
          endif

         until (lifted & ~
          touched & ~
          (0 < tx & tx < Wdbx ~
          & 0 < ty & ~
          ty < Htvls)) | ~
          plyend


         if !plyend then
          starttx = tx
          startty = ty
         endif

         jstplyd = True
    
         gosub Playorstop 
          ! draws play
         gosub Samtxtcl
         gr.render         

        endif
       
       else

        jstplyd = False

       endif

      until Okl < starttx & ~
       starttx < Okr~
       & bxbtmy < startty ~
       & startty < Htvls
      
      gr.hide ob[Vls]
      fade(ob[Ds], False)

      rpos = rposkeep
      rmode = rmodekeep

      vsflag = False
   
     elseif Skind[sti] = Dlog ~
      then

      dialog.message , ~
       "Reset All Settings?", ~
       ans, "Reset All","Cancel"

      if ans = 1 then

       bdstart(bpst, &bdf, 0)

       for sti = 1 to Numst - 1

        stv = Dflt[sti]
        gosub Stvtoeach       

        if sti = 1 then
         gosub Csts
        elseif sti = 2 then
         gosub Tgsts
        endif

       next
       
       gosub Savst
       

       bdend(&bdf, 0)
       gosub Drawst
       gr.render

      endif
     
     endif

     jstc = False
     v = 0
        
    endif

   endif

  endif
 
  if abs(v) < W / 8 then 
   stay = True
  endif

  ltc = False
  bchang = False

 endif

   
 ox = tx
 oy = ty
 posst -= v

 if posst < 0 then
  posst = 0
 elseif posst > Thtst - Htclp ~
  then
  posst = Thtst - Htclp
 endif
  

 v *= Inertia / 1000

 if abs(v) < Height/10 then
  v *= (Inertia / 1000) * 0.9
 endif

 if abs(v) < Minv then
  v = 0
 endif


until getout

return % End of Settings Mode



! =======================

!**********************
!**********************

Initialization :

!#8 initialization of variables

bdf = False % bitmap draw flag
obck = False % backkey
pending = False 
 ! pending backkey
saving = False
outoffunction = True

chosntxt = 0
! chosen text for 
!  edit or arrangement or delete


! set settings constants

read.from 1

for i = 1 to Numst - 1
 read.next Sname$[i], Skind[i],~
  wk$, dflt[i], Mins[i], Maxs[i]
 Sep$[i] = Stpt$ + wk$
next

read.next Sname$[i], Skind[i]
 ! Reset All Settings


list.create S, alp
list.create S, sigp

read.next wk$

while wk$ <> "end of data"

 if left$(wk$, 1) = "#" then
  wk$ = chr$(hex(mid$(wk$,2,4)))
 endif

 list.add alp, wk$
 read.next wk2$
 list.add sigp, wk2$

 read.next wk$

repeat


! loads file order

list.create S, fnpt 
 ! file name pointer

text.open r, ftn, Fopt$
if ftn <> -1 then % if exists

 text.readln ftn, wk$
 numfl = value(wk$)
 
 for i=1 to numfl
  text.readln ftn, wk$
  list.add fnpt, wk$
 next
 
 text.close ftn
 
endif


! loads text & data

list.create N, pospt
 ! current reading position ptr
list.create S, tptr
 ! text pointer
list.create N, drwnpt
 ! text drawn flag pointer


for i = 1 to numfl
 
 list.get fnpt, i, fn$
                 ! file name

 grabfile wk$, ~
  Txtpt$ + fn$ + ".txt", True
  
 if len(wk$) >= 2 & ~
  right$(wk$, 2) = chr$(13, 10)~
  then
  
  wk$ = left$(wk$, len(wk$) - 2)
 
 else
 

  
 endif
 
 list.add tptr, wk$

 wk = ~
  red(Pspt$+fn$, 1, 1, len(wk$))
 list.add pospt, wk
 list.add drwnpt, False
 
next


list.size tptr,wk
list.size pospt,wk
list.size drwnpt,wk


! loads current data
!  (mode & reading text)
mode = ~
 red(Crpt$+"mode", Ed, Ed, St)

trmode = mode % transition


rdtxt = red(Crpt$ + "rdtxt", ~
 1, 1, numfl)

! initializes rdtxt data *****
gosub Rdtxttotxtdata

! loads settings data

for sti = 1 to Numst - 1

 gosub Redst
 
 gosub Stvtoeach

next

! vertical scroll
list.create N, vscrl 
list.add vscrl, 0, 0.05 ~
 0.15, 0.3, 0.7, 0.85, 0.95, 1
list.size vscrl, vscrlsize


!############################
!############################
!#9 initialization of graphics

gr.open 255,0,0,0,0,1
! alp red grn blu sbar ort

gr.screen Width, Height

! drawing units
W = int(Width / 9)
H = int(Height / 16)

gr.color 255,200,200,200,Fill
gr.text.setfont "sans serif"
gr.text.size H / 2
gr.text.align 2 % center
gr.text.draw obstart, ~
 Width / 2, Height / 2, ~
 "starting..."

! constants for top switch bar
Mnmcenter = W
! mode name leftest position
Mnmint = W * 2.25
! mode name interval
Mbarw = W * 1.5
! mode bar width
Mbarh = H / 12
! mode bar hight
Msww = W * 2
! mode switch width

! constants of Edit mode
Topmged = H * 1.5 % top margin
Btmmged = H * 2 % bottom margin
Lftmged = W / 2 % left margin
Ritmged = Lftmged % right margin
Inted = H / 2 % interval
Htfed = H % height of a frame

Htclp = H * 14
! height of main clip
Topclip = H * 2
Maxedtxt = ceil~ 
 ((Htclp+Inted) / (Htfed+Inted))
dim Tsts[Maxedtxt] % text status
dim Txty[Maxedtxt] % text y


! constants of settings mode

! height of section of settings
Htst = H * 1.5
Topmgst = H     % top margin
Btmmgst = H * 2 % bottom margin
Thtst = Topmgst + Htst * Numst ~
 + Btmmgst      % total height

! constants of boxes
Wdbx = W * 8
Rstl = W * 0.25
Rstr = W * 2
Okl = W * 5 
Okr = W * 7 
Bxtopy = H * 1.3


! constants of choice box

Htcbx = H * 7
 ! height of choice box
Cbxx = W / 2
 ! choice box x position
Cbxy = (Height - Htcbx) / 2
 ! choice box y
Cbuttonx = W * 7
 ! coice button x 

! constants of volume & sample

Htvls = H * 10
Vlsx = W / 2
Vlsy = (Height - Htvls) / 2

Samcx = Wdbx / 2
Samcy = H * 6
Samrd = W * 2
Saml = Samcx - Samrd
Samr = Samcx + Samrd
Samt = Samcy - Samrd
Samb = Samcy + Samrd
Prjhalf = W / 2 
 ! half of projection width
Sldrd = H * 0.3 % slide radius
 ! ( height of slide / 2 )
Sldl = W / 2
Sldr = Wdbx - Sldl
Sldmin = Sldl + Sldrd + Prjhalf
Sldmax = Wdbx - Sldmin
Sldcy = H * 2 % slide center y




! background

bgcolor()

bkgr = bmcreatefill ~
 (Width, Height, &bdf)

white()
gr.color 180
gr.text.size H * 4.22
gr.text.setfont "serif"
gr.text.align 2 % center
gr.text.draw iv, W*4.5, H*9.6, ~
 "SMR"


strk = H / 80
y = H * 9.5
wk = H / 20

gr.color 255, 50, 50, 80

do
 gr.set.stroke strk
 gr.line iv, ~
  0, y, Width, y
 y -= wk
 wk += H / 20
 strk += H / 80
until y < H * 5


bdend(&bdf, 0)

opaque()
gr.bitmap.draw ob[Bg], bkgr, 0,0
gr.hide ob[Bg]


! position of "exit"

Extleft = 0
Exttop = H * 13
Extright = W * 2
Extbottom = H * 14


! creates exit bitmap

gr.color 200, 50, 50, 50, Fill
bpext = bmcreatefill ~
 (Extright - Extleft,~
 Extbottom - Exttop, &bdf)


gr.color 230, 100, 255, 100,Fill
gr.text.size H * 0.5
gr.text.align 2 % center
gr.text.setfont "monospace"
gr.text.draw iv, W, H * 0.7, ~
 "exit"

bdend(&bdf, 0)


! position of "+add"

Addleft = W * 5.5
Addtop = H * 12.3
Addright = W * 8.7
Addbottom = H * 13.7

! creates +add bitmap

gr.color 240,255,255,255,Fill
bpadd = bmcreatefill ~
 (Addright - Addleft,~
 Addbottom - Addtop, &bdf)


gr.color 240,150,150,200,Stroke
gr.set.stroke W/8
gr.rect iv, 0, 0, ~
 Addright - Addleft, ~
 Addbottom - Addtop

gr.color 255, 0, 0, 150, Fill
gr.text.size H * 0.8
gr.text.align 2 % center
gr.text.setfont "monospace"
gr.text.draw iv, W*1.6, H*0.85,~
 chr$(65291) + "add"
longpress(W*1.6, H*1.2, H / 4)

bdend(&bdf, 0)



! toggle switch

Tglw = W * 3
Tglh = H
Tglleft = W * 6

stcolor()
tgloff = ~
 bmcreatefill(Tglw, Tglh, &bdf)

tglslitcolor()
gr.rect iv, W, H * 0.4, ~
 W * 2, H * 0.6
gr.circle iv, W * 2, H * 0.5, ~
 H * 0.1

tglslitcontour()
gr.set.stroke H / 32      
gr.line iv, W, H * 0.4, ~
 W * 2, H * 0.4
gr.line iv, W, H * 0.6, ~
 W * 2, H * 0.6
gr.arc iv, W * 1.9 , H * 0.4, ~
 W * 2.1, H * 0.6, ~
 -90, 180, 0

tglslitcolor()
gr.circle iv, W, H * 0.5, ~
 H * 0.3
tglslitcontour()
gr.circle iv, W, H * 0.5, ~
 H * 0.3

bdend(&bdf, 0)


stcolor()
tglon = ~
 bmcreatefill(Tglw, Tglh, &bdf)

tglslitbright()
gr.rect iv, W, H * 0.4, ~
 W * 2, H * 0.6
gr.circle iv, W, H * 0.5,~
 H * 0.1

tglslitcontour()
gr.set.stroke H / 32      
gr.line iv, W, H * 0.4, ~
 W * 2, H * 0.4
gr.line iv, W, H * 0.6, ~
 W * 2, H * 0.6
gr.arc iv, W * 0.9 , H * 0.4,~
 W * 1.1, H * 0.6, ~
 90, 180, 0

tgllampcolor()
gr.circle iv, W * 2, H * 0.5, ~
 H * 0.3
tglslitcontour()
gr.circle iv, W * 2, H * 0.5, ~
 H * 0.3

bdend(&bdf, 0)


! draws top switch bar

gr.bitmap.create tpsw, Width, H

barmode = mode
chang = True
! mode just started to change

gosub Drawtpsw   

nearop()
gr.bitmap.draw ob[Ts], tpsw, ~
 0, H


! draws edit mode

posed = 0 % vertical position
gosub Rdintosc
 ! move reading text to screen
gr.bitmap.create edclip, ~
 Width, Htclp
gr.bitmap.create bped, 1, 1
! dummy
gosub Createdrawed
nearop()
gr.bitmap.draw ob[Ed], edclip, ~
 0, Topclip



! draws move mode

Mvwinl = W * 0.3
Mvwinr = Width - Mvwinl
Mvwinw = Mvwinr - Mvwinl
Mvwint = H
Mvwinb = Htclp - H * 3
Mvwinh = Mvwinb - Mvwint


Mvtxth = H * 0.545
gr.text.size Mvtxth
gr.text.setfont "monospace"
gr.text.height , Mvu, Mvd
Mvd_u = Mvd - Mvu
gr.text.width Mvtxtw, "A"

Mvcen = Mvwinh / 2
Mvtxttpmgn = Mvtxth
Mvtxtbtmmgn = Mvtxttpmgn
Mvtxtlftmgn = Mvtxtw * 0.8
Mvtxtritmgn = Mvtxtw / 2
Mvtxtlf = int(Mvd_u) 
 ! fractional part causes some
 ! troubles on screen
  
Maxty = Mvtxttpmgn - Mvu

Mvsighalf = H * 0.25
Mvsiglf = int(Mvsighalf * 2)
Mvsigside = W * 0.2
Mvsigtpmgn = Mvsiglf * 2
Mvsigbtmmgn = Mvsigtpmgn
Mvsiglftmgn = W
Maxsy = Mvsigtpmgn
Mvsigbrdt = H / 8
Mvsigwdth = W / 8
Csl = Mvsiglftmgn - Mvsigwdth

wk$ = "A"
i = 0
gr.text.width wk, wk$
while wk <= Mvwinw - ~
 Mvtxtlftmgn - Mvtxtritmgn
 i++
 wk$ += "A"
 gr.text.width wk, wk$
repeat

Mvlts = i 
! move mode letters in one line

Csrnum = ~
 ceil(Mvwinh / Mvsiglf) + 5
dim csr[Csrnum]
! cursor right x positions

! buttons

Txtsigw = W * 2.8
Txtsigh = H * 1.3
Txtsigl = W * 6
Txtsigr = Txtsigl + Txtsigw
Txtsigt = H * 12.5
Txtsigb = Txtsigt + Txtsigh

Bgnw = W * 2
Bgnh = H * 1.3
Bgnl = W * 2.8
Bgnr = Bgnl + Bgnw
Bgnt = H * 12.5
Bgnb = Bgnt + Bgnh


gr.bitmap.create bpmv, ~
 Width, Htclp
gr.bitmap.create bpmvwin, ~
 Mvwinw, Mvwinh
gr.bitmap.create bpmvscr, ~
 Mvwinw, Mvwinh

gr.color 150, 200, 200, 50, Fill
bptotxt = bmcreatefill ~
 (Txtsigw, Txtsigh, &bdf)
gr.color 100, 50, 50, 0,Stroke
gr.set.stroke W / 16
gr.rect iv, 0, 0, ~
 Txtsigw, Txtsigh
gr.color 245, 50, 50, 0, Fill
gr.text.size H * 0.7
gr.text.align 2 % center
gr.text.setfont "sans serif"
gr.text.draw iv, Txtsigw / 2, ~
 Txtsigh * 0.65, "Text"
bdend(&bdf, 0)
 
gr.color 50, 50, 100, 255, Fill
bptosig = bmcreatefill ~
 (Txtsigw, Txtsigh, &bdf)
gr.color 100, 30, 50, 50,Stroke
gr.set.stroke W / 16
gr.rect iv, 0, 0, ~
 Txtsigw, Txtsigh
gr.color 200, 0, 20, 50, Fill
gr.text.size H * 0.7
gr.text.align 2 % center
gr.text.setfont "sans serif"
gr.text.draw iv, Txtsigw / 2, ~
 Txtsigh * 0.65, "Signal"
bdend(&bdf, 0)


gr.color 50, 100, 30, 0, Fill
bpbgn = bmcreatefill ~
 (Bgnw, Bgnh, &bdf)
gr.color 100, 50, 50, 50,Stroke
gr.set.stroke W / 16
gr.rect iv, 0, 0, ~
 Bgnw, Bgnh
gr.color 200, 50, 0, 0, Fill
gr.text.size H * 0.7
gr.text.align 2 % center
gr.text.setfont "sans serif"
gr.text.draw iv, Bgnw / 2, ~
 Bgnh * 0.65, "Top"
bdend(&bdf, 0)

gosub Inimvvar
 
cen = rpos 
gosub Centomvty 
 
sigf = False
gosub Drawmvall 


nearop()
gr.bitmap.draw ob[Mv], bpmv, ~
 0, Topclip



! draws read mode

Lrkeyw = W * 4.3
Lrkeyh = H * 2
Lrkeyt = H * 12
Lrkeyb = Htclp
Lkeyl = W * 0.2
Lkeyr = Lkeyl + Lrkeyw
Rkeyl = W * 4.5
Rkeyr = Rkeyl + Lrkeyw


! left key
gr.bitmap.create bplk, ~
 Lrkeyw, Lrkeyh

 ! list for triangle
list.create n, trp
list.add trp, 0, 0, W/2, H/2, ~
 -W/2, H/2

! right key
gr.bitmap.create bprk, ~
 Lrkeyw, Lrkeyh

! list for reversed triangle
list.create n, rtrp
list.add rtrp, -W/2, 0, W/2, 0,~
 0, H/2


! list for big triangle
list.create n, btrp
list.add btrp, W, 0, -W * 0.7, ~
 H * 1.3, -W * 0.7, -H * 1.3


Win1w = W * 8
Win1h = H * 4.9
Win2w = Win1w
Win2h = H * 5

Win1l = W * 0.5
Win1t = H 
Win2l = Win1l
Win2t = H * 6

Win1txth = H * 0.7
gr.text.size Win1txth
gr.text.setfont "monospace"
gr.text.height , Win1txtu, ~
 Win1txtd
Win1linh = Win1txtd / 2 ~
 - Win1txtu
Win1txty = Win1h - Win1txtd * 2
Win1scrh = Win1h - Win1linh


Win2txth = H / 2
gr.text.size Win2txth
gr.text.setfont "monospace"
gr.text.height , Win2u, Win2d
Win2d_u = Win2d - Win2u
gr.text.width Win2txtw, "A"

Win2cen = Win2h / 2 + ~
 Win2txth * 0.35
Win2tpmgn = Win2txth / 3
Win2btmmgn = Win2tpmgn + ~
 Win2d_u - Win2txth
Win2lftmgn = Win2txtw
Win2ritmgn = Win2txtw / 2


! constants of play pause button
Ppbcx = Width / 2
Ppbcy = H * 6
Ppbrd = W * 3
Ppbl = Ppbcx - Ppbrd
Ppbr = Ppbcx + Ppbrd
Ppbt = Ppbcy - Ppbrd
Ppbb = Ppbcy + Ppbrd


wk$ = "A"
i = 0
gr.text.width wk, wk$
while wk <= Win2w - Win2lftmgn ~
 - Win2ritmgn
 i++
 wk$ += "A"
 gr.text.width wk, wk$
repeat

Rdlts = i 
! read mode letters in one line


gr.bitmap.create bpwin1, ~
 Win1w, Win1h
gr.bitmap.create bpwin1scr, ~
 Win1w, Win1scrh
gr.bitmap.create bpwin2, ~
 Win2w, Win2h
gr.bitmap.create bpwin2txt, ~
 Win2w, Win2h
 

gr.bitmap.create bpreado, ~
 Width, Htclp
gr.bitmap.create bpreadc, ~
 Width, Htclp

gosub Inicopy

gr.bitmap.create tall, ~
 Width, Htclp * 2
 
nearop()
gr.bitmap.draw ob[Rd], bpreadc,~
 0, Topclip

 
! draws settings mode

! list for play sample triangle
list.create n, plysam
list.add plysam, 0, -H * 0.7, ~
 W, 0, 0, H * 0.7

posst = 0
gr.bitmap.create stclip, ~
 Width, Htclp

stcolor()
bpst = ~
 bmcreatefill(Width,Thtst, &bdf)

gr.color 10,50,50,70, Stroke
gr.set.stroke W / 8
gr.line iv, 0, Topmgst - H / 8 ~
 , Width, Topmgst - H / 8
gr.set.stroke W / 16

for i = 1 to Numst + 1

 by = Topmgst + Htst * (i - 1)

 if i <= Numst then
 
  if i = 1 then
   ! reading mode  screen lock

   sti = i
   gosub Csts

  elseif i = 2 then % blackout

   sti = i
   gosub Tgsts

  endif

  if i = Numst then % reset all
   rstcolor()
  else
   stnamecolor()
  endif

  gr.text.size H * 0.45
  gr.text.draw iv,  ~
   w, by + H * 0.85 , Sname$[i]

 endif

 stlinecolord()
 gr.line iv, W/16, by - H/8, ~
  Width - W/16, by - H/8

 stlinecolorb()
 gr.line iv, W / 16, by, ~
  Width - W / 16, by

next

bdend(&bdf, 0)

gosub Drawst

nearop()
gr.bitmap.draw ob[St], stclip, ~
 0, Topclip


! draws mode lock
Mlmg = W / 8 % mode lock margin

gr.bitmap.create bpmlk, Width, ~
 Topclip
bdstart(bpmlk, &bdf, 0)
gr.color 255, 150, 150, 20, Fill
gr.rect iv, Mlmg, 0, ~
 Width - Mlmg, Topclip
gr.color 150,200,200,100, Stroke
gr.set.stroke W / 8
gr.rect iv, Mlmg + W / 8, ~
 H / 8, Width - Mlmg - W / 8, ~
 Topclip - H / 8
gr.text.align 2 % center
gr.text.setfont "sans serif"
gr.color 180, 255, 255, 255,Fill
gr.text.size H * 0.7
gr.text.draw iv, Width / 2, H, ~
 "Mode Locked"
gr.text.size H * 0.4
gr.text.draw iv, Width / 2, ~
 H * 1.5, ~
 "Long Press here to change mode"
 
bdend(&bdf, 0)
nearop()
gr.bitmap.draw ob[Mlk], bpmlk, ~
 0, -Topclip



! draws concealer

gr.bitmap.create bpccl, ~
 Width, Height
bdstart(bpccl, &bdf, 0)
bgcolor()
gr.rect iv, 0, 0, Width, Topclip
bdend(&bdf, 0)
gr.bitmap.draw ob[Ccl], bpccl, ~
 0, 0
gr.modify ob[Ccl], "alpha", 0



! draws dark screen

gr.color 255, 0, 0, 0, Fill
bpds = bmcreatefill(Width, ~ 
 Height, &bdf)
bdend(&bdf, 0)
gr.bitmap.draw ob[Ds],bpds, 0, 0
gr.modify ob[Ds], "alpha", 0


! draws choice box

gr.bitmap.create bpcbx, ~
 Wdbx, Htcbx

nearop() 
gr.bitmap.draw ob[Cbx], bpcbx, ~
 Cbxx, Cbxy

gr.hide ob[Cbx]



! draws volume with sample

gr.bitmap.create bpvls, ~
 Wdbx, Htvls

nearop() 
gr.bitmap.draw ob[Vls], bpvls, ~
 Vlsx, Vlsy

gr.hide ob[Vls]

gr.show ob[Bg]

vsflag = False




!*******************
!*******************
!#10 Main Routine **
!*******************
!*******************

do

 for i = Ed to St
  gr.hide ob[i]
 next
 gr.show ob[mode]
 
 gr.render


 sw.begin mode

 sw.case Ed
  gosub Editmode
 sw.break

 sw.case Mv
  gosub Movemode
 sw.break

 sw.case Rd
  gosub Readmode
 sw.break

 sw.case St
  gosub Settingsmode
 sw.break

 sw.end

until False


!**********************
!#11 data
!**********************

read.data ~
"Reading Mode", 2, "rmode", ~
 3, 1, 3, ~
"Blackout",1,"black", 0, 0, 1, ~
"General Vibration Length", 3, ~
 "vlen", 10, 2, 25, ~
"General Tone (& Silent) Length",~
 3, "tlen", 6, 1, 15, ~
"Length of Dot", 3, "ldot", ~
 10 , 1, 50, ~
"Length of Dash", 3, "ldash", ~
 45, 1, 200, ~
"Length of Short Space", 3, ~
 "lsspace", 60, 1, 200, ~
"Length of Long Space", 3, ~
 "llspace", 140, 1, 600, ~
"Length of White Space", 3, ~
 "lwspace", 500, 1, 3000, ~
"Pitch",3, "pitch", 37, 15, 60,~
"Reset All Settings", 4, ~



" ","_","\n","_", ~

"A",".-", ~
"B","-...", ~
"C","-.-.", ~
"D","-..", ~
"E",".", ~
"F","..-.", ~
"G","--.", ~
"H","....", ~
"I","..", ~
"J",".---", ~
"K","-.-", ~
"L",".-..", ~
"M","--", ~
"N","-.", ~
"O","---", ~
"P",".--.", ~
"Q","--.-", ~
"R",".-.", ~
"S","...", ~
"T","-", ~
"U","..-", ~
"V","...-", ~
"W",".--", ~
"X","-..-", ~
"Y","-.--", ~
"Z","--..", ~

! French accented letters
"#C9","E", ~
"#C8","E", ~
"#C0","A", ~
"#D9","U", ~
"#C2","A", ~
"#CA","E", ~
"#CE","I", ~
"#D4","O", ~
"#DB","U", ~
"#CB","E", ~
"#CF","I", ~
"#DC","U", ~
"#178","Y", ~
"#C7","C", ~

! Spanish letters
"#C1","A", ~ 
"#CD","I", ~
"#D3","O", ~
"#DA","U", ~
"#D1","N", ~


"0","-----", ~
"1",".----", ~
"2","..---", ~
"3","...--", ~
"4","....-", ~
"5",".....", ~
"6","-....", ~
"7","--...", ~
"8","---..", ~
"9","----.", ~


".",".-.-.-", ~
",","--..--", ~
":","---...", ~
";","-.-.-.", ~
"!","-.-.--", ~
"?","..--..", ~
"'",".----.", ~
"#2019","'", ~
"+",".-.-.", ~
"-","-....-", ~
"_","..--.-", ~
"/","-..-.", ~
"(","-.--.", ~
")","-.--.-", ~
"[","(", ~
"]",")", ~
"\"",".-..-.", ~
"#201C","\"", ~
"#201D","\"", ~
"@",".--.-.", ~
"&",".-...", ~
"=","-...-", ~
"%","------..-.-----", ~
! successive 0/0
"$","...-..-", ~
"#20AC","...-.-.---", ~
! successive EURO
"#A3","pound", ~
"#A5","yen", ~


"end of data"


!#0 End of the Program
!========================



