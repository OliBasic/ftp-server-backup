REM Start of BASIC! Program

rem Solar System V. 17

rem by Harold Hayes

print" Solar System"

print" Runs from January of the Year 2000 thru the Year selected."
print" Not totally accurate, mainly intended to show that"
print" Jupiter and Saturn come in line with each other"
print" every 20 years, i.e. 2000, 2020, 2040, 2060, etc."
print" and the realative movement of the 9 planets."
print" For Mercury and Venus, same as outer planets:
print" on-side, hrs. inline with Earths' hrs."
print" off-side, hrs. inline with Suns' hrs."

input"What Year to finish at: 1=2000, 2=2012, 3=2020, 4=2040, ?",yr2

if yr2=1 then yr2=2000

if yr2=2 then yr2=2012

if yr2=3 then yr2=2020

if yr2=4 then yr2=2040

da3=365

a$="        "
b$="       "
c$="      "

yr1=2000
da1=0
su1=18.7
su2=0
me1=18.1
ve1=12.3
ea1=6.8
ma1=24.4
ju1=1.5
sa1=2.5
ur1=21
ne1=20.4
pl1=16.7

go1:

da4=0

print"Sun is sid. loc. from Earth, planets are sid. loc. from Sun."
print"YEAR";a$;"DAY";a$;"SU";a$;"ME";a$;"VE";a$;"EA";a$;"MA";a$;
print"JU";a$;"SA";a$;"UR";a$;"NE";a$;"PL"

go2:

da1=da1+1
da4=da4+1

su1=su1+24/365
if su1>24 then su1=su1-24
su2=round(su1*10)/10

me1=me1+24/(0.241*365)
if me1>24 then me1=me1-24
me2=round(me1*10)/10

ve1=ve1+24/(0.614*365)
if ve1>24 then ve1=ve1-24
ve2=round(ve1*10)/10

ea1=ea1+24/365
if ea1>24 then ea1=ea1-24
ea2=round(ea1*10)/10

ma1=ma1+24/(1.9*365)
if ma1>24 then ma1=ma1-24
ma2=round(ma1*10)/10

ju1=ju1+24/(11.87*365)
if ju1>24 then ju1=ju1-24
ju2=round(ju1*10)/10

sa1=sa1+24/(29.45*365)
if sa1>24 then sa1=sa1-24
sa2=round(sa1*10)/10

ur1=ur1+24/(84.08*365)
if ur1>24 then ur1=ur1-24
ur2=round(ur1*10)/10

ne1=ne1+24/(164.93*365)
if ne1>24 then ne1=ne1
ne2=round(ne1*10)/10

pl1=pl1+24/(247.88*365)
if pl1>24 then pl1=pl1-24
pl2=round(pl1*10)/10

print yr1;c$;da1;c$;su2;c$;me2;c$;ve2;c$;ea2;c$;ma2;c$;ju2;c$;sa2;
print c$;ur2;c$;ne2;c$;pl2
 
if yr1=yr2 then if da1=da3 then end

if da4=30 then goto go1

if round(da1)>364 then yr1=yr1+1
if round(da1)>364 then da1=0

goto go2
 
end

 
 
 
