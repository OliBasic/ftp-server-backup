!tbname$ = "vm"                            % prefixe fondamental des fichiers utilisés
tbname$ = "ex"                            % prefixe fondamental des fichiers utilisés

!! Saisie assistée et graphique de fiches dans une base sql
On doit fournir comme datas au minimum 
- un prefixe (ici vm) défini dans le programme pour tbname$ = "vm" 
- un fichier vmmenu.csv (si le prefixe est vm) au contenu fixe utilisé dans le prog.
- deux fichiers .csv définissant les noms de colonne: vmsqlcol.csv et contenant les données vmvm.csv (si le prefixe est vm) qui au départ sont identiques, (à copier) 
Pour la création de fiches vierges, il y a des trucs:
*** si la colonne est "date", la date du jour est mise
*** si le nom de colonne commence par _ on copie le contenu de la fiche en cours
(facilite la saisie de séries)
Les fichiers en entrées et sortie sont tous en csv
Les autres fichiers csv sont le menu et des aides à la saisie, pour chaque ligne, sous forme de tableau. Dimensions et contenu libres, cases vides possibles. S'il n'y a pas de tableau, l'entrée est manuelle. 
*** Si la case d'aide contient "AUTRE", l'entrée est manuelle aussi.Chaque nom de colonne sql donne celui d'un nom de fichier d'aide.("vm"+nom+".csv")
La taille des zones graphiques est libre (définie à un seul endroit)
Les instructions sql ont des arguments à énumerer et pas des variables array.
On a donc des fonctions qui choisissent l'instruction selon le nombre de colonnes
définies dans le fichier vmsqlcol.csv, pour l'intant 3 à 12 colonnes
La base sql VM.DB,table vm, colonnes sqlc$[], se remplit avec les données vmvm.csv
Ses cases doivent toutes être remplies(risque si on édite à la main)
variables tableau: sqlc$(nom des colonnes sql) choix$(texte des cases d'aide) item$(texte des lignes de la fiche) gritem(ligne en objet graphique) faire$(texte du menu)
!!
! FONCTIONS ET GOSUB
! FONCTIONS SQL PARAMETREE (3 à 12 colonnes)
fn.def fnsqlnew(DB_Ptr, tbname$, sqlc$[],item$[])
  array.length nbcolsql, sqlc$[]  
  Time an$,mois$,jour$,heure$,minute$,seconde$
  date$= jour$+"/"+mois$+"/"+an$
  for n=1 to nbcolsql                            % valeurs spéciales des noms de colonne
    if left$(sqlc$[n],1) <> "_" then item$[n]=sqlc$[n]    % _ copie de la fiche en cours
    if sqlc$[n] = "date" then item$[n]=date$     % valeur "date" de nom de colonne sql 
  next n
  sw.begin nbcolsql
    sw.case 3
      sql.insert DB_Ptr, tbname$, sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3]
    sw.break
    sw.case 4
      sql.insert DB_Ptr, tbname$, sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3],item$[3],sqlc$[4],item$[4]
    sw.break
    sw.case 5
      sql.insert DB_Ptr, tbname$, sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3],item$[3],sqlc$[4],item$[4],sqlc$[5],item$[5]   
    sw.break
    sw.case 6
      sql.insert DB_Ptr, tbname$, sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3],item$[3],sqlc$[4],item$[4],sqlc$[5],item$[5],sqlc$[6],item$[6]    
    sw.break
    sw.case 7
      sql.insert DB_Ptr, tbname$, sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3],item$[3],sqlc$[4],item$[4],sqlc$[5],item$[5],sqlc$[6],item$[6],sqlc$[7],item$[7]   
    sw.break
    sw.case 8
      sql.insert DB_Ptr, tbname$, sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3],item$[3],sqlc$[4],item$[4],sqlc$[5],item$[5],sqlc$[6],item$[6],sqlc$[7],item$[7],sqlc$[8],item$[8]   
    sw.break
    sw.case 9
      sql.insert DB_Ptr, tbname$, sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3],item$[3],sqlc$[4],item$[4],sqlc$[5],item$[5],sqlc$[6],item$[6],sqlc$[7],item$[7],sqlc$[8],item$[8],sqlc$[9],item$[9]    
    sw.break
    sw.case 10
      sql.insert DB_Ptr, tbname$, sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3],item$[3],sqlc$[4],item$[4],sqlc$[5],item$[5],sqlc$[6],item$[6],sqlc$[7],item$[7],sqlc$[8],item$[8],sqlc$[9],item$[9],sqlc$[10],item$[10]    
    sw.break
    sw.case 11
      sql.insert DB_Ptr, tbname$, sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3],item$[3],sqlc$[4],item$[4],sqlc$[5],item$[5],sqlc$[6],item$[6],sqlc$[7],item$[7],sqlc$[8],item$[8],sqlc$[9],item$[9],sqlc$[10],item$[10],sqlc$[11],item$[11]    
    sw.break
    sw.case 12
      sql.insert DB_Ptr, tbname$, sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3],item$[3],sqlc$[4],item$[4],sqlc$[5],item$[5],sqlc$[6],item$[6],sqlc$[7],item$[7],sqlc$[8],item$[8],sqlc$[9],item$[9],sqlc$[10],item$[10],sqlc$[11],item$[11],sqlc$[12],item$[12]  
    sw.break
  sw.end
fn.rtn 0 
fn.end
fn.def fnsqlnext(nbcolsql,finite,cursor,novm$,item$[])
  sw.begin nbcolsql
    sw.case 3
      	sql.next finite,cursor,novm$,item$[1],item$[2],item$[3]
    sw.break
    sw.case 4
      	sql.next finite,cursor,novm$,item$[1],item$[2],item$[3],item$[4]
    sw.break
    sw.case 5
      	sql.next finite,cursor,novm$,item$[1],item$[2],item$[3],item$[4],item$[5]
    sw.break
    sw.case 6
      	sql.next finite,cursor,novm$,item$[1],item$[2],item$[3],item$[4],item$[5],item$[6]
    sw.break
    sw.case 7
      	sql.next finite,cursor,novm$,item$[1],item$[2],item$[3],item$[4],item$[5],item$[6],item$[7]
    sw.break
    sw.case 8
      	sql.next finite,cursor,novm$,item$[1],item$[2],item$[3],item$[4],item$[5],item$[6],item$[7],item$[8]
    sw.break
    sw.case 9
      	sql.next finite,cursor,novm$,item$[1],item$[2],item$[3],item$[4],item$[5],item$[6],item$[7],item$[8],item$[9]
    sw.break
    sw.case 10
      	sql.next finite,cursor,novm$,item$[1],item$[2],item$[3],item$[4],item$[5],item$[6],item$[7],item$[8],item$[9],item$[10]
    sw.break
    sw.case 11
      	sql.next finite,cursor,novm$,item$[1],item$[2],item$[3],item$[4],item$[5],item$[6],item$[7],item$[8],item$[9],item$[10],item$[11]
    sw.break
    sw.case 12
      	sql.next finite,cursor,novm$,item$[1],item$[2],item$[3],item$[4],item$[5],item$[6],item$[7],item$[8],item$[9],item$[10],item$[11],item$[12]
    sw.break  
  sw.end
  finite= !finite
  if finite then finite=val(novm$) 
fn.rtn finite   % rend 0 si pas trouvé ou novm, avec sql et item pointés sur novm
fn.end
fn.def fnsqlnewtab(DB_Ptr,tbname$,sqlc$[])
  array.length nbcolsql, sqlc$[]  
  sw.begin nbcolsql
    sw.case 3
     sql.new_table DB_Ptr,tbname$,sqlc$[1],sqlc$[2],sqlc$[3]
    sw.break  
    sw.case 4
     sql.new_table DB_Ptr,tbname$,sqlc$[1],sqlc$[2],sqlc$[3],sqlc$[4]
    sw.break
    sw.case 5
     sql.new_table DB_Ptr,tbname$,sqlc$[1],sqlc$[2],sqlc$[3],sqlc$[4],sqlc$[5]
    sw.break
    sw.case 6
     sql.new_table DB_Ptr,tbname$,sqlc$[1],sqlc$[2],sqlc$[3],sqlc$[4],sqlc$[5],sqlc$[6]
    sw.break
    sw.case 7
     sql.new_table DB_Ptr,tbname$,sqlc$[1],sqlc$[2],sqlc$[3],sqlc$[4],sqlc$[5],sqlc$[6],sqlc$[7]
    sw.break
    sw.case 8
     sql.new_table DB_Ptr,tbname$,sqlc$[1],sqlc$[2],sqlc$[3],sqlc$[4],sqlc$[5],sqlc$[6],sqlc$[7],sqlc$[8]
    sw.break
    sw.case 9
     sql.new_table DB_Ptr,tbname$,sqlc$[1],sqlc$[2],sqlc$[3],sqlc$[4],sqlc$[5],sqlc$[6],sqlc$[7],sqlc$[8],sqlc$[9]
    sw.break
    sw.case 10
     sql.new_table DB_Ptr,tbname$,sqlc$[1],sqlc$[2],sqlc$[3],sqlc$[4],sqlc$[5],sqlc$[6],sqlc$[7],sqlc$[8],sqlc$[9],sqlc$[10]
    sw.break
    sw.case 11
     sql.new_table DB_Ptr,tbname$,sqlc$[1],sqlc$[2],sqlc$[3],sqlc$[4],sqlc$[5],sqlc$[6],sqlc$[7],sqlc$[8],sqlc$[9],sqlc$[10],sqlc$[11]
    sw.break
    sw.case 12
     sql.new_table DB_Ptr,tbname$,sqlc$[1],sqlc$[2],sqlc$[3],sqlc$[4],sqlc$[5],sqlc$[6],sqlc$[7],sqlc$[8],sqlc$[9],sqlc$[10],sqlc$[11],sqlc$[12]
    sw.break
  sw.end
fn.rtn DB_Ptr
fn.end
fn.def fnsqlinsert(DB_Ptr,tbname$,sqlc$[],p$[])
  array.length nbcolsql, sqlc$[]  
  sw.begin nbcolsql
    sw.case 3
    sql.insert DB_Ptr, tbname$, sqlc$[1],p$[1],sqlc$[2],p$[2],sqlc$[3],p$[3]
    sw.break     
    sw.case 4
    sql.insert DB_Ptr, tbname$, sqlc$[1],p$[1],sqlc$[2],p$[2],sqlc$[3],p$[3],sqlc$[4],p$[4]     
    sw.break     
    sw.case 5
    sql.insert DB_Ptr, tbname$, sqlc$[1],p$[1],sqlc$[2],p$[2],sqlc$[3],p$[3],sqlc$[4],p$[4],sqlc$[5],p$[5]
    sw.break          
    sw.case 6
    sql.insert DB_Ptr, tbname$, sqlc$[1],p$[1],sqlc$[2],p$[2],sqlc$[3],p$[3],sqlc$[4],p$[4],sqlc$[5],p$[5],sqlc$[6],p$[6]
    sw.break          
    sw.case 7
    sql.insert DB_Ptr, tbname$, sqlc$[1],p$[1],sqlc$[2],p$[2],sqlc$[3],p$[3],sqlc$[4],p$[4],sqlc$[5],p$[5],sqlc$[6],p$[6],sqlc$[7],p$[7]
    sw.break           
    sw.case 8
    sql.insert DB_Ptr, tbname$, sqlc$[1],p$[1],sqlc$[2],p$[2],sqlc$[3],p$[3],sqlc$[4],p$[4],sqlc$[5],p$[5],sqlc$[6],p$[6],sqlc$[7],p$[7],sqlc$[8],p$[8]
    sw.break          
    sw.case 9
    sql.insert DB_Ptr, tbname$, sqlc$[1],p$[1],sqlc$[2],p$[2],sqlc$[3],p$[3],sqlc$[4],p$[4],sqlc$[5],p$[5],sqlc$[6],p$[6],sqlc$[7],p$[7],sqlc$[8],p$[8],sqlc$[9],p$[9]
    sw.break           
    sw.case 10
    sql.insert DB_Ptr, tbname$, sqlc$[1],p$[1],sqlc$[2],p$[2],sqlc$[3],p$[3],sqlc$[4],p$[4],sqlc$[5],p$[5],sqlc$[6],p$[6],sqlc$[7],p$[7],sqlc$[8],p$[8],sqlc$[9],p$[9],sqlc$[10],p$[10]
    sw.break           
    sw.case 11
    sql.insert DB_Ptr, tbname$, sqlc$[1],p$[1],sqlc$[2],p$[2],sqlc$[3],p$[3],sqlc$[4],p$[4],sqlc$[5],p$[5],sqlc$[6],p$[6],sqlc$[7],p$[7],sqlc$[8],p$[8],sqlc$[9],p$[9],sqlc$[10],p$[10],sqlc$[11],p$[11]
    sw.break           
    sw.case 12
    sql.insert DB_Ptr, tbname$, sqlc$[1],p$[1],sqlc$[2],p$[2],sqlc$[3],p$[3],sqlc$[4],p$[4],sqlc$[5],p$[5],sqlc$[6],p$[6],sqlc$[7],p$[7],sqlc$[8],p$[8],sqlc$[9],p$[9],sqlc$[10],p$[10],sqlc$[11],p$[11],sqlc$[12],p$[12]      
    sw.break
  sw.end
fn.rtn nbcolsql
fn.end
fn.def fnsqlupdate(DB_ptr,tbname$,sqlc$[],item$[],Oldfiche$) 
  array.length nbcolsql, sqlc$[]  
  sw.begin nbcolsql
    sw.case 3   
        sql.update DB_ptr, tbname$,sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3],item$[3]:Oldfiche$          
    sw.break
    sw.case 4   
        sql.update DB_ptr, tbname$,sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3],item$[3],sqlc$[4],item$[4]:Oldfiche$           
    sw.break
    sw.case 5   
        sql.update DB_ptr, tbname$,sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3],item$[3],sqlc$[4],item$[4],sqlc$[5],item$[5]:Oldfiche$         
    sw.break
    sw.case 6   
        sql.update DB_ptr, tbname$,sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3],item$[3],sqlc$[4],item$[4],sqlc$[5],item$[5],sqlc$[6],item$[6]:Oldfiche$          
    sw.break
    sw.case 7   
        sql.update DB_ptr, tbname$,sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3],item$[3],sqlc$[4],item$[4],sqlc$[5],item$[5],sqlc$[6],item$[6],sqlc$[7],item$[7]:Oldfiche$          
    sw.break
    sw.case 8   
        sql.update DB_ptr, tbname$,sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3],item$[3],sqlc$[4],item$[4],sqlc$[5],item$[5],sqlc$[6],item$[6],sqlc$[7],item$[7],sqlc$[8],item$[8]:Oldfiche$         
    sw.break
    sw.case 9   
        sql.update DB_ptr, tbname$,sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3],item$[3],sqlc$[4],item$[4],sqlc$[5],item$[5],sqlc$[6],item$[6],sqlc$[7],item$[7],sqlc$[8],item$[8],sqlc$[9],item$[9]:Oldfiche$         
    sw.break
    sw.case 10   
        sql.update DB_ptr, tbname$,sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3],item$[3],sqlc$[4],item$[4],sqlc$[5],item$[5],sqlc$[6],item$[6],sqlc$[7],item$[7],sqlc$[8],item$[8],sqlc$[9],item$[9],sqlc$[10],item$[10]:Oldfiche$ %          
    sw.break
    sw.case 11   
        sql.update DB_ptr, tbname$,sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3],item$[3],sqlc$[4],item$[4],sqlc$[5],item$[5],sqlc$[6],item$[6],sqlc$[7],item$[7],sqlc$[8],item$[8],sqlc$[9],item$[9],sqlc$[10],item$[10],sqlc$[11],item$[11]:Oldfiche$
    sw.break
    sw.case 12   
        sql.update DB_ptr, tbname$,sqlc$[1],item$[1],sqlc$[2],item$[2],sqlc$[3],item$[3],sqlc$[4],item$[4],sqlc$[5],item$[5],sqlc$[6],item$[6],sqlc$[7],item$[7],sqlc$[8],item$[8],sqlc$[9],item$[9],sqlc$[10],item$[10],sqlc$[11],item$[11],sqlc$[12],item$[12]:Oldfiche$         
    sw.break
  sw.end
fn.rtn 0
fn.end
! FONCTION : DESSIN D'UNE GRILLE DE PAVES
fn.def affgrille (nblin,nbcol,wdeb, hdeb, wend, hend, tonvert)
  wcell = (wend-wdeb)/nbcol 
  hcell = (hend-hdeb)/nblin 
  ! Lignes relief
  gr.color 128,0,0,0,1          % graphismes alpha,r,g,b,fill
  for l = 0 to nbcol
    gr.line n, wdeb+wcell*l, hdeb, wdeb+wcell*l, hend     % ligne verticale
  next l
  for l = 0 to nblin
    gr.line n, wdeb, hdeb+hcell*l, wend, hdeb+hcell*l     % ligne horizontale (rend n)
  next l
  ! Pavés couleur
  tonw = 255/nbcol
  tonh = 255/nblin
  for nolin = 0 to nblin -1
    for nocol = 0 to nbcol -1
       gr.color 128, 255-nocol*tonw,tonvert,255-nolin*tonh,1       % joli
       xrect = wdeb+wcell*nocol
       yrect = hdeb+hcell*nolin
       gr.rect nr, xrect+2, yrect+2, xrect+wcell, yrect+hcell
    next nocol
  next nolin
  fn.rtn 0
fn.end
! FONCTION: ALLUME CELLULE
fn.def cellon (x,y,hdeb,hend,nblin,wdeb,wend,nbcol,r,g,b)
    wcell = (wend-wdeb)/nbcol 
    hcell = (hend-hdeb)/nblin 
    xrect=(x-wdeb)-mod((x-wdeb),wcell)+wdeb
    yrect=(y-hdeb)-mod((y-hdeb),hcell)+hdeb
    gr.color 128, r,g,b,1
    gr.rect nr, 2+xrect, 2+yrect, xrect+wcell, yrect+hcell
    fn.rtn nr
fn.end
! FONCTION : AFFICHE LE TEXTE DANS LA CELLULE
fn.def affcell (txt$,nolin,nocol,hdeb,hend,nblin,wdeb,wend,nbcol)  
  wcell = (wend-wdeb)/nbcol 
  hcell = (hend-hdeb)/nblin 
  tailletxt = hcell/3
  if tailletxt > wcell/10 then tailletxt = wcell/10 % au moins 10 caracteres par case
  if tailletxt < 18 then tailletxt = 18  % taille min 18 pixels
  gr.text.size tailletxt                 % taille
  gr.text.align 2                        % alignement centre (1 gauche, 3 droit)
  xrect = wdeb+nocol*wcell-wcell         % le graphisme commence en 0 et pas en 1
  yrect = hdeb+nolin*hcell-hcell
  gr.color 255,0,0,0,1
  gr.text.draw celltxt, xrect+wcell/2, yrect+3*hcell/4, txt$
  fn.rtn celltxt
fn.end
! FONCTION: DIMENSIONS DU FICHIER EN TABLE
fn.def fich2d(c$) 
text.open R, idfich, c$               % fichier texte liste des choix
if idfich > -1                        % si le fichier existe
  nbmot = 0                           % premier tour pour avoir les dimensions
  nbligne = 0 
  do                              
  	text.readln idfich, a_line$
    nbligne = nbligne+1
    array.delete p$[]                
    split p$[], a_line$, ";"          % décompose le CSV dans le tableau p$[]
    array.length length, p$[]         % lit le nombre de colonnes
    if length > nbmot then nbmot = length  
  until a_line$ = "EOF"
  nbligne = nbligne-1
  text.close idfich
endif
d000=1000*nbligne+nbmot             % moche, pour rendre deux valeurs
fn.rtn d000
fn.end
! FONCTION: AFFICHE FICHIER GRILLE AIDE 
fn.def affaide(c$,choix$[],nblin,nbcol, wdeb,hdeb,wend, hend,tonvert) 
  n = affgrille (nblin,nbcol,wdeb, hdeb, wend, hend, tonvert)
  text.open R, idfich, c$              % fichier texte liste des choix 
  l=0                                  % ligne par ligne
  do
   	text.readln idfich, a_line$        % lit une ligne
    l= l+1.
    array.delete p$[]                
    split p$[], a_line$, ";"           % décompose la ligne dans le tableau p$[]
    array.length length, p$[]          % lit le nombre de colonnes de cette ligne
    for c= 1 to length
      celltxt = affcell (p$[c],l,c,hdeb,hend,nblin,wdeb,wend,nbcol)
      choix$[l,c]=p$[c]
    next c    
  until l = nblin
  text.close idfich                    % ferme fichier
fn.rtn 0
fn.end 
!FONCTION AFFICHE UNE LIGNE SOUS FORME LISTE
fn.def affliste(DB_Ptr,tbname$,nbcolsql,sqlc$[],Columns$,novm,item$[],gritem[],nblin, wdeb, hdeb,wend, hend,tonvert) 
n = affgrille (nblin,1,wdeb, hdeb, wend, hend, tonvert)
sql.query cursor, DB_Ptr, tbname$,  Columns$       % requête qui rend cursor 
finite = 0
do
   nolu= fnsqlnext(nbcolsql,finite,cursor,novm$,item$[])  % Lit >& avance
until  finite |novm=nolu
for l= 1 to nbcolsql
  gritem[l] = affcell (item$[l],l,1,hdeb,hend,nblin,wdeb,wend,1)
next l    
fn.rtn 0
fn.end

goto suite:
! GOSUB AFFICHE FICHE
affichefiche:
  gr.color 255,rouge,vert,bleu,1      % gomme
  gr.rect nr, wdeb,hdeb,wend,hend     % gomme choix
  gr.rect nr, lwdeb,lhdeb,lwend,lhend % gomme fiche
  n = affliste(DB_Ptr,tbname$,nbcolsql,sqlc$[],Columns$,novm,item$[],gritem[],lnblin,lwdeb,lhdeb,lwend,lhend,ltonvert) 
  gr.render
return
suite:

! Base VMEN, table vm
! le nom de la table sera mis en prefixe à tous les fichiers qu'elle utilise
! le fichier fondamental est (vm)sqlcol.csv qui contient les noms de colonnes sql
! la base générée s'appellera vmvm.csv, aux mêmes dimensions (sinon plante)
! les fichiers d'aide ont le nom ds colonnes sql exemple: _diag donne vm_diag.csv
! le _ indique une valeur item copiée de la fiche en cours pour une nouvelle fiche
! sinon les nouvelles fiches ont pour valeur item par défaut le nom de colonne sql

!tbname$ = "vm"                            % prefixe fondamental des fichiers utilisés
text.open R, idfich, tbname$+"sqlcol.csv"  % titres des colonnes sql sur une ligne
text.readln idfich, a_line$               % lit une ligne
array.delete sqlc$[]                
split sqlc$[], a_line$, ";"               % décompose la ligne dans le tableau sqlc$[].
array.length nbcolsql, sqlc$[]            % lit le nombre de colonnes sql

dbnom$ = "VMEN.db"                    % On ouvre une base
sql.open DB_Ptr, dbnom$
sql.drop_table DB_Ptr, tbname$        % crée table au nom du prefixe fondamental
n=fnsqlnewtab(DB_Ptr,tbname$,sqlc$[])

! POUR REQUETE: un index _id (obligatoire) et le nom des colonnes à voir
Columns$ = "_id,"
for n=1 to nbcolsql-1
  Columns$=Columns$+ sqlc$[n]  +"," 
next n
Columns$=Columns$+ sqlc$[n]


liste$=tbname$ + tbname$ +".csv"     % fichier csv même taille que sqlcol
! ENTRE FICHIER DANS BASE SQL
text.open R, idfich, liste$          % fichier texte 
do
  text.readln idfich, a_line$        % lit une ligne
  if a_line$ <> "EOF"
    array.delete p$[]                
    split p$[], a_line$, ";"         % décompose la ligne dans le tableau p$[].
     n=fnsqlinsert(DB_Ptr,tbname$,sqlc$[],p$[])  % met en sql,longueur de la ligne   
  endif
until a_line$ = "EOF"
text.close idfich                    % ferme fichier

! GRAPHISME GENERAL
rouge = 128
vert = 128
bleu = 128
gr.open 255, rouge, vert, bleu     % Fond opaque blanc
gr.orientation 0                   % Force landscape
gr.screen lecr, hecr               % Lit dimensions

! LISTE A REMPLIR
lwdeb = 10                      % marges en pixels
lhdeb = 17
lwend = lecr/3
lhend = 560
ltonvert = 200                  % couleur spécifique (on fait simple)


! REMPLIT ET AFFICHE LA VM
n=fich2d(liste$)                % lit dimensions en lignes et colonnes
lnblin = mod(n,1000)            % affichage de chaque ligne sous forme liste
nbvm= (n-lnblin)/1000           % nombre total de fiches
array.delete item$[]            % on  va mettre la fiche novm dedans
dim item$[lnblin]
dim old$[lnblin]                % on memorise la fiche à actualiser
dim gritem[lnblin]              % on aura besoin d'effacer les cases séparément

novm = nbvm                     % numero de la fiche à afficher
gosub affichefiche:
    
! GRILLE D'AIDE
wdeb = lecr/3                  % marges en pixels
hdeb = 17
wend = lecr-60
hend = hecr-100
tonvert = 43                   % couleur spécifique (on fait simple)


! MENU
mwdeb = lwend+10
mhdeb = hend+10
mwend = lecr-50
mhend = hecr-10
mtonvert = 255                  % couleur spécifique (on fait simple)
menu$ = tbname$+"menu.csv"
n=fich2d(menu$)                 % lit dimensions en lignes et colonnes
mnbcol=mod(n,1000)
mnblin= (n-mnbcol)/1000
array.delete faire$[]
dim faire$[mnblin,mnbcol]
n = affaide(menu$,faire$[],mnblin,mnbcol,mwdeb,mhdeb,mwend,mhend,mtonvert) 
gr.render


! GERE TOUCHE ECRAN
noitem=0
cherche =0
do
 gr.render                                          % actualise les cases touchées   
 gr.bounded.touch lstch,lwdeb,lhdeb,lwend,lhend     % a-t-on touche la liste
 gr.bounded.touch chtch,wdeb,hdeb,wend,hend         % a-t-on touche la grille aide
 gr.bounded.touch mtch,mwdeb,mhdeb,mwend,mhend      % a-t-on touche le menu 
   if lstch                             % on a touché la liste et on sait pas où
      tone 500,25
      tone 250,100
      Oldfiche$ = ""                    % Memorise la fiche à refiniter pour actualiser
      for n = 1 to nbcolsql-1
        Oldfiche$ = Oldfiche$+sqlc$[n]+"='"+item$[n]+"' AND "
      next n
      Oldfiche$ = Oldfiche$+sqlc$[n]+"='"+item$[n]+"'"
      !print Oldfiche$
     gr.touch flag, x, y
     noitem=ceil((y-lhdeb)/floor ((lhend-lhdeb)/lnblin )) 
     if noitem<>noitem2                 % on l'a touchée sur une autre ligne
       noitem2=noitem
       gr.hide lactif
       lactif = cellon (x,y,lhdeb,lhend,lnblin,lwdeb,lwend,1,0,0,0)
       gr.show lactif
       ltxt$=item$[ceil((y-lhdeb)/floor ((lhend-lhdeb)/lnblin))]
       noitem=ceil((y-lhdeb)/floor ((lhend-lhdeb)/lnblin ))
       fichaide$=tbname$+sqlc$[noitem]+".csv" 
       text.open R, idfich, fichaide$    % tente d'ouvrir un fichier d'aide
       if idfich  <> -1                  % s'il existe
         text.close idfich               % on le ferme pour les routines suivantes
         gr.color 255,rouge,vert,bleu,1       % gomme
         gr.rect nr, wdeb,hdeb,wend,hend
         ! REMPLIT ET AFFICHE CHOIX$
         n=fich2d(fichaide$)             % on affiche la nouvelle aide
         nbcol=mod(n,1000)
         nblin= (n-nbcol)/1000
         array.delete choix$[]
         dim choix$[nblin,nbcol]
         n = affaide(fichaide$,choix$[],nblin,nbcol, wdeb, hdeb,wend, hend,tonvert) 
       else                              % s'il n'y a pas de fichier d'aide
         gr.color 255,rouge,vert,bleu,1  % gomme
         gr.rect nr, wdeb,hdeb,wend,hend
         chtch = 1                      % faux choix fait
         txt$ = "AUTRE"                 % valeur pour entrée manuelle
       endif
     endif
   endif

   gr.hide actif                       % arrête le surlignage choix ou menu
   if chtch & noitem                   % choix sur liste et la liste attend son item
      tone 1000,25
      tone 500,100
      gr.touch flag, x, y
      gr.hide gritem[noitem]
      if txt$ <> "AUTRE"               % si ce n'est pas une entrée manuelle
        txt$=choix$[ceil((y-hdeb)/floor ((hend-hdeb)/nblin)),ceil((x-wdeb)/floor ((wend-wdeb)/nbcol))]                         % choix y compris "AUTRE"
         actif = cellon (x,y,hdeb,hend,nblin,wdeb,wend,nbcol,255,255,255)
      endif
      if txt$="AUTRE"                   % choix "AUTRE" dans la liste ou entrée manuelle
         gr.front 0                     % saisie manuelle
         kb.show
         input "Entrez "+sqlc$[noitem], txt$,""
         kb.hide
         gr.front 1
      endif
      item$[noitem]=txt$
      gritem[noitem]= affcell (txt$,noitem,1,lhdeb,lhend,lnblin,lwdeb,lwend,1) 
      gr.show gritem[noitem]
      txt$ = ""                          % désamorce les saisies et "AUTRE"
      lstch=1
      noitem=0
      gr.hide lactif
      ! actualiser aussi sql 
      if !cherche 
        sql.query Cursor, DB_Ptr, tbname$,  Columns$          % requête qui rend cursor
        n=fnsqlupdate(DB_ptr,tbname$,sqlc$[],item$[],Oldfiche$) % actualise avec item$  
      endif
   endif

  if mtch                                  % on a touché le menu
      tone 2000,25     
      tone 1000,200
      gr.color 255,rouge,vert,bleu,1       % gomme aide
      gr.rect nr, wdeb,hdeb,wend,hend
      gr.touch flag, x, y
      txt$=faire$[ceil((y-mhdeb)/floor ((mhend-mhdeb)/mnblin)),ceil((x-mwdeb)/floor ((mwend-mwdeb)/mnbcol))]
      actif = cellon (x,y,mhdeb,mhend,mnblin,mwdeb,mwend,mnbcol,128,0,255)

      if txt$ = "SUIVANTE"
        novm = novm+1
        if novm > nbvm
           tone 2000,1000
           novm = nbvm
        else
          gosub affichefiche:
        endif
      endif

      if txt$ = "PRECEDENTE"
        novm = novm-1
        if novm < 1 
           tone 2000,1000
           novm = 1
        else
           gosub affichefiche:
        endif
      endif

      if txt$ = "SAUVE" & !cherche
        Time an$,mois$,jour$,heure$,minute$,seconde$
        rename liste$,an$+mois$+jour$+heure$+minute$+liste$   % sauvegarde datée
        text.open W, idfich, liste$         % ouvre fichier texte à écrire
        sql.query Cursor, DB_Ptr, tbname$,  Columns$          % requête qui rend cursor 
        novm$=""                
        while fnsqlnext(nbcolsql,finite,cursor,novm$,old$[])  % tant qu'y a des fiches
          newlin$=""
          for l= 1 to nbcolsql-1              % forme la nouvelle ligne
             newlin$= newlin$+old$[l]+";"
          next l
          newlin$= newlin$+old$[l]
          text.writeln idfich, newlin$      % écrit la ligne           
        repeat                           
        text.close idfich                   % ferme fichier
      endif

      if txt$ = "NOUVELLE" & !cherche
        n=fnsqlnew(DB_Ptr, tbname$, sqlc$[],item$[])
        ! actualiser sql avec date,lieu,classe courants puis avec les titres de colonne
        nbvm = nbvm+1
        novm=nbvm    
        gosub affichefiche:           
      endif

      if txt$ = "CHERCHE"
        cherche = 1                    % flag qui évite de sauver sur la fiche en cours
        ! FORMULAIRE, les item$[] sont remplis par 
        for l= 1 to nbcolsql          % formulaire vide
         item$[l]= sqlc$[l]            % avec les titres de colonne
        next l    
        gr.color 255,rouge,vert,bleu,1      % gomme fiche
        gr.rect nr, lwdeb,lhdeb,lwend,lhend
        n = affgrille (lnblin,1,lwdeb, lhdeb,lwend, lhend, 0)         
        for l= 1 to nbcolsql            % affiche formulaire
            gritem[l] = affcell (item$[l],l,1,lhdeb,lhend,lnblin,lwdeb,lwend,1)
        next l     
        ! AFFICHE AIDE SAISIE
         n=fich2d(tbname$+"saisie.csv")         % on affiche la nouvelle aide
         nbcol=mod(n,1000)
         nblin= (n-nbcol)/1000
         array.delete choix$[]
         dim choix$[nblin,nbcol]
         n = affaide(tbname$ +"saisie.csv",choix$[],nblin,nbcol,wdeb,hdeb,wend, hend,255)  
      endif

      if txt$ = "TROUVE" & cherche
       !on ne retient que ce qui est different des sqlc$
       Where$ = ""
       for l= 1 to nbcolsql 
         if item$ [l] <> sqlc$[l] 
           if Where$ <> "" then Where$ = Where$+" AND " 
           Where$ = Where$+sqlc$[l]+" LIKE '"+item$[l]+"'"
         endif
       next l
       print Where$
!       Order$ = "last_name ASC"
       sql.query Cursor, DB_Ptr, tbname$,  Columns$ ,Where$      % rend Curseur 
       finite = 0
       gr.front 0
       cls
       print "Cliquez la loupe pour revenir en mode graphique et entrée ------->"
       while fnsqlnext(nbcolsql,finite,cursor,novm$,p$[])
            for n=1 to nbcolsql
              index$=index$+";"+p$[n]
             next n
             print index$
       repeat
       do
         inkey$ key$	  
       until key$="key 84"
       gr.front 1
       cherche=0                    % on revient à l'affichage 
       gosub affichefiche:          % de la fiche en cours  
      endif

   endif


until 0


OnError:
sql.close DB_Ptr
end
 
 
 
 
