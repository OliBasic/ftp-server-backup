! text on shake
! program has to be trained first
! on average jolts... 
! you can move the phone around gently for a minute
! or two to fill the buffer up
! and will display Ready! when it is ready for use
!
! WARNING:  Not recommended to use if you don't have unlimited SMS with your mobile plan!
!
!
FN.DEF writeln(f$, msg$) 
 TEXT.OPEN w, fh, f$
 TEXT.WRITELN fh, msg$
 TEXT.CLOSE fh 
FN.END

FN.DEF readln$(f$) 
 FILE.EXISTS isoldfile, f$ 
 IF isoldfile
  TEXT.OPEN r,fh3,f$
  TEXT.READLN fh3, a$
 ELSE
  LET A$=""
 ENDIF
 FN.RTN a$
FN.END
tts.init

Buf=0			% buffer index
Bufsiz=50		% ring buffer size
buffull=0		% true if buffer full
Noise = 20 	 % threshhold for noise
sens=3		 % sensitivity in # of standard deviations

DIM changes[bufsiz] % ring buffer

print "shake2text"

print "tap screen to change default phone# and msg"
print "...or wait 3 seconds"

tstart=CLOCK()
T=0 
DO 
pause 100
touch = (t=1) 
UNTIL touch | CLOCK()-tstart>3000
p$=readln$("phone.txt")
msg$=readln$("msg.txt")
if touch
CLS
input "enter phone to send text to:",p$,p$
writeln("phone.txt",p$)
input "enter message to send:",msg$,msg$
writeln("msg.txt",msg$)
endif 
CLS
PRINT "Filling buffer..."
PRINT "Move phone around gently until Ready"
PRINT

! Open the acclerometer sensor
SENSORS.OPEN 1
SENSORS.READ 1,oldy,oldx,oldz
TIMER.SET 100

! endless loop
do
pause 50
if jolt
jolt=0
sms.send p$,t$+chr$(10)+msg$
tts.speak "message sent",1
endif

until 0

TIMER.CLEAR
END

ONTIMER:
SENSORS.READ 1,y,x,z
Dx = (x-oldx)
Dy = (y-oldy)
Dz = (z-oldz)
Change = dx*dx+dy*dy+dz*dz
IF change > noise THEN
 Buf++
 IF buf>bufsiz
  buf=1
  buffull=1
 ENDIF
 Changes[buf] =change
ENDIF
Oldx=x
Oldy=y
Oldz=z


IF buffull
if !bflag
  print "ready!"
  bflag=1
endif
 ARRAY.AVERAGE avgchange, changes[]
 ARRAY.STD_DEV sdev, changes[]

 Bigchange=avgchange
 IF change > bigchange+sdev*sens
  PRINT change
  TIME year$, month$, day$, hour$, minutes$, seconds$
  t$ = month$+"-"+day$+" "+hour$+" :"+minutes$+":"+seconds$
  PRINT t$
jolt=1  % tell main program there is a jolt
    Bigjolts++
 ENDIF

ENDIF
TIMER.RESUME


! Set flag if screen touched
ONCONSOLETOUCH:
T=1
CONSOLETOUCH.RESUME

