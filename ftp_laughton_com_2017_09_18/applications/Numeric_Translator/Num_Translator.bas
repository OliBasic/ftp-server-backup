REM           Numerical translator by Alberto
REM                   March 2016
REM___________________________________________________________________

   LET F_apk = 1
   File.root Rt$
   If Is_In("RFO-BASIC",upper$(Rt$)) then LET F_apk = 0 
!_____________________________________________________________________


 let M_Text$ = ""
   Let Index = 0

gosub Matrix

!----------------------------------------------------------
!                     Set Keys layout
!----------------------------------------------------------

Let X_Offset = 100          % Offset X axis
Let Y_Offset = 220          % Offset Y axis

 Let Set_Col = 5            % Number of Columns (max 6)
 Let Set_Row = 4            % Number of Rows  (max 4)

   Let Width = 205          % Key width
  Let Height = 95           % Key height

!--------------------- Set Key Text -----------------------

    Let TSize = 35                    % Font size (Key Text)

 Let Btxt$[1] = "1"
 Let Btxt$[2] = "2"
 Let Btxt$[3] = "3"
 Let Btxt$[4] = "4"
 Let Btxt$[5] = "5"
 Let Btxt$[6] = "6"
 Let Btxt$[7] = "7"
 Let Btxt$[8] = "8"
 Let Btxt$[9] = "9"
Let Btxt$[10] = "0"
Let Btxt$[11] = "•"
Let Btxt$[12] = "BIN"
Let Btxt$[13] = "HEX"
Let Btxt$[14] = "OCT"
Let Btxt$[15] = "CHR$"
Let Btxt$[16] = "SIN"
Let Btxt$[17] = "COS"
Let Btxt$[18] = "TAN"
Let Btxt$[19] = "LN"
Let Btxt$[20] = "LOG"

!------------------ Set program text ----------------------

    Let MSize = 55              % Font size
   Let M_Txt$ = "Numeric translator"
      Let aa$ = M_Txt$
!----------------------------------------------------------
!                End Keyboard layout
!----------------------------------------------------------
 
If Set_Col < 1 then Set_Col = 1
If Set_Col > 6 then Set_Col = 6
If Set_Row < 1 then Set_Row = 1
If Set_Row > 4 then Set_Row = 4
  
Let BtN = (Set_Col * Set_Row)     % Total number of Keys

Let RSp = ((720 - (Y_Offset+60))/Set_Row)

If Set_Col = 1 then
   Let CSp = ((1280 - (2 * X_Offset)-(Set_Col * Width))) + Width
else
   Let CSp = ((1280 - (2 * X_Offset)-(Set_Col * Width))/(Set_Col-1)) + Width
Endif

gosub Fix_Col

!----------------------------------------------------------
!                Open Graphic Screen
!----------------------------------------------------------

Let Fix_W = 1280
Let Fix_H = 720

GR.OPEN 255, 140, 140, 140,0
GR.ORIENTATION 0

Pause 200

gr.screen DW, DH

  Let SW = DW/Fix_W
  Let SH = DH/Fix_H

gr.scale SW, SH

gosub Draw_PB

!---------------------- program loop ----------------------

Main_Loop:
pause 1
If (Index > 0) & (Index < 12) Then gosub Number
If (Index > 11) & (aa$ <> "") Then gosub Select
!Let Index = 0        % Reset Index 
goto Main_Loop

!-------------------------------------------------------
!                     Ini sub-routines 
!-------------------------------------------------------

Matrix:
Dim TT[24]
Dim RT[24]
Dim BT[24,4]
Dim X1[24,2]
Dim Y1[24,2]
Dim Btxt$[24]
Dim vib[2]
Let Index = 0
Let vib[1] = 0
Let vib[2] = 25
Return

!----------------------------------------------------------

Fix_Col:
Let ac = 0
Let ar = 0
for a = 1 to BtN
Let X1[a,1] = X_Offset + (CSp*ac)
Let Y1[a,1] = Y_Offset + (RSp*ar)

if Set_Col>ac then ++ac
if Set_Col = ac then ac = 0
if Set_Row>ar & ac = 0 then ++ar

next a
Return

!----------------------- Draw Key style -------------------

Draw_PB:

gr.color 255,0,0,0,1
gr.rect wa, 100,80,1185,190    % text box
gr.line M00,60,50,1220,50
gr.line M01,60,50,60,660

gr.color 255,255,255,255
gr.line M00,60,660,1220,660
gr.line M01,1220,50,1220,660

For a = 1 to BtN

GR.COLOR 255, 185, 185, 185, 1

Let X1[a,2] = X1[a,1] + Width
Let Y1[a,2] = Y1[a,1] + Height
GR.RECT RT[a], X1[a,1], Y1[a,1], X1[a,2], Y1[a,2]

GR.COLOR 255, 225, 225, 225,0

gr.set.stroke 4
gr.line BT[a,1], X1[a,2], Y1[a,1], X1[a,2], Y1[a,2]
gr.line BT[a,2], X1[a,1], Y1[a,1], X1[a,2], Y1[a,1]

gr.color 255, 30, 30, 30, 1

gr.set.stroke 3
gr.line BT[a,3], X1[a,1], Y1[a,1], X1[a,1], Y1[a,2]
gr.set.stroke 6
gr.line BT[a,4], X1[a,1], Y1[a,2], X1[a,2], Y1[a,2]

gr.color 255,0,0,0
gr.text.size TSize
gr.text.align 2
gr.text.draw TT[a],X1[a,1]+((X1[a,2]-X1[a,1])/2),Y1[a,1]+((Y1[a,2]-Y1[a,1])/2)+12,Btxt$[a]
next a

gr.color 255,255,255,255
gr.text.size MSize
gr.text.align 2
gr.text.draw MT,640,(Y_Offset/2)+40,M_Txt$

GR.RENDER

Return

!----------------------------------------------------------

Select:

Sw.Begin Index

Sw.Case 20
let M_Text$ = "LOG(" + aa$ + ") = " + Str$(LOG10(val(aa$)))
gr.modify MT, "text", M_Text$
gr.render
aa$ = ""
Sw.Break

Sw.Case 19
let M_Text$ = "LN(" + aa$ + ") = " + Str$(LOG(val(aa$)))
gr.modify MT, "text", M_Text$
gr.render
aa$ = ""
Sw.Break

Sw.Case 18
let M_Text$ = "TAN(" + aa$ + ") = " + Str$(TAN(val(aa$)))
gr.modify MT, "text", M_Text$
gr.render
aa$ = ""
Sw.Break

Sw.Case 17
let M_Text$ = "COS(" + aa$ + ") = " + Str$(COS(val(aa$)))
gr.modify MT, "text", M_Text$
gr.render
aa$ = ""
Sw.Break

Sw.Case 16
let M_Text$ = "SIN(" + aa$ + ") = " + Str$(SIN(val(aa$)))
gr.modify MT, "text", M_Text$
gr.render
aa$ = ""
Sw.Break

Sw.Case 15
let M_Text$ = "CHR$(" + aa$ + ") = " + chr$(34) + Chr$(val(aa$)) + chr$(34)
gr.modify MT, "text", M_Text$
gr.render
aa$ = ""
Sw.Break

Sw.Case 14
let M_Text$ = "Oct(" + aa$ + ") = " + Oct$(val(aa$))
gr.modify MT, "text", M_Text$
gr.render
aa$ = ""
Sw.Break

Sw.Case 13 
let M_Text$ = "Hex(" + aa$ + ") = " + hex$(val(aa$))
gr.modify MT, "text", M_Text$
gr.render
aa$ = ""
Sw.Break

Sw.Case 12
let M_Text$ = "Bin(" + aa$ + ") = " + bin$(val(aa$))
gr.modify MT, "text", M_Text$
gr.render
aa$ = ""
Sw.Break

Sw.End
Let Index = 0
Return

!----------------------------------------------------------

Number:
if Index = 11 Then
let aa$ = aa$ + "."
else
let aa$ = aa$ + Btxt$[Index]
endif
gr.modify MT,"text",aa$
gr.render
Let Index = 0
Return

!-------------- 3D effect & Touch decode ------------------

OnGrTouch:

do
GR.TOUCH touched, x, y
until touched

    let x = x / SW
    Let y = y / SH
Let Index = 0

for a = 1 to BtN
 if (x > X1[a,1]) & (x < X1[a,2]) then
 if (y > Y1[a,1]) & (y < Y1[a,2]) then
 Let Index = a
 endif
 endif
next a

if Index = 0 Then goto Skip_0
Let a = Index

gr.modify BT[a,1],"x1",X1[a,1],"y1",Y1[a,1],"x2",X1[a,1],"y2",Y1[a,2]
gr.modify BT[a,2],"x1",X1[a,1],"y1",Y1[a,2],"x2",X1[a,2],"y2",Y1[a,2]

gr.modify BT[a,3],"x1",X1[a,2],"y1",Y1[a,1],"x2",X1[a,2],"y2",Y1[a,2]
gr.modify BT[a,4],"x1",X1[a,1],"y1",Y1[a,1],"x2",X1[a,2],"y2",Y1[a,1]

gr.render

Vibrate vib[],-1

Skip_0:

DO
pause 100
GR.TOUCH touched, xx, yy
UNTIL !touched

if Index = 0 Then goto Skip_1

gr.modify BT[a,1],"x1",X1[a,2],"y1",Y1[a,1],"x2",X1[a,2],"y2",Y1[a,2]
gr.modify BT[a,2],"x1",X1[a,1],"y1",Y1[a,1],"x2",X1[a,2],"y2",Y1[a,1]

gr.modify BT[a,3],"x1",X1[a,1],"y1",Y1[a,1],"x2",X1[a,1],"y2",Y1[a,2]
gr.modify BT[a,4],"x1",X1[a,1],"y1",Y1[a,2],"x2",X1[a,2],"y2",Y1[a,2]

gr.render

Skip_1:

if abs(xx - x) > 400 & (yy< 200) & y < 200 then
Let aa$ = ""
let M_Text$ = ""
gr.modify MT, "text", aa$
gr.render
endif

Gr.onGrTouch.Resume

!--------------------------------- Quit command ----------------------

OnBackKey:
if aa$ <> "" then goto Skip_Quit
if M_Text$ <> "" then goto Skip_Quit
If F_apk = 0 then
end
else
exit
endif

Skip_Quit:
Back.Resume

!------------------------ End of code ---------------------

end  

