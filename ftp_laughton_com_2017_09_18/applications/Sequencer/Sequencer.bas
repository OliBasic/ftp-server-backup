Fn.def STG$(c)
  Fn.rtn Left$(Str$(c),Is_In(".",Str$(c))-1)
Fn.end

Fn.def gRVB(color$)
  gr.color 255, val(word$(color$,1)), val(word$(color$,2)), val(word$(color$,3)), 1
Fn.end

Fn.def dr(tpo,sig)  % to respect metronome  bpm = 40 - +/-2 - 60 - +/-3 - 72 - +/-4 - 120 - +/-6 - 144 - +/-8 - 24
if tpo<60
  dr = 2
elseif tpo=60
  if sig>0 then dr = 3 else dr = 2
elseif tpo<72
  dr = 3
elseif tpo=72
  if sig>0 then dr = 4 else dr = 3
elseif tpo<120
  dr = 4
elseif tpo=120
  if sig>0 then dr = 6 else dr = 4
elseif tpo<144
  dr = 6
elseif tpo=144
  if sig>0 then dr = 8 else dr = 6
elseif tpo<248
  dr = 8
endif
Fn.rtn dr
Fn.end

GR.OPEN 255,0,0,0,1,1    %  Black, StatusBar forced & orientation =portrait

! LOAD AND SCALE GRAPHICS !

GR.SCREEN real_w, real_h
di_w = 310
di_h = 466
scale_w = real_w/di_w
scale_h = real_h/di_h
GR.SCALE scale_w , scale_h
Path$ = "Sequencer/"

! CONSTANTS !

Sound$ = "1_CrCymbal 1_RyCymbal 1_RyBell 1_OpHihat 1_PeHihat "~           % color_name sounds files
        +"3_HiConga 3_LoConga 3_HiTimbal 3_LoTimbal 3_Hitom1 3_Hitom2 3_LoTom "~
        +"5_Snare 5_Snare1 4_HiBongo 4_LoBongo 4_HiWood 4_LoWood 4_Stick 4_Cowbell "~
        +"6_HaClap 6_Guiro 6_Logogo 6_Higogo "
NbSdMax = 24  % the nber of words in the string Sounds$.   24 max.
DIM notes[16*NbSdMax ,NbSdMax]
DIM clBrd[16,NbSdMax]
DIM notePtr[16,NbSdMax]
DIM pagePtr[NbSdMax]
DIM snd[NbSdMax]
DIM color$[6]
DIM butPtr[10]
bpm = 120  % Beat Per Minute  :  see the FUNCTION dr(tpo,sig)
nte = 16   % 1, 2, 4, 8, 16, 32, 64
ox= 0
oy= 36
color$[1]="000 255 000"  % green
color$[2]="010 110 000"  % darkgreen
color$[3]="255 000 255"  % pink
color$[4]="000 255 255"  % cyan
color$[5]="000 128 128"  % darkcyan
color$[6]="255 255 000"  % yellow

! Sounds !
Soundpool.open NbSdMax
for y=0 to NbSdMax-1
  soundname$= word$(Sound$,y+1)
  soundpool.load snd[y+1], path$+word$(soundname$,2,"_")+".wav"
next

! load bitmaps
GR.BITMAP.LOAD tmpBmp,path$+"Sequencer.png"   % 31x220

for b=1 to 10
  gr.bitmap.crop butPtr[b], tmpBmp, 1, (b-1)*17, 29, 16
next
! 1=open file
! 2=clear All
! 3=record
! 4=stop
! 5=play      % buttons
! 6=Up
! 7=Down
! 8=Cut
! 9=Copy
!10=Paste
gr.bitmap.crop ledB, tmpBmp, 1, 171, 13, 13
gr.bitmap.crop ledR, tmpBmp, 1, 186, 13, 13
gr.bitmap.crop ledV, tmpBmp, 1, 201, 13, 13
gr.bitmap.delete tmpBmp

stX=30
stY=40
dcell = 14
gr.text.size dcell-3
for y=0 to NbSdMax-1
  gr.bitmap.draw pagePtr[y+1], ledV, stX-30, stY+y*dcell
  call gRVB(color$[2])
  gr.text.draw nul, stX-15, stY+11+y*dcell, STG$(y+1)
  for x=0 to 15
    gr.bitmap.draw notePtr[x+1,y+1], ledV, stX+x*dcell, stY+y*dcell
    if x & mod(x,4)=0 then gr.line nul, stX-1+x*dcell, stY, stX+x*dcell, (NbSdMax+2.9)*dcell
  next
  soundname$=word$(Sound$,y+1)
  call gRVB(color$[val(word$(soundname$,1,"_"))])
  gr.text.draw nul, stX+2+x*dcell, stY+10+y*dcell, word$(soundname$,2,"_")
next

! scan line !
gr.bitmap.create scanP, dcell-1, NbSdMax*dcell
gr.bitmap.drawinto.start scanP
for s=0 to 4
  gr.bitmap.draw nul, ledB, 0, s*(dcell*5)
next
gr.bitmap.drawinto.end
gr.bitmap.draw scan, scanP, stX, stY
! Buttons !

gr.bitmap.draw bt6, butPtr[6], 1*dcell, 29*dcell
gr.bitmap.draw bt7, butPtr[7], 1*dcell, 31.5*dcell
gr.bitmap.draw bt6, butPtr[6], 4*dcell, 29*dcell
gr.bitmap.draw bt7, butPtr[7], 4*dcell, 31.5*dcell

gr.bitmap.draw bt2, butPtr[2],  7*(dcell+4), 29*dcell
gr.bitmap.draw bt1, butPtr[1],  9*(dcell+4), 29*dcell
gr.bitmap.draw bt3, butPtr[3], 11*(dcell+4), 29*dcell

gr.bitmap.draw bt9, butPtr[9],  7*(dcell+4), 31.5*dcell
gr.bitmap.draw bt8, butPtr[8],  9*(dcell+4), 31.5*dcell
gr.bitmap.draw bt10, butPtr[10], 11*(dcell+4), 31.5*dcell

gr.bitmap.draw bt5, butPtr[5], 14.7*(dcell+4), 29*dcell
gr.bitmap.draw bt4, butPtr[4], 14.7*(dcell+4), 31.5*dcell

call gRVB(color$[2])
gr.line nul, 0, stY-6, real_w, stY-6
gr.line nul, 0, stY+6+24*dcell, real_w*scale_w, stY+6+24*dcell
gr.set.stroke 2
gr.line nul, 0, stY+6+30*dcell, real_w*scale_w, stY+6+30*dcell

call gRVB(color$[4])
gr.text.size dcell-4
gr.text.draw nul, 4+1*dcell, 7+28*dcell, "Note"
gr.text.draw nul, 4+4*dcell, 7+28*dcell, "bpm"
call gRVB(color$[1])
gr.text.align 2
gr.text.draw txtnte, 1+2*dcell, 2+31*dcell, STG$(nte)
gr.text.draw txtbpm, 1+5*dcell, 2+31*dcell, STG$(bpm)

gr.text.draw info, stX+10*dcell, 28*dcell, ""   % just for debug

! title !
call gRVB(color$[2])
gr.text.size dcell*1.5
gr.text.bold 1
gr.text.skew -0.1
gr.text.draw nul, (real_w/scale_w)/2+2-20, 2.3*dcell+2, "S E Q U E N C E R"
call gRVB(color$[1])
gr.text.draw nul, (real_w/scale_w)/2-20, 2.3*dcell, "S E Q U E N C E R"
gr.render

page= 0
lastpage= 0
gr.modify pagePtr[page+1], "bitmap", ledR

tempo= floor((60000/bpm)*(4/nte))
Timer.set tempo
Trun=1

DO
  DO
    GR.TOUCH touched,tx,ty    % wait for touch
    if !background() then gr.render
  UNTIL touched
  DO
    GR.TOUCH touched, dx,dy    % wait for untouch
  UNTIL !touched
  dx/=scale_w
  dy/=scale_h
  sx= floor((dx-stX)/dcell)+1
  sy= floor((dy-stY)/dcell)+1
  
  gr.modify info, "text", stg$(sx)+" , "+stg$(sy)    % just for debug
  
  if sx<0 & sy>0 & sy<25    % the left alone column = pages    
    if lastpage<>sy-1
      if sy-1<lastpage
        for p=sy to lastpage
          if p<>page then gr.modify pagePtr[p+1], "bitmap", ledV
        next
      else
        for p=lastpage to sy-1
          if p<>page then gr.modify pagePtr[p+1], "bitmap", ledB
        next
      endif
      lastpage= sy-1
    endif
  
  elseif sx>0 & sx<17 & sy>0 & sy<25   % the grid of instruments
  
    if notes[page*16+sx,sy]=1
      notes[page*16+sx,sy]=0
      clBrd[sx,sy]=0
      gr.modify notePtr[sx,sy], "bitmap", ledV
    else
      notes[page*16+sx,sy]=1
      clBrd[sx,sy]=1
      gr.modify notePtr[sx,sy], "bitmap", ledR
      Soundpool.play nul,snd[sy],0.99,0.99,1,0,1
    endif
    scx= sx
    
  else                % buttons
    if sy=27 | sy=28
      if sx=0 | sx=1               % nte up
        nte*=2
        if nte>64 then nte=64
        gr.modify txtnte, "text", STG$(nte)
        majTempo=1
      elseif sx=3 | sx=4           % bpm up
        sign=1
        bpm= bpm+dr(bpm,sign)
        if bpm>=240 then bpm=240
        gr.modify txtbpm, "text", STG$(bpm)
        majTempo=1
      elseif sx>16 & sx<20 & !Trun        % play
        Timer.set tempo
        Trun=1
      endif
    elseif sy=29 | sy=30
      if sx=0 | sx=1               % nte down
        nte/=2
        if nte<1 then nte=1
        gr.modify txtnte, "text", STG$(nte)
        majTempo=1
      elseif sx=3 | sx=4           % bpm down
        sign=-1
        bpm= bpm-dr(bpm,sign)
        if bpm<=40 then bpm=40
        gr.modify txtbpm, "text", STG$(bpm)
        majTempo=1
      elseif sx>16 & sx<20 & Trun     % stop
        Timer.Clear
        Trun=0
      endif
    endif
    if majTempo
      majTempo=0
      tempo= floor((60000/bpm)*(4/nte))
      if tempo<101 then tempo=100
      if Trun
        Timer.Clear
        pause 10
        Timer.Set tempo
      endif
    endif
  endif
UNTIL 0

ShowPage:
for cy=1 to NbSdMax
  for cx=1 to 16
    if notes[page*16+cx,cy] <> clBrd[cx,cy]
      if notes[page*16+cx,cy]
        gr.modify notePtr[cx,cy], "bitmap", ledR
      else
        gr.modify notePtr[cx,cy], "bitmap", ledV
      endif
      clBrd[cx,cy]= notes[page*16+cx,cy] 
    endif
  next
next
RETURN

OnTimer:
  scx++
  if scx>16
    if page<=lastpage
      gr.modify pagePtr[page+1], "bitmap", ledB
      if page=lastpage
        page=0
      elseif page<lastpage
        page++
        if page>NbSdMax then page=0
      endif
    else
      gr.modify pagePtr[page+1], "bitmap", ledV
      page= 0
    endif
    gr.modify pagePtr[page+1], "bitmap", ledR      
    GOSUB ShowPage
    scx=1
  endif
  gr.modify scan, "x", stX+(scx-1)*dcell
  for note=1 to NbSdMax
    if notes[page*16+scx,note] then Soundpool.play nul,snd[note],0.99,0.99,1,0,1
  next
Timer.Resume

ONBACKKEY:
POPUP "Goodbye",0,0,0
PAUSE 1000
!OnError:
GR.CLOSE
END
