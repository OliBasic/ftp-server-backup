
Rem Calculate Miles per Gallon
Rem With RFO Basic!
Rem Using Nicolas's GW Library
Rem January 2015
Rem Version 1.03
Rem By Roy

n$=chr$(10) % new line

about$="M.P.G\n\nWith RFO Basic!\n\nUsing Nicolas's GW Library\n\nJanuary 2015\n\nVersion 1.03\n\nAuthor: Roy Shepherd"

help$="M.P.G:\n\n"

help$=help$+"To use this app first fill your vehicle up to the top with fuel and set the mile counter to\n"
help$=help$+"zero. When read to refuel, fill up to the top again and make a note of the miles traveled, pence\n"
help$=help$+"per litre and litres used. Apart from miles traveled, the other information will be on your receipt.\n\n"

help$=help$+"Put the three pieces of information into the app and press Calculate.\n\n"

help$=help$+"The app will then display the MPG and other information about the fuel costs of your vehicle\n\n"

help$=help$+"Most of the information and the date is saved to a history file, and is reloaded each time you\n"
help$=help$+"run the app.\n\n"

help$=help$+"As long as you always fill your vehicle up to the top and zero the miles counter and enter the\n"
help$=help$+"information into the app, you will have a record of your vehicle's fuel costs.\n\n"

help$=help$+"On low resolution devices the history box may not look right. If this is the case, try holding\n"
help$=help$+"the device horizontally.\n\n"

help$=help$+"To test try:\n\n"

help$=help$+"Miles Traveled  208.5\n"
help$=help$+"Pence Per Litre 119.7\n"
help$=help$+"Litres Used     15.6\n\n"

Rem Start of Functions

fn.def SaveHistory$(history$)
	historyPath$="../../MPG/data/"
	file.exists historyPresent,historyPath$+"history.txt"
	if historyPresent then
		text.open w,hs,historyPath$+"history.txt"
			text.writeln hs,history$
		text.close hs
	else
		file.mkdir historyPath$
		text.open w,hs,historyPath$+"history.txt"
			text.writeln hs,history$
		text.close hs
	endif
fn.end

fn.def LoadHistory$()
	historyPath$="../../MPG/data/"
	file.exists historyPresent,historyPath$+"history.txt"
	if historyPresent then
		text.open r,hs,historyPath$+"history.txt"
		do
			text.readln hs,bs$
			!if bs$<>"EOF" then history$+=bs$+chr$(10)
			if bs$<>"EOF" then history$+=bs$
		until bs$="EOF"
		!if right$(history$,1)=chr$(10) then history$=left$(history$,len(history$)-1)
		text.close hs
	else
		history$=""
	endif
	fn.rtn history$
fn.end

Rem End of Functionsadd

Rem Retrieve history data
MPG_history$ = LoadHistory$()

split.all tableHistor$[], MPG_history$ 
array.length  historySize,tableHistor$[]

list.create s, tableList
list.add tableList, ">Miles","M.P.G","C.P.L","Total","Date"

for col=2 TO historySize
 list.add tableList,tableHistor$[col]
next
  
list.toarray tableList, MPG_Table$[]

Rem Start of GW
include gw.bas
gw_force_mode(1)
!gw_load_theme("flat-ui")
!gw_load_theme ("classic")
!gw_load_theme ("ios")
gw_load_theme ("bootstrap")
!gw_load_theme("android-holo")
!gw_load_theme("square-ui")
!gw_load_theme("metro")

page = gw_new_page()

gw_use_theme_custo_once("color=b")
gw_add_titlebar(page,"M.P.G")
Rem Get required info from user
gw_inject_html(page,"<b><font color='green'>")
gw_add_text(page,"Required Information:")
gw_inject_html(page,"<font color='red'>")
milesTravaled=gw_add_inputnumber(page, "Enter Miles Traveled", "")
costPerLitre=gw_add_inputnumber(page,"Enter Pence per Litre","")
litresUsed=gw_add_inputnumber(page,"Enter Litres Used","")

gw_start_center(page)
gw_use_theme_custo_once("color=c inline icon=action")
gw_add_submit(page, "Calculate")
gw_stop_center(page)
gw_inject_html(page,"<b><font color='green'>")
gw_add_text(page,"Results:")

Rem Text boxes for the results
gallonsUsed=gw_add_textbox(page,"Gallons Used: ")
milesPerGallon=gw_add_textbox(page,"Miles Per Gallon: ")
milesPerLitre=gw_add_textbox(page,"Miles Per Litre: ")
costPerMile=gw_add_textbox(page,"Cost Per Mile: ")
totalCost=gw_add_textbox(page,"Total Cost: ")

Rem Text box for history

gw_add_text(page,"History:") 
gw_inject_html(page,"</b></font>")  
Rem Display History table
historyTable = GW_ADD_TABLE(page, 5, MPG_Table$[]) % cols = 5

gw_start_center(page)
Rem Add a row of button at the botton of the screen
Rem Exit MLG dialogbox and button
array.load MPGexit$[], "Yes>YES", "No>NO"
leaveMPG = gw_add_dialog_message(page, "Exit", "Do you want to leave MPG", MPGexit$[])
array.delete MPGexit$[]
gw_use_theme_custo_once("color=c inline icon=plus")
gw_add_button(page, "Exit MPG", gw_show_dialog_message$(leaveMPG))

Rem Clear History DialogBox and button
array.load MPGclear$[], "Yes>ClearHistory", "No>NO"
clearMPG = gw_add_dialog_message(page, "History", "Do you want to Clear MPG History", MPGclear$[])
array.delete MPGclear$[]
gw_use_theme_custo_once("color=c inline icon=plus")
gw_add_button(page, "Clear History", gw_show_dialog_message$(clearMPG))

Rem About DialogBox and button
array.load MPGabout$[], "OK>NO" % I've set r$=GW_WAIT_ACTION$() to ignore "NO"
aboutMPG = gw_add_dialog_message(page, "About", about$, MPGabout$[])
array.delete MPGabout$[]
GW_SET_TRANSITION(aboutMPG, "flip")
gw_use_theme_custo_once("color=e inline icon=info")
gw_add_button(page, "About", gw_show_dialog_message$(aboutMPG))

Rem Help DialogBox and button
array.load MPGhelp$[], "OK>NO"
helpMPG = gw_add_dialog_message(page, "Help", help$, MPGhelp$[])
array.delete MPGhelp$[]
GW_SET_TRANSITION(helpMPG, "flip")
gw_use_theme_custo_once("color=e inline icon=info")
gw_add_button(page, "Help", gw_show_dialog_message$(helpMPG))
gw_stop_center(page)

Rem Bad Input Box
array.load ban$[], "OK>NO"
badInput = gw_add_dialog_message(page, "Bad Input", "Please Re-Enter", ban$[])
array.delete ban$[]

gw_render(page)
gw_unload_theme()
do 
	r$=GW_WAIT_ACTION$()
	if r$="YES" then exit
	Rem if flag <>0  then some of the input boxes are zero or characters and the division by zero error would occur
	flag=0
	if gw_get_value(milesTravaled)<1 then flag =1
	if gw_get_value(costPerLitre)<1 then flag =1
	if gw_get_value(litresUsed)<1 then flag =1
	if flag=1 & r$<>"ClearHistory" & r$<>"NO" then gw_show_dialog_message(badInput)

	if r$<>"ClearHistory" & r$<>"NO" & flag = 0 then % if is needed to avoid division by zero error
		Rem work out costs and MPG
		gosub GallonsUsed
		
		gosub MPG
		
		gosub MilesPerLitre
		
		gosub CostPerMile
		
		gosub TotalCost
		
		gosub History
	endif
	if r$ = "ClearHistory" then gosub ClearHistory
	
until 0 % or back key is pressed

end

GallonsUsed:
	litres=gw_get_value(litresUsed)
	gallons=litres / 4.54609
	gallonsUsed$="Gallons Used: "+Format$("####%.#", gallons)
	gw_modify(gallonsUsed,"text",gallonsUsed$)
return

MPG:
	miles=gw_get_value(milesTravaled)
	mpg = miles / gallons
	mpg$="Miles Per Gallon: "+Format$("####%.#", mpg)
	gw_modify(milesPerGallon,"text",mpg$)
return

MilesPerLitre:
	miles_per_litre = miles / litres
	mpl$="Miles Per Litre: "+format$("###%.#",miles_per_litre)
	gw_modify(milesPerLitre,"text",mpl$)
return

CostPerMile:
	costPL=gw_get_value(costPerLitre)
	total_cost = costPL * litres
	cost_per_mile = total_cost / miles
	cpm$="Cost Per Mile: "+format$("###.##",cost_per_mile)+"p"
    gw_modify(costPerMile,"text",cpm$)       
return

TotalCost:
	total_cost = total_Cost/100
	total$="Total cost: "+format$("£###%.##",total_cost)
	gw_modify(totalCost,"text",total$)   
return

History:
	miles$=Format$("####%.#", miles)
	milesPG$=Format$("####%.#", mpg)
	cpl$=format$("###.#",costPL)
	fuelCost$=format$("£###%.##",total_cost)
	TIME Year$, Month$, Day$
	toDay$=Day$+"/"+Month$+"/"+right$(Year$,2)
	temp$=MPG_history$ % used temp so that new history is on top of list
	MPG_history$=" "+miles$+" "+milesPG$+" "+cpl$+"p"+" "+fuelCost$+" "+toDay$+temp$

	call SaveHistory$(MPG_history$)
	!MPG_history$ = LoadHistory$()
	gosub UpDateTableHistory
return

UpDateTableHistory:
	array.delete tableHistor$[]
	split.all tableHistor$[], MPG_history$ 

	array.length  historySize,tableHistor$[]
	list.clear tableList
	list.add tableList, ">Miles","M.P.G","C.P.L","Total","Date"
	for col=2 TO historySize
		list.add tableList,tableHistor$[col]
	next
	array.delete MPG_Table$[]
	list.toarray tableList, MPG_Table$[]
	
    gw_amodify(historyTable, "content", MPG_Table$[])
return

ClearHistory:
	MPG_history$=""
	call SaveHistory$(MPG_history$)
	gosub UpDateTableHistory
return
