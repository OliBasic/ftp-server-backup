REM Torch.bas
!torch with Flash & SOS mode
!portrait & landscape mode 
!Changelog:

!V1.20
!SOS & flash will not stop on device rotation, now stop only when display is touched.
!Buttons are showed on device rotation and display touch.
!Touched buttons are highlighted for touch confirmation (useful for tablets without vibration)

!V1.10 
!never posted

!V1.00
!initial release , SOS & flash stop on device rotation, button avaible only with device rotation

!setup variables...
FlashMode=0	%0= fixed light, 1=flash
SosMode=0	%0= fixed light, 1=SOS
SosUnit= 200	%time unit for morse code in msec (should be from 50 to 60msec)
FlashTmrOn= 1000	%msec
FlashTmrOff= 1000	%msec
FlashFlag=0
BkgFlag=0	%background mode flag, used for background popup message
XBtnSize=304	%button X size
YBtnSize=104	%button Y size

XBtnCenter=XBtnSize/2
YBtnCenter=YBtnSize/2

!load button data
dim Btn[3]	% num. of buttons for highlight when touched
array.load XBtnPosP [], 87,87,87
array.load YBtnPosP [], 400-YBtnSize-YBtnCenter-10,400-YBtncenter,400+YBtncenter+10
array.load XBtnPosL [], 400-XBtnCenter, 400-XBtnCenter, 400-XBtnCenter
array.load YBtnPosL [], 74,74+YBtnSize+10,74+YBtnSize+10+YBtnSize+10

!Vibrate when touch a key
array.load VbrPattern[], 1,100

!open a white graphic screen
gr.open 255, 255, 255, 255, 0
pause 1000	%need for sync
gr.orientation -1  %orientation from device
wakelock 4	%display always on
gr.brightness 1.00		%set as before automatically when exit

gr.bitmap.load BtnFlash,		"BtnFlash.png"
gr.bitmap.load BtnSos,			"BtnSos.png"
gr.bitmap.load BtnExit,			"BtnEsci.png"
gr.bitmap.load DspOffP,			"DspOffP.png"
gr.bitmap.load DspOffL,			"DspOffL.png"
gr.bitmap.load BtnTouched,		"BtnTouched.png"

reload_screen:
!jump here if orientation changed...
!read screen dimension and scale graphics
gr.screen actual_w, actual_h
!save actual screen orientation
old_w=actual_w		
old_h=actual_h

if actual_w< actual_h then
	di_height = 800 % y
	di_width = 480 % x
	ScreenMode=0
else
	di_height = 480 % y
	di_width = 800 % x
	ScreenMode=1
endif

!scale graphics (not the touch query result!!!)
scale_width = actual_w /di_width 
scale_height = actual_h /di_height 
gr.scale scale_width, scale_height

!clear screen, load button and splash screen...
gr.cls
timer.clear
timer.set 5000	%button visibility timeout
Btntimeout=0
!SosMode=0
!FlashMode=0

!Draw objects on screen

if actual_w < actual_h then
	!portrait
	gr.bitmap.draw ScreenOff, DspOffP,0,0
	gr.hide ScreenOff
	gr.bitmap.draw Btn1, BtnFlash, XBtnPosP[1], YBtnPosP[1]
	gr.bitmap.draw Btn2, BtnSos, XBtnPosP[2], YBtnPosP[2]
	gr.bitmap.draw Btn3, BtnExit, XBtnPosP[3], YBtnPosP[3]	
	gr.bitmap.draw Btn[1], BtnTouched, XBtnPosP[1], YBtnPosP[1]
	gr.bitmap.draw Btn[2], BtnTouched, XBtnPosP[2], YBtnPosP[2]
	gr.bitmap.draw Btn[3], Btntouched, XBtnPosP[3], YBtnPosP[3]	
	gr.hide Btn[1]
	gr.hide Btn[2]
	gr.hide Btn[3]
	
else
	!landscape
	gr.bitmap.draw ScreenOff, DspOffL,0,0
	gr.hide ScreenOff
	gr.bitmap.draw Btn1, BtnFlash, XBtnPosL[1], YBtnPosL[1]
	gr.bitmap.draw Btn2, BtnSos, XBtnPosL[2], YBtnPosL[2]
	gr.bitmap.draw Btn3, BtnExit, XBtnPosL[3], YBtnPosL[3]
	gr.bitmap.draw Btn[1], BtnTouched, XBtnPosL[1], YBtnPosL[1]
	gr.bitmap.draw Btn[2], BtnTouched, XBtnPosL[2], YBtnPosL[2]
	gr.bitmap.draw Btn[3], Btntouched, XBtnPosL[3], YBtnPosL[3]	
	gr.hide Btn[1]
	gr.hide Btn[2]
	gr.hide Btn[3]	
endif
gr.render

!Main loop of the app
DO 
	!check for orientation changed...
	gr.screen actual_w, actual_h
	if actual_w<>old_w then goto reload_screen
	
	!check if running in background, if so popup a message
	if background () then 
		!wakelock 5
		if BkgFlag=0 then
			BkgFlag=1
			!notify require too much cpu/battery in background, so use popup message 4 sec.
			popup "Torch is running in background" ,0,0,1
		endif
	else
		BkgFlag=0
		!wakelock 4
	endif
	
	!if running in background save battery drain doing nothing...
	!Thx to Mougino  :)
	while background ()	
		pause 1000
	repeat
	
	if FlashMode then
	!Flashmode
		pause FlashTmrOn
		gosub BtnQuery
		gr.show ScreenOff	%Black
		gr.render
		
		gr.screen actual_w, actual_h
		if actual_w<>old_w then goto reload_screen
		
		pause FlashTmrOff
		gosub BtnQuery
		gr.hide ScreenOff	%light
		gr.render
	endif
	
	if SosMode then
		gr.show ScreenOff	%Black
		gr.render
		for count=1 to 7	%pause from word to word
		pause SosUnit
		gosub BtnQuery
		gr.screen actual_w, actual_h
		if actual_w<>old_w then goto reload_screen
		next
		
		for count=1 to 3		
		gr.hide ScreenOff	%light dot
		gr.render
		pause SosUnit*1		%time for dot
		gosub BtnQuery
		gr.screen actual_w, actual_h
		if actual_w<>old_w then goto reload_screen
		gr.show ScreenOff	%black
		gr.render
		pause SosUnit*1		%time from dot/dash
		gosub BtnQuery
		gr.screen actual_w, actual_h
		if actual_w<>old_w then goto reload_screen
		next
		
		pause SosUnit*2		%time from chr to chr
		gosub BtnQuery
		gr.screen actual_w, actual_h
		if actual_w<>old_w then goto reload_screen
		
		for count=1 to 3
		gr.hide ScreenOff	%light dash
		gr.render
		pause SosUnit*3		%time for dash
		gosub BtnQuery
		gr.screen actual_w, actual_h
		if actual_w<>old_w then goto reload_screen
		gr.show ScreenOff	%black
		gr.render
		pause SosUnit*1		%time from dot/dash
		gosub BtnQuery
		gr.screen actual_w, actual_h
		if actual_w<>old_w then goto reload_screen
		next
		
		pause SosUnit*2		%time from chr to chr
		gosub BtnQuery
		gr.screen actual_w, actual_h
		if actual_w<>old_w then goto reload_screen
		
		for count=1 to 3		
		gr.hide ScreenOff	%light dot
		gr.render
		pause SosUnit*1		%time for dot
		gosub BtnQuery
		gr.screen actual_w, actual_h
		if actual_w<>old_w then goto reload_screen
		gr.show ScreenOff	%black 
		gr.render
		pause SosUnit*1		%time from dot/dash
		gosub BtnQuery
		gr.screen actual_w, actual_h
		if actual_w<>old_w then goto reload_screen
		next
	else
		gr.hide ScreenOff	%light
		gr.render	
	endif
	gosub BtnQuery
UNTIL 0

BtnQuery:	
	!exec app depending on orientation..
	if !ScreenMode then
		!Portrait: Scan for touched button...
		if BtnTimeout then return
	
		for btnnum =1 to 3
		!Check touch area
		gr.bounded.touch BtnScan,~
		XBtnPosP[btnnum]*scale_width ,~
		YBtnPosP[btnnum]*scale_height,~
		(XBtnPosP[btnnum]+XBtnSize)*scale_width,~	
		(YBtnPosP[btnnum]+YBtnSize)*scale_height
		
			if BtnScan =1  then
				gr.show Btn[btnnum]
				gr.render
				!change button image				
				!vibrate only one time...
				vibrate VbrPattern[],-1
				pause 150
				gr.hide Btn[btnnum]
				gr.render
				!check button...
				sw.begin btnnum
					sw.case 1
						!Flash
						FlashMode=1
						SosMode=0
						sw.break
					sw.case 2
						!SOS
						SosMode=1
						FlashMode=0
						sw.break
					sw.case 3
						!exit
						wakelock 5	%restore display timers settings
						pause 1000	%need for vibration
						exit
						sw.break
				sw.end
								
				!wait until untouched
				do
					gr.touch touched, x, y
				until !touched
				
			endif 
		next
		
	else
		!Landscape: Scan for touched button...
		if BtnTimeout then return
		
		for btnnum =1 to 3
		!Check touch area
		gr.bounded.touch BtnScan,~
		XBtnPosL[btnnum]*scale_width ,~
		YBtnPosL[btnnum]*scale_height,~
		(XBtnPosL[btnnum]+XBtnSize)*scale_width,~	
		(YBtnPosL[btnnum]+YBtnSize)*scale_height
		
			if BtnScan =1  then
				gr.show Btn[btnnum]
				gr.render
				!change button image				
				!vibrate only one time...
				vibrate VbrPattern[],-1
				pause 150
				gr.hide Btn[btnnum]
				gr.render
				sw.begin btnnum
					sw.case 1
						!Flash
						FlashMode=1
						SosMode=0
						sw.break
					sw.case 2
						!SOS
						SosMode=1
						FlashMode=0
						sw.break
					sw.case 3
						!exit
						wakelock 5	%restore display timers settings
						pause 1000	%need for vibration
						exit
						sw.break
				sw.end
				
				!wait until untouched
				do
					gr.touch touched, x, y
				until !touched
				
			endif	
		next
	endif
return	

ontimer:
BtnTimeout=1
timer.clear

gr.hide Btn1
gr.hide Btn2
gr.hide Btn3
gr.hide Btn[1]
gr.hide Btn[2]
gr.hide Btn[3]	
gr.render	
	
timer.resume



onBackKey:
wakelock 5	%restore display timers settings
!perform the exit action when press back key
exit

ongrtouch:
if BtnTimeout=0 then gr.ongrtouch.resume	%do nothing

!show buttons, stop SOS & Flash, restart timer to hide buttons
gr.show Btn1
gr.show Btn2
gr.show Btn3
gr.render
BtnTimeout=0
SosMode=0
FlashMode=0

!wait for untouch...
do
gr.touch touched, x, y
until !touched

timer.clear
timer.set 5000	%button visibility timeout

gr.ongrtouch.resume
