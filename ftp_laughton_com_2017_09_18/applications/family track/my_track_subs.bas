goto my_track_subs_end



gt_this_member:
GOSUB opens_ftp

ftp.CD mydirftp$+ familyname$+"/devices/"+member$
ftp.get "lkp",appdir$+"temp_loc"
GOSUB clos_ftp
grabfile data$, appdir$+"temp_loc"
file.delete del, appdir$+"temp_loc"
pos$=replace$(data$,":",",")
url$= surl$+"&zoom=17&size="+format$("####",mw)+"x"+format$("####",mw)
url$=url$+ "&markers=color:green%7Clabel:"+format$("###",ds) +"%7C"+ pos$



url$=replace$(url$," ","")
fname$=member$+".png"
GOSUB gt_ggl_map
gosub sh_map1

return

ontimer:
GOSUB refresh
timer.resume

clos_ftp:
ftp.close
mode_ftp=0
return

opens_ftp:
mode_ftp=1
FTP.OPEN url_ftp$,port,user$,pswrd$
FTP.CD mydirftp$
return

sh_map:
gr.bitmap.load lme_bmp,appdir$+"me.png"
gr.bitmap.draw me_bmp,lme_bmp,0,ts
gr.color 255,100,100,25,1
gr.text.size ts
gr.text.draw ttext,ts*3,ts*3, "Please Wait"
gr.bitmap.load fam_b,appdir$+"family.png"
gr.bitmap.scale fam_s,fam_b,ts*1.5,ts*1.5
gr.bitmap.draw fam,fam_s,ts,ts*2
gr.render

return

sh_map1:
gr.bitmap.load lme_bmp,appdir$+member$+".png"
gr.bitmap.draw mm_bmp,lme_bmp,0,ts

gr.render
pause 5000
gr.hide mm_bmp
gr.render
return

refresh:
gr.show ttext
gr.render
GOSUB upload_lkp
gosub gt_family
GOSUB gt_fam_loc
GOSUB clos_ftp
GOSUB mk_url
GOSUB gt_ggl_map
gr.bitmap.load lnme_bmp,appdir$+"me.png"
gr.modify me_bmp,"bitmap",lnme_bmp
gr.render

gr.hide ttext
gr.render
return



gt_fam_loc:
list.remove family,1
furl$=""
ftp.CD mydirftp$+familyname$
!dialog.select ds,family,"FAM"
list.size family,members
for t=1 to members
list.get family,t,member$
if member$<>username$
ftp.CD mydirftp$+familyname$+"/devices/"+member$
ftp.get "lkp",appdir$+"lkpt"
grabfile data$,appdir$+"lkpt"
data$=replace$(data$,chr$(10),"")
undim d$[]
split d$[],data$,":"
mlat=val(d$[1])
mlon=val(d$[2])
furl$=furl$+"&markers=color:green%7Clabel:2%7C"+format$("##%.########",mlat)+","+ format$("##%.########",mlon)
endif
next t
return

mk_url:
s1url$=surl$+"&size="+format$("####",mw)+"x"+format$("####",mw)
url$=s1url$+ "&markers=color:blue%7Clabel:1%7C"+format$("##%.########",my_lat)+","+ format$("##%.########",my_lon)

url$=url$+furl$

url$=replace$(url$," ","")
return

gt_ggl_map:

FILE.EXISTS iif, appdir$+fname$
if iif=1
FILE.delete del, appdir$+fname$
endif

BYTE.OPEN r,xx,url$
PAUSE 1000
BYTE.COPY xx, appdir$+fname$ 
DO
 FILE.EXISTS iif, appdir$+fname$
UNTIL iif>0
BYTE.CLOSE xx
return

start_app_defaults:
appdir$="../../mytrack/data/"
file.mkdir appdir$
url_ftp$="ftp.enteryourown.com"
port=21
user$="enter your own"
pswrd$="enter your own"
mydirftp$= "htdocs/cf/users/"
return

start_gr:
gr.open 255,125,125,125,1,1
gr.screen mw,mh
gr.statusbar ts
return

upload_lkp:
GOSUB locate_me
cls
print replace$(format$("##%.########",my_lat)+":"+ format$("##%.########",my_lon)," ","")
console.save appdir$+"mp"
cls
GOSUB opens_ftp
ftp.CD mydirftp$+familyname$+"/devices/"+username$
ftp.put "mp","lkp"
return

ld_user_details:
grabfile data$,appdir$+"user.txt"
data$=replace$(data$,chr$(10),"")
split userdata$[],data$,":"
familyname$= userdata$[1]
username$= userdata$[2]
print 
return

locate_me:
GPS.open
pause 1500
do
GPS.LOCATION ti_gps,~
 prov$,~
 cnt,~
 acc,~
 my_lat,~
 my_lon,~
 alt,~
 bear,~
 speed
nw=time()
go= (nw-ti_gps)
UNTIL (go)<=100& acc<=minacc&acc<>0
go=0
GPS.close

return

my_track_subs_end:
