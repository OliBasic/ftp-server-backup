print version$()
!open graphics -----------------------
flagOri                = -1
refW                   = 790
refH                   = 1255
IF !flagOri              THEN SWAP refW, refH
GR.OPEN                  255, 0 , 0 , 40 ,1,flagOri
GR.SCREEN                curW, curH
scaW                   = curW / refW
scaH                   = curH / refH
IF                 ABS(1-scaw)> ABS(1-scah) THEN sca=scaw ELSE sca=scah
GR.SCALE                 sca , sca
cx                     = refw/2
cy                     = refh*0.73

GOSUB userfun
!gr.front 0

LIST.CREATE              n, speedBrut
LIST.CREATE              n, speedNet
LIST.CREATE              n, dist
speedStop              = 8

!Brandenburger Tor, 13.37726,52.51625
ARRAY.LOAD               waypts$ [], "Brandenburger Tor" , "Pampa 1"
ARRAY.LOAD               waypts  [] ~
13.37726 , 52.51625  ~
13.30000 , 52.51000

ARRAY.LENGTH             couwp, waypts$[]
wptr                   = 1

GPS.OPEN
PAUSE                    200

dtdes                  = 500
tic                    = CLOCK()
ticStart               = tic

DO

 IF                      infix>1&!flag1stFix THEN flag1stfix=1
 latO=lat : lonO=lon : infixO = infix
 GPS.STATUS               stat, infix, inview
 GPS.LOCATION            _time,prov$,nsat,accu,lat,lon,alt,bear,speed
 speed                 *= 3.6
 IF speed               > speedStop THEN LIST.add speedBrut, speed
 LIST.ADD                 speedNet, speed
 IF flag1stfix THEN LIST.ADD  dist, latLonDist(lat,lon,latO,lonO)

 speedAvg1              = listMean( speedBrut )
 speedAvg2              = listMean( speedNet )
 distTrip               = listSum( dist )
 lonWp                  = waypts[wptr*2-1]
 latWp                  = waypts[wptr*2]


 bearWp                 = phi2Bear(latLonPhi(latWp, lonWp, lat,lon))
 distWp                 = latLonDist (lat, lon, latwp, lonwp)

 GOSUB                    modify

 !PRINT                    INT$(++ctrMain), INT$(toc), flag1stfix
 IF                       !BACKGROUND() THEN GR.RENDER
 toc                    = CLOCK()-tic
 PAUSE                    MAX(dtdes-toc , 1)
 tic                    = CLOCK()

UNTIL_END

END



!--------------------------
userfun:

FN.DEF                   listMean(list)
 LIST.SIZE               list, sz
 IF !sz                  THEN FN.RTN  -9999
 LIST.TOARRAY            list ,  tmp[]
 ARRAY.AVERAGE           mean , tmp[]
 FN.RTN                  mean
FN.END

FN.DEF                   listSum(list)
 LIST.SIZE               list, sz
 IF !sz                  THEN FN.RTN  -9999
 LIST.TOARRAY            list ,  tmp[]
 ARRAY.SUM               sum , tmp[]
 FN.RTN                  sum
FN.END


FN.DEF                   arrow ( arrLen , arrWid, headRelLen, headRelWid,facCorner)
 IF                     arrHeadRelWid< arrRelWidth THEN  arrHeadRelWid = arrRelWidth
 p0y	                 =	 arrLen * 0.5
 p0x	                 =	 arrWid * 0.5
 p1                   = -p0y    * (1-headRelLen*2)
 p2                   =  p0x    +  arrWid* headRelWid
 p3                   =  p1     + (1-facCorner) * headRelLen *  arrLen
 ARRAY.LOAD              a[], p0x,p0y,  p0x,p1 ,   p2,p3   , 0,-p0y ~
 -p2,p3  , -p0x,p1 , -p0x,p0y
 LIST.CREATE             n, poly
 LIST.ADD.ARRAY          poly, a[]
 FN.RTN                  poly
FN.END

FN.DEF                roundCornerRect (b, h, r)
 IF                   b <2*r THEN r=b/2
 IF                   h <2*r THEN r=h/2
 half_pi            = PI() / 2
 dphi               = half_pi / 8
 LIST.CREATE          N, S1
 mx                 = -b/2+r
 my                 = -h/2+r
 FOR phi            = 0 TO half_pi STEP dphi
  LIST.ADD             s1, mx-COS(phi)*r, my-sin (phi)*r
 NEXT
 mx                 = b/2-r
 my                 = -h/2+r
 FOR phi            = 0 TO half_pi STEP dphi
  LIST.ADD            s1, mx+SIN(phi)*r, my- cos (phi)*r
 NEXT
 mx                 = b/2-r
 my                 = h/2-r
 FOR phi            = 0 TO half_pi STEP dphi
  LIST.ADD            s1, mx+COS(phi)*r, my+sin (phi)*r
 NEXT
 mx                 = -b/2+r
 my                 =  h/2-r
 FOR phi            = 0 TO half_pi STEP dphi
  LIST.ADD            s1, mx-SIN(phi)*r, my+cos (phi)*r
 NEXT
 FN.RTN s1
FN.END

FN.DEF             gear(r1,r2,r3,ntooth)
 LIST.CREATE        n, li
 r=r1
 FOR i=0 TO 360 STEP 360/(ntooth*4)
  ctr+= 1
  IF ctr = 3 THEN
   ctr=1
   IF r=r1 THEN r=r2 ELSE r= r1
  END IF
  LIST.ADD li,  r*COS(TORADIANS(i)), r*SIN(TORADIANS(i))
 NEXT
 FOR i=360 TO 0 STEP -12
  LIST.ADD li,  r3*COS(TORADIANS(i)), r3*SIN(TORADIANS(i))
 NEXT
 LIST.ADD li,  r*COS(TORADIANS(0)), r*SIN(TORADIANS(0))
 FN.RTN             li
FN.END

FN.DEF             latLonDist (lat, lon, latOld, lonOld)
 rEarth          = 6371
 fac             = COS(TORADIANS((lat+latold)/2))
 dx              = rEarth *TORADIANS(lon-lonOld) * fac
 dy              = rEarth *TORADIANS(lat-latOld)
 FN.RTN            ROUND(SQR(dx^2+dy^2), 4)
FN.END


FN.DEF            latLonPhi(lat, lon, latOld, lonOld)
 fac             = COS(TORADIANS((lat+latold)/2))
 FN.RTN            TODEGREES( ATAN2(lat-latOld, (lon-lonOld)*fac ))
FN.END

FN.DEF             phi2Bear ( phi )
 offs            = 90
 IF                phi >  90  THEN offs = 450
 FN.RTN            ROUND(offs-phi , 2)
FN.END




rad                    = cx*0.85

GR.SET.STROKE            5
GR.COLOR                 255 , 35 , 35 , 35 , 1
GR.CIRCLE                nn, 0, 0, rad
GR.COLOR                 255 , 255 , 255 , 255 , 0
GR.CIRCLE                nn, 0 , 0, rad
GR.COLOR                 255 , 100 , 100 , 100 , 0
GR.CIRCLE                nn,  1, 1, rad
rt= rad *0.93
GR.COLOR                 255 , 70 , 255 , 90 , 1
GR.TEXT.ALIGN            2
GR.TEXT.SIZE             50
GR.TEXT.BOLD             1
ARRAY.LOAD               letters $[], "E", "S", "W", "N"  ,""
GR.ROTATE.START          0, 0, 0, compRot
FOR i= 0 TO 360  STEP 11.25
 x= COS(TORADIANS(i)) *rt : y= SIN(TORADIANS(i)) *rt
 rr= 4
 IF !mod (i, 22.5) THEN rr= 6
 IF !mod (i, 45) THEN rr= 9
 IF !mod (i, 90) THEN rr= 12
 GR.CIRCLE             nn, x, y, rr 
 IF !mod (i, 90) THEN 
  GR.TEXT.DRAW    nn, x*0.8, y*0.8+20, letters$[++ctrLet]
 ENDIF
NEXT

GR.SET.STROKE            2
GR.LINE                   nn, -rt*0.65, 0, rt*0.65, 0
GR.LINE                   nn, 0,-rt*0.65, 0,  rt*0.65
GR.COLOR                 255 , 55 , 55 , 55 , 1
GR.CIRCLE                nn, 0, 0, rad*0.15
GR. rotate.end 

GR.SET.STROKE            4
arrow1             =       arrow (rad*1.7, rad*0.03, 0.2, 1.3, 1.8)
GR.COLOR                 65 , 255 , 5 , 5 , 1
GR.POLY                   nn, arrow1
GR.COLOR                 255 , 255 , 5 , 5 , 0
GR.POLY                   nn, arrow1
GR.COLOR                 255 , 85 , 85 , 85 , 1
GR.CIRCLE                nn, 0, 0, rad*0.1

GR.GROUP.GETDL          compass
GR.MOVE                 compass,  cx, cy

GR.SET.STROKE            4
GR.ROTATE.START          0, 0, 0, dirRot
_diretion         =      arrow (rad*1.7, rad*0.1, 0.2, 1.3, 1)
GR.COLOR                 65 , 255 , 255 , 5 , 1
GR.POLY                   d1, _diretion
GR.COLOR                 255 , 255 , 255 , 5 , 0
GR.POLY                   d2, _diretion
GR. rotate.end
GR.COLOR                 255 , 85 , 85 , 85 , 1
GR.CIRCLE                d3, 0, 0, rad*0.08
GR.GROUP                 dirGroup, d1, d2, d3
GR.MOVE                 dirGroup ,  cx, cy
GR.MODIFY               dirRot,"x", cx,"y", cy,"angle", 0



ARRAY.LOAD              txtHead $[],~
"gps Fix" , "sat Inview" , "sat Infix" , "accuracy" ,~
"speed"     , "speed Avg" , "spd Avg(total)" , "dist Trip" ,~
"dist To WP"    , "bear To WP"   , "curr. WP" , "WP count" ,~
"time Trip"    , ""   , "" , "" 
txtsz                 = 26

GR.TEXT.SIZE            txtsz
GR.TEXT.ALIGN           2
GR.TEXT.BOLD            1

toffx = 5 : toffy = 50 : tdx = 195 : tdy = 100 : ctr=0
rcr                   = roundCornerRect (tdx-6, tdy-6, 15)
GR. circle              txtPtr, 0, 0, 0
FOR j= 1 TO 4
 FOR i= 1 TO 4
  x = i*tdx-tdx/2+toffx : y = j*tdy-tdy/2+ toffy
  GR.COLOR           255 , 35 , 35 , 35 , 1
  GR.POLY            nn, rcr, x, y
  GR.COLOR           255 , 255 , 255 , 255 , 0
  GR.POLY            nn, rcr, x, y
  GR.COLOR           255 , 100 , 100 , 100 , 0
  GR.POLY            nn, rcr, x+1, y+1

  GR.TEXT.SIZE       txtsz
  GR.COLOR           255, 5, 255, 5, 1
  GR.TEXT.DRAW       nn, x, y-txtsz*0.65 , txtHead $[++ctr]
  GR.TEXT.SIZE       txtsz*1.7
  GR.TEXT.DRAW       nn, x, y+txtsz*1.2  ,"---"
 NEXT
NEXT

x = cx : y = j*tdy-tdy/2+ toffy
rcr                   = roundCornerRect (tdx*4, tdy-6, 15)
GR.COLOR           255 , 35 , 35 , 35 , 1
GR.POLY            nn, rcr, x, y
GR.COLOR           255 , 255 , 255 , 255 , 0
GR.POLY            nn, rcr, x, y
GR.COLOR           255 , 100 , 100 , 100 , 0
GR.POLY            nn, rcr, x+1, y+1

GR.TEXT.SIZE       txtsz
GR.COLOR           255, 5, 255, 5, 1
GR.TEXT.DRAW       nn, x, y-txtsz*0.65 , "WP NAME
GR.TEXT.SIZE       txtsz*1.7
GR.TEXT.DRAW       nn, x, y+txtsz*1.2  ,"---"



rcr              = roundCornerRect ( 100, 100 ,15)
GR.COLOR           255 , 35 , 35 , 35 , 1
GR.POLY            nn, rcr, refw*0.1, refh/2
GR.COLOR           255 , 255 , 255 , 255 , 0
GR.POLY            nn, rcr, refw*0.1, refh/2
GR.COLOR           255 , 100 , 100 , 100 , 0
GR.POLY            nn, rcr, refw*0.1+1, refh/2+1

gear1           = gear(35,28,18,8)
GR.COLOR          255 , 70 , 255 , 90 , 1
GR.POLY           nn, gear1, refw*0.1, refh/2



ARRAY.LOAD        menue$[],"reset","END"

GR.RENDER

RETURN


modify:

tTime                  = (clock ()- ticStart)/1000
tTime$                 = ~
replace $(FORMAT$("%%",tTime/3600) +":"+ ~
FORMAT$("%%",tTime/60)+":"+FORMAT$("%%",MOD(ttime, 60))," ","")

 GR.MODIFY                comprot , "angle", -bear
 GR.MODIFY                dirRot  , "angle", bearWp-bear


 GR.MODIFY             txtptr+1*5,"text", INT$( infix> 0 )
 GR.MODIFY             txtptr+2*5,"text", INT$( inview )
 GR.MODIFY             txtptr+3*5,"text", INT$(infix)
 GR.MODIFY             txtptr+4*5,"text", INT$(accu)

 GR.MODIFY             txtptr+5*5,"text", STR$(ROUND( speed , 1))
 GR.MODIFY             txtptr+6*5,"text", STR$(ROUND( speedAvg1 ,1))
 GR.MODIFY             txtptr+7*5,"text", STR$(ROUND( speedAvg2 ,1))
 GR.MODIFY             txtptr+8*5,"text", STR$(ROUND( distTrip ,1))

 GR.MODIFY             txtptr+9*5,"text", STR$(ROUND( distWp , 2))
 GR.MODIFY             txtptr+10*5,"text", STR$(ROUND( bearWp ,1))
 GR.MODIFY             txtptr+11*5,"text", INT$(ROUND( wptr ))
 GR.MODIFY             txtptr+12*5,"text", INT$(ROUND( couwp ))

 GR.MODIFY             txtptr+13*5,"text", ttime $

 GR.MODIFY             txtptr+17*5,"text", waypts$[wptr]

 btx = refw*0.1: bty =refh/2
 GR.BOUNDED.TOUCH       bt, (btx-50)*sca, (bty-50)*sca, (btx+50)*sca, (bty+50)*sca 
 IF bt
  DIALOG.SELECT       sel, menue$[]
  if sel = 2 then _END = 1
  if sel = 1 then 
    list.clear speedBrut
    list.clear speedNet
    list.clear dist
    ticStart = clock()
  endif
 ENDIF

 RETURN




 !--------------------------
