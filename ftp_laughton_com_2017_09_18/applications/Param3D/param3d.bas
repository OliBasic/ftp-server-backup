REM Begin MergeSort.bas

FN.DEF PartialShuffle(lstPtr,key$,i1,i2,j2)
 j1 = i2+1
 i=i1
 LIST.GET lstPtr,i,b1
 BUNDLE.GET b1,key$,val1
 FOR j=j1 TO j2
  LIST.GET lstPtr,j,b2
  BUNDLE.GET b2,key$,val2
  WHILE !(val2<val1) & i<i2
   i=i+1
   LIST.GET lstPtr,i,b1
   BUNDLE.GET b1,key$,val1
  REPEAT
  IF val2<val1
   LIST.REMOVE lstPtr,j
   LIST.INSERT lstPtr,i,b2
   i=i+1
   i2=i2+1
  ENDIF
 NEXT j
FN.END

FN.DEF PartialMergeSort(lstPtr,key$,i1,j2)
 IF i1=j2
  FN.RTN 0
 ELSE if i1=(j2-1)
  LIST.GET lstPtr,i1,b1
  BUNDLE.GET b1,key$,val1
  LIST.GET lstPtr,j2,b2
  BUNDLE.GET b2,key$,val2
  IF val1>val2
   LIST.REMOVE lstPtr,j2
   LIST.INSERT lstPtr,i1,b2
   FN.RTN 0
  ENDIF
 ELSE
  i2 = FLOOR((i1+j2)/2)
  j1 = i2 + 1
  CALL PartialMergeSort(lstPtr,key$,i1,i2)
  CALL PartialMergeSort(lstPtr,key$,j1,j2)
  CALL PartialShuffle(lstPtr,key$,i1,i2,j2)
  FN.RTN 0
 ENDIF
FN.END

FN.DEF MergeSort(lstPtr,key$)
 LIST.SIZE lstPtr, n
 CALL PartialMergeSort(lstPtr,key$,1,n)
FN.END

REM Begin Param3D.bas

GR.OPEN 255,0,0,0,0,1
PAUSE 1000

quit=0
newExpr=0
shapeExists=0
menuLoop=1
GR.SCREEN scrW, scrH

ARRAY.LOAD menu$[], ~
"Enter x,y,z expressions", ~
"Enter limits on s and t", ~
"Enter number of facets", ~
"Enter rotation angles", ~
"View all current settings", ~
"Draw shape", ~
"Capture Image", ~
"Return to Graphic", ~
"Quit"

mrg = 25
xc = scrW/2
yc = scrW/2
xSmax = xc - mrg

GOSUB ReadDatFile

INCLUDE ../data/param3d.bas.txt

DIM XYZPr[10000,3]
DIM XYZ[10000,3]
LIST.CREATE n,facets
LIST.CREATE n,lstPoly
LIST.CREATE s,lstSettings

GOSUB DisplayMenu

GR.RENDER

DO
 IF menuLoop & !newExpr THEN gosub DisplayMenu
UNTIL quit|newExpr
GR.CLOSE
IF newExpr
 PAUSE 500
 RUN "param3d.bas"
ENDIF
END

ONGRTOUCH:
GOSUB DisplayMenu
GR.ONGRTOUCH.RESUME

DisplayMenu:
SELECT nsel,menu$[],"Main Menu"

SW.BEGIN nsel

 SW.CASE 1
  ! TEXT.INPUT xexp$,xexp$,"x(s,t) = "
  ! TEXT.INPUT yexp$,yexp$,"y(s,t) = "
  ! TEXT.INPUT zexp$,zexp$,"z(s,t) = "
  INPUT "x(s,t) = ",xexp$,xexp$
  INPUT "y(s,t) = ",yexp$,yexp$
  INPUT "z(s,t) = ",zexp$,zexp$
  GOSUB SetFunctions
  GOSUB WriteDatFile
  newExpr=1
  menuLoop=1
  SW.BREAK

 SW.CASE 2
  GOSUB GetSTLimits
  GOSUB WriteDatFile
  menuLoop=1
  SW.BREAK

 SW.CASE 3
  GOSUB GetNSNT
  GOSUB WriteDatFile
  menuLoop=1
  SW.BREAK

 SW.CASE 4
  dflt$=STR$(th)+" "+STR$(ph)
  !  TEXT.INPUT s$,dflt$,"Enter theta phi"
  INPUT "Enter theta phi",s$,dflt$
  w$=WORD$(s$,1)
  th=VAL(w$)
  w$=WORD$(s$,2)
  ph=VAL(w$)
  menuLoop=1
  GOSUB WriteDatFile
  SW.BREAK

 SW.CASE 5
  LIST.CLEAR lstSettings
  LIST.ADD lstSettings,"x(s,t)= "+xexp$
  LIST.ADD lstSettings,"y(s,t)= "+yexp$
  LIST.ADD lstSettings,"z(s,t)= "+zexp$
  s$="Smin= "+STR$(smin)+"  Smax= "+STR$(smax)
  LIST.ADD lstSettings,s$
  s$="Tmin= "+STR$(tmin)+"  Tmax= "+STR$(tmax)
  LIST.ADD lstSettings,s$
  s$="NS= "+STR$(ns)+"  NT= "+STR$(nt)
  LIST.ADD lstSettings,s$
  s$="Theta= "+STR$(th)+"  Phi= "+STR$(ph)
  LIST.ADD lstSettings,s$
  SELECT dum,lstSettings,"Current Settings"
  SW.BREAK

 SW.CASE 6
  IF !shapeExists THEN GOSUB GenerateShape
  GOSUB GenerateProjections
  GOSUB SortFacets
  GOSUB DrawShape
  GOSUB WriteDatFile
  menuLoop=0
  SW.BREAK

 SW.CASE 7
  INPUT "jpg file name:",jpgName$
  jpgName$ = jpgName$+".jpg"
  GR.SCREEN.TO_BITMAP bmFull
  GR.BITMAP.CROP bmCrop,bmFull,0,0,scrW,scrW
  GR.BITMAP.SAVE bmCrop,jpgName$
  GR.BITMAP.DELETE bmFull
  GR.BITMAP.DELETE bmCrop
  SW.BREAK

 SW.CASE 8
  menuLoop=0
  SW.BREAK

 SW.CASE 9
  quit = 1
  menuLoop=0
  SW.BREAK

SW.END

RETURN

SetFunctions:
TEXT.OPEN w,tfile,"param3d.bas.txt"
TEXT.WRITELN tfile, "fn.def fx(s,t)"
TEXT.WRITELN tfile, "fn.rtn ";xexp$
TEXT.WRITELN tfile, "fn.end"
TEXT.WRITELN tfile, "fn.def fy(s,t)"
TEXT.WRITELN tfile, "fn.rtn ";yexp$
TEXT.WRITELN tfile, "fn.end"
TEXT.WRITELN tfile, "fn.def fz(s,t)"
TEXT.WRITELN tfile, "fn.rtn ";zexp$
TEXT.WRITELN tfile, "fn.end"
TEXT.CLOSE tfile
RETURN

GetSTLimits:
dflt$=STR$(smin)+" "+STR$(smax)+" "+ ~
STR$(tmin)+" "+STR$(tmax)
! TEXT.INPUT s$,dflt$,"Enter Smin Smax Tmin Tmax"
INPUT "Enter Smin Smax Tmin Tmax",s$,dflt$
w$=WORD$(s$,1)
smin=VAL(w$)
w$=WORD$(s$,2)
smax=VAL(w$)
w$=WORD$(s$,3)
tmin=VAL(w$)
w$=WORD$(s$,4)
tmax=VAL(w$)
shapeExists=0
PRINT smin,smax,tmin,tmax
RETURN

GetNSNT:
dflt$=STR$(ns)+" "+STR$(nt)
! TEXT.INPUT s$,dflt$,"Enter NS NT"
INPUT "Enter NS NT",s$,dflt$
w$=WORD$(s$,1)
ns=VAL(w$)
w$=WORD$(s$,2)
nt=VAL(w$)
shapeExists=0
RETURN


GenerateShape:
POPUP "Generating Shape",0,0,0
ds = (smax-smin)/ns
dt = (tmax-tmin)/nt
r2max=0
FOR i=1 TO ns+1
 s=smin+(i-1)*ds
 FOR j=1 TO nt+1
  t=tmin+(j-1)*dt
  ind=(i-1)*(nt+1) + j
  xpr=fx(s,t)
  ypr=fy(s,t)
  zpr=fz(s,t)
  XYZPr[ind,1]=xpr
  XYZPr[ind,2]=ypr
  XYZPr[ind,3]=zpr
  r2=xpr*xpr+ypr*ypr+zpr*zpr
  IF r2>r2max THEN r2max=r2
 NEXT j
NEXT i
IF r2max>0.0001
 rmax=SQR(r2max)
ELSE
 rmax=0.01
ENDIF
GOSUB ClearFacets
LIST.CREATE n,facets
FOR i=1 TO ns
 FOR j=1 TO nt
  BUNDLE.CREATE b
  p = (i-1)*(nt+1)+j
  BUNDLE.PUT b,"p1",p
  p= p+1
  BUNDLE.PUT b,"p2",p
  p= p+nt+1
  BUNDLE.PUT b,"p3",p
  p=p-1
  BUNDLE.PUT b,"p4",p
  LIST.ADD facets,b
 NEXT j
NEXT i
shapeExists=1
RETURN

ClearFacets:
LIST.SIZE facets,nf
FOR i=1 TO nf
 LIST.GET facets,i,b
 BUNDLE.CLEAR b
NEXT i
LIST.CLEAR facets
RETURN

GenerateProjections:
POPUP "Generating Projections",0,0,0

costh=COS(th*PI()/180)
sinth=SIN(th*PI()/180)
cosph=COS(ph*PI()/180)
sinph=SIN(ph*PI()/180)
cthcph=costh*cosph
cthsph=costh*sinph
sthcph=sinth*cosph
sthsph=sinth*sinph

m11=costh*costh+sinth*sinth*cosph
m12=costh*sinth*(1-cosph)
m13=-sinth*sinph
m21=m12
m22=sinth*sinth+costh*costh*cosph
m23=costh*sinph
m31=-m13
m32=-m23
m33=cosph

FOR i=1 TO (ns+1)*(nt+1)
 xpr=XYZPr[i,1]
 ypr=XYZPr[i,2]
 zpr=XYZPr[i,3]
 !XYZ[i,1]=xpr*costh-ypr*sthcph-zpr*sthsph
 !XYZ[i,2]=xpr*sinth+ypr*cthcph+zpr*cthsph
 !XYZ[i,3]=-ypr*sinph+zpr*cosph
 XYZ[i,1]=m11*xpr+m12*ypr+m13*zpr
 XYZ[i,2]=m21*xpr+m22*ypr+m23*zpr
 XYZ[i,3]=m31*xpr+m32*ypr+m33*zpr
NEXT i
RETURN

SortFacets:
POPUP "Sorting Facets",0,0,0
FOR i=1 TO ns*nt
 LIST.GET facets,i,b
 BUNDLE.GET b,"p1",p1
 BUNDLE.GET b,"p2",p2
 BUNDLE.GET b,"p3",p3
 BUNDLE.GET b,"p4",p4
 ztot=XYZ[p1,3]+XYZ[p2,3]+XYZ[p3,3]+XYZ[p4,3]
 BUNDLE.PUT b,"ztot",ztot
NEXT i
CALL MergeSort(facets,"ztot")
RETURN


DrawShape:
POPUP "Drawing Shape",0,0,0
GOSUB ClearLstPoly
GR.CLS
GR.SET.STROKE 4
FOR i=1 TO ns*nt
 LIST.CREATE n,poly
 LIST.GET facets,i,b
 BUNDLE.GET b,"p1",p1
 x=XYZ[p1,1]
 y=XYZ[p1,2]
 xs=xc+x*xSmax/rmax
 ys=yc-y*xSmax/rmax
 LIST.ADD poly,xs,ys
 BUNDLE.GET b,"p2",p2
 x=XYZ[p2,1]
 y=XYZ[p2,2]
 xs=xc+x*xSmax/rmax
 ys=yc-y*xSmax/rmax
 LIST.ADD poly,xs,ys
 BUNDLE.GET b,"p3",p3
 x=XYZ[p3,1]
 y=XYZ[p3,2]
 xs=xc+x*xSmax/rmax
 ys=yc-y*xSmax/rmax
 LIST.ADD poly,xs,ys
 BUNDLE.GET b,"p4",p4
 x=XYZ[p4,1]
 y=XYZ[p4,2]
 xs=xc+x*xSmax/rmax
 ys=yc-y*xSmax/rmax
 LIST.ADD poly,xs,ys
 LIST.ADD lstPoly,poly
 GR.COLOR 255,0,0,0,1
 GR.POLY dum,poly
 GR.COLOR 255,255,255,255,0
 GR.POLY dum,poly
NEXT i
GR.RENDER
RETURN

ClearLstPoly:
LIST.SIZE lstPoly,lstPolySz
FOR i=1 TO lstPolySz
 LIST.GET lstPoly,i,poly
 LIST.CLEAR poly
NEXT i
LIST.CLEAR lstPoly
RETURN

ReadDatFile:
TEXT.OPEN r,fdat,"param3d.dat.txt"
TEXT.READLN fdat,xexp$
TEXT.READLN fdat,yexp$
TEXT.READLN fdat,zexp$
TEXT.READLN fdat,s$
smin=VAL(s$)
TEXT.READLN fdat,s$
smax=VAL(s$)
TEXT.READLN fdat,s$
tmin=VAL(s$)
TEXT.READLN fdat,s$
tmax=VAL(s$)
TEXT.READLN fdat,s$
ns=VAL(s$)
TEXT.READLN fdat,s$
nt=VAL(s$)
TEXT.READLN fdat,s$
th=VAL(s$)
TEXT.READLN fdat,s$
ph=VAL(s$)
TEXT.CLOSE fdat
RETURN

WriteDatFile:
TEXT.OPEN w,fdat,"param3d.dat.txt"
TEXT.WRITELN fdat,xexp$
TEXT.WRITELN fdat,yexp$
TEXT.WRITELN fdat,zexp$
TEXT.WRITELN fdat,smin
TEXT.WRITELN fdat,smax
TEXT.WRITELN fdat,tmin
TEXT.WRITELN fdat,tmax
TEXT.WRITELN fdat,ns
TEXT.WRITELN fdat,nt
TEXT.WRITELN fdat,th
TEXT.WRITELN fdat,ph
TEXT.CLOSE fdat
RETURN
