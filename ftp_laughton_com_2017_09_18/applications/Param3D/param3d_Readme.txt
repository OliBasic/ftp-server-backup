Param3d Instructions

Install param3d.bas to the rfo-basic/source folder.

Install param3d.dat.txt and param3d.bas.txt to the rfo-basic/data folder. Note that these will change as you enter new settings while running the program. The initial files provided are for a knotted donut.

Touch the graphics display to bring up the main menu. Menu options are:

A. Enter x,y,z expressions. These are parametric equations for a 3D surface. Example: for a donut shape, enter 
  x(s,t) = cos(s)*(1+0.3*cos(t))
  y(s,t) = sin(s)*(1+0.3*cos(t))
  z(s,t) = 0.3*sin(t)

Note: just enter the expression to the right of the equals sign, not the whole equation.

B. Enter limits on s and t (Smin Smax Tmin Tmax)
For the donut example, these limits would be
   0 6.28 0 6.28

Note: separate the numbers with spaces (not commas).

C. Enter the number of facets in the s and t dimensions (NS NT). Reasonable numbers for the donut would be
   36 12

D. Enter the angles for rotation of the shape, Theta and Phi, in DEGREES.

Here,s how the rotation works. With no rotation (both angles 0), x axis is rightward, y axis is upward, and z axis points directly out of the screen. The Theta angle selects an axis in the xy plane about which the shape will be rotated. The Phi angle is the magnitude of rotation about that axis.

For example, for Theta Phi enter
   20 65
This will rotate the shape by 65 degrees about an axis in the xy plane which is 20 degrees away from the x axis.

E. Display settings

F. Draw Shape. This draws the 3D shape based on current settings.

G. Capture Image. Save drawn shape as a jpg image.

H. Return to Graphic. Return to graphics screen with no action.

I. Quit

Comments:

A. No error handling has been implemented as yet.

B. Each interaction with the menu does only one task. Generating a drawn shape from new equations typically involves several steps:

  GrTouch
    Enter Expressions
  GrTouch
    Enter limits for s and t
  GrTouch
    Enter number of facets in s and t dimensions
  GrTouch
    Enter angles for rotation of shape
  GrTouch
    Draw Shape

  It is OK to make changes to one setting only and then redraw. This would require two trips to the menu (one to change setting and one to draw)

C. With each setting change, the initialization files are rewritten.

D. The "input" command was used for user input. I found it didn't work correctly when sandwiched between "gr.front 0" and "gr.front 1" commands as suggested in the manual. It does work fine on my device without the gr.front commands. I hope it will work on other devices as well. If not, I would use "text.input" instead (this worked but seemed clunkier).
