PolyFit.

A simple App to fit a polynomial to your data.
It has a lot of restrictions, but gives the opportunity to examine your (simple, small) datasets.
If you want to test a dataset, it has to comply to the following layout (see Sample.txt):

-----------------------
Line 1. Title
Line 2. Title of X-axis
Line 3. Title of Y-axis
Line 4. x1,y1
Line 5. x2,y2
Line n. xn,yn
-----------------------

At startup you are given the option to use a demo dataset, to play with the app.
When a dataset is loaded, 1st until 6th order polynomials are calculated. This takes a little while.
Then you are presented a graph with the data plotted into it. And the 1st order line (linear fit) is visible.
You can toggle the visibility of all fitted lines by tapping on their legend.
After exiting the graph, the equations for all visible lines are given on a text screen.

Remarks.
- I am NOT a statistician ! The routine to do the fit has been translated from Visual Basic....
- No values for error estimation are calculated.
- Be careful to use high order polynomials...search the Internet why....
- The demo dataset has no relevance whatsoever, I made it up to reasonably resemble a 6th order fit.
- Do not use very big or very small numbers in a dataset; typically in the range 0.01-9999 for both axis work OK.

Playing with data is fun !

Aat.