! PolyFit
! Aat Don @2014
GR.OPEN 255,0,0,0,0,1
PAUSE 1000
GR.SCREEN w,h
ScaleX=720
ScaleY=h/w*ScaleX
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
WAKELOCK 3
CONSOLE.TITLE "PolyFit"
AxisL=600
DIM Calc_Y[600],GraphLines[600,6],ShowLine[6],Expr$[6]
! Get Data (or not)
GR.SET.STROKE 10
GR.TEXT.SIZE 70
GR.COLOR 255,255,255,0,1
GR.TEXT.DRAW g,230,200,"PolyFit"
GR.COLOR 255,0,255,0,1
GR.RECT g,100,300,620,500
GR.COLOR 255,0,0,255,1
GR.RECT g,100,600,620,800
GR.COLOR 255,0,0,255,0
GR.RECT g,100,300,620,500
GR.COLOR 255,0,255,0,0
GR.RECT g,100,600,620,800
GR.COLOR 255,255,0,0,1
GR.TEXT.DRAW g,150,420,"Get DataFile"
GR.TEXT.DRAW g,120,720,"Use Demo Data"
GR.RENDER
DO
	GR.TOUCH touched,x,y
UNTIL touched
DO
	GR.TOUCH touched,x,y
UNTIL !touched
x/=sx
y/=sy
IF x>100 & x<620 & y>300 & Y<500 THEN GetFile=1
IF x>100 & x<620 & y>600 & Y<800 THEN GetFile=0
GR.CLS
IF GetFile=1 THEN
	GR.FRONT 0
	LIST.CREATE n,xV
	LIST.CREATE n,yV
	GOSUB ChooseFile
	TEXT.OPEN r,DataFile,Path$+d2$[s]
		TEXT.READLN DataFile,GraphTitle$
		TEXT.READLN DataFile,XAxisTitle$
		TEXT.READLN DataFile,YAxisTitle$
		DO
			TEXT.READLN DataFile,Line$
			SPLIT XY$[],Line$,","
			IF Line$<>"EOF" THEN
				LIST.ADD xV,VAL(XY$[1])
				LIST.ADD yV,VAL(XY$[2])
			ENDIF
			UNDIM XY$[]
		UNTIL Line$="EOF"
	TEXT.CLOSE DataFile
	LIST.TOARRAY xV,XVal[]
	LIST.TOARRAY yV,YVal[]
	GR.FRONT 1
ELSE
	! Sample data
	ARRAY.LOAD XVal[],0,5,14,17,26,34,40,46,52,64,60
	ARRAY.LOAD YVal[],0,60,24,20,40,54,46,38,52,53,90
	GraphTitle$="Sample data set"
	XAxisTitle$="X-axis"
	YAxisTitle$="Y-axis"
ENDIF
GR.SET.STROKE 3
GR.TEXT.SIZE 40
! Draw frame
GR.COLOR 255,255,255,0,0
GR.RECT g,0,0,719,719
GR.RECT g,0,719,719,820
ARRAY.LENGTH NumOfPoints,XVal[]
ARRAY.MIN X_Min,XVal[]
ARRAY.MAX X_Max,XVal[]
ARRAY.MIN Y_Min,YVal[]
ARRAY.MAX Y_Max,YVal[]
X_Range=X_Max-X_Min
Y_Range=Y_Max-Y_Min
X_unit=AxisL/X_Range
Y_unit=AxisL/Y_Range
GOSUB DrawGraph
GOSUB PolyFit
degree=1
GOSUB ToggleLine
POPUP "Tap anywhere on graph to view X,Y",0,0,1
DO
	GOSUB GetTouch
UNTIL degree=7
GR.CLOSE
PRINT "Summary"
PRINT "-------"
PRINT "Data set contains "+REPLACE$(FORMAT$("###",NumOfPoints)," ","")+" points"
PRINT "*************************";
FOR i=1 TO LEN(REPLACE$(FORMAT$("###",NumOfPoints)," ",""))
	PRINT "*";
NEXT i
PRINT
PrtLn=6
FOR i=1 TO 6
	IF ShowLine[i]=1 THEN
		sf$="th"
		IF i=1 THEN sf$="st"
		IF i=2 THEN sf$="nd"
		IF i=3 THEN sf$="rd"
		PRINT FORMAT$("#",i)+sf$+" order equation:"
		PRINT Expr$[i]
		PRINT "**"
		PrtLn=PrtLn+3
	ENDIF
NEXT i
PRINT "PolyFit has finished"
PRINT "Press here to end program"
PRINT "+++++++++++++++++++++++++"
PRINT
DO
UNTIL LNr=PrtLn
WAKELOCK 5
EXIT

OnConsoleTouch:
	CONSOLE.LINE.TOUCHED LNr
ConsoleTouch.RESUME
GetTouch:
	DO
		GR.TOUCH touched,x,y
	UNTIL touched
	DO
		GR.TOUCH touched,x,y
	UNTIL !touched
	x/=sx
	y/=sy
	IF y>720 & y<820 THEN
		degree=FLOOR((x-130)/80)+1
		IF degree>0 & degree<7 THEN
			GOSUB ToggleLine
		ENDIF
	ENDIF
	IF X>60 & x<670 & y>60 & y<660 THEN
		POPUP "X="+REPLACE$(FORMAT$("###%.##",(ROUND(x)-60)/X_unit+X_Min)," ","")+" ,Y="+REPLACE$(FORMAT$("###%.##",Y_Max-(ROUND(y)-60)/Y_unit)," ",""),0,-300,1
	ENDIF
RETURN
DrawGraph:
	GR.COLOR 255,255,255,255,1
	GR.LINE g,60,60,60,670
	GR.LINE g,50,660,660,660
	GR.TEXT.SIZE 40
	GR.TEXT.ALIGN 2
	GR.TEXT.DRAW g,360,60,GraphTitle$
	GR.TEXT.SIZE 20
	FOR i=160 TO 760 STEP 100
		GR.ROTATE.START 90,20,i-100
			GR.TEXT.DRAW g,20,i-100,REPLACE$(FORMAT$("###%.##",Y_Max-(i-160)/100*(Y_Max-Y_Min)/6)," ","")
		GR.ROTATE.END
		GR.LINE g,50,i-100,60,i-100
		GR.TEXT.DRAW g,i-100,700,REPLACE$(FORMAT$("###%.##",X_Min+(i-160)/100*(X_Max-X_Min)/6)," ","")
		GR.LINE g,i,660,i,670
	NEXT i
	GR.TEXT.ALIGN 1
	GR.TEXT.WIDTH L,XAxisTitle$
	GR.TEXT.DRAW g,660-L,650,XAxisTitle$
	GR.ROTATE.START 90,70,60
		GR.TEXT.DRAW g,70,60,YAxisTitle$
	GR.ROTATE.END
	! Draw grid lines
	GR.COLOR 100,192,192,192,1
	GR.SET.STROKE 1
	FOR i=160 TO 660 STEP 100
		GR.LINE g,60,i-100,660,i-100
		GR.LINE g,i,60,i,660
	NEXT i
	GR.SET.STROKE 3
	! Draw points
	GR.COLOR 155,0,255,0,1
	GR.CIRCLE g,60,750,10
	FOR i=1 TO NumOfPoints
		GR.CIRCLE g,60+XVal[i]*X_unit-X_Min*X_unit,660-YVal[i]*Y_unit+Y_Min*Y_unit,10
	NEXT i
	GR.COLOR 255,0,255,0,1
	GR.TEXT.DRAW g,40,780,"data"
	FOR i=1 TO 6
		sf$="th"
		IF i=1 THEN sf$="st"
		IF i=2 THEN sf$="nd"
		IF i=3 THEN sf$="rd"
		C$=RIGHT$("000"+BIN$(i),3)
		GR.COLOR 255,VAL(MID$(C$,1,1))*255,VAL(MID$(C$,2,1))*255,VAL(MID$(C$,3,1))*255,1
		GR.LINE g,140+(i-1)*80,750,200+(i-1)*80,750
		GR.TEXT.DRAW g,150+(i-1)*80,780,REPLACE$(FORMAT$("%",i)," ","")+sf$
		GR.TEXT.DRAW g,145+(i-1)*80,800,"order"
	NEXT i
	GR.COLOR 255,255,0,0,1
	GR.TEXT.SIZE 100
	GR.TEXT.DRAW g,620,810,CHR$(10008)
	GR.TEXT.SIZE 30
	GR.COLOR 255,255,255,255,1
	GR.TEXT.DRAW g,635,780,"EXIT"
	GR.COLOR 230,0,0,0,1
	GR.RECT Fade,0,0,720,821
	GR.COLOR 255,0,0,255,1
	GR.RECT DBack,325,190,395,260
	GR.TEXT.SIZE 100
	GR.TEXT.ALIGN 2
	GR.COLOR 255,255,255,0,1
	GR.TEXT.DRAW Dice,360,260," "
	GR.TEXT.ALIGN 1
RETURN
PolyFit:
	FOR degree=1 TO 6
		GR.MODIFY Dice,"text",CHR$(9862-degree)
		GR.RENDER
		DIM Coeffs[degree+1,degree+2]
		FOR j = 0 To degree
			Coeffs[j+1,degree+2]=0
			FOR pt=1 TO NumOfPoints
				Coeffs[j+1,degree+2]=Coeffs[j+1,degree+2]-XVal[pt]^j*YVal[pt]
			NEXT pt
			FOR a_sub=0 TO degree
				Coeffs[j+1,a_sub+1]=0
				FOR pt=1 TO NumOfPoints
					Coeffs[j+1,a_sub+1]=Coeffs[j+1,a_sub+1]-XVal[pt]^(a_sub+j)
				NEXT pt
			NEXT a_sub
		NEXT j
		max_equation=degree+1
		max_coeff=degree+2
		FOR i=1 TO max_equation
			IF Coeffs[i,i]=0 THEN
				FOR j=i+1 TO max_equation
					IF Coeffs[j,i]<>0 Then
						FOR k=i TO max_coeff
							SWAP Coeffs[i,k],Coeffs[j,k]
						NEXT k
						F_N.BREAK
					ENDIF
				NEXT j
			ENDIF
			coeff_i_i=Coeffs[i,i]
			IF coeff_i_i=0 THEN END "Error, please, check your data..."
			FOR j=i TO max_coeff
				Coeffs[i,j]=Coeffs[i,j]/coeff_i_i
			NEXT j
			FOR j=1 TO max_equation
				IF j<>i THEN
					coef_j_i=Coeffs[j,i]
					FOR d=1 TO max_coeff
						Coeffs[j,d]=Coeffs[j,d]-Coeffs[i,d]*coef_j_i
					NEXT d
				ENDIF
			NEXT j
		NEXT i
		! Calculate line
		Counter=0
		FOR i=X_Min TO X_Max STEP X_Range/599
			Counter=Counter+1
			Calc_Y[Counter]=0
			FOR j=1 TO max_equation
				Calc_Y[Counter]=Calc_Y[Counter]+Coeffs[j,max_coeff]*i^(j-1)
			NEXT j
		NEXT i
		! Compose string
		IF max_equation>2 THEN
			Expr$[degree]="Y="+REPLACE$(STR$(ROUND(Coeffs[max_equation,max_coeff]*10000000)/10000000)," ","")+"*X^"+REPLACE$(FORMAT$("#",max_equation-1)," ","")
		ELSE
			Expr$[degree]="Y="
		ENDIF
		FOR j=max_equation-1 TO 3 STEP -1
			IF Coeffs[j,max_coeff]<0 THEN
				Expr$[degree]=Expr$[degree]+REPLACE$(STR$(ROUND(Coeffs[j,max_coeff]*10000000)/10000000)," ","")+"*X^"+REPLACE$(FORMAT$("#",j-1)," ","")
			ELSE
				Expr$[degree]=Expr$[degree]+"+"+REPLACE$(STR$(ROUND(Coeffs[j,max_coeff]*10000000)/10000000)," ","")+"*X^"+REPLACE$(FORMAT$("#",j-1)," ","")
			ENDIF
		NEXT j
		IF Coeffs[2,max_coeff]<0 THEN
			Expr$[degree]=Expr$[degree]+REPLACE$(STR$(ROUND(Coeffs[2,max_coeff]*10000000)/10000000)," ","")+"*X"
		ELSE
			IF max_equation>2 THEN
				Expr$[degree]=Expr$[degree]+"+"+REPLACE$(STR$(ROUND(Coeffs[2,max_coeff]*10000000)/10000000)," ","")+"*X"
			ELSE
				Expr$[degree]=Expr$[degree]+REPLACE$(STR$(ROUND(Coeffs[2,max_coeff]*10000000)/10000000)," ","")+"*X"
			ENDIF
		ENDIF
		IF Coeffs[1,max_coeff]<0 THEN
			Expr$[degree]=Expr$[degree]+REPLACE$(STR$(ROUND(Coeffs[1,max_coeff]*10000000)/10000000)," ","")
		ELSE
			Expr$[degree]=Expr$[degree]+"+"+REPLACE$(STR$(ROUND(Coeffs[1,max_coeff]*10000000)/10000000)," ","")
		ENDIF
		UNDIM Coeffs[]
		! Draw calculated line into array
		ShowLine[degree]=0
		C$=RIGHT$("000"+BIN$(degree),3)
		GR.COLOR 255,VAL(MID$(C$,1,1))*255,VAL(MID$(C$,2,1))*255,VAL(MID$(C$,3,1))*255,1
		FOR Counter=1 TO 598
			GR.LINE GraphLines[Counter,degree],60+Counter-1,660-Calc_Y[Counter]*Y_unit+Y_Min*Y_unit,60+Counter,660-Calc_Y[Counter+1]*Y_unit+Y_Min*Y_unit
			GR.HIDE GraphLines[Counter,degree]
		NEXT Counter
		GR.MODIFY Fade,"alpha",230-degree*20
		GR.RENDER
	NEXT degree
	GR.HIDE DBack
	GR.MODIFY Dice,"text","Ready"
	GR.RENDER
	PAUSE 1000
	GR.HIDE Dice
	GR.HIDE Fade
RETURN
ToggleLine:
	IF ShowLine[degree]=0 THEN
		FOR i=1 TO Counter
			GR.SHOW GraphLines[i,degree]
		NEXT i
		ShowLine[degree]=1
	ELSE
		FOR i=1 TO Counter
			GR.HIDE GraphLines[i,degree]
		NEXT i
		ShowLine[degree]=0
	ENDIF
	GR.RENDER
RETURN
ChooseFile:
	! From f25_dir.bas
	Path$ = ""
	LOOP:
	ARRAY.DELETE d1$[]
	FILE.DIR Path$, d1$[]
	ARRAY.LENGTH length, d1$[]
	ARRAY.DELETE d2$[]
	DIM d2$[length+1]
	d2$[1] = ".."
	FOR i = 1 TO length
	 d2$[i + 1] = d1$[i]
	NEXT i
	SELECT s, d2$[], "Select Datafile..."
	IF s = 0 THEN RETURN
	IF s>1 THEN
		n = IS_IN("(d)", d2$[s])
		IF n = 0
			RETURN
		ENDIF
		dname$ = LEFT$(d2$[s],n-1)
		Path$=Path$+dname$+"/"
		GOTO LOOP
	ENDIF
	IF Path$ = "" THEN
		Path$ = "../"
		GOTO LOOP
	ENDIF
	ARRAY.DELETE p$[]
	SPLIT p$[], Path$, "/"
	ARRAY.LENGTH length, p$[]
	IF p$[length] = ".." THEN
		Path$ = Path$ + "../"
		GOTO LOOP
	ENDIF
	IF length = 1 THEN
		Path$ = ""
		GOTO LOOP
	ENDIF
	Path$ = ""
	FOR i = 1 TO length - 1
		Path$ = Path$ + p$[i] + "/"
	NEXT i
	GOTO LOOP
RETURN
