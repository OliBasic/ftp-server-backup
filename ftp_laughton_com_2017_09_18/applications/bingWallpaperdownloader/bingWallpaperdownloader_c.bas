
path$          = "bingWallpaperdownloader/"
limDownload    =  2
flagAdjustLim  =  1




GOSUB            initDownloader

!main loop -------------------------

DO



 GOSUB           downloadFile




UNTIL             sza >= limDownload
!-----------------------------------


regularExit:

PRINT             CHR$(10)+ CHR$(10)+ " downloadlimit reached...."
END



errorExit:
print            "unknownError:"
print            curError$
END




ONERROR:

errNo          = 0
curError$      = GETERROR$()
CLIPBOARD.PUT    GETERROR$()

if               is_in("byte.open",curError$,1) then errNo=1

if errNo       = 0 then GOTO  errorExit
if errNo       = 1 then GOTO  errNo_1_goAhead



!!
errNo=1
Problem: java.io.FileNotFoundException: http://cache.wallpaperdownloader.com/bing/img/hprichbg?p=rb%2fGreatGlacier_20111125.jpg at:
Line: byte.openr,tmp,link$
!!










!-----------------------------------
initDownloader:


GOSUB            userfunctions


FILE.EXISTS      ex, path$
IF               !ex THEN file.mkdir path$

FILE.DIR         path$, files$[]
LIST.CREATE      s,files
LIST.ADD.ARRAY   files, files$[]

LIST.SIZE        files, sz

pageIdxJump    = FLOOR(sz/11) +1
pageIdx        = 0



urlBas$        = "http://www.wallpaperdownloader.com/bing-wallpaper/archive/all/all/" + "@@@" + "/datedes/"

LIST.CREATE      s, linkNameList

emp $          = "                             "
f1 $           = "####.#"

IF flagAdjustLim
 INPUT            "Downloadlimit [MByte]:", limDownload, limDownload
ENDIF

limDownload    = limDownload*1000

RETURN
!-----------------------------------


!-----------------------------------
getFreshLinks:

url$           = REPLACE$(REPLACE$(urlBas$,"@@@",STR$(pageIdx)),".0","")
CLIPBOARD.PUT    url $
PRINT            "================================================"
PRINT            "pageNo: " ; pageIdx ; "   grabbing url.."
GRABURL          urlcont$ , url$
PRINT            "...done "

hits           = countOccurance(".jpg", urlcont$)

startpos       = 0
FOR i          = 1 TO hits

 startpos      = is_in     ( ".jpg" , urlcont$, startpos +2 )
 pos           = findBefore( "http" , urlcont$, startpos,300)
 link$         = MID$      ( urlcont$, pos, startpos-pos+4)
 pos           = findBefore ( "/"   , link$ , LEN( link$ ) ,300)
 name$         = MID$       ( link$ ,pos+1  , LEN( link$ ) -pos)
 
!!
only for testing: get the publishing-date ---------
 xx            = is_in     ( "Bing Image:" , urlcont$, startpos )
 kk$           = MID$      ( urlcont$ , xx , 30)
 print kk$
!!

 LIST.ADD        linkNameList , link$
 LIST.ADD        linkNameList , name$

NEXT

RETURN
!-----------------------------------


!-----------------------------------
DOwnloadFile:

 LIST.SIZE        linkNameList , szLnl

 IF !szLnl
  pageIdx++
  GOSUB           getFreshLinks
 ENDIF

 LIST.GET          linkNameList, 1, link$
 LIST.GET          linkNameList, 2, name$

 LIST. remove      linkNameList, 1
 LIST. remove      linkNameList, 1

 LIST.SEARCH       files, name$, ex


 IF !ex

  BYTE.OPEN        r, tmp, link$
  BYTE.COPY        tmp, path$  + name$
  BYTE.CLOSE       tmp
  FILE.SIZE        sz ,  path$ + name$
  sz            /= 1000
  sza           += sz
  name$         =  LEFT$(name$, 40)
  out $         =  name$ + LEFT$(emp$, 45-LEN(name$))
  out $         =  out$  + FORMAT$(f1$,sz) + "  /" + FORMAT$(f1$,sza)
  PRINT            out$

 ELSE

  PRINT            name$ ; "...already exists!"

  IF               !flagContact
   flagContact  =  1
   PRINT            " contact ! "
   FILE.DIR         path$, tmp$[]
   ARRAY.LENGTH     len , tmp$[]
   pageIdx        = FLOOR(len/11)
  ENDIF

 errNo_1_goAhead:
 ENDIF



RETURN
!-----------------------------------





 !-----------------------------------
 userFunctions:


 FN.DEF         countOccurance( searchStr$ , inpStr$ )
  DO
   ctr        +=1
   pos        = IS_IN( searchStr$ , inpStr$ , pos +1 )
  UNTIL        !pos
  FN.RTN       ctr-1
 FN.END


 FN.DEF findBefore( searchStr$ , inpStr$,  startpos, offsBefore )

  strTmp$     = MID$( inpStr$ ,1, startpos )
  pos         = LEN(strTmp$)- offsBefore
  IF  pos     <1 THEN pos=1
  DO
   pos        = IS_IN(searchStr$, strTmp$ ,pos+1)
   IF pos     < startpos & pos <> 0 THEN result = pos
  UNTIL         pos > startpos | pos =0

  FN.RTN        result

 FN.END

 RETURN
 !---------------------------------
