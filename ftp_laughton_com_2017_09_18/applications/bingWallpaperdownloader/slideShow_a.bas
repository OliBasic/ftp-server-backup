GR.OPEN              255, 0, 0, 0, 0, 0
GR.SCREEN            w, h


path$             =  "/bingWallpaperdownloader"
!path$             =  ""
desLooptime       =  7000
tChange           =  6000

flagScale         =  1
facScaleImg       =  1.1



GOSUB                initAndRiseFirstImage



!main loop ---------------------
DO




 GOSUB               changeImage




 DO
  PAUSE               100
 UNTIL                CLOCK() > looptime
 looptime           = CLOCK() + desLooptime

UNTIL                 0
!-------------------------------




END










'-------------------------------------
initAndRiseFirstImage:

ddAlpha           =  10000 / tChange

FILE.DIR             path$, files$[]
LIST.CREATE          s, fileListSlideShow

ARRAY.LENGTH         len, files$[]
FOR i             =  1 TO len
 tmp$             =  RIGHT$(files$[i], 3)
 flagTmp          =  tmp$="jpg" | tmp$="bmp" | tmp$="png"
 IF flagTmp          THEN LIST.ADD fileListSlideShow, files$[i] ELSE ctrx= ctrx+1
NEXT
UNDIM                files$[]
LIST.TOARRAY         fileListSlideShow, files$[]

PRINT len, ctrx


GOSUB                loadAndScaleRndImage


GR.BITMAP.DRAW       picAct    , bmpsca    , sx , sy
looptime          =  CLOCK() + desLooptime/2
dAlpha            =  0
DO
 GR.MODIFY           picAct    , "alpha", 0   + dAlpha
 dAlpha           += ddAlpha
 GR.RENDER
UNTIL                dAlpha>=255
DO
UNTIL                CLOCK() > looptime
looptime           = CLOCK() + desLooptime

RETURN
'-------------------------------------



'-------------------------------------
changeImage:

bmptmpOld         =  bmptmp
bmpscaOld         =  bmpsca
picActOld         =  picAct
sxOld             =  sx
syOld             =  sy

GOSUB                loadAndScaleRndImage

GR.CLS
GR.BITMAP.DRAW       picActOld , bmpscaOld , sxOld , syOld
GR.BITMAP.DRAW       picAct    , bmpsca    , sx    , sy


tic               =  clock()
dAlpha            =  0
DO
 GR.MODIFY           picActOld , "alpha", 255 - dAlpha
 GR.MODIFY           picAct    , "alpha", 0   + dAlpha
 dAlpha           += ddAlpha
 GR.RENDER
UNTIL                dAlpha>=255
toc               =  clock()
print                round (ddalpha*10)/10, (toc-tic)
ddAlpha           =  ddAlpha * (toc-tic)/ tChange


GR.BITMAP.DELETE     bmptmpOld
IF                   flagscale
 GR.BITMAP.DELETE    bmpscaOld
ENDIF

RETURN

'-------------------------------------


'-------------------------------------
loadAndScaleRndImage:

LIST.SIZE             fileListSlideShow , sz

IF !sz                THEN LIST.ADD.ARRAY fileListSlideShow, files$[]

picRnd              = floor (RND()*sz)+1
LIST.GET              fileListSlideShow, picRnd, dat$
LIST.REMOVE           fileListSlideShow, picRnd

GR.BITMAP.LOAD        bmptmp , path$ + "/" + dat$

IF                    flagscale
 GR.BITMAP.SCALE      bmpsca , bmptmp, w* facScaleImg , h* facScaleImg
 sx                =  w/2*(1- facScaleImg)
 sy                =  h/2*(1- facScaleImg)

ELSE
 GR.BITMAP.SIZE       bmptmp , xxx , yyy
 bmpsca             = bmptmp
 sx                 = (w - xxx) /2
 sy                 = (h - yyy) /2

ENDIF


RETURN
'-------------------------------------
