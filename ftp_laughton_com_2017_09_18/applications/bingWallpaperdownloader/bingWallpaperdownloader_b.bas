GOSUB            userfunctions

path$          = "bingWallpaperdownloader/"
FILE.EXISTS      ex, path$
IF               !ex THEN file.mkdir path$

lastPageIdx$   = path$ + "lastPageIdx.txt"
FILE.EXISTS      ex, lastPageIdx$
IF               !ex THEN
 TEXT.OPEN       w, fid, lastPageIdx$
 TEXT.WRITELN    fid, 1
 TEXT.CLOSE      fid
ENDIF

GRABFILE         tmp$, lastPageIdx$
pageIdx        = val (tmp$)

FILE.DIR         path$, files$[]
LIST.CREATE      s,files
LIST.ADD.ARRAY   files, files$[]



urlBas$        = "http://www.wallpaperdownloader.com/bing-wallpaper/archive/all/all/" + "@@@" + "/datedes/"

begin$         = "http"


!-----------------------------------
DO

 url$         = REPLACE$(REPLACE$(urlBas$,"@@@",STR$(pageIdx)),".0","")
 clipboard.put  url $
 print          "================================================"
 print          "pageNo: " ; pageIdx
 tt           = tic( "grabbing url..")
 GRABURL        urlcont$ , url$
 tt           = toc( "grabbing url.." , tt)

 hits         = countOccurance(".jpg", urlcont$)

 startpos     = 0
 FOR i        =1 TO hits

  startpos    = is_in     ( ".jpg" , urlcont$, startpos +2 )
  pos         = findBefore( begin$ , urlcont$, startpos,300)
  link$       = MID$      ( urlcont$, pos, startpos-pos+4)
  pos         = findBefore ( "/"   , link$ , LEN( link$ ) ,300)
  name$       = MID$       ( link$ ,pos+1  , LEN( link$ ) -pos)

  LIST.SEARCH   files,  name$ , ex

  IF !ex
   tt         = tic("download")
   PRINT        name$
   BYTE.OPEN    r, tmp, link$
   BYTE.COPY    tmp, path$  + name$
   tt         = toc("download", tt)
  ELSE
   PRINT name$ ; "...already exists!"
  ENDIF

 NEXT

 
 pageIdx++

 TEXT.OPEN       w, fid, lastPageIdx$
 TEXT.WRITELN    fid, pageIdx
 TEXT.CLOSE      fid

 

UNTIL           0
!-----------------------------------

END


!-----------------------------------
userFunctions:

  FN.DEF tic(strToPrint$)
   PRINT "------------------------------"
   PRINT strToPrint$ ; "..."
   FN.RTN CLOCK()
  FN.END

  FN.DEF toc(strToPrint$, tstart)
   dt$ = FORMAT$("##%.###", (CLOCK()-tstart)/1000 )
   PRINT strToPrint$ ; "...done in" ; dt$ ;" sec"
   PRINT "------------------------------"
   FN.RTN dt
  FN.END


  FN.DEF countOccurance( searchStr$ , inpStr$ )
   DO
    ctr+=1
    pos = IS_IN( searchStr$ , inpStr$ , pos +1 )
   UNTIL !pos
   FN.RTN ctr-1
  FN.END


  FN.DEF findBefore( searchStr$ , inpStr$,  startpos, offsBefore )

   strTmp$  = MID$( inpStr$ ,1, startpos )
   pos = LEN(strTmp$)- offsBefore
   IF pos <1 THEN pos=1
   DO
    pos = IS_IN(searchStr$, strTmp$ ,pos+1)
    IF pos < startpos & pos <> 0 THEN result = pos
   UNTIL pos > startpos | pos =0

   FN.RTN result

  FN.END

 RETURN
 !---------------------------------




