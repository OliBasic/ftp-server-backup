rem  Music Player for Android

songplaying=0
dim d1$[1]
dim Directory$[1]
dim Songs$[1]
dim dirs$[1]
dim PlayingSongs$[1]
dim abuff[3]


path$="../../music/"
kp$="@"
don=0

cls
print "s to pick, q to quit"
while don=0
  if songplaying=0 then

  else
    audio.position.current cpos
    if cpos<opos then
      song=mod(song, numsongs)+1
      audio.release abuff[1]
      abuff[1]=abuff[2]
      abuff[2]=abuff[3]
      audio.stop
      audio.play abuff[2]
      opos=-10000
      cpos=0
    
      print "loading: ";path$+PlayingSongs$[mod(song, numsongs)+1]   
      audio.load abuff[3], path$+PlayingSongs$[mod(song, numsongs)+1]
      print "done load"
      audio.length slong, abuff[2]     
    endif
    if cpos-opos>=1000 then
      opos=cpos
      cls
      print "s to pick, q to quit"
      tl=slong-cpos
      print path$

      print "previous:";PlayingSongs$[mod(song-2+numsongs, numsongs)+1]
      print "now playing:";PlayingSongs$[song];format$("####",tl/1000);" sec. remain"
      print "      cpos:";cpos
      print "     slong:";slong
      print "next:";PlayingSongs$[mod(song, numsongs)+1]
    endif
  endif
  inkey$ kp$
  if kp$="s" then
    
    newdirectory:
    print "newdirectory  ";path$
    array.delete d1$[]
    array.delete Directory$[]
    print "about to call file.dir"
    file.dir path$, d1$[]
    print "back from file.dir"
    array.length length, d1$[]
    print "length ";length
    %find total number of dirs and audio files
    numsongs=0
    numdirs=0
    print "counting dirs and songs"
!echo.on
    for x=1 to length
      if right$(d1$[x],3)="(d)" then
        numdirs=numdirs+1
      endif
       
tempstring$=upper$(right$(d1$[x],4))
	sw.begin tempstring$
        sw.case ".WAV"
        sw.case ".AAC"
        sw.case ".MP3"
        sw.case ".AMR"
        sw.case ".OGG"
          numsongs=numsongs+1
      sw.end
      sw.begin upper$(right$(d1$[x],5))
        sw.case ".FLAC"
        sw.case ".MIDI"
          numsongs=numsongs+1
      sw.end
    next
    print "copying dirs ";numdirs;"   and songs";numsongs
    tnds=0
    tnss=0
    
    if numsongs>0 then
      array.delete songs$[]
      dim songs$[numsongs]
    endif
    if numdirs>0 then
      array.delete dirs$[]
      dim dirs$[numdirs]
    endif 
    for x=1 to length
      if right$(d1$[x],3)="(d)" then
        tnds=tnds+1
        let dirs$[tnds]=d1$[x]
	endif
	sw.begin upper$(right$(d1$[x],4))
        sw.case ".WAV"
        sw.case ".AAC"
        sw.case ".MP3"
        sw.case ".AMR"
        sw.case ".OGG"
          tnss=tnss+1
          let songs$[tnss]=d1$[x]
      sw.end
      sw.begin upper$(right$(d1$[x],5))
        sw.case ".FLAC"
        sw.case ".MIDI"
          tnss=tnss+1
          let songs$[tnss]=d1$[x]
      sw.end
    next 
    print "sorting dirs and songs"
    if numsongs>0 then
      array.sort songs$[]
    endif
    if numdirs>0 then
      array.sort dirs$[]
    endif
    print "building list for select"
    dim Directory$[numdirs+numsongs+1]
    let Directory$[1]=".."
    tn=1
    for x=1 to numdirs
      tn=tn+1
      let directory$[tn]=dirs$[x]
    next
    for x=1 to numsongs
      tn=tn+1
      let directory$[tn]=songs$[x]
    next
    print "about to call select"
    select selection, Directory$[],path$
    songselection=0
    if selection>0 then
      print "selected",selection
      if selection=1 & path$="../" then 
        goto newdirectory
      endif
      print "not at root"
      if selection=1 then
        do
          path$=left$(path$,len(path$)-1)
        until right$(path$,1)="/"
        goto newdirectory %and list the directory
      endif
      print "about to check if selection is a directory"
    
      if right$(directory$[selection],3)="(d)" then
        path$=path$+left$(directory$[selection],len(directory$[selection])-3)+"/"
        goto newdirectory
      endif
      print "chose ";directory$[selection]
      tnn=0
      songselection=(selection-numdirs)-1
      array.delete PlayingSongs$[]
      dim playingsongs$[numsongs]
      print "making playlist"
      for x=1 to numsongs
        print x
        let playingsongs$[x]=songs$[x]
        print x
      next
!echo.on
      print "out of loop ";x
!    else
!      print "selection cancelled"
!      songselection=0
    endif 
    print "picked ";songselection
    if songselection>0 then
      song=songselection
      print "songselectoon ";songselection
      array.length numsongs, PlayingSongs$[]
      print "numsongs ";numsongs
      print "about to play  #";song;"  ";PlayingSongs$[song]
      audio.stop

!Load and play current Song
      audio.load abuff[2], path$+PlayingSongs$[song]
      audio.play abuff[2]
      audio.length slong, abuff[2]

!load previous song
      audio.load abuff[1], path$+PlayingSongs$[mod(song-2+numsongs,numsongs)+1]

!load next song
      audio.load abuff[3], path$+PlayingSongs$[mod(song, numsongs)+1]

      songplaying=1
	  opos=-10000
    cpos=0
    endif
  endif
  if kp$="q" then
    print "q pressed"
    let don=1.0
    print don
  endif
  if kp$="left" then
    if cpos>1000 then
      audio.position.seek 0
      opos=-10000
    else
      song=mod(song-2+numsongs,numsongs)+1
      audio.release abuff[3]
      abuff[3]=abuff[2]
      abuff[2]=abuff[1]
      audio.stop
      audio.play abuff[2]
      opos=-10000
      cpos=0
      print "loading: ";PlayingSongs$[mod(song-2+numsongs,numsongs)+1]
      audio.load abuff[1], path$+PlayingSongs$[mod(song-2+numsongs,numsongs)+1]
      print "done load"
      audio.length slong, abuff[2]
    endif
  endif 
  if kp$="right" then
    song=mod(song, numsongs)+1
    audio.release abuff[1]
    abuff[1]=abuff[2]
    abuff[2]=abuff[3]
    audio.stop
    audio.play abuff[2]
    opos=-10000
    cpos=0
  
    print "loading: ";PlayingSongs$[mod(song, numsongs)+1]
    audio.load abuff[3], path$+PlayingSongs$[mod(song, numsongs)+1]
    print "done load"
    audio.length slong, abuff[2]
  endif 

  if kp$<>"@" then
    print don, kp$
  endif
repeat
end

onerror:
print "error"
end
 
 
 
 
 
