! zink . zen ink
!
! By Luis Pieri 

! gfx init
gr.open 255,255,255,255,0,1
gr.orientation 1
gr.screen wx,wy
gr.set.antialias 1
gr.set.stroke 3
gr.color 255,0,0,0,1

! Vars
lx=0
ly=0
nc=0
np=0
Arch$=""
cx = (wx/480)
cy = 54*cx
stk=3*cx*1.02

! bitmaps
gr.bitmap.create bmpcanvas, wx,wy 
gr.bitmap.create bmpcanvas1, wx,wy 
gr.bitmap.create bmpcanvas2, wx,wy 
gr.bitmap.create bmpcanvast, wx,wy 

! loop init
gr.bitmap.draw Canvas,bmpcanvas,0,0
gr.bitmap.draw tapa,bmpcanvas2,0,0

GOSUB Caratula
GOSUB ProxPag
GOSUB ToolBar

gr.color 255,0,0,0,1
gr.text.size 30
gr.text.align 2
gr.text.bold 0
gr.text.draw Pag,wx/2,wy-20,"1"
GOSUB SetStk

! --------------------------------------------
Arranco:
DO
  gr.touch touched,tx,ty
  
  IF touched THEN
    IF ty<54 THEN
      gr.hide Pag
      GOSUB ToolTouch
      gr.modify Pag,"text",format$("###",np)
      gr.show Pag
      GOSUB SetStk
    ELSE
      ! Draw!
      GOSUB DrawStk
      lx=tx
      ly=ty
    ENDIF
  ELSE
    lx = 0
    ly = 0
  ENDIF
  
  gr.render
UNTIL 0
END


! --------------------------------------------
ClearCanvas:
  gr.bitmap.drawinto.start bmpcanvas
   gr.color 255,254,249,211,1
   gr.rect n,0,0,wx,wy
  gr.bitmap.drawinto.end
  RETURN


! --------------------------------------------
!OnError:
!  gr.front 1
!  GOTO Arranco
!  RETURN

    
! --------------------------------------------
ProxPag:
  ! grabo pag actual
  Arch$ = "zk-"+format$("%%%",np)+".png"
  IF np>0 THEN
    gr.bitmap.save bmpcanvas,Arch$,0
    gr.bitmap.delete bmpcanvas1
    gr.bitmap.create bmpcanvas1,wx,wy
    gr.color 255,0,0,0,1
    gr.bitmap.drawinto.start bmpcanvas1
     gr.bitmap.draw n,bmpcanvas,0,0
    gr.bitmap.drawinto.end
  ENDIF
  np=np+1
  Arch$ = "zk-"+format$("%%%",np)+".png"
  file.exists lb, Arch$ 
  GOSUB ClearCanvas
  IF lb THEN
      gr.bitmap.delete bmpcanvast
      gr.bitmap.load bmpcanvast,Arch$
      gr.color 255,0,0,0,1
      gr.bitmap.drawinto.start bmpcanvas
        gr.bitmap.draw n,bmpcanvast,0,0
      gr.bitmap.drawinto.end
  ENDIF
  gr.modify Canvas,"bitmap",bmpcanvas
  gr.bitmap.scale bmpcanvas2,bmpcanvas1,wx-1,wy,1
  gr.color 255,0,0,0,1
  gr.modify tapa,"bitmap",bmpcanvas2
  gr.show tapa
  gr.render
  pause 500
  FOR i = 10 to 1 STEP -1
    gr.bitmap.delete bmpcanvas2
    gr.bitmap.scale bmpcanvas2,bmpcanvas1,wx*(i/10)-1,wy,1
    gr.color 30,0,0,0,1 
    gr.bitmap.drawinto.start bmpcanvas2
      gr.rect n,0,0,wx*(i/10)-1,wy
    gr.bitmap.drawinto.end
    gr.modify tapa,"bitmap",bmpcanvas2
    !gr.bitmap.draw tapa,bmpcanvas2,0,0
    gr.render
  NEXT
  gr.hide tapa
  RETURN


! --------------------------------------------
AntePag:
  ! grabo pag actual
  Arch$ = "zk-"+format$("%%%",np)+".png"
  IF np>0 THEN
    gr.bitmap.save bmpcanvas,Arch$,0
    gr.bitmap.delete bmpcanvas1
    gr.color 255,0,0,0,1
    gr.bitmap.create bmpcanvas1,wx,wy
  ENDIF
  IF np>1 THEN
    np=np-1
    Arch$ = "zk-"+format$("%%%",np)+".png"
    file.exists lb, Arch$ 
    !GOSUB ClearCanvas
    IF lb THEN
      gr.bitmap.delete bmpcanvast
      gr.bitmap.load bmpcanvast,Arch$
      gr.color 255,0,0,0,1
      gr.bitmap.drawinto.start bmpcanvas1
        gr.bitmap.draw n,bmpcanvast,0,0
      gr.bitmap.drawinto.end
    ENDIF
    FOR i = 1 to 10
      gr.bitmap.delete bmpcanvas2
      gr.bitmap.scale bmpcanvas2,bmpcanvas1,wx*(i/10)-1,wy,1
      gr.color 30,0,0,0,1 
      gr.bitmap.drawinto.start bmpcanvas2
        gr.rect n,0,0,wx*(i/10)-1,wy
      gr.bitmap.drawinto.end
      gr.modify tapa,"bitmap",bmpcanvas2
      gr.show tapa
      gr.render
    NEXT
    gr.color 255,0,0,0,1
    gr.bitmap.drawinto.start bmpcanvas
      gr.bitmap.draw n,bmpcanvast,0,0
    gr.bitmap.drawinto.end
    gr.modify Canvas,"bitmap",bmpcanvas
    gr.hide tapa
  ENDIF
  RETURN


! --------------------------------------------
Caratula:
  gr.bitmap.drawinto.start bmpcanvas1
    gr.color 255,0,0,0,1
    gr.rect n,0,0,wx,wy
    gr.color 255,255,255,255,1
    gr.text.size 20
    gr.text.align 1
    gr.text.bold 0
    gr.text.draw n,wx/2,wy*0.8,"z e n  i n k"
  gr.bitmap.drawinto.end
  RETURN

  
! --------------------------------------------  
ToolBar:
  gr.color 255,0,0,0,1
  gr.text.size 40*cx
  gr.text.align 2
  gr.text.bold 1
  gr.text.draw n,30,cy/2,"<"
  gr.text.draw n,wx-30,cy/2,">"
  gr.text.draw n,wx/2,cy/2,"x"
  RETURN


! --------------------------------------------
ToolTouch:
  lx=0
  ly=0
  ! Toolbar touched
  IF tx<54*cx THEN
    GOSUB AntePag
  ELSE
    IF tx>wx-54*cx THEN
      GOSUB ProxPag
    ELSE
      IF tx>(wx/2)-28*cx & tx<(wx/2)+28*cx THEN
        GOSUB ClearCanvas
      ENDIF
    ENDIF
  ENDIF
  RETURN


! --------------------------------------------  
DrawStk:
  IF lx>0 & ly>0 THEN
    IF ABS(lx-tx)<3 & ABS(ly-ty)<3 THEN
      nc=nc+1
    ELSE
      nc=0
    ENDIF
    IF nc>2 THEN
      GOSUB drawpoint
      nc=0
    ENDIF
    gr.bitmap.drawinto.start bmpcanvas
      gr.line n,lx,ly,tx,ty
    gr.bitmap.drawinto.end
  ENDIF
  RETURN


! --------------------------------------------
DrawPoint:
  gr.bitmap.drawinto.start bmpcanvas
    gr.circle n,tx,ty,stk-1
  gr.bitmap.drawinto.end     
  RETURN


! --------------------------------------------  
SetStk:
  gr.color 170,0,0,0,1
  gr.set.stroke stk
  RETURN
