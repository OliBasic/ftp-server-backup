REM To calculate mass of air
Input "Pressure in lb/sq inch",p$
P=val(p$)
Input "Volume in cubic ft",v$
V=val(v$)
Input "Temperature in Centigrade",c$
C=val(c$)
M=(p*v)/(0.37*(((9*c+160)/5)+460))
Print "Mass of air in lb : ";m

End

OnError:
Print "Invalid input, quitting..." 

End
 
 
