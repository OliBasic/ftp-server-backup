REM word generator pro 2015-04-06 by qqtac

DIM ko[9,6,5] %utolso szam:1= x1,2=y1,3=x2,4=y2,5=re

GR.OPEN 255,205,205,205,0,0
kx=1300
ky=870 %arany=(1300/870)=1.494
GR.SCREEN w,h %most 1024x552 (arany=1.855)
xx1=(w/kx)*1.2
yy1=(h/ky)*1.2
IF (kx/ky)<=(w/h) THEN %automatic aspect ratio
 xx1=yy1
ELSE
 yy1=xx1
ENDIF

GR.SCALE xx1,yy1
tma=(h/(yy1)-ky)/2
lma=(w/(xx1)-kx)/2

DIM space[6],button[6]
DIM a$[3],LOCK[9]

osz=40 %oszlopkoz szelessege
osn=9 %oszlopok szama
hh=9 %hh=4-8 word length
gosz=85 % button wide
a$[1]="ABCDEFGHIJKLMNOPQRSTUVWXYZ" %all
a$[3]="BCDFGHJKLMNPQRSTVWXYZ"      %consonants 
a$[2]="AEIOU"                      %vowels

!dim word$[200]
!default settings 1=all, 2=vowe, 3=conso:
ARRAY.LOAD LTYPE[],3,2,3,2,3,2,3,2,3

GR.TEXT.ALIGN 2

! every #2 data button heights:
READ.DATA 40,80, 40,60, 40,60, 15,120, 15,60, 20,80
a=0
b=0
FOR j= 1 TO 6
 READ.NEXT a
 READ.NEXT c
 b+=a
 space[j]=b
 button[j]=c
 b+=c
NEXT j

krt=5 %rect border size
keretx=85
kerety=60
!gr.color 199,173,173,173,2 %szurke enyhen atlatszo

FOR j= 1 TO osn %oszlopok szama
 FOR n = 1 TO 6
  GR.COLOR 255,190,190,190,2 %szurke enyhen atlatszo elozo=199
  IF j>1 & j<>6 & n=1 THEN n=2

  IF n=1 & j=1 THEN
   x1=keretx+lma+((j-1)*(osz+gosz)) %szelesebb gomb kirajz.
   y1=kerety+tma+space[n]
   x2=x1+(5.4*gosz)
   y2=y1+button[n]

  ELSEIF n=1 & j=6 THEN
   x1=keretx+lma+((j-1)*(osz+gosz)) %szelesebb gomb kirajz.
   y1=kerety+tma+space[n]
   x2=x1+(5.4*gosz)
   y2=y1+button[n]
  ELSE
   !if j>1 & n=1 then n=2
   x1=keretx+lma+((j-1)*(osz+gosz))
   y1=kerety+tma+space[n]
   x2=x1+gosz
   y2=y1+button[n]
  ENDIF

  GR.RECT re,x1,y1,x2,y2
  ko[j,n,1]=x1
  ko[j,n,2]=y1
  ko[j,n,3]=x2
  ko[j,n,4]=y2

  
  GR.COLOR 255,255,255,255,2 %kereten beluli most feher
  GR.RECT re,x1+krt,y1+krt,x2-krt,y2-krt
  GR.COLOR 255,160,160,160,2
  GR.RECT re,x2-krt,y1+krt,x2,y2 % border shadow right
  GR.RECT re,x1,y2-krt,x2-krt,y2 % border shadow bottom
 
 NEXT n
NEXT j

GR.TEXT.SIZE 80
GR.COLOR 255,0,0,0,2
FOR j= 1 TO osn
 x1=keretx+lma+((j-1)*(osz+gosz))
 y1=kerety+tma+space[4]
 x2=x1+gosz
 y2=y1+button[4]
 GR.TEXT.DRAW re,x2-(gosz/2),y2-35," "
 IF j=1 THEN rect1=re
NEXT j

GR.TEXT.SIZE 22
FOR j= 1 TO osn %bcdf,lock,up,dn
 FOR n= 1 TO 6
  x1=keretx+lma+((j-1)*(osz+gosz))
  y1=kerety+tma+space[n]
  x2=x1+gosz
  y2=y1+button[n]
  IF j=1 & n=1 THEN gr.text.draw re,x1+(5*gosz/2),y2-30,"WORD LENGTH (4-9)"
  IF n=1 & j=6 THEN gr.text.draw re,x1+(6*gosz/2),y2-30," GENERATE "
  IF n=2 THEN gr.text.draw re,x2-(gosz/2),y2-20,LEFT$(a$[ltype[j]],5)
  IF n=3 THEN gr.text.draw re,x2-(gosz/2),y2-20,"UP"
  IF n=5 THEN gr.text.draw re,x2-(gosz/2),y2-20,"DOWN"
  IF n=6 THEN gr.text.draw re,x2-(gosz/2),y2-30,"LOCK"
  !if j=1 & n=1 then text1=re
  ko[j,n,5]=re
 NEXT n
NEXT j

generate:
FOR m= 1 TO 5
 word$=""
 FOR j= 1 TO hh
  x=ROUND(RND()*LEN(a$[ltype[j]]))
  IF lock[j]=1 THEN
   word$+=MID$(eword$,j,1)
  ELSE
   word$+=MID$(a$[LTYPE[j]],x,1)
  ENDIF
  GR.MODIFY rect1+j-1,"text",MID$(word$,j,1)
 NEXT j

 GR.RENDER
NEXT m

eword$=word$

start2:
DO
 GR.TOUCH touched,x,y
UNTIL touched

x/=(xx1)
y/=(yy1)

FOR j=1 TO 9
 FOR n=1 TO 6
  IF n=4 THEN n=5
  IF x>ko[j,n,1]-10 & y> ko[j,n,2]-10 & ~
   x<ko[j,n,3]+10 & y<(ko[j,n,4]+10)then

   IF n=1 THEN
    IF j=1 THEN %length
     IF hh<9 THEN
      hh+=1
      eword$=LEFT$(eword$+MID$(a$[ltype[hh]],2,1),hh)
     ELSE
      hh=4
      eword$=LEFT$(eword$,4)
     ENDIF

    ELSEIF j=6 THEN  %generate
     gen=1
    ENDIF

   ELSEIF n=2 THEN %abcde,bcdfg,aeiou
    LTYPE[j]+=1
    IF LTYPE[j]>3 THEN LTYPE[j]=1
    GR.MODIFY ko[j,n,5],"text",LEFT$(a$[LTYPE[j]],5)
    IF j<=hh THEN
     eword$=LEFT$(eword$,j-1)+MID$(a$[ltype[j]],2,1)+(MID$(eword$,j+1))
    ENDIF

   ELSEIF n=3 THEN      %up
    ya=LEN(a$[ltype[j]])
    FOR k= 1 TO ya
     IF MID$(eword$,j,1)=MID$(a$[ltype[j]],k,1)THEN
      k+=1
      IF k>ya THEN k=1
      eword$=LEFT$(eword$,j-1)+MID$(a$[ltype[j]],k,1)+(MID$(eword$,j+1))
     ENDIF
    NEXT k

   ELSEIF n=5 THEN   %down
    ya=LEN(a$[ltype[j]])
    FOR k= 1 TO ya
     IF MID$(eword$,j,1)=MID$(a$[ltype[j]],k,1)THEN
      k-=1
      IF k<1 THEN k=ya
      eword$=LEFT$(eword$,j-1)+MID$(a$[ltype[j]],k,1)+(MID$(eword$,j+1))
     ENDIF
    NEXT k

   ELSEIF n=6 THEN   %lock
    lock[j]=1-lock[j]
    IF lock[j]=1 THEN
     GR.MODIFY ko[j,n,5],"text","LOCKED"
    ELSE
     GR.MODIFY ko[j,n,5],"text","LOCK"
    ENDIF

   ENDIF
  ENDIF
 NEXT n
 GR.MODIFY rect1+j-1,"text",MID$(eword$,j,1)
NEXT j

IF gen=1 THEN
 gen=0
 GOTO generate
ENDIF

GR.RENDER
PAUSE 200
GOTO start2
