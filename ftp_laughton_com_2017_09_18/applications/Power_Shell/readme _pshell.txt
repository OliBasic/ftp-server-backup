This is a (kind of) customizable shell program.
It allows you to have a (kind of) customizable desktop 
& includes some basic utilities & a couple of simple games. 
Includes an ini file where you can make your changes.

[How to work with it]
You have a bar with the android icon. Pressing <android>
opens the menus, and you chose the program to run.
On your desktop there are 6 customizable icons: Press them
to open the relative programs. Some of them are internet shortcuts,
they will be opened in your browser.

[The menus]
Main menu is: RFO basic(forum) hints, Device Utilities,Games.

>>RFO basic(forum) hints will show you latest Basic! version, release date, users on line, total posts, total topics, newest member, total members, & the contents of the basic! ftp server.

>>Games menu includes 2 games, simplified craps & rock paper scissors.(every shell must include some game ;-) )

>>System utilities has 4 utilities: Sd card analyzer, System info, System tests, Record Calls
Sd card analyzer will tell you how many files & directories are present in your sd card how much space they occupy & optionally will list all files & directories.
System info: will show you info about your device, lan ip, wan ip, path to root, screen resolution, sensors, gps.
System tests: will do some simple tests  about multitouch capabilities, Vibration, speech synthesis, audio recording, & speed test. Speed test is the benchmarking f22 example program, for the graphics screen.
Record calls: Enter the program, choose type, then select "Enable Recording". An icon on the bar indicates that recording is on. Press home & your calls will be recorded. To disable, select "Disable recording" or exit whole app!

[The ini file]
There is a text file "rfo_ini.txt" in your data directory.There is one section for every icon on your desktop. Every section describes if the icon refers to an internal program or an external internet shortcut, the icon title, the icon file path, & the type of command. Open it in a text editor, there are more instructions on customizing it, and putting in your icons, there is also a blank icon you can use. You can even insert the code of your program to be executed (in the .bas file), with some restrictions, as i did with the f22 example program.

[Antonis]


