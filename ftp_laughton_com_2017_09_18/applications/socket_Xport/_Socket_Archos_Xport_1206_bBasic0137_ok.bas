!!
rev PF 06/12/2011
_Socket_Archos_Xport_1206.bas
RFO basic version 01.37 bBasic Ok
version 0138 Bad
ARchos43it hard A43-V5
Android 2.2.1 version 2.4.80
ecran 854x480

organisation de stockage des objets et images:
 radiobutton 20...26 	img 1,2
 leds1 30...37 		img 5,6   	8 entrees
 leds2 40...47     	img 3,4   	8 entrees
 lumiere 1...6 		img 7,8 	8 sorties
 chauffage 10 ...15 	img 9,10	8 sorties
 validation             img 11
 voir graf 16           img 12

 A1 a A7  	mesure numerique ana en points
 V1 a V7 	mesures en unites physiques UPH
 V11 a V71 	Mesures en point pour grafique
 Tc		objet ecriture Cde eclairage
 Te		objet ecriture Cde Chauffage
 Tr		Trame recue

!!
!#02/12/2011 20H30M40S;A1=0111;A2=0222;A3=0333;A4=0444;A5=0555;A6=0666;A7=0777;A8=0888;
!L1=01010101;L2=11110000;E=12345678;C=12345678;&

debug.on
debug.print 10

dim Bm[30]  % Bitmaps
dim ArrPlan[2]
dim Pixels1[400]
dim Pixels2[400]
dim Pixels3[400]
dim Pixels4[400]
dim Pixels5[400]
!dim Pixels6[400]
!dim Pixels7[400]
dim rec_Array$[14]

dim ObjFixe[2]
dim Obj[50]
dim ObjStatus[50]
dim RE[8]	% etat des retours de cde Eclairage
dim RC[8]	% etat des retours de cde Chauffage
dim mT$[8]	% mesures de Temperatures en ascii

EcrE$="00000000"  % Cde Eclairage
EcrC$="00000000"	% Cde Chauffage
RE$=""		% Retour Trame etat Eclairage
RC$=""		% Retour Trame etat Chauffage
Lec1$="00000000"	% Etat de 8 entrees
Lec2$="00000000"  % etat des 8 entrees suivantes
debug.print 15

Prog$="_Socket_Archos_Xport_1206"
NbMax=50 	% normal =1800
Nb1=1
 test=0   	% 1=simulation reception sockett
tSend=1  	% 1 autorise l'envoi de data au muController

print "Version basic : "; Version$()
Device DV$
print "Device Info : "; DV$

!input "Mode Test <O,N>",Q1$,"Oui"
!if left$(Q1$,1)="N" then
!test=0
! else
 !test=1
!end if
print "test=";test
! init neceessaire ?
for i=1 to 40
ObjStatus[i]=0
next i

print "Usage de Socket RFO Basic"
if test=0 then
  print "associe a 18F252_NokiaV9_Xport_archos_111203.c"
else
 print "socket simulee"
end if

debug.print 20
!gr.open alpha, red, green, blue {, ShowStatusBar}
gr.open 255,0,0,0,0   %   1 => status barre en haut de l'ecran
gr.orientation 0

! Recupere les parametres de l'ecran w=largeur et h=hauteur
gr.screen w,h

debug.print 30
gr.bitmap.load  ArrPlan[1],"Arrplan_b.jpg" 	% Image de fond d'ecran FIXE sans trendplot
!gr.bitmap.load  ArrPlan[2],"Arrplan_recorder_b.jpg" % zone trend plot
gr.bitmap.load bm[1],"RadioBOff.jpg"
gr.bitmap.load bm[2],"RadioBOn.jpg"
gr.bitmap.load bm[3],"LedR_off.jpg"
gr.bitmap.load bm[4],"LedR_on.jpg"
gr.bitmap.load bm[5],"LedV_off.jpg"
gr.bitmap.load bm[6],"LedV_on.jpg"
gr.bitmap.load bm[7],"Ampoule_OFF_.jpg"
gr.bitmap.load bm[8],"Ampoule_ON_.jpg"
gr.bitmap.load bm[9],"Radiat_off_H.jpg"
gr.bitmap.load bm[10],"Radiat_on_H.jpg"
gr.bitmap.load bm[11],"homes.png"
!gr.bitmap.load bm[12],"Next.jpg"

debug.print "40  chargement des images"

gr.bitmap.draw ObjFixe[1],Arrplan[1],0,0
!gr.bitmap.draw ObjFixe[2],Arrplan[2],360,20
gr.bitmap.draw Obj[29],Bm[11],520,420
!gr.bitmap.draw Obj[30],Bm[12],760,420
gr.text.draw Te,625,365,EcrE$
gr.text.draw Tc,625,395,EcrC$

debug.print 50
goto strap0

! Trace des  Axes en Vert/Green
! 0,0 en haut a gauche
gr.color 255,0,255,0,1  % vert
gr.set.stroke 2
gr.line axeH, 360, 20, 616, 20
gr.line axeV, 360, 20, 360, 420
gr.set.stroke 1

ObjStatus[11]=1
gr.bitmap.draw Obj[11],bm[2],260,40  % active par defaut

strap0:
gr.hide Obj[29]
gr.render
debug.print 60

ip$="192.168.0.105"  % test reels avec Xport  platine  PIC18F252

!ip$="192.168.0.100" % simul avec Vbray sur PC

port=10001
print "Xport at ";ip$;" Port:";format$(" #####",port)

! Connect to the Xport Client
if test=0 then
 Socket.client.connect ip$,port
 print "Connected to ";ip$
 ! Wait for Client to send a message
 ! or time out after 4 seconds
 debug.print 70
 flag=0
 maxclock = clock() + 4000
  ! data must be received within 4 sec
 do
  socket.client.read.ready  flag
  if clock() > maxclock then
   print "init socket Read time out"
   goto arreter
  end if
 until flag
 print " "
 print "flag=";flag
 ! Message recu
 socket.client.read.line recept$
 else
 debug.print 71

recept$=""
 recept$="#27/11/22 12:37:45;A1=0320;A2=0512;A3=0768;A4=0384;A5=0600;A6=1204;"
recept$=recept$+ "A7=3192;A8=4000;L1=00001010;L2=00001111;E=00100010;C=00110001;&"
 debug.print 72
end if   % test=0

debug.print recept$
debug.print 73


pause 50

! ======= debut boucle principale ====
j1=0
Dt=0
Nb1=1

DO
debug.print "75 main loop"
Nclock=Clock()
! normal sending from XPORT is every 3 secondes, so test with 4sec
if test=0 then
maxclock=clock()+4000
flag=0
do
socket.client.read.ready flag
 if clock() > maxclock then
  print "Timeout sur reception trame"
  goto arreter
 endif
until flag
debug.print 80
socket.client.read.line recept$
else
pause 2000
end if % test=0

k=len(recept$)
print "Lg Tr=";k
debug.print "recu : ";recept$


! decomposition des elements
!split rec_Array$[],recept$,";"
!array.length longueur, rec_Array$[]
gosub My_Split
debug.print "nb elem rec_Array$ : ";longueur

debug.print 83

for i=1 to longueur
debug.print i,rec_Array$[i]
next i

debug.print 90

Horodat$=right$(rec_Array$[1],17)
gr.color 255,255,255,255,1   % blanc
gr.text.draw Ho,20,30,Horodat$
debug.print 95

! affichage valeur indice
gr.text.align 1
gr.text.size h/24
gr.color 255,255,0,0,1   % rouge
ind$=format$("#####",j1)
gr.text.draw P0,280,30,ind$
debug.print 100

!gr.color alpha, red, green, blue, fill
gr.color 255,160,160,50,1
! Set the text align to Left = 1 center=2 right=3
gr.text.align 1

!extraction valeur de Entree Analogique A1
!A1=512
ch$=mid$(rec_Array$[2],4,4)
debug.print ch$
! TExt  -10  a +50C =>60degC pour 1024 points
A1=val(ch$)
if test=1 then A1=500+Rnd(0)*Nb1/20
V1=A1*60/1023
V1=V1-10  % offset -10degC
mT$[1]=format$(" ##.##",V1)
!affichage valeur
gr.color 200,100,255,255,1
gr.text.draw P1, 250,85, mt$[1]
V11=A1*0.25+360	% valeur axe Y1 pour grafique
debug.print 110

!extraction valeur de Entree Analogique A2
ch$=mid$(rec_Array$[3],4,4)
debug.print ch$
! Tamb echelle 50 C pour 1024 pts
A2=val(ch$)
if test=1 then  A2=480-rnd(0)*8;
V2=A2*50/1024
gr.color 200,255,255,255,1
mt$[2]=format$("##.##",V2)
gr.text.draw P2, 250,145, mt$[2]
V21=A2*0.25+360
debug.print 120

!extration valeur de Entree Analogique A3
!A3=256
ch$=mid$(rec_Array$[4],4,4)
debug.print ch$
A3=val(ch$)
if test=1 then  A3=384+Rnd(0)*5.0
V3=A3*50/1024
mt$[3]=format$("##.##",V3)
gr.text.draw P3, 250,205, mt$[3]
V31=A3*0.25+360
debug.print 130

!extration valeur de Entree Analogique A4
ch$=mid$(rec_Array$[5],4,4)
debug.print ch$
A4=val(ch$)
if test=1 then A4=600+Nb1/10
V4=A4*50/1024
mt$[4]=format$("##.##",V4)
gr.text.draw P4, 250,265, mt$[4]
V41=A4*0.25+360
debug.print 140

!extration valeur de Entree Analogique A5
ch$=mid$(rec_Array$[6],4,4)
debug.print ch$
A5=val(ch$)
if test=1 then A5=700+rnd(0)*5.0-Nb1/20
V5=A5*50/1024
mt$[5]=format$("##.##",V5)
gr.text.draw P5, 250,325, mt$[5]
V51=A5*0.25+360
debug.print 150

!extration valeur de Entree Analogique A6
ch$=mid$(rec_Array$[7],4,4)
debug.print ch$
A6=val(ch$)
if test=1 then A6=700+rnd(0)*5.0-Nb1/20
V6=A6*50/4096
mt$[6]=format$("##.##",V6)
gr.text.draw P6, 250,385, mt$[5]
V61=A6*0.25+360
debug.print 150

!extration valeur de Entree Analogique A7
ch$=mid$(rec_Array$[8],4,4)
debug.print ch$
A7=val(ch$)
if test=1 then A7=700+rnd(0)*5.0-Nb1/20
V7=A7*50/4096
mt$[7]=format$("##.##",V7)
gr.text.draw P7, 250,445, mt$[7]
V71=A7*0.25+360
debug.print 150

!extration valeur de Entree Analogique A8
ch$=mid$(rec_Array$[9],4,4)
debug.print ch$
A8=val(ch$)
if test=1 then A8=800+rnd(0)*5.0+Nb1/30
V8=A8*50/4096
mt$[8]=format$("##.##",V8)
!gr.text.draw P8, 250,35, mt$[8]
V81=A8*0.25+360
debug.print 155



! extraction valeur des Entrees  Lues "L1"
! et rafraichissement etat des leds
Lec1$="00000000"
Lec1$=mid$(rec_Array$[10],4,8)
for i=0 to 7
 if (mid$(Lec1$,i+1,1)="1" then
	gr.bitmap.draw Obj[30+i],bm[4],540,25+i*40  %led rouge on
	else
	gr.bitmap.draw Obj[30+i],bm[3],540,25+i*40  % led verte off
end if
next i
debug.print "160 Lec1$=";Lec1$
!
! extraction valeur des Entrees  Lues "L2"
! et rafraichissement etat des leds
Lec2$="00000000"
Lec2$=mid$(rec_Array$[11],4,8)
for i=0 to 7
 if (mid$(Lec2$,i+1,1)="1" then
	gr.bitmap.draw Obj[40+i],bm[6],740,25+i*40  %led verte on
	else
	gr.bitmap.draw Obj[40+i],bm[5],740,25+i*40  % led verte off
end if
next i
debug.print "165 Lec2$=";Lec2$
gr.text.size h/32

debug.print 170

! extraction valeur des Retours de commande "E"
! et rafraichissement etat des leds
RE$="00000000"
RE$=mid$(rec_Array$[12],3,8)
debug.print "RE$=";RE$
for i=0 to 7
 if (mid$(RE$,i+1,1)="1" then
	RE[i+1]=1
	else
	RE[i+1]=0
end if
next i
debug.print 180

! Zones Eclairages
EcrE$=""  % ecriture de 8 bits
for i=0 to 5
 t1=0
 dy=60*i
 gr.bounded.touch t1, 125,65+dy,155,95+dy
 if t1=1 then
  debug.print "zone lumiere touchee ",ObjStatus[1+i]
  Tone 600,200
  ObjStatus[i+1]=!ObjStatus[i+1]
  gr.bitmap.draw Obj[i+1],bm[7+ObjStatus[i+1]],120,60+dy
 end if
  if  ObjStatus[i+1]=1 then
    a$="1"+EcrE$
   else
    a$="0"+EcrE$
  end if
EcrE$=a$
next i
EcrE$="E00"+a$
gr.hide Te
gr.color 255,0,240,0,1
gr.text.draw Te,625,365,RE$
debug.print 200


! extraction valeur des Retours de commande "C"
RC$="00000000"
RC$=mid$(rec_Array$[13],3,8)
debug.print "RC$=";RC$
for i=0 to 7
 if (mid$(RC$,i+1,1)="1" then
	RC[i+1]=1
	else
	RC[i+1]=0
end if
next i
debug.print 205


!zones Chauffages
EcrC$=""  % ecriture de 8 bits
dy=0
for i=0 to 4
 t2=0
 dy=i*60
  gr.bounded.touch t2, 185,125+dy,235,155+dy
  if t2 =1 then
   debug.print "zone radiateur touchee = ",ObjStatus[10+i]
   ObjStatus[10+i]=!ObjStatus[10+i]
   Tone 440,200
   gr.bitmap.draw Obj[10+i],bm[9+ObjStatus[10+i]],180,120+dy
  end if    % t2=1
  if  ObjStatus[i+10]=1 then
    a$="1"+EcrC$
    else
    a$="0"+EcrC$
  end if
  EcrC$=a$
next i
EcrC$="C000"+a$
gr.hide Tc
gr.color 255,255,255,0,1
gr.text.draw Tc,625,395,RC$



!zone Radiobutton

for i=0 to 6
t2=0
dy=i*60
gr.bounded.touch t2,345,65+dy,375,90+dy
if t2 =1 then
gosub RazButton
   debug.print "zone Radiobutton touchee = ",i
ObjStatus[20+i]=!ObjStatus[20+i]
Tone 440,200
end if
gr.bitmap.draw Obj[20+i],bm[1+ObjStatus[20+i]],340,60+dy
next i
debug.print 220

goto strap1

! Trace des 2 valeurs et mise a jour mesure numerique
! Voie #1 temp exterieure
gr.color 255,200,20,200,0	% violet
gr.set.pixels Trend5,pixels5[],V11,Nb1+21
! voie #2 par selection
k=2  % par defaut
if ObjStatus[11]=1 then
gr.color 255,0,100,200,0 %bleu clair
gr.set.pixels Trend1,pixels2[],V21,Nb1+21
k=2
end if
if  ObjStatus[13]=1 then
gr.color 255,0,0,255,0	%bleu
gr.set.pixels Trend2,pixels2[],V31,Nb1+21
k=3
end if
if ObjStatus[14]=1 then
gr.color 255,255,0,0,0	%rouge
gr.set.pixels Trend3,pixels2[],V41,Nb1+21
k=4
end if
if ObjStatus[15]=1 then
gr.color 255,0,255,0,0	%vert
gr.set.pixels Trend4,pixels2[],V51,Nb1+21
k=5
end if
debug.print 235

! valeur numerique selectionnee
gr.hide P2
gr.text.size h/16
gr.text.draw P2,685,415,mt$[k]
debug.print 240

strap1:



! test si validation envoi modif
if ((test=0) & (tSend=1)) then
 t3=0
 gr.bounded.touch t3,525,425,555,450
 if (t3=1) then
   debug.print  "225 zone Validation touchee"
    gr.show Obj[29]
    Tone 1000,200
   EE$=EcrE$+"X"+EcrC$+chr$(3328) %13*256
   Socket.client.write.line EE$
   pause 500
   print "230 Socket write ";EE$
 else
  gr.hide Obj[29]
end if
end if


Nb1=Nb1+1
if Nb1=NbMax-1 then gr.save Prog$+".png"

!if Nb1 >= NbMax then
! Nb1=1
! gr.hide ObjFixe[2]
! gr.bitmap.draw ObjFixe[2],Arrplan[2],360,21
!end if



gr.render

debug.print 250
!duree de la boucle
Dt =Dt+clock()-Nclock
! fin de grafique => capture

j1=j1+1

! efface certaines zones et recommence
gr.hide P0  % indice
gr.hide P1  % Ana1
gr.hide P2
gr.hide P3
gr.hide P4
gr.hide P5
gr.hide P6
gr.hide P7  % Ana7
!gr.hide P8
gr.hide Ho  % date heure

!unDim rec_Array$[]
debug.print 300
! termine apres  1800*2=3600 secondes, soit 1H00
until (j1>NbMax)

! ---------  fin de boucle principale  -----
!
popup "fin normale..",250,250,2000
pause 2000
gr.close

socket.client.close
print "deconnection"
Tone 440,1000
Tone 800,1000
Tone 600,1000

pause 2500
ici:
print " "
print "_socket_xport_"+Prog$+".bas"
!Attention ne pas utiliser PRINT tout seul
print "version basic : ";VERSION$()
print "Resolution Ecran "
print "Larg = ";w
print "Haut = ";h
print "Temps ecoule (en heure)depuis le dernier demarrage: ";format$("##.###",clock()/3600000)
print "j1=";j1-1
print "Dt 1 boucle =";Dt/NbMax;" mS"
!browse "http://paulfjujo.free.fr"
arreter:
socket.client.close
pause 2000
Console.save Prog$+".txt"
end
OnError:
debug.print "sortie sur Erreur"
Console.save "Err_"+Prog$+".txt"
pause 2000
end

RazButton:
debug.print "Subroutine Radio Buttons off"
for jj=0 to 6
ObjStatus[20+jj]=0
next jj
return

Octet2Binaire_:
E1$=""
k1=j1 & 255
for kk=0 to 7
if (k1 && 2^(7-kk)) >0 then
E1$=E1$+"1"
else
E1$=E1$+"0"
end if
next kk
debug.print "E1$=";E1$
return

My_Split:
J3=1
L3=1
longueur=0
do
M3= Is_In(";",recept$,L3)
K3=len(recept$)
if M3>0 then
 rec_Array$[J3]=mid$(recept$,L3,M3)
 longueur=longueur + 1
 recept$=right$(recept$,K3-M3)
J3=J3+1
 end if
until M3=0
return









































 
 
 
 
