!!
rev PF 17/11/2011
RFO basic version 01.36
ARchos43it hard A43-V5
Android 2.2.1 version 2.4.80

organisation de stockage des objets et images:
 lumiere 1...5 		img 7,8
 chauffage 6 ...10 	img 9,10
 radiobutton 1...15 	img 1,2
 leds 20...27 		img 5,6

!!

debug.on
dim Bm[30]  % Bitmaps
dim ArrPlan[2]
dim Pixels1[400]
dim Pixels2[400]
dim Pixels3[400]-
dim Pixels4[400]
dim Pixels5[400]
dim ObjFixe[2]
dim Obj[40]
dim ObjStatus[40]
dim RT[8]	% etat des retours de cde
dim mT$[5]	% mesures de Temperatures en ascii

!A1 a A5  	mesure numerique ana en points
!V1 a V5 	mesures en deg C
!V11 a V51 	Mesures en point pour grafique
!Tc		objet ecriture Cde eclairage
!Te		objet ecriture Cde Chauffage
!Tr		Trame recue
EcrE$="00000000"   	% Cde Eclairage
EcrC$="00000000"	% Cde Chauffage
Lec1$="00000000"	% Lecture des  entrees 1 a 8
Lec2$="00000000"    % Lecture des entrees 9 a 16

J1=0
KK=0
K1=0

Prog$="111117_R"

debug.print 10
NbMax=500
Nb=1
test=0    % 1=simulation reception sockett
toSend=0  % 1 autorise l'envoi de data au �Controller


! init neceessaire ?
for i=1 to 40
ObjStatus[i]=0
next i

msg$="Usage de Socket RFO Basic"
print msg$

msg$="associe a Platine_16F877_nokia_RS_2011.asm"
print msg$



debug.print 20
!gr.open alpha, red, green, blue {, ShowStatusBar}
gr.open 255,0,0,0,0   %   1 => status barre en haut de l'ecran
gr.orientation 0

! Recupere les parametres de l'ecran w=largeur et h=hauteur
gr.screen w,h

debug.print 30
gr.bitmap.load  ArrPlan[1],"Arrplan.jpg" 	% Image de fond d'ecran FIXE
gr.bitmap.load  ArrPlan[2],"Arrplan_recorder.jpg" % zone trend plot
gr.bitmap.load bm[1],"RadioBOff.jpg"   	% Radio bouton etat OFF
gr.bitmap.load bm[2],"RadioBOn.jpg"		% Radio Bouton = ON
gr.bitmap.load bm[3],"LedR_off.jpg"		% Led Rouge etat OFF
gr.bitmap.load bm[4],"LedR_on.jpg"
gr.bitmap.load bm[5],"LedV_off.jpg"
gr.bitmap.load bm[6],"LedV_on.jpg"
gr.bitmap.load bm[7],"Ampoule_OFF_.jpg"
gr.bitmap.load bm[8],"Ampoule_ON_.jpg"
gr.bitmap.load bm[9],"Radiat_off_H.jpg"
gr.bitmap.load bm[10],"Radiat_on_H.jpg"
gr.bitmap.load bm[11],"homes.png"

debug.print 40

gr.bitmap.draw ObjFixe[1],Arrplan[1],0,0
gr.bitmap.draw ObjFixe[2],Arrplan[2],360,20

gr.text.draw Tc,710,450,EcrC$
gr.text.draw Te,615,450,EcrE$

!debug.on
debug.print 50

! Trace des  Axes en Vert/Green
! 0,0 en haut a gauche
gr.color 255,0,255,0,1  % vert
gr.set.stroke 2
gr.line axeH, 360, 20, 616, 20
gr.line axeV, 360, 20, 360, 420
gr.set.stroke 1

ObjStatus[11]=1
gr.bitmap.draw Obj[11],bm[2],260,40  % actif par defaut

gr.render
debug.print 60
!=============================

ip$="192.168.0.105"  % test reels avec Xport
!ip$="192.168.0.100"   % simul avec Vbray sur PC
port=10001
print "Xport at ";ip$;" Port:";format$(" #####",port)


! ======= debut boucle principale ====
j1=0
Dt=0
Nb=1

DO
Nclock=Clock()


! Connect to the Xport Client
if test=0 then
 socket.client.connect ip$,port
 print "Connected to ";ip$
recept$=""
else
 recept$="#11/11/06 12:37:45;A1=0512;A2=0768;A3=384;A4=600;A5=700;L=00001010;R=00100010;&"
debug.print "test ";recept$
pause 2000
end if

! normal sending from XPORT is every 2 secondes, so test with 4sec
if test=0 then
maxclock=clock()+4000
flag=0
while (flag=0)
socket.client.read.ready flag
 if clock() > maxclock then
  print "Timeout sur reception trame"
  goto arreter
 endif
repeat
debug.print 80
pause 500 
socket.client.read.line recept$
else
pause 2000
end if % test=0
split rec_Array$[],recept$,";"
array.length longueur, rec_Array$[]
debug.print "longueur : ";longueur
debug.print "recu :"; recept$


! affichage Trame pour debugging
gr.text.align 1
gr.text.size h/32
gr.color 255,255,255,255,1   % blanc
k=len(recept$)
debug.print "Lg Tr=";k
gr.text.draw Tr,10,450,mid$(recept$,len(rec_Array$[1])+2,k)

debug.print 90

!for i= 1 to longueur
!print rec_Array$[i]
!next i

Horodat$=right$(rec_Array$[1],18)
gr.color 255,255,255,255,1   % blanc
gr.text.draw Ho,110,17,Horodat$
debug.print 95
! affichage valeur indice
gr.text.align 1
gr.text.size h/32
gr.color 255,255,0,0,1   % rouge
ind$=format$("#####",j1)
gr.text.draw P0,45,15,ind$
debug.print 100

!gr.color alpha, red, green, blue, fill
gr.color 200,160,160,50,1
! Set the text align to Left = 1 center=2 right=3
gr.text.align 1
gr.text.size h/16

!extraction valeur de Entree Analogique A1
!A1=512
ch$=mid$(rec_Array$[2],4,4)
debug.print ch$
! TExt  -10  a +50C =>60degC pour 1024 points
A1=val(ch$)
if test=1 then A1=500+Rnd(0)*250
V1=A1*60/1023
V1=V1-10  % offset -10degC
mT$[1]=format$(" ##.##",V1)
!affichage valeur
gr.color 200,200,20,200,1
gr.text.draw P1, 245,155, mt$[1]
V11=A1*0.25+360	% valeur axe Y1 pour grafique
debug.print 110

!extraction valeur de Entree Analogique A2
ch$=mid$(rec_Array$[3],4,4)
debug.print ch$
! Tamb echelle 50 C pour 1024 pts
A2=val(ch$)
if test=1 then  A2=480-rnd(0)*j1;
V2=A2*50/1024
mt$[2]=format$("##.##",V2)
V21=A2*0.25+360
debug.print 120

!extration valeur de Entree Analogique A3
!A3=256
ch$=mid$(rec_Array$[4],4,4)
debug.print ch$
A3=val(ch$)
if test=1 then  A3=384+Rnd(0)*30.0
V3=A3*50/1024
mt$[3]=format$("##.##",V3)
V31=A3*0.25+360
debug.print 130

!extration valeur de Entree Analogique A4
!A4=256
ch$=mid$(rec_Array$[5],4,4)
debug.print ch$
A4=val(ch$)
if test=1 then A4=600+rnd(0)*15.0
V4=A4*50/1024
mt$[4]=format$("##.##",V4)
V41=A4*0.25+360
debug.print 140

!extration valeur de Entree Analogique A5
!A5=256
ch$=mid$(rec_Array$[6],4,4)
debug.print ch$
A5=val(ch$)
if test=1 then A5=700+rnd(0)*45.0
V5=A5*50/1024
mt$[5]=format$("##.##",V5)
V51=A5*0.25+360
debug.print 150

! extraction valeur des Entrees  Lues "L"
! et rafraichissement etat des leds
Lec1$="00000000"
Lec1$=mid$(rec_Array$[7],3,8)
for i=0 to 7
 if (mid$(Lec1$,i+1,1)="1" then
	gr.bitmap.draw Obj[20+i],bm[4],740,60+i*40  %led rouge on
	else
	gr.bitmap.draw Obj[20+i],bm[5],740,60+i*40  % led verte off
end if
next i
debug.print "160 Lec1$=";Lec1$

gr.text.size h/32

!gr.render
debug.print 170

! extraction valeur des Retours de commande "R"
! et rafraichissement etat des leds
R1$="00000000"
R1$=mid$(rec_Array$[8],3,8)
debug.print "R=";R1$
for i=0 to 7
 if (mid$(R1$,i+1,1)="1" then
	RT[i+1]=1
	else
	RT[i+1]=0
end if
next i
debug.print 180

! Zones Eclairages
EcrE$=""  % ecriture de 8 bits
for i=0 to 4
 t1=0
 dy=80*i
 gr.bounded.touch t1, 130,45+dy,150,75+dy
 if t1=1 then
  debug.print "zone lumiere touchee ",ObjStatus[1+i]
  Tone 600,200
  ObjStatus[i+1]=!ObjStatus[i+1]
  gr.bitmap.draw Obj[i+1],bm[7+ObjStatus[i+1]],120,40+dy
 end if
  if  ObjStatus[i+1]=1 then
    a$="1"+EcrE$
   else
    a$="0"+EcrE$
  end if
EcrE$=a$
next i
EcrE$="E000"+a$
gr.hide Te
gr.color 255,255,0,255,1
gr.text.draw Te,615,450,EcrE$
debug.print 190

!zones Chauffages
EcrC$=""  % ecriture de 8 bits
dy=0
for i=0 to 4
 t2=0
 dy=i*80
 if i<>1 then
  gr.bounded.touch t2, 190,45+dy,230,75+dy
  if t2 =1 then
   debug.print "zone radiateur touchee = ",ObjStatus[6+i]
   ObjStatus[6+i]=!ObjStatus[6+i]
   Tone 440,200
   gr.bitmap.draw Obj[6+i],bm[9+ObjStatus[6+i]],180,40+dy
  end if    % t2=1
  if  ObjStatus[i+6]=1 then
    a$="1"+EcrC$
    else
    a$="0"+EcrC$
  end if
  else
	A$="0"+EcrC$
 end if  % i=1
 EcrC$=a$
next i
EcrC$="C000"+a$
gr.hide Tc
gr.color 255,255,255,0,1
gr.text.draw Tc,710,450,EcrC$


! test si validation envoi modif
if ((Test=0)& (toSend=1)) then
 t3=0
 gr.bounded.touch t2,742,19,771,43
 if t3 =1 then
   debug.print "zone Validation touchee"
   ObjStatus[30]=1
   gr.bitmap.draw Obj[30],bm[11],760,30
   Socket.client.write.line EcrC$
    pause 10
   debug.print "210 Socket write Eclairage";EcrE$
 else
   gr.hide Obj[30]
 end if
end if

socket.client.close

!zone Radiobutton
for i=0 to 4
t2=0
dy=i*80
gr.bounded.touch t2,270,45+dy,290,75+dy
if t2 =1 then
   debug.print "zone Radiobutton touchee = ",ObjStatus[11+i]
gosub RazButton
ObjStatus[11+i]=!ObjStatus[11+i]
Tone 440,200
gr.bitmap.draw Obj[11+i],bm[1+ObjStatus[11+i]],260,40+dy
end if
next i
debug.print 220

! Trace des 2 valeurs et mise � jour mesure numerique
! Voie #1 temp exterieure
gr.color 255,200,20,200,0	% violet
gr.set.pixels Trend5,pixels5[],V11,Nb+21
! voie #2 par selection
k=2  % par defaut
if ObjStatus[11]=1 then
gr.color 255,0,100,200,0 %bleu clair
gr.set.pixels Trend1,pixels2[],V21,Nb+21
k=2
end if
if  ObjStatus[13]=1 then
gr.color 255,0,0,255,0	%bleu
gr.set.pixels Trend2,pixels2[],V31,Nb+21
k=3
end if
if ObjStatus[14]=1 then
gr.color 255,255,0,0,0	%rouge
gr.set.pixels Trend3,pixels2[],V41,Nb+21
k=4
end if
if ObjStatus[15]=1 then
gr.color 255,0,255,0,0	%vert
gr.set.pixels Trend4,pixels2[],V51,Nb+21
k=5
end if

! valeur numerique selectionnee
gr.hide P2
gr.text.size h/16
gr.text.draw P2,685,415,mt$[k]


Nb=Nb+1
if Nb>4 then     % for debugging
!if Nb>400 then
gr.save Prog$+".png"
 Nb=1
 gr.hide ObjFixe[2]
 gr.bitmap.draw ObjFixe[2],Arrplan[2],360,21
end if
gr.render

!duree de la boucle
Dt =Dt+clock()-Nclock
! fin de grafique => capture

j1=j1+1

! efface certaines zones et recommence
gr.hide P0
gr.hide P1
gr.hide Ho
gr.hide Tr

unDim rec_Array$[]
debug.print 250
! termine apres  1200*3=3600 secondes, soit 1H00
until (j1>5)    % debugging
!until (j1>1200)
print "j1=";j1

! ---------  fin de boucle principale  -----
!
popup "fin normale..",250,250,2000
pause 2000
gr.close

socket.client.close
print "deconnection"
Tone 440,1000
Tone 800,1000
Tone 600,1000

pause 2500
ici:
print " "
print "_socket_xport-"+Prog$+".bas"
!Attention ne pas utiliser PRINT tout seul
print "version basic : ";VERSION$()
print "Resolution Ecran "
print "Larg = ";w
print "Haut = ";h
print "Temps ecoule (en heure)depuis le dernier demarrage: ";format$("##.###",clock()/3600000)
print "j1=";j1;" Nb1=";Nb1
print "Dt 1 boucle =";Dt/NbMax;" mS"
!browse "http://paulfjujo.free.fr"
arreter:
socket.client.close
pause 2000
Console.save Prog$+".txt"
end
OnError:
debug.print "sortie sur Erreur"
Console.save "error_"+Prog$+".txt"
pause 2000
end

RazButton:
debug.print Subroutine
 !Radio Buttons off
for jj=0 to 4
if jj<>1 then gr.bitmap.draw Obj[11+jj],bm[1],260,40+80*jj
ObjStatus[11+jj]=0
next jj
return

Octet2Binaire:
E1$=""
k1=j1 & 255
for kk=0 to 7
if (k1 && 2^(7-kk)) >0 then
E1$=E1$+"1"
else
E1$=E1$+"0"
end if
next kk
debug.print "E1$=";E1$
return























 
