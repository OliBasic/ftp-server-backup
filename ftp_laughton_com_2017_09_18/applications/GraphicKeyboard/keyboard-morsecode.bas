! morse code and keyboard
array.load slett1[],33,64,35,36,44,46,38,42,40,41  % shifted 1-9-0
array.load lett1[],49,50,51,52,53,54,55,56,57,48   % 1-9-0
array.load lett2[],113,119,101,114,116,121,117,105,111,112  %qwertyuiop
array.load lett3[],97,115,100,102,103,104,106,107,108   %asdfghjkl
array.load lett4[],122,120,99,118,98,110,109   %zxcvbnm
array.load lett5$[],"<",">","shift"," " % remove after cursor added

array.load dits1$[],"-----",".----","..---","...--","....-",".....","-....","--...","---..","----.",".-.-.-"
array.load dits2$[],"--..",".--",".",".-.","-","-.--","..-","..","---",".--."
array.load dits3$[],".-","...","-..","..-.","--.","....",".---","-.-",".-.."
array.load dits4$[],"--..","-..-","-.-.","..._","-...","-.","--"

! Open Graphics
gr.open 255,0,0,0 
gr.orientation 0
gr.color 255,0,0,0,1
gr.text.size 30
gr.text.bold 1 

!Load images
gr.bitmap.load background, "84tgram.jpg"
gr.bitmap.draw b1pi, background,  1, 1 
gr.bitmap.load keyboard, "keyb4.jpg" 
gr.bitmap.draw b2pi,keyboard ,1,320

gr.render
shift=1   % is shift key on?
ovr=40    % next char
eline = 800 - ovr    % when to carriage return
row = 160 %first row to type
start:
do
   gr.touch flag, x , y 
top = 320         % keyboard graphic is 800 x 160     320+160 = 480 
 if ovr >= eline % check if return 
    row = row+50
       ovr = 40 
       endif
if y > 320 & y < 480
if y< 40+top & flag
x2 =ceil(x / 80)
! 1-0 keys 
if shift<1
gr.text.draw txt1,ovr,row,chr$(slett1[x2])
else
 gr.text.draw txt1, ovr,row, chr$(lett1[x2])
 gr.text.draw txt2, ovr,row+20, dits1$[x2] 
 dd$= dits1$[x2] 
 gosub ditdah
 endif 
 ovr=ovr+50
else if y<  80+top & flag
x2 =ceil(x / 80 )
!q-p keys
if shift <1
gr.text.draw txt1, ovr,row, chr$(lett2[x2]-32)
else
 gr.text.draw txt1, ovr,row, chr$(lett2[x2])
endif
 gr.text.draw txt2, ovr,row+20, dits2$[x2]
 ovr=ovr+50
     dd$= dits2$[x2]
     gosub ditdah           
else if y< 120+top & flag
if x > 35 & x < 765   % for arrow keys
x2 =ceil(x / 88)
!a-l keys
if shift <1
gr.text.draw txt1, ovr,row, chr$(lett3[x2]-32)
else
 gr.text.draw txt1, ovr,row, chr$(lett3[x2])
endif
 gr.text.draw txt2, ovr,row+20, dits3$[x2]
 ovr=ovr+50
     dd$= dits3$[x2]
     gosub ditdah
end if
if x >0 & x< 35 then print lett5$[1] % left arrow
if x >765 & x< 800 then print lett5$[2] % right arrow
else if y< 160+top
if x >80 & x<650 & flag
  x2 =ceil(x / 80 )-1
  !z-m keys
if shift <1
gr.text.draw txt1, ovr,row, chr$(lett4[x2]-32)
else
 gr.text.draw txt1, ovr,row, chr$(lett4[x2])
endif
 gr.text.draw txt2, ovr,row+20, dits4$[x2]
 ovr=ovr+50
 dd$= dits4$[x2]
     gosub ditdah 
  end if
  if x >0 & x< 80 & y>440 &flag 
  shift=shift *-1% shift
if  shift<1
gr.color 255,255,0,0,1
else
gr.color 255,0,0,0,1
endif
gr.text.draw sft,70,460,"•"
gr.color 255,0,0,0,1
gr.render
endif
  
  if x >650 & x< 800 & y>440 & flag then ovr=ovr+20 % space

  endif 
  endif
x2=0
gr.render
 until flag %& x>1 & y>1
 flag=0
goto start

ditdah:  %tones for morse code
gr.render
xx=len(dd$)
for t = 1to xx
if mid$(dd$,t,1) ="." then tone 800,400
if mid$(dd$,t,1) ="-" then tone 800,1000
next
return


!!
any code having to do with
dits x$,dd$,ditdah
has nothing to do with  keyboard

lett5$ only for cursor movement 
not needed once a flashing cursor added
!!
