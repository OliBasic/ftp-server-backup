REM Start of BASIC! Program

FN.DEF ISOLD(f$) 
 FILE.EXISTS isoldfile, f$ 
 FN.RTN ISOLDFILE 
FN.END

FN.DEF getanswer$(p$,v, saymore)
 cr$ = CHR$(10)
 answer$=""
 IF v
  DO
   STT.LISTEN p$
   STT.RESULTS thelist
   LIST.SIZE thelist, thesize
   LIST.GET thelist, 1,s$
   answer$ += s$ + cr$ 
   c = 0
   IF saymore 
    DIALOG.MESSAGE , "say more?", c, "yes", "no"
   ENDIF
  UNTIL c <> 1 


 ENDIF

 TEXT.INPUT answer$,answer$,p$

 FN.RTN answer$
FN.END

FN.DEF editor(f$, p$) 
 myfile$ = f$
 cr$=CHR$(10)
 GRABFILE unedited$, myfile$
 unedited$=TRIM$(unedited$)
 TEXT.INPUT edited$, unedited$,p$
 IF !editcancel
  TEXT.OPEN w, filename, myfile$
  TEXT.WRITELN filename, edited$ 
  TEXT.CLOSE filename
 ENDIF
FN.END

FN.DEF writeln(user$, msg$) 
 TEXT.OPEN W, filename, user$
 TEXT.WRITELN filename, msg$
 TEXT.CLOSE filename 
FN.END

FN.DEF appendln(user$, msg$) 
 TEXT.OPEN A, filename, user$
 TEXT.WRITELN filename, msg$
 TEXT.CLOSE filename 
FN.END


FN.DEF EmailEx(example$)
 FILE.EXISTS Isold, "email.txt"
 IF Isold
  TEXT.OPEN r, f,"email.txt"
  TEXT.READLN f, em$
  TEXT.CLOSE f
 ELSE
  INPUT "enter default email to send to",em$,em$,emcancel
  TEXT.OPEN w, f, "email.txt"
  TEXT.WRITELN f, em$
  TEXT.CLOSE f
 ENDIF
 TIME Year$, Month$, Day$, Hour$, Minute$, Second$, WeekDay, isDST
 Today$=month$+"-"+day$+"-"+year$

 DO
  DIALOG.MESSAGE , "email to " +em$, c, "yes", "edit", "no"
  IF c=2
   INPUT "change email", em$, em$, emcancel
   IF emcancel THEN FN.RTN 0
   TEXT.OPEN w, f, "email.txt"
   TEXT.WRITELN f, em$
   TEXT.CLOSE f
  ENDIF
  IF c=1
   EMAIL.SEND em$, "problem for "+today$, example$
  ENDIF
 UNTIL c<>2
FN.END


FN.DEF Asklist$(p,msg$,r)
 ! PRE: string list p
 !       msg$ is prompt to display
 ! POST: r = index of item in p chosen
 !     Returns  string of item
 !
 DO
  LET I$=""
  DIALOG.SELECT r, p, msg$
  IF r>0
   LIST.GET p, r, i$
  ENDIF
 UNTIL r>0
 FN.RTN i$
FN.END

FN.DEF pickexample$(f$)
 dash$="The problem is:"
 cr$=CHR$(10)
 IF !isold(f$) THEN FN.RTN ""
 LIST.CREATE s,elist

 GRABFILE all$,"my problems.txt"

 SPLIT aa$[],all$, dash$+cr$
 ARRAY.LENGTH n,aa$[]

 FOR i=2 TO n
  UNDIM l$[]
  SPLIT l$[],aa$[i],CHR$(10)
  ARRAY.LENGTH nn,l$[]
  event$ = l$[1]
  LIST.ADD elist,event$
 NEXT i

 LIST.ADD elist,"cancel"
 r=0
 e$=asklist$(elist,"choose problem",&r)
 IF r<0 | e$="cancel" THEN FN.RTN ""

 ! build example string
 ex$=dash$+cr$+aa$[r+1]

 FN.RTN ex$
FN.END
!
!
!
!
! MAIN PROGRAM
!
!

TTS.INIT

dash$="The problem is:"


ARRAY.LOAD menu$[],"add new problem","view/edit problems","delete problem", "email problems","quit"
mainloop:

msg$="choose option"
DIALOG.SELECT r, menu$[], msg$
IF r=0 | r=5 THEN EXIT

IF r=1
 GOSUB newex
 GOTO mainloop
ENDIF

IF r=2
 CLS
 ex$= pickexample$("my problems.txt")


 IF LEN(ex$)>1

  fix$=ex$
  GRABFILE all$,"my problems.txt"

  TEXT.INPUT fix$,fix$,"edit"
  all$=REPLACE$(all$,ex$,fix$)
  writeln("my problems.txt",all$)
 ENDIF
 GOTO mainloop
ENDIF

IF r=3

 CLS

 DIALOG.MESSAGE , "erase everything?", c, "yes", "no"
 IF c=1
  DIALOG.MESSAGE , "are you sure???", c, "yes", "no"
  IF c=1
   FILE.DELETE gone,"my problems.txt"
   POPUP "poof! everythings gone!",0,0,0
  ENDIF

  GOTO mainloop
 ENDIF


 ex$= pickexample$("my problems.txt")
 IF LEN(ex$)>1

  GRABFILE all$,"my problems.txt"

  all$=REPLACE$(all$,ex$,"")

  writeln("my problems.txt",all$)
  POPUP "deleted",0,0,0

  GOTO mainloop
 ENDIF
ENDIF

IF r=4
 FILE.EXISTS Isold, "my problems.txt"

 IF isold
  GRABFILE ex$, "my problems.txt"
  EmailEx(ex$)
 ENDIF

 IF !isold
  writeln("my problems.txt"," ")
 ENDIF

 GOTO mainloop
ENDIF

GOTO mainloop

! enter a new example
newex:

Cr$=CHR$(10)
DIALOG.MESSAGE , "use voice?", c, "yes", "no"
v=(c=1)


p$="Describe the problem:" 
issue$=getanswer$(p$,v,0)

p$="What are the causes/obstacles?"
causes$=getanswer$(p$,v,1)

p$="What are all possible solutions?"
solutions$=getanswer$(p$,v,1)

p$= "What are you going to do?"
action$=getanswer$(p$,v,1)

CLS
Example$="The problem is:" +cr$
Example$+=issue$+cr$+cr$
Example$+="Possible causes are:"+cr$
Example$+=causes$ +cr$+cr$
Example$+="My solutions are:"+cr$
Example$+=solutions$ +cr$
Example$+="I am going to:" +cr$
Example$+=action$ +cr$+cr$
writeln("recent.txt",example$)
Editor("recent.txt", "edit" )

GRABFILE e$, "recent.txt"
e$ = cr$+e$

Appendln("my problems.txt",TRIM$(e$) )

EmailEx(e$)

DIALOG.MESSAGE , "read aloud?", c, "yes", "no"
IF c=2 THEN RETURN
IF c=1
 PRINT e$
 TTS.SPEAK e$
ENDIF

RETURN


Waitfortouch:
LET Ttt=0
DO
UNTIL ttt
RETURN

! Set flag if screen touched
ONCONSOLETOUCH:
LET Ttt=1
CONSOLETOUCH.RESUME
