!
! Voice Math Quiz
!
! mookiebearapps
!

! v.1.89.04 or higher
!FN.DEF isnumber(s$)
!FN.RTN is_number(s$)
!FN.END 

FN.DEF isnumber(s$)
 let z=LEN(s$)
 let i=1
let bad=0
 do 
  LET C$=MID$(s$,I,1)
  if !IS_IN(c$,"-0123456789.") THEN bad=1
  i++
 until i>z | bad
 if bad   
   fn.rtn 0
 else 
   fn.rtn 1
 endif
FN.END 


FN.DEF makequestion(question$,answer,vox$)
 DO
  DO 
   let x=FLOOR(RND()*13)
   let y=FLOOR(RND()*13)
   let oper=FLOOR(RND()*4+1)
   let oper$=WORD$("+,-,*,/",oper,",")
   let v$=word$("plus,minus,times,divided by",oper,",")
  UNTIL !(y=0 & oper$="/")

if oper$<>"/"
 let question$="What is "+INT$(x)+oper$+INT$(y)+"?"
 let vox$="what is "+int$(x)+" "+v$+" "+ int$(y)+"?"
else
let question$="What is "+INT$(x*y)+oper$+INT$(y)+"?"
 let vox$="what is "+int$(x*y)+" "+v$+" "+ int$(y)+"?"
endif

  IF oper$="+"
   let answer=x+y
  ELSE if oper$="-"
  let answer=x-y
  ELSE if oper$="*"
  let answer=x*y
  ELSE if oper$="/" 
  let answer=x*y/y
  ENDIF
 UNTIL frac(answer)=0
FN.END



!
!
! Main


TTS.INIT
LIST.CREATE s,guess
n=10

LOOP:
let correct=0

FOR I=1 TO n
 let q$="":let a=0:let v$=""

 call makequestion(&q$,&a,&v$)
 DO

  CLS
  PRINT v$
  TTS.SPEAK v$
  CLS
  STT.LISTEN "(#"+INT$(i)+") "+q$
  STT.RESULTS guess
 let j=1
 let found=0
  LIST.size guess,z
  DO
   LIST.GET guess,j,g$
   j++
   IF isnumber(g$) THEN let found=1
  UNTIL found | j>z
  IF !found
   POPUP g$+"..eh? i didn't get that.. try again!"
   PAUSE  3000
  ENDIF
 UNTIL found

 IF a=VAL(g$)
  POPUP "you are right!"
  PAUSE 3000
  correct++
 ELSE 
  POPUP "Nope! The answer is "+int$(a)
  PAUSE 3000
 ENDIF

NEXT
CLS
let c$="You got "+INT$(correct)+" out of "+INT$(n)+" right!"

DIALOG.MESSAGE c$, "play again?", ok, "Sure thing!", "No way!" 

IF ok=1 THEN GOTO LOOP
POPUP "Come back soon!"

EXIT
