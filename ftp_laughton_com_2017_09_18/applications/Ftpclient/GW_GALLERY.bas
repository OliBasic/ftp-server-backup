!' This is the third party lib "GW_GALLERY.bas"
!' http://laughton.com/basic/programs/html/GW%20(GUI-Web%20lib)/
!'
!' This lib requires the main lib "GW.bas" and of course Android RFO-BASIC!
!' To download the lib when you have "GW.bas" on your device, do:
!'    GW_DOWNLOAD_THIRD_PARTY("GW_GALLERY.bas")
!'
!' USAGE: in your program do: INCLUDE "GW_GALLERY.bas"
!' Then call:
!'      mygallery = GW_ADD_GALLERY (mypage, thumbnails_height, imgs_list$[])
!'      GW_GALLERY_IS_OPEN (mygallery) % return 1 if gallery is open else 0
!'      GW_CLOSE_GALLERY (mygallery) % close an opened gallery
!'      GW_AMODIFY (mygallery, "content", new_imgs_list$[])

INCLUDE "GW.bas"

FN.DEF GW_ADD_GALLERY$(img$[]) %' (internal) builds a series of <img> for use with gallery
  ARRAY.LENGTH al, img$[]
  FOR i=1 TO al
    j=IS_IN(">", img$[i])
    IF j>0 THEN
      i$=LEFT$(img$[i], j-1)
      lnk$=WEB$("javascript:RFO("+CHR$(34)+MID$(img$[i], j+1)+CHR$(34)+")")
    ELSE
      i$=img$[i]
      lnk$="false"
    END IF
    e$+="<a href id='img"+INT$(i)+"big' data-download-url='"+lnk$+"'><img id='img"+INT$(i)+"thb'></a>"
    script1$+="var img"+INT$(i)+"dat='"+i$+"';$('#img"+INT$(i)+"thb').attr('src',img"+INT$(i)+"dat);"
    script2$+="$('#img"+INT$(i)+"big').attr('data-src',img"+INT$(i)+"dat);"
  NEXT
  FN.RTN e$+"<script>"+script1$+script2$+"</script>"
FN.END

FN.DEF GW_ADD_GALLERY(page, height, img$[])
  !' Check resources
  IF IS_APK()
    MAKE_SURE_IS_ON_SD("GW/lightgallery/lightgallery.min.css")
    MAKE_SURE_IS_ON_SD("GW/lightgallery/lightgallery.min.js")
    MAKE_SURE_IS_ON_SD("GW/lightgallery/lg-zoom.min.js")
    MAKE_SURE_IS_ON_SD("GW/lightgallery/lg-thumbnail.min.js")
    MAKE_SURE_IS_ON_SD("GW/lightgallery/lg.ttf")
    MAKE_SURE_IS_ON_SD("GW/lightgallery/justifiedGallery.min.css")
    MAKE_SURE_IS_ON_SD("GW/lightgallery/justifiedGallery.min.js")
  END IF
  GW_CHECK_AND_DOWNLOAD("GW/lightgallery/lightgallery.min.css")
  GW_CHECK_AND_DOWNLOAD("GW/lightgallery/lightgallery.min.js")
  GW_CHECK_AND_DOWNLOAD("GW/lightgallery/lg-zoom.min.js")
  GW_CHECK_AND_DOWNLOAD("GW/lightgallery/lg-thumbnail.min.js")
  GW_CHECK_AND_DOWNLOAD("GW/lightgallery/lg.ttf")
  GW_CHECK_AND_DOWNLOAD("GW/lightgallery/justifiedGallery.min.css")
  GW_CHECK_AND_DOWNLOAD("GW/lightgallery/justifiedGallery.min.js")
  !' Add 'LightGallery' and 'justifiedGallery' references to the page
  e$=GW_PAGE$(page)
  IF !IS_IN("GW/lightgallery/lightgallery", e$) THEN
    script$="<link rel='stylesheet' href='GW/lightgallery/lightgallery.min.css'>"
    script$+="<script src='GW/lightgallery/lightgallery.min.js'></script>"
    script$+="<script src='GW/lightgallery/lg-zoom.min.js'></script>"
    script$+="<script src='GW/lightgallery/lg-thumbnail.min.js'></script>"
    script$+="<link rel='stylesheet' href='GW/lightgallery/justifiedGallery.min.css'>"
    script$+="<script src='GW/lightgallery/justifiedGallery.min.js'></script>"
    i=IS_IN("</head>", e$)
    e$=LEFT$(e$,i-1)+script$+MID$(e$,i)
  END IF
  !' Create the control
  u$=GW_NEWID$("gallery")
  !' Add the element to the page
  e$+="<div id='"+u$+"'>"+GW_ADD_GALLERY$(img$[])+"</div>"
  e$+="<script>$('#"+u$+"').lightGallery();$('#"+u$+"').justifiedGallery("
  e$+="{rowHeight:"+INT$(height)+",lastRow:'justify'});</script>"
  GW_SET_SKEY("page", page, e$)
  !' Add gallery to the list of controls + return its index
  FN.RTN GW_ADD_SKEY("control", u$)
FN.END

FN.DEF GW_GALLERY_IS_OPEN(ctl_id) %' return 0|1 if gallery is closed|open
  JS("RFO($('body').hasClass('lg-on'))")
  r$=GW_WAIT_ACTION$()
  FN.RTN ("true"=r$)
FN.END

FN.DEF GW_CLOSE_GALLERY(ctl_id) %' close a gallery
  !' Get Unique IDentifier of the control
  ctl$ = GW_ID$(ctl_id)
  JS("$('#"+ctl$+"').data('lightGallery').destroy()")
FN.END
