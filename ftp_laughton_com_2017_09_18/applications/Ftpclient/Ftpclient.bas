REM Start of BASIC! Program

GW_SILENT_LOAD = 1   % to hide GW lib loading bar
include "GW.bas"
include "GW_GALLERY.bas"
include "GW_PICK_FILE.bas"

title$ = "FtpClient"
!foot$ ="&copy;Danny Maris"
foot$ = "./"
dim files$[1]
dim rslt$[1]
array.load upl$[], "/applications","/games","/utilities",~
                   "/html","/tools","/other"
stack.create S, paths
destp$ = "../../tmp/"
datapath$ = "../../Ftpclient/"
dwnerr = 0

call gw_default_transitions("page=none,panel=overlay,dialog=pop")
gosub functions

mpage = gw_new_page()

array.load btn$[], "Yes>EXIT", "No"
array.load barmenu$[], "#Menu", "DwnldFiles", ~
       "UploadFiles", "SearchFiles", "Exit"
call gw_use_theme_custo_once("style='width:10% ; " +~
        "background-color:FFC601 ; color:blue'")
barmenu$ = gw_add_bar_rmenu$(barmenu$[])
call gw_use_theme_custo_once("style='width:10% ; " +~
        "background-color:FFC601 ; color:blue'")
vsn$ = gw_add_bar_lbutton$("Version 1.0")
ti = gw_add_titlebar(mpage, vsn$ + gw_add_bar_title$(title$)+barmenu$)
call gw_add_listener(mpage, ti, "rmenuchange", "MENU")
!call gw_add_titlebar(mpage, vsn$ + gw_add_bar_title$(title$) +~
!        gw_add_bar_rbutton$("Exit>BACK"))
call gw_use_theme_custo_once("style='font-size:75%'")
ftr = gw_add_footbar(mpage, foot$)
call gw_use_theme_custo_once("inline style='background-color:FFC601'")
ex = gw_add_dialog_message(mpage, "Quiting ?", "", btn$[])
call gw_custo_dlgbtn(mpage, ex,btn$[1],"icon=power style='color:blue'")
call gw_custo_dlgbtn(mpage, ex,btn$[2],"icon=back")

files$[1] = " Fetching data, please wait ..."
lvf = gw_add_listview(mpage, files$[])

call gw_render(mpage)

socket.myip myip$
if myip$ = "" then
  undim files$[]: dim files$[2]
  files$[1] = "Not connected with the internet"
  files$[2] = "Program will exit ..."
  call gw_amodify(lvf, "content", files$[])
  pause 2000
  if is_apk() then exit else END "Bye...!"
endif

ftp.open "ftp.laughton.com",21,"basic","basic"

!--------------------------  Main loop -------------------------------
d$ = "./"
gosub getFiles
call gw_amodify(lvf, "content", files$[])
do
  r$ = gw_wait_action$()
  if is_in("MENU:", r$) = 1  then
    array.search barmenu$[], MID$(r$,6), m : m--
    r$ = WORD$("DWNLD UPLOAD SRCH BACK",m)
  endif
  if left$(r$,2) = "LC" then
    cc = val(mid$(r$,3))
    f$ = files$[cc] 
    f$ = left$(f$, is_in(">", f$) - 1)
    if ends_with("(d)", f$) then
      if f$ = "..(d)" then
        stack.isempty paths, n
        if !n then
          stack.pop paths, d$
          gosub getFiles
        endif
      else
        stack.push paths, d$
        d$ = left$(f$,len(f$)-3) + "/"
        stack.peek paths, tdir$
        d$ = tdir$ + d$
        gosub getFiles
      endif
    else
      !its a file
      ftp.get f$, destp$ + f$
      e$ = right$(lower$(f$),4)
      if e$ = ".jpg" | e$ = ".png" then
        gosub galaryPage
      elseif e$ = ".bas"| e$ = ".txt"| ~
             e$ = ".html"| e$ = ".htm" then
        gosub sourceViewer
      endif
    endif
  elseif r$ = "BACK" then 
    call gw_show_dialog(ex)
  elseif r$ = "DWNLD" then
    gosub dwnldFiles
  elseif r$ = "UPLOAD" then
    gosub uploadFiles
  elseif r$ = "SRCH" then 
    gosub searchFTP
  endif
  call gw_amodify(lvf, "content", files$[])
  call gw_modify(ftr, "title", d$)
until r$="EXIT"
ftp.close
if is_apk() then exit else end

getFiles:
  ftp.cd d$
  ftp.dir file_list
  list.size file_list,size
  n = 1
  undim files$[]
  dim files$[size]
  for i = 1 to size
    list.get file_list,i,name$
    if name$ = ".(d)" then F_N.continue 
    files$[n] = name$ +  ">LC" + int$(n)
    n++
  next
return

!----------------------  View image files in gallery mode -----------
galaryPage:
  if !galpage then
    gosub galListFiles
    galpage = gw_new_page()
    call gw_use_theme_custo_once("style='width:10% ; " +~
        "background-color:FFC601 ; color:blue'")
    rbc$ = gw_add_bar_rbutton$("Back>BACK")
    call gw_add_titlebar(galpage, gw_add_bar_title$(title$) + rbc$)
    call gw_use_theme_custo_once("style='font-size:75%'")
    call gw_add_footbar(galpage, foot$)
    galslide = gw_add_slider(galpage, "Thumbnails height: (pixel)", ~
       60, 140, 20, 60)
    call gw_add_listener(galpage, galslide, "change", "RESIZE")
    galctl = gw_add_gallery(galpage, 60, cgal$[])
    call gw_start_center(galpage)
    call gw_open_group(galpage)
    call gw_use_theme_custo_once("inline icon=arrow-l mini")
    galpv = gw_add_button(galpage, "Prev", "PREV")
    call gw_use_theme_custo_once("inline mini")
    galtx = gw_add_button(galpage, "Images " + int$ (cgalidx) +~
      "-" + int$(min(cgalidx+7, cgalmax)) + "/" + int$ (cgalmax), "")
    gw_use_theme_custo_once("inline icon=arrow-r iconpos=right mini")
    galnx = gw_add_button(galpage, "Next", "NEXT")
    call gw_close_group(galpage)
    call gw_stop_center(galpage)
    call gw_add_text(galpage, "")
  else
    gosub galListFiles
  endif
  cgalidx = 1
  call gw_render(galpage)
  gosub galUpdate
  if cgalidx = 1 then call gw_disable(galpv)
  call gw_disable (galtx)
  if cgalidx + 7 >= cgalmax then call gw_disable(galnx)
  do
    r$ = gw_wait_action$()
    if r$ = "NEXT" then
      cgalidx += 8
      IF cgalidx = 9 THEN gw_enable(galpv)
      IF cgalidx + 7 >= cgalmax then gw_disable(galnx)
      gosub galUpdate
    elseif r$ = "PREV" then
      IF cgalidx + 7 >= cgalmax then gw_enable(galnx)
      cgalidx -= 8
      IF cgalidx = 1 then gw_disable(galpv)
      gosub galUpdate
    elseif r$ = "BACK" & gw_gallery_is_open(galctl) then
      call gw_close_gallery(galctl)
      !r$ = ""
    elseif r$ = "RESIZE" then
      gosub galRefresh
    endif
  until r$="BACK"
  r$ = ""
  call gw_render(mpage)
return

galListFiles:
  cgalmax = 1
  dim gal$[1]
  dim cgal$[1]
  if galpath$ = "" then galpath$ = "/tmp"
  galpath$ = "/" + trim$(galpath$, "/") + "/"
  !Check existence of image folder
  file.exists fe, "../.." + galpath$
  canceled = 0
  while !fe & !canceled
    popup "Folder " + galpath$ + " does not exist!"
    pause 2000
    input "Select image folder", galpath$, galpath$, canceled
    galpath$ = "/" + trim$(galpath$, "/") + "/"
    file.exists fe, "../.." + galpath$
  repeat
  if canceled then return
  !Build original list of files
  file.dir "../.." + galpath$, ogal$[], "/"
  list.create s, ogal
  list.add.array ogal, ogal$[]
  array.length al, ogal$[]
  !Only keep JPG and PNG images
  for i = al to 1 step -1
    e$ = right$(lower$(ogal$[i]), 4)
    if e$ <> ".jpg" & e$ <> ".png" then
      list.remove ogal, i
      al--
    else
      list.replace ogal, i, "../.." + galpath$ + ogal$[i]
    endif
  next
  undim ogal$[]
  !Create smaller array of 8 images for the gallery
  if al = 0 then
    list.clear ogal
    popup galpath$ + " does not contain any jpg nor png image"
    pause 2000
    galpath$ = ""
    goto galListFiles
  else
    list.toarray ogal, gal$[]
    list.clear ogal
    array.length cgalmax, gal$[]
    dim cgal$[min(cgalmax, 8)]
  endif
return

galUpdate:
  gw_modify(galtx, "text", "Images " + int$ (cgalidx) + "-" +~
     int$(min(cgalidx+7, cgalmax)) + "/" + int$(cgalmax))
  array.copy gal$[ cgalidx, min(cgalidx+7, cgalmax) ], cgal$[]
  cgal$[min(cgalmax - cgalidx + 1, 8)] += ">BACK"
  call gw_amodify(galctl, "content", cgal$[])
return

galRefresh:
  e$ = "$('#" + gw_id$(galctl) + "').justifiedGallery("
  e$ += "{rowHeight:"+gw_get_value$(galslide)+",lastRow:'justify'})"
  call JS(e$)
return

!----------------------  View the content of txt files ----------------
sourceViewer:
  grabfile w$, destp$ + f$
  split tmp$[], w$ , "\n"
  if !srcpage then
    srcpage = gw_new_page()
    call gw_use_theme_custo_once("style='width:10% ; " +~
        "background-color:FFC601 ; color:blue'")
    rbc$ = gw_add_bar_rbutton$("Back>BACK")
    call gw_add_titlebar(srcpage, gw_add_bar_title$(title$) + rbc$)
    call gw_use_theme_custo_once("style='font-size:75%'")
    call gw_add_footbar(srcpage, foot$)
    tblids = gw_add_table(srcpage, 1, tmp$[])
  endif
  call gw_render(srcpage)
  call gw_amodify(tblids, "content", tmp$[])
  do
    r$ = gw_wait_action$()
  until r$="BACK"
  r$ = ""
  call gw_render(mpage)
return

!----------------------  Possibility to download files ----------------
dwnldFiles:
  if !dwnldpage then
    dwnldpage = gw_new_page()
    dpath$ = "../../Ftpclient/" + left$(f$,len(f$)-3)
    dpath = gw_add_inputline(dwnldpage, "Destination Path", dpath$)
    ctl_checkd = gw_add_checkbox(dwnldpage, "Select ALL")
    call gw_add_listener(dwnldpage, ctl_checkd, "change", "CHECK")
    call gw_add_button(dwnldpage, "Download", "DWNLD")
    call gw_use_theme_custo_once("style='width:10% ; " +~
        "background-color:FFC601 ; color:blue'")
    rbc$ = gw_add_bar_rbutton$("Back>BACK")
    call gw_add_titlebar(dwnldpage, gw_add_bar_title$(title$) + rbc$)
    call gw_use_theme_custo_once("style='font-size:75%'")
    ftrd = gw_add_footbar(dwnldpage, d$)
    gosub listFiles
    tblidf = gw_add_table(dwnldpage, 2, tabfiles$[])
  else
    gosub listFiles
  endif
  call gw_render(dwnldpage)
  call gw_amodify(tblidf, "content", tabfiles$[])
  do
    r$ = gw_wait_action$()
    if is_in ("IMG", r$) = 1 then
      n = val(mid$(r$,5))
      if ends_with("(d)", tabfiles$[n+1]) then D_U.continue
      idfiles[n] = 1 - idfiles[n]
      if idfiles[n] then
        call gw_modify(imgfiles[n], "content",~
                 "../../Ftpclient/data/green.jpg")
      else
         call gw_modify(imgfiles[n], "content",~
                 "../../Ftpclient/data/white.jpg")
      endif
    elseif r$ = "DWNLD" then
      gosub downloadFiles
    elseif r$ = "CHECK" then
      gosub selUnselFiles
    endif
  until r$="BACK"
  r$ = ""
  call gw_render(mpage)
return

listFiles:
  ftp.dir file_list
  list.size file_list,size
  n = 1
  undim tabfiles$[] : undim imgfiles[] : undim idfiles[]
  dim tabfiles$[size*2]
  dim imgfiles[size*2]
  dim idfiles[size*2]
  for i = 1 to size
    list.get file_list,i,name$
    if name$ = ".(d)" then F_N.continue
    if name$ = "..(d)" then F_N.continue
    tabfiles$[n] = ~
      gw_add_image$("../../Ftpclient/data/white.jpg>IMGW"+int$(n))
    imgfiles[n] = gw_last_id()
    idfiles[n++] = 0
    tabfiles$[n++] = name$
  next
return

downloadFiles:
  call gw_modify(ftrd, "title", " Downloading selected files ")
  dpath$ = gw_get_value$(dpath)
  file.exists fe, dpath$
  if !fe then
    file.mkdir dpath$
  endif
  for n = 1 to size*2 step 2
    if idfiles[n] = 1 then
      dpath$ = gw_get_value$(dpath) + "/"
      ftp.get tabfiles$[n+1], dpath$ + tabfiles$[n+1]
    endif
  next
  call gw_modify(ftrd, "title", d$)
return

selUnselFiles:
  for n = 1 to size*2 step 2
    if imgfiles[n] = 0 then F_N.continue
    if gw_checkbox_checked(ctl_checkd) then
      idfiles[n] = 1
      call gw_modify(imgfiles[n], "content",~
                 "../../Ftpclient/data/green.jpg")
    else
      idfiles[n] = 0
      call gw_modify(imgfiles[n], "content",~
                 "../../Ftpclient/data/white.jpg")
    endif
  next
return

!----------------------  Possibility to upload files ----------------
uploadFiles:
  newupl = 0: arexist = 0
  if !uploadpage then
    uploadpage = gw_new_page()  
    call gw_use_theme_custo_once("style='width:10% ; " +~
        "background-color:FFC601 ; color:blue'")
    rbc$ = gw_add_bar_rbutton$("Back>BACK")
    call gw_add_titlebar(uploadpage, gw_add_bar_title$(title$) + rbc$)
    gwpfil = gw_add_inputline(uploadpage, ~
         "Filter on file extensions:", "*.bas, *.txt")   
    upath$ = ""
    call gw_shelf_open(uploadpage)
    sb_upl = gw_add_selectbox(uploadpage, "Category", upl$[])
    call gw_add_listener(uploadpage, sb_upl, "change", "SB_CHANGED")
    call gw_shelf_newcell(uploadpage)
    upath = gw_add_inputline(uploadpage, ~
      "Upload Path(relative to category)", upath$)
    call gw_shelf_close(uploadpage)
    ctl_checku = gw_add_checkbox(uploadpage, "Select ALL")
    call gw_add_listener(uploadpage, ctl_checku, "change", "CHECK")
    !gwpfot = gw_add_textbox(uploadpage, "Folder:")
    call gw_shelf_open(uploadpage)
    gw_add_button(uploadpage,"Choose folder to upload from","PFO")
    call gw_shelf_newcell(uploadpage)
    call gw_add_button(uploadpage, "Upload", "UPLOAD")
    call gw_shelf_close(uploadpage)
    call gw_use_theme_custo_once("style='font-size:85%'")
    ftru = gw_add_footbar(uploadpage, "")
    dim tabfilesU$[2]
    tblidfu = gw_add_table(uploadpage, 2, tabfilesU$[])
  endif
  call gw_render(uploadpage)
  do
    r$ = gw_wait_action$()
    if is_in ("IMG", r$) = 1 then
      n = val(mid$(r$,5))
      if ends_with("(d)", tabfilesU$[n+1]) then D_U.continue
      idfilesU[n] = 1 - idfilesU[n]
      if idfilesU[n] then
        call gw_modify(imgfilesU[n], "content",~
                 "../../Ftpclient/data/green.jpg")
      else
         call gw_modify(imgfilesU[n], "content",~
                 "../../Ftpclient/data/white.jpg")
      endif
    elseif r$ = "PFO" then
      gosub pickFolder
      call gw_amodify(tblidfu, "content", tabfilesU$[])
    elseif r$ = "CHECK" then
      gosub selUnselFilesU
    elseif r$ = "UPLOAD" then
      upath$ = gw_get_value$(upath)
      if !arexist then
        popup "   No upload files selected!"
      elseif !newupl then
        popup "   No category choosen!"
      elseif trim$(upath$) = "" then
        popup "   No upload path specified!"
      elseif filesSelected(idfilesU[], size) = 0 then
        popup "   No upload files selected!"
      else
        gosub upldFiles
      endif
    elseif r$ = "SB_CHANGED" then
      newupl = gw_get_value(sb_upl)
    endif
  until r$="BACK"
  r$ = ""
  call gw_render(mpage)
return

pickFolder:
  filter$ = gw_get_value$(gwpfil)
  upath$ = gw_get_value$(upath)
  newupl = gw_get_value(sb_upl)
  e$ = gw_pick_file$("/")
  call gw_render(uploadpage)
  call gw_modify(gwpfil, "input", filter$)
  call gw_modify(upath, "input", upath$)
  call gw_modify(sb_upl, "selected", int$(newupl))
  if e$ = "" then
    e$ = "[cancelled by user]"
  else
    !while left$(e$, 3) = "../" : e$ = mid$(e$, 4) : repeat
    !e$ = CHR$(47) + e$ %+ CHR$(34)
    !call gw_modify(gwpfot, "text", "Folder: " + e$)
    call gw_modify(ftru, "title", e$)
    gosub lstFiles
  endif
return

lstFiles:
  file.dir e$, all$[]
  array.length size, all$[]
  if size = 0 then return
  n = 0
  for i = 1 to size
    if FinishWith(all$[i], filter$) then n++
  next
  if n = 0 then
    undim tabfilesU$[]: dim tabfilesU$[2]
    return
  endif
  arexist = 1
  undim tabfilesU$[] : undim imgfilesU[] : undim idfilesU[]
  dim tabfilesU$[n*2]
  dim imgfilesU[n*2]
  dim idfilesU[n*2]
  n = 1
  for i = 1 to size
    if all$[i] = ".(d)" | all$[i] = "..(d)" then F_N.continue
    if is_in("(d)",all$[i]) then F_N.continue
    if !FinishWith(all$[i], filter$) then F_N.continue
    tabfilesU$[n] = ~
      gw_add_image$("../../Ftpclient/data/white.jpg>IMGW"+int$(n))
    imgfilesU[n] = gw_last_id()
    idfilesU[n++] = 0
    tabfilesU$[n++] = all$[i]
  next
  array.length size, tabfilesU$[]: size = size / 2
return

selUnselFilesU:
  for n = 1 to size*2 step 2
    if imgfilesU[n] = 0 then F_N.continue
    if gw_checkbox_checked(ctl_checku) then
      idfilesU[n] = 1
      call gw_modify(imgfilesU[n], "content",~
                 "../../Ftpclient/data/green.jpg")
    else
      idfilesU[n] = 0
      call gw_modify(imgfilesU[n], "content",~
                 "../../Ftpclient/data/white.jpg")
    endif
  next
return

upldFiles:
  exdir = 1
  upath$ = gw_get_value$(upath)
  cdp$ = upl$[newupl] + "/" + upath$
  ftp.cd cdp$
  upldFiles_cd:
    if exdir = 0 then
      ftp.mkdir cdp$
      ftp.cd cdp$
    endif
  for i = 1 to size*2 step 2
    if idfilesU[i] = 1 then 
      !print tabfilesU$[i+1]
      ftp.put e$ + tabfilesU$[i+1], tabfilesU$[i+1]
    endif
  next
  popup "   Selected files uploaded!"
return

!----------------------  Possibility to search files ----------------
searchFTP:
  if !srchpage then
    list.create s, ftp_list
    stack.create s, stk
    pPath$ = "/"
    gosub getSrchFiles
    list.size ftp_list,l
    n = 0
    for i = 1 TO l
      list.get ftp_list ,i,name$
      if name$ = ".(d)" | name$ = "..(d)" then F_N.continue
      j = is_in("(d)",name$)
      if j > 0 then n++
    next
    dim sp$[n+1] : n = 1
    sp$[1] = ">/" : newsp = 1
    for i = 1 TO l
      list.get ftp_list ,i,name$
      if name$ = ".(d)" | name$ = "..(d)" then F_N.continue
      j = is_in("(d)",name$)
      if j > 0 then
        s$ = pPath$+replace$(name$ ,"(d)","/")
        sp$[++n] = s$
      endif
    next
    srchpage = gw_new_page()
    sb_sp = gw_add_selectbox(srchpage, "Path to start", sp$[])
    call gw_add_listener(srchpage, sb_sp, "change", "SB_CHANGED")
    searchstr = gw_add_inputline(srchpage, "Search String", searchstr$)
    ctl_check1 = gw_add_checkbox(srchpage, ">Search in filename")
    ctl_check2 = gw_add_checkbox(srchpage, "Search in dirname")
    !call gw_add_listener(srchpage, ctl_check1, "change", "CHECKF")
    !call gw_add_listener(srchpage, ctl_check2, "change", "CHECKD")
    src = gw_add_button(srchpage, "Search", "SRCH")
    call gw_use_theme_custo_once("style='width:10% ; " +~
        "background-color:FFC601 ; color:blue'")
    rbc$ = gw_add_bar_rbutton$("Back>BACK")
    call gw_add_titlebar(srchpage, gw_add_bar_title$(title$) + rbc$)
    call gw_use_theme_custo_once("style='font-size:75%'")
    ftrs = gw_add_footbar(srchpage, "Search FTP server...")
    rslt$[1] = ""
    lvs = gw_add_listview(srchpage, rslt$[])
  endif
  call gw_render(srchpage)
  do
    r$ = gw_wait_action$()
    if r$ = "SRCH" then
      undim rslt$[]: dim rslt$[1]: rslt$[1] = "Searching ..."
      call gw_amodify(lvs, "content", rslt$[])
      gosub searchNow
      file.exists fe, datapath$ + "res2File.txt"
      if fe then
        grabfile tmp$, datapath$ + "res2File.txt"
        split rslt$[], tmp$, "\n"
        array.length ln, rslt$[]
        for i = 1 to ln
          rslt$[i] = trim$(rslt$[i])
        next
        call gw_amodify(lvs, "content", rslt$[])
      endif
    elseif r$ = "SB_CHANGED" then
      newsp = gw_get_value(sb_sp)
    endif
  until r$="BACK"
  r$ = ""
  call gw_render(mpage)
return

searchNow:
  call gw_modify(ftrs, "title", " Executing search of ftp-server ")
  file.delete i, datapath$ + "res2File.txt"
  searchstr$ = gw_get_value$(searchstr)
  pPath$ = sp$[newsp]
  gosub getSrchFiles
  list.size ftp_list,l
  for i = 1 TO l
    list.get ftp_list ,i,name$
    if name$ = ".(d)" | name$ = "..(d)" then F_N.continue
    j = is_in("(d)",name$)
    if j > 0 then
      s$ = pPath$+replace$(name$ ,"(d)","/")
      stack.push stk, s$
    endif
  next
  n=0
  do
    stack.isempty stk, n
    if n <> 1 then
      stack.pop stk, s$
      pPath$=s$
      call gw_modify(ftrs, "title", pPath$)
      if gw_checkbox_checked(ctl_check2) then
        if is_in(lower$(searchstr$), lower$(pPath$)) then
          call res2File(datapath$, "Dir found in: "+pPath$)
        endif
      endif
      gosub getSrchFiles
      list.size ftp_list, l
      for i = 1 TO l
        list.get ftp_list, i, name$
        if name$ = ".(d)" | name$ = "..(d)" then F_N.continue
        j = is_in("(d)", name$)
        if j > 0 then
          s$=pPath$+replace$(name$, "(d)", "/")
          stack.push stk, s$
        endif
        if j = 0 then
          !now check name
          if gw_checkbox_checked(ctl_check1) then
            if is_in(lower$(searchstr$), lower$(name$)) then
              call res2File(datapath$, "File found in: "+pPath$+name$)
            endif
          endif
        endif
      next
    endif
  until n=1
  call gw_modify(ftrs, "title", " Finished search of ftp-server ")
return

getSrchFiles:
  ftp.cd pPath$
  list.clear ftp_list
  retry_ftpdir:
    ftp.dir ftp_list
return

functions:
  fn.def log2File(datapath$, logmsg$)
    text.open a,hs,datapath$ + "logfiles.txt"
    text.writeln hs, logmsg$
    text.close hs
  fn.end

  fn.def res2File(datapath$, result$)
    text.open a,hs,datapath$ + "res2File.txt"
    text.writeln hs, result$
    text.close hs
  fn.end

  fn.def filesSelected(idfilesU[], size)
    i = 0
    for n = 1 to size*2 step 2
      if idfilesU[n] = 1 then i++
    next
    fn.rtn i
  fn.end

return

ONERROR:
er$ = GETERROR$()
!print er$
if lower$(er$) = "download error" then dwnerr = 1
if is_in("(connection timed out)",lower$(er$)) then dwnerr = 2
if is_in("directory change failed",lower$(er$)) then dwnerr = 3
if dwnerr = 2 then
  ftp.close
  ftp.open "ftp.laughton.com",21,"basic","basic"
  ftp.cd pPath$
  goto retry_ftpdir
endif
if dwnerr = 3 then
  exdir = 0
  goto upldFiles_cd
endif
