! tell my fav
!
! simple app to text fav person
BUNDLE.CREATE global %' 'global' bundle pointer = #1
BUNDLE.PUT global, "mode", "console"

FN.DEF readln$(f$) 
 FILE.EXISTS e,f$ 
 IF e
  TEXT.OPEN r,h,f$
  TEXT.READLN h,a$
  TEXT.CLOSE h
 ELSE
  LET A$=""
 ENDIF
 FN.RTN a$
FN.END

FN.DEF isold(f$) 
 FILE.EXISTS e,f$ 
 FN.RTN e
FN.END

FN.DEF appendln(f$,m$) 
 TEXT.OPEN a,h,f$
 TEXT.WRITELN h,m$
 TEXT.CLOSE h
FN.END

FN.DEF writeln(f$,msg$) 
 TEXT.OPEN w,h,f$
 TEXT.WRITELN h,msg$
 TEXT.CLOSE h 
FN.END

!
!
! init display mode manager
!
FN.DEF dinit()
 LET s$="<html><body style=\"background-color:black\"></body></html>"
 HTML.CLOSE
 HTML.OPEN 0
 HTML.LOAD.STRING s$
 BUNDLE.PUT 1,"mode","html"
FN.END 

!
! change display mode to console,html or gr(aphics)
!
FN.DEF dmode(m$)
 BUNDLE.GET 1,"mode",oldm$
 IF oldm$=m$ THEN FN.RTN 0
 BUNDLE.PUT 1,"mode",m$
 IF oldm$="html" THEN HTML.CLOSE
 IF oldm$="gr" THEN GR.CLOSE
 IF m$="html" THEN HTML.OPEN 0
 IF m$="gr" THEN GR.OPEN 255,0,0,0,0,1
FN.END

! wait for click on html screen 
FN.DEF waitclick$()
 DO
  PAUSE 200
  HTML.GET.DATALINK data$
 UNTIL data$ <> ""

 IF IS_IN("BAK:",data$)=1
  EXIT
 ELSEIF IS_IN("DAT:",data$)=1
  data$=MID$(data$,5) 
 ELSEIF IS_IN("LNK:file:///",data$)=1&IS_IN("?",data$) %' Submit link
  i=IS_IN("?",data$)
  data$="SUBMIT&"+MID$(data$,i+1)+"&"
 ENDIF
 FN.RTN data$
FN.END

FN.DEF asklist$(l,msg$,c)
 r$="<br>"
 h$="<!DOCTYPE html><html lang=~en~><head>"
 h$+="<meta charset=~utf-8~ /> <title>Home</title>"
 h$+="<meta content='width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0' name='viewport' />"
 h$+="<meta name=~viewport~ content=~width=device-width; initial-scale=1.0;maximum-scale=1.0~/></head>"
 h$+="<h1 style=~color:#999999~>"+msg$+"</h1>"
 h$+="<body style=~background-color:#000000;text-align:center;~>"
 h$+="<div>"
 LIST.SIZE l,z
 FOR i=1 TO z
  LIST.GET l,i,s$
  s$=REPLACE$(s$,"'","&apos;")
  but$="<button type=~button~  style=~background-color:#333333;color:#FFFFFF;text-align:center;font-size:20px;width:300px;height:50px;~ onclick=~doDataLink('"+STR$(i)+"')~;>"+s$+"</button><br>"
  h$+=but$
 NEXT
 h$+="</div>"
 h$+="<script type=~text/javascript~>"
 h$+="function doDataLink(data)"
 h$+="{Android.dataLink(data);}</script>"
 h$+="</body>"
 h$+="</html>"
 h$=REPLACE$(h$,"~","\"")
 dmode("html")
 HTML.LOAD.STRING h$
 DO
  r$=waitclick$()
  c=VAL(r$)
 UNTIL c>0
 LIST.GET l,c,s$
 FN.RTN s$
FN.END

FN.DEF getphone()
 INPUT "enter phone",p$
 writeln("wifephone.txt",p$)
 POPUP "relaunch to text!"
 EXIT
FN.END

!
! main
!

dinit()
dmode("console")
TTS.INIT
LIST.CREATE s,thelist

IF !isold("wifephone.txt")
 getphone()
ENDIF

p$=readln$("wifephone.txt")
pr$="speak message or RESET to clear phone#"
STT.LISTEN pr$
STT.RESULTS thelist
LIST.SIZE thelist, thesize
LIST.ADD thelist,"cancel"
LIST.GET thelist,1,g$
IF g$="reset" THEN getphone()
c=0
pr$="pick correct one:"
msg$=asklist$(thelist,pr$,&c)
IF msg$="cancel" THEN EXIT
dmode("console")
PRINT msg$
TTS.SPEAK msg$
SMS.SEND p$,msg$
EXIT
