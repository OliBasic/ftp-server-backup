Fn.def TouchedInBox(x1, y1, x2, y2)
  di_height = 480
  di_ width = 800
  gr.screen actual_w, actual_h
  scale_width = actual_w /di_width 
  scale_height = actual_h /di_height
  gr.bounded.touch  z1, x1 * scale_width, y1 * scale_height, x2 * scale_width, y2 * scale_height
  gr.bounded.touch2 z2, x1 * scale_width, y1 * scale_height, x2 * scale_width, y2 * scale_height
  IF z1 | z2 THEN Fn.rtn 1 ELSE Fn.rtn 0
Fn.end

Fn.def TouchedAround(x, y)
  area = 30
  Fn.rtn TouchedInBox(x-area, y-area, x+area, y+area)
Fn.end

Fn.def TRIM$ (e$)
  t$ = e$
  WHILE LEFT$(t$, 1) = " "
    t$ = RIGHT$(t$, LEN(t$) - 1)
  REPEAT
  WHILE RIGHT$(t$, 1) = " "
    t$ = LEFT$(t$, LEN(t$) - 1)
  REPEAT
  Fn.rtn t$
Fn.end

Fn.def JulianDayOfWeek (JDN)
! Function JulianDayOfWeek returns Julian day of the week (Mon=1..Sun=7)
  Fn.rtn MOD(ROUND(JDN), 7) + 1
Fn.end

Fn.def DayOfWeek (wodyear, wodmonth, wodday)
! Function DayOfWeek returns day of the week (Mon=1..Sun=7)
  JD = Julian(wodyear, wodmonth, wodday)
  Fn.rtn JulianDayOfWeek(JD)
Fn.end

Fn.def Julian (JLyear, JLmonth, JLday)
! Function Julian - returns Julian Day Number (JDN)
! Actually it counts days elapsed since "11/25/-4713" (= Nov. 25, 4714 BCE)
  Jyear = JLyear
  Jmonth = JLmonth
  Jday = JLday
  IF Jmonth < 3 THEN
    Jmonth = Jmonth + 12
    Jyear = Jyear - 1
  ENDIF
  yearsBC = 4714 - 1
  yearsAD = Jyear - 1
  Days = FLOOR((yearsBC + yearsAD) * 365.25)
  Days = Days - (Jyear / 100)
  Days = Days + (Jyear / 400)
  Days = Days + FLOOR(30.6 * (Jmonth - 1) + 0.2)
  Fn.rtn Days + Jday
Fn.end

Fn.def INSTR(start, main$, sep$)
  i = 1
  DO
    If MID$(main$, i, LEN(sep$)) = sep$ then Fn.rtn i
    i = i + 1
  UNTIL i > LEN(main$)
  Fn.rtn 0
Fn.end

DIM YearDigitObjPtr[4]
DIM DayDigitObjPtr[2]
DIM NbSprite[10]
array.load Vibr[], 1, 500

WakeLock 4
GOSUB LoadResources

Time Year$, Month$, Day$, Hour$, Minute$, Second$
year = VAL(Year$)
month = VAL(Month$)
day = VAL(Day$)
GOSUB MaJecran

DO

  TMR = Clock()

  IF TouchedAround(120,50) & TouchedAround(670,420) THEN
    print "Pressed secret spots"
    vibrate Vibr[], -1
  ENDIF

  Time Year$, Month$, Day$, Hour$, Minute$, Second$
  y = VAL(Year$)
  m = VAL(Month$)
  d = VAL(Day$)
  IF d <> day | m <> month | y <> year THEN
    year = y
    month = m
    day = d
    GOSUB MaJecran
  ENDIF

  Time dummy$, dummy$, dummy$, Hour$, Minute$, Second$
  IF sec$ <> Second$ THEN
    sec$ = Second$
    gr.modify SecObjPtr, "text", sec$
    IF tim$ <> Hour$ + ":" + Minute$ THEN
      tim$ = Hour$ + ":" + Minute$
      gr.modify HeureObjPtr, "text", tim$
    ENDIF
    gr.render
  ENDIF

UNTIL 0

!OnError:
END

LoadResources:
  print "Entering LoadResources"
  di_height = 480
  di_width = 800
  gr.open 255, 0, 0, 0, 0
  gr.orientation 0
  gr.screen actual_w, actual_h
  scale_width = actual_w /di_width 
  scale_height = actual_h /di_height 
  gr.scale scale_width, scale_height

  gr.bitmap.load tmpBmp,"nimoclock.png"
  FOR i = 1 TO 10
    gr.bitmap.crop NbSprite[i], tmpBmp, 25 * (i - 1), 180, 25, 25
  NEXT i
  gr.bitmap.crop TitleBmp, tmpBmp, 0, 0, 640, 99
  gr.bitmap.crop LabelBmp, tmpBmp, 0, 285, 290, 30
  gr.bitmap.crop FooterBmp, tmpBmp, 0, 330, 640, 149
  gr.bitmap.delete tmpBmp 

RETURN

MaJecran:
  gr.cls
  gr.bitmap.draw TitlePtr, TitleBmp, 80, 0
  gr.bitmap.draw LabelPtr, LabelBmp, 80, 285
  gr.bitmap.draw FooterPtr, FooterBmp, 80, 330

  gr.bitmap.load tmpBmp, "nimoclock.png"

  wod = DayOfWeek(year, month, day)
  print " > Updating day #" + FORMAT$("#", wod)
  IF wod = 1 THEN
    gr.bitmap.crop DayNbBmp, tmpBmp, 0, 100, 640, 79
  ELSE
    gr.bitmap.crop DayNbBmp, tmpBmp, 0, 480 + (wod - 2) * 80, 640, 79
  ENDIF
  gr.bitmap.draw DayNbPtr, DayNbBmp, 80, 100

  xx = MOD(month - 1, 3) * 210
  yy = FLOOR((month - 1) / 3) * 120
  gr.bitmap.crop MonthBmp, tmpBmp, xx, 960 + yy, 205, 119
  gr.bitmap.draw MonthObjPtr, MonthBmp, 415, 200

  IF day < 10 THEN
    gr.bitmap.draw DayDigitObjPtr[1], NbSprite[day + 1], 378, 286
  ELSE
    gr.bitmap.draw DayDigitObjPtr[1], NbSprite[FLOOR((day - MOD(day, 10)) / 10) + 1], 365, 286
    gr.bitmap.draw DayDigitObjPtr[2], NbSprite[FLOOR(MOD(day, 10)) + 1], 390, 286
  ENDIF

  y$ = TRIM$(STR$(year))
  FOR i = 1 TO 4
    gr.bitmap.draw YearDigitObjPtr[i], NbSprite[VAL(MID$(y$, i, 1)) + 1], 620 + (i - 1) * 25, 286
  NEXT i

  gr.color 255, 245, 245, 255, 1
  gr.text.align 2
  gr.text.size 23
  gr.text.bold 1
  tim$ = Hour$ + ":" + Minute$
  gr.text.draw HeureObjPtr, 672, 425, tim$
  gr.text.size 14
  gr.text.bold 0
  sec$ = Second$
  gr.text.draw SecObjPtr, 672, 440, sec$

  gr.bitmap.delete tmpBmp 
  gr.render

RETURN
