REM Start of BASIC! Program
BUNDLE.CREATE global %' 'global' bundle pointer = #1
BUNDLE.PUT global,"mode","html"
BUNDLE.PUT global,"back","0"
FN.DEF _n()
let s$="<html><body style=\"background-color:black\"></body></html>"
HTML.OPEN 0
html.load.string s$
bundle.put 1,"mode","html"
FN.END
FN.DEF _s(m$)
bundle.get 1,"mode",oldm$
IF oldm$=m$ THEN FN.RTN 0
bundle.put 1,"mode",m$
IF oldm$="html" then HTML.CLOSE
IF oldm$="gr" then GR.CLOSE
IF m$="html" THEN HTML.OPEN 0
IF m$="gr" then GR.OPEN 255,0,0,0,0,1
pause 300
FN.END
FN.DEF _p(s$)
let s$=TRIM$(s$):let l=len(s$)
DO
i++
bad=!IS_IN(MID$(s$,i,1),"-0123456789.")
UNTIL bad|i>=l
IF !bad then FN.RTN VAL(s$)
FN.RTN 0
FN.END
FN.DEF _g$(f$)
IF _m(f$)
TEXT.OPEN r,h,f$
TEXT.READLN h,a$
TEXT.CLOSE h
ELSE
A$=""
ENDIF
FN.RTN a$
FN.END
FN.DEF _m(f$)
FILE.EXISTS o,f$
FN.RTN o
FN.END
FN.DEF _c(f$,m$)
TEXT.OPEN a,h,f$
TEXT.WRITELN h,m$
TEXT.CLOSE h
FN.END
FN.DEF _f(f$,msg$)
TEXT.OPEN w,hh,f$
TEXT.WRITELN hh,msg$
TEXT.CLOSE hh
FN.END
FN.DEF _o(m$)
_s("console")
DIALOG.MESSAGE,m$,c,"yes","no"
IF c=1 THEN FN.RTN 1
FN.RTN 0
FN.END
FN.DEF _b$()
TIME Y$,M$,D$,H$,Mi$,S$,WkD,DST
Dd$=m$+"-"+d$+"-"+y$
FN.RTN dd$
FN.END
FN.DEF _j(f$,p$)
r$=CHR$(10)
ue$=grab$(f$)
ue$=TRIM$(ue$)+r$
TEXT.INPUT e$,ue$,p$
IF !editcancel then call _f(f$,e$)
FN.END
FN.DEF _4(s$,d)
PAUSE 100
KB.HIDE
PAUSE 1000
KB.TOGGLE
IF d>=0
INPUT s$,v,d,c
ELSE
INPUT s$,v,,c
ENDIF
IF c THEN EXIT
FN.RTN v
FN.END
FN.DEF _a(s$)
FN.RTN IS_IN(s$,"-0123456789.")
FN.END
FN.DEF _8(s$)
I=1
Found=0
let l=len(s$)
DO
let found=_a(MID$(s$,I,1))
I++
UNTIL i>l|found
FN.RTN found
FN.END
FN.DEF _6(s$)
N$=""
FOR i=1 TO LEN(s$)
C$=MID$(s$,I,1)
IF _a(c$) THEN N$+=c$
NEXT i
IF LEN(n$)>0 THEN FN.RTN VAL(n$)
FN.RTN 0
FN.END
FN.DEF _3$(s$)
N$=""
FOR i=1 TO LEN(s$)
C$=MID$(s$,I,1)
IF !_a(c$) THEN N$+=c$
NEXT i
IF LEN(n$)>0 THEN FN.RTN n$
FN.RTN ""
FN.END
FN.DEF _r(db$,b)
BUNDLE.CLEAR b
FILE.EXISTS old,db$
IF !old THEN FN.RTN 0
TEXT.OPEN R,h,db$
DO
TEXT.READLN h,a$
s1$=WORD$(a$,1,"\t")
s2$=WORD$(a$,2,"\t")
IF s1$<>""&S2$<>"" THEN BUNDLE.PUT b,s1$,s2$
UNTIL a$="EOF"
TEXT.CLOSE h
FN.END
FN.DEF _5$()
DO
PAUSE 100
HTML.GET.DATALINK data$
IF BACKGROUND()
cs=clock()
do
pause 100
until(clock()-cs)>5000|!background()
if background() then EXIT
endif
UNTIL data$ <> ""
IF IS_IN("BAK:",data$)=1
EXIT
ELSEIF IS_IN("DAT:",data$)=1
data$=MID$(data$,5) %' User link
ELSEIF IS_IN("LNK:file:///",data$)=1&IS_IN("?",data$) %' Submit link
i=IS_IN("?",data$)
data$="SUBMIT&"+MID$(data$,i+1)+"&"
ENDIF
FN.RTN data$
FN.END
FN.DEF _0$(l,msg$,c)
r$="<br>"
h$="<!DOCTYPE html><html lang=~en~>"
h$+="<h1 style=~color:#ffffff;font-size:18px;~>"+msg$ +"</h1>"
h$+="<body style=~background-color:#000000; text-align:center;font-size:10;~>"
LIST.SIZE l,z
h$+="<div>"
FOR i=1 TO z
LIST.GET l,i,s$
s$=REPLACE$(s$,"'","&apos;")
but$="<button type=~button~  style=~background-color:#003300;color:#00FF00;text-align:center;font-size:20px;width:300px;height:50px;moz-border-radius: 15px; -webkit-border-radius: 15px;~ onclick=~doDataLink('"+STR$(i)+"')~;>"+s$+"</button><br>"
h$+=but$
NEXT i
h$+="</div>"
h$+="<script type=~text/javascript~>"
h$+="function doDataLink(data) "
h$+="{Android.dataLink(data);}</script>"
h$+="</body>"
h$+="</html>"
h$=REPLACE$(h$,"~","\"")
_s("html")
HTML.LOAD.STRING h$
do
r$=_5$()
c=_6(r$)
until c>0
LIST.GET l,c,s$
FN.RTN s$
FN.END
fn.def _l(p$)
list.create s,m
list.add m,"yes","no"
c=0
call _0$(m,p$,&c)
list.clear m
if c=1 then fn.rtn 1
fn.rtn 0
fn.end
FN.DEF _e(b)
IF !_m("cheapo.txt") THEN FN.RTN 0
TEXT.OPEN r,h,"cheapo.txt"
DO
TEXT.READLN h,a$
IF a$="EOF" THEN D_U.BREAK
UNDIM r$[]
SPLIT r$[],a$,","
ARRAY.LENGTH z,r$[]
IF z>=2
LIST.CREATE n,l
FOR i=2 TO z
LIST.ADD l,_p(r$[i])
NEXT i
BUNDLE.PUT b,r$[1],l
ENDIF
UNTIL a$="EOF"
FN.END
FN.DEF _d(b)
_f("cheapo.txt","")
BUNDLE.KEYS b,mlist
LIST.SIZE mlist,z
FOR i=1 TO z
LIST.GET mlist,i,m$
BUNDLE.GET b,m$,l
r$=m$+","+_7$(l,0)+cr$
_c("cheapo.txt",r$)
NEXT i
FN.END
FN.DEF _i(b,m$,v)
BUNDLE.CONTAIN b,m$,e
IF e
BUNDLE.GET b,m$,l
LIST.ADD l,v
BUNDLE.PUT b,m$,l
ELSE
LIST.CREATE n,l
LIST.ADD l,v
BUNDLE.PUT b,m$,l
ENDIF
FN.END
FN.DEF _7$(l,n)
LIST.SIZE l,z
if !n
let st=1
else
let st=max((z-n+1),1)
endif
FOR i=st TO z
LIST.GET l,i,v
s$+=STR$(v)
IF i<z THEN s$+=","
NEXT i
FN.RTN s$
FN.END
FN.DEF _9(l)
LIST.SIZE l,z
IF z<2 THEN FN.RTN 0
LIST.TOARRAY l,a$[]
ARRAY.SORT a$[]
ARRAY.LENGTH z,a$[]
LIST.CLEAR l
FOR i=1 TO z
LIST.ADD l,a$[i]
NEXT i
FN.END
FN.DEF _h(y[])
ARRAY.COPY y[],x[]
ARRAY.LENGTH n,x[]
ARRAY.SORT x[]
ARRAY.LENGTH i,x[]
IF i/2<>FLOOR(i/2)
M=x[FLOOR((i+1)/2)]
ELSE
M=(x[FLOOR((i)/2)]+ x[FLOOR((i)/2)+1])/2
ENDIF
UNDIM x[]
FN.RTN m
FN.END
fn.def _2(b,s$)
cr$="<br>"
BUNDLE.GET b,s$,l
UNDIM y[]
LIST.TOARRAY l,y[]
ARRAY.AVERAGE av,y[]
ARRAY.STD_DEV sd,y[]
ARRAY.MIN mn,y[]
ARRAY.MAX mx,y[]
p$=s$+cr$+"recent:"+_7$(l,5)+cr$
p$+="average:"+_q$(av)+cr$
p$+="median:"+_q$(_h(y[]))+cr$
p$+= "max:"+_q$(mx)+cr$
p$+="min:"+_q$(mn)+cr$
p$+="sd:"+_q$(sd)+cr$
p$+="trend:"+_q$(_k(y[]))+cr$
LIST.CREATE s,message
LIST.ADD message,"add$","delete","cancel"
let c=0
c$=_0$(message,p$,&c)
LIST.CLEAR message
IF c$="delete"
BUNDLE.REMOVE b,s$
_d(b)
POPUP s$+" deleted!"
ELSE if c$="add$"
_s("console")
INPUT "enter expense for "+s$,v
_i(b,s$,v)
_d(b)
POPUP "recorded!"
ENDIF
fn.end
FN.DEF _k(y[])
ARRAY.LENGTH n,y[]
IF n<2 THEN FN.RTN 0
FOR i=1 TO n
xsum+= i
ysum+= y[i]
xybar+=(i*y[i])
xxbar+=(i*i)
Yybar+=(y[i]*y[i])
NEXT i
yavg=ysum/n
LETxavg=xsum/n
xybar=xybar/n:xxbar=xxbar/n
Yybar=yybar/n
slope=(xybar-xavg*yavg) /(xxbar-xavg*xavg)
FN.RTN slope
FN.END
FN.DEF _q$(v)
f$=_g$("currency")
IF v<10000000
s$=FORMAT$(f$,v)
FN.RTN s$
ELSE
FN.RTN "n/a"
ENDIF
FN.END
_n()
BUNDLE.CREATE b
_e(&b)
IF !_m("currency")
_s("console")
f$="$####.##"
INPUT "enter currency format",f$,f$
_f("currency",f$)
ENDIF
DO
LIST.CREATE s,menu
LIST.ADD menu,"new merchant","settings","quit"
BUNDLE.KEYS b,mlist
_9(mlist)
LIST.SIZE mlist,z
FOR i=1 TO z
LIST.GET mlist,i,s$
LIST.ADD menu,s$
NEXT i
loopit:
mm=0
s$=_0$(menu,"select choice or merchant",&mm)
IF s$="settings"
_s("console")
f$=_g$("currency")
INPUT "enter currency format",f$,f$
_f("currency",f$)
GOTO loopit
ENDIF
IF mm>3 & s$<>"quit"
_2(b,s$)
endif
IF s$="new merchant"
_s("console")
INPUT "new merchant name:",m$
m$=REPLACE$(m$,","," ")
INPUT "amount spent:",v
_i(b,m$,v)
POPUP "added!"
_d(b)
ENDIF
LIST.CLEAR menu
UNTIL s$="quit"
EXIT

