REM Start of BASIC! Program
! cheapo.bas cheapskate app
!
INCLUDE cheapolib.bas

FN.DEF loadbun(b)
 IF !isold("cheapo.txt") THEN FN.RTN 0
 TEXT.OPEN r,h,"cheapo.txt"
 DO
  TEXT.READLN h,a$
  IF a$="EOF" THEN D_U.BREAK
  UNDIM r$[]
  SPLIT r$[],a$,","
  ARRAY.LENGTH z,r$[]
  IF z>=2
   LIST.CREATE n,l
   FOR i=2 TO z
    LIST.ADD l, val2(r$[i])
   NEXT i
   BUNDLE.PUT b,r$[1],l
  ENDIF
 UNTIL a$="EOF"
FN.END

FN.DEF savebun(b)
 writeln("cheapo.txt","")
 BUNDLE.KEYS b,mlist
 LIST.SIZE mlist,z
 FOR i=1 TO z
  LIST.GET mlist,i,m$
  BUNDLE.GET b,m$,l
  LET r$=m$+","+showlist$(l,0)+cr$
  appendln("cheapo.txt",r$)
 NEXT i
FN.END

FN.DEF record(b,m$,v)
 BUNDLE.CONTAIN b,m$,e
 IF e
  BUNDLE.GET b,m$,l
  LIST.ADD l,v
  BUNDLE.PUT b,m$,l
 ELSE
  LIST.CREATE n,l
  LIST.ADD l,v
  BUNDLE.PUT b,m$,l
 ENDIF
FN.END

FN.DEF showlist$(l,n)
 LIST.SIZE l,z
 if !n
   let st=1
else
   let st=max((z-n+1),1)
endif
 FOR i=st TO z
  LIST.GET l,i,v
  s$+=STR$(v)
  IF i<z THEN s$+=","
 NEXT i
 FN.RTN s$
FN.END



FN.DEF listsort(l)
 LIST.SIZE l,z
 IF z<2 THEN FN.RTN 0
 LIST.TOARRAY l,a$[]
 ARRAY.SORT a$[]
 ARRAY.LENGTH z,a$[]
 LIST.CLEAR l
 FOR i=1 TO z
  LIST.ADD l,a$[i]
 NEXT i
FN.END

FN.DEF median(y[])
 ARRAY.COPY y[],x[]
 ARRAY.LENGTH n,x[]
 ARRAY.SORT x[]
 ARRAY.LENGTH i,x[]
 IF i/2<>FLOOR(i/2)
  LET M=x[FLOOR((i+1)/2)]
 ELSE
  LET M=(x[FLOOR((i)/2)]+ x[FLOOR((i)/2)+1])/2
 ENDIF
 UNDIM x[]
 FN.RTN m
FN.END
fn.def pickmerchant(b,s$)

LET cr$="<br>"
  BUNDLE.GET b,s$,l
  UNDIM y[]
  LIST.TOARRAY l,y[]
  ARRAY.AVERAGE av,y[]
  ARRAY.STD_DEV sd,y[]
  ARRAY.MIN mn,y[]
  ARRAY.MAX mx,y[]

  LET p$=s$+cr$+"recent:"+showlist$(l,5)+cr$
  p$+="average:"+pr$(av)+cr$
  p$+="median:"+pr$(median(y[]))+cr$
  p$+= "max:"+pr$(mx)+cr$
  p$+="min:"+pr$(mn)+cr$
  p$+="sd:"+pr$(sd)+cr$
  p$+="trend:"+pr$(trend(y[]))+cr$

  LIST.CREATE s,message
  LIST.ADD message,"add$","delete","cancel"
  let c=0
  c$=asklist$(message,p$,&c)
  LIST.CLEAR message
  IF c$="delete"
   BUNDLE.REMOVE b,s$
   CALL savebun(b)
   POPUP s$+" deleted!"

  ELSE if c$="add$"
   dmode("console")
   INPUT "enter expense for "+s$,v
   CALL record(b,s$,v)
   CALL savebun(b)
   POPUP "recorded!"

  ENDIF
fn.end

FN.DEF trend(y[])
 ARRAY.LENGTH n,y[]
 IF n<2 THEN FN.RTN 0
 FOR i=1 TO n 
  xsum+= i
  ysum+= y[i]
  xybar+=(i*y[i]) 
  xxbar+=(i*i)
  Yybar+=(y[i]*y[i])
 NEXT i
 LET yavg=ysum/n
 LETxavg=xsum/n
 LET xybar=xybar/n:LET xxbar=xxbar/n
 LET Yybar=yybar/n
 LET slope=(xybar-xavg*yavg) /(xxbar-xavg*xavg)
 !let intercept=yavg-slope*xavg
 ! Rr=(xybar-xavg*yavg) / SQR((xxbar-xavg*xavg)* (yybar-yavg*yavg) )
 ! Rr=rr*rr
 FN.RTN slope
FN.END

FN.DEF pr$(v)
 f$=readln$("currency")

 IF v<10000000
  LET s$=FORMAT$(f$,v)
  FN.RTN s$
 ELSE
  FN.RTN "n/a"
 ENDIF
FN.END

CALL dinit()
BUNDLE.CREATE b
CALL loadbun(&b)
IF !isold("currency")
 dmode("console")
 f$="$####.##"
 INPUT "enter currency format",f$,f$
 writeln("currency",f$)
ENDIF
DO

 LIST.CREATE s,menu
 LIST.ADD menu,"new merchant","settings","quit"

 BUNDLE.KEYS b,mlist
 CALL listsort(mlist)
 LIST.SIZE mlist,z
 FOR i=1 TO z
  LIST.GET mlist,i,s$
  LIST.ADD menu,s$
 NEXT i

 loopit:
 
 LET mm=0
 LET s$=asklist$(menu,"select choice or merchant",&mm)
 IF s$="settings"
  dmode("console")
  f$=readln$("currency")
  INPUT "enter currency format",f$,f$
  writeln("currency",f$)
  GOTO loopit
 ENDIF

 IF mm>3 & s$<>"quit"
pickmerchant(b,s$)
endif

  IF s$="new merchant"
   dmode("console")
   INPUT "new merchant name:",m$
   LET m$=REPLACE$(m$,","," ")
   INPUT "amount spent:",v
   CALL record(b,m$,v)
   POPUP "added!"
   CALL savebun(b)
  ENDIF

  LIST.CLEAR menu

 UNTIL s$="quit"
 EXIT
