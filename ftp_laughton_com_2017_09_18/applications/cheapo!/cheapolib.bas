BUNDLE.CREATE global %' 'global' bundle pointer = #1
BUNDLE.PUT global, "mode", "html"
BUNDLE.PUT global, "back", "0"

! init display mode manager
FN.DEF dinit()
let s$="<html><body style=\"background-color:black\"></body></html>"
HTML.OPEN 0
html.load.string s$
bundle.put 1,"mode","html"
FN.END 


! change display mode to console,html or gr(aphics)
FN.DEF dmode(m$)
bundle.get 1,"mode",oldm$
 IF oldm$=m$ THEN FN.RTN 0
bundle.put 1,"mode",m$
 IF oldm$="html" then HTML.CLOSE
 IF oldm$="gr" then GR.CLOSE
 IF m$="html" THEN HTML.OPEN 0
 IF m$="gr" then GR.OPEN 255,0,0,0,0,1
 pause 300
FN.END


! slightly more tolerant val function
FN.DEF val2(s$)
 let s$=TRIM$(s$):let l=len(s$)
 DO
  i++
  LET bad=!IS_IN(MID$(s$,i,1),"-0123456789.")
 UNTIL bad|i>=l
 IF !bad then FN.RTN VAL(s$)  
FN.RTN 0
FN.END


FN.DEF readln$(f$) 
 IF isold(f$)
  TEXT.OPEN r,h,f$
  TEXT.READLN h,a$
  TEXT.CLOSE h
 ELSE
  LET A$=""
 ENDIF
 FN.RTN a$
FN.END


FN.DEF isold(f$) 
 FILE.EXISTS o,f$ 
 FN.RTN o
FN.END

FN.DEF appendln(f$,m$) 
 TEXT.OPEN a,h,f$
 TEXT.WRITELN h,m$
 TEXT.CLOSE h
FN.END

FN.DEF writeln(f$,msg$) 
 TEXT.OPEN w,hh,f$
 TEXT.WRITELN hh,msg$
 TEXT.CLOSE hh
FN.END





! returns true if users answers yes to prompt m$
FN.DEF cask(m$)
 CALL dmode("console")
 DIALOG.MESSAGE,m$,c,"yes","no"
 IF c=1 THEN FN.RTN 1
 FN.RTN 0
FN.END



FN.DEF getdate$()
 TIME Y$,M$,D$,H$,Mi$,S$,WkD,DST
 LET Dd$=m$+"-"+d$+"-"+y$
 FN.RTN dd$
FN.END



FN.DEF editor(f$,p$) 
 r$=CHR$(10)
 ue$=grab$(f$)
 ue$=TRIM$(ue$)+r$
 TEXT.INPUT e$,ue$,p$
 IF !editcancel then call writeln(f$,e$)

FN.END

FN.DEF INPUTNUMBER(s$,d)
 PAUSE 100
 KB.HIDE
 PAUSE 1000
 ?
 KB.TOGGLE
 IF d>=0
  INPUT s$,v,d,c
 ELSE
  INPUT s$,v,,c
 ENDIF
 IF c THEN EXIT 
 FN.RTN v
FN.END 



FN.DEF isadigit(s$) 
 FN.RTN IS_IN(s$,"-0123456789.")
FN.END

FN.DEF hasdigits(s$) 
 LET I=1
 LET Found=0
let l=len(s$)
 DO
 let found=isadigit(MID$(s$,I,1))
  I++
 UNTIL i>l|found
 FN.RTN found
FN.END


! extract number from s$
FN.DEF readnumber(s$) 
 LET N$=""
 FOR i=1 TO LEN(s$) 
  LET C$=MID$(s$,I,1)
  IF isadigit(c$) THEN N$+=c$
 NEXT i
 IF LEN(n$)>0 THEN FN.RTN VAL(n$) 
 FN.RTN 0
FN.END



! val that returns 0 if not valid
! returns s$ with all numbers removed
FN.DEF stripdigits$(s$) 
 LET N$=""
 FOR i=1 TO LEN(s$) 
  LET C$=MID$(s$,I,1)
  IF !isadigit(c$) THEN N$+=c$
 NEXT i 
 IF LEN(n$)>0 THEN FN.RTN n$
 FN.RTN "" 
FN.END


! load string bundle from file 
FN.DEF bld(db$,b) 
 BUNDLE.CLEAR b
 
 FILE.EXISTS old,db$
 IF !old THEN FN.RTN 0
 TEXT.OPEN R,h,db$
 DO
  TEXT.READLN h,a$
  LET s1$=WORD$(a$,1,"\t")
  LET s2$=WORD$(a$,2,"\t")
  IF s1$<>""&S2$<>"" THEN BUNDLE.PUT b,s1$,s2$
 UNTIL a$="EOF"
 TEXT.CLOSE h
FN.END

FN.DEF waitclick$()
 DO
  PAUSE 100
  HTML.GET.DATALINK data$
  IF BACKGROUND() 
cs=clock()
do
pause 100
until (clock()-cs)>5000|!background()
if background() then EXIT
endif
 UNTIL data$ <> ""
 ! popup data$,0,0,0
 IF IS_IN("BAK:",data$)=1
  EXIT
 ELSEIF IS_IN("DAT:",data$)=1
  data$=MID$(data$,5) %' User link
 ELSEIF IS_IN("LNK:file:///",data$)=1&IS_IN("?",data$) %' Submit link
  i=IS_IN("?",data$)
  data$="SUBMIT&"+MID$(data$,i+1)+"&"
 ENDIF
 FN.RTN data$
FN.END



FN.DEF asklist$(l,msg$,c)
 r$="<br>"
h$="<!DOCTYPE html><html lang=~en~>"
h$+="<h1 style=~color:#ffffff;font-size:18px;~>"+msg$ +"</h1>"
 h$+="<body style=~background-color:#000000; text-align:center;font-size:10;~>"

 
 LIST.SIZE l,z

 h$+="<div>"

 FOR i=1 TO z
  LIST.GET l,i,s$
  s$=REPLACE$(s$,"'","&apos;")
  but$="<button type=~button~  style=~background-color:#003300;color:#00FF00;text-align:center;font-size:20px;width:300px;height:50px;moz-border-radius: 15px; -webkit-border-radius: 15px;~ onclick=~doDataLink('"+STR$(i)+"')~;>"+s$+"</button><br>"
  h$+=but$
 NEXT i
 h$+="</div>"
 h$+="<script type=~text/javascript~>"
 h$+="function doDataLink(data) "
 h$+="{Android.dataLink(data);}</script>"
 h$+="</body>"
 h$+="</html>"
 h$=REPLACE$(h$,"~","\"")
dmode("html")
 HTML.LOAD.STRING h$
do
 r$=waitclick$()
 c=readnumber(r$)
until c>0
 !popup r$
 LIST.GET l,c,s$

 FN.RTN s$
FN.END

fn.def askyn(p$)
list.create s,m
list.add m,"yes","no"
c=0
call asklist$(m,p$,&c)
list.clear m
if c=1 then fn.rtn 1
fn.rtn 0
fn.end






	


