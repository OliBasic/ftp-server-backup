REM Program SW Wortfinder

!Setup
apptitel$ = "SW Wortfinder"
appversion$ = "0.10"
dateiname$ =  "wortliste.txt"

CONSOLE.TITLE apptitel$ + " - " + appversion$

REM load wordlist from file
FILE.EXISTS fe, dateiname$
IF fe = 1 THEN
 GRABFILE txt$, dateiname$
 SPLIT wordliste$[], txt$, "\n"
ELSE
 POPUP "Die Datei " + dateiname$ + " wurde nicht gefunden!",0,0,4
 END
ENDIF

word$ = "*r**********a**"

DIM idx [ LEN(word$) ]
FOR i= 1 TO LEN(word$)
 IF MID$(word$,i,1)<>"*" THEN
  sIndex ++
  idx [sIndex] = i
 ENDIF
NEXT

ARRAY.LENGTHsizeWordliste , wordliste$[]
DIM idxAntworten [ sizeWordliste ]
LIST.CREATE s, antworten
FOR i= 1 TO sizeWordliste
 FOR j= 1 TO sIndex
  flag= MID$(word$,idx[j],1) = MID$(wordliste$[i],idx[j],1)
  IF! flag THEN F_N.BREAK
 NEXT
 IF flag THEN LIST.ADD antworten, wordliste$[i]
 IF flag THEN PRINT wordliste$[i]
NEXT

END


!!
Abseitsstellung
Altweibersommer
Anwaltsgehilfin
Aprikosenkuchen
Auberginenpaste
Australienreise

Bananenmilchmix
Bankangestellte
Bernsteinzimmer
Bestellformular
Bildbearbeitung
Billardtischset
Blaubeerflecken
Brausetabletten
Broccoliauflauf
Brustkorbumfang

Charakterrollen
Chefsekretariat
Chinarestaurant
Cocktailtomaten

Dachdeckerstift
Dachshaarpinsel
Daherschleichen
Dahinschlendern
Damenbekleidung
Damenhandtasche
Damenmannschaft
Damoklesschwert
Dampflokomotive
Dienstfahrzeuge

Einbahnstrassen
Eisenbahnticket
Elefantengehege
Energiereserven
Erdbeermilchmix
Ermittlungswert

Fernsehprogramm
Fingermalfarben
Freiheitsstatue
Friedenspfeifen

Gewindeschraube
Goldsucherclaim
Gorillaweibchen

Halbgeschwister
Handwaschbecken
Haselnusskuchen
Hausrenovierung
Heimwerkermarkt
Heizungsmonteur
Hochgebirgsbahn
Hochzeitsmarsch
Holunderstrauch
Hotelfachschule
Hypothekenbrief

Immobilienmarkt
Industriemagnat
Innenministerin
Inselaufenthalt
Intensivstation

Kasperletheater
Kinderwagenkauf
Kleidersammlung
Kreisverwaltung

Lastwagenfahrer
Laufwerkschaden
Lederhandtasche
Literaturkritik

Marinadenrezept
Markenklamotten
Marktwirtschaft
Meerschweinchen
Mozzarellakugel

Naturschauspiel
Naturschutzpark

Osternestinhalt

Personalmanager

Quarkapfeltorte

Regenwaldspende
Rhabarberkuchen
Riesenschnauzer

Sandkastenspiel
Scheibenwischer
Schienenverkehr
Schulpsychologe
Sicherheitszone
Spielwarenladen

Tankstellenshop
Telefonrechnung
Tierheimleitung
Torwarttraining
Traktorenmuseum
Treibhauseffekt
Trickfilmmacher

Unterrichtsfach
Unverwechselbar

Vertragspartner

Weihnachtskugel
Winterabenteuer
Wohnungsanzeige
Wohnzimmerlampe
Wohnzimmertisch
Wurstaufschnitt

Zwillingsbruder
!!
