!parameters *****************************
f $              = "openthesaurus.txt"
desWordLen       = 15
!*****************************************

FILE.SIZE          fsz, f $

LIST.CREATE        s, output

c1               = 2^18
exp$             = "[A-Za-z]"     %{%d}"
nloop            = int (fsz/ c1)+1

! main loop ------------------------
FOR nn           = 1 TO  nloop

 BYTE.OPEN         r, fid, f $
 BYTE.POSITION.SET fid, nn*c1
 BYTE.READ.BUFFER  fid, c1, t$
 BYTE.CLOSE        fid

 t$              = replace $(t $, chr $(10),";")
 UNDIM             t$[]
 SPLIT             t$[], t $, ";"
 ARRAY.LENGTH      le, t $[]

 CLS
 PRINT             "------------"
 PRINT             "Loop ";nn; " of " nloop
 PRINT             "current array-size: "; le
 PAUSE             1500

 FOR i           = 1 TO le
  IF               LEN(t$[i]) = desWordLen THEN GOSUB lev1
 NEXT

NEXT
!--------------------------------

! output ------------------------
LIST.TOARRAY       output,out $[]
ARRAY.SORT         out $[]
ARRAY.LENGTH       le, out$[]
CLS
PRINT              out$[1]
FOR i            = 2 TO le
 IF                out$[i]<> out$[i-1]  THEN
  PRINT            out$[i]
  ctrout          ++
 ENDIF
NEXT
outName $      = LEFT$(f$,LEN(f$)-4)+"_"+INT$(desWordLen)+"Letters.txt"
CONSOLE.SAVE      outName $
PRINT             "Finished!"
PRINT             ctrout; " words saved to: " ; outName $ 

!--------------------------------

END


lev1:
LET t1            = ASCII(t$[i], 1)
IF                  t1> 64 & t1<92 THEN gosub lev2
RETURN

lev2:
LET flag          = LEN(WORD$( t$[i] , 1, exp $))
IF !flag THEN
 ctr++
 PRINT              t$[i], ctr
 LIST.ADD           output, t$[i]
END if
RETURN


