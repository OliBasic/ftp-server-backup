apptitel$ = "Überschrift"
appversion$ = "0.00"

CONSOLE.TITLE apptitel$ + " - " + appversion$

INCLUDE GW.bas
HTML.ORIENTATION 1
page = GW_NEW_PAGE()
GW_ADD_TITLEBAR(page, apptitel$)
GW_ADD_TEXT(page, "Bitte die bekannten Buchstaben eingeben: (Nicht bekannte mit * angeben)")
GW_ADD_INPUTLINE(page, "", "*r**********a**")

GW_ADD_BUTTON(page, "Suchen", "")

parent = GW_ADD_RADIO (page, 0, "Word from List 1")
child1 = GW_ADD_RADIO (page, parent, "Word from List 2")
child2 = GW_ADD_RADIO (page, parent, "Word from List  3")
child3 = GW_ADD_RADIO (page, parent, "Word from List  ...")

GW_ADD_SUBMIT(page, "In die Zwischenablage kopieren")
GW_RENDER(page)

DO
 GW_WAIT_ACTION$()
UNTIL 0
