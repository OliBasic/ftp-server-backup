FN.DEF writeln(user$, msg$) 
 TEXT.OPEN w, filename, user$
 TEXT.WRITELN filename, msg$
 TEXT.CLOSE filename 
FN.END

FN.DEF ISOLD(f$) 
 FILE.EXISTS isoldfile, f$ 
 FN.RTN ISOLDFILE 
FN.END

FN.DEF appendln(user$, msg$) 
 TEXT.OPEN a, filename, user$
 TEXT.WRITELN filename, msg$
 TEXT.CLOSE filename 
FN.END

FN.DEF readln$(user$) 
 IF ISOLD(user$)
  TEXT.OPEN r,FN3,user$
  TEXT.READLN FN3, a$
 ELSE
  A$=""
 ENDIF
 FN.RTN a$
FN.END

FN.DEF editor(f$, p$) 
 myfile$ = f$
 cr$=CHR$(10)
If !Isold(f$) then fn.rtn 0
 GRABFILE unedited$, myfile$
 unedited$=TRIM$(unedited$)
  TEXT.INPUT edited$, unedited$, p$
if !editcancel
 TEXT.OPEN w, filename, myfile$
 TEXT.WRITELN filename, edited$ 
 TEXT.CLOSE filename
endif
FN.END

TIME Year$, Month$, Day$, Hour$, Minute$, Second$, WeekDay, isDST
Date$=month$+"-"+day$+"-"+year$
HMS$=hour$+":"+minute$+":"+ second$
d$=CHR$(9)

CLS

PRINT
PRINT "Tap screen in next 3 seconds to analyze data else wait to record time and exit"
PRINT 
PRINT
PRINT
PRINT
PRINT

tstart=CLOCK()
T=0 
DO 
touch = (t=1) 
UNTIL touch | CLOCK()-tstart>3000

if !touch 
PRINT "Recording Time and Exiting..."
appendln("taplog.txt", date$+d$+hms$)
PRINT
PRINT
POPUP date$+" "+hms$+" recorded",0,0,0

PAUSE 1000
EXIT
ENDIF

CLS
PRINT "Analysis...." 
Editor("taplog.txt","log") 

EXIT

! Set flag if screen touched
ONCONSOLETOUCH:
T=1
CONSOLETOUCH.RESUME
