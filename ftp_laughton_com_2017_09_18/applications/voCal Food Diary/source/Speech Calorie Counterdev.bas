!voCal Calorie Counter for rfo-basic (BASIC!)
!
!by C. Terpin
!mookiebearapps
!


HTML.CLOSE

FN.DEF Render()
if !BACKGROUND() THEN GR.RENDER
Fn.end

FN.DEF LOG(msg$) 
 FN.RTN 0 % comment out for 79debugging
 f$="vocaldebug.txt"
 TIME Year$,Month$,Day$,Hour$,Minute$,Second$,WeekDay,isDST
 Date$=month$+"-"+day$+"-"+year$+" "+hour$+":"+minute$+":"+second$
 m$=date$+msg$
 TEXT.OPEN a,h,f$:TEXT.WRITELN h,m$:TEXT.CLOSE h
FN.END

HTML.OPEN 0

LET s$="saved.html":FILE.EXISTS e,s$
IF e 
 FILE.SIZE z,s$
 IF z>500
  GRABFILE ss$,s$:HTML.LOAD.STRING ss$
 ENDIF
ENDIF

!globals

BUNDLE.CREATE g
BUNDLE.PUT g,"back","0":BUNDLE.PUT g,"version",456
BUNDLE.PUT g,"mode","h":BUNDLE.PUT g,"date",""
BUNDLE.PUT g,"jd",0:BUNDLE.PUT g,"oldjd",0
BUNDLE.PUT g,"greg","":BUNDLE.PUT g,"home",0
BUNDLE.CREATE settings:BUNDLE.CREATE marklist
BUNDLE.PUT g,"marklist",marklist:BUNDLE.PUT g,"settings",settings

bready=1 
FILE.EXISTS e,"../databases/voCal":SQL.OPEN db,"voCal"

IF !e THEN SQL.NEW_TABLE db,"diary","food,calories,date"
BUNDLE.PUT g,"db",db
LIST.CREATE s,vl
LET ww$="<!DOCTYPE html><html> <style media=\"screen\" type=\"text/css\"> a{color:#fff;} h1,h2,h3,h4{font-family:arial;font-weight:bold;color:#fff;margin:0 0 0 0;}  h3{padding:7px;} h1{font-size:60px;font-weight:bold;color:#fff;} h2{font-size:18px;} h3{font-size:16px;} h4{font-size:14px;}  p{line-height:30px;font-size:16px;margin:0 0 0 0;} body{background:#003;margin-top:180px	;margin-left:0px;width:100%;} header{position:fixed;width:100%;height:180px;top:0;z-index:1;margin-left:0px;} header,footer{background:#003;color:#fff;} footer{border-top:1px solid #003;color:#007;font-size:12px;padding:0px 0px 20px 20px;} #myProgress{width:pct$%;height:4px;position:fixed;background-color:bc$;} </style></head> <div id=\"wrap\"><header style=\"background-color:#003;border-right:0px\"><div id=\"myProgress\"></div><div><img src=\"html/images/graphs.png\" onClick=\"DL('GRAPH')\" style=\"padding-top:8px;padding-right:8pt;float:right;width:27pt;height:27pt;\"/></div> cl$ <a onClick=\"DL('TSPK')\">speech$</a>brd$</header></div><body><style> aa{font-size:24px;} button{display:inline-block;width:90px;height:35px;color:white;background-color:blue;margin:0 10px 0 0;padding:3px 3px;font-size:16px; appearance:none;box-shadow:none;border-radius:0;} button:focus{outline:none}</style><div>table$</div><div style=\"top:65%;left:75%;position:fixed;\"><img src=\"html/images/autoc.png\" style=\"width:40pt;height:40pt;\" onClick=\"DL('TYPE')\"/></div><div style=\"top:80%;left:70%;position:fixed;\"><img src=\"html/images/stop$\" style=\"width:60pt;height:60pt;\" onClick=\"DL('VOICE')\"/></div></body> <footer><br><br><p><a onClick=\"DL('REMOVE')\">Remove Item</a></p><p><a ALIGN=\"right\" onClick=\"DL('AUTO')\">auto$</a></p><p><a ALIGN=\"right\" onClick=\"DL('START')\">listen$</a></p><p><a onClick=\"DL('BLIND')\">blind mode blind$</a></p></footer><script type=\"text/javascript\"> function DL(data){Android.dataLink(data);}</script></html>"

FN.DEF rset(s$)
 !PRE:s$ is setting name
 !POST:RETURNs value of setting s$ or 0 if not found
 BUNDLE.GET 1,"settings",b:BUNDLE.CONTAIN b,s$,e
 IF !e THEN FN.RTN 0
 BUNDLE.GET b,s$,v:FN.RTN v
FN.END



BUNDLE.PUT 1,"www",ww$

FN.DEF rLn$(f$)
 FILE.EXISTS e,f$:IF e THEN GRABFILE a$,f$
 FN.RTN TRIM$(a$)
FN.END

FN.DEF aLn(f$,m$)
 TEXT.OPEN a,h,f$:TEXT.WRITELN h,m$:TEXT.CLOSE h
FN.END

FN.DEF wLn(f$,msg$)
 TEXT.OPEN w,h,f$:TEXT.WRITELN h,msg$:TEXT.CLOSE h
FN.END

FN.DEF tallybundle(b,k$,newv)
 LET k$=LOWER$(k$)
 LET v=0:BUNDLE.CONTAIN b,k$,e:IF e THEN BUNDLE.GET b,k$,v
 BUNDLE.PUT b,k$,v+newv
FN.END

FN.DEF grab$(f$)
 FN.RTN rLn$(f$):FILE.EXISTS e,f$:IF e THEN GRABFILE s$,f$:FN.RTN s$ ELSE FN.RTN ""
FN.END

FN.DEF wset(s$,v)
 !PRE:s$ is setting name,v is value to set
 !POST:setting named s$ is set to v
 BUNDLE.GET 1,"settings",b:BUNDLE.PUT b,s$,v
 !save numeric bundle to file
 LET D$="\t":BUNDLE.KEYS b,l:LIST.SIZE l,z
 IF z<1 THEN CALL wLn("settings.ini",""):FN.RTN 0
 LIST.TOARRAY l,ll$[]
 FOR i=1 TO z
  LET K$=ll$[i]:BUNDLE.TYPE b,k$,t$
  IF t$="N" THEN BUNDLE.GET b,k$,v:o$+=k$+d$+str2$(v)+"\n"
 NEXT
 CALL wLn("settings.ini",o$):CALL LoadSettings()
FN.END

FN.DEF ToggleSetting(s$)
 !toggle setting s$ TRUE/FALSE
 CALL wset(s$,!rset(s$))
FN.END

FN.DEF LoadBundle(b$,b) 
 !Load string bundle b from file b$
 BUNDLE.CLEAR b:D$="\t":FILE.EXISTS e,b$
 IF !e THEN FN.RTN 0
 GRABFILE f$,b$:SPLIT ff$[],f$,"\n":ARRAY.LENGTH z,ff$[]
 FOR i=1 TO z
  SPLIT s$[],ff$[i],d$:ARRAY.LENGTH zz,s$[]
  IF zz=2 THEN BUNDLE.PUT b,TRIM$(s$[1]),TRIM$(s$[2])
 NEXT
FN.END

FN.DEF DateTotal(d$,nn,tot)
 ! total calories and count for date d$
 BUNDLE.GET 1,"db",db
 SQL.QUERY c,db,"diary","calories","date='"+d$+"'"
 DO
  SQL.NEXT done,c,cv1$
  IF !done & IS_NUMBER(cv1$) 
   LET v=VAL(cv1$):tot+=v:IF v>0 THEN nn++
  ENDIF
 UNTIL done
FN.END

FN.DEF good$()
 FN.RTN "&#9734;"
FN.END

FN.DEF bad$()
 FN.RTN "&#9785;"
FN.END

FN.DEF chart$()
 FN.RTN "<img src=\"html/images/graphs.png\" style=\"width:15pt;height:15pt;\"/>"
FN.END

FN.DEF exercise(d$)
 ! total calories burned for date d$
 BUNDLE.GET 1,"db",db:SQL.QUERY c,db,"diary","calories","date='"+d$+"'"
 DO
  SQL.NEXT done,c,cv1$
  IF !done & IS_NUMBER(cv1$) 
   LET v=VAL(cv1$):IF v<0 THEN tot+=v
  ENDIF
 UNTIL done
 FN.RTN -tot
FN.END

FN.DEF LoadSettings()
 BUNDLE.GET 1,"settings",b:B$="settings.ini"
 !Load numeric bundle b from file 
 D$="\t":FILE.EXISTS e,b$:IF !e THEN FN.RTN 0
 GRABFILE f$,b$:SPLIT ff$[],f$,"\n":ARRAY.LENGTH z,ff$[]
 FOR i=1 TO z
  LET s1$=TRIM$(WORD$(ff$[i],1,d$)):LET s2$=TRIM$(WORD$(ff$[i],2,d$))
  IF s1$<>"" & IS_NUMBER(s2$) THEN BUNDLE.PUT b,s1$,VAL(s2$)
 NEXT
FN.END

FN.DEF val2(s$)
 IF IS_NUMBER(s$) THEN FN.RTN VAL(s$)
 FN.RTN 0
FN.END

FN.DEF str2$(v)
 IF FRAC(v)>0 THEN FN.RTN STR$(v)
 FN.RTN INT$(v)
FN.END

FN.DEF SaveNumBundle(b$,b)
 !save numeric bundle to file
 D$="\t":BUNDLE.KEYS b,l:LIST.SIZE l,z
 IF z<1 THEN wLn(b$,""):FN.RTN 0
 LIST.TOARRAY l,ll$[]
 FOR i=1 TO z
  LET K$=ll$[i]:BUNDLE.TYPE b,k$,t$
  IF t$="N"
   BUNDLE.GET b,k$,v:IF k$<>"" THEN o$+=k$+d$+str2$(v)+"\n"
  ENDIF
 NEXT
 CALL wLn(b$,o$)
FN.END

FN.DEF LoadNumBundle(b$,b) 
 !Load numeric bundle b from file 
 BUNDLE.CLEAR b:LET D$="\t":FILE.EXISTS e,b$
 IF !e THEN FN.RTN 0
 GRABFILE f$,b$:SPLIT ff$[],f$,"\n":ARRAY.LENGTH z,ff$[]
 FOR i=1 TO z
  SPLIT s$[],ff$[i],d$:ARRAY.LENGTH zz,s$[]
  IF zz=2
   LET S$[1]=TRIM$(s$[1]):IF s$[1]<>"" & IS_NUMBER(S$[2]) THEN BUNDLE.PUT b,s$[1],VAL(s$[2])
  ENDIF
 NEXT
FN.END

FN.DEF grabday$(d$)
 !grab diary for date d$ from database
 BUNDLE.GET 1,"db",db
 SQL.QUERY cursor,db,"diary","food,calories,date","date='"+d$+"'"
 DO
  SQL.NEXT done,cursor,cv1$,cv2$,cv3$
  IF !done THEN r$+=cv1$+"\t"+cv2$+"\t"+cv3$+"\n"
 UNTIL done
 FN.RTN r$
FN.END

FN.DEF getdate$()
 TIME Y$,M$,D$,H$,Mi$,S$,WkD,DST:FN.RTN m$+"-"+d$+"-"+y$
FN.END

FN.DEF dm(m$)
 !PRE:m$=(h)tml (g)graphics or (g2) white background graphics or "" for console
 !POST:display mode is switched and recorded
 BUNDLE.GET 1,"mode",oldm$:IF oldm$=m$ THEN FN.RTN 0
 BUNDLE.PUT 1,"mode",m$
 IF oldm$="h" THEN HTML.CLOSE
 IF oldm$="g"|oldm$="g2" THEN GR.CLOSE
 IF m$="console"|m$="" THEN CLS:FN.RTN 0
 IF m$="h" THEN HTML.CLOSE:HTML.OPEN 0:PAUSE 100:HTML.ORIENTATION 1:FN.RTN 0
 IF m$="g" THEN GR.OPEN 255,0,0,0,0,1:PAUSE 100
 IF m$="g2" THEN GR.OPEN 255,255,255,255,0,1:PAUSE 100
FN.END

FN.DEF SetFlag(f$)
 CALL wset(f$,1)
FN.END

FN.DEF ClearFlag(f$)
 CALL wset(f$,0)
FN.END

FN.DEF isold(f$)
 FILE.EXISTS e,f$:FN.RTN e
FN.END

FN.DEF Stg$(c)
 FN.RTN LEFT$(STR$(c),IS_IN(".",STR$(c))-1)
FN.END

FN.DEF dow$(d$)
 LET j=calcdate(d$):LET sun=2451546:LET d=MOD(j-sun,7)
 FN.RTN "("+WORD$("sun,mon,tues,wed,thur,fri,sat",d+1,",")+")"
FN.END

FN.DEF date2julian(d$)
 BUNDLE.GET 1,"date",od$
 IF d$=od$ THEN BUNDLE.GET 1,"jd",jd:FN.RTN jd
 LET mm=VAL(WORD$(d$,1,"-")):LET dd=VAL(WORD$(d$,2,"-")):LET yy=VAL(WORD$(d$,3,"-"))

 LET a=FLOOR((14-mm)/12):LET y=yy+4800-a:LET m=mm+12*a-3
 LET j=dd+FLOOR((153*m+2)/5)+365*Y+FLOOR(y/4)-FLOOR(y/100)+FLOOR(y/400)-32045

 FN.RTN j
FN.END

FN.DEF juliantogreg$(jd)
 BUNDLE.GET 1,"oldjd",ojd
 IF ojd=jd THEN BUNDLE.GET 1,"greg",g$:FN.RTN g$
 LET q=FLOOR((jd/36524.25)-51.12264)
 LET r=jd+q-FLOOR(q/4)+1:s=r+1524
 LET t=FLOOR((s/365.25)-0.3343):u=FLOOR(t*365.25)
 LET v=FLOOR((s-u)/30.61):d=s-u-FLOOR(v*30.61)
 IF v>13.5 THEN cond=-1 ELSE cond=0
 m=(v-1)+12*cond
 IF m<2.5 THEN LET cond=-1 ELSE LET cond=0
 LET y=t-cond-4716:yr$=Stg$(y)
 WHILE LEN(yr$) < 4
  LET yr$ = "0"+yr$
 REPEAT
 LET mo$=Stg$(m)
 IF LEN(mo$) < 2 THEN LET mo$ = "0"+mo$
 LET da$ = Stg$(d)
 IF LEN(da$) < 2 THEN LET da$ = "0"+da$
 FN.RTN mo$+"-"+da$+"-"+yr$
FN.END

FN.DEF calcdate(s$)
 !return julian date or number 
 IF IS_IN("-",s$) THEN FN.RTN date2julian(s$)
 IF s$="" THEN FN.RTN -1
 IF IS_NUMBER(s$) THEN FN.RTN VAL(s$)
 FN.RTN 0
FN.END

FN.DEF caskyn(p$)
 dm(""):DIALOG.MESSAGE "voCal",p$,c,"yes","no"
 FN.RTN (c=1)
FN.END

FN.DEF now()
 !get julian date/time
 TIME y$,m$,d$,h$,mi$,s$

 LET j=calcdate(m$+"-"+d$+"-"+y$)
 LET f=VAL(h$)/24+VAL(mi$)/(24*60)
 FN.RTN j+f
FN.END

FN.DEF f$(n)
 !format number for stat$()
 FN.RTN TRIM$(FORMAT$("######",n))
FN.END

FN.DEF stat$(a[])
 ARRAY.LENGTH n,a[]:ARRAY.MAX max,a[]
 ARRAY.MIN min,a[]:ARRAY.STD_DEV sd,a[]
 ARRAY.AVERAGE avg,a[]:range=max-min
 s$="n="+INT$(n)+"  "+"avg:"+f$(avg)+"\nmin:"+f$(min)+"  max:"+f$(max)+"\nsd:"+f$(sd)+" range:"+f$(range)+"  median:"+f$(median(a[]))+"\n"
 FN.RTN s$
FN.END

FN.DEF frmcal$(n)
 !formal calories for html
 LET i$=INT$(n):s$="&nbsp;&nbsp;&nbsp;&nbsp;":FN.RTN MID$(s$,1,6*(4-LEN(i$)))+i$
FN.END

FN.DEF atehours()
 !hours since last food
 IF isold("atelast.txt") THEN LET e=now()-VAL(rLn$("atelast.txt")):LET e=FLOOR(24*e*100)/100
 FN.RTN FLOOR(e*10)/10
FN.END

FN.DEF cleanvoice(vl)
 LIST.SIZE vl,n
 FOR i=1 TO n
  LIST.GET vl,i,s$
  s$=REPLACE$(s$,"this morning ","")
  s$=REPLACE$(s$,"today ","")
  s$=REPLACE$(s$,"this afternoon ","")
  s$=REPLACE$(s$,"this evening ","")
  s$=REPLACE$(s$,"tonight ","")
  s$=REPLACE$(s$,"I ate an ","")
  s$=REPLACE$(s$,"I ate a ","")
  s$=REPLACE$(s$,"I ate ","")
  s$=REPLACE$(s$,"I had an ","")
  s$=REPLACE$(s$,"I had a ","")
  s$=REPLACE$(s$,"I had ","")
  s$=REPLACE$(s$,"for breakfast ","")
  s$=REPLACE$(s$,"for lunch ","")
  s$=REPLACE$(s$,"for dinner ","")
  s$=REPLACE$(s$,"for a snack ","")
  s$=REPLACE$(s$," for breakfast","")
  s$=REPLACE$(s$," for lunch","")
  s$=REPLACE$(s$," for dinner","")
  s$=REPLACE$(s$," for a snack","")

  LIST.REPLACE vl,i,s$
 NEXT 

FN.END

FN.DEF IsMarked(f$)
 LET f$=TRIM$(f$):BUNDLE.GET 1,"marklist",b:BUNDLE.CONTAIN b,f$,e
 IF !e THEN FN.RTN 0
 BUNDLE.GET b,f$,m$ 
 IF m$="-" THEN FN.RTN -1 ELSE FN.RTN 1
FN.END

FN.DEF loadmarks(m$,b)
 !load veggies list rom file m$ into bundle 
 CALL LoadBundle(m$,&b)
FN.END

FN.DEF clk$(s$)
 !html clickable element
 LET s$=REPLACE$(s$,"'","#")
 FN.RTN " onclick=~DL('"+s$+"')~ "
FN.END

FN.DEF weekavg()
 BUNDLE.GET 1,"db",db
 !calculate average calories for 7 days prior to today
 LET d=FLOOR(now()):j1=d-7:j2=d-1
 FOR j=j1 TO j2
  LET dayt=0:LET dd$=juliantogreg$(j):LET cc=0:CALL DateTotal(dd$,&cc,&dayt)
  IF dayt THEN wktot+=dayt:dayct++
 NEXT
 IF dayct THEN FN.RTN FLOOR(wktot/dayct) ELSE FN.RTN 0
FN.END

FN.DEF pick$(c,a$,b$)
 IF c THEN FN.RTN a$ ELSE FN.RTN b$
FN.END

FN.DEF ask(f$,d$)
 CALL dm(""):IF d$="-99999" THEN d$="0"
 LIST.CREATE s,l:q$=f$+" "+d$+". is that correct?"
 TTS.INIT:TTS.SPEAK q$:STT.LISTEN q$:STT.RESULTS l
 LIST.GET l,1,s$:s$=LOWER$(TRIM$(s$))
 IF s$="yes" 
  FN.RTN val2(d$)
 ELSE
  TTS.SPEAK "say the correct amount"
  STT.LISTEN "what is the correct amount?"
  STT.RESULTS l:LIST.GET l,1,s$
  TTS.SPEAK s$:s$=REPLACE$(s$,"calories","")
  s$=REPLACE$(s$,"points","")
  FN.RTN readnumber2(TRIM$(s$))
 ENDIF
 FN.RTN -99999
FN.END

FN.DEF IsApk() 
 !return true (1) if we are in apk mode,false (0) otherwise
 FILE.EXISTS Editor,"../source/Speech Calorie Counterdev.bas"
 FN.RTN (!Editor)
FN.END

FN.DEF PutResOnSd(f$) 
 !make sure the file f$ is on sd-card,else try to copy it from asset
 FILE.EXISTS onSd,f$     
 IF !onSd 
  BYTE.OPEN r,fid,f$:IF fid<0 THEN FN.RTN 0    
  LET j = IS_IN("/",f$)
  WHILE j 
   FILE.MKDIR LEFT$(f$,j):j = IS_IN("/",f$,j+1)
  REPEAT
  BYTE.COPY fid,f$
 ENDIF
FN.END

FN.DEF waitclick$(main)
 cstart2=CLOCK()
 DO
  PAUSE 100:HTML.GET.DATALINK data$
  IF main & rset("auto")&(CLOCK()-cstart2)>8000 THEN FN.RTN "QUIT"
 UNTIL data$ <> ""
 IF IS_IN("DAT:",data$)=1
  data$=MID$(data$,5)
 ELSEIF IS_IN("BAK:",data$)=1
  FN.RTN "QUIT"
 ELSEIF IS_IN("LNK:file:///",data$)=1&IS_IN("?",data$)
  i=IS_IN("?",data$):data$="SUBMIT&"+MID$(data$,i+1)+"&"
 ENDIF
 FN.RTN data$
FN.END

FN.DEF getremind$()
 LET r$=grab$("remind.txt")
 SPLIT rr$[],r$,"\n"
 ARRAY.LENGTH z,rr$[]
 LET i=FLOOR(RND()*z)+1
 IF z THEN LET rmd$=rr$[i]
 FN.RTN rmd$
FN.END

FN.DEF ShowDiary()
 LET star$="&#9734;":LET bad$="&#9785;":LET dt$=rLn$("current"):LET dispd$=dt$
 sq$="&#39;"
 IF dt$=getdate$() THEN LET dispd$="today" 
 CALL dm("h"):LET dl$="\n"
 LET x$="<button style=~width:20%;height:45px;~ "+clk$("PREV")+"> < </button><button style=~text-align:left;background-color:#006;border:none;width:50%;~"+clk$("TODAY")+">"+dispd$+" "+dow$(dt$)+"</button><button style=~width:20%;height:45px;~ "+clk$("NEXT")+">></button><br>"

 !grab diary for date dt$ 
 BUNDLE.GET 1,"db",db:SQL.QUERY cursor,db,"diary","food,calories,date","date='"+dt$+"'"
 DO
  SQL.NEXT done,cursor,s1$,s2$,s3$:IF done THEN D_U.BREAK
  i++:LET m$="":LET ss=ismarked(s1$)
  IF ss=1 THEN LET m$=star$:marks++
  IF ss=-1 THEN LET m$=bad$:frowns++

  IF IS_NUMBER(s2$)
   LET T=VAL(s2$):total+=t
   IF t>0 THEN n++ELSE burned+=-t
  ENDIF
  LET cb$="#00e;"

  IF MOD(i,2) THEN LET cb$="#009;"
  LET cs$=cb$
  IF ss=1 THEN LET cs$="#0b0;"
  IF ss=-1 THEN LET cs$="#444;"

  x$+="<button style=~width:20%; background-color:"+cs$+"~"+clk$("TOGGLE "+s1$)+">"+s2$+m$+"</button><button style=~text-align:left;width:72%;border:none;background-color:"+cb$+"~ "+clk$("QUIT")+">"+s1$+"</button><br>"
 UNTIL done
 IF !n
  x$+="<button style=~width:20%;~ >-</button><button style=~border:none;width:72%;~ "+clk$("QUIT")+">QUIT</button><br>"
 ENDIF
 !switches
 LET auto$="autoquit "
 IF rset("auto") THEN LET auto$+="&#10004;" 
 LET listen$="autolisten"+pick$(rset("listen"),"&#10004;","")
 LET speech$=pick$(rset("speak"),"&#128266;","&#128264;")

 LET blind$=pick$(rset("blind"),"&#10004;","")
 LET goal=rset("goal"):CALL wset("todaycal",total)

 IF total > goal
  LET pct$="100":LET warn$="#f00":LET stop$="red.png"
 ELSE if total > 0.8*goal
  LET pct$="0":IF goal>0 THEN LET pct$=INT$(100*total/goal)
  LET warn$="#ff0":LET stop$="yellow.png"
 ELSE
  LET pct$="0":IF goal>0 THEN LET pct$=INT$(MAX(0,100*total/goal))
  LET warn$="#0F0":LET stop$="green.png"
 ENDIF
 IF goal>0 THEN LET lft$=pick$(goal>=total,TRIM$(INT$(goal-total)),TRIM$(">"+INT$(total-goal)))
 IF !goal THEN LET lft$="set goal"
 LET cl$="<button "+clk$("GOAL")+" style=~height:60px;width:auto;font-size:50px;color:"+warn$+"~> "+lft$+"</button>"
 LET brd$="<h2 style=~text-align:right~>"+INT$(total+burned)+"-"+INT$(burned)+"="+INT$(total)+"/"+INT$(goal)+" wk avg:"+INT$(rset("weekavg"))
 LET ah=atehours()
 brd$+="<br>you ate "+STR$(ah)+" hours ago<br>"

 sss$="<h4>"+INT$(marks)+"&#9734; "+INT$(frowns)+bad$+" "+INT$(n)+" items</h4>"
 rmd$=grn$(getremind$())
 brd$+="<a "+clk$("REMIND")+">"+rmd$+"&#9999;</a></h2>"
 BUNDLE.GET 1,"www",ww$
 LET w$=REPLACE$(ww$,"brd$",brd$):LET w$=REPLACE$(w$,"pct$",pct$):LET w$=REPLACE$(w$,"bc$",warn$)
 LET w$=REPLACE$(w$,"cl$",cl$):LET w$=REPLACE$(w$,"stop$",stop$):LET w$=REPLACE$(w$,"table$",x$+sss$)
 LET w$=REPLACE$(w$,"speech$",speech$):LET w$=REPLACE$(w$,"auto$",auto$):LET w$=REPLACE$(w$,"listen$",listen$):LET w$=REPLACE$(w$,"blind$",blind$):LET w$=REPLACE$(w$,"~","\"")
 IF dispd$="today" THEN wLn("saved.html",&w$)
 HTML.LOAD.STRING w$
 IF rset("speak") & rset("spkworks") & goal>0 
  IF total<>rset("oldcal")
   CALL ClearFlag("spkworks"):TTS.SPEAK lft$,0:SetFlag("spkworks"):CALL wset("oldcal",total)
  ENDIF
 ENDIF

FN.END

FN.DEF PepTalk()
 dm("")
 GRABFILE f$,"remind.txt"
 f$=TRIM$(f$)
 IF LEN(f$)<2 THEN FN.RTN 0
 SPLIT s$[],f$,"\n"
 ARRAY.LENGTH z,s$[]
 IF z<1 THEN FN.RTN 0
 TTS.INIT
 BUNDLE.PUT 1,"ctouch",0
 DIM a[z]
 FOR i=1 TO z
  a[i]=i
 NEXT
 ARRAY.SHUFFLE a[1,z]
 FOR i=1 TO z
  PRINT s$[a[i]]
  TTS.SPEAK s$[a[i]]
  PAUSE 2000
  BUNDLE.GET 1,"ctouch",ct
  IF ct THEN F_N.BREAK
 NEXT
 CLS
FN.END

FN.DEF init()
 CALL LoadSettings():BUNDLE.CREATE m:CALL LoadBundle("marked.txt",&m):BUNDLE.PUT 1,"marklist",m

 !check to see if announcement worked correctly
 IF rset("speak")&rset("spkworks") THEN CALL ClearFlag("spkworks"):TTS.INIT:CALL SetFlag("spkworks")

 !if speak command failed last run then turn speech off
 IF rset("speak")&!rset("spkworks") THEN CALL ClearFlag("speak")
 LET date$=getdate$():CALL wLn("current",date$)
 IF isold("lastdate.txt")
  IF rLn$("lastdate.txt")="" THEN CALL wLn("lastdate.txt",Date$)
 ENDIF

 !if it's user's first time running program save date
 IF !isold("lastdate.txt")
  CALL wLn("lastdate.txt",Date$):CALL ClearFlag("speak")
  LET dflt$="0\n25\n50\n100\n150\n200\n250\n300\n400\n-50\n-100\n-200"
  LET q$="quickcal.txt"
  IF caskyn("use points instead of calories?")
   LET dflt$="0\n1\n2\n3\n4\n5\n6\n7\n8\n-1\n-2"
  ELSE
   LET f$=grab$("sampledb/samplefooddb.txt")
   CALL cleandb(&f$):CALL aLn("fooddb.txt",f$)
  ENDIF
  CALL wLn(q$,dflt$)
 ENDIF
 !listen on startup?
 IF rset("listen")
  LIST.CREATE s,vl
  CALL hear(&vl)
  CALL wLn("ok","1"):LET r$="VOICE"
  CALL PerformCommand(r$,vl)
  LIST.CLEAR vl
 ENDIF
FN.END

FN.DEF editdb(db$)
 IF !isold(db$) THEN wLn(db$,"")
 e$=TRIM$(grab$(db$))
 e$=REPLACE$(e$,"\t",":")
 DO
  e$=REPLACE$(e$,"\n\n","\n")
 UNTIL !IS_IN("\n\n",e$)
 e$=htmledit$("edit database",e$)
 cleandb(&e$):wLn(db$,e$):POPUP "saved!"
FN.END

FN.DEF isadigit(s$)
 FN.RTN IS_IN(s$,"-0123456789.")
FN.END

FN.DEF hasdigits(s$)
 LET I=1:LET l=LEN(s$)
 DO
  LET found=isadigit(MID$(s$,I,1)):I++
 UNTIL i>l|found
 FN.RTN found
FN.END

FN.DEF readnumber(s$)
 LET z=LEN(s$)
 FOR i=1 TO z
  LET C$=MID$(s$,I,1):IF isadigit(c$) THEN N$+=c$
 NEXT
 IF LEN(n$)>0 THEN FN.RTN VAL(n$)
 FN.RTN 0
FN.END

FN.DEF readnumber2(s$)
 !used with speech recognition to extract numbers
 LET s$=LOWER$(s$)
 IF hasdigits(s$)
  LET N$="":LET z=LEN(s$)
  FOR i=1 TO z
   LET C$=MID$(s$,I,1):IF isadigit(c$) THEN N$+=c$
  NEXT
  IF LEN(n$)>0 THEN FN.RTN VAL(n$)
 ELSE
  v=0:nn$="one,two,three,four,five,six,seven,eight,nine,ten,eleven,twelve,thirteen,fourteen,fifteen,sixteen,seventeen,eighteen,nineteen,twenty"
  FOR i=1 TO 20
   IF IS_IN(" "+WORD$(nn$,i,",")+" ",s$) THEN v=i :F_N.BREAK
  NEXT
  FN.RTN v
 ENDIF
 FN.RTN 0
FN.END

FN.DEF stripdigits$(s$)
 !remove all numeric data from string 
 LET z=LEN(s$)
 FOR i=1 TO z
  LET C$=MID$(s$,I,1):IF !isadigit(c$) THEN N$+=c$
 NEXT
 IF LEN(n$)>0 THEN FN.RTN n$
 FN.RTN ""
FN.END

FN.DEF SaveBundle(db$,b)
 !write string bundle 
 TEXT.OPEN w,f,db$:BUNDLE.KEYS b,l:LIST.SIZE l,z
 IF z<1 THEN TEXT.WRITELN f,"":TEXT.CLOSE f:FN.RTN 0
 LIST.TOARRAY l,ll$[]:ARRAY.SORT ll$[]
 FOR i=1 TO z
  LET K$=ll$[i]:IF k$<>"" THEN BUNDLE.GET b,k$,v$:TEXT.WRITELN f,k$+"\t"+v$
 NEXT
 TEXT.CLOSE f
FN.END

FN.DEF Guess(f$)
 !retrieve cals/pts for food f$
 ! returns -99999 if food not found
 LET f$=TRIM$(f$):BUNDLE.CREATE b:CALL LoadNumBundle("fooddb.txt",&b):BUNDLE.CONTAIN b,f$,found
 IF !found THEN FN.RTN -99999
 BUNDLE.GET b,f$,v:FN.RTN v
FN.END

FN.DEF asklist2$(l,msg$,c)
 !2 column version
 r$="<br>":h$="<!DOCTYPE html><html lang=~en~><head><meta charset=~utf-8~ /> <title>Home</title>"
 h$+="<meta content='width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0' name='viewport' />"
 h$+="<meta name=~viewport~ content=~width=device-width;initial-scale=1.0;maximum-scale=1.0~/></head>"
 h$+="<h1 style=~color:#aaa~>"+msg$+"</h1><body style=~background-color:#000;text-align:center;~> <div>"
 LIST.SIZE l,z
 FOR i=1 TO z
  LIST.GET l,i,s$
  s$=REPLACE$(s$,"'","'")
  but$="<button type=~button~  style=~background-color:#111;color:#fff;text-align:center;font-size:40px;width:140px;height:50px;~ onclick=~DL('"+STR$(i)+"')~;>"+s$+"</button>"
  h$+=but$
  IF FRAC(i/2)<>0 THEN h$+="<br>"
 NEXT
 h$+="</div><script type=~text/javascript~> function DL(data) {Android.dataLink(data);}</script> </body> </html>"
 h$=REPLACE$(h$,"~","\""):dm("h"):HTML.LOAD.STRING h$
 LIST.GET l,1,d$
 IF rset("speak") THEN TTS.SPEAK REPLACE$(msg$,"choose cals/pts for","")+" "+d$,0
 DO
  r$=waitclick$(0):c=val2(r$)
 UNTIL c>0
 LIST.GET l,c,s$:FN.RTN s$
FN.END

FN.DEF hgetkey$(msg$)
 !html numeric keypad
 LIST.CREATE s,l:LIST.ADD l,"1","2","3","4","5","6","7","8","9","0",".","-","<","x","E"
 r$="<br>":h$="<!DOCTYPE html><html lang=~en~><head>"
 h$+="<meta charset=~utf-8~ /> <title>Home</title><meta content='width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0' name='viewport' />"
 h$+="<meta name=~viewport~ content=~width=device-width;initial-scale=1.0;maximum-scale=1.0~/></head>"
 h$+="<h1 style=~font-size:35px;color:#0f0;~>"+msg$+"</h1><body style=~background-color:#000;text-align:center;~> <div>"
 LIST.SIZE l,z
 FOR i=1 TO z
  LIST.GET l,i,s$:s$=REPLACE$(s$,"'","'")
  but$="<button type=~button~  style=~background-color:#000;color:#0f0;text-align:center;font-size:35pt;width:25%;height:30%;~ onclick=~DL('"+STR$(i)+"')~;>"+s$+"</button>"
  h$+=but$:IF FRAC(i/3)=0 THEN h$+="<br>"
 NEXT
 h$+="</div><script type=~text/javascript~> function DL(data) {Android.dataLink(data);}</script> </body> </html>"
 h$=REPLACE$(h$,"~","\""):dm("h"):HTML.LOAD.STRING h$
 DO
  r$=waitclick$(0):c=val2(r$)
 UNTIL c>0
 LIST.GET l,c,s$
 FN.RTN s$
FN.END

FN.DEF askyn(p$)
 LIST.CREATE s,m:LIST.ADD m,"yes","no"
 c=0:AskList$(m,p$,&c)
 LIST.CLEAR m:IF c=1 THEN FN.RTN 1
 FN.RTN 0
FN.END

FN.DEF UseWeb(p$)
 !lookup info on web
 dm("h")
 P$=REPLACE$(p$,"find","")
 P$=REPLACE$(p$,"search","")
 m$="use web?"
 LIST.CREATE s,l
 LIST.ADD l,"USDA Foodapedia","google","CDC BMI calculator","bing "+p$,"duckduckgo "+p$,"BMR calculator","YouTube playlist","cancel"
 ch=0:AskList$(l,m$,&ch)
 IF ch>0 & ch<8 THEN POPUP "press BACK key to return to app"

 SW.BEGIN ch
  SW.CASE 1
   BROWSE "https://mnew.supertracker.usda.gov/FoodTracker"
   SW.BREAK
  SW.CASE 2
   dm("h")
   HTML.LOAD.URL "http:/www.google.com"
   SW.BREAK
  SW.CASE 3
   BROWSE "http://www.cdc.gov/healthyweight/assessing/bmi/adult_bmi/english_bmi_calculator/bmi_calculator.html"
   SW.BREAK
  SW.CASE 4
   dm("h")
   BROWSE "http://www.bing.com/?q="+p$+" calories"
   SW.BREAK
  SW.CASE 5
   dm("h")
   BROWSE "https://duckduckgo.com/?q="+p$+"+calories&ia=nutrition"
   SW.BREAK
  SW.CASE 6
   BROWSE "http://www.calculator.net/bmr-calculator.html"
   SW.BREAK
  SW.CASE 7
   BROWSE "https://www.youtube.com/playlist?list=PLi6Y_PQh4vOmk9JJinwzYC-bWct_CT8sK"
   SW.BREAK
 SW.END

 DO
  PAUSE 300
 UNTIL !BACKGROUND()
 PAUSE 1000
FN.END

FN.DEF bkhit()
 BUNDLE.GET 1,"back",b$:b=(b$="1")
 IF b THEN POPUP "back key hit":BUNDLE.PUT 1,"back","0"
 FN.RTN b
FN.END

FN.DEF choosefile$(path$)
 DO 
  ARRAY.DELETE d1$[]
  FILE.DIR path$,d1$[]
  ARRAY.LENGTH length,d1$[]
  ARRAY.DELETE d2$[]
  DIM d2$[length+1]
  d2$[1]=".."
  FOR i=1 TO length
   d2$[i+1]=d1$[i]
  NEXT 
  s=0
  LIST.CREATE s,l
  ARRAY.LENGTH z,d2$[]
  FOR i=1 TO z
   LIST.ADD l,d2$[i]
  NEXT
  AskList$(l,"select file",&s)
  IF s>1
   n=IS_IN("(d)",d2$[s])
   IF n=0
    D_U.BREAK
    FN.RTN path$+d2$[s]
   ENDIF
   dname$=LEFT$(d2$[s],n-1)
   path$+=dname$+"/"
   D_U.CONTINUE
  ENDIF

  IF path$=""
   path$="../"
   D_U.CONTINUE
  ENDIF
  ARRAY.DELETE p$[]
  SPLIT p$[],path$,"/"
  ARRAY.LENGTH length,p$[]
  IF p$[length]=".."
   path$+="../"
   D_U.CONTINUE
  ENDIF
  IF length=1
   path$=""
   D_U.CONTINUE
  ENDIF
  path$=""
  FOR i=1 TO length-1
   path$+=p$[i]+"/"
  NEXT 
 UNTIL 0
 FN.RTN path$+d2$[s]
FN.END

FN.DEF Asklistdone$(p,msg$,r)
 !PRE:string list p
 !      msg$ is prompt to display
 !POST:r = index of item in p chosen
 !    Returns  string of item
 !
 LIST.SIZE p,size
 IF size<1 THEN FN.RTN "done"
 LIST.ADD p,"done"
 a$=TRIM$(AskList$(p,msg$,&r))
 LIST.SIZE p,size
 LIST.REMOVE p,size
 FN.RTN a$
FN.END

FN.DEF SaveMarks(f$,b)
 !save marked food list from bundle 
 CALL SaveBundle(f$,b):BUNDLE.PUT 1,"marklist",b
FN.END

FN.DEF MarkFood(f$,type)
 !star a food
 LET mdb$="marked.txt":BUNDLE.CREATE b:CALL loadmarks(mdb$,&b)
 IF type=-1 THEN s$="-" ELSE s$="+"
 BUNDLE.PUT b,f$,s$:CALL SaveMarks(mdb$,b)
FN.END

FN.DEF ClearMark(f$)
 LET mdb$="marked.txt":BUNDLE.CREATE b:CALL loadmarks(mdb$,&b)
 BUNDLE.REMOVE b,TRIM$(f$):CALL SaveMarks(mdb$,b)
FN.END

FN.DEF AskToClear()
 c$=bad$()+",,"+good$()
 m$="marked.txt":BUNDLE.CREATE b
 DO
  loadmarks(m$,&b):BUNDLE.KEYS b,l:LIST.SIZE l,z
  IF z<1 THEN D_U.BREAK
  FOR i=1 TO z
   LIST.GET l,i,r$
   LIST.REPLACE l,i,r$+WORD$(c$,ismarked(r$)+2,",")
  NEXT
  r=0:f$=AskListdone$(l,"Select items to Unmark",&r)
  f$=REPLACE$(f$,good$(),"")
  f$=REPLACE$(f$,bad$(),"")
  IF F$<>"done" & r>0 THEN ClearMark(f$):LIST.REMOVE l,r:LIST.SIZE l,z
 UNTIL F$="done"|z<1
 FN.RTN 0
FN.END

FN.DEF ParseCmd$(p$)
 !parse out commands in spoken string p$
 LET p$=TRIM$(LOWER$(p$))
 IF p$="show"|p$="list"|p$="recognition cancelled" THEN FN.RTN "SHOW"
 IF IS_IN("plus",p$)|IS_IN("Plus",p$)|IS_IN("+",P$) THEN FN.RTN "BULK"
 IF IS_IN("calories",P$)|IS_IN("points",P$)|IS_IN("point",P$) THEN FN.RTN "FOOD"
 IF p$="display"|p$="graphs" THEN FN.RTN "GRAPH"
 IF IS_IN("remove",p$) THEN FN.RTN "REMOVE"
 IF IS_IN("find",p$)|IS_IN("search",p$) THEN FN.RTN "SEARCH"
 IF p$="toggle start" THEN FN.RTN "START"
 IF IS_IN("toggle",p$) THEN FN.RTN "TOGGLE"
 ARRAY.LOAD cmd$[],"target","GOAL","email","EMAIL","clear","UNMARK","speech on","SPEAK","speech off","NOSPEAK","history","ARCHIVE","record","WEIGHT","quit","QUIT","notes","NOTES","remind","REMIND","auto","AUTO","analyze","ANALYZE","reset","RESET","blind mode","BLIND","mysterioso","SETTINGS","help","HELP","inspire me","PEP"
 ARRAY.SEARCH cmd$[],p$,i
 IF !i THEN FN.RTN "NONE"
 FN.RTN cmd$[i+1]
FN.END

FN.DEF savetolookupdb(f$,cal$)
 !record food to food lookup database fooddb.txt
 !moves most recent entry to top of list

 LET f$=TRIM$(f$):LET cr$=CHR$(10):LET db$=grab$("fooddb.txt")
 LET g=IS_IN(cr$+f$+"\t",cr$+db$)
 IF !g THEN aLn("fooddb.txt",f$+"\t"+cal$):FN.RTN VAL(cal$)

 !old food item gets updated calories
 LET r$=WORD$(MID$(db$,g,100),1,cr$)
 ! move food to the top of the list
 LET db$=f$+"\t"+cal$+cr$+REPLACE$(cr$+db$,cr$+r$+cr$,cr$)
 LET db$=REPLACE$(db$," "+cr$,cr$)
 DO
  LET db$=REPLACE$(db$,cr$+cr$,cr$)
 UNTIL !IS_IN(cr$+cr$,db$)
 LET db$=TRIM$(db$):CALL wLn("fooddb.txt",db$):FN.RTN readnumber(cal$)
FN.END

FN.DEF RecordWeight(weight,date$)
 BUNDLE.CREATE b
 LoadNumBundle("weight.txt",&b)
 BUNDLE.PUT b,date$,weight
 SaveNumbundle("weight.txt",b) 
 wset("current weight",weight)
 POPUP w$+" recorded"
FN.END

!punish user for eating too much!

FN.DEF punishment()
 POPUP "Be careful!That's a lot of calories for one item!"
 ARRAY.LOAD v[],0,300,300
 VIBRATE v[],-1:PAUSE 2000
FN.END

FN.DEF punish2()
 POPUP "You are over your goal!"
 ARRAY.LOAD v[],0,300,300
 VIBRATE v[],-1:PAUSE 2000
FN.END

!delete item from today's diary
!

FN.DEF DeleteItem()
 LIST.CREATE s,menu:LIST.CREATE s,idx:BUNDLE.GET 1,"db",db
 d$=rLn$("current")
 SQL.QUERY cursor,db,"diary","_id,food","date='"+d$+"'"
 SQL.QUERY.LENGTH n,cursor
 IF !n THEN FN.RTN 0
 FOR i=1 TO n
  SQL.NEXT done,cursor,cv1$,cv2$:LIST.ADD menu,cv2$:LIST.ADD idx,cv1$
 NEXT 
 LIST.ADD menu,"cancel"
 c=0:del$=AskList$(menu,"delete?",&c)
 IF !c|del$="cancel" THEN FN.RTN 0
 LIST.GET idx,c,v$:SQL.DELETE db,"diary","_id='"+v$+"'"
 wset("weekavg",weekavg())
FN.END

!Record food to todays diary

FN.DEF RecordFood(f$,c)
 BUNDLE.GET 1,"db",db:LET date$=TRIM$(rLn$("current")):LET f$=TRIM$(f$)
 IF f$="" THEN POPUP "error:no food name!":FN.RTN 0
 LET cals$=INT$(c)
 IF c>0 & date$=getdate$() THEN CALL wLn("atelast.txt",STR$(now()))
 SQL.INSERT db,"diary","food",f$,"calories",INT$(c),"date",date$
 CALL savetolookupdb(f$,cals$):wset("weekavg",weekavg())
 !punish user if more than 300 calories are eaten
 IF c>=300 THEN Punishment()
 LET g=rset("goal")
 IF g & rset("todaycal") > g THEN punish2()
FN.END

FN.DEF ProcessBulk(p$)
 LET p$=REPLACE$(p$," Plus "," plus "):LET p$=REPLACE$(p$,"+"," plus ")
 SPLIT foods$[],p$," plus ":CALL dm("g"):LET date$=rLn$("current")
 ARRAY.LENGTH z,foods$[]
 FOR i=1 TO z
  LET f$=TRIM$(foods$[i])
  g=Guess(f$)
  IF g=-99999 THEN g=0
  LET msg$="input cals/points for:"
  LET c=inputcals(msg$,f$,g)
  IF c>-99999 THEN CALL RecordFood(f$,c):POPUP f$+" "+INT$(c)+" added"
 NEXT
FN.END

FN.DEF del(f$)
 FILE.DELETE e,f$
FN.END

FN.DEF ResetAll()
 dm(""):DIALOG.MESSAGE ,"reset and erase entire app?",q,"yes","no"
 IF q<>1 THEN FN.RTN 1
 DIALOG.MESSAGE ,"are you sure?",q,"yes","no"
 IF q<>1 THEN FN.RTN 1
 del("settings.ini"):del("archive.txt"):del("archive1.txt")
 del("fooddb.txt"):del("marked.txt"):del("lastdate.txt")
 del("weight.txt"):del("notes.txt"):del("../databases/voCal")
 del("../databases/voCal-journal")
 POPUP "diary reset":POPUP "please restart voCal"
 EXIT
FN.END

FN.DEF analysis()
 showhtml(Analyze$())
FN.END

FN.DEF enterweight()
 IF askyn("log today's weight?")
  w=getweight("Enter weight:","lbs/kg",rset("current weight"))
  IF w<=0 THEN FN.RTN 1
  RecordWeight(w,date$):wset("current weight",w)
  dm("g"):SetFlag("relgraph")
 ENDIF
 PlotFile("weight.txt","date","weight","\t",1)
 ClearFlag("relgraph")
FN.END

FN.DEF notes()
 IF !isold("notes.txt")
  wLn("notes.txt","notes")
  aLn("notes.txt","-----")
  aLn("notes.txt","\n")
 ENDIF
 GRABFILE n$,"notes.txt"
 n$=htmledit$("notes",n$)
 wLn("notes.txt",n$)
FN.END

FN.DEF cleandb(e$)
 r$="\n":e$=REPLACE$(e$,":","\t"):e$=REPLACE$(e$,"\n",r$)
 e$=REPLACE$(e$,CHR$(13),r$)
 DO
  e$=REPLACE$(e$,r$+r$,r$)
 UNTIL !IS_IN(r$+r$,e$)
FN.END

FN.DEF PerformCommand(u$,lll)
 !PRE:u$= command keyword
 !     lll is list of voice data
 !POST:performs command u$ 
 !Popup u$
 CALL wLn("lastcmd",u$)
 IF u$="SHOW" THEN FN.RTN 0
 IF u$="NEXT" THEN LET DD=calcdate(rLn$("current"))+1:CALL wLn("current",juliantogreg$(dd)):FN.RTN 0
 IF u$="PREV" THEN LET DD=calcdate(rLn$("current"))-1:CALL wLn("current",juliantogreg$(dd)):FN.RTN 0
 IF IS_IN("TOGGLE",u$) THEN LET p$=REPLACE$(u$,"TOGGLE ",""):LET u$="TOGGLE"

 IF u$="VOICE"
  !find command or food
  LIST.SIZE lll,z
  LET i=1:LET found=0:LET cal=-99999
  IF z=0 THEN FN.RTN 0
  DO
   LIST.GET lll,i,p$
   LET u$=ParseCmd$(p$)
   IF u$<>"NONE" THEN LET found=1 ELSE LET cal=Guess(p$):LET found=(cal>-99999):i++
  UNTIL found|i>z
 ENDIF

 LET date$=getdate$()
 IF u$="NONE"
  IF cal=-99999
   LIST.ADD lll,"quit":r=0:LIST.SIZE lll,z
   IF z=2 THEN LIST.GET lll,1,p$ ELSE LET p$=AskList$(lll,"what did you say?",&r)
   IF P$="quit" THEN FN.RTN 1
  ENDIF
  IF cal=-99999 THEN LET cal=0
  LET m$="input cals/pts for:"
  LET cal=inputcals(m$,p$,cal)
  IF cal>-99999 THEN CALL RecordFood(p$,cal):POPUP p$+" "+INT$(cal)+" added"
  FN.RTN 1
 ENDIF
 IF u$="FOOD"
  LET C=readnumber2(p$):LET P$=stripdigits$(p$)
  LET nn$="zero,one,two,three,four,five,six,seven,eight,nine,ten,eleven,twelve,thirteen,fourteen,fifteen,sixteen,seventeen,eighteen,nineteen,twenty"
  FOR i=1 TO 21
   LET p$=REPLACE$(p$," "+WORD$(nn$,i,",")+" "," ")
  NEXT 
  LET p$=REPLACE$(p$," calories",""):LET p$=REPLACE$(p$," points","")
  LET p$=REPLACE$(p$," point",""):LET p$=TRIM$(p$):CALL RecordFood(p$,c)
  POPUP p$+" "+INT$(c)+" added"
  FN.RTN 1
 ENDIF
 IF u$="BLIND"
  ToggleSetting("blind")
  IF rset("blind")
   SetFlag("speak"):SetFlag("spkworks"):SetFlag("listen")
   POPUP "blind mode on":POPUP "please restart":EXIT
  ELSE
   POPUP "blind mode off"
  ENDIF
 ENDIF

 IF u$="TOGGLE"
  p$=REPLACE$(p$,"#","'")
  LIST.CREATE s,mm
  LIST.ADD mm,"good","bad","benign","copy to today","cancel"
  DIALOG.SELECT cc,mm,p$
  p$=TRIM$(p$):type=ismarked(p$)
  IF cc=5 THEN FN.RTN 0

  IF cc=4 
   old$=rln$("current")
   wLn("current",getdate$())
   cal=guess(p$)
   IF cal=-99999 THEN cal=0
   m$="input cals/pts for:"
   cal=inputcals(m$,p$,cal)
   IF cal=-99999 THEN POPUP "cancelled":FN.RTN 0
   recordfood(p$,cal)
   wLn("current",old$)
   POPUP p$+" copied"
   FN.RTN 0
  ENDIF
  IF cc=1 THEN markfood(p$,1):FN.RTN 0
  IF cc=2 THEN markfood(p$,-1):FN.RTN 0
  IF cc=3 THEN clearmark(p$):FN.RTN 0
  FN.RTN 0 
 ENDIF

 IF u$="HELP" THEN Help():FN.RTN 0
 IF u$="BULK" THEN CALL ProcessBulk(p$):FN.RTN 0
 IF u$="QUIT" THEN CALL wLn("ok","2"):FN.RTN 0
 IF u$="AUTO" THEN ToggleSetting("auto"):FN.RTN 0
 IF u$="START" THEN ToggleSetting("listen"):FN.RTN 0
 IF u$="UNMARK" THEN AskToClear():FN.RTN 0
 IF u$="ANALYZE" THEN Analysis():FN.RTN 0
 IF u$="EMAIL" THEN sendmail():FN.RTN 0
 IF u$="PEP" THEN PepTalk():FN.RTN 0
 IF u$="ARCHIVE" THEN Viewarchive():FN.RTN 0
 IF u$="TSPK"
  ToggleSetting("speak"):IF rset("speak") THEN TTS.INIT
  SetFlag("spkworks")
 ENDIF
 IF u$="SPEAK" THEN SetFlag("speak"):TTS.INIT:SetFlag("spkworks"):FN.RTN 0
 IF u$="NOSPEAK" THEN ClearFlag("speak"):SetFlag("spkworks"):FN.RTN 0
 IF u$="SEARCH" THEN UseWeb(p$):FN.RTN 0
 IF u$="GRAPH" THEN GraphsMenu():FN.RTN 0
 IF u$="NOTES" THEN notes():FN.RTN 0
 IF u$="TODAY" THEN wLn("current",getdate$()):FN.RTN 0
 IF u$="SETTINGS"
  GRABFILE s$,"settings.ini"
  S$=HTMLEDIT$("settings...edit carefully!",REPLACE$(s$,"\t",":"))
  IF s$<>"" THEN wLn("settings.ini",REPLACE$(s$,":","\t")):POPUP "settings updated"
  FN.RTN 0
 ENDIF

 IF u$="REMIND"
  rmd$=htmledit$("What will you do differently?",grab$("remind.txt"))
  wLn("remind.txt",rmd$)
  FN.RTN 0
 ENDIF

 IF u$="GOAL"
  g=rset("goal")
  a=weekavg()
  IF a>1000 THEN g=a
  LET Goal=inputint("set daily goal","cals/pts",a)
  IF goal>0 THEN  wset("goal",goal)
  FN.RTN 0
 ENDIF

 IF u$="REMOVE" THEN DeleteItem():FN.RTN 0
 IF u$="WEIGHT" THEN enterweight():FN.RTN 0
 IF u$="RESET"  THEN ResetAll():FN.RTN 0
 IF u$="EDITDB" THEN dbmenu("fooddb.txt")

FN.END

FN.DEF hasdec(n)
 FN.RTN FRAC(n)<>0
FN.END

!display numeric keypad and put gr object labels in pad[] array

!returns graphical key hit or @ if nothing pressed

!input number with onscreen keypad using prompts p$ and a$
!with default value of d
!returns -99999 if user cancels input
!NOTE:graphics must be opened and closed by calling function

FN.DEF inputfloat(p$,a$,d,search)
 DIM pad[15]:n$=INT$(d)
 msg$=p$+"<br>"+a$+"<br><a style=\"font-size:50px;color:#ff0;\">"+n$+"</a>"
 n=d:khit=0
 DO
  k$=hgetKEY$(msg$):khit=1
  IF k$="E" THEN D_U.BREAK
  n=0
  DO
   IF IS_IN(k$,"0123456789")
    IF dc & FRAC(n)=0
     n+=SGN(n)*VAL(k$)/10
    ELSE
     IF hasdec(n)
      PAUSE 100
     ELSE if ABS(n)<5000
      n=n*10+VAL(k$)
     ENDIF
    ENDIF
   ENDIF
   IF k$="x" THEN D_U.BREAK
   IF k$="-" THEN n=-n
   IF k$="." THEN dc=1
   IF k$="<"
    IF hasdec(n)
     n=FLOOR(ABS(n))*SGN(n)
    ELSE
     n=SGN(n)*FLOOR(ABS(n)/10)
    ENDIF
    dc=0
   ENDIF
   IF hasdec(n) THEN n$=STR$(n) ELSE n$=INT$(n)
   msg$=p$+"<br>"+a$+"<br><a style=\"font-size:50px;color:#ff0;\">"+n$+"</a>"
   k$=hgetKEY$(msg$)
  UNTIL k$="x"|k$="E"
 UNTIL k$="x"|k$="E"
 IF k$="x" THEN n=-99999
 FN.RTN n
FN.END

FN.DEF quickcal(p$,a$,d)
 !quick calorie entry 
 IF rset("blind") THEN FN.RTN ask(a$,INT$(Guess(a$)))
 q$="quickcal.txt"
 dflt$="0\n25\n50\n100\n150\n200\n250\n300\n400"
 DO
  IF !isold(q$) THEN wLn(q$,dflt$)
  FILE.EXISTS e,q$
  IF e THEN GRABFILE cl$,q$
  UNDIM cc$[]:SPLIT cc$[],cl$,"\n"
  LIST.CREATE s,cl:LIST.ADD cl,INT$(d),"custom","search web"
  LIST.ADD.ARRAY cl,cc$[]:LIST.ADD cl,"edit","cancel"
  c=0
  DO
   v$=asklist2$(cl,"choose cals/pts for "+a$,&c)
   IF v$="search web" THEN UseWeb(a$)
   IF v$="edit" THEN cl$=htmledit$("edit default list",cl$):wLn(q$,cl$)
  UNTIL v$<>"search web"
 UNTIL v$<>"edit list"
 IF c=0|v$="cancel" THEN FN.RTN -99999
 IF v$="custom" THEN FN.RTN inputfloat(p$,a$,d,1)

 IF IS_NUMBER(v$) THEN FN.RTN VAL(v$) ELSE FN.RTN -99999

FN.END

FN.DEF inputint(p$,a$,d)
 FN.RTN inputfloat(p$,a$,d,0)
FN.END

FN.DEF inputcals(p$,a$,d)
 FN.RTN quickcal(p$,a$,d)
FN.END

FN.DEF getweight(p$,a$,d)
 FN.RTN inputfloat(p$,a$,d,0)
FN.END

FN.DEF findmatch$(db$,s$)
 !find matching food name s$ in string db$
 m$=s$
 IF s$="" THEN FN.RTN ""
 x=IS_IN("\n"+s$,"\n"+db$)
 IF x THEN m$=WORD$(MID$(db$,x,x+60),1,"\t")
 FN.RTN m$
FN.END

FN.DEF autocomplete$(msg$,d$)
 goodkeys$=" '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTHVWXYZ"
 dm("g")
 GR.ORIENTATION 1:GR.SCREEN w,h:GR.TEXT.SIZE h/18:GR.TEXT.ALIGN 2
 GR.COLOR 255,0,255,0,1:GR.TEXT.DRAW p,w/2,h/8,msg$
 GR.COLOR 255,255,255,0,1:GR.TEXT.DRAW et,w/2,h/4,""
 GR.COLOR 255,0,255,0,1:GR.TEXT.DRAW tt,w/2,h/3,""
 GR.TEXT.DRAW g,w/2,h/2-h/14,"hit enter for match"
 GR.TEXT.DRAW g,w/2,h/2," or press . for new item":Render()
 FILE.EXISTS e,d$
 IF e THEN GRABFILE db$,d$
 ?:CLS:KB.SHOW
 !wait for key release
 DO
  INKEY$ k$:bk=(bkhit()|BACKGROUND())
  IF bk THEN D_U.BREAK
  PAUSE 100
 UNTIL k$="@"
 IF bk THEN FN.RTN ""
 DO
  DO
   INKEY$ k$:PAUSE 100
   bk=(bkhit()|BACKGROUND())
   IF bk THEN D_U.BREAK
  UNTIL k$<>"@"
  IF bk THEN D_U.BREAK
  IF k$="key 75" THEN k$="'"
  IF k$="key 56" THEN D_U.BREAK
  IF k$="key 66" THEN D_U.BREAK
  !del key
  IF k$="key 67" & LEN(s$)>0
   s$=MID$(s$,1,LEN(s$)-1)
  ENDIF
  IF IS_IN(k$,goodkeys$)
   INKEY$ n$:bk=(bkhit()|BACKGROUND())
   IF bk THEN D_U.BREAK
   IF n$="key 59" THEN k$=UPPER$(k$)
   s$+=k$
  ENDIF
  x$=findmatch$(db$,s$)
  IF x$="" THEN x$=s$
  GR.MODIFY et,"text",s$:m$=""
  IF x$<>"" THEN m$=""
  GR.MODIFY tt,"text",m$+x$:Render()
 UNTIL k$="key 66"
 IF bk THEN FN.RTN ""
 DO
  INKEY$ w$:PAUSE 100
  bk=(bkhit()|BACKGROUND())
  IF bk THEN D_U.BREAK
 UNTIL w$="@"
 IF bk THEN FN.RTN ""
 IF k$="key 56" 
  FN.RTN s$ 
 ELSE
  FN.RTN x$
 ENDIF
FN.END

FN.DEF cAskList$(l,msg$,c)
 dm("")
 DIALOG.SELECT c,l,msg$
 IF c THEN LIST.GET l,c,c$
 FN.RTN c$
FN.END

!show menu from list l with prompt msg$
!returns selection and assigns c to choice number

FN.DEF AskList$(l,msg$,c)
 dm("h")
 r$="<br>"
 h$+="<!DOCTYPE html><html lang=~en~><body><body style=~background-color:#000000;text-align:center;~>"
 h$+="<h1 style=~color:#999999~>"+msg$+"</h1>"+r$
 LIST.SIZE l,z
 h$+="<div>"
 FOR i=1 TO z
  LIST.GET l,i,s$
  s$=REPLACE$(s$,"'","'")
  s$=REPLACE$(s$,"<g>",chart$())
  but$="<button type=~button~ style=~background-color:#333;color:#FFF;text-align:center;font-size:20px;width:300px;height:50px;~ onclick=~DL('"+STR$(i)+"')~;>"+s$+"</button><br>"
  h$+=but$
 NEXT
 h$+="<br><br><br><br></div><script type=~text/javascript~> function DL(data) {Android.dataLink(data);}</script></body></html>"
 h$=REPLACE$(h$,"~","\"")
 HTML.LOAD.STRING h$
 r$=waitclick$(0)
 IF IS_NUMBER(r$)
  c=VAL(r$):LIST.GET l,c,s$
 ENDIF
 s$=REPLACE$(s$,"<g>","")
 FN.RTN s$
FN.END

FN.DEF GraphsMenu()
 ClearFlag("relgraph"):LIST.CREATE s,g
 LIST.ADD g,"help","notes","web","history","weight<g>","weekly exercise<g>","monthly exercise<g>","daily cals<g>","weekly cals<g>","monthly cals<g>","weekly items<g>","monthly items<g>","food frequency<g>","weekly stars<g>","monthly stars<g>","weekly frowns<g>","monthly frowns<g>","cals histogram<g>","items histogram<g>","cals vs items<g>","analysis","edit data","Leave Us Feedback!"
 LIST.ADD g,"done"
 DO
  c=0:c$=AskList$(g,"",&c)
  IF !c THEN FN.RTN 0
  SW.BEGIN c$
   SW.CASE "help"
    help():SW.BREAK
   SW.CASE "notes"
    notes():SW.BREAK
   SW.CASE "weight" 
    enterweight():SW.BREAK
   SW.CASE "web"
    useweb(""):SW.BREAK
   SW.CASE "daily cals"
    Graphcals(0):SW.BREAK
   SW.CASE "weekly exercise"
    GraphExercise():SW.BREAK
   SW.CASE "monthly exercise"
    GraphMonthExercise():SW.BREAK
   SW.CASE "weekly items"
    GraphItems():SW.BREAK
   SW.CASE "monthly items"
    graphmonthitems():SW.BREAK
   SW.CASE "weekly cals"
    GraphweekCals():SW.BREAK
   SW.CASE "monthly cals"
    GraphmonthCals():SW.BREAK
   SW.CASE "food frequency"
    PlotFood():SW.BREAK
   SW.CASE "cals histogram"
    Graphcals(1):SW.BREAK
   SW.CASE "weekly stars"
    GraphMarks(7,1):SW.BREAK
   SW.CASE "monthly stars"
    GraphMarks(30,1):SW.BREAK
   SW.CASE "weekly frowns"
    GraphMarks(7,-1):SW.BREAK
   SW.CASE "monthly frowns"
    GraphMarks(30,-1):SW.BREAK
   SW.CASE "items histogram"
    histitems():SW.BREAK
   SW.CASE "cals vs items"
    plotcalsvsitems():SW.BREAK
   SW.CASE "history"
    viewarchive():SW.BREAK
   SW.CASE "analysis"
    analysis():SW.BREAK
   SW.CASE "Leave Us Feedback!"
    BROWSE "https://play.google.com/store/apps/details?id=com.rfo.speakcc"
    SW.BREAK
   SW.CASE "edit data"
    dbmenu("fooddb.txt")
    SW.BREAK
  SW.END 
 UNTIL c$="done"
FN.END

FN.DEF showhtml(x$)
 IF x$="" THEN FN.RTN 0
 dq$="\""
 h$="<html><head><style>div {background:#567;"
 h$+="background:linear-gradient(to right bottom,#,#134);border-radius:6px;"
 h$+="text=~#ff0~ font:bold 30px Courier height:auto;}</style> </head>"
 h$+="<div><body text=~#ff0~ style=~font:bold 16px Courier~> "
 h$+="<body onclick=~window.location.href='test';~ style=~cursor:pointer;width:100%;height:100%;~>"
 h$+=x$:h$+="</body></div></html>":h$=REPLACE$(h$,"~",dq$)
 dm("h"):wLn("temp.html",h$):HTML.LOAD.STRING h$:PAUSE 2000:waitclick$(0)
FN.END

FN.DEF grn$(s$)
 !highlight html text in green
 FN.RTN "<span style=\"color:#0F0\">"+s$+"</span>"
FN.END

FN.DEF help()
 Bb$="<br><br>"
 dm("h"):w$="<html>"
 w$+="<head><style> div {background:#567;background:linear-gradient(to right bottom,#599,#135);"
 w$+="border-radius:6px;text=\"#ff0\" font:bold 30px Courier height:auto;}</style> </head>"
 w$+="<div><body text=\"#fff\" style=\"font:bold 18px Arial\">"
 w$+="<body onclick=\"window.location.href='test';\" style=\"cursor:pointer;width:100%;height:100%;\"><br>"
 w$+="<br>use the voice command "+grn$("HELP")+" to display this help page"+bb$
 w$+="Enter food energy values using either calories or points but only use one system.<br>say new foods in the format:<br>"
 w$+="food ### calories/points<br>ex:<br>"
 w$+="\"banana 100 calories\"<br>or   \"banana 2 points\" <br>"
 w$+="<br>Add new food by saying  the food name,wait and key in a value for it."+bb$+"If food was already entered,don't use the keyword calories and it will recall the last value. Type in a new value or wait 10 seconds and it will accept the recalled value."+bb$
 w$+="Record multiple foods at once by using the keyword "+grn$("PLUS")+"<br>"
 w$+="example:\"banana plus cereal plus milk\""+bb$
 w$+="You can exit the main diary screen by tapping on the middle of the screen."+bb$+"Other commands:"+bb$
 w$+="say "+GRN$("TARGET")+" to enter in daily energy goal"+bb$
 w$+=GRN$("CLEAR")+" to unmark good foods. You can also toggle stars by clicking to the left of each food name on the calorie/point numbers in the food diary"+bb$
 w$+=GRN$("QUIT")+" to exit<br>"
 w$+="In autoquit mode app will also autoexit after each completed food entry"+bb$
 w$+=GRN$("SHOW")+" to display today's diary.  You can also click the speech cancel button to bring up the diary screen."+bb$
 w$+=GRN$("SPEECH ON")+" or "+GRN$("SPEECH OFF")+" to turn on/off text-to-speech calorie announcement"+bb$
 w$+=GRN$("REMOVE")+" to remove last entry"+bb$
 w$+=GRN$("RECORD")+" to record today's weight"+bb$
 w$+=GRN$("SEARCH")+" to look up calories on the web"+bb$
 w$+=grn$("NOTES")+" to record/view notes in diary"+bb$
 w$+=grn$("REMIND")+" to set personal reminder banner message"+bb$
 w$+=grn$("HISTORY")+" to view past days' food entries"+bb$
 w$+=grn$("DISPLAY")+" to show graphs of weight,daily calories and other items"+bb$
 w$+=grn$("ANALYZE")+" to analyze which foods in your history are associated with eating less or more daily calories"+bb$
 w$+=grn$("EMAIL")+" to email weight log,archive or food database"+bb$
 w$+=grn$("INSPIRE ME")+" to read your reminders out randomly"+bb$
 w$+=grn$("RESET")+" to clear entire app ***USE CAREFULLY***"+bb$

 w$+="Privacy Policy"+bb$

 w$+="We respect your privacy and do not collect or share your personal information with other parties."+bb$

 w$+="We do not use cookies or advertising,however if you link to an outside service/webpage such as Google,YouTube,bing etc. from within our apps,they probably do."+bb$

 w$+="You are able to optionally share some information (i.e. diet logs or grocery lists) with others via email functionality built into some of our apps. These require you to use your own email app to send data to third parties of your choice."+bb$

 w$+="Most databases and temporary files in our apps are NOT encrypted and can be used and edited yourself using SQLITE and text editors."+bb$

 w$+="Some of our applications (such as Voice Calorie Counter and Done List) use a Microphone with Google Speech Recognition which may involve sending sound samples of your voice to Google."+bb$

 w$+="The privacy policy for Google can be found here:"+bb$

 w$+="https://privacy.google.com/index.html"+bb$+"</body></div></html>"
 HTML.LOAD.STRING w$:PAUSE 2000:waitclick$(0)
FN.END

FN.DEF sortfood(ft$[],n)
 IF n<2 THEN FN.RTN 0
 !? "sorting "+INT$(n)+" items"
 DIM x[n],y$[n]
 FOR i=1 TO n
  x[i]=VAL(ft$[i,2]):y$[i]=ft$[i,1]
 NEXT
 FOR i=1 TO n-1
  ARRAY.MIN mn,x[i,n-i+1]:ARRAY.SEARCH x[i,n],mn,pos
  SWAP x[i],x[pos+i-1]:SWAP y$[i],y$[pos+i-1]
 NEXT
 FOR I=1 TO n
  ft$[i,2]=INT$(x[i]):ft$[i,1]=y$[i]
 NEXT
FN.END

FN.DEF showtable$(ft$[],nn)
 a$="":z$="<br>"
 TAB$="   ":start=0
 DO
  start++:s$=ft$[start,2]
  v=0
  IF s$<>"" THEN v=VAL(s$)
 UNTIL v<>0
 a$+="undereating foods"+z$
 a$+="<table><tr><th>avg</th><th>food</th></tr>"
 FOR i=start TO MIN(start+49,nn)
  c$=ft$[i,2]:f$=ft$[i,1]
  IF c$<>"0"
   a$+="<tr><td>"+c$+"</td><td>"+f$+"</td></tr>"
  ENDIF
 NEXT
 a$+="</table>"+z$+"overeating foods"+z$
 a$+="<table><tr><th>avg</th><th>food </th></tr>"
 FOR i=MAX(1,nn-49) TO nn
  c$=ft$[i,2]:f$=ft$[i,1]
  IF c$<>"0"
   a$+="<tr><td>"+c$+"</td> <td>"+f$+"</td></tr>"
  ENDIF
 NEXT
 a$+="</table>"
 FN.RTN a$
FN.END

FN.DEF analyze$()
 BUNDLE.GET 1,"db",db:dm(""):? "analysis..."
 BUNDLE.CREATE daytot:BUNDLE.CREATE foodsum:BUNDLE.CREATE foodct
 !calculate daily calorie totals
 now=FLOOR(now())
 FOR d=now-365 TO now
  d$=juliantogreg$(d):n=0:s=0:DateTotal(d$,&n,&s)
  tallybundle(&daytot,d$,s)
 NEXT 
 FOR i=now-365 TO now
  d$=juliantogreg$(i)
  SQL.QUERY c,db,"diary","food","date='"+d$+"'"
  SQL.QUERY.LENGTH n,c
  FOR j=1 TO n
   SQL.NEXT done,c,cv$
   cv$=stripdigits$(cv$)
   tallybundle(foodct,cv$,1)
   BUNDLE.GET daytot,d$,t
   tallybundle(foodsum,cv$,t)
  NEXT
 NEXT
 BUNDLE.KEYS foodsum,foodlist
 LIST.SIZE foodlist,nn
 ? "tallied"
 BUNDLE.CREATE foodavg
 !calculate out average daily calorie for each food
 k=0
 FOR i=1 TO nn
  LIST.GET foodlist,i,f$:BUNDLE.GET foodsum,f$,s:BUNDLE.GET foodct,f$,n
  IF n>=5 THEN BUNDLE.PUT foodavg,f$+"("+INT$(n)+"x)",FLOOR(s/n):k++
 NEXT
 IF k<10 THEN POPUP "not enough data yet!":FN.RTN ""
 ?INT$(k)+" averages calculated"
 !copy food average bundle into array ft$
 BUNDLE.KEYS foodavg,keys
 DIM ft$[k,2]
 FOR i=1 TO k
  LIST.GET keys,i,f$:BUNDLE.GET foodavg,f$,c
  ft$[i,1]=f$:ft$[i,2]=INT$(c)
 NEXT
 !sort table by ascending calories
 IF k>=10
  sortfood(&ft$[],k):? "sort done":PAUSE 2000:CLS
  FN.RTN showtable$(ft$[],k)
 ELSE
  FN.RTN "not enough entries yet!"
 ENDIF
FN.END

FN.DEF archivereport$()
 good$="+":bad$="-"
 BUNDLE.CREATE fb:LIST.CREATE s,tt
 z$="<br>"
 backdays=inputint("go back","how many days? ",30)
 CLS
 !? "loading... "
 now=FLOOR(now()):sd=now-backdays
 FOR d=sd TO now
  t=0:d$=juliantogreg$(d):diary$=grabday$(d$)
  IF LEN(diary$)<5 THEN F_N.CONTINUE
  LIST.ADD tt,d$,"  ":UNDIM dd$[]:SPLIT dd$[],diary$,"\n"
  ARRAY.LENGTH z,dd$[]
  FOR i=1 TO z
   r1$=WORD$(dd$[i],1,"\t"):r2$=WORD$(dd$[i],2,"\t"):r3$=WORD$(dd$[i],3,"\t")
   tallybundle(fb,r1$,1):c=val2(r2$)

   m=ismarked(r1$)
   IF m=1 THEN r1$+=good$
   IF m=-1 THEN r1$+=bad$
   t+=c
   LIST.ADD tt,frmcal$(c):LIST.ADD tt,r1$
  NEXT
  LIST.ADD tt,"----","-----------------------",frmcal$(t),INT$(z)+" items","    ","  "
 NEXT
 LIST.SIZE tt,z
 IF !z
  POPUP "not enough data!":FN.RTN ""
 ENDIF
 UNDIM tbl$[]:LIST.TOARRAY tt,tbl$[]
 x$="":ARRAY.LENGTH n,tbl$[]
 Sp$="      "
 FOR i=1 TO n-1 STEP 2
  x$+=tbl$[i]+" "+tbl$[i+1]+z$
 NEXT
 x$+=z$+z$+"food frequency (>3)"+z$
 !Get most popular foods
 BUNDLE.KEYS fb,keys:LIST.SIZE keys,z:DIM xx$[z,2]
 zz=0
 FOR i=1 TO z 
  LIST.GET keys,i,f$:BUNDLE.GET fb,f$,freq
  IF freq>3 THEN zz++:xx$[zz,1]=f$:xx$[zz,2]=INT$(freq)
 NEXT
 sortfood(&xx$[],zz)
 FOR i=zz TO 1 STEP -1
  x$+=xx$[i,2]+"x "+xx$[i,1]+z$
 NEXT
 FN.RTN x$+"<br>"+ss$
FN.END

FN.DEF viewarchive()
 X$=archivereport$():Xx$=REPLACE$(x$,"<br>","\n"):Xx$=REPLACE$(xx$,"&nbsp;"," ")
 CLIPBOARD.PUT xx$:POPUP "copied to clipboard":Showhtml(x$)
FN.END

FN.DEF emailarchive$()
 s$=archivereport$():s$=REPLACE$(s$,"<br>","\n"):s$=REPLACE$(s$,"&nbsp;"," ")
 FN.RTN s$
FN.END

FN.DEF SENDMAIL()
 dm("")
 IF Isold("email.txt")
  em$=rLn$("email.txt")
 ELSE
  INPUT "enter default email to send data to",em$,em$,cn
  IF cn THEN FN.RTN 0
  wLn("email.txt",em$)
 ENDIF
 TIME Year$,Month$,Day$,Hour$,Minute$,Second$,WeekDay,isDST
 Today$=month$+"-"+day$+"-"+year$
 DO
  DIALOG.MESSAGE ,"email to "+em$,c,"yes","edit","no"
  IF c=2
   INPUT "change email",em$,em$,cn
   IF cn THEN D_U.BREAK
   wLn("email.txt",em$)
  ENDIF
 UNTIL c<>2
 IF emcancel THEN FN.RTN 0
 IF c=3 THEN FN.RTN 0
 LIST.CREATE s,gr
 LIST.ADD gr,"weight log","archive","food db","cancel"
 DO
  c=0:AskList$(gr,"choose email type",&c)
  IF c=4 THEN D_U.BREAK
  IF c=1 & isold("weight.txt")
   GRABFILE f$,"weight.txt"
   EMAIL.SEND em$,"weight log "+today$,f$
  ENDIF
  IF c=2 
   f$=emailarchive$()
   IF f$<>"" THEN EMAIL.SEND em$,"archive "+today$,f$
  ENDIF
  IF c=3 & isold("fooddb.txt")
   GRABFILE f$,"fooddb.txt"
   EMAIL.SEND em$,"fooddb "+today$,f$
  ENDIF
  DO
   PAUSE 100
  UNTIL !BACKGROUND()
 UNTIL c=4
FN.END

FN.DEF EDIT$(p$,s$)
 TEXT.INPUT s$,s$,p$:FN.RTN s$
FN.END

FN.DEF htmledit$(p$,s$)
 !text.input now works better
 dm("")
 TEXT.INPUT s$,TRIM$(s$)+"\n"
 FN.RTN TRIM$(s$)

 dm("h")
 w$="<html> <head><meta http-equiv=\"content-type\"content=\"text/html;charset=UTF-8\"/>"
 w$+="<title>edit text</title> </head><script type=\"text/javascript\">"
 w$+="function DL(data) {Android.dataLink(data);} </script>"
 w$+="<body bgcolor=\"black\"><div align=\"left\">"
 w$+="<h2 style=\"color:#aaa;\">###prompt</h2>"
 w$+="<form id='main' method='get' action='FORM'>"
 w$+="<input type='submit' style=\"float:right;\" name='submit' value='Done'/>"
 w$+="<TEXTAREA NAME=\"id\" ROWS=18 COLS=35>"
 w$+="###edit"
 w$+="</TEXTAREA></form></div></body></html>"
 w$=REPLACE$(w$,"###prompt",p$):w$=REPLACE$(w$,"###edit",TRIM$(s$))
 HTML.LOAD.STRING w$
 HTML.LOAD.URL "javascript:DL(document.getElementById('id'))h
 r$=waitclick$(0)
 IF r$="QUIT" THEN FN.RTN S$
 s$=DECODE$("URL","UTF-8",r$):s$=REPLACE$(s$,"SUBMIT&submit=Done&id=",""):s$=LEFT$(s$,LEN(s$)-1)
 FN.RTN s$
FN.END

FN.DEF hear(vl)
 LET v$="say food name,command,Help or hit cancel"
 CALL dm("")
 STT.LISTEN v$
 STT.RESULTS vl
 CALL cleanvoice(vl)
FN.END

FN.DEF dbmenu(db$)
 LIST.CREATE s,m
 LIST.ADD m,"edit lookup db","erase lookup db","import food db","edit weight log","cancel"
 DO
  p$="select choice":c=0:c$=AskList$(m,p$,&c)
  IF !c THEN FN.RTN 0
  IF c$="edit lookup db" THEN editdb(db$)
  IF c$="erase lookup db"
   IF askyn("are you sure???")
    FILE.DELETE e,db$:POPUP "deleted"
   ELSE
    POPUP "del canceled"
   ENDIF
  ENDIF
  IF c$="import food db"
   b$=choosefile$("sampledb/"):f$=grab$(b$):aLn(db$,f$):editdb(db$):POPUP "database imported!"
  ENDIF
  IF c$="edit weight log" THEN editdb("weight.txt"):SetFlag("changed")
 UNTIL c$="cancel"
FN.END

!Main

CALL LoadSettings()
IF IsApk() 
 BUNDLE.GET 1,"version",nv
 IF nv<>rset("version")
  wset("version",nv)
 ENDIF
 LET h$="html/images/"
 ARRAY.LOAD htmlRes$[],"autoc.png","graphs.png","green.png","red.png", "yellow.png"
 ARRAY.LENGTH nres,htmlRes$[]
 FOR i=1 TO nres
  CALL PutResOnSd(h$+htmlRes$[i])
 NEXT
 CALL PutResOnSd("sampledb/samplefooddb.txt")
ENDIF

CALL init()

main:

LIST.CREATE s,vl
CALL ShowDiary()
GOSUB declare
LET r$=waitclick$(1)
IF r$="QUIT" 
 IF rset("listen") THEN GOTO leave
 EXIT
 !HOME mode not working
 HOME

 DO
  PAUSE 1000
 UNTIL !BACKGROUND()
 date$=getdate$():wLn("current",date$)
 GOTO main
ENDIF

!did user hit the floating button?
IF r$="VOICE" 
 LIST.CREATE s,vl
 hear(&vl)
ENDIF
IF r$="STT" THEN r$="VOICE":STT.RESULTS vl
IF r$<>"QUIT" THEN CALL PerformCommand(r$,vl)
!keyboard entry
IF r$="TYPE"
 r$="VOICE":LET f$=autocomplete$("Enter food name:","fooddb.txt"):dm("h")
 IF f$<>"" THEN LIST.ADD vl,f$:CALL PerformCommand(r$,vl)
ENDIF
GOTO main
!interrupts

Declare:
IF declared THEN RETURN
!
!
!
!
! GRAPHS 
!

INCLUDE "VoCal Graphs.bas"
Declared=1
RETURN

!
!
!
!

ONBACKKEY:
BUNDLE.PUT 1,"back","1"
BACK.RESUME

ONCONSOLETOUCH:
ct=1
IF bready
 BUNDLE.PUT 1,"ctouch",1
ENDIF
CONSOLETOUCH.RESUME

ONBACKGROUND:
IF bready
 IF BACKGROUND()
  BUNDLE.PUT 1,"home",0
 ELSE
  BUNDLE.PUT 1,"home",1
 ENDIF
ENDIF
BACKGROUND.RESUME

leave:
!dm("")
BUNDLE.GET 1,"db",db
SQL.CLOSE db
EXIT
