! VocalGraphs.bas
! by mookiebearapps
!
!
!
! GRAPHS begin here
!
!

FN.DEF median(y[])
 ARRAY.LENGTH n,y[]:DIM x[n]:ARRAY.COPY y[],x[]:ARRAY.SORT x[]:ARRAY.LENGTH i,x[]
 IF i/2<>FLOOR(i/2)
  M=x[FLOOR((i+1)/2)]
 ELSE
  M=(x[FLOOR((i)/2)]+x[FLOOR((i)/2)+1])/2
 ENDIF
 UNDIM x[]:FN.RTN m
FN.END

FN.DEF frmgraph$(n,prec)
 IF prec>=0 THEN FN.RTN TRIM$(FORMAT$("#######",n))
 IF prec=-1 THEN FN.RTN TRIM$(FORMAT$("#######.#",n))
 IF prec=-2 THEN FN.RTN TRIM$(FORMAT$("#######.##",n))
 FN.RTN TRIM$(FORMAT$("#######.###",n))
FN.END

! bar graph
FN.DEF bar(d[],l$[],title$)
! make bars red/green or green/red depending on type
 star=IS_IN("star",title$)|IS_IN("burned",title$)
 GR.SCREEN w,h
 GR.CLS
 WAKELOCK 3
 xo=0 :yo=-h/10
 x1=w*0.1:x2=w*0.9
 y1=h*0.1:y2=h*0.7
 dx=ABS(x2-x1) :dy=ABS(y2-y1) :dy2=0.15*dy
 ARRAY.MAX max,d[]
 ARRAY.AVERAGE avg,d[]
 ARRAY.LENGTH l,d[]
 med=median(d[])
 bw=dx/(l*4)
 g=50
 GR.COLOR 255,g,g,g,1
 GR.RECT gn,x1-xo-2,y1-yo-dy2-2,x2-xo+2,y2-yo+2
 GR.COLOR 255,255,255,255,0
 GR.SET.STROKE 2
 GR.RECT gn,x1-xo-2,y1-yo-dy2-2,x2-xo+2,y2-yo+2
 xspace=(x2-x1)/l
 GR.TEXT.SIZE dy/50
 GR.TEXT.ALIGN 2

 FOR i=1 TO l

  IF (d[i]>med & !star)|(star & d[i]<med)
   GR.COLOR 255,255,0,0,1
  ELSE
   GR.COLOR 255,0,255,0,1
  ENDIF
  t$=l$[i]
  bh=d[i]*dy/max
  GR.RECT gn,x1-xo+xspace*(i-0.5)-bw,y2-yo-bh,x1-xo+xspace*(i-0.5)+bw,y2-yo-2
  GR.COLOR 255,255,255,255,1
  GR.LINE gn,x1-xo+xspace*(i-0.5),y2-yo+10,x1-xo+xspace*(i-0.5),y2-yo

  GR.ROTATE.START 270,x1-xo+xspace*(i-0.5),y2-yo+dy/10
  GR.TEXT.DRAW gn,x1-xo+xspace*(i-0.5),y2-yo+dy/10,t$
  GR.ROTATE.END
  !t$=str$(round(d[i],1))
  t$=INT$(d[i])

  GR.ROTATE.START 270,x1-xo+xspace*(i-0.5),y2-yo-0.05*dy

  GR.TEXT.DRAW gn,x1-xo+xspace*(i-0.5),y2-yo-0.05*dy,t$
  GR.ROTATE.END
 NEXT
 GR.TEXT.SIZE dy/25
 GR.TEXT.TYPEFACE 4,1
 GR.TEXT.DRAW gn,x1+dx/2-xo,y1-yo-10,title$
FN.END


FN.DEF drawgraph(x[],y[],xname$,yname$,r)
! draw bargraph or scatter plot
! if r then scale y axis relative to max value
 dm("g")
 ARRAY.LENGTH z,x[]
 IF z>30
  ScatterPlot(x[],y[],xname$,yname$,r)
  FN.RTN 0
 ENDIF
 IF z <2 THEN FN.RTN 0
 DIM x$[z]
 FOR i=1 TO z
  x$[i]=Juliantogreg$(x[i])
 NEXT
 bar(y[],x$[],yname$)
 call Render()

 DO
  PAUSE 200
  GR.TOUCH t,x,y
 UNTIL t
 WAKELOCK 5
 DIALOG.MESSAGE yname$,stat$(y[]),c,"ok"

FN.END

FN.DEF ScatterPlot(x[],y[],xname$,yname$,graphweight)
 !PRE:x[],y[] are scatter plot points 
 !      xname$,yname$ are axis titles
 !	    graphweight=0 :plot absolute values
 !      graphweight=1 :plot relative to maximum y value (i.e. see weight loss)
 dm("g2"):today=calcdate(getdate$()):wLn("back.txt","0")

 relmode=rset("relgraph"):ARRAY.LENGTH n,x[]
 IF n<2 THEN POPUP "need at least 2 points to make graph":FN.RTN 0
 ARRAY.MIN xmin,x[]:ARRAY.MAX xmax,x[]:ARRAY.MIN yymin,y[]
 IF graphweight=2 THEN xaxis=1 ELSE ymin=yymin
 ARRAY.MAX ymax,y[]:ARRAY.AVERAGE xavg,x[]:ARRAY.AVERAGE yavg,y[]
 ARRAY.STD_DEV xsd,x[]:ARRAY.STD_DEV ysd,y[]
 xrange=xmax-xmin:yrange=ymin-ymax:xxbar=0:Yybar=0:xybar=0
 FOR i=1 TO n
  xybar+=(x[i]*y[i]):xxbar+=(x[i]*x[i]):Yybar+=(y[i]*y[i])
 NEXT
 xybar=xybar/n:xxbar=xxbar/n:Yybar=yybar/n
 slope=(xybar-xavg*yavg)/(xxbar-xavg*xavg):intercept=yavg-slope*xavg
 !Rr=(xybar-xavg*yavg)/ SQR((xxbar-xavg*xavg)*(yybar-yavg*yavg))
 !Rr=rr*rr
 pxmax=xmax+FLOOR(xrange*0.1):pymax=ymax*1.02
 pxmin=xmin-FLOOR(xrange*0.2):pymin=ymin*0.98
 DO
  flip=0:GR.CLS:GR.SCREEN acwidth,acheight
  diwidth=(pxmax-pxmin):diheight=(pymax-pymin)
  IF (diwidth=0|diheight=0) THEN POPUP "not enough data":D_U.BREAK
  Sx=acwidth/diwidth:Sy=acheight/diheight
  GR.SET.STROKE 1:GR.COLOR 200,255,200,0,1
  Ny=(pymax-(yavg-ysd))*sy
  GR.LINE yyy,1,ny,(pxmax-pxmin)*sx,ny 
  Ny=(pymax-(yavg+ysd))*sy
  GR.LINE yyy,1,ny,(pxmax-pxmin)*sx,ny 
  ytics=10^FLOOR(LOG10(ABS(pymax-pymin)/10))
  IF (pymax-pymin)/ytics > 10 THEN ytics*=10
  IF (pymax-pymin)/ytics < 5 THEN ytics=MAX(FLOOR(ytics/5),1)
  Yline1=FLOOR(pymin/ytics)*ytics
  Yline2=CEIL(pymax/ytics)*ytics
  GR.TEXT.SIZE 15:GR.SET.STROKE 1:GR.TEXT.ALIGN 1
  py=FLOOR(LOG10(ABS(ytics)))
  FOR y=yline1 TO yline2 STEP ytics
   GR.COLOR 50,50,50,50,1
   Newx=(pxmax-pxmin)*sx:Newy=(pymax-y)*sy
   GR.LINE yyy,1,newy,newx,newy 
   GR.COLOR 200,50,50,50,1
   IF relmode
    GR.TEXT.DRAW txt3,1,newy-3,INT$(y-ymax)
   ELSE
    GR.TEXT.DRAW txt3,1,newy-3,frmgraph$(y,py)
   ENDIF
  NEXT
  xtics=10^FLOOR(LOG10(ABS(pxmax-pxmin)/10))
  DO
   IF (pxmax-pxmin)/xtics > 7 THEN xtics*=7
  UNTIL xtics=0|((pxmax-pxmin)/xtics <=10)
  IF (pxmax-pxmin)/xtics < 5 THEN xtics=MAX(FLOOR(xtics/5),1)
  xline1=FLOOR(pxmin/xtics)*xtics
  xline2=CEIL(pxmax/xtics)*xtics
  !px=FLOOR(LOG10(ABS(xtics)))
  GR.TEXT.ALIGN 3
  FOR x=xline1 TO xline2 STEP xtics
   Newx=(x-pxmin)*sx:Newy=(pymax-pymin)*sy*0.97
   ! g$=juliantogregorian$(x)
   IF xaxis THEN g$=INT$(x) ELSE g$=INT$(x-today)
   GR.COLOR 200,0,0,0,1:GR.TEXT.DRAW txt3,newx,newy,g$:GR.COLOR 50,0,0,0,1:GR.LINE xxx,newx,1,newx,newy 
  NEXT
  GR.SET.STROKE 5
  Oldx=(x[1]-pxmin)*sx:Oldy=(pymax-y[1])*sy
  GR.COLOR 200,0,0,200,1
  FOR i=1 TO n
   Newx=(x[i]-pxmin)*sx:Newy=(pymax-y[i])*sy
   GR.COLOR 50,0,0,200,1:GR.CIRCLE p,newx+5,newy+5,5:GR.COLOR 200,0,0,200,1
   !  GR.line lol,oldx,oldy,newx,newy 
   ! GR.POINT p,newx,newy
   GR.CIRCLE p,newx,newy,5
   !  Oldx=newx:Oldy=newy
  NEXT
  IF slope <=0 THEN GR.COLOR 80,0,255,0,1 ELSE GR.COLOR 80,255,0,0,1
  GR.SET.STROKE 9
  X1=pxmin:Y1=slope*x1+intercept:Xn=pxmax:Yn=slope*xn+intercept
  GR.LINE fl,(x1-pxmin)*sx,sy*(pymax-y1),sx*(xn-pxmin),sy*(pymax-yn)
  GR.TEXT.SIZE 25:GR.COLOR 200,50,50,50,1:GR.SET.STROKE 1:GR.TEXT.ALIGN 3
  IF xaxis
   s=slope:s$=""
  ELSE
   s=slope*7:s$="/week"
  ENDIF
  GR.TEXT.DRAW txt3,acwidth*0.9,acheight*0.1,frmgraph$(s,-2)+s$
  GR.TEXT.ALIGN 2:GR.COLOR 255,0,0,0,1
  GR.TEXT.DRAW txt2,acwidth*0.5,acheight*0. 93,xname$
  GR.ROTATE.START -90,acwidth*0.1,acheight*0.5
  GR.TEXT.DRAW txt2,acwidth*0.05,acheight*0.5,yname$
  GR.ROTATE.END:call Render()

  WAKELOCK 3
  Cr$="\n":px=-2:py=-2
  msg$=yname$+cr$
  Msg$+=INT$(n)+" entries  in "+INT$(xrange+1)+" days"+cr$
  msg$+="Average= "+frmgraph$(yavg,py)+cr$
  msg$+="Median = "+frmgraph$(median(y[]),-1)+cr$+"Standard Deviation = "+frmgraph$(ysd,py)+cr$+"Min = "+frmgraph$(yymin,py)+cr$+"Max = "+frmgraph$(ymax,py)+cr$+"range = "+frmgraph$(ymax-yymin,py)+cr$
  DO
   PAUSE 100:bk=(bkhit()|BACKGROUND())
   IF bk THEN D_U.BREAK 
   GR.TOUCH touched,xx,yy:GR.SCREEN newwidth,newheight
   IF newwidth<>acwidth THEN flip=1:D_U.BREAK
  UNTIL touched 
  IF bk THEN D_U.BREAK 
 UNTIL !flop
 IF bk 
  WAKELOCK 5
  FN.RTN 0
 ENDIF
 IF msg$<>"" THEN DIALOG.MESSAGE ,msg$ ,c,"okay"
 dm("h")
 WAKELOCK 5
 FN.RTN 0
FN.END

!plot delimited file f$
!plot y axis relative to ymax if r is TRUE
!

FN.DEF PlotFile(f$,xname$,yname$,dlm$,r)
 IF !iSOLD(f$)THEN FN.RTN 0
 TEXT.OPEN R,FN2,f$:xfield=1:yfield=2
 LIST.CREATE n,xx:LIST.CREATE n,yy
 DO
  TEXT.READLN FN2,a_line$:TEXT.EOF fn2,eof
  IF !eof&a_line$<>""
   d$=WORD$(a_line$,xfield,dlm$):x=calcdate(&d$)
   IF x>0 THEN LIST.ADD xx,x:LIST.ADD yy,val2(WORD$(a_line$,yfield,dlm$))
  ENDIF
 UNTIL eof
 TEXT.CLOSE FN2:UNDIM x[]:UNDIM y[]:LIST.SIZE xx,xn:LIST.SIZE yy,yn
 IF !xn|!yn THEN POPUP "no points":FN.RTN 0
 LIST.TOARRAY xx,x[]:LIST.TOARRAY yy,y[]
 ScatterPlot(x[],y[],xname$,yname$,r)
 LIST.CLEAR xx:LIST.CLEAR yy
FN.END

FN.DEF PlotBundle(b,xname$,yname$,r)
 LIST.CREATE n,l2:LIST.CREATE n,l1
 BUNDLE.KEYS b,lll:LIST.SIZE lll,z
 IF z<2
  POPUP "need more points":FN.RTN 0
 ENDIF
 UNDIM ll$[]:LIST.TOARRAY lll,ll$[]:ARRAY.SORT ll$[]
 FOR i=1 TO z
  K$=ll$[i]:BUNDLE.GET b,k$,v:x=VAL(k$)
  IF x>0 THEN LIST.ADD l1,x:LIST.ADD l2,v
 NEXT
 UNDIM x[]:UNDIM y[]:LIST.TOARRAY l1,x[]:LIST.TOARRAY l2,y[]
 drawgraph(x[],y[],xname$,yname$,r)
 UNDIM x[]:UNDIM y[]:LIST.CLEAR l1:LIST.CLEAR l2:UNDIM ll$[]:BUNDLE.CLEAR lll
FN.END

FN.DEF calcperiodCals(b,p)
 BUNDLE.GET 1,"db",db
 now=FLOOR(now())-1
 FOR d=now-365 TO now
  LET d$=juliantogreg$(d):LET n=0:LET w$=INT$(FLOOR(d/p)*p):LET t=0:CALL DateTotal(d$,&n,&t)
  IF t THEN CALL tallybundle(&b,w$,t)
 NEXT
FN.END

FN.DEF GraphweekCals()
 BUNDLE.CREATE b:calcperiodcals(&b,7):PlotBundle(b,"week","cals/pts",0)
FN.END

FN.DEF GraphmonthCals()
 BUNDLE.CREATE b:CALL calcperiodcals(&b,30):PlotBundle(b,"30 days","cals/pts",0):BUNDLE.CLEAR b
FN.END

FN.DEF plotcalsvsitems()
 !graph of total daily calories vs number of items eaten
 BUNDLE.GET 1,"db",db
 LIST.CREATE n,xx:LIST.CREATE n,yy
 startd=FLOOR(now())-1
 BUNDLE.CREATE b:DIM x[366],y[366]
 FOR d=startd-365 TO startd
  n=0:t=0
LET d$=juliantogreg$(d)
CALL DateTotal(d$,&n,&t):t+=exercise(d$)

  LET c=0:IF n THEN LET c=t
  LET j=d-startd+366
  IF n THEN LIST.ADD xx,n:LIST.ADD yy,c
 NEXT 
 LIST.SIZE xx,z
 IF z
  LIST.TOARRAY xx,x[]:LIST.TOARRAY yy,y[]
  ScatterPlot(x[],y[],"items","daily cals",2)
 ELSE 
  POPUP "not enough data!"
 ENDIF
 BUNDLE.CLEAR b:LIST.CLEAR xx:LIST.CLEAR yy:UNDIM x[]:UNDIM y[]
FN.END

FN.DEF calccals(b,startd)
 !PRE:startd = julian date of starting date
 !POST:bundle is assigned date,total calories
 BUNDLE.GET 1,"db",db
 LET now=FLOOR(now())-1
 FOR d=startd TO now
  LET d$=juliantogreg$(d):LET n=0:LET t=0:CALL DateTotal(d$,&n,&t)
  IF t THEN BUNDLE.PUT b,INT$(d),t
 NEXT
FN.END

FN.DEF GraphCals(histo)
 !graph daily calories or histogram if histo=1
 IF histo THEN d=365 ELSE d=7
 g=inputint("days to go back","",d)
 startd=calcdate(getdate$())-g
 BUNDLE.CREATE b
 calccals(&b,startd)
 IF !histo
  PlotBundle(b,"days ago","cals/pts",0)
 ELSE
  SaveNumbundle("cals.txt",b)
  plothist("cals.txt","cals",2)
 ENDIF
 BUNDLE.CLEAR b
FN.END

FN.DEF calcitems(b)
 !count items in bundle date,count
 BUNDLE.GET 1,"db",db
 now=FLOOR(now())-1
 FOR d=now-365 TO now
  LET d$=juliantogreg$(d):LET n=0:LET t=0:CALL DateTotal(d$,&n,&t)
  IF n THEN BUNDLE.PUT b,INT$(d),n
 NEXT
FN.END

FN.DEF histitems()
 !make items histogram
 BUNDLE.CREATE b
 calcitems(&b)
 SaveNumbundle("items.txt",b)   
 plothist("items.txt","items",2):BUNDLE.CLEAR b
FN.END

FN.DEF calcperioditems(b,p)
 !count grouped items over daily period p
 BUNDLE.GET 1,"db",db
 now=FLOOR(now())
 FOR d=now-365 TO now
  LET d$=juliantogreg$(d):n=0
  LET w$=INT$(FLOOR(d/p)*p):LET t=0:CALL DateTotal(d$,&n,&t)
  IF n THEN CALL tallybundle(&b,w$,n)
 NEXT
FN.END

FN.DEF calcperiodexer(b,p)
 !count grouped exercise over daily period p
 BUNDLE.GET 1,"db",db
 now=FLOOR(now())
 FOR d=now-365 TO now
  d$=juliantogreg$(d)
  w$=INT$(FLOOR(d/p)*p)
  t=exercise(d$)
  IF t THEN CALL tallybundle(&b,w$,t)
 NEXT
FN.END

FN.DEF GraphExercise()
 BUNDLE.CREATE b
 calcperiodexer(&b,7)
 PlotBundle(b,"days ago","burned",0)
 BUNDLE.CLEAR b
FN.END

FN.DEF GraphMonthExercise()
 BUNDLE.CREATE b
 calcperiodexer(&b,30)
 PlotBundle(b,"days ago","burned (30 day)",0)
 BUNDLE.CLEAR b
FN.END

FN.DEF GraphItems()
 !graph weekly items
 BUNDLE.CREATE b
 calcperioditems(&b,7)
 PlotBundle(b,"days ago","items (by week)",0)
 BUNDLE.CLEAR b
FN.END

FN.DEF graphmonthitems()
 BUNDLE.CREATE b
 calcperioditems(&b,30)
 PlotBundle(b,"days ago","items (30 day)",0)
 BUNDLE.CLEAR b
FN.END

FN.DEF GraphMarks(p,type)
 BUNDLE.CREATE b
 BUNDLE.GET 1,"db",db
 SQL.QUERY cursor,db,"diary","date,food",""
 SQL.QUERY.LENGTH n,cursor
 FOR i=1 TO n
  SQL.NEXT done,cursor,cv1$,cv2$
  LET d=calcdate(cv1$)
  LET w$=INT$(FLOOR(d/p)*p)
  IF ismarked(cv2$)=type THEN CALL tallybundle(&b,w$,1)
 NEXT 
 IF type=1 THEN t$="stars" ELSE t$="frowns"
 PlotBundle(b,"date",t$,0)
 BUNDLE.CLEAR b
FN.END

!plot weekly frequency of a single food item
!

FN.DEF PlotFood()
 BUNDLE.CREATE b
 f$=autocomplete$("Enter food name:","fooddb.txt")
 KB.HIDE
 LIST.CREATE s,v
 BUNDLE.GET 1,"db",db
 SQL.QUERY cursor,db,"diary","date,food",""
 SQL.QUERY.LENGTH n,cursor
 FOR i=1 TO n
  SQL.NEXT done,cursor,cv1$,cv2$
  d=calcdate(cv1$)
  w$=INT$(FLOOR(d/7)*7)
  IF TRIM$(cv2$)=f$ THEN tallybundle(&b,w$,1)
 NEXT 
 PlotBundle(b,"week",f$,0)
 BUNDLE.CLEAR b
FN.END

FN.DEF calcbin(X,iv,minx,maxx)
 FN.RTN FLOOR((x-minx)/iv)+1
FN.END

FN.DEF binlow(b,iv,minx,maxx)
 FN.RTN CEIL((b-1)*iv)+minx
FN.END

FN.DEF binhigh(b,iv,minx,maxx)
 FN.RTN CEIL((b)*iv)+minx-1
FN.END

FN.DEF Histogram(x[],cutoff,n$)
 !el=clock()
 ARRAY.LENGTH i,x[]:ARRAY.AVERAGE avg,x[]
 ARRAY.STD_DEV sd,x[]:ARRAY.MIN Minx,x[]
 ARRAY.MAX Maxx,x[]:ARRAY.SORT x[]
 IF I < 3 THEN FN.RTN 0
 IF i/2<>FLOOR(i/2)
  Median=x[FLOOR((i+1)/2)]
 ELSE
  Median=(x[FLOOR((i)/2)]+x[FLOOR((i)/2)+1])/2
 ENDIF
 Nbins=FLOOR(POW(i,(1/3)))
 iv=((maxx-minx)/nbins)
 IF iv<1 THEN iv=1
 Cr$="\n":Ff$="#######.####"
 Stat$="n="+INT$(i)+cr$+"Average="+FORMAT$(ff$,avg)+cr$+"Standard Deviation="+FORMAT$(ff$,sd)+cr$+"Min="+FORMAT$(ff$,Minx)+cr$+"Max="+FORMAT$(ff$,Maxx)+cr$+"Median="+FORMAT$(ff$,median)
 dm("g")
 GR.ORIENTATION 0:GR.SCREEN w,h:GR.STATUSBAR statush,statshow
 IF statshow THEN h-=statush
 POPUP "swipe finger left/right to change interval size"
 wLn("back.txt","0"):Maxbins=500:DIM bins[maxbins]
 DO
  Nbins=calcbin(maxx,iv,minx,maxx)
  nbins=MIN(nbins,maxbins)
  ARRAY.FILL bins[1,nbins],0
  FOR j=1 TO i
   ! B=calcbin(x[j],iv,minx,maxx)
   B=FLOOR((x[j]-minx)/iv)+1:BINS[b]++
  NEXT
  Avgbin=calcbin(avg,iv,minx,maxx)
  Sd1bin=calcbin(avg-sd,iv,minx,maxx)
  Sd2bin=calcbin(avg+sd,iv,minx,maxx)
  Medbin=calcbin(median,iv,minx,maxx)
  binmax=0
  FOR k=1 TO nbins
   binmax=MAX(binmax,bins[k])
   !ARRAY.MAX binmax,bins[1,nbins-1] 
  NEXT 
  Maxh=binmax:sx=w/nbins
  sy=h/binmax:diwidth=nbins*sx
  diheight=binmax*sy:Th=FLOOR(23*h/480)
  GR.CLS:GR.SET.STROKE 1
  FOR bin=1 TO nbins
   !RED by default
   GR.COLOR 255,255,0,0,1
   IF bin=sd1bin|bin=sd2bin THEN GR.COLOR 255,255,0,100,1
   IF avgbin=bin THEN GR.COLOR 255,0,100,0,1 
   IF medbin=bin THEN GR.COLOR 255,0,0,255,1
   GR.RECT lx,(bin-1)*sx,(maxh-bins[bin])*sy,bin*sx,maxh*sy
   IF nbins<16
    F=1:GR.SET.STROKE 1
    !grey text
    GR.COLOR 255,70,70,70,1:GR.TEXT.SIZE th:GR.TEXT.ALIGN 2
    V1=binlow(bin,iv,minx,maxx):V2=binhigh(bin,iv,minx,maxx)
    !IF bins[bin]=binmax
    ! GR.TEXT.SIZE ceil(th/2)
    !  GR.TEXT.DRAW txt1,(bin-0.5)*sx,sy*maxh*0.05,INT$(binmax)
    ! ENDIF
    GR.TEXT.SIZE th
    GR.TEXT.DRAW txt1,(bin-0.5)*sx,sy*maxh*0.92,INT$(v1)+"-"
    GR.TEXT.DRAW txt1,(bin-0.5)*sx,sy*maxh*0.97,INT$(v2)
   ENDIF
  NEXT
  GR.TEXT.SIZE th*0.75:GR.COLOR 255,0,255,0,f:GR.TEXT.ALIGN 3
  GR.TEXT.DRAW txt2,nbins*sx*0.95,maxh*sy*0.1,n$
  GR.TEXT.DRAW txt2,nbins*sx*0.95,maxh*sy*0.15,INT$(iv)+" binsize"
  GR.TEXT.DRAW txt2,nbins*sx*0.95,maxh*sy*0.20,INT$(nbins)+" bins"
  GR.TEXT.DRAW txt2,nbins*sx*0.95,maxh*sy*0.25,INT$(binmax)+" binmax"
  call Render()
  tstart=CLOCK():Wait=0
  DO
   GR.TOUCH touched,xx,yy
   IF wait=0&(CLOCK()>tstart+3000)
    Wait=1
    !Draw a empty green rect
    fill=0:GR.SET.STROKE 2:GR.COLOR 255,0,200,0,fill
    l=w*0.1:t=h*0.1:r=w*0.3:b=h*0.3
    GR.RECT qb1,l,t,r,b
    !Label it
    GR.TEXT.SIZE 50:GR.COLOR 255,0,255,0,1:GR.TEXT.ALIGN 2
    GR.TEXT.DRAW qb2,w*0.2,h*0.2,"QUIT":call Render()
   ENDIF
   IF wait=1&(CLOCK()>tstart+7000)
    Wait=0:GR.HIDE qb1:GR.HIDE qb2:call Render()
    Tstart=CLOCK()
   ENDIF
   IF rLn$("back.txt")="1" THEN D_U.BREAK
  UNTIL touched
  IF yy>h/2&xx>3 THEN
   iv=FLOOR((maxx-minx)*(xx/(2*w)))
   IF iv<1 THEN iv=1
  ENDIF
  IF rLn$("back.txt")="1" THEN D_U.BREAK
 UNTIL (yy<h/2)
 DIALOG.MESSAGE ,stat$,c,"okay"
FN.END

!plot histogram from tab delimited file

FN.DEF PlotHist(f$,name$,fnum)
 dlm$="\t"
 IF !iSOLD(f$) THEN FN.RTN 0

 TEXT.OPEN R,FN2,f$:LIST.CREATE n,xx
 DO
  TEXT.READLN FN2,a_line$
  IF a_line$ <>"EOF"&a_line$ <> ""
   n++:x=val2(WORD$(a_line$,fnum,dlm$))
   IF x>0 THEN LIST.ADD xx,x
  ENDIF
 UNTIL a_line$="EOF"
 UNDIM x[]:LIST.SIZE xx,sz
 IF sz>3
  LIST.TOARRAY xx,x[]:Histogram(&x[],0,name$)
 ELSE
  POPUP "not enough data"
 ENDIF
 TEXT.CLOSE FN2
FN.END

!
!
!
!
! GRAPHS end here
!
