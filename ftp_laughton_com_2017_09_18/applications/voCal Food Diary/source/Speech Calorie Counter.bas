HTML.CLOSE
FN.DEF _3y()
if !BACKGROUND() THEN GR.RENDER
Fn.end
FN.DEF LOG(msg$)
FN.RTN 0 % comment out for 79debugging
f$="vocaldebug.txt"
TIME Year$,Month$,Day$,Hour$,Minute$,Second$,WeekDay,isDST
Date$=month$+"-"+day$+"-"+year$+" "+hour$+":"+minute$+":"+second$
m$=date$+msg$
TEXT.OPEN a,h,f$:TEXT.WRITELN h,m$:TEXT.CLOSE h
FN.END
HTML.OPEN 0
LET s$="saved.html":FILE.EXISTS e,s$
IF e
FILE.SIZE z,s$
IF z>500
GRABFILE ss$,s$:HTML.LOAD.STRING ss$
ENDIF
ENDIF
BUNDLE.CREATE g
BUNDLE.PUT g,"back","0":BUNDLE.PUT g,"version",456
BUNDLE.PUT g,"mode","h":BUNDLE.PUT g,"date",""
BUNDLE.PUT g,"jd",0:BUNDLE.PUT g,"oldjd",0
BUNDLE.PUT g,"greg","":BUNDLE.PUT g,"home",0
BUNDLE.CREATE settings:BUNDLE.CREATE marklist
BUNDLE.PUT g,"marklist",marklist:BUNDLE.PUT g,"settings",settings
bready=1
FILE.EXISTS e,"../databases/voCal":SQL.OPEN db,"voCal"
IF !e THEN SQL.NEW_TABLE db,"diary","food,calories,date"
BUNDLE.PUT g,"db",db
LIST.CREATE s,vl
LET ww$="<!DOCTYPE html><html> <style media=\"screen\" type=\"text/css\"> a{color:#fff;} h1,h2,h3,h4{font-family:arial;font-weight:bold;color:#fff;margin:0 0 0 0;}  h3{padding:7px;} h1{font-size:60px;font-weight:bold;color:#fff;} h2{font-size:18px;} h3{font-size:16px;} h4{font-size:14px;}  p{line-height:30px;font-size:16px;margin:0 0 0 0;} body{background:#003;margin-top:180px	;margin-left:0px;width:100%;} header{position:fixed;width:100%;height:180px;top:0;z-index:1;margin-left:0px;} header,footer{background:#003;color:#fff;} footer{border-top:1px solid #003;color:#007;font-size:12px;padding:0px 0px 20px 20px;} #myProgress{width:pct$%;height:4px;position:fixed;background-color:bc$;} </style></head> <div id=\"wrap\"><header style=\"background-color:#003;border-right:0px\"><div id=\"myProgress\"></div><div><img src=\"html/images/graphs.png\" onClick=\"DL('GRAPH')\" style=\"padding-top:8px;padding-right:8pt;float:right;width:27pt;height:27pt;\"/></div> cl$ <a onClick=\"DL('TSPK')\">speech$</a>brd$</header></div><body><style> aa{font-size:24px;} button{display:inline-block;width:90px;height:35px;color:white;background-color:blue;margin:0 10px 0 0;padding:3px 3px;font-size:16px; appearance:none;box-shadow:none;border-radius:0;} button:focus{outline:none}</style><div>table$</div><div style=\"top:65%;left:75%;position:fixed;\"><img src=\"html/images/autoc.png\" style=\"width:40pt;height:40pt;\" onClick=\"DL('TYPE')\"/></div><div style=\"top:80%;left:70%;position:fixed;\"><img src=\"html/images/stop$\" style=\"width:60pt;height:60pt;\" onClick=\"DL('VOICE')\"/></div></body> <footer><br><br><p><a onClick=\"DL('REMOVE')\">Remove Item</a></p><p><a ALIGN=\"right\" onClick=\"DL('AUTO')\">auto$</a></p><p><a ALIGN=\"right\" onClick=\"DL('START')\">listen$</a></p><p><a onClick=\"DL('BLIND')\">blind mode blind$</a></p></footer><script type=\"text/javascript\"> function DL(data){Android.dataLink(data);}</script></html>"
FN.DEF _4m(s$)
BUNDLE.GET 1,"settings",b:BUNDLE.CONTAIN b,s$,e
IF !e THEN FN.RTN 0
BUNDLE.GET b,s$,v:FN.RTN v
FN.END
BUNDLE.PUT 1,"www",ww$
FN.DEF _4l$(f$)
FILE.EXISTS e,f$:IF e THEN GRABFILE a$,f$
FN.RTN TRIM$(a$)
FN.END
FN.DEF aLn(f$,m$)
TEXT.OPEN a,h,f$:TEXT.WRITELN h,m$:TEXT.CLOSE h
FN.END
FN.DEF wLn(f$,msg$)
TEXT.OPEN w,h,f$:TEXT.WRITELN h,msg$:TEXT.CLOSE h
FN.END
FN.DEF _u(b,k$,newv)
LET k$=LOWER$(k$)
LET v=0:BUNDLE.CONTAIN b,k$,e:IF e THEN BUNDLE.GET b,k$,v
BUNDLE.PUT b,k$,v+newv
FN.END
FN.DEF _4a$(f$)
FN.RTN _4l$(f$):FILE.EXISTS e,f$:IF e THEN GRABFILE s$,f$:FN.RTN s$ ELSE FN.RTN ""
FN.END
FN.DEF _4k(s$,v)
BUNDLE.GET 1,"settings",b:BUNDLE.PUT b,s$,v
LET D$="\t":BUNDLE.KEYS b,l:LIST.SIZE l,z
IF z<1 THEN CALL wLn("settings.ini",""):FN.RTN 0
LIST.TOARRAY l,ll$[]
FOR i=1 TO z
LET K$=ll$[i]:BUNDLE.TYPE b,k$,t$
IF t$="N" THEN BUNDLE.GET b,k$,v:o$+=k$+d$+_49$(v)+"\n"
NEXT
CALL wLn("settings.ini",o$):CALL _m()
FN.END
FN.DEF _i(s$)
CALL _4k(s$,!_4m(s$))
FN.END
FN.DEF _2c(b$,b)
BUNDLE.CLEAR b:D$="\t":FILE.EXISTS e,b$
IF !e THEN FN.RTN 0
GRABFILE f$,b$:SPLIT ff$[],f$,"\n":ARRAY.LENGTH z,ff$[]
FOR i=1 TO z
SPLIT s$[],ff$[i],d$:ARRAY.LENGTH zz,s$[]
IF zz=2 THEN BUNDLE.PUT b,TRIM$(s$[1]),TRIM$(s$[2])
NEXT
FN.END
FN.DEF _2v(d$,nn,tot)
BUNDLE.GET 1,"db",db
SQL.QUERY c,db,"diary","calories","date='"+d$+"'"
DO
SQL.NEXT done,c,cv1$
IF !done & IS_NUMBER(cv1$)
LET v=VAL(cv1$):tot+=v:IF v>0 THEN nn++
ENDIF
UNTIL done
FN.END
FN.DEF _40$()
FN.RTN "&#9734;"
FN.END
FN.DEF _4j$()
FN.RTN "&#9785;"
FN.END
FN.DEF _3x$()
FN.RTN "<img src=\"html/images/graphs.png\" style=\"width:15pt;height:15pt;\"/>"
FN.END
FN.DEF _3h(d$)
BUNDLE.GET 1,"db",db:SQL.QUERY c,db,"diary","calories","date='"+d$+"'"
DO
SQL.NEXT done,c,cv1$
IF !done & IS_NUMBER(cv1$)
LET v=VAL(cv1$):IF v<0 THEN tot+=v
ENDIF
UNTIL done
FN.RTN -tot
FN.END
FN.DEF _m()
BUNDLE.GET 1,"settings",b:B$="settings.ini"
D$="\t":FILE.EXISTS e,b$:IF !e THEN FN.RTN 0
GRABFILE f$,b$:SPLIT ff$[],f$,"\n":ARRAY.LENGTH z,ff$[]
FOR i=1 TO z
LET s1$=TRIM$(WORD$(ff$[i],1,d$)):LET s2$=TRIM$(WORD$(ff$[i],2,d$))
IF s1$<>"" & IS_NUMBER(s2$) THEN BUNDLE.PUT b,s1$,VAL(s2$)
NEXT
FN.END
FN.DEF _4i(s$)
IF IS_NUMBER(s$) THEN FN.RTN VAL(s$)
FN.RTN 0
FN.END
FN.DEF _49$(v)
IF FRAC(v)>0 THEN FN.RTN STR$(v)
FN.RTN INT$(v)
FN.END
FN.DEF _h(b$,b)
D$="\t":BUNDLE.KEYS b,l:LIST.SIZE l,z
IF z<1 THEN wLn(b$,""):FN.RTN 0
LIST.TOARRAY l,ll$[]
FOR i=1 TO z
LET K$=ll$[i]:BUNDLE.TYPE b,k$,t$
IF t$="N"
BUNDLE.GET b,k$,v:IF k$<>"" THEN o$+=k$+d$+_49$(v)+"\n"
ENDIF
NEXT
CALL wLn(b$,o$)
FN.END
FN.DEF _g(b$,b)
BUNDLE.CLEAR b:LET D$="\t":FILE.EXISTS e,b$
IF !e THEN FN.RTN 0
GRABFILE f$,b$:SPLIT ff$[],f$,"\n":ARRAY.LENGTH z,ff$[]
FOR i=1 TO z
SPLIT s$[],ff$[i],d$:ARRAY.LENGTH zz,s$[]
IF zz=2
LET S$[1]=TRIM$(s$[1]):IF s$[1]<>"" & IS_NUMBER(S$[2]) THEN BUNDLE.PUT b,s$[1],VAL(s$[2])
ENDIF
NEXT
FN.END
FN.DEF _3g$(d$)
BUNDLE.GET 1,"db",db
SQL.QUERY cursor,db,"diary","food,calories,date","date='"+d$+"'"
DO
SQL.NEXT done,cursor,cv1$,cv2$,cv3$
IF !done THEN r$+=cv1$+"\t"+cv2$+"\t"+cv3$+"\n"
UNTIL done
FN.RTN r$
FN.END
FN.DEF _3f$()
TIME Y$,M$,D$,H$,Mi$,S$,WkD,DST:FN.RTN m$+"-"+d$+"-"+y$
FN.END
FN.DEF dm(m$)
BUNDLE.GET 1,"mode",oldm$:IF oldm$=m$ THEN FN.RTN 0
BUNDLE.PUT 1,"mode",m$
IF oldm$="h" THEN HTML.CLOSE
IF oldm$="g"|oldm$="g2" THEN GR.CLOSE
IF m$="console"|m$="" THEN CLS:FN.RTN 0
IF m$="h" THEN HTML.CLOSE:HTML.OPEN 0:PAUSE 100:HTML.ORIENTATION 1:FN.RTN 0
IF m$="g" THEN GR.OPEN 255,0,0,0,0,1:PAUSE 100
IF m$="g2" THEN GR.OPEN 255,255,255,255,0,1:PAUSE 100
FN.END
FN.DEF _3p(f$)
CALL _4k(f$,1)
FN.END
FN.DEF _2u(f$)
CALL _4k(f$,0)
FN.END
FN.DEF _48(f$)
FILE.EXISTS e,f$:FN.RTN e
FN.END
FN.DEF _4h$(c)
FN.RTN LEFT$(STR$(c),IS_IN(".",STR$(c))-1)
FN.END
FN.DEF _4g$(d$)
LET j=_3e(d$):LET sun=2451546:LET d=MOD(j-sun,7)
FN.RTN "("+WORD$("sun,mon,tues,wed,thur,fri,sat",d+1,",")+")"
FN.END
FN.DEF _t(d$)
BUNDLE.GET 1,"date",od$
IF d$=od$ THEN BUNDLE.GET 1,"jd",jd:FN.RTN jd
LET mm=VAL(WORD$(d$,1,"-")):LET dd=VAL(WORD$(d$,2,"-")):LET yy=VAL(WORD$(d$,3,"-"))
LET a=FLOOR((14-mm)/12):LET y=yy+4800-a:LET m=mm+12*a-3
LET j=dd+FLOOR((153*m+2)/5)+365*Y+FLOOR(y/4)-FLOOR(y/100)+FLOOR(y/400)-32045
FN.RTN j
FN.END
FN.DEF _f$(jd)
BUNDLE.GET 1,"oldjd",ojd
IF ojd=jd THEN BUNDLE.GET 1,"greg",g$:FN.RTN g$
LET q=FLOOR((jd/36524.25)-51.12264)
LET r=jd+q-FLOOR(q/4)+1:s=r+1524
LET t=FLOOR((s/365.25)-0.3343):u=FLOOR(t*365.25)
LET v=FLOOR((s-u)/30.61):d=s-u-FLOOR(v*30.61)
IF v>13.5 THEN cond=-1 ELSE cond=0
m=(v-1)+12*cond
IF m<2.5 THEN LET cond=-1 ELSE LET cond=0
LET y=t-cond-4716:yr$=_4h$(y)
WHILE LEN(yr$) < 4
LET yr$ = "0"+yr$
REPEAT
LET mo$=_4h$(m)
IF LEN(mo$) < 2 THEN LET mo$ = "0"+mo$
LET da$ = _4h$(d)
IF LEN(da$) < 2 THEN LET da$ = "0"+da$
FN.RTN mo$+"-"+da$+"-"+yr$
FN.END
FN.DEF _3e(s$)
IF IS_IN("-",s$) THEN FN.RTN _t(s$)
IF s$="" THEN FN.RTN -1
IF IS_NUMBER(s$) THEN FN.RTN VAL(s$)
FN.RTN 0
FN.END
FN.DEF _3w(p$)
dm(""):DIALOG.MESSAGE "voCal",p$,c,"yes","no"
FN.RTN (c=1)
FN.END
FN.DEF now()
TIME y$,m$,d$,h$,mi$,s$
LET j=_3e(m$+"-"+d$+"-"+y$)
LET f=VAL(h$)/24+VAL(mi$)/(24*60)
FN.RTN j+f
FN.END
FN.DEF f$(n)
FN.RTN TRIM$(FORMAT$("######",n))
FN.END
FN.DEF _47$(a[])
ARRAY.LENGTH n,a[]:ARRAY.MAX max,a[]
ARRAY.MIN min,a[]:ARRAY.STD_DEV sd,a[]
ARRAY.AVERAGE avg,a[]:range=max-min
s$="n="+INT$(n)+"  "+"avg:"+f$(avg)+"\nmin:"+f$(min)+"  max:"+f$(max)+"\nsd:"+f$(sd)+" range:"+f$(range)+"  median:"+f$(_3r(a[]))+"\n"
FN.RTN s$
FN.END
FN.DEF _3o$(n)
LET i$=INT$(n):s$="&nbsp;&nbsp;&nbsp;&nbsp;":FN.RTN MID$(s$,1,6*(4-LEN(i$)))+i$
FN.END
FN.DEF _3d()
IF _48("atelast.txt") THEN LET e=now()-VAL(_4l$("atelast.txt")):LET e=FLOOR(24*e*100)/100
FN.RTN FLOOR(e*10)/10
FN.END
FN.DEF _2b(vl)
LIST.SIZE vl,n
FOR i=1 TO n
LIST.GET vl,i,s$
s$=REPLACE$(s$,"this morning ","")
s$=REPLACE$(s$,"today ","")
s$=REPLACE$(s$,"this afternoon ","")
s$=REPLACE$(s$,"this evening ","")
s$=REPLACE$(s$,"tonight ","")
s$=REPLACE$(s$,"I ate an ","")
s$=REPLACE$(s$,"I ate a ","")
s$=REPLACE$(s$,"I ate ","")
s$=REPLACE$(s$,"I had an ","")
s$=REPLACE$(s$,"I had a ","")
s$=REPLACE$(s$,"I had ","")
s$=REPLACE$(s$,"for breakfast ","")
s$=REPLACE$(s$,"for lunch ","")
s$=REPLACE$(s$,"for dinner ","")
s$=REPLACE$(s$,"for a snack ","")
s$=REPLACE$(s$," for breakfast","")
s$=REPLACE$(s$," for lunch","")
s$=REPLACE$(s$," for dinner","")
s$=REPLACE$(s$," for a snack","")
LIST.REPLACE vl,i,s$
NEXT
FN.END
FN.DEF _3c(f$)
LET f$=TRIM$(f$):BUNDLE.GET 1,"marklist",b:BUNDLE.CONTAIN b,f$,e
IF !e THEN FN.RTN 0
BUNDLE.GET b,f$,m$
IF m$="-" THEN FN.RTN -1 ELSE FN.RTN 1
FN.END
FN.DEF _2t(m$,b)
CALL _2c(m$,&b)
FN.END
FN.DEF _4f$(s$)
LET s$=REPLACE$(s$,"'","#")
FN.RTN " onclick=~DL('"+s$+"')~ "
FN.END
FN.DEF _3n()
BUNDLE.GET 1,"db",db
LET d=FLOOR(now()):j1=d-7:j2=d-1
FOR j=j1 TO j2
LET dayt=0:LET dd$=_f$(j):LET cc=0:CALL _2v(dd$,&cc,&dayt)
IF dayt THEN wktot+=dayt:dayct++
NEXT
IF dayct THEN FN.RTN FLOOR(wktot/dayct) ELSE FN.RTN 0
FN.END
FN.DEF _46$(c,a$,b$)
IF c THEN FN.RTN a$ ELSE FN.RTN b$
FN.END
FN.DEF ask(f$,d$)
CALL dm(""):IF d$="-99999" THEN d$="0"
LIST.CREATE s,l:q$=f$+" "+d$+". is that correct?"
TTS.INIT:TTS.SPEAK q$:STT.LISTEN q$:STT.RESULTS l
LIST.GET l,1,s$:s$=LOWER$(TRIM$(s$))
IF s$="yes"
FN.RTN _4i(d$)
ELSE
TTS.SPEAK "say the correct amount"
STT.LISTEN "what is the correct amount?"
STT.RESULTS l:LIST.GET l,1,s$
TTS.SPEAK s$:s$=REPLACE$(s$,"calories","")
s$=REPLACE$(s$,"points","")
FN.RTN _s(TRIM$(s$))
ENDIF
FN.RTN -99999
FN.END
FN.DEF _45()
FILE.EXISTS Editor,"../source/Speech Calorie Counterdev.bas"
FN.RTN (!Editor)
FN.END
FN.DEF _2a(f$)
FILE.EXISTS onSd,f$
IF !onSd
BYTE.OPEN r,fid,f$:IF fid<0 THEN FN.RTN 0
LET j = IS_IN("/",f$)
WHILE j
FILE.MKDIR LEFT$(f$,j):j = IS_IN("/",f$,j+1)
REPEAT
BYTE.COPY fid,f$
ENDIF
FN.END
FN.DEF _20$(main)
cstart2=CLOCK()
DO
PAUSE 100:HTML.GET.DATALINK data$
IF main & _4m("auto")&(CLOCK()-cstart2)>8000 THEN FN.RTN "QUIT"
UNTIL data$ <> ""
IF IS_IN("DAT:",data$)=1
data$=MID$(data$,5)
ELSEIF IS_IN("BAK:",data$)=1
FN.RTN "QUIT"
ELSEIF IS_IN("LNK:file:///",data$)=1&IS_IN("?",data$)
i=IS_IN("?",data$):data$="SUBMIT&"+MID$(data$,i+1)+"&"
ENDIF
FN.RTN data$
FN.END
FN.DEF _29$()
LET r$=_4a$("remind.txt")
SPLIT rr$[],r$,"\n"
ARRAY.LENGTH z,rr$[]
LET i=FLOOR(RND()*z)+1
IF z THEN LET rmd$=rr$[i]
FN.RTN rmd$
FN.END
FN.DEF _2s()
LET star$="&#9734;":LET bad$="&#9785;":LET dt$=_4l$("current"):LET dispd$=dt$
sq$="&#39;"
IF dt$=_3f$() THEN LET dispd$="today"
CALL dm("h"):LET dl$="\n"
LET x$="<button style=~width:20%;height:45px;~ "+_4f$("PREV")+"> < </button><button style=~text-align:left;background-color:#006;border:none;width:50%;~"+_4f$("TODAY")+">"+dispd$+" "+_4g$(dt$)+"</button><button style=~width:20%;height:45px;~ "+_4f$("NEXT")+">></button><br>"
BUNDLE.GET 1,"db",db:SQL.QUERY cursor,db,"diary","food,calories,date","date='"+dt$+"'"
DO
SQL.NEXT done,cursor,s1$,s2$,s3$:IF done THEN D_U.BREAK
i++:LET m$="":LET ss=_3c(s1$)
IF ss=1 THEN LET m$=star$:marks++
IF ss=-1 THEN LET m$=bad$:frowns++
IF IS_NUMBER(s2$)
LET T=VAL(s2$):total+=t
IF t>0 THEN n++ELSE burned+=-t
ENDIF
LET cb$="#00e;"
IF MOD(i,2) THEN LET cb$="#009;"
LET cs$=cb$
IF ss=1 THEN LET cs$="#0b0;"
IF ss=-1 THEN LET cs$="#444;"
x$+="<button style=~width:20%; background-color:"+cs$+"~"+_4f$("TOGGLE "+s1$)+">"+s2$+m$+"</button><button style=~text-align:left;width:72%;border:none;background-color:"+cb$+"~ "+_4f$("QUIT")+">"+s1$+"</button><br>"
UNTIL done
IF !n
x$+="<button style=~width:20%;~ >-</button><button style=~border:none;width:72%;~ "+_4f$("QUIT")+">QUIT</button><br>"
ENDIF
LET auto$="autoquit "
IF _4m("auto") THEN LET auto$+="&#10004;"
LET listen$="autolisten"+_46$(_4m("listen"),"&#10004;","")
LET speech$=_46$(_4m("speak"),"&#128266;","&#128264;")
LET blind$=_46$(_4m("blind"),"&#10004;","")
LET goal=_4m("goal"):CALL _4k("todaycal",total)
IF total > goal
LET pct$="100":LET warn$="#f00":LET stop$="red.png"
ELSE if total > 0.8*goal
LET pct$="0":IF goal>0 THEN LET pct$=INT$(100*total/goal)
LET warn$="#ff0":LET stop$="yellow.png"
ELSE
LET pct$="0":IF goal>0 THEN LET pct$=INT$(MAX(0,100*total/goal))
LET warn$="#0F0":LET stop$="green.png"
ENDIF
IF goal>0 THEN LET lft$=_46$(goal>=total,TRIM$(INT$(goal-total)),TRIM$(">"+INT$(total-goal)))
IF !goal THEN LET lft$="set goal"
LET cl$="<button "+_4f$("GOAL")+" style=~height:60px;width:auto;font-size:50px;color:"+warn$+"~> "+lft$+"</button>"
LET brd$="<h2 style=~text-align:right~>"+INT$(total+burned)+"-"+INT$(burned)+"="+INT$(total)+"/"+INT$(goal)+" wk avg:"+INT$(_4m("weekavg"))
LET ah=_3d()
brd$+="<br>you ate "+STR$(ah)+" hours ago<br>"
sss$="<h4>"+INT$(marks)+"&#9734; "+INT$(frowns)+bad$+" "+INT$(n)+" items</h4>"
rmd$=_4d$(_29$())
brd$+="<a "+_4f$("REMIND")+">"+rmd$+"&#9999;</a></h2>"
BUNDLE.GET 1,"www",ww$
LET w$=REPLACE$(ww$,"brd$",brd$):LET w$=REPLACE$(w$,"pct$",pct$):LET w$=REPLACE$(w$,"bc$",warn$)
LET w$=REPLACE$(w$,"cl$",cl$):LET w$=REPLACE$(w$,"stop$",stop$):LET w$=REPLACE$(w$,"table$",x$+sss$)
LET w$=REPLACE$(w$,"speech$",speech$):LET w$=REPLACE$(w$,"auto$",auto$):LET w$=REPLACE$(w$,"listen$",listen$):LET w$=REPLACE$(w$,"blind$",blind$):LET w$=REPLACE$(w$,"~","\"")
IF dispd$="today" THEN wLn("saved.html",&w$)
HTML.LOAD.STRING w$
IF _4m("speak") & _4m("spkworks") & goal>0
IF total<>_4m("oldcal")
CALL _2u("spkworks"):TTS.SPEAK lft$,0:_3p("spkworks"):CALL _4k("oldcal",total)
ENDIF
ENDIF
FN.END
FN.DEF _3m()
dm("")
GRABFILE f$,"remind.txt"
f$=TRIM$(f$)
IF LEN(f$)<2 THEN FN.RTN 0
SPLIT s$[],f$,"\n"
ARRAY.LENGTH z,s$[]
IF z<1 THEN FN.RTN 0
TTS.INIT
BUNDLE.PUT 1,"ctouch",0
DIM a[z]
FOR i=1 TO z
a[i]=i
NEXT
ARRAY.SHUFFLE a[1,z]
FOR i=1 TO z
PRINT s$[a[i]]
TTS.SPEAK s$[a[i]]
PAUSE 2000
BUNDLE.GET 1,"ctouch",ct
IF ct THEN F_N.BREAK
NEXT
CLS
FN.END
FN.DEF _4e()
CALL _m():BUNDLE.CREATE m:CALL _2c("marked.txt",&m):BUNDLE.PUT 1,"marklist",m
IF _4m("speak")&_4m("spkworks") THEN CALL _2u("spkworks"):TTS.INIT:CALL _3p("spkworks")
IF _4m("speak")&!_4m("spkworks") THEN CALL _2u("speak")
LET date$=_3f$():CALL wLn("current",date$)
IF _48("lastdate.txt")
IF _4l$("lastdate.txt")="" THEN CALL wLn("lastdate.txt",Date$)
ENDIF
IF !_48("lastdate.txt")
CALL wLn("lastdate.txt",Date$):CALL _2u("speak")
LET dflt$="0\n25\n50\n100\n150\n200\n250\n300\n400\n-50\n-100\n-200"
LET q$="quickcal.txt"
IF _3w("use points instead of calories?")
LET dflt$="0\n1\n2\n3\n4\n5\n6\n7\n8\n-1\n-2"
ELSE
LET f$=_4a$("sampledb/samplefooddb.txt")
CALL _3k(&f$):CALL aLn("fooddb.txt",f$)
ENDIF
CALL wLn(q$,dflt$)
ENDIF
IF _4m("listen")
LIST.CREATE s,vl
CALL _4b(&vl)
CALL wLn("ok","1"):LET r$="VOICE"
CALL _0(r$,vl)
LIST.CLEAR vl
ENDIF
FN.END
FN.DEF _3v(db$)
IF !_48(db$) THEN wLn(db$,"")
e$=TRIM$(_4a$(db$))
e$=REPLACE$(e$,"\t",":")
DO
e$=REPLACE$(e$,"\n\n","\n")
UNTIL !IS_IN("\n\n",e$)
e$=_2j$("edit database",e$)
_3k(&e$):wLn(db$,e$):POPUP "saved!"
FN.END
FN.DEF _3b(s$)
FN.RTN IS_IN(s$,"-0123456789.")
FN.END
FN.DEF _2r(s$)
LET I=1:LET l=LEN(s$)
DO
LET found=_3b(MID$(s$,I,1)):I++
UNTIL i>l|found
FN.RTN found
FN.END
FN.DEF _28(s$)
LET z=LEN(s$)
FOR i=1 TO z
LET C$=MID$(s$,I,1):IF _3b(c$) THEN N$+=c$
NEXT
IF LEN(n$)>0 THEN FN.RTN VAL(n$)
FN.RTN 0
FN.END
FN.DEF _s(s$)
LET s$=LOWER$(s$)
IF _2r(s$)
LET N$="":LET z=LEN(s$)
FOR i=1 TO z
LET C$=MID$(s$,I,1):IF _3b(c$) THEN N$+=c$
NEXT
IF LEN(n$)>0 THEN FN.RTN VAL(n$)
ELSE
v=0:nn$="one,two,three,four,five,six,seven,eight,nine,ten,eleven,twelve,thirteen,fourteen,fifteen,sixteen,seventeen,eighteen,nineteen,twenty"
FOR i=1 TO 20
IF IS_IN(" "+WORD$(nn$,i,",")+" ",s$) THEN v=i :F_N.BREAK
NEXT
FN.RTN v
ENDIF
FN.RTN 0
FN.END
FN.DEF _l$(s$)
LET z=LEN(s$)
FOR i=1 TO z
LET C$=MID$(s$,I,1):IF !_3b(c$) THEN N$+=c$
NEXT
IF LEN(n$)>0 THEN FN.RTN n$
FN.RTN ""
FN.END
FN.DEF _27(db$,b)
TEXT.OPEN w,f,db$:BUNDLE.KEYS b,l:LIST.SIZE l,z
IF z<1 THEN TEXT.WRITELN f,"":TEXT.CLOSE f:FN.RTN 0
LIST.TOARRAY l,ll$[]:ARRAY.SORT ll$[]
FOR i=1 TO z
LET K$=ll$[i]:IF k$<>"" THEN BUNDLE.GET b,k$,v$:TEXT.WRITELN f,k$+"\t"+v$
NEXT
TEXT.CLOSE f
FN.END
FN.DEF _44(f$)
LET f$=TRIM$(f$):BUNDLE.CREATE b:CALL _g("fooddb.txt",&b):BUNDLE.CONTAIN b,f$,found
IF !found THEN FN.RTN -99999
BUNDLE.GET b,f$,v:FN.RTN v
FN.END
FN.DEF _2q$(l,msg$,c)
r$="<br>":h$="<!DOCTYPE html><html lang=~en~><head><meta charset=~utf-8~ /> <title>Home</title>"
h$+="<meta content='width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0' name='viewport' />"
h$+="<meta name=~viewport~ content=~width=device-width;initial-scale=1.0;maximum-scale=1.0~/></head>"
h$+="<h1 style=~color:#aaa~>"+msg$+"</h1><body style=~background-color:#000;text-align:center;~> <div>"
LIST.SIZE l,z
FOR i=1 TO z
LIST.GET l,i,s$
s$=REPLACE$(s$,"'","'")
but$="<button type=~button~  style=~background-color:#111;color:#fff;text-align:center;font-size:40px;width:140px;height:50px;~ onclick=~DL('"+STR$(i)+"')~;>"+s$+"</button>"
h$+=but$
IF FRAC(i/2)<>0 THEN h$+="<br>"
NEXT
h$+="</div><script type=~text/javascript~> function DL(data) {Android.dataLink(data);}</script> </body> </html>"
h$=REPLACE$(h$,"~","\""):dm("h"):HTML.LOAD.STRING h$
LIST.GET l,1,d$
IF _4m("speak") THEN TTS.SPEAK REPLACE$(msg$,"choose cals/pts for","")+" "+d$,0
DO
r$=_20$(0):c=_4i(r$)
UNTIL c>0
LIST.GET l,c,s$:FN.RTN s$
FN.END
FN.DEF _3a$(msg$)
LIST.CREATE s,l:LIST.ADD l,"1","2","3","4","5","6","7","8","9","0",".","-","<","x","E"
r$="<br>":h$="<!DOCTYPE html><html lang=~en~><head>"
h$+="<meta charset=~utf-8~ /> <title>Home</title><meta content='width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0' name='viewport' />"
h$+="<meta name=~viewport~ content=~width=device-width;initial-scale=1.0;maximum-scale=1.0~/></head>"
h$+="<h1 style=~font-size:35px;color:#0f0;~>"+msg$+"</h1><body style=~background-color:#000;text-align:center;~> <div>"
LIST.SIZE l,z
FOR i=1 TO z
LIST.GET l,i,s$:s$=REPLACE$(s$,"'","'")
but$="<button type=~button~  style=~background-color:#000;color:#0f0;text-align:center;font-size:35pt;width:25%;height:30%;~ onclick=~DL('"+STR$(i)+"')~;>"+s$+"</button>"
h$+=but$:IF FRAC(i/3)=0 THEN h$+="<br>"
NEXT
h$+="</div><script type=~text/javascript~> function DL(data) {Android.dataLink(data);}</script> </body> </html>"
h$=REPLACE$(h$,"~","\""):dm("h"):HTML.LOAD.STRING h$
DO
r$=_20$(0):c=_4i(r$)
UNTIL c>0
LIST.GET l,c,s$
FN.RTN s$
FN.END
FN.DEF _43(p$)
LIST.CREATE s,m:LIST.ADD m,"yes","no"
c=0:_35$(m,p$,&c)
LIST.CLEAR m:IF c=1 THEN FN.RTN 1
FN.RTN 0
FN.END
FN.DEF _3u(p$)
dm("h")
P$=REPLACE$(p$,"find","")
P$=REPLACE$(p$,"search","")
m$="use web?"
LIST.CREATE s,l
LIST.ADD l,"USDA Foodapedia","google","CDC BMI calculator","bing "+p$,"duckduckgo "+p$,"BMR calculator","YouTube playlist","cancel"
ch=0:_35$(l,m$,&ch)
IF ch>0 & ch<8 THEN POPUP "press BACK key to return to app"
SW.BEGIN ch
SW.CASE 1
BROWSE "https://mnew.supertracker.usda.gov/FoodTracker"
SW.BREAK
SW.CASE 2
dm("h")
HTML.LOAD.URL "http:/www.google.com"
SW.BREAK
SW.CASE 3
BROWSE "http://www.cdc.gov/healthyweight/assessing/bmi/adult_bmi/english_bmi_calculator/bmi_calculator.html"
SW.BREAK
SW.CASE 4
dm("h")
BROWSE "http://www.bing.com/?q="+p$+" calories"
SW.BREAK
SW.CASE 5
dm("h")
BROWSE "https://duckduckgo.com/?q="+p$+"+calories&ia=nutrition"
SW.BREAK
SW.CASE 6
BROWSE "http://www.calculator.net/bmr-calculator.html"
SW.BREAK
SW.CASE 7
BROWSE "https://www.youtube.com/playlist?list=PLi6Y_PQh4vOmk9JJinwzYC-bWct_CT8sK"
SW.BREAK
SW.END
DO
PAUSE 300
UNTIL !BACKGROUND()
PAUSE 1000
FN.END
FN.DEF _42()
BUNDLE.GET 1,"back",b$:b=(b$="1")
IF b THEN POPUP "back key hit":BUNDLE.PUT 1,"back","0"
FN.RTN b
FN.END
FN.DEF _r$(path$)
DO
ARRAY.DELETE d1$[]
FILE.DIR path$,d1$[]
ARRAY.LENGTH length,d1$[]
ARRAY.DELETE d2$[]
DIM d2$[length+1]
d2$[1]=".."
FOR i=1 TO length
d2$[i+1]=d1$[i]
NEXT
s=0
LIST.CREATE s,l
ARRAY.LENGTH z,d2$[]
FOR i=1 TO z
LIST.ADD l,d2$[i]
NEXT
_35$(l,"select file",&s)
IF s>1
n=IS_IN("(d)",d2$[s])
IF n=0
D_U.BREAK
FN.RTN path$+d2$[s]
ENDIF
dname$=LEFT$(d2$[s],n-1)
path$+=dname$+"/"
D_U.CONTINUE
ENDIF
IF path$=""
path$="../"
D_U.CONTINUE
ENDIF
ARRAY.DELETE p$[]
SPLIT p$[],path$,"/"
ARRAY.LENGTH length,p$[]
IF p$[length]=".."
path$+="../"
D_U.CONTINUE
ENDIF
IF length=1
path$=""
D_U.CONTINUE
ENDIF
path$=""
FOR i=1 TO length-1
path$+=p$[i]+"/"
NEXT
UNTIL 0
FN.RTN path$+d2$[s]
FN.END
FN.DEF _k$(p,msg$,r)
LIST.SIZE p,size
IF size<1 THEN FN.RTN "done"
LIST.ADD p,"done"
a$=TRIM$(_35$(p,msg$,&r))
LIST.SIZE p,size
LIST.REMOVE p,size
FN.RTN a$
FN.END
FN.DEF _2p(f$,b)
CALL _27(f$,b):BUNDLE.PUT 1,"marklist",b
FN.END
FN.DEF _30(f$,type)
LET mdb$="marked.txt":BUNDLE.CREATE b:CALL _2t(mdb$,&b)
IF type=-1 THEN s$="-" ELSE s$="+"
BUNDLE.PUT b,f$,s$:CALL _2p(mdb$,b)
FN.END
FN.DEF _2o(f$)
LET mdb$="marked.txt":BUNDLE.CREATE b:CALL _2t(mdb$,&b)
BUNDLE.REMOVE b,TRIM$(f$):CALL _2p(mdb$,b)
FN.END
FN.DEF _26()
c$=_4j$()+",,"+_40$()
m$="marked.txt":BUNDLE.CREATE b
DO
_2t(m$,&b):BUNDLE.KEYS b,l:LIST.SIZE l,z
IF z<1 THEN D_U.BREAK
FOR i=1 TO z
LIST.GET l,i,r$
LIST.REPLACE l,i,r$+WORD$(c$,_3c(r$)+2,",")
NEXT
r=0:f$=_k$(l,"Select items to Unmark",&r)
f$=REPLACE$(f$,_40$(),"")
f$=REPLACE$(f$,_4j$(),"")
IF F$<>"done" & r>0 THEN _2o(f$):LIST.REMOVE l,r:LIST.SIZE l,z
UNTIL F$="done"|z<1
FN.RTN 0
FN.END
FN.DEF _2n$(p$)
LET p$=TRIM$(LOWER$(p$))
IF p$="show"|p$="list"|p$="recognition cancelled" THEN FN.RTN "SHOW"
IF IS_IN("plus",p$)|IS_IN("Plus",p$)|IS_IN("+",P$) THEN FN.RTN "BULK"
IF IS_IN("calories",P$)|IS_IN("points",P$)|IS_IN("point",P$) THEN FN.RTN "FOOD"
IF p$="display"|p$="graphs" THEN FN.RTN "GRAPH"
IF IS_IN("remove",p$) THEN FN.RTN "REMOVE"
IF IS_IN("find",p$)|IS_IN("search",p$) THEN FN.RTN "SEARCH"
IF p$="toggle start" THEN FN.RTN "START"
IF IS_IN("toggle",p$) THEN FN.RTN "TOGGLE"
ARRAY.LOAD cmd$[],"target","GOAL","email","EMAIL","clear","UNMARK","speech on","SPEAK","speech off","NOSPEAK","history","ARCHIVE","record","WEIGHT","quit","QUIT","notes","NOTES","remind","REMIND","auto","AUTO","analyze","ANALYZE","reset","RESET","blind mode","BLIND","mysterioso","SETTINGS","help","HELP","inspire me","PEP"
ARRAY.SEARCH cmd$[],p$,i
IF !i THEN FN.RTN "NONE"
FN.RTN cmd$[i+1]
FN.END
FN.DEF _a(f$,cal$)
LET f$=TRIM$(f$):LET cr$=CHR$(10):LET db$=_4a$("fooddb.txt")
LET g=IS_IN(cr$+f$+"\t",cr$+db$)
IF !g THEN aLn("fooddb.txt",f$+"\t"+cal$):FN.RTN VAL(cal$)
LET r$=WORD$(MID$(db$,g,100),1,cr$)
LET db$=f$+"\t"+cal$+cr$+REPLACE$(cr$+db$,cr$+r$+cr$,cr$)
LET db$=REPLACE$(db$," "+cr$,cr$)
DO
LET db$=REPLACE$(db$,cr$+cr$,cr$)
UNTIL !IS_IN(cr$+cr$,db$)
LET db$=TRIM$(db$):CALL wLn("fooddb.txt",db$):FN.RTN _28(cal$)
FN.END
FN.DEF _j(weight,date$)
BUNDLE.CREATE b
_g("weight.txt",&b)
BUNDLE.PUT b,date$,weight
_h("weight.txt",b)
_4k("current weight",weight)
POPUP w$+" recorded"
FN.END
FN.DEF _25()
POPUP "Be careful!That's a lot of calories for one item!"
ARRAY.LOAD v[],0,300,300
VIBRATE v[],-1:PAUSE 2000
FN.END
FN.DEF _3l()
POPUP "You are over your goal!"
ARRAY.LOAD v[],0,300,300
VIBRATE v[],-1:PAUSE 2000
FN.END
FN.DEF _24()
LIST.CREATE s,menu:LIST.CREATE s,idx:BUNDLE.GET 1,"db",db
d$=_4l$("current")
SQL.QUERY cursor,db,"diary","_id,food","date='"+d$+"'"
SQL.QUERY.LENGTH n,cursor
IF !n THEN FN.RTN 0
FOR i=1 TO n
SQL.NEXT done,cursor,cv1$,cv2$:LIST.ADD menu,cv2$:LIST.ADD idx,cv1$
NEXT
LIST.ADD menu,"cancel"
c=0:del$=_35$(menu,"delete?",&c)
IF !c|del$="cancel" THEN FN.RTN 0
LIST.GET idx,c,v$:SQL.DELETE db,"diary","_id='"+v$+"'"
_4k("weekavg",_3n())
FN.END
FN.DEF _23(f$,c)
BUNDLE.GET 1,"db",db:LET date$=TRIM$(_4l$("current")):LET f$=TRIM$(f$)
IF f$="" THEN POPUP "error:no food name!":FN.RTN 0
LET cals$=INT$(c)
IF c>0 & date$=_3f$() THEN CALL wLn("atelast.txt",STR$(now()))
SQL.INSERT db,"diary","food",f$,"calories",INT$(c),"date",date$
CALL _a(f$,cals$):_4k("weekavg",_3n())
IF c>=300 THEN _25()
LET g=_4m("goal")
IF g & _4m("todaycal") > g THEN _3l()
FN.END
FN.DEF _q(p$)
LET p$=REPLACE$(p$," Plus "," plus "):LET p$=REPLACE$(p$,"+"," plus ")
SPLIT foods$[],p$," plus ":CALL dm("g"):LET date$=_4l$("current")
ARRAY.LENGTH z,foods$[]
FOR i=1 TO z
LET f$=TRIM$(foods$[i])
g=_44(f$)
IF g=-99999 THEN g=0
LET msg$="input cals/points for:"
LET c=_2m(msg$,f$,g)
IF c>-99999 THEN CALL _23(f$,c):POPUP f$+" "+INT$(c)+" added"
NEXT
FN.END
FN.DEF del(f$)
FILE.DELETE e,f$
FN.END
FN.DEF _39()
dm(""):DIALOG.MESSAGE ,"reset and erase entire app?",q,"yes","no"
IF q<>1 THEN FN.RTN 1
DIALOG.MESSAGE ,"are you sure?",q,"yes","no"
IF q<>1 THEN FN.RTN 1
del("settings.ini"):del("archive.txt"):del("archive1.txt")
del("fooddb.txt"):del("marked.txt"):del("lastdate.txt")
del("weight.txt"):del("notes.txt"):del("../databases/voCal")
del("../databases/voCal-journal")
POPUP "diary reset":POPUP "please restart voCal"
EXIT
FN.END
FN.DEF _38()
_34(_32$())
FN.END
FN.DEF _p()
IF _43("log today's weight?")
w=_2l("Enter weight:","lbs/kg",_4m("current weight"))
IF w<=0 THEN FN.RTN 1
_j(w,date$):_4k("current weight",w)
dm("g"):_3p("relgraph")
ENDIF
_2z("weight.txt","date","weight","\t",1)
_2u("relgraph")
FN.END
FN.DEF _41()
IF !_48("notes.txt")
wLn("notes.txt","notes")
aLn("notes.txt","-----")
aLn("notes.txt","\n")
ENDIF
GRABFILE n$,"notes.txt"
n$=_2j$("notes",n$)
wLn("notes.txt",n$)
FN.END
FN.DEF _3k(e$)
r$="\n":e$=REPLACE$(e$,":","\t"):e$=REPLACE$(e$,"\n",r$)
e$=REPLACE$(e$,CHR$(13),r$)
DO
e$=REPLACE$(e$,r$+r$,r$)
UNTIL !IS_IN(r$+r$,e$)
FN.END
FN.DEF _0(u$,lll)
CALL wLn("lastcmd",u$)
IF u$="SHOW" THEN FN.RTN 0
IF u$="NEXT" THEN LET DD=_3e(_4l$("current"))+1:CALL wLn("current",_f$(dd)):FN.RTN 0
IF u$="PREV" THEN LET DD=_3e(_4l$("current"))-1:CALL wLn("current",_f$(dd)):FN.RTN 0
IF IS_IN("TOGGLE",u$) THEN LET p$=REPLACE$(u$,"TOGGLE ",""):LET u$="TOGGLE"
IF u$="VOICE"
LIST.SIZE lll,z
LET i=1:LET found=0:LET cal=-99999
IF z=0 THEN FN.RTN 0
DO
LIST.GET lll,i,p$
LET u$=_2n$(p$)
IF u$<>"NONE" THEN LET found=1 ELSE LET cal=_44(p$):LET found=(cal>-99999):i++
UNTIL found|i>z
ENDIF
LET date$=_3f$()
IF u$="NONE"
IF cal=-99999
LIST.ADD lll,"quit":r=0:LIST.SIZE lll,z
IF z=2 THEN LIST.GET lll,1,p$ ELSE LET p$=_35$(lll,"what did you say?",&r)
IF P$="quit" THEN FN.RTN 1
ENDIF
IF cal=-99999 THEN LET cal=0
LET m$="input cals/pts for:"
LET cal=_2m(m$,p$,cal)
IF cal>-99999 THEN CALL _23(p$,cal):POPUP p$+" "+INT$(cal)+" added"
FN.RTN 1
ENDIF
IF u$="FOOD"
LET C=_s(p$):LET P$=_l$(p$)
LET nn$="zero,one,two,three,four,five,six,seven,eight,nine,ten,eleven,twelve,thirteen,fourteen,fifteen,sixteen,seventeen,eighteen,nineteen,twenty"
FOR i=1 TO 21
LET p$=REPLACE$(p$," "+WORD$(nn$,i,",")+" "," ")
NEXT
LET p$=REPLACE$(p$," calories",""):LET p$=REPLACE$(p$," points","")
LET p$=REPLACE$(p$," point",""):LET p$=TRIM$(p$):CALL _23(p$,c)
POPUP p$+" "+INT$(c)+" added"
FN.RTN 1
ENDIF
IF u$="BLIND"
_i("blind")
IF _4m("blind")
_3p("speak"):_3p("spkworks"):_3p("listen")
POPUP "blind mode on":POPUP "please restart":EXIT
ELSE
POPUP "blind mode off"
ENDIF
ENDIF
IF u$="TOGGLE"
p$=REPLACE$(p$,"#","'")
LIST.CREATE s,mm
LIST.ADD mm,"good","bad","benign","copy to today","cancel"
DIALOG.SELECT cc,mm,p$
p$=TRIM$(p$):type=_3c(p$)
IF cc=5 THEN FN.RTN 0
IF cc=4
old$=_4l$("current")
wLn("current",_3f$())
cal=_44(p$)
IF cal=-99999 THEN cal=0
m$="input cals/pts for:"
cal=_2m(m$,p$,cal)
IF cal=-99999 THEN POPUP "cancelled":FN.RTN 0
_23(p$,cal)
wLn("current",old$)
POPUP p$+" copied"
FN.RTN 0
ENDIF
IF cc=1 THEN _30(p$,1):FN.RTN 0
IF cc=2 THEN _30(p$,-1):FN.RTN 0
IF cc=3 THEN _2o(p$):FN.RTN 0
FN.RTN 0
ENDIF
IF u$="HELP" THEN _4c():FN.RTN 0
IF u$="BULK" THEN CALL _q(p$):FN.RTN 0
IF u$="QUIT" THEN CALL wLn("ok","2"):FN.RTN 0
IF u$="AUTO" THEN _i("auto"):FN.RTN 0
IF u$="START" THEN _i("listen"):FN.RTN 0
IF u$="UNMARK" THEN _26():FN.RTN 0
IF u$="ANALYZE" THEN _38():FN.RTN 0
IF u$="EMAIL" THEN _31():FN.RTN 0
IF u$="PEP" THEN _3m():FN.RTN 0
IF u$="ARCHIVE" THEN _o():FN.RTN 0
IF u$="TSPK"
_i("speak"):IF _4m("speak") THEN TTS.INIT
_3p("spkworks")
ENDIF
IF u$="SPEAK" THEN _3p("speak"):TTS.INIT:_3p("spkworks"):FN.RTN 0
IF u$="NOSPEAK" THEN _2u("speak"):_3p("spkworks"):FN.RTN 0
IF u$="SEARCH" THEN _3u(p$):FN.RTN 0
IF u$="GRAPH" THEN _z():FN.RTN 0
IF u$="NOTES" THEN _41():FN.RTN 0
IF u$="TODAY" THEN wLn("current",_3f$()):FN.RTN 0
IF u$="SETTINGS"
GRABFILE s$,"settings.ini"
S$=_2j$("settings...edit carefully!",REPLACE$(s$,"\t",":"))
IF s$<>"" THEN wLn("settings.ini",REPLACE$(s$,":","\t")):POPUP "settings updated"
FN.RTN 0
ENDIF
IF u$="REMIND"
rmd$=_2j$("What will you do differently?",_4a$("remind.txt"))
wLn("remind.txt",rmd$)
FN.RTN 0
ENDIF
IF u$="GOAL"
g=_4m("goal")
a=_3n()
IF a>1000 THEN g=a
LET Goal=_36("set daily goal","cals/pts",a)
IF goal>0 THEN  _4k("goal",goal)
FN.RTN 0
ENDIF
IF u$="REMOVE" THEN _24():FN.RTN 0
IF u$="WEIGHT" THEN _p():FN.RTN 0
IF u$="RESET"  THEN _39():FN.RTN 0
IF u$="EDITDB" THEN _3s("fooddb.txt")
FN.END
FN.DEF _3t(n)
FN.RTN FRAC(n)<>0
FN.END
FN.DEF _22(p$,a$,d,search)
DIM pad[15]:n$=INT$(d)
msg$=p$+"<br>"+a$+"<br><a style=\"font-size:50px;color:#ff0;\">"+n$+"</a>"
n=d:khit=0
DO
k$=_3a$(msg$):khit=1
IF k$="E" THEN D_U.BREAK
n=0
DO
IF IS_IN(k$,"0123456789")
IF dc & FRAC(n)=0
n+=SGN(n)*VAL(k$)/10
ELSE
IF _3t(n)
PAUSE 100
ELSE if ABS(n)<5000
n=n*10+VAL(k$)
ENDIF
ENDIF
ENDIF
IF k$="x" THEN D_U.BREAK
IF k$="-" THEN n=-n
IF k$="." THEN dc=1
IF k$="<"
IF _3t(n)
n=FLOOR(ABS(n))*SGN(n)
ELSE
n=SGN(n)*FLOOR(ABS(n)/10)
ENDIF
dc=0
ENDIF
IF _3t(n) THEN n$=STR$(n) ELSE n$=INT$(n)
msg$=p$+"<br>"+a$+"<br><a style=\"font-size:50px;color:#ff0;\">"+n$+"</a>"
k$=_3a$(msg$)
UNTIL k$="x"|k$="E"
UNTIL k$="x"|k$="E"
IF k$="x" THEN n=-99999
FN.RTN n
FN.END
FN.DEF _37(p$,a$,d)
IF _4m("blind") THEN FN.RTN ask(a$,INT$(_44(a$)))
q$="quickcal.txt"
dflt$="0\n25\n50\n100\n150\n200\n250\n300\n400"
DO
IF !_48(q$) THEN wLn(q$,dflt$)
FILE.EXISTS e,q$
IF e THEN GRABFILE cl$,q$
UNDIM cc$[]:SPLIT cc$[],cl$,"\n"
LIST.CREATE s,cl:LIST.ADD cl,INT$(d),"custom","search web"
LIST.ADD.ARRAY cl,cc$[]:LIST.ADD cl,"edit","cancel"
c=0
DO
v$=_2q$(cl,"choose cals/pts for "+a$,&c)
IF v$="search web" THEN _3u(a$)
IF v$="edit" THEN cl$=_2j$("edit default list",cl$):wLn(q$,cl$)
UNTIL v$<>"search web"
UNTIL v$<>"edit list"
IF c=0|v$="cancel" THEN FN.RTN -99999
IF v$="custom" THEN FN.RTN _22(p$,a$,d,1)
IF IS_NUMBER(v$) THEN FN.RTN VAL(v$) ELSE FN.RTN -99999
FN.END
FN.DEF _36(p$,a$,d)
FN.RTN _22(p$,a$,d,0)
FN.END
FN.DEF _2m(p$,a$,d)
FN.RTN _37(p$,a$,d)
FN.END
FN.DEF _2l(p$,a$,d)
FN.RTN _22(p$,a$,d,0)
FN.END
FN.DEF _21$(db$,s$)
m$=s$
IF s$="" THEN FN.RTN ""
x=IS_IN("\n"+s$,"\n"+db$)
IF x THEN m$=WORD$(MID$(db$,x,x+60),1,"\t")
FN.RTN m$
FN.END
FN.DEF _e$(msg$,d$)
goodkeys$=" '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTHVWXYZ"
dm("g")
GR.ORIENTATION 1:GR.SCREEN w,h:GR.TEXT.SIZE h/18:GR.TEXT.ALIGN 2
GR.COLOR 255,0,255,0,1:GR.TEXT.DRAW p,w/2,h/8,msg$
GR.COLOR 255,255,255,0,1:GR.TEXT.DRAW et,w/2,h/4,""
GR.COLOR 255,0,255,0,1:GR.TEXT.DRAW tt,w/2,h/3,""
GR.TEXT.DRAW g,w/2,h/2-h/14,"hit enter for match"
GR.TEXT.DRAW g,w/2,h/2," or press . for new item":_3y()
FILE.EXISTS e,d$
IF e THEN GRABFILE db$,d$
?:CLS:KB.SHOW
DO
INKEY$ k$:bk=(_42()|BACKGROUND())
IF bk THEN D_U.BREAK
PAUSE 100
UNTIL k$="@"
IF bk THEN FN.RTN ""
DO
DO
INKEY$ k$:PAUSE 100
bk=(_42()|BACKGROUND())
IF bk THEN D_U.BREAK
UNTIL k$<>"@"
IF bk THEN D_U.BREAK
IF k$="key 75" THEN k$="'"
IF k$="key 56" THEN D_U.BREAK
IF k$="key 66" THEN D_U.BREAK
IF k$="key 67" & LEN(s$)>0
s$=MID$(s$,1,LEN(s$)-1)
ENDIF
IF IS_IN(k$,goodkeys$)
INKEY$ n$:bk=(_42()|BACKGROUND())
IF bk THEN D_U.BREAK
IF n$="key 59" THEN k$=UPPER$(k$)
s$+=k$
ENDIF
x$=_21$(db$,s$)
IF x$="" THEN x$=s$
GR.MODIFY et,"text",s$:m$=""
IF x$<>"" THEN m$=""
GR.MODIFY tt,"text",m$+x$:_3y()
UNTIL k$="key 66"
IF bk THEN FN.RTN ""
DO
INKEY$ w$:PAUSE 100
bk=(_42()|BACKGROUND())
IF bk THEN D_U.BREAK
UNTIL w$="@"
IF bk THEN FN.RTN ""
IF k$="key 56"
FN.RTN s$
ELSE
FN.RTN x$
ENDIF
FN.END
FN.DEF _2k$(l,msg$,c)
dm("")
DIALOG.SELECT c,l,msg$
IF c THEN LIST.GET l,c,c$
FN.RTN c$
FN.END
FN.DEF _35$(l,msg$,c)
dm("h")
r$="<br>"
h$+="<!DOCTYPE html><html lang=~en~><body><body style=~background-color:#000000;text-align:center;~>"
h$+="<h1 style=~color:#999999~>"+msg$+"</h1>"+r$
LIST.SIZE l,z
h$+="<div>"
FOR i=1 TO z
LIST.GET l,i,s$
s$=REPLACE$(s$,"'","'")
s$=REPLACE$(s$,"<g>",_3x$())
but$="<button type=~button~ style=~background-color:#333;color:#FFF;text-align:center;font-size:20px;width:300px;height:50px;~ onclick=~DL('"+STR$(i)+"')~;>"+s$+"</button><br>"
h$+=but$
NEXT
h$+="<br><br><br><br></div><script type=~text/javascript~> function DL(data) {Android.dataLink(data);}</script></body></html>"
h$=REPLACE$(h$,"~","\"")
HTML.LOAD.STRING h$
r$=_20$(0)
IF IS_NUMBER(r$)
c=VAL(r$):LIST.GET l,c,s$
ENDIF
s$=REPLACE$(s$,"<g>","")
FN.RTN s$
FN.END
FN.DEF _z()
_2u("relgraph"):LIST.CREATE s,g
LIST.ADD g,"help","notes","web","history","weight<g>","weekly exercise<g>","monthly exercise<g>","daily cals<g>","weekly cals<g>","monthly cals<g>","weekly items<g>","monthly items<g>","food frequency<g>","weekly stars<g>","monthly stars<g>","weekly frowns<g>","monthly frowns<g>","cals histogram<g>","items histogram<g>","cals vs items<g>","analysis","edit data","Leave Us Feedback!"
LIST.ADD g,"done"
DO
c=0:c$=_35$(g,"",&c)
IF !c THEN FN.RTN 0
SW.BEGIN c$
SW.CASE "help"
_4c():SW.BREAK
SW.CASE "notes"
_41():SW.BREAK
SW.CASE "weight"
_p():SW.BREAK
SW.CASE "web"
_3u(""):SW.BREAK
SW.CASE "daily cals"
_2g(0):SW.BREAK
SW.CASE "weekly exercise"
_b():SW.BREAK
SW.CASE "monthly exercise"
_2():SW.BREAK
SW.CASE "weekly items"
_w():SW.BREAK
SW.CASE "monthly items"
_3():SW.BREAK
SW.CASE "weekly cals"
_c():SW.BREAK
SW.CASE "monthly cals"
_7():SW.BREAK
SW.CASE "food frequency"
_2x():SW.BREAK
SW.CASE "cals histogram"
_2g(1):SW.BREAK
SW.CASE "weekly stars"
_v(7,1):SW.BREAK
SW.CASE "monthly stars"
_v(30,1):SW.BREAK
SW.CASE "weekly frowns"
_v(7,-1):SW.BREAK
SW.CASE "monthly frowns"
_v(30,-1):SW.BREAK
SW.CASE "items histogram"
_2e():SW.BREAK
SW.CASE "cals vs items"
_5():SW.BREAK
SW.CASE "history"
_o():SW.BREAK
SW.CASE "analysis"
_38():SW.BREAK
SW.CASE "Leave Us Feedback!"
BROWSE "https://play.google.com/store/apps/details?id=com.rfo.speakcc"
SW.BREAK
SW.CASE "edit data"
_3s("fooddb.txt")
SW.BREAK
SW.END
UNTIL c$="done"
FN.END
FN.DEF _34(x$)
IF x$="" THEN FN.RTN 0
dq$="\""
h$="<html><head><style>div {background:#567;"
h$+="background:linear-gradient(to right bottom,#,#134);border-radius:6px;"
h$+="text=~#ff0~ font:bold 30px Courier height:auto;}</style> </head>"
h$+="<div><body text=~#ff0~ style=~font:bold 16px Courier~> "
h$+="<body onclick=~window.location.href='test';~ style=~cursor:pointer;width:100%;height:100%;~>"
h$+=x$:h$+="</body></div></html>":h$=REPLACE$(h$,"~",dq$)
dm("h"):wLn("temp.html",h$):HTML.LOAD.STRING h$:PAUSE 2000:_20$(0)
FN.END
FN.DEF _4d$(s$)
FN.RTN "<span style=\"color:#0F0\">"+s$+"</span>"
FN.END
FN.DEF _4c()
Bb$="<br><br>"
dm("h"):w$="<html>"
w$+="<head><style> div {background:#567;background:linear-gradient(to right bottom,#599,#135);"
w$+="border-radius:6px;text=\"#ff0\" font:bold 30px Courier height:auto;}</style> </head>"
w$+="<div><body text=\"#fff\" style=\"font:bold 18px Arial\">"
w$+="<body onclick=\"window.location.href='test';\" style=\"cursor:pointer;width:100%;height:100%;\"><br>"
w$+="<br>use the voice command "+_4d$("HELP")+" to display this help page"+bb$
w$+="Enter food energy values using either calories or points but only use one system.<br>say new foods in the format:<br>"
w$+="food ### calories/points<br>ex:<br>"
w$+="\"banana 100 calories\"<br>or   \"banana 2 points\" <br>"
w$+="<br>Add new food by saying  the food name,wait and key in a value for it."+bb$+"If food was already entered,don't use the keyword calories and it will recall the last value. Type in a new value or wait 10 seconds and it will accept the recalled value."+bb$
w$+="Record multiple foods at once by using the keyword "+_4d$("PLUS")+"<br>"
w$+="example:\"banana plus cereal plus milk\""+bb$
w$+="You can exit the main diary screen by tapping on the middle of the screen."+bb$+"Other commands:"+bb$
w$+="say "+_4d$("TARGET")+" to enter in daily energy goal"+bb$
w$+=_4d$("CLEAR")+" to unmark good foods. You can also toggle stars by clicking to the left of each food name on the calorie/point numbers in the food diary"+bb$
w$+=_4d$("QUIT")+" to exit<br>"
w$+="In autoquit mode app will also autoexit after each completed food entry"+bb$
w$+=_4d$("SHOW")+" to display today's diary.  You can also click the speech cancel button to bring up the diary screen."+bb$
w$+=_4d$("SPEECH ON")+" or "+_4d$("SPEECH OFF")+" to turn on/off text-to-speech calorie announcement"+bb$
w$+=_4d$("REMOVE")+" to remove last entry"+bb$
w$+=_4d$("RECORD")+" to record today's weight"+bb$
w$+=_4d$("SEARCH")+" to look up calories on the web"+bb$
w$+=_4d$("NOTES")+" to record/view notes in diary"+bb$
w$+=_4d$("REMIND")+" to set personal reminder banner message"+bb$
w$+=_4d$("HISTORY")+" to view past days' food entries"+bb$
w$+=_4d$("DISPLAY")+" to show graphs of weight,daily calories and other items"+bb$
w$+=_4d$("ANALYZE")+" to analyze which foods in your history are associated with eating less or more daily calories"+bb$
w$+=_4d$("EMAIL")+" to email weight log,archive or food database"+bb$
w$+=_4d$("INSPIRE ME")+" to read your reminders out randomly"+bb$
w$+=_4d$("RESET")+" to clear entire app ***USE CAREFULLY***"+bb$
w$+="Privacy Policy"+bb$
w$+="We respect your privacy and do not collect or share your personal information with other parties."+bb$
w$+="We do not use cookies or advertising,however if you link to an outside service/webpage such as Google,YouTube,bing etc. from within our apps,they probably do."+bb$
w$+="You are able to optionally share some information (i.e. diet logs or grocery lists) with others via email functionality built into some of our apps. These require you to use your own email app to send data to third parties of your choice."+bb$
w$+="Most databases and temporary files in our apps are NOT encrypted and can be used and edited yourself using SQLITE and text editors."+bb$
w$+="Some of our applications (such as Voice Calorie Counter and Done List) use a Microphone with Google Speech Recognition which may involve sending sound samples of your voice to Google."+bb$
w$+="The privacy policy for Google can be found here:"+bb$
w$+="https://privacy.google.com/index.html"+bb$+"</body></div></html>"
HTML.LOAD.STRING w$:PAUSE 2000:_20$(0)
FN.END
FN.DEF _33(ft$[],n)
IF n<2 THEN FN.RTN 0
DIM x[n],y$[n]
FOR i=1 TO n
x[i]=VAL(ft$[i,2]):y$[i]=ft$[i,1]
NEXT
FOR i=1 TO n-1
ARRAY.MIN mn,x[i,n-i+1]:ARRAY.SEARCH x[i,n],mn,pos
SWAP x[i],x[pos+i-1]:SWAP y$[i],y$[pos+i-1]
NEXT
FOR I=1 TO n
ft$[i,2]=INT$(x[i]):ft$[i,1]=y$[i]
NEXT
FN.END
FN.DEF _y$(ft$[],nn)
a$="":z$="<br>"
TAB$="   ":start=0
DO
start++:s$=ft$[start,2]
v=0
IF s$<>"" THEN v=VAL(s$)
UNTIL v<>0
a$+="undereating foods"+z$
a$+="<table><tr><th>avg</th><th>food</th></tr>"
FOR i=start TO MIN(start+49,nn)
c$=ft$[i,2]:f$=ft$[i,1]
IF c$<>"0"
a$+="<tr><td>"+c$+"</td><td>"+f$+"</td></tr>"
ENDIF
NEXT
a$+="</table>"+z$+"overeating foods"+z$
a$+="<table><tr><th>avg</th><th>food </th></tr>"
FOR i=MAX(1,nn-49) TO nn
c$=ft$[i,2]:f$=ft$[i,1]
IF c$<>"0"
a$+="<tr><td>"+c$+"</td> <td>"+f$+"</td></tr>"
ENDIF
NEXT
a$+="</table>"
FN.RTN a$
FN.END
FN.DEF _32$()
BUNDLE.GET 1,"db",db:dm(""):? "analysis..."
BUNDLE.CREATE daytot:BUNDLE.CREATE foodsum:BUNDLE.CREATE foodct
now=FLOOR(now())
FOR d=now-365 TO now
d$=_f$(d):n=0:s=0:_2v(d$,&n,&s)
_u(&daytot,d$,s)
NEXT
FOR i=now-365 TO now
d$=_f$(i)
SQL.QUERY c,db,"diary","food","date='"+d$+"'"
SQL.QUERY.LENGTH n,c
FOR j=1 TO n
SQL.NEXT done,c,cv$
cv$=_l$(cv$)
_u(foodct,cv$,1)
BUNDLE.GET daytot,d$,t
_u(foodsum,cv$,t)
NEXT
NEXT
BUNDLE.KEYS foodsum,foodlist
LIST.SIZE foodlist,nn
? "tallied"
BUNDLE.CREATE foodavg
k=0
FOR i=1 TO nn
LIST.GET foodlist,i,f$:BUNDLE.GET foodsum,f$,s:BUNDLE.GET foodct,f$,n
IF n>=5 THEN BUNDLE.PUT foodavg,f$+"("+INT$(n)+"x)",FLOOR(s/n):k++
NEXT
IF k<10 THEN POPUP "not enough data yet!":FN.RTN ""
?INT$(k)+" averages calculated"
BUNDLE.KEYS foodavg,keys
DIM ft$[k,2]
FOR i=1 TO k
LIST.GET keys,i,f$:BUNDLE.GET foodavg,f$,c
ft$[i,1]=f$:ft$[i,2]=INT$(c)
NEXT
IF k>=10
_33(&ft$[],k):? "sort done":PAUSE 2000:CLS
FN.RTN _y$(ft$[],k)
ELSE
FN.RTN "not enough entries yet!"
ENDIF
FN.END
FN.DEF _9$()
good$="+":bad$="-"
BUNDLE.CREATE fb:LIST.CREATE s,tt
z$="<br>"
backdays=_36("go back","how many days? ",30)
CLS
now=FLOOR(now()):sd=now-backdays
FOR d=sd TO now
t=0:d$=_f$(d):diary$=_3g$(d$)
IF LEN(diary$)<5 THEN F_N.CONTINUE
LIST.ADD tt,d$,"  ":UNDIM dd$[]:SPLIT dd$[],diary$,"\n"
ARRAY.LENGTH z,dd$[]
FOR i=1 TO z
r1$=WORD$(dd$[i],1,"\t"):r2$=WORD$(dd$[i],2,"\t"):r3$=WORD$(dd$[i],3,"\t")
_u(fb,r1$,1):c=_4i(r2$)
m=_3c(r1$)
IF m=1 THEN r1$+=good$
IF m=-1 THEN r1$+=bad$
t+=c
LIST.ADD tt,_3o$(c):LIST.ADD tt,r1$
NEXT
LIST.ADD tt,"----","-----------------------",_3o$(t),INT$(z)+" items","    ","  "
NEXT
LIST.SIZE tt,z
IF !z
POPUP "not enough data!":FN.RTN ""
ENDIF
UNDIM tbl$[]:LIST.TOARRAY tt,tbl$[]
x$="":ARRAY.LENGTH n,tbl$[]
Sp$="      "
FOR i=1 TO n-1 STEP 2
x$+=tbl$[i]+" "+tbl$[i+1]+z$
NEXT
x$+=z$+z$+"food frequency (>3)"+z$
BUNDLE.KEYS fb,keys:LIST.SIZE keys,z:DIM xx$[z,2]
zz=0
FOR i=1 TO z
LIST.GET keys,i,f$:BUNDLE.GET fb,f$,freq
IF freq>3 THEN zz++:xx$[zz,1]=f$:xx$[zz,2]=INT$(freq)
NEXT
_33(&xx$[],zz)
FOR i=zz TO 1 STEP -1
x$+=xx$[i,2]+"x "+xx$[i,1]+z$
NEXT
FN.RTN x$+"<br>"+ss$
FN.END
FN.DEF _o()
X$=_9$():Xx$=REPLACE$(x$,"<br>","\n"):Xx$=REPLACE$(xx$,"&nbsp;"," ")
CLIPBOARD.PUT xx$:POPUP "copied to clipboard":_34(x$)
FN.END
FN.DEF _d$()
s$=_9$():s$=REPLACE$(s$,"<br>","\n"):s$=REPLACE$(s$,"&nbsp;"," ")
FN.RTN s$
FN.END
FN.DEF _31()
dm("")
IF _48("email.txt")
em$=_4l$("email.txt")
ELSE
INPUT "enter default email to send data to",em$,em$,cn
IF cn THEN FN.RTN 0
wLn("email.txt",em$)
ENDIF
TIME Year$,Month$,Day$,Hour$,Minute$,Second$,WeekDay,isDST
Today$=month$+"-"+day$+"-"+year$
DO
DIALOG.MESSAGE ,"email to "+em$,c,"yes","edit","no"
IF c=2
INPUT "change email",em$,em$,cn
IF cn THEN D_U.BREAK
wLn("email.txt",em$)
ENDIF
UNTIL c<>2
IF emcancel THEN FN.RTN 0
IF c=3 THEN FN.RTN 0
LIST.CREATE s,gr
LIST.ADD gr,"weight log","archive","food db","cancel"
DO
c=0:_35$(gr,"choose email type",&c)
IF c=4 THEN D_U.BREAK
IF c=1 & _48("weight.txt")
GRABFILE f$,"weight.txt"
EMAIL.SEND em$,"weight log "+today$,f$
ENDIF
IF c=2
f$=_d$()
IF f$<>"" THEN EMAIL.SEND em$,"archive "+today$,f$
ENDIF
IF c=3 & _48("fooddb.txt")
GRABFILE f$,"fooddb.txt"
EMAIL.SEND em$,"fooddb "+today$,f$
ENDIF
DO
PAUSE 100
UNTIL !BACKGROUND()
UNTIL c=4
FN.END
FN.DEF _3z$(p$,s$)
TEXT.INPUT s$,s$,p$:FN.RTN s$
FN.END
FN.DEF _2j$(p$,s$)
dm("")
TEXT.INPUT s$,TRIM$(s$)+"\n"
FN.RTN TRIM$(s$)
dm("h")
w$="<html> <head><meta http-equiv=\"content-type\"content=\"text/html;charset=UTF-8\"/>"
w$+="<title>edit text</title> </head><script type=\"text/javascript\">"
w$+="function DL(data) {Android.dataLink(data);} </script>"
w$+="<body bgcolor=\"black\"><div align=\"left\">"
w$+="<h2 style=\"color:#aaa;\">###prompt</h2>"
w$+="<form id='main' method='get' action='FORM'>"
w$+="<input type='submit' style=\"float:right;\" name='submit' value='Done'/>"
w$+="<TEXTAREA NAME=\"id\" ROWS=18 COLS=35>"
w$+="###edit"
w$+="</TEXTAREA></form></div></body></html>"
w$=REPLACE$(w$,"###prompt",p$):w$=REPLACE$(w$,"###edit",TRIM$(s$))
HTML.LOAD.STRING w$
HTML.LOAD.URL "javascript:DL(document.getElementById('id'))h
r$=_20$(0)
IF r$="QUIT" THEN FN.RTN S$
s$=DECODE$("URL","UTF-8",r$):s$=REPLACE$(s$,"SUBMIT&submit=Done&id=",""):s$=LEFT$(s$,LEN(s$)-1)
FN.RTN s$
FN.END
FN.DEF _4b(vl)
LET v$="say food name,command,Help or hit cancel"
CALL dm("")
STT.LISTEN v$
STT.RESULTS vl
CALL _2b(vl)
FN.END
FN.DEF _3s(db$)
LIST.CREATE s,m
LIST.ADD m,"edit lookup db","erase lookup db","import food db","edit weight log","cancel"
DO
p$="select choice":c=0:c$=_35$(m,p$,&c)
IF !c THEN FN.RTN 0
IF c$="edit lookup db" THEN _3v(db$)
IF c$="erase lookup db"
IF _43("are you sure???")
FILE.DELETE e,db$:POPUP "deleted"
ELSE
POPUP "del canceled"
ENDIF
ENDIF
IF c$="import food db"
b$=_r$("sampledb/"):f$=_4a$(b$):aLn(db$,f$):_3v(db$):POPUP "database imported!"
ENDIF
IF c$="edit weight log" THEN _3v("weight.txt"):_3p("changed")
UNTIL c$="cancel"
FN.END
CALL _m()
IF _45()
BUNDLE.GET 1,"version",nv
IF nv<>_4m("version")
_4k("version",nv)
ENDIF
LET h$="html/images/"
ARRAY.LOAD htmlRes$[],"autoc.png","graphs.png","green.png","red.png", "yellow.png"
ARRAY.LENGTH nres,htmlRes$[]
FOR i=1 TO nres
CALL _2a(h$+htmlRes$[i])
NEXT
CALL _2a("sampledb/samplefooddb.txt")
ENDIF
CALL _4e()
main:
LIST.CREATE s,vl
CALL _2s()
GOSUB declare
LET r$=_20$(1)
IF r$="QUIT"
IF _4m("listen") THEN GOTO leave
EXIT
HOME
DO
PAUSE 1000
UNTIL !BACKGROUND()
date$=_3f$():wLn("current",date$)
GOTO main
ENDIF
IF r$="VOICE"
LIST.CREATE s,vl
_4b(&vl)
ENDIF
IF r$="STT" THEN r$="VOICE":STT.RESULTS vl
IF r$<>"QUIT" THEN CALL _0(r$,vl)
IF r$="TYPE"
r$="VOICE":LET f$=_e$("Enter food name:","fooddb.txt"):dm("h")
IF f$<>"" THEN LIST.ADD vl,f$:CALL _0(r$,vl)
ENDIF
GOTO main
Declare:
IF declared THEN RETURN
FN.DEF _3r(y[])
 ARRAY.LENGTH n,y[]:DIM x[n]:ARRAY.COPY y[],x[]:ARRAY.SORT x[]:ARRAY.LENGTH i,x[]
 IF i/2<>FLOOR(i/2)
  M=x[FLOOR((i+1)/2)]
 ELSE
  M=(x[FLOOR((i)/2)]+x[FLOOR((i)/2)+1])/2
 ENDIF
 UNDIM x[]:FN.RTN m
FN.END
FN.DEF _2i$(n,prec)
 IF prec>=0 THEN FN.RTN TRIM$(FORMAT$("#######",n))
 IF prec=-1 THEN FN.RTN TRIM$(FORMAT$("#######.#",n))
 IF prec=-2 THEN FN.RTN TRIM$(FORMAT$("#######.##",n))
 FN.RTN TRIM$(FORMAT$("#######.###",n))
FN.END
FN.DEF bar(d[],l$[],title$)
 star=IS_IN("star",title$)|IS_IN("burned",title$)
 GR.SCREEN w,h
 GR.CLS
 WAKELOCK 3
 xo=0 :yo=-h/10
 x1=w*0.1:x2=w*0.9
 y1=h*0.1:y2=h*0.7
 dx=ABS(x2-x1) :dy=ABS(y2-y1) :dy2=0.15*dy
 ARRAY.MAX max,d[]
 ARRAY.AVERAGE avg,d[]
 ARRAY.LENGTH l,d[]
 med=_3r(d[])
 bw=dx/(l*4)
 g=50
 GR.COLOR 255,g,g,g,1
 GR.RECT gn,x1-xo-2,y1-yo-dy2-2,x2-xo+2,y2-yo+2
 GR.COLOR 255,255,255,255,0
 GR.SET.STROKE 2
 GR.RECT gn,x1-xo-2,y1-yo-dy2-2,x2-xo+2,y2-yo+2
 xspace=(x2-x1)/l
 GR.TEXT.SIZE dy/50
 GR.TEXT.ALIGN 2
 FOR i=1 TO l
  IF (d[i]>med & !star)|(star & d[i]<med)
   GR.COLOR 255,255,0,0,1
  ELSE
   GR.COLOR 255,0,255,0,1
  ENDIF
  t$=l$[i]
  bh=d[i]*dy/max
  GR.RECT gn,x1-xo+xspace*(i-0.5)-bw,y2-yo-bh,x1-xo+xspace*(i-0.5)+bw,y2-yo-2
  GR.COLOR 255,255,255,255,1
  GR.LINE gn,x1-xo+xspace*(i-0.5),y2-yo+10,x1-xo+xspace*(i-0.5),y2-yo
  GR.ROTATE.START 270,x1-xo+xspace*(i-0.5),y2-yo+dy/10
  GR.TEXT.DRAW gn,x1-xo+xspace*(i-0.5),y2-yo+dy/10,t$
  GR.ROTATE.END
  !t$=str$(round(d[i],1))
  t$=INT$(d[i])
  GR.ROTATE.START 270,x1-xo+xspace*(i-0.5),y2-yo-0.05*dy
  GR.TEXT.DRAW gn,x1-xo+xspace*(i-0.5),y2-yo-0.05*dy,t$
  GR.ROTATE.END
 NEXT
 GR.TEXT.SIZE dy/25
 GR.TEXT.TYPEFACE 4,1
 GR.TEXT.DRAW gn,x1+dx/2-xo,y1-yo-10,title$
FN.END
FN.DEF _2h(x[],y[],xname$,yname$,r)
 dm("g")
 ARRAY.LENGTH z,x[]
 IF z>30
  _n(x[],y[],xname$,yname$,r)
  FN.RTN 0
 ENDIF
 IF z <2 THEN FN.RTN 0
 DIM x$[z]
 FOR i=1 TO z
  x$[i]=_f$(x[i])
 NEXT
 bar(y[],x$[],yname$)
 call _3y()
 DO
  PAUSE 200
  GR.TOUCH t,x,y
 UNTIL t
 WAKELOCK 5
 DIALOG.MESSAGE yname$,_47$(y[]),c,"ok"
FN.END
FN.DEF _n(x[],y[],xname$,yname$,graphweight)
 !PRE:x[],y[] are scatter plot points 
 !      xname$,yname$ are axis titles
 !	    graphweight=0 :plot absolute values
 !      graphweight=1 :plot relative to maximum y value (i.e. see weight loss)
 dm("g2"):today=_3e(_3f$()):wLn("back.txt","0")
 relmode=_4m("relgraph"):ARRAY.LENGTH n,x[]
 IF n<2 THEN POPUP "need at least 2 points to make graph":FN.RTN 0
 ARRAY.MIN xmin,x[]:ARRAY.MAX xmax,x[]:ARRAY.MIN yymin,y[]
 IF graphweight=2 THEN xaxis=1 ELSE ymin=yymin
 ARRAY.MAX ymax,y[]:ARRAY.AVERAGE xavg,x[]:ARRAY.AVERAGE yavg,y[]
 ARRAY.STD_DEV xsd,x[]:ARRAY.STD_DEV ysd,y[]
 xrange=xmax-xmin:yrange=ymin-ymax:xxbar=0:Yybar=0:xybar=0
 FOR i=1 TO n
  xybar+=(x[i]*y[i]):xxbar+=(x[i]*x[i]):Yybar+=(y[i]*y[i])
 NEXT
 xybar=xybar/n:xxbar=xxbar/n:Yybar=yybar/n
 slope=(xybar-xavg*yavg)/(xxbar-xavg*xavg):intercept=yavg-slope*xavg
 !Rr=(xybar-xavg*yavg)/ SQR((xxbar-xavg*xavg)*(yybar-yavg*yavg))
 !Rr=rr*rr
 pxmax=xmax+FLOOR(xrange*0.1):pymax=ymax*1.02
 pxmin=xmin-FLOOR(xrange*0.2):pymin=ymin*0.98
 DO
  flip=0:GR.CLS:GR.SCREEN acwidth,acheight
  diwidth=(pxmax-pxmin):diheight=(pymax-pymin)
  IF (diwidth=0|diheight=0) THEN POPUP "not enough data":D_U.BREAK
  Sx=acwidth/diwidth:Sy=acheight/diheight
  GR.SET.STROKE 1:GR.COLOR 200,255,200,0,1
  Ny=(pymax-(yavg-ysd))*sy
  GR.LINE yyy,1,ny,(pxmax-pxmin)*sx,ny 
  Ny=(pymax-(yavg+ysd))*sy
  GR.LINE yyy,1,ny,(pxmax-pxmin)*sx,ny 
  ytics=10^FLOOR(LOG10(ABS(pymax-pymin)/10))
  IF (pymax-pymin)/ytics > 10 THEN ytics*=10
  IF (pymax-pymin)/ytics < 5 THEN ytics=MAX(FLOOR(ytics/5),1)
  Yline1=FLOOR(pymin/ytics)*ytics
  Yline2=CEIL(pymax/ytics)*ytics
  GR.TEXT.SIZE 15:GR.SET.STROKE 1:GR.TEXT.ALIGN 1
  py=FLOOR(LOG10(ABS(ytics)))
  FOR y=yline1 TO yline2 STEP ytics
   GR.COLOR 50,50,50,50,1
   Newx=(pxmax-pxmin)*sx:Newy=(pymax-y)*sy
   GR.LINE yyy,1,newy,newx,newy 
   GR.COLOR 200,50,50,50,1
   IF relmode
    GR.TEXT.DRAW txt3,1,newy-3,INT$(y-ymax)
   ELSE
    GR.TEXT.DRAW txt3,1,newy-3,_2i$(y,py)
   ENDIF
  NEXT
  xtics=10^FLOOR(LOG10(ABS(pxmax-pxmin)/10))
  DO
   IF (pxmax-pxmin)/xtics > 7 THEN xtics*=7
  UNTIL xtics=0|((pxmax-pxmin)/xtics <=10)
  IF (pxmax-pxmin)/xtics < 5 THEN xtics=MAX(FLOOR(xtics/5),1)
  xline1=FLOOR(pxmin/xtics)*xtics
  xline2=CEIL(pxmax/xtics)*xtics
  !px=FLOOR(LOG10(ABS(xtics)))
  GR.TEXT.ALIGN 3
  FOR x=xline1 TO xline2 STEP xtics
   Newx=(x-pxmin)*sx:Newy=(pymax-pymin)*sy*0.97
   ! g$=juliantogregorian$(x)
   IF xaxis THEN g$=INT$(x) ELSE g$=INT$(x-today)
   GR.COLOR 200,0,0,0,1:GR.TEXT.DRAW txt3,newx,newy,g$:GR.COLOR 50,0,0,0,1:GR.LINE xxx,newx,1,newx,newy 
  NEXT
  GR.SET.STROKE 5
  Oldx=(x[1]-pxmin)*sx:Oldy=(pymax-y[1])*sy
  GR.COLOR 200,0,0,200,1
  FOR i=1 TO n
   Newx=(x[i]-pxmin)*sx:Newy=(pymax-y[i])*sy
   GR.COLOR 50,0,0,200,1:GR.CIRCLE p,newx+5,newy+5,5:GR.COLOR 200,0,0,200,1
   !  GR.line lol,oldx,oldy,newx,newy 
   ! GR.POINT p,newx,newy
   GR.CIRCLE p,newx,newy,5
   !  Oldx=newx:Oldy=newy
  NEXT
  IF slope <=0 THEN GR.COLOR 80,0,255,0,1 ELSE GR.COLOR 80,255,0,0,1
  GR.SET.STROKE 9
  X1=pxmin:Y1=slope*x1+intercept:Xn=pxmax:Yn=slope*xn+intercept
  GR.LINE fl,(x1-pxmin)*sx,sy*(pymax-y1),sx*(xn-pxmin),sy*(pymax-yn)
  GR.TEXT.SIZE 25:GR.COLOR 200,50,50,50,1:GR.SET.STROKE 1:GR.TEXT.ALIGN 3
  IF xaxis
   s=slope:s$=""
  ELSE
   s=slope*7:s$="/week"
  ENDIF
  GR.TEXT.DRAW txt3,acwidth*0.9,acheight*0.1,_2i$(s,-2)+s$
  GR.TEXT.ALIGN 2:GR.COLOR 255,0,0,0,1
  GR.TEXT.DRAW txt2,acwidth*0.5,acheight*0. 93,xname$
  GR.ROTATE.START -90,acwidth*0.1,acheight*0.5
  GR.TEXT.DRAW txt2,acwidth*0.05,acheight*0.5,yname$
  GR.ROTATE.END:call _3y()
  WAKELOCK 3
  Cr$="\n":px=-2:py=-2
  msg$=yname$+cr$
  Msg$+=INT$(n)+" entries  in "+INT$(xrange+1)+" days"+cr$
  msg$+="Average= "+_2i$(yavg,py)+cr$
  msg$+="Median = "+_2i$(_3r(y[]),-1)+cr$+"Standard Deviation = "+_2i$(ysd,py)+cr$+"Min = "+_2i$(yymin,py)+cr$+"Max = "+_2i$(ymax,py)+cr$+"range = "+_2i$(ymax-yymin,py)+cr$
  DO
   PAUSE 100:bk=(_42()|BACKGROUND())
   IF bk THEN D_U.BREAK 
   GR.TOUCH touched,xx,yy:GR.SCREEN newwidth,newheight
   IF newwidth<>acwidth THEN flip=1:D_U.BREAK
  UNTIL touched 
  IF bk THEN D_U.BREAK 
 UNTIL !flop
 IF bk 
  WAKELOCK 5
  FN.RTN 0
 ENDIF
 IF msg$<>"" THEN DIALOG.MESSAGE ,msg$ ,c,"okay"
 dm("h")
 WAKELOCK 5
 FN.RTN 0
FN.END
FN.DEF _2z(f$,xname$,yname$,dlm$,r)
 IF !_48(f$)THEN FN.RTN 0
 TEXT.OPEN R,FN2,f$:xfield=1:yfield=2
 LIST.CREATE n,xx:LIST.CREATE n,yy
 DO
  TEXT.READLN FN2,a_line$:TEXT.EOF fn2,eof
  IF !eof&a_line$<>""
   d$=WORD$(a_line$,xfield,dlm$):x=_3e(&d$)
   IF x>0 THEN LIST.ADD xx,x:LIST.ADD yy,_4i(WORD$(a_line$,yfield,dlm$))
  ENDIF
 UNTIL eof
 TEXT.CLOSE FN2:UNDIM x[]:UNDIM y[]:LIST.SIZE xx,xn:LIST.SIZE yy,yn
 IF !xn|!yn THEN POPUP "no points":FN.RTN 0
 LIST.TOARRAY xx,x[]:LIST.TOARRAY yy,y[]
 _n(x[],y[],xname$,yname$,r)
 LIST.CLEAR xx:LIST.CLEAR yy
FN.END
FN.DEF _x(b,xname$,yname$,r)
 LIST.CREATE n,l2:LIST.CREATE n,l1
 BUNDLE.KEYS b,lll:LIST.SIZE lll,z
 IF z<2
  POPUP "need more points":FN.RTN 0
 ENDIF
 UNDIM ll$[]:LIST.TOARRAY lll,ll$[]:ARRAY.SORT ll$[]
 FOR i=1 TO z
  K$=ll$[i]:BUNDLE.GET b,k$,v:x=VAL(k$)
  IF x>0 THEN LIST.ADD l1,x:LIST.ADD l2,v
 NEXT
 UNDIM x[]:UNDIM y[]:LIST.TOARRAY l1,x[]:LIST.TOARRAY l2,y[]
 _2h(x[],y[],xname$,yname$,r)
 UNDIM x[]:UNDIM y[]:LIST.CLEAR l1:LIST.CLEAR l2:UNDIM ll$[]:BUNDLE.CLEAR lll
FN.END
FN.DEF _8(b,p)
 BUNDLE.GET 1,"db",db
 now=FLOOR(now())-1
 FOR d=now-365 TO now
  LET d$=_f$(d):LET n=0:LET w$=INT$(FLOOR(d/p)*p):LET t=0:CALL _2v(d$,&n,&t)
  IF t THEN CALL _u(&b,w$,t)
 NEXT
FN.END
FN.DEF _c()
 BUNDLE.CREATE b:_8(&b,7):_x(b,"week","cals/pts",0)
FN.END
FN.DEF _7()
 BUNDLE.CREATE b:CALL _8(&b,30):_x(b,"30 days","cals/pts",0):BUNDLE.CLEAR b
FN.END
FN.DEF _5()
 !graph of total daily calories vs number of items eaten
 BUNDLE.GET 1,"db",db
 LIST.CREATE n,xx:LIST.CREATE n,yy
 startd=FLOOR(now())-1
 BUNDLE.CREATE b:DIM x[366],y[366]
 FOR d=startd-365 TO startd
  n=0:t=0
LET d$=_f$(d)
CALL _2v(d$,&n,&t):t+=_3h(d$)
  LET c=0:IF n THEN LET c=t
  LET j=d-startd+366
  IF n THEN LIST.ADD xx,n:LIST.ADD yy,c
 NEXT 
 LIST.SIZE xx,z
 IF z
  LIST.TOARRAY xx,x[]:LIST.TOARRAY yy,y[]
  _n(x[],y[],"items","daily cals",2)
 ELSE 
  POPUP "not enough data!"
 ENDIF
 BUNDLE.CLEAR b:LIST.CLEAR xx:LIST.CLEAR yy:UNDIM x[]:UNDIM y[]
FN.END
FN.DEF _2y(b,startd)
 !PRE:startd = julian date of starting date
 !POST:bundle is assigned date,total calories
 BUNDLE.GET 1,"db",db
 LET now=FLOOR(now())-1
 FOR d=startd TO now
  LET d$=_f$(d):LET n=0:LET t=0:CALL _2v(d$,&n,&t)
  IF t THEN BUNDLE.PUT b,INT$(d),t
 NEXT
FN.END
FN.DEF _2g(histo)
 !graph daily calories or histogram if histo=1
 IF histo THEN d=365 ELSE d=7
 g=_36("days to go back","",d)
 startd=_3e(_3f$())-g
 BUNDLE.CREATE b
 _2y(&b,startd)
 IF !histo
  _x(b,"days ago","cals/pts",0)
 ELSE
  _h("cals.txt",b)
  _2w("cals.txt","cals",2)
 ENDIF
 BUNDLE.CLEAR b
FN.END
FN.DEF _2f(b)
 !count items in bundle date,count
 BUNDLE.GET 1,"db",db
 now=FLOOR(now())-1
 FOR d=now-365 TO now
  LET d$=_f$(d):LET n=0:LET t=0:CALL _2v(d$,&n,&t)
  IF n THEN BUNDLE.PUT b,INT$(d),n
 NEXT
FN.END
FN.DEF _2e()
 !make items histogram
 BUNDLE.CREATE b
 _2f(&b)
 _h("items.txt",b)   
 _2w("items.txt","items",2):BUNDLE.CLEAR b
FN.END
FN.DEF _4(b,p)
 !count grouped items over daily period p
 BUNDLE.GET 1,"db",db
 now=FLOOR(now())
 FOR d=now-365 TO now
  LET d$=_f$(d):n=0
  LET w$=INT$(FLOOR(d/p)*p):LET t=0:CALL _2v(d$,&n,&t)
  IF n THEN CALL _u(&b,w$,n)
 NEXT
FN.END
FN.DEF _6(b,p)
 !count grouped exercise over daily period p
 BUNDLE.GET 1,"db",db
 now=FLOOR(now())
 FOR d=now-365 TO now
  d$=_f$(d)
  w$=INT$(FLOOR(d/p)*p)
  t=_3h(d$)
  IF t THEN CALL _u(&b,w$,t)
 NEXT
FN.END
FN.DEF _b()
 BUNDLE.CREATE b
 _6(&b,7)
 _x(b,"days ago","burned",0)
 BUNDLE.CLEAR b
FN.END
FN.DEF _2()
 BUNDLE.CREATE b
 _6(&b,30)
 _x(b,"days ago","burned (30 day)",0)
 BUNDLE.CLEAR b
FN.END
FN.DEF _w()
 !graph weekly items
 BUNDLE.CREATE b
 _4(&b,7)
 _x(b,"days ago","items (by week)",0)
 BUNDLE.CLEAR b
FN.END
FN.DEF _3()
 BUNDLE.CREATE b
 _4(&b,30)
 _x(b,"days ago","items (30 day)",0)
 BUNDLE.CLEAR b
FN.END
FN.DEF _v(p,type)
 BUNDLE.CREATE b
 BUNDLE.GET 1,"db",db
 SQL.QUERY cursor,db,"diary","date,food",""
 SQL.QUERY.LENGTH n,cursor
 FOR i=1 TO n
  SQL.NEXT done,cursor,cv1$,cv2$
  LET d=_3e(cv1$)
  LET w$=INT$(FLOOR(d/p)*p)
  IF _3c(cv2$)=type THEN CALL _u(&b,w$,1)
 NEXT 
 IF type=1 THEN t$="stars" ELSE t$="frowns"
 _x(b,"date",t$,0)
 BUNDLE.CLEAR b
FN.END
FN.DEF _2x()
 BUNDLE.CREATE b
 f$=_e$("Enter food name:","fooddb.txt")
 KB.HIDE
 LIST.CREATE s,v
 BUNDLE.GET 1,"db",db
 SQL.QUERY cursor,db,"diary","date,food",""
 SQL.QUERY.LENGTH n,cursor
 FOR i=1 TO n
  SQL.NEXT done,cursor,cv1$,cv2$
  d=_3e(cv1$)
  w$=INT$(FLOOR(d/7)*7)
  IF TRIM$(cv2$)=f$ THEN _u(&b,w$,1)
 NEXT 
 _x(b,"week",f$,0)
 BUNDLE.CLEAR b
FN.END
FN.DEF _3j(X,iv,minx,maxx)
 FN.RTN FLOOR((x-minx)/iv)+1
FN.END
FN.DEF _3q(b,iv,minx,maxx)
 FN.RTN CEIL((b-1)*iv)+minx
FN.END
FN.DEF _3i(b,iv,minx,maxx)
 FN.RTN CEIL((b)*iv)+minx-1
FN.END
FN.DEF _2d(x[],cutoff,n$)
 !el=clock()
 ARRAY.LENGTH i,x[]:ARRAY.AVERAGE avg,x[]
 ARRAY.STD_DEV sd,x[]:ARRAY.MIN Minx,x[]
 ARRAY.MAX Maxx,x[]:ARRAY.SORT x[]
 IF I < 3 THEN FN.RTN 0
 IF i/2<>FLOOR(i/2)
  Median=x[FLOOR((i+1)/2)]
 ELSE
  Median=(x[FLOOR((i)/2)]+x[FLOOR((i)/2)+1])/2
 ENDIF
 Nbins=FLOOR(POW(i,(1/3)))
 iv=((maxx-minx)/nbins)
 IF iv<1 THEN iv=1
 Cr$="\n":Ff$="#######.####"
 Stat$="n="+INT$(i)+cr$+"Average="+FORMAT$(ff$,avg)+cr$+"Standard Deviation="+FORMAT$(ff$,sd)+cr$+"Min="+FORMAT$(ff$,Minx)+cr$+"Max="+FORMAT$(ff$,Maxx)+cr$+"Median="+FORMAT$(ff$,median)
 dm("g")
 GR.ORIENTATION 0:GR.SCREEN w,h:GR.STATUSBAR statush,statshow
 IF statshow THEN h-=statush
 POPUP "swipe finger left/right to change interval size"
 wLn("back.txt","0"):Maxbins=500:DIM bins[maxbins]
 DO
  Nbins=_3j(maxx,iv,minx,maxx)
  nbins=MIN(nbins,maxbins)
  ARRAY.FILL bins[1,nbins],0
  FOR j=1 TO i
   ! B=_3j(x[j],iv,minx,maxx)
   B=FLOOR((x[j]-minx)/iv)+1:BINS[b]++
  NEXT
  Avgbin=_3j(avg,iv,minx,maxx)
  Sd1bin=_3j(avg-sd,iv,minx,maxx)
  Sd2bin=_3j(avg+sd,iv,minx,maxx)
  Medbin=_3j(median,iv,minx,maxx)
  binmax=0
  FOR k=1 TO nbins
   binmax=MAX(binmax,bins[k])
   !ARRAY.MAX binmax,bins[1,nbins-1] 
  NEXT 
  Maxh=binmax:sx=w/nbins
  sy=h/binmax:diwidth=nbins*sx
  diheight=binmax*sy:Th=FLOOR(23*h/480)
  GR.CLS:GR.SET.STROKE 1
  FOR bin=1 TO nbins
   !RED by default
   GR.COLOR 255,255,0,0,1
   IF bin=sd1bin|bin=sd2bin THEN GR.COLOR 255,255,0,100,1
   IF avgbin=bin THEN GR.COLOR 255,0,100,0,1 
   IF medbin=bin THEN GR.COLOR 255,0,0,255,1
   GR.RECT lx,(bin-1)*sx,(maxh-bins[bin])*sy,bin*sx,maxh*sy
   IF nbins<16
    F=1:GR.SET.STROKE 1
    !grey text
    GR.COLOR 255,70,70,70,1:GR.TEXT.SIZE th:GR.TEXT.ALIGN 2
    V1=_3q(bin,iv,minx,maxx):V2=_3i(bin,iv,minx,maxx)
    !IF bins[bin]=binmax
    ! GR.TEXT.SIZE ceil(th/2)
    !  GR.TEXT.DRAW txt1,(bin-0.5)*sx,sy*maxh*0.05,INT$(binmax)
    ! ENDIF
    GR.TEXT.SIZE th
    GR.TEXT.DRAW txt1,(bin-0.5)*sx,sy*maxh*0.92,INT$(v1)+"-"
    GR.TEXT.DRAW txt1,(bin-0.5)*sx,sy*maxh*0.97,INT$(v2)
   ENDIF
  NEXT
  GR.TEXT.SIZE th*0.75:GR.COLOR 255,0,255,0,f:GR.TEXT.ALIGN 3
  GR.TEXT.DRAW txt2,nbins*sx*0.95,maxh*sy*0.1,n$
  GR.TEXT.DRAW txt2,nbins*sx*0.95,maxh*sy*0.15,INT$(iv)+" binsize"
  GR.TEXT.DRAW txt2,nbins*sx*0.95,maxh*sy*0.20,INT$(nbins)+" bins"
  GR.TEXT.DRAW txt2,nbins*sx*0.95,maxh*sy*0.25,INT$(binmax)+" binmax"
  call _3y()
  tstart=CLOCK():Wait=0
  DO
   GR.TOUCH touched,xx,yy
   IF wait=0&(CLOCK()>tstart+3000)
    Wait=1
    !Draw a empty green rect
    fill=0:GR.SET.STROKE 2:GR.COLOR 255,0,200,0,fill
    l=w*0.1:t=h*0.1:r=w*0.3:b=h*0.3
    GR.RECT qb1,l,t,r,b
    !Label it
    GR.TEXT.SIZE 50:GR.COLOR 255,0,255,0,1:GR.TEXT.ALIGN 2
    GR.TEXT.DRAW qb2,w*0.2,h*0.2,"QUIT":call _3y()
   ENDIF
   IF wait=1&(CLOCK()>tstart+7000)
    Wait=0:GR.HIDE qb1:GR.HIDE qb2:call _3y()
    Tstart=CLOCK()
   ENDIF
   IF _4l$("back.txt")="1" THEN D_U.BREAK
  UNTIL touched
  IF yy>h/2&xx>3 THEN
   iv=FLOOR((maxx-minx)*(xx/(2*w)))
   IF iv<1 THEN iv=1
  ENDIF
  IF _4l$("back.txt")="1" THEN D_U.BREAK
 UNTIL (yy<h/2)
 DIALOG.MESSAGE ,stat$,c,"okay"
FN.END
FN.DEF _2w(f$,name$,fnum)
 dlm$="\t"
 IF !_48(f$) THEN FN.RTN 0
 TEXT.OPEN R,FN2,f$:LIST.CREATE n,xx
 DO
  TEXT.READLN FN2,a_line$
  IF a_line$ <>"EOF"&a_line$ <> ""
   n++:x=_4i(WORD$(a_line$,fnum,dlm$))
   IF x>0 THEN LIST.ADD xx,x
  ENDIF
 UNTIL a_line$="EOF"
 UNDIM x[]:LIST.SIZE xx,sz
 IF sz>3
  LIST.TOARRAY xx,x[]:_2d(&x[],0,name$)
 ELSE
  POPUP "not enough data"
 ENDIF
 TEXT.CLOSE FN2
FN.END
Declared=1
RETURN
ONBACKKEY:
BUNDLE.PUT 1,"back","1"
BACK.RESUME
ONCONSOLETOUCH:
ct=1
IF bready
BUNDLE.PUT 1,"ctouch",1
ENDIF
CONSOLETOUCH.RESUME
ONBACKGROUND:
IF bready
IF BACKGROUND()
BUNDLE.PUT 1,"home",0
ELSE
BUNDLE.PUT 1,"home",1
ENDIF
ENDIF
BACKGROUND.RESUME
leave:
BUNDLE.GET 1,"db",db
SQL.CLOSE db
EXIT

