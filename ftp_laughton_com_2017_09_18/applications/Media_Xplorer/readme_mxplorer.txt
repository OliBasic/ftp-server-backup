media explorer v1.0
by [Antonis]
Created on Feb.15.2012

This program is a file browser, a text viewer, an audio player
and an image viewer, in one package! 

The program supports:

image files of the type:
"jpg","jpeg","gif","png","bmp","webp"

audio files:
"3gp","mp4","m4a","aac","ts","flac","mp3","mid","xmf","mxmf",
"rtttl","rtx","ota","imy","ogg","mkv","wav"

text files:
"txt","bas","html","htm","xml"
any other type of file can be opened as a text file.
Some type of files would be supported on OSes>3.0 or >4.0
