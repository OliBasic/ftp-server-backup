rem media explorer v1.0
rem Feb.15.2012
rem by [Antonis]


! begin audio routine
! plays myfile till end or stopped
FN.def mplay(myfile$)
a=0
AUDIO.load au,myfile$
AUDIO.length aulen,au
GR.set.stroke 4
GR.TEXT.size 40
GR.color 255,255,0,0,1
GR.rect ob0, 0,200,799,750
GR.color 255,255,255,255,1
GR.TEXT.draw ob1,250,240,"Audio Player"
FILE.Size s,myfile$
GR.color 255,0,0,255,1
GR.TEXT.draw ob,50,340,"Size: "+LEFT$(STR$(s),LEN(STR$(s))-2)+" bytes"
GR.TEXT.draw ob2,50,400,"Length "+STR$(aulen/1000)+" sec."
GR.TEXT.draw ob3,50,460,"Playing:..."+RIGHT$(myfile$,23)
GR.color 255,0,255,0,1
GR.rect ob4, 50,500,750,600
GR.render
x1=0
y1=0
POPUP "Tap screen to stop!",0,100,0
PAUSE 1000
AUDIO.play au
GR.color 255,0,255,255,1
GR.hide ob5
GR.line ob5, 50+x1, 500+y1, 50+x1, 600+y1
initialCLOCK=CLOCK()
DO 
 currentCLOCK=CLOCK()-initialCLOCK
 PAUSE 50
 AUDIO.position.current  curpos
 newposfraction=curpos/aulen
 x1=700*newposfraction
 IF curpos<>0 %& curpos<=aulen THEN
  c$=STR$(curpos/1000)
  l=LEN(STR$(curpos/1000))
  GR.hide ob7
  GR.TEXT.draw ob7,50,640,"Current position "+LEFT$(c$,l-2) 
  GR.hide ob5
  GR.line ob5, 50+x1, 500+y1, 50+x1, 600+y1
 ENDIF  
 GR.render
 GR.bounded.touch touched, 0, 0, 799, 1232  
UNTIL curpos>=aulen | touched>0
AUDIO.stop
GR.hide ob5
GR.line ob5, 752, 500, 752, 600
GR.render
IF touched=0 THEN 
  POPUP "end of file",0,0,0
 ELSE
   POPUP "audio stopped",0,0,0
ENDIF
AUDIO.release au
GR.color 255,255,255,0,1
GR.TEXT.size 60

! wait FOR touch
GR.TEXT.draw ob6,30,920,"TOUCH SCREEN TO RETURN"
GR.render
lastCLOCK=CLOCK()
off=1
DO
 PAUSE 50
 GR.touch touched,x,y
 IF CLOCK()-lastCLOCK>500 THEN 
     off=!off
     lastCLOCK=CLOCK()
   IF off>0 THEN
     GR.hide ob6 
   ELSE 
    GR.show ob6
 ENDIF
 GR.render
ENDIF
UNTIL touched>0
POPUP "end",0,0,0
GR.set.stroke 0
GR.cls
a=1
FN.rtn a
FN.end

! returns 1 or 2 
FN.def ok_cancel(x,y,text$,a1$,a2$)
GR.screen w,h
x1=x
y1=y
x2=x1+w/2
y2=y1+(3/4)*w/2
GR.screen w,h
sx=w/800
sy=h/1232
GR.TEXT.align 2
t1$=LEFT$(text$,20)
t2$=MID$(text$,21,LEN(text$)-20)
GR.set.stroke 4

! external,internal rect+line of caption 
GR.color 255,0,0,0,1
GR.rect r0,x1,y1,x2,y2
GR.color 255,0,0,255,0
GR.rect r1,x1,y1,x2,y2
GR.line lin1,x1,y1+(y2-y1)/4,x2,y1+(y2-y1)/4

! ok, cancel rect
GR.color 255,0,255,255,1
GR.set.stroke 0
GR.rect r2,x1+(x2-x1)/11,y1+7*(y2-y1)/11,x1+5*(x2-x1)/11,y1+10*(y2-y1)/11 
GR.rect r3,x1+6*(x2-x1)/11,y1+7*(y2-y1)/11,x1+10*(x2-x1)/11,y1+10*(y2-y1)/11

! text ok cancel
GR.color 255,255,0,0,1
GR.TEXT.size (x2-x1)/10
GR.TEXT.bold 4
xx=(x1+(x2-x1)/11)+((x1+5*(x2-x1)/11)-(x1+(x2-x1)/11))/2
GR.TEXT.draw txt1, xx,y1+5.7*(y2-y1)/7,a1$
xx=x1+6*(x2-x1)/11 + (x1+10*(x2-x1)/11-(x1+6*(x2-x1)/11))/2
GR.TEXT.draw txt2,xx,y1+5.7*(y2-y1)/7,a2$

! Please select
GR.TEXT.bold 0
GR.TEXT.size ((x2-x1)/8)
GR.color 255,255,255,0,0
xx=x1+(x2-x1)/2
GR.TEXT.draw txt3, xx,y1+(y2-y1)/5, "Please Choose!"

! text to print
GR.TEXT.size (x2-x1)/10
GR.color 255,255,0,255,255
xx=x1+(x2-x1)/2
GR.TEXT.draw txt4, xx,y1+2.1*(y2-y1)/5, t1$
GR.TEXT.draw txt5, xx,y1+2.9*(y2-y1)/5, t2$
GR.render
retouch:
DO 
   GR.touch touched, x, y
 UNTIL touched
IF x>sx*(x1+(x2-x1)/11) & y>sy*(y1+7*(y2-y1)/11) & x<sx*(x1+5*(x2-x1)/11) & y<sy*(y1+10*(y2-y1)/11) THEN
       GR.color 255,255,140,0,1
	   GR.rect r4,x1+(x2-x1)/11,y1+7*(y2-y1)/11,x1+5*(x2-x1)/11,y1+10*(y2-y1)/11 
	   GR.color 255,255,255,255,0
       xx=(x1+(x2-x1)/11)+((x1+5*(x2-x1)/11)-(x1+(x2-x1)/11))/2
	   GR.TEXT.draw txt6, xx,y1+5.7*(y2-y1)/7,a1$
	   GR.render
    ENDIF
IF x>sx*(x1+6*(x2-x1)/11) & y>sy*(y1+7*(y2-y1)/11) & x<sx*(x1+10*(x2-x1)/11) & y<sy*(y1+10*(y2-y1)/11) THEN
       GR.color 255,255,140,0,1
       GR.rect r5,x1+6*(x2-x1)/11,y1+7*(y2-y1)/11,x1+10*(x2-x1)/11,y1+10*(y2-y1)/11
	   GR.color 255,255,255,255,0
	   xx=x1+6*(x2-x1)/11 + (x1+10*(x2-x1)/11-(x1+6*(x2-x1)/11))/2
	   GR.TEXT.draw txt7, xx,y1+5.7*(y2-y1)/7,a2$
	   GR.render
    ENDIF
 DO 
   GR.touch touched, xfake, yfake
 UNTIL !touched
IF x>sx*(x1+(x2-x1)/11) & y>sy*(y1+7*(y2-y1)/11) & x<sx*(x1+5*(x2-x1)/11) & y<sy*(y1+10*(y2-y1)/11) THEN
	   a=1
	   GOTO fn_finished
    ENDIF
IF x>sx*(x1+6*(x2-x1)/11) & y>sy*(y1+7*(y2-y1)/11) & x<sx*(x1+10*(x2-x1)/11) & y<sy*(y1+10*(y2-y1)/11) THEN
	   a=2
	   GOTO fn_finished
    ENDIF
GOTO retouch	   

fn_finished:
GR.TEXT.align 1
GR.hide r0
GR.hide r1
GR.hide r2
GR.hide r3
GR.hide r4
GR.hide r5
GR.hide lin1
GR.hide txt1
GR.hide txt2
GR.hide txt3
GR.hide txt4
GR.hide txt5
GR.hide txt6
GR.hide txt7
GR.render
FN.rtn a
FN.end
! **********************

! finds type of file
FN.def ftype(myfile$)
ARRAY.load Formats$[],"jpg","jpeg","gif","png","bmp","webp"~
"3gp","mp4","m4a","aac","ts","flac","mp3","mid","xmf","mxmf","rtttl","rtx","ota","imy","ogg","mkv","wav"~
"video3gp","mp4","videots","webm","videomkv"~
"txt","bas","html","htm","xml"
ARRAY.length lform,Formats$[]
! file recognition
a$=""
i=0
DO
 b$= MID$(myfile$,LEN(myfile$)-i,1)
 a$=a$+b$
 i=i+1
UNTIL b$="."
c$=""
FOR j=0 TO i-2
 c$=c$+MID$(a$,i-1-j,1)
NEXT j
c$=lower$(c$)
index=0
FOR i=1 TO 33
 IF formats$[i]=c$ THEN index=i
NEXT i
IF index=0 THEN type=0 % unsupported
IF index>0 & index<=6 THEN type=1 % image
IF index>6 & index<=23 THEN type=2 % audio
IF index>23 & index<=28 THEN type=3 % video
IF index>28 & index<=33 THEN type=4 % text
FN.rtn type
FN.end

! *****************************
! Lists a directory. Gets and
! returns the user's selection
FN.def getfile$(path$)
GR.screen w,h
sx=w/800
sy=h/1232
unDIM recd[] 
unDIM arcd[]
unDIM txtd[]
DIM recd[150],arcd[150],txtd[150]
GR.cls

MainLoop:
GR.color 255,255,255,0,1
GR.TEXT.size 40
GR.hide status
GR.TEXT.draw status,5, 1230,path$
GR.TEXT.size 60

! Get the listing from the path directory and sort it
ARRAY.delete d1$[]
FILE.dir path$, d1$[]
ARRAY.length length, d1$[]

! Copy the file list adding the top ",," entry
ARRAY.delete d2$[] 
ARRAY.delete d3$[] % d3$[] dirs only
ARRAY.delete d4$[] % d4$[] files only
DIM d2$[length+1]
DIM d3$[length] 
DIM d4$[length]
d2$[1] = ".."
j=0
FOR i = 1 TO length
 d2$[i + 1] = d1$[i]
 n = is_in("(d)", d1$[i])
 IF n <> 0 THEN 
    d3$[i]=d1$[i] 
   ELSE 
	 j=j+1
	 d4$[j]=d1$[i]
  ENDIF
NEXT i

! Present the list and get the user'schoice
! ** graphics part **
GR.color 255, 49, 0, 98 ,1
GR.rect bkg,0,0,800,1187
GR.color 255,255,255,0,1
x=0
y=15
dy=70

! count dirs & files
nfiles=0
ndirs=0
FOR i=1 TO length
     n = is_in("(d)", d2$[1+i])
    IF n=0 THEN nfiles=nfiles+1 ELSE ndirs=ndirs+1
 NEXT i
dz=0

! print rectangles for objs
 FOR i=1 TO 17
    GR.color 255,255,255,0,0
    GR.rect arr,0,y-15+dz,799,y+55+dz
    GR.color 255,255,255,0,1
    dz=dz+70
 NEXT i

! print arrow+up key
  IF d2$[1]=".." THEN
     GR.color 255,0,255,0,1
     LIST.create n,mypoly
     LIST.add mypoly,x,y+20,x+30,y-5,x+30,y+45
     GR.poly polyObj,mypoly 
     GR.rect rec0,x+30,y+10,x+60,y+30
     GR.TEXT.draw tback,x+70,y+40,"Up"

     GR.render
     !list.clear mypoly
  ENDIF
GR.color 255,255,255,255,1
GR.TEXT.size 35  
GR.TEXT.draw tony, 225,50," Media Explorer v1.0 by [Antonis]"
GR.TEXT.size 60

! print dirs, files
npagesofall=ceil(length/15)
dpagelen=length-15
k=0
FOR j=1 TO npagesofall
  GR.hide tmore
  FOR l=1 TO 15
   GR.hide recd[l]
   GR.hide arcd[l]
   GR.hide txtd[l]
   GR.hide tback3
  NEXT l
  IF dpagelen>0 THEN npagelen=15 ELSE npagelen=dpagelen+15
  FOR i=1 TO npagelen 
    n = is_in("(d)", d1$[i+k])
    
    IF n<>0 THEN % dir name
     GR.color 255,255,215,0,1
     GR.rect recd[i], x, y+dy, x+60,y+40+dy   
     GR.arc arcd[i], x,y-10+dy,x+40,y+10+dy, 0,-180, 1
     GR.TEXT.draw txtd[i],x+70,y+40+dy,LEFT$(d1$[i+k],LEN(d1$[i+k])-3)
     dy=dy+70
     GR.render
    ELSE  % filenames
     GR.color 255,0,255,255,1
     GR.TEXT.size 45
     GR.TEXT.draw txtd[i],x,y+40+dy,d1$[i+k]
     GR.TEXT.size 60
     dy=dy+70
     GR.render
    ENDIF
  NEXT i
  
  ! IF more_dir_to_print THEN show <next> arrow
  IF dpagelen>0 THEN 
    GR.color 255,155,155,255,1
    LIST.create n,mypoly2
    LIST.add mypoly2,x+35,y+70*16-5,x+65,y+20+70*16,x+35,y+45+70*16
    GR.poly polyObj2,mypoly2
    GR.rect rec2,x,y+10+70*16,x+40,y+30+70*16
    GR.TEXT.draw tmore,x+70,y+40+70*16,"Next"
    GR.render
 ENDIF
  
 ! IF less_dir_to_print THEN show <previous> arrow
    IF k+i-1 > 15
     GR.color 255,255,140,0,1
     LIST.create n,mypoly3
     LIST.add mypoly3,x+700+30,y+20+70*16,x+30+700+30,y-5 +70*16 ,x+30+700+30,y+45 +70*16
     GR.poly polyObj3,mypoly3 
     GR.rect rec3,x+30+700+30,y+10 +70*16 ,x+60+700+30,y+30 +70*16
     GR.TEXT.draw tback3,380+x+70+30,y+40 +70*16 ,"Previous"
     GR.render
     !list.clear mypoly3
    ENDIF

 ! rects FOR  NEXT + previous
 GR.color 255,155,155,255,0

! finished current page wait FOR previous, next, up, folder, dir
touchme:
 DO
  GR.touch touched,x1,y1
  PAUSE 50
 UNTIL touched>0

! scan coordinates
dz=0

! rectangles for objs
 FOR l=1 TO 17
     IF y1>(y-15+dz)*sy & y1<(y+55+dz)*sy THEN 
          yfound=y1
          index=l  
        ENDIF
    dz=dz+70
 NEXT l

! <next>
 IF y1>1115*sy & x1<400*sy THEN 
        IF j=npagesofall THEN 
            POPUP "nothing to show",0,0,0
            GOTO touchme
        ELSE
           k=k+15
           dy=70  
           dpagelen=dpagelen-15           
        ENDIF
 ENDIF

! <previous>
IF y1>1115*sy & x1>400*sy THEN  
     IF j=1 THEN 
       POPUP "nothing to show",0,0,0
       GOTO touchme
     ELSE
       j=j-2
       dy=70
       k=k-15
      dpagelen=dpagelen+15       
     ENDIF
ENDIF

! current page is equal to j
! current selected element is equal to index-1
! ( j-1 )*15 + index-1
IF y1>70*sy & y1<1115*sy THEN 
 element= ( j-1 )* 15 + index-1
 IF element>length THEN
    POPUP "empty",0,0,0
    GOTO touchme
 ENDIF
 IF element<=ndirs THEN
  ! IF dir (& not blank) selected THEN change path to path+filename
  n = is_in("(d)", d3$[element])
  dname$ = LEFT$(d3$[element],n-1)
  path$=path$+dname$+"/"
  GOTO MainLoop
 ENDIF
 IF element>ndirs  THEN 
  ! IF dir (& not blank) selected THEN change path to path+filename
  fname$ = d4$[element-ndirs]
  IF fname$="" THEN 
           !POPUP "empty",0,0,0
  ELSE
    GOTO finished % function end
  ENDIF  
   GOTO touchme
 ENDIF
ENDIF

! IF up selected
! IF at start path THEN back up one level
IF y1<70*sy THEN 
 IF path$ = "../../../../"
  path$ = "../../../../"
  GOTO MainLoop
 ENDIF

! Not at start path split the path by the "/" chars
 ARRAY.delete p$[]
 split p$[], path$, "/"
 ARRAY.length length, p$[]

! IF the last entry is ".." THEN add "../"
! to back up one level
 IF p$[length] = ".."
  path$ = path$ + "../"
  GOTO MainLoop
  ENDIF

! Last entry is not ".." so must delete the
! last directory from the path
! IF only one entry THEN path is back to the base path
 IF length = 1
  path$ = "../../"
  GOTO MainLoop
  ENDIF

! Reconstruct path without the last directory
 path$ = ""
 FOR i = 1 TO length - 1
  path$ = path$ + p$[i] + "/"
 NEXT i
 GOTO MainLoop
ENDIF

nextloop:
NEXT j

GOTO MainLoop
 
finished:
a$=path$+fname$
POPUP  a$,0,100,0
FN.rtn a$
FN.end

! *******************************************
! text viewer IF width>screen THEN new line...
FN.def textview(filename$)
unDIM tx[]
DIM tx[100]

GR.screen w,h
scalex=w/800
scaley=h/1232
line=0
char=0
TEXT.open r, Ftable, filename$
DO
 TEXT.readln ftable, line$
 line=line+1
 char=char+LEN(line$)
UNTIL line$="EOF"
TEXT.close ftable

FILE.Size fsize,filename$
GR.TEXT.size 42
GR.color 255,255,0,0,1
GR.rect r0,0,0,799,160
GR.rect r1,0,1120,799,1232
GR.color 255,255,255,255,1
GR.TEXT.draw t1,100,33,"Size: " + LEFT$(STR$(fsize),LEN(STR$(fsize))-2)+ " bytes"
GR.TEXT.draw t2,100,73,"Chars: "+LEFT$(STR$(char-3),LEN(STR$(char-3))-2)
GR.TEXT.draw t3,100,113,"Lines: "+LEFT$(STR$(line-1),LEN(STR$(line-1))-2)
GR.TEXT.draw t4,100,153,"Name: ..."+RIGHT$(filename$,23)
GR.render
GR.color 255,0,0,255,1
j=1
k=0
line$=""
TEXT.open r, Ftable, filename$
while line$<>"EOF"
 TEXT.readln ftable, line$
 IF LEN(line$)>0 THEN
 a$=""
 found=1 
  j=1
 !GOSUB check_k
 FOR i=2 TO LEN(line$)
  a$=a$+MID$(line$,i-1,1)
  b$=a$+MID$(line$,i,1)
  GR.TEXT.width w1,a$
  GR.TEXT.width w2,b$
  IF w1<=800*j & w2>800*j THEN   
       
       j=j+1       
       toprint$=MID$(line$,found,i-found)
       k=k+1
       GR.TEXT.draw tx[k], 0,200+(k-1)*38,toprint$
       GR.render
       found=i
       GOSUB check_k 
  ENDIF
 NEXT i
 IF found=1 THEN 
   toprint$=line$
   k=k+1
   GR.TEXT.draw tx[k], 0,200+(k-1)*38,toprint$
   GR.render
   GOSUB check_k
 ELSE
      k=k+1
      toprint$=MID$(line$,found,LEN(line$)-found+1)
      GR.TEXT.draw tx[k], 0,200+(k-1)*38,toprint$
      GR.render 
      GOSUB check_k
 ENDIF
 GR.render
 ENDIF
repeat

POPUP "Touch screen to return",0,0,0
       DO 
         GR.touch touched,x,y 
       UNTIL touched>0
TEXT.close ftable
GOTO finish

! checks k
check_k:
 IF k=25 THEN 
  k=0
  POPUP "Touch lower part to quit, upper to continue:",0,0,0
  GR.color 255,255,255,255,1
  GR.hide tob
  GR.TEXT.draw tob,40,1200,"TAP HERE TO QUIT, ELSEWHERE TO GO"
  GR.render
lastCLOCK=CLOCK()
off=1

DO
 PAUSE 50
 GR.touch touched,x,y
 IF CLOCK()-lastCLOCK>500 THEN 
     off=!off
     lastCLOCK=CLOCK()
   IF off>0 THEN
     GR.hide tob
   ELSE 
    GR.show tob
 ENDIF
 GR.render
ENDIF
UNTIL touched>0

GR.color 255,0,0,255,1
       IF y>1120*scaley THEN 
        TEXT.close ftable
        FOR l=1 TO 25
          GR.hide tx[l]
        NEXT l
        GR.hide t1
        GR.hide t2
        GR.hide t3
        GR.hide t4
        GR.hide lin
        GR.hide r0
        GR.hide r1
         GOTO finish
       ENDIF

    FOR l=1 TO 25
      GR.hide tx[l]
    NEXT l
  ENDIF
return
finish:
k=1
FN.rtn k
FN.end

! **************
! image viewer
! begin graphics routine
FN.def gview(myfile$)
GR.cls
gv=0
GR.screen ww,hh
scalex=ww/1232
scaley=hh/800
GR.TEXT.size 40
GR.color 255,255,0,0,1
GR.rect ob0, 0,200,799,1000
GR.color 255,255,255,255,1
GR.TEXT.draw ob1,230,50,"Graphics Viewer"
GR.bitmap.load bmp0,myfile$
GR.bitmap.size bmp0,ww,hh
GR.color 255,0,50,255,1
FILE.Size s,myfile$
GR.TEXT.draw ob,20,100,"Size: "+left$(STR$(s),len(str$(s))-2)+" bytes"
GR.TEXT.draw ob2,20,140,"Original resolution: " + LEFT$(STR$(ww),LEN(STR$(ww))-2) + "x"+ LEFT$(STR$(hh),LEN(STR$(hh))-2)
GR.TEXT.draw ob3,20,180,"File: "+myfile$
currentscale=1
IF ww>=800*scalex | hh>=800*scaley THEN % image >800x800
  IF ww>=hh THEN currentscale=800/ww ELSE currentscale=800/hh
ENDIF
GR.bitmap.scale bmp1, bmp0, currentscale*ww, currentscale*hh

! image DIM are now x->currentscale*ww y->currentscale*hh
! center image
xpos=(800-currentscale*ww)/2
ypos=200+(800-currentscale*hh)/2
GR.rect frec,xpos-1,ypos-1,xpos+1+currentscale*ww,ypos+1+ currentscale*hh
GR.bitmap.draw bmp2,bmp1,xpos,ypos
GR.bitmap.delete bmp0
GR.render
GR.color 255,255,255,0,1
GR.TEXT.size 60
! wait to return
GR.TEXT.draw ob0,25,1050,"TOUCH SCREEN TO RETURN"
GR.render
lastCLOCK=CLOCK()
off=1
DO
 PAUSE 50
 GR.touch touched,x,y
 IF CLOCK()-lastCLOCK>500 THEN 
     off=!off
     lastCLOCK=CLOCK()
   IF off>0 THEN
     GR.hide ob0 
   ELSE 
    GR.show ob0
 ENDIF
 GR.render
ENDIF
UNTIL touched>0

gv=1
FN.rtn gv
FN.end
! ********************

!-------{main}-------
GR.open 255,0,0,0
PAUSE 1000
GR.orientation 1 
GR.color 255,255,255,0,1
GR.TEXT.size 60
GR.screen w,h
sx=w/800
sy=h/1232
GR.scale sx,sy

! start from "sdcard/"
pp$="../../"
start:
GR.cls

! get a file
a$=getfile$(pp$)

! find the type of file
a=ftype(a$)

IF  a=0 THEN % unsupported
  b=ok_cancel (100,100,"Unsupported type.   open it as text?","YES","NO")
  GR.cls
  IF b=1 THEN k=textview(a$)
ENDIF
GR.cls

IF a=1 THEN % image
      POPUP "image file",0,100,0
      k=gview(a$)
ENDIF
GR.cls

IF a=2 THEN % audio
      POPUP "audio file",0,100,0
      k=mplay(a$)
ENDIF
GR.cls

IF a=3 THEN % video
    b=ok_cancel (100,100,"Video file detected, open it as text?","YES","NO")
    GR.cls
    IF b=1 THEN k=textview(a$)
ENDIF	
GR.cls

IF  a=4 THEN  % text
       POPUP "text file",0,100,0
	   k=textview(a$)
ENDIF

! find current path from full filename
! pass it to f$ & loop
f=0
FOR i=1 TO LEN(a$)
     aa$=MID$(a$,i,1)
     IF aa$="/" THEN f=i
NEXT i
pp$=LEFT$(a$,f)
GR.cls

GOTO start

end

