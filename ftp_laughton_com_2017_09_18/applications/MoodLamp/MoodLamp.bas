! MoodLamp
! Aat Don @2014
FN.DEF SetNewColor(Lamp1,Lamp2,TransP,NwCol,NxtCol)
	GR.COLOR 255-TransP,VAL(MID$(RIGHT$("000"+BIN$(NwCol),3),1,1))*255,VAL(MID$(RIGHT$("000"+BIN$(NwCol),3),2,1))*255,VAL(MID$(RIGHT$("000"+BIN$(NwCol),3),3,1))*255,1
	GR.PAINT.GET NewColor
	GR.MODIFY Lamp1,"paint",NewColor
	GR.COLOR TransP,VAL(MID$(RIGHT$("000"+BIN$(NxtCol),3),1,1))*255,VAL(MID$(RIGHT$("000"+BIN$(NxtCol),3),2,1))*255,VAL(MID$(RIGHT$("000"+BIN$(NxtCol),3),3,1))*255,1
	GR.PAINT.GET NewColor
	GR.MODIFY Lamp2,"paint",NewColor
	GR.RENDER
FN.END
GR.OPEN 255,0,0,0,0,-1
PAUSE 1000
POPUP "Hit screen to EXIT the moodlamp....",0,0,1
GR.SCREEN w,h
WAKELOCK 3
! One cycle is approx. 8 minutes
Delay=300
GR.RECT Lamp1,0,0,w,h
GR.RECT Lamp2,0,0,w,h
DO
	t=CLOCK()+Delay
	FOR i=0 TO 255
		CALL SetNewColor(Lamp1,Lamp2,i,6,1)
		GOSUB Delay_
		IF Touched THEN F_N.BREAK
	NEXT i
	Mood=1
	DO
		t=CLOCK()+Delay
		FOR i=0 TO 255
			CALL SetNewColor(Lamp1,Lamp2,i,Mood,Mood+1)
			GOSUB Delay_
			IF Touched THEN F_N.BREAK
		NEXT i
		Mood=Mood+1
	UNTIL Mood=6 | touched
UNTIL touched
WAKELOCK 5
GR.CLOSE
EXIT
Delay_:
	DO
		GR.TOUCH touched,x,y
	UNTIL CLOCK()>t | touched
	t=CLOCK()+Delay
RETURN
