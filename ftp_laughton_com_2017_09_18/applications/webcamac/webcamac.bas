REM Start of BASIC! Program
!   Simple suvreillance camera app
!   madmac@email.de

! Interval in minutes
interval=10

!Disable sleep mode
wakelock 2

webcam:
!---------- Take photo
gr.open 255,0,0,0
!---------- If you want to change settings for autoshoot see parameters command gr.camera.
gr.camera.autoShoot photoid,2
 !--------- Pause for interval seconds (set above))
pause 60000*interval
gr.close

!---------- Rename file to YYYYMMDDHHMMSS.jpg
Time year$, month$, day$, hrs$, min$, sec$
datefile$ = year$ + month$ + day$ + hrs$ + min$ + sec$ + ".jpg"
file.rename "image.jpg", datefile$

!---------- Remove ! in next line to get a beep whenever a photo is taken
!tone 15000,100

gosub ftp
gosub deletelocal
goto webcam
 
!---------- Upload photo to ftp-server
ftp:
!---------- Remove ! in next two lines and enter data for your ftp-server after ftp.open.
!ftp.open "ftp.yourserver.com",21,"username","password"
!ftp.put datefile$,datefile$
ftp.close
return

!---------- Delete local file after upload to ftp-server
deletelocal:
!---------- If you want delete files on smartphone remove ! at the beginning of next line.
!file.delete ok,datefile$
return
