!!


by christopher Ogden 2015

love my family yes i do.

~````~`~~~`~@

! -----------= FONT SCALING NOTES =--------- !
 
1000x2000-800x480-1280x764

100-40
smart 60-30
clock & save 70-30
100-40





!!
gr.open 0,0,0,0,0
gr.screen w,h
print w,h
gr.render
!------= Default Values =------!

let o_c$ = "0" % Other Passes Count
let m_c$ = "0" % Monthly Passes Count
let t_c$ = "0" % Ticket Day Count
let r_c$ = "0" % Regular Recipt Count
let s_c$ = "0" % Smart Car Count
let tc_c$ = "0"% Total Car Count
let t_c1$ = "0" % Ticket Per site

!-= Font =-!

let fl = 100  % font large
let fs = 40   % font small
let sfl = 60  % smart and clock text large
let sfs = 30  %   "         "        small


!------= Measurements =-------!

let gw = w/5
let gh = h/8
let tb = gh % Top Buffer
let bb = gh*7 %bottom Buffer
let lb = gw
let rb = gw*4
let mbw = gw/4      % Minus button width
let mbh = gh/3      %              height
let sgw = w/9       % Side Grid width
let sgh = h/9       % Side Grid Height

!-------= Draw User Interface =-----------!

!-------= Time =--------------------------!

TIME Year$, Month$, Day$, Hour$, Minute$, Second$, WeekDay, isDST
hour = val(hour$)

if hour < 12 then tyme$ = hour$ + ":" + minute$ + " am"

if hour > 12 then 

hour = hour - 12 
hour$ = str$(hour)
hour$ = replace$(hour$,".","d")
split hour$[],hour$,"d"
hour$ = hour$[1]
tyme$ = hour$ + ":" + minute$ + " pm"
undim hour$[]
endif

if hour = 12 then tyme$ = hour$ + ":" + minute$ + " pm"


gr.text.size 30
Gr.Color 255, 255, 255, 255

gr.text.draw tyme,mbw/3,mbh*2,tyme$


!--------= Monthly Button =---------------!

Gr.Color 255, 255, 37, 0
gr.rect mnth, lb,tb,gw*2,gh*3

  % Monthly Count Minus Button
  
  Gr.Color 255, 255, 255, 255
gr.rect mm, lb-mbw,tb,lb,mbh*2+tb

  % Minus "-"
  
Gr.Color 255, 0, 0, 0
gr.text.size 40
gr.text.draw mmc, lb-mbw+25,tb+mbh+35,"-"
  
  % Monthly Count Text
  
Gr.Color 255, 255, 255, 255
gr.text.draw mc, lb+gw/2,gh*2,m_c$
gr.text.draw mt, lb,tb-mbh*0.5,"Monthly"
	
!---------= Other Passes Button =----------!

Gr.Color 200, 0, 255, 0
gr.rect othe, gw*3,gh,gw*4,gh*3
Gr.Color 255, 255, 255, 255
gr.text.draw ot, gw*2.8,gh-mbh/2,"Site Specific"

  % Count 

Gr.Color 255, 255, 255, 255
gr.text.draw oc, gw*3+gw/2,gh*2,o_c$  
   

   % Other Minus Button
   
Gr.Color 255, 255, 255, 255
gr.rect om,rb,tb,rb+mbw,tb+mbh*2

!-------= Tickets Button =---------------!

Gr.Color 255, 0, 41, 255
gr.rect tix,lb,gh*5,gw*2,gh*7
Gr.Color 255, 255, 255, 255
gr.text.draw tt,lb*1.07,gh*5-mbh/3,"Tickets"

   % Count Minus Button
Gr.Color 255, 255, 255, 255
gr.rect tm, lb-mbw,gh*5+mbh*4,lb,gh*7

   % Count Text
Gr.Color 255, 255, 255, 255
gr.text.draw tc, lb+gw/2,gh*6,t_c$

!--------= Regular Recept Button =----------!

Gr.Color 255, 168, 0, 255
gr.rect rr,gw*3,gh*5,gw*4,gh*7

   %  Regular Recept Text

Gr.Color 255, 255, 255, 255
gr.text.draw rc, gw*3+gw/2,gh*6,r_c$
Gr.Color 255, 255, 255, 255
gr.text.draw rt, gw*3.07,gh*4.82, "Regular"
   
   % Regular Minus Button

Gr.Color 255, 255, 255, 255
gr.rect rm,rb,bb-mbh*2,rb+mbw,bb

!-----= Smart Count =--------------------!

Gr.Color 255, 0, 255, 218
gr.rect smart, sgw*8,sgh*8,sgw*9,sgh*9
Gr.Color 255, 255, 255, 255
gr.text.size 30
gr.text.draw smrt, sgw*8+23,sgh*8-10,"Smart"
gr.text.size 40
Gr.Color 255, 0, 0, 0
gr.text.draw sc,sgw*8+mbw,sgh*8+mbw,s_c$

!-----= Count Total =--------------------!

Gr.Color 255, 255, 255, 255
gr.rect tot, gw*2,gh*3,gw*3,gh*5

   % Count Text

Gr.Color 255, 0,0,0
gr.text.draw tcc, gw*2+gw/2,gh*4,tc_c$
Gr.Color 255, 255, 255, 255
gr.text.draw ct,gw*2.15,gh*2.82,"Total"

!------= Save Button =-------------------!

Gr.Color 255, 255, 0, 230
gr.rect save, sgw*8,0,sgw*9,sgh
Gr.Color 255, 255, 255, 255
gr.text.size 30
gr.text.draw st, sgw*8.15,sgh*0.7,"Save"
!-----= Render =-----------!
gr.render
!-------= View Counts Dot =-----------!
Gr.Color 255, 255, 255, 255
gr.circle dot, gw/6,gh*7.6, 20

gr.render
!---------= Touch Censors =--------------!
DO 
DO

GR.TOUCH FLAG,X,Y

gr.bounded.touch DOT,0,gh*7,gw/3,h
GR.bounded.TOUCH SAVE, sgw*8,0,sgw*9,sgh
gr.bounded.touch SMART, sgw*8,sgh*8,sgw*9,sgh*9
gr.bounded.touch MTH,lb,tb,gw*2,gh*3
GR.BOUNDED.TOUCH OTH,gw*3,gh,gw*4,gh*3
GR.BOUNDED.TOUCH TIX,lb,gh*5,gw*2,gh*7
GR.BOUNDED.TOUCH REG,gw*3,gh*5,gw*4,gh*7

GR.BOUNDED.TOUCH MTH_M,lb-mbw,tb,lb,mbh*2+tb
GR.BOUNDED.TOUCH OTH_M,rb,tb,rb+mbw,tb+mbh*2
GR.BOUNDED.TOUCH TIX_M,lb-mbw,gh*5+mbh*4,lb,gh*7
GR.BOUNDED.TOUCH REG_M,rb,bb-mbh*2,rb+mbw,bb


UNTIL FLAG
!-------= Update Time =------------!
TIME Year$, Month$, Day$, Hour$, Minute$, Second$, WeekDay, isDST
hour = val(hour$)

date$ = month$ + "/" + day$ + "/" + year$

if hour < 12 then tyme$ = hour$ + ":" + minute$ + " am"
if hour > 12 then 
hour = hour - 12 
hour$ = str$(hour)
hour$ = replace$(hour$,".","d")
split hour$[],hour$,"d"
hour$ = hour$[1]
tyme$ = hour$ + ":" + minute$ + " pm"
endif
if hour = 12 then tyme$ = hour$ + ":" + minute$ + " pm"
gr.modify tyme, "text", tyme$
undim hour$[]


gr.render

!------= Let go touch pause =------!

do
gr.touch flag, x,y
until !flag

!------= if touched ____ button then =-----!

IF MTH THEN

m_c = m_c + 1
m_c$ = str$(m_c)
m_c$ = REPLACE$(m_c$,".","d")
split m_c$[],m_c$,"d"
let m_c$ = m_c$[1]
gr.modify mc,"text",m_c$[1]
undim m_c$[]
gr.render

ENDIF

IF OTH THEN

o_c = o_c + 1
o_c$ = str$(o_c)
o_c$ = REPLACE$(o_c$,".","d")
split o_c$[],o_c$,"d"
let o_c$ = o_c$[1]
gr.modify oc,"text",o_c$[1]
undim o_c$[]
gr.render

ENDIF

IF TIX THEN

t_c = t_c + 1
t_c1 = t_c1 + 1
t_c$ = str$(t_c)
t_c$ = REPLACE$(t_c$,".","d")
split t_c$[],t_c$,"d"
let t_c$ = t_c$[1]
gr.modify tc,"text",t_c$[1]
undim t_c$[]
gr.render

ENDIF

IF REG THEN

r_c = r_c + 1
r_c$ = str$(r_c)
r_c$ = REPLACE$(r_c$,".","d")
split r_c$[],r_c$,"d"
let r_c$ = r_c$[1]
gr.modify rc,"text",r_c$[1]
undim r_c$[]
gr.render

ENDIF

IF MTH_M THEN

m_c = m_c - 1
m_c$ = str$(m_c)
m_c$ = REPLACE$(m_c$,".","d")
split m_c$[],m_c$,"d"
let m_c$ = m_c$[1]
gr.modify mc,"text",m_c$[1]
undim m_c$[]
gr.render

ENDIF

IF OTH_M THEN

o_c = o_c - 1
o_c$ = str$(o_c)
o_c$ = REPLACE$(o_c$,".","d")
split o_c$[],o_c$,"d"
let o_c$ = o_c$[1]
gr.modify oc,"text",o_c$[1]
undim o_c$[]
gr.render

ENDIF

IF TIX_M THEN

t_c = t_c - 1
t_c1 = t_c1 - 1
t_c$ = str$(t_c)
t_c$ = REPLACE$(t_c$,".","d")
split t_c$[],t_c$,"d"
let t_c$ = t_c$[1]
gr.modify tc,"text",t_c$[1]
undim t_c$[]
gr.render

ENDIF

IF REG_M THEN

r_c = r_c - 1
r_c$ = str$(r_c)
r_c$ = REPLACE$(R_c$,".","d")
split r_c$[],r_c$,"d"
let r_c$ = r_c$[1]
gr.modify rc,"text",r_c$[1]
gr.render
undim r_c$[]

ENDIF
if SMART THEN

s_c = s_c + 1
s_c$ = str$(s_c)
s_c$ = REPLACE$(s_c$,".","d")
split s_c$[],s_c$,"d"
let s_c$ = s_c$[1]
gr.modify sc,"text",s_c$[1]
gr.render
undim s_c$[]

endif

if DOT then 

let path$ = "../../Documents/"+ month$+"-"+day$+"-"+year$+".txt"

file.exists ex, path$

if ex then

grabfile file$,path$ % grabfile from path$ and place it in file$
text.input new$,file$% open text editing window and load file$ in for editing
cls
print new$ % print edited text to console screen
console.line.count clc % get number of lines on the console
dim c_line$[clc]
text.open w,file,path$

ddd$ = "---------------------------------------------"

for line = 1 to clc

  console.line.text line, c_line$[line]
  text.writeln file, c_line$[line] 
  
next

text.close file
undim c_line$[]

!!
text.open w,file,path$
text.writeln file, file$
text.close file
!!
else 


print new$ % print edited text to console screen
console.line.count clc % get number of lines on the console
dim c_line$[clc]
text.open w, file, path$
text.writeln file, date$

for line = 1 to clc

  console.line.text line, c_line$[line]
  text.writeln file, c_line$[line]
  
next

text.close file
undim c_line$[]

endif


  endif
  
if SAVE THEN

input "Lot:",lot$,"",iscanceled
input "Milage:",mile$,l_mile$,iscanceled
!let miles = val(mile$)
let l_mile$ = left$(mile$,4)

if !iscanceled then

TIME Year$, Month$, Day$, Hour$, Minute$, Second$, WeekDay, isDST

t_c1$ = str$(t_c1) 
t_c1$ = replace$(t_c1$,".","d")
split t_c1$[],t_c1$,"d"
t_c1$ = t_c1$[1]
undim t_c1$[]

let Entry$ = " " + lot$ + "-" + Hour$ + ":" + minute$ + "-" + tc_c$+ " Total Cars" + "-" + m_c$ +" Monthly" + "-" + o_c$ + " Site Specific" + "-" + t_c1$ + " Ticket(s)" + "-" + s_c$ + " Smart Car(s)" + " Odometer: " + mile$

print entry$

let path$ = "../../Documents/"+month$+"-"+day$+"-"+year$+".txt"

file.exists ex, path$



text.open a, file, path$
if !ex then text.writeln file, date$
text.writeln file, entry$
text.close file
popup entry$
popup "Entry Saved"
let o_c = 0
let m_c = 0
let r_c = 0
let t_c1 = 0
let tc_c = 0
let s_c = 0
let o_c$ = "0"
let m_c$ = "0"
let r_c$ = "0"
let tc_c$ = "0"
let s_c$ = "0"
gr.modify sc, "text",s_c$
gr.modify rc, "text",r_c$
gr.modify mc, "text",m_c$
gr.modify oc, "text",o_c$
endif
endif
!--------= Update Total Car Count =-------!

let tc_c = m_c + o_c + r_c + t_c1 + s_c
let tc_c$ = str$(tc_c)
tc_c$ = REPLACE$(tc_c$,".","d")
split tc_c$[],tc_c$,"d"
let tc_c$ = tc_c$[1]
gr.modify tcc,"text",tc_c$[1]
undim tc_c$[]
gr.render

until 0







onbackkey:
input "Quit without Saving?",quit$,"Y",ISCANCELED
IF ISCANCELED then back.resume
if Quit$ = "Y" then end
back.resume




















