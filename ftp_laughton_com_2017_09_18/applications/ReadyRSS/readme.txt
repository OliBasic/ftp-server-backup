ReadyRSS RSS feed reader.

Features:
* grid-select widget using cell drag/drop.
* progress bar widget
* themes

Be sure to delete the /readyrss directory on your sdcard if upgrading.

Latest versions can be found at
http://humpty.drivehq.com/promotes/rfo-basic/rfo.html#readyrss
