REM MergeSort
! Sorts a list of bundles based on the value 
! contained in a specific key. Will sort on numeric 
! or string values.
!Syntax:
!
!  call mergesort(lstPtr, key$, desc)
!
!where,
!  lstPtr is pointer to the list to be sorted
!  key$ is the name of the key on which to sort
!  desc: 1 for descending, 0 for ascending sort
!
!return value of mergesort is always 0.
!

FN.DEF mShuffleAS(lstPtr,key$,i1,i2,j2)
 j1 = i2+1
 i=i1
 list.get lstPtr,i,b1
 bundle.get b1,key$,val1$
 for j=j1 to j2
  list.get lstPtr,j,b2
  bundle.get b2,key$,val2$
  while !(val2$<val1$) & i<i2
   i=i+1
   list.get lstPtr,i,b1
   bundle.get b1,key$,val1$
  repeat
  if val2$<val1$
   list.remove lstPtr,j
   list.insert lstPtr,i,b2
   i=i+1
   i2=i2+1
  endif 
 next j
FN.END

FN.DEF pMergeSortAS(lstPtr,key$,i1,j2)
 IF i1=j2
  FN.RTN 0
 ELSE if i1=(j2-1)
  LIST.GET lstPtr,i1,b1
  BUNDLE.GET b1,key$,val1$
  LIST.GET lstPtr,j2,b2
  BUNDLE.GET b2,key$,val2$
  IF val2$<val1$
   LIST.REMOVE lstPtr,j2
   LIST.INSERT lstPtr,i1,b2
   FN.RTN 0
  ENDIF
 ELSE
  i2 = FLOOR((i1+j2)/2)
  j1 = i2 + 1
  CALL pMergeSortAS(lstPtr,key$,i1,i2)
  CALL pMergeSortAS(lstPtr,key$,j1,j2)
  CALL mShuffleAS(lstPtr,key$,i1,i2,j2)
  FN.RTN 0
 ENDIF
FN.END

FN.DEF mShuffleAN(lstPtr,key$,i1,i2,j2)
 j1 = i2+1
 i=i1
 list.get lstPtr,i,b1
 bundle.get b1,key$,val1
 for j=j1 to j2
  list.get lstPtr,j,b2
  bundle.get b2,key$,val2
  while !(val2<val1) & i<i2
   i=i+1
   list.get lstPtr,i,b1
   bundle.get b1,key$,val1
  repeat
  if val2<val1
   list.remove lstPtr,j
   list.insert lstPtr,i,b2
   i=i+1
   i2=i2+1
  endif 
 next j
FN.END

FN.DEF pMergeSortAN(lstPtr,key$,i1,j2)
 IF i1=j2
  FN.RTN 0
 ELSE if i1=(j2-1)
  LIST.GET lstPtr,i1,b1
  BUNDLE.GET b1,key$,val1
  LIST.GET lstPtr,j2,b2
  BUNDLE.GET b2,key$,val2
  IF val2<val1
   LIST.REMOVE lstPtr,j2
   LIST.INSERT lstPtr,i1,b2
   FN.RTN 0
  ENDIF
 ELSE
  i2 = FLOOR((i1+j2)/2)
  j1 = i2 + 1
  CALL pMergeSortAN(lstPtr,key$,i1,i2)
  CALL pMergeSortAN(lstPtr,key$,j1,j2)
  CALL mShuffleAN(lstPtr,key$,i1,i2,j2)
  FN.RTN 0
 ENDIF
FN.END


FN.DEF mShuffleDS(lstPtr,key$,i1,i2,j2)
 j1 = i2+1
 i=i1
 list.get lstPtr,i,b1
 bundle.get b1,key$,val1$
 for j=j1 to j2
  list.get lstPtr,j,b2
  bundle.get b2,key$,val2$
  while !(val2$>val1$) & i<i2
   i=i+1
   list.get lstPtr,i,b1
   bundle.get b1,key$,val1$
  repeat
  if val2$>val1$
   list.remove lstPtr,j
   list.insert lstPtr,i,b2
   i=i+1
   i2=i2+1
  endif 
 next j
FN.END

FN.DEF pMergeSortDS(lstPtr,key$,i1,j2)
 IF i1=j2
  FN.RTN 0
 ELSE if i1=(j2-1)
  LIST.GET lstPtr,i1,b1
  BUNDLE.GET b1,key$,val1$
  LIST.GET lstPtr,j2,b2
  BUNDLE.GET b2,key$,val2$
  IF val2$>val1$
   LIST.REMOVE lstPtr,j2
   LIST.INSERT lstPtr,i1,b2
   FN.RTN 0
  ENDIF
 ELSE
  i2 = FLOOR((i1+j2)/2)
  j1 = i2 + 1
  CALL pMergeSortDS(lstPtr,key$,i1,i2)
  CALL pMergeSortDS(lstPtr,key$,j1,j2)
  CALL mShuffleDS(lstPtr,key$,i1,i2,j2)
  FN.RTN 0
 ENDIF
FN.END

FN.DEF mShuffleDN(lstPtr,key$,i1,i2,j2)
 j1 = i2+1
 i=i1
 list.get lstPtr,i,b1
 bundle.get b1,key$,val1
 for j=j1 to j2
  list.get lstPtr,j,b2
  bundle.get b2,key$,val2
  while !(val2>val1) & i<i2
   i=i+1
   list.get lstPtr,i,b1
   bundle.get b1,key$,val1
  repeat
  if val2>val1
   list.remove lstPtr,j
   list.insert lstPtr,i,b2
   i=i+1
   i2=i2+1
  endif 
 next j
FN.END

FN.DEF pMergeSortDN(lstPtr,key$,i1,j2)
 IF i1=j2
  FN.RTN 0
 ELSE if i1=(j2-1)
  LIST.GET lstPtr,i1,b1
  BUNDLE.GET b1,key$,val1
  LIST.GET lstPtr,j2,b2
  BUNDLE.GET b2,key$,val2
  IF val2>val1
   LIST.REMOVE lstPtr,j2
   LIST.INSERT lstPtr,i1,b2
   FN.RTN 0
  ENDIF
 ELSE
  i2 = FLOOR((i1+j2)/2)
  j1 = i2 + 1
  CALL pMergeSortDN(lstPtr,key$,i1,i2)
  CALL pMergeSortDN(lstPtr,key$,j1,j2)
  CALL mShuffleDN(lstPtr,key$,i1,i2,j2)
  FN.RTN 0
 ENDIF
FN.END


FN.DEF MergeSort(lstPtr,key$,desc)
 LIST.SIZE lstPtr, n
 IF n=0 THEN fn.rtn 0
 LIST.GET lstPtr,1,b
 BUNDLE.TYPE b,key$,t$
 if desc
  if t$="S"
   call pMergeSortDS(lstPtr,key$,1,n)
  else
   call pMergeSortDN(lstPtr,key$,1,n)
  endif 
 else
  if t$="S"
   call pMergeSortAS(lstPtr,key$,1,n)
  else
   call pMergeSortAN(lstPtr,key$,1,n)
  endif
 endif 
 FN.RTN 0
FN.END

