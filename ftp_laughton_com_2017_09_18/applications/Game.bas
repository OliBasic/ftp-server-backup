speed = 1
gr.open 255, 0, 0, 0,1
gr.color 225, 0,0,255,1
gr.circle ball, round(rnd())*300+10,20,10
gr.color 255, 255, 0, 0, 1
gr.rect rct1, 205, 270, 275,250
gr.render
start:
do
gr.touch touched, x,y
until touched
gr.get.position rct1,x1,y1
gr.get.position ball, x2,y2
if x < (x1+10)
let d = -15
elseif x > (x1+40)
let d = 15
else
let d = 0
endif
gr.modify ball, "y", y2+round(rnd()*speed)*5
gr.modify rct1, "left", (x1+d+10)
gr.modify rct1, "right", (x1+d+75)
gr.render
if y2 > 250
gr.modify ball, "y",20*rnd()
gr.modify ball, "x", round(rnd()*speed)*100
endif
if gr_collision(rct1,ball)
speed = speed+2
goto start
endif 
goto start
