! Grocery list generator
!

BUNDLE.CREATE global
TTS.INIT
! graphics mode
BUNDLE.PUT global, "mode", "console"
! back key hit
BUNDLE.PUT global, "back", "0"
! background state
BUNDLE.PUT global, "bkg","0"
! Grocery starting database
Db$="grocdb.txt"
! User working database
Work$="working.txt"
! Pretty printed output

!  recipient
Em$="noreply@email.com"

FN.DEF dinit()
 !dinit:
 LET s$="<html><body style=\"background-color:black\"></body></html>"
 HTML.CLOSE
 HTML.OPEN 0
 HTML.LOAD.STRING s$
 BUNDLE.PUT 1,"mode","html"
FN.END

FN.DEF dmode(m$)
 BUNDLE.GET 1,"mode",oldm$
 IF oldm$=m$ THEN FN.RTN 0
 BUNDLE.PUT 1,"mode",m$
 IF oldm$="html" THEN HTML.CLOSE

 IF oldm$="gr" THEN GR.CLOSE

 IF m$="html" THEN HTML.OPEN 0

 IF m$="gr" THEN GR.OPEN 255,0,0,0,0

 IF m$="console" THEN CLS

FN.END

FN.DEF cask(m$)
 CALL dmode("console")
 DIALOG.MESSAGE,m$,c,"yes","no"
 IF c=1 THEN FN.RTN 1
 FN.RTN 0
FN.END

FN.DEF writeln(f$,msg$)
 TEXT.OPEN w,h,f$:TEXT.WRITELN h,msg$:TEXT.CLOSE h
FN.END

FN.DEF readgroc(fn$,b)
 ! read 2 field delimited file fn$ into string lists l1, l2
 LIST.CREATE s,l1
 LIST.CREATE s,l2
 Delim$=CHR$(9)

 TEXT.OPEN R, FN2,fn$

 DO
  TEXT.READLN FN2, a$
  s1$=WORD$(a$,1,Delim$)
  s2$=WORD$(a$,2,Delim$)
  IF a$<>"EOF" & s1$<>"" & S2$<>""
   DO
    Ws= ENDS_WITH(" ", s1$)
    IF ws>1
     S1$=MID$(s1$,1,ws-1)
    ENDIF
   UNTIL ws=0
   DO
    Ws= ENDS_WITH(" ", s2$)
    IF ws>1
     S2$=MID$(s2$,1,ws-1)
    ENDIF
   UNTIL ws=0

   LIST.ADD l1,s1$
   LIST.ADD l2,s2$
  ENDIF
 UNTIL a$="EOF"
 TEXT.CLOSE fn2
 BUNDLE.PUT b,"depts",l1
 BUNDLE.PUT b,"items",l2

FN.END

FN.DEF writegroc(f$,b)
 ! write contents of ordered string lists l1, l2 to file
 BUNDLE.GET b,"depts",l1
 BUNDLE.GET b,"items",l2
 Delim$=CHR$(9)
 TEXT.OPEN W, FN2,f$
 LIST.SIZE l1, n1
 LIST.SIZE l2, n2
 n = MIN(n1,n2)
 FOR i = 1 TO n
  LIST.GET l1,i,s1$
  LIST.GET l2,i,s2$
  a$ = s1$ + delim$ + s2$
  TEXT.WRITELN fn2, a$
 NEXT i
 TEXT.CLOSE fn2
FN.END

FN.DEF prettyprint$(b)
 ! Pretty Print grocery list
 BUNDLE.GET b,"depts",l2
 BUNDLE.GET b,"items",l1
 dl$="\n"
 LIST.SIZE l1, n
 Od$=""
 FOR i = 1 TO n
  LIST.GET l2,i,d$
  IF d$<>od$
   a$+=d$+dl$
   Od$=d$
  ENDIF
  LIST.GET l1,i,s1$
  a$+="___"+s1$+dl$
 NEXT i
 fn.rtn trim$(a$)
FN.END

FN.DEF toggle(s$)
 IF IS_IN("*",s$)
  S$=REPLACE$(s$,"*","")
 ELSE
  s$+="***"
 ENDIF
FN.END

FN.DEF off$(s$)
 FN.RTN REPLACE$( s$,"*","")
FN.END

FN.DEF ispicked(s$)
 IF IS_IN("*",s$)
  FN.RTN 1
 ELSE
  FN.RTN 0
 ENDIF

FN.END

FN.DEF editor(f$)
 dmode("console")
 myfile$ = f$
 GRABFILE unedited$, myfile$
 TEXT.INPUT edited$, unedited$,"edit"
 TEXT.OPEN w, filename, myfile$
 TEXT.WRITELN filename, edited$ 
 TEXT.CLOSE filename
FN.END

FN.DEF editdb(f$)
 dmode("console")
 myfile$ = f$
 GRABFILE unedited$, myfile$
 unedited$=REPLACE$(unedited$,CHR$(9),":")
 TEXT.INPUT edited$, unedited$,"master list"
 EDITED$= REPLACE$(edited$,":",CHR$(9))
 TEXT.OPEN w, filename, myfile$
 TEXT.WRITELN filename, edited$ 
 TEXT.CLOSE filename
FN.END

FN.DEF deldups(l)
 !remove duplicates from list l
 I=1
 LIST.SIZE  l, n
 DO
  LIST.GET l, I,  s$

  DO
   LIST.SEARCH l, s$, found, i+1
   IF found>0
    LIST.REMOVE l, found
   ENDIF
  UNTIL found=0
  LIST.SIZE  l, n

  I++
 UNTIL i>n

FN.END

FN.DEF clearlist(b)
bundle.get b,"items",l
 LIST.SIZE l, n
 FOR i=1 TO n
  LIST.GET l, I, i$
  LIST.REPLACE l, I,off$(i$)
 NEXT i
FN.END

FN.DEF mark(b, d$, i$)
 BUNDLE.GET b,"depts",dl
 BUNDLE.GET b,"items",il
 LIST.SEARCH dl, d$, d
 LIST.SEARCH IL, i$, I, d
 IF i>0
  LIST.GET dl, I,dept$
  LIST.GET IL, I,item$
 ENDIF
 IF Dept$=D$ & Item$ =i$ & i>0
  toggle(&i$)
  LIST.REPLACE IL, i, i$
 ENDIF
 BUNDLE.PUT b,"depts",dl
 BUNDLE.PUT b,"items",il
FN.END

FN.DEF update(b, d$, i$, n$)
 BUNDLE.GET b,"depts",dl
 BUNDLE.GET b,"items",il
 LIST.SEARCH dl, d$, d
 LIST.SEARCH IL, i$, I, d

 IF i>0
  LIST.GET dl,I,dept$
  LIST.GET IL,I,item$
  LIST.REPLACE IL, I, n$
 ENDIF

 BUNDLE.PUT b,"depts",dl
 BUNDLE.PUT b,"items",il

FN.END

FN.DEF Insert(b, d$, i$)
 BUNDLE.GET b,"depts",deptlist
 BUNDLE.GET b,"items",itemlist

 LIST.SEARCH deptlist, d$, i
 IF i>0
  LIST.INSERT deptlist, I, d$
  LIST.INSERT itemlist, I, i$
 ENDIF
 BUNDLE.PUT b,"depts",deptlist
 BUNDLE.PUT b,"items",itemlist
FN.END

FN.DEF delItem(b, d$, i$)

 BUNDLE.GET b,"depts",deptlist
 BUNDLE.GET b,"items",itemlist
 LIST.SEARCH deptlist, d$, i
 LIST.SEARCH itemlist,i$, j, i
 IF i>0 & j>0
  LIST.REMOVE deptlist, j
  LIST.REMOVE itemlist, j
 ENDIF

 BUNDLE.PUT b,"depts",deptlist
 BUNDLE.PUT b,"items",itemlist

FN.END

FN.DEF waitclick$()
 DO
  PAUSE 100
  HTML.GET.DATALINK data$
  IF BACKGROUND() 
   cs=CLOCK()
   DO
    PAUSE 100
   UNTIL (CLOCK()-cs)>5000|!BACKGROUND()
   IF BACKGROUND() THEN EXIT
  ENDIF
 UNTIL data$ <> ""
 ! popup data$,0,0,0
 IF IS_IN("BAK:",data$)=1
  EXIT
 ELSEIF IS_IN("DAT:",data$)=1
  data$=MID$(data$,5) %' User link
 ELSEIF IS_IN("LNK:file:///",data$)=1&IS_IN("?",data$) %' Submit link
  i=IS_IN("?",data$)
  data$="SUBMIT&"+MID$(data$,i+1)+"&"
 ENDIF
 FN.RTN data$
FN.END

FN.DEF readnumber(s$)
 IF IS_NUMBER(s$)
  FN.RTN VAL(s$)
 ELSE
  FN.RTN 0
 ENDIF
FN.END

FN.DEF asklist$(l,msg$,c,txt$)
 r$="<br>"
 h$="<!DOCTYPE html><html lang=~en~><head>"
 h$+="<meta charset=~utf-8~ /> <title>Home</title>"
 h$+="<meta content='width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0' name='viewport' />"
 h$+="<meta name=~viewport~ content=~width=device-width; initial-scale=1.0;maximum-scale=1.0~/></head>"
 h$+="<h1 style=~color:#fff~>"+msg$+"</h1>"
 h$+="<a style=~color:#fff~ >"+replace$(txt$,"\n","<br>")+"</a>"

 h$+="<body style=~background-color:#000;text-align:center;~>"
h$+="<div>"
 LIST.SIZE l,z
 FOR i=1 TO z
  LIST.GET l,i,s$
  s$=REPLACE$(s$,"'","'")

  s$=REPLACE$(s$,"***","&#x2714;")

  but$="<button type=~button~  style=~background-color:#cfc;color:#000;text-align:center;font-size:30px;width:100%;height:70px;border-width:5px;  moz-border-radius: 15px; -webkit-border-radius: 15px;~ onclick=~doDataLink('"+STR$(i)+"')~;>"+s$+"</button><br>"
  h$+=but$
 NEXT
 h$+="</div>"
 h$+="<script type=~text/javascript~>"
 h$+="function doDataLink(data)"
 h$+="{Android.dataLink(data);}</script>"
 h$+="</body>"
 h$+="</html>"
 h$=REPLACE$(h$,"~","\"")
 dmode("html")
 ! writeln("temp.html",h$)
 HTML.LOAD.STRING h$
 DO
  r$=waitclick$()
  c=readnumber(r$)
 UNTIL c>0
 LIST.GET l,c,s$
 s$=REPLACE$(s$,"&#x2714;","***")
 say$=REPLACE$(s$,"*","")
 say$=REPLACE$(say$,"-","")

 TTS.SPEAK say$
 FN.RTN s$
FN.END

FN.DEF Asklist(p,msg$,r,i$,g$)

 i$=asklist$(p,msg$,&r,g$)
 FN.RTN 0
 ! PRE: string list p
 !       msg$ is prompt to display
 ! POST: r = index of item in p chosen
 !       i$ = string of item
 !

 DIALOG.SELECT r, p, msg$
 IF r>0
  LIST.GET p, r, i$
 ENDIF

FN.END

FN.DEF iscommand(s$)
 ! return true if s$ is a reserved command word
 fn.rtn MID$(s$,1,3)="---"
FN.END

FN.DEF Generate$(b)
 BUNDLE.GET b,"depts",deptlist
 BUNDLE.GET b,"items",itemlist
 ! Generate grocery list
 LIST.CREATE s, userdept
 LIST.CREATE s, useritem
 LIST.SIZE itemlist, ql
 FOR gi = 1 TO ql
  LIST.GET deptlist, gi, gd$
  LIST.GET itemlist, gi, gi$
  IF ispicked(gi$)
   LIST.ADD userdept, gd$
   LIST.ADD useritem,off$(gi$)
  ENDIF
 NEXT gi
 BUNDLE.CREATE user
 BUNDLE.PUT user,"depts",userdept
 BUNDLE.PUT user,"items",useritem
 ! Save user's grocery list
 fn.rtn Prettyprint$(user)
FN.END

FN.DEF makenotes()
 Notefile$="groc notes.txt"
 FILE.EXISTS Isold, notefile$

 IF !isold
  TEXT.OPEN w, f, notefile$
  TEXT.CLOSE f
 ENDIF
 IF cask("add voice note?")

  STT.LISTEN
  STT.RESULTS thelist
  LIST.SIZE thelist, thesize
  LIST.GET thelist, 1,phrase$
  TEXT.OPEN a, filename, notefile$
  TEXT.WRITELN filename, phrase$
  TEXT.CLOSE filename
 ENDIF

 Editor(notefile$)
FN.END

FN.DEF HearNew$() 

 dmode("console")
 STT.LISTEN "listening for new item"
 STT.RESULTS thelist
 LIST.SIZE thelist, thesize
 LIST.GET thelist, 1,phrase$
 FN.RTN phrase$

FN.END

FN.DEF clearnotes() 
 Notefile$="groc notes.txt"

 TEXT.OPEN w, filename, notefile$
 TEXT.CLOSE filename
FN.END

FN.DEF getnotes$() 
 Notefile$="groc notes.txt"

 FILE.EXISTS Isold, notefile$

 IF isold 
  GRABFILE n$, notefile$
 ELSE
  N$=""
 ENDIF 
 FN.RTN n$
FN.END

! Retrieve all items from dept d$ and store them in picklist p

FN.DEF Getdept(d$, p, b)
 BUNDLE.GET b,"depts",dl
 BUNDLE.GET b,"items",il
 LIST.CLEAR p
 LIST.SIZE dl, q
 J=1
 DO
  LIST.SEARCH dl, d$, I, j
  IF i>0
   LIST.GET dl, I, dept$
   LIST.GET il, I, item$
   LIST.ADD P,Item$
   J=i+1
  ENDIF
 UNTIL i=0 
FN.END

FN.DEF Getchecked( p,d, b)
 BUNDLE.GET b,"depts",dl
 BUNDLE.GET b,"items",il
 LIST.CLEAR p
 LIST.CLEAR d
 LIST.SIZE dl, q
 FOR i = 1 TO q

  LIST.GET dl, I, dept$
  LIST.GET il, I, item$
  IF ispicked(item$)

   LIST.ADD P,Item$
   LIST.ADD d, dept$

  ENDIF
 NEXT i
FN.END

FN.DEF Pickanitem(msg$,d$,i$, b )
 LIST.CREATE s, picklist

 Getdept(d$, &picklist, b)

 LIST.ADD Picklist, "---VIEW---"
 LIST.ADD Picklist, "---NEW---"
 LIST.ADD Picklist, "---VOICE ADD---"

 LIST.ADD Picklist, "---EDIT---"
 LIST.ADD Picklist, "---DEL---"
 LIST.ADD Picklist, "---NOTES---"
 LIST.INSERT Picklist,1, "---BACK---"

 R=0
 I$=""
 Asklist(picklist,msg$, &r, &i$,g$)
 IF r=0
  I$="---BACK---"
 ENDIF
FN.END

FN.DEF View(f$, t) 
 dmode("console")
 
 Unedited$=f$+CHR$(10)
 m$="\n\ntap screen to close"
 PRINT unedited$+getnotes$() +m$
 ! Wait for screen touch flag
 T=0
 DO
  PAUSE 100
 UNTIL t
 CLS
FN.END

FN.DEF Pickitemnocmd(msg$,d$,i$, b)

 LIST.CREATE s, picklist

 Getdept(d$, &picklist,b)
 R=0
 I$=""
 Asklist(picklist,msg$, &r, &i$,g$)

FN.END

FN.DEF checkofflist(work$,b )
 LIST.CREATE s, picklist
 LIST.CREATE s, pickdept
 Getchecked( &picklist, &pickdept , b)
 LIST.ADD Picklist, "---BACK---"
 DO
  R=0
  I$=""
  Msg$="check off items"
  Asklist(picklist,msg$, &r, &i$,g$)
  IF i$<>"---BACK---" & r>0
   LIST.GET pickdept, r, d$
   Mark(&b,d$, i$)
   Toggle(&i$)
   LIST.REPLACE picklist, r, i$
  ENDIF
 UNTIL i$="---BACK---"
FN.END

FN.DEF PICKITEMS(b, D$, work$, t)
 Gg$="list.txt"
 DO
  ! build item selection list from department
  DO
   I$=""
   Msg$="Select Item from "+d$

   Pickanitem(msg$,d$,&i$, b)

   IF !Iscommand(i$)
    delItem(b, d$, i$)
    Insert(b, d$, i$)
    Mark(b, d$, i$)
   ENDIF

  UNTIL iscommand(i$)
  IF i$= "---DEL---"
   Msg$="pick item to delete
   E$=""

   Pickitemnocmd(msg$,d$,&e$, b)

   IF cask("delete " +e$+"?")
    delItem(b, d$, e$)
    writegroc(work$, b)
   ENDIF

  ELSE If i$="---VIEW---"
   g$=generate$( b)
   view(g$ , &t)
  ELSE If i$= "---NEW---"
   PRINT
   PAUSE 100 
   KB.HIDE
   PAUSE 500
   KB.TOGGLE
   INPUT "ENTER NEW item", n$, "", iscanceled
   KB.HIDE
   IF !iscanceled
    Insert(b, d$, n$)
    Mark(b, d$, n$) 
    writegroc(work$,b)

   ENDIF
  ELSE if i$="---VOICE ADD---"
   N$=HearNew$() 
   Insert(b, d$, n$)
   Mark(b, d$, n$) 
   writegroc(work$, b)
  ELSE if i$="---NOTES---"
   MakeNotes() 
  ELSE if i$="---EDIT---"
   E$=""
   Msg$="pick item to edit
   Pickitemnocmd(msg$, d$,&e$, b)
   INPUT "edit item", n$, e$
   Update(b, d$, e$, n$)
   writegroc(work$, b)
  ENDIF

 UNTIL I$="---BACK---"

FN.END

! MAIN 
!
!
!
dinit()
dmode("console")
Delim$=CHR$(9)
BUNDLE.CREATE b
LIST.CREATE s, deptlist
LIST.CREATE s, itemlist
BUNDLE.PUT b,"depts",deptlist
BUNDLE.PUT b,"items",itemlist
FILE.EXISTS Isold, "email.txt"
IF Isold
 TEXT.OPEN r, f,"email.txt"
 TEXT.READLN f, em$
 TEXT.CLOSE f
ELSE
 INPUT "enter default email to send list to", em$
 TEXT.OPEN w, f, "email.txt"
 TEXT.WRITELN f, em$
 TEXT.CLOSE f
ENDIF
! Use workspace if it exists
FILE.EXISTS Isold, work$
IF Isold
 readgroc(work$, &b)
ELSE
 readgroc(db$, &b)
 Clearlist(&b)
ENDIF
IF cask("Start new list?")
BUNDLE.GET b,"items",itemlist
 Clearlist(&b)
 Clearnotes() 
ENDIF
LIST.CREATE s, depts
BUNDLE.GET b,"depts",deptlist
LIST.ADD.LIST depts, deptlist
Deldups(&depts)
LIST.ADD depts, "---VIEW---"
LIST.ADD depts,"---VOICE ADD---" 
LIST.ADD depts,"---EDIT DB---"
LIST.ADD depts, "---CHECK---"
LIST.ADD depts, "---NOTES---"
LIST.INSERT depts, 1,"---QUIT---"
CLS
CONSOLE.TITLE "Grocery List"
LIST.SIZE itemlist, q
DO
 T=0
 Msg$="Select Department"
 Rnum=0
 D$=""
 
 g$=GENERATE$( b)
 Asklist(depts, msg$, &rnum, &d$,g$)
 IF rnum=0 
  D$= "---QUIT---"
 ENDIF
 IF D$= "---VOICE ADD---"
  N$=HearNew$() 
  IF IS_IN("---",d$)
   d$="OTHER"
  ENDIF
  Insert(b, d$, n$)
  Mark(b, d$, n$) 
  writegroc(work$,b)
 ENDIF
 IF D$= "---QUIT---"
  DIALOG.MESSAGE , "are you sure you want to quit?", c, "yes", "no"
  IF c=1
   finish=1
   D_U.BREAK
  ENDIF
 ENDIF
 IF d$="---CHECK---"
  checkofflist(work$,b)
 ENDIF
 IF D$="---VIEW---"
  g$=GENERATE$(b)
  T=0
  view(g$, &t)
 ENDIF
 IF d$="---NOTES---"
  Makenotes() 
 ENDIF
 IF d$="---EDIT DB---"
  editdb(work$) 
 ENDIF
 IF MID$(D$, 1,3)<>"---"
  T=0
  PICKITEMS(b, d$, work$,&t)
 ENDIF
UNTIL finish

Sendlist:
g$=generate$( b)
! Save workspace
writegroc(work$,b)
DO
 DIALOG.MESSAGE , "email to " +em$, c, "yes", "edit", "no"
 IF c=2
  INPUT "change email", em$, em$
  TEXT.OPEN w, f, "email.txt"
  TEXT.WRITELN f, em$
  TEXT.CLOSE f
 ENDIF
 IF c=1
  TIME Year$, Month$, Day$, Hour$, Minute$, Second$, WeekDay, isDST
  date$=year$+"-"+month$+"-"+Day$
  GRABFILE groc$, "list.txt"
! groc$=replace$(groc$,"\n","<br>")

groc$+=getnotes$
Groc$=encode$("UTF-8",groc$)
  EMAIL.SEND em$, "grocery list "+date$, groc$
 ENDIF
UNTIL c<>2
EXIT

ONCONSOLETOUCH:
T=1
CONSOLETOUCH.RESUME
