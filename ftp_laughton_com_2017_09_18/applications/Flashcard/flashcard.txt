
Flashcard:
 4 files, 3 of which are needed
flashcard.bas - the code
tick.mid - click sound on keypress
card.txt - your cards with Q & A's
tick.mid and card.txt go in RFO-Basic/data  folder for running under Basic!
cloud.png if you want the icon.

Read REM's if your going to make into .apk
Only permission needed is Write for .apk
By writing the card.txt on your device you
 can edit the file from your android device.

Card.txt is made up of your ANSWERS and Questions.

All ANSWERS have to be in CAPS.
ANSWERS can only be CAP. LETTER characters
ANSWERS come 1st
ANSWERS are less than 60 chars.
Don't put a space before or after your ANSWERS

Questions can be CAPS or not or Mixed
Questions can be of any typeable character
Questions are 2nd after it's ANSWER 
Questions can be up to 130 chars. long

Flashcard has a cheat,
even though, there is no such thing as cheating when your learning.
Tap the top textbox for help with question

Making your .apk
Use RFO-BASIC App Builder by Mougino
An excellent program, painless .apk's
found else where on this forum. 
Use MyAppv01.68.zip to make your .apk
 v1.74 crashes after install

In App resources, add tick.mid and card.txt
Set permissions to at least: Write external sources
Change icon to cloud.png if you want it.

-Hotwrench-