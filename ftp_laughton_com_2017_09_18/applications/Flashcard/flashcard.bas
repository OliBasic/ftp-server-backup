REM  FlashCard
REM  -Hotwrench-
REM  use MyAppv01.68.zip to make your .apk
REM  for some reason, v1.74 crashes 
REM  Notice the Rems below for .apk and BASIC!
REM  last 2 lines also, End and Exit
REM  By useing  TEXT.OPEN r,cfile,"../../flashcard/data/card.txt",
REM  it allows you to edit your card.txt file on you device
REM  only permission you need will be WRITE.

DIM fcard$[500] % 500 = 250 q&a's in .txt
ARRAY.LOAD kbdat$[],"Q","W","E","R","T","Y","U","I","O","P","A","S","D","F","G","H","J","K","L","Z","X","C","V","B","N","M"
ARRAY.LOAD pd[],10,95,790,95,20,100,780,100,30,105,770,105  %3d graphic

! AUDIO.LOAD playw,"../../flashcard/data/tick.mid" 
! TEXT.OPEN r,cfile,"../../flashcard/data/card.txt"
! unrem the above 2 lines for .apk and rem next 2 lines

AUDIO.LOAD playw,"tick.mid" 
TEXT.OPEN r,cfile,"card.txt"
! use the above 2 lines for BASIC! if not, rem the above 2 lines

card=0
DO
 TEXT.READLN cfile,fdata$
 card=card+1
 fcard$[card]=fdata$
UNTIL fdata$="EOF"
 card=card-1
TEXT.CLOSE cfile

GR.OPEN 255,100,120,255
GR.ORIENTATION 0
GR.SCREEN W, H
X1 = w /800
Y1 = h /480
GR.SCALE X1,Y1
GR.TEXT.SIZE 25
GR.SET.STROKE 4
GR.COLOR 255,0,0,0,0
GR.RECT rtb1, 10, 20, 790,85
GR.RECT rtb2, 10,120,790,180
GR.RECT rtb4,700,410,790,460
GR.COLOR 255,120,120,120,1
GR.RECT rct4, 700, 410, 790, 460 %exit
FOR t=1TO12 STEP4 % 3d graphics
 GR.LINE lne,pd[t],pd[t+1]-6,pd[t+2],pd[t+3]-6
 GR.LINE lne,pd[t],pd[t+1]+90,pd[t+2],pd[t+3]+90
NEXT
GR.SET.STROKE 1
GR.COLOR 255,220,220,220,1
GR.RECT rct1, 10, 20, 790,85   %top text
GR.RECT rct2, 10, 120, 790,180 % bottom text
GR.COLOR 255,230,230,230,0
GR.LINE elt,700,410,790,410  %3d exit
GR.LINE els,700,410,700,460
GR.COLOR 255,6,6,6,1
GR.TEXT.DRAW ques,20,45,"" %Question
GR.TEXT.DRAW ques2,20,75,"" %Question2
GR.TEXT.DRAW ans,20,160,"" %Answer
GR.TEXT.DRAW exit,725,445,"Exit" %exit btn
GR.TEXT.SIZE 30
GR.TEXT.DRAW spc,570,430,"<space>"
GR.COLOR 255,120,255,120,1
GR.TEXT.DRAW crt,340,230,"- Correct -"
GR.HIDE crt
GR.COLOR 255,6,6,6,1
GR.TEXT.SIZE 60
GR.TEXT.DRAW kbi,20,300,"Q  W  E  R  T  Y  U  I  O  P"
GR.TEXT.DRAW kbi,50,370,"A  S  D  F  G  H  J  K  L"
GR.TEXT.DRAW kbi,80,440,"Z  X  C  V  B  N  M"
GR.TEXT.DRAW baksp,660,370," <"
GR.RENDER

DO
 IF nques=0
  ndx=FLOOR(RND()*card)
  IF MOD(ndx,2)<>0 THEN ndx++
  flen=LEN(fcard$[ndx])
  IF flen <=65
   GR.MODIFY ques,"text",fcard$[ndx]
   GR.HIDE ques2
  ELSE
   FOR t= 55 TO 66
    IF MID$(fcard$[ndx],t,1)=" "THEN tt = t
   NEXT
   GR.SHOW ques2
   GR.MODIFY ques,"text",LEFT$(fcard$[ndx],tt)
   GR.MODIFY ques2,"text",RIGHT$(fcard$[ndx],flen-tt)
  END if 
  GR.MODIFY ans,"text",""+"_"
  nques=1
  GR.RENDER
 END if
 DO
  GR.TOUCH touched,x,y
 UNTIL touched

 DO
  GR.TOUCH touched,x,y

 UNTIL !touched
 x/=x1
 y/=y1
 IF y>240&y<300&x<700
  GR.MODIFY ans,"text",a$+kbdat$[CEIL(x/70)]+"_"
  a$=a$+kbdat$[CEIL(x/70)]
audio.play playw 
pause20
audio.stop 

 END if
 IF y>300 & y<370
  IF  x>50& x<630
   GR.MODIFY ans,"text",a$+kbdat$[CEIL((x-30)/70)+10]+"_"
   a$=a$+kbdat$[CEIL((x-30)/70)+10]
  END if
  IF x>650 & x<730 % backspace
   a$=LEFT$(a$,LEN(a$)-1)
   GR.MODIFY ans,"text",a$+"_"
  END if
audio.play playw 
pause20
audio.stop 
 END if

 IF y>370 & y<440
  IF x>80 & x<560
   GR.MODIFY ans,"text",a$+kbdat$[CEIL((x-70)/70)+19]+"_"
   a$=a$+kbdat$[CEIL((x-70)/70)+19]
  END if
  IF x>560 & x<670 % space
   a$=a$+" "
   GR.MODIFY ans,"text",a$+"_"
   !a$=fcard$[ndx-1]
  END if
audio.play playw 
pause20
audio.stop 
 END if

 IF  x > 10 & x < 790 & y> 20 & y< 110
  POPUP fcard$[ndx-1],0,-180,5  %cheat
 END if
 IF X >710 &  Y >410
  ex=1 %END
 END if
 GR.RENDER
 IF a$=fcard$[ndx-1]
  GR.SHOW crt
  GR.RENDER
  TONE 800,400,0
  PAUSE 1200
  GR.HIDE crt
  ndx=ndx+2
  nques=0
  a$=""
 END if
UNTIL ex=1
END   % use End for BASIC!
!Exit % use Exit for .apk

