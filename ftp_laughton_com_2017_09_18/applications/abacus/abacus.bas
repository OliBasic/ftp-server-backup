REM Abacus   -Hotwrench-
dim bd[24]
dim tbd[6]
gr.open 255,65,65,65
gr.screen w, h
x1=w/800
y1=h/480 
gr.scale x1,y1 
 gr.bitmap.create l1,50,50
gr.bitmap.drawinto.start l1
gr.set.stroke 3
rad=25
 for t=1 to rad
gr.color 255,0,0,t*250,0
GR.circle pt, 25,25,t
next
gr.bitmap.drawinto.end 
gr.set.stroke 15
gr.color 255,130,130,130,0
for t= 1 to 6
nt = nt + 115
gr.line g,nt,60,nt,480
next
 gr.color 255,175,80,0,0
 gr.rect g,7,60,794,480
 gr.line g,0,200,800,200 
 gr.color 255,0,0,255,1
 gr.text.size 40
 gr.text.draw gt,400,40,"" 
for tt = 1 to 6 
tb=tb+1
rem 6 row
bead = 0
row=row+115
gr.bitmap.draw tbd[tb],l1,row-25,145 
for t =1to 4
b=b+1
  rem 4 lower beads  
bead = bead + 50
gr.bitmap.draw bd[b],l1,row-25,bead+225
 next
next tt
gr.render
     
touch:
gr.touch touched,x,y
x/=x1
y/=y1

!ones
if touched &x>665 & x< 795

if y >175 &y< 225 &sum1=0
 gr.modify tbd[6],"y",70 
sum1=sum1+5
end if
if y >100 &y< 150 & sum1 >=5
 gr.modify tbd[6],"y",145
sum1=sum1 -5
end if

if y >275 &y< 325 & sum =0
 gr.modify bd[21],"y",210
sum++
end if 
if y >225 &y< 275 & sum =1
 gr.modify bd[21],"y",275 
sum--
end if

if y >325 & y<375 & sum = 1
 gr.modify bd[22],"y",260
sum++
end if
if y >275 & y<325 & sum = 2
 gr.modify bd[22],"y",325
sum--
end if

if y >375 & y<425 & sum = 2
 gr.modify bd[23],"y",310
sum++
end if
if y >325 & y<375 & sum = 3
 gr.modify bd[23],"y",375
sum--
end if

if y >425 & y<475 & sum = 3
 gr.modify bd[24],"y",360
sum++
end if
if y >375 & y<425 & sum = 4
 gr.modify bd[24],"y",425
sum--
end if

 gr.render
end if

! 10's
if touched &x>550 & x< 600 
 if y >175 &y< 225 & sum3=0
 gr.modify tbd[5],"y",70 
sum3=sum3+50
end if
if y >100 &y< 150 & sum3 >=50
 gr.modify tbd[5],"y",145
sum3=sum3 -50
end if 

if y >275 &y< 325 & sum2 =0
 gr.modify bd[17],"y",210 
sum2=sum2+10
end if
if y >225 &y< 275 & sum2 =10
 gr.modify bd[17],"y",275
sum2 = sum2 -10
end if

if y >325 & y<375 & sum2 = 10
 gr.modify bd[18],"y",260
sum2 = sum2 +10
end if
if y >275 & y<325 & sum2 = 20
 gr.modify bd[18],"y",325
sum2 = sum2 -10
end if

if y >375 & y<425 & sum2 = 20
 gr.modify bd[19],"y",310
sum2 =sum2 +10
end if
if y >325 & y<375 & sum2 = 30
 gr.modify bd[19],"y",375
sum2 =sum2 -10
end if

if y >425 & y<475 & sum2 = 30
 gr.modify bd[20],"y",360
sum2 =sum2 +10
end if
if y >375 & y<425 & sum2 = 40
 gr.modify bd[20],"y",425
sum2 =sum2 -10
end if

gr.render
end if

! 100's
if touched &x>435 & x< 485 
 if y >175 &y< 225 & sum5=0
 gr.modify tbd[4],"y",70 
sum5=sum5+500
end if
if y >100 &y< 150 & sum5 >=500
 gr.modify tbd[4],"y",145
sum5=sum5 -500
end if 

if y >275 &y< 325 & sum4 =0
 gr.modify bd[13],"y",210 
sum4=sum4+100
end if
if y >225 &y< 275 & sum4 =100
 gr.modify bd[13],"y",275
sum4 = sum4 -100
end if

if y >325 & y<375 & sum4 = 100
 gr.modify bd[14],"y",260
sum4 = sum4 +100
end if
if y >275 & y<325 & sum4 = 200
 gr.modify bd[14],"y",325
sum4 = sum4 -100
end if

if y >375 & y<425 & sum4 = 200
 gr.modify bd[15],"y",310
sum4 =sum4 +100
end if
if y >325 & y<375 & sum4 = 300
 gr.modify bd[15],"y",375
sum4 =sum4 -100
end if

if y >425 & y<475 & sum4 = 300
 gr.modify bd[16],"y",360
sum4 =sum4 +100
end if
if y >375 & y<425 & sum4 = 400
 gr.modify bd[16],"y",425
sum4 =sum4 -100
end if

gr.render
end if


! 1000's
if touched &x>320 & x< 370 
 if y >175 &y< 225 & sum7=0
 gr.modify tbd[3],"y",70 
sum7=sum7+5000
end if
if y >100 &y< 150 & sum7 >=5000
 gr.modify tbd[3],"y",145
sum7=sum7 -5000
end if 

if y >275 &y< 325 & sum6 =0
 gr.modify bd[9],"y",210 
sum6=sum6+1000
end if
if y >225 &y< 275 & sum6 =1000
 gr.modify bd[9],"y",275
sum6 = sum6 -1000
end if

if y >325 & y<375 & sum6 = 1000
 gr.modify bd[10],"y",260
sum6 = sum6 +1000
end if
if y >275 & y<325 & sum6 = 2000
 gr.modify bd[10],"y",325
sum6 = sum6 -1000
end if

if y >375 & y<425 & sum6 = 2000
 gr.modify bd[11],"y",310
sum6 =sum6 +1000
end if
if y >325 & y<375 & sum6 = 3000
 gr.modify bd[11],"y",375
sum6 =sum6 -1000
end if

if y >425 & y<475 & sum6 = 3000
 gr.modify bd[12],"y",360
sum6 =sum6 +1000
end if
if y >375 & y<425 & sum6 = 4000
 gr.modify bd[12],"y",425
sum6 =sum6 -1000
end if

gr.render
end if

! 10000's
if touched &x>215 & x< 265 
 if y >175 &y< 225 & sum9=0
 gr.modify tbd[2],"y",70 
sum9=sum9+50000
end if
if y >100 &y< 150 & sum9 >=50000
 gr.modify tbd[2],"y",145
sum9=sum9 -50000
end if 

if y >275 &y< 325 & sum8 =0
 gr.modify bd[5],"y",210 
sum8=sum8+10000
end if
if y >225 &y< 275 & sum8 =10000
 gr.modify bd[5],"y",275
sum8 = sum8 -10000
end if

if y >325 & y<375 & sum8 = 10000
 gr.modify bd[6],"y",260
sum8 = sum8 +10000
end if
if y >275 & y<325 & sum8 = 20000
 gr.modify bd[6],"y",325
sum8 = sum8 -10000
end if

if y >375 & y<425 & sum8 = 20000
 gr.modify bd[7],"y",310
sum8 =sum8 +10000
end if
if y >325 & y<375 & sum8 = 30000
 gr.modify bd[7],"y",375
sum8 =sum8 -10000
end if

if y >425 & y<475 & sum8 = 30000
 gr.modify bd[8],"y",360
sum8 =sum8 +10000
end if
if y >375 & y<425 & sum8 = 40000
 gr.modify bd[8],"y",425
sum8 =sum8 -10000
end if

gr.render
end if

! 100000's
if touched &x>110 & x< 160 
 if y >175 &y< 225 & sum11=0
 gr.modify tbd[1],"y",70 
sum11=sum11+500000
end if
if y >100 &y< 150 & sum11 >=500000
 gr.modify tbd[1],"y",145
sum11=sum11 -500000
end if 

if y >275 &y< 325 & sum10 =0
 gr.modify bd[1],"y",210 
sum10=sum10+100000
end if
if y >225 &y< 275 & sum10 =100000
 gr.modify bd[1],"y",275
sum10 = sum10 -100000
end if

if y >325 & y<375 & sum10 = 100000
 gr.modify bd[2],"y",260
sum10 = sum10 +100000
end if
if y >275 & y<325 & sum10 = 200000
 gr.modify bd[2],"y",325
sum10 = sum10 -100000
end if

if y >375 & y<425 & sum10 = 200000
 gr.modify bd[3],"y",310
sum10 =sum10 +100000
end if
if y >325 & y<375 & sum10 = 300000
 gr.modify bd[3],"y",375
sum10 =sum10 -100000
end if

if y >425 & y<475 & sum10 = 300000
 gr.modify bd[4],"y",360
sum10 =sum10 +100000
end if
if y >375 & y<425 & sum10 = 400000
 gr.modify bd[4],"y",425
sum10 =sum10 -100000
end if

gr.render
end if

gr.modify gt,"text",str$(sum+sum1+sum2+sum3+sum4+sum5+sum6+sum7+sum8+sum9+sum10+sum11) 
gr.render
goto touch
