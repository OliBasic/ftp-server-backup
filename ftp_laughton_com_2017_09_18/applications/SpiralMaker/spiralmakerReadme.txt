FILE MENU
Save - Save current image to file
Clear - Clear all spirals
Quit

COLOR MENU - Self explanatory

RATIO
Spiral is based on a small circle rolling around either the inside or the outside of a large circle. Ratio is the ratio of the radii of the large and small circles. The denominator, m, will be the number of lobes appearing in the spiral shape. The numerator is referred to as n.

RPR
The drawing pen passes passes through a small hole in the small circle. The distance from the center of the small circle to the pen is r-prime. RPR refers to the ratio of r-prime to the radius of the small circle.

RAD MENU
Rad is the radius of the large circle relative to the half-width of the screen.

STROKE MENU - select drawing stroke

FILL MENU - select fill mode

MODE MENU
Select an item to indicate whether the small circle rolls around the inside or outside of the large circle.

Sh/Hd Menu - Show or hide individual spirals

DRAW - Draw a spiral based on current settings

