!!
* BEGINNING OF BUTTONS.BAS
*  Functions for creating buttons and menu buttons
* in graphics mode.
*
* gr.open must be called before using these.
* 
* initButtons() initializes a new list of buttons,
* and returns a pointer to the list.
*
* Each Button object is stored as a bundle with
* the following keys:
*   text: the text displayed on the button
*   nrect: pointer to the button rectangle
*   next: pointer to the button text
*   left,top,width, height: bounding rectangle
*   color: see discussion below
*   items: if <0, indicates a simple button with no 
*       menu.  if not <0, items is the pointer to
*       a list of strings that holds the menu choices
*       for the button.
*
* btnAdd(cmdBtns,text$,l,t,w,h,col,items)
*    this function creates a new button and adds it 
*    to the list indicated by pointer cmdBtns.
*    Other parameters include the button's text,
*    left-top corner, width, height,color, and items 
*    list pointer. Text color is determined using 
*    bitwise assessment of the col parameter. Red is
*    4's bit, green the 2's bit, and blue the 1's
*    bit. So col=6 would indicate RGB color of
*    255,255,0. The color of the rectangle is the
*    complement of the text color. Pass in -1 for
*    the items parameter if this is a simple
*    button. The return value is the pointer to the
*    new button.
*
* btnCreate(text$,l,t,w,h,col,items)
*    this function creates a single button (without
*    adding it to any list).  See btnAdd regarding
*    parameters. The return value is the pointer
*    to the new button.
*
* btnQuery(nCmdBtn)
*    this function determines whether a specific
*    button (with pointer nCmdBtn) has been pressed.
*    If not, it returns 0. If the button has been
*    pressed and is a simple button (items=-1, ie 
*    no menu), the return value is -1. If the button
*    has been pressed and it has a menu (ie an
*    items list), the menu is displayed.  The return
*    value is then the index of the selected
*    menu item.
*
* queryButtons$(cmdBtns)
*    this function enumerates a list of buttons 
*    (cmdBtns) to determine which one button (if any)
*    has been pressed. If no button in the list has
*    been pressed, the returned string is "".
*    If a simple button has been pressed, the
*    returned string is the text of that button.
*    If a menu button has been pressed, the menu
*    is displayed. The returned string is then the
*    text of the menu item selected. Note: this
*    function would typically be called at the
*    start of a OnGRTouch: block.
*
*
!!
FN.DEF btnCreate(text$,l,t,w,h,col,items)
 GR.TEXT.SIZE 100
 GR.TEXT.ALIGN 1
 GR.GET.TEXTBOUNDS text$,ltxt,ttxt,rtxt,btxt
 sz = 100 * (h-20)/(btxt-ttxt)
 testw = (rtxt-ltxt)*sz/100
 IF testw>(w-20) THEN sz = 100 * (w-20)/(rtxt-ltxt)
 GR.TEXT.SIZE sz
 GR.GET.TEXTBOUNDS text$, ltxt, ttxt, rtxt, btxt

 red = BAND(4,col)/4
 green = BAND(2,col)/2
 blue = BAND(1,col)

 GR.COLOR 255,127*(1-red),127*(1-green),~
 127*(1-blue),1
 GR.RECT nrect,l,t,l+w,t+h
 GR.COLOR 255,255*red,255*green,255*blue,1
 GR.TEXT.DRAW ntxt,l+10,t+10-ttxt,text$

 BUNDLE.CREATE nCmdBtn
 BUNDLE.PUT nCmdBtn, "text", text$
 BUNDLE.PUT nCmdBtn, "nrect", nrect
 BUNDLE.PUT nCmdBtn, "ntxt", ntxt
 BUNDLE.PUT nCmdBtn, "items", items
 BUNDLE.PUT nCmdBtn, "left", l
 BUNDLE.PUT nCmdBtn, "top", t
 BUNDLE.PUT nCmdBtn, "width", w
 BUNDLE.PUT nCmdBtn, "height", h
 BUNDLE.PUT nCmdBtn, "color", col

 FN.RTN nCmdBtn
FN.END

FN.DEF btnQuery(nCmdBtn)
 BUNDLE.GET nCmdBtn, "left", left
 BUNDLE.GET nCmdBtn, "top", top
 BUNDLE.GET nCmdBtn, "width", width
 BUNDLE.GET nCmdBtn, "height", height
 GR.BOUNDED.TOUCH tchd, left, top, left+width-1, top+height-1
 IF !tchd THEN fn.rtn 0
 BUNDLE.GET nCmdBtn, "text", text$
 BUNDLE.GET nCmdBtn, "items", items
 IF items < 0 THEN fn.rtn -1
 SELECT sel, items, text$+" Menu"
 GR.RENDER
 FN.RTN sel
FN.END

FN.DEF initButtons()
 LIST.CREATE n,ptr
 FN.RTN ptr
FN.END

FN.DEF btnAdd(cmdBtns,text$,l,t,w,h,col,items)
 nCmdBtn = btnCreate(text$,l,t,w,h,col,items)
 LIST.ADD cmdBtns, nCmdBtn
 FN.RTN 0
FN.END

FN.DEF queryButtons$(cmdBtns)
 LIST.SIZE cmdBtns, count
 retValue$ = ""
 FOR n = 1 TO count
  LIST.GET cmdBtns, n, nbtn
  btnQ = btnQuery(nbtn)
  IF btnQ
   IF btnQ<0
    BUNDLE.GET nbtn, "text", retValue$
   ELSE
    BUNDLE.GET nbtn,"items",items
    LIST.GET items, btnQ, retValue$
   ENDIF
   F_N.BREAK
  ENDIF
 NEXT n
 FN.RTN retValue$
FN.END

! END OF BUTTONS.BAS

! BEGINNING OF SPIROGRAPH.BAS
! INCLUDE buttons.bas

GR.OPEN 255,20,20,20,0,1
PAUSE 1000
GR.SCREEN scrW, scrH

quit = 0


!!
Initialize variables:
  radius is the large circle radius in units of
    screen width
  n/m is ratio of small to large circle radii
  note m is also the number of lobes in pattern
  rrprm is ratio of r-prime to small circle radius,
    where r-prime is distance from center of small
    circle to pen tip
  mode indicates whether the small circle rolls
    around inside (mode 0) or outside (mode 1) of
    large circle
  fill is the fill mode used in gr.color statements
  fp is multiplied by m to determine how many points
    to calculate for a spiral
!!
radius=1
n=3
m=7
rrprm=0.5
mode=0
fp=36
fill = 0

ARRAY.LOAD primes[],2,3,5,7,11,13,17,21,23,29,31, ~
37,41,43
ARRAY.LENGTH nprimes,primes[]

!Allowable values for ratio denominator, m
ARRAY.LOAD denom$[],"3","4","5","6","7","8","9", ~
"10","11","12","13","14","15","16","17","18","19", ~
"20","21","22","23","24","25","26","27","28","29", ~
"30","31","32","33","34","35","36","37","38","39", ~
"40","41","42","43","44","45"

!This list will hold allowed values for numerator, n
LIST.CREATE s,numer

!!
lstSpirals is a list of pointers to bundles.
Each bundle in the list contains the following information about a single spiral:
  lstPoly:pointer to the list of x,y points
  visible:is the spiral currently visible?
  objectPtr:pointer to this object used by
     graphics commands
!!
LIST.CREATE n,lstSpirals

!List to be displayed in the show/hide menu
LIST.CREATE s, showHideOptions

!Initialize the menu buttons
btns = initButtons()

!Margin size
mrg = 10
!Button width
btnW = (scrW - 6*mrg)/5
!Button height
btnH = btnW/2
!Half-width of spiral drawing space
drW = scrW/2 - mrg
! x,y coordinates of center of drawing space
xScrC = mrg + drw
yScrC = 2*mrg + btnH + drW

!Add File menu
LIST.CREATE s,l1
LIST.ADD l1, "Save","Clear", "Quit"
CALL btnAdd(btns,"File",mrg,mrg,btnW,btnH,4,l1)

!Add Color menu
LIST.CREATE s,l2
LIST.ADD l2,"White","Red","Yellow","Green", ~
"BlueGreen","Blue","Purple"
CALL btnAdd(btns,"Color",2*mrg+btnW,mrg, ~
btnW,btnH,4,l2)

!Add Ratio button
CALL btnAdd(btns,"Ratio",3*mrg+2*btnW,mrg, ~
btnW,btnH,4,-1)

!Add RPR menu
LIST.CREATE s,l4
LIST.ADD l4,"rpr 0.0","rpr 0.125","rpr 0.25", ~
"rpr 0.375","rpr 0.5","rpr 0.625", ~
"rpr 0.75","rpr 0.875","rpr 1.0"
CALL btnAdd(btns,"RPR",4*mrg+3*btnW,mrg, ~
btnW,btnH,4,l4)

!Add Rad menu
LIST.CREATE s,l5
LIST.ADD l5,"rad 0.1","rad 0.2","rad 0.3", ~
"rad 0.4","rad 0.5","rad 0.6","rad 0.7", ~
"rad 0.8","rad 0.9","rad 1.0"
CALL btnAdd(btns,"Rad",5*mrg+4*btnW,mrg, ~
btnW,btnH,4,l5)

!Add Fill menu
LIST.CREATE s,l8
LIST.ADD l8,"fill 0","fill 1","fill 2"
CALL btnAdd(btns,"Fill",mrg,yScrC+drW+mrg, ~
btnW,btnH,4,l8)

!Add Stroke menu
LIST.CREATE s,l6
LIST.ADD l6,"str 4","str 6","str 8", ~
"str 12","str 16","str 20","str 24", ~
"str 32","str 40"
CALL btnAdd(btns,"Stroke",2*mrg+btnW, ~
yScrC+drW+mrg,btnW,btnH,4,l6)

!Add Mode menu
LIST.CREATE s,l7
LIST.ADD l7,"Inside","Outside"
CALL btnAdd(btns,"Mode",3*mrg+2*btnW,yScrC+drW+mrg, ~
btnW,btnH,4,l7)

!Add Show/Hide button
CALL btnAdd(btns,"Sh/Hd",4*mrg+3*btnW, ~
yScrC+drW+mrg,btnW,btnH,4,-1)

!Add Draw button
CALL btnAdd(btns,"Draw",5*mrg+4*btnW,yScrC+drW+mrg, ~
btnW,btnH,4,-1)

! menuDL contains the UI graphics objects
GR.GETDL menuDL[]

IF !BACKGROUND() THEN GR.RENDER

!Initialize color and stroke
red=255
green=255
blue=255
GR.COLOR 255,red,green,blue,fill
GR.SET.STROKE 4

!Main program loop
DO
UNTIL quit

GR.CLOSE
PRINT "Successful Exit"
END

! Graphics screen touch logic
ONGRTOUCH:

!!
  Determine whether a button or menu button has been
  touched.  If so, cmd$ will contain the text of the 
  menu item selected or for a simple button, the name
  of the button
!!
cmd$ = queryButtons$(btns)

SW.BEGIN cmd$

!Save image to file
 SW.CASE "Save"
  GR.SCREEN.TO_BITMAP bmPtr1
  GR.BITMAP.CROP bmPtr2,bmPtr1,xScrC-drW, ~
  yScrC-drW,2*drW,2*drW
  GR.FRONT 0
  INPUT "File Name",fname$
  GR.FRONT 1
  GR.BITMAP.SAVE bmPtr2, fname$+".jpg"
  GR.BITMAP.DELETE bmPtr1
  GR.BITMAP.DELETE bmPtr2
  IF !BACKGROUND() THEN GR.RENDER
  SW.BREAK

  !Quit
 SW.CASE "Quit"
  quit=1
  SW.BREAK

  !Clear all spirals
 SW.CASE "Clear"
  GR.NEWDL menuDL[]
  LIST.CLEAR lstSpirals
  IF !BACKGROUND() THEN GR.RENDER
  SW.BREAK

  !Change color
 SW.CASE "White"
 SW.CASE "Red"
 SW.CASE "Yellow"
 SW.CASE "Green"
 SW.CASE "BlueGreen"
 SW.CASE "Blue"
 SW.CASE "Purple"
  red=0
  IF cmd$="White"|cmd$="Red"|cmd$="Yellow" ~
   |cmd$="Purple" then red=255
   green=0
   IF cmd$="White"|cmd$="Yellow"|cmd$="Green" ~
    |cmd$="BlueGreen" then green=255
    blue=0
    IF cmd$="White"|cmd$="BlueGreen"|cmd$="Blue" ~
     |cmd$="Purple" then blue=255
     GR.COLOR 255,red,green,blue,fill
     SW.BREAK

!!
  Change ratio of small to large circles
  That is, set new n/m values
  Any denominator m from the array denom$[] may be
  selected.
  Values for the numerator n are restricted to
  numbers less than m which have no common factors
  with m
!!
 SW.CASE "Ratio"
  SELECT nsel,denom$[],"Denominator(#Lobes)"
  IF nsel=0
   GR.RENDER
   SW.BREAK
  ENDIF
  m = VAL(denom$[nsel])
  LIST.CLEAR numer
  FOR j=1 TO m-1
   validN = 1
   FOR k=1 TO nprimes
    IF MOD(j,primes[k])=0 & MOD(m,primes[k])=0
     validN=0
     GOTO divisibilitytestbreak
    END if
   NEXT k
   divisibilitytestbreak:
   IF validN THEN LIST.ADD numer,STR$(j)
  NEXT j
  SELECT nsel,numer,"Numerator(#Rotations)"
  IF nsel=0 THEN nsel=1
  LIST.GET numer,nsel,n$
  n = VAL(n$)
  GR.RENDER
  SW.BREAK

  !Change r-prime ratio
 SW.CASE "rpr 0.0"
 SW.CASE "rpr 0.125"
 SW.CASE "rpr 0.25"
 SW.CASE "rpr 0.375"
 SW.CASE "rpr 0.5"
 SW.CASE "rpr 0.625"
 SW.CASE "rpr 0.75"
 SW.CASE "rpr 0.875"
 SW.CASE "rpr 1.0"
  rrprm= VAL(WORD$(cmd$,2))
  SW.BREAK

  !Set radius
 SW.CASE "rad 0.1"
 SW.CASE "rad 0.2"
 SW.CASE "rad 0.3"
 SW.CASE "rad 0.4"
 SW.CASE "rad 0.5"
 SW.CASE "rad 0.6"
 SW.CASE "rad 0.7"
 SW.CASE "rad 0.8"
 SW.CASE "rad 0.9"
 SW.CASE "rad 1.0"
  radius = VAL(WORD$(cmd$,2))
  SW.BREAK

  !Set fill mode
 SW.CASE "fill 0"
 SW.CASE "fill 1"
 SW.CASE "fill 2"
  fill = VAL(WORD$(cmd$,2))
  GR.COLOR 255,red,green,blue,fill
  SW.BREAK

  !Set stroke
 SW.CASE "str 4"
 SW.CASE "str 6"
 SW.CASE "str 8"
 SW.CASE "str 12"
 SW.CASE "str 16"
 SW.CASE "str 20"
 SW.CASE "str 24"
 SW.CASE "str 32"
 SW.CASE "str 40"
  GR.SET.STROKE VAL(WORD$(cmd$,2))
  SW.BREAK

  !Set outside/inside drawing mode
 SW.CASE "Outside"
  mode=1
  SW.BREAK

 SW.CASE "Inside"
  mode=0
  SW.BREAK

  !Show or Hide individual spirals
 SW.CASE "Sh/Hd"
  LIST.SIZE lstSpirals,nSpir
  IF nSpir=0 THEN SW.BREAK
  LIST.CLEAR showHideOptions
  FOR i=1 TO nSpir
   LIST.GET lstSpirals,i,pSpiral
   BUNDLE.GET pSpiral,"visible",visible
   IF visible
    LIST.ADD showHideOptions, STR$(i)+" visible"
   ELSE
    LIST.ADD showHideOptions, STR$(i)+" hidden"
   ENDIF
  NEXT i
  SELECT nsel,showHideOptions,"Spirals"
  IF nsel=0
   GR.RENDER
   SW.BREAK
  ENDIF
  LIST.GET lstSpirals,nsel,pSpiral
  BUNDLE.GET pSpiral,"visible",visible
  BUNDLE.GET pSpiral,"objectPtr",objPtr
  IF visible
   GR.HIDE objPtr
   BUNDLE.PUT pSpiral,"visible",0
  ELSE
   GR.SHOW objPtr
   BUNDLE.PUT pSpiral,"visible",1
  END if
  GR.RENDER
  SW.BREAK

  !Create and draw spiral based on current settings
 SW.CASE "Draw"
  IF mode=0
   r0= radius*(1-n/m)
  ELSE
   r0= radius*(1+n/m)
  ENDIF
  rprm=rrprm*radius*n/m
  xOld= r0 + rprm
  yOld= 0
  npts=m*fp
  LIST.CREATE n, lstPoly
  FOR i = 1 TO npts
   theta=i*2*PI()*n/npts
   costh=COS(theta)
   sinth=SIN(theta)
   costhmn=COS(theta*m/n)
   sinthmn=SIN(theta*m/n)
   x=r0*costh+rprm*(costhmn*costh+sinthmn*sinth)
   y=r0*sinth+rprm*(costhmn*sinth-sinthmn*costh)
   xScr=xScrC+x*drW
   yScr=yScrC+y*drW
   LIST.ADD lstPoly,xScr,YScr
  NEXT i
  GR.POLY dum,lstPoly
  IF !BACKGROUND() THEN GR.RENDER
  BUNDLE.CREATE bSpiral
  BUNDLE.PUT bSpiral,"polygon",lstPoly
  BUNDLE.PUT bSpiral,"objectPtr",dum
  BUNDLE.PUT bSpiral,"visible",1
  LIST.ADD lstSpirals,bSpiral
  SW.BREAK

SW.END
GR.ONGRTOUCH.RESUME
