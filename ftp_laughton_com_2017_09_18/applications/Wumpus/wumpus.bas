rem  ###  THIS VERSION IS FROM THE PC-BLUE COLLECTION ON SIMTEL20, 
rem  ###  AND WAS POSTED BY MAGNUS OLSSON ON USENET IN 1992.
rem  ###  CONVERTED TO BASIC! FOR ANDROID BY ALEX KLOIAN IN SEPT-2011.
rem
rem
rem *** HUNT THE WUMPUS ***
dim p[5]
input "Instructions (y-n)", i$
if i$="n" then goto line35
gosub line375
line35:
goto line80
line80:
rem *** set up cave (dodecahedral node list) ***
dim s[20,3]
s[1,1] = 2
s[1,2] = 5
s[1,3] = 8
s[2,1] = 1
s[2,2] = 3
s[2,3] = 10
s[3,1] = 2
s[3,2] = 4
s[3,3] = 12
s[4,1] = 3
s[4,2] = 5
s[4,3] = 14
s[5,1] = 1
s[5,2] = 4
s[5,3] = 6
s[6,1] = 5
s[6,2] = 7
s[6,3] = 15
s[7,1] = 6
s[7,2] = 8
s[7,3] = 17
s[8,1] = 1
s[8,2] = 7
s[8,3] = 9
s[9,1] = 8
s[9,2] = 10
s[9,3] = 18
s[10,1] = 2
s[10,2] = 9
s[10,3] = 11
s[11,1] = 10
s[11,2] = 12
s[11,3] = 19
s[12,1] = 3
s[12,2] = 11
s[12,3] = 13
s[13,1] = 12
s[13,2] = 14
s[13,3] = 20
s[14,1] = 4
s[14,2] = 13
s[14,3] = 15
s[15,1] = 6
s[15,2] = 14
s[15,3] = 16
s[16,1] = 15 
s[16,2] = 17
s[16,3] = 20
s[17,1] = 7
s[17,2] = 16
s[17,3] = 18
s[18,1] = 9
s[18,2] = 17
s[18,3] = 19
s[19,1] = 11
s[19,2] = 18
s[19,3] = 20
s[20,1] = 13
s[20,2] = 16
s[20,3] = 19
fn.def fna(x)
x=round((20*rnd())+0.5)
fn.rtn x
fn.end

fn.def fnb(x)
x=round((3*rnd())+0.5)
fn.rtn x
fn.end

fn.def fnc(x)
x=round((4*rnd())+0.5)
fn.rtn x
fn.end

rem *** locate l array items ***
rem *** 1-you, 2-wumpus, 3&4-pits, 5&6-bats ***
dim l[6]
dim m[6]
line170:
for j=1 to 6
l[j]=fna(0)
m[j]=l[j]
next j
rem *** check for crossovers (ie l(1)=l(2), etc) ***
for j=1 to 6
for k=1 to 6
if j=k then goto line215
if l[j]=l[k] then goto line170
line215:
next k
next j
rem *** set no. of arrows ***
line230:
a=5
l=l[1]
rem *** run the game ***
print "hunt the wumpus"
rem *** hazard warning and location ***
line255:
gosub line585
rem *** move or shoot ***
gosub line670
if o = 1 then goto line280
if o = 2 then goto line300
line280:
rem *** shoot ***
gosub line715
if f=0 then goto line255
goto line310
rem *** move ***
line300:
gosub line975
if f=0 then goto line255
line310:
if f>0 then goto line335
rem *** lose ***
print "ha ha ha - you lose!"
goto Line340
rem *** move ***
line335:
print "hee hee hee - the wumpus'll get you next time!!"
line340:
for j=1 to 6
l[j]=m[j]
next j
input "same setup (y-n)", i$
if i$<>"y" then goto line170
goto line230
line375:
rem *** instructions ***
print "Welcome to 'hunt the wumpus'"
print "  the wumpus lives in a cave of 20 rooms. each room"
print "has 3 tunnels leading to other rooms. (look at a"
print "dodecahedron to see how this works-if you don't know"
print "what a dodecahedron is, ask someone)"
print " "
pause(7000)
print "     Hazards:"
print " Bottomless pits - two rooms have bottomless pits in them
print "     if you go there, you fall into the pit (& lose!)"
print " Super bats - two other rooms have super bats. if you"
print "     go there, a bat grabs you and takes you to some other"
print "     room at random. (which may be troublesome)"
REM input "type an e then return ", w9
print "     Wumpus:"
print " the wumpus is not bothered by hazards (he has sucker"
print " feet and is too big for a bat too lift).  usually"
print " he is asleep.  two things wake him up:you shooting an arrow"
print " or you entering his room."
print "     if the wumpus wakes he moves (p=.75) one room"
print " or stays still (p=.25).  after that, if he is where you"
print " are, he eats you up and you lose!"
print " "
pause(7000)
print "     you:"
print " each turn you may move or shoot a crooked arrow"
print "   moving:  you can move one room (thru one tunnel)"
print "   arrows:  you have 5 arrows.  you lose when you run out
print "   each arrow can go from 1 to 5 rooms. you aim by telling"
print "   the computer the room#s you want the arrow to go to."
print "   if the arrow can't go that way (if no tunnel) it moves"
print "   at random to the next room."
print "     if the arrow hits the wumpus, you win."
print "     if the arrow hits you, you lose."
pause(7000)
print "    Warnings:"
print "     when you are one room away from a wumpus or hazard,"
print "     the computer says:"
print " wumpus:  'i smell a wumpus'"
print " bat   :  'bats nearby'"
print " pit   :  'i feel a draft'"
print " "
pause(7000)
return
line585:
rem *** print location & hazard warnings ***
print " "
for j=2 to 6
for k=1 to 3
if s[l[1],k]<>l[j] then goto line640
if j=2 then goto line615
if j=3 then goto line625
if j=4 then goto line625
if j=5 then goto line635
if j=6 then goto line635
line615:
print "i smell a wumpus!"
goto line640
line625:
print "i feel a draft"
goto line640
line635:
print "bats nearby!"
line640:
next k
next j
print "You are in room ";
print format$("##",l[1])
print "Tunnels lead to ";
print format$("##",s[l,1]);
print format$("##",s[l,2]);
print format$("##",s[l,3])
print " "
return
line670:
rem *** choose option ***
line675:
pause(4500)
input "shoot or move (j-m)", i$
if i$<>"s" then goto line700
o=1
return
line700:
if i$<>"m" then goto line675
o=2
return
line715:
rem *** arrow routine ***
f=0
rem *** path of arrow ***
line735:
input "no. of rooms (1-5)", j9
if j9<1 then goto line735
if j9>5 then goto line735
for k=1 to j9
line760:
input "room #", p[k]
if k<=2 then goto line790
if p[k]<>p[k-2] then goto line790
print "arrows aren't that crooked - try another room"
goto line760
line790:
next k
rem *** shoot arrow ***
l=l[1]
for k=1 to j9
for k1=1 to 3
if s[l,k1]=p[k] then goto line895
next k1
rem *** no tunnel for arrow ***
l=s[l,fnb(1)]
goto line900
line840:
next k
print "missed"
l=l[1]
rem *** move wumpus ***
gosub line935
rem *** ammo check ***
a=a-1
if a>0 then goto line885
line880:
f=-1
line885:
return
rem *** see if arrow is at l(1) or at l(2)
line895:
l=p[k]
line900:
if l<>l[2] then goto line920
print "aha! you got the wumpus!"
pause(5000)
f=1
return
line920:
if l<>l[1] then goto line840
print "ouch! arrow got you!"
pause(5000)
goto line880
line935:
rem *** move wumpus routine ***
line940:
k=fnc(0)
if k=4 then goto line955
l[2]=s[l[2],k]
line955:
if l[2]<>l then goto line970
print "tsk tsk tsk - wumpus got you!"
pause(5000)
f=-1
line970:
return
line975:
rem *** move routine ***
f=0
line985:
input "where to", l
if l<1 then goto line985
if l>20 then goto line985
for k=1 to 3
rem *** check if legal move ***
 if s[l[1],k]=l then goto line1045
next k
if l=l[1] then goto line1045
print "not possible -";
pause(2500)
goto line985
rem *** check for hazards ***
line1045:
l[1]=l
rem *** wumpus ***
if l<>l[2] then goto line1090
print "... oops! bumped a wumpus!"
pause(2500)
rem *** move wumpus ***
gosub line940
if f=0 then goto line1090
return
rem *** pit ***
line1090:
if l=l[3] then goto line1100
if l<>l[4] then goto line1120
line1100:
print "yyyyiiiieeee . . . fell in pit"
pause(5000)
f=-1
return
rem *** bats ***
line1120:
if l=l[5] then goto line1130
if l<>l[6] then goto line1145
line1130:
print "zap--super bat snatch! elsewhereville for you!"
pause(3500)
l=fna(1)
goto line1045
line1145:
return
end
 
 
 
 
 
 
 
 
 
 
 
 
