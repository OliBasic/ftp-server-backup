!
! veggies.bas
! fruit and veggie countdown
!

FN.DEF isold(f$) 
 FILE.EXISTS e,f$ 
 FN.RTN e
FN.END

FN.DEF readln$(f$) 
 FILE.EXISTS e,f$ 
 IF e
  TEXT.OPEN r,h,f$
  TEXT.READLN h,a$
  TEXT.CLOSE h
 ELSE
  LET A$=""
 ENDIF
 FN.RTN a$
FN.END



FN.DEF writeln(f$,msg$) 
 TEXT.OPEN w,h,f$
 TEXT.WRITELN h,msg$
 TEXT.CLOSE h 
FN.END

FN.DEF getdate$()
 TIME y$, m$, d$, h$, mi$, s$
 FN.RTN m$+"-"+d$+"-"+y$
FN.END


od$=readln$("last.txt")

v$=readln$("veggies.txt")
IF v$="" THEN v$="7"
d$=getdate$()
IF od$<>d$
 writeln("last.txt",d$)
 v$="7"
ENDIF

v=VAL(v$)-1
IF v<0 THEN v=0
writeln("veggies.txt",INT$(v))

let Slide$= "veggie"+INT$(v)+".png"
  IF isold(slide$) 
GR.OPEN 255,0,0,0
gr.orientation 1
GR.SCREEN w,h
GR.BITMAP.LOAD bbb,slide$
GR.BITMAP.SIZE bbb,ww,hh
 let neww = (h/hh)*ww
 GR.BITMAP.SCALE nbm,bbb,neww,h
 GR.BITMAP.DRAW nn,nbm,(w - neww)/2+1,1
 GR.SHOW nn
 GR.RENDER
 PAUSE 2000 
 GR.CLOSE
else
p$=INT$(v)+" more veggies to go!"
POPUP p$
  ENDIF

EXIT
