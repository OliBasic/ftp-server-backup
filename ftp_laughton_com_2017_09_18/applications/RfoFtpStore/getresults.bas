REM Example Display Reader Results

Text.open r, file_number, "results.txt"

IF file_number <>0 THEN
  Do
    Text.readln file_number, line$
    Print line$
  Until line$ = "EOF"

  Text.close file_number
  File.Delete file_number, "results.txt"

ENDIF

