REM RFO Basic! FTP Store v1.1r1
REM by Antonis [tony_gr]
REM July 23, 2012

! pointers of icons, icon flag array
dim folders[33],title[33],progs[33],images[33],icon_on[33]
! upper menu pointers, preview text pointers
dim lin[10],txmenu[11], prvtx[25],ok[10]
DIM tobj1[26],tobj2[10],tobj3[10],robj[50] % keyboard pointers

count_dnld=0 % counts files to download
GR.open 255,0,0,45,0,1
GR.orientation 1
err$="#1a..contacting server"

FTP.open "FTP.laughton.com",21,"basic","basic"

err$="#1b..creating database"
 Stack.create S,paths % paths visited
 Stack.create S,downloads % holds files to download
 List.create S, dnldlist % auxiliary list to show files to download

 List.create S,Suploads % holds source files to upload
 List.create S,Duploads % holds data files to upload
 List.create S,dyrlistS % auxiliary list to show source files to upload
 List.create S,dyrlistD % auxiliary list to show data files to upload
 List.create S,checkdir % list to check if dir already present.
 
 stack.push paths,"./"
GR.screen w,h
sx=w/800
sy=h/1232
GR.scale sx,sy


! background
err$="#2..loading graphics"
GR.text.align 2
GR.text.size 150
 GR.bitmap.load sp,"space.jpg" 
 GR.bitmap.scale sp1, sp,w/sx,h/sy
 GR.bitmap.draw skin1,sp1,0,0
 GR.bitmap.delete sp
 GR.hide skin1
 GR.bitmap.load sp,"sea.jpg"
 GR.bitmap.scale sp1,sp,w/sx,h/sy
 GR.bitmap.draw skin2,sp1,0,0
 GR.bitmap.delete sp
 GR.hide skin2
GR.color 85,155,155,55,0
GR.color 85,155,155,55,0
GR.text.draw bas,400,300,"RFO"
GR.text.draw bas,400,500,"BASIC!"
GR.text.draw bas,400,700,"FTP STORE"

! bitmap loading
err$="#2..bitmap loading"
GR.color 255,255,255,55,0
GR.text.size 25
GR.bitmap.load  p1,"actionbar.png" % android bar
GR.bitmap.load  p2,"appmenu.png" % app menu
GR.bitmap.load  p3,"folder.png" % generic folder
GR.bitmap.load  p4,"prog.png" % program or not folder
GR.bitmap.load  p5,"image.png" % image icon
GR.bitmap.load  p10,"upwelcome.png" % welcome 1
GR.bitmap.load  p11,"lowwelcome.png" % welcome 2
GR.bitmap.draw  bar,p1,0,1080
GR.bitmap.draw  low,p11,30,873
GR.bitmap.draw  upw,p10,620,50
! draw & hide folders & programs icons & their titles
k=0
for i=1 to 11
for j=1 to 3
 k=k+1
 ! GR.rect rec,80+(j-1)*270, (i-1)*100, 190+(j-1)*270, (i-1)*100+80
 GR.bitmap.draw  folders[k],p3,100+(j-1)*270+10, (i-1)*100
 GR.bitmap.draw  progs[k],p4,100+(j-1)*270+10, (i-1)*100
 GR.bitmap.draw  images[k],p5,100+(j-1)*270+10, (i-1)*100
 GR.text.draw title[k],(j-1)*270+120 +10,(i-1)*100+60,"12345678901234567"
 GR.hide folders[k]
 GR.hide progs[k]
 GR.hide images[k]
 GR.hide title[k]
 
next j
next i

! lower menu
GR.bitmap.draw  menu,p2, 0,580
GR.hide menu

! upper menu & choices
GR.color 255,0,0,0,1
GR.text.align 1
GR.rect upmenu,540,50,800,710
GR.color 255,255,255,255,1
GR.text.size 36
GR.text.draw txmenu[1], 550,90," Get selected"
GR.text.draw txmenu[2] ,550,150," Deselect All"
GR.text.draw txmenu[3] ,550,210," Deselect Last"  
GR.text.draw txmenu[4] ,550,270," Download List"
GR.text.draw txmenu[5] ,550,330," Preview last"
GR.text.draw txmenu[6] ,550,390," Upload Files.."
GR.text.draw txmenu[7] ,550,450," AutoPreview"
GR.text.draw txmenu[8] ,550,510," Basic! Forum"
GR.text.draw txmenu[9] ,550,570," Change skin"
GR.text.draw txmenu[10] ,550,630," About"
GR.text.draw txmenu[11],550,690," Exit"
GR.line lin[1] ,550,105,800,105
GR.line lin[2] ,550,165,800,165
GR.line lin[3] ,550,225,800,225
GR.line lin[4] ,550,285,800,285
GR.line lin[5] ,550,345,800,345
GR.line lin[6] ,550,405,800,405
GR.line lin[7] ,550,465,800,465
GR.line lin[8] ,550,525,800,525
GR.line lin[9] ,550,585,800,585
GR.line lin[10],550,645,800,645

for i=1 to 10
  GR.hide txmenu[i]
  GR.hide lin[i]
next i
GR.hide txmenu[11]
GR.hide upmenu

! auropreview icon
GR.color 255,255,0,0,1
gr.rect auproff, 772,420,800,450
GR.color 255,0,255,0,1
gr.rect aupron, 772,420,800,450
gr.hide aupron
gr.hide auproff
! upper menu icon
GR.color 255,255,255,255,1
GR.rect rct,770,5,780,15
GR.rect rct,770,20,780,30
GR.rect rct,770,35,780,45
! GR.rect r,750,0,800,50
GR.text.size 25

! initilize
path$= "./"
morepages=0
showedpages=0
skin=0
firsttime=0
firstkeyb=0
welcome=0
showpreview=0

! bar log : paths & messages
GR.color 255,0,155,0,1
GR.text.draw msg,155,1180," "
GR.color 255,255,0,0,1
GR.text.draw cur_path,155,1145,"./"
GR.color 255,255,255,55,0
gosub updatepath
upmenuon=0
menuon=0

! preview black box, text lines
GR.color 255,0,0,0,1
GR.rect preview, 0,160,800,1040
GR.hide preview
GR.text.size 30
GR.color 255,0,255,255,1
for i=1 to 25
  GR.text.draw prvtx[i],5,190+(i-1)*35," "
  GR.hide prvtx[i]
next i

gr.set.stroke 4
! yes no button
gr.color 255,0,0,0,1
gr.rect ok[1],250,200,550,400
gr.color 255,255,255,0,1
gr.line ok[2],255,260,546,260
gr.color 255,255,255,0,0
gr.rect ok[3] ,254,204,546,396
GR.text.align 1
GR.text.size 35
gr.color 255,255,255,0,1
gr.text.draw ok[4] ,280,245,"okQWER TYPE" % title
GR.color 255,0,255,255,1
gr.rect ok[5] ,280,300,380,350
gr.rect ok[6] ,420,300,520,350
gr.color 255, 0,65,0,1
GR.text.size 40
gr.text.bold 1
gr.text.draw ok[7] ,295,340,"YES"
gr.text.draw ok[8] ,445,340,"NO"
gr.color 255,0,0,0,1
GR.text.align 2
gr.text.bold 0
for i=1 to 8 
 gr.hide ok[i]
next

GR.text.align 2
GR.color 255,255,255,55,0
GR.text.size 25
GR.render


! main program
start: % main loop
 
  gosub wait % for a touch
  if welcome=0 then
     welcome=1
	 gr.hide upw
	 gr.hide low
	 gr.bitmap.delete p10
	 gr.bitmap.delete p11
	 gr.render
  endif
! if icon selected, execute command
  icon_selected=0
  if menuon=0 & upmenuon=0 then 
  for i=1 to 11
    for j=1 to 3
	 icon_selected=icon_selected+1
     if x>(80+(j-1)*270)*sx & y>((i-1)*100)*sy & x<((j-1)*270+190)*sx & y<((i-1)*100+80)*sy & icon_on[icon_selected]=1 then
	   derectory=0
	   gosub checkicon
	   if derectory=1 then % directory choosed
	       gosub chgdir              
	   else % file choosed
		   gosub file_selected
	   endif
	   finished=1
	   f_n.break
     endif
    next j
	if finished=1 then f_n.break
  next i
  finished=0
 endif
  
! menu/basic icon selected
 if x<150*sx & y>1080*sy & upmenuon=0 then
   menuon=!menuon
   if menuon=1 then GR.show menu else GR.hide menu
! GR.show sp2
   GR.render
 endif

! upmenu
 if x>750*sx & y<50*sy & menuon=0 then
   upmenuon=!upmenuon
   gosub show_upmenu
   gosub up_menu
 endif

! more pages selected  
if upmenuon=0 & x>200*sx  & y>1080*sy & menuon=0 & pagesleft>0 then
  gosub morepages
endif
  
! applications
 if x<=300*sx & y>=580*sy & y<670*sy & menuon=1 & upmenuon=0 then
   GR.hide menu
   GR.render
   menuon=0
   path$= "./applications/"
   neupath$= "applications/"
   stack.push paths,"applications/"
   pagesleft=0
   gosub chgdir
  endif
 
! games 
 if x<=300*sx & y>=670*sy & y<750*sy & menuon=1 & upmenuon=0 then
   GR.hide menu
   GR.render
   menuon=0
   path$= "./games/"
   neupath$= ".games/"
   stack.push paths, "games/"
   pagesleft=0
   gosub chgdir
  endif

! utilities
 if x<=300*sx & y>=750*sy & y<830*sy & menuon=1 & upmenuon=0 then
   GR.hide menu
   GR.render
   menuon=0
   path$= "./utilities/"
   neupath$= ".utilities/"
   stack.push paths, "utilities/"
   pagesleft=0
   gosub chgdir
 endif

! html 
 if x<=300*sx & y>=830*sy & y<910*sy & menuon=1 & upmenuon=0 then
   GR.hide menu
   GR.render
   menuon=0
   path$= "./html/"
   neupath$= ".html/"
   stack.push paths, "html/"
   pagesleft=0
   gosub chgdir
 endif
  
! tools 
 if x<=300*sx & y>=910*sy & y<990*sy & menuon=1 & upmenuon=0 then
   GR.hide menu
   GR.render
   menuon=0
   path$= "./tools/"
   neupath$= ".tools/"
   stack.push paths, "tools/"
   pagesleft=0
   gosub chgdir
 endif
  
  ! other 
 if x<=300*sx & y>=990*sy & y<1080*sy & menuon=1 & upmenuon=0 then
   GR.hide menu
   GR.render
   menuon=0
   path$= "./other/"
   neupath$= ".other/"
   stack.push paths, "other/"
   pagesleft=0
   gosub chgdir
 endif

goto start % main loop



! ***************
! * SUBROUTINES *
! ***************

! ***change dir/show new dir***
chgdir:
err$="#3..ftp chdir error"
jj=0
for i=1 to 33
  icon_on[i]=0
next i
icon_selected=0

FTP.cd path$
gosub updatepath
FTP.dir archives_list
if path$= "./" then % hide something
   list.get archives_list,3,name$ 
   if name$=".ftpquota" then List.remove archives_list,3
   list.get archives_list,3,name$ 
   if name$=".htaccess" then List.remove archives_list,3
endif

list.size archives_list,asize

gosub check_num_of_pages

if asize>33 then % add root,up choice
   for i=2 to  numpages
    list.insert archives_list,(i-1)*33+1,"..(d)"
    list.insert archives_list,(i-1)*33+1,".(d)"
  next i
endif

list.size archives_list,asize

gosub check_num_of_pages

pagesleft=numpages

! this is called also as a subroutine ;)
! when more than 33 items to show
morepages:
if pagesleft=0 then return
jj=jj+1
pagesleft=pagesleft-1
if  pagesleft>0 then 
   GR.modify msg,"text","More Files: Touch here to go"
 else 
   GR.modify msg,"text","Waiting:"
endif
GR.render
gosub hid_icons_titles
ii=0
for i=(jj-1)*33+1 to jj*33
  ii=ii+1
  if i<=asize then
   list.get archives_list,i,name$ 
   if len(name$)>22 then name$="~"+right$(name$,20)
   if right$(name$,3)= "(d)" then
      name$=left$(name$,len(name$)-3)
      if name$="." then name$= "/"
      if name$=".." then name$="Up"
      GR.show folders[ii]
     else % i have a file
	   fileTemp$=right$(lower$(name$),4)
	   if fileTemp$=".png" | fileTemp$=".jpg" | fileTemp$="jpeg" then gr.show images[ii] else GR.show progs[ii]
   endif  
   GR.show title[ii]
   icon_on[ii]=1
   GR.modify title[ii],"text",name$
  endif
next i
GR.render
if showpreview=1 then gosub imagepreview
return 
!*end of chgdir*

!*** checks an icon pressed***
checkicon:       
     err$="#4..icon checking"
	 cur_element=round((numpages-pagesleft-1)*33)+icon_selected
     list.get archives_list,cur_element,name$	 
     a=0
     a=is_in("(d)",name$)
     if a=0 then derectory=0 % we have a file
	 if a<>0 then  % we have a dir
	   if name$= ".(d)" then 
	       path$= "./"
		     gosub updatepath
	    elseif name$= "..(d)" then
	       Stack.IsEmpty paths, n
		     if n=0 then
		        stack.pop paths, apath$
            ! up
			      path$=left$(path$,len(path$)-len(apath$))
			    	gosub updatepath
		     endif 	
	    else
        neupath$ = left$(name$,len(name$)-3)+"/"
	      path$=path$+neupath$
	      stack.push paths,neupath$
        gosub updatepath
	   endif  
     derectory=1
   endif % for directory 
gosub updatepath
return
! *end of checkicon*

! ***treats upper menu when opened***
up_menu:
 err$="#5..upper menu opening"

rewait:

gosub wait

if x>750*sx & y<50*sy then 
   ! close menu
   gosub hide_upmenu
   upmenuon=!upmenuon
   return
endif 
 
if x>550*sx & y>50*sy & y<=105*sy then 
 ! 1st option selected / get selected
 err$="#6..file downloading"
 popup "downloading "+replace$(str$(count_dnld),".0","")+" files",0,0,4
 gosub hide_upmenu
 upmenuon=!upmenuon
 do
   Stack.IsEmpty downloads, n
   if n=0 then      
     Stack.pop downloads,file$
     GR.modify msg,"text","Downloading: "+right$(file$,40) 
     GR.render
     fileTemp$=lower$(right$(file$,4))
     for l=1 to len(file$)
       if mid$(file$,l,1)="/" then index=l
     next l
     fname$=right$(file$,len(file$)-index)
	 ! get rid of "." now
 	 file$=right$(file$,len(file$)-1)
     if fileTemp$=".bas" then	
        FTP.get file$,"../source/"+fname$
     elseif fileTemp$=".apk" then
        FTP.get file$,"../"+fname$
     else           
       FTP.get file$,fname$
     endif
   endif	 
 until n=1
 GR.modify msg,"text","Downloaded: "+ replace$(str$(count_dnld),".0","")+" files."
 GR.render
 popup "Finished!",0,0,0
 tone 800,800
 count_dnld=0
 Stack.clear downloads
 return
endif % for 1st option

  
 if x>550*sx & y>105*sy & y<=165*sy then 
 ! 2nd option selected / Deselect all
  err$="#7..file deselecting"
  Stack.clear downloads
  popup "Download list Empty!",0,0,0
  count_dnld=0
  pause 1500
  gosub hide_upmenu
  upmenuon=!upmenuon
  return
 endif % for 2nd option
  
 if x>550*sx & y>165*sy & y<=225*sy then 
 ! 3d option selected / Deselect last 
  err$="#7..file deselecting"
  Stack.IsEmpty downloads, n
  if n=0 then
    stack.pop downloads,temp$
    popup "Last file deselected!",0,0,0
    count_dnld=count_dnld-1
  endif
  pause 1500
  gosub hide_upmenu
  upmenuon=!upmenuon
  return
 endif% for 3rd option
 
 if x>550*sx & y>225*sy & y<=285*sy then 
 ! 4th option selected / Download list 
  err$="#7..list"
  gosub hide_upmenu
  upmenuon=!upmenuon
  GR.modify msg,"text","Showing download list.."
  for i=1 to count_dnld
   stack.pop downloads,temp$
   list.add dnldlist,temp$
  next i
  for i=count_dnld to 1 step -1
    list.get dnldlist,i,temp$
    stack.push downloads,temp$
  next i	
  GR.show preview
  for i=1 to 25
     GR.modify prvtx[i],"text",""
     GR.show prvtx[i]
  next i
  ! print list/wait/remove list elements
  k=0  
  for i=1 to count_dnld
    if mod(i,25)=1 & i>2 then
          GR.render
          k=0
          gosub wait
    endif
      k=k+1
      list.get dnldlist,i,temp$
      if len(temp$)>45 then temp$="~"+right$(temp$,45)
      GR.modify prvtx[k],"text",temp$   
   next i
  for l=k+1 to 25
     GR.hide prvtx[l]
  next 
  extramsg$=""
  if  pagesleft>0 then  extramsg$=" More Files: touch here to go!"  
  GR.modify msg,"text","End of list. Touch screen to quit."+ extramsg$
  GR.render
  gosub wait
  GR.hide preview
  for i=1 to 25
     GR.hide prvtx[i]
  next i 
  GR.modify msg,"text","Waiting.."+ extramsg$
  GR.render
  for i=1 to count_dnld
      List.remove dnldlist,1
  next i    
  return
 endif % for 4th option

 
 if x>550*sx & y>285*sy & y<=345*sy then 
 ! 5th option selected / Preview last 
 err$="#8..preview"
  Stack.IsEmpty downloads, n
  if n=1 then 
    popup "Nothing to preview",0,0,0
    gosub hide_upmenu
    upmenuon=!upmenuon
   else 
    gosub hide_upmenu
    upmenuon=!upmenuon
    Stack.peek downloads, file$
    fileTemp$=lower$(file$)
    fileTemp$=right$(filetemp$,4)

    if fileTemp$=".bas" | fileTemp$=".txt" | fileTemp$=".htm" | fileTemp$="html" | fileTemp$=".xml" then
    ! **************
    ! *text  viewer*
    ! **************
	GR.text.size 30
    finished=0
    stack.peek downloads,file$
    for l=1 to len(file$)
       if mid$(file$,l,1)="/" then index=l
    next l
    fname$=right$(file$,len(file$)-index)
    GR.modify msg,"text","Loading '"+fname$+"'"
    GR.render
    file$=right$(file$,len(file$)-1)
    FTP.get file$, fname$
    FILE.Size fsize,fname$
    GR.modify msg,"text","Showing file: '"+fname$+"', Size is:"+replace$(str$(fsize),".0","")
    GR.show preview
    GR.render
    for i=1 to 25
     GR.modify prvtx[i],"text",""
     GR.show prvtx[i]
    next i
    
    j=1
    k=0
    line$=""
    TEXT.open r, Ftable, fname$
    while line$<>"EOF" & finished=0
       TEXT.readln ftable, line$
       if LEN(line$)<41 THEN
           toprint$=line$
           k=k+1
           GR.modify prvtx[k],"text",toprint$
           GR.render
           GOSUB check_k
       endif
       IF LEN(line$)>=41 THEN
          a$=left$(line$,40)
          found=1 
          j=1
          FOR i=40 TO LEN(line$)
            a$=a$+MID$(line$,i-1,1)
            b$=a$+MID$(line$,i,1)
            GR.TEXT.width w1,a$
            GR.TEXT.width w2,b$
            IF w1<=800*j & w2>800*j THEN          
              j=j+1       
              toprint$=MID$(line$,found,i-found)
              k=k+1
              GR.modify prvtx[k],"text",toprint$
              GR.render
              found=i
              GOSUB check_k 
            ENDIF
          NEXT i
          IF found=1 THEN 
           toprint$=line$
           k=k+1
           GR.modify prvtx[k],"text",toprint$
           GR.render
           GOSUB check_k
          ELSE
           k=k+1
           toprint$=MID$(line$,found,LEN(line$)-found+1)
           GR.modify prvtx[k],"text",toprint$
           GR.render 
           GOSUB check_k
          ENDIF
       ENDIF
    repeat
    gosub wait
    file.delete f,"../"+name$
    GR.hide preview
    for ll=1 to 25
       GR.modify prvtx[ll],"text",""
      GR.hide prvtx[ll]
    next ll
      
   GR.modify msg,"text","Viewing finished: Waiting.."
   GR.render

  endif % text viewer

    if fileTemp$="jpeg" | fileTemp$=".jpg" | fileTemp$=".png" then
    ! **************
    ! *image viewer*
    ! **************
    stack.peek downloads,file$
     for l=1 to len(file$)
       if mid$(file$,l,1)="/" then index=l
     next l
     fname$=right$(file$,len(file$)-index)
     GR.modify msg,"text","Loading '"+fname$+"'"
  ! 	GR.show preview
     GR.render
    file$=right$(file$,len(file$)-1)
    ! now i have the full name in fname$

    gosub imageviewer

   endif % for gfx viewer
  endif % n=1
  return
 endif % for 5th option

! 6th option selected / upload
! Upload files 
if x>550*sx & y>345*sy & y<=405*sy then  % if start
err$="#9..Upload files"
  GR.modify msg,"text","Select .bas files to upload, then touch bar"
  pause 500  
  gosub hide_upmenu
  upmenuon=!upmenuon
  GR.show preview
  for i=1 to 25
   GR.show prvtx[i]
   gr.modify prvtx[i],"text",""
  next i
  gr.render
  
  mypath$="../source/"
  uploads=Suploads
  dyrlist=dyrlistS
  gosub fileselector
!  gosub wait
  for i=1 to 25
   gr.modify prvtx[i],"text",""
  next i
  GR.modify msg,"text","Select data files to upload, then touch bar"
  gr.render
  mypath$=""
  uploads=Duploads
  dyrlist=dyrlistD
  gosub fileselector
 ! gosub wait
 GR.modify msg,"text","Files to upload selected. Waiting.."
 gr.hide preview
for i=1 to 25
   gr.modify prvtx[i],"text",""
   gr.hide prvtx[i]
  next i
 gr.render

yesnotxt$="Upload files?"
gosub yesnobutton

if yes=1 then 
  for i=1 to 25
   gr.show prvtx[i]
  next i
  gr.show preview
  ! Select Directory
   GR.modify msg,"text","Please select one of the above categories.."
   gr.modify prvtx[1],"text","    >SELECT CATEGORY BELOW<"
   gr.modify prvtx[3],"text","       [APPLICATIONS]"
   gr.modify prvtx[4],"text","          [GAMES]"
   gr.modify prvtx[5],"text","         [UTILITY]"
   gr.modify prvtx[6],"text","          [TOOLS]"
   gr.modify prvtx[7],"text","          [HTML]"
   gr.modify prvtx[8],"text","          [OTHER]"
   gr.modify prvtx[9],"text","        [BETA-TEST]"
gr.render
found=0
do
   gosub wait
   for i1=3 to 9 % 
	    if y>=(195+(i1-2)*35)*sy &  y<(195+(i1-1)*35)*sy then found=i1-2
   next i1
until found<>0

if found=1 then target$="applications"
if found=2 then target$="games"
if found=3 then target$="utility"
if found=4 then target$="tools"
if found=5 then target$="html"
if found=6 then target$="other"
if found=7 then target$="beta-test"
 GR.modify msg,"text","Selected upload category: "+target$
 gr.render

 gosub keyboard
 
 ! now upload everything to writed$ directory
  GR.modify msg,"text", "Please wait, uploading files.."
  gr.render
  ! check if dir exists.
  ftp.cd "./"+target$
  ftp.dir checklist
  d_exists=0
  list.size checklist,cl
  for i=1 to cl
    list.get checklist,i,checkname$
	
    if right$(checkname$,3)= "(d)" then
	   checkname$=left$(checkname$,len(checkname$)-3)
	   if checkname$=writed$ then 
	    writed$=checkname$
	    d_exists=1
	    f_n.break
	   endif
	endif
	next i
  
  ! check finished, go on
  list.size Suploads,ll1
  list.size Duploads,ll2
  if d_exists=0 then ftp.mkdir writed$
  for i=1 to ll1
    list.get Suploads,i,myfile$
    ftp.put  "../source/"+myfile$, writed$+"/"+myfile$
 
  next i
  for i=1 to ll2
    list.get Duploads,i,myfile$
    ftp.put  myfile$, writed$+"/"+myfile$
 
  next i
  tone 800,400
  GR.modify msg,"text","Files Uploaded to "+target$+"/"+writed$+". Upload list is empty. "
  gr.render
 else
    ! give a messagge
    GR.modify msg,"text","Upload cancelled, Upload list is empty. "+target$
  gr.render
 endif % for yes/no, uploading files

 for i=1 to 25
   gr.modify prvtx[i],"text",""
   gr.hide prvtx[i]
  next i
 gr.render
 list.size Suploads,ll1
  list.size Duploads,ll2
  ! list.size Duploads,ll2
  for i=1 to ll1
   list.remove Suploads,1
  next i
  for i=1 to ll2
   list.remove Duploads,1
  next i 
  
 list.size dyrlistD,ll1
  list.size dyrlistS,ll2
  ! list.size Duploads,ll2
  for i=1 to ll1
   list.remove dyrlistD,1
  next i
  for i=1 to ll2
   list.remove dyrlistS,1
  next i 
gr.hide preview
for i=1 to 25
   gr.modify prvtx[i],"text",""
   gr.hide prvtx[i]
  next i
 gr.render
 return
endif %  % if end for upload files option


! 7th option selected / autopreview
if x>550*sx & y>405*sy & y<=465*sy then 
   showpreview=!showpreview
  if showpreview=1 then 
   gr.show aupron
   gr.hide auproff
  else 
   gr.show auproff
   gr.hide aupron
  endif
  gr.render
  pause 1000  
  gosub hide_upmenu
  upmenuon=!upmenuon
  return
endif

! 8th option selected / Link
if x>550*sx & y>465*sy & y<=525*sy then
  gosub hide_upmenu
  upmenuon=!upmenuon
  Browse "http://rfobasic.freeforums.org/"
  return
endif

 if x>550*sx & y>525*sy & y<=585*sy then 
 ! 6th option selected | change skin
  err$="#9..skin"
  if skin=0 then 
      GR.show skin1
      GR.hide skin2
      skin=1
  elseif skin=1 then
     GR.hide skin1
     GR.show skin2
     skin=2
  else
     GR.hide skin1
     GR.hide skin2
     skin=0
   endif
  GR.render
  gosub hide_upmenu
  upmenuon=!upmenuon
  return
 endif
 
if x>550*sx & y>585*sy & y<=645*sy then 
 ! 9th option selected | About
  err$="#9..about"
  GR.modify msg,"text","About.. / Touch screen to quit!"
  pause 500  
  gosub hide_upmenu
  upmenuon=!upmenuon
  GR.show preview
  for i=1 to 6
   GR.show prvtx[i*2]
  next i
  GR.modify prvtx[2],"text","         >>_Rfo Basic! FTP Store_<<"
  GR.modify prvtx[4],"text","         >>_version 1.1 r1_<<"
  GR.modify prvtx[6],"text","         >>_July 23, 2012_<<"
  GR.modify prvtx[8],"text","         >>_by Antonis [tony_gr]_<<"
  GR.modify prvtx[10],"text","Join the rfobasic.freeforums.org for questions or"
  GR.modify prvtx[12],"text","comments about this program."
  GR.render
  gosub wait
  GR.modify msg,"text","Waiting.."
  for i=1 to 6
   GR.hide prvtx[i*2]
  next i
  GR.hide preview
  GR.render
  return 
 endif

 if x>550*sx & y>645*sy & y<=705*sy then 
 ! 10th option selected / exit
  popup "Quitting",0,0,0
  err$="#10..session closing"
  FTP.close
  gosub hide_upmenu
  upmenuon=!upmenuon
  print "Bye!"
  END

  return 
 endif  % for 8th option
 
goto rewait
! return
! *end of up_menu*

! ***hide_icons_titles***
hid_icons_titles:
 err$="#11..icon"
 for m=1 to 33
  GR.hide folders[m]
  GR.hide title[m]
  GR.hide progs[m]
  GR.hide images[m]
  icon_on[m]=0
 next m
GR.render
return
! *end of hid_icons_titles*

! ***file_selected***
file_selected:
err$="#12..file_selecting"
count_dnld=count_dnld+1
GR.modify msg,"text","Selected: "+left$(name$,40) 
stack.push downloads,path$+name$
GR.render
return
! * end offile_selected*

! ***check_num_of_pages of files+dirs***
check_num_of_pages:
err$="#13..page_selecting"
! file_selected
if asize >33 then 
   a=asize
   rest=mod(a,33)
   numpages=ROUND((a-rest)/33)+1
   morepages=1
 else
 numpages=1
endif
return
! *end of check_num_of_pages*

! ***show_upmenu***
show_upmenu:
err$="#14..upmenu_opening"
for i=1 to 10
  GR.show txmenu[i]
  GR.show lin[i]
next i
GR.show txmenu[11]
if showpreview=1 then gr.show aupron else gr.show auproff
GR.show upmenu
GR.render
return
! *end of show_upmenu*

! ***hide_upmenu***
hide_upmenu:
err$="#15..upmenu_closing"
gr.hide aupron
gr.hide auproff
for i=1 to 10
  GR.hide txmenu[i]
  GR.hide lin[i]
next i
GR.hide txmenu[11]
GR.hide upmenu
GR.render
return
! *end of show_upmenu*

! ***updatepath***
updatepath:
err$="#15..path updating"
GR.modify cur_path,"text","path: "+right$(path$,45)
GR.render
return
! *end of updatepath*

! *** wait for a touch***
wait:

do
  GR.touch touched,x,y
  until touched
 do
   GR.touch touched,x,y
 until !touched

return
! *end of wait*

! ***checks k***
! called by text viewer
check_k:
 IF k=25 THEN 
  k=0
  GR.modify msg,"text","Touch bar to quit, elsewhere to continue."  
  GR.render
  GOSUB wait
  IF y>1080*sy THEN  
      TEXT.close ftable
      finished=1
	  return
  endif 
  for l=1 to 25
     GR.modify prvtx[l],"text",""
	 GR.render
    next l
endif	
GR.render
return


fileselector:
 ! get listing & len of source dir
  
  File.Dir mypath$, FileArray$[]
  array.length ll,FileArray$[]
  ! copy to dyrlist without dirs
  
  for i=1 to ll
   if right$(FileArray$[i],3)<> "(d)" then list.add dyrlist, FileArray$[i]
  next i
  array.delete FileArray$[]
  ! ll is the number of files in the source directory
  list.size dyrlist,ll
  
  fcount=0 % counter for current page 1..25
  fpage=0 % page number of showed files
  nfpages=((ll-mod(ll,25))/25)+1
  
  ! show pages and select
  for i=1 to ll % 8
    fcount=fcount+1
	List.get dyrlist,i, name$
	gr.modify prvtx[fcount],"text",name$
    ! check fcount
	if fcount=25 then fcount=0
	if i=ll | fcount=0 then % 7 i am in last page, or end of current page
	if fcount<>0 then
	  for i4=fcount+1 to 25
	       gr.modify prvtx[i4],"text",""
      next i4
	endif
    gr.render
	do % 6
	  gosub wait
	  ! screen touched, find what
	  for i1=1 to 25 % 5
	    if y>=(195+(i1-2)*35)*sy &  y<(195+(i1-1)*35)*sy & i1+fpage*25<=ll then % 4 found
		  element=i1
		  ! get it
		  List.get dyrlist,i1+fpage*25,name$ % only if i1+fpage*25<=ll
		  ! see if already in uploadlist +
		  if left$(name$,1)<>"+" then % 3 not in upload list,  add it
			list.add uploads,name$ % add it
			List.replace dyrlist,i1+fpage*25,"+" + name$
			gr.modify prvtx[i1],"text", "+" + name$ % update screen
			gr.render
			
		  else % already in upload list, remove it
		      ! find it and remove it, update screen
		       name$=mid$(name$,2,len(name$))
           List.search uploads, name$, i2
		      	list.remove uploads,i2
			    	List.replace dyrlist,i1+fpage*25,name$
           gr.modify prvtx[i1],"text",name$ % update screen		  
			    	gr.render
		   endif % 3 for adding or removing element from upload list
	    endif % 4 for choosing from screen
     next i1 % 5
   	until y>1080*sy  % 6 finished, with source
	if fcount=0 then fpage=fpage+1
	if fpage=nfpages then f_n.break
    endif % 7
  next i % 8
return

yesnobutton:
yes=-1
 for i=1 to 9
  gr.show ok[i]
 next i
gr.modify ok[4],"text",yesnotxt$
gr.render
do
gosub wait
if x>280*sx & y>300*sy & x<380*sy & y<350*sy then yes=1
if x>420*sx & y>300*sy & x<520*sy & y<350*sy then yes=0
until yes<>-1
pause 500
for i=1 to 9
  gr.hide ok[i]
 next i
gr.render
return


! ******************
! * keyboard input *
! ******************
keyboard:


! ****keyboard chars****
gr.text.bold 0
gr.text.align 1
gr.set.stroke 0
gr.color 255,0,0,255,1
gr.render
if firsttime=0 then
str0$="qwertyuiopasdfghjklzxcvbnm"
str1$="1234567890"
str2$="@.()+-_~<>"
endif

! ****draw keyboard rectangle****
GR.text.size 40
GR.color 255,0,0,255,1
if firsttime=0 then
 GR.rect rectObj, 0, 601, 799, 1230
else 
 gr.show rectObj
endif
GR.color 255,0,255,0,0
if firsttime=0 then
  GR.text.draw txtObj, 10, 1175, "Chars left:"
else
 gr.show txtObj
endif
GR.color 255,255,255,255,0
if firsttime=0 then
 GR.text.draw txp, 50, 1225,">Insert your app's directory name<"
else
 gr.show txp
endif
 GR.text.size 60

! ****PRINT characters of keyboard****
! the writed$ str contains my text
! letters "qwertyuiopasdfghjklzxcvbnm"
ydisp=0
xdisp=0
j=0
k=1
l=0
FOR i=1 to 26
 strg$=MID$(str0$,i,1)
 IF  i=11 | i=20  THEN 
    xdisp=35*k
    ydisp=70*k 
    j=0
    k=k+1    
 ENDIF 
 j=j+1
 l=l+1
if firsttime=0 then
GR.color 255,255,255,255,1
 GR.text.draw tobj1[i],(10+(j-1)*81+xdisp),(810+ydisp),strg$
GR.color 255,255,255,255,0
 GR.rect robj[l],(3+(j-1)*81+xdisp),(760+ydisp),(68+(j-1)*81+xdisp),(820+ydisp) 
else
 gr.show tobj1[i]
 gr.show robj[i]
endif
NEXT i

! ****numbers****
FOR j=1 to len(str1$)
 strg$=MID$(str1$,j,1)
 l=l+1
if firsttime=0 then
GR.color 255,255,255,255,1
 GR.text.draw tobj2[j],(10+(j-1)*81),740,strg$
GR.color 255,255,255,255,0
 GR.rect robj[l],(3+(j-1)*81),690,(68+(j-1)*81), 750
else 
 gr.show tobj2[j]
 gr.show robj[l]
endif
NEXT j

! ****special chars ",.():;-'!?"****
FOR j=1 to len(str2$)
 strg$=MID$(str2$,j,1)
 l=l+1
if firsttime=0 then
GR.color 255,255,255,255,1
 GR.text.draw tobj3[j],(10+(j-1)*81),1020,strg$ 
GR.color 255,255,255,255,0
 GR.rect robj[l],(3+(j-1)*81),970,(68+(j-1)*81),1030
else 
 gr.show tobj3[j]
 gr.show robj[l]
endif
NEXT j

! ****capslock, space, delete, RETURN****
GR.text.bold 1
GR.color 255,255,255,255,1
if firsttime=0 then
 GR.text.draw t1,80,1100,"A"
 GR.text.draw t2,250,1100,"   [________] "
 GR.text.draw t3,140,1100,"Del"
 GR.text.draw t4,600 ,1100,"ENTER"
 ! GR.text.draw tstop,600 ,1190,"STOP"
else
 gr.show t1
 gr.show t2
 gr.show t3
 gr.show t4
endif

GR.text.bold 0
GR.color 255,255,255,255,0

if firsttime=0 then
 GR.rect rob1,70,1040,125,1120
 GR.rect rob2,240,1040,580,1120
 GR.rect rob3,135,1040,230,1120
 GR.rect rob4,590,1040,790,1120
 ! GR.rect rob4,590,1130,755,1210
else 
 gr.show rob1
 gr.show rob2
 gr.show rob3
 gr.show rob4
endif

GR.color 255,255,255,255,1

GR.render
! ---PRINTing characters finished--- 

! ****wait for user input****

 writed$=""
 touchme:
 DO % check FOR max lenght
   touched = -1
   GR.touch touched, x, y
   PAUSE 50
 UNTIL touched>0
 IF  len(writed$)=30 THEN 
    POPUP "maximum length, press ENTER",0,0,2
  touchsend:
  DO
   touched = -1
   GR.touch touched, x, y
  UNTIL touched>0
  IF  x>590*sx & y>1040*sy & x<755*sx &y<1120*sy THEN 
      GOTO enterpressed 
	 ELSE 
	   GOTO touchsend
  ENDIF  
 ENDIF 


 ! ****scan keyboard for numbers****
 FOR j=1 to 10
 IF  x>(3+(j-1)*81)*sx & x<((j-1)*81+68)*sx & y>690*sy & y<750*sy THEN 
  TONE 1200,100
  writed$=writed$+MID$(str1$,j,1)  
  GOSUB printmystring
  GR.render
 ENDIF 
 NEXT j

 ! scan keyboard for letters
 ydisp=0
 xdisp=0
 j=0
 k=1
 FOR i=1 to 26
 IF  i=11 | i=20  THEN 
    xdisp=35*k
    ydisp=70*k
    j=0
    k=k+1    
 ENDIF 
 j=j+1
!GR.rect robj[l],(3+(j-1)*81+xdisp),(760+ydisp),(68+(j-1)*81+xdisp),(820+ydisp) 
 IF  x>(3+(j-1)*81+xdisp)*sx & y>(760+ydisp)*sy & x<(68+(j-1)*81+xdisp)*sx &y<(820+ydisp)*sy THEN   
  writed$=writed$+MID$(str0$,i,1)
  TONE 1200,100   
  GOSUB printmystring
  GR.render
 ENDIF 
 NEXT i

 ! ****scan keyboard for CAPS****
 IF  x>70*sx & y>1040*sy & x<125*sx &y<1120*sy then
  ! CAPS ON OFF invert str0$
   IF  LEFT$(str0$,1)="q" THEN str0$=upper$(str0$) ELSE str0$=lower$(str0$)
 TONE 1200,100
 ydisp=0
 xdisp=0
 j=0 
 k=1
 FOR i=1 to 26
 strg$=MID$(str0$,i,1)
 IF  i=11 | i=20  THEN 
    xdisp=35*k
    ydisp=70*k
    j=0
    k=k+1    
 ENDIF 
 j=j+1
 GR.hide tobj1[i]
 GR.color 255,255,255,255,1
 GR.text.draw tobj1[i],(10+(j-1)*81+xdisp),(810+ydisp),strg$
 GR.color 255,255,255,255,0
 NEXT i
 GR.color 255,255,255,255,1
 GR.render
 ENDIF 

 ! ****scan keyboard for space****
 IF  x>240*sx & y>1040*sy & x<580*sx &y<1120*sy then
  ! SPACE   
  writed$=writed$+" "
  TONE 1200,100   
  GOSUB printmystring
  GR.render
 ENDIF 

 ! ****scan keyboard for DEL****
 IF  x>135*sx & y>1040*sy & x<230*sx &y<1120*sy then
  ! DEL 
  IF  len(writed$)>=1 then
    writed$=LEFT$(writed$,len(writed$)-1)
    TONE 1200,100  
    GOSUB printmystring
    GR.render
  ENDIF 
 ENDIF 

! ****scan keyboard FOR ENTER****
 IF  x>590*sx & y>1040*sy & x<755*sx &y<1120*sy then
     IF LEN(writed$)>0 THEN 
       GOTO enterpressed
     ELSE 
       Popup "empty text",0,0,0
     ENDIF 
 ENDIF 
 
 ! ****scan keyboard for special chars****
 FOR j=1 to 10
 IF  x>(3+(j-1)*81)*sx & x<((j-1)*81+68)*sx & y>970*sy & y<1030*sy THEN 
  TONE 1200,100
  writed$=writed$+MID$(str2$,j,1)   
  GOSUB printmystring
  GR.render
 ENDIF 
 NEXT i

 ! *** loop FOR NEXT char *** 
 GOTO touchme 
 
! ****subroutines****

! ****prints on screen my text****
printmystring:
gr.text.size 40
GR.color 255,25,255,0,1
  GR.hide tobj
  GR.hide tobjj
  GR.hide tobjjj
l=len(writed$)
! split writed$ into 3 pieces
GR.text.width ww,writed$
IF  ww<=800*sx THEN 
                 wrtd$=writed$
                 wr$=""
                 w$=""
           ENDIF 
IF  ww>800*sx & ww<=1600*sx then
 FOR i=2 to l
   GR.text.width w1,LEFT$(writed$,i-1)
   GR.text.width w2,LEFT$(writed$,i)
   IF  w1<=800*sx & w2>800*sx THEN ll=i
  NEXT i
  wrtd$=LEFT$(writed$,ll-1)
  wr$=MID$(writed$,ll,l-ll+1)
  w$=""
ENDIF 
IF  ww>1600*sx then
 FOR i=2 to l
   GR.text.width w1,LEFT$(writed$,i-1)
   GR.text.width w2,LEFT$(writed$,i)
   IF  w1<=800 & w2>800 THEN ll=i
   IF  w1<=1600 & w2>1600 THEN lll=i
  NEXT i
  wrtd$=LEFT$(writed$,ll-1)
  wr$=MID$(writed$,ll,lll-ll)
  w$=MID$(writed$,lll,l-lll+1)
 
ENDIF  

        p1=640
        p2=680
        p3=670 % not used
        GR.text.draw tobj,0,p1, wrtd$     
        IF  wr$<>"" THEN GR.text.draw tobjj,0,p2, wr$    
        IF  w$<>""  THEN GR.text.draw tobjjj,0,p3, w$
        GR.render

  counter=30-l
  k=Ends_with( ".0", str$(counter))
  counter$=LEFT$(str$(counter),k-1)
  GR.hide countobj
  GR.text.draw countobj, 210, 1175,counter$
  gr.text.size 60
  GR.render
RETURN

enterpressed:
 ! clear traces...
  tone 1200,300 
  gr.hide keyp
  gr.hide tobj
  gr.hide tobjj
   gr.hide tobjjj
  for i=1 to 10 
   gr.hide tobj2[i]
   gr.hide tobj3[i]
  next i
  for i=1 to 26
   gr.hide tobj1[i]
  next i
  gr.hide t1
  gr.hide t2
  gr.hide t3
  gr.hide t4
  gr.hide rectobj
  for i=1 to 46
    gr.hide robj[i]
  next i
  gr.hide rob1
  gr.hide rob2
  gr.hide rob3
  gr.hide rob4
  gr.hide txtobj
  gr.hide txp
  gr.hide countobj
  gr.text.align 2
  gr.color 255,255,255,0,1
   gr.set.stroke 4
  gr.text.size 25
  gr.text.bold 0
firsttime=1
y=-1
gr.render
return

imagepreview:
! if fileTemp$="jpeg" | fileTemp$=".jpg" | fileTemp$=".png" then


found$=""
for i=1 to asize
  list.get archives_list,i,name$ 
  fileTemp2$=left$(lower$(name$),6) 
  fileTemp1$=right$(lower$(name$),4)
  if (fileTemp2$="screen") & (fileTemp1$=".png" | fileTemp1$=".jpg" | fileTemp1$="jpeg") then 
   found$=name$
   f_n.break
  endif
next i
if found$="" then 
  for i=1 to asize
  list.get archives_list,i,name$  
  fileTemp1$=right$(lower$(name$),4)
  if fileTemp1$=".png" | fileTemp1$=".jpg" | fileTemp1$="jpeg" then
     found$=name$
     f_n.break
   endif
   next i
endif
if found$<>"" then 
   GR.modify msg,"text","Previewing "+name$
   gr.render
   file$=path$+found$
   file$=right$(file$,len(file$)-1)
   fname$=found$
   gosub imageviewer % for name$ 
   GR.modify msg, "text", "Preview finished. Waiting.. "
   gr.render
endif
return

! renders an image
imageviewer:

    FTP.get file$, "../"+fname$
    GR.modify msg,"text","Rendering image: '"+fname$+"'"
    GR.render
    GR.bitmap.load bmp0, "../"+fname$
    GR.bitmap.size bmp0,w,h
    currentscale=1
    IF w>=800*sx | h>=800*sy THEN % image >800x800
       IF w>=h THEN currentscale=800/w ELSE currentscale=800/h
    ENDIF
    GR.bitmap.scale bmp1, bmp0, currentscale*w, currentscale*h
    
    ! image DIM are now x->currentscale*ww y->currentscale*hh
    ! center image
    xpos=(800-currentscale*w)/2
    ypos=200+(800-currentscale*h)/2
    GR.bitmap.draw bmp2,bmp1,xpos,ypos
    GR.modify msg,"text","Preview finished! Touch Screen to go on!"
    GR.render
    
    ! wait to go on
    gosub wait
    GR.bitmap.delete bmp0
    GR.bitmap.delete bmp1
    GR.hide bmp2
   	GR.hide preview
    GR.render
    file.delete f,"../"+name$
    GR.modify msg,"text","Waiting.."
    GR.render
return % imageviewer

return
! ***close connection on back key***
OnBackKey:
print "stopped!"
FTP.close
end

! on error treatment
!!
OnError:
 print err$
 GR.cls
 GR.text.draw t,10,100,"Unexpected error;"
 GR.text.draw t,10,200,err$
 GR.render
 pause 7000
end
!!
