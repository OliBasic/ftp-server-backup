REM RFO Basic! FTP Store v1.2r1
REM by Antonis [tony_gr]
REM August 3, 2012
!

! pointers of icons, titles,icon flag array
DIM folders[33],title[33],progs[33],images[33],basic[33],icon_on[33]
DIM details[33] % details$[33]
! upper menu pointers, preview text pointers
DIM lin[11],txmenu[12], prvtx[25],ok[10]
DIM tobj1[26],tobj2[10],tobj3[10],robj[50] % keyboard pointers

count_dnld=0 % counts files to download
GR.OPEN 255,0,0,45,0,1
GR.ORIENTATION 1
err$="#1a..contacting server"

FTP.OPEN "FTP.laughton.com",21,"basic","basic"

err$="#1b..creating database"
STACK.create S,paths % paths visited
STACK.create S,downloads % holds files to download
LIST.CREATE S, dnldlist % auxiliary list to show files to download

LIST.CREATE S,Suploads % holds source files to upload
LIST.CREATE S,Duploads % holds data files to upload
LIST.CREATE S,dyrlistS % auxiliary list to show source files to upload
LIST.CREATE S,dyrlistD % auxiliary list to show data files to upload
LIST.CREATE S,checkdir % list to check if dir already present.
LIST.CREATE S, fsiz    % holds file sizes & dates of remote files
LIST.CREATE S, fnam    % holds file names of remote files
STACK.push paths,"./"

! check to see if file exists
FILE.EXISTS ex, "rfo_ftp_store.data"
IF ex=0 THEN
 TEXT.OPEN w,filn,"rfo_ftp_store.data"
 TEXT.WRITELN filn,"o#6" % this version includes 6 options
 TEXT.WRITELN filn,"c01#0" % autopreview
 TEXT.WRITELN filn,"c02#0" % show detailes
 TEXT.WRITELN filn,"c03#3" % skin
 TEXT.WRITELN filn,"c04#0"
 TEXT.WRITELN filn,"c05#0"
 TEXT.WRITELN filn,"c06#0"
 TEXT.CLOSE filn
ENDIF
! load ini file
! first line, skin
! second line, preview
! third line, details
count=0
TEXT.OPEN r,filn,"rfo_ftp_store.data"
TEXT.READLN filn,line$

TEXT.READLN filn,line$
showpreview=VAL(RIGHT$(line$,1))
TEXT.READLN filn,line$
showdetails=VAL(RIGHT$(line$,1))
TEXT.READLN filn,line$
skin=VAL(RIGHT$(line$,1))
TEXT.CLOSE filn

GR.SCREEN w,h
sx=w/800
sy=h/1232
GR.SCALE sx,sy


! background
err$="#2..loading graphics"
GR.TEXT.ALIGN 2
GR.TEXT.SIZE 150
GR.BITMAP.LOAD sp,"space.jpg"
GR.BITMAP.SCALE sp1, sp,w/sx,h/sy
GR.BITMAP.DRAW skin1,sp1,0,0
GR.BITMAP.DELETE sp
GR.HIDE skin1
GR.BITMAP.LOAD sp,"sea.jpg"
GR.BITMAP.SCALE sp1,sp,w/sx,h/sy
GR.BITMAP.DRAW skin2,sp1,0,0
GR.BITMAP.DELETE sp
GR.HIDE skin2
GR.BITMAP.LOAD sp,"coloredbg.jpg"
GR.BITMAP.SCALE sp1, sp,w/sx,h/sy
GR.BITMAP.DRAW skin3,sp1,0,0
GR.BITMAP.DELETE sp
GR.HIDE skin3
GR.COLOR 85,155,155,55,0
GR.COLOR 85,155,155,55,0
GR.TEXT.DRAW bas,400,300,"RFO"
GR.TEXT.DRAW bas,400,500,"BASIC!"
GR.TEXT.DRAW bas,400,700,"FTP STORE"

! bitmap loading
err$="#2..bitmap loading"
GR.COLOR 255,255,255,55,0
GR.TEXT.SIZE 25
GR.BITMAP.LOAD  p1,"actionbar.png" % android bar
GR.BITMAP.LOAD  p2,"appmenu.png" % app menu
GR.BITMAP.LOAD  p3,"folder.png" % generic folder
GR.BITMAP.LOAD  p4,"prog.png" % program or not folder
GR.BITMAP.LOAD  p5,"image.png" % image icon
GR.BITMAP.LOAD  p6,"basiclogo.png" % image icon
GR.BITMAP.LOAD  p10,"upwelcome.png" % welcome 1
GR.BITMAP.LOAD  p11,"lowwelcome.png" % welcome 2
GR.BITMAP.DRAW  bar,p1,0,1085
GR.BITMAP.DRAW  low,p11,30,873
GR.BITMAP.DRAW  upw,p10,620,50
! draw & hide folders & programs icons & their titles
k=0
FOR i=1 TO 11
 FOR j=1 TO 3
  k=k+1
  ! GR.rect rec,80+(j-1)*270, (i-1)*100, 190+(j-1)*270, (i-1)*100+80
  GR.BITMAP.DRAW  folders[k],p3,100+(j-1)*270+10, (i-1)*100
  GR.BITMAP.DRAW  progs[k],p4,100+(j-1)*270+10, (i-1)*100
  GR.BITMAP.DRAW  images[k],p5,100+(j-1)*270+10, (i-1)*100
  GR.BITMAP.DRAW  basic[k],p6,100+(j-1)*270+10, (i-1)*100
  GR.TEXT.DRAW title[k],(j-1)*270+120 +10, (i-1)*100+60," "
  GR.TEXT.SIZE 22
  GR.TEXT.DRAW details[k],(j-1)*270+120 +10, (i-1)*100+85," "
  GR.TEXT.SIZE 25
  GR.HIDE folders[k]
  GR.HIDE progs[k]
  GR.HIDE images[k]
  GR.HIDE title[k]
  GR.HIDE details[k]
  GR.HIDE basic[k]
 NEXT j
NEXT i

! lower menu
GR.BITMAP.DRAW  menu,p2, 0,580
GR.HIDE menu

! upper menu & choices
GR.COLOR 255,0,0,0,1
GR.TEXT.ALIGN 1
GR.RECT upmenu,540,50,800,770
GR.COLOR 255,255,255,255,1
GR.TEXT.SIZE 36
GR.TEXT.DRAW txmenu[1], 550,90," Get selected"
GR.TEXT.DRAW txmenu[2] ,550,150," Deselect All"
GR.TEXT.DRAW txmenu[3] ,550,210," Deselect Last"
GR.TEXT.DRAW txmenu[4] ,550,270," Download List"
GR.TEXT.DRAW txmenu[5] ,550,330," Preview last"
GR.TEXT.DRAW txmenu[6] ,550,390," Upload Files.."
GR.TEXT.DRAW txmenu[7] ,550,450," AutoPreview"
GR.TEXT.DRAW txmenu[8] ,550,510," Show Details"
GR.TEXT.DRAW txmenu[9] ,550,570," Basic! Forum"
GR.TEXT.DRAW txmenu[10] ,550,630," Change skin"
GR.TEXT.DRAW txmenu[11] ,550,690," About"
GR.TEXT.DRAW txmenu[12],550,750," Exit"
GR.LINE lin[1] ,550,105,800,105
GR.LINE lin[2] ,550,165,800,165
GR.LINE lin[3] ,550,225,800,225
GR.LINE lin[4] ,550,285,800,285
GR.LINE lin[5] ,550,345,800,345
GR.LINE lin[6] ,550,405,800,405
GR.LINE lin[7] ,550,465,800,465
GR.LINE lin[8] ,550,525,800,525
GR.LINE lin[9] ,550,585,800,585
GR.LINE lin[10],550,645,800,645
GR.LINE lin[11],550,705,800,705
FOR i=1 TO 11
 GR.HIDE txmenu[i]
 GR.HIDE lin[i]
NEXT i
GR.HIDE txmenu[12]
GR.HIDE upmenu

! auropreview show details switch
GR.COLOR 255,255,0,0,1
GR.RECT auproff, 777,425,800,450
GR.RECT detoff,  777,485,800,510
GR.COLOR 255,0,255,0,1
GR.RECT aupron, 777,425,800,450
GR.RECT deton,  777,485,800,510
GR.HIDE aupron
GR.HIDE auproff
GR.HIDE deton
GR.HIDE detoff

! upper menu icon
GR.COLOR 255,255,255,255,1
GR.RECT rct,770,5,780,15
GR.RECT rct,770,20,780,30
GR.RECT rct,770,35,780,45
! GR.rect r,750,0,800,50
GR.TEXT.SIZE 25

! initilize
path$= "./"
morepages=0
showedpages=0
firsttime=0
firstkeyb=0
welcome=0
! showpreview=0
! showdetails=0
! skin=0

! bar log : paths & messages
GR.COLOR 255,0,155,0,1
GR.TEXT.DRAW msg,155,1180," "
GR.COLOR 255,255,0,0,1
GR.TEXT.DRAW cur_path,155,1145,"./"
GR.COLOR 255,255,255,55,0
GOSUB updatepath
upmenuon=0
menuon=0

! preview black box, text lines
GR.COLOR 255,0,0,0,1
GR.RECT preview, 0,160,800,1040
GR.HIDE preview
GR.TEXT.SIZE 30
GR.COLOR 255,0,255,255,1
FOR i=1 TO 25
 GR.TEXT.DRAW prvtx[i],5,190+(i-1)*35," "
 GR.HIDE prvtx[i]
NEXT i

GR.SET.STROKE 4
! yes no button
GR.COLOR 255,0,0,0,1
GR.RECT ok[1],250,200,550,400
GR.COLOR 255,255,255,0,1
GR.LINE ok[2],255,260,546,260
GR.COLOR 255,255,255,0,0
GR.RECT ok[3] ,254,204,546,396
GR.TEXT.ALIGN 1
GR.TEXT.SIZE 35
GR.COLOR 255,255,255,0,1
GR.TEXT.DRAW ok[4] ,280,245,"okQWER TYPE" % title
GR.COLOR 255,0,255,255,1
GR.RECT ok[5] ,280,300,380,350
GR.RECT ok[6] ,420,300,520,350
GR.COLOR 255, 0,65,0,1
GR.TEXT.SIZE 40
GR.TEXT.BOLD 1
GR.TEXT.DRAW ok[7] ,295,340,"YES"
GR.TEXT.DRAW ok[8] ,445,340,"NO"
GR.COLOR 255,0,0,0,1
GR.TEXT.ALIGN 2
GR.TEXT.BOLD 0
FOR i=1 TO 8
 GR.HIDE ok[i]
NEXT

GR.TEXT.ALIGN 2
GR.COLOR 255,255,255,55,0
GR.TEXT.SIZE 25

GOSUB showskin

GR.RENDER


! main program
start: % main loop

GOSUB wait % for a touch
IF welcome=0 THEN
 welcome=1
 GR.HIDE upw
 GR.HIDE low
 GR.BITMAP.DELETE p10
 GR.BITMAP.DELETE p11
 GR.RENDER
ENDIF
! if icon selected, execute command
icon_selected=0
IF menuon=0 & upmenuon=0 THEN
 FOR i=1 TO 11
  FOR j=1 TO 3
   icon_selected=icon_selected+1
   IF x>(80+(j-1)*270)*sx & y>((i-1)*100)*sy & x<((j-1)*270+190)*sx & y<((i-1)*100+80)*sy & icon_on[icon_selected]=1 THEN
    derectory=0
    GOSUB checkicon
    IF derectory=1 THEN % directory choosed
     GOSUB chgdir
    ELSE % file choosed
     GOSUB file_selected
    ENDIF
    finished=1
    F_N.BREAK
   ENDIF
  NEXT j
  IF finished=1 THEN f_n.break
 NEXT i
 finished=0
ENDIF

! menu/basic icon selected
IF x<150*sx & y>1080*sy & upmenuon=0 THEN
 menuon=!menuon
 IF menuon=1 THEN GR.show menu ELSE GR.hide menu
 ! GR.show sp2
 GR.RENDER
ENDIF

! upmenu
IF x>750*sx & y<50*sy & menuon=0 THEN
 upmenuon=!upmenuon
 GOSUB show_upmenu
 GOSUB up_menu
ENDIF

! more pages selected
IF upmenuon=0 & x>200*sx  & y>1080*sy & menuon=0 & pagesleft>0 THEN
 GOSUB morepages
ENDIF

! applications
IF x<=300*sx & y>=580*sy & y<670*sy & menuon=1 & upmenuon=0 THEN
 GR.HIDE menu
 GR.RENDER
 menuon=0
 path$= "./applications/"
 neupath$= "applications/"
 STACK.push paths,"applications/"
 pagesleft=0
 GOSUB chgdir
ENDIF

! games
IF x<=300*sx & y>=670*sy & y<750*sy & menuon=1 & upmenuon=0 THEN
 GR.HIDE menu
 GR.RENDER
 menuon=0
 path$= "./games/"
 neupath$= ".games/"
 STACK.push paths, "games/"
 pagesleft=0
 GOSUB chgdir
ENDIF

! utilities
IF x<=300*sx & y>=750*sy & y<830*sy & menuon=1 & upmenuon=0 THEN
 GR.HIDE menu
 GR.RENDER
 menuon=0
 path$= "./utilities/"
 neupath$= ".utilities/"
 STACK.push paths, "utilities/"
 pagesleft=0
 GOSUB chgdir
ENDIF

! html
IF x<=300*sx & y>=830*sy & y<910*sy & menuon=1 & upmenuon=0 THEN
 GR.HIDE menu
 GR.RENDER
 menuon=0
 path$= "./html/"
 neupath$= ".html/"
 STACK.push paths, "html/"
 pagesleft=0
 GOSUB chgdir
ENDIF

! tools
IF x<=300*sx & y>=910*sy & y<990*sy & menuon=1 & upmenuon=0 THEN
 GR.HIDE menu
 GR.RENDER
 menuon=0
 path$= "./tools/"
 neupath$= ".tools/"
 STACK.push paths, "tools/"
 pagesleft=0
 GOSUB chgdir
ENDIF

! other
IF x<=300*sx & y>=990*sy & y<1080*sy & menuon=1 & upmenuon=0 THEN
 GR.HIDE menu
 GR.RENDER
 menuon=0
 path$= "./other/"
 neupath$= ".other/"
 STACK.push paths, "other/"
 pagesleft=0
 GOSUB chgdir
ENDIF

GOTO start % main loop



! ***************
! * SUBROUTINES *
! ***************

! ***change dir/show new dir***
chgdir:
err$="#3..ftp chdir error"
GR.MODIFY msg,"text","Loading, please wait.."
GR.RENDER
jj=0
FOR i=1 TO 33
 icon_on[i]=0
NEXT i
icon_selected=0

FTP.CD path$
GOSUB updatepath
FTP.DIR archives_list
IF path$= "./" THEN % hide something
 LIST.GET archives_list,3,name$
 IF name$=".ftpquota" THEN List.remove archives_list,3
 LIST.GET archives_list,3,name$
 IF name$=".htaccess" THEN List.remove archives_list,3
ENDIF
IF showdetails=1 THEN gosub searchURL
LIST.SIZE archives_list,asize

GOSUB check_num_of_pages

IF asize>33 THEN % add root,up choice
 FOR i=2 TO  numpages
  LIST.INSERT archives_list,(i-1)*33+1,"..(d)"
  LIST.INSERT archives_list,(i-1)*33+1,".(d)"
 NEXT i
ENDIF

LIST.SIZE archives_list,asize

GOSUB check_num_of_pages

pagesleft=numpages

! this is called also as a subroutine ;)
! when more than 33 items to show
morepages:
IF pagesleft=0 THEN return
jj=jj+1
pagesleft=pagesleft-1
IF  pagesleft>0 THEN
 GR.MODIFY msg,"text","More Files: Touch here to go"
ELSE
 GR.MODIFY msg,"text","Waiting:"
ENDIF
GR.RENDER
GOSUB hid_icons_titles
ii=0
FOR i=(jj-1)*33+1 TO jj*33
 ii=ii+1
 IF i<=asize THEN
  LIST.GET archives_list,i,name$
  IF showdetails=1 & RIGHT$(name$,3)<> "(d)" THEN

   LIST.SEARCH fnam,name$,index

   IF index>0 THEN list.get fsiz,index, det$

  ENDIF
  IF LEN(name$)>22 THEN name$="~"+RIGHT$(name$,20)
  IF RIGHT$(name$,3)= "(d)" THEN
   name$=LEFT$(name$,LEN(name$)-3)
   IF name$="." THEN name$= "/"
   IF name$=".." THEN name$="Up"
   GR.SHOW folders[ii]
  ELSE % i have a file
   IF showdetails=1 THEN
    GR.SHOW details[ii]
    GR.MODIFY details[ii],"text",det$
   ENDIF
   fileTemp$=RIGHT$(LOWER$(name$),4)
   IF fileTemp$=".png" | fileTemp$=".jpg" | fileTemp$="jpeg" THEN
    GR.SHOW images[ii]
   ELSEIF  fileTemp$=".bas" then
    GR.SHOW basic[ii]
   ELSE
    GR.SHOW progs[ii]
   ENDIF
  ENDIF
  GR.SHOW title[ii]
  icon_on[ii]=1
  GR.MODIFY title[ii],"text",name$
 ENDIF
NEXT i
GR.RENDER
IF showpreview=1 THEN gosub imagepreview
RETURN
!*end of chgdir*

!*** checks an icon pressed***
checkicon:
err$="#4..icon checking"
cur_element=ROUND((numpages-pagesleft-1)*33)+icon_selected
LIST.GET archives_list,cur_element,name$
a=0
a=IS_IN("(d)",name$)
IF a=0 THEN derectory=0 % we have a file
IF a<>0 THEN  % we have a dir
 IF name$= ".(d)" THEN
  path$= "./"
  GOSUB updatepath
 ELSEIF name$= "..(d)"  then
  IF path$<>"./" THEN
   STACK.IsEmpty paths, n
   IF n=0 THEN
    STACK.pop paths, apath$
    ! up
    path$=LEFT$(path$,LEN(path$)-LEN(apath$))
    GOSUB updatepath
   ENDIF
  ENDIF
 ELSE
  neupath$ = LEFT$(name$,LEN(name$)-3)+"/"
  path$=path$+neupath$
  STACK.push paths,neupath$
  GOSUB updatepath
 ENDIF
 derectory=1
ENDIF % for directory
GOSUB updatepath
RETURN
! *end of checkicon*

! ***treats upper menu when opened***
up_menu:
err$="#5..upper menu opening"

rewait:

GOSUB wait

IF x>750*sx & y<50*sy THEN
 ! close menu
 GOSUB hide_upmenu
 upmenuon=!upmenuon
 RETURN
ENDIF

IF x>550*sx & y>50*sy & y<=105*sy THEN
 ! 1st option selected / get selected
 err$="#6..file downloading"
 POPUP "downloading "+REPLACE$(STR$(count_dnld),".0","")+" files",0,0,4
 GOSUB hide_upmenu
 upmenuon=!upmenuon
 DO
  STACK.IsEmpty downloads, n
  IF n=0 THEN
   STACK.pop downloads,file$
   GR.MODIFY msg,"text","Downloading: "+RIGHT$(file$,40)
   GR.RENDER
   fileTemp$=LOWER$(RIGHT$(file$,4))
   FOR l=1 TO LEN(file$)
    IF MID$(file$,l,1)="/" THEN index=l
   NEXT l
   fname$=RIGHT$(file$,LEN(file$)-index)
   ! get rid of "." now
   file$=RIGHT$(file$,LEN(file$)-1)
   IF fileTemp$=".bas" THEN
    FTP.GET file$,"../source/"+fname$
   ELSEIF fileTemp$=".apk" then
    FTP.GET file$,"../"+fname$
   ELSE
    FTP.GET file$,fname$
   ENDIF
  ENDIF
 UNTIL n=1
 GR.MODIFY msg,"text","Downloaded: "+ REPLACE$(STR$(count_dnld),".0","")+" files."
 GR.RENDER
 POPUP "Finished!",0,0,0
 TONE 800,800
 count_dnld=0
 STACK.clear downloads
 RETURN
ENDIF % for 1st option


IF x>550*sx & y>105*sy & y<=165*sy THEN
 ! 2nd option selected / Deselect all
 err$="#7..file deselecting"
 STACK.clear downloads
 POPUP "Download list Empty!",0,0,0
 count_dnld=0
 PAUSE 1500
 GOSUB hide_upmenu
 upmenuon=!upmenuon
 RETURN
ENDIF % for 2nd option

IF x>550*sx & y>165*sy & y<=225*sy THEN
 ! 3d option selected / Deselect last
 err$="#7..file deselecting"
 STACK.IsEmpty downloads, n
 IF n=0 THEN
  STACK.pop downloads,temp$
  POPUP "Last file deselected!",0,0,0
  count_dnld=count_dnld-1
 ENDIF
 PAUSE 1500
 GOSUB hide_upmenu
 upmenuon=!upmenuon
 RETURN
ENDIF% for 3rd option

IF x>550*sx & y>225*sy & y<=285*sy THEN
 ! 4th option selected / Download list
 err$="#7..list"
 GOSUB hide_upmenu
 upmenuon=!upmenuon
 GR.MODIFY msg,"text","Showing download list.."
 FOR i=1 TO count_dnld
  STACK.pop downloads,temp$
  LIST.ADD dnldlist,temp$
 NEXT i
 FOR i=count_dnld TO 1 STEP -1
  LIST.GET dnldlist,i,temp$
  STACK.push downloads,temp$
 NEXT i
 GR.SHOW preview
 FOR i=1 TO 25
  GR.MODIFY prvtx[i],"text",""
  GR.SHOW prvtx[i]
 NEXT i
 ! print list/wait/remove list elements
 k=0
 FOR i=1 TO count_dnld
  IF MOD(i,25)=1 & i>2 THEN
   GR.RENDER
   k=0
   GOSUB wait
  ENDIF
  k=k+1
  LIST.GET dnldlist,i,temp$
  IF LEN(temp$)>45 THEN temp$="~"+RIGHT$(temp$,45)
  GR.MODIFY prvtx[k],"text",temp$
 NEXT i
 FOR l=k+1 TO 25
  GR.HIDE prvtx[l]
 NEXT
 extramsg$=""
 IF  pagesleft>0 THEN  extramsg$=" More Files: touch here to go!"
 GR.MODIFY msg,"text","End of list. Touch screen to quit."+ extramsg$
 GR.RENDER
 GOSUB wait
 GR.HIDE preview
 FOR i=1 TO 25
  GR.HIDE prvtx[i]
 NEXT i
 GR.MODIFY msg,"text","Waiting.."+ extramsg$
 GR.RENDER
 FOR i=1 TO count_dnld
  LIST.REMOVE dnldlist,1
 NEXT i
 RETURN
ENDIF % for 4th option


IF x>550*sx & y>285*sy & y<=345*sy THEN
 ! 5th option selected / Preview last
 err$="#8..preview"
 STACK.IsEmpty downloads, n
 IF n=1 THEN
  POPUP "Nothing to preview",0,0,0
  GOSUB hide_upmenu
  upmenuon=!upmenuon
 ELSE
  GOSUB hide_upmenu
  upmenuon=!upmenuon
  STACK.peek downloads, file$
  fileTemp$=LOWER$(file$)
  fileTemp$=RIGHT$(filetemp$,4)

  IF fileTemp$=".bas" | fileTemp$=".txt" | fileTemp$=".htm" | fileTemp$="html" | fileTemp$=".xml" THEN
   ! **************
   ! *text  viewer*
   ! **************
   GR.TEXT.SIZE 30
   finished=0
   STACK.peek downloads,file$
   FOR l=1 TO LEN(file$)
    IF MID$(file$,l,1)="/" THEN index=l
   NEXT l
   fname$=RIGHT$(file$,LEN(file$)-index)
   GR.MODIFY msg,"text","Loading '"+fname$+"'"
   GR.RENDER
   file$=RIGHT$(file$,LEN(file$)-1)
   FTP.GET file$, fname$
   FILE.SIZE fsize,fname$
   GR.MODIFY msg,"text","Showing file: '"+fname$+"', Size is:"+REPLACE$(STR$(fsize),".0","")
   GR.SHOW preview
   GR.RENDER
   FOR i=1 TO 25
    GR.MODIFY prvtx[i],"text",""
    GR.SHOW prvtx[i]
   NEXT i

   j=1
   k=0
   line$=""
   TEXT.OPEN r, Ftable, fname$
   WHILE line$<>"EOF" & finished=0
    TEXT.READLN ftable, line$
    IF LEN(line$)<41 THEN
     toprint$=line$
     k=k+1
     GR.MODIFY prvtx[k],"text",toprint$
     GR.RENDER
     GOSUB check_k
    ENDIF
    IF LEN(line$)>=41 THEN
     a$=LEFT$(line$,40)
     found=1
     j=1
     FOR i=40 TO LEN(line$)
      a$=a$+MID$(line$,i-1,1)
      b$=a$+MID$(line$,i,1)
      GR.TEXT.WIDTH w1,a$
      GR.TEXT.WIDTH w2,b$
      IF w1<=800*j & w2>800*j THEN
       j=j+1
       toprint$=MID$(line$,found,i-found)
       k=k+1
       GR.MODIFY prvtx[k],"text",toprint$
       GR.RENDER
       found=i
       GOSUB check_k
      ENDIF
     NEXT i
     IF found=1 THEN
      toprint$=line$
      k=k+1
      GR.MODIFY prvtx[k],"text",toprint$
      GR.RENDER
      GOSUB check_k
     ELSE
      k=k+1
      toprint$=MID$(line$,found,LEN(line$)-found+1)
      GR.MODIFY prvtx[k],"text",toprint$
      GR.RENDER
      GOSUB check_k
     ENDIF
    ENDIF
   REPEAT

   FILE.DELETE f,"../"+name$
   GR.HIDE preview
   FOR ll=1 TO 25
    GR.MODIFY prvtx[ll],"text",""
    GR.HIDE prvtx[ll]
   NEXT ll

   GR.MODIFY msg,"text","Viewing finished: Waiting.."
   GR.RENDER

  ENDIF % text viewer

  IF fileTemp$="jpeg" | fileTemp$=".jpg" | fileTemp$=".png" THEN
   ! **************
   ! *image viewer*
   ! **************
   STACK.peek downloads,file$
   FOR l=1 TO LEN(file$)
    IF MID$(file$,l,1)="/" THEN index=l
   NEXT l
   fname$=RIGHT$(file$,LEN(file$)-index)
   GR.MODIFY msg,"text","Loading '"+fname$+"'"
   ! 	GR.show preview
   GR.RENDER
   file$=RIGHT$(file$,LEN(file$)-1)
   ! now i have the full name in fname$

   GOSUB imageviewer

  ENDIF % for gfx viewer
 ENDIF % n=1
 RETURN
ENDIF % for 5th option

! 6th option selected / upload
! Upload files
IF x>550*sx & y>345*sy & y<=405*sy THEN  % if start
 err$="#9..Upload files"
 GR.MODIFY msg,"text","Select .bas files to upload, then touch bar"
 PAUSE 500
 GOSUB hide_upmenu
 upmenuon=!upmenuon
 GR.SHOW preview
 FOR i=1 TO 25
  GR.SHOW prvtx[i]
  GR.MODIFY prvtx[i],"text",""
 NEXT i
 GR.RENDER

 mypath$="../source/"
 uploads=Suploads
 dyrlist=dyrlistS
 GOSUB fileselector
 !  gosub wait
 FOR i=1 TO 25
  GR.MODIFY prvtx[i],"text",""
 NEXT i
 GR.MODIFY msg,"text","Select data files to upload, then touch bar"
 GR.RENDER
 mypath$=""
 uploads=Duploads
 dyrlist=dyrlistD
 GOSUB fileselector
 ! gosub wait
 GR.MODIFY msg,"text","Files to upload selected. Waiting.."
 GR.HIDE preview
 FOR i=1 TO 25
  GR.MODIFY prvtx[i],"text",""
  GR.HIDE prvtx[i]
 NEXT i
 GR.RENDER

 yesnotxt$="Upload files?"
 GOSUB yesnobutton

 IF yes=1 THEN
  FOR i=1 TO 25
   GR.SHOW prvtx[i]
  NEXT i
  GR.SHOW preview
  ! Select Directory
  GR.MODIFY msg,"text","Please select one of the above categories.."
  GR.MODIFY prvtx[1],"text","    >SELECT CATEGORY BELOW<"
  GR.MODIFY prvtx[3],"text","       [APPLICATIONS]"
  GR.MODIFY prvtx[4],"text","          [GAMES]"
  GR.MODIFY prvtx[5],"text","         [UTILITY]"
  GR.MODIFY prvtx[6],"text","          [TOOLS]"
  GR.MODIFY prvtx[7],"text","          [HTML]"
  GR.MODIFY prvtx[8],"text","          [OTHER]"
  GR.MODIFY prvtx[9],"text","        [BETA-TEST]"
  GR.RENDER
  found=0
  DO
   GOSUB wait
   FOR i1=3 TO 9 %
    IF y>=(195+(i1-2)*35)*sy &  y<(195+(i1-1)*35)*sy THEN found=i1-2
   NEXT i1
  UNTIL found<>0

  IF found=1 THEN target$="applications"
  IF found=2 THEN target$="games"
  IF found=3 THEN target$="utility"
  IF found=4 THEN target$="tools"
  IF found=5 THEN target$="html"
  IF found=6 THEN target$="other"
  IF found=7 THEN target$="beta-test"
  GR.MODIFY msg,"text","Selected upload category: "+target$
  GR.RENDER

  GOSUB keyboard

  ! now upload everything to writed$ directory
  GR.MODIFY msg,"text", "Please wait, uploading files.."
  GR.RENDER
  ! check if dir exists.
  FTP.CD "./"+target$
  FTP.DIR checklist
  d_exists=0
  LIST.SIZE checklist,cl
  FOR i=1 TO cl
   LIST.GET checklist,i,checkname$

   IF RIGHT$(checkname$,3)= "(d)" THEN
    checkname$=LEFT$(checkname$,LEN(checkname$)-3)
    IF checkname$=writed$ THEN
     writed$=checkname$
     d_exists=1
     F_N.BREAK
    ENDIF
   ENDIF
  NEXT i

  ! check finished, go on
  LIST.SIZE Suploads,ll1
  LIST.SIZE Duploads,ll2
  IF d_exists=0 THEN ftp.mkdir writed$
  FOR i=1 TO ll1
   LIST.GET Suploads,i,myfile$
   FTP.PUT  "../source/"+myfile$, writed$+"/"+myfile$

  NEXT i
  FOR i=1 TO ll2
   LIST.GET Duploads,i,myfile$
   FTP.PUT  myfile$, writed$+"/"+myfile$

  NEXT i
  TONE 800,400
  GR.MODIFY msg,"text","Files Uploaded to "+target$+"/"+writed$+". Upload list is empty. "
  GR.RENDER
 ELSE
  ! give a messagge
  GR.MODIFY msg,"text","Upload cancelled, Upload list is empty. "+target$
  GR.RENDER
 ENDIF % for yes/no, uploading files

 FOR i=1 TO 25
  GR.MODIFY prvtx[i],"text",""
  GR.HIDE prvtx[i]
 NEXT i
 GR.RENDER
 LIST.SIZE Suploads,ll1
 LIST.SIZE Duploads,ll2
 ! list.size Duploads,ll2
 FOR i=1 TO ll1
  LIST.REMOVE Suploads,1
 NEXT i
 FOR i=1 TO ll2
  LIST.REMOVE Duploads,1
 NEXT i

 LIST.SIZE dyrlistD,ll1
 LIST.SIZE dyrlistS,ll2
 ! list.size Duploads,ll2
 FOR i=1 TO ll1
  LIST.REMOVE dyrlistD,1
 NEXT i
 FOR i=1 TO ll2
  LIST.REMOVE dyrlistS,1
 NEXT i
 GR.HIDE preview
 FOR i=1 TO 25
  GR.MODIFY prvtx[i],"text",""
  GR.HIDE prvtx[i]
 NEXT i
 GR.RENDER
 RETURN
ENDIF %  % if end for upload files option


! 7th option selected / autopreview
IF x>550*sx & y>405*sy & y<=465*sy THEN
 showpreview=!showpreview
 IF showpreview=1 THEN
  GR.SHOW aupron
  GR.HIDE auproff
 ELSE
  GR.SHOW auproff
  GR.HIDE aupron
 ENDIF
 GR.RENDER
 PAUSE 1000
 GOSUB hide_upmenu
 upmenuon=!upmenuon
 RETURN
ENDIF

! 8th option selected / details
IF x>550*sx & y>465*sy & y<=525*sy THEN
 showdetails=!showdetails
 IF showdetails=1 THEN
  GR.SHOW deton
  GR.HIDE detoff
 ELSE
  GR.SHOW detoff
  GR.HIDE deton
 ENDIF
 GR.RENDER
 PAUSE 1000
 GOSUB hide_upmenu
 upmenuon=!upmenuon
 RETURN
ENDIF

! 9th option selected / Link
IF x>550*sx & y>525*sy & y<=585*sy THEN
 GOSUB hide_upmenu
 upmenuon=!upmenuon
 BROWSE "http://rfobasic.freeforums.org/"
 RETURN
ENDIF

IF x>550*sx & y>585*sy & y<=645*sy THEN
 ! 10th option selected | change skin
 err$="#9..skin"
 IF skin=0 THEN % no skin
  GR.SHOW skin1
  GR.HIDE skin2
  GR.HIDE skin3
  skin=1
 ELSEIF skin=1 then
  GR.HIDE skin1
  GR.SHOW skin2
  GR.HIDE skin3
  skin=2
 ELSEIF skin=2 then
  GR.HIDE skin1
  GR.HIDE skin2
  GR.SHOW skin3
  skin=3
 ELSE % skin 3
  GR.HIDE skin1
  GR.HIDE skin2
  GR.HIDE skin3
  skin=0
 ENDIF
 GR.RENDER
 GOSUB hide_upmenu
 upmenuon=!upmenuon
 RETURN
ENDIF

IF x>550*sx & y>645*sy & y<=705*sy THEN
 ! 11th option selected | About
 err$="#9..about"
 GR.MODIFY msg,"text","About.. / Touch screen to quit!"
 PAUSE 500
 GOSUB hide_upmenu
 upmenuon=!upmenuon
 GR.SHOW preview
 FOR i=1 TO 14
  GR.SHOW prvtx[i]
 NEXT i
 GR.MODIFY prvtx[1],"text","         >>_Rfo Basic! FTP Store_<<"
 GR.MODIFY prvtx[2],"text","         >>_version 1.2 r1_<<"
 GR.MODIFY prvtx[3],"text","         >>_July 30, 2012_<<"
 GR.MODIFY prvtx[4],"text","         >>_by Antonis [tony_gr]_<<"
 GR.MODIFY prvtx[5],"text",""
 GR.MODIFY prvtx[6],"text","Join the rfobasic.freeforums.org for questions or"
 GR.MODIFY prvtx[7],"text","comments about this program."
 GR.MODIFY prvtx[8],"text",""
 GR.MODIFY prvtx[9],"text","New to this version:"
 GR.MODIFY prvtx[10],"text"," Details option, for ftp files"
 GR.MODIFY prvtx[11],"text"," Preferences are now saved upon exit"
 GR.MODIFY prvtx[12],"text"," Added icon for .bas files"
 GR.MODIFY prvtx[13],"text"," Added a new colored skin"
 GR.MODIFY prvtx[14],"text"," fixed one bug"

 GR.RENDER
 GOSUB wait
 GR.MODIFY msg,"text","Waiting.."
 FOR i=1 TO 14
  GR.HIDE prvtx[i]
 NEXT i
 GR.HIDE preview
 GR.RENDER
 RETURN
ENDIF

IF x>550*sx & y>705*sy & y<=765*sy THEN
 ! 12th option selected / exit
 POPUP "Quitting",0,0,0
 err$="#10..session closing"
 TEXT.OPEN w,filn,"rfo_ftp_store.data"
 TEXT.WRITELN filn,"o#6" % this version includes 6 options
 TEXT.WRITELN filn,"c01#"+REPLACE$(STR$(showpreview),".0","") % autopreview
 TEXT.WRITELN filn,"c02#"+REPLACE$(STR$(showdetails),".0","") % show detailes
 TEXT.WRITELN filn,"c03#"+REPLACE$(STR$(skin),".0","") % skin
 TEXT.WRITELN filn,"c04#0"
 TEXT.WRITELN filn,"c05#0"
 TEXT.WRITELN filn,"c06#0"
 TEXT.CLOSE filn
 FTP.CLOSE
 GOSUB hide_upmenu
 upmenuon=!upmenuon
 PRINT "Bye!"
 END

 RETURN
ENDIF  % for 12th option

GOTO rewait
! return
! *end of up_menu*

! ***hide_icons_titles***
hid_icons_titles:
err$="#11..icon"
FOR m=1 TO 33
 GR.HIDE folders[m]
 GR.HIDE title[m]
 GR.HIDE progs[m]
 GR.HIDE images[m]
 GR.HIDE basic[m]
 GR.HIDE details[m]
 icon_on[m]=0
NEXT m
GR.RENDER
RETURN
! *end of hid_icons_titles*

! ***file_selected***
file_selected:
err$="#12..file_selecting"
count_dnld=count_dnld+1
GR.MODIFY msg,"text","Selected: "+LEFT$(name$,40)
STACK.push downloads,path$+name$
GR.RENDER
RETURN
! * end offile_selected*

! ***check_num_of_pages of files+dirs***
check_num_of_pages:
err$="#13..page_selecting"
! file_selected
IF asize >33 THEN
 a=asize
 rest=MOD(a,33)
 numpages=ROUND((a-rest)/33)+1
 morepages=1
ELSE
 numpages=1
ENDIF
RETURN
! *end of check_num_of_pages*

! ***show_upmenu***
show_upmenu:
err$="#14..upmenu_opening"
FOR i=1 TO 11
 GR.SHOW txmenu[i]
 GR.SHOW lin[i]
NEXT i
GR.SHOW txmenu[12]
IF showpreview=1 THEN gr.show aupron ELSE gr.show auproff
IF showdetails=1 THEN gr.show deton ELSE gr.show detoff

GR.SHOW upmenu
GR.RENDER
RETURN
! *end of show_upmenu*

! ***hide_upmenu***
hide_upmenu:
err$="#15..upmenu_closing"
GR.HIDE aupron
GR.HIDE auproff
GR.HIDE deton
GR.HIDE detoff
FOR i=1 TO 11
 GR.HIDE txmenu[i]
 GR.HIDE lin[i]
NEXT i
GR.HIDE txmenu[12]
GR.HIDE upmenu
GR.RENDER
RETURN
! *end of show_upmenu*

! ***updatepath***
updatepath:
err$="#15..path updating"
GR.MODIFY cur_path,"text","path: "+RIGHT$(path$,45)
GR.RENDER
RETURN
! *end of updatepath*

! *** wait for a touch***
wait:

DO
 GR.TOUCH touched,x,y
UNTIL touched
DO
 GR.TOUCH touched,x,y
UNTIL !touched

RETURN
! *end of wait*

! ***checks k***
! called by text viewer
check_k:
IF k=25 THEN
 k=0
 GR.MODIFY msg,"text","Touch bar to quit, elsewhere to continue."
 GR.RENDER
 GOSUB wait
 IF y>1080*sy THEN
  TEXT.CLOSE ftable
  finished=1
  RETURN
 ENDIF
 FOR l=1 TO 25
  GR.MODIFY prvtx[l],"text",""
  GR.RENDER
 NEXT l
ENDIF
GR.RENDER
RETURN

! sub fileselector
fileselector:
! get listing & len of source dir

FILE.DIR mypath$, FileArray$[]
ARRAY.LENGTH ll,FileArray$[]
! copy to dyrlist without dirs

FOR i=1 TO ll
 IF RIGHT$(FileArray$[i],3)<> "(d)" THEN list.add dyrlist, FileArray$[i]
NEXT i
ARRAY.DELETE FileArray$[]
! ll is the number of files in the source directory
LIST.SIZE dyrlist,ll

fcount=0 % counter for current page 1..25
fpage=0 % page number of showed files
nfpages=((ll-MOD(ll,25))/25)+1

! show pages and select
FOR i=1 TO ll % 8
 fcount=fcount+1
 LIST.GET dyrlist,i, name$
 GR.MODIFY prvtx[fcount],"text",name$
 ! check fcount
 IF fcount=25 THEN fcount=0
 IF i=ll | fcount=0 THEN % 7 i am in last page, or end of current page
  IF fcount<>0 THEN
   FOR i4=fcount+1 TO 25
    GR.MODIFY prvtx[i4],"text",""
   NEXT i4
  ENDIF
  GR.RENDER
  DO % 6
   GOSUB wait
   ! screen touched, find what
   FOR i1=1 TO 25 % 5
    IF y>=(195+(i1-2)*35)*sy &  y<(195+(i1-1)*35)*sy & i1+fpage*25<=ll THEN % 4 found
     element=i1
     ! get it
     LIST.GET dyrlist,i1+fpage*25,name$ % only if i1+fpage*25<=ll
     ! see if already in uploadlist +
     IF LEFT$(name$,1)<>"+" THEN % 3 not in upload list,  add it
      LIST.ADD uploads,name$ % add it
      LIST.REPLACE dyrlist,i1+fpage*25,"+" + name$
      GR.MODIFY prvtx[i1],"text", "+" + name$ % update screen
      GR.RENDER

     ELSE % already in upload list, remove it
      ! find it and remove it, update screen
      name$=MID$(name$,2,LEN(name$))
      LIST.SEARCH uploads, name$, i2
      LIST.REMOVE uploads,i2
      LIST.REPLACE dyrlist,i1+fpage*25,name$
      GR.MODIFY prvtx[i1],"text",name$ % update screen
      GR.RENDER
     ENDIF % 3 for adding or removing element from upload list
    ENDIF % 4 for choosing from screen
   NEXT i1 % 5
  UNTIL y>1080*sy  % 6 finished, with source
  IF fcount=0 THEN fpage=fpage+1
  IF fpage=nfpages THEN f_n.break
 ENDIF % 7
NEXT i % 8
RETURN

! sub yes/no button
yesnobutton:
yes=-1
FOR i=1 TO 9
 GR.SHOW ok[i]
NEXT i
GR.MODIFY ok[4],"text",yesnotxt$
GR.RENDER
DO
 GOSUB wait
 IF x>280*sx & y>300*sy & x<380*sy & y<350*sy THEN yes=1
 IF x>420*sx & y>300*sy & x<520*sy & y<350*sy THEN yes=0
UNTIL yes<>-1
PAUSE 500
FOR i=1 TO 9
 GR.HIDE ok[i]
NEXT i
GR.RENDER
RETURN


! ******************
! * keyboard input *
! ******************
keyboard:


! ****keyboard chars****
GR.TEXT.BOLD 0
GR.TEXT.ALIGN 1
GR.SET.STROKE 0
GR.COLOR 255,0,0,255,1
GR.RENDER
IF firsttime=0 THEN
 str0$="qwertyuiopasdfghjklzxcvbnm"
 str1$="1234567890"
 str2$="@.()+-_~<>"
ENDIF

! ****draw keyboard rectangle****
GR.TEXT.SIZE 40
GR.COLOR 255,0,0,255,1
IF firsttime=0 THEN
 GR.RECT rectObj, 0, 601, 799, 1230
ELSE
 GR.SHOW rectObj
ENDIF
GR.COLOR 255,0,255,0,0
IF firsttime=0 THEN
 GR.TEXT.DRAW txtObj, 10, 1175, "Chars left:"
ELSE
 GR.SHOW txtObj
ENDIF
GR.COLOR 255,255,255,255,0
IF firsttime=0 THEN
 GR.TEXT.DRAW txp, 50, 1225,">Insert your app's directory name<"
ELSE
 GR.SHOW txp
ENDIF
GR.TEXT.SIZE 60

! ****PRINT characters of keyboard****
! the writed$ str contains my text
! letters "qwertyuiopasdfghjklzxcvbnm"
ydisp=0
xdisp=0
j=0
k=1
l=0
FOR i=1 TO 26
 strg$=MID$(str0$,i,1)
 IF  i=11 | i=20  THEN
  xdisp=35*k
  ydisp=70*k
  j=0
  k=k+1
 ENDIF
 j=j+1
 l=l+1
 IF firsttime=0 THEN
  GR.COLOR 255,255,255,255,1
  GR.TEXT.DRAW tobj1[i],(10+(j-1)*81+xdisp),(810+ydisp),strg$
  GR.COLOR 255,255,255,255,0
  GR.RECT robj[l],(3+(j-1)*81+xdisp),(760+ydisp),(68+(j-1)*81+xdisp),(820+ydisp)
 ELSE
  GR.SHOW tobj1[i]
  GR.SHOW robj[i]
 ENDIF
NEXT i

! ****numbers****
FOR j=1 TO LEN(str1$)
 strg$=MID$(str1$,j,1)
 l=l+1
 IF firsttime=0 THEN
  GR.COLOR 255,255,255,255,1
  GR.TEXT.DRAW tobj2[j],(10+(j-1)*81),740,strg$
  GR.COLOR 255,255,255,255,0
  GR.RECT robj[l],(3+(j-1)*81),690,(68+(j-1)*81), 750
 ELSE
  GR.SHOW tobj2[j]
  GR.SHOW robj[l]
 ENDIF
NEXT j

! ****special chars ",.():;-'!?"****
FOR j=1 TO LEN(str2$)
 strg$=MID$(str2$,j,1)
 l=l+1
 IF firsttime=0 THEN
  GR.COLOR 255,255,255,255,1
  GR.TEXT.DRAW tobj3[j],(10+(j-1)*81),1020,strg$
  GR.COLOR 255,255,255,255,0
  GR.RECT robj[l],(3+(j-1)*81),970,(68+(j-1)*81),1030
 ELSE
  GR.SHOW tobj3[j]
  GR.SHOW robj[l]
 ENDIF
NEXT j

! ****capslock, space, delete, RETURN****
GR.TEXT.BOLD 1
GR.COLOR 255,255,255,255,1
IF firsttime=0 THEN
 GR.TEXT.DRAW t1,80,1100,"A"
 GR.TEXT.DRAW t2,250,1100,"   [________] "
 GR.TEXT.DRAW t3,140,1100,"Del"
 GR.TEXT.DRAW t4,600 ,1100,"ENTER"
 ! GR.text.draw tstop,600 ,1190,"STOP"
ELSE
 GR.SHOW t1
 GR.SHOW t2
 GR.SHOW t3
 GR.SHOW t4
ENDIF

GR.TEXT.BOLD 0
GR.COLOR 255,255,255,255,0

IF firsttime=0 THEN
 GR.RECT rob1,70,1040,125,1120
 GR.RECT rob2,240,1040,580,1120
 GR.RECT rob3,135,1040,230,1120
 GR.RECT rob4,590,1040,790,1120
 ! GR.rect rob4,590,1130,755,1210
ELSE
 GR.SHOW rob1
 GR.SHOW rob2
 GR.SHOW rob3
 GR.SHOW rob4
ENDIF

GR.COLOR 255,255,255,255,1

GR.RENDER
! ---PRINTing characters finished---

! ****wait for user input****

writed$=""
touchme:
DO % check FOR max lenght
 touched = -1
 GR.TOUCH touched, x, y
 PAUSE 50
UNTIL touched>0
IF  LEN(writed$)=30 THEN
 POPUP "maximum length, press ENTER",0,0,2
 touchsend:
 DO
  touched = -1
  GR.TOUCH touched, x, y
 UNTIL touched>0
 IF  x>590*sx & y>1040*sy & x<755*sx &y<1120*sy THEN
  GOTO enterpressed
 ELSE
  GOTO touchsend
 ENDIF
ENDIF


! ****scan keyboard for numbers****
FOR j=1 TO 10
 IF  x>(3+(j-1)*81)*sx & x<((j-1)*81+68)*sx & y>690*sy & y<750*sy THEN
  TONE 1200,100
  writed$=writed$+MID$(str1$,j,1)
  GOSUB printmystring
  GR.RENDER
 ENDIF
NEXT j

! scan keyboard for letters
ydisp=0
xdisp=0
j=0
k=1
FOR i=1 TO 26
 IF  i=11 | i=20  THEN
  xdisp=35*k
  ydisp=70*k
  j=0
  k=k+1
 ENDIF
 j=j+1
 !GR.rect robj[l],(3+(j-1)*81+xdisp),(760+ydisp),(68+(j-1)*81+xdisp),(820+ydisp)
 IF  x>(3+(j-1)*81+xdisp)*sx & y>(760+ydisp)*sy & x<(68+(j-1)*81+xdisp)*sx &y<(820+ydisp)*sy THEN
  writed$=writed$+MID$(str0$,i,1)
  TONE 1200,100
  GOSUB printmystring
  GR.RENDER
 ENDIF
NEXT i

! ****scan keyboard for CAPS****
IF  x>70*sx & y>1040*sy & x<125*sx &y<1120*sy THEN
 ! CAPS ON OFF invert str0$
 IF  LEFT$(str0$,1)="q" THEN str0$=UPPER$(str0$) ELSE str0$=LOWER$(str0$)
 TONE 1200,100
 ydisp=0
 xdisp=0
 j=0
 k=1
 FOR i=1 TO 26
  strg$=MID$(str0$,i,1)
  IF  i=11 | i=20  THEN
   xdisp=35*k
   ydisp=70*k
   j=0
   k=k+1
  ENDIF
  j=j+1
  GR.HIDE tobj1[i]
  GR.COLOR 255,255,255,255,1
  GR.TEXT.DRAW tobj1[i],(10+(j-1)*81+xdisp),(810+ydisp),strg$
  GR.COLOR 255,255,255,255,0
 NEXT i
 GR.COLOR 255,255,255,255,1
 GR.RENDER
ENDIF

! ****scan keyboard for space****
IF  x>240*sx & y>1040*sy & x<580*sx &y<1120*sy THEN
 ! SPACE
 writed$=writed$+" "
 TONE 1200,100
 GOSUB printmystring
 GR.RENDER
ENDIF

! ****scan keyboard for DEL****
IF  x>135*sx & y>1040*sy & x<230*sx &y<1120*sy THEN
 ! DEL
 IF  LEN(writed$)>=1 THEN
  writed$=LEFT$(writed$,LEN(writed$)-1)
  TONE 1200,100
  GOSUB printmystring
  GR.RENDER
 ENDIF
ENDIF

! ****scan keyboard FOR ENTER****
IF  x>590*sx & y>1040*sy & x<755*sx &y<1120*sy THEN
 IF LEN(writed$)>0 THEN
  GOTO enterpressed
 ELSE
  POPUP "empty text",0,0,0
 ENDIF
ENDIF

! ****scan keyboard for special chars****
FOR j=1 TO 10
 IF  x>(3+(j-1)*81)*sx & x<((j-1)*81+68)*sx & y>970*sy & y<1030*sy THEN
  TONE 1200,100
  writed$=writed$+MID$(str2$,j,1)
  GOSUB printmystring
  GR.RENDER
 ENDIF
NEXT i

! *** loop FOR NEXT char ***
GOTO touchme

! ****subroutines****

! ****prints on screen my text****
PRINTmystring:
GR.TEXT.SIZE 40
GR.COLOR 255,25,255,0,1
GR.HIDE tobj
GR.HIDE tobjj
GR.HIDE tobjjj
l=LEN(writed$)
! split writed$ into 3 pieces
GR.TEXT.WIDTH ww,writed$
IF  ww<=800*sx THEN
 wrtd$=writed$
 wr$=""
 w$=""
ENDIF
IF  ww>800*sx & ww<=1600*sx THEN
 FOR i=2 TO l
  GR.TEXT.WIDTH w1,LEFT$(writed$,i-1)
  GR.TEXT.WIDTH w2,LEFT$(writed$,i)
  IF  w1<=800*sx & w2>800*sx THEN ll=i
 NEXT i
 wrtd$=LEFT$(writed$,ll-1)
 wr$=MID$(writed$,ll,l-ll+1)
 w$=""
ENDIF
IF  ww>1600*sx THEN
 FOR i=2 TO l
  GR.TEXT.WIDTH w1,LEFT$(writed$,i-1)
  GR.TEXT.WIDTH w2,LEFT$(writed$,i)
  IF  w1<=800 & w2>800 THEN ll=i
  IF  w1<=1600 & w2>1600 THEN lll=i
 NEXT i
 wrtd$=LEFT$(writed$,ll-1)
 wr$=MID$(writed$,ll,lll-ll)
 w$=MID$(writed$,lll,l-lll+1)

ENDIF

p1=640
p2=680
p3=670 % not used
GR.TEXT.DRAW tobj,0,p1, wrtd$
IF  wr$<>"" THEN GR.text.draw tobjj,0,p2, wr$
IF  w$<>""  THEN GR.text.draw tobjjj,0,p3, w$
GR.RENDER

counter=30-l
k=ENDS_WITH( ".0", STR$(counter))
counter$=LEFT$(STR$(counter),k-1)
GR.HIDE countobj
GR.TEXT.DRAW countobj, 210, 1175,counter$
GR.TEXT.SIZE 60
GR.RENDER
RETURN

enterpressed:
! clear traces...
TONE 1200,300
GR.HIDE keyp
GR.HIDE tobj
GR.HIDE tobjj
GR.HIDE tobjjj
FOR i=1 TO 10
 GR.HIDE tobj2[i]
 GR.HIDE tobj3[i]
NEXT i
FOR i=1 TO 26
 GR.HIDE tobj1[i]
NEXT i
GR.HIDE t1
GR.HIDE t2
GR.HIDE t3
GR.HIDE t4
GR.HIDE rectobj
FOR i=1 TO 46
 GR.HIDE robj[i]
NEXT i
GR.HIDE rob1
GR.HIDE rob2
GR.HIDE rob3
GR.HIDE rob4
GR.HIDE txtobj
GR.HIDE txp
GR.HIDE countobj
GR.TEXT.ALIGN 2
GR.COLOR 255,255,255,0,1
GR.SET.STROKE 4
GR.TEXT.SIZE 25
GR.TEXT.BOLD 0
firsttime=1
y=-1
GR.RENDER
RETURN

imagepreview:
! if fileTemp$="jpeg" | fileTemp$=".jpg" | fileTemp$=".png" then


found$=""
FOR i=1 TO asize
 LIST.GET archives_list,i,name$
 fileTemp2$=LEFT$(LOWER$(name$),6)
 fileTemp1$=RIGHT$(LOWER$(name$),4)
 IF (fileTemp2$="screen") & (fileTemp1$=".png" | fileTemp1$=".jpg" | fileTemp1$="jpeg") THEN
  found$=name$
  F_N.BREAK
 ENDIF
NEXT i
IF found$="" THEN
 FOR i=1 TO asize
  LIST.GET archives_list,i,name$
  fileTemp1$=RIGHT$(LOWER$(name$),4)
  IF fileTemp1$=".png" | fileTemp1$=".jpg" | fileTemp1$="jpeg" THEN
   found$=name$
   F_N.BREAK
  ENDIF
 NEXT i
ENDIF
IF found$<>"" THEN
 GR.MODIFY msg,"text","Previewing "+name$
 GR.RENDER
 file$=path$+found$
 file$=RIGHT$(file$,LEN(file$)-1)
 fname$=found$
 GOSUB imageviewer % for name$
 GR.MODIFY msg, "text", "Preview finished. Waiting.. "
 GR.RENDER
ENDIF
RETURN

! renders an image
imageviewer:

FTP.GET file$, "../"+fname$
GR.MODIFY msg,"text","Rendering image: '"+fname$+"'"
GR.RENDER
GR.BITMAP.LOAD bmp0, "../"+fname$
GR.BITMAP.SIZE bmp0,w,h
currentscale=1
IF w>=800*sx | h>=800*sy THEN % image >800x800
 IF w>=h THEN currentscale=800/w ELSE currentscale=800/h
ENDIF
GR.BITMAP.SCALE bmp1, bmp0, currentscale*w, currentscale*h

! image DIM are now x->currentscale*ww y->currentscale*hh
! center image
xpos=(800-currentscale*w)/2
ypos=200+(800-currentscale*h)/2
GR.BITMAP.DRAW bmp2,bmp1,xpos,ypos
GR.MODIFY msg,"text","Preview finished! Touch Screen to go on!"
GR.RENDER

! wait to go on
GOSUB wait
GR.BITMAP.DELETE bmp0
GR.BITMAP.DELETE bmp1
GR.HIDE bmp2
GR.HIDE preview
GR.RENDER
FILE.DELETE f,"../"+name$
GR.MODIFY msg,"text","Waiting.."
GR.RENDER
RETURN % imageviewer

! retrieves file sizes and dates
searchURL:
myurl$=REPLACE$(MID$(path$,2,LEN(path$))," ","%20")
GRABURL s$,"http://laughton.com/basic/programs"+myurl$
LIST.CLEAR fsiz
LIST.CLEAR fnam
a=IS_IN("Parent Directory", s$)
s$=MID$(s$,a,LEN(s$))
ARRAY.DELETE weblines$[]
SPLIT weblines$[], s$, "<a href="
ARRAY.LENGTH  lweb,weblines$[]

FOR i=2 TO lweb
 s1$=weblines$[i]
 s1$=REPLACE$(s1$,"<hr></pre>","")
 s1$=REPLACE$(s1$,"</body></html>","")
 a3=IS_IN(">",s1$)
 namefound$=MID$(s1$,2,a3-3) % name ok

 s1$=REPLACE$(s1$,CHR$(10),"")
 a2=IS_IN("</a>",s1$)
 s2$=RIGHT$(s1$,LEN(s1$)-a2-4)
 ! suppress initial spaces
 DO
  IF LEFT$(s2$,1)=" " THEN s2$=RIGHT$(s2$,LEN(s2$)-1)
 UNTIL LEFT$(s2$,1)<>" "
 ! suppress final spaces
 DO
  IF RIGHT$(s2$,1)=" " THEN s2$=LEFT$(s2$,LEN(s2$)-1)
 UNTIL RIGHT$(s2$,1)<>" "
 IF RIGHT$(s2$,1)<>"K" & RIGHT$(s2$,1)<>"M" THEN s2$=s2$+"B"
 dat_siz$=s2$
!! 
   ! find size
   for j=1 to len(s2$)
    if mid$(s2$,j,1)=" " then a2=j
   next j
   fdate$=left$(s2$,a2)
   fsize$=right$(s2$,len(s2$)-a2)
!!   
 IF namefound$<>".ftpquota" & namefound$<>".htaccess" THEN
  LIST.ADD fsiz,dat_siz$
  LIST.ADD fnam,namefound$
 ENDIF

NEXT i

LIST.SIZE fsiz,Lfsiz
LIST.SIZE fnam,Lfnam
RETURN

! skin change
showSkin:
IF skin=0 THEN % / no skin
 GR.HIDE skin1
 GR.HIDE skin2
 GR.HIDE skin3

ELSEIF skin=1 then
 GR.SHOW skin1
 GR.HIDE skin2
 GR.HIDE skin3

ELSEIF skin=2 then
 GR.HIDE skin1
 GR.SHOW skin2
 GR.HIDE skin3

ELSE % if skin=3
 GR.HIDE skin1
 GR.HIDE skin2
 GR.SHOW skin3

ENDIF
RETURN

! ***close connection on back key***
OnBackKey:
PRINT "stopped!"
FTP.CLOSE
END

! on error treatment
!!
OnError:
 GR.cls
 GR.text.draw t,10,100,"Unexpected error;"
 GR.text.draw t,10,200,err$
 GR.render
 pause 7000
 print err$
end
!!
