REM RFO Basic! FTP Store v1.0r1
REM by Antonis [tony_gr]
REM July 18, 2012

! pointers of icons, icon flag array
dim folders[33],title[33],progs[33],icon_on[33]
! upper menu pointers, preview text pointers
dim lin[7],txmenu[8], prvtx[25]

count_dnld=0 % counts files to download
GR.open 255,0,0,45,0,1
GR.orientation 1
err$="#1a..contacting server"

FTP.open "FTP.laughton.com",21,"basic","basic"

err$="#1b..creating database"
 Stack.create S,paths % paths visited
 Stack.create S,downloads % holds files to download
 List.create S, dnldlist % auxiliary list to show files to download
 
 
 stack.push paths,"./"
GR.screen w,h
sx=w/800
sy=h/1232
GR.scale sx,sy


! background
err$="#2..loading graphics"
GR.text.align 2
GR.text.size 150
 GR.bitmap.load sp,"../../rfo-ftpstore/data/space.jpg" 
 GR.bitmap.scale sp1, sp,w/sx,h/sy
 GR.bitmap.draw skin1,sp1,0,0
 GR.bitmap.delete sp
 GR.hide skin1
 GR.bitmap.load sp,"../../rfo-ftpstore/data/sea.jpg"
 GR.bitmap.scale sp1,sp,w/sx,h/sy
 GR.bitmap.draw skin2,sp1,0,0
 GR.bitmap.delete sp
 GR.hide skin2
GR.color 85,155,155,55,0
GR.color 85,155,155,55,0
GR.text.draw bas,400,300,"RFO"
GR.text.draw bas,400,500,"BASIC!"
GR.text.draw bas,400,700,"FTP STORE"
GR.text.size 25

! bitmap loading
err$="#2..bitmap loading"
GR.color 255,255,255,55,0
GR.bitmap.load  p1,"../../rfo-ftpstore/data/actionbar.png" % android bar
GR.bitmap.load  p2,"../../rfo-ftpstore/data/appmenu.png" % app menu
GR.bitmap.load  p3,"../../rfo-ftpstore/data/folder.png" % generic folder
GR.bitmap.load  p4,"../../rfo-ftpstore/data/prog.png" % program or not folder
GR.bitmap.draw  bar,p1, 0,1080

! draw & hide folders & programs icons & their titles
k=0
for i=1 to 11
for j=1 to 3
 k=k+1
 ! GR.rect rec,80+(j-1)*270, (i-1)*100, 190+(j-1)*270, (i-1)*100+80
 GR.bitmap.draw  folders[k],p3,100+(j-1)*270+10, (i-1)*100
 GR.bitmap.draw  progs[k],p4,100+(j-1)*270+10, (i-1)*100
 GR.text.draw title[k],(j-1)*270+120 +10,(i-1)*100+60,"12345678901234567"
 GR.hide folders[k]
 GR.hide progs[k]
 GR.hide title[k]
next j
next i

! lower menu
GR.bitmap.draw  menu,p2, 0,580
GR.hide menu

! upper menu & choices
GR.color 255,0,0,0,1
GR.text.align 1
GR.rect upmenu,540,50,800,530
GR.color 255,255,255,255,1
GR.text.size 37
GR.text.draw txmenu[1], 550,90," Get selected"
GR.text.draw txmenu[2] ,550,150," Deselect All"
GR.text.draw txmenu[3] ,550,210," Deselect Last"  
GR.text.draw txmenu[4] ,550,270," Download List"
GR.text.draw txmenu[5] ,550,330," Preview last"
GR.text.draw txmenu[6] ,550,390," Change skin"
GR.text.draw txmenu[7] ,550,450," About"
GR.text.draw txmenu[8] ,550,510," Exit"
GR.line lin[1] ,550,105,800,105
GR.line lin[2] ,550,165,800,165
GR.line lin[3] ,550,225,800,225
GR.line lin[4] ,550,285,800,285
GR.line lin[5] ,550,345,800,345
GR.line lin[6] ,550,405,800,405
GR.line lin[7] ,550,465,800,465
for i=1 to 7
  GR.hide txmenu[i]
  GR.hide lin[i]
next i
GR.hide txmenu[8]
GR.hide upmenu

! upper menu icon
GR.rect rct,770,5,780,15
GR.rect rct,770,20,780,30
GR.rect rct,770,35,780,45
! GR.rect r,750,0,800,50
GR.text.size 25

! initilize
path$= "./"
morepages=0
showedpages=0
skin=0

! bar log : paths & messages
GR.color 255,0,155,0,1
GR.text.draw msg,155,1180," "
GR.color 255,255,0,0,1
GR.text.draw cur_path,155,1145,"./"
GR.color 255,255,255,55,0
gosub updatepath
upmenuon=0
menuon=0

! preview black box, text lines
GR.color 255,0,0,0,1
GR.rect preview, 0,160,800,1040
GR.hide preview
GR.text.size 30
GR.color 255,0,255,255,1
for i=1 to 25
  GR.text.draw prvtx[i],5,190+(i-1)*35," "
  GR.hide prvtx[i]
next i
GR.text.align 2
GR.color 255,255,255,55,0
GR.text.size 25
GR.render



! main program
start: % main loop
 
  gosub wait % for a touch

! if icon selected, execute command
  icon_selected=0
  if menuon=0 & upmenuon=0 then 
  for i=1 to 11
    for j=1 to 3
	 icon_selected=icon_selected+1
     if x>(80+(j-1)*270)*sx & y>((i-1)*100)*sy & x<((j-1)*270+190)*sx & y<((i-1)*100+80)*sy & icon_on[icon_selected]=1 then
	   derectory=0
	   gosub checkicon
	   if derectory=1 then % directory choosed
	       gosub chgdir              
	   else % file choosed
		   gosub file_selected
	   endif
	   finished=1
	   f_n.break
     endif
    next j
	if finished=1 then f_n.break
  next i
  finished=0
 endif
  
! menu/basic icon selected
 if x<150*sx & y>1080*sy & upmenuon=0 then
   menuon=!menuon
   if menuon=1 then GR.show menu else GR.hide menu
! GR.show sp2
   GR.render
 endif

! upmenu
 if x>750*sx & y<50*sy & menuon=0 then
   upmenuon=!upmenuon
   gosub show_upmenu
   gosub up_menu
 endif

! more pages selected  
if upmenuon=0 & x>200*sx  & y>1080*sy & menuon=0 & pagesleft>0 then
  gosub morepages
endif
  
! applications
 if x<=300*sx & y>=580*sy & y<670*sy & menuon=1 & upmenuon=0 then
   GR.hide menu
   GR.render
   menuon=0
   path$= "./applications/"
   neupath$= "applications/"
   stack.push paths,"applications/"
   pagesleft=0
   gosub chgdir
  endif
 
! games 
 if x<=300*sx & y>=670*sy & y<750*sy & menuon=1 & upmenuon=0 then
   GR.hide menu
   GR.render
   menuon=0
   path$= "./games/"
   neupath$= ".games/"
   stack.push paths, "games/"
   pagesleft=0
   gosub chgdir
  endif

! utilities
 if x<=300*sx & y>=750*sy & y<830*sy & menuon=1 & upmenuon=0 then
   GR.hide menu
   GR.render
   menuon=0
   path$= "./utilities/"
   neupath$= ".utilities/"
   stack.push paths, "utilities/"
   pagesleft=0
   gosub chgdir
 endif

! html 
 if x<=300*sx & y>=830*sy & y<910*sy & menuon=1 & upmenuon=0 then
   GR.hide menu
   GR.render
   menuon=0
   path$= "./html/"
   neupath$= ".html/"
   stack.push paths, "html/"
   pagesleft=0
   gosub chgdir
 endif
  
! tools 
 if x<=300*sx & y>=910*sy & y<990*sy & menuon=1 & upmenuon=0 then
   GR.hide menu
   GR.render
   menuon=0
   path$= "./tools/"
   neupath$= ".tools/"
   stack.push paths, "tools/"
   pagesleft=0
   gosub chgdir
 endif
  
  ! other 
 if x<=300*sx & y>=990*sy & y<1080*sy & 6=1 & upmenuon=0 then
   GR.hide menu
   GR.render
   menuon=0
   path$= "./other/"
   neupath$= ".other/"
   stack.push paths, "other/"
   pagesleft=0
   gosub chgdir
 endif

goto start % main loop



! ***************
! * SUBROUTINES *
! ***************

! ***change dir/show new dir***
chgdir:
err$="#3..ftp chdir error"
jj=0
for i=1 to 33
  icon_on[i]=0
next i
icon_selected=0

FTP.cd path$
gosub updatepath
FTP.dir archives_list
if path$= "./" then % hide something
   list.get archives_list,3,name$ 
   if name$=".ftpquota" then List.remove archives_list,3
   list.get archives_list,3,name$ 
   if name$=".htaccess" then List.remove archives_list,3
endif

list.size archives_list,asize

gosub check_num_of_pages

if asize>33 then % add root,up choice
   for i=2 to  numpages
    list.insert archives_list,(i-1)*33+1,"..(d)"
    list.insert archives_list,(i-1)*33+1,".(d)"
  next i
endif

list.size archives_list,asize

gosub check_num_of_pages

pagesleft=numpages

! this is called also as a subroutine ;)
! when more than 33 items to show
morepages:
if pagesleft=0 then return
jj=jj+1
pagesleft=pagesleft-1
if  pagesleft>0 then 
   GR.modify msg,"text","More Files: Touch here to go"
 else 
   GR.modify msg,"text","Waiting:"
endif
GR.render
gosub hid_icons_titles
ii=0
for i=(jj-1)*33+1 to jj*33
  ii=ii+1
  if i<=asize then
   list.get archives_list,i,name$ 
   if len(name$)>22 then name$="~"+right$(name$,20)
   if right$(name$,3)= "(d)" then
      name$=left$(name$,len(name$)-3)
      if name$="." then name$= "/"
      if name$=".." then name$="Up"
      GR.show folders[ii]
     else
      GR.show progs[ii]
   endif  
   GR.show title[ii]
   icon_on[ii]=1
   GR.modify title[ii],"text",name$
  endif
next i
GR.render
return 
!*end of chgdir*

!*** checks an icon pressed***
checkicon:       
     err$="#4..icon checking"
	 cur_element=round((numpages-pagesleft-1)*33)+icon_selected
     list.get archives_list,cur_element,name$	 
     a=0
     a=is_in("(d)",name$)
     if a=0 then derectory=0 % we have a file
	 if a<>0 then  % we have a dir
	   if name$= ".(d)" then 
	       path$= "./"
		   gosub updatepath
	    elseif name$= "..(d)" then
	       Stack.IsEmpty paths, n
		   if n=0 then
		        stack.pop paths, apath$
			    path$=replace$(path$,apath$,"")
				gosub updatepath
		   endif 	
	    else
        neupath$ = left$(name$,len(name$)-3)+"/"
	    path$=path$+neupath$
	    stack.push paths,neupath$
        gosub updatepath
	   endif  
       derectory=1
    endif % for directory 
gosub updatepath
return
! *end of checkicon*

! ***treats upper menu when opened***
up_menu:
 err$="#5..upper menu opening"

rewait:

gosub wait

if x>750*sx & y<50*sy then 
   ! close menu
   gosub hide_upmenu
   upmenuon=!upmenuon
   return
endif 
 
if x>550*sx & y>50*sy & y<=105*sy then 
 ! 1st option selected / get selected
 err$="#6..file downloading"
 popup "downloading "+replace$(str$(count_dnld),".0","")+" files",0,0,4
 gosub hide_upmenu
 upmenuon=!upmenuon
 do
   Stack.IsEmpty downloads, n
   if n=0 then      
     Stack.pop downloads,file$
     GR.modify msg,"text","Downloading: "+right$(file$,40) 
     GR.render
     fileTemp$=lower$(right$(file$,4))
     for l=1 to len(file$)
       if mid$(file$,l,1)="/" then index=l
     next l
     fname$=right$(file$,len(file$)-index)
	 ! get rid of "." now
 	 file$=right$(file$,len(file$)-1)
     if fileTemp$=".bas" then	
        FTP.get file$,"../source/"+fname$
     elseif fileTemp$=".apk" then
        FTP.get file$,"../"+fname$
     else           
       FTP.get file$,fname$
     endif
   endif	 
 until n=1
 GR.modify msg,"text","Downloaded: "+ replace$(str$(count_dnld),".0","")+" files."
 GR.render
 popup "Finished!",0,0,0
 tone 800,800
 count_dnld=0
 Stack.clear downloads
 return
endif % for 1st option

  
 if x>550*sx & y>105*sy & y<=165*sy then 
 ! 2nd option selected / Deselect all
  err$="#7..file deselecting"
  Stack.clear downloads
  popup "Download list Empty!",0,0,0
  count_dnld=0
  pause 1500
  gosub hide_upmenu
  upmenuon=!upmenuon
  return
 endif % for 2nd option
  
 if x>550*sx & y>165*sy & y<=225*sy then 
 ! 3d option selected / Deselect last 
  err$="#7..file deselecting"
  Stack.IsEmpty downloads, n
  if n=0 then
    stack.pop downloads,temp$
    popup "Last file deselected!",0,0,0
    count_dnld=count_dnld-1
  endif
  pause 1500
  gosub hide_upmenu
  upmenuon=!upmenuon
  return
 endif% for 3rd option
 
 if x>550*sx & y>225*sy & y<=285*sy then 
 ! 4th option selected / Download list 
  err$="#7..list"
  gosub hide_upmenu
  upmenuon=!upmenuon
  GR.modify msg,"text","Showing download list.."
  for i=1 to count_dnld
   stack.pop downloads,temp$
   list.add dnldlist,temp$
  next i
  for i=count_dnld to 1 step -1
    list.get dnldlist,i,temp$
    stack.push downloads,temp$
  next i	
  GR.show preview
  for i=1 to 25
     GR.modify prvtx[i],"text",""
     GR.show prvtx[i]
  next i
  ! print list/wait/remove list elements
  k=0  
  for i=1 to count_dnld
    if mod(i,25)=1 & i>2 then
          GR.render
          k=0
          gosub wait
    endif
      k=k+1
      list.get dnldlist,i,temp$
      if len(temp$)>45 then temp$="~"+right$(temp$,45)
      GR.modify prvtx[k],"text",temp$   
   next i
  for l=k+1 to 25
     GR.hide prvtx[l]
  next 
  extramsg$=""
  if  pagesleft>0 then  extramsg$=" More Files: touch here to go!"  
  GR.modify msg,"text","End of list. Touch screen to quit."+ extramsg$
  GR.render
  gosub wait
  GR.hide preview
  for i=1 to 25
     GR.hide prvtx[i]
  next i 
  GR.modify msg,"text","Waiting.."+ extramsg$
  GR.render
  for i=1 to count_dnld
      List.remove dnldlist,1
  next i    
  return
 endif % for 4th option

 
 if x>550*sx & y>285*sy & y<=345*sy then 
 ! 5th option selected / Preview last 
 err$="#8..preview"
  Stack.IsEmpty downloads, n
  if n=1 then 
    popup "Nothing to preview",0,0,0
    gosub hide_upmenu
    upmenuon=!upmenuon
   else 
    gosub hide_upmenu
    upmenuon=!upmenuon
    Stack.peek downloads, file$
    fileTemp$=lower$(file$)
    fileTemp$=right$(filetemp$,4)

    if fileTemp$=".bas" | fileTemp$=".txt" | fileTemp$=".htm" | fileTemp$="html" | fileTemp$=".xml" then
    ! **************
    ! *text  viewer*
    ! **************
	GR.text.size 30
    finished=0
    stack.peek downloads,file$
    for l=1 to len(file$)
       if mid$(file$,l,1)="/" then index=l
    next l
    fname$=right$(file$,len(file$)-index)
    GR.modify msg,"text","Loading '"+fname$+"'"
    GR.render
    file$=right$(file$,len(file$)-1)
    FTP.get file$, fname$
    FILE.Size fsize,fname$
    GR.modify msg,"text","Showing file: '"+fname$+"', Size is:"+replace$(str$(fsize),".0","")
    GR.show preview
    GR.render
    for i=1 to 25
     GR.modify prvtx[i],"text",""
     GR.show prvtx[i]
    next i
    
    j=1
    k=0
    line$=""
    TEXT.open r, Ftable, fname$
    while line$<>"EOF" & finished=0
       TEXT.readln ftable, line$
       if LEN(line$)<41 THEN
           toprint$=line$
           k=k+1
           GR.modify prvtx[k],"text",toprint$
           GR.render
           GOSUB check_k
       endif
       IF LEN(line$)>=41 THEN
          a$=left$(line$,40)
          found=1 
          j=1
          FOR i=40 TO LEN(line$)
            a$=a$+MID$(line$,i-1,1)
            b$=a$+MID$(line$,i,1)
            GR.TEXT.width w1,a$
            GR.TEXT.width w2,b$
            IF w1<=800*j & w2>800*j THEN          
              j=j+1       
              toprint$=MID$(line$,found,i-found)
              k=k+1
              GR.modify prvtx[k],"text",toprint$
              GR.render
              found=i
              GOSUB check_k 
            ENDIF
          NEXT i
          IF found=1 THEN 
           toprint$=line$
           k=k+1
           GR.modify prvtx[k],"text",toprint$
           GR.render
           GOSUB check_k
          ELSE
           k=k+1
           toprint$=MID$(line$,found,LEN(line$)-found+1)
           GR.modify prvtx[k],"text",toprint$
           GR.render 
           GOSUB check_k
          ENDIF
       ENDIF
    repeat
    gosub wait
    file.delete f,"../"+name$
    GR.hide preview
    for ll=1 to 25
       GR.modify prvtx[ll],"text",""
      GR.hide prvtx[ll]
    next ll
   
   
   GR.modify msg,"text","Viewing finished: Waiting.."
   GR.render

  endif % text viewer

    if fileTemp$="jpeg" | fileTemp$=".jpg" | fileTemp$=".png" then
    ! **************
    ! *image viewer*
    ! **************
    stack.peek downloads,file$
     for l=1 to len(file$)
       if mid$(file$,l,1)="/" then index=l
     next l
     fname$=right$(file$,len(file$)-index)
    GR.modify msg,"text","Loading '"+fname$+"'"
	GR.show preview
    GR.render
    file$=right$(file$,len(file$)-1)
    FTP.get file$, "../"+fname$
    GR.modify msg,"text","Rendering image: '"+fname$+"'"
    GR.render
    GR.bitmap.load bmp0, "../"+fname$
    GR.bitmap.size bmp0,w,h
    currentscale=1
    IF w>=800*sx | h>=800*sy THEN % image >800x800
       IF w>=h THEN currentscale=800/w ELSE currentscale=800/h
    ENDIF
    GR.bitmap.scale bmp1, bmp0, currentscale*w, currentscale*h
    
    ! image DIM are now x->currentscale*ww y->currentscale*hh
    ! center image
    xpos=(800-currentscale*w)/2
    ypos=200+(800-currentscale*h)/2
    GR.bitmap.draw bmp2,bmp1,xpos,ypos
    GR.modify msg,"text","Preview finished! Touch Screen to go on!"
    GR.render
    
    ! wait to go on
    gosub wait
    GR.bitmap.delete bmp0
    GR.bitmap.delete bmp1
    GR.hide bmp2
	GR.hide preview
    GR.render
    file.delete f,"../"+name$
    GR.modify msg,"text","Waiting.."
    GR.render
    ! *end of gviewer*
   endif % for gfx viewer
  endif % n=1
  return
 endif % for 5th option
 
 if x>550*sx & y>345*sy & y<=405*sy then 
 ! 6th option selected | change skin
  err$="#9..skin"
  if skin=0 then 
      GR.show skin1
      GR.hide skin2
      skin=1
  elseif skin=1 then
     GR.hide skin1
     GR.show skin2
     skin=2
  else
     GR.hide skin1
     GR.hide skin2
     skin=0
   endif
  GR.render
  gosub hide_upmenu
  upmenuon=!upmenuon
  return
 endif
 
if x>550*sx & y>405*sy & y<=465*sy then 
 ! 7th option selected | About
  err$="#9..about"
  GR.modify msg,"text","About.. / Touch screen to quit!"
  pause 500  
  gosub hide_upmenu
  upmenuon=!upmenuon
  GR.show preview
  for i=1 to 6
   GR.show prvtx[i*2]
  next i
  GR.modify prvtx[2],"text","         >>_Rfo Basic! FTP Store_<<"
  GR.modify prvtx[4],"text","         >>_version 1.0 r1_<<"
  GR.modify prvtx[6],"text","         >>_July 18, 2012_<<"
  GR.modify prvtx[8],"text","         >>_by Antonis [tony_gr]_<<"
  GR.modify prvtx[10],"text","Join the rfobasic.freeforums.org for questions or"
  GR.modify prvtx[12],"text","comments about this program."
  GR.render
  gosub wait
  GR.modify msg,"text","Waiting.."
  for i=1 to 6
   GR.hide prvtx[i*2]
  next i
  GR.hide preview
  GR.render
  return
 endif

 if x>550*sx & y>465*sy & y<=525*sy then 
 ! 8th option selected / exit
  popup "Quitting",0,0,0
  err$="#10..session closing"
  FTP.close
  gosub hide_upmenu
  upmenuon=!upmenuon
  print "Bye!"
  END

  return 
 endif  % for 8th option
 
goto rewait
! return
! *end of up_menu*

! ***hide_icons_titles***
hid_icons_titles:
 err$="#11..icon"
 for m=1 to 33
  GR.hide folders[m]
  GR.hide title[m]
  GR.hide progs[m]
  icon_on[m]=0
 next m
GR.render
return
! *end of hid_icons_titles*

! ***file_selected***
file_selected:
err$="#12..file_selecting"
count_dnld=count_dnld+1
GR.modify msg,"text","Selected: "+left$(name$,40) 
stack.push downloads,path$+name$
GR.render
return
! * end offile_selected*

! ***check_num_of_pages of files+dirs***
check_num_of_pages:
err$="#13..page_selecting"
! file_selected
if asize >33 then 
   a=asize
   rest=mod(a,33)
   numpages=ROUND((a-rest)/33)+1
   morepages=1
 else
 numpages=1
endif
return
! *end of check_num_of_pages*

! ***show_upmenu***
show_upmenu:
err$="#14..upmenu_opening"
for i=1 to 7
  GR.show txmenu[i]
  GR.show lin[i]
next i
GR.show txmenu[8]
GR.show upmenu
GR.render
return
! *end of show_upmenu*

! ***show_upmenu***
hide_upmenu:
err$="#15..upmenu_closing"
for i=1 to 7
  GR.hide txmenu[i]
  GR.hide lin[i]
next i
GR.hide txmenu[8]
GR.hide upmenu
GR.render
return
! *end of show_upmenu*

! ***updatepath***
updatepath:
err$="#15..path updating"
GR.modify cur_path,"text","path: "+right$(path$,45)
GR.render
return
! *end of updatepath*

! *** wait for a touch***
wait:
do
  GR.touch touched,x,y
  until touched
 do
   GR.touch touched,x,y
 until !touched
return
! *end of wait*

! ***checks k***
! called by text viewer
check_k:
 IF k=25 THEN 
  k=0
  GR.modify msg,"text","Touch bar to quit, elsewhere to continue."  
  GR.render
  GOSUB wait
  IF y>1080*sy THEN  
      TEXT.close ftable
      finished=1
	  return
  endif 
  for l=1 to 25
     GR.modify prvtx[l],"text",""
	 GR.render
    next l
endif	
GR.render
return


! ***close connection on back key***
OnBackKey:
print "stopped!"
FTP.close
end

! on error treatment
!!
OnError:
 print err$
 GR.cls
 GR.text.draw t,10,100,"Unexpected error;"
 GR.text.draw t,10,200,err$
 GR.render
 pause 7000
end
!!
