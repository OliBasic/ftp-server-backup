Rem Console Library Demo1
Rem With RFO Basic!
Rem September / October 2015
Rem Version 1.00
Rem By Roy Shepherd

di_height = 1120 % set to my Device
di_width = 720

gr.open 255,0,0,0 % Black background colour
gr.color 255,255,255,255,1 % White foreground colour
gr.orientation 1
pause 1000
WakeLock 3 

gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y

!-------------------------------------------------
! Setup screen for drawing.
!-------------------------------------------------
gr.bitmap.create screen, di_width, di_height
gr.bitmap.draw    nn, screen, 0, 0
gr.bitmap.drawinto.start screen
!-------------------------------------------------

!-------------------------------------------------
! Include ConLib.bas after opening graphics
! and after gr.bitmap.create screen, di_width, di_height 
! and before creating any bundles 
!-------------------------------------------------
include ConLib.bas % include 'ConLib.bas' here
gosub Functions
!-------------------------------------------------

!-------------------------------------------------
! Put your bundle's here, after the included ConLib.bas
! 'global' bundle pointer = 2.
!-------------------------------------------------
bundle.create global
bundle.put global, "Divice_W", di_width

! The 18 colours are:
! 0 = Black, 1 = Grey, 2 = Silver, 3 = White, 4 = Maroon, 5 = Red, 6 = Olive, 7 = Yellow,
! 8 = Green, 9 = Lime, 10 = Teal, 11 = Aqua, 12 = Navy, 13 = Blue, 14 = Purple, 
! 15 = Fuchsia, 16 = Brown, 17 = Gold.
!-----------------------------------------------------
do
	!-------------------------------------------------
	! Scroll text
	!-------------------------------------------------
	call PrintAndScroll()

	!-------------------------------------------------
	! Print different size text
	!-------------------------------------------------
	call TextSizes()

	!-------------------------------------------------
	! User to Rearrange Numbers to 0123456789
	!-------------------------------------------------
	call RearrangeNumbers()

	!-------------------------------------------------
	! User pick background and foreground colours
	!-------------------------------------------------
	call PickColours()

	!-------------------------------------------------
	! Print all the Colours and background colours
	!-------------------------------------------------
	call AllColours()

	!-------------------------------------------------
	! Print a selected Times Table
	!-------------------------------------------------
	call TimesTable()

	!-------------------------------------------------
	! Print End of Demo message 
	!-------------------------------------------------
	call EndOfDemo()
	
	!-------------------------------------------------
	! Rerun demo if user taps Yes
	!-------------------------------------------------
	Con_TextSize(30)
until YesNo("Rerun Demo") = 1 % No

onBackKey:


end"End of ConLib.bas Demo"
!-------------------------------------------------
! Start of functions
!-------------------------------------------------
Functions:

!-------------------------------------------------
! Print text at the center of the screen in row y
!-------------------------------------------------
fn.def Centre(y, t$)
	Con_PrintAt(round(Con_GetMaxX() / 2 - len(t$) / 2 ,, "U"), y, t$)
fn.end 

!-------------------------------------------------
! Prints OK at row y and waits for it to be tapped 
!-------------------------------------------------
fn.def OK(y) 
	call Centre(y, " OK ") : flag = 0
	findCentre = floor(Con_GetMaxX() / 2)
	do 
		if Con_GetTouchY() = y then 
			if Con_GetTouchX() > findCentre - 3 & Con_GetTouchX() < findCentre +3 then flag = 1 
		endif 
	until flag
fn.end

!-------------------------------------------------
! Print background and foreground colours
!-------------------------------------------------
fn.def AllColours()
	Con_Background(0) : Con_Colour(3) : Con_Cls()
	Con_TextSize(40) : Con_Background(5)
	Centre(1, "Colour Pallet")
	Con_TextSize(20)
	Con_Colour(3) : Con_Background(0)
	Con_Bold(1)
	Con_Locate(4,4)
	Con_RenderOff()
	Con_Print("0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17")
	for y = 0 to 17 : call Con_PrintAt(1, y + 5, int$(y)) : next
	Con_Render()
	for y = 0 to 17 
		Con_Background(y)
		for x = 1 to 18
			Con_Locate(x * 3,y + 5) 
			Con_Colour(x - 1)
			Con_Print(" X ")
		next
	next
	Con_Background(0) : Con_Colour(3)
	Centre(24, "The Columns are Foreground Colours")
	Centre(25, "The Row are Background Colours")
	Con_RenderOn() : Con_Bold(0)
	Con_TextSize(40) : call OK(17)
fn.end

!-------------------------------------------------
! Print box with heading. Works same as gr.rect
!-------------------------------------------------
fn.def Box(bLeft, top, bRight, bottom, t$)
	
	tl$ = chr$(9556): tr$ = chr$(9559) : bl$ = chr$(9562) : br$ = chr$(9565)
	lrUp$ = chr$(9553) : lrCross$ = chr$(9552) : hl$ = chr$(9569) : hr$ = chr$(9566)
	Con_Background(2) : Con_RenderOff() % Turn rendering off for speed
	! Gray background for the box
	for x = bLeft + 1 to bRight - 1 
		for y = top + 1 to bottom - 1
			Con_PrintAt(x, y, chr$(32)) 
		next
	next
	! Print each corner 
	Con_Colour(7) % Yellow box
	Con_PrintAt(bLeft, top, tl$) : Con_PrintAt(bRight, bottom, br$)
	Con_PrintAt(bLeft, bottom, bl$) : Con_PrintAt(bRight, top, tr$)
	! Print left and right sides
	for y = top + 1 to bottom - 1
		Con_PrintAt(bLeft, y, lrUp$)
		Con_PrintAt(bRight, y, lrUp$)
	next
	! Print top and bottom
	for x = bLeft + 1 to bRight - 1
		Con_PrintAt(x, top, lrCross$)
		Con_PrintAt(x, bottom, lrCross$)
	next
	! Print the heading if t$ is not ""
	if t$ <> "" then 
		s$ = hl$ + t$ + hr$ 
		tl = len(s$) / 2
		x = (bRight + bLeft) / 2
		x = x - tl
		Con_PrintAt(x, top, s$)
	endif
	Con_RenderOn() % Turn rendering back on to show box
fn.end

!-------------------------------------------------
! Select a Times Table to be printed
!-------------------------------------------------	
fn.def TimesTable()
	do
		Con_TextSize(30) : Con_Background(13)
		Con_Colour(3) : Con_Cls() : Con_Bold(1) : Con_Underline(1)
		call Centre(1, "Times Tables") : Con_Bold(0) : Con_Underline(0)
		call Centre(3, "Please Tap a Table from the list below")
		Con_Background(15)
		Con_TextSize(40)
		Con_Locate(2, 5)
		for x = 2 to 10
			if x < 10 then Con_Background(x + 3) else : Con_Background(14)
			Con_Print(" " + int$(x) + " ")
		next
		do 
			do : y = Con_GetTouchY() : until y
			do : x = Con_GetTouchX() : until x
		until y = 5 & x > 1 & x < 30
		call PrintTable(x)
		Con_TextSize(30)
	until YesNo("Another Table") = 1 % No
fn.end

!-------------------------------------------------
! Print out the Times Table the user selects
!-------------------------------------------------	
fn.def PrintTable(table)
	! Find the tabel that was selected
	t = 2
	for x = 3 to 27 step 3 
		if table = x | table - 1 = x | table + 1 = x then table = t
		t += 1
	next
	if table = 29 then table = 10
	Con_Background(13) : call Centre(7, int$(table) + " Times Table")
	for t = 1 to 10 
		Con_PrintAt(10, 8 + t, int$(t) + " X " + int$(table) + " = " + int$(t * table))
	next
	
fn.end

!-------------------------------------------------
! Print a Dialog Box at the bottom of the screen. 
! Returns 2 for Yes and 1 for No
!-------------------------------------------------
fn.def YesNo(t$)
	!Con_TextSize(30)         % set text size first
	bg = Con_GetBackground() % save background colour 
	fg = Con_GetColour()     % save foreground colour
	scw = Con_GetMaxX() - 2     % get screen width 
	sch = Con_GetMaxY()      % get screen height 
	
	flag = 0
	tl$ = chr$(9556): tr$ = chr$(9559) : bl$ = chr$(9562) : br$ = chr$(9565)
	lrUp$ = chr$(9553) : lrCross$ = chr$(9552) : hl$ = chr$(9569) : hr$ = chr$(9566)
	Con_Background(2) : Con_RenderOff() % Turn rendering off for speed

	! Gray background for the box
	for x = 3 to scw - 1 
		for y = sch - 3 to sch - 1
			Con_PrintAt(x, y, chr$(32)) 
		next
	next
	! Print each corner 
	Con_Colour(0) % black box
	Con_PrintAt(2, sch - 4, tl$) : Con_PrintAt(scw, sch , br$)
	Con_PrintAt(2, sch , bl$) : Con_PrintAt(scw, sch - 4, tr$)

	! Print left and right sides
	for y = sch - 3 to sch - 1
		Con_PrintAt(2, y, lrUp$)
		Con_PrintAt(scw, y, lrUp$)
	next
	! Print top and bottom
	for x = 3 to scw - 1
		Con_PrintAt(x, sch - 4, lrCross$)
		Con_PrintAt(x, sch, lrCross$)
	next
	! Print the heading at the top of the box
	s$ = hl$ + t$ + hr$ 
	tl = len(s$) / 2
	x = (scw) / 2 : x -= tl
	call Con_PrintAt(x, sch - 4, s$)
	
	! Print Yes No 
	Con_PrintAt(8, sch - 2,"< Yes >") : Con_PrintAt(scw - 13, sch - 2,"< No  >")
	Con_RenderOn() % Turn rendering back on to show box
	Con_Background(13) : Con_Colour(3)
	do
		do : y = Con_GetTouchY() : until y
		if y = sch - 2 then 
			x = Con_GetTouchX()
			if x > 7 & x < 15 then flag = 2 : Con_PrintAt(8, sch - 2,"< Yes >") % Yes
			if x > scw - 14 & x < scw - 6 then flag = 1 : Con_PrintAt(scw - 13, sch - 2,"< No  >") % No
			do : gr.touch t, tx, ty : until ! t
		endif
	until flag
	Con_Background(bg) : Con_Colour(fg) % restore foreground and background colours
	fn.rtn flag
fn.end	

!-------------------------------------------------
! Pick background and foreground colours
!-------------------------------------------------
fn.def PickColours()
	do
		Con_TextSize(30) : Con_Background(0) : Con_Colour(3) : Con_Cls()
		Con_Background(5) : call Centre(1, "Tap a colour for the background")
		Con_Locate(1,3)
		
		array.load c$[], "Black", "Grey", "Silver", "White", "Maroon", "Red" ~ 
						 "Olive", "Yellow", "Green", "Lime", "Teal", "Aqua" ~ 
						 "Navy", "Blue", "Purple", "Fuchsia", "Brown", "Gold"
		array.length n, c$[]
		Con_Background(0) : Con_Colour(3)
		! Add a new line to the end of the colour name and print it
		for x = 1 to n
			p$ = c$[x] + "/n"
			Con_Print(p$)
		next
		! User taps and background colour. 
		flag = 0
		do
			for c = 1 to 20
				y = Con_GetTouchY()
				if y > 0 & y - 2 = c then Con_Background(c - 1) : Con_Locate(1, y) : Con_Print(c$[c]) : flag = c
			next
		until flag
		! Screen is cleared to the new backgroud
		pause 500 : Con_Cls()
		b = Con_GetBackground()
		! Reprint the colour names.
		Con_Background(5) : call Centre(1, "Tap a colour for the foreground") : Con_Background(b)
		Con_Locate(1, 3)
		if b = 3 then Con_Colour(0)
		for x = 1 to n
			p$ = c$[x] + "/n"
			Con_Print(p$)
		next
		! User taps a foreground colour
		flag = 0
		do
			for c = 1 to 20
				y = Con_GetTouchY()
				if y > 0 & y - 2 = c then Con_Colour(c - 1) : Con_Locate(1, y) : Con_Print(c$[c]) : flag = c
			next
		until flag
		fg = Con_GetColour() : bg = Con_GetBackground()
		! Print results 
		if fg = bg then 
			Con_Background(0) : Con_Colour(3)
			call Centre(22, c$[fg + 1] + " on a " + c$[bg + 1] + " Background !")
		else
			call Centre(22, c$[fg + 1] + " on a " + c$[bg + 1] + " Background")
		endif
		array.delete c$[]
	until YesNo("Try Another Combination") = 1
fn.end

!-------------------------------------------------
! Rearrange Numbers to 0123456789
!-------------------------------------------------
fn.def RearrangeNumbers()
	array.load numbers[], 0,1,2,3,4,5,6,7,8,9
	do
		array.shuffle numbers[]
		trys = 0 : giveUp = 0
		Con_Background(12) : Con_Colour(2) : Con_Cls() 
		Con_TextSize(30) : call Centre(1, "Rearrange Numbers")
		Con_Locate(1,3) 
		Con_Print("Rearrange the Numbers too '0123456789'./n")
		Con_Print("Tap a number to swap it with the end/nnumber./n/n")
		call Centre(7, "+-+-+-+-+-+-+-+-+")
		Call Box(1, 10, Con_GetMaxX(), 15, "Rearrange Numbers")
		Con_TextSize(80)
		Con_locate(3,5)
		call PrintNumbers(numbers[])
		! Get use input (which number to swap
		call PrintAttempts(trys) : Con_RenderOff()
		Con_Background(5) : Con_Locate(1,9) : Con_PrintSpc(Con_GetMaxX())
		call Centre(9, " <I Give Up>") : Con_Background(2) : Con_RenderOn()
		do
			do : y = Con_GetTouchY() : until y
			if y = 9 then giveUp = 1
			if y = 5 then 
				x = Con_GetTouchX()
				if x > 2 & x < 13 then 
					do : gr.touch t, tx, ty : until ! t
					x -= 2
					swap numbers[x], numbers[10]
					call PrintNumbers(numbers[])
					trys += 1 : call PrintAttempts(trys)
				endif
			endif
		until Sorted(numbers[]) | giveUp
		if ! giveUp then Con_Background(5) : call Centre(9, " Well Done! ")
		Con_TextSize(30)
	until YesNo("Another Game") = 1 % No
fn.end

!-------------------------------------------------
! Mix up the string numbers$
!-------------------------------------------------
fn.def PrintNumbers(numbers[])
	Con_locate(3,5) : Con_RenderOff()
	for x = 1 to 10
		con_Print(int$(numbers[x]))
	next
	Con_RenderOn()
fn.end

!-------------------------------------------------
! Print the number of attempts so far.
!-------------------------------------------------
fn.def PrintAttempts(trys)
	Con_Background(12)
	Con_PrintAt(3,7, "Attempts: " + int$(trys)) 
	Con_Background(2)
fn.end

!-------------------------------------------------
! Test to see if the numbers are arranged 0123456789
!-------------------------------------------------
fn.def Sorted(numbers[])
	flag = 1
	for x = 0 to 9
		if numbers[x + 1] <> x then flag = 0
	next
	fn.rtn flag
fn.end

!-------------------------------------------------
! Test to see if the numbers are arranged 0123456789
!-------------------------------------------------
fn.def EndOfDemo()
	Con_Background(12) : Con_Colour(2) : Con_Cls() 
	Con_TextSize(40) : call Centre(2, "End ")
	Con_TextSize(60) : call Centre(3, "of")
	Con_TextSize(80) : call Centre(4, "Demo")
fn.end

!-------------------------------------------------
! Different text sizes
!-------------------------------------------------
fn.def TextSizes()
	do
		for xx = 20 to 70 step 10
			Con_TextSize(xx) : Con_Colour(3) : Con_Background(0) : Con_Cls()
			x = Con_GetMaxX() : y = Con_GetMaxY() : t = Con_GetTextSize() 
			call Centre(1,"Text Size: " + int$(t))
			Con_PrintAt(1,2,"Max  Columns: " + int$(x))
			Con_PrintAt(1,3,"Max Rows: " + int$(y))
			for x = 4 to 10
				Con_Background(x) : Con_Colour(x+1)
				Con_Locate(1,x)
				Con_Print("This is line: " + int$(x))
			next
			call Centre(12, " < Tap to go on >")
			do : pause 1 : gr.touch t, tx, ty : until t
		next
		Con_ChrPerLine(40)
	until  YesNo("Rerun") = 1 % No
fn.end

!-------------------------------------------------
! When text gets to bottom of screen, scroll the text up.
! The text is lost when it goes off the top of the screen 
!-------------------------------------------------
fn.def PrintAndScroll()
	do
		Con_ChrPerLine(40) : Con_Colour(3) : Con_Background(9) : Con_Cls()
		Con_Background(5) : call Centre(9,"Scroll Demo")
		Con_Locate(1,10) : Con_Background(8)
		for x = 1 to 40
			Con_Print("This is line: " + int$(x) + "/n")
		next
		Con_Print("End/n")
		Con_Colour(5) : Con_Background(7) : Con_Print( "   <   Tap to Scroll a bit more   >   ")

		do : pause 1 : gr.touch t, tx, ty : until t
		Con_Background(12) : Con_Cls() : Con_Background(7)
		Con_locate(10,Con_GetMaxY() - 5)
		for x = 1 to 50
			Con_Print("X now = " + int$(x) + " ")
		next
		Con_Print("/nEnd/n")
		Con_Colour(5) : Con_Background(7) : Con_Print("   <   Tap to End   >   ")
		do : pause 1 : gr.touch t, tx, ty : until t
	until YesNo("Rerun") = 1 % No
fn.end

return
