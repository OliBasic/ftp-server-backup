Rem Logo Demo1
Rem With RFO Basic!
Rem July/August 2015
Rem Version 1.01
Rem By Roy
!!
The Logo colours are:
0 = Black, 1 = Gray, 2 = Silver, 3 = White, 4 = Maroon, 5 = Red, 6 = Olive, 7 = Yellow,
8 = Green, 9 = Lime, 10 = Teal, 11 = Aqua, 12 = Navy, 13 = Blue, 14 = Purple, 
15 = Fuchsia, 16 = Brown, 17 = Gold.
!!

di_height = 1120 % set to my Device
di_width = 720

gr.open 255,0,0,0 % Black
gr.orientation 1
pause 1000
WakeLock 3 

gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y
gr.set.stroke 2
gr.text.size 40
!gr.set.AntiAlias 0

!------------------------------------------------
! Setup screen for drawing and include LogoLib.bas
!-------------------------------------------------
gr.bitmap.create screen, di_width, di_height
gr.bitmap.draw    nn, screen, 0, 0
include LogoLib.bas % include 'LogoLib.bas' here
gr.bitmap.drawinto.start screen
!----------------------------------
if Logo_Lib$ <> "v1.01" then
	?"Please Update LogoLib.bas to 'v1.01'" 
	?"at: http://laughton.com/basic/programs/libraries/Logo_Library_Roy/"
	end
endif

gosub Functions % Let Basic see the Functions before they are called

!-------------------------------------------------
! Start of demo1
!-------------------------------------------------
do
	
	call AnalogClock(di_width / 2, 320, 200)

	call Wheel()

	call SpinBoxes(4, 100, 5)

	call Flower(2,10)

	call Flower(6,10)
	
	call TwoDisks()

	call DrawGameScene()

	call WigglyBall( 20)

	call WigglyBall( 100)

	call Maze(12)

	call Spiral(60, 10, 10, 121, 1)

	call Spiral(80, 10, 10, 200, 0)
	 
	call Polygon( 1,123,3)

	call DottyCircle(100,10)

	call DottyCircle(100,5)

	call SpiralCircle(250)

	call DrawStars(di_width, di_height)

	call Ball1(40)

	call ExplodingStar(100,500)

	call CirclesOnSticks(200, 20)
	
	call Heading(50, "End of Demo one.")
	call Heading(100,"Tap to Rerun or Back Key to end.")
	do : gr.touch t, tx, ty : until t : Logo_ClearScreen()
 until 0

onBackKey: 

end"End of LogoLib Demo one"

Functions:
	
!------------------------------------------------
! Wheel
!-------------------------------------------------
fn.def Wheel()
	Logo_SetBgColour(1) : Logo_TurtleHide()
	Logo_PenUp() : Logo_SetPosX(100) : Logo_PenDown() : Logo_RenderOff()
	call Heading(50,"Wheels")
	for x = 1 to 9 
		Logo_SetPenColour(x + 3)
		call Wee() : Logo_Render()
		Logo_Forward(150)
	next : Logo_Render()
	call FillWheel()
	call WaitForTouch()
fn.end 

fn.def Tri()
	for x = 1 to 3 
		Logo_Forward(40) : Logo_RightTurn(120) 
	next
fn.end

fn.def Wee()
	for x = 1 to 20 
		Logo_RightTurn(20)
		call Tri() : Logo_Forward(20)
	next
fn.end 

fn.def FillWheel()
	Logo_PenUp() : Logo_TurtleHome() : Logo_PenDown()
	Logo_SetPenColour(10)
	Logo_Fill()
	Logo_SetPenColour(17)
	Logo_MoveBack(80) : Logo_RightTurn(15)
	for x = 1 to 9 
		Logo_MoveForward(220) 
		Logo_Fill()
		Logo_MoveBack(220)
		Logo_LeftTurn(366 / 9)
	next : RD()
fn.end

!-------------------------------------------------
! Draw scene for a game
!-------------------------------------------------
fn.def DrawGameScene()
	call Heading(50, "Draw scene for game")
	Logo_TurtleHide()
	Logo_RenderOff()
	gr.screen w, h : gr.set.AntiAlias 0
	flag = 1 : t = 0
	Logo_PenUp()
	Logo_Jump(-1,900) : Logo_PenDown() : Logo_RightTurn(90) : Logo_SetPenSize(4)
	do
		y = floor(100 * rnd() - 50)
		
		if flag & Logo_GetPosX() > 100 & floor(100 * rnd()) > 90 then 
			shipX = Logo_GetPosX() : shipY = Logo_GetPosY()
			Logo_ReSetAngle() : Logo_RightTurn(90) : Logo_Forward(100) : flag = 0 
		endif
		if flag & Logo_GetPosX() > 600 then
			shipX = Logo_GetPosX() : shipY = Logo_GetPosY()
			Logo_ReSetAngle() : Logo_RightTurn(90) : Logo_Forward(100) : flag = 0 
		endif
		
		Logo_LeftTurn(y) : Logo_Forward(10)
		Logo_ReSetAngle() : Logo_RightTurn(90)
		!Need 'Logo_GetPosX() - Logo_GetPosY() > w' for old devices. Should just need 'Logo_GetPosX() > w'
	until Logo_GetPosX() - Logo_GetPosY() > w
	Logo_PenUp() : Logo_Jump(10,999) : Logo_PenDown() : Logo_SetPenColour(9) : Logo_Fill()
	call DrawSpaceShip(shipX, shipY) : call Stars()
	Logo_Render() : gr.set.AntiAlias 1
	call WaitForTouch() : Logo_ClearScreen() 
fn.end

!-------------------------------------------------
! Draw stars of game scene
!-------------------------------------------------
fn.def Stars()
	Logo_SetPenSize(5)
	for s = 1 to 100 
		x = floor(710 * rnd() + 1) : y = floor(1100 * rnd() + 1)
		c = TestColour(x, y)
		if c = 0 then
			Logo_SetPC(floor(255 * rnd() + 1),floor(255 * rnd() + 1),floor(255 * rnd() + 1),floor(255 * rnd() + 1), 1)
			Logo_PenUp() : Logo_Jump(x, y) : Logo_PenDown()
			Logo_Dot()
		endif
	next
fn.end

!-------------------------------------------------
! Draw Space Ship of game scene
!-------------------------------------------------
fn.def DrawSpaceShip(shipX, shipY)
	Logo_ReSetAngle() : Logo_PenUp() : Logo_Jump(shipX + 100, shipY - 45) : Logo_PenDown()
	Logo_SetPenColour(3)
	! Draw Body
	call AllShapes(4, 100, 0)
	! Draw Legs
	Logo_RightTurn(90) : Logo_MoveBack(50)
	call AllShapes(3, 50, 1)
	 Logo_MoveBack(50) : call AllShapes(3, 50, 1)
	! Draw Top
	Logo_ReSetAngle() : Logo_MoveForward(100)
	Logo_RightTurn(90) : Logo_MoveForward(50)
	Logo_LeftTurn(180) : Logo_SetPenSize(4)
	for x = 1 to 184 step 4
		Logo_SetPenColour(floor(rnd() * 17) + 1) 
		Logo_MoveForward(50) : Logo_Dot() 
		Logo_MoveBack(50) : Logo_RightTurn(4)
	next
	! Fill Top 
	Logo_ReSetAngle() : Logo_Moveforward(25)
	Logo_SetPenColour(14) : Logo_Fill()
	! Window 
	Logo_MoveBack(75)
	for x = 1 to 360 step 4
		Logo_SetPenColour(floor(rnd() * 17) + 1) 
		Logo_MoveForward(25) : Logo_Dot() 
		Logo_MoveBack(25) : Logo_RightTurn(4)
	next
	! Fill Body
	Logo_MoveBack(35) : Logo_SetPenColour(4)
	Logo_Fill()
	! Fill Legs
	Logo_MoveBack(30) : Logo_LeftTurn(90)
	Logo_MoveForward(30) : Logo_SetPenColour(7) 
	Logo_Fill() : Logo_MoveBack(60)
	Logo_Fill() : Logo_Render()
	! Draw Thrusters
	Logo_SetPenColour(3)
	Logo_ReSetAngle() : Logo_LeftTurn(90)
	Logo_MoveForward(80) : Logo_RightTurn(90)
	Logo_MoveForward(90) : Logo_LeftTurn(135)
	Logo_Forward(50) : Logo_LeftTurn(125)
	Logo_Forward(35)
	
	Logo_ReSetAngle() : Logo_Forward(40)
	Logo_RightTurn(90) : Logo_MoveForward(100)
	Logo_RightTurn(45) : Logo_Forward(50)
	Logo_RightTurn(125) : Logo_Forward(35)
	
	! Fill Thrusters 
	Logo_MoveBack(10): Logo_ReSetAngle()
	Logo_MoveForward(10) : Logo_LeftTurn(90)
	Logo_SetPenColour(5) : Logo_Fill()
	Logo_MoveForward(120) : Logo_Fill()
fn.end

fn.def TestColour(xx,yy)
   gr.get.bmpixel 1,xx, yy, alpha, red, green, blue
   test = red*65536 + green*256 + blue
   fn.rtn test
fn.end

!-------------------------------------------------
! Draw any shape
!-------------------------------------------------

fn.def AllShapes(shape, side, direction)
	for x = 1 to shape
		Logo_forward( side)
		if direction then Logo_RightTurn( 360 / shape) else : Logo_LeftTurn(360 / shape)
	next
fn.end

!-------------------------------------------------
! Circles on sticks 
!-------------------------------------------------

fn.def CirclesOnSticks(size, sides)
	call Heading(50, "Circles on sticks")
	Logo_TurtleHide()
	for x = 1 to 360 / sides 
		Logo_SetPenColour(floor(rnd() * 17 + 1))
		Logo_Forward(size)
		Logo_Circle(size/8)
		Logo_MoveBack(size)
		Logo_RightTurn(sides)
	next
	Logo_Circle(20)
	call WaitForTouch()
fn.end

!-------------------------------------------------
! Exploding Star
!-------------------------------------------------

fn.def ExplodingStar(size, loop)
	call Heading(50, "Exploding Star")
	Logo_SetPenSize(5) : Logo_TurtleShow()
	xf = size
	for x = 1 to loop
		Logo_SetPenColour(floor(rnd() * 17 + 1))
		Logo_Moveforward( xf)
		Logo_Dot()
		Logo_MoveBack( xf)
		Logo_RightTurn( xf)
		xf -= 1
	next
	call WaitForTouch()
fn.end

!-------------------------------------------------
! Random coloured ball
!-------------------------------------------------

fn.def Ball1(size)
	call Heading(50, "Random Coloured Ball")
	for y = 1 to size / 2
		for x=1 to 360 / size
			Logo_SetPenColour(floor(8 * rnd() + 1))
			Logo_forward(size)
			Logo_LeftTurn(size)
		next
		Logo_LeftTurn(20)
	next
	call WaitForTouch()
fn.end

!-------------------------------------------------
! Spiral Circle
!-------------------------------------------------

fn.def DrawStars(di_width, di_height)
	call Heading(50, "Stars")
	Logo_SetPenColour(7) : Logo_TurtleHide()
	Logo_PenUp()
	Logo_Jump(100,100) : Logo_PenDown() : call Star(150)
	Logo_PenUp()
	Logo_Jump(di_width - 100,100) : Logo_PenDown() : Logo_ReSetAngle() : call Star(150)
	Logo_PenUp()
	Logo_Jump(100,di_height - 100) : Logo_PenDown() : Logo_ReSetAngle() :  call Star(150)
	Logo_PenUp()
	Logo_Jump(di_width - 100, di_height - 100) : Logo_PenDown() : Logo_ReSetAngle() : call Star(150)
	Logo_PenUp()
	Logo_Jump((di_width / 2) + 50, (di_height / 2) + 100) : Logo_PenDown()
	Logo_SetPenColour(14) : Logo_ReSetAngle() : call Star(500)
	Logo_PenUp()
	Logo_Jump(200, 200) : Logo_PenDown() : Logo_SetPenColour(6) : Logo_Fill()
	call WaitForTouch()
fn.end

fn.def Star(size)
	radios = floor(size / 2)
	Logo_Moveforward(radios)
	for x = 1 to size / 10
		Logo_LeftTurn(size)
		Logo_forward(radios) 
	next
fn.end

!-------------------------------------------------
! Spiral Circle
!-------------------------------------------------

fn.def SpiralCircle(radios)
	call Heading(50, "Spiral Circle")
	increas = 10 
	Logo_RenderOff()
	Logo_TurtleHide()
	Logo_SetPenSize(4)
	for x = 1 to radios
		Logo_SetPenColour(floor(18 * rnd() + 1))
		Logo_Moveforward( increas) 
		Logo_Dot()
		Logo_MoveBack( increas)
		Logo_RightTurn( 20)
		increas += 0.8 
		if ! mod(x,20) then RD()
	next : Logo_RenderOn()
	Call WaitForTouch()
fn.end

!-------------------------------------------------
! Dotty Circle
!-------------------------------------------------

fn.def DottyCircle(radios , side)
	call Heading(50, "Circles made with Dots")
	Logo_RenderOff() : Logo_TurtleHide()
	for y=10 to 2 step - 1
		Logo_SetPenSize(y)
		for x = 1 to 360 / side
			Logo_SetPenColour(floor(18 * rnd() + 1))
			Logo_Moveforward(radios)
			Logo_Dot()
			Logo_MoveBack( radios)
			Logo_RightTurn(side)
		next
		Logo_Render()
		Logo_RightTurn(side / 2)
		radios -= y
	next : Logo_Render()
	call WaitForTouch()
fn.end

!-------------------------------------------------
! Polygon
!-------------------------------------------------

fn.def Polygon(side, angle, amt)
	call Heading(50, "Polygon")
	Logo_TurtleHide()
	Logo_SetPenColour(1)
	for x = 1 to 200
		if x = 50 : Logo_SetPenColour(4)
			elseif x = 100 : Logo_SetPenColour(6)
			elseif x = 150 : Logo_SetPenColour(9)
		endif
		Logo_forward(side)
		Logo_RightTurn(angle)
		side += amt
	next
	call WaitForTouch()
	Logo_TurtleShow()
fn.end

!-------------------------------------------------
! Spiral
!-------------------------------------------------

fn.def Spiral( segment, initLength, increment, angle, colour)
	call Heading(50, "Spiral") 
	Logo_TurtleShow()
	c = segment / 4
	length = initLength
	Logo_SetPenColour(7)
	for x = 1 to segment
		if colour then
			if x = c : Logo_SetPenColour(4)
				elseif x = c + c : Logo_SetPenColour(6)
				elseif x = c * 3 : Logo_SetPenColour(8)
			endif
		endif
		Logo_forward( length) 
		Logo_LeftTurn( angle)
		length += initLength
	next
	call WaitForTouch()
fn.end

!-------------------------------------------------
! Maze
!-------------------------------------------------

fn.def Maze(size)
	call Heading(50, "Maze")
	x = size : increase = size / 2
	Logo_RightTurn( 45)
	Logo_SetPenColour(1)
	for m = 1 to 20 
		if m = 5 then Logo_SetPenColour(4)
		if m = 10 then Logo_SetPenColour(5)
		if m = 15 then Logo_SetPenColour(6)
		Logo_forward( x) : x+=5
		Logo_RightTurn( 90)
		
		Logo_forward( x) : x += increase
		Logo_RightTurn( 90)
		
		Logo_forward( x) : x += increase
		Logo_RightTurn( 90)
		
		Logo_forward( x) : x += increase
		Logo_RightTurn( 90)
	next
	call WaitForTouch()
fn.end

!-------------------------------------------------
! Wiggly ball with random colours
!-------------------------------------------------

fn.def WigglyBall(size)
	call Heading(50, "Wiggly Ball with Random Colours")
	Logo_TurtleHide() 
	Logo_RenderOff()
	for y = 1 to 20
		for x=1 to 9
			Logo_SetPenColour(floor(17 * rnd() + 1))
			Logo_forward(size * 2)
			Logo_LeftTurn(size)
			Logo_Moveforward(size * 2)
			Logo_LeftTurn(size)
		next : RD()
		Logo_LeftTurn(20)
	next
	call WaitForTouch()
fn.end

!-------------------------------------------------
! Disks in diferent colours and sizes
!-------------------------------------------------
fn.def TwoDisks()
	call Disks(120,5)
	call Disks(60,7)
	call WaitForTouch()
fn.end

fn.def Disks(size, c)
	call Heading(50, "Disks in Diferent Colours and Sizes")
	Logo_RenderOff()
	Logo_TurtleHide()
	Logo_SetPenColour(c)
	for x = 1 to 360 / 10
		call AllShapes(6, size, 0)
		Logo_LeftTurn(10)
	next : Logo_Render()
fn.end

!-------------------------------------------------
! Flower
!-------------------------------------------------

fn.def Flower(style, size)
	!style 1 to 6, size 1 to 12
	call Logo_SetBgColour(13)
	call Heading(50, "Flower")
	call Logo_SetPenColour(3)
	Logo_PenUp() : Logo_SetPosX(150) : Logo_PenDown()
	Logo_RenderOff()
	Logo_TurtleHide()
	For x = 1 to 8 
		Logo_RightTurn(45)
		for y = 1 to style
			for z = 1 to 45
				Logo_Forward(size)
				Logo_RightTurn(4)
			next z : Logo_Render()
			Logo_RightTurn(90)
		next y : Logo_Render()
	next x 
	
	!Colour flower
	Logo_RenderOn()
	Logo_PenUp() : Logo_Jump(350,470) : Logo_PenDown()
	Logo_SetPenColour(17)
	Logo_Fill()
	Logo_ReSetAngle()
	
	Logo_SetPenColour(5)
	for x = 1 to 4 
		Logo_MoveForward(210)
		Logo_fill()
		Logo_MoveBack(210)
		Logo_RightTurn(90)
	next
	Logo_SetPenColour(7)
	Logo_RightTurn(45)
	for x = 1 to 4
		Logo_MoveForward(210)
		Logo_Fill()
		Logo_MoveBack(210)
		Logo_RightTurn(90)
	next
	Logo_SetPenColour(9)
	Logo_RightTurn(20)
	for x = 1 to 8
		Logo_MoveForward(170)
		Logo_Fill()
		Logo_MoveBack(170)
		Logo_RightTurn(45)
	next
	call WaitForTouch()
fn.end	

!-------------------------------------------------
! Boxes filled with colour
!-------------------------------------------------

fn.def SpinBoxes(shape, side, numBoxes)
	Logo_SetBgColour(17)
	Logo_SetPenColour(7)
	call Heading(50, "Boxes Filled with Colour")
	Logo_TurtleShow()
	for x = 1 to numBoxes
		call AllShapes(shape, side, 1)
		Logo_RightTurn(360 / numBoxes)
	next
	!Colour
	Logo_LeftTurn(numBoxes * 2)
	for x = 1 to numBoxes
		Logo_Moveforward(side / 2)
		Logo_SetPenColour(x)
		Logo_Fill()
		Logo_MoveBack(side / 2)
		Logo_RightTurn(360 / numBoxes)
	next
	!Colour
	Logo_SetPenColour(7)
	Logo_RightTurn(numBoxes * 4)
	for x = 1 to numBoxes
		Logo_Moveforward(side / 2)
		Logo_Fill()
		Logo_MoveBack(side / 2)
		Logo_RightTurn(360 / numBoxes)
	next
	call WaitForTouch()
fn.end

!-------------------------------------------------
! Analog Clock
!-------------------------------------------------

fn.def AnalogClock( cx, cy, size)
	call Heading(50, "Analog Clock")
	gr.text.align 2 : gr.text.draw nul,370,1000, "Tap to go on" : gr.render
	Logo_PenUp()
	Logo_Jump( cx, cy) :Logo_RenderOff() : Logo_TurtleHide()
	Logo_PenDown()
	call ClockFace( size)
	do
		time year$, month$, day$, hour$, minute$, second$
		hour = val(hour$) : minute = val(minute$) : second = val(second$)
		if hour = 0 then hour = 12 
		if hour > 12 then hour -= 12
		ClockHands( hour, minute, second, size)
		pause 100 : gr.touch t, tx, ty : RD()
	until t
	Logo_ClearScreen()
fn.end

fn.def ClockFace( size)
	m = 0 
	for x = 1 to 60
		if m = 5 then
			Logo_SetPenSize(8) : Logo_SetPenColour(1) : m = 0
		else 
			Logo_SetPenSize(2) : Logo_SetPenColour(7)
		endif
		m += 1
		if x = 1 then Logo_SetPenSize(8) : Logo_SetPenColour(1)
		Logo_Moveforward( size)
		Logo_Dot()
		Logo_MoveBack(size)
		Logo_RightTurn( 360 / 60)
	next
	
fn.end

fn.def ClockHands( hour, minute, second, size)
	m = 0 : ss = 0 : if second = 0 then ss = 1
	h = size / 3 : h += h
	for s = ss to 0 step - 1
		Logo_SetPenSize(8 + (s * 2))
		if minute > 1 then m = int(minute / 2)
		Logo_ReSetAngle()
		Logo_SetPenColour(5)
		if s = 1 then Logo_SetPenColour(0)
		Logo_RightTurn( hour * 30 + (m - s))
		Logo_forward( h + s)
		Logo_MoveBack( h + s)
	next
	call ClockMinute( minute, second, size)
	call ClockSeconed( second, size)
fn.end

fn.def ClockMinute( minute, second, size)
	ss = 0 : if second = 0 then ss = 1
	sh = floor(size / 4) : sh *= 3.3
	for s = ss to 0 step - 1
		Logo_SetPenSize(5 + (s * 2)) 
		Logo_ReSetAngle()
		Logo_SetPenColour(6)
		if s = 1 then Logo_SetPenColour(0)
		Logo_RightTurn( (minute - s) * 6)
		Logo_forward( sh + s)
		Logo_MoveBack( sh + s)
	next
fn.end

fn.def ClockSeconed(second, size)
	b = floor(size / 4)
	for s = 1 to 0 step - 1
		Logo_SetPenSize(2+s)
		Logo_ReSetAngle()
		Logo_SetPenColour(7)
		if s = 1 then Logo_SetPenColour(0) 
		Logo_RightTurn((second-s) * 6)
		Logo_MoveBack( b)
		Logo_forward( size + b/2)
		Logo_MoveBack( size - b + (b/2))
	next
fn.end

!-------------------------------------------------
! Wait until screen is taped 
!-------------------------------------------------

fn.def WaitForTouch()
	Logo_SetPenColour(3)
	gr.text.align 2 : gr.text.draw t, 360, 1080, "Tap to go on" : gr.render
	do : gr.touch t, tx, ty : until t : do : gr.touch t, tx, ty : until ! t
	Logo_ClearScreen()
fn.end

!-------------------------------------------------
! Heading for each demo
!-------------------------------------------------

fn.def Heading(y, h$)
	Logo_SetPenColour(3)
	gr.text.align 2 : gr.text.draw t, 360, y, h$ : gr.render
fn.end

return


