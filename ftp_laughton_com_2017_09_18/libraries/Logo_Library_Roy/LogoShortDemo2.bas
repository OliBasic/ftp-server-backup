Rem LogoLib Short Demo2
Rem With RFO Basic!
Rem July/August 2015
Rem Version 1.00
Rem By Roy

di_height = 1120 % set to my Device
di_width = 720

gr.open 255,0,0,0 % Black
gr.orientation 1
pause 1000
WakeLock 3 

gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y
gr.text.size 40
gr.set.AntiAlias 0

!------------------------------------------------
! Setup screen for drawing
!-------------------------------------------------
gr.bitmap.create screen, di_width, di_height
gr.bitmap.draw    nn, screen, 0, 0
include LogoLib.bas % *** IMPORTENT include 'LogoLib.bas' here ***
gr.bitmap.drawinto.start screen
!----------------------------------

gosub Functions % Let Basic see the Functions before they are called

!------------------------------------------------
! Draw a box, then change the angle (360 / 10). Do this 10 times
!-------------------------------------------------
do
	
	for loop = 0 to 1
		if loop then Logo_TurtleHide() : Logo_RenderOff()
		call Heading(50, "Draw 10 boxes at 10 deferent angels.")
		Logo_SetPenSize(4)
		for x = 1 to 10
			call Box(200)
			Logo_LeftTurn(360 / 10)
		next
		
		if loop then Logo_Render()
		
		call FillDesign()
		if Loop then 
			Logo_RenderOn() 
		else 
			Logo_SetPenColour(3)
			call Heading(1050,"Tap to Rerun Fast.")
			do : gr.touch t, tx, ty : until t : Logo_ClearScreen()
		endif
	next
	Logo_SetPenColour(3)
	call Heading(1000, "End of Short Demo.")
	call Heading(1050,"Tap to Rerun or Back Key to end.")
	do : gr.touch t, tx, ty : until t : Logo_ClearScreen()
until 0

onBackKey:

end "End of LogoLib short demo 2"

Functions:

!------------------------------------------------
! Draw a box
!-------------------------------------------------
fn.def Box(size)
	for x = 1 to 4
		logo_Forward(size)
		Logo_RightTurn(90)
	next
fn.end

!------------------------------------------------
! Fill the design with colours
!-------------------------------------------------
fn.def FillDesign()
	Logo_RightTurn(10)
	c = 4
	for x = 1 to 20 
		Logo_SetPenColour(c)
		Logo_MoveForward(100)
		!Logo_Circle(10) 
		Logo_fill()
		Logo_MoveBack(100)
		Logo_RightTurn(360 / 20)
		c += 1 : if c = 6 then c = 4
		if c = 4 then 
			Logo_SetPenColour(7)
			Logo_MoveForward(250) 
			!Logo_Circle(10) 
			Logo_Fill()
			Logo_MoveBack(250)
		endif
	next
	Logo_Circle(20)
	Logo_TurtleReSet()
fn.end

!------------------------------------------------
! Put information on screen
!-------------------------------------------------
fn.def Heading(y, h$)
	gr.text.align 2 : gr.text.draw t, 360, y, h$ : gr.render
fn.end

return