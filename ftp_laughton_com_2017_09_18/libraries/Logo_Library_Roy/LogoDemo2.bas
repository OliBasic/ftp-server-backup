Rem Logo Demo 2
Rem With RFO Basic!
Rem July/August 2015
Rem Version 1.01
Rem By Roy
!!
The Logo colours are:
0 = Black, 1 = Gray, 2 = Silver, 3 = White, 4 = Maroon, 5 = Red, 6 = Olive, 7 = Yellow,
8 = Green, 9 = Lime, 10 = Teal, 11 = Aqua, 12 = Navy, 13 = Blue, 14 = Purple, 
15 = Fuchsia, 16 = Brown, 17 = Gold.
!!

di_height = 720 % set to my Device
di_width = 1120

gr.open 255,0,0,0 % Black

gr.orientation 0
pause 1000
WakeLock 3 

gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y
gr.text.size 40
 
!-------------------------------------------------
! Setup screen for drawing and include LogoLib.bas
!-------------------------------------------------
gr.bitmap.create screen, di_width, di_height
gr.bitmap.draw    nn, screen, 0, 0
include LogoLib.bas %*** IMPORTANT Include LogoLib hare  ***
gr.bitmap.drawinto.start screen
!----------------------------------
if Logo_Lib$ <> "v1.01" then
	?"Please Update LogoLib.bas to 'v1.01'" 
	?"at: http://laughton.com/basic/programs/libraries/Logo_Library_Roy/"
	end
endif

gosub Functions % Let Basic see the Functions before they are called

do
	call Peltonwheel(0)
	call Peltonwheel(1)
	!------------------------------------------------
	call Pentagon()
	!------------------------------------------------
	call Hypercube(100)
	!-------------------------------------------------
	call FanFlower(100)
	!-------------------------------------------------
	call Forest()
	!-------------------------------------------------
	call CrossFractal() 
	!-------------------------------------------------
	call Hexagon(1, 50)
	call Hexagon(0, 10)
	!-------------------------------------------------
	call PineTreeSetUp()
	!-------------------------------------------------
	call StartBroccoli( 100, 8)
	!-------------------------------------------------
	call Scribble(di_width, di_height, 100)
	!-------------------------------------------------
	call RandomLines(di_width, di_height, 100)
	!-------------------------------------------------
	call Heading(50, "End of Demo two.")
	call Heading(100,"Tap to Rerun or Back Key to end.")
	do : gr.touch t, tx, ty : until t : Logo_ClearScreen()

until 0

onBackKey: 

end"End of LogoLib Demo Two"

Functions:
!-------------------------------------------------
! Scribble lines in random colours
!-------------------------------------------------
fn.def Scribble(di_width, di_height, numLines)
	call Heading(50, "Scribble")
	Logo_SetPenSize(1)
	for x = 1 to numLines 
		Logo_SetPenColour(floor(rnd() * 17) + 1)
		px = floor(rnd() * di_width) : py = floor(rnd() * di_height)
		Logo_Jump(px, py)
	next
	call WaitForTouch()
fn.end

!-------------------------------------------------
! Draw random stright lines in random colours
!-------------------------------------------------
fn.def RandomLines(di_width, di_height, numLines)
	call Heading(50, "Random Stright Lines")
	for x = 1 to numLines 
		Logo_SetPenColour(floor(rnd() * 17) + 1)
		px = floor(rnd() * di_width) : py = floor(rnd() * di_height)
		Logo_SetPosX(px) 
		Logo_SetPosY(py)
	next
	call WaitForTouch()
fn.end

!-------------------------------------------------
! Pentagon
!-------------------------------------------------
fn.def Pentagon()
	call Heading(50, "Pentagin")
	Logo_SetPenColour(8)
	Logo_TurtleHide() : Logo_RenderOff()
	for p = 100 to 20 step - 10 
		for y = 1 to 5 
			Logo_SetPenColour(y * 2)
			for z = 1 to 5 
				Logo_Forward(p) 
				Logo_LeftTurn(72)
			next 
			Logo_LeftTurn(72)
		next : Logo_Render()
	next
	call WaitForTouch()
fn.end 

!-------------------------------------------------
! Hypercube
!-------------------------------------------------
fn.def Hypercube(size)
	Call Heading(50, "Hypercube")
	Logo_SetPenSize(2)
	Logo_SetPosX(450)
	for x= 1 to 8
		for y = 1 to 4
		Logo_RightTurn(90)
		Logo_Forward(size)
	next
	Logo_MoveBack(size)
	Logo_LeftTurn(45)	
	next
	call WaitForTouch()
fn.end
!-------------------------------------------------
! Fan Flower
!-------------------------------------------------
fn.def FanFlower(size)
	Logo_SetPenSize(1)
	Logo_SetBgColour(4)
	call Heading(50, "Fan Flower")
	Logo_RenderOff()
	Logo_SetPosY(300)
	 for x= 1 to 12
		if x = 6 then Logo_SetPenColour(7)
		for y = 1 to 75
			Logo_Forward(size)
			Logo_MoveBack(size)
			Logo_RightTurn(2)
		next : Logo_Render()
		f = size * 2 + (size/2)
		Logo_Forward(f)
	next
	call WaitForTouch()
fn.end
!-------------------------------------------------
! Fractal Broccoli
!-------------------------------------------------
fn.def StartBroccoli(x, y)
	Logo_SetPenColour(5) : Logo_SetBgColour(13) 
	Logo_TurtleHide() : Logo_RenderOff() : Logo_MoveBack(200)
	call Heading(50, "Fractal Broccoli")
	call Broccoli(100,8)
	Logo_Render()
	call WaitForTouch()
fn.end

fn.def Broccoli(x, y)
	if x < y then Logo_Render()
	if x < y then fn.end 
	call Logo_Square(x)
	Logo_Forward(x)
	Logo_LeftTurn(45) 
	call Broccoli(x/sqr(2),y)
	Logo_RightTurn(90) 
	Logo_MoveForward(x/sqr(2))
	call Broccoli(x/sqr(2),y)
	Logo_MoveBack(x/sqr(2)) 
	Logo_LeftTurn(45) 
	Logo_Back(x)
fn.end

fn.def Logo_Square(x) 
	for r = 1 to 4 : Logo_Forward(x) :  Logo_RightTurn(90) : next
fn.end

!-------------------------------------------------
! Hexagon
!-------------------------------------------------

fn.def Hexagon(fill, shape)
call Heading(50, "Hexagon")
Logo_RenderOff()
	for h = 100 to 30 step - shape
		for x = 1 to 6 
			for y = 1 to 6 
				Logo_Forward(h) 
				Logo_LeftTurn(60) 
			next : Logo_Render()
			Logo_LeftTurn(60) 
		next 
	next 
	if fill then 
	Logo_LeftTurn(30)
		for x = 1 to 6
		
		Logo_MoveForward(30)
		Logo_SetPenColour(5) : Logo_Fill()
		Logo_MoveForward(100)
		Logo_SetPenColour(14) : Logo_Fill()
		Logo_MoveBack(130)
		Logo_LeftTurn(60)
	next
	Logo_LeftTurn(30)
	Logo_SetPenColour(7)
	for x = 1 to 6 
		Logo_MoveForward(130) : Logo_Fill()
		Logo_MoveBack(130) : Logo_LeftTurn(60)
	next
	endif
	Logo_TurtleHide()
call WaitForTouch()
fn.end

!-------------------------------------------------
! Peltonwheel
!-------------------------------------------------
fn.def Peltonwheel(style) 
	Logo_SetBgColour(17)
	Logo_TurtleHide() : RF()
	call Heading(50, "Peltonwheel")
	Logo_SetPenSize(1)
	Logo_SetPenColour(0)
	for p = 0 to 220 step 1
		if style then 
			Logo_MoveForward(p) 
			Logo_SetPenColour(floor(rnd() * 10) + 4)
			Logo_Circle(5)
		else
			Logo_Forward(p)
		endif
		Logo_MoveBack(p) 
		Logo_RightTurn(51)
	next 
	call WaitForTouch()
fn.end

!-------------------------------------------------
! Pine Tree
!-------------------------------------------------

fn.def PineTreeSetUp()
	Logo_SetBgColour(10)
	Logo_TurtleHide()
	call Heading(50, "Pine Tree")
	Logo_RenderOff()
	Logo_MoveBack(300)
	call PineTree(8, 100)
	call WaitForTouch()
fn.end 

fn.def PineTree(leaves ,size )
	if leaves = 0 then call Pins() : Logo_Render()
	if leaves = 0 then fn.end	
	Logo_SetPenColour(16)
	s = 2 * (leaves - 1) : if s < 1 then s = 1
	Logo_SetPenSize(s)
	Logo_Forward(size)
	Logo_RightTurn(45) 
	call PineTree (leaves - 1 ,size * 0.7)
	Logo_LeftTurn(45) 
	Logo_Forward(size) 
	Logo_LeftTurn(30) 
	call PineTree (leaves - 1 ,size * 0.5 )
	Logo_RightTurn(30)
	Logo_Back(size * 2 )
	Logo_SetPenSize(2 * (leaves + 1))
fn.end

fn.def Pins() 
	c = int(rnd() * (3 + 1))
	Logo_SetPenColour(6 + c) 
	!if int(rnd() * 2) = 1 then Logo_SetPenColour(7)
	Logo_SetPenSize(2) 
	Logo_Forward(10)
	Logo_LeftTurn(30) 
	Logo_SetPenSize(1) 
	for r = 1 to 6 : Logo_Forward(10) : Logo_Back(10) : Logo_RightTurn(12) : next 
	Logo_SetPenSize(2) 
	Logo_LeftTurn(42) 
	Logo_Back(10) 
	Logo_SetPenColour(16)
fn.end

!-------------------------------------------------
! Forest
!-------------------------------------------------

fn.def Forest()
	Logo_TurtleHide() : Logo_SetBgColour(1) 
	call Heading(50, "Forest")
	Logo_RenderOff() : Logo_SetPenColour(6)
	!call Sycamore1( 150) : Logo_Render()
	Logo_SetPenColour(7)
	call Tree( 150, 700, 120, 20, 6) : Logo_Render()
	Logo_SetPenColour(8)
	call Tree( 700, 600,  80, 25, 6) : Logo_Render()
	Logo_SetPenColour(9)
	call Tree( 1020, 580, 110, 27, 7) : Logo_Render()
	Logo_SetPenColour(2)
	call Tree( 300, 180, 20, 27, 7) : Logo_Render()
	Logo_SetPenColour(17)
	call Sycamore1( 100) : Logo_Render()
	Logo_Wait(120) : call FadeToNight()
	call WaitForTouch()
fn.end

fn.def Sycamore1(x)
	Logo_PenUp() : Logo_Jump(560,700) : Logo_PenDown()
	call Sycamore2(x)
fn.end

fn.def Sycamore2(x) 
	if x < 12 then fn.end
	Logo_Forward(x)
	Logo_RightTurn(25) : call Sycamore2( x * 0.7)
	Logo_LeftTurn(25) : call Sycamore2( x * 0.6)
	Logo_LeftTurn(28) : call Sycamore2( x * 0.7) : if int(rnd() * 20) = 10 then Logo_Render()
	Logo_RightTurn(28)
	Logo_Back(x) 
fn.end

fn.def Tree(x, y, u, a, s) % draws tree1 at position (x,y) 
	Logo_PenUp() : Logo_Jump(x, y) : Logo_PenDown()
	call Tree1(u ,a ,s)
fn.end

fn.def Tree1( u ,a ,s) % note - draws tree in current cursor position with size :u, angle :a, stage :s 
	if s = 0 then fn.end
	Logo_Forward(u) 
	Logo_RightTurn(a)
	r = int(rnd() * 10)
	call Tree1(u *(0.5 + 0.04 * r) ,a ,s - 1)
	Logo_LeftTurn(2 * a )
	r = int(rnd() * 10)
	call Tree1(u*(0.5 + 0.04 * r) ,a ,s - 1)
	Logo_RightTurn(a) 
	Logo_Back(u)
fn.end

fn.def FadeToNight()
	for x = 1 to 25 step 1
		Logo_SetBG(x, 0,0,0, 1)
	next
fn.end
!-------------------------------------------------
! Cross Fractal
!-------------------------------------------------

fn.def CrossFractal() 
	Logo_SetBgColour(6)
	call Heading(50, "Cross Fractal")
	Logo_RenderOff()
	Logo_TurtleHide()
	for x = 1 to 4 
		call Shape(75, 2) 
		Logo_RightTurn(90) 
		Logo_Render()
	next
	Logo_RightTurn(45)
	Logo_MoveForward(50)
	Logo_SetPenColour(17)
	Logo_Fill()
	call WaitForTouch()
fn.end

fn.def Shape(size, level) % Cross fractal
	if level < 1 then Logo_Forward(size)
	if level < 1 then fn.end
	call Shape (size/3 ,level-1) : Logo_LeftTurn(90)
	call Shape (size/3 ,level-1) : Logo_LeftTurn(90)
	call Shape (size/3 ,level-1) : Logo_RightTurn(90)
	call Shape (size/3 ,level-1) : Logo_RightTurn(90)
	call Shape (size/3 ,level-1) : Logo_LeftTurn(90)
	call Shape (size/3 ,level-1) : Logo_RightTurn(90)
	call Shape (size/3 ,level-1) : Logo_RightTurn(90)
	call Shape (size/3 ,level-1) : Logo_LeftTurn(90)
	call Shape (size/3 ,level-1) : Logo_RightTurn(90)
	call Shape (size/3 ,level-1) : Logo_RightTurn(90)
	call Shape (size/3 ,level-1) : Logo_LeftTurn(90)
	call Shape (size/3 ,level-1) : Logo_LeftTurn(90)
	call Shape (size/3 ,level-1) 
fn.end

!-------------------------------------------------
! Wait until screen is taped 
!-------------------------------------------------

fn.def WaitForTouch()   
	gr.color 255, 255, 255, 255, 1
	gr.text.align 2 : gr.text.draw t, 560, 680, "Tap to go on" : gr.render
	do : gr.touch t, tx, ty : until t : do : gr.touch t, tx, ty : until ! t
	Logo_ClearScreen()
fn.end

!-------------------------------------------------
! Heading for each demo
!-------------------------------------------------

fn.def Heading(y, h$)
	gr.text.align 2 : gr.text.draw t, 560, y, h$ : gr.render
fn.end


return