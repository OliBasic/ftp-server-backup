Rem Logo Lib
Rem With RFO Basic!
Rem July/August 2015
Rem Version 1.01
Rem By Roy Shepherd

Logo_Lib$ = "v1.01"
!!
This is my 'LogoLib.bas' include file.
It adds Logo graphic commands to RFO-BASIC!

------------------------------------------------------------------------------------------------------------------
Logos  Default Settings
------------------------------------------------------------------------------------------------------------------
"
Logo starts off from the centre of the screen.
The angle  is set to 0 pointing straight up.
Pen is down.
Pen size is 2
Turtle is down
Pen colour is white.
Background is black.
Rendering is on.

The Logo colours are:
0 = Black, 1 = Grey, 2 = Silver, 3 = White, 4 = Maroon, 5 = Red, 6 = Olive, 7 = Yellow,
8 = Green, 9 = Lime, 10 = Teal, 11 = Aqua, 12 = Navy, 13 = Blue, 14 = Purple, 
15 = Fuchsia, 16 = Brown, 17 = Gold.
"
------------------------------------------------------------------------------------------------------------------
Pen and Colour Commands
------------------------------------------------------------------------------------------------------------------
"
Logo_Pen() or PU()
Lifts the 'pen' from the screen so that moving the turtle doesn't draw a line.
Example: PU()

Logo_PenDown() or PD()
Puts the pen down so that moving the turtle draws a line
Example: PD()

Logo_SetPenSize(n) or PS(n)
Sets the width of the pen to n pixels.
Example: Logo_SetPenSize(5)

Logo_SetPenColour(c) or SC(c)
Sets the pen colour to one of the 18 Logo colours.
Example: Logo_SetPenColour(5)  Set colour to Red

Logo_SetPC(Alpha, Red, Green, Blue, Style) or PC(Alpha, Red, Green, Blue, Style). (All number must be entered).
Sets the pen colour to the appropriate RGB (Alpha, Red, Green, Blue) values,
where Alpha, Red, Green, Blue are numbers that range from 0 to 255. Style as a number range between 0 and 2
Alpha sets the transparency.
Example: Logo_SetPC(255,255 0 0, 1) (Sets pen colour to red)

Logo_SetBG(Alpha, Red, Green, Blue, Style) or BG(Alpha, Red, Green, Blue, Style).(All number must be entered).
Clears the screen to the colour set by (Alpha, Red, Green, Blue, Style) values,
where Alpha, Red, Green, Blue are numbers that range from 0 to 255. Style as a number range between 0 and 2. Alpha sets the transparency.
 Logo will be set to it’s default settings, with the exception of the pen colour, and the turtle they will stay the same as they are.
Example: Logo_SetBG(255,0 0 255, 1) (Clears screen to Blue)
 
Logo_SetBgColour(c) or BC(c)
Clears the screen to one of the 18 Logo colours and reset Logo to its default settings,
with the exception of the pen colour, and the turtle they will stay the same as they are.
Example: Logo_SetBgColour(14) Clears the screen to the colour Purple.

Logo_ClearScreen() or CS()
Clears the screen and reset Logo to its default settings
Example: Logo_ClearScreen() 

Logo_Fill() or FL()
Floods the area bounded by lines with whatever colour was specified in the Logo_SetPenColour(c) or Logo_SetPC(a, r g , b) command.
Example: FL()
"
------------------------------------------------------------------------------------------------------------------
Draw and Move commands
------------------------------------------------------------------------------------------------------------------
"
Logo_Forward(x) or FD(x)
Draw forward x pixels
Example: FD(100)

Logo_Back(x) or BK(x)
Draw Backward x pixels
Example: BK(100)

Logo_MoveForward(x) or MF(x)
Move  forward x pixels (Does not draw)
Example: MF(100)

Logo_MoveBack(x) or MB(x)
Move  back x pixels (Does not draw)
Example: MB(100)

Logo_Dot() or DT()
Puts a dot on the screen at the current position in the size of the current pen size.
Example: Logo_Dot()

Logo_Circle(radius) or CL(radius)
Draws a circle on the screen at the current position in the size of radius.
Example: Logo_Circle(100)

Logo_LeftTurn(x) or LT(x)
Rotate the turtle x degrees left
Example: LT(45)

Logo_RightTurn(x) or RT(x)
Rotate the turtle x degrees right.
Example: RT(45)
"
------------------------------------------------------------------------------------------------------------------
Turtle and Position commands
------------------------------------------------------------------------------------------------------------------
"
Logo_TurtleHide() or TH()
Hides the turtle. (arrow)
Example: Logo_TurtleHide() 

Logo_TurtleShow() or TS()
Shows the turtle. (arrow)
Example: TS()

Logo_Jump(x, y) or JP(x, y)
Sets the absolute x and y position of the turtle.
If the pen is down a line will be drawn to x and y.
If the pen is up, no line will be drawn.
Example: Logo_Jump(200, 300) Sets the turtle x = 200 and y = 300

Logo_ReSetAngle() or RA()
Resets the angle to 0, pointing straight up
Example: Logo_ReSetAngle()  

Logo_GetPosX() or GX()
Returns x position of the turtle.
Example: x = Logo_GetPosX() 

Logo_GetPosY() or GY()
Returns y position of the turtle.
Example: y = Logo_GetPosY() 

Logo_SetPosX(x)  or SX(x)
Sets the absolute x position of the turtle.
If the pen is down a line will be drawn to x.
If the pen is up, no line will be drawn.
Example:Logo_SetPosX(250)  Sets the turtle x = 250.

Logo_SetPosY(y) or SY(y)
Sets the absolute y position of the turtle.
If the pen is down a line will be drawn to y.
If the pen is up, no line will be drawn.
Example: Logo_SetPosY(350)  Sets the turtle y = 350.

Logo_TurtleHome() or TM()
Sends the turtle to the center of the screen,
and resets the angle to 0, pointing straight up
If the pen is down a line will be drawn to the center of the screen,
If the pen is up, no line will be drawn.
Exampl: Logo_TurtleHome()

Logo_TurtleReSet() or TR()
To speed up drawing, calls to Logo_LeftTurn(n), Logo_RightTurn(n), 
Logo_MoveBack(n) and Logo_MoveForward(n), does not move the turtle (arrow).

This means that if your last command to the LogoLib where any of the above,
the turtle will still be in the place it was before the last commands.
You can use Logo_TurtleReset() to place the turtle in the right position.
Exampl: Logo_TurtleReset() 
"
------------------------------------------------------------------------------------------------------------------
Save Screen command
------------------------------------------------------------------------------------------------------------------
"
Logo_SaveScreen() or SS()
Saves the screen to a file, in folder 'LogoDrawings'.
The file name is chosen using the time function and as the 'png' extension.
Example: Logo_SaveScreen()
"
------------------------------------------------------------------------------------------------------------------
Programing commands
------------------------------------------------------------------------------------------------------------------
"
Logo_Wait(n) or WT(n)
Pauses for the specified amount of time, measured in 1/60 seconds,
before executing the next command. So, if wait is 60, the program will pause for 1 second.
Example: Logo_Wait(30) (half a second)

Logo_RenderOff() or RF()
Turns off graphic rendering.
Drawings will be done much faster, but you will not see anything until you use Logo_Render() or RD()
Example: Logo_RenderOF()

Logo_RenderOn() or RN()
Turns on graphic rendering.
See your drawing as it is drawn.
Example: RN()

Logo_Render() or RD()
Renders drawings to screen. 
Use after Logo_RenderOff() when you want to show your drawings.
Example: Logo_Render()
"
------------------------------------------------------------------------------------------------------------------
Speed up your programs
------------------------------------------------------------------------------------------------------------------
"
To speed up your drawing turn off rendering with Logo_RenderOff() and
hide the turtle with Logo_TurtleHide()
To see your drawing in steps use Logo_Render() every so often, say at the end 
of a for next loop. There are many examples of using this method in the demos.
To turn rendering back on use Logo_RenderOn(). 
To show the turtle use Logo_TurtleShow(). 
"
------------------------------------------------------------------------------------------------------------------
Using Fill
------------------------------------------------------------------------------------------------------------------
"
When using the Logo_Fill() command, if you are not sure your calculations are correct,
then use Logo_Circle(5) to show a dot where the fill will start. If it’s not in the right
place then recalculate. When you’ve got it right, comment out Logo_Circle(n) and put in
the Logo_Fill() Command.

There is an example of using this method in LogoShortDemo2.bas

The gr.set.AntiAlias command is set to on by default and the drawing of lines
is smoother with it on, however Logo_Fill() fills an area better if it has been drawn
with gr.set.AntiAlias 0 (off). It is up to you to decide which to use.
To turn gr.set.AntiAlias on, use gr.set.AntiAlias 1 and off with  gr.set.AntiAlias 0.
"
------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------
!!

!-----------------------------------------
!Start of LogoLib
!-----------------------------------------

! 'global' bundle pointer = 1
bundle.create global  
bundle.put global, "SetX", di_width / 2 % Start at centre screen
bundle.put global, "SetY", di_height / 2
bundle.put global, "Device_W", di_width
bundle.put global, "Device_H", di_height
bundle.put global, "Turn", 0 % is the angle (pointing up)
bundle.put global, "Pen", 1 % Pen is down
bundle.put global, "Screen", screen % To use Flood Fill
bundle.put global, "Render", 1 % Rendering on, turn off for fast drawing
bundle.put global, "Rotate", 2 % myArrowRotObj pointer
bundle.put global, "Arrow", 3 % arrow pointer
bundle.put global, "Turtle", 1 % Turtle is down
! Colours
bundle.put global, "Alpha", 255 % sets the transparency 
bundle.put global, "Red",   255
bundle.put global, "Green", 255
bundle.put global, "Blue",  255 % all 255 white
bundle.put global, "Style", 1 % style 1 will be filled 0 not filled

!----------------------------------------------------------------
! Pen and Colour Commands
!----------------------------------------------------------------

fn.def SC(c) : Logo_SetPenColour(c) : fn.end
fn.def Logo_SetPenColour(c)
	if c = 0 : Alpha = 255 : Red = 0 : Green = 0 : Blue = 0 : Style = 1                % Black
		elseif c = 1 : Alpha = 255 : Red = 128 : Green = 128 : Blue = 128 : Style = 1  % Gray 
		elseif c = 2 : Alpha = 255 : Red = 192 : Green = 192 : Blue = 192 : Style = 1  % Silver
		elseif c = 3 : Alpha = 255 : Red = 255 : Green = 255 : Blue = 255 : Style = 1  % White
		elseif c = 4 : Alpha = 255 : Red = 128 : Green = 0 : Blue = 0 : Style = 1      % Maroon 
		elseif c = 5 : Alpha = 255 : Red = 255 : Green = 0 : Blue = 0 : Style = 1      % Red
		elseif c = 6 : Alpha = 255 : Red = 128 : Green = 128 : Blue = 0 : Style = 1    % Olive
		elseif c = 7 : Alpha = 255 : Red = 255 : Green = 255 : Blue = 0 : Style = 1    % Yellow
		elseif c = 8 : Alpha = 255 : Red = 0 : Green = 128 : Blue = 0 : Style = 1      % Green
		elseif c = 9 : Alpha = 255 : Red = 0 : Green = 255 : Blue = 0 : Style = 1      % Lime
		elseif c = 10 : Alpha = 255 : Red = 0 : Green = 128 : Blue = 128 : Style = 1   % Teal
		elseif c = 11 : Alpha = 255 : Red = 0 : Green = 255 : Blue = 255 : Style = 1   % Aqua 
		elseif c = 12 : Alpha = 255 : Red = 0 : Green = 0 : Blue = 128 : Style = 1     % Navy
		elseif c = 13 : Alpha = 255 : Red = 0 : Green = 0 : Blue = 255 : Style = 1     % Blue
		elseif c = 14 : Alpha = 255 : Red = 128 : Green = 0 : Blue = 128 : Style = 1   % Purple 
		elseif c = 15 : Alpha = 255 : Red = 255 : Green = 0 : Blue = 255 : Style = 1   % Fuchsia  
		elseif c = 16 : Alpha = 255 : Red = 199 : Green = 97 : Blue = 20 : Style = 1   % Brown 
		elseif c = 17 : Alpha = 255 : Red = 218 : Green = 165 : Blue = 32 : Style = 1  % Gold 
	endif
	gr.color Alpha, Red, Green, Blue, Style
	bundle.put 1, "Alpha", Alpha 
	bundle.put 1, "Red",   Red
	bundle.put 1, "Green", Green
	bundle.put 1, "Blue",  Blue 
	bundle.put 1, "Style", Style
fn.end

fn.def PU() : Logo_PenUp() : fn.end
fn.def Logo_PenUp()
	bundle.put 1, "Pen", 0
fn.end

fn.def PD() : Logo_PenDown() : fn.end
fn.def Logo_PenDown()
	bundle.put 1, "Pen", 1
fn.end

fn.def PS(s) : Logo_SetPenSize(s) : fn.end
fn.def Logo_SetPenSize(s)
	gr.set.stroke s
fn.end

fn.def CS() : Logo_ClearScreen() : fn.end
fn.def Logo_ClearScreen()
	bundle.get 1, "Device_W", w 
	bundle.get 1, "Device_H", h
	Logo_SetPenColour(0)
	gr.rect r, 0, 0, w, h : gr.render
	Logo_SetPenColour(3)
	Logo_SetPenSize(2)
	call ReSetBundgle(1)
	Logo_TurtleShow()
fn.end

fn.def BC(c) : Logo_SetBgColour(c) : fn.end
fn.def Logo_SetBgColour(c)
	bundle.get 1, "Alpha", Alpha 
	bundle.get 1, "Red",   Red
	bundle.get 1, "Green", Green
	bundle.get 1, "Blue",  Blue 
	bundle.get 1, "Style", Style
	bundle.get 1, "Turtle", turtle
	bundle.get 1, "Device_W", w
	bundle.get 1, "Device_H", h
	Logo_SetPenColour(c)
	gr.rect r, 0, 0, w, h : gr.render
	call ReSetBundgle(0)
	if turtle then Logo_TurtleShow()
	gr.color Alpha, Red, Green, Blue, Style : gr.render
fn.end

fn.def PC(Alpha, Red, Green, Blue, Style) : Logo_SetPC(Alpha, Red, Green, Blue, Style) : fn.end
fn.def Logo_SetPC(Alpha, Red, Green, Blue, Style)
	bundle.put 1, "Alpha", Alpha 
	bundle.put 1, "Red",   Red
	bundle.put 1, "Green", Green
	bundle.put 1, "Blue",  Blue 
	bundle.put 1, "Style", Style
	gr.color Alpha, Red, Green, Blue, Style
fn.end

fn.def BG(Alpha, Red, Green, Blue, Style) : Logo_SetBG(Alpha, Red, Green, Blue, Style) : fn.end 
fn.def Logo_SetBG(Alpha, Red, Green, Blue, Style)
	bundle.get 1, "Alpha", pen_Alpha 
	bundle.get 1, "Red",   pen_Red
	bundle.get 1, "Green", pen_Green
	bundle.get 1, "Blue",  pen_Blue 
	bundle.get 1, "Style", pen_Style
	bundle.get 1, "Turtle", turtle
	bundle.get 1, "Device_W", w
	bundle.get 1, "Device_H", h
	gr.color Alpha, Red, Green, Blue, Style
	gr.rect r, 0, 0, w, h : gr.render
	call ReSetBundgle(0)
	if turtle then Logo_TurtleShow()
	gr.color pen_Alpha, pen_Red, pen_Green, pen_Blue, pen_Style : gr.render
fn.end

fn.def FL() : Logo_Fill() : fn.end
fn.def Logo_Fill()
	bundle.get 1, "SetX", FillX
	bundle.get 1, "SetY", FillY
	bundle.get 1, "Render", render
	bundle.get 1, "Screen", screen
	bundle.get 1, "Device_W", w
	bundle.get 1, "Device_H", h
	if FillX > - 1 & FillY > - 1 & FillX < w & FillY < h then
		gr.bitmap.fill screen ,FillX, FillY 
		if render then gr.render
	endif
	call ShowArrow()

fn.end

!----------------------------------------------------------------
! Draw and Move commands
!----------------------------------------------------------------

fn.def LT(turnLeft) : Logo_LeftTurn(turnLeft) : fn.end
fn.def Logo_LeftTurn(turnLeft)
    bundle.get 1, "Turn", turn
	turn -= turnLeft
	bundle.put 1, "Turn", turn
	!call ShowArrow() % not calling to speed thing up
fn.end

fn.def RT(turnRight) : Logo_RightTurn(turnRight) : fn.end
fn.def Logo_RightTurn(turnRight)
	bundle.get 1, "Turn", turn
	turn += turnRight
	bundle.put 1, "Turn", turn
	!call ShowArrow()
fn.end

fn.def RA() : Logo_ReSetAngle() : fn.end
fn.def Logo_ReSetAngle()
	bundle.put 1, "Turn", 0
	call ShowArrow()
fn.end

fn.def BK(goBack) : Logo_Back(goBack) : fn.end
fn.def Logo_Back(goBack)
	bundle.get 1, "SetX", Xorigin
	bundle.get 1, "SetY", Yorigin
	bundle.get 1, "Turn", turn : bundle.get 1, "Pen", pen
	x = Xorigin + goBack*cos(toradians(turn+90))
	y = Yorigin + goBack*sin(toradians(turn+90))
	if pen then gr.line l, Xorigin,Yorigin, x , y
	bundle.put 1, "SetX", x : bundle.put 1, "SetY", y
	call ShowArrow()
fn.end

fn.def MB(goBack) : Logo_MoveBack(goBack) : fn.end
fn.def Logo_MoveBack(goBack)
	bundle.get 1, "SetX", Xorigin
	bundle.get 1, "SetY", Yorigin
	bundle.get 1, "Turn", turn
	x = Xorigin + goBack*cos(toradians(turn+90))
	y = Yorigin + goBack*sin(toradians(turn+90))
	bundle.put 1, "SetX", x : bundle.put 1, "SetY", y
	!call ShowArrow()
fn.end
  
fn.def FD(goForward) : Logo_forward(goForward) : fn.end  
fn.def Logo_Forward(goForward)
	bundle.get 1, "SetX", Xorigin
	bundle.get 1, "SetY", Yorigin
	bundle.get 1, "Turn", turn : bundle.get 1, "Pen", pen
	x = Xorigin - goForward*cos(toradians(turn+90))
	y = Yorigin - goForward*sin(toradians(turn+90))
	if pen then gr.line l, Xorigin,Yorigin, x , y 
	bundle.put 1, "SetX", x : bundle.put 1, "SetY", y
	call ShowArrow()
fn.end

fn.def MF(goForward) : Logo_Moveforward(goForward) : fn.end
fn.def Logo_MoveForward(goForward)
	bundle.get 1, "SetX", Xorigin
	bundle.get 1, "SetY", Yorigin
	bundle.get 1, "Turn", turn : bundle.get 1, "Pen", pen
	x = Xorigin - goForward*cos(toradians(turn+90))
	y = Yorigin - goForward*sin(toradians(turn+90))
	bundle.put 1, "SetX", x : bundle.put 1, "SetY", y
	!call ShowArrow()
fn.end

fn.def DT() : Logo_Dot() : fn.end
fn.def Logo_Dot()
	bundle.get 1, "Pen", pen
	if pen then gr.point p, GX(), GY()
	call ShowArrow()
fn.end

fn.def CL(radius) : Logo_Circle(radius) : fn.end
fn.def Logo_Circle(radius)
	bundle.get 1, "Pen", pen
	if pen then gr.circle nul, Logo_GetPosX(), Logo_GetPosY(), radius
	call ShowArrow()
fn.end 

!------------------------------------------------------------
! Turtle and Position commands
!------------------------------------------------------------

fn.def TH() : Logo_TurtleHide() : fn.end
fn.def Logo_TurtleHide()
	bundle.put 1, "Turtle", 0
	bundle.get 1, "Arrow", arrow
	gr.hide arrow
	gr.render
fn.end

fn.def TS() : Logo_TurtleShow() : fn.end
fn.def Logo_TurtleShow()
	bundle.put 1, "Turtle", 1
	bundle.get 1, "Arrow", arrow
	bundle.get 1, "Render", render
	gr.show arrow
	call ShowArrow() : if render then gr.render
fn.end

fn.def JP(jumpX, jumpY) : Logo_Jump(jumpX, jumpY) : fn.end
fn.def Logo_Jump(jumpX, jumpY)
	bundle.get 1, "Pen", pen
	if pen then gr.line null, Logo_GetPosX(), Logo_GetPosY(), JumpX, JumpY
	bundle.put 1, "SetX", jumpX
	bundle.put 1, "SetY", jumpY
	call ShowArrow()
fn.end

fn.def SX(posX) : Logo_SetPosX(posX) : fn.end 
fn.def Logo_SetPosX(posX)
	bundle.get 1, "Pen", pen
	if pen then gr.line null, Logo_GetPosX(), Logo_GetPosY(), posX, Logo_GetPosY()
	bundle.put 1, "SetX", posX
	call ShowArrow()
fn.end

fn.def SY(posY) : Logo_SetPosY(posY) : fn.end 
fn.def Logo_SetPosY(posY)
	bundle.get 1, "Pen", pen
	if pen then gr.line null, Logo_GetPosX(), Logo_GetPosY(), Logo_GetPosX(), posY
	bundle.put 1, "SetY", posY
	call ShowArrow()
fn.end

fn.def GX() : bundle.get 1, "SetX", posX : fn.rtn posX : fn.end 
fn.def Logo_GetPosX()
	bundle.get 1, "SetX", posX
fn.rtn posX
fn.end

fn.def GY() : bundle.get 1, "SetY", posY : fn.rtn posY : fn.end 
fn.def Logo_GetPosY()
	bundle.get 1, "SetY", posY
fn.rtn posY
fn.end

fn.def TM() : Logo_TurtleHome() : fn.end
fn.def Logo_TurtleHome()
	bundle.get 1, "Pen", pen
	bundle.get 1, "Device_W", w
	bundle.get 1, "Device_H", h
	if pen then gr.line null, Logo_GetPosX(), Logo_GetPosY(), w / 2, h / 2
	bundle.put 1, "SetX", w / 2
	bundle.put 1, "SetY", h / 2
	Logo_ReSetAngle()
	call ShowArrow()
fn.end

fn.def TR() : Logo_TurtleReset() : fn.end
fn.def Logo_TurtleReset() 
	call ShowArrow() 
fn.end

!----------------------------------------------------------------
! Save Screen command
!----------------------------------------------------------------

fn.def SS() : Logo_SaveScreen() : fn.end 
fn.def Logo_SaveScreen() 
	bundle.get 1, "Screen", screen
	file.exists pathPresent,"../../LogoDrawings/"
	if ! pathPresent then File.mkdir"../../LogoDrawings/"	
	Path$="../../LogoDrawings/"
	drawing$ = mid$(str$(TIME()),3) + ".png"
	gr.bitmap.save screen, Path$ + drawing$
	popup "Drawing Saved"
fn.end

!----------------------------------------------------------------
! Programing commands
!----------------------------------------------------------------

fn.def WT(wait) : Logo_Wait(wait) : fn.end
fn.def Logo_Wait(wait)
	pause wait * 16.66
fn.end

fn.def RN() : Logo_RenderOn() : fn.end
fn.def Logo_RenderOn()
	bundle.put 1, "Render", 1 
	gr.render
fn.end

fn.def RF() : Logo_RenderOff() : fn.end
fn.def Logo_RenderOff()
	bundle.put 1, "Render", 0 
fn.end

fn.def RD() : Logo_Render() : fn.end
fn.def Logo_Render() 
	gr.render 
fn.end

!-----------------------------------------------------------------------------------
! Other Stuff
!-----------------------------------------------------------------------------------

fn.def ReSetBundgle(t)
	bundle.get 1, "Device_W", w
	bundle.get 1, "Device_H", h
	bundle.put 1, "SetX", w / 2
	bundle.put 1, "SetY", h / 2
	bundle.put 1, "Turn", 0 % is the angle
	bundle.put 1, "Pen", 1
	bundle.put 1, "Render", 1 
	if t then bundle.put 1, "Turtle", 1 % Turtle is down when clearing screen
fn.end

fn.def ShowArrow()
	bundle.get 1, "Turtle", turtle 
	bundle.get 1, "Render", render
	if turtle then
		bundle.get 1, "Rotate", rotateArrow
		bundle.get 1, "Arrow", arrow
		bundle.get 1, "Turn", turn
		GR.MODIFY rotateArrow, "angle" , turn, "x",Logo_GetPosX() , "y",Logo_GetPosY()
		GR.MODIFY arrow, "x",Logo_GetPosX()-25, "y",Logo_GetPosY()
	endif
	if render then gr.render
fn.end

fn.def MakeArrow(di_width, di_height)   % create the arrow's bitmap.
	array.load apts[], 25,2.5, 47.5,20, 32.5,17.5, 35,47.5, 25,42.5, 15,47.5, 17.5,17.5, 2.5,20   % 8 points.
	list.create n, li
	list.add.array li, apts[]
	gr.set.stroke 2
	gr.bitmap.create myArrow, 50,50
	gr.bitmap.drawinto.start myArrow
	gr.color 155,0,0,255,1
	gr.poly nul, li, 0, 0
	gr.color 255,255,0,0,0
	gr.poly nul, li, 0, 0
	gr.bitmap.drawinto.end
	! Display Arrow
	
	gr.rotate.start 0, (di_width / 2), (di_height / 2), myArrowRotObj
	gr.bitmap.draw arrow, myArrow, (di_width / 2), (di_height / 2)
	gr.rotate.end
	
	Logo_SetPenColour(3)
	Logo_TurtleShow()
	fn.rtn myArrow
fn.end

!-------------------------------------------------
! Setup Library 
!-------------------------------------------------

myArrow = MakeArrow(di_width, di_height)

!-------------------------------------------------
! *** IMPORTANT Include LogoLib in your apps as showen below ***
!gr.bitmap.create screen, di_width, di_height
!gr.bitmap.draw    nn, screen, 0, 0
!include LogoLib.bas
!gr.bitmap.drawinto.start screen
!-------------------------------------------------

