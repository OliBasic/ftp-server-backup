Rem Christmas Memory Game
Rem With RFO Basic!
Rem December 2015
Rem Version 1.00
Rem By Roy Shepherd

di_height = 672 % set to my Device
di_width = 1152

gr.open
gr.orientation 0 % Landscape
pause 1000
WakeLock 3 
gr.text.size 40

gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y

bundle.create global % 'global' bundle pointer = 1

gosub Functions % Let basic see the function before they are called
!-------------------------------------------------
! Global variables
!-------------------------------------------------
setg("Divice_W", di_width)
setg("Divice_H", di_height)
setg("Scale_X", scale_x) 
setg("Scale_Y", scale_y)

setg("Sound", 1) : setg("Buzz", 1)
setg("Best", 0)  : setg("Trys", 0)
setg("BackgroundSound", 1)

!-------------------------------------------------
! Lists and there         Global pointer
!-------------------------------------------------
list.create N, turnedOver : setg("TurnedOver", turnedOver)
list.create N, cards      : setg("Cards", cards)
list.create N, cardBacks  : setg("CardBacks", cardBacks)	

!-------------------------------------------------
! Start the game
!-------------------------------------------------
call Initialise()
do
	call DrawCards()
	call PlayGame()
until 0

end
!-------------------------------------------------
! Back key for menu
!-------------------------------------------------
onBackKey: 
	call Menu()
	back.resume
end

!-------------------------------------------------
! Functions
!-------------------------------------------------
Functions: % Let basic see the function before they are called

!-------------------------------------------------
! Do this stuff once
!-------------------------------------------------
fn.def Initialise()
	call LoadBest()
	dataPath$="../../ChristmasMemory/data/"
	di_width = getg("Divice_W") : turnedOver = getg("TurnedOver") 
	cards = getg("Cards") : cardBacks = getg("CardBacks")
	
	gr.bitmap.load bg, dataPath$ + "bg.png"
	gr.bitmap.draw back, bg, 0, 0 : gr.render : pause 1000
	!Load the cards twice (two of each card)
	for x = 1 to 10 
		gr.bitmap.load card, dataPath$ + "c" + int$(x) + ".png"
		gr.bitmap.draw c, card, -200, -200 : gr.render
		list.add cards, c
	next
	for x = 1 to 10 
		gr.bitmap.load card, dataPath$ + "c" + int$(x) + ".png"
		gr.bitmap.draw c, card, -200, -200
		list.add cards, c
	next
	Load 20 card backs
	for x = 1 to 20
		gr.bitmap.load backCard, dataPath$ + "c11.png"
		gr.bitmap.draw bc, backCard, - 200, -200
		list.add cardBacks, bc
	next
	! Draw Trys and Best and set there ponters to global
	gr.color 255, 255, 0, 0, 1
	gr.text.draw tryPtr, 70, 140, "Trys:" : gr.text.align 3 
	gr.text.draw bestPtr, di_width - 70, 140, "Best:" + int$(getg("Best")) : gr.text.align 1
	setg("TrysPtr",tryPtr) : setg("BestPtr", bestPtr)
	gr.render
	call LoadSounds() : call LoopSound()
fn.end

!------------------------------------------------
! Load sounds and give each sound a global name
!------------------------------------------------
fn.def LoadSounds()
	dataPath$="../../ChristmasMemory/sound/"
	Soundpool.open 5
	Soundpool.load  lose, dataPath$ + "lose.wav"        : setg("S_Lose", lose)
	Soundpool.load  huh, dataPath$ + "huh.wav"          : setg("S_Huh", huh)
	Soundpool.load  newBest, dataPath$ + "newBest.wav"  : setg("S_NewBest", newBest)
	Soundpool.load  turnedOver, dataPath$ + "right.wav" : setg("S_TurnedOver",turnedOver)
	Soundpool.load  goodBye, dataPath$ + "xmas2.mp3"    : setg("S_GoodBye", goodBye)
	! Load background sound
	audio.load loopSound, dataPath$ + "12day.mp3"       : setg("S_Loop", loopSound)
	if loopSound = 0 then e$ = GETERROR$() : ?e$ : end
fn.end

!-------------------------------------------------
! Shuffle the cards and draw them and cover them with the card backs
!-------------------------------------------------
fn.def DrawCards()
	cards = getg("Cards") : cardBack = getg("CardBacks")
	call ShuffleList(cards)
	y = 150 : xx = 70
	!Draw the cards
	for x = 1 to 20
		List.get cards, x, c
		gr.modify c, "x", xx, "y", y 
		xx += 200
		if x = 5 | x = 10 | x = 15 then y+=130 : xx = 70
	next
	gr.render
	
	!Cover the cards with the card backs
	y = 150 : xx = 70
	for x = 1 to 20
		List.get cardBack, x, bc
		gr.modify bc, "x", xx, "y", y
		gr.show bc
		xx += 200
		if x = 5 | x = 10 | x = 15 then y+=130 : xx = 70
	next
	gr.render
fn.end

!-------------------------------------------------
! Turn over two cards and if they match stay turned over, otherwise cover them with the card backs
!-------------------------------------------------
fn.def PlayGame()
	scale_x = getg("Scale_X") : scale_y = getg("Scale_Y")
	try = getg("Trys") : best = getg("Best") : turnedOver = getg("TurnedOver")
	cardBack = getg("CardBacks") : card = getg("Cards")
	setg("Trys", -1) : call ShowTrys() % set Trys to -1 call ShowTrys which moves it up 1 to 0
	do
		!Show first card
		do
			firstCard = 0
			do : gr.touch touched, tx, ty : until touched
			do : gr.touch touched, tx, ty : until ! touched
			tx/=scale_x 
			ty/=scale_x
			
			if tx > 70 & tx < 1066 & ty > 149 & ty < 666 then
				tx = int((tx-70)/200)+1
				ty = int((ty-150)/130)+1
				if ty > 1 then firstCard = (ty - 1) * 5
				firstCard += tx
				List.get cardBack, firstCard, firstHide %  Card back to hide so card front can be seen. 
				gr.hide firstHide : gr.render
				List.get card, firstCard, firstCardNum 
				call Buzz()
			endif
		
			gr.render
			List.search turnedOver, firstCardNum, alreadySelected
			if alreadySelected then call PlaySound(getg("S_Huh"))
		until firstCard & ! alreadySelected
		! Show second card
		do
			secondCard = 0
			do : gr.touch touched, tx, ty : until touched
			do : gr.touch touched, tx, ty : until ! touched
			tx/=scale_x 
			ty/=scale_x
			
			if tx > 70 & tx < 1066 & ty > 149 & ty < 666 then
				tx = int((tx-70)/200)+1
				ty = int((ty-150)/130)+1
				if ty > 1 then secondCard = (ty - 1) * 5
				secondCard += tx
				List.get cardBack, secondCard, secondHide %  Card back to hide so card front can be seen. 
				gr.hide secondHide : gr.render
				List.get card, secondCard, secondCardNum 
				if secondCard = firstCard then secondCard = 0 : call PlaySound(getg("S_Huh")) % use tapped an already turned over card
				call Buzz()
			endif
		
			gr.render
			List.search turnedOver, secondCardNum, alreadySelected
			if alreadySelected then call PlaySound(getg("S_Huh"))
		until secondCard & ! alreadySelected
		
		call ShowTrys() % Increase the number of trys and display
		!If a match leave turned over and add the matching cards to th turnedOver list otherwise cover with card backs
		if CompareCards(firstCardNum, secondCardNum)
			list.add turnedOver, firstCardNum, secondCardNum
			call PlaySound(getg("S_TurnedOver"))
		else 
			call PlaySound(getg("S_Lose"))
			pause 2000
			gr.show firstHide 
			gr.show secondHide 
			gr.render
		endif
		List.size turnedOver, size
	until size = 20
	list.clear turnedOver % clear ready for next game
	
	! Ask user if another game is wanted 
	if BestScore() then mess$ = "A New Low Score\nAnother Game" : call PlaySound(getg("S_NewBest")) else mess$ = "Another Game"	
	do : dialog.message "Game over",mess$,yn,"Yes","No" : until yn > 0 
	if yn = 2 then call GoodBye() % User tapped No so end game
	
fn.end

!-------------------------------------------------
! Compare the two turned over cards and if they are the same return 1 otherwise return 0
!-------------------------------------------------
fn.def CompareCards(firstCardNum, secondCardNum)
	flag = 1
	Gr.bitmap.size firstCardNum, width, height
	y = int(height / 2)
	for x = 1 to width - 1
		Gr.get.bmpixel firstCardNum, x, y, alpha, red, green, blue 
		test = red*65536 + green*256 + blue
		
		Gr.get.bmpixel secondCardNum, x, y, alpha, red, green, blue
		test2 = red*65536 + green*256 + blue

		if test <> test2 then flag = 0 : f_n.break
	
	if flag = 0 then f_n.break
	next
    fn.rtn flag
 fn.end

!-------------------------------------------------
! Put the list into an array and Shuffle the cards then put back into the list
!-------------------------------------------------
fn.def ShuffleList(ptr)
	list.toarray ptr, tempArray[]
	array.shuffle tempArray[]
	list.clear ptr 
	list.add.array ptr, tempArray[]
fn.end

!-------------------------------------------------
! Display the number of trys so far to complete the game
!-------------------------------------------------
fn.def ShowTrys()
	tryPtr = getg("TrysPtr") : t = getg("Trys")
	t++ : setg("Trys", t)
	gr.modify tryPtr,"text","Try:" + int$(t)
	gr.render
fn.end

!------------------------------------------------
! if sound is on. Play sound (ptr)
!------------------------------------------------
fn.def PlaySound(ptr)
	audio.pause
	if getg("Sound") then Soundpool.play s,ptr,0.99,0.99,1,0,1
	pause 500 : call LoopSound()
fn.end

!-------------------------------------------------
! If sound is on then play the background sound in a loop
!-------------------------------------------------
fn.def LoopSound()
	if getg("BackgroundSound") then 
		audio.play getg("S_Loop") 
		audio.volume 0.5, 0.5
		audio.loop
	else 
		audio.stop
	endif
fn.end

!-------------------------------------------------
! if buzz is on then vibrate when user taps screen
!-------------------------------------------------
fn.def Buzz()
	if getg("Buzz") then 
		array.load buzzGame[], 1, 100
		vibrate buzzGame[], -1
		array.delete buzzGame[]
	endif
fn.end

!-------------------------------------------------
! Record and display the best score
!------------------------------------------------- 
fn.def BestScore()
	flag = 0
	try = getg("Trys")
	best = getg("Best")
	if best = 0 then 
		best = try : flag = 1
	else
		if try < best then best = try : flag = 1 
	endif
	if flag	then setg("Best", best)%: call PlaySound(getg("S_NewBest"))
	gr.modify getg("BestPtr"), "text", "Best: "+int$(getg("Best"))
	gr.render
	fn.rtn flag
fn.end

!-------------------------------------------------
! Save best score, sound and vibration
!-------------------------------------------------
fn.def SaveBest()
	dataPath$="../../ChristmasMemory/data/"
	best = getg("Best")
	sound = getg("Sound")
	loopSound = getg("BackgroundSound")
	vibration = getg("Buzz")
	file.exists BestPresent,dataPath$+"ChristmasBest.txt"
	if ! BestPresent then file.mkdir dataPath$
	text.open w,hs,dataPath$+"ChristmasBest.txt"
		text.writeln hs,int$(best)
		text.writeln hs,int$(sound)
		text.writeln hs,int$(loopSound)
		text.writeln hs,int$(vibration)
	text.close hs
fn.end

!-------------------------------------------------
! Loads best score, sound and vibration
!-------------------------------------------------
fn.def LoadBest()
	dataPath$="../../ChristmasMemory/data/"
	file.exists BestPresent,dataPath$+"ChristmasBest.txt"
	if BestPresent then
		text.open r,hs,dataPath$+"ChristmasBest.txt"
			text.readln hs,best$
			best = val(best$)
			
			text.readln hs,best$
			sound = val(best$)
			
			text.readln hs,best$
			loopSound = val(best$)
			
			text.readln hs,best$
			vibration = val(best$)
			
			setg("Best", best)
			setg("Sound", sound)
			setg("BackgroundSound", loopSound)
			setg("Buzz", vibration)
		text.close hs
	endif
fn.end

!-------------------------------------------------
! Close the game
!------------------------------------------------- 
fn.def GoodBye()
	audio.stop
	call SaveBest()
	call PlaySound(getg("S_GoodBye"))
	gr.color 50, 0, 0, 0, 1
	gr.rect fade, 0, 0, getg("Divice_W"), getg("Divice_H")
	for a = 60 to 250 step 10 
		gr.modify fade, "alpha", a
		pause 50
		gr.render
	next
	gr.color 0,255,255,255,1 
	gr.text.size 200 
	gr.text.draw bye, 0, 400, "Happy Xmas"
	for a = 1 to 255 step 5
		gr.modify bye, "alpha", a
		pause 50
		gr.render
	next
	pause 1000
	soundpool.release
	exit
fn.end

!-------------------------------------------------
! Back key tapped. Call the menu
!-------------------------------------------------
fn.def Menu()
	sound = getg("Sound")
	vibration = getg("Buzz")
	loopSound = getg("BackgroundSound")
	if sound then sound$="Sound On" else sound$="Sound Off"
	if loopSound then loopsound$ = "Background Sound On" else loopSound$ = "Background Sound Off"
	if vibration then vibration$="Vibration On" else vibration$="Vibration Off"
	
	array.load menu$[],	sound$,~
						loopSound$,~
						vibration$,~
						"Reset Best Score",~
						"About",~
						"Help",~
						"Back to Game",~
						"Exit"
	dialog.select menuSelected, menu$[], "Main"
	
	sw.begin menuSelected
		sw.case 1
			if sound then sound = 0 else sound = 1	
			setg("Sound", sound) 
		sw.break
		
		sw.case 2 
			if loopSound then loopSound = 0 else loopSound = 1
			setg("BackgroundSound", loopSound) : call LoopSound()
		sw.break
			
		sw.case 3
			if vibration then vibration = 0 else vibration = 1
			setg("Buzz", vibration)
		Sw.break
		
		sw.case 4
			do : dialog.Message"Reset Best Score to zero?",,yn,"Yes","No" : until yn > 0
			if yn = 1 then 
				setg("Best", 0)
				call SaveBest()
				gr.modify getg("BestPtr"), "text", "Best: "+int$(getg("Best"))
				gr.render
			endif
		sw.break
		
		sw.case 5
			Dialog.Message " About:\n\n",~
			"   Christmas Memory Game for Android\n\n" + ~
			"   With: RFO BASIC!\n\n" + ~
			"   December 2015\n\n" + ~
			"   Version: 1.00\n\n" + ~ 
			"   Author: Roy Shepherd\n\n", ~
			ok, "OK"
		sw.break
		
		sw.case 6
			help$ = "Christmas Memory, is a little memory test game.\n\n" +~
			"There are twenty cards, with Christmas images, lying face\n" +~
			"down – ten pairs.\n\n" +~
			"You tap two cards to turn them over\n" +~
			"and if they are a matching pair they\n" +~
			"stay right side up, if they do not\n" +~
			"match there turn back over.\n\n" +~
			"You have to turn all the matching\n" +~
			"pairs over in as few tries as possible.\n\n"
	
			dialog.Message " Christmas Memory Help:",help$,ok,"OK"
		sw.break
		
		sw.case 7
			! Back to game
		sw.break
			
		sw.case 8
			do : dialog.Message"Exit Game?",,yn,"Yes","No" : until yn > 0
			if yn=1 then call GoodBye()
		sw.break
	sw.end
fn.end

!----------------------------------------------------
! These functions give simulate Global variables
! Thanks to Mougino for the functions
!----------------------------------------------------
FN.DEF SETG(var$, n) % Set global-numeric-variable
  BUNDLE.PUT 1, var$, STR$(n)
FN.END

FN.DEF SETG$(var$, s$) % Set string-numeric-variable
  BUNDLE.PUT 1, var$, s$
FN.END

FN.DEF GETG(var$) % Get global-numeric-variable
  BUNDLE.GET 1, var$, s$
  FN.RTN VAL(s$)
FN.END

FN.DEF GETG$(var$) % Get string-numeric-variable
  BUNDLE.GET 1, var$, s$
  FN.RTN s$
FN.END

return
