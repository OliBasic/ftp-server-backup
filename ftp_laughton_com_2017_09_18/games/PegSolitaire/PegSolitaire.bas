Rem Peg Solitaire
Rem With RFO Basic!
Rem December 2014
Rem Version 1.04
Rem By Roy
Rem

console.title"Peg Solitaire"
Rem Open Graphics
di_width = 1280 % set to my Device
di_height = 800

gr.open 255,0,0,0 % Black
gr.orientation 0
pause 1000
gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y
gr.text.typeface 1
gr.text.size 30
wakelock 3

Rem Globel Vars
dim buttonTap[6]
dim pegBoard[7,7]
dim pegBG[2]
dim pegPlace[7,7]
dim pegNum[1]
dim pegs[15]
dim info[6] % 1 & 3:Modify pegs left 2 & 4:modify best 5:number of pegs left 6: best score
dim sound[9] % 1 holds sound.png, 2 sound on or off 3: show and hide sound.png
sound[2]=1 % on
info[5]=32
info[6]=32
pegNum[1]=3

Rem sound[4],"buttonClick.wav"
Rem sound[5],"error.wav"
Rem sound[6],"newBest.wav"
Rem sound[7],"solSound.wav"
Rem sound[8],"wrong.wav"

Rem Start of Functions

Fn.def BringToFront( ptr )  % ptr is the pointer to become the last one into DL.
  gr.getdl ndl[],1
  array.length sz, ndl[]
  if !ptr | sz =1 | ndl[sz] =ptr
    array.delete ndl[]
    Fn.rtn 0
  endif
  array.search ndl[],ptr,n
  if n
    if ndl[n] =ptr
      for nn=n to sz-1
        ndl[nn] = ndl[nn+1]
      next
      ndl[sz] = ptr
      GR.NEWDL ndl[]
    endif
  endif
  array.delete ndl[]
Fn.end

fn.def LoadSounds(sound[])
	Soundpool.open 1
	Soundpool.load sound[4],"buttonClick.wav"
	Soundpool.load sound[5],"error.wav"
	Soundpool.load sound[6],"newBest.wav"
	Soundpool.load sound[7],"solSound.wav"
	Soundpool.load sound[8],"wrong.wav"
fn.end

fn.def UpDateInfoFirst(info[])
	gr.set.stroke 1
	gr.color 255,255,255,255,1
	gr.text.align 2
	gr.text.bold 1
	gr.text.draw info[1],100,50,"Pegs:"+int$(info[5])
	gr.text.draw info[2],950,52,"Best:"+int$(info[6])
	gr.color 255,0,0,0,0
	gr.text.draw info[3],100,50,"Pegs:"+int$(info[5])
	gr.text.draw info[4],950,52,"Best:"+int$(info[6])
	gr.text.bold 0
	gr.text.align 1
	gr.render
fn.end

fn.def UpDateInfo(info[])
	gr.modify info[1],"text","Pegs:"+int$(info[5])
	gr.modify info[2],"text","Best:"+int$(info[6])
	gr.modify info[3],"text","Pegs:"+int$(info[5])
	gr.modify info[4],"text","Best:"+int$(info[6])
	gr.render
fn.end

fn.def LoadImages(pegBG[],pegs[],sound[])
	Rem Split Pegs
	gr.bitmap.load SrcPegs,"Pegs.png"
	for x=1 to 15
		gr.bitmap.crop pegs[x], SrcPegs, (x-1)*50,0,50,50
		gr.bitmap.scale pegs[x],pegs[x],75,75
	next
	gr.bitmap.delete SrcPegs

	gr.bitmap.load pegBG[1],"PegBoard2.png" 
	gr.bitmap.load sound[1],"SoundOn.png"
	gr.bitmap.scale sound[1],sound[1],85,85
fn.end

fn.def LoadPegBoardArray(pegBoard[])
	Rem 0 No go areas. corners
	for y=1 to 7
		for x=1 to 7
			pegBoard[x,y]=0 % Set all to no go areas to start with
		next 
	next

	for y=3 to 5
		for x=1 to 7
			pegBoard[x,y]=1 % Set to where pegs start off
		next 
	next
	
	for y=1 to 7
		for x=3 to 5
			pegBoard[x,y]=1 % Set to where pegs go
		next 
	next
	pegBoard[4,4]=2 % Centre hole hiden
	
fn.end

fn.def PutPegsOnBoard(pegBoard[],pegPlace[],pegs[],pegNum[])
	px=103 
	py=105
	for y=1 to 7
		for x=1 to 7
			if pegBoard[x,y]>0 then % if 1 then a peg go on board 
				gr.bitmap.draw pegPlace[x,y],pegs[pegNum[1]],px,py
			endif
			if pegBoard[x,y]=2 then gr.hide pegPlace[x,y]
			px+=85
		next 
		px=103
		py+=85
	next
	gr.render
fn.end

fn.def SetUpScreen(pegBG[],info[],sound[],buttonTap[])
	tap=1
	gr.bitmap.draw d,pegBG[1],0,0  
	gr.bitmap.draw sound[3],sound[1],915,590
	call UpDateInfoFirst(info[])
	gr.color 100,0,0,0,1
	for y=165 to 500 step 65 %show when button is presed
		gr.rect buttonTap[tap] ,880,y,1032,y+40 
		gr.hide buttonTap[tap]
		tap++
	next
	gr.color 255,0,0,0,1
	gr.render
fn.end

fn.def NewGame(buttonTap[],sound[],pegBoard[],pegPlace[],pegs[],pegNum[],info[],s)
	if s & sound[2] then Soundpool.play s,sound[4],0.99,0.99,1,0,1
	gr.show buttonTap[1]
	gr.render
	yn=0
	if MoveLeft(pegBoard[]) then 
		do
			Dialog.Message "New Game:",~
			" Are You Sure You Want to Start a New Game - There are Some Moves Left", ~
			yn, "Yes","No"
			if yn = 0 & sound[2] then Soundpool.play s,sound[8],0.99,0.99,1,0,1
		until yn<>0
		if sound[2] then Soundpool.play s,sound[4],0.99,0.99,1,0,1
	endif
	if yn < 2 then
		info[5]=32
		call HideOldPegs(pegBoard[],pegPlace[],pegs[],pegNum[])
		call LoadPegBoardArray(pegBoard[])
		call PutPegsOnBoard(pegBoard[],pegPlace[],pegs[],pegNum[])
		call UpDateInfo(info[])
	endif
	gr.hide buttonTap[1]
	gr.render
fn.end

fn.def HideOldPegs(pegBoard[],pegPlace[],pegs[],pegNum[])
	for y=1 to 7
		for x=1 to 7
			gr.hide pegPlace[x,y] % hide the old pegs
		next 
	next
	gr.render
fn.end

fn.def Pegs(buttonTap[],sound[],pegBoard[],pegPlace[],pegs[],pegNum[])
	if sound[2] then Soundpool.play s,sound[4],0.99,0.99,1,0,1
	gr.show buttonTap[2]
	gr.render
	call HideOldPegs(pegBoard[],pegPlace[],pegs[],pegNum[])
	pegNum[1]++
	if pegNum[1]=16 then pegNum[1]=1
	call PutPegsOnBoard(pegBoard[],pegPlace[],pegs[],pegNum[])
	do 
		gr.touch touched, tx,ty 
	until !touched
	gr.hide buttonTap[2]
	gr.render
fn.end

fn.def Help(buttonTap[],sound[])
	if sound[2] then Soundpool.play s,sound[4],0.99,0.99,1,0,1
	gr.show buttonTap[3]
	gr.render
	p$=chr$(10)+chr$(10) % Do a paragraph brake
	do
		Dialog.Message "Help:",~
		" Peg solitaire is an old board game for one player." + p$ + ~
		" The game board is in cross form and has 33 holes in it." + ~
		" 32 pegs are in the board with the centre hole being empty." + p$ + ~ 
		" You have to remove the pegs one after the other by jumping" + ~
		" horizontally  or vertically over one peg and into a hole." + p$ + ~
		" The pegs you jump over are removed." + p$ + ~
        " The aim of the game is to end with one peg left in the centre hole." + p$ + ~
		" Tap the Peg button to cycle through the fifteen available Pegs." + p$ + ~
		" Not sure how to play. Try watching the three Demos.", ~
		ok, "OK"
		if ok = 0 & sound[2] then Soundpool.play s,sound[8],0.99,0.99,1,0,1
	until ok=1
	if sound[2] then Soundpool.play s,sound[4],0.99,0.99,1,0,1
	gr.hide buttonTap[3]
	gr.render
fn.end

fn.def About(buttonTap[],sound[])
	if sound[2] then Soundpool.play s,sound[4],0.99,0.99,1,0,1
	gr.show buttonTap[4]
	gr.render
	p$=chr$(10)+chr$(10) % Do a paragraph brake
	do
		Dialog.Message "About:",~
		" Peg Solitaire: for Android" + p$ + ~
		" With RFO Basic" + p$ + ~
		" December 2014." + p$ +~
		" Version 1.02" + p$ + ~ 
		" Author: Roy Shepherd", ~
		ok, "OK"
		if ok =0 & sound[2] then Soundpool.play s,sound[8],0.99,0.99,1,0,1
	until ok=1
	if sound[2] then Soundpool.play s,sound[4],0.99,0.99,1,0,1
	gr.hide buttonTap[4]
	gr.render
fn.end

fn.def Demo(buttonTap[],sound[],info[],pegBoard[],pegPlace[],pegs[],pegNum[],scale_x,scale_y)
	if sound[2] then Soundpool.play s,sound[4],0.99,0.99,1,0,1
	gr.show buttonTap[5]
	gr.render
	array.load demo1[],27, 25, 12, 26, 21, 19, 35, 21, 26, 12, 5, 19, 18, 20, 21, 19, 16, 18, 19, 17, 4, 18, 18, 16, 3, 17,	16, 18, 30, 16, 15, 17, 29, 15, 18, 16, 15, 17, 32, 30, 17, 31, 30, 32, 45, 31, 32, 30, 34, 32, 47, 33, 33, 31, 46, 32, 25, 39, 30, 32, 39, 25
	Rem Demo 2. The "world record" is a solution with 18 moves by E. Bergholt from 1912. 
	array.load demo2[],27, 25, 12, 26, 21, 19, 18, 20, 16, 18, 3, 17, 24, 10, 5, 3, 3, 17, 33, 19, 47, 33, 38, 24, 24, 10, 10, 12, 12, 26, 26, 40, 35, 21, 21, 19, 19, 17, 29, 31, 32, 30, 15, 29, 29, 31, 45, 47, 47, 33, 34, 32, 32, 30, 30, 16, 16, 18, 18, 32, 39, 25
	Rem Demo 3. The following solution is clever. You use the L-move four times and get the house figure (the house stands on its head). Then you take down this figure with a six-jump move.
	array.load demo3[],39, 25, 34, 32, 47, 33, 45, 47, 26, 40, 47, 33, 12, 26, 21, 19, 35, 21, 18, 20, 21, 19, 16, 18, 3, 17, 5, 3, 24, 10, 3, 17, 38, 24, 29, 31, 15, 29, 32, 30, 29, 31, 18, 16, 16, 30, 30, 32, 32, 34, 34, 20, 20, 18, 25, 27, 11, 25, 24, 26, 27, 25

	array.load demo$[], "Demo 1","Demo 2","Demo 3","Cancel"
	do
		dialog.select demoNum, demo$[], "Select a Demo"
		if demoNum =0 & sound[2] then Soundpool.play s,sound[8],0.99,0.99,1,0,1
	until demoNum>0
	if sound[2] then Soundpool.play s,sound[4],0.99,0.99,1,0,1
	if demoNum<4 then
		stepThrough = MyDialogMess(530,600,scale_x,scale_y,sound[],"Demo:","Step Through","Yes","No")
		Rem setUp ready for demo
		p1=1
		info[5]=32
		call HideOldPegs(pegBoard[],pegPlace[],pegs[],pegNum[])
		call LoadPegBoardArray(pegBoard[])
		call PutPegsOnBoard(pegBoard[],pegPlace[],pegs[],pegNum[])
		call UpDateInfo(info[])
		if demoNum=1 then 
			array.length demoLen, demo1[]
			for d=1 to demoLen step 2
				p1=demo1[d]
				p2=demo1[d+1]
				call DemoDoMove(info[],pegBoard[],pegPlace[],pegs[],pegNum[],sound[],p1,p2,stepThrough,scale_x,scale_y)
			next
		endif
		
		if demoNum=2 then 
			array.length demoLen, demo2[]
			for d=1 to demoLen step 2
				p1=demo2[d]
				p2=demo2[d+1]
				call DemoDoMove(info[],pegBoard[],pegPlace[],pegs[],pegNum[],sound[],p1,p2,stepThrough,scale_x,scale_y)
			next
		endif
		
		if demoNum=3 then 
			array.length demoLen, demo3[]
			for d=1 to demoLen step 2
				p1=demo3[d]
				p2=demo3[d+1]
				call DemoDoMove(info[],pegBoard[],pegPlace[],pegs[],pegNum[],sound[],p1,p2,stepThrough,scale_x,scale_y)
			next
		endif
		
	endif
	array.delete demo1[],demo2[],demo3[]
	array.delete demo$[]
	gr.hide buttonTap[5]
	gr.render
	fn.rtn demoNum
fn.end

fn.def DemoDoMove(info[],pegBoard[],pegPlace[],pegs[],pegNum[],sound[],p1,p2,stepThrough,scale_x,scale_y)
	for pegFind = 1 to 3
		pegCount=1
		px=103
		py=105
		for y=1 to 7
			for x=1 to 7
				if pegFind=1 & pegCount=p1 then
					pegX1=x 
					pegY1=y
					mpx1=px
					mpy1=py
				endif
				if pegFind=2 & pegCount=p2 then
					pegX2=x 
					pegY2=y
				endif
				pegCount++
				px+=85
			next 
			px=103
			py+=85
		next
	next
	pegBoard[pegX1,pegY1]=2 % empty hole
	pegBoard[pegX2,pegY2]=1 % peg in hole
	if pegX1=pegX2 then 
		if pegY1>pegY2 then 
			pegBoard[pegX2,pegY2+1]=2
			call BringToFront(pegPlace[pegX1,pegY1])
			for p=1 to 84 step 4
				gr.modify pegPlace[pegX1,pegY1],"y",mpy1-p
				gr.render 
			next
			gr.hide pegPlace[pegX1,pegY1-1]
			for p=84 to 170  step 4
				gr.modify pegPlace[pegX1,pegY1],"y",mpy1-p
				gr.render 
			next
		endif
		if pegY1<pegY2 then 
			pegBoard[pegX2,pegY2-1]=2
			call BringToFront(pegPlace[pegX1,pegY1])
			for p=1 to 84  step 4
				gr.modify pegPlace[pegX1,pegY1],"y",mpy1+p
				gr.render 
			next 
			gr.hide pegPlace[pegX1,pegY1+1]
			for p=84 to 170  step 4
				gr.modify pegPlace[pegX1,pegY1],"y",mpy1+p
				gr.render 
			next 
		endif
	endif
	Rem move x
	if pegY1=pegY2 then 
		if pegX1>pegX2 then 
			pegBoard[pegX2+1,pegY2]=2
			call BringToFront(pegPlace[pegX1,pegY1])
			for p=1 to 84  step 4
				gr.modify pegPlace[pegX1,pegY1],"x",mpx1-p
				gr.render 
			next
			gr.hide pegPlace[pegX1-1,pegY1]
			for p=84 to 170  step 4
				gr.modify pegPlace[pegX1,pegY1],"x",mpx1-p
				gr.render 
			next
		endif
		if pegX1<pegX2 then 
			pegBoard[pegX2-1,pegY2]=2
			call BringToFront(pegPlace[pegX1,pegY1])
			for p=1 to 84  step 4
				gr.modify pegPlace[pegX1,pegY1],"x",mpx1+p
				gr.render 
			next
			gr.hide pegPlace[pegX1+1,pegY1]
			for p=84 to 170  step 4
				gr.modify pegPlace[pegX1,pegY1],"x",mpx1+p
				gr.render 
			next
		endif
	endif

	call ReDisplayBoard(pegBoard[],pegPlace[],pegs[],pegNum[],pegX2,pegY2)
	info[5]-- 
	call UpDateInfo(info[])
	if stepThrough=1 then
		call MyDialogMess(865,536,scale_x,scale_y,sound[],"Demo:","Next Step","OK","")
	endif
fn.end

fn.def PegSwap(peg1,peg2)

fn.end

fn.def Exit(buttonTap[],sound[])
	if sound[2] then Soundpool.play s,sound[4],0.99,0.99,1,0,1
	gr.show buttonTap[6]
	gr.render
	p$=chr$(10)+chr$(10) % Do a paragraph brake
	do
		Dialog.Message "Exit:",~
		" Are You Sure You Want to Leave Peg Solitaire", ~
		yn, "Yes","No"
		if ok = 0 & sound[2] then Soundpool.play s,sound[8],0.99,0.99,1,0,1
	until yn<>0
	if sound[2] then Soundpool.play s,sound[4],0.99,0.99,1,0,1
	gr.hide buttonTap[6]
	gr.render
	if yn=1 then call Goodbye()
fn.end

fn.def MyDialogMess(x,y,scale_x,scale_y,sound[],t$,m$,b1$,b2$)
	answer=0
	dialog1=0
	dialog2=0
	yy=200
	gr.text.width w, m$
	gr.bitmap.create dialog1, w+50,yy
	gr.bitmap.drawinto.start dialog1
	gr.color 255,192,192,192,1
	gr.rect r,1,1,w+50,yy
	gr.color 255,0,0,0,1
	gr.rect r,3,3,w+47,yy-75
	gr.color 255,255,255,255,1
	gr.text.draw d,10,50,t$
	gr.text.draw d,10,100,m$
	rem Draw Buttons
	gr.text.align 2
	gr.text.size 20
	if len(b2$)>0 then 
		centre=(w+50)/2
		xb2=center +(w+50)/2+(w+50)/4
		gr.rect r,5,yy-70,centre-5,yy-5
		gr.rect r,centre+5,yy-70,w+45,yy-5
		gr.color 255,0,0,0,1
		gr.text.draw d,centre/2,yy-30,b1$
		gr.text.draw d,xb2,yy-30,b2$
	else
		gr.rect r,15,yy-70,w+35,yy-5
		gr.color 255,0,0,0,1
		gr.text.draw d,(w+50)/2,yy-30,b1$
	endif
	gr.text.size 30 % back to what ever yours is
	gr.text.align 1
	gr.bitmap.drawinto.end
	
	gr.bitmap.draw dialog2, dialog1, x,y 

	gr.render
	do
		do 
			gr.touch touched,tx,ty 
		until touched
		gr.render
		do 
			gr.touch touched,tx,ty 
		until !touched
		tx/=scale_x 
		ty/=scale_y
		if len(b2$)>0 then
			if  tx>x & tx<x+centre & ty>y+125 & ty<y+200 then answer = 1
			if  tx>x + centre & tx<x+w+40 & ty>y+125 & ty<y+200 then answer = 2
		else
			if  tx>x & tx<x+w+50 & ty>y+125 & ty<y+200 then answer = 1
		endif
		if answer=0 & sound[2] then Soundpool.play s,sound[8],0.99,0.99,1,0,1
	until answer>0
	if sound[2] then Soundpool.play s,sound[4],0.99,0.99,1,0,1
	gr.hide dialog2
	gr.bitmap.delete dialog1
	gr.render
	fn.rtn answer
fn.end

fn.def Goodbye()
	pause 1000
	wakelock 5
	soundpool.release
	gr.close
	exit
fn.end

fn.def ToddleSound(sound[])
if sound[2] then Soundpool.play s,sound[4],0.99,0.99,1,0,1
	if sound[2]=0 then 
		sound[2]=1
		gr.show sound[3]
	else 
		sound[2]=0
		gr.hide sound[3]
	endif
	do 
		gr.touch touched,tx,ty 
	until !touched
	gr.render
	if sound[2] then Soundpool.play s,sound[4],0.99,0.99,1,0,1
fn.end

fn.def Menu(buttonTap[],sound[],pegBoard[],pegPlace[],pegs[],pegNum[],info[],scale_x,scale_y)
	gr.touch touched,tx,ty
	if touched then
		tx/=scale_x
		ty/=scale_y
		if tx>870 & tx<1050 then 
			if ty>165 & ty<205 then call NewGame(buttonTap[],sound[],pegBoard[],pegPlace[],pegs[],pegNum[],info[],1)
			if ty>230 & ty<270 then call Pegs(buttonTap[],sound[],pegBoard[],pegPlace[],pegs[],pegNum[])
			if ty>295 & ty<335 then call Help(buttonTap[],sound[])
			if ty>360 & ty<410 then call About(buttonTap[],sound[])
			if ty>425 & ty<465 then 
				cancel = Demo(buttonTap[],sound[],info[],pegBoard[],pegPlace[],pegs[],pegNum[],scale_x,scale_y)
				if cancel <> 4 then
					demoOver = MyDialogMess(530,600,scale_x,scale_y,sound[],"Demo Over:","Start New Game","OK","")
					if demoOver=1 then call NewGame(buttonTap[],sound[],pegBoard[],pegPlace[],pegs[],pegNum[],info[],0)
				endif
			endif
			if ty>490 & ty<530 then call Exit(buttonTap[],sound[])
		endif
		if tx>929 & tx<1000 & ty>569 & ty<680 then ToddleSound(sound[])
	endif
fn.end

fn.def NoMovesLeft(info[],pegBoard[],scale_x,scale_y,sound[])
	g$="No Moves Left"
	if info[5]<info[6] then % info[6] holds best score
		info[6]=info[5]
		call UpDateInfo(info[])
		g$="No Moves Left. A New Best Well Done"
		if sound[2] then Soundpool.play s,sound[6],0.99,0.99,1,0,1
		if info[5]=1 & pegBoard[4,4]=1 then g$="Can't Get Better Than This"
	endif
	gr.text.width w,g$
	!x=640-((w+40)/2)
	x=1280-(w+60)
	newOrExit = MyDialogMess(x,600,scale_x,scale_y,sound[],"Game Over:",g$,"New Game","Exit")
	fn.rtn newOrExit
fn.end

fn.def MoveLeft(pegBoard[])
	flag=0
	for y=1 to 7
		for x=1 to 7
			if x<6 then if pegBoard[x,y]=1 & pegBoard[x+1,y]=1 & pegBoard[x+2,y]=2 then flag=1 % look left to right
			if x<6 then if pegBoard[x,y]=2 & pegBoard[x+1,y]=1 & pegBoard[x+2,y]=1 then flag=1 % look right to left
			
			if y<6 then if pegBoard[x,y]=1 & pegBoard[x,y+1]=1 & pegBoard[x,y+2]=2 then flag=1 % look top to bottm
			if y<6 then if pegBoard[x,y]=2 & pegBoard[x,y+1]=1 & pegBoard[x,y+2]=1 then flag=1 % look bottom to top
		next
	next 
	fn.rtn flag
fn.end

fn.def ReDisplayBoard(pegBoard[],pegPlace[],pegs[],pegNum[],sx,sy)
	px=103
	py=105
	for y=1 to 7
		for x=1 to 7
			if pegBoard[x,y]=1 then % if 1 then a peg go on board 
				gr.modify pegPlace[x,y],"x",px,"y",py
			endif
			if pegBoard[x,y]=2 then gr.hide pegPlace[x,y]
			if x=sx & y=sy then gr.show pegPlace[sx,sy]
			px+=85
		next 
		px=103
		py+=85
	next
	gr.render
fn.end

fn.def IsMoveLegal(pegBoard[],pegPlace[],pegNum[],pegs[],sound[],ox,oy,tx,ty)
	flag=0
	px=103
	py=105
	for y = 1 to 7
		for x=1 to 7
			if tx>=px & tx<px+80 & ty>=py & ty<py+80 then 
				Rem look left to right
				if x+2=ox & y=oy & !flag then
					if pegBoard[x+1,y]= 1 &  pegBoard[x,y]= 2 then % peg in hole  & empty hole
						pegBoard[ox,oy]=2 % hide Peg
						pegBoard[x+1,y]= 2 % hide peg
						pegBoard[x,y]=1 % peg jumped into hole
						call ReDisplayBoard(pegBoard[],pegPlace[],pegNum[],pegs[],x,y)
						flag=1
					endif
				endif
				Rem look right to left
				if x-2=ox & y=oy & !flag then 
					if pegBoard[x-1,y]= 1 &  pegBoard[x,y]= 2 then % peg in hole  & empty hole
						pegBoard[ox,oy]=2 % hide Peg
						pegBoard[x-1,y]= 2 % hide peg
						pegBoard[x,y]=1 % peg jumped into hole
						call ReDisplayBoard(pegBoard[],pegPlace[],pegNum[],pegs[],x,y)
						flag=1
					endif
				endif
				
				if y+2=oy & x=ox & !flag then
					if pegBoard[x,y+1]= 1 &  pegBoard[x,y]= 2 then % peg in hole  & empty hole
						pegBoard[ox,oy]=2 % hide Peg
						pegBoard[ox,oy-1]= 2 % hide peg
						pegBoard[x,y]=1 % peg jumped into hole
						call ReDisplayBoard(pegBoard[],pegPlace[],pegNum[],pegs[],x,y)
						flag=1
					endif
				endif
				
				if y-2=oy & x=ox & !flag then 
					if pegBoard[x,y-1]= 1 &  pegBoard[x,y]= 2 & pegBoard[x,y] =2 then % pegs in hole  & empty hole (hiden peg)
						pegBoard[ox,oy]=2 % hide Peg
						pegBoard[ox,oy+1]= 2 % hide Peg
						pegBoard[x,y] = 1 % peg jumped into hole
						call ReDisplayBoard(pegBoard[],pegPlace[],pegNum[],pegs[],x,y)
						flag=1
					endif
				endif
				
			endif
			px+=85
		next 
		px=103
		py+=85
	next
	if !flag then call ReDisplayBoard(pegBoard[],pegPlace[],pegNum[],pegs[],0,0)
	if !flag & sound[2] then Soundpool.play s,sound[5],0.99,0.99,1,0,1
	if flag & sound[2] then Soundpool.play s,sound[7],0.99,0.99,1,0,1
	
	fn.rtn flag 
fn.end
!!
Rem My functions
fn.def PlayPegSolitaire(pegBoard[],pegPlace[],pegNum[],pegs[],info[],buttonTap[],sound[],scale_x,scale_y)
	do 
		whatNow=0
		ox=0 
		oy=0
		flag=0
		legal=0
		px=103
		py=105
		for y=1 to 7
			for x=1 to 7
				if pegBoard[x,y]=1 then % if 1 then a peg is on board 
					gr.touch touched,tx,ty
					tx/=scale_x
					ty/=scale_y
					If touched & tx>=px & tx<px+70 & ty>=py & ty<py+70
						ox=x 
						oy=y
						flag=1
						do 
							gr.touch touched, tx,ty 
							tx/=scale_x
							ty/=scale_y
							call BringToFront(pegPlace[x,y])
							gr.modify pegPlace[x,y],"x",tx-20,"y",ty-20
							gr.render
						until !touched
						if flag then legal = IsMoveLegal(pegBoard[],pegPlace[],pegNum[],pegs[],sound[],ox,oy,tx,ty)
						if legal then 
							info[5]-- % pegs left
							call UpDateInfo(info[])
						endif
					endif
				endif
				px+=85
			next 
			px=103
			py+=85
		next
		call Menu(buttonTap[],sound[],pegBoard[],pegPlace[],pegs[],pegNum[],info[],scale_x,scale_y) % see if any menu button touched
		if !MoveLeft(pegBoard[]) then whatNow = NoMovesLeft(info[],pegBoard[],scale_x,scale_y,sound[])
		if whatNow=1 then call NewGame(buttonTap[],sound[],pegBoard[],pegPlace[],pegs[],pegNum[],info[],0)
		if whatNow=2 then call Goodbye()
	until 0
fn.end
!!
Rem Gilles improved function
fn.def PlayPegSolitaire(pegBoard[],pegPlace[],pegNum[],pegs[],info[],buttonTap[],sound[],scale_x,scale_y)
DO
     whatNow=0
     ox=0
     oy=0
     flag=0
     legal=0
     GR.TOUCH touched,tx,ty
     tx/=scale_x
     ty/=scale_y
     x =int((tx-103)/85)+1   % cell calculated
     y =int((ty-105)/85)+1
     If touched & x & x<8 & y & y<8
        if pegBoard[x,y]=1    % if 1 then a peg is on board
           gr.get.position pegPlace[x,y], posx, posy  % current position
           deltaX =posx-tx
           deltaY =posy-ty
           call BringToFront(pegPlace[x,y])  % bitmap go front
           ox=x
           oy=y
           flag=1
           do
              gr.touch touched, tx,ty
              tx/=scale_x
              ty/=scale_y
              gr.modify pegPlace[x,y],"x",tx+deltaX,"y",ty+deltaY   % move peg
              gr.render
           until !touched
           if flag then legal = IsMoveLegal(pegBoard[],pegPlace[],pegNum[],pegs[],sound[],ox,oy,tx,ty)
           if legal
              info[5]-- % pegs left
              call UpDateInfo(info[])
           endif
        endif
      endif
      call Menu(buttonTap[],sound[],pegBoard[],pegPlace[],pegs[],pegNum[],info[],scale_x,scale_y) % see if any menu button touched
      if !MoveLeft(pegBoard[]) then whatNow = NoMovesLeft(info[],pegBoard[],scale_x,scale_y,sound[])
      if whatNow=1 then call NewGame(buttonTap[],sound[],pegBoard[],pegPlace[],pegs[],pegNum[],info[],0)
      if whatNow=2 then call Goodbye()
UNTIL 0
fn.end

Rem End of Functions

Rem Main
Call LoadSounds(sound[])
call LoadImages(pegBG[],pegs[],sound[])
call LoadPegBoardArray(pegBoard[])
call SetUpScreen(pegBG[],info[],sound[],buttonTap[])
call PutPegsOnBoard(pegBoard[],pegPlace[],pegs[],pegNum[])

call PlayPegSolitaire(pegBoard[],pegPlace[],pegNum[],pegs[],info[],buttonTap[],sound[],scale_x,scale_y)

Rem End of main

Rem catch back key
onBackKey:
call Exit(buttonTap[],sound[])
back.resume
end

