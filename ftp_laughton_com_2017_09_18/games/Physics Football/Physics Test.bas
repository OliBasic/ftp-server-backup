% Opening graphics.
GR.OPEN 255, 0, 0, 0
GR.SCREEN sWd, sHe
GR.ORIENTATION 0

% path$ = "user/RandomStuff/"	% This my folder for game files.

% The center of the screen.
sWd_m = sWd/2
sHe_m = sHe/2

% I discovered that the code runs alot faster (almost double speed) if I don't use any number directly on it.
Zero = 0
One = 1

playerStrenght = sWd/80	% How much spinach the player ate.

% Ball description.
bSize = sWd/20	% Size.
bSize_m = bSize/2	% Half size for touch and DRAW purposes.
bFriction = bSize/10	% Friction for when it hits a wall or the floor.
gForce = sHe/50/30	% Gravity vector.

% Vector to find the position to DRAW the ball.
vecBmpX = bSize/2
vecBmpY = bSize/2

% Ball initial position.
bolaX = sWd_m
bolaY = Zero

% Screen limits.
bolaXlimit1 = vecBmpX	
bolaXlimit2 = sWd-vecBmpX
bolaYlimit = sHe-vecBmpY

% Initial values to DRAW the ball.
bolaBmpX = bolaX+vecBmpX
bolaBmpY = bolaY+vecBmpY

% Initial speed vector of the ball,.
vecSpeedX = Zero
vecSpeedY = Zero

% Bitmap LOAD.
GR.BITMAP.LOAD temp_ptr, path$+"Bola.png"
GR.BITMAP.SCALE bola_ptr, temp_ptr, bSize, bSize
GR.BITMAP.DELETE temp_ptr

% Main loop.
DO

	GR.CLS
	GOSUB touchTest
	GOSUB ballMove
	GR.RENDER
	
UNTIL 0

ballMove:
	% Here you can see how it's easy to calculate gravity in action.
	vecSpeedX += Zero
	vecSpeedY += gForce

	bolaX += vecSpeedX
	bolaY += vecSpeedY

	% Collisions checking.
	IF bolaY > bolaYlimit
		vecSpeedY = -vecSpeedY+bFriction
		bolaY = bolaYlimit
	ENDIF

	IF bolaX < bolaXlimit1
		vecSpeedX = -vecSpeedX-bFriction
		bolaX = bolaXlimit1
		
		IF bolaY = bolaYlimit THEN vecSpeedX -= bFriction
	ENDIF
	
	IF bolaX > bolaXlimit2
		vecSpeedX = -vecSpeedX+bFriction
		bolaX = bolaXlimit2

		IF bolaY = bolaYlimit THEN vecSpeedX += bFriction
	ENDIF
	
	% Time to find the DRAW positions.
	bolaBmpX = bolaX-vecBmpX
	bolaBmpY = bolaY-vecBmpY
	
	% Here it is! The protagonist of the game.
	GR.BITMAP.DRAW bola, bola_ptr, bolaBmpX, bolaBmpY
RETURN

touchTest:
	% If you touch the ball the game will stuck here.
	GR.BOUNDED.TOUCH bTouch, bolaBmpX, bolaBmpY, bolaBmpX+bSize, bolaBmpY+bSize
	IF bTouch
		DO			
			GR.TOUCH sTouch, tcX, tcY
			bolaX = tcX
			bolaY = tcY

			GR.CLS
			GR.COLOR 100, 255, 255, 255, 1
			GR.CIRCLE bSelect, bolaX, bolaY, bSize_m
			GR.RENDER
		UNTIL !sTouch
		
		% Now that you repositioned the ball touch it again and keep holding.
		DO
			GR.BOUNDED.TOUCH bTouch, bolaX-vecBmpX, bolaY-vecBmpY~
			, bolaX+vecBmpX, bolaY+vecBmpY
		UNTIL bTouch

		% And finally check calculate the momentum that will be put in to the ball.		
		DO
			GR.TOUCH sTouch, tcX, tcY
		
			% Momentum vector.
			mmX = tcX-bolaX
			mmY= tcY-bolaY	

			GR.CLS
			GR.COLOR 100, 255, 255, 255, 1
			GR.CIRCLE bSelect, bolaX, bolaY, bSize_m

			GR.COLOR 100, 0, 255, 0, 0
			GR.SET.STROKE 4
			GR.LINE bMoment, bolaX, bolaY, tcX, tcY
			GR.RENDER
		UNTIL !sTouch

		% Change speed based on how far in X and Y positions...
		% you were from the ball before you lifted your finger.

		vecSpeedX = -mmX/playerStrenght
		vecSpeedY = -mmY/playerStrenght
	ENDIF		
RETURN