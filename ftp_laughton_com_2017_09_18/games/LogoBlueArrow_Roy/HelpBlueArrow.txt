Logo Blue Arrow.

Blue Arrow, is a space game that uses my LogoLib.bas

You are the pilot of the new Blue Arrow space ship,
which you fly through space avoiding the planets and flying over the small gold stars.
After you fly over a star it changes into a small new planet. It must now be avoided.

The game starts with just two star and  once you have flown over all the stars, 
more star appear and the planets will be in different places. The space ship will increase in speed.

Fuel:
        You start off with 100 LB's of fuel, which is being used as you fly. 
	Each time you fly over a star you gain another 100 LB's of fuel.

Points:
	You gain 10 points each time you fly over a star and 100 bonus points for
	flying over all the stars in a round.

Game Over:
	The game is over if you run out of fuel. An alarm sounds when you get down to 50 LB's
	Flying into a planet is not going to end well and your space ship will explode anding  the game. 

Controls:
	Press the left side of the screen to Back Left.
	Press the right side of the screen to Bank Right.
	If you fly off the screen you will reappear at the other side.
Menu:
	Tap the Back Key to access the Menu, this also pauses the game
	From the menu, you can:
	Turn the sound on and off.
	Turn vibration on and of.
	Turn the vapour trail on and off.
	Reset the Best Score to zero.
	Read About or Help.
	Go back to the game. 
	Exit the game.

	

	
