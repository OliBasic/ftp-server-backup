REM Start of BASIC! Program
! 15 game by Tom Hatcher
! Nov 3, 2012

! numeric variables
numberPicked = 0.0
moveNumber=0.0
numberSqStillOpen=9.0


! boolean values
gameOver = false % 1??
turn$ = "human"  % whose turn


! arrays
DIM valuLine[8]   % value of all 8 lines
DIM ContentSq[9]  % what's on the square


! assigning data. could use Read/Data now
! 1
DIM squaresInLine[8,3]    % 8 lines of 3 in ttt

ARRAY.LOAD squaresInLineArray[], 1,2,3,4,5,6,7,8,9,1,4,7,2,5,8,3,6,9,1,5,9,3,5,7
FOR I = 1 TO 8
 FOR j= 1 TO 3
  squaresInLine[I,j]= squaresInLineArray[j+(i-1)*3]
 NEXT j
NEXT i

! 2
! numberLinesThruSq[9] lines thru each of 9 squares
ARRAY.LOAD numberLinesThruSq[], 3,2,3,2,4,2,3,2,3


! 3
DIM linesThruSq[9,4] % up to 4 lines, sq 5 has 4
ARRAY.LOAD linesThruSqArray[], 1,4,7,1,5,1,6,8,2,4,2,5,7,8,2,6,3,4,8,3,5,3,6,7

k=1
FOR I=1 TO 9
 FOR j =1 TO numberLinesThruSq[i]
  linesThruSq[I,j]=linesThruSqArray[k]
  k=k+1
 NEXT j
NEXT I


! 4
! for converting sq to number. computer needs
! the real board. 2 is in sq 1 etc
ARRAY.LOAD numberOnSqN[], 2,9,4,7,5,3,6,1,8

!for I=1 to 9
!print numberOnSqN[I]
! next I


!5 for converting number to square. opp picks number
! this converts the number to a square
! tells the location that number I is in
! 1 is in sq 8 on the real board

ARRAY.LOAD squareContainingNumber[],8,1,6,3,5,7,4,9,2

! for I=1 to 9
! print squareContainingNumber[I]
! next I






!  DIM valueLine[8] % 8 lines, each has a value
!  DIM statusSq[9]  % what's on each square

! array of text messages
DIM txtMessage[4] % arrays are objects action oriented BASIC!

! data all assigned




GOTO here7
!=======≠========================================

! begin picking by voice

! must be connected?  yes
TTS.INIT
TTS.SPEAK "what number do you pick?"
STT.LISTEN

STT.RESULTS guesses
LIST.GET guesses,1,pick$
I=VAL(pick$)
PRINT I*I
TTS.SPEAK "you picked"
TTS.SPEAK pick$



INPUT "Do you want me to go first?" , firstMove$

IF firstMove$="y" THEN GOTO ComputerMoves




tryAgain:
! player goes first
INPUT "What number do you select", N

IF (N<1 | N>9) THEN GOTO  tryAgain

PRINT N



ComputerMoves:

PRINT "yes!"


here7:



!           graphical interaction
! ≠====================================graphics


GR.OPEN 18,0,0,0,0,1
GR.SCREEN w,h

cellW=w/3
cellH=h/3

BL=0
BT=0
BR=w
BB=w % screen width, w
d=3

N=d*d

playerMove=0


DIM xLocSq[9]
DIM yLocSq[9]
!  Dim contentSq[9]
DIM numberDisplayed$[9]
DIM playerMove[5].   % for location picked #'s

! for number displayed
FOR I =1 TO 9
 numberDisplayed$[I]=LEFT$(STR$(I),1)
NEXT I


! find location of centers of squares
FOR row=1 TO 3
 FOR col=1 TO 3
  xLocSq[col+(row-1)*3]=BL+cellW/2+(col-1)*cellW
  yLocSq[col+(row-1)*3]=BT+cellW/2+(row-1)*cellW
 NEXT col
NEXT row

!     5555555555 added 11 23
DIM txt[9]
DIM PX[5]
DIM CXLoc[5]
DIM xLocPickedNumber[9]

Here8:

! draw gui
GR.COLOR 255, 0,200,0,1
GR.SET.STROKE w/100

! vertical lines
FOR I = 1 TO d-1
 GR.LINE ln1, BL+cellW*I, BT,BL+cellW*I ,BB
NEXT I


! horizontal lines
FOR I =1 TO d-1
 GR.LINE ln2,BL,BT+I*cellW, BR,I*cellW+BT
NEXT I
GR.RENDER


! place numbers on grid in center of square
GR.COLOR 255,255,0,0,1
!      Dim txt[9]
GR.TEXT.SIZE w/8

FOR I =1 TO 9
 GR.TEXT.DRAW txt[I],xLocSq[I]-h/40,yLocSq[I]+h/40,numberDisplayed$[I]
NEXT i

GR.RENDER
!pause 5000


!      Dim PX[5]     % location picked numbers
FOR I=1 TO 5
 PX[I]=w/10*(I-1)
NEXT i

!      DIM CXLoc[5]
FOR i=1 TO 5
 CXLoc[i]=w/2+w/10*i
NEXT i

! hold location of picked number
!      DIM xLocPickedNumber[9]



! ready for play *********************************
playerMove=1
computerMove=1


!**************************************************
Main:     % like Java
!    print "in main player move ";playerMove
GOSUB CheckForCatsGame
IF catsGame=1.0 THEN GOTO Ends
GOSUB PlayerMove
!        print "in main computer move ";computerMove
GOSUB CheckForPlayerWin
IF playerWins=1.0 THEN GOTO Ends
GOSUB CheckForCatsGame
IF catsGame=1.0 THEN GOTO Ends

GOSUB ComputerMove
GOSUB CheckForComputerWin
GOSUB CheckForCatsGame
IF catsGame=1.0 THEN GOTO Ends
IF computerWins=1.0 THEN GOTO Ends

GOTO Main

! *************************************************

PlayerMove:
DO
 GR.TOUCH touched, x, y
UNTIL touched
! necessary
DO
 GR.TOUCH touched, x,y
UNTIL !touched

IF y>w THEN GOTO Main

! get square touched
row=FLOOR(y/cellW)+1
col=FLOOR(x/cellW)+1
sq=col+(row-1)*3    % picked sq this number
!  need M[sq] here, but keep sqa

! added 11/12
sqRealBoard=squareContainingNumber[sq]

IF contentSq[sqRealBoard]<>0.0 THEN GOTO PlayerMove

! move picked number to below
GR.MODIFY txt[sq], "x", PX[playerMove]

xLocPickedNumber[sq]=PX[playerMove]

GR.MODIFY txt[sq], "y",w*5/4
playerMove=playerMove+1

! also need to do housekeeping

contentSq[sqRealBoard]=10   % puts 10 on sq if Player
!  puts 1 on sq if computer picks that sq

! check to see if Player has won
PRINT "player picks ";numberOnSqN[sqRealBoard]

numberSqStillOpen=numberSqStillOpen-1


GR.RENDER
RETURN



! //////////////////////////////////////////////
ComputerMove:

maxScore=0
FOR i=1 TO 9
 IF contentSq[i]=0 THEN %%% GOTO here0: % sq taken
 score=0
 duble=0
 FOR L=1 TO numberLinesThruSq[i]
  IF valuLine[linesThruSq[i,L]]=20 THEN score=score+500
  IF valuLine[linesThruSq[i,L]]=0 THEN score=score+1
  IF valuLine[linesThruSq[i,L]]=2 THEN score=score+1000
  IF MOD (i, 2) > 0.0 THEN score = score + 0.1
  IF valuLine[linesThruSq[i,L]]=10
   score=score+1
   duble=duble+1
   IF duble=2 THEN score=score+100
  ENDIF
 NEXT L

 IF score>=maxScore THEN
  maxScore= score
  maxI=i
  winningLine=L
  !    print "maxI= "; maxI;" ";i
 ENDIF

 ! special cases
  endif

!    here0:
NEXT i

PAUSE 1000

PRINT "computer picks ";numberOnSqN[maxI];"  with maxscore ="; maxScore

! move picked number
!  lblCMoves: handy?

GR.MODIFY txt[numberOnSqN[maxI]],"x",CXLoc[computerMove]
GR.MODIFY txt[numberOnSqN[maxI]],"y",5*w/4

xLocPickedNumber[numberOnSqN[maxI]]=CXLoc[computerMove]
GR.RENDER
! housekeeping
contentSq[maxI]=1
computerMove=computerMove+1

numberSqStillOpen=numberSqStillOpen-1
RETURN



!  Other Subroutines              P R O C E D U R E S
! ++++++++++++++++++++++++++++++++++++++++++++++++++

CheckLines2000:  % calculate valu each line
FOR i = 1 TO 8
 valuLine[I]=0.0
NEXT i

FOR i=1 TO 8
 FOR j=1 TO 3
  valuLine[i]= valuLine[i]+ contentSq[squaresInLine[i,j]]
 NEXT j
NEXT i

RETURN

! ++++++++++++++++++++++++++++++++++++++++++++++++

CheckForPlayerWin:
GOSUB CheckLines2000
FOR i =1 TO 8
 IF valuLine[i]=30
  GOSUB Congratulations
  PRINT "valu line = "; valuLine[i]
  !     i is the winning line
  winningLine=i
  GOSUB showWinningNumbers
  !   playerWins=1.0
 ENDIF
NEXT i
RETURN

! ++++++++++++++++++++++++++++++++++++++++++++++

CheckForCatsGame:
IF numberSqStillOpen =0 THEN
 catsGame=1.0
 GOSUB CatsGame
ENDIF
RETURN

! +++++++++++++++++++++++++++++++++++++++++++++++

Congratulations:
PRINT "you win"
GR.TEXT.DRAW txtMessage[3],w/4,3*h/4,"You win?"
GR.RENDER
!  pause 4000
playerWins=1.0
result=3

RETURN


Condolences:

GR.TEXT.DRAW txtMessage[2], w/10,3*h/4, "Sorry. You lose."
GR.RENDER
!  pause 4000
computerWins=1.0
result=2


RETURN
! ++++++++++++++++++++++++++++++++++++++++++++++++

CatsGame:
GR.TEXT.DRAW txtMessage[4],w/5,3*h/4, "Cat's game."
GR.RENDER
!   pause 4000
result=4
RETURN

! +++++++++++++++++++++++++++++++++++++++++++++++++
CheckForComputerWin:
GOSUB CheckLines2000
FOR i=1 TO 8
 IF valuLine[i]=3
  GOSUB Condolences
  winningLine=i
  GOSUB ShowWinningNumbers
  computerWins=1.0
 ENDIF
NEXT i
RETURN

! ++++++++++++++++++++++++++++++++++++++++++++++++
ShowWinningNumbers:
! i is the winning line. could use var winningLine
! yellow
GR.COLOR 255,255,255,0,1
! winningLine= the winning line
! what sqs on line winningLine
FOR j=1 TO 3
 dummy= squaresInLine[winningLine,j]
 PRINT numberOnSqN[dummy]; i
 PRINT "winning line = ";winningLine
 GR.TEXT.DRAW txt[numberOnSqN[dummy]],xLocPickedNumber[numberOnSqN[dummy]],w*5/4,numberDisplayed$[numberOnSqN[dummy]]

 GR.RENDER
NEXT j
!  pause 1000

RETURN

! +++++++++++++++++++++++++++++++++++++++++++++++++

! reset variables for new game, a subroutine

Reset:
catsGame=0.0
playerWins=0.0
computerWins=0.0
numberSqStillOpen=9.0
GR.CLS
GR.RENDER
! reset valuLine
FOR i = 1 TO 8
 valuLine[i]=0.0
NEXT i

! reset real board, arrays
FOR i=1 TO9
 contentSq[i]=0.0
NEXT i
RETURN

! ++++++++++++++++++++++++++++++++++++++++++




ENDs:
PAUSE 3000


! gr.modify txtMessage[2], "text","Play again?"
! gr.modify txtMessage[3], "text", "Play again"
GR.MODIFY txtMessage[result], "x", w/80
GR.MODIFY txtMessage[result], "text","Tap to play again!"
GR.RENDER

!  pause 5000

DO
 GR.TOUCH touched,x,y
UNTIL touched

DO
 GR.TOUCH touched,x,y
UNTIL !touched


GOSUB Reset
GOTO Here8


END


