! Life (Conway's game of life)
! Aat Don @2013
GR.OPEN 255,0,0,0,0,1
GR.SCREEN w,h
ScreenWidth=720
ScreenHeight=h/w*ScreenWidth
sX=w/ScreenWidth
sY=h/ScreenHeight
GR.SCALE sX,sY
GR.SET.STROKE 5
GR.TEXT.SIZE 40
WAKELOCK 3
Size=31
DIM OG[Size,Size],NG[Size,Size],Green[Size,Size]
! Intro
CLS
CONSOLE.TITLE "Conway's game of LIFE"
GR.FRONT 0
FOR i=1 TO 5
  PRINT
NEXT i
PRINT "   The Game of"
PRINT "LL"
PRINT "LL           FF"
PRINT "LL     II   FF     EE"
PRINT "LL          FF    EEEE"
PRINT "LL     II FFFFFF EE   EE"
PRINT "LL     II   FF   EEEEEEE"
PRINT "LL     II   FF   EE"
PRINT "LLLLLL II   FF    EEEEE"
PRINT "LLLLLL II   FF     EEE    O"
PRINT "Please wait, initialising..."
! Draw buttons
GR.BITMAP.CREATE Button1,720,150
GR.BITMAP.DRAWINTO.START Button1
  FOR i=1 TO 8
    GR.COLOR 255-i*30,255,255,0,0
    GR.OVAL b,30+i*5,5+i*5,230-i*5,95-i*5
    GR.OVAL b,260+i*5,5+i*5,460-i*5,95-i*5
    GR.OVAL b,490+i*5,5+i*5,690-i*5,95-i*5
  NEXT i
  GR.COLOR 255,255,255,255,1
  GR.RECT b,50,30,210,70
  GR.RECT b,280,30,440,70
  GR.RECT b,510,30,670,70
  GR.COLOR 255,0,0,255,1
  GR.TEXT.DRAW b,60,65,"SELECT"
  GR.TEXT.DRAW b,290,65,"CREATE"
  GR.TEXT.DRAW b,540,65,"LOAD"
  GR.COLOR 255,255,255,255,1
  GR.TEXT.SIZE 60
  GR.TEXT.DRAW b,90,145,"PATTERN OPTIONS"
GR.BITMAP.DRAWINTO.END
GR.BITMAP.DRAW But1,Button1,0,720
! Draw border
GR.COLOR 255,120,120,120,1
GR.RECT g,0,180,719,720
GR.COLOR 255,0,0,0,1
GR.RECT g,110,200,610,700
! Draw matrix
GR.COLOR 255,0,255,0,1
FOR i=4 TO Size-3
  FOR j=4 TO Size-3
    GR.CIRCLE Green[i,j],i*20+40,j*20+130,10
    GR.HIDE Green[i,j]
  NEXT Y
  PRINT
  PAUSE 200
NEXT X
! DRAW GRID to create pattern
GR.BITMAP.CREATE GR1,251,380
GR.BITMAP.DRAWINTO.START GR1
  GR.COLOR 240,255,255,255,0
  FOR i=1 TO 251 STEP 50
    GR.LINE g,i,1,i,250
    GR.LINE g,1,i,250,i
  NEXT i
  GR.COLOR 255,255,255,255,1
  GR.TEXT.SIZE 30
  GR.SET.STROKE 1
  GR.TEXT.DRAW g,30,290,"Tap squares"
  GR.TEXT.DRAW g,15,325,"to create pattern"
  GR.TEXT.SIZE 40
  GR.COLOR 255,0,0,255,1
  GR.RECT g,25,330,225,375
  GR.COLOR 240,255,255,255,1
  GR.TEXT.DRAW g,65,365,"READY"  
GR.BITMAP.DRAWINTO.END
GR.BITMAP.DRAW Grid,GR1,240,300
GR.HIDE Grid
GR.FRONT 1
CLS
GR.RENDER
! Select option
DO
  DO
    GR.TOUCH touched,x,y
  UNTIL touched
  DO
    GR.TOUCH touched,x,y
  UNTIL !touched
  x=ROUND(x/sX)
  y=ROUND(y/sY)
UNTIL y>720 & y<820
GR.HIDE But1
GR.RENDER
IF x<240 THEN
  ! Select pattern
  Retry:
  INPUT "Select Pattern (1-6, 7=random)",Pattern
  IF Pattern<1 | Pattern>7 THEN GOTO Retry
  IF Pattern=7 THEN
    ! Random Pattern
    FOR i=14 TO 18
      FOR j=14 TO 18
        OG[j,i]=ROUND(RND())
      NEXT j
    NEXT i
  ELSE
    ! Get Pattern
    READ.FROM 1+(Pattern-1)*25
    FOR i=14 TO 18
      FOR j=14 TO 18
        READ.NEXT OG[j,i]
      NEXT j
    NEXT i
  ENDIF
ENDIF
IF x>240 & x<480 THEN
  ! Create Pattern
  GR.SHOW Grid
  GR.RENDER
  DO
    GR.TOUCH touched,x,y
  UNTIL touched
  DO
    GR.TOUCH touched,x,y
  UNTIL !touched
  x=ROUND(x/sX)
  y=ROUND(y/sY)
  DO
    DO
      GR.TOUCH touched,x,y
    UNTIL touched
    DO
      GR.TOUCH touched,x,y
    UNTIL !touched
    x=ROUND(x/sX)
    y=ROUND(y/sY)
    ix=FLOOR((x-240)/50)+1
    iy=FLOOR((y-300)/50)+1
    IF ix>0 & ix<6 & iy>0 & iy<6 THEN
      GR.BITMAP.DRAWINTO.START GR1
        IF OG[ix+13,iy+13]=0 THEN
          OG[ix+13,iy+13]=1
          GR.COLOR 255,0,255,0,1
          GR.CIRCLE g,25+(ix-1)*50,25+(iy-1)*50,22
        ELSE
          OG[ix+13,iy+13]=0
          GR.COLOR 255,0,0,0,1
          GR.CIRCLE g,25+(ix-1)*50,25+(iy-1)*50,22
        ENDIF
      GR.BITMAP.DRAWINTO.END
      GR.RENDER
    ENDIF
  UNTIL x>265 & x<465 & y>630 & y<675
  GR.HIDE Grid
  GR.RENDER
ENDIF
IF x>480 THEN
  ! Load pattern
  TEXT.OPEN R,F,"Pattern.txt"
    TEXT.READLN F,N$
    Incorrect:
    INPUT FORMAT$("#%",VAL(N$))+" pattern(s) found. Enter number 1 -"+FORMAT$("#%",VAL(N$)),n
    IF n<1 | n>VAL(N$) THEN GOTO Incorrect
    FOR i=1 TO n
      TEXT.READLN F,R$
    NEXT i
  TEXT.CLOSE F
  FOR i=1 TO 5
    FOR j=1 TO 5
      OG[j+13,i+13]=VAL(WORD$(R$,j+(i-1)*5,","))
    NEXT j
  NEXT i
ENDIF
! Save start pattern in string
FOR i=1 TO 5
  FOR j=1 TO 5
    P$=P$+STR$(OG[j+13,i+13])
    IF i*j<25 THEN P$=P$+","
  NEXT j
NEXT i
INPUT "Enter max. number of generations",MaxGens,50
! These vars are used to restrict search area to living cells only
CellStrtX=13
CellEndX=19
CellStrtY=13
CellEndY=19
POPUP "Starting pattern",0,50,0
! Get population
ARRAY.SUM Pop,OG[]
! Display generation 0
GR.COLOR 255,0,255,0,1
FOR i=CellStrtX TO CellEndX
  FOR j=CellStrtY TO CellEndY
    IF OG[i,j] > 0 THEN
      GR.SHOW Green[i,j]
    END IF
  NEXT j
NEXT i
GR.COLOR 255,255,255,0,1
GR.TEXT.DRAW q,180,50,"Game of LIFE"
GR.TEXT.DRAW q,10,100,"Generation:"
GR.TEXT.DRAW q,350,100,"Population:"
GR.TEXT.DRAW qG,200,100,FORMAT$("##%",Gens)
GR.TEXT.DRAW qP,540,100,FORMAT$("##%",Pop)
GR.RENDER
PAUSE 3000
DO
  NewStrtX=CellEndX
  NewEndX=CellStrtX
  NewStrtY=CellEndY
  NewEndY=CellStrtY
  Q=0
  Stable=0
  FOR i=CellStrtX-1 TO CellEndX+1
    FOR j=CellStrtY-1 TO CellEndY+1
      Nbrs=0
      ! Cell on grid ?
      Outside=0
      IF ((i+1)<Size) & ((j+1)<Size) & ((i-1)>0) & ((j-1)>0) THEN
        !count Neighbours
        IF OG[i-1,j-1]=1 THEN Nbrs=Nbrs+1
        IF OG[i-1,j]=1 THEN Nbrs=Nbrs+1
        IF OG[i-1,j+1]=1 THEN Nbrs=Nbrs+1
        IF OG[i,j-1]=1 THEN Nbrs=Nbrs+1
        IF OG[i,j+1]=1 THEN Nbrs=Nbrs+1
        IF OG[i+1,j-1]=1 THEN Nbrs=Nbrs+1
        IF OG[i+1,j]=1 THEN Nbrs=Nbrs+1
        IF OG[i+1,j+1]=1 THEN Nbrs=Nbrs+1
        ! If cell is alive and has 2 or 3 Neighbours it will stay alive
        ! If cell is dead and has 3 Neighbours, it will be (re)born
        ! Else cell will die
        IF ((OG[i,j]=1) & ((Nbrs=2) | (Nbrs=3))) | ((OG[i,j]=0) & (Nbrs=3)) THEN
          NG[i,j]=1
          IF i-1<NewStrtX THEN NewStrtX=i-1
          IF i+1>NewEndX THEN NewEndX=i+1
          IF j-1<NewStrtY THEN NewStrtY=j-1
          IF j+1>NewEndY THEN NewEndY=j+1
        ENDIF
        ! Check if stable
        IF OG[i,j]=NG[i,j] THEN Q=Q+1
      ELSE
        Outside=1
      ENDIF
    NEXT j
  NEXT i
  IF Q=(CellEndY-CellStrtY+3)*(CellEndX-CellStrtX+3) THEN Stable=1
  IF Outside=0 THEN
    ! Get new population
    ARRAY.SUM Pop,NG[]
    ! Display next generation
    FOR i=CellStrtX TO CellEndX
      FOR j=CellStrtY TO CellEndY
        IF NG[i,j]=1 THEN
          GR.SHOW Green[i,j]
        ELSE
          GR.HIDE Green[i,j]
        ENDIF
      NEXT j
    NEXT i
    Gens=Gens+1
    GR.COLOR 255,255,255,0,1
    GR.MODIFY qG,"text",FORMAT$("##%",Gens)
    GR.MODIFY qP,"text",FORMAT$("##%",Pop)
    GR.RENDER
    ! Copy new cells and erase old
    FOR i=CellStrtX TO CellEndX
      FOR j=CellStrtY TO CellEndY
        OG[i,j]=NG[i,j]
        NG[i,j]=0
      NEXT
    NEXT
    CellStrtX=NewStrtX
    CellEndX=NewEndX
    CellStrtY=NewStrtY
    CellEndY=NewEndY
  ELSE
    Gens=MaxGens
  ENDIF
UNTIL Gens=MaxGens | Pop=0 | Stable=1
GR.COLOR 255,255,255,0,1
IF Stable=1 THEN
  GR.TEXT.DRAW q,180,150,"Stabilized !"
ELSE
  IF Pop=0 THEN
    GR.TEXT.DRAW q,180,150,"Extinguished !"
  ELSE
    IF Outside=1 THEN
      GR.TEXT.DRAW q,180,150,"Border reached !"
    ELSE
      GR.TEXT.DRAW q,180,150,"Final generation !"
    ENDIF
  ENDIF
ENDIF
GR.RENDER
PAUSE 2000
! Save pattern ?
INPUT "Save start Pattern (Y/N)",SP$
IF UPPER$(SP$)="Y" THEN
  ! Create file if does not exist
  FILE.EXISTS FE,"Pattern.txt"
  IF FE<>0 THEN
    ! Replace 1st line
    GRABFILE AF$,"Pattern.txt"
    l=IS_IN(CHR$(13),AF$,1)-1
    Num$=STR$(VAL(LEFT$(AF$,l))+1)
    AF$=Num$+RIGHT$(AF$,LEN(AF$)-l)
    TEXT.OPEN W,F,"Pattern.txt"
      TEXT.WRITELN F,AF$+P$
    TEXT.CLOSE F
  ELSE
    TEXT.OPEN W,F,"Pattern.txt"
      TEXT.WRITELN F,STR$(1)
      TEXT.WRITELN F,P$
    TEXT.CLOSE F
  ENDIF
ENDIF
GR.COLOR 255,255,255,0,1
GR.TEXT.DRAW q,160,760,"Hit screen to exit"
GR.RENDER
DO
  GR.TOUCH touched,x,y
UNTIL touched
DO
  GR.TOUCH touched,x,y
UNTIL !touched
GR.CLOSE
WAKELOCK 5
EXIT
!Pattern1 standard
READ.DATA 0,0,1,1,1,0,1,0,0,1,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0
!Pattern2 oscillator
READ.DATA 0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0
!Pattern3 spaceship
READ.DATA 0,0,0,0,0,0,1,0,0,0,0,0,1,1,0,0,1,1,0,0,0,0,0,0,0
!Pattern4 stable after 9 generations
READ.DATA 0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,0,1,1,1,1,0,0
!Pattern5 starts oscillation after 12 generations
READ.DATA 1,1,0,0,1,0,1,0,0,0,1,1,0,0,1,0,1,1,1,1,0,1,1,1,0
!Pattern6 part oscillates, part stable after 26 generations
READ.DATA 0,1,1,1,0,0,1,1,0,1,1,0,0,1,1,0,1,1,0,1,0,0,0,1,1

