REM Jeu de Labirynthes  @Cassiope34 1216

Fn.def v(c$,p)    % convert character to value  p=1 -> x, p=2 -> y
  Fn.rtn ASCII(c$,p)-64
Fn.end

Fn.def v$(x,y)    % convert x,y values to characters
  Fn.rtn chr$(64+x, 64+y)
Fn.end

FN.DEF afft(dx,dy,ax,ay,tpt[],cl[],act)   % act=0 hide, act=1 show
 trx =0
 if     abs(ax-dx)=1 & ay=dy     % horizontal
   trx =2*(max(ax,dx)-1)
   try =2*ay-1
 elseif abs(ay-dy)=1 & ax=dx     % vertical
   trx =2*ax-1
   try =2*(max(ay,dy)-1)
 endif
 if trx
   gr.modify tpt[trx,try], "paint", cl[act]
   gr.show tpt[trx,try]
 endif
FN.END

FN.DEF sens(dx,dy,ax,ay)   % direction  1 North  2 East  3 South  4 West
 se =0
 if     abs(ax-dx)=1 & ay=dy     % horizontal
   if ax>dx then se =2 else se =4
 elseif abs(ay-dy)=1 & ax=dx     % vertical
   if ay>dy then se =3 else se =1
 endif
 FN.RTN se
FN.END

gr.open 255,252,241,211,0,1    % ivoire
gr.screen w,h
scx =800
scy =1280
sx  =w/scx
sy  =h/scy
gr.scale sx,sy

wakelock 3
DIM cl[2]
ARRAY.LOAD diX[],0,1,0,-1  % 1 North  2 East  3 South  4 West
ARRAY.LOAD diY[],-1,0,1,0

pgx =40    % position de la grille en pixels.
pgy =40
ncx =17    % doivent être impairs car les bords sont des murs...
ncy =2*ncx

DO                % ---------- new game -----------
  new  =0
  quit =0
  tr$  ="" :ntc =0
  GOSUB Draw_Maze
  grx =nx : gry =ny
  
  DO              % ---------- principal loop -----------
    do
      gr.touch touched, tx, ty
      pause 100
      if !background() then gr.render
    until touched | quit
    if quit then d_u.break
    tx/=sx
    ty/=sy
    ctx =int((tx-pgx)/cs)+1
    cty =int((ty-pgy)/cs)+1
    if ty<scy-100
      if tr$=""    % départ
        if ctx<>STARTX | cty<>STARTY
          d_u.continue
        else
          oldcx =ctx : oldcy =cty : tr$ =v$(ctx,cty)
        endif
      elseif IS_IN(v$(ctx,cty),tr$) & v$(ctx,cty)<>right$(tr$,2)    % touche le trait hors dernier point.
        oldcx =ctx : oldcy =cty
      endif
      gr.modify grid, "text", int$(nx)+" x "+int$(ny)   % the true grid size
    endif
    do
      gr.touch touched, x, y
      x/=sx
      y/=sy
      cx =int((x-pgx)/cs)+1
      cy =int((y-pgy)/cs)+1
      if cx<=nx & cy<=ny & (cx<>oldcx | cy<>oldcy)
        di =sens(oldcx,oldcy,cx,cy)    % direction  0 si pas voisine.
        if di
          if !BAND(c[oldcx,oldcy],POW(2,di))
            afft(oldcx,oldcy,cx,cy,tpt[],cl[],1)
            cc$ =v$(cx,cy)
            if !IS_IN(cc$,tr$) then tr$ +=cc$ : ntc++
            oldcx =cx : oldcy =cy
            gr.modify mess, "text", int$(ntc)+" / "+int$(pl) : gr.render
            if cx=TARGX & cy=TARGY
              if ntc=pl then POPUP "  BRAVO!  " else GOSUB Trace
              d_u.break
            endif
          endif
        endif
      endif
    until !touched
    if y>scy-100
      if x<180      % reset
        Dialog.message win$,"       Are you sure?", ok, " New ", "Same"," Cancel "
        new =(ok=1) : if ok =2 then GOSUB Encore
      elseif x>scx-100    % '+'
        if grx<20 then grx+=2 :gr.modify grid, "text", int$(grx)+" x "+word$("12 15 18 21 24 27 30",grx/2-3)
      elseif x>scx-340 & x<scx-260    % '-'
        if grx>8 then grx-=2 :gr.modify grid, "text", int$(grx)+" x "+word$("12 15 18 21 24 27 30",grx/2-3)
      endif
    endif
    
  UNTIL new
  
  popup " Please wait... "
  ncx =grx*2+1   % de 8 à 20 cases horizontales...!
  ncy =ncx*2
  
UNTIL quit
!gr.close :pause 100  % ????
wakelock 5
END" Bye...!"

OnBackKey:
  Dialog.message win$,"       What do you want ?", ok, " Exit ", " Cancel "
  quit =(ok=1)
back.resume

Encore:           %  re-init the same maze.
for x=1 to 2*nx-1 : for y=1 to 2*ny-1 : gr.hide tpt[x,y] : next : next
ntc =0 : tr$ ="" : gr.modify mess, "text", int$(ntc)+" / "+int$(pl) : gr.render
return

Draw_Maze:
nx =int(ncx/2) : ny =int(ncy/2)
cs =int((scx-2*pgx)/nx)   % cellule carrée
if pgy+ny*cs >scy-200 then ny =int((scy-200)/cs)   % adapte 'ny' pour laisser la place au menu en bas.
ncx =nx*2+1 : ncy =ny*2+1
GR.CLS
GOSUB MazeGenerate
undim c[] : DIM c[nx,ny]
bsx =scx : bsy =2*pgy+ny*cs
!if maze then gr.bitmap.delete maze
!gr.bitmap.create maze, bsx, bsy
!gr.bitmap.drawinto.start maze
gr.set.stroke pgx
gr.color 255,172,172,172,1
gr.rect nul, 0, 0, bsx, bsy  % gris
gr.color 255,252,241,211,1
gr.rect nul, pgx, pgy, pgx+nx*cs, pgy+ny*cs  % ivoire
gr.set.stroke 6
gr.color 255,72,72,72,0
for y = 2 to ncy step 2    % conversion de cas[] vers c[]
  for x = 2 to ncx step 2
    for d=1 to 4
      dx =x+diX[d] : dy =y+diY[d]
      if cas[dy,dx]
        ax =int(x/2) : ay =int(y/2)
        c[ax,ay] +=POW(2,d)   %  binaire en dec.
        if d=1
          dlx =pgx+(ax-1)*cs : alx =pgx+ax*cs
          dly =pgy+(ay-1)*cs : aly =pgy+(ay-1)*cs
        elseif d=2
          dlx =pgx+ax*cs     : alx =pgx+ax*cs
          dly =pgy+(ay-1)*cs : aly =pgy+ay*cs
        elseif d=3
          dlx =pgx+(ax-1)*cs : alx =pgx+ax*cs
          dly =pgy+ay*cs     : aly =pgy+ay*cs
        elseif d=4
          dlx =pgx+(ax-1)*cs : alx =pgx+(ax-1)*cs
          dly =pgy+(ay-1)*cs : aly =pgy+ay*cs        
        endif
        gr.line nul, dlx, dly, alx, aly
      endif
    next
  next
next
gr.color 255,252,241,211,1    % ivoire
STARTX =int(rnd()*nx) : STARTY =1
gr.rect nul, pgx+STARTX*cs, 0, pgx+(STARTX+1)*cs, pgy+3
TARGX  =int(rnd()*nx) : TARGY =ny
gr.rect nul, pgx+TARGX*cs, pgy+ny*cs-3, pgx+(TARGX+1)*cs, pgy+(ny+1)*cs
!gr.bitmap.drawinto.end
!gr.bitmap.draw nul, maze, 0, 0
GOSUB Traits
gr.color 255,0,255,0,1    % vert
gr.circle curs, pgx+(STARTX+0.5)*cs, pgy+(STARTY-0.5)*cs, cs/3
gr.paint.get cl[1]
  gr.color 255,0,150,255,1
  gr.text.size 40
  gr.text.align 2
  gr.text.draw grid, scx-180, scy-40, int$(nx)+" x "+int$(ny)   % dimension
  gr.text.draw mess, scx/2.5, scy-40, ""
  
  gr.color 255,255,150,0,1   % orange
  gr.paint.get cl[2]
  gr.text.draw nul, 90, scy-42, "RESET"
  gr.color 80,200,200,200,1
  gr.rect nul, 10, scy-100, 180, scy-14
  gr.rect nul, scx-340, scy-100, scx-260, scy-14
  gr.rect nul, scx-100, scy-100, scx-20, scy-14
  gr.text.size 70
  gr.color 255,255,150,0,0
  gr.text.draw nul, scx-300, scy-35, "-"
  gr.text.draw nul, scx-60, scy-35, "+"
gr.render
GOSUB PathFinder    % gives the shortest path.
RETURN

Traits:    % crée tous les traits.
undim tpt[] : DIM tpt[2*nx-1,2*ny-1]
gr.color 255,0,255,0,1    % vert
ts =cs/2
for x=1 to 2*nx-1
  for y=1 to 2*ny-1
    dx =0
    if mod(x,2)=0 & mod(y,2)>0     % horizontal
      dx =pgx+(x-1)*ts : dy =pgy+y*ts
      ax =pgx+(x+1)*ts : ay =pgy+y*ts
    elseif mod(x,2)>0 & mod(y,2)=0   % vertical
      dx =pgx+x*ts : dy =pgy+(y-1)*ts
      ax =pgx+x*ts : ay =pgy+(y+1)*ts
    endif
    if dx then gr.line tpt[x,y], dx, dy, ax, ay : gr.hide tpt[x,y]
  next
next
return

MazeGenerate:
 td = clock()
 nRows = 0
 nColumns = 0
 totalCells = 0
 visitedCells = 0
 currentCell = 0
 cellNext = 0
 lastCell$ = ""
 path$ = ""
 undim cas[]
 DIM cas[ncy,ncx]
 
 for y = 1 to ncy    % une case sur 2 est un mur au départ...
   for x = 1 to ncx
     cas[y,x] =1 : if y>1 & x>1 & !mod(x,2) & !mod(y,2) & x<ncx & y<ncy then cas[y,x] = 0
   next
 next

 nRows    = int(ncx/2)
 nColumns = int(ncy/2)
 undim cell[]
 DIM cell[nRows+1, nColumns+1]
 For row = 1 to nRows
   For column = 1 to nColumns
     cell[row,column] = (100*row + column) * -1
   Next
 Next
 cell[nRows,1]= Abs(cell[nRows,1])
 currentCell  = cell[nRows,1]
 totalCells   = nRows*nColumns
 visitedCells = 1
 cellNext     = 1

 While visitedCells < totalCells
   GOSUB CellNextS
   IF cellNext > 0
     visitedCells++
     currentCell = cellNext
     GOSUB CurrentCellS
     path$ = path$+"-"+currentCell$
   ELSEIF cellNext = 0
     path$ = Left$(path$,Len(path$)-7)
     lastCell$ = Right$(path$,6)
     currentCell = Val(Left$(lastCell$,3))*100 + Val(Right$(lastCell$,3))
   EndIF
 Repeat
 
 ! how to increase the difficulty ?!  by removing some walls...?
 trm =0 : tre =int(((nx*ny)/100)*10)
 do
   xe =int(rnd()*(ncx-8))+4
   ye =int(rnd()*(ncy-8))+4
   if cas[ye,xe] then cas[ye,xe] =0 :trm++
 until trm =tre
 
RETURN

CellNextS:   % depend of currentCell
 nCm =0 : cellNext =0 : path =0 : nC$ ="" :cellNext$ =""
 mx  =Abs(int(currentCell/100))
 my  =Abs(mx*100 - currentCell)
 for d=1 to 4
   dx =mx+diX[d] : dy =my+diY[d]
   if dx>0 & dy>0
     if cell[dx,dy]<0 then nCm++ : nC$ +=int$(Abs(cell[dx,dy]))+" "
   endif
 next
 if nCm
   rndPath   =int(Rnd()*nCm)+1
   cellNext$ =Word$(nC$, rndPath)
   cellNext  =Val(cellNext$)
   rN =int(cellNext/100)
   cN =cellNext - rN*100
   If mx - rN = 1 Then path = 1     % North East South West
   If cN - my = 1 Then path = 2
   If rN - mx = 1 Then path = 3
   If my - cN = 1 Then path = 4
   cell[rN,cN]= Abs(cell[rN,cN])
   if cas[my*2+diX[path],mx*2+diY[path]]<>0 then cas[my*2+diX[path],mx*2+diY[path]] =0
 endif
RETURN

CurrentCellS:    % depend of currentCell
 r  =int(currentCell/100)
 r$ =right$(int$(1000+r),3)
 cc =currentCell - r*100
 c$ =right$(int$(1000+cc),3)
 currentCell$ =r$+c$
RETURN

!  ************ DEBUT de la RECHERCHE du PLUS COURT CHEMIN *************

PathFinder:
STARTX++
TARGX++
 xc  =STARTX  % case courante
 yc  =STARTY
 ppp =0   % distance pour arriver là du parent... ici 0 puisque c'est la case de départ !
 success =0
 
! format des listes = "_XXYYxxyypppCCC_XXYYxxyypppCCC" etc...  une case = 15 caractères.
     ! XXYY = coord. de la case
     ! xxyy = coord. de son parent (la case d'ou elle vient)
     ! ppp  = cout de ce parent pour venir jusqu'ici dans le chemin.  (cpv)
     ! CCC  = cout de la case = ppp + 1 + distance de la case à l'arrivée à vol d'oiseau...

!         la case       son parent         ppp         son cout (sur 3 chiffres)
 lf$ = "_"+right$(format$("%%",STARTX),2)+right$(format$("%%",STARTY),2)~
          +right$(format$("%%",STARTX),2)+right$(format$("%%",STARTY),2)  % liste fermée
 lo$ = ""    % liste ouverte
 td  = clock()
 DO
   FOR i=1 to 4          % check all 4 directions
     if !BAND(c[xc,yc],POW(2,i))   % if not a wall in 'i' direction.
       Xnext = xc + diX[i]    % coordonnée de la case voisine dans la direction i.
       Ynext = yc + diY[i]
       ccc = ppp + abs(TARGY-Ynext) + abs(TARGX-Xnext)  % Calcul du Cout de cette case voisine.
       ct$ = "_"+ right$(format$("%%",Xnext),2)+ right$(format$("%%",Ynext),2)  % case à rechercher...
       if Ynext = TARGY & Xnext = TARGX   % arrivée...
          lf$ = lf$ + ct$ +right$(format$("%%",xc),2)+right$(format$("%%",yc),2)  % liste fermée
          success = 1
          F_n.break
       else
          if Is_In(ct$, lf$) = 0   % si la case n'est PAS dans la liste fermée ...
            slo = Is_In(ct$, lo$)  % on la cherche dans la Liste Ouverte lo$
            if slo = 0     % si la case n'est pas dans la liste ouverte : on l'y met et c'est tout...
              lo$= lo$ +ct$ + right$(format$("%%",xc),2)+ right$(format$("%%",yc),2)~
                   + right$(format$("%%%",ppp),3)+ right$(format$("%%%",ccc),3)   % liste ouverte
              if show 
                gr.modify casPtr[Xnext,Ynext], "bitmap", casBmp[5]
                gr.render
              endif

            else
               ! si elle y est : comparer les 2 coûts
              cclo = val(mid$(lo$, slo + 12, 3))   % cout de cette case déjà présente dans lo$
              if ccc < cclo   % si le cout actuel est inférieur à celui de cette case trouvée dans lo$ :
       ! mise à jour des données de cette case dans la liste ouverte : " parent ppp et cout"
                lt$ = left$(lo$,slo+4)   % partie gauche de lo$ y compris les XXYY de la case à mettre à jour
                lt$ = lt$ + right$(format$("%%",xc),2) + right$(format$("%%",yc),2)~
                          + right$(format$("%%%",ppp),3) + right$(format$("%%%",ccc),3) % mise à jour du parent + cout
                lo$ = lt$ + right$(lo$,len(lo$)-len(lt$))     % reconstruction de lo$
              endif
            endif
          endif
        endif
      endif
    NEXT
    if success then D_u.break
    nclo = len(lo$)/15  % nbre total de cases dans lo$ (liste ouverte)  (rappel: 1 case = 15 caractères)

    if nclo = 0         % si liste ouverte vide = pas de solution... on arrête.
       D_u.break

    elseif nclo = 1     % s'il n'y a qu'une case dans lo$ alors c'est celle là qui est sélectionnée bien sur...!
       ncppc = 1

    else                % localise dans lo$ le n° de la case qui a le plus petit cout... = ncppc
       mc = val(right$(lo$,3))  % cout référence = celui de la DERNIERE case enregistrée dans lo$
       ncppc = nclo
       for n = nclo to 1 step -1   % puis on remonte case par case dans lo$ en partant de la fin...
         mc2 = val(mid$(lo$, n*15-2, 3))  % cout de la case précédente dans lo$
         if mc2 < mc    % cout inférieur trouvé...
            mc = mc2
            ncppc = n   % ncppc = localisation (base 15) dans lo$ de la case qui a le plus petit cout.
         endif
       next
    endif

  ! maintenant retirer de lo$ la case qui a le plus petit cout (déterminée ci-dessus)
  ! la mettre dans la liste fermée.
  ! en faire la case courante.

   lt$ = mid$(lo$,(ncppc-1)*15+1,15)  % la case choisie dans lo$ est mise dans une chaine temporaire.
   xc  = val(mid$(lt$,2,2))  % devient la nouvelle case courante, (et donc le nouveau parent...)
   yc  = val(mid$(lt$,4,2))
   ppp = val(mid$(lt$,10,3)) + 1  % nouveau cout pour venir jusqu'ici (dans le chemin)

   lf$ = lf$ + left$(lt$,9)       % ajoutée à la liste fermée  (constitution du chemin)

    ! retrait de cette case de lo$ (liste ouverte) :
   lt$ = left$(lo$,(ncppc-1)*15)  % partie gauche de lo$ avant la case choisie
   rst = nclo - (len(lt$)/15+1)
   lo$ = lt$ + right$(lo$,rst*15)  % reconstitution de la Liste Ouverte

 UNTIL success>0   % --------------  FIN de la RECHERCHE du PLUS COURT CHEMIN  ---------------

 t = clock() - td
 
 if success
   pl =0
   i  =Is_In("_"+right$(format$("%%",Xnext),2)+right$(format$("%%",Ynext),2), lf$)
   while i > 1
     ax = val(mid$(lf$,i+5,2))
     ay = val(mid$(lf$,i+7,2))
     i = Is_In("_"+right$(format$("%%",ax),2)+right$(format$("%%",ay),2), lf$)
     pl++
   Repeat
   gr.modify mess, "text", "0 / "+int$(pl)
 else
   gr.modify mess, "text", "impossible."
 endif
RETURN

Trace:     % draw the pathfinder's path.
i  =Is_In("_"+right$(format$("%%",Xnext),2)+right$(format$("%%",Ynext),2), lf$)
while i > 1
  dx = val(mid$(lf$,i+1,2))
  dy = val(mid$(lf$,i+3,2))
  ax = val(mid$(lf$,i+5,2))
  ay = val(mid$(lf$,i+7,2))
  afft(dx,dy,ax,ay,tpt[],cl[],2)   % draw the path.
  i = Is_In("_"+right$(format$("%%",ax),2)+right$(format$("%%",ay),2), lf$)
Repeat
return
