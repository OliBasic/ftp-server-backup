Rem Xmas Match
Rem For Android
Rem With RFO Basic!
Rem December 2016
Rem Version 1.00
Rem By Roy Shepherd

di_height = 672 % set to my Device
di_width = 1152

gr.open 255,0, 0, 128
gr.orientation 0 % Landscape  
pause 1000
WakeLock 3
 
gr.screen screenWidth, screenHeight

scale_x = screenWidth / di_width
scale_y = screenHeight / di_height
gr.scale scale_x, scale_y

gosub Functions
gosub LoadSounds
gosub ShowStartScreen
gosub Initialise

do
    gosub SetUp
    gosub PlayXmasMatch
    gosub NewGame
until GameOver
gosub SaveData : pause 500

exit

onBackKey:
    do : dialog.message "Exit Game",,yn,"Yes","No" : until yn > 0
    if yn = 1 then gosub SaveData : pause 50 : exit 
back.resume 

!------------------------------------------------
! Snow scene at start of game
!------------------------------------------------
ShowStartScreen:
    dim snow[100], snowX[100], snowY[100]
    path$ = "XmasMatch/data/"
    ! load snow scene
    
    gr.bitmap.load snowScene, path$ + "snowScene.png"
    if snowScene = - 1 then call ImageLoadError("snowScene.png")
    gr.bitmap.scale snowScene, snowScene, di_width, di_height 
    gr.bitmap.draw snowScene, snowScene, 0, 0

    ! Load and display start button
    gr.bitmap.load startButton, path$ + "startButton.png"
    if startButton =- 1 then call ImageLoadError("startButton.png")
    gr.bitmap.draw startButton, startButton, (di_width / 2) - 100, (di_height / 2) + 200
    
    ! Draw the snow
    gr.render : gr.color 255, 255, 255, 255
    gr.set.stroke 3
    
    for x = 1 to 100 
        snowX[x] = floor(di_width * rnd())
        snowY[x] = floor(- di_height * rnd())
        gr.point snow[x], snowX[x], snowY[x]
    next
    
    ! Collision for showing the toys
   gr.color 0 : gr.point collision, - 1, - 1 : gr.color 255
    do
        gr.touch t, tx, ty
        if t then 
            tx /= scale_x : ty /= scale_y
            gr.modify collision, "x", tx, "y", ty
        endif
        gosub FallingSnow
        
    until gr_collision(collision, startButton)
    call PlaySound(sound, buttonClick)
    if buzz then vibrate buzzGame[], - 1
    do : gr. touch t, tx, ty : gosub FallingSnow : until ! t
    gr.bitmap.delete snowScene
return

!------------------------------------------------
! Do once at first run of game
!------------------------------------------------
Initialise:
    dim toys[10], box[10], rotBox[10], gifts[5], underTree[3]
    w = di_width : h = di_height
    
    array.load boxX[], 546, 486, 596, 426, 536, 646, 376, 486, 596, 706
    array.load boxY[], 150, 260, 260, 370, 370, 370, 480, 480, 480, 480
    
    ! Load, scale, and draw the tree
    gr.bitmap.load tree, path$ + "tree5.png"
    if tree = - 1 then call ImageLoadError("tree5.png")
    gr.bitmap.scale tree, tree, w, h
    gr.bitmap.size tree, treeSizeX, treeSizeY
    gr.bitmap.draw xmasTree, tree, (w / 2) - (treeSizeX / 2), h - (treeSizeY)
    
    ! Load the fairy that sits on the tree
    gr.bitmap.load fairy, path$ + "fairy.png"
    gr.bitmap.draw fairy, fairy, (w / 2) - 20, 25
    
    ! Load and separate the toys 
    gr.bitmap.load gift, path$ + "allToys.png"
    if gift = - 1 then call ImageLoadError("allToys.png")
	
    for p = 1 to 5
        gr.bitmap.crop gifts[p], gift, (p - 1) * 100, 0, 100, 100
        gr.bitmap.scale gifts[p], gifts[p], 100, 100
    next
    
    ! Draw the toys off screen
    toy = 1
    for t = 1 to 10 step 2
        gr.bitmap.draw toys[t], gifts[toy], - 100, - 100
        gr.bitmap.draw toys[t + 1], gifts[toy], - 100, - 100
        toy ++ 
        ! if toy = 6 then toy = 1
    next
    
    ! load and draw the options button
    gr.bitmap.load options, path$ + "options.png"
    if options = - 1 then call ImageLoadError("options.png")
    gr.bitmap.draw optionsButton, options, di_width - 150, 50
    
    ! Load and draw the baubles that hide the gifts
    gr.bitmap.load b, path$ + "box.png"
    if b =- 1 then call ImageLoadError("box.png")
    gr.bitmap.scale b, b, 104, 104
    for x = 1 to 10
        gr.rotate.start 0, boxX[x], boxY[x], rotBox[x]
             gr.bitmap.draw box[x], b, boxX[x], boxY[x]
        gr.rotate.end
    next
    
    ! Group all baubles (boxes)
    gr.group allBoxes, box[1], box[2], box[3], box[4], box[5], box[6], box[7], box[8], box[9], box[10]
    
    
    
    ! Load and draw the gift boxes under the tree
    xx = 150
    for x = 1 to 3
        gr.bitmap.load giftBox, path$ + "underTree" + int$(x) + ".png"
        if giftBox =- 1 then ImageLoadError("underTree.png")
        gr.bitmap.draw giftBox, giftBox, xx, h - 100
        xx += 400
    next
    
    ! Draw moves made
    gr.color 255, 255, 255, 255, 1
    call CurvyWindow(50,80,120,135)
    gr.color 255, 0, 100, 255
    call CurvyWindow(52,82,118,133) :  gr.color 255, 255, 255, 255
    gr.text.size 30 : gr.text.draw txtMoves, 70, 118, "0"
    
    gr.render
    
    gosub DrawOptionsMenu
    gosub DrawAbout
    gosub DrawGameHelp
    gosub DrawMessageBox
return

!------------------------------------------------
! Draw message box for when a game is over
!------------------------------------------------
DrawMessageBox:
    dim mess[7]
   
    gr.color 255,0,0,0,1 : gr.set.stroke 1
    gr.rect mess[1], 200, di_height - 80, di_width - 200, di_height
    gr.color 255,255,255,255,0
    gr.rect mess[2], 200, di_height - 80, di_width - 200, di_height
    gr.color 255,255,255,255,1 : gr.text.align 2
    gr.text.draw mess[3], di_width / 2, di_height - 50, ""
    gr.color 255,255,255,255,1
    gr.rect mess[4], 300, di_height - 40, (di_width / 2) - 10, di_height - 5
    gr.rect mess[5], (di_width / 2) + 10, di_height - 40, (di_width) - 300, di_height - 5
    gr.color 255,0,0,0
    gr.text.draw mess[6], (di_width / 2) - 150, di_height - 12, "Play"
    gr.text.draw mess[7], (di_width / 2) + 150, di_height - 12, "Exit"
    
    gr.group messageBox, mess[1], mess[2], mess[3], mess[4], mess[5], mess[6], mess[7]
    gr.hide messageBox
   
return

!------------------------------------------------
! Load the sounds
!------------------------------------------------
LoadSounds:
	FALSE = 0 : TRUE = 1
	music = TRUE : sound = TRUE : buzz = TRUE
	path$ = "XmasMatch/data/"
  gosub LoadData
  
	soundpool.open 3
	
    soundpool.load buttonClick,path$ + "buttonClick.wav"
    if ! buttonClick then call SoundLoadError("buttonClick")

    soundpool.load notMatch, path$ + "notMatch.wav"
    if ! notMatch then call SoundLoadError("notMatch")
    
    soundpool.load A_match, path$ + "match.wav"
    if ! A_match then call SoundLoadError("match")
	
	! Audio loads Xmas Songs
    audio.load xmasSongs, Path$ + "xmasSongs.mp3" 
    if ! xmasSongs then call SoundLoadError("xmasSongs")
    
    array.load buzzGame[], 1, 100
    
    gosub StartMusic
return

!------------------------------------------------
! Do at start of each game
!------------------------------------------------
SetUp:
    moves = 0 : matches = 0 : gameOver = FALSE
    gr.modify txtMoves, "text", int$(moves)
    
    ! Hide the toys under the baubles 
    array.shuffle toys[]
    for x = 1 to 10 
        gr.modify toys[x], "x", boxX[x], "y", boxY[x]
    next
    gr.show allBoxes
return

!------------------------------------------------
! Play Xmas Match Seconed go
!------------------------------------------------
PlayXmasMatch:
    baubleCount = 0 : baubleNum1 = 0 : baubleNum2 = 0
    do 
        do
            gr.touch t, tx, ty
            if t then 
                tx /= scale_x : ty /= scale_y
                gr.modify collision, "x", tx, "y", ty
                if gr_collision(collision, optionsButton) then gosub ShowOptions
                for x = 1 to 10
                    if gr_collision(collision, box[x]) then
                        call PlaySound(sound, buttonClick)
                        if buzz then vibrate buzzGame[], - 1
                        for r = 0 to 360 step 90
                            gr.modify rotBox[x], "angle", r, "x", boxX[x] + 52, "y", boxY[x] + 52
                            gosub FallingSnow
                        next
                        gr.hide box[x]
                        boxCount ++ 
                        if boxCount = 1 then toyNum1 = toys[x] : boxNum1 = x : f_n.break
                        if boxCount = 2 then toyNum2 = toys[x] : boxNum2 = x : f_n.break
                        if t then do : gosub FallingSnow : gr.touch t, tx, ty : until ! t
                    endif
                next
            endif
            gosub FallingSnow
        until boxCount = 2
            
            moves ++ : gr.modify txtMoves, "text", int$(moves)
            gr.render
            match = TRUE        
        
            if CompareToys(toyNum1, toyNum2) then
                matches ++
                call PlaySound(sound, A_match)
            else
                call PlaySound(sound, notMatch)
                delay = time() + 1000
                do 
                    gosub FallingSnow
                until time() > delay
                gr.show box[boxNum1] : gr.show box[boxNum2]
                gr.render
            endif
            boxCount = 0 : boxNum1 = 0 : boxNum2 = 0
       
    until matches = 5
    
return

!------------------------------------------------
! Show how many move to complete the game
! and another game or not
!------------------------------------------------
NewGame:
    messFlag = FALSE
    m$ = "You found all the toys in " + int$(moves) + " moves."
    gr.show messageBox
    gr.modify mess[3], "text", m$ : gr.render
    do : gosub FallingSnow : gr.touch t, tx, ty : until ! t
    do
        gr.touch t, tx, ty
        if t then
            tx /= scale_x : ty /= scale_y 
            gr.modify collision, "x", tx, "y", ty
            if gr_collision(collision, optionsButton) then gosub ShowOptions
            if gr_collision(collision, mess[5]) then 
                gameOver = TRUE : messFlag = TRUE
            endif
            
            if gr_collision(collision, mess[4]) then 
                gr.hide messageBox : messFlag = TRUE
            endif
        endif
        gosub FallingSnow
    until messFlag
    call PlaySound(sound, buttonClick)
    if buzz then vibrate buzzGame[], - 1
return

!------------------------------------------------
! Falling snow
!------------------------------------------------
FallingSnow:
    for flakes = 1 to 100
        snowY[flakes] += 3 : snowX[flakes] += 0.5
        if snowY[flakes] > di_height then
            snowY[flakes] = floor(- di_height * rnd())
        elseif snowX[flakes] > di_width then
            snowX[flakes] = 0
        else
            gr.modify snow[flakes], "x", snowX[flakes], "y", snowY[flakes] 
        endif
    next
    gr.render
return

!-------------------------------------------------
! Start background music at start of game, if music = TRUE
!-------------------------------------------------
StartMusic:
if music then
    audio.play xmasSongs 
    audio.loop 
  endif 
return

!-------------------------------------------------
! Display the help screen
!-------------------------------------------------
ShowHelp:
    call PlaySound(sound, buttonClick)
    if buzz then vibrate buzzGame[], - 1
	gr.show displayHelp : gr.render
	do 
		gosub FallingSnow
		gr. touch t, tx, ty 
		if t then 
			tx / = scale_x : ty / = scale_y 
			gr.modify collision, "x", tx, "y", ty 
		endif
	until gr_collision(collision, help[10])
  do : gr. touch t, tx, ty : gosub FallingSnow : until ! t
  
  call PlaySound(sound, buttonClick)
  if buzz then vibrate buzzGame[], - 1
  
	gr.hide displayHelp : gr.modify collision, "x", - 1, "y", - 1
  gr.render
	
return

!------------------------------------------------
! Display the About info
!------------------------------------------------
ShowAbout:
    call PlaySound(sound, buttonClick)
    if buzz then vibrate buzzGame[], - 1
    gr.show displayAbout : gr.render
    do 
        gosub FallingSnow
        gr. touch t, tx, ty 
        if t then 
            tx / = scale_x : ty / = scale_y 
            gr.modify collision, "x", tx, "y", ty 
        endif
    
    until gr_collision(collision, about[9]) : Rem Close button
   
    do : gosub FallingSnow : gr.touch t, tx, ty : until ! t
    
    call PlaySound(sound, buttonClick)
    if buzz then vibrate buzzGame[], - 1
    
    gr.hide displayAbout : gr.modify collision, "x", - 1, "y", - 1 
    gr.render
return

!------------------------------------------------
! How to play the game
!------------------------------------------------
 DrawGameHelp:
	dim help[12] : helpX = 150 : helpY = 150 : inc = 50	
	gr.text.size 30 : gr.text.align 2 : gr.set.stroke 2
	gr.color 255, 0, 0, 255, 1
	gr.rect help[1], 100,100, di_width - 100, di_height - 100 
	
	gr.color 255, 255, 255, 255, 0
	gr.rect help[2], 100,100, di_width - 100, di_height - 100 
	gr.color , , , , 1
	
	gr.text.draw help[3], di_width / 2, helpY, "Xmas Match" : helpY += inc : gr.text.align 1
	
	gr.text.draw help[4], helpX, helpY, "Xmas Match is a little memory test game. There are ten toys" : helpY += inc
	
	gr.text.draw help[5], helpX, helpY, "hiden behind the baubles on the Christmas tree – five pairs." : helpY += inc
	
	gr.text.draw help[6], helpX, helpY, "You tap two baubles to show the toys and if they are" : helpY += inc
	gr.text.draw help[7], helpX, helpY, "matching toys they stay on display, if they are deferent" : helpY += inc
	gr.text.draw help[8], helpX, helpY, "toys then the baubles hides them again." : helpY += inc
	
	gr.text.draw help[9], helpX, helpY, "You have to find all the toys in as few moves as you can." : helpY += inc
	
  ! Close Button
    helpY += inc / 2
    gr.color 255, 0, 0, 128, 1 : gr.text.align 2
    gr.rect help[10], (w / 2) - 130, 480, (w / 2) + 130, 520 : Rem Close
    gr.color 255,255,255,255, 0
    gr.rect help[11], (w / 2) - 130, 480, (w / 2) + 130, 520
    gr.color 255, 255, 255, 255, 1
    gr.text.draw help[12], (w / 2), 510, "Close"
	
	gr.group displayHelp, help[1], help[2], help[3], help[4], help[5], help[6], help[7], help[8], help[9], help[10], help[11], help[12] 
	gr.hide displayHelp
	
return

!------------------------------------------------
! Draw the About info
!------------------------------------------------
DrawAbout:
    dim about[11] : w = (di_width)
    gr.text.size 30 : gr.set.stroke 2 : gr.text.align 2
    gr.color 255, 0, 0, 255, 1
    gr.rect about[1], (w / 2) - 200, 200, (w / 2) + 200, di_height - 130

    gr.color 255, 255, 255, 255, 0
    gr.rect about[2], (w / 2) - 200, 200, (w / 2) + 200, di_height - 130
    gr.color , , , , 1
    
    gr.text.draw about[3], (w / 2), 250, "About" 
    gr.text.draw about[4], (w / 2), 290, "Xmas Match for Android" 
    gr.text.draw about[5], (w / 2), 330, "Built with RFO BASIC!" 
    gr.text.draw about[6], (w / 2), 370, "December 2016"
    gr.text.draw about[7], (w / 2), 410, "Version: 1.00"
    gr.text.draw about[8], (w / 2), 450, "Author: Roy Shepherd"
    
    gr.color 255, 0, 0, 128, 1 : gr.text.align 2
    gr.rect about[9], (w / 2) - 130, 480, (w / 2) + 130, 520 : Rem Close
    gr.color 255,255,255,255, 0
    gr.rect about[10], (w / 2) - 130, 480, (w / 2) + 130, 520
    gr.color 255, 255, 255, 255, 1
    gr.text.draw about[11], (w / 2), 510, "Close"
    
    gr.group displayAbout, about[1], about[2], about[3], about[4], about[5], ~
                           about[6], about[7], about[8], about[9], about[10], about[11]
	  gr.hide displayAbout
return

!------------------------------------------------
! Draw the options menu
!------------------------------------------------
DrawOptionsMenu:
    dim opt[20] : w = di_width : h = di_height
    ! Load the tick
    gr.bitmap.load tick, path$ + "tick.png"
    if tick =- 1 then call ImageLoadError("tick.png")
    gr.bitmap.scale tick, tick, 25, 25
    
    gr.text.size 30 : gr.set.stroke 2
    gr.color 255, 0, 0, 255, 1
    gr.rect opt[1], (w / 2) - 200, 200, (w / 2) + 200, di_height - 130

    gr.color 255, 255, 255, 255, 0
    gr.rect opt[2], (w / 2) - 200, 200, (w / 2) + 200, di_height - 130
    gr.color , , , , 1
    
    gr.text.draw opt[3], (w / 2) - 150, 250, "Music" : Rem Music
    gr.text.draw opt[4], (w / 2) - 150, 290, "Sound" : Rem Sound
    gr.text.draw opt[5], (w / 2) - 150, 330, "Buzz" : Rem Buzz
    gr.color , , , , 0
    gr.rect opt[6], (w / 2) + 130, 230, (w / 2) + 150, 250
    gr.rect opt[7], (w / 2) + 130, 270, (w / 2) + 150, 290
    gr.rect opt[8], (w / 2) + 130, 310, (w / 2) + 150, 330
    
    ! Draw the ticks
    gr.bitmap.draw opt[9], tick, (w / 2) + 130, 225  : Rem tick for music
    gr.bitmap.draw opt[10], tick, (w / 2) + 130, 265 : Rem tick for sound
    gr.bitmap.draw opt[11], tick, (w / 2) + 130, 305 : Rem tick for buzz
    
    ! About button
    gr.text.align 2 : gr.color 255, 0, 0, 128, 1
    gr.rect opt[12], (w / 2) - 130, 360, (w / 2) + 130, 400 : Rem About
    gr.color 255,255,255,255, 0
    gr.rect opt[13], (w / 2) - 130, 360, (w / 2) + 130, 400
    gr.color 255, 255, 255, 255, 1
    gr.text.draw opt[14], (w / 2), 390, "About"
    
    ! Help Button
    gr.color 255, 0, 0, 128, 1
    gr.rect opt[15], (w / 2) - 130, 420, (w / 2) + 130, 460 : Rem Help
    gr.color 255,255,255,255, 0
    gr.rect opt[16], (w / 2) - 130, 420, (w / 2) + 130, 460
    gr.color 255, 255, 255, 255, 1
    gr.text.draw opt[17], (w / 2), 450, "Help"
    
    ! Close Button
    gr.color 255, 0, 0, 128, 1
    gr.rect opt[18], (w / 2) - 130, 480, (w / 2) + 130, 520 : Rem Close
    gr.color 255,255,255,255, 0
    gr.rect opt[19], (w / 2) - 130, 480, (w / 2) + 130, 520
    gr.color 255, 255, 255, 255, 1
    gr.text.draw opt[20], (w / 2), 510, "Close"
    
    gr.group options, opt[1], opt[2], opt[3], opt[4], opt[5], opt[6], opt[7], opt[8], opt[9], opt[10], ~
                      opt[11], opt[12], opt[13], opt[14], opt[15], opt[16], opt[17], opt[18], opt[19], opt[20] 
   
    gr.hide options
return

!------------------------------------------------
! Display the options menu
!------------------------------------------------
ShowOptions:
    call PlaySound(sound, buttonClick)
    if buzz then vibrate buzzGame[], - 1
    
    gr.show options 
    if music then gr.show opt[9]  else gr.hide opt[9]
    if sound then gr.show opt[10] else gr.hide opt[10]
    if buzz  then gr.show opt[11] else gr.hide opt[11]
    gr.render
    do
        gosub FallingSnow : flag = FALSE
        gr.touch t, tx, ty 
        if t then 
            tx /= scale_x : ty /= scale_y 
            gr.modify collision, "x", tx, "y", ty
            if gr_collision(collision, opt[3]) then gosub ModMusic : flag = TRUE
            if gr_collision(collision, opt[4]) then gosub ModSound : flag = TRUE
            if gr_collision(collision, opt[5]) then gosub ModBuzz  : flag = TRUE
            
            if gr_collision(collision, opt[12]) then gosub ShowAbout : flag = TRUE
            if gr_collision(collision, opt[15]) then gosub ShowHelp  : flag = TRUE
            
            if flag then do : gosub FallingSnow : gr.touch t, tx, ty : until ! t
        endif
    until gr_collision(collision, opt[18]) : Rem Close button
        
    do : gosub FallingSnow : gr.touch t, tx, ty : until ! t
    gr.modify collision, "x", - 1, "y", - 1
    call PlaySound(sound, buttonClick)
    if buzz then vibrate buzzGame[], - 1
    
    gr.hide options : gr.render
return

!------------------------------------------------
! Turn music on and off
!------------------------------------------------
ModMusic:
  call PlaySound(sound, buttonClick)
  if buzz then vibrate buzzGame[], - 1
  if music then
    music = FALSE
    gr.hide opt[9]
    audio.stop 
  else
    music = TRUE
    gr.show opt[9]
    audio.play xmasSongs
    audio.loop 
  endif
  gr.render
return

!------------------------------------------------
! Turn sound on and off
!------------------------------------------------
ModSound:
  call PlaySound(sound, buttonClick)
  if buzz then vibrate buzzGame[], - 1
  if sound then
    sound = FALSE
   gr.hide opt[10]
  else
    sound = TRUE
    gr.show opt[10]
  endif
  gr.render 
return

!------------------------------------------------
! Turn buzz on and off
!------------------------------------------------
ModBuzz:
  call PlaySound(sound, buttonClick)
  if buzz then vibrate buzzGame[], - 1
  if buzz then
    buzz = FALSE
    gr.hide opt[11]
  else
    buzz = TRUE
    gr.show opt[11]
  endif
  gr.render
return

!------------------------------------------------
! Save Music, sound, and Buzz
!------------------------------------------------
SaveData:
    file.exists pathPresent,path$ + "XmasMatch.txt"
    if ! pathPresent then file.mkdir path$

    text.open w,hs,Path$ + "XmasMatch.txt"
    text.writeln hs,int$(music)
    text.writeln hs,int$(sound)
    text.writeln hs,int$(buzz)
    text.close hs
return

!------------------------------------------------
! Load Music, sound, and Buzz
!------------------------------------------------
LoadData:
    file.exists pathPresent,path$  +"XmasMatch.txt"
    if pathPresent then
        text.open r,hs,path$ + "XmasMatch.txt"
        text.readln hs,music$
        text.readln hs,sound$
        text.readln hs,buzz$
        text.close hs
        music = val(music$)
        sound = val(sound$)
        buzz = val(buzz$)
    endif
return

!------------------------------------------------
! All functions in this gosub
!------------------------------------------------
Functions:

!------------------------------------------------
! Draw a rectangle with curved corners 
!------------------------------------------------
fn.def CurvyWindow(x,y,l,h)
	gr.rect r,x,y+25,l,h-25 
	gr.rect r,x+25,y,l-25,h 
	gr.circle c, x+25,y+25,25 
	gr.circle c,l-25,y+25,25 
	gr.circle c,x+25,h-25,25
	gr.circle c,l-25,h-25,25
	gr.render
fn.end

!-------------------------------------------------
! Compare the two revealed toys 
! and if they are the same return 1 otherwise return 0
!-------------------------------------------------
fn.def CompareToys(toy1, toy2)
    flag = 0
        if toy1 > toy2 then swap toy1, toy2
        
        if toy1 = 106 & toy2 = 107 then flag = 1
        if toy1 = 108 & toy2 = 109 then flag = 1
        if toy1 = 110 & toy2 = 111 then flag = 1
        if toy1 = 112 & toy2 = 113 then flag = 1
        if toy1 = 114 & toy2 = 115 then flag = 1
       
    fn.rtn flag
 fn.end
 
!------------------------------------------------
! if Play sound (ptr)
!------------------------------------------------
    fn.def PlaySound(sound, ptr)
      if sound then soundpool.play s,ptr,0.99,0.99,1,0,1
    fn.end
  
!------------------------------------------------
! load Sound files not found
!------------------------------------------------
    fn.def SoundLoadError(e$)
        dialog.message "Sound file " + e$ + " not found", "Game will end",OK,"OK" 
        end
    fn.end
    
!------------------------------------------------
! Load Image file not found
!------------------------------------------------
    fn.def ImageLoadError(e$)
        dialog.message "Image file " + e$ + " not found", "Game will end",OK,"OK" 
        end
    fn.end

return

