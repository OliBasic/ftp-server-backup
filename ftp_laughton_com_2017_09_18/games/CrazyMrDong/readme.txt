(C) Antonis [tony_gr] March 2013

Apk of this game can be found on Google Play store, Slide Me, and AppsLib.


Crazy Mr Dong is a funny and relaxing retro game based on the idea of the classic "Crazy Kong".

[The story]
Mrs Dong is in trouble. A big rat is preventing her from returning home. Mr Dong has to help her!
But to reach her must first avoid obstacles and collect all hearts.

[The game]
Collect all hearts and go to highest row to finish level.
Your score is the number of collected hearts. You get an extra live when completing level 3 and 5.
Includes 5 different levels:
"Moving fires", "Banana peels", "Rotating balls", "Flying rocks" and "Angry Scorpions".

How far can you go?

Requires Android 2.1 or higher. App movable to SD.