! Crazy Mr Dong v1.0
! (C) Antonis [tony_gr] March 2013

! poly line for words
LIST.CREATE N,words
LIST.ADD words, 80,40,50,85,120,30

! go graphics
GR.OPEN 255,205,155,55,0,1
PAUSE 1000
SOUNDPOOL.OPEN 3
SOUNDPOOL.LOAD heartDong, "heartdong.ogg"
SOUNDPOOL.LOAD DongKilled, "dongkillwav.ogg"

GR.ORIENTATION 1
GR.SCREEN w,h
sx=w/800
sy=h/1232
GR.SCALE sx,sy

! all levels easy calculations
x1=0
x2=150*sx
x3=300*sx
x4=500*sx
x5=650*sx
x6=800*sx
y1=1000*sy
y2=1070*sy
hole1=50
hole2=700
hole3=200
hole4=550
hole5=50
hole6=375
hole7=700

firstGame=1

! ***restart here***
start:
level=0
TotHearts=0
stars=0
sbmp=1
lives=3

FILE.EXISTS ex,"../../MrDong/mrdong.data"
IF ex=1 THEN
 GOSUB reedDataFile
ELSE
 TotHearts$="0"
 TotStars$="0"
 BestScore$="0"
 LastGame$="0"
 fake$="0"
 GOSUB CreateDataFile
ENDIF

! all level bitmaps
GR.BITMAP.LOAD m1,"kchar1.png"
GR.BITMAP.LOAD m2,"kchar2.png"
GR.BITMAP.LOAD m3,"kchar3.png"
GR.BITMAP.LOAD m4,"kchar4.png"
GR.BITMAP.LOAD m5,"kchar5.png"
GR.BITMAP.LOAD m6,"kchar6.png"
GR.BITMAP.LOAD m7,"kchar7.png"
GR.BITMAP.LOAD m8,"kchar8.png"
GR.BITMAP.LOAD down1,"kchara.png"
GR.BITMAP.LOAD down2,"kcharb.png"
GR.BITMAP.LOAD hea,"heart.png"
GR.BITMAP.LOAD wom,"kwchar.png"
GR.BITMAP.LOAD hou,"house.png"
GR.BITMAP.LOAD mou,"mouse.png"

GR.BITMAP.LOAD kin,"kintro.png"
GR.BITMAP.DRAW kintro,kin,0,0
GR.RENDER
GR.TEXT.SIZE 45
GR.COLOR 255,0,0,155,1
GR.TEXT.BOLD 1
FOR i=1 TO 2
 GR.TEXT.DRAW w, 5+i, 1000, "Total hearts: "+TotHearts$
 GR.TEXT.DRAW w, 5+i, 1050, "Total Stars: "+TotStars$
 GR.TEXT.DRAW w, 415+i, 1000, "Best Score: "+BestScore$
 GR.TEXT.DRAW w, 415+i, 1050, "Last Game: "+LastGame$
NEXT
GR.TEXT.DRAW w, 5+i+1, 1000, "Total hearts: "+TotHearts$
GR.TEXT.DRAW w, 5+i+1, 1050, "Total Stars: "+TotStars$
GR.TEXT.DRAW w, 415+i+1, 1000, "Best Score: "+BestScore$
GR.TEXT.DRAW w, 415+i+1, 1050, "Last Game: "+LastGame$

GR.TEXT.DRAW w, 5+i+1, 999, "Total hearts: "+TotHearts$
GR.TEXT.DRAW w, 5+i+1, 1049, "Total Stars: "+TotStars$
GR.TEXT.DRAW w, 415+i+1, 999, "Best Score: "+BestScore$
GR.TEXT.DRAW w, 415+i+1, 1049, "Last Game: "+LastGame$

GR.COLOR 255,0,185,0,1
GR.TEXT.DRAW w, 5+i, 1000, "Total hearts: "+TotHearts$
GR.TEXT.DRAW w, 5+i, 1050, "Total Stars: "+TotStars$
GR.TEXT.DRAW w, 415+i, 1000, "Best Score: "+BestScore$
GR.TEXT.DRAW w, 415+i, 1050, "Last Game: "+LastGame$
GR.COLOR 255,255,0,255,0
GR.SET.STROKE 8
GR.RECT r1, 550,2,799,112
GR.HIDE r1
GR.RENDER

helpOn=0
DO
 object=0
 GOSUB wait
 IF x>550*sx & y<112*sy THEN
  IF helpOn=0 THEN
   GR.SHOW r1
   GR.BITMAP.LOAD hlp, "khelp.jpg"
   GR.BITMAP.DRAW HlpScr,hlp,75,115
   GR.RENDER
   helpOn=!helpOn
  ELSE
   GR.HIDE HlpScr
   GR.HIDE r1
   GR.BITMAP.DELETE hlp
   helpOn=!helpOn
   GR.RENDER
  ENDIF
 ENDIF
UNTIL !(x>550*sx & y<112*sy) & helpOn=0

GR.BITMAP.DELETE kin
GR.CLS

! new level starts here
nexxtLevel:
GR.TEXT.TYPEFACE 3
GR.TEXT.SIZE 30
level=level+1

IF level=6 THEN  % last level=6, delete bmp of last level
 level=1
 stars=stars+1
 GR.BITMAP.DELETE ckboard4
 GR.BITMAP.DELETE ro
 TONE 500,500
 lives=lives+1
 POPUP "You get an extra life & and a Star!",0,0,4
ENDIF
IF level=1 THEN
 GOSUB clearScreen
 GR.BITMAP.LOAD ckboard1,"kboard1.png"
 GR.BITMAP.LOAD fire,"fire.png"
 xx1=0    % fire initial positions
 xx2=150
 xx3=300
 xx4=450
 drfire1=1 % fire initial directions
 drfire2=1
 drfire3=1
 drfire4=1
 xh1=100
 yh1=800
 xh2=300
 yh2=600
 xh3=500
 yh3=400
 xh4=700
 yh4=200

ELSEIF level=2 then
 GOSUB clearScreen
 GR.BITMAP.DELETE ckboard1
 GR.BITMAP.DELETE fire
 GR.BITMAP.LOAD ckboard2,"kboard2.png"
 GR.BITMAP.LOAD lif,"klift.png"
 GR.BITMAP.LOAD ban,"banana.png"
 lx1=375 % lift1 x,y
 ly1=750
 lx2=75 % lift2 x,y
 ly2=630
 lx3=675 % lift3 x,y
 ly3=630
 lx4=225 % lift4 x,y
 ly4=500
 lx5=525 % lift5 x,y
 ly5=500
 lx6=375 % lift6 x,y
 ly6=200
 drl1=-1 % lift1 down
 drl2=1 % lift2 up
 drl3=1 % lift3 up
 drl4=-1 % lift4 down
 drl5=-1 % lift5 up
 drl6=1 % lift6 up
 bx1=535 % banana 1
 by1=930
 bx2=215 % banana 2
 by2=930
 bx3=535 % banana 3
 by3=730
 bx4=215 % banana 4
 by4=730
 bx5=380 % banana 5
 by5=530
 bx6=75 % banana 6
 by6=330
 bx7=665 % banana 7
 by7=330
 xh1=20
 yh1=800
 xh2=380
 yh2=600
 xh3=380
 yh3=400
 xh4=750
 yh4=200

ELSEIF level=3 then % level 3
 GOSUB clearScreen
 GR.BITMAP.DELETE ckboard2
 GR.BITMAP.DELETE lif
 GR.BITMAP.DELETE ban
 GR.BITMAP.LOAD ckboard3,"kboard3.png"
 GR.BITMAP.LOAD bb,"kball.png"
 GR.BITMAP.LOAD bb2,"kball2.png"
 bx=50
 by=109
 bbx=700
 bby=109
 bxa=150
 bya=509
 bbxa=600
 bbya=509
 ballbmp=1
 xh1=100
 yh1=800
 xh2=300
 yh2=600
 xh3=500
 yh3=400
 xh4=700
 yh4=200

ELSEIF level=4 then
 lives=lives+1
 POPUP "You get an extra life!",0,0,4
 GOSUB clearScreen
 GR.BITMAP.DELETE ckboard3
 GR.BITMAP.DELETE lif
 GR.BITMAP.DELETE ban
 GR.BITMAP.LOAD ckboard4,"kboard4.png"
 GR.BITMAP.LOAD ro,"rock.png"
 xx1=55
 yy1=700
 xx2=205
 yy2=550
 xx3=555
 yy3=400
 xx4=705
 yy4=250
 drr1=1 % rock initial directions
 drr2=1
 drr3=1
 drr4=1
 xh1=50
 yh1=800
 xh2=0
 yh2=600
 xh3=750
 yh3=400
 xh4=700
 yh4=200

ELSE % level 5
 GOSUB clearScreen
 GR.BITMAP.DELETE ckboard4
 GR.BITMAP.DELETE ro
 GR.BITMAP.LOAD ckboard5,"kboard5.png"
 GR.BITMAP.LOAD sco1,"scorpion1.png"
 GR.BITMAP.LOAD sco2,"scorpion2.png"
 xh1=375
 yh1=800
 xh2=375
 yh2=600
 xh3=375
 yh3=400
 xh4=375
 yh4=200
 xs1=375 % scorpions
 ys1=910
 xs2=75
 ys2=710
 xs3=650
 ys3=710
 xs4=375
 ys4=510
 xs5=200
 ys5=310
 xs6=600
 ys6=310
ENDIF

! board & hearts all levels
IF level=1 THEN
 GR.BITMAP.DRAW kboard,ckboard1,0,0
ELSEIF level=2 then
 GR.BITMAP.DRAW kboard,ckboard2,0,0
ELSEIF level=3 then
 GR.BITMAP.DRAW kboard,ckboard3,0,0
ELSEIF level=4 then
 GR.BITMAP.DRAW kboard,ckboard4,0,0
ELSE % level 5
 GR.BITMAP.DRAW kboard,ckboard5,0,0
ENDIF

GR.BITMAP.DRAW h1,hea,xh1,yh1 % hearts
GR.BITMAP.DRAW h2,hea,xh2,yh2
GR.BITMAP.DRAW h3,hea,xh3,yh3
GR.BITMAP.DRAW h4,hea,xh4,yh4

GR.BITMAP.DRAW mouse,mou,80,120
!!
! rectangles for movement
GR.COLOR 255,255, 255,55,0
GR.RECT r1,1 ,1000, 150, 1070 % up
GR.RECT r1,150 ,1000, 300, 1070 % down
GR.RECT r1,300 ,1000, 500, 1070 % left
GR.RECT r1,500 ,1000, 650, 1070 % right
GR.RECT r1,650 ,1000, 799, 1070 % jump
!!

IF level=1 THEN
 GR.BITMAP.DRAW fire1,fire,0,910+7
 GR.BITMAP.DRAW fire2,fire,150,710+7
 GR.BITMAP.DRAW fire3,fire,300,510+7
 GR.BITMAP.DRAW fire4,fire,450,310+7

ELSEIF level=2 then
 GR.BITMAP.DRAW klift1,lif, lx1,ly1
 GR.BITMAP.DRAW klift2,lif, lx2,ly2
 GR.BITMAP.DRAW klift3,lif, lx3,ly3
 GR.BITMAP.DRAW klift4,lif, lx4,ly4
 GR.BITMAP.DRAW klift5,lif, lx5,ly5
 GR.BITMAP.DRAW klift6,lif, lx6,ly6
 GR.BITMAP.DRAW banana1,ban, bx1,by1
 GR.BITMAP.DRAW banana2,ban, bx2,by2
 GR.BITMAP.DRAW banana3,ban, bx3,by3
 GR.BITMAP.DRAW banana4,ban, bx4,by4
 GR.BITMAP.DRAW banana5,ban, bx5,by5
 GR.BITMAP.DRAW banana6,ban, bx6,by6
 GR.BITMAP.DRAW banana7,ban, bx7,by7

ELSEIF level=3
! moved

ELSEIF level=4
 GR.BITMAP.DRAW rock1,ro,xx1,yy1
 GR.BITMAP.DRAW rock2,ro,xx2,yy2
 GR.BITMAP.DRAW rock3,ro,xx3,yy3
 GR.BITMAP.DRAW rock4,ro,xx4,yy4
ELSE % level 5
 GR.BITMAP.DRAW scorpion1,sco1, xs1,ys1
 GR.BITMAP.DRAW scorpion2,sco2, xs2,ys2
 GR.BITMAP.DRAW scorpion3,sco1, xs3,ys3
 GR.BITMAP.DRAW scorpion4,sco2, xs4,ys4
 GR.BITMAP.DRAW scorpion5,sco1, xs5,ys5
 GR.BITMAP.DRAW scorpion6,sco2, xs6,ys6
ENDIF

! draw womans words,level,hearts
GR.COLOR 255,255, 155,55,1
GR.TEXT.BOLD 1
GR.ARC words1, 70,5, 180, 60, 0, 360, 1
GR.POLY words2, words
GR.COLOR 255,255, 255,255,1
GR.TEXT.DRAW words3, 88, 43, "Help!"
GR.COLOR 255,255, 0,0,1
GR.TEXT.SKEW -0.40
GR.TEXT.DRAW levText, 210, 35, "Level: "+REPLACE$(STR$(level),".0","")
GR.TEXT.DRAW heaText, 540, 35, ": "+ REPLACE$(STR$(tothearts),".0","")
GR.BITMAP.DRAW h0,hea,500,2 % hearts
GR.TEXT.DRAW livText, 350, 35, "Lives: "+REPLACE$(STR$(lives),".0","")
GR.TEXT.SKEW 0
! draw man, woman, man K.O.,house
GR.BITMAP.DRAW kman,m1,700,850
GR.BITMAP.DRAW kmana,down1,0,0
GR.BITMAP.DRAW kmanb,down2,0,0
GR.BITMAP.DRAW woman,wom,10,50
GR.ROTATE.START -90, 50,70
GR.BITMAP.DRAW woman2, wom, 50,50
GR.ROTATE.END
GR.HIDE woman2
GR.BITMAP.DRAW house,hou,625,5
if level=3 then 
GR.BITMAP.DRAW ball,bb,bx,by
 GR.BITMAP.DRAW ball2,bb,bbx,bby
 GR.BITMAP.DRAW ball3,bb,bxa,bya
 GR.BITMAP.DRAW ball4,bb,bbxa,bbya
ENDIF
GR.HIDE kmana
GR.HIDE kmanb

! inits for all levels
bmp=1   %  front sprite
currentBmp=1
x0=700  % initial position man x,y all levels?
y0=850  %
jflag=0 % jump flag
jcount=0 % jump count
row=1
onstairs=0
lbmp=0 % left bmp Dong
rbmp=0 % roght bmp Dong
finishedLevel=0
hearts=0   % hearts got
heartOn1=1 % hearts shown/hidden
heartOn2=1
heartOn3=1
heartOn4=1
! for heart collision
xhh1a=xh1-20
xhh2a=xh2-20
xhh3a=xh3-20
xhh4a=xh4-20
xhh1b=xh1+20
xhh2b=xh2+20
xhh3b=xh3+20
xhh4b=xh4+20

GR.BITMAP.LOAD kge,"kgetready.png"
GR.BITMAP.DRAW kget,kge,100,250
GR.RENDER
object=kget
GOSUB wait
GR.HIDE kget
GR.BITMAP.DELETE kge
GR.RENDER
scorpBmp=0
cxx1=CLOCK() % for fires, lifts, rock-rocks,balls
cxx2=CLOCK() % for kman left right
cxx3=CLOCK() % for showing front bitmap
sclk=CLOCK()
DO

 DO % ***** actions while waiting for a touch ******

  GR.TOUCH touched,x,y

  IF CLOCK()-cxx1>=15 THEN % clock 1 for fires
   GOSUB move_jump % all levels
   GOSUB checkHearts % all levels

   IF level=1 THEN
    GOSUB checkFires
    GOSUB movefires
   ENDIF

   IF level=2 THEN
    GOSUB checkLiftRide
    GOSUB moveLift
    IF onLift<>0 THEN GOSUB moveOnLift
    IF onLift>0 & (y0=850 | y0=650 | y0=450 | y0=250 | y0=50 ) THEN % go out in 1,2 row
     x0=x0-80
     onLift=0
    ENDIF
    GOSUB checkBananas % level 2
   ENDIF

   IF level=3 THEN
    GOSUB checkBall
    GOSUB guide
   ENDIF

   IF level=4 THEN
    GOSUB checkRocks
    GOSUB moveRocks
   ENDIF

   IF level=5 THEN
    GOSUB checkHoles
    GOSUB checkScorpions
   ENDIF

   IF finishedLevel=1 THEN GOTO nexxtlevel
   cxx1=CLOCK()
  ENDIF

  IF finishedLevel=1 THEN GOTO nexxtlevel

  IF CLOCK()-cxx3>=100 THEN % clock 3 for bmp only
   IF bmp>1 THEN
    bmp=1
    GR.MODIFY kman,"bitmap",m1
   ENDIF


  ENDIF
 UNTIL touched % ***finished actions while waiting for a touch***


 ! ****** actions while touched ************
 DO
  GR.TOUCH touched,x,y

  IF touched & CLOCK()-cxx2>=10 THEN
   GOSUB checkHearts % all levels
   IF finishedLevel=1 THEN GOTO nexxtlevel
   moveFlag=1

   ! right
   IF x>x5 & y>y1 & y<y2  & jflag<>3 & x0<=750 & onstairs=0 & !(y0=50 & x0>650) THEN
    x0=x0+10
    rbmp=rbmp+1
    IF rbmp<=4 THEN
     GR.MODIFY kman,"bitmap",m3
     bmp=3
    ELSEIF rbmp>4 & rbmp<=8
     GR.MODIFY kman,"bitmap",m6
     bmp=6
    ELSE
     GR.MODIFY kman,"bitmap",m3
     rbmp=0
     bmp=3
    ENDIF
   ENDIF

   ! left
   IF x>x4 & y>y1 & x<x5 & y<y2 & jflag<>3 & x0>=0 & onstairs=0 & !(y0=50 & x0<140) THEN
    x0=x0-10
    lbmp=lbmp+1
    IF lbmp<=4 THEN
     GR.MODIFY kman,"bitmap",m4
     bmp=4
    ELSEIF lbmp>4 & lbmp<=8
     GR.MODIFY kman,"bitmap",m5
     bmp=5
    ELSE
     GR.MODIFY kman,"bitmap",m4
     bmp=4
     lbmp=0
    ENDIF
   ENDIF

   ! jump
   IF x>x3 & y>y1 & x<x4 & y<y2 & jflag=0 & onstairs=0 & onlift=0 THEN
    jflag=1
    jcount=jcount+1
    IF jcount<8 THEN y0=y0-10
    IF jcount>=8 THEN y0=y0+5
    IF jcount=24 THEN
     jflag=0
     jcount=0
    ENDIF
   ENDIF

   IF level=1  THEN
    GOSUB checkStairs
    IF onstairs=1  THEN
     sbmp=sbmp+1
     IF sbmp=7 THEN sbmp=1
     IF sbmp<=3 THEN
      GR.MODIFY kman,"bitmap",m7
      bmp=6
     ELSE
      GR.MODIFY kman,"bitmap",m8
      bmp=7
     ENDIF
    ENDIF
    GOSUB movefires
    GOSUB checkFires
   ENDIF

   IF level=2 THEN
    GOSUB checkLiftRide
    GOSUB moveLift
    IF onLift<>0 THEN GOSUB moveOnLift
    GOSUB checkBananas
   ENDIF

   IF level=3 THEN
    GOSUB checkStairs
    IF onstairs=1  THEN
     sbmp=sbmp+1
     IF sbmp=7 THEN sbmp=1
     IF sbmp<=3 THEN
      GR.MODIFY kman,"bitmap",m7
      bmp=6
     ELSE
      GR.MODIFY kman,"bitmap",m8
      bmp=7
     ENDIF
    ENDIF
    GOSUB checkBall % inverted!
    GOSUB guide
   ENDIF

   IF level=4 THEN

    GOSUB checkStairs
    IF onstairs=1  THEN
     sbmp=sbmp+1
     IF sbmp=7 THEN sbmp=1
     IF sbmp<=3 THEN
      GR.MODIFY kman,"bitmap",m7
      bmp=6
     ELSE
      GR.MODIFY kman,"bitmap",m8
      bmp=7
     ENDIF
    ENDIF
    GOSUB checkRocks
    GOSUB moveRocks
   ENDIF

   IF level=5 THEN
    GOSUB checkStairs
    IF onstairs=1  THEN
     sbmp=sbmp+1
     IF sbmp=7 THEN sbmp=1
     IF sbmp<=3 THEN
      GR.MODIFY kman,"bitmap",m7
      bmp=6
     ELSE
      GR.MODIFY kman,"bitmap",m8
      bmp=7
     ENDIF
    ENDIF
    GOSUB checkHoles
    GOSUB checkScorpions
   ENDIF

   GR.MODIFY kman,"x",x0
   GR.MODIFY kman,"y",y0
   GOSUB move_Jump % all levels
   cxx2=CLOCK()
  ENDIF

 UNTIL !touched
 moveFlag=0

UNTIL 0


checkStairs:
IF row<>1 & row<>2 THEN GOTO as
! move in stairs up row 1->2
! allow go <up> in 3 stairs if appropriate x,y
IF level=1 | level=4 THEN condition= x0<=10 | x0>=740  | (x0>=340 & x0<=360)
IF level=3 THEN condition= (x0<=310 & x0>=290) | (x0>=440 & x0<=460)
IF level=5 THEN condition= x0<=10 | x0>=740
IF (x>x2 & y>y1 & x<x3 & y<y2) & condition  THEN
 IF y0<=850 & y0>650 THEN
  y0=y0-5 % not on row 2, go higher
  onstairs=1
 ENDIF
 IF y0<=650 THEN
  row=2 % on row 2, mark this
  onstairs=0
 ENDIF
 GOSUB adjustR12
ENDIF

as:
IF row<>2 & row<>1 THEN GOTO ad
! move in stairs down 2->1
! allow go <down> in 3 stairs if appropriate x,y
IF level=1 | level=4 THEN condition= x0<=10 | x0>=740 | (x0>=340 & x0<=360)
IF level=3 THEN condition= (x0<=310 & x0>=290) | (x0>=440 & x0<=460)
IF level=5 THEN condition= x0<=10 | x0>=740
IF (y>y1 & x<x2 & y<y2) & condition THEN
 IF y0>=650 & y0<850 THEN
  y0=y0+5
  onstairs=1
 ENDIF
 IF y0>=850 THEN
  row=1
  onstairs=0
 ENDIF
 GOSUB adjustR12
ENDIF

ad:
IF row<>2 & row<>3 THEN GOTO af
! print row,y0,level
! move in stairs up row 2->3
! allow go <up> in 3 stairs if appropriate x,y
IF level<>5 THEN condition=(x0>=190 & x0<=210)  | (x0>=540 & x0<=560)
IF level=5 THEN condition=(x0>=240 & x0<=260)  | (x0>=490 & x0<=510)
! print x0,condition
IF (x>x2 & y>y1 & x<x3 & y<y2) & condition   THEN
 IF y0<=650 & y0>450 & condition THEN
  y0=y0-5 % not on row 3, go higher
  onstairs=1
 ENDIF
 IF y0<=450 THEN
  row=3 % on row 3, mark this
  onstairs=0
 ENDIF
 GOSUB adjustR23
ENDIF

af:
IF row<>3 & row<>2 THEN GOTO ag
! move in stairs down 3->2
! allow go <down> in 3 stairs if appropriate x,y
IF level<>5 THEN condition=(x0>=190 & x0<=210)  | (x0>=540 & x0<=560)
IF level=5 THEN condition=(x0>=240 & x0<=260)  | (x0>=490 & x0<=510)
IF (y>y1 & x<x2 & y<y2) & condition   THEN
 IF y0>=450 & y0<650 THEN
  y0=y0+5
  onstairs=1
 ENDIF
 IF y0>=650 THEN
  row=2
  onstairs=0
 ENDIF
 GOSUB adjustR23
ENDIF

ag:
IF row<>3 & row<>4 THEN GOTO ah
! move in stairs up row 3->4
! allow go <up> in  stairs if appropriate x,y
IF level=1 | level=4 THEN condition= (x0>=40 & x0<=60)  | (x0>=340 & x0<=360) | (x0>=690 & x0<=710)
IF level=3 THEN condition= (x0>=40 & x0<=60) | (x0>=690 & x0<=710)
IF level=5 THEN condition= (x0>=90 & x0<=110) | (x0>=640 & x0<=660)
IF (x>x2 & y>y1 & x<x3 & y<y2) & condition  THEN
 IF y0<=450 & y0>250 THEN
  y0=y0-5 % not on row 3, go higher
  onstairs=1
 ENDIF
 IF y0<=250 THEN
  row=4 % on row 3, mark this
  onstairs=0
 ENDIF
 GOSUB adjustR34
ENDIF

ah:
IF row<>4 & row<>3 THEN GOTO aj
! move in stairs down 4->3
! allow go <down> in 3 stairs if appropriate x,y
IF level=1 | level=4 THEN condition= (x0>=40 & x0<=60)  | (x0>=340 & x0<=360) | (x0>=690 & x0<=710)
IF level=3 THEN condition= (x0>=40 & x0<=60) | (x0>=690 & x0<=710)
IF level=5 THEN condition= (x0>=90 & x0<=110) | (x0>=640 & x0<=660)
IF (y>y1 & x<x2 & y<y2) & condition  THEN
 IF y0>=250 & y0<450 THEN
  y0=y0+5
  onstairs=1
 ENDIF
 IF y0>=450 THEN
  row=3
  onstairs=0
 ENDIF
 GOSUB adjustR34
ENDIF

aj:
IF row<>4 & row<>5 THEN GOTO ak
! move in stairs up row 4->5
! allow go <up> in  stairs if appropriate x,y
IF (x>x2 & y>y1 & x<x3 & y<y2) & ((x0>=190 & x0<=210)  | (x0>=540 & x0<=560) )   THEN
 IF y0<=250 & y0>50 THEN
  y0=y0-5 % not on row 3, go higher
  onstairs=1
 ENDIF
 IF y0<=50 THEN
  row=5 % on row 5, mark this
  onstairs=0
 ENDIF
 GOSUB adjustR45
ENDIF

ak:
IF row<>5 & row<>4 THEN GOTO al
! move in stairs down 5->4
! allow go <down> in 3 stairs if appropriate x,y
IF (y>y1 & x<x2 & y<y2) & ((x0>=190 & x0<=210)  | (x0>=540 & x0<=560) )  THEN
 IF y0>=50 & y0<250 THEN
  y0=y0+5
  onstairs=1
 ENDIF
 IF y0>=250 THEN
  row= 4
  onstairs=0
 ENDIF
 GOSUB adjustR45
ENDIF

al:
RETURN

! fires movement level 1
movefires:
GR.MODIFY fire1,"x",xx1
IF xx1>=750 THEN drfire1=-1
IF xx1<=0 THEN drfire1=1
IF drfire1=-1 THEN xx1=xx1-5
IF drfire1=1 THEN xx1=xx1+5

GR.MODIFY fire2,"x",xx2
IF xx2>=750 THEN drfire2=-1
IF xx2<=0 THEN drfire2=1
IF drfire2=-1 THEN xx2=xx2-5
IF drfire2=1 THEN xx2=xx2+5

GR.MODIFY fire3,"x",xx3
IF xx3>=750 THEN drfire3=-1
IF xx3<=0 THEN drfire3=1
IF drfire3=-1 THEN xx3=xx3-5
IF drfire3=1 THEN xx3=xx3+5

GR.MODIFY fire4,"x",xx4
IF xx4>=750 THEN drfire4=-1
IF xx4<=0 THEN drfire4=1
IF drfire4=-1 THEN xx4=xx4-5
IF drfire4=1 THEN xx4=xx4+5
RETURN

! fires movement level 1
moveRocks:
GR.MODIFY rock1,"y",yy1
IF yy1>=800 THEN drr1=-1
IF yy1<=200 THEN drr1=1
IF drr1=-1 THEN yy1=yy1-5
IF drr1=1 THEN yy1=yy1+5

GR.MODIFY rock2,"y",yy2
IF yy2>=800 THEN drr2=-1
IF yy2<=200 THEN drr2=1
IF drr2=-1 THEN yy2=yy2-5
IF drr2=1 THEN yy2=yy2+5

GR.MODIFY rock3,"y",yy3
IF yy3>=800 THEN drr3=-1
IF yy3<=200 THEN drr3=1
IF drr3=-1 THEN yy3=yy3-5
IF drr3=1 THEN yy3=yy3+5

GR.MODIFY rock4,"y",yy4
IF yy4>=800 THEN drr4=-1
IF yy4<=200 THEN drr4=1
IF drr4=-1 THEN yy4=yy4-5
IF drr4=1 THEN yy4=yy4+5
RETURN

! dong movement all levels
move_jump:
IF jflag=1 THEN
 jcount=jcount+1
 IF jcount<=8 THEN y0=y0-10
 IF jcount>8 THEN y0=y0+5
 IF jcount=24 THEN
  jflag=0
  jcount=0
 ENDIF
ENDIF
GR.MODIFY kman,"x",x0
GR.MODIFY kman,"y",y0
IF jflag=0 & bmp<>1 & moveFlag=0 THEN
 GR.MODIFY kman,"bitmap",m1
 bmp=1
ENDIF
GR.RENDER
RETURN

! this adjustment is for stairs level1, make it generic!
adjustR12:
IF level=3 THEN GOTO lev3a
IF level=5 THEN GOTO lev5a
IF x0<=10 THEN x0=0 % position adjustment for stairs 1
IF x0>=340 & x0<=360  THEN x0= 350 % position adjustment stairs 2
IF x0>=740 THEN x0=750 % position adjustment for stairs 3
RETURN
lev3a:
IF x0>=290 & x0<=310 THEN x0= 300 % position adjustment for stairs 1
IF x0>=440 & x0<=460  THEN x0= 450 % position adjustment stairs 2
RETURN
lev5a:
IF x0<=10 THEN x0=0
IF x0>=740 THEN x0=750
RETURN

adjustR23:
IF level=5 THEN GOTO lev5b
IF x0>=190 & x0<=210 THEN x0=200 % position adjustment for stairs 1
IF x0>=540 & x0<=560 THEN x0=550 % position adjustment for stairs 2
RETURN
lev5b:
IF x0>=240 & x0<=260 THEN x0=250
IF x0>=490 & x0<=510 THEN x0=500
RETURN

adjustR34:
IF level=5 THEN GOTO lev5c
IF x0>=40 & x0<=60 THEN x0=50 % position adjustment for stairs 1
IF level=1 & x0>=340 & x0<=360 THEN x0=350 % position adjustment for stairs 2
IF x0>=690 & x0<=710 THEN x0=700 % position adjustment for stairs 3
RETURN
lev5c:
IF x0>=90 & x0<=110 THEN x0=100
IF x0>=640 & x0<=660 THEN x0=650
RETURN

adjustR45:
IF x0>=190 & x0<=210 THEN x0=200 % position adjustment for stairs 1
IF x0>=540 & x0<=560 THEN x0=550 % position adjustment for stairs 2
RETURN


! * checks fires, level 1, make it generic
checkfires:
a1= Gr_collision ( fire1, kman)
a2= Gr_collision ( fire2, kman)
a3= Gr_collision ( fire3, kman)
a4= Gr_collision ( fire4, kman)
aa=a1+a2+a3+a4
IF aa=0 THEN RETURN
! effects when burned
SOUNDPOOL.PLAY kDong, dongkilled , 0.9, 0.9, 1, 0, 1
IF a1=1 THEN
 aux=fire1
ELSEIF a2=1 then
 aux=fire2
ELSEIF a3=1 then
 aux=fire3
ELSE
 aux=fire4
ENDIF
GR.MODIFY aux,"x", x0+20
GR.MODIFY aux,"y", y0+60
GR.RENDER
PAUSE 200
GOSUB loseEffect
GOSUB updateLives % level 1
RETURN

! * checks fires, level 1, make it generic
checkRocks:
a1= Gr_collision ( rock1, kman)
a2= Gr_collision ( rock2, kman)
a3= Gr_collision ( rock3, kman)
a4= Gr_collision ( rock4, kman)
aa=a1+a2+a3+a4
IF aa=0 THEN RETURN
! effects when burned
SOUNDPOOL.PLAY kDong, dongkilled , 0.9, 0.9, 1, 0, 1
IF a1=1 THEN
 aux=rock1
ELSEIF a2=1 then
 aux=rock2
ELSEIF a3=1 then
 aux=rock3
ELSE
 aux=rock4
ENDIF
GR.MODIFY aux,"x",x0+30
GR.MODIFY aux,"y",y0+60
GOSUB loseEffect
GOSUB updateLives % level 1
RETURN

! effect for losing all levels
loseEffect:
SOUNDPOOL.PLAY kDong, dongkilled , 0.9, 0.9, 1, 0, 1
GR.HIDE kman
GR.MODIFY words3,"text","Oh,No!"
GR.MODIFY words3,"x",80
FOR i=1 TO 4
 GR.SHOW kmana
 GR.MODIFY kmana,"x",x0
 GR.MODIFY kmana,"y",y0
 GR.RENDER
 PAUSE 150
 GR.HIDE kmana
 GR.SHOW kmanb
 GR.MODIFY kmanb,"x",x0
 GR.MODIFY kmanb,"y",y0
 GR.RENDER
 PAUSE 150
 GR.HIDE kmanb
NEXT i
x0=700
y0=850
GR.MODIFY kman,"bitmap",m1
GR.MODIFY kman,"x",x0
GR.MODIFY kman,"x",y0
GR.RENDER
bmp=1   %  front sprite
currentBmp=1
jflag=0 % jump flag
jcount=0 % jump count
row=1
onstairs=0
lbmp=0 % left bmp Dong
rbmp=0 % roght bmp Dong
IF level=1 THEN GOSUB ResetPositions
IF level=2 THEN GOSUB resetDong
IF level=3 THEN GOSUB ResetPositionsLevel3
IF level=4 THEN GOSUB ResetPositionsLevel4
GR.SHOW kman
GR.MODIFY words3,"text","Help!"
GR.MODIFY words3,"x",88
GR.RENDER
IF lives<>1 THEN POPUP "Get Ready!",0,0,0
IF lives<>0 THEN PAUSE 500
RETURN

! check collision with heart positions all levels fix for lev 2?
checkHearts:
! PRINT level
IF x0>=xhh1a & x0<=xhh1b & y0<810 & y0>790 & heartOn1=1 THEN
 GR.HIDE h1
 heartOn1=0
 GOSUB updateHearts
ENDIF
IF x0>=xhh2a & x0<=xhh2b & y0<610 & y0>590 & heartOn2=1 THEN
 GR.HIDE h2
 heartOn2=0
 GOSUB updateHearts
ENDIF
IF x0>=xhh3a & x0<=xhh3b & y0<410 & y0>390 & heartOn3=1 THEN
 GR.HIDE h3
 heartOn3=0
 GOSUB updateHearts
ENDIF
IF x0>=xhh4a & x0<=xhh4b & y0<210 & y0>190 & heartOn4=1 THEN
 GR.HIDE h4
 heartOn4=0
 GOSUB updateHearts
ENDIF

IF hearts=4 & y0<=50 THEN
! IF (hearts=0 & level=1) | (hearts=0 & level=2) | (hearts=0 & level=3 ) |  (hearts=0 & level=4 ) | (hearts=4 & level=5 & y0<=50 ) THEN
 gr.modify kman,"bitmap",m1
 GR.MODIFY words3,"x",88
 GR.MODIFY words3,"text","Yes!"
 GR.RENDER
 PAUSE 500
 IF level=3 THEN
  GR.HIDE ball
  GR.HIDE ball2
  GR.HIDE ball3
  GR.HIDE ball4
 ENDIF
 GOSUB effect
 finishedLevel=1
ENDIF
RETURN

! wait subroutine
wait:
cccl=CLOCK()
flash=1
DO
 GR.TOUCH touched,x,y
 IF object<>0 & CLOCK()-cccl>=600 THEN
  IF flash=1 THEN GR.HIDE object ELSE GR.SHOW object
  GR.RENDER
  flash=!flash
  cccl=CLOCK()
 ENDIF
UNTIL touched
DO
 GR.TOUCH touched,fx1,fy1
UNTIL !touched
RETURN

! update hearts all levels
updateHearts:
hearts=hearts+1
TotHearts=TotHearts+1
GR.MODIFY heaText,"text",": "+REPLACE$(STR$(TotHearts),".0","")
SOUNDPOOL.PLAY hDong, heartDong , 0.9, 0.9, 1, 0, 1
RETURN

! for level 1
resetPositions:
GR.SHOW kman
GR.MODIFY kman,"x", 700
GR.MODIFY kman,"y", 850
row=1
onStairs=0
x0=700
y0=850
xx1=0
xx2=150
xx3=300
xx4=450
jflag=0
jcount=0
drfire1=1 % fire initial direction
drfire2=1
drfire3=1
drfire4=1
GR.MODIFY fire1 ,"x",0
GR.MODIFY fire1 ,"y",910+7
GR.MODIFY fire2 ,"x",150
GR.MODIFY fire2 ,"y",710+7
GR.MODIFY fire3 ,"x",300
GR.MODIFY fire3 ,"y",510+7
GR.MODIFY fire4 ,"x",450
GR.MODIFY fire4 ,"y",310+7
GR.RENDER
PAUSE 1000
RETURN

! for level 4
ResetPositionsLevel4:
GR.SHOW kman
GR.MODIFY kman,"x", 700
GR.MODIFY kman,"y", 850
row=1
onStairs=0
x0=700
y0=850
xx1=55
yy1=700
xx2=205
yy2=550
xx3=555
yy3=400
xx4=705
yy4=250
jflag=0
jcount=0
drr1=1 % fire initial direction
drr2=1
drr3=1
drr4=1
GR.MODIFY rock1 ,"x",xx1
GR.MODIFY rock1 ,"y",yy1
GR.MODIFY rock2 ,"x",xx2
GR.MODIFY rock2 ,"y",yy2
GR.MODIFY rock3 ,"x",xx3
GR.MODIFY rock3 ,"y",yy3
GR.MODIFY rock4 ,"x",xx4
GR.MODIFY rock4 ,"y",yy4
GR.RENDER
PAUSE 1000
RETURN

! finishing all levels
effect:
GR.HIDE words1
GR.HIDE words2
GR.HIDE words3

GR.BITMAP.SCALE mou1, mou, 50,30
GR.BITMAP.SCALE mou2, mou, 50,15
GR.MODIFY kman,"x", 80
GR.MODIFY kman,"y", 0
GR.RENDER

! mou 80,120
! 1
FOR i=0 TO 30 STEP 10 % down to 50
 GR.MODIFY kman,"y", i
 GR.RENDER
 PAUSE 15
NEXT
GR.MODIFY mouse,"bitmap",mou1
GR.MODIFY mouse,"y",140
GR.MODIFY kman,"y", 40
GR.RENDER
PAUSE 15

! 2
FOR i=20 TO 0 STEP -10 % up
 GR.MODIFY kman,"y", i
 GR.RENDER
 PAUSE 20
NEXT
FOR i=0 TO 40 STEP 10 % down
 GR.MODIFY kman,"y", i
 GR.RENDER
 PAUSE 20
NEXT
GR.MODIFY mouse,"bitmap",mou2
GR.MODIFY mouse,"y",145
GR.MODIFY kman,"y", 50
GR.RENDER
PAUSE 20

! 3
FOR i=30 TO 0 STEP -10 % up
 GR.MODIFY kman,"y", i
 GR.RENDER
 PAUSE 20
NEXT

FOR i=10 TO 50 STEP 10 % down
 GR.MODIFY kman,"y", i
 GR.RENDER
 PAUSE 20
NEXT

GR.HIDE mouse
GR.RENDER
PAUSE 30

GR.HIDE woman
GR.SHOW woman2
x0=50
y0=50
GR.MODIFY kman,"x",x0
GR.MODIFY kman,"y",y0
FOR i= 50 TO 710 STEP 10
 IF i=650 THEN GR.HIDE woman2
 x0=i
 rbmp=rbmp+1
 IF rbmp<=4 THEN
  GR.MODIFY kman,"bitmap",m3
  bmp=3
 ELSEIF rbmp>4 & rbmp<=8
  GR.MODIFY kman,"bitmap",m6
  bmp=6
 ELSE
  GR.MODIFY kman,"bitmap",m3
  rbmp=0
  bmp=3
 ENDIF
 GR.MODIFY kman,"x",x0
 GR.MODIFY woman2,"y",x0
 GR.RENDER
 PAUSE 30
NEXT i
GR.BITMAP.DELETE mou1
GR.BITMAP.DELETE mou2
IF level<>5 THEN RETURN
TONE 500,500
lives=lives+1
! POPUP "You get an extra life!",0,0,4
GOSUB updateLives
RETURN

clearScreen:
GR.CLS
GR.SET.STROKE 4
GR.TEXT.SIZE 30
GR.COLOR 255,0,0,255,1
RETURN

! for level 2
moveLift:
IF drl1=1 & ly1>745 THEN ly1=ly1-5
IF drl1=1 & ly1=745 THEN
 ly1=ly1+5
 drl1=-1
ENDIF
IF drl1=-1 & ly1<990 THEN ly1=ly1+5
IF drl1=-1 & ly1=990 THEN
 ly1=ly1-5
 drl1=1
ENDIF
IF drl2=1 & ly2>545 THEN ly2=ly2-5
IF drl2=1 & ly2=545 THEN
 ly2=ly2+5
 drl2=-1
ENDIF
IF drl2=-1 & ly2<790 THEN ly2=ly2+5
IF drl2=-1 & ly2=790 THEN
 ly2=ly2-5
 drl2=1
ENDIF
IF drl3=1 & ly3>545 THEN ly3=ly3-5
IF drl3=1 & ly3=545 THEN
 ly3=ly3+5
 drl3=-1
ENDIF
IF drl3=-1 & ly3<790 THEN ly3=ly3+5
IF drl3=-1 & ly3=790 THEN
 ly3=ly3-5
 drl3=1
ENDIF
IF drl4=1 & ly4>345 THEN ly4=ly4-5
IF drl4=1 & ly4=345 THEN
 ly4=ly4+5
 drl4=-1
ENDIF
IF drl4=-1 & ly4<590 THEN ly4=ly4+5
IF drl4=-1 & ly4=590 THEN
 ly4=ly4-5
 drl4=1
ENDIF
IF drl5=1 & ly5>345 THEN ly5=ly5-5
IF drl5=1 & ly5=345 THEN
 ly5=ly5+5
 drl5=-1
ENDIF
IF drl5=-1 & ly5<590 THEN ly5=ly5+5
IF drl5=-1 & ly5=590 THEN
 ly5=ly5-5
 drl5=1
ENDIF
IF drl6=1 & ly6>145 THEN ly6=ly6-5
IF drl6=1 & ly6=145 THEN
 ly6=ly6+5
 drl6=-1
ENDIF
IF drl6=-1 & ly6<390 THEN ly6=ly6+5
IF drl6=-1 & ly6=390 THEN
 ly6=ly6-5
 drl6=1
ENDIF

GR.MODIFY klift1,"y", ly1
GR.MODIFY klift2,"y", ly2
GR.MODIFY klift3,"y", ly3
GR.MODIFY klift4,"y", ly4
GR.MODIFY klift5,"y", ly5
GR.MODIFY klift6,"y", ly6
RETURN

! for level 2
checkliftRide:
IF (x0>=lx1-25 & x0<=lx1+60) & ((y0=850 & ly1=950) | (y0=650 & ly1=750)) THEN
 onLift=1 % first lift
ELSEIF (x0>=lx2-25 & x0<=lx2+60) & ((y0=650 & ly2=750) | (y0=450 & ly2=550)) THEN
 onLift=2 % second lift
ELSEIF (x0>=lx3-25 & x0<=lx3+60) & ((y0=650 & ly3=750) | (y0=450 & ly3=550)) THEN
 onLift=3 % third lift
ELSEIF (x0>=lx4-25 & x0<=lx4+60) & ((y0=450 & ly4=550) | (y0=250 & ly4=350)) THEN
 onLift=4
ELSEIF (x0>=lx5-25 & x0<=lx5+60) & ((y0=450 & ly5=550) | (y0=250 & ly5=350)) THEN
 onLift=5
ELSEIF (x0>=lx6-25 & x0<=lx6+60) & ((y0=250 & ly6=350) | (y0=50 & ly6=150)) THEN
 onLift=6
ELSE
 ! nothing
ENDIF
RETURN

! for level 2
moveOnLift:
IF onLift=1 THEN
 x0=lx1
 y0=ly1-100
ELSEIF onLift=2
 x0=lx2
 y0=ly2-100
ELSEIF onLift=3
 x0=lx3
 y0=ly3-100
ELSEIF onLift=4
 x0=lx4
 y0=ly4-100
ELSEIF onLift=5
 x0=lx5
 y0=ly5-100
ELSEIF onLift=6
 x0=lx6
 y0=ly6-100
ELSE
 ! nothing
ENDIF
RETURN

! for level 2
checkBananas:
bFlag=0
! row 1
IF x0>bx1-50 & x0<bx1+55 & y0<=850 & y0>=840 THEN bFlag=1
IF x0>bx2-50 & x0<bx2+55 & y0<=850 & y0>=840 THEN bFlag=1
! row 2
IF x0>bx3-50 & x0<bx3+55 & y0<=650 & y0>=640 THEN bFlag=1
IF x0>bx4-50 & x0<bx4+55 & y0<=650 & y0>=640 THEN bFlag=1
! row 3
IF x0>bx5-50 & x0<bx5+55 & y0<=450 & y0>=440 THEN bFlag=1
! row 4
IF x0>bx6-50 & x0<bx6+55 & y0<=250 & y0>=240 THEN bFlag=1
IF x0>bx7-50 & x0<bx7+55 & y0<=250 & y0>=240 THEN bFlag=1
IF bFlag=1 THEN
 GOSUB loseEffect
 GOSUB updateLives % level 2
 bFlag=0
!!
 y0=850
 x0=700
GR.MODIFY kman,"x",x0
GR.MODIFY kman,"x",y0
GR.RENDER
!!
ENDIF
RETURN

! for level 5
checkScorpions:
SFlag=0

! row 1
IF x0>xs1-50 & x0<xs1+65 & y0<=850 & y0>=830 THEN sFlag=1
! row2
IF x0>xs2-50 & x0<xs2+65 & y0<=650 & y0>=630 THEN sFlag=1
IF x0>xs3-50 & x0<xs3+65 & y0<=650 & y0>=630 THEN sFlag=1
! row 3
IF x0>xs4-50 & x0<xs4+65 & y0<=450 & y0>=430 THEN sFlag=1
! row 4
IF x0>xs5-50 & x0<xs5+65 & y0<=250 & y0>=230 THEN sFlag=1
IF x0>xs6-50 & x0<xs6+65 & y0<=320 & y0>=230 THEN sFlag=1
IF sFlag=1 THEN
 GOSUB loseEffect
 GOSUB updateLives % level 5
 sFlag=0
 GOSUB resetDong
ENDIF
IF CLOCK()-sclk<1000 THEN RETURN
GOSUB updateScorpions
sClk=CLOCK()
RETURN

! for level 2
resetDong:
x0=700
y0=850
GR.MODIFY kman,"x",x0
GR.MODIFY kman,"y",y0
jflag=0
jcount=0
RETURN

! for level 3
guide:
! stairs 200,50,200
IF by=109 & bx<200 THEN bx=bx+5 % row4
IF bya=109 & bxa<200 THEN bxa=bxa+5 % row4
IF by>=109 & by<309 & bx=200 THEN by=by+5 % row4
IF bya>=109 & bya<309 & bxa=200 THEN bya=bya+5 % row4
IF bby=109 & bbx>550 THEN bbx=bbx-5 % row4
IF bbya=109 & bbxa>550 THEN bbxa=bbxa-5 % row4
IF bby>=109 & bby<309 & bbx=550 THEN bby=bby+5 % row4
IF bbya>=109 & bbya<309 & bbxa=550 THEN bbya=bbya+5 % row4

IF by=309 & bx>50 THEN bx=bx-5 % row3
IF bya=309 & bxa>50 THEN bxa=bxa-5 % row3
IF by>=309 & by<509 & bx=50  THEN by=by+5 % row3
IF bya>=309 & bya<509 & bxa=50  THEN bya=bya+5 % row3
IF bby=309 & bbx<700 THEN bbx=bbx+5 % row3
IF bbya=309 & bbxa<700 THEN bbxa=bbxa+5 % row3
IF bby>=309 & bby<509 & bbx=700 THEN bby=bby+5 % row3
IF bbya>=309 & bbya<509 & bbxa=700 THEN bbya=bbya+5 % row3

IF by=509 & bx<200 THEN bx=bx+5 % row2
IF bya=509 & bxa<200 THEN bxa=bxa+5 % row2
IF by>=509 & by<709 & bx=200 THEN by=by+5 % row2
IF bya>=509 & bya<709 & bxa=200 THEN bya=bya+5 % row2
IF bby=509 & bbx>550 THEN bbx=bbx-5 % row2
IF bbya=509 & bbxa>550 THEN bbxa=bbxa-5 % row2
IF bby>=509 & bby<709 & bbx=550 THEN bby=bby+5 % row2
IF bbya>=509 & bbya<709 & bbxa=550 THEN bbya=bbya+5 % row2

IF by=709 & bx<300 THEN bx=bx+5 % row1
IF bya=709 & bxa<300 THEN bxa=bxa+5 % row1
IF by>=709 & by<909 & bx=300 THEN  by=by+5 % row1
IF bya>=709 & bya<909 & bxa=300 THEN  bya=bya+5 % row1
IF bby=709 & bbx>450 THEN bbx=bbx-5 % row1
IF bbya=709 & bbxa>450 THEN bbxa=bbxa-5 % row1
IF bby>=709 & bby<909 & bbx=450 THEN bby=bby+5 % row1
IF bbya>=709 & bbya<909 & bbxa=450 THEN bbya=bbya+5 % row1

IF by=909  THEN  bx=bx-5 % row1
IF by>709 & by<=909 & bx=-50 THEN % row1 restart
 bx=50
 by=109
ENDIF
IF bby=909  THEN  bbx=bbx+5 % row1
IF bby>709 & bby<=909 & bbx=800 THEN % row1 restart
 bbx=700
 bby=109
ENDIF
IF bya=909 THEN  bxa=bxa-5 % row1
IF bya>709 & bya<=909 & bxa=-50 THEN % row1 restart
 bxa=50
 bya=109
ENDIF
IF bbya=909  THEN  bbxa=bbxa+5 % row1
IF bbya>709 & bbya<=909 & bbxa=800 THEN % row1 restart
 bbxa=700
 bbya=109
ENDIF
ballbmp=ballbmp+1
IF ballbmp<=4 THEN
 GR.MODIFY ball,"bitmap",bb
 GR.MODIFY ball2,"bitmap",bb2
 GR.MODIFY ball3,"bitmap",bb
 GR.MODIFY ball4,"bitmap",bb2
ELSE
 GR.MODIFY ball,"bitmap",bb2
 GR.MODIFY ball2,"bitmap",bb
 GR.MODIFY ball3,"bitmap",bb2
 GR.MODIFY ball4,"bitmap",bb
ENDIF
IF ballbmp=8 THEN ballbmp=0

GR.MODIFY ball,"x",bx
GR.MODIFY ball,"y",by
GR.MODIFY ball2,"x",bbx
GR.MODIFY ball2,"y",bby
GR.MODIFY ball3,"x",bxa
GR.MODIFY ball3,"y",bya
GR.MODIFY ball4,"x",bbxa
GR.MODIFY ball4,"y",bbya
RETURN

! * checks balls, level 3, make it generic
checkBall:
a1= Gr_collision ( ball, kman)
a2= Gr_collision ( ball2, kman)
a3= Gr_collision ( ball3, kman)
a4= Gr_collision ( ball4, kman)
aa=a1+a2+a3+a4
IF aa=0 THEN RETURN
GR.RENDER
! effects when burned
GOSUB loseEffect
GOSUB updateLives % level 3
RETURN

ResetPositionsLevel3:
GR.SHOW kman
GR.MODIFY kman,"x", 700
GR.MODIFY kman,"y", 850
bx=50
by=109
bbx=700
bby=109
bxa=150
bya=509
bbxa=600
bbya=509
ballbmp=1
xh1=100
yh1=800
xh2=300
yh2=600
xh3=500
yh3=400
xh4=700
yh4=200
x0=700  % initial position man x,y all levels?
y0=850  %
jflag=0 % jump flag
jcount=0 % jump count
row=1
onstairs=0
lbmp=0 % left bmp Dong
rbmp=0 % roght bmp Dong
finishedLevel=0
GR.MODIFY ball,"x",bx
GR.MODIFY ball,"y",by
GR.MODIFY ball2,"x",bbx
GR.MODIFY ball2,"y",bby
GR.MODIFY ball3,"x",bxa
GR.MODIFY ball3,"y",bya
GR.MODIFY ball4,"x",bbxa
GR.MODIFY ball4,"y",bbya
GR.RENDER
PAUSE 500
RETURN

! all levels
updateLives:
lives=lives-1
GR.MODIFY livText,"text","Lives: "+ REPLACE$(STR$(lives),".0","")
GR.RENDER
IF lives=0 THEN
 GOSUB gameOver
 GR.CLS
 GR.RENDER
ELSE
 RETURN
ENDIF
IF lives=0 THEN GOTO start
RETURN

! reads current data file
reedDataFile:
! read data
TEXT.OPEN r,filn,"../../MrDong/mrdong.data"
TEXT.READLN filn, TotHearts$ % Total hearts collected
TEXT.READLN filn, TotStars$ % Total Stars Won
TEXT.READLN filn, BestScore$
TEXT.READLN filn, LastGame$ % Date
TEXT.READLN filn, fake$ % future expansion
TEXT.READLN filn, fake$ % future expansion
TEXT.READLN filn, fake$ % future expansion
TEXT.CLOSE filn
RETURN
! *******************
! creates mrdong.data
! *******************
CreateDataFile:
TEXT.OPEN w,filn,"../../MrDong/mrdong.data"
TEXT.WRITELN filn, TotHearts$
TEXT.WRITELN filn, TotStars$
TEXT.WRITELN filn, BestScore$
TEXT.WRITELN filn, LastGame$
TEXT.WRITELN filn, fake$
TEXT.WRITELN filn, fake$
TEXT.WRITELN filn, fake$
TEXT.CLOSE filn
RETURN

gameOver:
GOSUB reedDataFile
TotHearts$=REPLACE$(STR$(VAL(TotHearts$)+tothearts),".0","")
TotStars$=REPLACE$(STR$(VAL(TotStars$)+stars),".0","")
IF VAL(BestScore$)<tothearts THEN BestScore$=REPLACE$(STR$(tothearts),".0","")
TIME Year$, Month$, Day$, Hour$, Minute$, Second$
LastGame$=Month$+"/"+Day$
fake$="0"
GOSUB CreateDataFile

! screen for game over
GR.BITMAP.LOAD kgam,"kgameover.png"
GR.BITMAP.DRAW kgameov,kgam,175,300
GR.RENDER
DO
 GR.TOUCH touched,x,y
UNTIL touched
DO
 GR.TOUCH touched,fx1,fy1
UNTIL !touched
GR.BITMAP.DELETE kgam

! delete bitmaps
GR.BITMAP.DELETE m1
GR.BITMAP.DELETE m2
GR.BITMAP.DELETE m3
GR.BITMAP.DELETE m4
GR.BITMAP.DELETE m5
GR.BITMAP.DELETE m6
GR.BITMAP.DELETE m7
GR.BITMAP.DELETE m8
GR.BITMAP.DELETE down1
GR.BITMAP.DELETE down2
GR.BITMAP.DELETE hea
GR.BITMAP.DELETE wom
GR.BITMAP.DELETE hou
GR.BITMAP.DELETE mou
SW.BEGIN level
 SW.CASE 1
  GR.BITMAP.DELETE ckboard1
  GR.BITMAP.DELETE fire
  SW.BREAK
 SW.CASE 2
  GR.BITMAP.DELETE ckboard2
  GR.BITMAP.DELETE lif
  GR.BITMAP.DELETE ban
  SW.BREAK
 SW.CASE 3
  GR.BITMAP.DELETE ckboard3
  GR.BITMAP.DELETE bb
  GR.BITMAP.DELETE bb2
  SW.BREAK
 SW.CASE 4
  GR.BITMAP.DELETE ckboard4
  GR.BITMAP.DELETE ro
  SW.BREAK
 SW.CASE 5
  GR.BITMAP.DELETE ckboard5
  GR.BITMAP.DELETE sco1
  GR.BITMAP.DELETE sco2
  SW.BREAK
SW.END
RETURN

checkHoles:
hole=0
IF  y0=650 & (x0>=25 & x0<=75) THEN hole=hole1
IF  y0=650 & (x0>=675 & x0<725) THEN hole=hole2
IF  y0=450 & (x0>=175 & x0<=225) THEN hole=hole3
IF  y0=450 & (x0>=525 & x0<=575) THEN hole=hole4
IF  y0=250 & (x0>=25 & x0<=75) THEN hole=hole5
IF  y0=250 & (x0>=325 & x0<=425) THEN hole=hole6
IF  y0=250 & (x0>=725 & x0<=775)  THEN hole=hole7
IF hole=0 THEN RETURN
IF hole=1 THEN y0=hole1
IF hole=2 THEN y0=hole2
IF hole=3 THEN y0=hole3
IF hole=4 THEN y0=hole4
IF hole=5 THEN y0=hole5
IF hole=6 THEN y0=hole6
IF hole=7 THEN y0=hole7
GR.MODIFY kman,"x",hole
FOR ii=10 TO 200 STEP 10
 GR.MODIFY kman,"y",y0+ii
 GOSUB updateScorpions
 GR.RENDER
 PAUSE 30
NEXT
y0=y0+200
GOSUB loseEffect
GOSUB updateLives % level 1
y0=850
x0=700
GR.MODIFY kman,"x",x0
GR.MODIFY kman,"x",y0
GR.RENDER
PAUSE 500
RETURN

updateScorpions:
scorpBmp=!scorpBmp
IF scorpBmp=1 THEN
 GR.MODIFY scorpion1,"bitmap",sco1
 GR.MODIFY scorpion2,"bitmap",sco2
 GR.MODIFY scorpion3,"bitmap",sco1
 GR.MODIFY scorpion4,"bitmap",sco2
 GR.MODIFY scorpion5,"bitmap",sco1
 GR.MODIFY scorpion6,"bitmap",sco2
ELSE
 GR.MODIFY scorpion1,"bitmap",sco2
 GR.MODIFY scorpion2,"bitmap",sco1
 GR.MODIFY scorpion3,"bitmap",sco2
 GR.MODIFY scorpion4,"bitmap",sco1
 GR.MODIFY scorpion5,"bitmap",sco2
 GR.MODIFY scorpion6,"bitmap",sco1
ENDIF
RETURN

ONERROR:
vv$=VERSION$()
Device dv$
t$=""
a$=""
s$=GETERROR$()+dv$
k$=s$
FOR i=1 TO LEN(s$)
 t$=MID$(s$,i,1)
 a$=a$+CHR$(ASCII(t$)+32)
NEXT i
s$= " v1.07"
FOR i=1 TO LEN(s$)
 t$=MID$(s$,i,1)
 a$=a$+CHR$(ASCII(t$)+32)
NEXT i
TEXT.OPEN w,filN,"../../MrDong/errorLog.dat"
TEXT.WRITELN filN,a$
TEXT.CLOSE filN
EXIT

