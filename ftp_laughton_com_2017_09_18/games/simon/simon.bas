REM Simon game with a Twist
REM -Hotwrench-
REM Speed provided by cassiope34

Fn.def STG$(c)
  Fn.rtn Left$(Str$(c),Is_In(".",Str$(c))-1)
Fn.End

Fn.def gRVB(color$)
  gr.color 255, val(word$(color$,1)), val(word$(color$,2)), val(word$(color$,3)), 1
Fn.End

GR.OPEN 255,0,0,0
GR.ORIENTATION 0
GR.SCREEN w, h
x1=w/800
y1=h/480
GR.SCALE x1,y1
GR.TEXT.SIZE 90
GR.SET.STROKE 1
GR.TEXT.ALIGN 2
GR.COLOR 255,255,255,255,0
for lt=0 to 4
  GR.TEXT.DRAW simon,100,100+90*lt,mid$("SIMON",lt+1,1)
next
GR.TEXT.SIZE 30
level =1
gc=70
FOR gs = 210 TO 170 STEP -1
  gc++
  GR.COLOR 255,gc,gc,gc,1 % fix me
  GR.CIRCLE outcir,410,250,gs
NEXT

GR.SET.STROKE 3
GR.COLOR 255,0,0,0,0
GR.CIRCLE ulcir,410,250,172

DIM color$[8]
DIM arc[8]
color$[1]= "175 000 000"  % red
color$[2]= "000 175 000"  % green
color$[3]= "175 175 000"  % yellow
color$[4]= "000 000 175"  % blue
color$[5]= "255 000 000"  % red+
color$[6]= "000 255 000"  % green+
color$[7]= "255 255 000"  % yellow+
color$[8]= "000 000 255"  % blue+

for colr=8 to 1 step -1   % draw all pie
  call gRVB(color$[colr])
  GR.ARC arc[colr], 240, 80, 580, 420,val(word$("180 270 90 0 180 270 90 0",colr)),90,1
  if colr<5 then gr.hide arc[colr]
next
GR.COLOR 255,0,0,0,1
GR.CIRCLE ulcir,410,250,61
gr.color 255,110,110,110,1
GR.CIRCLE midcir,410,250,60
 gr.color 255,90,90,90,1
GR.CIRCLE midcir,410,250,60  % miscir was flat, lighting adds interest
GR.SET.STROKE 1
GR.COLOR 255,255,255,255,0
GR.TEXT.DRAW lvl,400,260, FORMAT$("##",level)

for colr=1 to 4    % intro
  gosub drawColor
next

DO
PAUSE 1000
p$ =""
d$ =""
GOSUB choose

DO
   DO
     GR.TOUCH Touched,x,y
   UNTIL Touched
   x/=x1
   y/=y1
   colr=0
   IF X > 240 & X <  420 & Y >  80 & Y <  240
    colr=1
   elseIF X > 420 & X <  600 & Y >  80 & Y <  240
    colr=2
   elseIF X > 240 & X <  420 & Y >  240 & Y <  400
    colr=3
   elseIF X > 420 & X <  600 & Y >  240 & Y <  400
    colr=4
   ENDif
   
   if colr
     p$+= stg$(colr)
     GOSUB drawColor
   ENDIF
   
  UNTIL p$<>left$(d$,len(p$)) | p$=d$
  
  IF p$=d$ THEN level=level+1 ELSE level=1    % won or lost

  gr.modify lvl, "text", FORMAT$("##",level)
  gr.render

  if level=1 then gosub drawAll    % signal for lost
 
UNTIL 0
END

choose:
FOR tt =1 TO level
  colr= FLOOR(RND()*4+1)
  d$+= stg$(colr)
  gosub drawColor
NEXT
RETURN

drawColor:
  gr.hide arc[colr]
gr.hide midcir  % lighting
  gr.render
  tone val(word$("523 587 698 659",colr)), 250,0  %0 = don't sweat tone length
  GR.show arc[colr]
gr.show midcir
  gr.render
RETURN
 
drawAll:    % light 'em up,thats an error
gr.hide arc[1]
gr.hide arc[2]
gr.hide arc[3]  % yes, it could be a loop
gr.hide arc[4]
gr.hide midcir
gr.render
tone 1000,500   % hand slap tone
gr.show arc[1]
gr.show arc[2]
gr.show arc[3]
gr.show arc[4]
gr.show midcir 
  gr.render
RETURN 

