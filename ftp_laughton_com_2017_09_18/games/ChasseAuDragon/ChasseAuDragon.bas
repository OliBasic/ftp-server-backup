!'---------------------------------------------------------------------------------------------------
!'
!'                               CHASSE AU DRAGON, PAR MOUGINO (2012)
!'                    JEU GENERE PAR ANDROID RPG MAKER ET PROPULSE PAR RFO-BASIC!
!'
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Definition des fonctions usuelles
!'---------------------------------------------------------------------------------------------------
Fn.def Stg$(c)
  Fn.rtn Left$(Str$(c),Is_In(".",Str$(c))-1)
Fn.end

Fn.def Label(x,y,txt$)
  gr.color 255,255,255,255,1
  For dy = -1 to 1
    For dx = -1 to 1
      gr.text.draw o,x+dx,y+dy,txt$
    Next dx
  Next dy
  gr.color 255,0,0,0,1
  gr.text.draw o,x,y,txt$
Fn.end

Fn.def IndexObjetMultiple(x,y)
  !' Valeurs des tiles correspondant a des objets multiples : 12,17
  if x=32 & y=31 then Fn.rtn 45 %' piece d'or
  if x=15 & y=44 then Fn.rtn 46 %' piece d'or
  if x=19 & y=45 then Fn.rtn 47 %' piece d'or
  if x=138 & y=50 then Fn.rtn 48 %' piece d'or
  if x=19 & y=53 then Fn.rtn 49 %' piece d'or
  if x=19 & y=56 then Fn.rtn 50 %' piece d'or
  if x=135 & y=67 then Fn.rtn 51 %' piece d'or
  if x=137 & y=67 then Fn.rtn 52 %' piece d'or
  if x=136 & y=68 then Fn.rtn 53 %' piece d'or
  if x=27 & y=86 then Fn.rtn 54 %' piece d'or
  if x=4 & y=91 then Fn.rtn 55 %' piece d'or
  if x=8 & y=92 then Fn.rtn 56 %' piece d'or
  if x=66 & y=196 then Fn.rtn 57 %' piece d'or
  if x=67 & y=196 then Fn.rtn 58 %' piece d'or
  if x=68 & y=196 then Fn.rtn 59 %' piece d'or
  if x=65 & y=197 then Fn.rtn 60 %' piece d'or
  if x=67 & y=197 then Fn.rtn 61 %' piece d'or
  if x=68 & y=197 then Fn.rtn 62 %' piece d'or
  if x=64 & y=198 then Fn.rtn 63 %' piece d'or
  if x=65 & y=198 then Fn.rtn 64 %' piece d'or
  if x=66 & y=198 then Fn.rtn 65 %' piece d'or
  if x=67 & y=198 then Fn.rtn 66 %' piece d'or
  if x=66 & y=199 then Fn.rtn 67 %' piece d'or
  if x=67 & y=199 then Fn.rtn 68 %' piece d'or
  if x=63 & y=200 then Fn.rtn 69 %' piece d'or
  if x=64 & y=200 then Fn.rtn 70 %' piece d'or
  if x=65 & y=200 then Fn.rtn 71 %' piece d'or
  if x=66 & y=200 then Fn.rtn 72 %' piece d'or
  if x=67 & y=200 then Fn.rtn 73 %' piece d'or
  if x=62 & y=201 then Fn.rtn 74 %' piece d'or
  if x=65 & y=201 then Fn.rtn 75 %' piece d'or
  if x=66 & y=201 then Fn.rtn 76 %' piece d'or
  if x=63 & y=202 then Fn.rtn 77 %' piece d'or
  if x=64 & y=202 then Fn.rtn 78 %' piece d'or
  if x=0 & y=32 then Fn.rtn 79 %' bourse
  if x=161 & y=49 then Fn.rtn 80 %' bourse
  if x=126 & y=73 then Fn.rtn 81 %' bourse
  if x=103 & y=83 then Fn.rtn 82 %' bourse
  if x=26 & y=87 then Fn.rtn 83 %' bourse
  if x=5 & y=123 then Fn.rtn 84 %' bourse
  if x=5 & y=127 then Fn.rtn 85 %' bourse
  if x=71 & y=194 then Fn.rtn 86 %' bourse
  if x=70 & y=195 then Fn.rtn 87 %' bourse
  if x=71 & y=195 then Fn.rtn 88 %' bourse
  if x=70 & y=196 then Fn.rtn 89 %' bourse
  if x=71 & y=196 then Fn.rtn 90 %' bourse
  if x=66 & y=197 then Fn.rtn 91 %' bourse
  if x=69 & y=197 then Fn.rtn 92 %' bourse
  if x=70 & y=197 then Fn.rtn 93 %' bourse
  if x=69 & y=198 then Fn.rtn 94 %' bourse
  if x=64 & y=199 then Fn.rtn 95 %' bourse
  if x=68 & y=199 then Fn.rtn 96 %' bourse
  if x=69 & y=199 then Fn.rtn 97 %' bourse
  if x=68 & y=200 then Fn.rtn 98 %' bourse
  if x=63 & y=201 then Fn.rtn 99 %' bourse
Fn.end
Dim objet[99] %' index max comprenant les objets multiples
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Demarrage
!'---------------------------------------------------------------------------------------------------
repjeu$ = "../../" + "ChasseAuDragon"
Gosub InitVar
mapWmax = mapw+nthor+3
mapHmax = maph+ntver+3
Dim bgndMap$[mapHmax]
Dim solMap$[mapHmax]
Dim objMap$[mapHmax]
Dim chrMap$[mapHmax]
Array.load VibrCrt[], 1, 75
Goto GetPrefs
!'---------------------------------------------------------------------------------------------------

InitVar:
!'---------------------------------------------------------------------------------------------------
!' Initialisation des variables de jeu
!'---------------------------------------------------------------------------------------------------
tilew = 18
tileh = 18
mapw = 240
maph = 250
sol0 = 71
obj0 = 91
chr0 = 141
nthor = 21
ntver = 13
mapscroll = 200 %' temps global de deplacement du heros (en ms)
padsize = 1.25 %' en tiles
herox = 8 %95
heroy = 5 %60
heroform = 1
pdv = 100 %' sante (points de vie)
usd = 0   %' argent
ata = 1   %' des d'attaque
def = 2   %' niveau de defense
neq = 0   %' nb d'objets dans l'equipement
batx = 40
baty = 125
music$ = "ContinentOuest.mid"
Return
!'---------------------------------------------------------------------------------------------------

GetPrefs:
!'---------------------------------------------------------------------------------------------------
!' Verif. repertoire du jeu + preferences (mode silencieux)
!'---------------------------------------------------------------------------------------------------
File.Exists o, repjeu$
If !o Then File.Mkdir repjeu$
File.Exists o, repjeu$ + "/prefs.txt"
If !o Then
  mute = 0
  Text.open w, c, repjeu$ + "/prefs.txt"
  Text.writeln c, Stg$(mute)
  Text.close c
Else
  Text.open r, c, repjeu$ + "/prefs.txt"
  Text.readln c, e$
  mute = VAL(e$)
  Text.close c
End If
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Ecran de titre + Chargement des ressources et calques de la carte
!'---------------------------------------------------------------------------------------------------
gr.open 255,0,0,0
gr.orientation 0
gr.screen real_w, real_h
if Mod(nthor,2) = 0 then nthor = nthor - 1
if Mod(ntver,2) = 0 then ntver = ntver - 1
if herox < 1 then herox = 1
if heroy < 1 then heroy = 1
if herox > mapw then herox = mapw
if heroy > maph then heroy = maph
dx = Ceil(nthor/2)+1
dy = Ceil(ntver/2)+1

di_w = 640
di_h = 480
scale_w = real_w/di_w
scale_h = real_h/di_h
gr.scale scale_w, scale_h
gr.bitmap.load tmpBmp,"title.png"
gr.bitmap.scale titBmp, tmpBmp, 640, 400
gr.bitmap.delete tmpBmp

gr.bitmap.draw tmpPtr, titBmp, 0, 0
gr.color 255,245,245,245,1
gr.text.size 14
gr.text.align 1
gr.text.draw o,10,475,"Jeu realise grace aux programmes Android RPG Maker et BASIC! (Open Source GPL)"

gr.color 255,255,255,255,1
gr.text.size 18
gr.text.align 2
gr.text.width zd, "Chargement des ressources..."
gr.text.draw o,320,420,"Chargement des ressources..."
gr.arc b,320-zd/2-3,430,320-zd/2+3,436,90,180,1
gr.arc b,320+zd/2-3,430,320+zd/2+3,436,-90,180,1
gr.color 255,255,255,255,0
gr.rect zu,320-zd/2,430,320+zd/2,436
gr.color 255,255,255,255,1
gr.rect zu,320-zd/2,430,320-zd/2,436

for b = 1 to 4
  gr.bitmap.load tmpBmp,"hero.form" + Stg$(b) + ".png"
  gr.bitmap.size tmpBmp, w, h
  x = h/tileh * w/tilew + 1
  if b = 1 then dim heroTile[4,x]
  for y = 0 to h/tileh - 1
    for x = 0 to w/tilew - 1
      gr.bitmap.crop heroTile[b, y*w/tilew+x+1], tmpBmp, x*tilew, y*tileh, tilew, tileh
    next x
  next y
  gr.bitmap.delete tmpBmp
next b

gr.bitmap.load tmpBmp,"background.png"
gr.bitmap.size tmpBmp, w, h
x = h/tileh * w/tilew + 1
dim bgndTile[x]
for y = 0 to h/tileh - 1
  for x = 0 to w/tilew - 1
    gr.bitmap.crop bgndTile[y*w/tilew+x+1], tmpBmp, x*tilew, y*tileh, tilew, tileh
  next x
next y
gr.bitmap.delete tmpBmp

gr.bitmap.load tmpBmp,"objects.png"
gr.bitmap.size tmpBmp, w, h
x = h/tileh * w/tilew + 1
dim objTile[x]
dim equipement[x]
for y = 0 to h/tileh - 1
  for x = 0 to w/tilew - 1
    gr.bitmap.crop objTile[y*w/tilew+x+1], tmpBmp, x*tilew, y*tileh, tilew, tileh
  next x
next y
gr.bitmap.delete tmpBmp

gr.bitmap.load tmpBmp,"characters.png"
gr.bitmap.size tmpBmp, w, h
x = h/tileh * w/tilew + 1
dim chrTile[x]
dim perso[x]
for y = 0 to h/tileh - 1
  for x = 0 to w/tilew - 1
    gr.bitmap.crop chrTile[y*w/tilew+x+1], tmpBmp, x*tilew, y*tileh, tilew, tileh
  next x
next y
gr.bitmap.delete tmpBmp

sql.open db, "JERU2.DB"
i = 1
sql.raw_query y, db, "select cast(ascii as text) from background"
DO
  sql.next Done,y,e$
  if !Done then bgndMap$[i] = e$
  if Mod(25*i,mapHmax) = 0 then
    gr.modify zu,"right",320-zd/2+zd*i/(4*mapHmax)
    gr.render
  end if
  i = i + 1
UNTIL Done

i = 1
sql.raw_query y, db, "select cast(ascii as text) from solid"
DO
  sql.next Done,y,e$
  if !Done then solMap$[i] = e$
  if Mod(i,10) = 0 then
    gr.modify zu,"right",320-zd/2+zd*(0.25+i/(4*mapHmax))
    gr.render
  end if
  i = i + 1
UNTIL Done

i = 1
sql.raw_query y, db, "select cast(ascii as text) from objects"
DO
  sql.next Done,y,e$
  if !Done then objMap$[i] = e$
  if Mod(i,10) = 0 then
    gr.modify zu,"right",320-zd/2+zd*(0.5+i/(4*mapHmax))
    gr.render
  end if
  i = i + 1
UNTIL Done

i = 1
sql.raw_query y, db, "select cast(ascii as text) from characters"
DO
  sql.next Done,y,e$
  if !Done then chrMap$[i] = e$
  if Mod(i,10) = 0 then
    gr.modify zu,"right",320-zd/2+zd*(0.75+i/(4*mapHmax))
    gr.render
  end if
  i = i + 1
UNTIL Done
sql.close db
!'---------------------------------------------------------------------------------------------------

MenuPrincipal:
!'---------------------------------------------------------------------------------------------------
!' Menu principal: Son On/Off | Nouveau jeu & niveau de difficulte | Continuer
!'---------------------------------------------------------------------------------------------------
InMenuPrincipal = 1
InSousMenu = 0
InJeu = 0
gr.cls
gr.color 255,245,245,245,1
gr.bitmap.draw tmpPtr, titBmp, 0, 0
gr.text.size 14
gr.text.align 1
gr.text.draw o,10,475,"Jeu realise grace aux programmes Android RPG Maker et BASIC! (Open Source GPL)"
gr.text.align 3
gr.text.underline 1
gr.text.draw o,630,475,"En savoir plus"
gr.text.underline 0

gr.text.size 26
gr.text.bold 1
gr.text.align 2
gr.text.width s, "Nouveau jeu"
s = s + 20

gr.color 255,255,255,255,1
gr.arc a1,200-s/2-6,410,200-s/2+6,455,90,180,1
gr.arc a2,200+s/2-6,410,200+s/2+6,455,-90,180,1
gr.rect a3,200-s/2-1,410,200+s/2+1,455
gr.color 255,0,0,0,1
gr.text.draw o,200,440,"Nouveau jeu"

File.Exists Sauvegarde, repjeu$ + "/jeu.sav"
If Sauvegarde Then gr.color 255,255,255,255,1 Else gr.color 255,80,80,80,1
gr.arc b1,440-s/2-6,410,440-s/2+6,455,90,180,1
gr.arc b2,440+s/2-6,410,440+s/2+6,455,-90,180,1
gr.rect b3,440-s/2-1,410,440+s/2+1,455
gr.color 255,0,0,0,1
gr.text.draw o,440,440,"Continuer"
gr.text.bold 0

!' Icone haut-parleur
gr.color 255,255,255,255,1
gr.circle zu,30,432,26
gr.color 255,0,0,0,0
gr.set.stroke 2
gr.rect zu,11,426,23,438
gr.line zu,23,438,37,452
gr.line zu,37,452,38,452
gr.line zu,38,452,38,412
gr.line zu,38,412,37,412
gr.line zu,37,412,23,426
gr.set.stroke 4
gr.line zr,4,406,56,458
gr.line zl,4,458,56,406
gr.set.stroke 0
If !mute Then
  gr.hide zr
  gr.hide zl
End If
gr.render

DO
  !' L'utilisateur touche l'icone haut-parleur
  gr.bounded.touch zu,4*scale_w,406*scale_h,56*scale_w,458*scale_h
  if zu Then
    mute = 1 - mute
    Text.open w, c, repjeu$ + "/prefs.txt"
    Text.writeln c, Stg$(mute)
    Text.close c
    if mute then
      gr.show zr
      gr.show zl
      gr.render
      for i = 1 to 3
        vibrate VibrCrt[], -1
        Pause 150
      next i
    else
      gr.hide zr
      gr.hide zl
      gr.render
      audio.load snd, "Bourse.mp3"
      audio.length c, snd
      audio.stop
      audio.play snd
      Pause c
      audio.stop
      audio.release snd
    end if
    do
      gr.touch zu, x, y
    until !zu
  end if
  gr.bounded.touch zu,(200-s/2-1)*scale_w,410*scale_h,(200+s/2+1)*scale_w,455*scale_h
  if zu Then Goto NouveauJeu
  if Sauvegarde then
    gr.bounded.touch zu,(440-s/2-1)*scale_w,410*scale_h,(440+s/2+1)*scale_w,455*scale_h
    if zu then Goto Continuer
  end if
  gr.bounded.touch zu,524*scale_w,455*scale_h,640*scale_w,480*scale_h
  if zu then Browse "http://laughton.com/basic/"
UNTIL 0

!' L'utilisateur presse la touche "Nouveau jeu"
NouveauJeu:
InMenuPrincipal = 0
InSousMenu = 1
InJeu = 0
Gosub EffetAppuiMenu
If !Sauvegarde Then Goto ChoisirDifficulte

!' Une sauvegarde existe deja --> l'ecraser ?
gr.cls
gr.color 255,0,0,0,1
gr.bitmap.draw tmpPtr, titBmp, 0, 0
gr.color 255,245,245,245,1
gr.text.size 14
gr.text.align 1
gr.text.draw o,10,475,"Jeu realise grace aux programmes Android RPG Maker et BASIC! (Open Source GPL)"
gr.text.align 3
gr.text.underline 1
gr.text.draw o,630,475,"En savoir plus"
gr.text.underline 0

gr.color 255,255,255,255,1
gr.text.size 22
gr.text.bold 1
gr.text.align 1
gr.text.draw o,60,430,"Reinitialiser le jeu?"
gr.text.size 20
gr.text.draw o,40,450,"(la sauvegarde sera perdue)"

gr.text.size 26
gr.text.align 2
gr.text.width s, "OUI"
s = s + 20

gr.color 255,255,255,255,1
gr.arc a1,380-s/2-6,410,380-s/2+6,455,90,180,1
gr.arc a2,380+s/2-6,410,380+s/2+6,455,-90,180,1
gr.rect a3,380-s/2-1,410,380+s/2+1,455
gr.color 255,0,0,0,1
gr.text.draw o,380,440,"OUI"

gr.color 255,255,255,255,1
gr.arc b1,480-s/2-6,410,480-s/2+6,455,90,180,1
gr.arc b2,480+s/2-6,410,480+s/2+6,455,-90,180,1
gr.rect b3,480-s/2-1,410,480+s/2+1,455
gr.color 255,0,0,0,1
gr.text.draw o,480,440,"NON"
gr.text.bold 0

gr.render
DO
  gr.bounded.touch zu,(380-s/2-1)*scale_w,410*scale_h,(380+s/2+1)*scale_w,455*scale_h
  if zu Then
    Gosub EffetAppuiMenu
    JeuDejaPret = 0
    Gosub InitVar
    Array.length c, objet[]
    for i = 1 to c
      objet[i] = 0
    next i
    Array.length c, equipement[]
    for i = 1 to c
      equipement[i] = 0
    next i
    Array.length c, perso[]
    for i = 1 to c
      perso[i] = 0
    next i
    Goto ChargerCarte
  end if
  gr.bounded.touch zu,(480-s/2-1)*scale_w,410*scale_h,(480+s/2+1)*scale_w,455*scale_h
  if zu Then
    a1 = b1
    a2 = b2
    a3 = b3
    Gosub EffetAppuiMenu
    Goto MenuPrincipal
  end if
  gr.bounded.touch zu,524*scale_w,455*scale_h,640*scale_w,480*scale_h
  if zu Then Browse "http://laughton.com/basic/"
UNTIL 0

!' Choix de la difficulte
ChoisirDifficulte:
gr.cls
gr.color 255,245,245,245,1
gr.bitmap.draw tmpPtr, titBmp, 0, 0
gr.text.size 14
gr.text.align 1
gr.text.draw o,10,475,"Jeu realise grace aux programmes Android RPG Maker et BASIC! (Open Source GPL)"
gr.text.align 3
gr.text.underline 1
gr.text.draw o,630,475,"En savoir plus"
gr.text.underline 0

!' Arc-en-ciel
for x = 170 to 600 step 5
  zu = 360-x/600*360
  zu = zu/60
  i = FLOOR(zu)
  if i = 0 then
    r = 255
    v = 255 * zu
    b = 0
  elseif i = 1 then
    r = 255 * (2 - zu)
    v = 255
    b = 0
  elseif i = 2 then
    r = 0
    v = 255
    b = 255 * (zu - 2)
  elseif i = 3 then
    r = 0
    v = 255 * (4 - zu)
    b = 255
  elseif i = 4 then
    r = 255 * (zu - 4)
    v = 0
    b = 255
  elseif i = 5 then
    r = 255
    v = 0
    b = 255 * (6 - zu)
  end if
  gr.color 255,r,v,b,1
  gr.rect zu,x-100,405,x-100+6,460
next i
gr.text.align 2
gr.text.size 23
Call Label(285,425,"C h o i s i s s e z   l a   d i f f i c u l t e :")
gr.text.size 18
Call Label(100,455,"Facile")
Call Label(280,455,"Normal")
Call Label(470,455,"Difficile")
gr.render

DO
  gr.bounded.touch zu,70*scale_w,405*scale_h,506*scale_w,460*scale_h
  if zu Then
    gr.touch zu, x, y
    difficulte = Floor((x/scale_w - 70) / 4.36)
    Popup "Niveau de difficulte choisi : "+Stg$(difficulte)+"%",0,0,0
    gr.set.stroke 3
    gr.color 255,255,255,255,0
    gr.arc a1,(x-3)/scale_w,403,(x+3)/scale_w,405,180,180,0
    gr.arc a2,(x-3)/scale_w,460,(x+3)/scale_w,463,0,180,0
    gr.rect a3,(x-3)/scale_w,405,(x+3)/scale_w,460
    gr.set.stroke 0
    Gosub EffetAppuiMenu
    gr.hide n1
    gr.hide n2
    gr.hide n3
    gr.render
    Goto ChargerCarte
  end if
  gr.bounded.touch zu,524*scale_w,455*scale_h,640*scale_w,480*scale_h
  If zu Then Browse "http://laughton.com/basic/"
UNTIL 0

!' L'utilisateur presse la touche "Continuer"
Continuer:
a1 = b1
a2 = b2
a3 = b3
Gosub EffetAppuiMenu
Gosub ChargerSauvegarde
Goto ChargerCarte
!'---------------------------------------------------------------------------------------------------

OnBackKey:
!'---------------------------------------------------------------------------------------------------
!' Gestion de la touche Back
!'---------------------------------------------------------------------------------------------------
If InMenuPrincipal Then
  Gosub FadeOut
  Exit
ElseIf InSousMenu Then
  Goto MenuPrincipal
ElseIf InJeu Then
  if !mute then audio.stop
  Gosub SauverPartie
  Gosub FadeOut
  di_w = 640
  di_h = 480
  scale_w = real_w/di_w
  scale_h = real_h/di_h
  gr.scale scale_w, scale_h
  Goto MenuPrincipal
End If
!'---------------------------------------------------------------------------------------------------

SceneIntro:
!'---------------------------------------------------------------------------------------------------
!' Scene d'intro lors d'une nouvelle partie
!'---------------------------------------------------------------------------------------------------
!'---------------------------------------------------------------------------------------------------

ChargerCarte:
!'---------------------------------------------------------------------------------------------------
!' Chargement de la carte
!'---------------------------------------------------------------------------------------------------
If JeuDejaPret Then Goto DemarrerJeu
a1=Len(bgndMap$[1])/mapWmax
a2=Len(solMap$[1])/mapWmax
b1=Len(objMap$[1])/mapWmax
b2=Len(chrMap$[1])/mapWmax
!' Creer bitmap de l'inventaire
gr.color 255,0,0,0,1
if equBmp then gr.bitmap.delete equBmp
gr.bitmap.create equBmp, nthor*tilew, ntver*tileh
gr.bitmap.drawinto.start equBmp
for y = 0 to ntver-3 step 2
  for x = 0 to nthor-3 step 2
    if y<>0 | x<>nthor-3 then
      gr.color 150,0,0,0,1
      gr.rect tmpPtr,(x+0.1)*tilew,(y+0.1)*tileh,(x+1.9)*tilew,(y+1.9)*tileh
      gr.color 150,255,255,255,0
      gr.rect tmpPtr,(x+0.1)*tilew,(y+0.1)*tileh,(x+1.9)*tilew,(y+1.9)*tileh
      i=(y*(nthor-3)+x)/2+1
      if y>=1 then i=i+1
      if i <= neq then
        gr.color 255,0,0,0,1
        gr.bitmap.scale o,objTile[equipement[i]],1.8*tilew,1.8*tileh
        gr.bitmap.draw tmpPtr,o,(x+0.1)*tilew,(y+0.1)*tileh
        gr.bitmap.delete o
      end if
    end if
  next x
next y
gr.bitmap.drawinto.end
!' Creer bitmap de decor
gr.cls
gr.color 255,245,245,245,1
gr.bitmap.draw tmpPtr, titBmp, 0, 0
gr.text.size 14
gr.text.align 1
gr.text.draw o,10,475,"Jeu realise grace aux programmes Android RPG Maker et BASIC! (Open Source GPL)"
gr.color 255,255,255,255,1
gr.text.size 18
gr.text.align 2
gr.text.width zd, "Chargement des ressources..."
gr.text.draw o,320,420,"Chargement de la partie..."
gr.arc b,320-zd/2-3,430,320-zd/2+3,436,90,180,1
gr.arc b,320+zd/2-3,430,320+zd/2+3,436,-90,180,1
gr.color 255,255,255,255,0
gr.rect zu,320-zd/2,430,320+zd/2,436
gr.color 255,255,255,255,1
gr.rect zu,320-zd/2,430,320-zd/2,436
if bgnBmp then gr.bitmap.delete bgnBmp
gr.bitmap.create bgndBmp, (nthor+2)*tilew, (ntver+2)*tileh
gr.bitmap.drawinto.start bgndBmp
e$ = ""
for y = heroy+dy-Floor(ntver/2)-1 to heroy+dy+Floor(ntver/2)+1
  for x = herox+dx-Floor(nthor/2)-1 to herox+dx+Floor(nthor/2)+1
    Gosub AfficherDecor
    if Mod(x-herox-dx,Floor(nthor/4)) = 0 then
      o = (nthor*(y-heroy-dy+Floor(ntver/2)+1)+x-herox-dx+Floor(nthor/2)+1) / ((nthor+2)*(ntver+2))
      gr.modify zu,"right",320-zd/2+zd*o
      gr.render
    end if
  next x
next y
gr.bitmap.drawinto.end
gr.modify zu,"right",320+zd/2 %' barre a 100%
gr.render
!'---------------------------------------------------------------------------------------------------

DemarrerJeu:
!'---------------------------------------------------------------------------------------------------
!' Demarrage du moteur de jeu
!'---------------------------------------------------------------------------------------------------
InMenuPrincipal = 0
InSousMenu = 0
InJeu = 1
JeuDejaPret = 1
Gosub FadeOut

di_w = nthor * tilew
di_h = ntver * tileh
scale_w = real_w/di_w
scale_h = real_h/di_h
gr.scale scale_w, scale_h
Gosub InitEcranDeJeu

!' Demarrer la musique
if !mute then
  audio.load musPtr, music$
  audio.stop
  audio.play musPtr
  audio.loop
end if

Gosub FadeIn

BouclePrincipale:
DO
  !'Gosub JeuEnArrierePlan
  gr.bounded.touch i,real_w-padsize*tilew*2*scale_w,padsize*tileh*0.5*scale_h,real_w-padsize*tilew*0.5*scale_w,padsize*tileh*2*scale_h
  if i then Goto Inventaire
  gr.bounded.touch i,Floor(nthor/2)*tilew*scale_w,Floor(ntver/2)*tileh*scale_h,(Floor(nthor/2)+1)*tilew*scale_w,(Floor(ntver/2)+1)*tileh*scale_h
  if i then Goto ActionHeros
  gr.bounded.touch zu,padsize*tilew*2*scale_w,real_h-padsize*tileh*4.5*scale_h,padsize*tilew*3*scale_w,real_h-padsize*tileh*3.5*scale_h
  gr.bounded.touch zd,padsize*tilew*2*scale_w,real_h-padsize*tileh*1.5*scale_h,padsize*tilew*3*scale_w,real_h-padsize*tileh*0.5*scale_h
  gr.bounded.touch zr,padsize*tilew*3.5*scale_w,real_h-padsize*tileh*3*scale_h,padsize*tilew*4.5*scale_w,real_h-padsize*tileh*2*scale_h
  gr.bounded.touch zl,padsize*tilew*0.5*scale_w,real_h-padsize*tileh*3*scale_h,padsize*tilew*1.5*scale_w,real_h-padsize*tileh*2*scale_h
  if zu | zd | zr | zl then Goto HerosBouge
UNTIL 0
!'---------------------------------------------------------------------------------------------------

JeuEnArrierePlan:
!'---------------------------------------------------------------------------------------------------
!' L'utilisateur a appuye sur la touche Home du telephone -> jeu "en veille"
!'---------------------------------------------------------------------------------------------------
if Background() then
  Gosub SauverPartie
  audio.pause
  DO
    !' wait in background doing nothing
  UNTIL !Background()
  audio.play musPtr
end if
Return
!'---------------------------------------------------------------------------------------------------

InitEcranDeJeu:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine d'initialisation de l'ecran de jeu
!'---------------------------------------------------------------------------------------------------
gr.cls
coeurPtr = 0
gr.color 255,0,0,0,1
gr.bitmap.draw bgndPtr,bgndBmp,-tilew,-tileh
gr.bitmap.draw heroPtr, heroTile[heroform,4], Floor(nthor/2)*tilew, Floor(ntver/2)*tileh
!' Pad directionnel
for i = 1 to 2
  if i = 1 then
    gr.color 75,255,255,255,1
  else
    gr.color 75,0,0,0,0
    gr.set.stroke 2
  end if
  gr.circle y, padsize*tilew*2.5, di_h - padsize*tileh*4, padsize*tilew %' up
  gr.circle y, padsize*tilew*1, di_h - padsize*tileh*2.5, padsize*tilew %' left
  gr.circle y, padsize*tilew*2.5, di_h - padsize*tileh, padsize*tilew %' down
  gr.circle y, padsize*tilew*4, di_h - padsize*tileh*2.5, padsize*tilew %' right
next i
!' Bouton de l'inventaire
gr.color 75,255,255,255,1
gr.rect y, di_w - padsize*tilew*2, padsize*tileh*0.5, di_w - padsize*tilew*0.5, padsize*tileh*2
gr.color 75,0,0,0,0
gr.set.stroke 2
gr.rect y, di_w - padsize*tilew*2, padsize*tileh*0.5, di_w - padsize*tilew*0.5, padsize*tileh*2
gr.line y, di_w - padsize*tilew*1.8, padsize*tileh*1, di_w - padsize*tilew*0.7, padsize*tileh*1
gr.line y, di_w - padsize*tilew*1.8, padsize*tileh*1.25, di_w - padsize*tilew*0.7, padsize*tileh*1.25
!' Barre de vie, argent
gr.color 175,255,0,0,1
gr.bitmap.draw tmpPtr,objTile[13],0,0 %' coeur
gr.rect pdvPtr, tilew*1.1, tileh*0.4, tilew*4.1, tileh*0.6
gr.modify pdvPtr, "right", tilew*(1.1+3*pdv/100)
gr.color 175,50,50,50,0
gr.rect tmpPtr, tilew*1.1, tileh*0.4, tilew*4.1, tileh*0.6
gr.bitmap.draw tmpPtr,objTile[12],0,tileh %' bourse
gr.color 175,10,10,20,0
gr.text.size 12
gr.text.align 1
gr.text.bold 0
gr.set.stroke 1
gr.text.draw usdPtr, tilew*1.1, tileh*1.8, Stg$(usd)
Return
!'---------------------------------------------------------------------------------------------------

ActionHeros:
!'---------------------------------------------------------------------------------------------------
!' Routine d'action du heros (l'utilisateur "touche" le heros au centre de l'ecran)
!'---------------------------------------------------------------------------------------------------
x = herox + dx
y = heroy + dy
Gosub GetB
destx = herox - 1
desty = heroy - 1
i = 4*tileh*scale_h
!'いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�
!' [Script 1][pas de deplacement] Le heros fait une action (l'utilisateur le touche)
!'      b = (background) tile de decor sur lequel le heros se tient
!'      destx, desty = coordonnees actuelles du heros
!'いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�
if heroform=2 & b=11 then Goto DeterreUnCoeur %' loup creuse la terre meuble de la prairie
if heroform=3 & b=58 then Goto DeterreUnCoeur %' baleine mange le plancton des eaux profondes
!' [Fin du Script 1] FinScript1
!'いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�
FinScript1:
Goto BouclePrincipale

Inventaire:
!'---------------------------------------------------------------------------------------------------
!' Routine de consultation de l'equipement
!'---------------------------------------------------------------------------------------------------
gr.cls
gr.color 255,0,0,0,1
gr.bitmap.draw bgndPtr,bgndBmp,-tilew,-tileh
gr.bitmap.draw heroPtr, heroTile[heroform,4], Floor(nthor/2)*tilew, Floor(ntver/2)*tileh
!' Bouton de l'inventaire
gr.color 75,255,255,255,1
gr.rect y, di_w - padsize*tilew*2, padsize*tileh*0.5, di_w - padsize*tilew*0.5, padsize*tileh*2
gr.color 75,0,0,0,0
gr.set.stroke 2
gr.rect y, di_w - padsize*tilew*2, padsize*tileh*0.5, di_w - padsize*tilew*0.5, padsize*tileh*2
gr.line y, di_w - padsize*tilew*1.8, padsize*tileh*1, di_w - padsize*tilew*0.7, padsize*tileh*1
gr.line y, di_w - padsize*tilew*1.8, padsize*tileh*1.25, di_w - padsize*tilew*0.7, padsize*tileh*1.25
for i = 1 to 3
  vibrate VibrCrt[], -1
next i
gr.color 255,0,0,0,1
gr.bitmap.draw tmpPtr, equBmp, di_w, -di_h
for x = di_w-tilew/2 to tilew/2 step -tilew
  y = tileh - x/di_w*di_h
  gr.modify tmpPtr, "x", x
  gr.modify tmpPtr, "y", y
  gr.render
  Pause Floor(mapscroll/tilew)
next x
do
  gr.touch zu, x, y
until !zu

DO
  !'Gosub JeuEnArrierePlan
  gr.bounded.touch i,real_w-padsize*tilew*2*scale_w,padsize*tileh*0.5*scale_h,real_w-padsize*tilew*0.5*scale_w,padsize*tileh*2*scale_h
  if i then
    for i = 1 to 1
      vibrate VibrCrt[], -1
    next i
    for x = tilew/2 to di_w-tilew/2 step tilew
      y = tileh - x/di_w*di_h
      gr.modify tmpPtr, "x", x
      gr.modify tmpPtr, "y", y
      gr.render
      if x < di_w then Pause Floor(mapscroll/tilew)
    next x
    Gosub InitEcranDeJeu
    gr.render
    do
      gr.touch zu, x, y
    until !zu
    Goto BouclePrincipale
  else
    gr.touch zu, x, y
    if zu then
      x = Floor(0.5*(x-tilew/2*scale_w)/(tilew*scale_w))
      y = Floor(0.5*(y-tileh/2*scale_h)/(tileh*scale_h))
      if x<0 then x=0
      if x>(nthor-3)/2 then x=(nthor-3)/2
      if y<0 then y=0
      if y>(ntver-3)/2 then y=(ntver-3)/2
      if y<>0 | x<>(nthor-3)/2 then
        i=y*(nthor-3)/2+x+1
        if y>=1 then i=i+1
        if i <= neq then
          o = equipement[i]
          for i = 1 to 3
            vibrate VibrCrt[], -1
          next i
          for i = 250 to 0 step -25
            gr.modify tmpPtr, "alpha", i
            gr.render
            Pause Floor(mapscroll/tilew)
          next i
          Gosub InitEcranDeJeu
          gr.render
          Goto UtiliseObjet
        end if
        do
          gr.touch zu, x, y
        until !zu
      end if
    end if
  end if
UNTIL 0
!'---------------------------------------------------------------------------------------------------

UtiliseObjet:
!'---------------------------------------------------------------------------------------------------
!' Routine d'utilisation de l'equipement
!'---------------------------------------------------------------------------------------------------
x = herox + dx
y = heroy + dy
Gosub GetS
destx = herox - 1
desty = heroy - 1
i = -3*tileh*scale_h
!'いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�
!' [Script 2][pas de deplacement] Le heros utilise un objet de son equipement
!'      o = objet utilise ; s = solidite du tile de decor sur lequel le heros se trouve actuellement
!'      destx, desty = coordonnees actuelles du heros
!'いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�
if o=14 then Popup "Vous portez deja la Bague du Loup",0,i,0
if o=18 & s<>14 & s<>15 & s<>17 & s<>18 then
  if heroform=2 then
    heroform=1
  else
    heroform=2
  end if
  Goto Transformation
end if
!' [Fin du Script 2] FinScript2
!'いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�
FinScript2:
Goto BouclePrincipale

Transformation:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine du Script 2 : le heros se transforme
!'---------------------------------------------------------------------------------------------------
gr.color 0,0,0,0,0
gr.bitmap.draw tmpPtr, heroTile[heroform,4], Floor(nthor/2)*tilew, Floor(ntver/2)*tileh
for i = 0 to 255 step 5
  gr.modify heroPtr, "alpha", 255-i
  gr.modify tmpPtr, "alpha", i
  gr.render
  Pause Floor(mapscroll/tilew)
next i
gr.hide heroPtr
gr.render
heroPtr = tmpPtr
Goto BouclePrincipale
!'---------------------------------------------------------------------------------------------------

DeterreUnCoeur:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine : le Loup deterre un coeur depuis le tile (destx,desty) et recupere des points de vie
!'---------------------------------------------------------------------------------------------------
if !mute then
  audio.load snd, "Coeur.wav"
  audio.stop
  audio.play snd
end if
gr.color 255,0,0,0,1
if !coeurPtr then
  gr.bitmap.draw coeurPtr, objTile[13], (di_w - tilew)/2, (di_h - tileh)/2
else
  gr.show coeurPtr
end if
for y = 0 to tileh
  gr.modify coeurPtr, "x", (di_w - tilew)/2 + 0.25*tilew*Cos(3.14159265*4*y/tileh) %' + (destx-1+herox)*tilew + Cos(y)*tilew/2
  gr.modify coeurPtr, "y", (di_h - tileh)/2 - y %' + (desty-1+heroy)*tileh
  gr.render
  Pause Floor(mapscroll/tileh)
next y
gr.hide coeurPtr
!' Mise a jour des points de vie
pdv = pdv + 10
if pdv > 100 then pdv = 100
gr.modify pdvPtr, "right", tilew*(1.1+3*pdv/100)
gr.render
if !mute then
  DO
    Audio.isdone i
  UNTIL i
  audio.stop
  audio.release snd
  audio.play musPtr
  audio.loop
end if
Goto BouclePrincipale
!'---------------------------------------------------------------------------------------------------

HerosBouge:
!'---------------------------------------------------------------------------------------------------
!' Routine de deplacement du heros
!'---------------------------------------------------------------------------------------------------

!' Rencontre avec un perso
x = herox + dx + zr - zl
y = heroy + dy + zd - zu
Gosub GetC
if c then
  if zr | zl then gr.modify heroPtr,"bitmap",heroTile[heroform,6*zr+10*zl+1]
  if zu | zd then gr.modify heroPtr,"bitmap",heroTile[heroform,3*zd+1]
  gr.render
end if
destx = herox + zr - zl - 1
desty = heroy + zd - zu - 1
i = -3*tileh*scale_h
!'いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�
!' [Script 3][AVANT deplacement] Le heros veut se deplacer sur un tile contenant un personnage
!'      c = perso rencontre ; zu = (up) le heros l'aborde par le bas (il se deplace vers le haut)
!'      zd = (down) il l'aborde par le haut ; zr (right) = par la gauche ; zl (left) = par la droite
!'      destx, desty = coordonnees de destination du heros
!'いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�
if heroform=4 then Goto FinScript3 %' l'aigle survole tous les personnages
if c=11 | c=33 | c=34 | c=35 then Goto FinScript3 %' tiles de perso "transparents"
if c=7 then
  e$ = "Sorciere de l'Ouest: "
  if heroform<>1 then
    Popup "...",0,i,0
    Pause 2000
  else if zu | zd | zr then
    Popup "(la Sorciere de l'Ouest regarde ailleurs)",0,i,0
    Pause 2000
  else
    if objet[18] then %' sphere du loup
      Popup e$+"Je vois que tu es en possession de la Sphere du Loup",0,i,0
      Pause 2000
      Popup e$+"Active la depuis ton inventaire pour te transformer en loup !",0,i,0
      Pause 2000
      Popup e$+"Etre un loup a du bon...",0,i,0
      Pause 2000
      Popup e$+"Tu peux flairer les coeurs enfouis dans la terre",0,i,0
      Pause 2000
      Popup e$+"Et creuser les sols meubles ou au pied des souches d'arbre",0,i,0
      Pause 2000
      Popup e$+"Un loup peut en outre parcourir les zones glaciaires sans heurts",0,i,0
      Pause 2000
      Popup e$+"Mais le desert brulant le blesse !",0,i,0
      Pause 2000
      Popup e$+"Souviens toi de mes paroles...",0,i,0
      Pause 2000
    elseif objet[14] then %' bague du loup
      Popup e$+"Te voila equipe de la Bague du Loup",0,i,0
      Pause 2000
      Popup e$+"Avec elle tu peux franchir la barriere magique",0,i,0
      Pause 2000
      Popup e$+"qui protege la Sphere du Loup",0,i,0
      Pause 2000
      Popup e$+"Cherche cette derniere, elle ne peut etre bien loin !",0,i,0
      Pause 2000
      Popup e$+"Une fois trouvee, reviens me voir pour d'autres conseils",0,i,0
      Pause 2000
    elseif perso[c] then
      Popup e$+"Cherche la Bague du Loup",0,i,0
      Pause 2000
      Popup e$+"Une fois trouvee, reviens me voir pour d'autres conseils",0,i,0
      Pause 2000
    else
      Popup e$+"Bienvenue sur le Continent de l'Ouest jeune aventurier",0,i,0
      Pause 2000
      Popup e$+"La plus proche cite renferme un artefact",0,i,0
      Pause 2000
      Popup e$+"Cet objet pourra t'etre utile dans ta quete",0,i,0
      Pause 2000
      Popup e$+"Je vais t'ouvrir les portes de la cite",0,i,0
      Pause 2000
      Popup e$+"Ne me remercie pas !",0,i,0
      Pause 2000
      perso[c]=1
      Gosub DebloqueSecretMusique
    end if
  end if

elseif c=19 then
  e$ = "Garde: "
  if heroform<>1 then
    Popup "...",0,i,0
    Pause 2000
  else if zd then
    Popup "(le garde regarde ailleurs)",0,i,0
    Pause 2000
  else if perso[c]=2 then
    Popup "Je ne peux rien faire de plus pour toi",0,i,0
    Pause 2000
  else if perso[20]=2 then
    Popup "Que veux-tu ?",0,i,0
    Pause 2000
  else if perso[20]=1 then
    Popup "Tu as fait connaissance avec mon collegue ?",0,i,0
    Pause 2000
    Popup "C'est bien, tu as fait l'effort de visiter notre contree",0,i,0
    Pause 2000
    Popup "En recompense je t'ouvre les portes de la cite du sud",0,i,0
    Pause 2000
    perso[c]=2
    Gosub DebloqueSecretMusique
  else
    Popup e$+"Le bonjour Voyageur !",0,i,0
    Pause 2000
    Popup e$+"As-tu fait le tour de nos terres ?",0,i,0
    Pause 2000
    Popup e$+"Si tu croises mon collegue, passe-lui le bonjour",0,i,0
    Pause 2000
    perso[c]=1
  end if

elseif c=20 then
  e$ = "Garde: "
  if heroform<>1 then
    Popup "...",0,i,0
    Pause 2000
  else if zd then
    Popup "(le garde regarde ailleurs)",0,i,0
    Pause 2000
  else if perso[c]=2 then
    Popup "Je ne peux rien faire de plus pour toi",0,i,0
    Pause 2000
  else if perso[19]=2 then
    Popup "Qu'est-ce que tu veux ?",0,i,0
    Pause 2000
  else if perso[19]=1 then
    Popup "Tu as fait connaissance avec mon collegue ?",0,i,0
    Pause 2000
    Popup "C'est bien, tu as fait l'effort de visiter notre contree",0,i,0
    Pause 2000
    Popup "En recompense je t'ouvre les portes de la cite du sud",0,i,0
    Pause 2000
    perso[c]=2
    Gosub DebloqueSecretMusique
  else
    Popup e$+"Le bonjour Voyageur !",0,i,0
    Pause 2000
    Popup e$+"As-tu fait le tour de nos terres ?",0,i,0
    Pause 2000
    Popup e$+"Si tu croises mon collegue, passe-lui le bonjour",0,i,0
    Pause 2000
    perso[c]=1
  end if

elseif c=9 then
  e$ = "Nain marin: "
  if heroform<>1 then
    Popup "...",0,i,0
    Pause 2000
  elseif !perso[c] then
    Popup e$+"La crique du bord du monde...",0,i,0
    Pause 2000
    Popup e$+"Je ne me lasse pas de regarder ce vide stellaire",0,i,0
    Pause 2000
    Popup e$+"Quel repos, quelle quietude !",0,i,0
    Pause 2000
    Popup e$+"Si tu souhaites prendre la mer a bord de mon bateau",0,i,0
    Pause 2000
    Popup e$+"il t'en coutera 48 pieces d'or",0,i,0
    Pause 2000
    Popup e$+"As-tu cette somme sur toi ?",0,i,0
    Pause 2000
    if usd < 48 then
      Popup e$+"C'est bien ce qu'il me semblait...",0,i,0
      Pause 2000
    else
      Popup e$+"Oh ! Tu as "+Stg$(usd)+" pieces d'or ?!",0,i,0
      Pause 2000
      Popup e$+"Mes services de capitaine te sont acquis !",0,i,0
      Pause 2000
      Popup e$+"Rejoins-moi au bateau et nous levons l'ancre !",0,i,0
      Pause 2000
      perso[c] = 1
      usd = usd - 48
      gr.modify usdPtr, "text", Stg$(usd)
      gr.render
      Gosub DebloqueSecretMusique
    end if
  else if perso[c]=1 & destx=108 & desty=181 then
      Popup e$+"Tu es sourd mon bonhomme ?",0,i,0
      Pause 2000
      Popup e$+"J'ai dit rejoins mon bateau et nous appareillons",0,i,0
      Pause 2000
      Popup e$+"Il est a quai pres de la cite du sud",0,i,0
      Pause 2000
  end if

elseif c=23 | c=24 | c=25 then
  if zu & heroform=1 then
    if !perso[9] then
      Popup "Le bateau est vide",0,i,0
      Pause 2000
      Popup "Le capitaine a du partir en balade",0,i,0
      Pause 2000
    end if
  end if
end if
!' [Fin du Script 3] FinScript3
!'いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�
if c then Goto BouclePrincipale
FinScript3:

!' Bloque sur les bords de la carte
bloque = 0
if (zl & herox=1) | (zr & herox=mapw) | (zu & heroy=1) | (zd & heroy=maph) then bloque=1
!' Blocage selon la solidite du tile de destination
x = herox + dx + zr - zl
y = heroy + dy + zd - zu
Gosub GetB
Gosub GetS
i = -3*tileh*scale_h
!'いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�
!' [Script 4][AVANT deplacement] La solidite du tile de decor de destination est testee
!'      b = (background) tile du decor de destination ; s = solidite de ce tile
!'      "bloque=1" stoppe le deplacement a venir du heros
!'      destx, desty = coordonnees de destination du heros
!'いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�
!' Solidites de base
!'-------------------
if s=1 then
  bloque=1
elseif s=2 & !objet[14] then
  bloque=1
elseif s=3 & !objet[15] then
  bloque=1
else if s=4 & !objet[16] then
  bloque=1
elseif s=5 & !objet[4] then
  bloque=1
elseif s=6 & !objet[5] then
  bloque=1
elseif s=7 & !objet[7] then
  bloque=1
elseif s=8 & !objet[8] then
  bloque=1
elseif s=9 & !objet[9] then
  bloque=1
elseif s=10 & !objet[10] then
  bloque=1
elseif s=11 & !objet[6] then
  bloque=1
elseif s=12 & !objet[11] then
  bloque=1
elseif s=13 & heroform=3 then
  bloque=1
elseif s=14 & heroform<>4 then
  bloque=1
elseif s=15 & heroform<>3 & heroform<>4 then
  bloque=1
elseif s=16 & heroform<>2 & heroform<>4 then
  bloque=1
elseif s=17 & heroform=2 then
  bloque=1
elseif s=18 & heroform=1 then
  bloque=1
elseif s=19 & heroform<>1 then
  bloque=1
end if
!' Evolution des solidites au cours de l'aventure
!'------------------------------------------------
if perso[7] & destx=18 & (desty=25 | desty=26) then
  bloque=0 %' sorciere de l'ouest debloque porte cite nord
elseif (perso[19]=2 | perso[20]=2) & destx=4 & (desty=124 | desty=125) then
  bloque=0 %' les 2 gardes debloquent porte cite sud
elseif !perso[70] & b=70 & destx>=103 & destx<=107 & desty>=177 & desty<=189 then
  bloque=1 %' passage du dauphin pas encore debloque
end if
!' [Fin du Script 4] FinScript4
!'いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�
FinScript4:

!' Heros en mouvement
if !bloque then
  herox = herox + zr - zl
  heroy = heroy + zd - zu
end if
if zr | zl then
  for x = 1 to tilew
    if !bloque then gr.modify bgndPtr,"x",-tilew + x*(zl - zr)
    if Floor(4*x/tilew) <> Floor(4*(x-1)/tilew) then
      if Floor(4*x/tilew) = 1 then
        gr.modify heroPtr,"bitmap",heroTile[heroform,6*zr+10*zl+2]
      elseif Floor(4*x/tilew) = 2 then
        gr.modify heroPtr,"bitmap",heroTile[heroform,6*zr+10*zl+3]
      elseif Floor(4*x/tilew) = 3 then
        gr.modify heroPtr,"bitmap",heroTile[heroform,6*zr+10*zl+2]
      elseif Floor(4*x/tilew) = 4 then
        gr.modify heroPtr,"bitmap",heroTile[heroform,6*zr+10*zl+1]
      end if
      gr.render
    end if
    if x < tilew then Pause Floor(mapscroll/tilew)
  next x
elseif zu | zd then
  for y = 1 to tileh
    if !bloque then gr.modify bgndPtr,"y",-tileh + y*(zu - zd)
    if Floor(4*y/tileh) <> Floor(4*(y-1)/tileh) then
      if Floor(4*y/tileh) = 1 then
        gr.modify heroPtr,"bitmap",heroTile[heroform,3*zd+2]
      elseif Floor(4*y/tileh) = 2 then
        gr.modify heroPtr,"bitmap",heroTile[heroform,3*zd+3]
      elseif Floor(4*y/tileh) = 3 then
        gr.modify heroPtr,"bitmap",heroTile[heroform,3*zd+2]
      elseif Floor(4*y/tileh) = 4 then
        gr.modify heroPtr,"bitmap",heroTile[heroform,3*zd+1]
      end if
      gr.render
    end if
    if y < tileh then Pause Floor(mapscroll/tileh)
  next y
end if

!' Trouve un objet
x = herox + dx
y = heroy + dy
Gosub GetO
if !o then Goto FinScript5 %' aucun objet
i = 4*tileh*scale_h
!'いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�
!' [Script 5][APRES deplacement] Le heros est arrive sur un tile contenant un objet
!'      o = objet du tile de destination (premiere rencontre, jamais ramasse)
!'      destx, desty = coordonnees de destination du heros
!'いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�
if heroform<>1 then Goto FinScript5 %' seul le heros sous sa forme "humaine" peut ramasser les objets
if o<=3 then Goto FinScript5 %' objets sans interaction
if o>=31 then Goto FinScript5 %' objets sans interaction
if o=17 then %' piece d'or (OBJET MULTIPLE)
  argent = 1
  Goto RamasseArgent
elseif o=12 then %' bourse (OBJET MULTIPLE)
  argent = 10
  Goto RamasseArgent
elseif o=21 then %' tornade passage-secret
  !' ToDo
elseif o=14 then %' bague du loup
  e$ = "Vous avez trouve la Bague du Loup !"
  Goto TrouveObjet
elseif o=18 then %' sphere du loup
  e$ = "Vous avez trouve la Sphere du Loup !"
  Goto TrouveObjet
elseif o=9 then %' bouclier sacre
  e$ = "Vous avez trouve le Bouclier Sacre !"
  Goto TrouveObjet
end if
!' [Fin du Script 5] FinScript5
!'いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�
FinScript5:

!' Mise a jour de la carte
if bloque then
  Pause mapscroll
else
  gr.color 255,0,0,0,1
  gr.bitmap.crop tmpBmp, bgndBmp, tilew*zr, tileh*zd, (nthor+1+zu+zd)*tilew, (ntver+1+zl+zr)*tileh
  gr.bitmap.delete bgndBmp
  gr.bitmap.create bgndBmp, (nthor+2)*tilew, (ntver+2)*tileh
  gr.bitmap.drawinto.start bgndBmp
  gr.bitmap.draw tmpPtr, tmpBmp, tilew*zl, tileh*zu
  gr.bitmap.delete tmpBmp
  if zr | zl then
    x = herox+dx+(zr-zl)*(Floor(nthor/2)+1)
    for y = heroy+dy-Floor(ntver/2)-1 to heroy+dy+Floor(ntver/2)+1
      Gosub AfficherDecor
    next y
  elseif zu | zd then
    y = heroy+dy+(zd-zu)*(Floor(ntver/2)+1)
    for x = herox+dx-Floor(nthor/2)-1 to herox+dx+Floor(nthor/2)+1
      Gosub AfficherDecor
    next x
  end if
  gr.bitmap.drawinto.end
  gr.modify bgndPtr,"x",-tilew
  gr.modify bgndPtr,"y",-tileh
  gr.modify bgndPtr,"bitmap",bgndBmp
end if

Goto BouclePrincipale
!'---------------------------------------------------------------------------------------------------

AfficherDecor:
!'---------------------------------------------------------------------------------------------------
!' Routine d'affichage du decor
!'---------------------------------------------------------------------------------------------------
Gosub GetB
Gosub GetO
Gosub GetC
destx = x - dx - 1
desty = y - dy - 1
!'いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�
!' [Script 6][LORS DU deplacement] Rafraichit les tiles de decor en bord de carte lorsqu'elle defile
!'      b = (background) tile de decor ; o = tile d'objet ; c = (character) tile de perso
!'      destx, desty = coordonnees du tile courant a afficher
!'いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�
if perso[7] & destx=18 & desty=25 then
  b=19 %' sorciere de l'ouest debloque porte cite du nord
elseif perso[7] & destx=18 & desty=26 then
  b=18 %' sorciere de l'ouest debloque porte cite du nord
elseif (perso[19]=2 | perso[20]=2) & destx=4 & desty=124 then
  b=19 %' les 2 gardes debloquent porte cite sud
elseif (perso[19]=2 | perso[20]=2) & destx=4 & desty=125 then
  b=18 %' les 2 gardes debloquent porte cite sud
elseif c=9 & perso[9] then
  c=0 %' nain marin sur son bateau
elseif !perso[70] & b=70 & destx>=103 & destx<=107 & desty>=177 & desty<=189 then
  b=56 %' passage du dauphin
end if
!' [Fin du Script 6] FinScript6
!'いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�
FinScript6:
gr.color 255,0,0,0,1
if b then gr.bitmap.draw tmpPtr, bgndTile[b], (x-herox-dx+Floor(nthor/2)+1)*tilew, (y-heroy-dy+Floor(ntver/2)+1)*tileh
if o then gr.bitmap.draw tmpPtr, objTile[o], (x-herox-dx+Floor(nthor/2)+1)*tilew, (y-heroy-dy+Floor(ntver/2)+1)*tileh
if c then gr.bitmap.draw tmpPtr, chrTile[c], (x-herox-dx+Floor(nthor/2)+1)*tilew, (y-heroy-dy+Floor(ntver/2)+1)*tileh
Return
!'---------------------------------------------------------------------------------------------------

TrouveObjet:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine du Script 5 lorsque le heros trouve un objet qui enrichit son equipement
!'---------------------------------------------------------------------------------------------------
!' Animation
Popup e$,0,i,4
if !mute then
  audio.load snd, "TrouveObjet.mp3"
  audio.stop
  audio.play snd
end if
gr.color 255,0,0,0,1
gr.modify heroPtr,"bitmap",heroTile[heroform,15]
gr.bitmap.drawinto.start bgndBmp
x = herox + dx
y = heroy + dy
Gosub GetB %' tile de decor sous l'objet
if b > 0 then gr.bitmap.draw tmpPtr, bgndTile[b], (x+zr-zl-herox-dx+Floor(nthor/2)+1)*tilew, (y+zd-zu-heroy-dy+Floor(ntver/2)+1)*tileh
gr.bitmap.drawinto.end
gr.bitmap.draw tmpPtr, objTile[o], (di_w - tilew)/2, (di_h - tileh)/2
for y = (di_h - tileh)/2 to padsize*tileh*0.5 Step -1
  gr.modify tmpPtr, "y", y
  gr.render
  Pause 2*Floor(mapscroll/tileh)
next y
!' Mise a jour de l'inventaire
objet[oIndx] = 1
neq = neq + 1
equipement[neq] = oIndx
gr.bitmap.drawinto.start equBmp
if neq <= (nthor-3)/2 then
  x = neq-1
  y = 0
else
  x = Mod(neq-1,(nthor-3)/2)
  y = (neq-x-1)/(nthor-3)*2
end if
gr.color 255,0,0,0,1
gr.bitmap.scale c,objTile[o],1.8*tilew,1.8*tileh
gr.bitmap.draw i,c,(2*x+0.1)*tilew,(2*y+0.1)*tileh
gr.bitmap.delete c
gr.bitmap.drawinto.end
!' Fin animation
if !mute then
  DO
    Audio.isdone i
  UNTIL i
  audio.stop
  audio.release snd
  audio.play musPtr
  audio.loop
end if
gr.modify heroPtr,"bitmap",heroTile[heroform,4]
for x = (di_w - tilew)/2 to di_w - padsize*tilew*1.25 Step tilew/2
  gr.modify tmpPtr, "x", x
  gr.render
  Pause Floor(mapscroll/tileh)
next y
gr.hide tmpPtr
gr.render
Goto FinScript5
!'---------------------------------------------------------------------------------------------------

RamasseArgent:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine du Script 5 lorsque le heros ramasse de l'argent
!'---------------------------------------------------------------------------------------------------
if !mute then
  audio.load snd, "Bourse.mp3"
  audio.stop
  audio.play snd
end if
gr.color 255,0,0,0,1
gr.bitmap.drawinto.start bgndBmp
x = herox + dx
y = heroy + dy
Gosub GetB %' tile de decor sous l'objet
if b > 0 then gr.bitmap.draw tmpPtr, bgndTile[b], (x+zr-zl-herox-dx+Floor(nthor/2)+1)*tilew, (y+zd-zu-heroy-dy+Floor(ntver/2)+1)*tileh
gr.bitmap.drawinto.end
gr.bitmap.draw tmpPtr, objTile[o], (di_w - tilew)/2, (di_h - tileh)/2
for x = (di_w - tilew)/2 to padsize*tilew*1.25 Step -tilew/2
  y = x/di_w*di_h
  gr.modify tmpPtr, "x", x
  gr.modify tmpPtr, "y", y
  gr.render
  Pause Floor(mapscroll/tileh)
next y
gr.hide tmpPtr
gr.render
!' Mise a jour de la bourse du heros
objet[oIndx] = 1
usd = usd + argent
gr.modify usdPtr, "text", Stg$(usd)
gr.render
if !mute then
  DO
    Audio.isdone i
  UNTIL i
  audio.stop
  audio.release snd
  audio.play musPtr
  audio.loop
end if
Goto FinScript5
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Sous-routines "GOSUB" diverses
!'---------------------------------------------------------------------------------------------------
FadeIn:
gr.color 255,0,0,0,1
gr.rect rectPtr, 0, 0, di_w, di_h
for i = 255 to 0 step -15
  gr.modify rectPtr, "alpha", i
  gr.render
  Pause 20
next i
gr.hide rectPtr
gr.render
Return

FadeOut:
gr.color 0,0,0,0,1
gr.rect rectPtr, 0, 0, di_w, di_h
for i = 0 to 255 step 15
  gr.modify rectPtr, "alpha", i
  gr.render
  Pause 20
next i
Return
 
EffetAppuiMenu:
if mute then
  for i = 1 to 5
    vibrate VibrCrt[], -1
    gr.hide a1
    gr.hide a2
    gr.hide a3
    gr.render
    Pause 50
    gr.show a1
    gr.show a2
    gr.show a3
    gr.render
    Pause 50
  next i
else
  audio.load snd, "Select.mp3"
  audio.stop
  audio.play snd
  DO
    gr.hide a1
    gr.hide a2
    gr.hide a3
    gr.render
    Pause 50
    gr.show a1
    gr.show a2
    gr.show a3
    gr.render
    Pause 50
    Audio.isdone i
  UNTIL i
  audio.stop
  audio.release snd
end if
Return

DebloqueSecretMusique:
if !mute then
  audio.load snd, "debloque.mid"
  audio.length i, snd
  audio.stop
  audio.play snd
  Pause i
  audio.stop
  audio.release snd
  audio.play musPtr
  audio.loop
else
  for i = 1 to 3
    vibrate VibrCrt[], -1
    Pause 150
  next i
end if
Return

SauverPartie:
Byte.open w, o, repjeu$ + "/jeu.sav"
e$ = Chr$(difficulte)
e$ = e$ + Chr$(herox)
e$ = e$ + Chr$(heroy)
e$ = e$ + Chr$(heroform)
e$ = e$ + Chr$(pdv)
e$ = e$ + Chr$(usd)
e$ = e$ + Chr$(ata)
e$ = e$ + Chr$(def)
Array.length c, objet[]
for i = 1 to c
  e$ = e$ + Chr$(objet[i])
next i
e$ = e$ + Chr$(neq)
Array.length c, equipement[]
for i = 1 to c
  e$ = e$ + Chr$(equipement[i])
next i
Array.length c, perso[]
for i = 1 to c
  e$ = e$ + Chr$(perso[i])
next i
e$ = e$ + Chr$(batx)
e$ = e$ + Chr$(baty)
e$ = e$ + music$
Byte.write.buffer o, e$
Byte.close o
Sauvegarde = 1
Return

ChargerSauvegarde:
File.size i, repjeu$ + "/jeu.sav"
Byte.open r, o, repjeu$ + "/jeu.sav"
Byte.read.buffer o, i, e$
difficulte = Ascii(e$)
  e$ = Right$(e$, Len(e$) - 1)
herox = Ascii(e$)
  e$ = Right$(e$, Len(e$) - 1)
heroy = Ascii(e$)
  e$ = Right$(e$, Len(e$) - 1)
heroform = Ascii(e$)
  e$ = Right$(e$, Len(e$) - 1)
pdv = Ascii(e$)
  e$ = Right$(e$, Len(e$) - 1)
usd = Ascii(e$)
  e$ = Right$(e$, Len(e$) - 1)
ata = Ascii(e$)
  e$ = Right$(e$, Len(e$) - 1)
def = Ascii(e$)
  e$ = Right$(e$, Len(e$) - 1)
Array.length c, objet[]
for i = 1 to c
  objet[i] = Ascii(e$)
  e$ = Right$(e$, Len(e$) - 1)
next i
neq = Ascii(e$)
  e$ = Right$(e$, Len(e$) - 1)
Array.length c, equipement[]
for i = 1 to c
  equipement[i] = Ascii(e$)
  e$ = Right$(e$, Len(e$) - 1)
next i
Array.length c, perso[]
for i = 1 to c
  perso[i] = Ascii(e$)
  e$ = Right$(e$, Len(e$) - 1)
next i
batx = Ascii(e$)
  e$ = Right$(e$, Len(e$) - 1)
baty = Ascii(e$)
  e$ = Right$(e$, Len(e$) - 1)
music$ = e$
Byte.close o
Return

GetB:
if a1=1 then
  b = Ascii(Mid$(bgndMap$[y],x,1))
else
  b = 0 %' (B=background) retourne le tile de decor aux coordonnees x,y
  for i = 1 to a1
    b = b + Ascii(Mid$(bgndMap$[y],a1*(x-1)+i,1))*256^(a1-i)
  next i
end if
Return

GetS:
if a2=1 then
  s = Ascii(Mid$(solMap$[y],x,1))
else
  s = 0 %' (S=solidite) retourne la solidite du tile aux coordonnees x,y
  for i = 1 to a2
    s = s + Ascii(Mid$(solMap$[y],a2*(x-1)+i,1))*256^(a2-i)
  next i
end if
Return

GetO:
if b1=1 then
  o = Ascii(Mid$(objMap$[y],x,1))
else
  o = 0 %' (O=objet) retourne le tile d'objet aux coordonnees x,y
  for i = 1 to b1
    o = o + Ascii(Mid$(objMap$[y],b1*(x-1)+i,1))*256^(b1-i)
  next i
end if
if !o then Return
%' oIndx retourne l'index de l'objet aux coordonnees x,y (comprenant objets uniques ET multiples)
if o=12 | o=17 then %' OBJETS MULTIPLES
  oIndx = IndexObjetMultiple(x-dx-1,y-dy-1)
else %' OBJETS UNIQUES
  oIndx = o
end if
!'if oIndx then
  if objet[oIndx] then o = 0 %' objet deja trouve
!'end if
Return

GetC:
!' Bateau
a3=x-batx-dx
b3=y-baty-dy
if a3>=1 & a3<=3 & b3>=1 & b3<=2 then
  c=22+10*(b3-1)+a3
  if perso[9] & a3=1 & b3=1 then c=71
  Return
end if
!' Autres persos
if b2=1 then
  c = Ascii(Mid$(chrMap$[y],x,1))
else
  c = 0 %' (C=character) retourne le perso aux coordonnees x,y
  for i = 1 to b2
    c = c + Ascii(Mid$(chrMap$[y],b2*(x-1)+i,1))*256^(b2-i)
  next i
end if
Return
!'---------------------------------------------------------------------------------------------------