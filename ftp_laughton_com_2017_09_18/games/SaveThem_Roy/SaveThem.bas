Rem Save Them
Rem For Android
Rem With RFO Basic!
Rem February 2016
Rem Version 1.00
Rem By Roy Shepherd

di_height = 672 % set to my Device
di_width = 1152

gr.open
WakeLock 3 

gr.screen screenWidth, screenHeight
scale_x = screenWidth / di_width
scale_y = screenHeight / di_height
gr.scale scale_x, scale_y
gr.text.size 40

gosub Functions % Let Basic see the functions before they are called.

!------------------------------------------------
! Start game
!------------------------------------------------

gosub Initialise
gosub SetUp
gosub PlayGame
end

onBackKey:
    dialog.message "Exit",,yn,"Yes","No"
    if yn = 1 then gosub GoodBye
back.resume 
end

!------------------------------------------------
! Set global variables and lists.
! Load and scale bitmaps. Display background 
! Put the animals pointers into the list animalList and draw them off screen
!------------------------------------------------
Initialise:
    path$ = "../../SaveThem/data/Images/"
    list.create n, animalList
    list.create n, lostAnimals
    gameTime = 60 : score = 0 : best = 0 : outTime = 2 : sound = 1
    gosub LoadBest
    gr.bitmap.load bg, path$ + "b3.jpg"
    gr.bitmap.scale bg, bg, di_width - 50, di_height
    gr.bitmap.draw bg, bg, 0, 0
    Rem sound icons
    gr.bitmap.load sOff, path$ + "sOff.jpg" 
    gr.bitmap.scale sOff, sOff, 50, 50
    gr.bitmap.draw sOff, sOff, di_width - 50, di_height - 50
    
    gr.bitmap.load sOn, path$ + "sOn.jpg" 
    gr.bitmap.scale sOn, sOn, 50, 50
    gr.bitmap.draw sOn, sOn, di_width - 50, di_height - 50
    if ! sound then gr.hide sOn
    Rem game help
    gr.bitmap.load help, path$ + "SaveThem.png"
    gr.bitmap.size help, w, helpHeight
    gr.bitmap.draw help, help, 300, - di_width - 20
    Rem gun shot
    gr.bitmap.load shot, path$ + "explode.png" 
    gr.bitmap.scale shot, shot, 25, 25
    gr.bitmap.draw shot, shot, - 50, - 50
    Rem Show score and best
    gr.text.draw showScore, 1, 40, "Score: " + int$(score)
    gr.text.align 2
    gr.text.draw showTime, di_width / 2, 40, "Time Left: " + int$(gameTime) 
    gr.text.align 3
    gr.text.draw showBest, di_width - 50, 40, "Best: " + int$(best) 
    gr.text.align 1
    gr.render
    Rem Animals
    for x = 1 to 15 
        gr.bitmap.load a, path$ + "a" + int$(x) +".png"
        gr.bitmap.scale a, a, 50, 75
        gr.bitmap.draw la, a, - 50, - 50
        list.add animalList, la
    next
    Rem This comes from Gikam's UI generator
    Rem Detect when an animal tapped using collision
    gr.color 0, 0, 0, 0
    gr.point collision, - 1, - 1 
    gosub LoadSound
return

!------------------------------------------------
! Load the game sounds
!------------------------------------------------
LoadSound:
    dataPath$="../../SaveThem/data/Sounds/"
    soundpool.open 5
    soundpool.load  saved, dataPath$   + "saved.wav"
    soundpool.load  wow, dataPath$     + "wow.wav"            
    soundpool.load  opps, dataPath$    + "haa.wav"      
    soundpool.load  goodBye, dataPath$ + "goodbye.wav"   
    soundpool.load  explode, dataPath$ + "explode.wav" 
    
    audio.load paintBlack, dataPath$ + "PaintBlack.mp3" 
	  if paintBlack = 0 then e$ = GETERROR$() : ?e$ : end
    call LoopSound(sound, paintBlack)
return

!------------------------------------------------
! Do at start of each game
!------------------------------------------------
SetUp:
     saved_y = 0
     gr.modify showScore, "text", "Score: " + int$(score)
     list.add.list lostAnimals, animalList
     
     for x = 1 to 15 
        List.get lostAnimals, x, animal
        gr.modify animal, "x", - 50, "y", - 50
     next
return

!------------------------------------------------
! Save the animals
!------------------------------------------------
PlayGame:
    do : dialog.message "Start or Help",,sh,"Start","Help" : until sh > 0
    if sh = 2 then gosub help
    do
        tick = time()
        do
            do
                Rem Turn sound on and off
                if gr_collision(collision, sOn) then 
                    gr.hide sOn : sound = 0
                    call LoopSound(sound, paintBlack)
                elseif gr_collision(collision, sOff) 
                    gr.show sOn : sound = 1
                    call LoopSound(sound, paintBlack)
                endif
                Rem Show an animal; user has a short time to tap it.
                gosub ShowLostAnimal
                now = time()
                do 
                    tock = (time() - tick) / 1000
                    if tock > gameTime then tock = gameTime
                    gr.modify showTime, "text", "Time Left: " + int$(gameTime - tock) : gr.render
                    gr.touch t, tx, ty : tt = (time() - now) / 1000 
                until t | tt > outTime
                do : gr.touch t, tx, ty : tt = (time() - now) / 1000 : until ! t | tt > 1
                tx /= scale_x : ty /= scale_y
                gr.modify collision, "x", tx, "y", ty : gr.render
                                
                if gr_collision(collision, animal) & tt <= outTime then
                   
                    List.remove lostAnimals, animalNum
                    call BringToFront(animal)
                    gr.modify animal, "x", di_width - 50, "y", saved_y 
                    score += 10 : saved_y += 40
                    gr.modify showScore, "text", "Score: " + int$(score)
                    call PlaySound(sound, saved)
                    
                else
                    call PlaySound(sound, opps)
                    gr.modify animal, "x", - 50, "y", - 50
                endif
                
                gr.render
                List.size lostAnimals, listSize
               
            until listSize = 0 | tock >= gameTime
            if tock < gameTime then  gosub WickedHunters : gosub SetUp
            if outTime > 0.05 then outTime -= 0.50
        until tock >= gameTime
        gosub NewGame
    until 0
return

!------------------------------------------------
! Wicked hunters fire on the animal and frighten them away
!------------------------------------------------
WickedHunters:
    for s = 1 to 15
        x = floor(rnd() * (di_width - 100))
        y = floor(rnd() * (di_height - 100))
        gr.modify shot, "x", x, "y", y
        call PlaySound(sound, explode)
        gr.render : pause 10
    next
    gr.modify shot, "x", - 50, "y", - 50 : gr.render
    pause 100
return

!------------------------------------------------
! Game over. Update beat score if need be and start a new game or end
!------------------------------------------------
NewGame:
    outTime = 2
    m$="Another game?"
    if score > best then 
        call PlaySound(sound, wow)
        m$ = "A new best score\nAnother game" 
        best = score 
        gr.modify showBest, "text", "Best: " + int$(best)
        gr.render
    endif
    do : dialog.message "Time Up",m$,yn,"Yes","No" : until yn > 0
    if yn = 2 then gosub GoodBye
    score = 0 : gameTime = 60
    list.clear lostAnimals
    gosub SetUp
return

!------------------------------------------------
! Display an animal at a random position 
!------------------------------------------------
ShowLostAnimal:
    list.size lostAnimals, listSize
    animalNum = floor(rnd() * listSize) + 1
    List.get lostAnimals, animalNum, animal
    x = floor(rnd() * (di_width - 100))
    y = floor(rnd() * (di_height - 100))
    gr.modify animal, "x", x, "y", y
    gr.render
return

!------------------------------------------------
! Display Help
!------------------------------------------------
Help:
    for y = - helpHeight to 50 step 10 
        gr.modify help, "y", y
        gr.render
    next
    do : gr.touch t, tx, ty : pause 1 : until t
    do : gr.touch t, tx, ty : pause 1 : until ! t
    for y = 50 to - helpHeight - 20 step - 10 
        gr.modify help, "y", y
        gr.render
    next
return

!------------------------------------------------
! Close the game
!------------------------------------------------
GoodBye:
    gosub SaveBest
    audio.stop
    list.size animalList, animalLength 
    call PlaySound(sound, goodbye)
    for x = 1 to animalLength
        list.get animalList, x, animal
        gr.modify animal, "x", (x * 75) - 75, "y", 200
    next
    WakeLock 5
   
    gr.color 255, 255, 255, 255, 1 
    gr.text.size 100: gr.text.align 2
    gr.text.draw null, di_width / 2, 400, "Thanks For Saving Us" 
    gr.text.draw null, di_width / 2, 600, "Good Bye" 
    gr.render
    pause 3000
    soundpool.release
    exit
return

!-------------------------------------------------
! Loads best score and sound
!-------------------------------------------------
LoadBest:
	dataPath$="../../SaveThem/data/"
	file.exists BestPresent,dataPath$+"SaveThemBest.txt"
	if BestPresent then
		text.open r,hs,dataPath$+"SaveThemBest.txt"
			text.readln hs,best$
			best = val(best$)
			
			text.readln hs,best$
			sound = val(best$)
		text.close hs
	endif
return

!-------------------------------------------------
! Saves best score and sound
!-------------------------------------------------
SaveBest:
	dataPath$="../../SaveThem/data/"
	file.exists BestPresent,dataPath$+"SaveThemBest.txt"
	if ! BestPresent then file.mkdir dataPath$
	text.open w,hs,dataPath$+"SaveThemBest.txt"
		text.writeln hs,int$(best)
		text.writeln hs,int$(sound)
	text.close hs
return

!-------------------------------------------------
! Let Basic see the Functions before they are called
!-------------------------------------------------
Functions:

!------------------------------------------------
! if sound is on. Play sound (ptr)
!------------------------------------------------
fn.def PlaySound(sound, ptr)
    if sound then Soundpool.play s,ptr,0.99,0.99,1,0,1
fn.end

!-------------------------------------------------
! If sound is on then play the background sound in a loop
!-------------------------------------------------
fn.def LoopSound(sound, paintBlack)
	if sound then 
		audio.play paintBlack
		audio.loop
	else 
		audio.stop
	endif
fn.end

!-------------------------------------------------
! Thanks to Gilles for this function
! Bring a bitmap to the front 
!-------------------------------------------------
Fn.def BringToFront( ptr )  % ptr is the pointer to become the last one into DL.
  gr.getdl ndl[],1
  array.length sz, ndl[]
  if !ptr | sz = 1 | ndl[sz] = ptr then array.delete ndl[] : Fn.rtn 0
 
  array.search ndl[],ptr,n
  if n
    if ndl[n] =ptr
      for nn=n to sz-1
        ndl[nn] = ndl[nn+1]
      next
      ndl[sz] = ptr
      GR.NEWDL ndl[]
    endif
  endif
  array.delete ndl[]
Fn.end

return
 