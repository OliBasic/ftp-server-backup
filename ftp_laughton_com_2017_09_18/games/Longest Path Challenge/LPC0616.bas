! LPC
DEBUG.ON
WantsToPlay=0.0
here100:
DO

GOSUB StartScreen


UNTIL WantsToPlay

Main:

Here1:
GOSUB Setup
GOSUB SaveStateArray
Here2:

GOSUB NewMainFlow

playerzTurn=1.0
programzTurn=0.0

pause 1000
gosub MainPipes
for I = 1 to playerzLongestPipe
  GR.SHOW rPS[I]
  GR.SHOW rPC[I]
next I
GR.RENDER
gr.color 255,0,255,255,1
gr.render

GR.RENDER
nRPS=playerzLongestPipe
GR.MODIFY tt2, "text","        "
GR.RENDER
IF lPSF-1>nRPS
  GR.MODIFY tt5,"text","You lose!"
ELSEIF lPSF-1<nRPS
  GR.MODIFY tt5,"text","You win!"
ELSE
  GR.MODIFY tt5,"text","Tie!!!"
ENDIF


GR.MODIFY tt8, "text", "                 Continue"
gr.show tt8

GR.RENDER

DO
  GR.TOUCH touched,x,yy
UNTIL touched

GR.HIDE tt8
GR.HIDE tt4

 gr.hide tt5

GR.RENDER

FOR I= 1 TO playerzLongestPipe
  GR.HIDE rPS[I]
  GR.HIDE rPC[I]
NEXT I
GR.RENDER

FOR R=1 TO lPSF
  GR.HIDE cPS[R]
  GR.HIDE clpc[R]
NEXT
GR.RENDER

GOSUB PlayAgain



IF pA=2

   GR.CLOSE
   END
ELSEIF pA=1


   GR.MODIFY tt1,"text"," 00.0 sec"

   GR.MODIFY tt3,"text","               "
   GR.MODIFY tt5,"text","            "
   gr.modify tt4,"text","            "
   gr.hide tt7




   IF sS=1
     GOSUB Repeatable
     GOSUB ResetStateArray
     GR.SHOW tt4
     GR.SHOW tt5

     GR.RENDER

     GOTO Here2
   ELSE

      gr.cls
      gr.render

      gosub RFNG

     GR.CLOSE
     GOTO HERE1
   ENDIF
ENDIF
Skip2Here:

GR.CLOSE
END

Setup:
GOSUB Gridsize


D=side
OD=D+2
N=OD*OD
GOSUB ObstacleSetup
! print numberObstacles



GR.OPEN 255,0,0,0,0,1
GR.SCREEN w,h
DIM S[N]
DIM xLocSq[N]
DIM yLocSq[N]
DIM crcl[N]
DIM longestPipe[N]
DIM cSA[N]
DIM newSq[N]
DIM D[8]
DIM redCrcl[2]
DIM rPS[N]
DIM rPC[N]
DIM sRPC[500]
DIM redCirclesAt[2]
DIM StateArray[N]
DIM cPS[N]
DIM clpc[N]
BL=0
BT=0
BR=w
BB=w
rF=w/800
oldX=1 
oldY=1
cS=1
nRPS=0.0

cellW=(BR-BL)/D
cellH=cellW
borderW=cellW

GOSUB Repeatable

GOSUB CalculateSqCenters
GOSUB SetInitialBoard
GR.TEXT.SIZE w/15
GOSUB BoardSetup
GOSUB DrawGrid
GOSUB AssignPipesArrayVariables
GR.COLOR 255, 0,0,255,1
GR.RENDER
GOSUB DrawGrid
gr.color 170,255,0,0,1
GR.RENDER



GR.RECT rct2, BL+w/40,11*h/12,BR-w/40,h

GR.SET.STROKE w/80

GR.COLOR 255,0,255,5,0

GR.RECT rct3000, BL+w/80, 11*h/12, BR-w/80, h-w/80

gr.color 255,0,255,0,1

gr.text.size w/15

gr.set.stroke 1
gr.render

GR.TEXT.DRAW tt8, w/10, 11.7*h/12," "

gr.render
GOSUB DrawBigCircle
GOSUB CreateRedCircles
GR.RENDER

gr.front 1
gr.render
GR.MODIFY tt1,"text", "Tap start square "
GR.TEXT.DRAW tt2, w/16,9.5*h/12,"        "
GR.TEXT.DRAW tt3, w/16,10.5*h/12,"   "
GR.RENDER
GR.COLOR 255,0,255,0,1
GR.RENDER
GOSUB DrawGrid
IF randomOrManual = 1.0
    gosub graphicalStartSquare
    gosub randomobstaclesetup
ENDIF
IF randomOrManual = 2.0
    GOSUB GraphicalObstacleSetup
ENDIF
GOSUB SaveStateArray

gosub DrawPlayAgain
RETURN


NewMainFlow:
playerzTurn=1.0

n1=0
 TIMER.SET 1000

DO
GR.SHOW tt7
GR.RENDER
DO 
   GR.TOUCH touched, x,y
UNTIL touched
GR.MODIFY tt1,"text", " "
GR.RENDER


IFy>(10*h/12)
  tPlayer=n1
  timer.clear
  gameOver=1.0
GR.HIDE tt7

gr.render

 GR.MODIFY tt5, "text",STR$(n1)+" secs"
  GR.MODIFY tt4, "text", STR$(nRPS)+" length"

  GR.RENDER
pause 3000
  cS=startSq
  playerzLongestPipe=nRPS
  gosub EraseRedPipeWhenTouchDownOnIt
  gr.render
GOTO GameOver

ENDIF


oldX=x
oldY=y
sqX=FLOOR(x/cellW)+1
sqY=FLOOR(y/cellW)+1
cS=sqX+1 +sqY*OD
IF cS=startSq
 gr.color 88,255,0,0,1
 gr.show crl1
 GR.RENDER
ENDIF
IF cS = startSq 
  IF nRPS > 0.0
    FOR I = 1 TO nRPS
      GR.MODIFY rPS[I], "x1", w+100
      GR.MODIFY rPS[I], "x2", w+200
      GR.MODIFY rPC[I], "x", w+100
      S[sRPC[I]]=0.0
    NEXT  I

    GR.RENDER
    nRPS =0.0
    GOSUB EraseRedPipeWhenTouchDownOnIt
    gosub restoreStateCircles
  ENDIF
ELSEIF S[cS]>1000

  gosub EraseRedPipeWhenTouchDownOnIt
ELSEIF S[cS]<1000
  sessionOver=1.0
ENDIF
DO
DO
 GR.TOUCH touched, x,y
 IF !touched
   GR.HIDE crl1
   sessionOver=1.0
   D_U.BREAK
 ENDIF  
UNTIL touched  
sqCol=FLOOR(x/cellW)+1
sqRow=FLOOR(y/cellW)+1
newSq= sqCol+1+sqRow*OD
IF newSq<>cS & newSq<>startSq
  GOSUB CheckSq2
  GR.MODIFY crl1, "x", x
  GR.MODIFY crl1, "y", y
  GR.RENDER
ELSEIF newSq=cS
  GR.MODIFY crl1, "x", x
  GR.MODIFY crl1, "y", y
  GR.RENDER
  cS=newSq
ENDIF
oldX=x
oldY=y
UNTIL sessionOver=1.0
sessionOver=0.0

GameOver:
UNTIL gameOver=1.0
TIMER.CLEAR
RETURN

CheckSq2:
IF ( S[newSq]=11.0)  | ( S[newSq]=99.0)
 x=oldX
 y= oldY
ELSEIF S[cS]=0
 x=oldX
 y=oldY
ELSEIF S[newSq]>1000
 GOSUB EraseRedPipeWhenRedRunIntoIt
 oldX=x
 oldY=y
 cS= newSq
ELSEIF ABS(newSq-cS)<>1 & ABS(newSq-cs)<>OD
x=oldX
y=oldY
ELSE
 GR.COLOR 255,red,green,blue,1
 GR.SET.STROKE cirradius*2*(D-2)/D
 GOSUB DrawPipe
 cS=newSq
 oldX=x
 oldY=y
ENDIF
RETURN

DrawBigCircle:
GR.COLOR 88, 200,green,blue,1
GR.RENDER
GR.CIRCLE crl1, BL+(D-1)*cellW/2, BT+(D-1)*cellW+cellW/2, cellW/2
Iteration = Iteration + 1
GR.HIDE crl1
GR.RENDER
RETURN

DrawPipe:
gr.color 255,255,0,0,1
gr.render

 nRPS= nRPS+1
 GR.LINE rPS[nRPS], xLocSq[cS],yLocSq[cS],xLocSq[newSq],yLocSq[newSq]
 GR.CIRCLE rPC[nRPS],xLocSq[newSq],yLocSq[newSq],cellW/4
 S[newSq]=1000+nRPS
 !GR.MODIFY tt5, "text",Str$(tPlayer)+" seconds"
 GR.MODIFY tt4, "text",STR$(nRPS)
 sRPC[nRPS]=newSq
gr.render


RETURN

EraseRedPipeWhenTouchDownOnIt:
pSN=S[cS]-1000
FOR I = pSN+1 TO nRPS
 S[sRPC[I]]=0.0
 GR.HIDE rPS[I]
 GR.HIDE rPC[I]
NEXT I
GR.RENDER
nRPS=pSN



RETURN

EraseRedPipeWhenRedRunIntoIt:
pSN=S[newSq]-1000
FOR I=pSN+1 TO nRPS
 S[sRPC[I]]=0.0
 GR.MODIFY rPS[I],"x1",w+100
 GR.MODIFY rPS[I],"x2",w+200
 GR.MODIFY rPC[I],"x",w+100
NEXT I
GR.RENDER
nRPS=pSN
GOSUB RestoreStateCircles

!attempt
gr.modify tt5, "text", Str$(nRPS)+" 2 "

RETURN

Repeatable:
GOSUB ResetStateArray
gameOver=0.0
need2Return=0.0
goneBackOnce=0.0
cs=startSq
L=1.0
cPL=1.0
LPSF=0.0
obsDone=0.0



RETURN

ResetStateArray:
FOR K=1 TO N
 S[K]=StateArray[K]
NEXT

RETURN

RestoreStateCircles:
 S[startSq]=1000
RETURN

SaveStateArray:
FOR K=1 TO N
  StateArray[K]=S[K]
NEXT
RETURN



SessionOver:
GR.HIDE crl1
GR.RENDER
RETURN


MainPipes:
playerzTurn=0.0

gr.hide tt7
gr.render
cSA[L]= startSq
n1=0
TIMER.SET 1000
LOOP1:
GOSUB Recursion
Total=Total + cPL
attempts=attempts+1
GOSUB ResetPosition
IF n1<tPlayer
  GOTO LOOP1
ENDIF
TIMER.CLEAR
pause 1000
GOSUB DisplayLongestPipe


AssignPipesArrayVariables:
D[1]=-OD
D[2]=1
D[3]=OD
D[4]=-1
D[5]=-OD
D[6]=1
D[7]=OD
D[8]=-1
RETURN

BoardSetup:
GR.COLOR 255,200,200,200,0
GR.RECT rtc1,BL,BT,BR,BB
GR.COLOR 128,0,255,255,1
GR.RECT rct2, BL,BB,BR,h
GR.COLOR 255,200,200,200,1
cirradius= cellW/4
!GR.TEXT.DRAW tt1, w/16,w+100*F,"Please wait . . ."
GR.TEXT.DRAW tt1, w/16, 8.5*h/12,"Please wait . . ."


GR.TEXT.DRAW tt4, w*5/8, h*10.5/12, " "
GR.TEXT.DRAW tt5, w*5/8, h*8.5/12, " "

GR.TEXT.DRAW tt7, w/16, 11.5*h/12, " "


gr.render
RETURN

CalculateSqCenters:
for row=1 to OD
  for col =1 to OD
   I= col+ OD*( row-1)
   xlocsq[I]= BL+ cellW/2+cellW*(col-2)
   ylocsq[I]= BT+cellW/2+ cellW*( row-2)
  next col
next 
RETURN

CreateRedCircles:
gr.color 255,0,255,0,1
FOR I=1 TO N
GR.CIRCLE crcl[I],xlocsq[I], ylocsq[I],cirradius
gr.HIDE crcl[I]
GR.RENDER
NEXT I
RETURN

DisplayLongestPipe:
GR.RENDER
 GR.set.stroke cellW/5
GR.COLOR 255,255,0,255,1
FOR R=1 TO lPSF-1
 GR.LINE cPS[R], xLocSq[LongestPipe[R]],yLocSq[LongestPipe[R]],xLocSq[LongestPipe[R+1]],yLocSq[LongestPipe[R+1]]
  GR.CIRCLE clpc[R], xLocSq[LongestPipe[R+1]], yLocSq[LongestPipe[R+1]],cellW/12

NEXT R
GR.RENDER
pause 1000
RETURN

DrawGrid:
GR.SET.STROKE 1
GR.RENDER
FOR I =1 TO D-1
   GR.LINE ln1, BL+I*cellW,BT,BL+I*cellW, BB % -cellW
NEXT I


FOR I=1 TO D-1
 GR.LINE ln2,BL,BT+ I*cellH,BR,I* cellH+ BT
NEXT I
RETURN

GoBackOnce:
S[cSA[L]]=2.0
storedValue =cSA[L]
GR.HIDE crcl[cSA[L]]
GR.RENDER
L=L-1
cPL=L
goneBackOnce=1.0
RETURN


GraphicalObstacleSetup:
GR.COLOR 15,255,0,0,1
cellW=(BR-BL)/D
 GR.RECT rtc2,BL, 11*h/12,BR,h
GR.RENDER
GOSUB GraphicalStartSquare

GR.MODIFY tt1 , "text", "Now place obstacles"
gr.modify tt8, "text" ,"Tap green letters when done"
GR.RENDER
GR.COLOR 255,30,150, 240,1
GR.RENDER
DO
DO
  GR.TOUCH touched,x,y
UNTIL touched
IF y> w
  gr.color 255,255,0,0,1
  gr.hide tt8

  gr.render
  obsDone=1
  GR.MODIFY tt1, "text", " Begin drawing! "
  gr.modify tt3, "text","                          "
gr.color 255,255,255,255,1
 gr.text.draw tt7, w/16, 11.5*h/12,"Tap here when done"
GR.SHOW tt7
  gr.render
  D_u.break
ENDIF
obstSqX=FLOOR(x/cellW)+1
obstSqY=FLOOR(y/cellW)+1
OBSQ=obstSqX+1+obstSqY*OD
if S[OBSQ]=0.0
 S[OBSQ]=11
 GR.CIRCLE crl7, xLocSq[OBSQ], yLocSq[OBSQ],cirRadius
 GR.RENDER
endif
UNTIL obsDone
gr.render
 GR.COLOR 255,30,150, 255,1
GR.RENDER
pause 500

RETURN


GraphicalStartSquare:
OD=D+2
GR.COLOR 255,240,140,0,1
gr.RENDER
DO
  GR.TOUCH touched,x,y
UNTIL touched
startSqX=FLOOR(x/cellW)+1
startSqY=FLOOR(y/cellW)+1
adjustedX=startSqX+1
adjustedY=startSqY+1
startSq=adjustedX+(adjustedY-1)*OD
S[startSq]=1000
GR.CIRCLE crcl6, xLocSq[startSq], yLocSq[startSq], cirRadius
GR.RENDER
PAUSE 500
gr.color 255,0,255,0,1
gr.render
gosub drawGrid
RETURN

! HideCLongestPipe
FOR R =1 TO N
  GR.HIDE cPS[R]
NEXT
RETURN

Recursion:
IF need2return=1.0
  RETURN
ENDIF
di=FLOOR(RND()*4)+1
FOR I = di TO di+3
newSq[L]=cSA[L]+D[I]
IF S[newSq[L]]=0.0
  L=L+1
  cSA[L]=newSq[L-1]
  cPL=L
  IF cPL>lPSF
    lPSF=cPL
    GR.MODIFY tt3, "text",STR$(lPSF-1)+" longest"
    FOR J=1 TO lPSF
      LongestPipe[J]=cSA[J]
    NEXT J


    IF lPSF>playerzLongestPipe+1 & n1>=1 THEN n1=tPlayer
  ENDIF
  S[cSA[L]]=1000.0
  GR.SHOW crcl[cSA[L]]
  GR.RENDER
  GOSUB Recursion
ENDIF
NEXT I
IF goneBackOnce=0.0
  GOSUB GoBackOnce
  GOSUB Recursion
ENDIF
need2return=1.0
RETURN

ResetPosition:
L=1.0
cSA[L]=startSq
pipeLength=cPL
GR.MODIFY tt2, "text",STR$(pipeLength-1)+" recent  "
cPL=1
FOR II = 2 TO pipeLength
  GR.HIDE crcl[cSA[II]]
  S[cSA[II]]=0.0
NEXT II
GR.RENDER
need2return=0.0
goneBackOnce=0.0
S[storedValue]=0.0
RETURN

SetInitialBoard:
FOR I =1 TO N
  S[I]=0
NEXT I
FOR I=1 TO OD
  S[I]=99
  S[I+(OD-1)*OD]=99
NEXT I
FOR I=1 TO OD*(OD-1)+1 STEP OD
  S[I]=99
  S[I+OD-1]=99
NEXT I
RETURN


ON TIMER:
n1=n1+1

IF playerzTurn=1.0
   GR.MODIFY tt5, "text", STR$(n1)+" secs"
ELSE
   GR.MODIFY tt1,"text",STR$(n1)+" secs"
ENDIF

GR.RENDER
TIMER.RESUME


RandomObstacleSetup:
GOSUB DrawGrid
GR.COLOR 255,30,150,255,1
GR.RENDER
DO
  RS=FLOOR(RND()*N)+1
  IF S[RS]=0.0 & ABS(RS-startSq)<>OD
    S[RS]=11.0
    count=count+1.0
    GR.CIRCLE crl8, xLocSq[RS], yLocSq[RS], cirRadius
    gr.render
  ELSE
    S[RS]=S[RS]
  ENDIF
UNTIL Count=numberObstacles
count=0.0
GR.MODIFY tt1,"text", "Begin drawing!"
gr.color 255,255,255,255,1
 gr.text.draw tt7,w/8, 11.5* h/12,"Tap here when done."
GR.SHOW tt7
gr.render
pause 500
RETURN

RFNG:
UNDIM S[]
UNDIM xLocSq[]
UNDIM yLocSq[]
UNDIM crcl[]
UNDIM longestPipe[]
UNDIM cSA[]
UNDIM newSq[]
UNDIM D[]
UNDIM redCrcl[]
UNDIM rPS[]
UNDIM rPC[]
UNDIM sRPC[]
UNDIM redCirclesAt[]
UNDIM StateArray[]
UNDIM cPS[]
UNDIM clpc[]

UNDIM ChsX[]
UNDIM ChsY[]
UNDIM Chs$[]
UNDIM chs[]


RETURN

!-------------------------------------------
GridSize:
REM Start of BASIC! Program
GR.OPEN 250,0,0,100,0,1
GR.SCREEN w,h
GR.COLOR 255,255,255,0,1

GR.TEXT.SIZE w/16
GR.SET.STROKE w/80

GR.TEXT.SKEW -0.25
GR.TEXT.DRAW TTT1,w/3,h/7, "Board size?"
GR.RENDER


!draw the lines
FOR I =1 TO 5
  GR.LINE LNE1, w/5, (I+1)*h/7,4*w/5,(I+1)*h/7
NEXT I

FOR I=1 TO 4
  GR.LINE LNE2, I*w/5,2*h/7,I*w/5, 6*h/7
NEXT I

! Arrays
DIM ChsX[4,7]
DIM ChsY[4,7]
DIM Chs$[4,7]
DIM chs[4,7]

Chs$[1,2]="4 x 4"
Chs$[2,2]="5 x 5"
Chs$[3,2]="6 x 6"
Chs$[1,3]="7 x 7"
Chs$[2,3]="8 x 8"
Chs$[3,3]="9 x 9"
Chs$[1,4]="10x10"
Chs$[2,4]="11x11"
Chs$[3,4]="12x12"
Chs$[1,5]="13x13" 
Chs$[2,5]="14x14"
Chs$[3,5]="15x15"

deltaY=w/40

!calculate choices locations put delta in here
FOR col =1 TO 4
  FOR row = 2 TO 6
    IF row < 4
      deltaX=w/14
    ELSEIF row>3
      deltaX = w/11
    
    ENDIF
    chsX[col,row]=(2*col+1)*w/10-deltaX
    chsY[col,row]=(2*row +1)*h/14+deltaY

  NEXT row
NEXT col

!place choices

FOR I=1 TO 3
  FOR J = 2 TO 5
gr.text.draw chs[I,J], chsX[I,J], chsY[I,J], chs$[I,J]
  NEXT J
  gr.render
NEXT I

GR.RENDER

DO
GR.TOUCH touched, x,y
UNTIL touched

col=FLOOR(5*x/w)
row=FLOOR(7*y/h)-1

row$=STR$(row)
col$=STR$(col)

side=col+3*(row-1)+3

GR.RENDER
pause 500
gr.cls

pause 500
RETURN

!--------------------------------------
ObstacleSetup:
REM Start of BASIC! Program
GR.SCREEN w,h
lineSpacing=h/20
f=w/800
numberSpacing = w/10

!make background red
GR.COLOR 255,255,255,0,1
!draw rectangle red right half screen
GR.RECT ll1,0,0,w/2,h

gr.text.size w/16 % 50 %           w/16
halfTextSize=w/32

!text.size determines number and letter size
!large letters close to marked line 
gr.text.underline 1.0
GR.TEXT.DRAW tt20, w*9/16,h/24,"Human setup"
gr.text.underline 0.0
GR.TEXT.draw tt22, 55*w/100,h/8,"Player"
GR.TEXT.DRAW tt23, 55*w/100,h/8+lineSpacing,"decides"
GR.TEXT.DRAW tt24, 55*w/100,h/8+2*lineSpacing,"number and"
GR.TEXT.DRAW tt25, 55*w/100,h/8+3*lineSpacing,"placement of"
GR.TEXT.DRAW tt26,55*w/100,h/8+4*lineSpacing,"obstacles"
GR.TEXT.DRAW tt27,55*w/100,h/8+6*lineSpacing,"Tap this side."
GR.TEXT.DRAW tt28,55*w/100,h/8+7*lineSpacing,"Follow on-"
GR.TEXT.DRAW tt29,55*w/100,h/8+8*lineSpacing,"screen instruc-"
GR.TEXT.DRAW tt21,55*w/100,h/8+9*lineSpacing,"tions.

!set.stroke sets width of lines
GR.Set.stroke w/64

GR.COLOR 255,0,0,10,1
GR.RENDER
GR.LINE pln1, w/4,h/4,w/4,11*h/12
GR.LINE topln1, w/8,h/4,3*w/8,h/4
GR.LINE botmlin2, w/8,11*h/12,3*w/8,11*h/12


!experimental
gr.text.underline 1.0
gr.text.draw tt41,w/16,h/24,"Random setup"
gr.text.underline 0.0
gr.text.draw tt42, h/24,h/8,"Select % of grid"
!GR.TEXT.DRAW tt42, h/24,w/16+ 1.5*lineSpacing,"Select % of grid"
GR.TEXT.DRAW tt43, h/24,h/8+1*lineSpacing,"to be covered"
GR.TEXT.DRAW tt44, h/24,h/8+2*lineSpacing,"by obstacles."

gr.set.stroke 1
gr.text.size w/16

gr.render


! thin lines
GR.SET.STROKE 1
FOR I = 22 TO 100 STEP 2
  GR.LINE ln3, 2*w/10,h/4+I*h/150,3*w/10,h/4+I*h/150
NEXT I

!thick lines
GR.SET.STROKE 5*f
FOR I=0 TO 100 STEP 10
  GR.LINE ln4,h/8,h/4+I*h/150,h/8+w/8,h/4+I*h/150
NEXT I

gr.set.stroke 1

!numbers
startY= 3*h/10   %  h/4+w/48

! start drawing numbers.

GR.TEXT.DRAW tt38, w/8, startY+2*numberSpacing,"70"
GR.TEXT.DRAW tt37, w/8, startY+3*numberSpacing,"60"
GR.TEXT.DRAW tt36, w/8, startY+4*numberSpacing,"50"

GR.TEXT.DRAW tt35, w/8, startY+5*numberSpacing,"40"
GR.TEXT.DRAW tt34, w/8, startY+6*+numberSpacing, "30"
GR.TEXT.DRAW tt33, w/8, startY+7* numberSpacing,"20"
GR.TEXT.DRAW tt32, w/8, startY+8*numberSpacing,"10"



GR.TEXT.DRAW tt30,w/12,startY+10*numberSpacing,"Surprise me"
GR.TEXT.DRAW tt31,w/8, startY+9*numberSpacing, "0"

GR.RENDER



DO 
  GR.TOUCH touched,x,y
UNTIL touched


IF x>=w/2
  randomOrManual=2
!  PRINT randomOrManual
ELSE
  randomOrManual=1.0
  percentObstacles=100-FLOOR(10*(y-h/4)/numberSpacing)

! print "percent obst=" ; percentObstacles
  IF percentObstacles>80 THEN percentObstacles=80
  IF percentObstacles < 0.0 THEN percentObstacles=RND()*35+15


ENDIF

!now must multiply percentObstacles*N to get numberObstacles
numberObstacles=FLOOR(percentObstacles*D*D/100)

GR.CLOSE

RETURN

! ----------------------------------------------
PlayAgain:

GR.SHOW RCT1
GR.SHOW RCT2
GR.SHOW RCT3
GR.SHOW RCT4
GR.SHOW TT33
GR.SHOW TT34
GR.SHOW TT340
GR.SHOW TT35
GR.SHOW TT36
GR.SHOW TT37
GR.SHOW TT38
gr.render

pause 500
DO
  GR.TOUCH touched,x,y
UNTIL touched



IF x>w/2  
  pA=2
ELSEIF x<w/2
  pA=1
  IF y<h/2
    sS=1
  ELSE
    sS=2
  ENDIF
ENDIF


GR.HIDE RCT1
GR.HIDE RCT2
GR.HIDE RCT3
GR.HIDE RCT4
GR.HIDE TT33
GR.HIDE TT34
GR.HIDE TT340
GR.HIDE TT35
GR.HIDE TT36
GR.HIDE TT37
GR.HIDE TT38
gr.render
RETURN

!-------------------------------------------

DrawPlayAgain:

GR.COLOR 255,155,0,0,1
GR.RECT RCT1,0,0,w,h

GR.COLOR 255,0,0,255,1
GR.RECT RCT2,0,0,w/2,h

gr.color 255,255,255,0,1
gr.rect rct3, 0,0,w,h/8

GR.COLOR 255,0,0,125,1
GR.RECT rct4,0,9*h/16,w/2,h

GR.COLOR 255,255,0,0,1
GR.TEXT.SIZE h/10
GR.TEXT.SKEW -0.25
GR.TEXT.DRAW TT33, w/8, h/10, "Play again?"

GR.COLOR 255,255,255,255,1
GR.TEXT.SIZE h/20
GR.TEXT.DRAW TT34, 3.5*W/5,9*h/16, "No"
GR.TEXT.DRAW TT340, w/5, 3*h/16, "Yes"

!GR.TEXT.SIZE h/20

GR.COLOR 255,255,255,255,1
GR.TEXT.DRAW TT35, 1*w/6,h/3, "Same"
GR.TEXT.DRAW TT36, 1*w/6,h/3+numberSpacing, "setup"
GR.TEXT.DRAW TT37, 1*w/6,7*h/9,"New"
GR.TEXT.DRAW TT38,W/6,7*h/9+numberSpacing,"setup"

GR.HIDE RCT1
GR.HIDE RCT2
GR.HIDE RCT3
GR.HIDE RCT4
GR.HIDE TT33
GR.HIDE TT34
GR.HIDE TT340
GR.HIDE TT35
GR.HIDE TT36
GR.HIDE TT37
GR.HIDE TT38
GR.RENDER

RETURN


! ------------------------------------------------

StartScreen:
REM Start of BASIC! Program
GR.OPEN 255,8,12,18,0,1
GR.SCREEN w,h
titleLine =h/8
titleMargin=w/6
deltaY=h/10
lineSpacing=h/10
leftMargin=w/2
topLine=h/2 +h/60
deltaX=-w/15
DIM startScreenLn[7]
DIM startScreenVar$[7]

DIM ssln[5]

I=1



gr.color 255,255,0,0,1
GR.RENDER
gr.set.stroke w/80

GR.RENDER


gr.text.size h/10
gr.text.draw startScreenLn[5],titleMargin,titleLine,"LONGEST"

gr.color 255,255,255,255,1
gr.text.draw startScreenLn[6],titleMargin+w/7,titleLine+w/6,"PATH"

gr.color 200,0,128,255,1
gr.text.draw startScreenLn[7],titleMargin-w/10, titleLine+w/3,"CHALLENGE"
GR.RENDER

gr.color 125, 255,255,0,0
gr.rect rct7, 0,0,w,3*w/5
gr.render



GR.COLOR 128,0,255,0,1
GR.OVAL ovl1, 3*w/8, 11*h/24, 5*w/8, 13*h/24
! GR.RENDER


GR.COLOR 255,255,255,200,1
GR.TEXT.SIZE h/20
gr.render


startScreenVar$[1]="Play "
startScreenVar$[2]="Introduction"
startScreenVar$[3]="Directions"
startScreenVar$[4]="About"
startScreenVar$[5]="Exit"


FOR I = 1 TO 4
IF I=2 THEN deltaX=-w/5
IF I=3 THEN deltaX=-13*w/80
IF I=4 THEN deltaX=-w/10

GR.TEXT.DRAW startScreenLn[I], leftMargin+deltaX, topline+(I-1)*lineSpacing,startScreenVar$[I]
NEXT I
GR.RENDER

GR.TEXT.DRAW ssln[5],44*w/100,11.7*h/12,startScreenVar$[5]

GR.RENDER


DO
  GR.TOUCH touched,x,y
UNTIL touched
GR.CLOSE


IF y>topLine+lineSpacing+h/40-h/12 & y<topLine+lineSpacing+h/40



  GOSUB Introduction


ELSEIF y>topLine+2*lineSpacing+h/40-h/12 & y<topLine+2*lineSpacing+h/40

  GOSUB Directions


ELSEIF y>topLine+2*lineSpacing & y < topLine+ 3*lineSpacing


  GOSUB About

ELSEIF y> TopLine+ 3*lineSpacing
  END


ELSE 
  WantsToPlay=1
  RETURN

ENDIF

here200:
! print y

UNDIM startScreenLn[]
UNDIM startScreenVar$[]

UNDIM ssln[]


!GR.CLOSE
RETURN

!----------------------------------------------------
About:

gr.open 255,8,12,18,0,1
gr.screen w,h
gr.text.size h/30
gr.render
gr.rect rct10,0,0,w,3*w/5
gr.render
gr.color 255,255,255,255,1

gr.text.draw ab1, w/30,h/30,"About"
titleLine=h/8
titleMargin=w/6
gr.text.size h/10
gr.color 255,255,0,0,1
gr.text.draw ab2, titleMargin, titleLine,"LONGEST"
gr.color 255,255,255,255,1
gr.text.draw ab3,titleMargin+w/7, titleLine+w/6, "PATH"
gr.color 250,0, 120,205,1
gr.text.draw ab4,titleMargin-w/10,titleLine+w/3,"CHALLENGE"



gr.render

gr.color 125,255,255,0,0
gr.rect rct8,0,0,w,3*w/5
gr.color 120,0,175,200,1 % 190,90,0,1
gr.rect rct9,0,3*w/5,w,h
gr.render
gr.color 255,255,255,255,1
gr.text.size h/25
gr.text.draw A1, w/15,4.5*h/10, "Written by Tom Hatcher in BASIC!."
gr.text.draw About1, w/15,5*h/10, "BASIC!, or RFO Basic, is a free app" 

gr.text.draw A3,w/15,5.5*h/10,"on Google Play. You can convert"

gr.text.draw A4,w/15,6.0*h/10,"your Basic! programs to Android"

gr.text.draw A5,w/15,6.5*h/10, "apps easily, without using Java"
gr.text.draw A6, w/15,7.0*h/10,"or mastering Eclipse."

gr.text.draw A7, w/15,8.0*h/10,"Send comments, feedback,"
gr.text.draw A8, w/15, 8.5*h/10, "questions etc., to:"
gr.text.draw A9, w/15, 9.0*h/10, "computerchesstom@gmail.com"
gr.text.draw A10, w/15, 9.75* h/10,"                  Continue"
gr.render
gr.touch touched,x,y
gr.render
gr.set.stroke w/40

gr.color 255,200,200,40,1

gr.circle crl1, 7*w/8, h/12,w/70
gr.render

gr.color 255,255,0,0,1
gr.render


! line 1
FOR I=w/8 TO 3*h/12  STEP 5

  GR.LINE LN1,7*w/8,I,7*w/8,I+10

  GR.TOUCH touched, x,y

  IF touched=1.0 THEN F_n.break

  gr.render

NEXT I

IF touched =1.0 THEN GOTO here500

gr.touch touched,x,y

!line 2
FOR I = 7*w/8 TO 15.37*w/16 STEP 5


  GR.LINE LN2,I,h/4,I+10,h/4
  GR.TOUCH touched, x,y
  IF touched=1.0 THEN F_n.break

  GR.RENDER

NEXT I

IF touched= 1.0 THEN GOTO here500

gr.touch touched,x,y


!line 3
FOR I=h/4 TO 6*h/16 STEP 5

  GR.LINE LN3,15.25*w/16, I,15.25*w/16,I+10
  GR.TOUCH touched,x,y
  If touched=1.0 THEN F_n.break

  Gr.render

NEXT I

IF touched= 1.0 THEN GOTO here500
  GR.TOUCH touched,x,y

! line 4
FOR I=15.25*w/16 TO 1*w/16-20 STEP-10

  GR.LINE LN4, I,3*h/8,I-10,3*h/8
  GR.TOUCH touched,x,y
  IF touched=1.0 THEN F_n.break

  GR.RENDER

NEXT I

IF touched =1.0 THEN GOTO here500

GR.TOUCH touched,x,y


!line 5
FOR I = 3*h/8 to h/4 STEP-5
  GR.LINE LN5,w/24,I,w/24,I-10
  GR.TOUCH touched, x,y
  IF touched=1.0 THEN F_n.break

  gr.render
NEXT I

IF touched = 1.0 THEN GOTO here500
GR.TOUCH touched,x,y

!line 6 right

FOR I = w/24 to 2.75* w/10 STEP 5

  GR.LINE LN6,I, h/4,I+10,h/4
  gr.touch touched,x,y
  IF touched =1.0 THEN F_n.break

  GR.RENDER

NEXT I

GR.CIRCLE CRL1,2.75*w/10,h/4,w/80
gr.render
IF touched = 1.0 THEN GOTO here500

gr.touch touched,x,y


!line 7 up
FOR I= h/4 TO h/6 STEP -5

  GR.LINE LN7, 2.75*w/10,I,2.75*w/10,I-10
  GR.TOUCH touched,x,y
  IF touched=1.0 THEN F_n.break
  GR.RENDER

NEXT I

IF touched = 1.0 THEN GOTO here500

GR.TOUCH touched,x,y


! Line 8 LEFT
FOR I = 2.75*w/10 TO w/24 STEP -10
  GR.LINE LN8, I,h/6,I-10,h/6
  GR.TOUCH touched,x,y
  IF touched=1.0 THEN F_n.break

  GR.RENDER

NEXT I

GR.CIRCLE CRL2,w/24,h/6,w/80
gr.render

GR.TOUCH touched,x,y

!LINE 9 UP

FOR I =h/6 TO h/15 STEP-10
  GR.LINE LN9, w/24,I,w/24,I-10
  GR.TOUCH touched,x,y
  IF touched=1.0 THEN F_n.break
  GR.RENDER

NEXT I

  GR.CIRCLE CRL3,w/24,h/15,w/80
  GR.RENDER

GR.TOUCH touched,x,y

!line10 right

FOR I = w/24 TO w/7 STEP 10
  GR.LINE LN10, I,h/15,I+10,h/15
  GR.TOUCH touched,x,y
  IF touched =1.0 THEN F_n.break
  GR.RENDER
NEXT I
GR.TOUCH touched,x,y



!line 11DOWN

FOR I = h/15 TO h/7.5 STEP 10

  GR.LINE LN11,w/7,I,w/7,I+10
  GR.TOUCH touched,x,y
  IF touched = 1.0 THEN F_n.break
  GR.RENDER
NEXT I
GR.TOUCH touched,x,y


!line12 LEFT
FOR I=w/7 TO w/10 STEP-10
  GR.LINE LN12,I,h/7.5,I-10,h/7.5
  GR.TOUCH touched,x,y
  IF touched =1.0 THEN F_N.BREAK
  GR.RENDER
NEXT I
GR.TOUCH touched,x,y

GR.CIRCLE CRL4,w/10.5,h/7.5,w/80
gr.render


!line 13
FOR I=h/7.5 TO h/10 STEP -10
  GR.LINE LN13,w/10.5,I,w/10.5,I-10
  GR.TOUCH touched,x,y
  IF touched=1.0 THEN F_n.break
  GR.RENDER
NEXT I

here500:
GR.RENDER
!goto end
do
   gr.touch touched,x,y
until touched


GR.CLOSE
RETURN
!-------------------------------------------------

Introduction:
REM Start of BASIC! Program

GR.OPEN 120,134,1,0,0,1


lineSpacing=h/20
firstLine=h/20
leftMargin=w/20
DIM A$[18]
DIM IntroLn[18]

FOR I =1 TO 17
  GR.TEXT.DRAW IntroLn[I], leftMargin, I*50, " 1"
  gr.render
  
NEXT I


A$[1]="This game pits the most powerful life-"
A$[2]="based intellect in the known universe,"
A$[3]="your brain, against this puny, pathetic,"
A$[4]="piddling little program, in which you"
A$[5]="and the program each try to find the"
A$[6]="longest continuous path on a playing"
A$[7]="field containing obstacles."
A$[9]="You can use you eyes, even pencil"
A$[10]="and paper. You decide the playing"
A$[11]="field size, the start square and the"
A$[12]="placement of obstacles. You even"
A$[13]="decide how long to search."
A$[14]=" "
A$[15]="The program searches randomly, but it"
A$[16]="searches fast. That's its only advantage."
A$[18]="                             Continue"


GR.COLOR 128,128,20,0,1
GR.RECT rct12,0,14.5*h/16,w,15.5*h/16
GR.RENDER


GR.COLOR 255,200,200,200,1
GR.TEXT.SIZE w/20
FOR I = 1 TO 18
  GR.TEXT.DRAW IntroLn[I], leftMargin, firstLine+I*lineSpacing, A$[I]
! gr.render
NEXT I
GR.RENDER

!PAUSE 5000
!make red rectangle at bottom

DO
  GR.TOUCH touched,x,y
  IF y<5*h/6 THEN touched=0.0
UNTIL touched


UNDIM A$[]
UNDIM  IntroLn[]

GR.CLOSE


RETURN
!----------------------------------------

Directions:

lineSpacing=h/20

GR.OPEN 120,0,0,0,0,1
GR.SCREEN w,h
lineSpacing = h/20

GR.COLOR 255,0,120,0,1
GR.RECT rec4,0,0,w,h
GR.RENDER

GR.COLOR 255,225,20,0,1
gr.render
GR.RECT rec5,0,10.8*h/12,w,h
gr.render
GR.TEXT.SIZE W/10

GR.COLOR 255,255,255,255,1

firstLine=h/12
leftMargin=w/20

DIM diA$[17]
DIM TT50[17]

GR.TEXT.DRAW TT50,leftMargin,firstLine,"DIRECTIONS"

gr.text.size w/20

gr.text.draw tt501,leftMargin+2*w/3, firstLine, "(general)"

GR.TEXT.SIZE w/20


DIM DirLn[17]

FOR I = 1 TO 17
   GR.TEXT.DRAW DirLn[I], leftMargin, firstLine+I* lineSpacing, " "

NEXT I



!DIM dirA$[17]
DIM diB$[17]

diA$[1]="The object is to draw the longest pipe,"
diA$[2]="trail, or path. You can go N, E, S, or W,"
diA$[3]="but not diagonally. You can't enter a"
diA$[4]="square containing an obstacle or go"
diA$[5]= "outside the border."
diA$[6]=" "
diA$[7]="To start over, tap your start square. If"
diA$[8]="you run into your pipe it erases"
diA$[9]="from where you run into it."
diA$[10]=" "
diA$[11]="Take as long as you want. After you're"
diA$[12]="done, the program will try to find a longer"
diA$[13]="path in the same amount of time that"
diA$[14]="you took."
diA$[15]=" "
diA$[16]="Please follow all on-screen instructions."

! make red?
diA$[17]="                           Continue"


FOR I = 1 TO 17
  GR.MODIFY DirLn[I], "text",diA$[I]
 ! GR.RENDER
NEXT I

gr.render

DO
  GR.TOUCH touched, x,y
  IF y<5*h/6 THEN touched= 0.0
UNTIL touched




!gr.modify TT67, "text", " "
gr.render

gr.modify TT501, "text","(specific)"
GR.RENDER

diB$[1]="First decide the playing field's size."


diB$[3]="Next, decide if you want the program to"
diB$[4]="place obstacles randomly around the"
diB$[5]="grid, and if so, how many. Or you"
diB$[6]="may manually arrange the obstacles,"
diB$[7]="leaving at least one open square."

diB$[9]="Choose a start square. The start square"
diB$[10]="is orange. Obstacles are blue."
diB$[12]="The on-screen instructions will tell"
diB$[13]="you what to do next. Good luck!"

diB$[17]="                           Continue"
FOR I = 1 TO 17
   GR.MODIFY DirLn[I],"text",diB$[I]
gr.render

NEXT I
! pause 500
gr.render
DO
  GR.TOUCH touched,x,y
  IF y<5*h/6 THEN touched = 0.0
UNTIL touched  


gr.render
gr.color 255,255,255,255,1
gr.render




GR.CLOSE
UNDIM diA$[]
UNDIM diB$[]
UNDIM TT50[]
UNDIM dirLn[]



RETURN
!----------------------------------------
