! TouchingColors multi grids by @Cassiope34 Nov 2014

Fn.def clr( cl$ )
  gr.color val(word$(cl$,1)), val(word$(cl$,2)), val(word$(cl$,3)), ~
           val(word$(cl$,4)), val(word$(cl$,5))
Fn.end

FN.DEF roundCornerRect (b, h, r)
 half_pi = 3.14159 / 2
 dphi    = half_pi / 8
 LIST.CREATE N, S1
 mx      = -b/2+r
 my      = -h/2+r
 FOR phi= 0 TO half_pi STEP dphi
  LIST.ADD s1, mx-COS(phi)*r, my-sin (phi)*r
 NEXT
 mx      = b/2-r
 my      = -h/2+r
 FOR phi= 0 TO half_pi STEP dphi
  LIST.ADD s1, mx+SIN(phi)*r, my- cos (phi)*r
 NEXT
 mx      = b/2-r
 my      = h/2-r
 FOR phi= 0 TO half_pi STEP dphi
  LIST.ADD s1, mx+COS(phi)*r, my+sin (phi)*r
 NEXT
 mx      = -b/2+r
 my      =  h/2-r
 FOR phi= 0 TO half_pi STEP dphi
  LIST.ADD s1, mx-SIN(phi)*r, my+cos (phi)*r
 NEXT
 FN.RTN s1
FN.END

Fn.def LastPtr( ptr )  % put the graphic pointer 'ptr' (so the bitmap) UP to all other.
  gr.getDL ndl[],1
  array.length sz, ndl[]
  if !ptr | sz=1 | ndl[sz] =ptr
    array.delete ndl[]
    Fn.rtn 0
  endif
  array.search ndl[],ptr,n
  if n
    for nn=n to sz-1
      ndl[nn] =ndl[nn+1]
    next
    ndl[sz] =ptr
    gr.newDL ndl[]
  endif
  array.delete ndl[]
Fn.end

File.Root tmp$
IstandAlone =Is_In("rfo-basic",tmp$)    % to know if it's an APK for the way to exit.

gr.open 255,163,163,163,0,1
gr.screen scrx,scry
svx =1600
svy =2560
sx  =scrx/svx
sy  =scry/svy
gr.scale sx,sy

ARRAY.LOAD diX[],0,1,0,-1,-1,1,-1,1  % 1 North  2 East  3 South  4 West  rest=diagonals
ARRAY.LOAD diY[],-1,0,1,0,-1,-1,1,1
Dim c$[6], cell[8,8], scm[10], scmt[5], grid$[5]
c$[1] ="255 255 0 0 1"    % rouge
c$[2] ="255 255 192 0 1"  % orange
c$[3] ="255 228 255 0 1"  % jaune
c$[4] ="255 24 255 0 1"   % vert
c$[5] ="255 0 240 255 1"  % bleu clair
c$[6] ="255 0 54 255 1"   % bleu
filesave$ ="TchColors.sav"

hsx =svx-140         % help size
hsy =hsx
gr.bitmap.create bhelp, hsx, hsy    % help bitmap creation
gr.bitmap.drawinto.start bhelp
gr.color 200,0,0,0,1
gr.rect nul,0,0,hsx,hsy
gr.color 255,255,255,0,1
gr.text.size 68
iln =100
gr.text.draw nul, hsx/2-80, iln, "H E L P"
gr.text.draw nul, 20, iln*3, "The goal is to fill the grid."
gr.text.draw nul, 20, iln*4, "To achieve this you have to choose the color" 
gr.text.draw nul, 20, iln*5, "  that the computer will put at the place of "
gr.text.draw nul, 20, iln*6, "  the black dot it chooses randomly among the "
gr.text.draw nul, 20, iln*7, "  empty cells.
gr.text.draw nul, 20, iln*9, "To change grid touch it's button."
gr.text.draw nul, 20, iln*10, "To select a new game touch a second time the"
gr.text.draw nul, 20, iln*11, "  grid button."
gr.text.draw nul, 20, iln*13, "Anyway you will find the grids as you have left."
gr.bitmap.drawinto.end

File.exists fe, filesave$
if fe
  gosub loadgame
  new2 =0
else
  for nc=4 to 8       % init grid$[]
    cc =min(nc,6)
    for c=1 to cc
      do
        cx =int(rnd()*nc)+1
        cy =int(rnd()*nc)+1
      until cell[cx,cy]=0
      cell[cx,cy] =c
    next
    gosub savegrid
  next
  new2 =1
  nc   =4   % 4,5,6,7,8
endif

New:         %  NEW GAME
gr.cls
nt  =0
fin =0
unDim cell[]
Dim cell[8,8]

sc =(svx-140)/nc     % cell size

sbx =svx-140         % grid background
sby =sbx
poly = roundCornerRect (sbx-10, sby-10, sc/2)
GR.SET.STROKE 6
GR.COLOR  140 ,100 ,150, 255, 1
GR.POLY  nul ,poly ,70+sbx/2-2 ,100+sby/2-2

gr.color 255,255,255,255,0     % grid white circles
gr.set.stroke 2
for y=1 to nc
  for x=1 to nc
    gr.circle nul, 70+x*sc-sc/2, 100+y*sc-sc/2, sc/2-12
  next
next

sbx =1000             % six colors fond
sby =660
pcx =(svx-sbx)/2
pcy =1880
poly = roundCornerRect (sbx-10, sby-10, 150)
GR.SET.STROKE 6
GR.COLOR  140 ,100 ,150 ,255, 1
GR.POLY  nul ,poly ,pcx+sbx/2-2 ,pcy+sby/2-2

c  =0
lg =sbx/3
for y=1 to 2       % draw six colors buttons
  for x=1 to 3
    c++
    clr(c$[c])
    gr.circle nul, pcx+x*lg-lg/2, pcy+y*lg-lg/2, lg/2-30
  next
next

gr.text.size 100
gr.text.align 2
ic =svx/5
poly = roundCornerRect (ic-30, 150, 30)
GR.SET.STROKE 4
gr.color 40, 0, 0, 200, 1
GR.POLY ptnc ,poly ,5+ic/2+(nc-4)*ic ,pcy-226
GR.COLOR 160 ,255 ,255, 255, 0
for cg=1 to 5
  GR.POLY nul, poly, 5+ic/2+(cg-1)*ic, pcy-226
  gr.text.draw nul, 5+ic/2+(cg-1)*ic, pcy-190, int$(cg+3)+"x"+int$(cg+3)
next

if new2     % install starting colors into new grid
  gosub newgrid
else        % charge grid$[nc]
  gosub loadgrid
endif

call clr(c$[2])
gr.text.draw mess, pcx/2, pcy+370, int$(nt)
gr.text.draw nul, pcx+sbx+pcx/2, pcy+370, "?"    % help button

gr.text.size 60
gr.color 255,0,255,255,1
for cg=1 to 5
  gr.text.draw scmt[cg], 5+ic/2+(cg-1)*ic, pcy-70, int$(scm[cg])+"/"+int$(scm[cg+5])
next

gr.color 255,0,0,0,1
gr.circle pt,0,0,20     % little black point

gr.bitmap.draw helpb, bhelp, 70, 100
gr.hide helpb

new2 =0
quit =0
new  =0
ok   =1

do
  if nt<nc*nc & !fin & ok & !help
    do
      cx =int(rnd()*nc)+1
      cy =int(rnd()*nc)+1
    until cell[cx,cy]=0
    gr.modify pt, "x", 70+cx*sc-sc/2, "y", 100+cy*sc-sc/2
  endif
  do
    gr.touch touched,x,y
    if !background() then gr.render
  until touched | quit
  if quit then d_u.break
  if help
    help =0
    gr.hide helpb
    d_u.continue
  endif

  do
    gr.touch touched,tx,ty
  until !touched
  tx/=sx
  ty/=sy
  
  if tx>pcx & tx<pcx+3*lg & ty>pcy & ty<pcy+2*lg & !fin & nt<nc*nc & !help
    cc =int((tx-pcx)/lg)+1
    if ty>pcy+lg then cc+=3
    call clr(c$[cc])
    gr.circle nul, 70+cx*sc-sc/2, 100+cy*sc-sc/2, sc/2-10
    cell[cx,cy] =cc
    nt++
    gr.modify mess,"text",int$(nt)
    gosub ctrl
    if !fin & nt=nc*nc  % win
      scm[nc-3]=nt
      scm[nc-3+5]=nt
    elseif fin          % lost
      scm[nc-3]=nt-1
      if scm[nc-3]>scm[nc-3+5] then scm[nc-3+5]=nt-1
    endif
    gr.modify scmt[nc-3], "text", int$(scm[nc-3])+"/"+int$(scm[nc-3+5])
    ok =1
    
  else
    if ty>svx-40 & ty<svx+150 & !help    % 5 grids buttons
      tnc =3+int(tx/ic)+1
      if tnc<>nc
        gosub savegrid
        nc =tnc
        gr.modify ptnc, "x", 5+ic/2+(nc-4)*ic        
        quit =1
        new  =1
      else
        dialog.message ,"New game ?",qok,"Yes","No"
        if qok=1
          quit =1
          new  =1
          new2 =1
        endif
      endif
      
    elseif tx>pcx+3*lg & ty>pcy & ty<pcy+2*lg    % help "?" touched
      if !help
        help =1
        Call LastPtr(helpb)
        gr.show helpb
      endif
      
    else
      gr.hide helpb
      help =0
      
    endif
    ok =0
  endif

until quit
if new then goto new
gosub savegrid
gosub savegame

if IstandAlone
  END "Bye...!"
else
  EXIT
endif      %  ---------------  exit  ---------------

onbackkey:
if help
  gr.hide helpb
  help =0
  Back.resume
endif
lastime =clock()
if lastime-thistime <1000
  quit =1
else
  quit =0
  thistime =lastime
  popup " Press a second time to quit. ",0,0,0
endif
Back.resume

ctrl:
for rc=1 to 8
  if cx+dix[rc]>0 & cx+dix[rc]<=nc & cy+diy[rc]>0 & cy+diy[rc]<=nc
    if cell[ cx+dix[rc], cy+diy[rc] ]=cc
      fin =rc
      f_n.break
    endif
  endif
next
if fin
  gr.color 255,255,255,255,1
  gr.circle nul, 70+cx*sc-sc/2, 100+cy*sc-sc/2, 20
  gr.circle nul, 70+(cx+dix[fin])*sc-sc/2, 100+(cy+diy[fin])*sc-sc/2, 20
  gr.line nul, 70+cx*sc-sc/2, 100+cy*sc-sc/2, ~
               70+(cx+dix[fin])*sc-sc/2, 100+(cy+diy[fin])*sc-sc/2
endif
return

newgrid:       % make new grid with starting colors.
cc =min(nc,6)
for c=1 to cc
  do
    cx =int(rnd()*nc)+1
    cy =int(rnd()*nc)+1
  until cell[cx,cy]=0
  call clr(c$[c])
  gr.circle nul, 70+cx*sc-sc/2, 100+cy*sc-sc/2, sc/2-10
  cell[cx,cy] =c
  nt++
next
return

savegrid:
grid$[nc-3] =""
for y=1 to nc
  for x=1 to nc
    grid$[nc-3] =grid$[nc-3]+int$(cell[x,y])
  next
next
return

loadgrid:
n  =0
nt =0
for cy=1 to nc
  for cx=1 to nc
    n++
    cell[cx,cy] =val(mid$(grid$[nc-3],n,1))
    if cell[cx,cy]
      nt++
      cc =cell[cx,cy]
      call clr(c$[cc])
      gr.circle nul, 70+cx*sc-sc/2, 100+cy*sc-sc/2, sc/2-10
      if !fin then gosub ctrl
    endif
  next
next
return

savegame:
cls
print nc
print scm[1], scm[2], scm[3], scm[4], scm[5], scm[6], scm[7], scm[8], scm[9], scm[10]
print grid$[1]
print grid$[2]
print grid$[3]
print grid$[4]
print grid$[5]
console.save filesave$
cls
return

loadgame:
grabfile ldgame$, filesave$
split ln$[], ldgame$, "\n"
nc =val(ln$[1])
for s=1 to 10
  scm[s] =val(word$(ln$[2],s,","))
next
for g=1 to 5
  grid$[g] =ln$[2+g]
next
return




