Rem Touching Colours
Rem with RFO-Basic
Rem September 2014
Rem bug Fix & Best score is now saved. November 2014
Rem Version 1.02
Rem For Android
Rem Roy

Rem Open Graphics and set for all devices
di_width = 1280
di_height = 800

gr.open 255,0,0,255  % Blue
gr.orientation 0
pause 1000
gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y
gr.text.typeface 2 %font monospace needed for the info

Rem Global vars
dim board[6,6]
dim score[1]
dim best[1]
score[1] = 0
gameOver = 0
maxHeight=0

Rem Start of Functions

fn.def Info(di_width,di_height,maxWidth,maxHeight,scale_x,scale_y)
	call PrintTab(1,4,2,"The playing board as 36 places, and so that you can't")
	call PrintTab(1,5,2,"use some sort of pateren, 6 of them are filled.")

	call PrintTab(1,7,4,"To play: Touch a colour, in the pallet, from the six")
	call PrintTab(1,8,4,"available. The colour will replace the man, in the")
	call PrintTab(1,9,4,"playing board, and the man will move to a new")
	call PrintTab(1,10,4,"location, you then touch another colour.")

	call PrintTab(1,12,5,"Points: You get a point for each colour entered and if")	
	call PrintTab(1,13,5,"you complete the board you get 100 bonus points,")	
	call PrintTab(1,14,5,"and a new board.")
			
	call PrintTab(1,16,6,"The game is over if there is a same colour next to the")	
	call PrintTab(1,17,6,"one you picked, ether to the right, left, up, down or")	
	call PrintTab(1,18,6,"diagonally.")
	
	call Gcol(1)
	gr.rect r,di_width/2-100,di_height-100,di_width/2+100,di_height-50
	call Gcol(8)
	s$="OK"
	gr.text.width w,s$
	gr.text.draw d,di_width/2-w/2,di_height-65,s$
	do
		gr.render
		gr.touch touched,tx,ty
	until touched & tx>(di_width/2-100)*scale_x & tx<(di_width/2+100)*scale_x & ty>(di_height-100)*scale_y & ty<(di_height-50)*scale_y
	gr.text.typeface 1
	gr.text.size 50
fn.end

fn.def SetUp(board[],score[],best[])
	gr.cls
	call LoadBestScore(best[])
	call LoadBoardArray(board[])
	call LoadFirstSix(board[])
	call DrawBoard(board[])
	call DoText(720,50,"Touching Colours")
	call ShowScore(score[])
	call ShowBest(best[])
	gr.render
fn.end

fn.def ChrPerLine(di_width,di_height,cpl,maxHeight)
ts=1
	do
		gr.text.size ts
		gr.text.width textWidth,"W"
		ts++
	until textWidth>=di_width/cpl
	maxHeight=1
	gr.get.textbounds "|",l,top,r,bottom
	maxHeight*=abs(top)+bottom
	maxHeight=int(di_height/maxHeight)
	fn.rtn cpl
fn.end

fn.def PrintTab(x,y,c,t$)
	x--
	getLen=len(t$) % gr.get.textbounds losses leading and trailing spaces, so did this to get around it
	gr.get.textbounds "|",l,top,r,bottom
	y*=abs(top)+bottom
	gr.get.textbounds "W",l,t,r,b
	getLen*=r
	x*=r
	gr.color 255,0,0,255,1 % White background or set to whatever background colour you have
	gr.rect r,x,top+y,x+getLen,bottom+y % do a rectangle over where the new text is going, in case there is some old text in the same place
	call Gcol(c) % set text to whatever colout you like from the Gcol() function
				 % or comment out Gcol() and add your own colour ie gr.color 255,255,0,255,1 magenta
	gr.text.draw d,x,y,t$ % Do the text a tab x,y
	gr.render % if you want speed then comment this out and print a load of stuff then do a gr.render
fn.end

fn.def SaveBestScore(best[])
	BestScorePath$="../../TouchingColours/data/"
	file.exists BestPresent,BestScorePath$+"BestScore.tst"
	if BestPresent then
		text.open w,hs,BestScorePath$+"BestScore.tst"
			text.writeln hs,int$(best[1])
		text.close hs
	else
		file.mkdir BestScorePath$
		text.open w,hs,BestScorePath$+"BestScore.tst"
			text.writeln hs,int$(best[1])
		text.close hs
	endif
fn.end

fn.def LoadBestScore(best[])
	BestScorePath$="../../TouchingColours/data/"
	file.exists BestPresent,BestScorePath$+"BestScore.tst"
	if BestPresent then
		text.open r,hs,BestScorePath$+"BestScore.tst"
			text.readln hs,bs$
			best[1]=val(bs$)
		text.close hs
	else
		best[1]=0
	endif
fn.end

fn.def ShowScore(score[])
	gr.text.align 3
	call Gcol(3)
	gr.rect r,750,150,1060,210
	call DoText(950,200,"Score: "+int$(score[1]))
	gr.text.align 1
fn.end

fn.def ShowBest(best[])
	gr.text.align 3
	call Gcol(3)
	gr.rect r,750,210,1060,270
	call DoText(950,250,"Best: "+int$(best[1]))
	gr.text.align 1
fn.end

fn.def DoText(x,y,t$)
	gr.text.bold 1
	gr.color 255,0,0,0,1 % black
	call printat(x,y,t$)
	gr.color 255,255,0,0,1 % Red
	call printat(x+3,y+3,t$)
	gr.render
	gr.text.bold 0
fn.end

fn.def LoadBoardArray(board[])
	for y=1 to 6
		for x=1 to 6
			board[x,y]=0
		next
	next
fn.end

fn.def Gcol(c)

	if c=0 then gr.color 255,0,0,0,1 % black
	if c=1 then gr.color 255,255,0,0,1 % Red
	if c=2 then gr.color 255,0,255,0,1 % green
	if c=3 then gr.color 255,0,0,255,1 % blue
	if c=4 then gr.color 255,255,255,0,1 % yellow
	if c=5 then gr.color 255,0,255,255,1 % cyan
	if c=6 then gr.color 255,255,0,255,1 % magenta
	if c=7 then gr.color 255,192,192,192,1 % gray
	if c=8 then gr.color 255,255,255,255,1 % white

fn.end

fn.def LoadFirstSix(board[])
	for c=1 to 6
		do
			x=floor(rnd() * 6)+1
			y=floor(rnd() * 6)+1
		until board[x,y]=0
		board[x,y]=c
	next
fn.end

fn.def printat(x,y,t$)
	gr.text.draw p,x,y, t$
fn.end

fn.def DrawBoard(board[])
	cx=1
	cy=1
	call Gcol(7)
	gr.rect r,30,30,670,670
	call Gcol(0)
	gr.rect r,40,40,660,660
	call Gcol(7)
	for y=100 to 600 step 100
		for x=100 to 600 step 100
			if board[cx,cy]>0 then
				call Gcol(board[cx,cy])
			else
				call Gcol(7)
			endif
			gr.circle c,x,y,45
			cx++
		next
		cx=1
		cy++
	next
	Rem Draw Pallet
	cx=1
	call Gcol(7)
	gr.rect r,750,400,1100,650
	call Gcol(0)
	gr.rect r,760,410,1090,640
	
	for x=810 to 1040 step 110
		call Gcol(cx)
		cx++
		gr.circle c,x,470,40
	next
	for x=810 to 1040 step 110
		call Gcol(cx)
		cx++
		gr.circle c,x,570,40
	next
	gr.render
fn.end

fn.def NewBoard(board[],score[],best[])
	for s=500 to 1000 step 200
		tone s,100
	 next
	call SetUp(board[],score[],best[])
fn.end

fn.def BoardFull(board[])
	flag=1
	for y=1 to 6
		for x=1 to 6
			if board[x,y]=0 then flag = 0
		next
	next
	fn.rtn flag
fn.end

fn.def TouchingColours(board[],score[],best[])
	flag = 0
	call Gcol(0)
	for y=1 to 6
		for x=1 to 6
			if board[x,y]>0 then
				if x<6 then 
					if board[x,y]=board[x+1,y] then flag=Doline(x*100,y*100,(x+1)*100,y*100) %Look left to right
				endif 
				
				if y<6 then
					if board[x,y]=board[x,y+1] then flag=Doline(x*100,y*100,x*100,(y+1)*100) %Look top tp bottom
				endif
				
				if x>1 & x<6 & y>1 & y<6 then % Look diaganaly
					if board[x,y]=board[x-1,y-1] then flag=Doline(x*100,y*100,(x-1)*100,(y-1)*100)
					if board[x,y]=board[x+1,y-1] then flag=Doline(x*100,y*100,(x+1)*100,(y-1)*100)
					if board[x,y]=board[x-1,y+1] then flag=Doline(x*100,y*100,(x-1)*100,(y+1)*100)
					if board[x,y]=board[x+1,y+1] then flag=Doline(x*100,y*100,(x+1)*100,(y+1)*100)
				endif
				
				if board[1,5]>0 & board[1,5]=board[2,6] then flag=Doline(100,500,200,600) % Bottom Left
				if board[5,6]>0 & board[5,6]=board[6,5] then flag=Doline(500,600,600,500) % Bottom Right
				if board[1,2]>0 & board[1,2]=board[2,1] then flag=Doline(100,200,200,100) % Top Left
				if board[5,1]>0 & board[5,1]=board[6,2] then flag=Doline(500,100,600,200) % Top Right
			endif
		next
	next
	 if flag then
		if score[1]> best[1] then
			for s=500 to 1000 step 200
				tone s,100
			 next
		else
			for s=550 to 200 step -50
				tone s,100
			next
		endif
	endif 
	fn.rtn flag
fn.end

fn.def Doline(x,y,x1,y1)
	call Gcol(8)
	gr.line dr, x,y,x1,y1
	gr.circle cr,x,y,10
	gr.circle cr,x1,y1,10
	gr.render
	fn.rtn 1
fn.end

fn.def NewGame(board[],score[],best[],scale_x,scale_y)
	flag=2
	call Gcol(3)
	gr.rect r,740,390,1110,700
	call Gcol(6)
	gr.rect r,780,420,1080,520
	gr.rect r,780,560,1080,660
	gr.color 255,0,0,0,0
	gr.rect r,780,560,1080,660
	gr.rect r,780,560,1080,660
	call Gcol(0)
	gr.text.draw d,810,485,"New Game"
	gr.text.draw d,880,625,"Quit"
	gr.render
	
	do
		do
			gr.touch touched, tx,ty
		until touched
		if touched & tx>780*scale_x & tx<1080*scale_x & ty>420*scale_y & ty<520*scale_y then flag=0
		if touched & tx>780*scale_x & tx<1080*scale_x & ty>560*scale_y & ty<660*scale_y then flag=1
	until flag=0 | flag=1
	do
		gr.touch touched,tx,ty
	until !tounched
	fn.rtn flag
fn.end

fn.def DrawMan(x,y)
	call Gcol(0)
	call printat(x-15,y-5,"O") % Place to fill
	call printat(x-15,y+15,chr$(214)) % Place to fill
	call printat(x-25,y+15,"/")
	call printat(x+10,y+15,chr$(92))
	call printat(x-15,y+25,"| |") % Place to fill
fn.end

fn.def PlayGame(board[],score[],best[],scale_x,scale_y)
	do
		do
			do
				bx=floor(rnd() * 6)+1
				by=floor(rnd() * 6)+1
			until board[bx,by] = 0
			x=bx * 100
			y=by * 100
			call Gcol(0)
			call DrawMan(x,y)
			gr.render
			
			do
				flag=0	
				gr.touch touched,tx,ty
				if touched then
					c=1
					for cx=780 to 1100 step 110
						if tx>cx*scale_x & tx<(cx+110)*scale_x & ty>450*scale_y & ty<550*scale_y then board[bx,by]=c
						c++
					next
					for cx=780 to 1100 step 110
						if tx>cx*scale_x & tx<(cx+110)*scale_y & ty>550*scale_y & ty<650*scale_y then board[bx,by]=c
						c++
					next
				endif
				gr.render % this bring game back when phone as been a sleep
				if board[bx,by]>0 then 
					tone 400,100
					call Gcol(board[bx,by])
					gr.circle cir,x,y,45
					gr.render
					do
						gr.touch touched,wx,wy
					until !touched								
					flag=1
					gr.render
				endif							
										
			until flag
			gameOver=TouchingColours(board[],score[],best[])
			if !gameOver then
				score[1]+=1
				call ShowScore(score[])
			endif
			if !gameOver & BoardFull(board[]) then
				call NewBoard(board[],score[],best[])
				score[1]+=100
				call ShowScore(score[])
			endif
			!gr.render
		until gameOver
		if score[1]>best[1] then
			best[1]=score[1]
			call SaveBestScore(best[])
			call ShowBest(best[])
			call DoText(770,350,"A New Best!!")
		endif
	   gameOver=NewGame(board[],score[],best[],scale_x,scale_y)
	   if !gameOver then
			score[1]=0
			gr.cls
			call SetUp(board[],score[],best[])
		endif
	until gameOver	

fn.end

Rem End of Functions

Rem Main

gr.text.size 40
s$="Touching Colours"
gr.text.width w,s$
call DoText(di_width/2-w/2,50,s$)

maxWidth = ChrPerLine(di_width,di_height,60,&maxHeight) 
call Info(di_width,di_height,maxWidth,maxHeight,scale_x,scale_y)

Rem main
call SetUp(board[],score[],best[])

call PlayGame(board[],score[],best[],scale_x,scale_y)

wakelock 5 
gr.close

exit

onBackKey:

do
	Dialog.Message "Exit:", "Are you sure you want to leave Touching Colours", yn, "Yes","No"
	if yn=0 then tone 400,400
until yn=1 | yn=2

if yn=2 then 
	back.resume
else
	if score[1]>best[1] then
		best[1]=score[1]
		call SaveBestScore(best[])
		Dialog.Message "New Best:", int$(best[1])+" Is A New Best Score Well done", ok, "OK"
	endif 
endif


wakelock 5 
gr.close

exit


		