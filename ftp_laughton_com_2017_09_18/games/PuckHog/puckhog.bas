pi = atan(1) * 4
tpi = 2 * pi
hpi = 0.5 * pi
thpi = 1.5 * pi
speed=0
lscore=0
rscore=0

fn.def atan2(nx, ny)

 nc = 0.0001
 tanout = atan((ny) / (nx + nc))
 If (nx) > 0 Then tanout = pi - tanout
 fn.rtn tanout
fn.End

fn.def OnBall(nx2, ny2, ox, oy, radius)
    dist = Sqr((nx2 - ox) ^ 2 + (ny2 - oy) ^ 2)
    fn.rtn (dist <= radius)
fn.End
 

gr.open 255, 255, 255, 255
gr.Orientation 0
gr.Screen mx, my
gr.Color 255, 0, 0, 0, 0
gr.rect l1, 0, 0, 1, my
gr.rect l2, mx , 0, mx - 1, my
gr.rect l3, 0, my, mx, my - 1
gr.rect l4, 0, 0, mx, 1
gr.line l5,mx/2,0,mx/2,my
gr.circle l6, mx/2,my/2,mx/12
gr.color 255,0,0,255,1
gr.rect goalL,0,my*9/20,1,my*11/20
gr.rect goalR,mx,my*9/20,mx-1,my*11/20
gr.rect maskL,0,my*4/10,9,my*6/10
gr.rect maskR,mx,my*4/10,mx-9,my*6/10 
gr.text.size my/15
gr.color 192,0,0,0,1 
gr.text.draw leftscore, mx*4/48, my*3/24, " 0"
gr.text.draw rightscore, mx*40/48, my*3/24, " 0"
m = 0
gr.Color 255, 255, 0, 0, 1
r = mx/40
r2=r*2
gr.circle ball, mx/2, my/2, r

gr.text.size my/4
gr.text.align 2
gr.color 128,0,0,0,1 
gr.text.draw winner, mx/2, my*2/5, "WINNER" 
gr.hide winner

oldx = mx / 2
oldy = my / 2
prex = oldx
prey = oldy
gr.Render



Do
      flag=0
      gr.touch flag, x, y
      if flag then

          If OnBall(x, y, oldx, oldy,r2) Then
              speed=r*1.5
              zx =  oldx-x
              zy =  oldy-y
              pit = atan2(zx, zy)
          End If
      endif

      flag=0
      gr.touch2 flag, x, y
 
      if flag then
          If OnBall(x, y, oldx, oldy,r2) Then
              speed=r*1.5
              zx =  oldx-x
              zy =  oldy-y
              pit = atan2(zx, zy)
          End If
      endif
 
      If gr_collision(goalL , ball) Then 
         oldx = mx / 2
         oldy = my / 2
         prex = oldx
         prey = oldy 
         gr.modify ball, "x", oldx
         gr.modify ball, "y", oldy 
         rscore=rscore+1
         gr.modify rightscore, "text",format$("##",rscore)
         speed=0 
    ElseIf gr_collision(goalR , ball) Then 
         oldx = mx / 2
         oldy = my / 2
         prex = oldx
         prey = oldy 
         gr.modify ball, "x", oldx
         gr.modify ball, "y", oldy 
         lscore=lscore+1
         gr.modify leftscore, "text",format$("##",lscore)
         speed=0
    ElseIf gr_collision(l1, ball) Then
         oldx = prex
         oldy = prey
         gr.modify ball, "x", oldx
         gr.modify ball, "y", oldy
         tmp = pit - tpi
         pit = pi - tmp
    ElseIf gr_collision(l4, ball) Then
         oldx = prex
         oldy = prey
         gr.modify ball, "x", oldx
         gr.modify ball, "y", oldy
         tmp = pit - hpi
         pit = thpi - tmp
    
    ElseIf gr_collision(l2, ball) Then
         oldx = prex
         oldy = prey
         gr.modify ball, "x", oldx
         gr.modify ball, "y", oldy
         tmp = pit - pi
         pit = tpi - tmp
    ElseIf gr_collision(l3, ball) Then
         oldx = prex
         oldy = prey
         gr.modify ball, "x", oldx
         gr.modify ball, "y", oldy
         tmp = pit - thpi
         pit = hpi - tmp
    elseif speed<=0 then
         speed=0
         prex=oldx
         prey=oldy
    Else
         prex = oldx
         prey = oldy
         oldy = oldy - Sin(pit) * speed
         If zx < 0 Then
            oldx = oldx - Cos(pit) * speed
         Else
            oldx = oldx + Cos(pit) * speed
         End If
         gr.modify ball, "x", oldx
         gr.modify ball, "y", oldy
         speed=speed - 0.15
    End If
 
    if (lscore=7) | (rscore=7) then 
         gr.show winner
         gr.render
         pause 2000
         gr.hide winner 
         oldx = mx / 2
         oldy = my / 2
         prex = oldx
         prey = oldy 
         lscore=0
         rscore=0
         speed=0
         gr.modify rightscore, "text", " 0"
         gr.modify leftscore, "text", " 0"

    endif

    gr.Render
    
    pause 1

until 0

OnError:
gr.Close
End
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
