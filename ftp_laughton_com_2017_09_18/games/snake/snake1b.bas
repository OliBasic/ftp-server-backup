REM Start of BASIC! Program
  

GOSUB userfunctions



h              = 800
w              = 1280
GR.OPEN          255,0,0,40,1,0
PAUSE            200
GR.SCREEN        cur_w, cur_h
sca_width      = cur_w /w
sca_height     = cur_h /h
GR.SCALE         sca_width , sca_height



rad                     =  12
dia                     =  2 * rad
pGrX                    =  42
pGrY                    =  32
offsX                   =  5
offsY                   =  5

ARRAY.LOAD                 vib [], 0, 50
DIM                         cntr  [10, 60 ]
   
speed                   = 55
GOSUB                     speedHandles



restart:



flagInit                =  0
GR.CLS

UNDIM                        px []
UNDIM                        py []
UNDIM                        pl []
UNDIM                        pl2 []
UNDIM                        pl3 []
UNDIM                        pGr []

DIM                        px [ 300 ]
DIM                        py [ 300 ]
DIM                        pl [ 300 ]
DIM                        pl2 [ 300 ]
DIM                        pl3 [ 300 ]
DIM                        pGr [ pGrX , pGrY ]



le             =  5
colact         =  50
anzCtr         =  5
bf             =  75
hc             =  h/3.5
wc             =  w*0.87
rr             =  0.85*bf
txtSz          =  45

GR.TEXT.ALIGN     2
GR.TEXT.SIZE      txtSz
GR.TEXT.BOLD      1

no             =  1
cntr [no,1]    =  wc - rr
cntr [no,2]    =  hc

no             =  2
cntr [no,1]  =   wc
cntr [no,2]  =  hc -rr

no           =  3
cntr [no,1]  =  wc+rr
cntr [no,2]  =  hc

no             =  4
cntr [no,1]  =  wc
cntr [no,2]  =  hc +rr

no             =  5
ww= 60
hh= 15
cntr [no,1]  =  wc - ww +bf/2
cntr [no,2]  =  hc*2 - hh


!circle around controls ------
GR.SET.STROKE     6
GR.COLOR         40 , 0 , 0 , 140 ,1
GR.CIRCLE        nn, wc + bf/2 , hc + bf/2 ,rr*1.8
GR.COLOR         255 , 255 , 255 , 255 ,0
GR.CIRCLE        nn, wc + bf/2 , hc + bf/2 ,rr*1.8
GR.COLOR         255 , 100 , 100 , 100 ,0
GR.CIRCLE        nn, wc + bf/2+1 , hc + bf/2+1 ,rr*1.8


! slider (back) ----------------
poly           = roundCornerRect ( 15 , 300 , 5 )
GR.SET.STROKE    6
GR.COLOR         160 , 0 , 0 , 0 ,1
GR.POLY          cntr [no,50] , poly , wc +bf/2  , h*7/10
GR.COLOR         255 , 255 , 255 , 255 ,0
GR.POLY          cntr [no,50] , poly , wc +bf/2  , h*7/10
GR.COLOR         255 , 100 , 100 , 100 , 0
GR.POLY          cntr [no,50] , poly , wc+1 +bf/2  , h*7/10



! controls ---------------
FOR no = 1 TO anzCtr

 IF no <=4 THEN

  cntr [no,3]  =  cntr[no,1] + bf
  cntr [no,4]  =  cntr[no,2] + bf

  poly          = roundCornerTriang ( bf/2 *0.6, 15, (no-2)*90)

  GR.SET.STROKE    6
  GR.COLOR         160 , 0 , 0 , 100 ,1
  GR.POLY          cntr [no,50] , poly , cntr[no,1] + bf/2 , cntr[no,2] + bf/2

  GR.COLOR         255 , 255 , 255 , 255 ,0
  GR.POLY          cntr [no,50] , poly , cntr[no,1] + bf/2 , cntr[no,2] + bf/2

  GR.COLOR         255 , 100 , 100 , 100 , 0
  GR.POLY          cntr [no,50] , poly , cntr[no,1] + bf/2 +1, cntr[no,2] + bf/2 +1

 ELSE

  cntr [no,3]  =  cntr[no,1] + 2*ww
  cntr [no,4]  =  cntr[no,2] + 2*hh

  cntr[no,5]    =  cntr[no,1] + bf/2
  cntr[no,6]    =  cntr[no,2] + bf/2 + txtSz/3

  poly          = roundCornerRect ( 2*ww , 2*hh , 8)

  GR.SET.STROKE    5
  GR.COLOR         220 , 0 , 0 , 100 ,1
  GR.POLY          cntr [no,52] , poly , cntr[no,1] + ww  , cntr[no,2] + hh

  GR.COLOR         255 , 255 , 255 , 255 ,0
  GR.POLY          cntr [no,51] , poly , cntr[no,1] + ww  , cntr[no,2] + hh

  GR.COLOR         255 , 100 , 100 , 100 , 0
  GR.POLY          cntr [no,50] , poly , cntr[no,1] + ww +1  , cntr[no,2] + hh +1

 ENDIF

 cntr[no,7]    =  cntr[no,1] * sca_width
 cntr[no,8]    =  cntr[no,2] * sca_height
 cntr[no,9]    =  cntr[no,3] * sca_width
 cntr[no,10]   =  cntr[no,4] * sca_height


NEXT

! info box -------------
rcr                     = roundCornerRect( rr*3 , rr*1.1 , 15)
GR.COLOR                  40 , 0 , 0 , 140 ,1
GR.POLY                   nn, rcr, wc + bf/2 , offsy+ rr
GR.COLOR                  255 , 255 , 255 , 255 ,0
GR.POLY                   nn, rcr, wc + bf/2 , offsy+ rr
GR.COLOR                  255 , 100 , 100 , 100 , 0
GR.POLY                   nn, rcr, wc + bf/2+1 , offsy+ rr+1

GR.COLOR                  255,0,250,250,1
GR.TEXT.BOLD              0
GR.TEXT.SIZE              40
GR.TEXT.ALIGN             2
GR.TEXT.DRAW              info , wc + bf/2 -10 , offsy+ rr+12, ""


! playground ---------------------
GR.SET.STROKE             dia*0.5
rcr                     = roundCornerRect( (pGrX-1)*dia , (pGrY-1)*dia , 1.2*dia)

GR.COLOR                  255 , 20 , 20 , 20 ,1
GR.POLY                   nn, rcr, offsX + (pGrX*dia)/2+0.5*diA, offsY+(pGrY*dia+1)/2 +0.5*diA
GR.COLOR                  255 , 255 , 255 , 255 ,0
GR.POLY                   nn, rcr, offsX + (pGrX*dia)/2+0.5*diA, offsY+(pGrY*dia+1)/2 +0.5*diA
GR.COLOR                  255 , 100 , 100 , 100 , 0
GR.POLY                   nn, rcr, offsX + (pGrX*dia)/2+0.5*diA +1 , offsY+(pGrY*dia+1)/2 +0.5*diA +1


! snake ------------
GR.COLOR                  255 , 170 , 170 , 220 , 1
FOR i                   =  1 TO le
 px [i]                 =  1+i
 py [i]                 =  15
 GR.CIRCLE                 pl [i] , offsX + px [i] * dia , offsY + py [i] * dia , rad
 pGr [px[i],py[i]]      =  1
NEXT


! walls in playmatrix ------------
FOR i                   =  1 TO pGrX
 pGr [i    , 1    ]     =  2
 pGr [i    , pGrY ]     =  2
NEXT
FOR i                   =  1 TO pGrY
 pGr [1    , i ]        =  2
 pGr [pGrX , i ]        =  2
NEXT


! cookie -----------------
GR.COLOR                  200 , 200 , 0 , 0 , 1
GR.CIRCLE                 cookie, 0, 0, rad
GOSUB                     throwCookie



ptr                     =  le
dPlx                    =  1
dPly                    =  0
chk                     =  17
flagJump                =  0
flagInit                =  1

for i=1 to 4
 cntr[i,chk]            =  0
next

gosub                     setSpeed
ctrFound                = 30
GR.RENDER
pause 1000



! main loop ------------------------------------------

DO

 !update direction ----------------------
 IF                          cntr[1,chk]|cntr[3,chk] THEN gosub cntr_13
 IF                          cntr[2,chk]|cntr[4,chk] THEN gosub cntr_24

 !perform move -----------------------------
 ptrOld                    = ptr
 ptr++
 IF ptr                    > le THEN ptr = 1

 pGr[px[ptr],py[ptr]]      = 0
 px [ ptr ]                = px [ ptrOld ] + dPlx
 py [ ptr ]                = py [ ptrOld ] + dPly
 found                     = pGr[px[ptr],py[ptr]]
 IF found                    THEN gosub eventHandles
 pGr[px[ptr],py[ptr]]      = 1

 GR.MODIFY                   pl [ ptr ] ,"x", offsX + px [ ptr ] * dia
 GR.MODIFY                   pl [ ptr ] ,"y", offsY + py [ ptr ] * dia
 GR.MODIFY                   info , "text", replace $(str$(round(ctrFound)) ,".0","")  
 GR.RENDER

 !loop-time-handle / controls chk --------------------------
 DO
   pause 5
 UNTIL clock ()         >= looptime
 looptime               =  clock ()+ desLoopTime

 FOR no                 =  1 TO anzCtr   
   IF cntr[no,chk]         THEN gr.modify cntr[no,50],"alpha", 220
   GR.BOUNDED.TOUCH        cntr[no,chk] ,cntr[no,7],cntr[no,8],cntr[no,9],cntr[no,10]
   IF cntr[no,chk]         THEN gr.modify cntr[no,50],"alpha", 0  
 NEXT

 IF cntr[5,chk]            THEN gosub adjustSpeed
 ctrFound               =  ctrFound- desLoopTime^1.1/1000

UNTIL                      0

!-----------------------------------------------------------


!-----------------------------------------------------------
cntr_13:
IF dPly              <> 0
 IF cntr[1, chk ]       THEN dPlx = -1 ELSE dPlx =  1
 VIBRATE                vib [], -1
 dPly                 = 0
ENDIF
RETURN

cntr_24:
IF dPlx              <> 0
 IF cntr[2, chk ]       THEN dPly = -1 ELSE dPly =  1
 VIBRATE                vib [],-1
 dPlx                 = 0
ENDIF
RETURN
!-----------------------------------------------------------


!-----------------------------------------------------------
eventHandles:

IF found = 1 | found = 2 THEN
 tmp $="T A I L B I T E ! 
 if found = 2  then tmp $="W A L L H I T !
 popup tmp$ + chr$(10) + chr$(10) + " - Game over - ", 0, 0,0
 PAUSE 2000
 GOTO restart
ENDIF
IF found                =  99 THEN gosub throwCookie

RETURN
!-----------------------------------------------------------


!-----------------------------------------------------------
throwCookie:

ctrFound     += round (le/3)


if flaginit then GOSUB enlargeSnake

DO
 cx                      =  3 + floor (RND()* (pGrX-4))
 cy                      =  3 + floor (RND()* (pGrY-4))
 tmp                     =  pGr [cx,cy]
UNTIL                       !tmp

pGr [cx,cy]              =  99


GR.MODIFY                   cookie,"x"   , offsX + cx * dia
GR.MODIFY                   cookie,"y"   , offsY + cy * dia
!gosub dump1

RETURN
!-----------------------------------------------------------


!-----------------------------------------------------------
enlargeSnake:

FOR i=1 TO 5
 le++
 GR.COLOR                  255 , 170 , 170 , 220 , 1
 px[le]                 =  px[le-1]
 py[le]                 =  py[le-1]
 GR.CIRCLE                 pl [le] , offsX + px [le] * dia , offsY + py [le] * dia , rad
 pGr [px[le],py[le]]    =  1
NEXT

RETURN
!-----------------------------------------------------------


!-----------------------------------------------------------
adjustSpeed:
ymin = h*7/10-145
ymax =ymin + 290
dymima = ymax- ymin
no=5
DO
 GR.TOUCH           tt, x, y
 y                = y / sca_height
 IF                 y <Ymin THEN y=ymin
 IF                 y >Ymax THEN y=ymax
 GR.MODIFY          cntr [no,50],"y", y
 GR.MODIFY          cntr [no,51],"y", y
 GR.MODIFY          cntr [no,52],"y", y
 speed              = (y -ymin) / dymima *100
 gosub                speedHandles
 GR.MODIFY          info , "text", str $(round (10000/ desLoopTime)/10) +" Hz"
 GR.RENDER

UNTIL !tt
cntr[no,chk]       = 0
GR.MODIFY              cntr[no,50],"alpha", 255
cntr[no,2]         =  y-hh
cntr[no,4]         =  cntr[no,2] + 2*hh
cntr[no,8]         =  cntr[no,2] * sca_height
cntr[no,10]        =  cntr[no,4] * sca_height
PAUSE 1000

RETURN
!-----------------------------------------------------------


!-----------------------------------------------------------
setSpeed:

no=5
ymin = h*7/10-145
ymax =ymin + 290
dymima = ymax- ymin
 y                = h*7/10-145 + speed/100* dymima
 GR.MODIFY          cntr [no,50],"y", y
 GR.MODIFY          cntr [no,51],"y", y
 GR.MODIFY          cntr [no,52],"y", y
 GR.RENDER
 cntr[no,2]         =  y-hh
 cntr[no,4]         =  cntr[no,2] + 2*hh
 cntr[no,8]         =  cntr[no,2] * sca_height
 cntr[no,10]        =  cntr[no,4] * sca_height

RETURN
!-----------------------------------------------------------


!-----------------------------------------------------------
speedHandles:

 desLoopTime       =  120 - 70 * speed/100

RETURN
!-----------------------------------------------------------



!-----------------------------------------------------------
jump:

IF px[ptr] >              pGrX-1 THEN px[ptr] = 2
IF px[ptr] <              2      THEN px[ptr] = pGrX-1
IF py[ptr] >              pGry-1 THEN py[ptr] = 2
IF py[ptr] <              2      THEN py[ptr] = pGry-1

RETURN
!-----------------------------------------------------------



!-----------------------------------------------------------
dump1:

FOR i=1 TO pGrY
 FOR j=1 TO pGrX
  IF  j < pGrX THEN print replace$ (FORMAT$("%",pGr [ j,i])," ",""); ELSE print replace$ (FORMAT$("%",pGr [ j,i])," ","")
 NEXT
NEXT
PRINT "----"

RETURN
!-----------------------------------------------------------



!-----------------------------------------------------------
userfunctions:

FN.DEF                roundCornerTriang ( h, r, dphiGlob)

 dphiGlob           = toradians ( dphiGlob )

 n                  = 8
 phioffs            = 20
 phiStart           = toradians ( phioffs )
 phiend             = toradians (180- phioffs )
 dphi               = (phiend-phistart) / n

 LIST.CREATE          N, S1

 FOR j               = 0 TO 240 STEP 120

  mx                 =  h*sin (toradians (j) +dphiGlob )
  my                 = -h*cos (toradians (j) +dphiGlob )
  dphi2              = toradians (j)

  FOR phi            = phiStart TO phiEnd STEP dphi
   phii              = phi + dphi2 + dphiGlob
   LIST.ADD            s1, mx-COS(phii)*r, my-sin (phii )*r
  NEXT

 NEXT

 FN.RTN s1

FN.END



FN.DEF                roundCornerRect (b, h, r)

 half_pi            = 3.14159 / 2
 dphi               = half_pi / 8
 LIST.CREATE          N, S1

 mx                 = -b/2+r
 my                 = -h/2+r
 FOR phi            = 0 TO half_pi STEP dphi
  LIST.ADD             s1, mx-COS(phi)*r, my-sin (phi)*r
 NEXT
 mx                 = b/2-r
 my                 = -h/2+r
 FOR phi            = 0 TO half_pi STEP dphi
  LIST.ADD            s1, mx+SIN(phi)*r, my- cos (phi)*r
 NEXT
 mx                 = b/2-r
 my                 = h/2-r
 FOR phi            = 0 TO half_pi STEP dphi
  LIST.ADD            s1, mx+COS(phi)*r, my+sin (phi)*r
 NEXT
 mx                 = -b/2+r
 my                 =  h/2-r
 FOR phi            = 0 TO half_pi STEP dphi
  LIST.ADD            s1, mx-SIN(phi)*r, my+cos (phi)*r
 NEXT

 FN.RTN s1

FN.END

RETURN
!-----------------------------------------------------------
