REM AI (console) pour "Code finder"  01.04.2017 by gege_95crj + Cassiope34.

 dim cat[10,3], c$[3] ,co$[20], nco[20], p[20], nbco[20]
 mmx =10 : ti =clock()
 DO              %  principal loop
   array.fill cat[],9 : tr =0 : pr =0 : co=0 : dprop$=""
   do : a =int(rnd()*10) : b =int(rnd()*10) : c =int(rnd()*10) : until a<>b & a<>c & b<>c
   secret$ = int$(a)+int$(b)+int$(c) : s1$ =mid$(secret$,1,1) : s2$ =mid$(secret$,2,1) : s3$ =mid$(secret$,3,1)
  ?
  ? "*************************"
  ? "     Code secret "+secret$
  ? "*************************"
    
   do
     if pr<3 & tr<3
       prop$ =word$("012 345 678 ",pr+1)
        
     else
       do            % fabrique une prop$
         do
           a =int(rnd()*len(c$[1])/2)+1 : b =int(rnd()*len(c$[2])/2)+1 : c =int(rnd()*len(c$[3])/2)+1
           p1 =val(word$(c$[1],a)) : p2 =val(word$(c$[2],b)) : p3 =val(word$(c$[3],c))
           prop$ =int$(p1)+int$(p2)+int$(p3)
         until p1<>p2 & p1<>p3 & p2<>p3 & !IS_IN(prop$,dprop$)

         ok= 1 : z =0
         if co        %  compare prop si ok avec prop précédentes
           do
             z++ : b1= val(mid$(co$[z],1,1)) : b2= val(mid$(co$[z],2,1)) : b3= val(mid$(co$[z],3,1))
             if (b1=p1)+(b2=p2)+(b3=p3)<>nbco[z] | (b1=p1 | b1=p2 | b1=p3)+(b2=p1 | b2=p2 | b2=p3)+(b3=p1 | b3=p2 | b3=p3)<>nco[z]
               ok=0 : dprop$ += prop$ +" "
             endif
           until z=co | ok=0
         endif
       until ok

     endif
     
     GOSUB verif
     
   until rep$ ="30"
   
   nt++ : npr+=pr : if pr>pmx then pmx =pr
   p[pr]++ : if pr<mmx then mmx =pr
  ?
  ?"nb codes: "+int$(nt)+"    moyenne: "+int$(npr/nt)+"     ( min = ";int$(mmx);"   max = ";int$(pmx);" )       en "+int$(clock()-ti)+" ms."
  for e=2 to pmx
    ?"     ";int$(e);" =";int$(p[e]*100/nt);"\%  ";
  next
  ?
  ! ?"      Touche l'écran pour continuer ou 2x BackKey pour quitter."
  ! if pr>7 then do : pause 1 : until tch : tch =0    % attente relance en touchant l'écran.
UNTIL  nt=100
END

OnConsoleTouch:
  tch =1
ConsoleTouch.resume

Verif:       % contrôle
 dprop$ += prop$+" "
 p1$ =mid$(prop$,1,1) : p2$ =mid$(prop$,2,1) : p3$ =mid$(prop$,3,1)
 bp =(s1$=p1$) + (s2$=p2$) + (s3$=p3$)
 mp =(s1$=p2$ | s1$=p3$) + (s2$=p1$ | s2$=p3$) + (s3$=p1$ | s3$=p2$)
 rep$ =int$(bp)+int$(mp) : tr+=bp+mp : pr++
 ? int$(pr)+" - prop$ = \""+prop$+"\"  rep = \""+rep$+"\""
 
 if rep$ ="30" then return
 
 if tr=3
   if pr<4 then ca=10 : gosub cat3
   if pr<3 then ca= 9 : gosub cat3 : ca=8 : gosub cat3 : ca=7 : gosub cat3
   if pr=1 then ca= 6 : gosub cat3 : ca=5 : gosub cat3 : ca=4 : gosub cat3
 endif
 
 op$ =p1$+p3$+p2$+" "+p2$+p1$+p3$+" "+p2$+p3$+p1$+" "+p3$+p1$+p2$+" "+p3$+p2$+p1$+" "
  
 p1 =val(p1$)+1 : p2 =val(p2$)+1 : p3 =val(p3$)+1
 
 if bp+mp then co++ : co$[co]= prop$ : nco[co]= bp+mp : nbco[co]= bp
 
 if bp+mp=3
   for z=0 to 9
      if z<>p1-1 & z<>p2-1 & z<>p3-1 then ca=z+1 : gosub cat3
   next
 
 elseif bp+mp=0
     ca=p1 : gosub cat3 : ca=p2 : gosub cat3 : ca=p3 : gosub cat3 : dprop$ += op$

 elseif bp & !mp
     cat[p1,2]=0 : cat[p1,3]=0 : cat[p2,1]=0 : cat[p2,3]=0 : cat[p3,1]=0 : cat[p3,2]=0 : dprop$ += op$

 elseif mp & !bp
     cat[p1,1]=0 : cat[p2,2]=0 : cat[p3,3]=0
     if mp<3 then dprop$ += op$
     
 endif

 for y=1 to 3      %  construit chaine des chiffres possibles par ligne (1-3)
   c$[y] =""
   for x=1 to 10
      if cat[x,y]<>0 then c$[y]+=int$(x-1)+" "
   next
 next
 ? "   \"";c$[1]+"\",       \"";c$[2]+"\",      \"";c$[3];"\""
return

cat3:   % met cat[ca] à 0 
  cat[ca,1]=0 : cat[ca,2]=0 : cat[ca,3]=0
return
