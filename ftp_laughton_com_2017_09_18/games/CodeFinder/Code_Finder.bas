REM Code à trouver -> CrackLockCode de Roy.  @Cassiope34 03/2017

FN.DEF RectR(x,y, c, lx,ly, fill)
GR.ARC nul, x, y, x+c, y+c, -90, -90, fill     % top left
GR.ARC nul, x+lx-c,y, x+lx, y+c, -90, 90, fill   % top right
GR.ARC nul, x, y+ly-c,x+c, y+ly, -180, -90, fill   % bottom left
GR.ARC nul, x+lx-c, y+ly-c, x+lx, y+ly, 0, 90, fill  % bottom right
if !fill
  GR.LINE r1, x+c/2, y, x+lx-c/2, y     % left
  GR.LINE r2, x, y+c/2, x, y+ly-c/2     % up
  GR.LINE r3, x+c/2, y+ly, x+lx-c/2, y+ly  % right
  GR.LINE r4, x+lx, y+c/2, x+lx, y+ly-c/2  % down
else
  c*=0.4
  gr.rect nul, x+c, y, x+lx-c, y+ly
  gr.rect nul, x, y+c, x+lx, y+ly-c
endif
FN.END

gr.open 255,255,255,255,0,0   %  paysage
gr.screen w,h
scx =1280
scy =800
sx =w/scx
sy =h/scy
gr.scale sx, sy

File.Root tmp$
IstandAlone =Is_In("rfo-basic",tmp$)    % to know if it's an APK for the way to exit
if IstandAlone then path$ ="Code_a_trouver/"

DIM prp[9,3], rep[3,3,2]
array.load diag$[], "New human play", "New A.I. play", "EXIT", "Cancel"

DIM cat[10,3], c$[3] ,co$[20], nco[20], nbco[20], p[20]

GOSUB bmpMaker
Timer.Set 250
human =1

DO
  new =0 : fin =0
  GOSUB initGame
  gr.modify huma, "text", word$("A.I. play;Human play",human+1,";")
  secret$ =""       %  détermine le code secret sans doublon.
  FOR c=1 to 3 : DO : s$ =int$(int(rnd()*9)+1) : UNTIL !IS_IN(s$,secret$) : secret$+=s$ : NEXT

  DO
    gr.modify cod, "text", prop$    % affiche le code proposé.
    do
      gr.touch touched,x,y
      if !background() then gr.render
    until touched | new | quit
    if new | quit then D_U.break
    gr.modify mess, "text", "" : gr.render   %  efface un éventuel message d'erreur.
    do
      gr.touch touched,x,y
    until !touched
    x/=sx : y/=sy
    if x>scx-90 & y<90      % menu
      GOSUB BoiteMenu
    
    elseif fin     % relance une AI sans passer par le menu...
      fin =0 : new =1
      
    elseif x>dtx & y>dty & !fin   % touches du clavier
      tx =int((x-dtx)/etx)+1
      ty =int((y-dty)/ety)+1
      
      if tx=3 & ty=4           %  touche "enter"
        if len(prop$)=5
          if human
            GOSUB Verif        %  vérification et réponse
          else
            secret$ =replace$(prop$," ","") : GOSUB AI    % AI trouve...
          endif
        endif

      elseif tx=1 & ty=4       %  touche "cancel"
        if len(prop$)>1 & prop$<>"CODE ?" then prop$ =left$(prop$,len(prop$)-2) else prop$ ="CODE ?"
        
      elseif tx & tx<4 & ty & ty<5   % les touches numériques
        c$ =int$(tx)+int$(ty)
        co =IS_IN(c$,"24 11 21 31 12 22 32 13 23 33")
        if co>1 then co =int(co/3) else co--
        if IS_IN(int$(co),prop$)
          gr.modify mess, "text", "pas de doublon svp !"
        elseif len(prop$)>5 | len(prop$)=4
          prop$ =int$(co)
        elseif len(prop$)<5
          prop$ =prop$+" "+int$(co)
        endif
      endif
      
    endif

  UNTIL new
UNTIL quit
if !IstandAlone then EXIT
END"Bye...!"

OnBackKey:
  GOSUB BoiteMenu
back.resume

onTimer:
 if !fin & human & scrd then scrd-- : gr.modify sco, "text", int$(scrd)
Timer.resume

BoiteMenu:
  Dialog.select nvar, diag$[], "              What do we do?"
  if nvar =1
    new =1 : human =1
  elseif nvar =2
    new =1 : human =0
  elseif nvar =3
    quit =1
  endif
return

AI:
 array.fill cat[],9 : array.fill nco[],0 : array.fill nbco[],0 : pr =0 : tr =0 : dprop$ ="" : rep$ ="" : co =0
   do
     if pr<3 & tr<3
       prop$ =word$("012 345 678 ",pr+1)
        
     else
       do            % fabrique une prop$
         do
           a =int(rnd()*len(c$[1])/2)+1 : b =int(rnd()*len(c$[2])/2)+1 : c =int(rnd()*len(c$[3])/2)+1
           p1 =val(word$(c$[1],a)) : p2 =val(word$(c$[2],b)) : p3 =val(word$(c$[3],c))
           prop$ =int$(p1)+int$(p2)+int$(p3)
         until p1<>p2 & p1<>p3 & p2<>p3 & !is_in(prop$,dprop$)
         ok= 1 : z =0
         if co        %  compare prop si ok avec prop précédentes
           do
             z++ : b1= val(mid$(co$[z],1,1)) : b2= val(mid$(co$[z],2,1)) : b3= val(mid$(co$[z],3,1))
             if (b1=p1)+(b2=p2)+(b3=p3)<>nbco[z] | (b1=p1 | b1=p2 | b1=p3)+(b2=p1 | b2=p2 | b2=p3)+(b3=p1 | b3=p2 | b3=p3)<>nco[z]
               ok=0 : dprop$ +=prop$ +" "
             endif
           until z=co | ok=0
         endif
       until ok
       
     endif
     
     gosub verifAI
     
   until rep$ ="30"
   fin =1
return

VerifAI:       % contrôle prop$
 GOSUB Verif
 dprop$ += prop$+" "
 rep$ =int$(bp)+int$(mp) : tr+=bp+mp : pr++
 if rep$="30" then return
 
 if tr=3
   if pr<4 then ca=10 : gosub cat3
   if pr<3 then ca= 9 : gosub cat3 : ca=8 : gosub cat3 : ca=7 : gosub cat3
   if pr=1 then ca= 6 : gosub cat3 : ca=5 : gosub cat3 : ca=4 : gosub cat3
 endif

 op$ =p1$+p3$+p2$+" "+p2$+p1$+p3$+" "+p2$+p3$+p1$+" "+p3$+p1$+p2$+" "+p3$+p2$+p1$+" "
 
 p1 =val(p1$)+1 : p2 =val(p2$)+1 : p3 =val(p3$)+1
 
 if bp+mp then co++ : co$[co]= prop$ : nco[co]= bp+mp : nbco[co]= bp
 
 if bp+mp=3
   for z=0 to 9
      if z<>p1-1 & z<>p2-1 & z<>p3-1 then ca=z+1 : gosub cat3
   next

 elseif bp+mp=0
     ca=p1 : gosub cat3 : ca=p2 : gosub cat3 : ca=p3 : gosub cat3 : dprop$ += op$
     
 elseif bp & !mp
     cat[p1,2]=0 : cat[p1,3]=0 : cat[p2,1]=0 : cat[p2,3]=0 : cat[p3,1]=0 : cat[p3,2]=0 : dprop$ += op$

 elseif mp & !bp
     cat[p1,1]=0 : cat[p2,2]=0 : cat[p3,3]=0
     if mp<3 then dprop$ += op$
     
 endif

 for y=1 to 3     % construit chaine des chiffres possibles par ligne (1-3)
   c$[y] =""
   for x=1 to 10
      if cat[x,y]<>0 then c$[y]+=int$(x-1)+" "
   next
 next
return

cat3:   % met cat[ca] à 0 
  cat[ca,1]=0 : cat[ca,2]=0 : cat[ca,3]=0
return

Verif:
nprop++
prop$ =replace$(prop$," ","")   % enlève les espaces qui étaient seulement pour l'affichage.
if nprop<10
  prx =val(word$("1 4 7 1 4 7 1 4 7",nprop))    % pour afficher la nouvelle proposition au bon endroit.
  pry =val(word$("1 1 1 2 2 2 3 3 3",nprop))
  for n=1 to 3 : gr.modify prp[prx,pry], "text", mid$(prop$,n,1) : prx++ : next
endif
s1$ =mid$(secret$,1,1) : s2$ =mid$(secret$,2,1) : s3$ =mid$(secret$,3,1)
p1$ =mid$(prop$,1,1) : p2$ =mid$(prop$,2,1) : p3$ =mid$(prop$,3,1)
bp =(s1$=p1$) + (s2$=p2$) + (s3$=p3$)
mp =(s1$=p2$ | s1$=p3$) + (s2$=p1$ | s2$=p3$) + (s3$=p1$ | s3$=p2$)

if nprop<10
  prx =val(word$("1 2 3 1 2 3 1 2 3",nprop))    % détermine l'endroit des réponses pour cette 'nprop' proposition.
  pry =val(word$("1 1 1 2 2 2 3 3 3",nprop))
  if bp then gr.modify rep[prx,pry,1], "text", word$("Un Deux Trois",bp)+" bien placé"+word$("s",(bp>1))
  if mp then gr.modify rep[prx,pry,2], "text", word$("Un Deux Trois",mp)+" mal placé"+word$("s",(mp>1))
  if bp+mp=0 then gr.modify rep[prx,pry,1], "text", "rien"
endif
if bp=3
  gr.modify mess, "text", "BRAVO !"
  if human & scrd>scrmx then scrmx=scrd : gr.modify scm, "text", int$(scrmx) : fin =1
elseif nprop=10
  gr.modify mess, "text", "Domage! le code était "+secret$ : fin =1
else
  if human then prop$ ="CODE ?"
endif
return

bmpMaker:
gr.bitmap.load fond1, path$ + "carbon-kevlar.jpg"   % 190 x189
!gr.bitmap.load fond2, path$ + "fibres-texture.jpg"  % 626 x626
gr.bitmap.load clav, path$ + "Clavier2.png"         % 1459x1456
Font.load ft1, path$ + "EHSMB.TTF"
Font.load ft2, path$ + "digital-7.ttf"
Font.load ft3, path$ + "Batavia.TTF"
!gr.bitmap.scale fd, fond2, 800, 800
gr.bitmap.scale cla, clav, 620, 700
gr.bitmap.delete clav
return

initGame:
gr.text.setFont ft1
!gr.bitmap.draw nul, fd, 0,0 : gr.bitmap.draw nul, fd, 800,0
!
for y=1 to int(scy/189)+1
  for x=1 to int(scx/190)+1
    gr.bitmap.draw nul, fond1, (x-1)*190, (y-1)*189
  next
next
!
clx =660 : cly =95
gr.bitmap.draw nul, cla, clx,cly
gr.set.stroke 3
gr.color 255,255,144,0,1
gr.text.size 55
gr.text.align 2
gr.text.draw nul, scx/2, 70, "Code a trouver"
gr.color 255,0,210,240,1
gr.text.size 35
gr.text.align 1
gr.text.draw nul, 10, 160, "Score :          Best :"
gr.color 255,200,200,200,2
RectR(scx-80,18, 8, 60,8, 2)   % menu
RectR(scx-80,38, 8, 60,8, 2)
RectR(scx-80,58, 8, 60,8, 2)

ccx =220 : ccy =200        % tous les rectangles réponses.
for y=1 to 3
  for x=1 to 3
    gr.color 255,100,100,100,2
    RectR(10+(x-1)*ccx,200+(y-1)*ccy, 25, ccx-20,ccy-20, 2)
    gr.color 255,238,238,238,2
    for c=1 to 3
      RectR(10+(x-1)*ccx+10+(c-1)*64,200+(y-1)*ccy+10, 20, 52,70, 2)
    next
    gr.color 255,238,238,238,0
    RectR(10+(x-1)*ccx,200+(y-1)*ccy, 25, ccx-20,ccy-20, 0)
  next
next

gr.color 255,110,91,251,2    % violet
gr.rect nul, clx+144,cly+61, clx+144+331,cly+61+90

prop$ ="CODE ?" : nprop =0
gr.text.size 70
gr.text.align 2
gr.color 255,0,0,0,1
gr.text.draw cod, clx+144+331/2, cly+61+90-15, prop$

dtx =clx+81 : dty =cly+234 : etx =152 : ety =103

gr.text.setFont ft2

scrd =1000
gr.text.size 60
gr.text.align 1
gr.color 255,0,255,12,1   %  vert
gr.text.draw sco, 180, 160, "1000"
gr.text.draw scm, 520, 160, int$(scrmx)

gr.text.size 80
gr.color 255,0,0,0,1         % les codes à vérifier
for lry=1 to 3 : c =1
  for lrx=1 to 9
    gr.text.draw prp[lrx,lry], 26+(c-1)*65, 272+(lry-1)*ccy, "" : c++ : if lrx=3 | lrx=6 then c+=0.38
  next
next

gr.text.setFont ft3
gr.text.size 18
gr.color 255,255,255,0,1      % les réponses textes
for repy=1 to 3
  for repx =1 to 3
    gr.text.draw rep[repx,repy,1], 23+(repx-1)*220, 326+(repy-1)*ccy, ""
    gr.text.draw rep[repx,repy,2], 23+(repx-1)*220, 360+(repy-1)*ccy, ""
  next
next
gr.set.stroke 2
gr.text.size 22
gr.text.align 2
gr.text.draw mess, clx+310, cly+40, ""

gr.color 255,0,255,12,0
RectR(10,10, 20, 200,40, 0)
gr.color 255,0,255,12,1 : gr.text.draw huma, 105, 40, word$("A.I. play;Human play",human+1,";")
return

