Fn.def stg$(c)
  Fn.rtn Left$(Str$(c),Is_In(".",Str$(c))-1)
Fn.end

Fn.def xy2lt$(x,y)      % set lt$ en fonction de x,y
  Fn.rtn mid$("ABCDEFGHIJKLMNOPQRSTUVWXY", (y-1)*5+x, 1)
Fn.end

Fn.def lt2x(lt$)       % set x en fonction de lt$
  vt = ascii(upper$(lt$))-64
  if mod(vt,5) then Fn.rtn mod(vt,5) else Fn.rtn 5
Fn.end

Fn.def lt2y(lt$)     % set y en fonction de lt$
  vt = ascii(upper$(lt$))-64
  if mod(vt,5) then Fn.rtn floor(vt/5)+1 else Fn.rtn vt/5
Fn.end

Fn.def lang$()
SYSTEM.OPEN
SYSTEM.WRITE "getprop"
PAUSE 100
DO
  SYSTEM.READ.LINE l$
  SYSTEM.READ.READY ready
  fd= is_in("language",l$)
UNTIL !ready | fd
SYSTEM.CLOSE
if fd then Fn.rtn mid$(word$(l$,2,":"),3,2)
Fn.end

GR.OPEN 255,120,75,18,1,0    %  dark green

! LOAD AND SCALE GRAPHICS !

GR.SCREEN real_w, real_h
di_w = 800+600
di_h = 800+36
scale_w = real_w/di_w
scale_h = real_h/di_h
GR.SCALE scale_w, scale_h
Path$ = "Neutron/"

! load bitmaps
GR.BITMAP.LOAD plaBmp,path$+"Plateau.png"   % 800x800
GR.BITMAP.LOAD mainBmp,path$+"Main2.png"
GR.BITMAP.LOAD tmpBmp,path$+"Boules.png"

gr.bitmap.crop bouleN,tmpBmp,0,  0, 111,111
gr.bitmap.crop bouleB,tmpBmp,0,111, 111,111
gr.bitmap.crop bouleR,tmpBmp,0,222, 111,111
gr.bitmap.crop boulBB,tmpBmp,0,333, 111,111
gr.bitmap.crop boulBN,tmpBmp,0,444, 111,111
gr.bitmap.delete tmpBmp

! CONSTANTS !

! si "fr" => langue=2 sinon 1 anglais
language  = 2-1*(lang$()<>"fr")
DIM cell[5,5,3]
array.load dix[],0,1,1,1,0,-1,-1,-1    % all 8 directions start to 1= North
array.load diy[],-1,-1,0,1,1,1,0,-1
ox= (real_w-680)/2
oy= 36
dcell= 800/5
dcl= (dcell-111)/2

! INIT !

New:
unDim cell[]
DIM cell[5,5,3]
undo$= ""
undo = 0
player= 2    %  the player 2 (bottom) always start.
cycle= 4
NeutronX= 3
NeutronY= 3

gr.text.size dcell/2     %  the title
gr.text.bold 5
gr.text.skew -0.25
for tt=1 to 7
  gr.color 180,0,0,0,1
  gr.text.draw nul, dcell/4, oy+tt*dcell/1.5, mid$("NEUTRON",tt,1)
  gr.color 255,255,120,50,1
  gr.text.draw nul, dcell/4-5, oy+tt*dcell/1.5-5, mid$("NEUTRON",tt,1)
next
gr.bitmap.draw plaPtr, plaBmp, ox, oy
gr.bitmap.draw mainPtr, mainBmp, ox-dcell+60, oy+4*dcell+50
gr.bitmap.draw bt1, boulBB, ox+6*dcell, dcl+oy+20
gr.bitmap.draw bt3, boulBN, ox+6*dcell, dcl+oy+2*dcell
gr.bitmap.draw bt2, boulBB, ox+6*dcell, dcl+oy+4*dcell-20

gr.color 90,255,255,0,1
gr.text.size dcell/2
gr.text.draw nul, 2*dcell-110, real_h/2+94, "?"

gr.color 255,255,255,0,1
gr.text.size dcell/5
gr.text.bold 3
gr.text.skew -0.12
gr.text.draw win1, ox+6*dcell-50, oy+2*dcell-30, "White  WIN"
gr.text.draw win2, ox+6*dcell-50, oy+3*dcell+55, "Black  WIN"
gr.hide win1
gr.hide win2

gr.text.size dcell/4.5       %  buttons
gr.text.bold 0
gr.text.skew -0.12
gr.color 180,0,0,0,1
gr.text.draw nul, ox+6*dcell+11, dcl+oy+86, "Reset"
gr.color 255,255,120,50,1
gr.text.draw nul, ox+6*dcell+11+3, dcl+oy+86+3, "Reset"
gr.color 180,0,0,0,1
gr.text.draw nul, ox+6*dcell+11, dcl+oy+4*dcell+48, "Undo"
gr.color 255,255,120,50,1
gr.text.draw nul, ox+6*dcell+11+3, dcl+oy+4*dcell+48+3, "Undo"
gr.color 255, 150, 105, 26, 1

GOSUB links    %  all possibles trajectories
for cy=0 to 4
  for cx=0 to 4
    gr.circle nul, ox+dcell/2+cx*dcell, oy+dcell/2+cy*dcell, 10
  next
next
gr.color 255, 255, 255, 0, 1
for cy=0 to 4
  for cx=0 to 4
    if cy=0
      gr.bitmap.draw cell[cx+1,cy+1,2], bouleB, dcl+ox+cx*dcell, dcl+oy+cy*dcell
      cell[cx+1,cy+1,1]= 1
    endif
    if cy=2 & cx=2
      gr.bitmap.draw cell[cx+1,cy+1,2], bouleR, dcl+ox+cx*dcell, dcl+oy+cy*dcell
      cell[cx+1,cy+1,1]= 3
    endif
    if cy=4
      gr.bitmap.draw cell[cx+1,cy+1,2], bouleN, dcl+ox+cx*dcell, dcl+oy+4*dcell
      cell[cx+1,cy+1,1]= 2
    endif
    gr.circle cell[cx+1,cy+1,3], ox+dcell/2+cx*dcell, oy+dcell/2+cy*dcell, 11
    gr.hide cell[cx+1,cy+1,3]
  next
next
gr.circle cursR, ox+dcell/2+2*dcell, oy+dcell/2+2*dcell, 14
gr.modify cursR, "alpha", 0
GOSUB MakeHelp
if reverseC then GOSUB ReverseColors

DO
  gr.modify mainPtr, "y", oy+(4*(player-1))*dcell+50   % the hand
  loop:
  DO
    GR.TOUCH touched,tx,ty    % wait for touch
    pause 5
    if cycle= 1 | cycle= 3
      hoverAlpha += 3*hoverPulse
      if hoverAlpha=0 | hoverAlpha=84 then hoverPulse *= -1
      gr.modify cursR, "alpha", hoverAlpha
    endif
    if !background() then gr.render
  UNTIL touched
  if help
    help=0
    gr.hide helpP
    goto loop
  endif
  tx/=scale_w
  ty/=scale_h
  cx= FLOOR((tx-ox)/dcell)+1
  cy= FLOOR((ty-oy)/dcell)+1
  move= 0
  if cx>0 & cx<6 & cy>0 & cy<6
    if (cell[cx,cy,1]=player & (cycle= 2 | cycle=4)) | (cell[cx,cy,1]=3 & (cycle= 1 | cycle=3))
      hoverAlpha=0
      hoverPulse=1
      gr.get.position cell[cx,cy,2],px,py
      deltaX= px-tx
      deltaY= py-ty
      move=1
      GOSUB possible     % to show all possibles moves
      le = len(poss$)
      while le
        sx = lt2x(mid$(poss$,le,1))
        sy = lt2y(mid$(poss$,le,1))
        gr.show cell[sx,sy,3]
        le--
      repeat
    endif
  endif   
  DO
    GR.TOUCH touched, dx,dy    % wait for untouch
    PAUSE 5
    dx/=scale_w
    dy/=scale_h
    if move
      gr.modify cell[cx,cy,2], "x", dx+deltaX
      gr.modify cell[cx,cy,2], "y", dy+deltaY
      gr.render
    endif    
  UNTIL !touched
  
  if len(poss$)
    le=0
    do       %  hide all possibles moves
      le++
      sx = lt2x(mid$("ABCDEFGHIJKLMNOPQRSTUVWXY",le,1))
      sy = lt2y(mid$("ABCDEFGHIJKLMNOPQRSTUVWXY",le,1))
      gr.hide cell[sx,sy,3]
    until le= 25
  endif
  
  if move
    GOSUB moveBall   % and chge cycle
    GOSUB CtrlEOG   % ctrl the end of game.
  
  elseif cx=0 & cy=3
    help=1
    gr.show helpP
    
  elseif cx=7 & cy=1        % reset button
    gr.cls
    goto New
    
  elseif cx=7 & cy=3   % the bi-color button
    reverseC=1-reverseC
    GOSUB ReverseColors

  elseif cx=7 & cy=5   % undo button
    lu= len(undo$)
    if lu
      gr.modify cursR, "alpha", 0
      cx= lt2x( mid$(undo$,lu,1))
      cy= lt2y( mid$(undo$,lu,1))
      ax= lt2x( mid$(undo$,lu-1,1))
      ay= lt2y( mid$(undo$,lu-1,1))
      GOSUB movement
      swap cell[cx,cy,1], cell[ax,ay,1]
      swap cell[cx,cy,2], cell[ax,ay,2]
      undo--
      undo$= left$(undo$,lu-2)
      cycle= cycle-1+4*(cycle=1)
      if cycle=2 | cycle=4
        player=3-player
      endif
      if cell[ax,ay,1]=3
        gr.modify cursR,"x", ox+dcell/2+(ax-1)*dcell
        gr.modify cursR,"y", oy+dcell/2+(ay-1)*dcell
        gr.modify cursR,"alpha",0
        NeutronX=ax
        NeutronY=ay
      endif
    endif
  endif
UNTIL 0

CtrlEOG:
! where is Neutron and who WIN ?
tts=0
cx = NeutronX
cy = NeutronY
GOSUB possible   % make poss$
if poss$=""
  if player=1 then tts=win2 else tts=win1
else
  w$= xy2lt$(NeutronX,NeutronY)
  if Is_In(w$,"ABCDE")
    tts=win1
  elseif Is_In(w$,"UVWXY")
    tts=win2
  endif
endif
if tts=win1
  gr.modify win1, "text", word$("White Black",reverseC+1)+"  WIN"
  gr.show win1
elseif tts=win2
  gr.modify win2, "text", word$("Black White",reverseC+1)+"  WIN"
  gr.show win2
endif
RETURN

ReverseColors:
j1$=""
j2$=""
for cy=1 to 5
  for cx=1 to 5
    if cell[cx,cy,1]=1
      j1$+= xy2lt$(cx,cy)
    elseif cell[cx,cy,1]=2
      j2$+= xy2lt$(cx,cy)
    endif
  next
next
for r=1 to 5
  x1=lt2x(mid$(j1$,r,1))
  y1=lt2y(mid$(j1$,r,1))
  x2=lt2x(mid$(j2$,r,1))
  y2=lt2y(mid$(j2$,r,1))
  if reverseC
    gr.modify cell[x1,y1,2], "bitmap", bouleN
    gr.modify cell[x2,y2,2], "bitmap", bouleB
  else
    gr.modify cell[x1,y1,2], "bitmap", bouleB
    gr.modify cell[x2,y2,2], "bitmap", bouleN
  endif
next
if reverseC
  if rotaBmp then gr.bitmap.delete rotaBmp
  gr.bitmap.create rotaBmp, 111, 111
  gr.bitmap.drawinto.start rotaBmp
  gr.rotate.start 180, 55, 55
   gr.bitmap.draw nul, boulBN, 0,0
  gr.rotate.end
  gr.bitmap.drawinto.end
  gr.modify bt3, "bitmap", rotaBmp
else
  gr.modify bt3, "bitmap", boulBN
endif
RETURN

moveBall:
! detect the direction with tx,ty & dx,dy
!  8 1 2
!  7   3
!  6 5 4
dmax = dcell/3
ori  = 0
distx = abs(dx-tx)
disty = abs(dy-ty)
if distx < dmax & disty > dmax & dy < ty
  ori = 1
elseif distx > dmax & disty > dmax & dx > tx & dy < ty
  ori = 2
elseif distx > dmax & disty < dmax & dx > tx
  ori = 3
elseif distx > dmax & disty > dmax & dx > tx & dy > ty
  ori = 4
elseif distx < dmax & disty > dmax & dy > ty              % direction detection...
  ori = 5
elseif distx > dmax & disty > dmax & dx < tx & dy > ty
  ori = 6
elseif distx > dmax & disty < dmax & dx < tx
  ori = 7
elseif distx > dmax & disty > dmax & dx < tx & dy < ty
  ori = 8
endif
ax= cx
ay= cy
if ori
  drx= cx
  dry= cy
  while (drx+dix[ori])<6 & (drx+dix[ori])>0 & (dry+diy[ori])<6 & (dry+diy[ori])>0
    if cell[drx+dix[ori], dry+diy[ori], 1] then W_r.break
    drx+= dix[ori]
    dry+= diy[ori]
  repeat
  if drx<>cx | dry<>cy
    ax= drx
    ay= dry
  endif
endif
GOSUB movement
if ax<>cx | ay<>cy
  swap cell[cx,cy,1], cell[ax,ay,1]
  swap cell[cx,cy,2], cell[ax,ay,2]
  undo$= undo$+ xy2lt$(cx,cy) +xy2lt$(ax,ay)
  undo++
  cycle= cycle+1-4*(cycle=4)
  if cycle=1 | cycle=3
    player=3-player
  endif
  if cell[ax,ay,1]=3
    gr.modify cursR,"x", ox+dcell/2+(ax-1)*dcell
    gr.modify cursR,"y", oy+dcell/2+(ay-1)*dcell
    gr.modify cursR,"alpha",0
    NeutronX=ax
    NeutronY=ay
  endif
endif
RETURN

movement:
 ! slide the ball from cx,cy to ax,ay
 destx = dcl+ox+(ax-1)*dcell
 desty = dcl+oy+(ay-1)*dcell
 do
    gr.get.position cell[cx,cy,2],px,py
    ix = floor((px - destx)/2)
    iy = floor((py - desty)/2)
    if abs(ix)<5 & abs(iy)<5 then D_u.break
    gr.modify cell[cx,cy,2], "x", px - ix
    gr.modify cell[cx,cy,2], "y", py - iy
    gr.render
    pause 5
 until 0
 gr.modify cell[cx,cy,2], "x", destx
 gr.modify cell[cx,cy,2], "y", desty
 gr.hide win1
 gr.hide win2
 gr.render
RETURN

possible:
poss$=""     % string of all moves for cx,cy
for i=1 to 8
  drx=cx
  dry=cy
  while (drx+dix[i])<6 & (drx+dix[i])>0 & (dry+diy[i])<6 & (dry+diy[i])>0
    if cell[drx+dix[i], dry+diy[i], 1] then W_r.break
    drx+=dix[i]
    dry+=diy[i]
  repeat
  if drx<>cx | dry<>cy
    poss$ = poss$ + xy2lt$(drx,dry)
  endif
next
RETURN

links:
  li$ = "AE FJ KO PT UY AU BV CW DX EY FB KC PD UE VJ WO XT DJ CO BT AY FX KW PV"
  ctr= dcell/2
  for c=1 to 24
    lt$ = left$(word$(li$,c),1)
    dx= lt2x(lt$)-1
    dy= lt2y(lt$)-1
    lt$ = right$(word$(li$,c),1)
    ax= lt2x(lt$)-1
    ay= lt2y(lt$)-1
    gr.line nul, ox+ctr+dx*dcell, oy+ctr+dy*dcell, ox+ctr+ax*dcell, oy+ctr+ay*dcell
  next
RETURN

MakeHelp:
! the NeutronHelp.txt file must have a pair number of line.
GRABFILE LdLvl$, path$+"NeutronHelp.txt"
SPLIT help$[], LdLvl$, "\n"    % CHR$(10)  end of line as separator.
Array.length nL, help$[]
if !nL then return
stl=0
if language=2 then stl=floor(nL/2)
if helpPtr then gr.bitmap.delete helpPtr
Gr.bitmap.create helpPtr, real_w+70, real_h+50
Gr.bitmap.drawinto.start helpPtr
GR.COLOR 220,55,55,55,1   % text color yellow
gr.rect nul, 0, 0, real_w+70, real_h+30
gr.color 255,255,255,0,1
gr.text.size dcell/5
for L=1 to floor(nL/2)
  gr.text.draw nul, 20, L*35, help$[stl+L]
next
array.delete help$[]
Gr.bitmap.drawinto.end
gr.bitmap.draw helpP, helpPtr, 20, oy+20
gr.hide helpP
RETURN

ONBACKKEY:
POPUP "Goodbye",0,0,0
PAUSE 1000
OnError:
GR.CLOSE
END

