! Baller game clone
! 
! public domain demo game of BASIC!
!
FN.DEF buttons$()
 a=100
 GR.SCREEN w,h
 whalf=FLOOR(w/2)
 hhalf=FLOOR(h/2)
 DIM pad[2]
 ARRAY.LOAD b$[],"play again","quit"
 sq=h/8

 FOR i=1 TO 2
  GR.TEXT.DRAW pad[i],whalf,hhalf+hhalf*i*0.3,b$[i]
 NEXT i

 found=0
 DO
  FOR i=1 TO 2
   GR.GET.POSITION pad[i],sx,sy
   GR.BOUNDED.TOUCH touched,sx-sq,sy-sq,sx+sq,sy+sq
   IF touched
    LET found=1
    F_N.BREAK
   ENDIF
   a+=4
   IF a>255 THEN a=100
   GR.MODIFY pad[i],"alpha",a
  NEXT I
  GR.RENDER
  PAUSE 50
 UNTIL found
 FN.RTN b$[i]
FN.END


Nbox=8


DIM bx[nbox],by[nbox] % box coords
DIM vx[nbox],vy[nbox] % box velocities
DIM bh[nbox],bw[nbox] % box dimensions
DIM box[nbox] % box objects

POPUP "tap and hold the screen anywhere to move the ball up.  avoid hitting all blue walls",0,0,1
Playagain:
lives=5
score=0
Cantouch=0
Games++

GR.OPEN 255,0,0,0,0,1
pause 1000
GR.SET.ANTIALIAS 1
GR.SCREEN w,h
whalf=FLOOR(w/2)
hhalf=FLOOR(h/2)


FOR i=1 TO nbox
 vx[i]=5+FLOOR(RND()*10)
 IF FLOOR(RND()*2)=1 THEN vx[i]=-vx[i]
 vy[i]=0
 bx[i]=FLOOR(RND()*w)
 by[i]=FLOOR(RND()*(h-50))
 bw[i]=80+10*FLOOR(RND()*6)
 bh[i]=FLOOR(h/80)
NEXT i

gr.render



gameend=0



GR.COLOR 255,100,100,100,1 % grey frame
GR.SET.STROKE 0


score=0
lives=5
GR.SET.STROKE 0

FOR i=1 TO nbox
 GR.COLOR 255,0,0,255,1 % blue boxes
 GR.RECT box[i],bx[i],by[i],bx[i]+bw[i],by[i]+bh[i]
 GR.SHOW box[i]
NEXT i

GR.TEXT.TYPEFACE 2
GR.COLOR 255,200,200,00,1
GR.TEXT.SIZE 40
GR.TEXT.BOLD 1


sx=w/2
sy=h


! red player
GR.COLOR 255,255,0,0,1
GR.CIRCLE player,sx,sy,h/40
GR.SHOW player

GR.RENDER
DO
 GR.TOUCH touched, x,y
UNTIL touched

tstart=CLOCK()

Cantouch=1
tic=CLOCK()
!------------------------------------------
DO

 ! process blue boxes ---------------
 FOR i=1 TO nbox
  GR.MOVE box[i],vx[i],0
  GR.GET.POSITION box[i],fx,fy

  IF fx>w+150
   fx=-150
   fy=FLOOR(RND()*(h-50))
   GR.MODIFY box[i],"left",fx,"right",fx+bw[i]
   GR.MODIFY box[i],"top",fy,"bottom",fy+bh[i]

  ELSE if fx<-150
   fx=w+150
   fy=FLOOR(RND()*(h-50))
   GR.MODIFY box[i],"left",fx,"right",fx+bw[i]
  ENDIF
  GR.MODIFY box[i],"top",fy,"bottom",fy+bh[i]

  IF GR_COLLISION(player,box[i]) 
   lives--
   GR.MODIFY player,"y",h
   TONE 9000,50,0
  ENDIF
 NEXT i

 !process touch ---------------------
 GR.TOUCH touched,x,y
 IF touched THEN GOSUB hold

 GR.RENDER

 !timing ---------------
 LET toc=CLOCK()-tic
 PAUSE MAX(33-toc,1)
 LET tic=CLOCK()

UNTIL lives=0
!-------------------------------------



GR.SET.ANTIALIAS 1
GR.COLOR 255,0,255,0,1  
GR.TEXT.SIZE 40
GR.TEXT.ALIGN 2
GR.TEXT.DRAW txtend,whalf,hhalf*0.2,"game over"
GR.TEXT.ALIGN 2
GR.TEXT.DRAW txtend,whalf,hhalf*0.4,"score "+INT$(score)


DO
 GR.TOUCH touched,x,y
 PAUSE 100
UNTIL !touched

b$=buttons$()



IF b$="play again" THEN
 GR.CLOSE
 PAUSE 300
 Tend=0
 drag=0
 GOTO playagain
ENDIF

EXIT

!-----------------------------------


hold:
GR.MOVE player,0,-12
GR.GET.POSITION player,x,y
IF y<1 
 TONE 440,50,0
 score++
 GR.MODIFY player,"y",h
ENDIF
RETURN
!-----------------------------------
ONERROR:
GR.CLOSE
PRINT err$
