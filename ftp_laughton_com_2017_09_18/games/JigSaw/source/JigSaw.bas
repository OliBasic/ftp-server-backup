! JigSaw.bas
! Aat Don 2014
GR.OPEN 255,92,192,92,0,0
PAUSE 1000
GR.SCREEN w,h
ScaleX=900
ScaleY=h/w*ScaleX
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
WAKELOCK 3
DataPath$="../../JigSaw/data/"
PicXOffSet=20
PicYOffSet=20
PuzzleSize=450
SOUNDPOOL.OPEN 1
SOUNDPOOL.LOAD Win95,DataPath$+"Win95.mp3"
SOUNDPOOL.LOAD Spin,DataPath$+"Spin.mp3"
SOUNDPOOL.LOAD Tick,DataPath$+"Tick.mp3"
SOUNDPOOL.LOAD Magic,DataPath$+"Magic.mp3"
DIM VizPic[3]
! Create Menu
GR.BITMAP.CREATE MenuBMP,240,320
GR.COLOR 200,0,255,0,0
GR.SET.STROKE 5
GR.TEXT.SIZE 40
GR.TEXT.BOLD 1
GR.TEXT.ALIGN 2
GR.BITMAP.DRAWINTO.START MenuBMP
	GR.COLOR 200,255,255,255,1
	GR.RECT g,0,0,240,320
	GR.COLOR 200,0,255,0,0
	GR.RECT g,3,3,317,317
	GR.LINE g,3,80,237,80
	GR.LINE g,3,160,237,160
	GR.LINE g,3,240,237,240
	GR.SET.STROKE 1
	GR.COLOR 200,0,0,0,1
	GR.TEXT.DRAW g,120,60,"CANCEL"
	GR.TEXT.DRAW g,120,140,"EXIT"
	GR.TEXT.DRAW g,120,220,"SAVE & EXIT"
	GR.TEXT.DRAW g,120,300,"Solve Puzzle"
GR.BITMAP.DRAWINTO.END
GR.TEXT.ALIGN 1
! Intro
LIST.CREATE N,MakePiece
LIST.ADD MakePiece,0,56,85,56,77,49,72,40,70,30,72,20,77,11,85,4,95,0,105,0,115,4,123,11,128,20,130,30,128,40,123,49,115,56,200,56,200,141,193,133,184,128
LIST.ADD MakePiece,174,126,164,128,155,133,148,141,144,151,144,161,148,171,155,179,164,184,174,186,184,184,193,179,200,171,200,256,115,256,123,263,128,272
LIST.ADD MakePiece,130,282,128,292,123,301,115,308,105,312,95,312,85,308,77,301,72,292,70,282,72,272,77,263,85,256,0,256,0,171,7,179,16,184,26,186,36,184
LIST.ADD MakePiece,45,179,52,171,56,161,56,151,52,141,45,133,36,128,26,126,16,128,7,133,0,141
GR.SET.ANTIALIAS 1
GR.TEXT.SIZE 55
GR.TEXT.BOLD 1
GR.SET.STROKE 4
GR.BITMAP.CREATE IntroPiece,201,313
GR.BITMAP.DRAWINTO.START IntroPiece
	FOR i=0 TO 1
		GR.COLOR 255,(i=0)*255,0,(i=1)*255,(i=0)*1
		GR.POLY g,MakePiece
		GR.COLOR 255,(i=1)*255,255,0,(i=0)*1
		GR.TEXT.DRAW g,10,110,"JigSaw"
		GR.TEXT.DRAW g,20,240,"Puzzle"
	NEXT i
GR.BITMAP.DRAWINTO.END
PAUSE 500
SOUNDPOOL.PLAY Snd1,Win95,0.99,0.99,1,0,1
FOR i=0.5 TO 1.7 STEP 0.1
	GR.CLS
	XSZ=201*i
	YSZ=313*i
	GR.BITMAP.SCALE BigPiece,IntroPiece,XSZ,YSZ
	GR.ROTATE.START i*225,ScaleX/2,ScaleY/2
		GR.BITMAP.DRAW g,BigPiece,ScaleX/2-XSZ/2,ScaleY/2-YSZ/2
	GR.ROTATE.END
	GR.RENDER
	TONE i*500,100,0
	PAUSE 100
NEXT i
GR.ROTATE.START 90,ScaleX/2-XSZ,ScaleY/2
	GR.BITMAP.DRAW g,BigPiece,ScaleX/2-XSZ/2-XSZ,ScaleY/2-YSZ/2
GR.ROTATE.END
GR.ROTATE.START 270,ScaleX/2+XSZ,ScaleY/2
	GR.BITMAP.DRAW g,BigPiece,ScaleX/2-XSZ/2+XSZ,ScaleY/2-YSZ/2
GR.ROTATE.END
GR.RENDER
GR.TEXT.SIZE 50
GR.BITMAP.CREATE FinPiece1,201,313
GR.BITMAP.DRAWINTO.START FinPiece1
	FOR i=0 TO 1
		GR.COLOR 255,0,(i=0)*255,(i=1)*255,(i=0)*1
		GR.POLY g,MakePiece
	NEXT i
	GR.COLOR 255,0,0,255,1
	GR.TEXT.DRAW g,40,120,"NEW"
	GR.TEXT.DRAW g,5,240,"PUZZLE"
GR.BITMAP.DRAWINTO.END
GR.BITMAP.SCALE StrtPiece,FinPiece1,80,120
GR.BITMAP.CREATE FinPiece2,313,313
GR.BITMAP.DRAWINTO.START FinPiece2
	FOR i=0 TO 1
		GR.COLOR 255,(i=0)*255,0,(i=1)*255,(i=0)*1
		GR.ROTATE.START 90,156,156
			GR.POLY g,MakePiece
		GR.ROTATE.END
	NEXT i
	GR.COLOR 255,255,255,0,1
	GR.TEXT.DRAW g,95,120,"QUIT"
GR.BITMAP.DRAWINTO.END
GR.BITMAP.CROP FP,FinPiece2,0,0,313,201
GR.BITMAP.SCALE StpPiece,FP,120,80
POPUP "Loading....",0,0,0
PAUSE 2000
SOUNDPOOL.UNLOAD Win95
! Recover saved game
FILE.EXISTS SavedGame,DataPath$+"SaveGame.pzl"
IF SavedGame THEN
	POPUP "Saved Puzzle found, loading....",0,0,0
	TEXT.OPEN R,SG,DataPath$+"SaveGame.pzl"
		TEXT.READLN SG,FileName$
		GR.BITMAP.LOAD OrgPuzzle,DataPath$+FileName$
		GR.BITMAP.SCALE UsePuzzle,OrgPuzzle,PuzzleSize,PuzzleSize
		GR.CLS
		TEXT.READLN SG,Mask$
		Mask=VAL(Mask$)
		TEXT.READLN SG,Size$
		Size=VAL(Size$)
		StrokeSet=19/Size+2
		GOSUB SetVars
		FOR i=1 TO NumPieces
			TEXT.READLN SG,Line$
			PlacePieceX[i]=VAL(Line$)
			TEXT.READLN SG,Line$
			PlacePieceY[i]=VAL(Line$)
		NEXT i
		FOR i=1 TO NumPieces
			TEXT.READLN SG,Line$
			PieceNr[i]=VAL(Line$)
		NEXT i
	TEXT.CLOSE SG
	FILE.DELETE g,DataPath$+"SaveGame.pzl"
	GOTO ContSaved
ENDIF
NwPuzzle:
! Size 3,5,7 or 9
Size=3
GR.SET.ANTIALIAS 1
GR.TEXT.SIZE 40
GR.TEXT.BOLD 1
Start=0
SelPuzzle=0
MenuOption=1
! Get Puzzles
GR.CLS
FILE.DIR DataPath$,Files$[]
ARRAY.LENGTH L,Files$[]
DIM Pics$[L]
PCount=0
FOR i=1 TO L
	IF LOWER$(RIGHT$(Files$[i],3))="png" | LOWER$(RIGHT$(Files$[i],3))="jpg" THEN
		PCount=PCount+1
		Pics$[PCount]=Files$[i]
	ENDIF
NEXT i
IF PCount=0 THEN
	POPUP "No Pictures found !"
	GOTO PuzzleQuit
ENDIF
ARRAY.COPY Pics$[1,PCount],PicsFound$[]
ARRAY.DELETE Files$[]
ARRAY.DELETE Pics$[]
ARRAY.DELETE OrgPuzzle[]
ARRAY.DELETE ScaledPuzzle[]
DIM OrgPuzzle[PCount]
DIM ScaledPuzzle[PCount]
MenuXOffset=(ScaleX-3*PuzzleSize/2)/4
MenuYOffset=(ScaleY-PuzzleSize/2)/2
GR.TEXT.SIZE 50
GR.TEXT.BOLD 1
GR.TEXT.ALIGN 2
GR.COLOR 255,255,255,0,1
! Read pictures
IF PCount>3 THEN
	ShowNum=3
	GR.TEXT.DRAW g,250,100,"<"
	GR.TEXT.DRAW g,450,100,"Swipe"
ELSE
	ShowNum=PCount
ENDIF
GR.TEXT.DRAW g,450,MenuYOffset+PuzzleSize/2+100,"Tap picture to select"
FOR i=1 TO ShowNum
	GR.BITMAP.LOAD OrgPuzzle[i],DataPath$+PicsFound$[i]
	GR.BITMAP.SCALE ScaledPuzzle[i],OrgPuzzle[i],PuzzleSize/2,PuzzleSize/2
	GR.BITMAP.DRAW VizPic[i],ScaledPuzzle[i],MenuXOffset+MenuXOffset*(i-1)+PuzzleSize*((i-1)*0.5),MenuYOffset
NEXT i
FOR j=PuzzleSize/4 TO 0 STEP -PuzzleSize/16
	FOR i=1 TO ShowNum
		GR.MODIFY VizPic[i],"x",MenuXOffset+MenuXOffset*(i-1)+PuzzleSize*((i-1)*0.5)-j*(Swiped*2-3)
	NEXT i
	GR.RENDER
	PAUSE 50
NEXT j
! Get Swipe
StartPic=1
SelPic=0
Swiped=0
DO
	DO
		DO
			GR.TOUCH Touched,x1,y1
		UNTIL Touched
		DO
			GR.TOUCH Touched,x2,y2
		UNTIL !Touched
		x1/=sx
		y1/=sy
		x2/=sx
		y2/=sy
		IF ABS(x1-x2)<5 & ABS(y1-y2)<5 & y2>MenuYOffset & y2<MenuYOffset+PuzzleSize/2 THEN
			SelPic=INT(x2/(MenuXOffset+PuzzleSize*0.5))+StartPic
			Swiped=3
		ENDIF
		IF x1-x2>100 THEN Swiped=1
		IF x2-x1>100 THEN Swiped=2
	UNTIL Swiped>0
	IF Swiped=1 & StartPic<PCount-2 & ShowNum=3 THEN
		! Move to left
		PicOffS=2
		DelPic=StartPic
		StartPic=StartPic+1
		GOSUB ShowPics
	ENDIF
	IF Swiped=2 & StartPic>1 & ShowNum=3 THEN
		! Move to right
		PicOffS=0
		DelPic=StartPic+2
		StartPic=StartPic-1
		GOSUB ShowPics
	ENDIF
UNTIL SelPic>0 & SelPic<=PCount
GR.BITMAP.SCALE UsePuzzle,OrgPuzzle[SelPic],PuzzleSize,PuzzleSize
FOR i=StartPic TO StartPic+ShowNum-1
	IF i<>SelPic THEN GR.BITMAP.DELETE OrgPuzzle[i]
	GR.BITMAP.DELETE ScaledPuzzle[i]
NEXT i
FileName$=PicsFound$[SelPIc]
ARRAY.DELETE PicsFound$[]
GR.TEXT.ALIGN 1
GR.TEXT.SIZE 40
! Get Puzzle Size
Mask=0
DO
	GR.CLS
	StrokeSet=19/Size+2
	GR.SET.STROKE StrokeSet
	GOSUB DrawBackGrnd
	GR.COLOR 192,0,0,0,1
	GR.RECT g,PicXOffSet+10,PicYOffSet+190,PicXOffSet+380,PicYOffSet+290
	GR.COLOR 255,92,92,92,0
	FOR i=1 TO Size-1
		GR.LINE g,PicXOffSet-1,PicYOffSet-1+i*(PuzzleSize/Size),PicXOffSet+PuzzleSize,PicYOffSet-1+i*(PuzzleSize/Size)
		GR.LINE g,PicXOffSet-1+i*(PuzzleSize/Size),PicYOffSet-1,PicXOffSet-1+i*(PuzzleSize/Size),PicYOffSet+PuzzleSize
	NEXT i
	T1$="Choose Puzzle Size"
	T2$="#Pieces ="+FORMAT$("##",Size*Size)
	GOSUB DrawTxt
	MenuVal=(Size-1)/2
	GOSUB GetValue
	Size=MenuVal*2+1
UNTIL Start
! Get Background type
Mask=170
Start=0
DO
	GR.CLS
	GOSUB DrawBackGrnd
	GR.COLOR 192,0,0,0,1
	GR.RECT g,PicXOffSet+10,PicYOffSet+190,PicXOffSet+380,PicYOffSet+290
	T1$="Set Background"
	T2$="visibility for puzzle"
	GOSUB DrawTxt
	MenuVal=4-(Mask/85)
	GOSUB GetValue
	Mask=255-(MenuVal-1)*85
UNTIL Start
GR.CLS
GR.SET.ANTIALIAS 0
POPUP "Please, wait... creating your puzzle...",0,0,1
GOSUB SetVars
FOR i=1 TO NumPieces
	PieceNr[i]=i
NEXT i
ARRAY.SHUFFLE PieceNr[]
ContSaved:
FOR j=1 TO Size
	FOR i=1 TO Size
		XOffSet=PieceSize*(i-1)
		YOffSet=PieceSize*(j-1)
		IF (i/2=INT(i/2) & j/2=INT(J/2)) | (i/2<>INT(i/2) & j/2<>INT(J/2)) THEN
			GR.BITMAP.CREATE Pieces[i+(j-1)*Size],PieceSize,PieceSize/3*5
			GR.BITMAP.CROP Part1,UsePuzzle,XOffSet,YOffSet,PieceSize,PieceSize/3+1
			GR.BITMAP.CROP Part2,UsePuzzle,XOffSet+PieceSize/3-15/Size,YOffSet+PieceSize/3,PieceSize/3+30/Size+1,PieceSize/3+1
			GR.BITMAP.CROP Part3,UsePuzzle,XOffSet,YOffSet+PieceSize/3*2,PieceSize,PieceSize/3+ROUND(Size/11)
			GR.BITMAP.DRAWINTO.START Pieces[i+(j-1)*Size]
				GR.BITMAP.DRAW P1,Part1,0,PieceSize/3
				GR.BITMAP.DRAW P2,Part2,PieceSize/3-15/Size,PieceSize/3*2
				GR.BITMAP.DRAW P3,Part3,0,PieceSize
				IF j>1 THEN
					cX=XOffSet+PieceSize/2
					cY=YOffSet+PieceSize/3-60/Size
					FOR angle=130 TO 270 STEP ROUND(Size*0.8-1.3)
						x1=L*COS(angle*PI()/180)+cX
						y1=L*SIN(angle*PI()/180)+cY
						x2=L*COS((540-angle)*PI()/180)+cX
						y2=L*SIN((540-angle)*PI()/180)+cY
						! Read from Puzzle
						GR.BITMAP.CROP P,UsePuzzle,x1,y1-PieceSize/3,x2-x1+1,1
						! Paste in Piece
						GR.BITMAP.DRAW g,P,x1-XOffSet,y1-YOffSet
					NEXT angle
					GR.SET.ANTIALIAS 1
					GR.COLOR 192,92,92,92,0
					GR.ARC g,PieceSize/3,L/5,PieceSize/3*2,PieceSize/3+L/5,130,280,0
					GR.LINE g,0,PieceSize/3,L*COS(130*PI()/180)+cX-XOffSet,PieceSize/3
					GR.LINE g,L*COS(310*PI()/180)+cX-XOffSet,PieceSize/3,PieceSize,PieceSize/3
					GR.COLOR 255,255,255,255,0
					GR.SET.ANTIALIAS 0
				ELSE
					! Top side
					GR.SET.ANTIALIAS 1
					GR.COLOR 192,92,92,92,0
					GR.LINE g,0,PieceSize/3,PieceSize,PieceSize/3
					GR.COLOR 255,255,255,255,0
					GR.SET.ANTIALIAS 0
				ENDIF
				IF j<Size THEN
					cX=XOffSet+PieceSize/2
					cY=YOffSet+PieceSize/3*4+60/Size
					FOR angle=90 TO 234 STEP ROUND(Size*0.8-1.3)
						x1=L*COS(angle*PI()/180)+cX
						y1=L*SIN(angle*PI()/180)+cY
						x2=L*COS((540-angle)*PI()/180)+cX
						y2=L*SIN((540-angle)*PI()/180)+cY
						! Read from Puzzle
						GR.BITMAP.CROP P,UsePuzzle,x1,y1-PieceSize/3,x2-x1+1,1
						! Paste in Piece
						GR.BITMAP.DRAW g,P,x1-XOffSet,y1-YOffSet
					NEXT angle
					GR.SET.ANTIALIAS 1
					GR.COLOR 192,92,92,92,0
					GR.ARC g,PieceSize/3,PieceSize/3*4-L/5,PieceSize/3*2,PieceSize/3*5-L/5,320,280,0
					GR.LINE g,0,PieceSize/3*4,L*COS(130*PI()/180)+cX-XOffSet,PieceSize/3*4
					GR.LINE g,L*COS(310*PI()/180)+cX-XOffSet,PieceSize/3*4,PieceSize,PieceSize/3*4
					GR.COLOR 255,255,255,255,0
					GR.SET.ANTIALIAS 0
				ELSE
					! Bottom side
					GR.SET.ANTIALIAS 1
					GR.COLOR 192,92,92,92,0
					GR.LINE g,0,PieceSize/3*4,PieceSize,PieceSize/3*4
					GR.COLOR 255,255,255,255,0
					GR.SET.ANTIALIAS 0
				ENDIF
				IF i>1 THEN
					cX=XOffSet+60/Size
					cY=YOffSet+PieceSize/2
					FOR angle=0 TO 140 STEP ROUND(Size*0.8-1.3)
						x1=L*COS(angle*PI()/180)+cX
						y1=L*SIN(angle*PI()/180)+cY
						x2=L*COS((360-angle)*PI()/180)+cX
						y2=L*SIN((360-angle)*PI()/180)+cY
						! Read from Puzzle
						GR.BITMAP.CROP P,UsePuzzle,x1,y1,1,YOffSet+PieceSize/3*2-y1+1
						! Paste in Piece
						GR.BITMAP.DRAW g,P,x1-XOffSet,y1-YOffSet+PieceSize/3
						! Read from Puzzle
						GR.BITMAP.CROP P,UsePuzzle,x2,YOffSet+PieceSize/3,1,ROUND(y2-YOffSet-PieceSize/3)+1
						! Paste in Piece
						GR.BITMAP.DRAW g,P,x2-XOffSet,PieceSize/3*2
					NEXT angle
					GR.SET.ANTIALIAS 1
					GR.COLOR 192,92,92,92,0
					GR.ARC g,0-L/5,PieceSize/3*2,PieceSize/3-L/5,PieceSize,220,280,0
					GR.LINE g,0,PieceSize/3,0,L*SIN(220*PI()/180)+cY-YOffSet+PieceSize/3
					GR.LINE g,0,L*SIN(140*PI()/180)+cY-YOffSet+PieceSize/3,0,PieceSize/3*4
					GR.COLOR 255,255,255,255,0
					GR.SET.ANTIALIAS 0
				ELSE
					! Left side
					GR.BITMAP.CROP Part4,UsePuzzle,XOffSet,YOffSet+PieceSize/3,PieceSize/3,PieceSize/3+1
					GR.BITMAP.DRAW P4,Part4,0,PieceSize/3*2
					GR.SET.ANTIALIAS 1
					GR.COLOR 192,92,92,92,0
					GR.LINE g,0,PieceSize/3,0,PieceSize/3*4
					GR.COLOR 255,255,255,255,0
					GR.SET.ANTIALIAS 0
				ENDIF
				IF i<Size THEN
					cX=XOffSet+PieceSize-60/Size
					cY=YOffSet+PieceSize/2
					FOR angle=40 TO 180 STEP ROUND(Size*0.8-1.3)
						x1=L*COS(angle*PI()/180)+cX
						y1=L*SIN(angle*PI()/180)+cY
						x2=L*COS((360-angle)*PI()/180)+cX
						y2=L*SIN((360-angle)*PI()/180)+cY
						! Read from Puzzle
						GR.BITMAP.CROP P,UsePuzzle,x1,y1,1,YOffSet+PieceSize/3*2-y1+1
						! Paste in Piece
						GR.BITMAP.DRAW g,P,x1-XOffSet,y1-YOffSet+PieceSize/3
						! Read from Puzzle
						GR.BITMAP.CROP P,UsePuzzle,x2,YOffSet+PieceSize/3,1,ROUND(y2-YOffSet-PieceSize/3)+1
						! Paste in Piece
						GR.BITMAP.DRAW g,P,x2-XOffSet,PieceSize/3*2
					NEXT angle
					GR.SET.ANTIALIAS 1
					GR.COLOR 192,92,92,92,0
					GR.ARC g,PieceSize/3*2+L/5,PieceSize/3*2,PieceSize+L/5,PieceSize,40,280,0
					GR.LINE g,PieceSize,PieceSize/3,PieceSize,L*SIN(220*PI()/180)+cY-YOffSet+PieceSize/3
					GR.LINE g,PieceSize,L*SIN(140*PI()/180)+cY-YOffSet+PieceSize/3,PieceSize,PieceSize/3*4
					GR.COLOR 255,255,255,255,0
					GR.SET.ANTIALIAS 0
				ELSE
					! Right side
					GR.BITMAP.CROP Part5,UsePuzzle,XOffSet+PieceSize/3*2,YOffSet+PieceSize/3,PieceSize/3,PieceSize/3+1
					GR.BITMAP.DRAW P5,Part5,PieceSize/3*2+1,PieceSize/3*2
					GR.SET.ANTIALIAS 1
					GR.COLOR 192,92,92,92,0
					GR.LINE g,PieceSize,PieceSize/3,PieceSize,PieceSize/3*4
					GR.COLOR 255,255,255,255,0
					GR.SET.ANTIALIAS 0
				ENDIF
			GR.BITMAP.DRAWINTO.END
			GR.BITMAP.DRAW ShowPieces[i+(j-1)*Size],Pieces[i+(j-1)*Size],XOffSet+PicXOffSet-0,YOffSet+PicYOffSet-PieceSize/3
		ELSE
			GR.BITMAP.CREATE Pieces[i+(j-1)*Size],PieceSize/3*5,PieceSize
			GR.BITMAP.CROP Part1,UsePuzzle,XOffSet,YOffSet,PieceSize/3+1,PieceSize
			GR.BITMAP.CROP Part2,UsePuzzle,XOffSet+PieceSize/3,YOffSet+PieceSize/3-15/Size,PieceSize/3+1,PieceSize/3+30/Size+1
			GR.BITMAP.CROP Part3,UsePuzzle,XOffSet+PieceSize/3*2,YOffSet,PieceSize/3+ROUND(Size/11),PieceSize
			GR.BITMAP.DRAWINTO.START Pieces[i+(j-1)*Size]
				GR.BITMAP.DRAW P1,Part1,PieceSize/3,0
				GR.BITMAP.DRAW P2,Part2,PieceSize/3*2,PieceSize/3-15/Size
				GR.BITMAP.DRAW P3,Part3,PieceSize,0
				IF j>1 THEN
					cX=XOffSet+PieceSize/2+PieceSize/3
					cY=YOffSet+60/Size
					FOR angle=90 TO 230 STEP ROUND(Size*0.8-1.3)
						x1=L*COS(angle*PI()/180)+cX
						y1=L*SIN(angle*PI()/180)+cY
						x2=L*COS((180-angle)*PI()/180)+cX
						y2=L*SIN((180-angle)*PI()/180)+cY
						! Read from Puzzle
						GR.BITMAP.CROP P,UsePuzzle,XOffSet+PieceSize/3,y1,x1-PieceSize/3*2-XOffSet+1+ROUND(Size/11),1
						! Paste in Piece
						GR.BITMAP.DRAW g,P,PieceSize/3*2,y1-YOffSet
						! Read from Puzzle
						GR.BITMAP.CROP P,UsePuzzle,x2-PieceSize/3,y2,XOffSet+PieceSize-x2+1+ROUND(Size/11),1
						! Paste in Piece
						GR.BITMAP.DRAW g,P,x2-XOffSet,y1-YOffSet
					NEXT angle
					GR.SET.ANTIALIAS 1
					GR.COLOR 192,92,92,92,0
					GR.ARC g,PieceSize/3*2,0-L/5,PieceSize,PieceSize/3-L/5,310,280,0
					GR.LINE g,PieceSize/3,0,L*COS(130*PI()/180)+cX-XOffSet,0
					GR.LINE g,L*COS(310*PI()/180)+cX-XOffSet,0,PieceSize/3*4,0
					GR.COLOR 255,255,255,255,0
					GR.SET.ANTIALIAS 0
				ELSE
					! Top side
					GR.BITMAP.CROP Part4,UsePuzzle,XOffSet+PieceSize/3,YOffSet,PieceSize/3+1,PieceSize/3
					GR.BITMAP.DRAW P4,Part4,PieceSize/3*2,0
					GR.SET.ANTIALIAS 1
					GR.COLOR 192,92,92,92,0
					GR.LINE g,PieceSize/3,0,PieceSize/3*4,0
					GR.COLOR 255,255,255,255,0
					GR.SET.ANTIALIAS 0
				ENDIF
				IF j<Size THEN
					cX=XOffSet+PieceSize/2+PieceSize/3
					cY=YOffSet+PieceSize-60/Size
					FOR angle=120 TO 260 STEP ROUND(Size*0.8-1.3)
						x1=L*COS(angle*PI()/180)+cX
						y1=L*SIN(angle*PI()/180)+cY
						x2=L*COS((180-angle)*PI()/180)+cX
						y2=L*SIN((180-angle)*PI()/180)+cY
						! Read from Puzzle
						GR.BITMAP.CROP P,UsePuzzle,XOffSet+PieceSize/3,y1,x1-PieceSize/3*2-XOffSet+1+ROUND(Size/11),1
						! Paste in Piece
						GR.BITMAP.DRAW g,P,PieceSize/3*2,y1-YOffSet
						! Read from Puzzle
						GR.BITMAP.CROP P,UsePuzzle,x2-PieceSize/3,y2,XOffSet+PieceSize-x2+1+ROUND(Size/11),1
						! Paste in Piece
						GR.BITMAP.DRAW g,P,x2-XOffSet,y1-YOffSet
					NEXT angle
					GR.SET.ANTIALIAS 1
					GR.COLOR 192,92,92,92,0
					GR.ARC g,PieceSize/3*2,PieceSize/3*2+L/5,PieceSize,PieceSize+L/5,130,280,0
					GR.LINE g,PieceSize/3,PieceSize,L*COS(130*PI()/180)+cX-XOffSet,PieceSize
					GR.LINE g,L*COS(310*PI()/180)+cX-XOffSet,PieceSize,PieceSize/3*4,PieceSize
					GR.COLOR 255,255,255,255,0
					GR.SET.ANTIALIAS 0
				ELSE
					! Bottom side
					GR.BITMAP.CROP Part5,UsePuzzle,XOffSet+PieceSize/3,YOffSet+PieceSize/3*2,PieceSize/3+1,PieceSize/3
					GR.BITMAP.DRAW P5,Part5,PieceSize/3*2,PieceSize/3*2
					GR.SET.ANTIALIAS 1
					GR.COLOR 192,92,92,92,0
					GR.LINE g,PieceSize/3,PieceSize,PieceSize/3*4,PieceSize
					GR.COLOR 255,255,255,255,0
					GR.SET.ANTIALIAS 0
				ENDIF
				IF i>1 THEN
					cX=XOffSet+PieceSize/3-60/Size
					cY=YOffSet+PieceSize/2
					FOR angle=180 TO 320 STEP ROUND(Size*0.8-1.3)
						x1=L*COS(angle*PI()/180)+cX
						y1=L*SIN(angle*PI()/180)+cY
						x2=L*COS((360-angle)*PI()/180)+cX
						y2=L*SIN((360-angle)*PI()/180)+cY
						! Read from Puzzle
						GR.BITMAP.CROP P,UsePuzzle,x1-PieceSize/3,y1,1,y2-y1+1
						! Paste in Piece
						GR.BITMAP.DRAW g,P,x1-XOffSet,y1-YOffSet
					NEXT angle
					GR.SET.ANTIALIAS 1
					GR.COLOR 192,92,92,92,0
					GR.ARC g,L/5,PieceSize/3,L/5+PieceSize/3,PieceSize/3*2,40,280,0
					GR.LINE g,PieceSize/3,0,PieceSize/3,L*SIN(220*PI()/180)+cY-YOffSet
					GR.LINE g,PieceSize/3,L*SIN(140*PI()/180)+cY-YOffSet,PieceSize/3,PieceSize
					GR.COLOR 255,255,255,255,0
					GR.SET.ANTIALIAS 0
				ELSE
					! Left side
					GR.SET.ANTIALIAS 1
					GR.COLOR 192,92,92,92,0
					GR.LINE g,PieceSize/3,0,PieceSize/3,PieceSize0
					GR.COLOR 255,255,255,255,0
					GR.SET.ANTIALIAS 0
				ENDIF
				IF i<Size THEN
					cX=XOffSet+PieceSize/3*4+60/Size
					cY=YOffSet+PieceSize/2
					FOR angle=220 TO 360 STEP ROUND(Size*0.8-1.3)
						x1=L*COS(angle*PI()/180)+cX
						y1=L*SIN(angle*PI()/180)+cY
						x2=L*COS((360-angle)*PI()/180)+cX
						y2=L*SIN((360-angle)*PI()/180)+cY
						! Read from Puzzle
						GR.BITMAP.CROP P,UsePuzzle,x1-PieceSize/3,y1,1,y2-y1+1
						! Paste in Piece
						GR.BITMAP.DRAW g,P,x1-XOffSet,y1-YOffSet
					NEXT angle
					GR.SET.ANTIALIAS 1
					GR.COLOR 192,92,92,92,0
					GR.ARC g,PieceSize/3*4-L/5,PieceSize/3,PieceSize/3*5-L/5,PieceSize/3*2,220,280,0
					GR.LINE g,PieceSize/3*4,0,PieceSize/3*4,L*SIN(220*PI()/180)+cY-YOffSet
					GR.LINE g,PieceSize/3*4,L*SIN(140*PI()/180)+cY-YOffSet,PieceSize/3*4,PieceSize
					GR.COLOR 255,255,255,255,0
					GR.SET.ANTIALIAS 0
				ELSE
					! Right side
					GR.SET.ANTIALIAS 1
					GR.COLOR 192,92,92,92,0
					GR.LINE g,PieceSize/3*4,0,PieceSize/3*4,PieceSize
					GR.COLOR 255,255,255,255,0
					GR.SET.ANTIALIAS 0
				ENDIF
			GR.BITMAP.DRAWINTO.END
			GR.BITMAP.DRAW ShowPieces[i+(j-1)*Size],Pieces[i+(j-1)*Size],XOffSet+PicXOffSet-PieceSize/3,YOffSet+PicYOffSet-0
		ENDIF
		GR.RENDER
	NEXT j
NEXT i
Pause 1000
POPUP "Please, wait... placing pieces...",0,0,1
MenuOption=1
PiecesLeft=NumPieces
GR.CLS
GR.SET.STROKE StrokeSet
GOSUB DrawBackGrnd
GR.SET.STROKE 1
GR.TEXT.SIZE 30
! Redraw Shuffled Pieces
LB=ScaleX-PieceSize*Size
BB=ScaleY/Size
! Place saved pieces first -> Zorder is lower
FOR i=1 TO Size
	FOR j=1 TO Size
		! Saved Game
		IF PlacePieceX[PieceNr[i+(j-1)*Size]]=-100 THEN
			! Piece already placed before in saved game
			IF PieceNr[i+(j-1)*Size]/2=INT(PieceNr[i+(j-1)*Size]/2) THEN
				xr=MOD(PieceNr[i+(j-1)*Size]-1,Size)*PieceSize-PieceSize/3+PicXOffSet
				yr=INT((PieceNr[i+(j-1)*Size]-1)/Size)*PieceSize+PicYOffSet
			ELSE
				xr=MOD(PieceNr[i+(j-1)*Size]-1,Size)*PieceSize+PicXOffSet
				yr=INT((PieceNr[i+(j-1)*Size]-1)/Size)*PieceSize-PieceSize/3+PicYOffSet
			ENDIF
			PiecesLeft=PiecesLeft-1
			GR.BITMAP.DRAW ShowPieces[PieceNr[i+(j-1)*Size]],Pieces[PieceNr[i+(j-1)*Size]],xr,yr
			GR.RENDER
			PAUSE 300/Size
		ENDIF
	NEXT j
NEXT i
FOR i=1 TO Size
	FOR j=1 TO Size
		IF PieceNr[i+(j-1)*Size]/2=INT(PieceNr[i+(j-1)*Size]/2) THEN
			xr=(i-1)*PieceSize+LB-PieceSize/2
			yr=(j-1)*BB-PieceSize/3+PieceSize/2-10
			xc=xr+PieceSize/3*2.5
			yc=yr+PieceSize/2
		ELSE
			xr=(i-1)*PieceSize+LB-PieceSize/3
			yr=(j-1)*BB-10
			xc=xr+PieceSize/2
			yc=yr+PieceSize/3*2.5
		ENDIF
		SOUNDPOOL.PLAY Snd3,Tick,0.99,0.99,1,0,1
		IF PlacePieceX[PieceNr[i+(j-1)*Size]]>-100 THEN
			PlacePieceX[PieceNr[i+(j-1)*Size]]=xc
			PlacePieceY[PieceNr[i+(j-1)*Size]]=yc
			GR.BITMAP.DRAW ShowPieces[PieceNr[i+(j-1)*Size]],Pieces[PieceNr[i+(j-1)*Size]],xr,yr
			GR.RENDER
			PAUSE 300/Size
		ENDIF
	NEXT j
NEXT i
GR.SET.STROKE 2
GR.TEXT.DRAW Txt,20,510,"Pieces left"
GR.TEXT.DRAW TxtNP,180,510,LEFT$(STR$(PiecesLeft),LEN(STR$(PiecesLeft))-2)
GR.RENDER
! Solve Puzzle by drag and drop
AutoSolved=0
MenuOption=2
DO
	DO
		PieceFound=0
		DO
			GR.TOUCH Touched,x,y
		UNTIL Touched | PiecesLeft=0
		x/=sx
		y/=sy
		! Get piece pointed to by searching positions in array
		FOR i=1 TO NumPieces
			IF x>(PlacePieceX[i]-PieceSize/2) & x<(PlacePieceX[i]+PieceSize/2) & y>(PlacePieceY[i]-PieceSize/2) & y<(PlacePieceY[i]+PieceSize/2) THEN
				PieceFound=i
			ENDIF
		NEXT i
	UNTIL PieceFound>0 | PiecesLeft=0
	IF PiecesLeft=0 THEN D_U.BREAK
	SOUNDPOOL.PLAY Snd2,Spin,0.99,0.99,1,0,1
	! Move piece to top of Z-order
	GR.GETDL ZOrder[]
	ARRAY.LENGTH L,ZOrder[]
	ARRAY.SEARCH ZOrder[],ShowPieces[PieceFound],Zplace
	NewZTop=ZOrder[Zplace]
	FOR i=Zplace TO L-1
		ZOrder[i]=ZOrder[i+1]
	NEXT i
	ZOrder[L]=NewZTop
	GR.NEWDL ZOrder[]
	ARRAY.DELETE ZOrder[]
	GR.RENDER
	DO
		GR.TOUCH Touched,x,y
		x/=sx
		y/=sy
		IF x>ScaleX-PieceSize/2 THEN x=x-PieceSize/2
		XP=x-PieceSize/3*(1-VAL(RIGHT$(BIN$(PieceFound),1)))-PieceSize/2
		YP=y-PieceSize/3*VAL(RIGHT$(BIN$(PieceFound),1))-PieceSize/2
		GR.MODIFY ShowPieces[PieceFound],"x",XP
		GR.MODIFY ShowPieces[PieceFound],"y",YP
		GR.RENDER
	UNTIL !Touched
	XPos=MOD((PieceFound-1),Size)*PieceSize+PicXOffSet
	YPos=INT((PieceFound-1)/Size)*PieceSize+PicYOffSet
	IF x>XPos & x<XPos+PieceSize & y>YPos & y<YPos+PieceSize THEN
		SOUNDPOOL.PLAY Snd3,Tick,0.99,0.99,1,0,1
		! Correct position, place piece
		IF PieceFound/2=INT(PieceFound/2) THEN
			XPos=MOD(PieceFound-1,Size)*PieceSize-PieceSize/3
			YPos=INT((PieceFound-1)/Size)*PieceSize
		ELSE
			XPos=MOD(PieceFound-1,Size)*PieceSize
			YPos=INT((PieceFound-1)/Size)*PieceSize-PieceSize/3
		ENDIF
		GR.MODIFY ShowPieces[PieceFound],"x",XPos+PicXOffSet
		GR.MODIFY ShowPieces[PieceFound],"y",YPos+PicYOffSet
		PiecesLeft=PiecesLeft-1
		PlacePieceX[PieceFound]=-100
		PlacePieceY[PieceFound]=-100
		GR.MODIFY TxtNP,"text",LEFT$(STR$(PiecesLeft),LEN(STR$(PiecesLeft))-2)
		GR.RENDER
	ELSE
		! Save new position
		SOUNDPOOL.PLAY Snd2,Spin,0.99,0.99,1,0,1
		PlacePieceX[PieceFound]=x
		PlacePieceY[PieceFound]=y
	ENDIF
UNTIL PiecesLeft=0
MenuOption=0
GR.HIDE TxtNP
GR.HIDE Txt
IF AutoSolved=0 THEN
	SOUNDPOOL.PLAY Snd4,Magic,0.99,0.99,1,0,1
	GR.TEXT.SIZE 65
	GR.TEXT.TYPEFACE 4
	GR.TEXT.BOLD 1
	GR.COLOR 255,255,0,255,1
	GR.TEXT.DRAW w1,30,260,"WELL DONE !"
	GR.COLOR 255,255,255,0,0
	GR.TEXT.DRAW w2,30,260,"WELL DONE !"
	GR.RENDER
	FOR i=1 TO 5
		GR.HIDE w1
		GR.SHOW w2
		GR.RENDER
		PAUSE 300
		GR.HIDE w2
		GR.SHOW w1
		GR.RENDER
		PAUSE 300
	NEXT i
	GR.SHOW w2
	GR.RENDER
	GR.TEXT.TYPEFACE 1
ENDIF
POPUP "Ready...",-250,0,0
PAUSE 2000
GR.BITMAP.DRAW g,StrtPiece,510,200
GR.BITMAP.DRAW g,StpPiece,490,300
GR.RENDER
Q=0
DO
	GOSUB GetTouch
	IF x>510 & x<590 THEN
		IF y>220 & y<300 THEN Q=1
		IF y>300 & y<380 THEN Q=2
	ENDIF
UNTIL Q>0
IF Q=1 THEN
	POPUP "Loading....",0,0,0
	GOTO NwPuzzle
ENDIF
PuzzleQuit:
SOUNDPOOL.RELEASE
WAKELOCK 5
GR.CLOSE
EXIT

ONBACKKEY:
	IF MenuOption=0 THEN
		POPUP "Menu NOT Available !",0,0,0
		GOTO MenuExit
	ENDIF
	ShowOpt=0
	IF MenuOption=1 THEN ShowOpt=160
	GR.BITMAP.DRAW Menu,MenuBMP,ScaleX-240,ScaleY-320+ShowOpt
	GR.RENDER
	GOSUB GetTouch
	IF x>ScaleX-240 & y>ScaleY-320+ShowOpt THEN
		IF y<ScaleY-240+ShowOpt & y>ScaleY-320+ShowOpt THEN MenuSel=1
		IF y<ScaleY-160+ShowOpt & y>ScaleY-240+ShowOpt THEN MenuSel=2
		IF y<ScaleY-80+ShowOpt & y>ScaleY-160+ShowOpt THEN MenuSel=3
		IF y<ScaleY+ShowOpt & y>ScaleY-80+ShowOpt THEN MenuSel=4
	ENDIF
	GR.HIDE Menu
	GR.RENDER
	IF MenuSel=1 THEN GOTO MenuExit
	IF MenuSel=2 THEN
		GOTO PuzzleQuit
	ENDIF
	IF MenuSel=3 THEN
		! Save state
		TEXT.OPEN W,SG,DataPath$+"SaveGame.pzl"
			TEXT.WRITELN SG,FileName$
			TEXT.WRITELN SG,Mask
			TEXT.WRITELN SG,Size
			FOR i=1 TO NumPieces
				TEXT.WRITELN SG,PlacePieceX[i]
				TEXT.WRITELN SG,PlacePieceY[i]
			NEXT i
			FOR i=1 TO NumPieces
				TEXT.WRITELN SG,PieceNr[i]
			NEXT i
		TEXT.CLOSE SG
		POPUP "Puzzle SAVED !",0,0,0
		GOTO PuzzleQuit
	ENDIF
	IF MenuSel=4 THEN
		! Solve rest of puzzle
		POPUP "Please, wait... solving puzzle...",0,0,1
		MenuOption=0
		FOR i=1 TO NumPieces
			IF PlacePieceX[PieceNr[i]]>-100 THEN
				SOUNDPOOL.PLAY Snd3,Tick,0.99,0.99,1,0,1
				IF PieceNr[i]/2=INT(PieceNr[i]/2) THEN
					XPos=MOD(PieceNr[i]-1,Size)*PieceSize-PieceSize/3
					YPos=INT((PieceNr[i]-1)/Size)*PieceSize
				ELSE
					XPos=MOD(PieceNr[i]-1,Size)*PieceSize
					YPos=INT((PieceNr[i]-1)/Size)*PieceSize-PieceSize/3
				ENDIF
				GR.MODIFY ShowPieces[PieceNr[i]],"x",PicXOffSet+XPos
				GR.MODIFY ShowPieces[PieceNr[i]],"y",PicYOffSet+YPos
				PiecesLeft=PiecesLeft-1
				GR.MODIFY TxtNP,"text",LEFT$(STR$(PiecesLeft),LEN(STR$(PiecesLeft))-2)
				GR.RENDER
				PAUSE 900/Size
			ENDIF
		NEXT i
		AutoSolved=1
		MenuOption=1
	ENDIF
MenuExit:
BACK.RESUME

GetTouch:
	DO
		GR.TOUCH Touched,x,y
	UNTIL Touched
	DO
		GR.TOUCH Touched,x,y
	UNTIL !Touched
	x/=sx
	y/=sy
RETURN

ShowPics:
	SOUNDPOOL.PLAY Snd3,Tick,0.99,0.99,1,0,1
	GR.BITMAP.LOAD OrgPuzzle[StartPic+PicOffS],DataPath$+PicsFound$[StartPic+PicOffS]
	GR.BITMAP.SCALE ScaledPuzzle[StartPic+PicOffS],OrgPuzzle[StartPic+PicOffS],PuzzleSize/2,PuzzleSize/2
	GR.CLS
	GR.COLOR 255,255,255,0,1
	FOR i=StartPic TO StartPic+2
		GR.BITMAP.DRAW VizPic[i-StartPic+1],ScaledPuzzle[i],MenuXOffset+MenuXOffset*(i-StartPic)+PuzzleSize*((i-StartPic)*0.5),MenuYOffset
	NEXT i
	GR.TEXT.DRAW g,450,100,"Swipe"
	IF StartPic<PCount-2 THEN GR.TEXT.DRAW g,250,100,"<"
	IF StartPic>1 THEN GR.TEXT.DRAW g,650,100,">"
	GR.TEXT.DRAW g,450,MenuYOffset+PuzzleSize/2+100,"Tap picture to select"
	FOR j=PuzzleSize/4 TO 0 STEP -PuzzleSize/16
		FOR i=StartPic TO StartPic+2
			GR.MODIFY VizPic[i-StartPic+1],"x",MenuXOffset+MenuXOffset*(i-StartPic)+PuzzleSize*((i-StartPic)*0.5)-j*(Swiped*2-3)
		NEXT i
		GR.RENDER
		PAUSE 50
	NEXT j
	GR.BITMAP.DELETE OrgPuzzle[DelPic]
RETURN

DrawBackGrnd:
	GR.COLOR 255,192,192,192,1
	GR.RECT g,PicXOffSet-11,PicYOffSet-11,PicXOffSet+PuzzleSize+10,PicYOffSet+PuzzleSize+10
	GR.BITMAP.DRAW g,UsePuzzle,PicXOffSet-1,PicYOffSet-1
	GR.COLOR Mask,255,255,255,1
	GR.RECT g,PicXOffSet-1,PicYOffSet-1,PicXOffSet+PuzzleSize,PicYOffSet+PuzzleSize
	GR.SET.ANTIALIAS 1
	GR.COLOR 192,92,92,92,0
	GR.RECT g,PicXOffSet-1,PicYOffSet-1,PicXOffSet+PuzzleSize,PicYOffSet+PuzzleSize
	GR.SET.ANTIALIAS 0
	GR.COLOR 255,255,255,255,0
	GR.RENDER
RETURN

DrawTxt:
	GR.COLOR 255,255,0,0,1
	GR.TEXT.DRAW g,PicXOffSet+20,PicYOffSet+PuzzleSize/2,T1$
	GR.TEXT.DRAW g,PicXOffSet+20,PicYOffSet+PuzzleSize/1.6,T2$
	GR.COLOR 255,255,0,255,1
	GR.OVAL g,PicXOffSet+PuzzleSize+20,PicYOffSet+PuzzleSize/3-45,PicXOffSet+PuzzleSize+170,PicYOffSet+PuzzleSize/3+15
	GR.OVAL g,PicXOffSet+PuzzleSize+20,PicYOffSet+PuzzleSize/3*2-45,PicXOffSet+PuzzleSize+170,PicYOffSet+PuzzleSize/3*2+15
	GR.COLOR 255,0,255,255,1
	GR.OVAL g,PicXOffSet+PuzzleSize+20,PicYOffSet+PuzzleSize/2-45,PicXOffSet+PuzzleSize+170,PicYOffSet+PuzzleSize/2+15
	GR.COLOR 255,255,255,0,1
	GR.TEXT.DRAW g,PicXOffSet+PuzzleSize+40,PicYOffSet+PuzzleSize/3," LESS"
	GR.TEXT.DRAW g,PicXOffSet+PuzzleSize+40,PicYOffSet+PuzzleSize/3*2,"MORE"
	GR.COLOR 255,0,0,255,1
	GR.TEXT.DRAW g,PicXOffSet+PuzzleSize+40,PicYOffSet+PuzzleSize/2,"   OK !"
	GR.RENDER
RETURN

GetValue:
	DO
		GOSUB GetTouch
		OldVal=MenuVal
		IF x>PicXOffSet+PuzzleSize+20 & x<PicXOffSet+PuzzleSize+170 THEN
			IF y>PicYOffSet+PuzzleSize/3-45 & y<PicYOffSet+PuzzleSize/3+15 THEN
				IF MenuVal>1 THEN MenuVal=MenuVal-1
			ENDIF
			IF y>PicYOffSet+PuzzleSize/3*2-45 & y<PicYOffSet+PuzzleSize/3*2+15 THEN
				IF MenuVal<4 THEN MenuVal=MenuVal+1
			ENDIF
			IF	y>PicYOffSet+PuzzleSize/2-45 & y<PicYOffSet+PuzzleSize/2+15 THEN
				Start=1
			ENDIF
		ENDIF
	UNTIL OldVal<>MenuVal | Start
RETURN

SetVars:
	MenuOption=0
	NumPieces=Size*Size
	PieceSize=PuzzleSize/Size
	L=75/Size
	GR.SET.STROKE StrokeSet
	GOSUB DrawBackGrnd
	ARRAY.DELETE Pieces[]
	DIM Pieces[NumPieces]
	ARRAY.DELETE ShowPieces[]
	DIM ShowPieces[NumPieces]
	ARRAY.DELETE PieceNr[]
	DIM PieceNr[NumPieces]
	ARRAY.DELETE PlacePieceX[]
	ARRAY.DELETE PlacePieceY[]
	DIM PlacePieceX[NumPieces]
	DIM PlacePieceY[NumPieces]
RETURN
