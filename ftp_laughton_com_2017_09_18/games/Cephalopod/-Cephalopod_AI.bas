! CEPHALOPOD  @Cassiope34 09/2014

Fn.def vl( cc$, c[])   % valeur de la case c$
 x =val(left$( cc$,1))
 y =val(right$(cc$,1))
 Fn.rtn c[x,y,4]
Fn.end

Fn.def around$( x, y, c[], nx, mx, my)    % les cases sommables autour de x,y
 st$=""
 if y-1>0 then if c[x,y-1,4]>0 & c[x,y-1,4]<nx then st$ =int$(x*10+y-1)+" "
 if x+1<=mx then if c[x+1,y,4]>0 & c[x+1,y,4]<nx then st$ =st$+int$((x+1)*10+y)+" "
 if y+1<=my then if c[x,y+1,4]>0 & c[x,y+1,4]<nx then st$ =st$+int$(x*10+y+1)+" "
 if x-1>0 then if c[x-1,y,4]>0 & c[x-1,y,4]<nx then st$ =st$+int$((x-1)*10+y)+" "
 Fn.rtn st$
Fn.end

Fn.def clr( cl$ )
 gr.color val(word$(cl$,1)), val(word$(cl$,2)), val(word$(cl$,3)), val(word$(cl$,4)), val(word$(cl$,5))
Fn.end

FN.DEF roundCornerRect (b, h, r)
 half_pi       = 3.14159 / 2
 dphi          = half_pi / 8
 LIST.CREATE   N, S1
 mx            = -b/2+r
 my            = -h/2+r
 FOR phi       = 0 TO half_pi STEP dphi
  LIST.ADD     s1, mx-COS(phi)*r, my-sin (phi)*r
 NEXT
 mx            = b/2-r
 my            = -h/2+r
 FOR phi       = 0 TO half_pi STEP dphi
  LIST.ADD     s1, mx+SIN(phi)*r, my- cos (phi)*r
 NEXT
 mx            = b/2-r
 my            = h/2-r
 FOR phi       = 0 TO half_pi STEP dphi
  LIST.ADD     s1, mx+COS(phi)*r, my+sin (phi)*r
 NEXT
 mx            = -b/2+r
 my            =  h/2-r
 FOR phi       = 0 TO half_pi STEP dphi
  LIST.ADD     s1, mx-SIN(phi)*r, my+cos (phi)*r
 NEXT
 FN.RTN s1
FN.END

File.Root tmp$
IstandAlone =Is_In("rfo-basic",tmp$)    % to know if it's an APK for the way to exit.

gr.open 255,116,212,129,0,0   % vert clair
gr.screen w,h
scx =1280
scy =800
sx  =w/scx
sy  =h/scy
gr.scale sx,sy
dc  =floor(scy/5)    % dimension d'une case
ddx =dc-30           % dimension d'un dé
umax=20              % nbre maxi de undo.

DIM c[5,5,6], cl$[4], dice[2], diceN[6], main[2], pmx[2], pmy[2]
DIM ntd[2], ptn[2], ts$[10], res[10], prd[5], undo$[umax]
jo =1    % joueur 1 ou 2 a qui appartient cette case
pg =2    % pointeur graph. du dé
pn =3    % pointeur nombre du dé
vl =4    % valeur du dé
se =5    % selectionné ?  0 ou 1
pr =6    % pointeur d'un rond de sélection

for t=1 to 10
  ts$[t] =word$("12 13 23 123 14 24 34 134 234 1234" ,t)
next
nmax =6

ARRAY.LOAD diX[],0,1,0,-1    % 1 North  2 East  3 South  4 West  pour l'AI...
ARRAY.LOAD diY[],-1,0,1,0

cl$[1] ="255 255 255 212 1"  % Ivoire
!cl$[2] ="255 160 160 160 1"  % gris foncé (dés)
cl$[2] ="255 255 230 144 2"  % orange clair
cl$[3] ="255 220 220 220 0"  % gris clair traits
cl$[4] ="255 116 212 129 1"  % fond
fileSav$ ="Cephalopod.sav"

! fabrication des bitmaps
gr.bitmap.create dice[1], ddx, ddx       % fond dé ivoire
gr.bitmap.drawinto.start dice[1]
poly =roundCornerRect(ddx-5,ddx-5,32)
gr.set.stroke 5
call clr(cl$[1])
gr.poly nul, poly, ddx/2-2, ddx/2
gr.bitmap.drawinto.end

gr.bitmap.create dice[2], ddx, ddx       % fond dé orange
gr.bitmap.drawinto.start dice[2]
poly =roundCornerRect(ddx-5,ddx-5,32)
gr.set.stroke 5
call clr(cl$[2])
gr.poly nul, poly, ddx/2-2, ddx/2
gr.bitmap.drawinto.end

pts$ ="000010000100000001100010001101000101101010101101101101"
gr.color 255, 0, 0, 0, 1
ec =floor(ddx/4)
n =0
for d=1 to 6
  gr.bitmap.create diceN[d], ddx, ddx    % bitmap des 6 nombres, faces des dés.
  gr.bitmap.drawinto.start diceN[d]
  for y=1 to 3
    for x=1 to 3
      n++
      if val(mid$(pts$,n,1)) then gr.circle nul, x*ec-1, y*ec, 11    % point noir
    next
  next
  gr.bitmap.drawinto.end
next

dhx =6.3*dc
dhy =4.5*dc
gr.bitmap.create bhelp, dhx, dhy    % help screen
gr.bitmap.drawinto.start bhelp
 gr.color 255,106,200,120,1
 gr.rect nul, 0, 0, dhx, dhy
 gr.color 255,240,255,0,0
 gr.rect nul, 0, 0, dhx, dhy
 gr.set.stroke 4
 gr.color 255,0,255,70,2
 gr.text.size 40
 gr.text.draw nul,dhx/2-200, 50, "-  C E P H A L O P O D  -"
 dim h$[14]
 h$[1] ="The goal:"
 h$[2] =" - the player with the highest number of dice of his color when the grid"
 h$[3] ="is filled wins. "
 h$[4] ="The rules:"
 h$[5] =" - Each player in turn place a die of his color that show ONE. But when"
 h$[6] ="the die introduced have neighbors in vertical or/and horizontal"
 h$[7] ="(not diagonal) regardless of color, the player must add at least 2 of them."
 h$[8] =" Dice summed will disappear and the die placed will take the result value."
 h$[9] =" The die placed stay to ONE if all possible additions exceed the value 6."
 h$[10] ="In practice, all dice to be added around the selected cell will be"
 h$[11] ="surrounded by blue turning red or blue if you touch them.
 h$[12] ="Touch the central cell to validate and only the blue dice will be added."
 h$[13] =""
 h$[14] ="  (original game by Mark Steere)                                      Have fun."
 gr.set.stroke 1
 gr.text.size 30
 gr.color 255, 234, 255, 0, 2
 ecl =45
 for l=1 to 14
   gr.text.draw nul, 15, 60+l*ecl, h$[l]
 next
 array.delete h$[]
gr.bitmap.drawinto.end
! ----------------------------------------------------------------------

cmx =5    % dimension de la grille par défaut.
cmy =5

Newp:
gr.cls
UnDim c[]
Dim c[5,5,6]

j   =1     % dés ivoire commence.
mdp =1     % mode players :  1 =AI play, 2 =2 players mode.

File.exists fe, fileSav$     % donne les derniers cmx,cmy utilisés.
if fe
  GRABFILE ldgame$, fileSav$
  split ln$[], ldgame$, "\n"
  cmx =val(ln$[1])
  cmy =val(ln$[2])
  array.delete ln$[]
endif

plx =floor((scx-scy)/2)     % position du plateau de jeu
if cmx=3 then plx =plx+dc
ply =0
if cmy=3 then ply =dc

gr.color 255,106,200,120,1
gr.rect nul, plx, ply, plx+dc*cmx, ply+dc*cmy     % fond de la grille vert foncé
gr.color 155,220,220,220,1
gr.set.stroke 2                % la grille
for y=0 to cmy
  gr.line nul, plx, ply+y*dc, plx+cmx*dc, ply+y*dc
next
for x=0 to cmx
  gr.line nul, plx+x*dc, ply, plx+x*dc, ply+cmy*dc
next

gr.set.stroke 3
gr.color 255, 0, 0, 255, 0
gr.paint.get bleu
gr.color 255, 255, 0, 0, 0
gr.paint.get rouge

!   toutes les cases ont un pointeur graphique d'un fond de dé (joueur)
!   et un pointeur de la valeur du dé affiché.
!   Si case vide les 2 pointeurs sont hide
!   Il suffit de gr.modify ptr, "bitmap", .. pour afficher les bonnes infos...
gr.set.stroke 4
gr.color 255, 0, 0, 0, 2
d =0
n =1
for y=0 to 4         % tous les bitmaps de la grille (cachés au départ).
  for x=0 to 4
    d =d+1-6*(d=6)
    if d=1 then n=3-n
    gr.bitmap.draw c[x+1,y+1,pg], dice[n] , plx+x*dc+17, ply+y*dc+15
    gr.bitmap.draw c[x+1,y+1,pn], diceN[d], plx+x*dc+17, ply+y*dc+15
    gr.hide c[x+1,y+1,pg]
    gr.hide c[x+1,y+1,pn]
  next
next

gr.set.stroke 2
gr.color 255, 0, 0, 255, 0        %   les 5 ronds de sélections (bleu, rouge)
for cr=1 to 5
  gr.circle prd[cr], plx, ply, dc/2.1    % grands cercles
  gr.hide prd[cr]
next

gr.bitmap.scale d1, dice[1], ddx/2, ddx/2    % la main
gr.bitmap.draw main[1],d1,65,5.7*ddx/2
gr.bitmap.scale d2, dice[2], ddx/2, ddx/2
gr.bitmap.draw main[2],d2,65,5.7*ddx/2

gr.set.stroke 3       %  infos de gauche
gr.text.align 2
gr.text.size 60
gr.color 255,255,255,212,2    % Ivoire
gr.text.draw ptn[1], 100, 1.6*dc, int$(ntd[1])    % nombre de dés
call clr( cl$[2] )
!gr.color 255,160,160,160,2    % gris foncé (dés)
gr.text.draw ptn[2], 100, 3.7*dc, int$(ntd[2])    % de chaque couleur.
gr.color 255,0,255,0,2
gr.text.draw cg1, scx/2, 1.6*dc, "3 x 3"
gr.text.draw cg2, scx/2, 2.6*dc, "5 x 3"
gr.text.draw cg3, scx/2, 3.6*dc, "5 x 5"
gr.hide cg1
gr.hide cg2
gr.hide cg3

gr.color 255, 0, 0, 0, 1
gr.text.size 32
gr.text.draw nul, scx-80, scy-dc/3, "N E W"
gr.text.draw nul, scx-80, dc/2, "UNDO"
gr.text.draw nul, 100, dc/2, "HELP"
gr.text.draw mde, 100, scy-dc/3, "1 Player"

gr.bitmap.draw helpb, bhelp, (scx-dhx)/2, (scy-dhy)/2     % help
gr.hide helpb

gr.color 255, 246, 255, 0, 2   % jaune
gr.set.stroke 1
gr.text.align 2
gr.text.size 28
gr.text.draw mess2, 100, scy-40, ""   % debug
gr.text.draw mess, scx-100, scy/2, ""

if fe
  gosub loadgame
  array.delete ln$[]
  File.delete fe, fileSav$    % delete last game file
endif
gr.modify mde, "text", word$("1 player,2 players",mdp,",")

mcp =0   % pour les UNDO
cpj =0

DO
  if j=1 then gr.hide main[2] else gr.show main[2]     % à qui le tour.
  gosub compte
  do
    gr.touch touched, x, y
    if !background() then gr.render
  until touched | quit
  if quit then D_U.continue
  do
    gr.touch touched, tx, ty
  until !touched
  if help
    help =0
    gr.hide helpb
    D_u.continue
  endif
  tx/=sx
  ty/=sy
  cx =floor((tx-plx)/dc)+1
  cy =floor((ty-ply)/dc)+1
  if new
    if tx>scx/2-dc/2 & tx<scx/2-dc/2+dc    % choix de grille
      if ty>dc & ty<2*dc
        cmx =3
        cmy =3
      elseif ty>2*dc & ty<3*dc
        cmx =5
        cmy =3
      elseif ty>3*dc & ty<4*dc
        cmx =5
        cmy =5
      endif
    endif
    new =0
    D_u.break

  elseif tx>scx-dc & ty>scy-dc      % new
    gosub unsel
    for y=1 to cmy       % clean grid
      for x=1 to cmx
        gr.hide c[x,y,pg]
        gr.hide c[x,y,pn]
        c[x,y,jo] =0
        c[x,y,vl] =0
      next
    next
    cpj =0
    gr.show cg1    % présenter le choix de grille: 3x3, 5x3, 5x5
    gr.show cg2
    gr.show cg3
    new =1

  elseif tx<dc & ty<dc         % help
    help =1
    gr.show helpb

  elseif tx>scx-dc & ty<dc     % undo...
    gosub undo
    if mdp=1 then gosub undo

  elseif tx<dc & ty>scy-dc     % mode players
    mdp =3-mdp
    gr.modify mde, "text", word$("1 player,2 players",mdp,",")
    if mdp =1 then gosub AI

  elseif cx>0 & cx<=cmx & cy>0 & cy<=cmy          % dans la grille
    if c[cx,cy,jo]=0 & !chx   % si vide
      oldcx =cx
      oldcy =cy
    ! chaine des cases (dés) sommables autour de cx,cy
      ar$ =around$(cx,cy,c[],nmax,cmx,cmy)
      lar =len(ar$)/3
      if lar > 1
        gosub autom    % fabrique ch$ si c'est évidant...
        if ch$<>""     % une seule possibilité d'addition : maj
          gosub majok
        else           % sinon choix manuel des dés à additionner.
          ! mettre les ronds bleus dans chaque cases de ar$ et sur cx,cy
          gr.modify prd[1], "x", plx+(cx-0.5)*dc
          gr.modify prd[1], "y", ply+(cy-0.5)*dc
          gr.show prd[1]
          for rd=1 to lar                    %  les ronds bleus
            vx =val(left$(word$(ar$,rd),1))
            vy =val(right$(word$(ar$,rd),1))
            gr.modify prd[rd+1], "x", plx+(vx-0.5)*dc
            gr.modify prd[rd+1], "y", ply+(vy-0.5)*dc
            gr.modify prd[rd+1], "paint", bleu
            gr.show prd[rd+1]
            c[vx,vy,se] =1
            c[vx,vy,pr] =prd[rd+1]
          next
          chx =1
        endif
      else
        ch$ =int$(cx*10+cy)   % 1 dans la case.
        gosub majok
      endif

    elseif c[cx,cy,se]    % une case avec rond bleu ou rouge
      if c[cx,cy,se]=1
         c[cx,cy,se]=2
         gr.modify c[cx,cy,pr], "paint", rouge
      elseif c[cx,cy,se]=2
         c[cx,cy,se]=1
         gr.modify c[cx,cy,pr], "paint", bleu
      endif

    elseif cx=oldcx & cy=oldcy     % la case centrale pour valider
      nch$=""
      stt =0
      for l=1 to lar
        vx =val(left$( word$(ar$,l),1))
        vy =val(right$(word$(ar$,l),1))
        if c[vx,vy,se]=1
          stt +=c[vx,vy,vl]
          nch$ = nch$+word$(ar$,l)+" "
        endif
      next
      if stt<=nmax & len(nch$)>=6      % au moins 2 dés sélectionnés.
        ch$ = int$(cx*10+cy)+" "+nch$
        gosub majok
      endif

    else      % effacer les ronds et init des c[x,y,se] à 0
      gosub unsel

    endif
    if ok & !chx
      j =3-j
      gr.render
      if mdp=1 then gosub AI
    endif
    ok =0

  endif
UNTIL quit

if !quit then goto Newp

gosub savegame     % sauvera au minimum cmx,cmy

if IstandAlone
  END "Bye...!"
else
  EXIT
endif      %  ----------------------  exit  ----------------------

onbackkey:
if help
  gr.hide helpb
  help =0
  Back.resume
endif
lastime =clock()
if lastime-thistime <1000
  quit =1
else
  quit =0
  thistime =lastime
  popup " Presser encore pour quitter ",0,0,0
endif
Back.resume

compte:
ntd[1] =0
ntd[2] =0
for ny=1 to cmy
  for nx=1 to cmx
    if c[nx,ny,jo] then ntd[ c[nx,ny,jo] ]++
  next
next
gr.modify ptn[1], "text", int$(ntd[1])
gr.modify ptn[2], "text", int$(ntd[2])
RETURN

autom:   % sommage automatique de la chaine des cases sommables ou non...
! doit fabriquer ch$ contenant cx,cy et toutes les cases à sommer et effacer.
UnDim res[]
Dim res[10]
ad =0
au =0
la =len(ar$)/3
lt =val(word$("0 1 4 10",la))
for so =1 to lt            % toutes les additions possibles.
  for l=1 to len(ts$[so])
    res[so] =res[so] +vl( word$(ar$,val(mid$(ts$[so],l,1))), c[] )   % addition
  next
  if res[so]<=nmax
    au++
    ad =so
  endif
next
ch$ ="" 
if au =1       % fabrique la chaine du coup à jouer si évident.
  ch$ =int$(cx*10+cy)+" "
  for l=1 to len(ts$[ad])
    ch$ =ch$+word$(ar$,val(mid$(ts$[ad],l,1)))+" "
  next
elseif au =0
  ch$ =int$(cx*10+cy)+" "    % pas d'addition possible donc 1 dans ch$
endif
RETURN

majok:     % execute le contenu de ch$ (case à jouer + les cases a additionner)
la =len(ch$)/3
vd =1      % nombre mini dans une case.
und$ =""
if la>2
  vd =0
  for so=2 to la
    vx =val(left$( word$(ch$,so),1))
    vy =val(right$(word$(ch$,so),1))
    gr.hide c[vx,vy,pg]
    gr.hide c[vx,vy,pn]
    vd += c[vx,vy,vl]
    und$ =und$+int$(vx*10+vy)+int$(c[vx,vy,jo]*10+c[vx,vy,vl])+" "
    c[vx,vy,jo] =0
    c[vx,vy,vl] =0
  next
endif
vx =val(left$( word$(ch$,1),1))
vy =val(right$(word$(ch$,1),1))

mcp++
if mcp>umax              % seulement 2 undo possibles.
  for u=1 to umax-1
    undo$[u]=undo$[u+1]
  next
  mcp =umax
endif
undo$[mcp] =und$+int$(vx*10+vy)    % pour le undo

gr.modify c[vx,vy,pg], "bitmap", dice[j]
gr.modify c[vx,vy,pn], "bitmap", diceN[vd]
gr.show c[vx,vy,pg]
gr.show c[vx,vy,pn]
c[vx,vy,jo] =j
c[vx,vy,vl] =vd
gosub unsel
ok =1

cpj++
!gr.modify mess, "text", int$(cpj)   % compte le nbre de coups...

RETURN

AI:           %  tous les coups jouables, analyse, choix.
UnDim sol$[],vc[]
Dim sol$[80],vc[80]
sol =0
for cy=1 to cmy
  for cx=1 to cmx
   if c[cx,cy,jo] =0
    ar$ =around$(cx,cy,c[],nmax,cmx,cmy)    % chaine des voisins sommables
    lar =len(ar$)/3
    if lar >1
      lt =val(word$("0 1 4 10",lar))
      UnDim res[]
      Dim res[10]
      au =0
      for so =1 to lt      % toutes les additions possibles.
        sl$ =""
        rc  =0
        for l=1 to len(ts$[so])
          c$ =word$(ar$,val(mid$(ts$[so],l,1)))
          vx =val(left$(c$,1))
          vy =val(right$(c$,1))
          sl$ =sl$+c$+" "
          res[so] += c[vx,vy,vl]     % résultat de l'addition.
          if c[vx,vy,jo]=j then rc =rc-1 else rc =rc+1   % les couleurs...
        next
        if res[so]<=nmax
          au++
          sol++
          sol$[sol] =int$(cx*10+cy)+" "+sl$    % le coup jouable.
          if rc>=0 then vc[sol] += (10+rc)     % plus de couleur adverse prise.
          if res[so] =nmax               % un 6 fabriqué.
            vc[sol]+=20
          elseif res[so] <nmax
            vlc =res[so]
            gosub plctnok      %  ne pas donner un 6 à l'adversaire.
            if nok>0 then vc[sol] =-50
          endif
        endif
      next
    endif
    if lar<2 | au=0    % aucune addition possible ou pas de dé à additionner.
      sol++
      sol$[sol] =int$(cx*10+cy)+" "   % 1 dans la case.   <--------------
      vc[sol]+=8
      n   =0       %  valoriser +14 si cx,cy entourée de 6...!
      six =0
      over=0
      for s1=1 to 4
        if cx+diX[s1]>0 & cx+diX[s1]<=cmx & cy+diY[s1]>0 & cy+diY[s1]<=cmy
          n++
          if c[cx+diX[s1],cy+diY[s1],vl] =nmax then six++
          if c[cx,cy,vl] + c[cx+diX[s1],cy+diY[s1],vl] >nmax then over++
        endif
      next
      if n=six | n=over then vc[sol] +=11

      sl$ =""
      vlc =1
      gosub plctnok      %  ne pas donner un futur 6 à l'adversaire.
      if nok then vc[sol] =-50

    endif
   endif
  next
next
if sol      % prend le meilleur vc[]
  UnDim cr[]
  Dim cr[60]
  so =0
  vr =-100
  for s=1 to sol
    if vc[s]>vr
      so =1
      cr[so] =s
      vr =vc[s]
    elseif vc[s]=vr
      so++
      cr[so] =s
    endif
  next
  ch$ =sol$[ cr[ floor(rnd()*so)+1 ] ]    % hasard parmis solutions équivalentes.
  if (cmy=5 & ntd[1]+ntd[2]>12) | cmy=3 then pause 200
  gosub majok
  j=3-j
  ok =0
endif
RETURN

plctnok:   % cx,cy est-il un coup mauvais car il donnerait un 6 à l'adversaire ?
nok =0
lct =0
for s1=1 to 4
  cvx =cx+diX[s1]
  cvy =cy+diY[s1]
  if cvx>0 & cvx<=cmx & cvy>0 & cvy<=cmy
    dj =IS_IN( int$(cvx*10+cvy), sl$ )
    if c[cvx,cvy,vl]=0 | dj>0
      s2 =val(word$("3 4 1 2",s1))   % sens opposé à s1.
      ns =0
      ct$=""
      do          % chaine des dés additionnables ds la case voisine de cvx,cvy
        ns++
        s2 =s2+1-4*(s2=4)
        cvxx =cvx+diX[s2]
        cvyy =cvy+diY[s2]
        if cvxx>0 & cvxx<=cmx & cvyy>0 & cvyy<=cmy
          if c[cvxx,cvyy,vl]>0 & c[cvxx,cvyy,vl]<nmax
            ct$ =ct$ + int$((cvxx)*10+cvyy)+" "
          endif
        endif
      until ns=3
      lct =floor(len(ct$)/3)     %  est-ce qu'un 6 est possible avec cvx,cvy ?
      if lct
        v1$ = word$(ct$,1)
        if vlc+vl( v1$, c[] )=nmax then nok=1
        if lct>1 & nok=0
          v2$ = word$(ct$,2)
          if vlc+vl( v2$, c[] )=nmax then nok=1
          if vlc+vl( v1$, c[] )+ vl( v2$, c[] )=nmax then nok=1
          if dj & nok=0
            if vl( v1$, c[] )+ vl( v2$, c[] )=nmax then nok=1
          endif
        endif
        if lct>2 & nok=0
          v3$ = word$(ct$,3)
          if vlc+vl( v3$ , c[] )=nmax then nok=1
          if vlc+vl( v2$, c[] )+ vl( v3$ , c[] )=nmax then nok=1
          if vlc+vl( v1$, c[] )+ vl( v3$ , c[] )=nmax then nok=1
          if vlc+vl( v1$, c[])+vl( v2$, c[] )+ vl( v3$, c[] )=nmax then nok=1
          if dj & nok=0
            if vl( v2$, c[] )+ vl( v3$, c[] )=nmax then nok=1
            if vl( v1$, c[] )+ vl( v3$, c[] )=nmax then nok=1
            if vl( v1$, c[] )+ vl( v2$, c[] )+ vl( v3$, c[] )=nmax then nok=1
          endif
        endif
      endif
    endif
  endif
  if nok then F_n.break
next
return

undo:
if mcp
  nc =len(undo$[mcp])/5
  for n=1 to nc
    vx =val(mid$(word$(undo$[mcp],n),1,1))
    vy =val(mid$(word$(undo$[mcp],n),2,1))
    c[vx,vy,jo] =val(mid$(word$(undo$[mcp],n),3,1))
    c[vx,vy,vl] =val(mid$(word$(undo$[mcp],n),4,1))
    gr.modify c[vx,vy,pg], "bitmap", dice[ c[vx,vy,jo] ]
    gr.modify c[vx,vy,pn], "bitmap", diceN[ c[vx,vy,vl] ]
    gr.show c[vx,vy,pg]
    gr.show c[vx,vy,pn]
  next
  vx =val(mid$(word$(undo$[mcp],nc+1),1,1))
  vy =val(mid$(word$(undo$[mcp],nc+1),2,1))
  gr.hide c[vx,vy,pg]
  gr.hide c[vx,vy,pn]
  c[vx,vy,jo] =0
  c[vx,vy,vl] =0
  mcp--
  j =3-j
  cpj--
  if chx then gosub unsel
endif
RETURN

unsel:
for prr=1 to 5     % cache les ronds
  gr.hide prd[prr]
next
ch$ =""
chx =0
oldcx =0
oldcy =0
for y=1 to cmy       % nettoye les cases sélectionnées.
  for x=1 to cmx
    c[x,y,se] =0
  next
next
RETURN

savegame:
cls
print cmx
print cmy
print mdp
if ntd[1]+ntd[2]>0   %  & ntd[1]+ntd[2]<(cmx*cmy)
  print j
  for y=1 to cmy
    sv$ =""
    for x=1 to cmx
      sv$ =sv$+int$(c[x,y,jo])+int$(c[x,y,vl])
    next
    print sv$
  next
endif
console.save fileSav$
cls
RETURN

loadgame:
  GRABFILE ldgame$, fileSav$
  split ln$[], ldgame$, "\n"
  cmx =val(ln$[1])
  cmy =val(ln$[2])
  mdp =val(ln$[3])
  array.length lg, ln$[]
  if lg>3
   j =val(ln$[4])
   for y=1 to cmy
    c=0
    for x=1 to cmx
      c++
      c[x,y,jo] =val(mid$(ln$[y+4],c,1))
      c++
      c[x,y,vl] =val(mid$(ln$[y+4],c,1))
      if c[x,y,jo]
        gr.modify c[x,y,pg], "bitmap", dice[ c[x,y,jo] ]
        gr.modify c[x,y,pn], "bitmap", diceN[ c[x,y,vl] ]
        gr.show c[x,y,pg]
        gr.show c[x,y,pn]
      endif
    next
   next
   game =1
  endif
RETURN
