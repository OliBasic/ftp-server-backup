!Sudoku v1.0
!by Technorobbo
!-------------------
!--------------------
!fn.defs
!--------------------
fn.def blockCheck(B ,idx , N ,box[]) 
T = 0
For i = 1 To N
    T = i + T
Next
X = floor(B / 3) * 27 + floor(idx / 3) * 3

i = 0
For k = 0 To 2
    For j = 0 To 2
        i = i + box[X + k * 9 + j + 1]

    Next
Next

if ((i + N) = T) then
	fn.rtn 1
else
	fn.rtn 0
endif
fn.end
!----------------------------
fn.def rowcheck(B ,idx , N ,box[]) 
T = 0
For i = 1 To N
    T = i + T
Next
B = B * 9

X = 0
For i = B To B + 8
    X = box[i + 1] + X
Next
if ((X + N) = T) then
	fn.rtn 1
else
	fn.rtn 0
endif
fn.end
!------------------------
fn.def colcheck(B ,idx , N ,box[]) 
T = 0
For i = 1 To N
    T = i + T
Next

For i = idx To 80 Step 9
    X = box[i + 1] + X
Next
if ((X + N) = T) then
	fn.rtn 1
else
	fn.rtn 0
endif
fn.end
!------------------------
fn.def wincheck(mask[],game[],box[]) 
	wc = 1
	For i = 1 To 81
	 If mask[i]=0 Then
	     If game[i] <> box[i] Then wc = 0
	 End If
	Next
	fn.rtn wc
fn.end


!--------------------------------
!-------------------------
!Global variables
!--------------------------
mx =0
my =0
solution=0
mxt =0
myt =0
mxc =0
mxn =0
!option 0=easy 1=medium 2=hard
Option=0

Dim box[81]
Dim Tbox[81]
Dim mask[81]
Dim game[81]
Dim Tile[81]
Dim Lines[39]
array.load numstr$[],"1","2","3","4","5","6","7","8","9"
Dim Digits[12]

!debug.on
!debug.echo.on

gr.open 255, 255, 255, 255
gr.Orientation 0
gr.Screen mx, my
gr.Color 255, 0, 0, 0, 0
!-------------------
!screen scaling 
!--------------------
If mx > my Then
    myt = my
    mxt = my
Else
    myt = mx
    mxt = mx
End If
mxc = (mx - mxt) / 2
mxn = mxt / 9

gosub splashscreen

!-------------------
!font scaling 
!--------------------

gr.text.size 10
gr.text.width t,"5"
i=(mxn / 3)*10/t
gr.text.size i

!--------------------
!init graphics
!--------------------
gr.color 255,0,0,0,1
gr.set.stroke 1
For i = 0 To 80
   CurrentX = mxc + mxn * mod(i,9) + mxn / 4
   CurrentY = mxn * floor(i / 9) + mxn*3/4 
	gr.text.draw Tile[i+1],CurrentX,CurrentY," "
Next

gr.set.stroke 1
gr.color 255,0,0,0,1
For i = 0 To 8
    gr.Line Lines[i+1],i * mxn + mxc, 0,i * mxn + mxc, myt
Next
For i = 0 To 8
    gr.Line Lines[i+10], mxc, i * mxn,mxn * 9 + mxc, i * mxn
Next
gr.set.stroke 5
gr.color 255,0,0,0,0
For i = 0 To 9 Step 3
    gr.Line Lines[i+19],i * mxn + mxc, 0,i * mxn + mxc, myt
Next
For i =0 To 9 Step 3
    gr.Line Lines[i+29],mxc, i * mxn,mxn * 9 + mxc, i * mxn
Next

!-----Digits Menu------------
gr.color 255,64,64,64,1
gr.rect Digits[1],mx/2-1.5 *mxn,mxn*2,mx/2+1.5*mxn,mxn*6
gr.color 64,64,64,64,1
gr.rect Digits[2],mx/2-1*mxn,mxn*2.5,mx/2+2*mxn,mxn*6.5
gr.modify Digits[2],"alpha",256
gr.color 255,255,255,255,1

For i = 0 To 8
   CurrentX = mxc + mxn * mod(i,3) + mxn*3 + mxn / 4
   CurrentY = mxn * floor(i / 3) + mxn*2 + mxn*3/4 
	gr.text.draw Digits[i+3],CurrentX,CurrentY,numstr$[i+1]
Next

CurrentX = mx/2
CurrentY = mxn * 5 + mxn*3/4 
gr.text.align 2
gr.text.draw Digits[11],CurrentX,CurrentY,"Clear"

for i=1 to 12
	gr.hide Digits[i]
next

!-------End Menu---------------

!clear mask
gosub MakeClock
gosub gensoduko
Gosub HideClock
gosub genmask
gosub writenums

gr.render

gosub SaveSolution
!-------------------
!Game loop 
!--------------------

do 
	gr.touch touched,x,y

	if touched then
		gosub touchback
	endif

until 0


!OnError:
gr.Close
End

!------------------- 
!------------------- 
!-------------------
!Subroutines
!-------------------- 
!------------ 
genmask:

Mmax = 65
Mmin = 35
For i = 0 To 2
    If Option=i Then X = (i / 2) * (Mmax - Mmin) + Mmin
Next
x=floor(x)
For i = 1 To 81
    mask[i] = 1
    game[i] = 0
Next

For i = 1 To X
    Do
        idx = floor(rnd(0) * 81)
    until mask[1 + idx]=1
    mask[1 + idx] = 0
Next
return
!--------------------------
gensoduko:

NoSoduko = 1
retrys=5

While NoSoduko
    For X = 0 To 80
        box[X + 1] = 0
    Next
    redo = 0
    N=1
    backtrack=retrys
    do
    	for x=1 to 81
    		TBox[x]=Box[x]
    	next
		x=0
		do
			
			idx = floor(rnd(0) * 9)
			redo = 1
			i=0
			do
				
				idx =Mod(idx + 1,9)
				If (box[X * 9 + idx + 1] = 0) then
					test=1
				else
					test=0
				endif
				Gosub DrawClock
				test=test + blockCheck(X,idx, N,box[])
				test=test + rowcheck(X,idx, N,box[])
				test=test + colcheck(X,idx, N,box[])
				
				If test=4 then
					redo = 0
					box[X * 9 + idx + 1] = N
					i=9
				End If
				i=i+1
			until (i>8)
			x=x+1
		until redo=1|(x>8)
		if redo=1 then
			for x=1 to 81
    			Box[x]=TBox[x]
    		next
    		backtrack=backtrack-1
		else
			backtrack=retrys
			n=n+1
		endif
    until (n>9 | backtrack=0)
    
    NoSoduko = redo
repeat
return
!----------------------
writenums:

For i = 1 To 81

    If mask[i]=1 Then
    	  gr.color 255,0,0,0,1
    	  gr.paint.get p
        gr.modify Tile[i],"paint",p
        gr.modify Tile[i],"text", numstr$[box[i]]
    ElseIf solution=1 Then
        gr.color 255,255,0,0,1
    	  gr.paint.get p
        gr.modify Tile[i],"paint",p
        gr.modify Tile[i],"text", numstr$[box[i]]
    ElseIf game[i] > 0 Then
    	  gr.color 255,0,0,255,1
    	  gr.paint.get p
        gr.modify Tile[i],"paint",p
        gr.modify Tile[i],"text", numstr$[game[i]]
    else
    	  gr.color 255,0,0,0,1
    	  gr.paint.get p
        gr.modify Tile[i],"paint",p
        gr.modify Tile[i],"text", " "   
    End If
Next

return
!------------------------
!Splash Screen
splashscreen:

gr.text.size 10
gr.text.width t,"SUDOKU"
i=(mx *2/ 3)*10/t
gr.text.size i
gr.color 255,0,0,255,0
gr.text.draw txt,(mx-mx*2/3)/2,my/2,"SUDOKU"
gr.render
pause 2000
gr.cls
gr.text.size 10
gr.text.width t,"MEDIUM"
i=(mx * 1/ 3)*10/t
gr.text.size i
gr.color 255,0,0,128,1
gr.text.draw txt,(mx/14),my/2,"EASY"
gr.text.draw txt,(mx/14*4.5),my/2,"MEDIUM"
gr.text.draw txt,(mx/14*10),my/2,"HARD"
gr.render

Recheck:

do 
	gr.touch touched,x,y
until touched
gr.bounded.touch touched1, (mx/14),0,(mx/14*4.5),my
gr.bounded.touch touched2, (mx/14*4.5),0,(mx/14*10),my
gr.bounded.touch touched3, (mx/14*10),0,mx,my
gr.bounded.touch2 touched4, (mx/14),0,(mx/14*4.5),my
gr.bounded.touch2 touched5, (mx/14*4.5),0,(mx/14*10),my
gr.bounded.touch2 touched6, (mx/14*10),0,mx,my

if touched1 | touched4 then 
	Option=0
elseif touched2 | touched5 then
	Option=1
elseif touched3 | touched6 then
	Option=2	
else
	goto Recheck
endif

gr.cls
gr.render

popup "Creating Sudoku. Process may take 1 or more minutes",0,0,1

return
!-------------------------
!screen touching
touchback:

If X >= mxc & X < (mxc + mxn * 9) Then
    X = floor((X - mxc) / mxn)
    Y = floor(Y / mxn) * 9
    idx = X + Y
    If mask[1 + idx] Then goto leave
    
    gosub getdigits

    If num > -1 & num < 10 Then 
    	 game[1 + idx] = num
	    gosub writenums
	    gr.render
    endif
End If

If wincheck(mask[],game[],box[]) Then popup "Winner",0,0,0

leave:
return
!--------------------------
!Save Solution and screen cap
SaveSolution:


text.open w,fn,"SudokuSolution.txt"
if fn<>-1 then
		for i=0 to 8
			ftmp$=""
			for j= 0 to 8
				ftmp$=ftmp$ + "|" + numstr$[box[i*9+j+1]] + "|"
			next	
			text.writeln fn,ftmp$
		next	
		text.close fn
endif
gr.save	"CurrentSudoku.png"			

return
!---------------------------
!Get Digit 
getdigits:

do 
	gr.touch touched,dx,dy
until touched=0
 
for i=1 to 12
	gr.show Digits[i]
next
gr.render

do 
	gr.touch touched,dx,dy
until touched

If dx> (mx/2-1.5 *mxn) & dy>(mxn*2) & dx<(mx/2+1.5*mxn) & dy<(mxn*6)
    dx = floor((dx - mxc) / mxn)
    dy = floor(dy / mxn) * 9
    dindex= dx + dy
    
    sw.begin dindex
    	sw.case 21
    		num=1
    		sw.break
    	sw.case 22
    		num=2
    		sw.break
    	sw.case 23
    		num=3
    		sw.break
    	sw.case 30
    		num=4
    		sw.break    	
    	sw.case 31
    		num=5
    		sw.break
    	sw.case 32
    		num=6
    		sw.break
    	sw.case 39
    		num=7
    		sw.break     	
    	sw.case 40
    		num=8
    		sw.break
    	sw.case 41
    		num=9
    		sw.break
    	sw.case 48
    		num=0
    		sw.break     	
    	sw.case 49
    		num=0
    		sw.break
    	sw.case 50
    		num=0
    		sw.break
    	sw.default
    		num=-1
    		sw.break
 	sw.end     	

	 
End If

do 
	gr.touch touched,dx,dy
until touched=0

for i=1 to 12
	gr.hide Digits[i]
next

gr.render 
return
 
 
!--------------------------------------------
!--------------------------------------------
!Draw Clock while processing
MakeClock: 
pi=atan(1)*4
gr.color 255,255,255,255,1
gr.circle DCO1,mx/2,my/2,20
gr.color 255,0,0,0,0
gr.circle DCO2,mx/2,my/2,20 
DCR=mod(clock,1000)/1000 
gr.line DCO3, mx/2,my/2, cos(DCR * pi)*20+mx/2, sin(DCR * pi)*20+my/2
DCSkip=0
gr.hide DCO3
gr.hide DCO2
gr.hide DCO1

return

HideClock:

gr.hide DCO3
gr.hide DCO2
gr.hide DCO1
DCSkip=1
return


return

DrawClock:
if DCSKip=1 then goto XitSub_DrawClock
gr.show DCO1
gr.show DCO2
gr.show DCO3
DCR=mod(clock(),1000)/500 
gr.color 255,0,0,0,0
gr.modify DCO3,"x2",cos(DCR * pi)*20+mx/2
gr.modify DCO3,"y2",sin(DCR * pi)*20+my/2
gr.render

XitSub_DrawClock:
return 
 
 
 
 
 
 
 
 
 
 
 
 
 
