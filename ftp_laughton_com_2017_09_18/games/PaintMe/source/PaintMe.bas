! PaintMe.bas (REQUIRES BASIC! v.1.87.05 or higher)
! Aat Don @2015
GR.OPEN 255,0,0,0,0,0
GR.SCREEN w,h
ScaleX=720
ScaleY=w/h*ScaleX
sx=h/ScaleX
sy=w/ScaleY
GR.SCALE sx,sy
WAKELOCK 3
DataDir$="../../PaintMe/data/"
DIM ThePics[9],ScaledPics[9]
MakeSnd=1
ARRAY.LOAD PicCoord[],22,24,8,19,0,18,33,17,25
GR.TEXT.SIZE 20
GR.BITMAP.CREATE Help,540,830
GR.BITMAP.DRAWINTO.START Help
	GR.COLOR 255,0,255,255,1
	GR.TEXT.DRAW g,270,20,"Hit screen to skip directions."
	GR.COLOR 255,255,255,0,1
	GR.TEXT.UNDERLINE 1
	GR.TEXT.DRAW g,0,40,"PaintMe directions"
	GR.TEXT.UNDERLINE 0
	GR.TEXT.DRAW g,0,80,"Target group for PaintMe are children who like to start with"
	GR.TEXT.DRAW g,0,100,"painting pictures.....Please explain these instructions...."
	GR.TEXT.DRAW g,0,140,"When the start screen is shown,you can"
	GR.TEXT.DRAW g,50,160,"- Toggle sound on/off by tapping the sound button"
	GR.TEXT.DRAW g,50,180,"- Exit PaintMe by tapping the exit button"
	GR.TEXT.DRAW g,50,200,"- Choose an image by tapping on it"
	GR.TEXT.DRAW g,0,240,"After chosing an image you will see the 'paint screen'."
	GR.TEXT.DRAW g,0,260,"On the paint screen you will see the chosen picture on"
	GR.TEXT.DRAW g,0,280,"the lefthand side."
	GR.TEXT.DRAW g,0,300,"On the righthand side you'll see the following controls:"
	GR.TEXT.DRAW g,0,320," 45 color patches -"
	GR.TEXT.DRAW g,170,320,"tap on a color to select it."
	GR.TEXT.DRAW g,170,340,"the selected color will be shown on"
	GR.TEXT.DRAW g,170,360,"both the square below the patches"
	GR.TEXT.DRAW g,170,380,"and the tip of the paintbrush."
	GR.TEXT.DRAW g,0,400," Button 'Done' -"
	GR.TEXT.DRAW g,140,400,"tap on this button to return to the"
	GR.TEXT.DRAW g,140,420,"start screen. You will loose your"
	GR.TEXT.DRAW g,140,440,"colorized image."
	GR.TEXT.DRAW g,0,460," Button 'CLR' -"
	GR.TEXT.DRAW g,130,460,"tap on this button to restore the image"
	GR.TEXT.DRAW g,130,480,"to black and white."
	GR.TEXT.DRAW g,0,500," Button 'Save' -"
	GR.TEXT.DRAW g,140,500,"tap on this button to save the image in"
	GR.TEXT.DRAW g,140,520,"it's current state. This image replaces"
	GR.TEXT.DRAW g,140,540,"the original on your device."
	GR.TEXT.DRAW g,140,560,"(So to have a fresh start simply"
	GR.TEXT.DRAW g,140,580,"clear and save the image.....)"
	GR.TEXT.DRAW g,0,600," A blinking paint brush -"
	GR.TEXT.DRAW g,210,600,"As soon as you tap on the brush (and"
	GR.TEXT.DRAW g,210,620,"keep it tapped) it will be enlarged."
	GR.TEXT.DRAW g,0,660," Drag this paint brush's tip inside the area you want to"
	GR.TEXT.DRAW g,0,680," paint. Lifting your finger will paint the area."
	GR.TEXT.DRAW g,0,700," The 'hot spot' of the paint brush is the very upper left."
	GR.TEXT.DRAW g,0,720," You are not allowed to paint the original black lines."
	GR.TEXT.DRAW g,0,760,"Tapping on the square which shows the current paint color"
	GR.TEXT.DRAW g,0,780,"will fill the image with randomly chosen colors (Auto Fill)."
	GR.TEXT.DRAW g,0,820,"Hit the screen to start and have fun !"
GR.BITMAP.DRAWINTO.END
vtxt=680
GR.BITMAP.DRAW g,Help,200,vtxt
GR.COLOR 255,255,255,255,0
GR.RECT g,18,58,62,662
GR.COLOR 255,0,0,255,1
GR.RECT Bar,20,60,60,660
GR.COLOR 255,0,255,255,1
Barsize=60
GR.RECT Bar,20,60,60,Barsize
GR.TEXT.SIZE 45
GR.TEXT.BOLD 1
GR.COLOR 255,255,0,0,1
GR.ROTATE.START 270,55,440
	GR.TEXT.DRAW g,55,440,"PaintMe"
GR.ROTATE.END
GR.TEXT.SIZE 23
GR.TEXT.ALIGN 2
GR.TEXT.DRAW Counter,40,100,"0"
GR.TEXT.DRAW g,40,130,"%"
GR.TEXT.ALIGN 1
GR.TEXT.BOLD 0
GR.TEXT.SIZE 20
ShowHelp=1
GR.SET.ANTIALIAS 0
FOR i=1 TO 3
	GR.BITMAP.LOAD ThePics[i],DataDir$+"Pic"+INT$(i)+".png"
	GR.BITMAP.SCALE ScaledPics[i],ThePics[i],237,237
NEXT i
GR.BITMAP.LOAD ThePics[4],DataDir$+"Pic4.png"
GR.BITMAP.SCALE ScaledPics[4],ThePics[4],237,237
GR.BITMAP.LOAD ScaledPics[5],DataDir$+"Menu.png"
GR.BITMAP.LOAD ThePics[6],DataDir$+"Pic6.png"
GR.BITMAP.SCALE ScaledPics[6],ThePics[6],237,237
FOR i=7 TO 9
	GR.BITMAP.LOAD ThePics[i],DataDir$+"Pic"+INT$(i)+".png"
	GR.BITMAP.SCALE ScaledPics[i],ThePics[i],237,237
NEXT i
GR.SET.STROKE 2
GR.COLOR 255,0,0,255,1
GR.BITMAP.CREATE Choice,720,720
GR.BITMAP.DRAWINTO.START Choice
	FOR j=0 TO 2
		FOR i=1 TO 3
			GR.BITMAP.DRAW g,ScaledPics[i+j*3],(i-1)*243,j*243
		NEXT i
	NEXT j
GR.BITMAP.DRAWINTO.END
GR.BITMAP.FILL Choice,239,0
! Draw paint brush
GR.COLOR 255,0,0,0,0
GR.BITMAP.CREATE Brush,80,31
GR.BITMAP.DRAWINTO.START Brush
	GR.ARC g,0,-10,30,10,90,90,0
	GR.ARC g,-20,0,20,10,270,90,0
	GR.LINE g,20,5,14,12
	GR.LINE g,20,5,30,8
	GR.LINE g,14,12,24,15
	GR.LINE g,30,8,24,15
	GR.LINE g,30,8,80,28
	GR.LINE g,24,15,80,31
	GR.COLOR 255,128,128,128,1
	GR.BITMAP.FILL Brush,20,10
	GR.COLOR 255,255,0,0,1
	GR.BITMAP.FILL Brush,35,15
	GR.COLOR 255,0,255,255,1
	GR.BITMAP.FILL Brush,4,4
GR.BITMAP.DRAWINTO.END
GR.BITMAP.SCALE LargeBrush,Brush,200,75,0
FOR i=60 TO 660 STEP 60
	GR.ROTATE.START i/10,100,i
		GR.BITMAP.DRAW g,Brush,60,i
	GR.ROTATE.END
NEXT i
AUDIO.LOAD Intro,DataDir$+"Bubble.mp3"
AUDIO.LOAD ColPress,DataDir$+"Plop.mp3"
AUDIO.LOAD Finito,DataDir$+"Aah.mp3"
AUDIO.LOAD Beep,DataDir$+"Beep.mp3"
GR.RENDER
TIMER.SET 500
GOSUB GetTouch
TIMER.CLEAR
GR.CLS
ShowHelp=0
GR.BITMAP.DELETE Help
DO
	! Show menu
	GR.CLS
	GR.BITMAP.DRAW g,Choice,0,0
	GR.RENDER
	Valid=0
	DO
		DO
			GOSUB GetTouch
			PointedTo=INT(x/240)+1+INT(y/240)*3
		UNTIL x<720 & y<720
		IF PointedTo=5 Then
			IF x>275 & x<445 & y>245 & y<280 THEN
				! Sound on off
				IF MakeSnd=0 THEN
					MakeSnd=1
					GR.COLOR 255,0,255,0,1
					GR.BITMAP.FILL Choice,425,265
					GR.BITMAP.FILL Choice,415,265
					GR.RENDER
				ELSE
					MakeSnd=0
					GR.COLOR 255,255,0,0,1
					GR.BITMAP.FILL Choice,425,265
					GR.BITMAP.FILL Choice,415,265
					GR.RENDER
				ENDIF
			ENDIF
			IF x>275 & x<445 & y>425 & y<460 THEN
				! Exit
				IF makeSnd THEN AUDIO.STOP
				WAKELOCK 5
				GR.CLOSE
				EXIT
			ENDIF
		ELSE
			Valid=1
		ENDIF
	UNTIL Valid
	GR.CLS
	IF makeSnd THEN AUDIO.STOP:AUDIO.PLAY Intro
	! Current colour
	GR.COLOR 255,255,255,255,0
	GR.RECT g,738,8,822,472
	GR.RECT g,758,480,802,524
	GR.COLOR 255,0,255,255,1
	GR.RECT Cur_Col,760,482,800,522
	GR.COLOR 255,255,255,0,1
	GR.TEXT.SIZE 20
	GR.ROTATE.START 270,755,520
		GR.TEXT.DRAW g,755,520,"Auto"
	GR.ROTATE.END
	GR.ROTATE.START 270,820,515
		GR.TEXT.DRAW g,820,515,"Fill"
	GR.ROTATE.END
	! Exit button
	GR.COLOR 255,255,255,0,0
	GR.RECT g,740,530,820,570
	GR.COLOR 255,255,255,0,1
	GR.TEXT.BOLD 1
	GR.TEXT.SIZE 30
	GR.TEXT.DRAW g,745,560,"Done"
	! Clear button
	GR.COLOR 255,255,255,0,0
	GR.RECT g,740,580,820,620
	GR.COLOR 255,255,255,0,1
	GR.TEXT.DRAW g,750,610,"CLR"
	! Save button
	GR.COLOR 255,255,255,0,0
	GR.RECT g,740,630,820,670
	GR.COLOR 255,255,255,0,1
	GR.TEXT.DRAW g,745,660,"Save"
	! Paint brush dock
	GR.COLOR 255,255,0,0,1
	GR.RECT g,738,685,822,720
	GR.TEXT.SIZE 20
	GR.COLOR 255,0,0,0,1
	GR.TEXT.DRAW g,760,701,"NOT"
	GR.TEXT.DRAW g,745,719,"allowed"
	GR.COLOR 255,255,255,255,1
	GR.RECT Flash,738,685,822,720
	! Colour patches
	! Black
	GR.COLOR 255,1,1,1,1
	GR.RECT g,740,10,820,30
	! Gray
	FOR i=1 TO 8
		r=i*32-1:g=i*32-1:b=i*32-1:GR.COLOR 255,r,g,b,1
		GR.RECT g,740+(i>4)*40,10+i*20-(i>4)*80,780+(i>4)*40,30+i*20-(i>4)*80
	NEXT i
	FOR i=3 TO 8
		! Red
		r=i*32-1:g=0:b=0:GR.COLOR 255,r,g,b,1
		GR.RECT g,740,50+i*20,780,70+i*20
		! Green
		r=0:g=i*32-1:GR.COLOR 255,r,g,b,1
		GR.RECT g,740,170+i*20,780,190+i*20
		! Blue
		g=0:b=i*32-1:GR.COLOR 255,r,g,b,1
		GR.RECT g,740,290+i*20,780,310+i*20
		! Yellow
		r=i*32-1:g=i*32-1:b=0:GR.COLOR 255,r,g,b,1
		GR.RECT g,780,50+i*20,820,70+i*20
		! Cyan
		r=i*32-1:g=0:b=i*32-1:GR.COLOR 255,r,g,b,1
		GR.RECT g,780,170+i*20,820,190+i*20
		! Magenta
		r=0:g=i*32-1:b=i*32-1:GR.COLOR 255,r,g,b,1
		GR.RECT g,780,290+i*20,820,310+i*20
	NEXT i
	! Create object to be painted
	GR.BITMAP.CREATE Draw,720,720
	GR.BITMAP.DRAWINTO.START Draw
		GR.BITMAP.DRAW g,ThePics[PointedTo],0,0
	GR.BITMAP.DRAWINTO.END
	GR.BITMAP.DRAW PaintDraw,Draw,0,0
	! Start colour
	GR.PAINT.GET PaintCol
	GR.MODIFY Cur_Col,"paint",PaintCol
	GR.BITMAP.FILL Brush,4,4
	! Show Paint brush(es)
	GR.BITMAP.DRAW PaintBrush,Brush,740,687
	GR.BITMAP.DRAW BrushBig,LargeBrush,0,0
	GR.HIDE BrushBig
	TIMER.SET 300
	GR.RENDER
	L=740:T=687:Kap=0
	DO
		DO
			GR.TOUCH touched,x,y
		UNTIL touched
		x=ROUND(x/sx)
		y=ROUND(y/sy)
		IF x>L & x<L+80 & y>T & y<T+30 THEN
			TIMER.CLEAR
			! Center big brush
			GR.MODIFY BrushBig,"x",x-100,"y",y-38
			GR.SHOW BrushBig
			GR.HIDE PaintBrush
			XOffset=x-L
			YOffset=y-T
			DO
				GR.TOUCH touched,x,y
				x=ROUND(x/sx)
				y=ROUND(y/sy)
				GR.MODIFY PaintBrush,"x",x-XOffSet,"y",y-YOffSet
				GR.MODIFY BrushBig,"x",x-100,"y",y-38
				GR.RENDER
			UNTIL !touched
			IF x-100>0 & x-100<720 & y-38>0 & y-38<720 THEN
				GR.GET.BMPIXEL Draw,(x-100),(y-38),A,Rc,Gc,Bc
				IF Rc<>Rf | Gc<>Gf | Bc<>Bf THEN				
					IF Rc>0 | Gc>0 | Bc>0 THEN
						IF makeSnd THEN AUDIO.STOP:AUDIO.PLAY ColPress
						GR.BITMAP.FILL Draw,x-100,y-38
						GR.RENDER
					ELSE
						! Do not change black drawing lines
						FOR i=1 TO 5
							GR.HIDE Flash
							GR.RENDER
							PAUSE 400
							GR.SHOW Flash
							GR.RENDER
						NEXT i
					ENDIF
				ENDIF
			ENDIF
			L=x-XOffset
			T=y-YOffset
			! Keep brush on screen
			IF x<40 THEN GR.MODIFY PaintBrush,"x",0:L=0
			IF x>820 THEN GR.MODIFY PaintBrush,"x",820:L=820
			IF y<15 THEN GR.MODIFY PaintBrush,"y",0:T=0
			IF y>690 THEN GR.MODIFY PaintBrush,"y",690:T=690
			GR.HIDE BrushBig
			TIMER.SET 300
		ELSE
			IF x>740 & x<820 & y>10 & y<470 THEN
				! Pick colour
				IF makeSnd THEN AUDIO.STOP:AUDIO.PLAY Beep
				GR.GET.PIXEL x*sx,y*sy,A,Rf,Gf,Bf
				GR.COLOR 255,Rf,Gf,Bf,1
				GR.PAINT.GET PaintCol
				GR.MODIFY Cur_Col,"paint",PaintCol
				GR.BITMAP.FILL Brush,4,4
				GR.BITMAP.FILL LargeBrush,10,10
				GR.RENDER
			ENDIF
			IF 	x>758 & x<802 & y>480 & y<524 THEN
				! Place back paintbrush
				GR.MODIFY PaintBrush,"x",740,"y",687
				L=740:T=687
				GR.GET.BMPIXEL Brush,4,4,A,Rs,Gs,Bs
				ARRAY.SUM n,PicCoord[1,PointedTo]
				n=n*2-PicCoord[PointedTo]*2+1
				READ.FROM n
				IF makeSnd THEN AUDIO.STOP:AUDIO.PLAY Intro
				FOR i=1 TO PicCoord[PointedTo]
					! Random colour
					ColNr$="00"+BIN$(FLOOR(RND()*7+1)):ColNr$=RIGHT$(ColNr$,3)
					r=VAL(MID$(ColNr$,1,1)):g=VAL(MID$(ColNr$,2,1)):b=VAL(MID$(ColNr$,3,1))
					p=FLOOR(RND()*6+3):r=r*(p*32-1):g=g*(p*32-1):b=b*(p*32-1)
					GR.COLOR 255,r,g,b,1
					READ.NEXT p,q
					GR.BITMAP.FILL Draw,p,q
				NEXT i
				! Reset current colour
				GR.COLOR 255,Rs,Gs,Bs,1
				GR.BITMAP.FILL Brush,4,4
				GR.BITMAP.FILL LargeBrush,10,10
				GR.RENDER
				POPUP "WOW !",-250
			ENDIF
			IF 	x>740 & x<820 & y>530 & y<570 THEN
				! Done
				TIMER.CLEAR
				GR.HIDE PaintBrush
				GR.RENDER
				IF makeSnd THEN AUDIO.STOP:AUDIO.PLAY Finito
				Kap=1
				POPUP "READY !",-250
				PAUSE 2000
			ENDIF
			IF 	x>740 & x<820 & y>580 & y<620 THEN
				! Place back paintbrush
				GR.MODIFY PaintBrush,"x",740,"y",687
				L=740:T=687
				GR.GET.BMPIXEL Brush,4,4,A,Rs,Gs,Bs
				! Clear
				ARRAY.SUM n,PicCoord[1,PointedTo]
				n=n*2-PicCoord[PointedTo]*2+1
				IF makeSnd THEN AUDIO.STOP:AUDIO.PLAY Intro
				READ.FROM n
				GR.COLOR 255,255,255,255,1
				FOR i=1 TO PicCoord[PointedTo]
					READ.NEXT p,q
					GR.BITMAP.FILL Draw,p,q
				NEXT i
				! Reset current colour
				GR.COLOR 255,Rs,Gs,Bs,1
				GR.BITMAP.FILL Brush,4,4
				GR.BITMAP.FILL LargeBrush,10,10
				GR.RENDER
				POPUP "CLEARED !",-250
			ENDIF
			IF 	x>740 & x<820 & y>630 & y<670 THEN
				! Save
				IF makeSnd THEN AUDIO.STOP:AUDIO.PLAY Beep
				GR.BITMAP.SAVE Draw,DataDir$+"Pic"+INT$(PointedTo)+".png"
				GR.BITMAP.LOAD ThePics[PointedTo],DataDir$+"Pic"+INT$(PointedTo)+".png"
				GR.BITMAP.SCALE ScaledPics[PointedTo],ThePics[PointedTo],237,237
				GR.BITMAP.DRAWINTO.START Choice
					GR.BITMAP.DRAW g,ScaledPics[PointedTo],MOD(PointedTO-1,3)*243,INT((PointedTo-1)/3)*243
				GR.BITMAP.DRAWINTO.END
				POPUP "ARTWORK SAVED !",-250
			ENDIF
		ENDIF
	UNTIL Kap
UNTIL 0
END
GetTouch:
	DO
		GR.TOUCH touched,x,y
	UNTIL touched
	DO
		GR.TOUCH touched,x,y
	UNTIL !touched
	x=ROUND(x/sx)
	y=ROUND(y/sy)
RETURN
ONTIMER:
	IF ShowHelp=1 Then
		vtxt=vtxt-5
		GR.MODIFY Help,"y",vtxt
		IF vtxt<-830 THEN vtxt=680:Barsize=60:Percent=0
		GR.MODIFY Bar,"bottom",Barsize
		IF Barsize<660 THEN
			Barsize=Barsize+3.9
			Percent=((Barsize-60)/600)*100
		ENDIF
		GR.MODIFY Counter,"text",INT$(Percent)
		GR.RENDER
	ELSE
		GR.SHOW.TOGGLE Paintbrush
		GR.RENDER
	ENDIF
TIMER.RESUME
ONBACKKEY:
	POPUP "Please, use the menu to Exit",-250
	IF makeSnd THEN AUDIO.STOP:AUDIO.PLAY Beep
BACK.RESUME
READ.DATA 114,103,305,113,292,151,373,163,312,201,405,205,452,125,537,116,489,251,500,451,511,264,151,305,237,271,295,327,425,334,343,294,397,369,406,426,460,373,426,366,445,426,309,525
READ.DATA 617,645,238,590,174,182,542,158,197,319,267,350,252,372,144,360,125,359,249,419,172,470,215,229,485,196,500,251,544,235,410,207,438,345,571,383,503,393,417,477,435,193,451,205,287,220,264,218
READ.DATA 573,477,507,145,460,497,350,675,254,670,113,552,239,568,247,362
READ.DATA 179,195,440,135,378,160,357,328,353,181,323,199,321,209,365,197,374,271,229,346,457,395,442,346,241,317,244,377,361,408,344,579,317,471,302,595,417,615
READ.DATA 502,423,506,491,504,463,464,481,481,488,484,512,594,353,639,322,601,344,585,415,350,528,466,385,412,381,392,332,422,353,461,342,356,269,267,172
READ.DATA 127,136,253,166,271,516,295,490,328,505,384,497,417,483,443,418,444,475,469,445,240,504,272,546,325,555,312,574,355,619,514,577,445,537,292,557,292,523,444,461,228,529,463,411,415,449,334,336,308,382,398,347,400,435,387,440,430,364,519,153,487,79,528,274,529,326
READ.DATA 576,638,417,524,302,183,243,203,200,157,182,243,384,506,513,276,381,184,513,578,578,598,549,434,389,340,494,373,401,448,434,539,323,502
READ.DATA 126,336,216,147,367,156,303,246,278,358,301,368,322,414,407,250,332,685,471,608,501,605,538,656,403,313,536,295,321,466,316,542,262,539,401,564,463,534,333,353,391,386,417,500,268,641,258,578,411,447