!!
---------------------------------
Number Puzzle V2 by Rob Tauler (TechnoRobbo) 
---------------------------------
!!
debug.on

!debug.echo.on

gr.open 255,0, 0, 0, 0
gr.Orientation 0
gr.Screen mx, my
dim xy[16,4]
dim pt[16]
dim tile[16]
dim bmptile[16]

array.load swap[],-4,-1,1,4
gr.color 255,0,64,0,1

gr.rect board, 0,0,my,my
mxt=my/4

myt=mxt

stopwatch=0
swOn=0
moves=0

 
gr.bitmap.load tmp,"NumPuz.PNG"
gr.bitmap.scale gfx,tmp, mxt*4,myt*4
gr.bitmap.delete tmp 
gr.bitmap.size gfx, bx,by

for I =0 to 15
   gr.bitmap.crop bmptile[I+1],gfx,mod(I,4)*mxt,floor(I/4)*myt,mxt,myt

next
gr.bitmap.delete gfx

gr.set.stroke 2

 gr.color 255,255,255,255,0 
for I =0 to 15
   
   xy[I+1,1] = mod(I,4)*mxt
   xy[I+1,2]=floor(I/4)*myt 
   xy[I+1,3] = xy[I+1,1]+mxt 
   xy[I+1,4] = xy[I+1,2]+myt
   pt[I+1]=I+1
next  

for I = 1 to 16
     gr.color 255,256-16*I,16*I-1,255,1
     gr.bitmap.draw tile[I],bmptile[I],xy[I,1], xy[I,2] 
next
gr.hide tile[16]
oldindex=16

gosub reset
 
gr.color 255,255,0,0,1
gr.text.size myt/4
gr.text.align 2
gr.text.draw swtext,mxt*4.5,myt, format$("####%.#",0) 
gr.text.draw swtext2,mxt*4.5,myt*0.5, "TIMER"

gr.color 255,0,0,255,1 
gr.rect button, 4.1*mxt,2.25*myt ,5*mxt,2.6*myt
gr.color 255,0,0,0,1
gr.text.draw swtext3,mxt*4.5,myt*2.5, " RESET" 
GR.HIDE button
do
	gosub getindex
	if pt[index]<16 & touched then
	   oldindex=index    
	   do
	      gosub getindex
	   until (touched=0  | pt[index]=16)
	   if pt[index]=16 then
	   	delta=abs(oldindex-index)
			 if delta=1 | delta=4 then
				tmp= pt[index] 
				pt[index]=pt[oldindex]
				pt[oldindex]=tmp
				do
				    gr.touch touched,x,y 
				until touched= 0
				if swon=0 then
					swon=1
					stopwatch=clock()
				endif
				for I =1 to 16
				   gr.modify tile[pt[i]],"x",xy[I,1]
				   gr.modify tile[pt[i]],"y",xy[I,2]  
				next 
			endif
	   endif 
	 endif
   if swon=1 then  gosub checkwin
   if swon=1 then
   	tmp=(clock()-stopwatch)/1000
   	gr.modify swtext,"text",format$("####%.#",tmp)
   elseif swon=2 then
      gr.bounded.touch touched, 4.1*mxt,2.25*myt ,5*mxt,2.6*myt
      if touched then 
         swon=0 
         gosub reset
         gr.hide button
      endif
   endif
   gr.render
until 0
 
OnError:
gr.Close 
End 
 
getindex:
   gr.bounded.touch touched,0,0,mxt*4,myt*4
   index=oldindex
   if touched then 
     gr.touch touched,x,y
     index=floor(x/mxt)+floor(y/myt)*4+1 
     if index>16 then index=16
     if index<1 then index=1
   endif
return 

checkwin:
good=1
for I = 1 to 16
   if pt[I]<>I then good=0
next
if good =1 then
  swon=2
  tone 2000,500
  popup "Finished!!!",0,0,0
  gr.show button
endif
return
 
reset:
!array.shuffle pt[]
gosub fifteen
stopwatch=0
gr.modify swtext,"text",format$("####%.#",0) 
for I =1 to 16
   gr.modify tile[pt[i]],"x",xy[I,1]
	gr.modify tile[pt[i]],"y",xy[I,2]  
next 
return
 
fifteen:
for I = 1 to 16
   if pt[I]=I
next 
xtile=16
for I=1 to 1000
   idx=swap[floor(rnd(0)*4)+1]

   if idx=-1 then
      if xtile=1 | xtile=5 | xtile=9 | xtile=13 then idx=1
   elseif idx=1 then
      if xtile=4 | xtile=8 | xtile=12 | xtile=16 then idx=-1 
   elseif idx=-4
      if xtile=1 | xtile=2 | xtile=3 | xtile=4 then idx=4
   elseif idx=4 then
      if xtile=13 | xtile=14 | xtile=15 | xtile=16 then idx=-4 
   endif
   pt[xtile]= pt[xtile+idx] 
   xtile= xtile+idx
   pt[xtile]=16  
next
return
 
 
 
 
