! Card trick
! Aat Don 2014
 FN.DEF roundButtons(Txt$,TxtSize)
	! TxT$ is comma separated text (number of words = number of buttons)
 	SPLIT T$[],Txt$,","
	ARRAY.Length L,T$[]
	LL=0
	LT=1
	FOR i=1 TO L
		IF LEN(T$[i])>LL THEN
			LL=LEN(T$[i])
			LT=i
		ENDIF
	NEXT i
	GR.TEXT.SIZE TxtSize*1.3
	GR.GET.TEXTBOUNDS T$[LT],l1,t1,r1,b1
	Stroke=TxTSize/4
	b=1.5*r1
	h=2.3*-t1
	x=b/2+Stroke/2
	y=h/2+Stroke/2
	r=30
	GR.SET.STROKE Stroke
	GR.TEXT.ALIGN 2
	GR.TEXT.BOLD 1
	LIST.CREATE N,Buttons
	LIST.ADD Buttons,L
	FOR i=1 TO L
		GR.BITMAP.CREATE P,b+Stroke+1,h+Stroke+1
		GR.BITMAP.DRAWINTO.START P
			if b<2*r then r=b/2
			if  h<2*r then r=h/2
			half_pi=3.14159/2
			dphi=half_pi/8
			LIST.CREATE N,S1
			mx=-b/2+r
			my=-h/2+r
			FOR phi=0 TO half_pi STEP dphi
				LIST.ADD s1,mx-COS(phi)*r,my-sin(phi)*r
			NEXT phi
			mx=b/2-r
			my=-h/2+r
			FOR phi=0 TO half_pi STEP dphi
				LIST.ADD s1,mx+SIN(phi)*r,my-cos(phi)*r
			NEXT phi
			mx=b/2-r
			my=h/2-r
			FOR phi=0 TO half_pi STEP dphi
				LIST.ADD s1,mx+COS(phi)*r,my+sin(phi)*r
			NEXT phi
			mx=-b/2+r
			my=h/2-r
			FOR phi=0 TO half_pi STEP dphi
				LIST.ADD s1,mx-SIN(phi)*r,my+cos(phi)*r
			NEXT phi
			C$=RIGHT$("000"+BIN$(i),3)
			! Inside
			GR.COLOR 255,VAL(MID$(C$,2,1))*255,VAL(MID$(C$,3,1))*255,VAL(MID$(C$,1,1))*255,1
			GR.POLY nn0,s1,x,y
			! Border shadow
			GR.COLOR 255,255,255,255,0
			GR.POLY nn1,s1,x,y
			! Border
			GR.COLOR 255,120,120,140,0
			GR.POLY nn2,s1,x+1,y+1
			! Text
			GR.COLOR 255,VAL(MID$(C$,1,1))*255,VAL(MID$(C$,2,1))*255,VAL(MID$(C$,3,1))*255,1
			GR.TEXT.DRAW g,b/2+Stroke/2,h/2+h/6+Stroke/2,T$[i]
			GR.COLOR 255,255,0,0,0
		GR.BITMAP.DRAWINTO.END
		LIST.ADD Buttons,P
	NEXT i
	UNDIM T$[]
	LIST.ADD Buttons,b+Stroke+1,h+Stroke+1
	! Buttons contains: number of pointers + pointers to drawable bitmaps + their width + their height
	FN.RTN Buttons
FN.END
GR.OPEN 255,0,0,0,0,1
PAUSE 1000
GR.SCREEN w,h
ScaleX=720
ScaleY=h/w*ScaleX
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
WAKELOCK 3
DIM FullDeck[52],DeckPics[52],SelCards[21],Temp[21],Suits[4,3],Intro[18]
GR.TEXT.SIZE 40
GR.COLOR 255,255,0,255,1
GR.TEXT.DRAW SM,20,50,"Please, read this carefully...."
GR.TEXT.SIZE 150
GR.COLOR 255,128,128,128,1
GR.TEXT.DRAW SP,RND()*200+50,300,CHR$(9824)
GR.COLOR 255,128,0,0,1
GR.TEXT.DRAW HE,RND()*100+400,400,CHR$(9827)
GR.COLOR 255,128,128,128,1
GR.TEXT.DRAW CL,RND()*200+50,700,CHR$(9829)
GR.COLOR 255,128,0,0,1
GR.TEXT.DRAW DI,RND()*100+400,800,CHR$(9830)
GR.RENDER
GR.TEXT.SIZE 40
GR.COLOR 255,255,255,0,1
GR.TEXT.DRAW Intro[1],20,150,"This wellknown trick always works !"
GR.TEXT.DRAW Intro[2],20,200,"If you search the Internet you will find"
GR.TEXT.DRAW Intro[3],20,250,"how it works. I certainly won't tell !"
GR.TEXT.DRAW Intro[4],20,300,"You pick a card from a set of 21 cards"
GR.TEXT.DRAW Intro[5],20,350,"(out of 52) and memorize it well."
GR.TEXT.DRAW Intro[6],20,400,"I will lay out the cards in 3 columns"
GR.TEXT.DRAW Intro[7],20,450,"and ask you to select the row"
GR.TEXT.DRAW Intro[8],20,500,"containing your memorized card."
GR.TEXT.DRAW Intro[9],20,550,"Then I will lay out the cards again"
GR.TEXT.DRAW Intro[10],20,600,"and ask the same question."
GR.TEXT.DRAW Intro[11],20,650,"Again the same procedure, after which"
GR.TEXT.DRAW Intro[12],20,700,"I will read your mind and (hopefully)"
GR.TEXT.DRAW Intro[13],20,750,"will point out YOUR CARD !!!"
GR.TEXT.DRAW Intro[14],20,800,"Have fun......."
GR.TEXT.DRAW Intro[15],20,850,"And amaze your friend with this app.. "
GR.TEXT.DRAW Intro[16],20,900,"Created with RFO Basic !"
GR.TEXT.DRAW Intro[17],20,950,"------------------------"
FOR i=1 TO 17
	 GR.HIDE Intro[i]
NEXT i
GR.COLOR 255,0,255,255,1
GR.PAINT.GET txtcol
GR.RENDER
! Define deck
LN=0
TIMER.SET 1000
GOSUB MakeJack
GOSUB MakeQueen
GOSUB MakeKing
GR.BITMAP.CREATE Jack,73,132
GR.BITMAP.DRAWINTO.START Jack
	GR.BITMAP.DRAW g,JackPic,0,0
	GR.ROTATE.START 180,73,132
		GR.BITMAP.DRAW g,JackPic,73,132
	GR.ROTATE.END
GR.BITMAP.DRAWINTO.END
GR.BITMAP.CREATE Queen,73,132
GR.BITMAP.DRAWINTO.START Queen
	GR.BITMAP.DRAW g,QueenPic,0,0
	GR.ROTATE.START 180,73,132
		GR.BITMAP.DRAW g,QueenPic,73,132
	GR.ROTATE.END
GR.BITMAP.DRAWINTO.END
GR.BITMAP.CREATE King,73,132
GR.BITMAP.DRAWINTO.START King
	GR.BITMAP.DRAW g,KingPic,0,0
	GR.ROTATE.START 180,73,132
		GR.BITMAP.DRAW g,KingPic,73,132
	GR.ROTATE.END
GR.BITMAP.DRAWINTO.END
GR.HIDE PB
GR.SET.STROKE 6
GR.TEXT.ALIGN 2
GR.TEXT.BOLD 1
GR.TEXT.SIZE 30
GR.TEXT.TYPEFACE 4
! Spades large
GR.BITMAP.CREATE Suits[1,1],60,80
GR.BITMAP.DRAWINTO.START Suits[1,1]
	LIST.CREATE n,SpadesPart
	LIST.ADD SpadesPart,30,0,59,37,1,37
	GR.COLOR 255,0,0,0,1
	GR.CIRCLE g,15,45,15
	GR.CIRCLE g,45,45,15
	GR.LINE g,30,35,30,70
	GR.ARC g,20,60,40,80,180,180,1
	GR.POLY g,SpadesPart
GR.BITMAP.DRAWINTO.END
! Spades small
GR.BITMAP.SCALE Suits[1,3],Suits[1,1],20,27
! Spades medium
GR.BITMAP.SCALE Suits[1,2],Suits[1,1],25,33
! Hearts large
GR.BITMAP.CREATE Suits[2,1],60,80
GR.BITMAP.DRAWINTO.START Suits[2,1]
	LIST.CREATE n,HeartPart
	LIST.ADD HeartPart,1,22,59,22,30,65
	GR.COLOR 255,255,0,0,1
	GR.CIRCLE g,15,15,15
	GR.CIRCLE g,45,15,15
	GR.CIRCLE g,30,20,5
	GR.POLY g,HeartPart
GR.BITMAP.DRAWINTO.END
! Hearts small
GR.BITMAP.SCALE Suits[2,3],Suits[2,1],20,27
! Hearts medium
GR.BITMAP.SCALE Suits[2,2],Suits[2,1],25,33
! Clubs large
GR.BITMAP.CREATE Suits[3,1],60,80
GR.BITMAP.DRAWINTO.START Suits[3,1]
	GR.COLOR 255,0,0,0,1
	GR.CIRCLE g,30,10,10
	GR.CIRCLE g,15,30,10
	GR.CIRCLE g,45,30,10
	GR.LINE g,20,30,40,30
	GR.LINE g,30,15,30,50
	GR.ARC g,20,40,40,60,180,180,1
GR.BITMAP.DRAWINTO.END
! Clubs small
GR.BITMAP.SCALE Suits[3,3],Suits[3,1],20,27
! Clubs medium
GR.BITMAP.SCALE Suits[3,2],Suits[3,1],25,33
! Diamonds large
GR.BITMAP.CREATE Suits[4,1],60,80
GR.BITMAP.DRAWINTO.START Suits[4,1]
	GR.COLOR 255,255,0,0,1
	GR.ROTATE.START 45,21,21
		GR.RECT g,20,10,62,52
	GR.ROTATE.END
GR.BITMAP.DRAWINTO.END
! Diamonds small
GR.BITMAP.SCALE Suits[4,3],Suits[4,1],20,27
! Diamonds medium
GR.BITMAP.SCALE Suits[4,2],Suits[4,1],25,33
! Draw empty cards
FOR i=1 TO 52
	GR.BITMAP.CREATE DeckPics[i],160,220
	GR.BITMAP.DRAWINTO.START DeckPics[i]
		! Empty Card 
		GR.COLOR 255,255,255,255,1
		GR.RECT g,10,0,150,220
		GR.RECT g,0,10,160,210
		GR.ARC g,0,0,21,21,180,90,1
		GR.ARC g,139,0,160,21,270,90,1
		GR.ARC g,139,199,160,220,0,90,1
		GR.ARC g,0,199,21,220,90,90,1
		GR.COLOR 150,255,255,0,0
		GR.LINE g,10,0,150,0
		GR.LINE g,10,220,150,220
		GR.RECT g,0,10,0,210
		GR.RECT g,160,10,160,210
		GR.ARC g,0,0,21,21,180,90,0
		GR.ARC g,139,0,160,21,270,90,0
		GR.ARC g,139,199,160,220,0,90,0
		GR.ARC g,0,199,21,220,90,90,0
	GR.BITMAP.DRAWINTO.END
NEXT i
DO
UNTIL LN>=17
TIMER.CLEAR
TONE 1000,100,0
GR.HIDE SP
GR.HIDE HE
GR.HIDE CL
GR.HIDE DI
GR.MODIFY SM,"paint",txtcol
GR.MODIFY SM,"text","Tap screen to get startled !"
GR.RENDER
DO
	GR.TOUCH touched,x,y
UNTIL touched
TONE 1000,100,0
FOR Suit=1 TO 4
	! 1=Spades. 2=Hearts 3=Clubs 4=Diamonds
	! Ace
	GR.BITMAP.DRAWINTO.START DeckPics[1+(Suit-1)*13]
		GR.COLOR 255,0,0,0,1
		GR.TEXT.DRAW g,20,30,"A"
		GR.ROTATE.START 180,140,190
			GR.TEXT.DRAW g,140,190,"A"
		GR.ROTATE.END
		GR.BITMAP.DRAW g,Suits[Suit,1],50,70
		GR.BITMAP.DRAW g,Suits[Suit,3],12,35
		GR.ROTATE.START 180,150,185
			GR.BITMAP.DRAW g,Suits[Suit,3],150,185
		GR.ROTATE.END
	GR.BITMAP.DRAWINTO.END
	GR.BITMAP.DRAW g,DeckPics[1+(Suit-1)*13],10+RND()*100+(Suit-1)*150,100+RND()*500
	GR.RENDER
	TONE 800+RND()*200,5,0
	! 2-10
	FOR i=2 TO 10
		GR.BITMAP.DRAWINTO.START DeckPics[i+(Suit-1)*13]
			GR.COLOR 255,0,0,0,1
			GR.TEXT.DRAW g,20,30,REPLACE$(FORMAT$("#%",i)," ","")
			GR.ROTATE.START 180,140,190
				GR.TEXT.DRAW g,140,190,REPLACE$(FORMAT$("#%",i)," ","")
			GR.ROTATE.END
			GR.BITMAP.DRAW g,Suits[Suit,3],12,35
			GR.ROTATE.START 180,150,185
				GR.BITMAP.DRAW g,Suits[Suit,3],150,185
			GR.ROTATE.END
			IF i=2 | i=3 THEN
				GR.BITMAP.DRAW g,Suits[Suit,2],70,30
				GR.ROTATE.START 180,95,190
					GR.BITMAP.DRAW g,Suits[Suit,2],95,190
				GR.ROTATE.END
			ENDIF
			IF i=3 | i=5 | i=9 THEN
				GR.BITMAP.DRAW g,Suits[Suit,2],70,100
			ENDIF
			IF i>3 & i<11 THEN
				GR.BITMAP.DRAW g,Suits[Suit,2],45,30
				GR.BITMAP.DRAW g,Suits[Suit,2],95,30
				GR.ROTATE.START 180,70,195
					GR.BITMAP.DRAW g,Suits[Suit,2],70,195
				GR.ROTATE.END
				GR.ROTATE.START 180,120,195
					GR.BITMAP.DRAW g,Suits[Suit,2],120,195
				GR.ROTATE.END
			ENDIF
			IF i>5 & i<9 THEN
				GR.BITMAP.DRAW g,Suits[Suit,2],45,100
				GR.BITMAP.DRAW g,Suits[Suit,2],95,100
			ENDIF
			IF i=7 | i=8 THEN
				GR.BITMAP.DRAW g,Suits[Suit,2],70,65
			ENDIF
			IF i=8 THEN
			GR.ROTATE.START 180,95,165
				GR.BITMAP.DRAW g,Suits[Suit,2],95,165
			GR.ROTATE.END
			ENDIF
			IF i=9 | i=10 THEN
				GR.BITMAP.DRAW g,Suits[Suit,2],45,75
				GR.BITMAP.DRAW g,Suits[Suit,2],95,75
				GR.ROTATE.START 180,70,155
					GR.BITMAP.DRAW g,Suits[Suit,2],70,155
				GR.ROTATE.END
				GR.ROTATE.START 180,120,155
					GR.BITMAP.DRAW g,Suits[Suit,2],120,155
				GR.ROTATE.END
			ENDIF
			IF i=10 THEN
				GR.BITMAP.DRAW g,Suits[Suit,2],70,55
				GR.ROTATE.START 180,95,175
					GR.BITMAP.DRAW g,Suits[Suit,2],95,175
				GR.ROTATE.END
			ENDIF
		GR.BITMAP.DRAWINTO.END
		GR.BITMAP.DRAW g,DeckPics[i+(Suit-1)*13],10+RND()*100+(Suit-1)*150,100+RND()*500
		GR.RENDER
		TONE 800+RND()*200,5,0
	NEXT i
	! Jack, Queen, King
	FOR i=11 TO 13
		GR.BITMAP.DRAWINTO.START DeckPics[i+(Suit-1)*13]
			GR.SET.STROKE 6
			GR.COLOR 255,0,0,0,1
			GR.BITMAP.DRAW g,Suits[Suit,3],12,35
			GR.ROTATE.START 180,150,185
				GR.BITMAP.DRAW g,Suits[Suit,3],150,185
			GR.ROTATE.END
			IF Suit/2=FLOOR(Suit/2) THEN
				GR.COLOR 255,255,0,255,1
			ELSE
				GR.COLOR 255,0,255,255,1
			ENDIF
			GR.RECT g,35,40,125,180
			GR.COLOR 255,0,0,0,1
			IF i=11 THEN
				K$="J"
				GR.BITMAP.DRAW g,Jack,46,44
			ENDIF
			IF i=12 THEN
				K$="Q"
				GR.BITMAP.DRAW g,Queen,46,44
			ENDIF
			IF i=13 THEN
				K$="K"
				GR.BITMAP.DRAW g,King,46,44
			ENDIF
			GR.TEXT.DRAW g,20,30,K$
			GR.ROTATE.START 180,140,190
				GR.TEXT.DRAW g,140,190,K$
			GR.ROTATE.END
			GR.COLOR 255,0,0,0,0
			GR.SET.STROKE 1
			GR.RECT g,35,40,125,180
		GR.BITMAP.DRAWINTO.END
		GR.BITMAP.DRAW g,DeckPics[i+(Suit-1)*13],10+RND()*100+(Suit-1)*150,100+RND()*500
		GR.RENDER
		TONE 800+RND()*200,5,0
	NEXT i
NEXT Suit
GR.TEXT.ALIGN 1
GR.TEXT.SIZE 40
GR.TEXT.TYPEFACE 1
GOSUB Shuffle
GR.MODIFY SM,"text","I will show you 21 shuffled cards"
GR.COLOR 0,0,0,0,1
GR.RECT Fade,0,90,720,1000
FOR i=5 TO 255 STEP 5
	GR.MODIFY Fade,"alpha",i
	GR.RENDER
	PAUSE 20
NEXT i
Again:
GR.CLS
GR.COLOR 255,255,255,0,1
GR.TEXT.DRAW g,10,45,"Please, select a card to memorize"
GR.TEXT.DRAW g,10,90,"then tap on the column it is in"
GR.RENDER
GOSUB Deal
GOSUB GetColumn
FOR q=1 TO 2
	GOSUB DoTrick
	GR.TEXT.DRAW g,10,45,"Please, tap on column your card is in"
	GR.RENDER
	GOSUB GetColumn
NEXT q
GR.TEXT.DRAW g,50,100,"Now, concentrate on your card !"
GR.TEXT.DRAW g,50,200,"and let me think......"
PAUSE 2000
A$="ABRACADABRA"
GR.TEXT.SIZE 80
GR.COLOR 255,255,0,0,1
FOR i=1 TO LEN(A$)
	GR.TEXT.DRAW g,20+(i-1)*60,400,MID$(A$,i,1)
	GR.RENDER
	TONE 500+i*50,100,0
	PAUSE 500
NEXT i
GR.CLS
GR.TEXT.SIZE 40
GR.COLOR 255,255,255,0,1
GR.TEXT.DRAW g,50,50,"Your card ......."
GR.RENDER
GR.BITMAP.SCALE TheCard,DeckPics[SelCards[Col+9]],480,660
PAUSE 2000
GR.BITMAP.DRAW g,TheCard,120,150
GR.RENDER
TONE 1000,100,0
PAUSE 2000
BtnTxt$="YES,NO"
Buttons=roundButtons(BtnTxt$,50)
GR.SET.STROKE 3
GR.TEXT.ALIGN 1
GR.TEXT.BOLD 0
GR.TEXT.SIZE 50
GR.COLOR 255,0,255,0,1
Q$="PLAY AGAIN ?"
GR.TEXT.DRAW g,204,554,Q$
GR.COLOR 255,255,255,255,1
GR.TEXT.DRAW g,200,550,Q$
GR.COLOR 255,255,0,0,0
GR.TEXT.DRAW g,200,550,Q$
GR.RENDER
! Retrieve number of bitmaps
LIST.GET Buttons,1,NumOfPntrs
FOR i=1 TO NumOfPntrs
	! Retrieve pointers to the bitmaps
	LIST.GET Buttons,i+1,BMP
	GR.BITMAP.DRAW g1,BMP,160+(i-1)*200,600
NEXT i
GR.RENDER
! Retrieve width and height of bitmaps
LIST.GET Buttons,NumOfPntrs+2,BWidth
LIST.GET Buttons,NumOfPntrs+3,BHeight
Tapped=0
DO
	DO
		GR.TOUCH touched,x,y
	UNTIL touched
	DO
		GR.TOUCH touched,x,y
	UNTIL !touched
	x/=sx
	y/=sy
	IF x>160 & x<160+BWidth & Y>600 & Y<600+BHeight THEN
		Tapped=1
	ENDIF
	IF x>360 & x<360+BWidth & Y>600 & Y<600+BHeight THEN
		Tapped=2
	ENDIF
UNTIL Tapped>0
TONE 1000,100,0
IF Tapped=1 THEN
	GR.TEXT.SIZE 40
	GOSUB Shuffle
	GOTO Again
ENDIF
WAKELOCK 5
GR.CLOSE
EXIT
GetColumn:
	DO
		GR.TOUCH touched,x,y
	UNTIL touched
	DO
		GR.TOUCH touched,x,y
	UNTIL !touched
	x/=sx
	Col=FLOOR(x/240)+1
	GR.COLOR 255,0,255,0,1
	GR.TEXT.SIZE 20
	GR.TEXT.DRAW g,20+(Col-1)*240,900,"OK, THIS COLUMN !"
	GR.RENDER
	TONE 1000,100,0
	PAUSE 1000
	GR.CLS
	GR.TEXT.SIZE 40
	GR.COLOR 255,255,255,0,1
RETURN
Shuffle:
	FOR i=1 TO 52
		FullDeck[i]=i
	NEXT i
	ARRAY.SHUFFLE FullDeck[]
	ARRAY.COPY FullDeck[1,21],SelCards[]
RETURN
Deal:
	FOR i=1 TO 7
		FOR j=1 TO 3
			GR.ROTATE.START 5-RND()*11,35+(j-1)*225,i*80+50
				GR.BITMAP.DRAW g,DeckPics[SelCards[j+(i-1)*3]],35+RND()*50+(j-1)*225,i*80+50+RND()*20
			GR.ROTATE.END
			GR.RENDER
			TONE 800+RND()*200,5,0
		NEXT j
	NEXT i
	GR.LINE g,240,850,240,960
	GR.LINE g,480,850,480,960
	GR.RENDER
RETURN
DoTrick:
	FOR i=1 TO 19 STEP 3
		SWAP SelCards[1+i],SelCards[Col-1+i]
	NEXT i
	ARRAY.COPY SelCards[],Temp[]
	FOR i=1 TO 21
		SelCards[i]=Temp[MOD(1+(i-1)*3,21)+FLOOR(i/7.5)]
	NEXT i
	GOSUB Deal
RETURN
MakeJack:
GOSUB GetJackData
PWidth=73
PHeight=66
GR.BITMAP.CREATE JackPic,PWidth,PHeight
GR.BITMAP.DRAWINTO.START JackPic
GOSUB Decode
RETURN
MakeQueen:
GOSUB GetQueenData
GR.BITMAP.CREATE QueenPic,PWidth,PHeight
GR.BITMAP.DRAWINTO.START QueenPic
GOSUB Decode
RETURN
MakeKing:
GOSUB GetKingData
GR.BITMAP.CREATE KingPic,PWidth,PHeight
GR.BITMAP.DRAWINTO.START KingPic
GOSUB Decode
RETURN
Decode:
x1=0
y1=0
x2=0
y2=0
Counter=1
DO
	n=UCODE(MID$(PDat$,Counter,1))-128
	Counter=Counter+1
	r=UCODE(MID$(PDat$,Counter,1))-128
	C ounter=Counter+1
	g=UCODE(MID$(PDat$,Counter,1))-128
	Counter=Counter+1
	b=UCODE(MID$(PDat$,Counter,1))-128
	Counter=Counter+1
	IF r=255 & g=255 & b=255 THEN
		d=1
	ELSE
		d=0
	ENDIF
	GR.COLOR 255,r,g,b,1
	y2=y2+n-1
	IF y2<PHeight THEN
		IF d=0 THEN GR.LINE g,x1,y1,x2,y2+1
			y2=y2+1
			y1=y2
	ELSE
		WHILE y2>=PHeight-1
			IF d=0 THEN GR.LINE g,x1,y1,x2,PHeight
			y2=y2-PHeight
			y1=0
			x1=x1+1
			x2=x1
		REPEAT
		IF d=0 THEN GR.LINE g,x1,y1,x2,y2+1
		y2=y2+1
		y1=y2
	ENDIF
UNTIL x1>=PWidth
GR.BITMAP.DRAWINTO.END
GR.RENDER
RETURN
GetJackData:
PDat$="ăſſſŷŦť¸ſſſŷŦťŷųŵſſſŷųŵſſſĲĵĭµſſſŷŦťſſſŷųŵŷŦťŷųŵſſſŷŦťſſſĲĵĭſſſŷųŵ©ſſſŷųŵŷŦťŷųŵſſſŷŦťſſſŷŦťſſſŷųŵĲĵĭÌ±°ſſſŷŦťŲĭðŷŦť¥ſſſŷŦťŷųŵſſſŷŦťŷųŵŷŦťŷųŵŷŦťĲĵĭÌ±°ſſſŷŦťŲĭðŷŦť¤ſſſŷųŵŷŦťſſſŷŦťŷųŵŷŦťĲĵĭęĄâŷŦťſſſŲĭðſſſŷųŵŷŦťŲĭðŷŦťŷųŵſſſŷŦťſſſŲĭðŷųŵſſſŷųŵŷŦťŲĭðĶ®£ŲĭðĲĵĭŷŦťĲĵĭŷŦťſſſŲĭðŷŦťŲĭðĶ®£Ì±°ŷŦťŷųŵŷŦťŷųŵŷŦťęĄâſſſŲĭðĶ®£Ì±°ęĄâĶ®£ŷŦťŷųŵĲĵĭÌ±°ſſſŷŦťŲĭðŷŦťŷųŵſſſŷųŵŷŦťŲĭðęĄâĶ®£Ì±°Ķ®£ŷŦťſſſŲĭðŷŦťŷųŵŷŦťęĄâÌ±°ſſſŷŦťŷųŵſſſŷŦťęĄâÌ±°Ķ®£ŲĭðŷųŵŷŦťŷųŵĲĵĭÌ±°ſſſŷŦťŷųŵſſſŷŦťęĄâÌ±°ĲĵĭęĄâĶ®£ŲĭðŷųŵŷŦťęĄâÌ±°ęĄâÌ±°Ķ®£ſſſŷŦťŷųŵſſſŷųŵĲĵĭÌ±°ęĄâŷŦťſſſĲĵĭĶ®£ŲĭðſſſŷŦťÌ±°ęĄâÌ±°Ķ®£ſſſŷŦťſſſĲĵĭęĄâÌ±°ęĄâĲĵĭſſſŷŦťŲĭðĶ®£ĲĵĭÌ±°Ķ®£ſſſŷŦťĲĵĭŷŦťſſſŷŦťęĄâÌ±°ĲĵĭŷųŵſſſĲĵĭĶ®£Ì±°Ķ®£ſſſĲĵĭŷŦťĲĵĭÌ±°ęĄâŷŦťſſſŷųŵŲĭðĶ®£Ì±°ęĄâÌ±°Ķ®£ſſſŷŦťĲĵĭÌ±°ęĄâĲĵĭŷŦťſſſĲĵĭęĄâĶ®£Ì±°Ķ®£ſſſŷŦťęĄâŷŦťſſſŷŦťſſſŷųŵęĄâŲĭðęĄâÌ±°Ķ®£ſſſĲĵĭęĄâĲĵĭſſſŷŦťſſſŷŦťęĄâĶ®£ęĄâĶ®£ſſſŷŦťĲĵĭŷŦťſſſŷŦťſſſĲĵĭęĄâĶ®£¨ſſſŷŦťſſſĲĵĭęĄâĶ®£ªſſſŷŦťſſſŷųŵęĄâĶ®£ęĄâĶ®£¬ſſſŷŦťęĄâĶ®£ęĄâĶ®£¬ſſſŷŦťęĄâĶ®£«ſſſŷųŵęĄâĶ®£ſſſŷŦťŷųŵ¢ſſſŲĭðĶ®£ęĄâĶ®£Ì±°Ķ®£ſſſŷŦťęĄâĲĵĭŷŦť ſſſŲĭðęĄâĶ®£Ì±°Ķ®£ſſſŷŦťęĄâÌ±°ęĄâĲĵĭſſſŷŦťęĄâĶ®£Ì±°Ķ®£ſſſĲĵĭęĄâĲĵĭęĄâĲĵĭſſſŷųŵŷŦťŷųŵſſſŲĭðęĄâĶ®£Ì±°Ķ®£ſſſŷŦťęĄâĲĵĭęĄâÌ±°ŷŦťĲĵĭŷŦťŷųŵĲĵĭſſſŷųŵęĄâĶ®£Ì±°Ķ®£ſſſĲĵĭęĄâĲĵĭęĄâÌ±°ŷŦťÌ±°ęĄâŷŦťŷųŵŷŦťÌ±°ęĄâſſſŷųŵſſſĲĵĭſſſŷŦťęĄâĶ®£ęĄâĶ®£Ì±°Ķ®£ſſſĲĵĭęĄâĲĵĭęĄâÌ±°ŷŦťŷųŵÌ±°ęĄâĲĵĭęĄâŷŦťÌ±°ęĄâŷŦťŷųŵęĄâÌ±°ĲĵĭŷųŵſſſŲĭðęĄâĶ®£ęĄâĶ®£Ì±°Ķ®£ſſſęĄâĲĵĭęĄâÌ±°ŷųŵŷŦťÌ±°ĲĵĭÌ±°ŷŦťŷųŵęĄâÌ±°ĲĵĭŷŦťĲĵĭŷųŵŷŦťſſſŲĭðęĄâĶ®£Ì±°Ķ®£ſſſęĄâĲĵĭęĄâÌ±°ęĄâŷųŵĲĵĭÌ±°ŷŦťęĄâĲĵĭŷŦťĲĵĭęĄâÌ±°ĲĵĭſſſŷŦťſſſŷŦťęĄâĶ®£Ì±°Ķ®£ſſſęĄâĲĵĭęĄâÌ±°ęĄâŷųŵĲĵĭÌ±°ŷŦťęĄâĲĵĭŷųŵŷŦťÌ±°ĲĵĭŷųŵŷŦťſſſŷŦťęĄâĶ®£Ì±°Ķ®£ſſſŷųŵęĄâÌ±°ęĄâŷųŵŷŦťÌ±°ĲĵĭŷŦťĲĵĭŷŦťŷųŵſſſŲĭðęĄâĶ®£Ì±°ſſſŷųŵęĄâÌ±°ęĄâŷųŵŷŦťĲĵĭŷŦťŷųŵſſſŷųŵŲĭðęĄâĶ®£Ì±°ĲĵĭſſſŷųŵęĄâÌ±°ĲĵĭŷųŵŷŦťŷųŵŷŦťŷųŵſſſŷŦťĶ®£Ì±°ĲĵĭſſſĲĵĭęĄâÌ±°ĲĵĭŷŦťĲĵĭŷŦťŷųŵŷŦťĲĵĭŷŦťęĄâĶ®£Ì±°ĲĵĭſſſĲĵĭęĄâÌ±°ęĄâŷŦťŷųŵŷŦťĲĵĭęĄâĲĵĭŷŦťŷųŵęĄâĶ®£Ì±°ŷŦťſſſĲĵĭęĄâĲĵĭęĄâÌ±°ęĄâĲĵĭęĄâĲĵĭęĄâĲĵĭŷŦťĶ®£Ì±°ĲĵĭęĄâĶ®£ſſſĲĵĭęĄâĲĵĭęĄâÌ±°ęĄâĲĵĭęĄâĲĵĭŷŦťĲĵĭęĄâĲĵĭŷŦťĶ®£Ì±°ęĄâÌ±°ſſſŷŦťęĄâĲĵĭęĄâÌ±°ęĄâĲĵĭęĄâĲĵĭęĄâĲĵĭŷŦťęĄâŷŦťŷųŵęĄâĶ®£Ì±°ęĄâŷųŵÌ±°Ķ®£ęĄâſſſŷŦťĲĵĭęĄâÌ±°ęĄâĲĵĭŷŦťęĄâŷŦťŷųŵęĄâĶ®£Ì±°ęĄâſſſŷŦťÌ±°Ķ®£Ì±°ęĄâſſſŷųŵĲĵĭęĄâÌ±°ęĄâĲĵĭŷŦťĲĵĭŷŦťŷųŵęĄâĶ®£Ì±°ęĄâſſſĲĵĭÌ±°Ķ®£Ì±°ęĄâſſſĲĵĭęĄâÌ±°ęĄâĲĵĭęĄâĲĵĭŷŦťĲĵĭŷŦťĶ®£Ì±°ĲĵĭſſſęĄâĶ®£Ì±°ęĄâĲĵĭſſſĲĵĭſſſŷŦťĲĵĭŷŦťĲĵĭęĄâÌ±°ęĄâĲĵĭęĄâĲĵĭęĄâĲĵĭŷŦťĲĵĭŷŦťĶ®£Ì±°ĲĵĭŷųŵŷŦťĲĵĭęĄâĶ®£ęĄâĲĵĭŷųŵĲĵĭſſſŷųŵĲĵĭŷŦťĲĵĭęĄâÌ±°ęĄâĲĵĭęĄâĲĵĭęĄâĲĵĭŷŦťĶ®£Ì±°ĲĵĭÌ±°Ķ®£ęĄâŷŦťęĄâſſſĲĵĭŷŦťĲĵĭęĄâÌ±°ęĄâĲĵĭęĄâĲĵĭęĄâĲĵĭŷŦťŷųŵŲĭðĶ®£Ì±°ŷŦťĲĵĭÌ±°Ķ®£ęĄâŷųŵęĄâſſſŷŦťĲĵĭŷŦťĲĵĭęĄâÌ±°ęĄâĲĵĭŷŦťĲĵĭŷŦťŷųŵŲĭðĶ®£Ì±°ŷŦťſſſÌ±°Ķ®£ęĄâĲĵĭŷŦťĲĵĭęĄâĲĵĭſſſĲĵĭŷŦťĲĵĭęĄâÌ±°ęĄâĲĵĭŷŦťĲĵĭŷŦťĶ®£Ì±°ŷųŵſſſŷŦťÌ±°Ķ®£ęĄâĲĵĭſſſĲĵĭęĄâŷŦťĲĵĭſſſĲĵĭŷŦťĲĵĭęĄâÌ±°ęĄâĲĵĭŷŦťĲĵĭŷųŵŲĭðĶ®£Ì±°ęĄâſſſĲĵĭĶ®£ęĄâĲĵĭęĄâŷŦťęĄâÌ±°ſſſĲĵĭŷŦťĲĵĭęĄâÌ±°ęĄâĲĵĭŷŦťĲĵĭŷŦťŲĭðĶ®£Ì±°ęĄâŷųŵŷŦťĲĵĭĶ®£Ì±°ęĄâŷŦťſſſęĄâĲĵĭŷŦťęĄâÌ±°ęĄâſſſŷŦťĲĵĭŷŦťĲĵĭęĄâÌ±°ęĄâĲĵĭŷŦťĲĵĭŷŦťŲĭðĶ®£Ì±°ęĄâÌ±°Ķ®£Ì±°ęĄâĲĵĭęĄâĲĵĭŷŦťÌ±°ſſſŷŦťĲĵĭŷŦťĲĵĭęĄâĲĵĭŷŦťĲĵĭŷųŵŲĭðĶ®£Ì±°ĲĵĭÌ±°Ķ®£ęĄâĲĵĭſſſĲĵĭęĄâĲĵĭÌ±°ęĄâÌ±°ſſſŷŦťĲĵĭŷŦťĲĵĭŲĭðĶ®£ęĄâĲĵĭŷŦťĲĵĭŷųŵĲĵĭĶ®£Ì±°ĲĵĭſſſęĄâÌ±°Ķ®£ęĄâĲĵĭęĄâĲĵĭŷŦťĲĵĭęĄâŷŦťĲĵĭÌ±°ĲĵĭſſſŷųŵŷŦťŲĭðĶ®£ęĄâĲĵĭŷŦťŷųŵſſſŷųŵŷŦťĲĵĭŷųŵÌ±°ĲĵĭſſſŷŦťÌ±°Ķ®£ęĄâŷŦťŷųŵęĄâŷŦťęĄâÌ±°ęĄâÌ±°ĲĵĭſſſŷŦťŷųŵſſſŷŦťĲĵĭŲĭðĶ®£ŷŦťŷųŵſſſŷųŵŷŦťſſſĲĵĭĶ®£ęĄâŷŦťĲĵĭęĄâĲĵĭŷŦťÌ±°ĲĵĭęĄâſſſŷŦťŷųŵſſſŷŦťſſſŷųŵſſſŷŦťęĄâĲĵĭŷŦťĶ®£ęĄâŷųŵĲĵĭęĄâĲĵĭÌ±°ęĄâĲĵĭęĄâ©ſſſŷųŵÌ±°Ķ®£ęĄâĲĵĭſſſŷŦťęĄâĲĵĭÌ±°ęĄâĲĵĭęĄâªſſſęĄâĶ®£ęĄâĲĵĭŷŦťĲĵĭęĄâŷŦťęĄâÌ±°ĲĵĭęĄâªſſſŲĭðęĄâŷŦťſſſĲĵĭÌ±°ĲĵĭŷŦťęĄâÌ±°ęĄâÌ±°ĲĵĭęĄâªſſſŷŦťęĄâĲĵĭęĄâĲĵĭŷŦťÌ±°ęĄâĲĵĭęĄâ«ſſſĲĵĭſſſŷŦťęĄâĲĵĭÌ±°ęĄâÌ±°ęĄâĲĵĭęĄâ«ſſſĲĵĭęĄâĲĵĭęĄâÌ±°ĲĵĭęĄâ«ſſſĲĵĭÌ±°ęĄâŷŦťęĄâÌ±°ęĄâÌ±°ĲĵĭęĄâ«ſſſŷŦťĲĵĭŷŦťÌ±°ĲĵĭęĄâ«ſſſŷųŵĲĵĭÌ±°ęĄâĲĵĭęĄâ¬ſſſÌ±°ĲĵĭęĄâ¬ſſſĲĵĭÌ±°ĲĵĭęĄâ¬ſſſŷųŵęĄâÌ±°ĲĵĭęĄâ"
RETURN
GetQueenData:
PDat$="ĂſſſŷųŵÁſſſŷŦť½ſſſŷųŵ¶ſſſŷųŵŷŦť±ſſſŷŦťĲĵĭŷŦťŷųŵ®ſſſŷŦťĲĵĭŷŦťŷųŵĲĵĭŷŦťſſſŷųŵŷŦťŷųŵ¤ſſſŷųŵĲĵĭŲĭðŷųŵſſſŷŦťĲĵĭŷųŵſſſĲĵĭęĄâſſſŷųŵŷŦť¤ſſſŷųŵŲĭðĶ®£ŲĭðĲĵĭŷŦťſſſĲĵĭęĄâĲĵĭſſſŷŦťŲĭð¥ſſſŲĭðĶ®£ŲĭðſſſĲĵĭęĄâĲĵĭſſſŷųŵŷŦť¥ſſſŲĭðĶ®£ŲĭðſſſĲĵĭſſſŷŦťŲĭðĲĵĭŷŦť¥ſſſŷŦťĶ®£ŲĭðſſſŲĭðĶ®£¦ſſſĶ®£ŲĭðſſſŷŦťŲĭðĶ®£ŷŦť¦ſſſŷŦťĶ®£ŷŦťſſſĲĵĭÌ±°Ķ®£Ųĭð¨ſſſŲĭðĶ®£ŷŦťſſſŲĭðĶ®£Ì±°Ķ®£µſſſŷųŵęĄâÌ±°Ķ®£Ì±°Ķ®£´ſſſŷŦťĶ®£Ì±°Ķ®£Ì±°Ķ®£³ſſſŲĭðĶ®£Ì±°Ķ®£Ì±°Ķ®£±ſſſŷųŵŲĭðĶ®£Ì±°Ķ®£Ì±°Ķ®£°ſſſŷŦťęĄâĶ®£Ì±°Ķ®£Ì±°Ķ®£¯ſſſĲĵĭĶ®£Ì±°Ķ®£Ì±°Ķ®£®ſſſŲĭðĶ®£Ì±°Ķ®£Ì±°¬ſſſŷųŵŲĭðĶ®£Ì±°Ķ®£«ſſſŷŦťęĄâĶ®£Ì±°Ķ®£ªſſſŷŦťĶ®£Ì±°Ķ®£ªſſſŲĭðĶ®£Ì±°ſſſŷŦťſſſŷųŵſſſŷŦťÌ±°Ķ®£ſſſŷųŵŷŦťŲĭðęĄâŲĭðŷŦťſſſŷųŵŷŦťſſſŷųŵĲĵĭęĄâÌ±°Ķ®£ŷųŵŷŦťĲĵĭęĄâĶ®£ęĄâŲĭðŷŦťŷųŵŷŦťŷųŵŷŦťĲĵĭŷŦťſſſŷųŵŷŦťęĄâÌ±°Ķ®£ŷŦťĶ®£ęĄâĲĵĭĶ®£ęĄâŲĭðŷŦťęĄâĶ®£ŲĭðŷųŵŷŦťŷųŵſſſŷŦťŷųŵŷŦťęĄâÌ±°Ķ®£ſſſŷŦťĶ®£ŲĭðſſſŷųŵĶ®£ęĄâŲĭðŷųŵŷŦťŲĭðŷŦťŷųŵĲĵĭŷųŵŷŦťŷųŵŷŦťęĄâŷŦťſſſŷųŵŷŦťŷųŵęĄâÌ±°Ķ®£ſſſęĄâĲĵĭĶ®£ęĄâŷŦťŲĭðŷŦťĲĵĭęĄâŷŦťŷųŵŷŦťſſſŷųŵŷŦťŷųŵęĄâÌ±°Ķ®£ſſſŲĭðĶ®£ŲĭðĶ®£ŲĭðŷŦťŷųŵŷŦťŲĭðŷŦťĲĵĭęĄâÌ±°ĲĵĭŷųŵŷŦťŷųŵŷŦťŷųŵÌ±°Ķ®£ſſſŲĭðęĄâſſſŷŦťĶ®£ŲĭðŷŦťŷųŵŲĭðŷŦťÌ±°ĲĵĭŷŦťŲĭðŷŦťÌ±°Ķ®£ſſſŲĭðęĄâŲĭðĶ®£ŷŦťŷųŵŲĭðſſſĲĵĭęĄâŷųŵŷŦťŲĭðŷŦťŷųŵęĄâÌ±°Ķ®£ſſſŷųŵŲĭðęĄâŷŦťĶ®£ŲĭðŷųŵŷŦťŲĭðŷųŵŷŦťęĄâŷŦťŲĭðŷŦťÌ±°Ķ®£ſſſŷųŵŲĭðęĄâŷŦťĶ®£ŲĭðŷųŵŷŦťŲĭðŷŦťŷųŵŷŦťŷųŵęĄâÌ±°Ķ®£ſſſŷųŵŲĭðęĄâĶ®£ęĄâŷŦťŲĭðĲĵĭŲĭðĲĵĭŷųŵŷŦťŷųŵĲĵĭÌ±°Ķ®£ſſſŷŦťŲĭðęĄâĶ®£Ųĭðŷųŵ¡ŷŦťŲĭðŷŦťŷųŵŷŦťÌ±°Ķ®£ſſſŷŦťŲĭðĶ®£ęĄâŷŦťŷųŵ ŷŦťŷųŵŲĭðŷųŵĲĵĭŲĭðŷŦťŷųŵŷŦťŷųŵęĄâĶ®£ſſſŷŦťŲĭðſſſŷŦťęĄâĶ®£ęĄâŷŦťŷųŵ ŷŦťŷųŵŲĭðĶ®£ŲĭðŷŦťŷųŵĲĵĭÌ±°Ķ®£ſſſŲĭðŷŦťŲĭðęĄâĶ®£ęĄâŲĭðŷŦťŷųŵ ŷŦťĶ®£ŷŦťŷųŵĲĵĭÌ±°Ķ®£ſſſŷŦťŲĭðſſſŷŦťŲĭðęĄâŲĭðŷŦťŲĭðŷŦťŲĭðĶ®£ŷŦťŷųŵŷŦťÌ±°Ķ®£ſſſŷŦťŲĭðŷŦťŲĭðęĄâŲĭðŷŦťŲĭðŷŦťŷųŵŷŦťŲĭðęĄâĲĵĭŷųŵŷŦťŲĭðĶ®£ſſſŷŦťŲĭðŷŦťſſſŲĭðęĄâŲĭðŷŦťŷųŵŷŦťęĄâŷųŵŷŦťŲĭðĶ®£ſſſŷŦťŲĭðŷŦťſſſŲĭðęĄâŲĭðŷŦťŷųŵŲĭðŷųŵŷŦťŲĭðĶ®£ęĄâŲĭðĲĵĭŷŦťſſſŷųŵŲĭðŷŦťŲĭðęĄâŲĭðŷŦťęĄâŷŦťŲĭðĶ®£ŲĭðĶ®£ŲĭðŷŦťſſſŷųŵŷŦťſſſŷųŵŲĭðŷŦťſſſŷŦťŲĭðęĄâŲĭðŷŦťŷųŵŲĭðĶ®£ŲĭðŷŦťſſſŷųŵſſſĲĵĭęĄâÌ±°ęĄâſſſŲĭðŷŦťſſſŲĭðŷŦťŲĭðęĄâŲĭðŷŦťŷųŵŲĭðĶ®£ŲĭðŷŦťŷųŵſſſŷųŵęĄâĲĵĭÌ±°ĲĵĭęĄâŷŦťſſſŷŦťŲĭðŷŦťſſſŷŦťŷųŵŷŦťŲĭðęĄâŲĭðŷŦťŲĭðĶ®£ŲĭðŷŦťſſſŷųŵĲĵĭÌ±°ęĄâÌ±°ęĄâŷŦťſſſŷŦťĲĵĭſſſŷųŵŲĭðęĄâŲĭðŷŦťŷųŵŲĭðŷŦťŲĭðęĄâŷŦťŷųŵŷŦťŲĭðŷŦťſſſŷųŵęĄâÌ±°ęĄâÌ±°ĲĵĭŷųŵſſſŷŦťŲĭðĶ®£ſſſŷŦťŲĭðŷŦťŲĭðŷŦťſſſŷŦťſſſŷŦťŲĭðŷŦťŲĭðŷŦťŷųŵſſſęĄâÌ±°ęĄâÌ±°ŷŦťſſſŷųŵŷŦťŲĭðĶ®£ſſſŷųŵŲĭðŷŦťŲĭðŷųŵŷŦťſſſŷŦťſſſŷųŵſſſŷŦťŷųŵŷŦťŲĭðŷŦťŷųŵſſſęĄâÌ±°ęĄâÌ±°ŷųŵſſſŷŦťŲĭðĶ®£ſſſŷŦťŲĭðŷŦťŲĭðŷŦťſſſŷŦťſſſŷųŵŲĭðŷųŵſſſęĄâÌ±°ęĄâÌ±°ŷųŵŲĭðĶ®£ſſſŷųŵ¥ŲĭðŷŦťſſſęĄâÌ±°ęĄâÌ±°ŷųŵŲĭðĶ®£ſſſŷŦť£ŲĭðſſſĲĵĭęĄâÌ±°ſſſŷųŵŲĭðĶ®£ſſſŷŦť¡ŲĭðĲĵĭęĄâÌ±°ŷŦťſſſŲĭðĶ®£ſſſŷŦť ŲĭðęĄâÌ±°ĲĵĭŷŦťŷųŵŲĭðĶ®£ſſſŷŦť ŲĭðĲĵĭſſſŷŦťŲĭðĶ®£ſſſŷŦťŲĭðſſſŲĭðĶ®£ſſſŷŦť³ŲĭðĶ®£ſſſŷųŵŷŦť¬ŲĭðĶ®£ſſſŷųŵŷŦť¦ŲĭðĶ®£ſſſŷųŵŷŦť¡ŲĭðĶ®£ŲĭðĶ®£ſſſŷųŵŷŦťŲĭðĶ®£ŲĭðĶ®£ŲĭðſſſŷųŵŷŦťŲĭðĶ®£ŲĭðſſſŷųŵŷŦť¦ŲĭðſſſŷųŵŷŦť Ųĭð£ſſſŷųŵŷŦťŲĭð«ſſſŲĭð«ſſſŷųŵŲĭðĶ®£­ſſſŷŦťŲĭðÂſſſ"
RETURN
GetKingData:
PDat$="«ſſſĲĵĭĶ®£Ì±°ªſſſŷųŵĶ®£Ì±°ªſſſŲĭðĶ®£Ì±°ªſſſĶ®£Ì±°©ſſſŷŦťĶ®£Ì±°©ſſſŷŦťĶ®£©ſſſŲĭðĶ®£©ſſſĶ®£¨ſſſŷŦťĶ®£¨ſſſŷŦťĶ®£¨ſſſŷŦťĶ®£¡ſſſŷųŵŷŦťſſſŲĭðęĄâĶ®£ſſſĲĵĭſſſŷŦťęĄâÌ±°ęĄâŷŦťſſſŲĭðęĄâĶ®£ſſſŷŦťęĄâĲĵĭŷŦťſſſŷŦťęĄâÌ±°ęĄâſſſęĄâĶ®£ſſſĲĵĭÌ±°ĲĵĭŷŦťſſſŷųŵĲĵĭÌ±°ĲĵĭſſſĲĵĭęĄâĶ®£ſſſŷųŵęĄâĲĵĭŷŦťĲĵĭŷŦťſſſŷŦťęĄâÌ±°ĲĵĭŷųŵĲĵĭęĄâĶ®£Ì±°Ķ®£ſſſĲĵĭęĄâĲĵĭŷŦťĲĵĭęĄâÌ±°ęĄâĲĵĭęĄâÌ±°ęĄâŷųŵĲĵĭęĄâĶ®£Ì±°Ķ®£ſſſŷųŵĲĵĭęĄâÌ±°ĲĵĭŷŦťĲĵĭÌ±°ęĄâÌ±°ęĄâŷųŵŷŦťĲĵĭęĄâĶ®£Ì±°Ķ®£ſſſĲĵĭÌ±°ęĄâĲĵĭŷŦťĲĵĭęĄâÌ±°ŷŦťĲĵĭęĄâĶ®£ſſſęĄâĲĵĭęĄâÌ±°ŷŦťĲĵĭęĄâĶ®£ŲĭðſſſŷŦťęĄâĲĵĭÌ±°ŷŦťĲĵĭęĄâĲĵĭŷŦťŷųŵſſſŷŦťęĄâſſſĲĵĭęĄâĲĵĭÌ±°ŷŦťĲĵĭęĄâĲĵĭŷŦťſſſŷŦťęĄâÌ±°ſſſŷŦťęĄâĲĵĭÌ±°ŷŦťĲĵĭęĄâĲĵĭŷŦťſſſŷŦťęĄâÌ±°ſſſĲĵĭęĄâĲĵĭÌ±°ŷŦťĲĵĭęĄâĲĵĭŷųŵſſſŷŦťęĄâÌ±°ſſſĲĵĭęĄâĲĵĭÌ±°ŷŦťĲĵĭſſſĲĵĭęĄâÌ±°Ķ®£ſſſĲĵĭęĄâĲĵĭÌ±°ŷŦťĲĵĭŷŦťęĄâÌ±°Ķ®£ſſſĲĵĭęĄâĲĵĭÌ±°ŷŦťĲĵĭęĄâÌ±°Ķ®£ſſſĲĵĭęĄâĲĵĭęĄâĲĵĭÌ±°ŷŦťĲĵĭęĄâÌ±°Ķ®£ęĄâĶ®£ſſſĲĵĭęĄâĲĵĭÌ±°ęĄâŷųŵŷŦťĲĵĭÌ±°ęĄâŲĭðęĄâĶ®£ſſſŷųŵĲĵĭęĄâĲĵĭęĄâÌ±°ęĄâŷųŵŷŦťĲĵĭęĄâŷŦťſſſŲĭðęĄâĶ®£ſſſŷŦťĲĵĭŷųŵŷŦťŷųŵŷŦťĲĵĭęĄâÌ±°ĲĵĭŷŦťĲĵĭÌ±°ŷŦťſſſŲĭðęĄâĶ®£ſſſĲĵĭŷŦťŷųŵŷŦťęĄâÌ±°ŷŦťĲĵĭÌ±°ęĄâſſſŷŦťęĄâĶ®£ſſſŷųŵĲĵĭŷŦťĲĵĭęĄâÌ±°ĲĵĭŷŦťĲĵĭęĄâĲĵĭęĄâÌ±°ĲĵĭſſſŷŦťęĄâĶ®£ſſſĲĵĭŷŦťŷųŵĲĵĭęĄâŷŦťſſſŷųŵŷŦťĲĵĭęĄâĲĵĭęĄâŷŦťſſſŲĭðęĄâĶ®£ſſſŷųŵĲĵĭŷŦťęĄâŷŦťŷųŵŷŦťĲĵĭęĄâÌ±°ęĄâſſſŷŦťęĄâĲĵĭęĄâÌ±°ęĄâÌ±°ęĄâŷųŵęĄâĶ®£ſſſĲĵĭŷŦťŷųŵęĄâŷŦťŷųŵĲĵĭÌ±°ĲĵĭſſſŷŦťęĄâĲĵĭÌ±°ęĄâÌ±°ęĄâſſſŷŦťęĄâſſſŷŦťĲĵĭŷŦťŷųŵÌ±°ęĄâŷųŵŷŦťĲĵĭÌ±°ĲĵĭſſſĲĵĭęĄâĲĵĭęĄâÌ±°ęĄâÌ±°ĲĵĭſſſŲĭðęĄâſſſĲĵĭŷŦťŷųŵÌ±°ęĄâĲĵĭŷŦťÌ±°ŷŦťſſſĲĵĭęĄâÌ±°ęĄâÌ±°ŷŦťſſſŲĭðęĄâſſſŷųŵĲĵĭęĄâĲĵĭŷŦťŷųŵęĄâÌ±°ĲĵĭŷŦťŷųŵĲĵĭÌ±°ęĄâĲĵĭŷŦťÌ±°ŷųŵſſſĲĵĭęĄâÌ±°ęĄâÌ±°ŷųŵęĄâſſſŷųŵĲĵĭŷŦťĲĵĭęĄâĲĵĭŷŦťŷųŵĲĵĭÌ±°ęĄâĲĵĭŷųŵŷŦťŷųŵĲĵĭÌ±°ĲĵĭſſſŷųŵſſſęĄâÌ±°ęĄâſſſĲĵĭęĄâÌ±°ęĄâÌ±°ęĄâſſſŷŦťęĄâſſſŷŦťĲĵĭęĄâĲĵĭŷŦťĲĵĭÌ±°ĲĵĭŷŦťŷųŵĲĵĭÌ±°ĲĵĭſſſĲĵĭŷųŵſſſŷŦťęĄâÌ±°ęĄâÌ±°ĲĵĭſſſŲĭðęĄâſſſŷŦťĲĵĭęĄâÌ±°ęĄâĲĵĭŷŦťŷųŵŷŦťŷųŵŷŦťĲĵĭŷųŵſſſŷŦťęĄâÌ±°ęĄâÌ±°ŷŦťſſſŲĭðęĄâſſſŷųŵŷŦťĲĵĭęĄâĲĵĭŷŦťŷųŵſſſŷųŵŷŦťŷųŵſſſŷųŵęĄâÌ±°ęĄâÌ±°ęĄâÌ±°ŷųŵęĄâſſſŷųŵ¦ſſſĲĵĭÌ±°ęĄâÌ±°ęĄâÌ±°ęĄâÌ±°ęĄâſſſŷŦťęĄâ©ſſſĲĵĭęĄâÌ±°ęĄâÌ±°ĲĵĭſſſŲĭð©ſſſŷųŵÌ±°ęĄâÌ±°ęĄâÌ±°ęĄâŷŦť«ſſſĲĵĭÌ±°ęĄâÌ±°ęĄâÌ±°ęĄâÌ±°ęĄâÌ±°ęĄâÌ±°ŷųŵ«ſſſĲĵĭÌ±°ęĄâÌ±°ęĄâÌ±°ęĄâÌ±°ĲĵĭŷŦť¬ſſſĲĵĭÌ±°ęĄâÌ±°ęĄâÌ±°ĲĵĭŷųŵŷŦť­ſſſĲĵĭęĄâÌ±°ęĄâÌ±°ęĄâÌ±°ęĄâŷŦťŷųŵŷŦť®ſſſŷŦťęĄâÌ±°ęĄâÌ±°ęĄâÌ±°ęĄâŷŦťĲĵĭŷŦťſſſŷųŵŷŦťŷųŵŷŦťĲĵĭŷŦťĲĵĭŷŦťĲĵĭŷŦťĲĵĭŷŦťĲĵĭŷŦťĲĵĭęĄâÌ±°ĲĵĭŷŦťſſſŷŦťĲĵĭęĄâĲĵĭĶ®£Ì±°Ķ®£Ì±°Ķ®£Ì±°Ķ®£Ì±°Ķ®£Ì±°Ķ®£Ì±°Ķ®£Ì±°Ķ®£Ì±°Ķ®£ĲĵĭŷųŵŷŦťſſſĲĵĭęĄâÌ±°ęĄâĲĵĭęĄâĶ®£Ì±°Ķ®£Ì±°Ķ®£Ì±°Ķ®£Ì±°Ķ®£Ì±°Ķ®£Ì±°Ķ®£Ì±°Ķ®£Ì±°ęĄâŷŦťŷųŵŷŦťĲĵĭŷŦťſſſŷųŵĲĵĭęĄâĲĵĭŲĭðĲĵĭŲĭðĲĵĭŲĭðĲĵĭŲĭðĲĵĭŲĭðĲĵĭŲĭðĲĵĭŲĭðęĄâÌ±°Ķ®£Ì±°ęĄâŷųŵŷŦťĲĵĭęĄâŷŦťŷųŵŷŦťſſſŷųŵĲĵĭęĄâĲĵĭŷŦťŷųŵĲĵĭŷųŵſſſĲĵĭÌ±°ęĄâĲĵĭęĄâĲĵĭŷŦťŷųŵĲĵĭſſſĲĵĭęĄâĲĵĭŷŦťſſſĲĵĭŷŦťſſſŷųŵſſſĲĵĭÌ±°ęĄâÌ±°ĲĵĭŷųŵŷŦťęĄâĲĵĭŷųŵſſſŷŦťĲĵĭŷųŵſſſŷŦťĲĵĭŷŦťŷųŵŷŦťſſſŷŦťęĄâÌ±°ęĄâĲĵĭŷųŵŷŦťęĄâŷųŵŷŦťſſſŷŦťĲĵĭŷųŵſſſŷųŵŷŦťĲĵĭŷŦťſſſĲĵĭÌ±°ęĄâÌ±°ęĄâſſſŷŦťęĄâſſſŷŦťŷųŵſſſŷųŵ¡ſſſŷŦťÌ±°ęĄâŷŦťĲĵĭęĄâ»ſſſŷŦťęĄâÌ±°ęĄâĲĵĭ½ſſſĲĵĭÌ±°¾ſſſŷųŵęĄâÌ±°ÀſſſĲĵĭÌ±°ÁſſſĲĵĭʐſſſ"
RETURN
ONTIMER:
	LN=LN+1
	IF LN<18 THEN
		GR.SHOW Intro[LN]
		GR.RENDER
	ENDIF
	t=RND()*128+128
	GR.MODIFY SP,"alpha",t
	GR.MODIFY HE,"alpha",t
	GR.MODIFY CL,"alpha",t
	GR.MODIFY DI,"alpha",t
	GR.RENDER
TIMER.RESUME
