!!
--------------------------------------
What's My Song - v1
--------------------------------------
*** v21 but with smaller arrays ***
*** alternative to 30a ***
--------------------------------------
To Do:
--------------------------------------
check if max folders/files read DONE
mid-game phone call checks?     DONE
opts timeout reset with press   DONE
bitmap errs when menuscrn shown DONE
game over message?              DONE
Initial search time to main menuDONE
Read folder contents into array DONE
Select song                     DONE
Select random position          DONE
Play song                       DONE
Check answer                    DONE
Lives                           DONE
More backgrounds                DONE
Score                           DONE
Skill progression (see below)   DONE
Offer options
      filetypes mp3 wav etc
      min file size             DONE
      display filename/mp3tag   DONE
If audio is off, show alert     DONE
save hiscores                   DONE
     record database size?
shift outro back 2 secs         DONE
     and end 2 secs from end    DONE
credits page                    DONE
records page                    DONE
     1 streak in a row
     2 number of 900+
     3 most pts in one guess
     4 number of games
More efficient memory use?      DONE
Don't pre-read full mp3 header  DONE
Loss of life resets multiplier  DONE
--------------------------------------
Embellishments:
--------------------------------------
glitterball on top/transparent when>7x?
Better search for music
Allow play offsets for songs
--------------------------------------
skill progression:
--------------------------------------
Start with 3 answers and 8 seconds DONE
answer 2 songs for an extra answer DONE
until when have 6 answers then
reduce by 0.5s until down to 4s    DONE

At all times if guess in 2s:
multiplier 1.5x 2x 2.5x etc to 5x  DONE
if answer after 3s then slide      DONE
down mulitplier.
--------------------------------------
!!

GOSUB init_functions
GOSUB init_setup1
GOSUB init_files
GOSUB init_setup2
GOSUB showlogo

IF numofsongs<10 THEN GOSUB firstrun

!Game Loop ============================
DO
 background$=INT$((RND()*5)+2)
 GOSUB mainmenu
 IF gametype=4 THEN
  GOSUB options
 ELSEIF gametype=5 THEN
  GOSUB credits
 ELSEIF gametype=6 THEN
  GOSUB showhi
 ELSEIF gametype=7 THEN
  GOSUB showrecs
 ELSEIF gametype>0 THEN
  GOSUB gamesetup
  GR.BITMAP.LOAD backgroundf,filepath$+background$+".jpg"
  records[3]=records[3]+1
  backbutton=1         %flag for menu button check
  DO
   GOSUB songselect
   GOSUB updatescreen
   GOSUB playsong
  UNTIL lives<=0
  backbutton=0

  IF quitnow=0 THEN
   GOSUB saverecs
   PAUSE 1000
   GOSUB checkhi
   GOSUB showhi
  ENDIF
  IF savenew=1 THEN GOSUB write_songs %save songlist
  glitterball=0
 ENDIF
 GR.BITMAP.DELETE backgroundf
 quitnow=0
UNTIL quitgame=1

GOSUB showlogo

EXIT


!--------------------------------------
!Whole Game Setups - variables
!--------------------------------------
init_setup1:

!set up screen and scale
GR.OPEN 255,0,0,0,0
!PAUSE 300

GR.SCREEN real_h,real_w
!set the device independent sizes
di_h = 1280
di_w = 720

!calculate the scale factors
scale_w = real_w/di_w
scale_h = real_h/di_h
GR.SCALE scale_w,scale_h     %Set the scale
GR.ORIENTATION 1             %portrait
GR.TEXT.ALIGN 1
GR.FRONT 1                   %gfx display to front

SOUNDPOOL.OPEN 6

DIM allfolderlist$[3000]     %total folders
DIM allsonglist$[2500,2]     %total songs: path & name
DIM songstoshow[6]           %songs to select from
DIM screenlist1[6],screenlist2[6]
DIM answerlist[6], menufx[6]
DIM menutext1$[6],menutext2$[6],audlist[6]
DIM glitterf[13]
DIM settings[5]
DIM records[5]
DIM hiscore[7,3],hiname$[7,3],hisex[7,3],hiletter[28]
DIM backmenu$[2],backbar[2],backtext[2]

RETURN


!--------------------------------------
!Whole Game Setups - files
!--------------------------------------
init_setup2:

SOUNDPOOL.LOAD correct,filepath$+"correct.mp3"
SOUNDPOOL.LOAD wrong,filepath$+"wrong.mp3"
SOUNDPOOL.LOAD whoosh,filepath$+"whoosh.mp3"

FONT.LOAD timefont,filepath$+"repet___.ttf"
FONT.LOAD titlefont,filepath$+"dist_mus.ttf"
FONT.LOAD picfont,filepath$+"musicele.ttf"
FONT.LOAD textfont,filepath$+"atrian1.ttf"

GR.BITMAP.LOAD touchmap,filepath$+"touchmap.png"
GR.BITMAP.LOAD stave1,filepath$+"stave_white.png"
GR.BITMAP.LOAD stave2,filepath$+"stave_green.png"
GR.BITMAP.LOAD stave3,filepath$+"stave_red.png"
GR.BITMAP.LOAD stave4,filepath$+"stave_cyan.png"
GR.BITMAP.LOAD stave5,filepath$+"stave_purple.png"
GR.BITMAP.LOAD stave6,filepath$+"stave_yellow.png"
GR.BITMAP.LOAD stave7,filepath$+"stave_orange.png"
GR.BITMAP.LOAD cog,filepath$+"cog1.png"
GR.BITMAP.LOAD rec,filepath$+"record.png"

FOR z1=1 TO 13
 a$="gball"+INT$(z1)+".png"
 GR.BITMAP.LOAD glitterf[z1],filepath$+a$
NEXT z1

RETURN


!--------------------------------------
!Main Menu
!--------------------------------------
mainmenu:

menupick=0
slidespeed=50
score=0
rturnshow=1
GR.CLS
GR.BITMAP.LOAD backgroundf,filepath$+"1.jpg"
GR.BITMAP.DRAW background,backgroundf,0,0
GR.COLOR 255,255,255,255,2
GR.TEXT.ALIGN 1
GR.TEXT.SETFONT timefont
GR.TEXT.SIZE 130
GR.SET.STROKE 9
GR.COLOR 255,0,0,0,2
GR.TEXT.DRAW cred,500,1195,"?"
GR.TEXT.SETFONT titlefont
GOSUB displaymenutop
GR.BITMAP.DRAW cog1,cog,600,1100
GR.BITMAP.DRAW rec1,rec,360,1100
GR.COLOR 140,255,255,0,2
GR.TEXT.SIZE 52
GR.SET.STROKE 1
numopts=5
offset=26
FOR z1=1 TO 6
 menutext1$[z1]=""
 menutext2$[z1]=""
NEXT z1
menutext1$[1]="intro!"
menutext1$[3]="outro!"
menutext1$[5]="rando!"
GOSUB displaymenu
z9=CLOCK()
DO
 GOSUB getmenu
 IF touched & touchy>=1100 & touchy<=1200
  IF touchx>600 & touchx<=700 THEN menupick=99
  IF touchx>=500 & touchx<=600 THEN menupick=98
  IF touchx>=360 & touchx<=470 THEN menupick=97
 ENDIF
 IF CLOCK()-z9>4000 THEN menupick=96
UNTIL menupick=1 | menupick=3 | menupick=5 | menupick>=96

IF menupick=1 THEN %Intro Game
 gametype=1
 GR.MODIFY screenlist1[1],"text","INTRO!"
ELSEIF menupick=3 THEN %Outro Game
 gametype=2
 GR.MODIFY screenlist1[3],"text","OUTRO!"
ELSEIF menupick=5 THEN %Rando Game
 gametype=3
 GR.MODIFY screenlist1[5],"text","RANDO!"
ELSEIF menupick=99 THEN %Options Cog
 gametype=4
ELSEIF menupick=98 THEN %Question Mark
 gametype=5
ELSEIF menupick=97 THEN %records
 gametype=7
ELSEIF menupick=96 THEN %timeout
 gametype=6
ENDIF

IF !BACKGROUND() THEN GR.render
GR.BITMAP.DELETE backgroundf

RETURN


!--------------------------------------
!First Run (or no music found)
!--------------------------------------
firstrun:

menupick=0
slidespeed=50
GR.CLS
GR.BITMAP.LOAD backgroundf,filepath$+"1.jpg"
GR.COLOR 255,255,255,255,2
GR.BITMAP.DRAW background,backgroundf,0,0
offset=10
GOSUB displaymenutop
GR.BITMAP.DRAW cog1,cog,600,1100
GR.COLOR 140,255,255,0,2
GR.SET.STROKE 2
GR.TEXT.SIZE 40
GR.TEXT.SETFONT textfont
numopts=4
FOR z1=1 TO 6
 menutext1$[z1]=""
 menutext2$[z1]=""
NEXT z1
menutext1$[3]="  Song database empty: touch for options"
GOSUB displaymenu
DO
 GOSUB getmenu
 IF touched & touchx>=600 & touchx<=700 & touchy>=1100 & touchy<=1200 THEN menupick=99
UNTIL menupick>0
GOSUB options

!if !background() then gr.render
GR.BITMAP.DELETE backgroundf

RETURN


!--------------------------------------
!Game Setup
!--------------------------------------
gamesetup:

lives$="cdg"
lives=3
score=0
multiplier=1
songstoshow=3
streak=0                         % when reaches streaktarget, level up
streaktarget=3                   % used for level up
songstreak=0                     % used for menu slide speed
cliplength=8000                  % in milliseconds
correctstreak=0                  % correct guesses in a row
level=1
slidespeed=34                    % speed menu bars apppear
newrecord=0
gbcount=1                        % glitterball spin anim counter
quitnow=0                        % backmenu quit flag

RETURN


!--------------------------------------
!Read Folder Contents into array
!--------------------------------------
init_songs:

GR.COLOR 255,255,255,255,2
GR.TEXT.ALIGN 2
GR.TEXT.SIZE 30
GR.TEXT.SETFONT textfont
GR.TEXT.DRAW null,505,545,"Songs:"
GR.TEXT.SETFONT timefont
GR.TEXT.SIZE 40
GR.TEXT.DRAW num,505,585,"0"

FILE.EXISTS z1,SDfilepath$+"songlist.txt"
IF z1=0 THEN
 !doesn't exist - no songs to load - scan needed
 GR.MODIFY num,"text","none"
ELSE
 TEXT.OPEN r, infile, SDfilepath$+"songlist.txt"
 z4=0
 DO
  TEXT.READLN infile,a$
  TEXT.READLN infile,b$
  IF a$<>"EOF" & a$>""
   FILE.EXISTS z1,rootfolder$+a$+b$
   IF z1<>0 THEN
    allsonglist$[z4+1,1]=a$
    allsonglist$[z4+1,2]=b$
    z4=z4+1
   ENDIF
  ENDIF
 UNTIL a$="EOF" | b$="EOF"
ENDIF

IF z4>0 THEN GR.MODIFY num,"text",INT$(z4)
IF !BACKGROUND() THEN GR.RENDER
numofsongs=z4
PAUSE 1500

RETURN


!--------------------------------------
!Update screen
!--------------------------------------
updatescreen:

GR.CLS
GR.COLOR 255,255,255,255,2
GR.BITMAP.DRAW background,backgroundf,0,0
GR.TEXT.ALIGN 1
numopts=songstoshow

FOR z1=1 TO numopts
 f$=rootfolder$+allsonglist$[songstoshow[z1],1]+allsonglist$[songstoshow[z1],2]
 a$=getheader$(f$)
 IF settings[1]=1 | LEN(a$)<8 THEN % filename
  a$=allsonglist$[songstoshow[z1],2]

  % mp3 tag
 ENDIF
 b$=RIGHT$(UPPER$(a$),4)
 IF b$=".MP3" | b$=".WAV" THEN a$=LEFT$(a$,LEN(a$)-4)
 a1$=a$
 a2$=""
 !check for long text and split
 IF LEN(a$)>60 THEN
  FOR z2=50 TO 65
   b$=MID$(a$,z2,1)
   IF b$="-" | b$=" " THEN %split at sensible place
    a1$=LEFT$(a$,z2)
    a2$=RIGHT$(a$,LEN(a$)-z2+1)
   ENDIF
   IF b$="-" | b$=" " THEN F_N.BREAK
  NEXT z2
 ENDIF
 menutext1$[z1]=a1$
 menutext2$[z1]=a2$
NEXT z1

GR.COLOR 255,195,195,195,2
GR.TEXT.SIZE 35
GR.SET.STROKE 3
GR.TEXT.SETFONT titlefont

GR.LINE glitterline,628,0,628,450-(50*multiplier)
GR.BITMAP.DRAW glitterball,glitterf[1],550,450-(50*multiplier)

GR.COLOR 255,255,255,255,2
GR.TEXT.SIZE 55
GR.TEXT.SETFONT timefont
ll2$=INT$(multiplier)+"x"
GR.TEXT.DRAW multishow,606,550-(50*multiplier), ll2$ %temp

GR.TEXT.SETFONT titlefont
GR.ROTATE.START -10,360,640
GR.TEXT.ALIGN 2
GR.TEXT.SIZE 40
GR.TEXT.DRAW scoretext1,450,70,"SCORE"
GR.TEXT.SIZE 110
GR.SET.STROKE 3
GR.TEXT.DRAW scoretext2,450,200,INT$(score)
GR.ROTATE.END

GR.COLOR 255,255,255,255,2
GR.SET.STROKE 1
GR.TEXT.ALIGN 1
GR.TEXT.SIZE 60
GR.TEXT.SETFONT picfont
GR.TEXT.DRAW livestext,555,1220,LEFT$(lives$,lives)

GR.SET.STROKE 2
GR.TEXT.SIZE 30
GR.TEXT.SETFONT textfont
offset=-15
rturnshow=2
GOSUB displaymenu

RETURN


!--------------------------------------
!Display menu tiles (WMS)
!--------------------------------------
displaymenutop:

GR.TEXT.ALIGN 1
GR.TEXT.SETFONT titlefont
GR.COLOR 255,255,255,255,2
GR.TEXT.SIZE 100
GR.SET.STROKE 0
GR.TEXT.DRAW title,10,150,"whats"
GR.TEXT.DRAW title,10,280,"my"
GR.TEXT.DRAW title,10,410,"song?"

RETURN


!--------------------------------------
!Display credits
!--------------------------------------
credits:

GR.CLS
GR.BITMAP.LOAD backgroundf,filepath$+"6.jpg"
GR.BITMAP.DRAW background,backgroundf,0,0
GOSUB displaymenutop
GR.COLOR 255,255,255,255,2
GR.TEXT.SIZE 45 % 57
GR.ROTATE.START -10,360,640
GR.TEXT.DRAW title2,20,530,"by mister  retro"
GR.ROTATE.END

slidespeed=50
GR.COLOR 140,255,255,0,2
GR.TEXT.SIZE 35
GR.SET.STROKE 1
numopts=5
offset=-5 %26
GR.TEXT.SETFONT textfont
FOR z1=1 TO 6
 menutext1$[z1]=""
 menutext2$[z1]=""
NEXT z1
menutext1$[1]="BETA TESTERS"
menutext2$[1]="Crosswacks - Identiti - Jackie41 - Spiritswind"
menutext1$[3]="GRAPHICS"
menutext2$[3]="becuo.com rgbstock.com superbwallpapers.com"
menutext1$[5]="FONTS"
menutext2$[5]="anke-art.de fontframe.com/tepidmonkey"
GOSUB displaymenu

z9=CLOCK()
DO
 GR.TOUCH touched,touchx,touchy
UNTIL touched | CLOCK()-z9>8000

GR.BITMAP.DELETE backgroundf

RETURN


!--------------------------------------
!Display menu
!--------------------------------------
displaymenu:

GR.ROTATE.START -10,360,640

!initial off screen plots
GR.COLOR 255,0,0,0,2
GR.BITMAP.DRAW progbar,stave1,0-(4*17)-750,500+(4*100)
GR.HIDE progbar

FOR z1=1 TO numopts
 IF menutext1$[z1]="" THEN
  GR.COLOR 0,0,0,0,2      %black option
  z9=1
 ELSE
  GR.COLOR 195,0,0,0,2
  z9=FLOOR(RND()*7)+1
 ENDIF
 IF z9=1 THEN
  GR.BITMAP.DRAW answerlist[z1],stave1,0-(z1*17)-750,500+(z1*100)
 ELSEIF z9=2 THEN
  GR.BITMAP.DRAW answerlist[z1],stave2,0-(z1*17)-750,500+(z1*100)
 ELSEIF z9=3 THEN
  GR.BITMAP.DRAW answerlist[z1],stave3,0-(z1*17)-750,500+(z1*100)
 ELSEIF z9=4 THEN
  GR.BITMAP.DRAW answerlist[z1],stave4,0-(z1*17)-750,500+(z1*100)
 ELSEIF z9=5 THEN
  GR.BITMAP.DRAW answerlist[z1],stave5,0-(z1*17)-750,500+(z1*100)
 ELSEIF z9=6 THEN
  GR.BITMAP.DRAW answerlist[z1],stave6,0-(z1*17)-750,500+(z1*100)
 ELSEIF z9=7 THEN
  GR.BITMAP.DRAW answerlist[z1],stave7,0-(z1*17)-750,500+(z1*100)
 ENDIF

 GR.COLOR 255,0,0,0,2
 GR.SET.STROKE 2
 GR.TEXT.ALIGN 1
 GR.TEXT.DRAW screenlist1[z1],20-(z1*17)-750,550+(z1*100)+offset,menutext1$[z1]
 GR.TEXT.DRAW screenlist2[z1],20-(z1*17)-750,550+(z1*100)+offset+40,menutext2$[z1]
NEXT z1
GR.ROTATE.END

! hide return on options if no music
IF rturnshow=0 THEN
 GR.HIDE answerlist[6]
 GR.HIDE screenlist1[6]
 GR.HIDE screenlist2[6]
ENDIF
!now slide across screen
FOR z1=1 TO numopts
 IF menutext1$[z1]>"" THEN
  IF z1<>6 | z1=6 & rturnshow<>0 THEN
   IF !BACKGROUND() THEN SOUNDPOOL.PLAY menufx[z1],whoosh,0.99,0.99,1,0,1
  ENDIF
  FOR z2=0 TO 720 STEP slidespeed
   GR.MODIFY answerlist[z1],"x",0-(z1*17)-720+z2
   GR.MODIFY screenlist1[z1],"x",20-(z1*17)-720+z2
   GR.MODIFY screenlist2[z1],"x",20-(z1*17)-720+z2
   IF glitterball>0 THEN GOSUB glitterball
   IF !BACKGROUND() THEN GR.render
  NEXT z2
 ENDIF
 !last update to show perfect alignment
 GR.MODIFY answerlist[z1],"x",0-(z1*17)
 GR.MODIFY screenlist1[z1],"x",20-(z1*17)
 GR.MODIFY screenlist2[z1],"x",20-(z1*17)
NEXT z1
GR.RENDER %will stop game if in background

RETURN


!--------------------------------------
!Get Selection
!--------------------------------------
getmenu:

menupick=-1
GR.TOUCH touched,touchx,touchy
touchx=touchx/scale_w
touchy=touchy/scale_h
GR.GET.BMPIXEL touchmap, touchx, touchy, z4, z1, z2, z3

IF touched THEN
 IF z1=0 & z2=0 & z3=0 THEN menupick=0
 IF z1=255 & z2=0 & z3=0 THEN menupick=1
 IF z1=255 & z2=255 & z3=0 THEN menupick=2
 IF z1=0 & z2=255 & z3=0 THEN menupick=3
 IF z1=0 & z2=255 & z3=255 THEN menupick=4
 IF z1=0 & z2=0 & z3=255 THEN menupick=5
 IF z1=255 & z2=0 & z3=255 THEN menupick=6
 IF z1=255 & z2=255 & z3=255 THEN menupick=7
ENDIF

RETURN


!--------------------------------------
!Select Songs for list
!--------------------------------------
songselect:

FOR z1=1 TO songstoshow
 DO
  DO
   exist=0
   z2=FLOOR(RND()*numofsongs)+1
   IF z1>1 THEN
    FOR z3=1 TO z1-1
     IF songstoshow[z3]=z2 THEN exist=1
     IF LEFT$(allsonglist$[songstoshow[z3],2],6)=LEFT$(allsonglist$[z2,2],6) THEN exist=1
    NEXT z3
   ENDIF
  UNTIL exist=0  
  a$=rootfolder$+allsonglist$[z2,1]+allsonglist$[z2,2]
  IF audlist[z1]>0 THEN AUDIO.RELEASE audlist[z1]
  AUDIO.STOP
  
  AUDIO.LOAD audlist[z1],a$
  if audlist[z1]=0 then
   if z2=numofsongs then z2=z2-1                  %song IS last in list so set as last but one
   allsonglist$[z2,1]=allsonglist$[numofsongs,1]  %swap bad file with last in list
   allsonglist$[z2,2]=allsonglist$[numofsongs,2]
   numofsongs=numofsongs-1                        %reduce number of list count by one
   savenew=1                                      %set flag indicating file list needs to be resaved
  ENDIF
 UNTIL audlist[z1]>0
 songstoshow[z1]=z2
 IF glitterball>0 THEN
  GOSUB glitterball
  IF !BACKGROUND() THEN GR.RENDER
 ENDIF
NEXT z1
playsong=FLOOR(RND()*songstoshow)+1

RETURN


!--------------------------------------
!Play Song
!--------------------------------------
playsong:

!gbcount=1
songstreak=songstreak+1
AUDIO.STOP
AUDIO.PLAY audlist[playsong]
AUDIO.LENGTH audiolen,audlist[playsong]
IF gametype=1 THEN
 ! do nothing AUDIO.POSITION.SEEK audiolen-cliplength
ELSEIF gametype=2 THEN
 AUDIO.POSITION.SEEK audiolen-2000-cliplength
ELSEIF gametype=3 THEN
 AUDIO.POSITION.SEEK FLOOR(RND()*audiolen-cliplength)
ENDIF

GR.TEXT.SIZE 130
GR.SET.STROKE 1
GR.TEXT.ALIGN 2
GR.COLOR 255,255,255,255,2
GR.TEXT.SETFONT timefont
GR.TEXT.DRAW songtime,360,490,""
flag=0
z9c=CLOCK()
DO
 !AUDIO.POSITION.CURRENT audiopos
 roundscore=FLOOR(1000+(z9c-CLOCK())/(cliplength/1000))
 GR.MODIFY songtime,"text",INT$(roundscore)
 GOSUB glitterball
 ll2$=INT$(multiplier)+"x"
 GR.MODIFY multishow,"text", ll2$ %temp
 GR.RENDER
 IF BACKGROUND() THEN GOSUB backmenudo
 slidespeed=39+(songstreak*2)
 IF slidespeed>144 THEN slidespeed=144
 GOSUB getmenu
 IF CLOCK()-z9c>cliplength THEN menupick=10
UNTIL quitnow=1 | menupick=10 | menupick>0 & menupick<=songstoshow
AUDIO.STOP
IF menupick=10 THEN
 !timeout
 GR.MODIFY songtime,"text", "0"
 IF !BACKGROUND() THEN GR.render
 GOSUB loselife
ELSEIF quitnow=0
 ! now check & animate selection
 IF menupick=playsong THEN  %correct guess
  GOSUB correctguess
 ELSE      %if menupick>0 then
  ! incorect guess
  GOSUB loselife
 ENDIF
ENDIF

RETURN


!--------------------------------------
!Actions for guessing correctly
!--------------------------------------
correctguess:

SOUNDPOOL.PLAY presssound,correct,0.99,0.99,1,0,1

GR.TEXT.SETFONT titlefont
GR.COLOR 205,40,205,40,2
GR.SET.STROKE 4
GR.TEXT.SIZE 60
GR.ROTATE.START -10,360,640
GR.TEXT.DRAW wrong1,360-(menupick*17),580+(menupick*100),"correct"
GR.ROTATE.END

FOR z2=195 TO 0 STEP-19.5
 GR.MODIFY answerlist[menupick],"alpha",z2
 GR.MODIFY screenlist1[menupick],"alpha",z2
 GR.MODIFY screenlist2[menupick],"alpha",z2
 GR.MODIFY wrong1,"alpha",195-z2
 GOSUB glitterball
 IF !BACKGROUND() THEN GR.render
NEXT z2

correctstreak=correctstreak+1
IF roundscore>=900 THEN
 records[2]=records[2]+1
 newrecord=1
ENDIF
z1=(roundscore*ROUND(multiplier))
score=score+z1

IF z1>records[4] THEN
 records[4]=z1
 newrecord=1
ENDIF

!check if multiplier increases
IF CLOCK()-z9c<2000 & multiplier<10 THEN
 multiplier=multiplier+1
ELSE
 multiplier=multiplier-0.5
 IF multiplier<1 THEN multiplier=1
ENDIF
! check level up etc here
streak=streak+1
IF streak>=streaktarget THEN
 IF songstoshow<6 THEN
  songstoshow=songstoshow+1
  streaktarget=3
 ELSEIF cliplength>3000 THEN
  cliplength=cliplength-500
  streaktarget=1
 ENDIF
 streak=0
ENDIF

RETURN


!--------------------------------------
!Actions for losing a life
!--------------------------------------
loselife:

SOUNDPOOL.PLAY presssound,wrong,0.99,0.99,1,0,1

GR.TEXT.SETFONT titlefont
GR.COLOR 205,205,40,40,2
GR.SET.STROKE 4
GR.TEXT.SIZE 60
GR.ROTATE.START -10,360,640
a$="wrong"
IF lives=1 THEN a$="game over"
GR.TEXT.DRAW wrong1,360-(menupick*17),580+(menupick*100),a$
GR.ROTATE.END

IF menupick<>10 THEN
 FOR z2=195 TO 0 STEP-19.5
  GR.MODIFY answerlist[menupick],"alpha",z2
  GR.MODIFY screenlist1[menupick],"alpha",z2
  GR.MODIFY screenlist2[menupick],"alpha",z2
  GOSUB glitterball
  IF !BACKGROUND() THEN GR.RENDER
 NEXT z2
ENDIF

IF correctstreak>records[1] THEN
 newrecord=1
 records[1]=correctstreak
ENDIF
correctstreak=0

FOR z1=1 TO 3
 GR.MODIFY livestext,"text",LEFT$(lives$,lives-1)
 FOR z2=1 TO 30
  GOSUB glitterball
  IF !BACKGROUND() THEN GR.RENDER
 NEXT z2
 GR.MODIFY livestext,"text",LEFT$(lives$,lives)
 FOR z2=1 TO 30
  GOSUB glitterball
  IF !BACKGROUND() THEN GR.RENDER
 NEXT z2
NEXT z1
GR.MODIFY livestext,"text",LEFT$(lives$,lives-1)
IF !BACKGROUND() THEN GR.render
lives=lives-1
multiplier=1

RETURN


!====================================
!Glitterball animation
!====================================
glitterball:

gbcount=gbcount+1
IF gbcount>13 THEN gbcount=1
GR.MODIFY glitterball,"bitmap", glitterf[gbcount]

RETURN


!====================================
!copy writable files are copied to SD
!====================================
init_files:

filepath$="Whats My Song/"
SDfilepath$="../../whats my song/"
GOSUB findmnt

FILE.EXISTS z1,SDfilepath$
IF !z1 THEN
 FILE.MKDIR SDfilepath$
ENDIF

FILE.EXISTS fileid, SDfilepath$+"hiscore.txt"      %is file on SD?
IF !fileid THEN                                    %if not exist
 BYTE.OPEN r, fileid,filepath$+"orig_hiscore.txt"  %locate file in assets
 BYTE.COPY fileid, SDfilepath$+"hiscore.txt"       %copy it to SD
END IF

FILE.EXISTS fileid, SDfilepath$+"settings.txt"     %is file on SD?
IF !fileid THEN                                    %if not exist
 TEXT.OPEN w,fileid,SDfilepath$+"settings.txt"     %create one
 FOR z1=1 TO 5
  settings[z1]=1
  TEXT.WRITELN fileid,settings[z1]
 NEXT z1
 TEXT.CLOSE fileid
END IF

FILE.EXISTS fileid, SDfilepath$+"records.txt"     %is file on SD?
IF !fileid THEN                                    %if not exist
 TEXT.OPEN w,fileid,SDfilepath$+"records.txt"     %create one
 FOR z1=1 TO 5
  records[z1]=0
  TEXT.WRITELN fileid,records[z1]
 NEXT z1
 TEXT.CLOSE fileid
END IF

RETURN


!====================================
!options
!====================================
options:

GR.CLS
GR.BITMAP.LOAD backgroundf,filepath$+"5.jpg"
slidespeed=50
GR.BITMAP.DRAW background,backgroundf,0,0
GR.COLOR 255,255,255,255,2
GR.TEXT.SETFONT titlefont
GR.TEXT.SIZE 45
GR.ROTATE.START -10,360,640
GR.TEXT.DRAW title2,20,530,"options"
GR.ROTATE.END
GR.COLOR 255,255,255,255,2
GOSUB displaymenutop
GR.COLOR 140,255,255,0,2
GR.TEXT.SIZE 52
GR.SET.STROKE 1
numopts=6
offset=26
FOR z1=1 TO 6
 menutext1$[z1]=""
 menutext2$[z1]=""
NEXT z1
IF settings[1]=1 THEN
 menutext1$[1]="names:FILE"
ELSE
 menutext1$[1]="names:MP3TAG"
ENDIF

IF settings[2]=1 THEN
 menutext1$[2]="minsize:0.5MB"
ELSEIF settings[2]=2 THEN
 menutext1$[2]="minsize:1MB"
ELSEIF settings[2]=3 THEN
 menutext1$[2]="minsize:1.5MB"
ELSEIF settings[2]=4 THEN
 menutext1$[2]="minsize:2MB"
ELSEIF settings[2]=5 THEN
 menutext1$[2]="minsize:ALL"
ENDIF

IF settings[3]=1 THEN
 menutext1$[3]="search:MUSIC"
ELSEIF settings[3]=2 THEN
 menutext1$[3]="search:EXT SD"
ELSE
 menutext1$[3]="search:ALL"
ENDIF

menutext1$[4]="touch to scan"
menutext1$[6]="return"
IF numofsongs<10 THEN rturnshow=0 ELSE rturnshow=2

GOSUB displaymenu
z9=CLOCK()
DO
 GOSUB getmenu
 IF menupick>0 THEN z9=CLOCK()
 IF menupick=1 THEN
  settings[1]=-settings[1]
  IF settings[1]=1 THEN GR.MODIFY screenlist1[1],"text","names:FILE"
  IF settings[1]=-1 THEN GR.MODIFY screenlist1[1],"text","names:MP3TAG"
  PAUSE 300
  IF !BACKGROUND() THEN GR.RENDER
 ENDIF
 IF menupick=2 THEN
  settings[2]=settings[2]+1
  IF settings[2]>5 THEN settings[2]=1
  IF settings[2]=1 THEN GR.MODIFY screenlist1[2],"text","minsize:0.5MB"
  IF settings[2]=2 THEN GR.MODIFY screenlist1[2],"text","minsize:1MB"
  IF settings[2]=3 THEN GR.MODIFY screenlist1[2],"text","minsize:1.5MB"
  IF settings[2]=4 THEN GR.MODIFY screenlist1[2],"text","minsize:2MB"
  IF settings[2]=5 THEN GR.MODIFY screenlist1[2],"text","minsize:ALL"
  PAUSE 300
  IF !BACKGROUND() THEN GR.RENDER
 ENDIF
 IF menupick=3 THEN
  settings[3]=settings[3]+1
  IF settings[3]>3 THEN settings[3]=1
  IF settings[3]=1 THEN GR.MODIFY screenlist1[3],"text","search:MUSIC"
  IF settings[3]=2 THEN
   IF extsd$>"" THEN
    GR.MODIFY screenlist1[3],"text","search:EXT SD"
   ELSE
    settings[3]=settings[3]+1                        %no extsd so go to next setting
   ENDIF
  ENDIF
  IF settings[3]=3 THEN GR.MODIFY screenlist1[3],"text","search:ALL"
  PAUSE 300
  IF !BACKGROUND() THEN GR.RENDER
 ENDIF
 IF menupick=4 THEN GOSUB catalogue_songs
 IF rturnshow=1 THEN
  GR.SHOW answerlist[6]
  GR.SHOW screenlist1[6]
  GR.SHOW screenlist2[6]
  !now slide across screen
  SOUNDPOOL.PLAY menufx[6],whoosh,0.99,0.99,1,0,1
  FOR z2=0 TO 720 STEP 144
   GR.MODIFY answerlist[6],"x",0-(6*17)-720+z2
   GR.MODIFY screenlist1[6],"x",20-(6*17)-720+z2
   GR.MODIFY screenlist2[6],"x",20-(6*17)-720+z2
   IF !BACKGROUND() THEN GR.render
  NEXT z2
  rturnshow=2
 ENDIF
UNTIL menupick=6 & numofsongs>=10 | CLOCK()-z9>12000 & numofsongs>=10
rturnshow=1

TEXT.OPEN w, outfile,SDfilepath$+"settings.txt"
FOR z1=1 TO 5
 TEXT.WRITELN outfile,STR$(settings[z1])
NEXT z1
TEXT.CLOSE outfile

GR.BITMAP.DELETE backgroundf

RETURN


!====================================
!records
!====================================
showrecs:

GR.CLS
GR.BITMAP.LOAD backgroundf,filepath$+"5.jpg"
slidespeed=50
GR.BITMAP.DRAW background,backgroundf,0,0
GOSUB displaymenutop
GR.COLOR 255,255,255,255,2
GR.TEXT.SETFONT titlefont
GR.TEXT.SIZE 45
GR.ROTATE.START -10,360,640
GR.TEXT.DRAW title2,10,530,"records"
GR.COLOR 255,255,255,255,2
GR.ROTATE.END
GR.COLOR 140,255,255,0,2
GR.TEXT.SIZE 52
GR.SET.STROKE 1
numopts=6
offset=26
FOR z1=1 TO 6
 menutext1$[z1]=""
 menutext2$[z1]=""
NEXT z1
menutext1$[1]="played: "+INT$(records[3])
menutext1$[2]="streak: "+INT$(records[1])
menutext1$[3]="900 plus: "+INT$(records[2])
menutext1$[4]="best1: "+INT$(records[4])

GOSUB displaymenu

z9=CLOCK()
DO
 GR.TOUCH touched, touchx, touchy
UNTIL touched | CLOCK()-z9>8000
GR.BITMAP.DELETE backgroundf

RETURN


!====================================
!save records
!====================================
saverecs:

TEXT.OPEN w, outfile,SDfilepath$+"records.txt"
FOR z1=1 TO 5
 TEXT.WRITELN outfile,STR$(records[z1])
NEXT z1
TEXT.CLOSE outfile

RETURN


!====================================
!show logo
!====================================
showlogo:

GR.BITMAP.LOAD logo,filepath$+"mister retro.png"

GR.COLOR 0,255,255,255,2
GR.BITMAP.DRAW logo2, logo, 90,450
GR.TEXT.SIZE 40
GR.TEXT.ALIGN 2
GR.TEXT.TYPEFACE 3
GR.TEXT.DRAW logotitle,360,725,"M i s t e r   R e t r o"
FOR z1=0 TO 255 STEP 3+(quitgame*30)
 GR.COLOR z1,255,255,255,2
 GR.PAINT.GET newpaint
 GR.MODIFY logo2,"paint",newpaint
 GR.MODIFY logotitle,"paint",newpaint
 IF !BACKGROUND() THEN GR.render
NEXT z1

IF quitgame<>1 THEN
 !do jobs while logo is shown
 GOSUB init_songs
 GOSUB loadhi

 RINGER.GET.MODE z2

 IF z2<>2 THEN
  numopts=4
  offset=10
  slidespeed=50
  GR.TEXT.SETFONT textfont
  GR.TEXT.SIZE 40
  GR.SET.STROKE 2
  FOR z1=1 TO 6
   menutext1$[z1]=""
   menutext2$[z1]=""
  NEXT z1
  menutext1$[4]="  This game makes sounds on muted devices"
  GOSUB displaymenu
  menupick=0
  DO
   GOSUB getmenu
  UNTIL menupick>0
 ENDIF
ENDIF

IF !BACKGROUND() THEN GR.render

GR.TEXT.ALIGN 2
GR.TEXT.SIZE 40

IF quitgame<>1 THEN
 GR.HIDE null
 GR.HIDE num
 GR.TEXT.TYPEFACE 3
ENDIF

FOR z1=255 TO 0 STEP -10-(quitgame*20)
 GR.MODIFY logo2,"alpha",z1
 GR.MODIFY logotitle,"alpha",z1
 IF quitgame<>1 THEN GR.MODIFY answerlist[4],"alpha",z1
 IF !BACKGROUND() THEN GR.render
NEXT z1

GR.BITMAP.DELETE logo

RETURN


!====================================
!show hiscores
!====================================
showhi:

IF gametype=6 THEN gmenu=INT(RND()*3)+1 ELSE gmenu=gametype
%non-attract mode show is 0

a$=" hi scores"
IF gmenu=1 THEN
 a$="intro"+a$
ELSEIF gmenu=2 THEN
 a$="outro"+a$
ELSEIF gmenu=3 THEN
 a$="rando"+a$
ENDIF

glitterball=0
slidespeed=50
GR.CLS
GR.COLOR 255,255,255,255,2
GR.BITMAP.LOAD backgroundf,filepath$+background$+".jpg"
GR.BITMAP.DRAW background,backgroundf,0,0
GR.TEXT.ALIGN 1
offset=-15
GR.TEXT.SETFONT titlefont
GR.TEXT.SIZE 45
GR.ROTATE.START -10,360,640
GR.TEXT.DRAW title2,20,530,a$
GR.HIDE title2
GR.COLOR 255,255,255,255,2
GR.TEXT.ALIGN 2
GR.TEXT.SIZE 110
GR.SET.STROKE 3
GR.TEXT.DRAW scoretext1,450,200,INT$(score)
IF gametype=6 THEN GR.hide scoretext1
GR.ROTATE.END
GR.COLOR 140,255,255,0,2
GR.TEXT.SIZE 52
GR.SET.STROKE 1
numopts=6
rturnshow=1
offset=26
FOR z1=1 TO 6
 menutext1$[z1]=""
 menutext2$[z1]=""
NEXT z1
z8=0
FOR z1=1 TO 6
 IF hiscore [z1,gmenu]=score
  z8=z1
  menutext1$[z1]=" "
 ELSE
  menutext1$[z1]=INT$(hiscore[z1,gmenu])+"    "+hiname$[z1,gmenu]
 ENDIF
NEXT z1
IF gametype=6 THEN GR.show title2
GOSUB displaymenu
IF z8>0 & z8<7 THEN        %drift score to hi score table
 GR.ROTATE.START -10,360,640
 GR.COLOR 255,255,255,255,2
 GR.TEXT.ALIGN 1
 GR.SET.STROKE 1
 GR.TEXT.SIZE 110
 GR.TEXT.DRAW scoretext2,450,200,INT$(score)
 GR.ROTATE.END
 GR.HIDE scoretext1
 GR.TEXT.WIDTH z5, scoretext2
 z6=(110-52)
 finalx=-20-(z8*17)+42
 finaly=400+(z8*100)+15
 scorex=220
 scorey=200 % 385

 z3=(scorex-finalx)
 z4=(((z8*100)+385))

 z3=z3/20
 z4=z4/20
 z6=z6/20
 FOR z2=1 TO 20
  GR.TEXT.SIZE (110-(z6*z2))
  GR.PAINT.GET gtext
  GR.MODIFY scoretext2,"x",scorex,"y",scorey, "paint", gtext
  scorex=scorex-z3
  scorey=scorey+z4
  IF !BACKGROUND() THEN GR.RENDER
 NEXT z2

 GR.TEXT.WIDTH z5,scoretext2
 GR.MODIFY scoretext2,"x",finalx,"y",finaly+160
 IF !BACKGROUND() THEN GR.RENDER
 GOSUB getname
ENDIF

GR.BITMAP.DELETE backgroundf

z9=CLOCK()
DO
 GR.TOUCH touched,touchx,touchy
UNTIL touched | CLOCK()-z9>8000

RETURN


!====================================
!check score vs hi. Get name if needed
!====================================
checkhi:

FOR z1=1 TO 6
 IF score>hiscore[z1, gametype] THEN
  !push other scores down
  FOR z2=6 TO z1 STEP -1
   hiscore[z2+1,gametype]=hiscore[z2,gametype]
   hiname$[z2+1,gametype]=hiname$[z2,gametype]
   hisex[z2+1, gametype]=hisex[z2,gametype]
  NEXT z2

  hiscore[z1,gametype]=score
  F_N.BREAK
 ENDIF
NEXT z1

RETURN


!====================================
!load hiscores and names + settings
!====================================
loadhi:

TEXT.OPEN r, hifile,SDfilepath$+"hiscore.txt"

FOR z3=1 TO 3
 FOR z2=1 TO 6
  TEXT.READLN hifile,a$
  SPLIT result$[],a$,","
  hiscore[z2,z3]=FLOOR(VAL(result$[1]))
  hiname$[z2,z3]=result$[2]
  hisex[z2,z3]=VAL(result$[3])
  ARRAY.DELETE result$[]
 NEXT z2
NEXT z3

TEXT.CLOSE hifile

TEXT.OPEN r,setfile,SDfilepath$+"settings.txt"
FOR z5=1 TO 5
 TEXT.READLN setfile,b$
 settings[z5]=VAL(b$)
NEXT z5
TEXT.CLOSE setfile

TEXT.OPEN r,recfile,SDfilepath$+"records.txt"
FOR z5=1 TO 5
 TEXT.READLN recfile,b$
 records[z5]=VAL(b$)
NEXT z5
TEXT.CLOSE setfile


RETURN


!====================================
!save hiscores and names
!====================================
savehi:

TEXT.OPEN w, hifile,SDfilepath$+"hiscore.txt"

FOR z3=1 TO 3
 FOR z2=1 TO 6
  a$=STR$(hiscore[z2,z3])+","
  a$=a$+hiname$[z2,z3]+","
  a$=a$+STR$(hisex[z2,z3])
  TEXT.WRITELN hifile,a$
 NEXT z2
NEXT z3

TEXT.CLOSE hifile

RETURN


!====================================
!get hiscore name - version 2
!====================================
getname:

GR.COLOR 255,235,235,235,2
GR.TEXT.SETFONT titlefont
GR.TEXT.SIZE 80
GR.COLOR 255,255,255,255,2
a$="_"
!rotate this and place next to hiscore
GR.ROTATE.START -10,360,640
GR.MODIFY scoretext2,"text",INT$(score)+"    "+a$
GR.ROTATE.END
GR.TEXT.ALIGN 2

FOR z6=1 TO 4
 FOR z2=1 TO 7
  z3=((z6-1)*7)+z2
  a$=""
  IF z3<=26 THEN
   a$=CHR$(z3+96)
  ELSE
   IF z3=27 THEN a$="<"
   IF z3=28 THEN a$=">>"
  ENDIF
  GR.TEXT.DRAW hiletter[z3],(z2*100)-50,120+(z6*110),a$
 NEXT z2
NEXT z6
IF !BACKGROUND() THEN GR.render
a$=""
z2=0
flash=30
GR.TEXT.SIZE 52
GR.TEXT.ALIGN 1
GR.COLOR 255,255,255,255,2
GR.PAINT.GET paintwht
GR.COLOR 255,0,0,0,2
GR.PAINT.GET paintblk
WHILE z2<5
 DO
  GR.TOUCH touched, touchx, touchy
  touchx=touchx/scale_w
  touchy=touchy/scale_h
  IF flash=30 THEN
   GR.MODIFY scoretext2,"paint",paintwht
  ELSEIF flash=0 THEN
   GR.MODIFY scoretext2,"paint",paintblk
   flash=60
  ENDIF
  flash=flash-1
  IF !BACKGROUND() THEN GR.RENDER
 UNTIL touched & touchy<561 & touchy>119
 touchx=FLOOR((touchx)/100)+1
 touchy=FLOOR((touchy-120)/110)
 z4=(touchy*7)+touchx
 IF z4<29 THEN % valid selection
  IF z4=27 THEN
   IF LEN(a$)>0 THEN
    a$=LEFT$(a$,(LEN(a$)-1))
    z2=z2-1
   ENDIF
  ELSEIF z4=28 THEN
   IF LEN(a$)<1 THEN a$="RETRO"
   z2=5
  ELSE
   a$=a$+CHR$(z4+64)
   GR.COLOR 255,55,55,55,2
   GR.TEXT.SIZE 80
   GR.TEXT.ALIGN 2
   GR.TEXT.DRAW hibox,(touchx*100)-50,120+((touchy+1)*110),CHR$(((touchy*7)+touchx)+96)
   z2=z2+1
  ENDIF
  IF z2<5 THEN b$="__" ELSE b$=""
  GR.MODIFY scoretext2,"text",INT$(score)+"    "+a$+b$
  SOUNDPOOL.PLAY click1,correct,0.99,0.99,0,0,1.8
  IF !BACKGROUND() THEN GR.RENDER
 ENDIF
 PAUSE 200
 GR.HIDE hibox
REPEAT
hiname$[z8, gametype]=a$
GR.HIDE higrid
GOSUB savehi

FOR z3=1 TO 28
 GR.HIDE hiletter[z3]
NEXT z3
GR.SHOW title2
IF !BACKGROUND() THEN GR.RENDER

RETURN


!======================================
! Song search
!--------------------------------------
! Part 1: Read Folder Contents into array
!--------------------------------------
catalogue_songs:

storepointer=0
readpointer=1

! now we know what device has available.
! check against option

IF settings[3]=3 THEN                 %all
 allfolderlist$[1]=intsd$
 storepointer=1
 if extsd$>"" then
  allfolderlist$[2]=extsd$
  storepointer=2
 endif
ELSEIF settings[3]=2 THEN             %external sd
 allfolderlist$[1]=extsd$
 storepointer=1
ELSE                                  %music folders only
 allfolderlist$[1]=intsd$+"Music/"    %internal music folder
 allfolderlist$[2]=extsd$+"Music/"    %external music folder
 storepointer=2
ENDIF
seekfolder$=allfolderlist$[1]

DO
 FILE.EXISTS foldexist,rootfolder$+seekfolder$
 IF foldexist<>0 THEN
  FILE.DIR rootfolder$+seekfolder$, tempfolderlist$[],"**"
  ARRAY.LENGTH z4,tempfolderlist$[]
  FOR z1=1 TO z4
   a$=tempfolderlist$[z1]
   IF a$>"" & RIGHT$(a$,2)="**" THEN
    a$=LEFT$(a$,LEN(a$)-2)
    IF LEFT$(a$,1)="." | LEFT$(a$,3)="com" | LEFT$(a$,4)="proc" | LEFT$(a$,3)="sys" THEN
    ELSE
     IF storepointer<3000 THEN
      storepointer=storepointer+1
      allfolderlist$[storepointer]=seekfolder$+a$+"/"
      IF storepointer=3000 THEN
       z1=z4
       readpointer=storepointer
      ENDIF
     ENDIF
    ENDIF
   ENDIF %foldexist
  NEXT z1
 ENDIF

 readpointer=readpointer+1
 a$=allfolderlist$[readpointer]
 seekfolder$=a$

 ARRAY.DELETE tempfolderlist$[]
 !IF storepointer>0 THEN a$=INT$((readpointer/storepointer)*100) ELSE a$="0"
 IF storepointer>0 THEN a$=INT$(storepointer) ELSE a$="0"
 GR.MODIFY screenlist1[4],"text","folders:"+a$
 IF !BACKGROUND() THEN GR.RENDER
UNTIL readpointer>storepointer

!--------------------------------------
! Part 2: Search Folders for mp3s
!--------------------------------------

z9=1
GR.SHOW progbar

FOR z3=1 TO storepointer
 songfolder$=allfolderlist$[z3]
 FILE.EXISTS foldexist,rootfolder$+songfolder$
 IF foldexist<>0 THEN
  FILE.DIR rootfolder$+songfolder$, folderlist$[],"**"
  ARRAY.LENGTH z2,folderlist$[]

  FOR z1=1 TO z2
   a$=folderlist$[z1]
   IF rootfolder$+songfolder$>"" & RIGHT$(a$,2)<>"**" THEN
    !check for being wav/mp3 here
    b$=UPPER$(RIGHT$(a$,3))
    IF b$="WAV" | b$="MP3" THEN
     FILE.SIZE fsize,rootfolder$+songfolder$+a$
     IF settings[2]=5 | settings[2]<>5 & fsize>settings[2]*500000 THEN
      allsonglist$[z9,1]=songfolder$
      allsonglist$[z9,2]=a$
      IF z9<2500 THEN  %max number of songs allowed
       z9=z9+1
      ELSE
       z1=z2
       z3=storepointer
      ENDIF
     ENDIF
    ENDIF
   ENDIF
  NEXT z1
  a$=FORMAT$("###",(z3/storepointer)*100)+"%"
  GR.COLOR 195,0,0,0,2
  GR.MODIFY screenlist1[4],"text","progress"+a$
  GR.COLOR 255,0,0,0,2
  GR.MODIFY progbar,"x", -790+(720*(z3/storepointer))
  IF !BACKGROUND() THEN GR.RENDER
  ARRAY.DELETE folderlist$[]
 ENDIF
NEXT z3

numofsongs=z9-1
a$=INT$(numofsongs)
GR.MODIFY screenlist1[4],"text","found:"+a$
GR.HIDE progbar
IF !BACKGROUND() THEN GR.render

GOSUB write_songs

IF numofsongs>=10 THEN
 rturnshow=rturnshow+1
 IF rturnshow>2 THEN rturnshow=2
ELSE
 IF rturnshow>0 THEN
  FOR z2=720 TO -144 STEP -144
   GR.MODIFY answerlist[6],"x",0-(6*17)-720+z2
   GR.MODIFY screenlist1[6],"x",20-(6*17)-720+z2
   GR.MODIFY screenlist2[6],"x",20-(6*17)-720+z2
   IF !BACKGROUND() THEN GR.render
  NEXT z2
 ENDIF
 rturnshow=0
ENDIF
z9=CLOCK()

RETURN


!--------------------------------------
!output songs to file
!--------------------------------------
write_songs:

TEXT.OPEN w, outfile, SDfilepath$+"songlist.txt"
FOR z1=1 TO numofsongs
 TEXT.WRITELN outfile, allsonglist$[z1,1]
 TEXT.WRITELN outfile, allsonglist$[z1,2]
NEXT z1
TEXT.CLOSE outfile
savenew=0

RETURN


!--------------------------------------
! Part 3 : Read mp3 header
!--------------------------------------
!--------------------------------------
! Create functions before they are used
!--------------------------------------
init_functions:

FN.DEF getheader$(f$)
 BYTE.OPEN r, firstfile,f$
 BYTE.READ.BUFFER firstfile,10, header$

 ! check if ID3 tag present ---------
 IF LEFT$(header$,3)="ID3" THEN
  tagversion=ASCII(MID$(header$,4,1))
  flag$=BIN$(ASCII(MID$(header$,6,1)))
  IF LEN(flag$)>1 THEN
   flagunsync=VAL(LEFT$(flag$,1))
   flagextend=VAL(MID$(flag$,2,1))
  ENDIF
  size=syncsafe(MID$(header$,7,4))

  IF flagextend=1 THEN
   ! read extended header here if exists
  ENDIF
  BYTE.POSITION.MARK firstfile,size
  a$=INT$(z1)+f$
 ENDIF

 !now go through fulltag$ reading frames
 z2=0
 songtitle$=""
 songartist$=""
 WHILE z2<size
  BYTE.READ.BUFFER firstfile,4,frameid$
  BYTE.READ.BUFFER firstfile,4,a$
  framesize=unsafe(a$)
  IF frameid$="TIT2" | frameid$="TPE1"
   BYTE.READ.BUFFER firstfile,2,frameflag$
   BYTE.READ.BUFFER firstfile,framesize,framedata$
  ELSE
   BYTE.POSITION.SET firstfile,z2+12+framesize
  ENDIF
  z2=z2+10+framesize
  IF frameid$="TIT2" THEN songtitle$=framedata$
  IF frameid$="TPE1" THEN songartist$=framedata$
  IF songtitle$>"" & songartist$>"" THEN z2=size
 REPEAT
 BYTE.CLOSE firstfile
 a$=songtitle$+" - "+songartist$
 FN.RTN a$
FN.END

FN.DEF unsafe(a$)
 z1=ASCII(MID$(a$,1,1))*16777216
 z1=z1+ASCII(MID$(a$,2,1))*32768
 z1=z1+ASCII(MID$(a$,3,1))*256
 z1=z1+ASCII(MID$(a$,4,1))
 FN.RTN z1
FN.END

FN.DEF syncsafe(a$)
 z1=ASCII(MID$(a$,1,1))*2097152
 z1=z1+ASCII(MID$(a$,2,1))*16384
 z1=z1+ASCII(MID$(a$,3,1))*128
 z1=z1+ASCII(MID$(a$,4,1))
 FN.RTN z1
FN.END

RETURN

!======================================
!find mnt folder and see what available
!======================================
findmnt:

newpath$=""
FILE.ROOT f$
SPLIT.ALL mainlist$[],f$,"/"
ARRAY.LENGTH fdepth,mainlist$[]
target=0
FOR z1=1 TO fdepth
 IF mainlist$[z1]="mnt" THEN target=z1
NEXT z1

IF target=0 THEN target=2

FOR z1=target TO fdepth
 newpath$=newpath$+"../"
NEXT z1
newpath$=newpath$+"mnt/"

! read mnt directory folders

FILE.DIR newpath$, mountfldr$[]
ARRAY.LENGTH a,mountfldr$[]
FOR z1=1 TO a
 a$=LEFT$(mountfldr$[z1],-3)
 IF LEFT$(a$,3)="ext" THEN extsd$=a$+"/"
 IF LEFT$(a$,2)="sd" THEN intsd$=a$+"/"
NEXT z1

ARRAY.DELETE mainlist$[], mountfldr$[]
rootfolder$=newpath$

RETURN


!====================================
!back key trapping
!====================================
ONBACKKEY:

GOSUB backmenudo

BACK.RESUME


backmenudo:
oldbackbutton=backbutton
IF backbutton=0 THEN
 quitgame=1
 GR.COLOR 255,255,255,2
 GR.CLS
 GOSUB showlogo
 EXIT
ENDIF

AUDIO.ISDONE musiccheck
backbutton=0
backmenu$[1]="resume"
backmenu$[2]="menu"
backclock=CLOCK()

AUDIO.PAUSE
GR.COLOR 0,5,5,5,2
GR.RECT fade,0,0,720,1280

GR.TEXT.ALIGN 2
GR.TEXT.SIZE 52
GR.TEXT.SETFONT titlefont

GR.ROTATE.START -10,360,640

!initial off screen plot
GR.COLOR 255,0,0,0,2
FOR z0=1 TO 2
 z9=FLOOR(RND()*7)+1
 GR.BITMAP.DRAW backbar[z0],stave1,0-(z0*17)-750,500+(z0*100)
 GR.COLOR 255,0,0,120,2
 GR.SET.STROKE 1
 GR.TEXT.ALIGN 2
 GR.TEXT.DRAW backtext[z0],20-(z0*17)-375,550+(z0*100)+25,backmenu$[z0]
NEXT z0
GR.ROTATE.END

!now slide across screen
FOR z0=1 TO 2
 SOUNDPOOL.PLAY menufx[z0],whoosh,0.99,0.99,1,0,1
 FOR z10=0 TO 720 STEP 72
  GR.MODIFY backbar[z0],"x",0-(z0*17)-720+z10
  GR.MODIFY backtext[z0],"x",20-(z0*17)-360+z10
  IF z0=1 THEN GR.MODIFY fade,"alpha", z10/3.2
  GR.RENDER
 NEXT z10

 !last update to show perfect alignment
 GR.MODIFY backbar[z0],"x",0-(z0*17)
 GR.MODIFY backtext[z0],"x",20-(z0*17)+343
NEXT z0

IF !BACKGROUND() THEN GR.render

bmenupick=0
DO
 GR.TOUCH touched,touchx,touchy
 touchx=touchx/scale_w
 touchy=touchy/scale_h
 GR.GET.BMPIXEL touchmap, touchx, touchy, cz4, cz1, cz2, cz3

 IF touched THEN
  IF cz1=0 & cz2=0 & cz3=0 THEN bmenupick=0
  IF cz1=255 & cz2=0 & cz3=0 THEN bmenupick=1
  IF cz1=255 & cz2=255 & cz3=0 THEN bmenupick=2
 ENDIF
UNTIL bmenupick>0

FOR z0=1 TO 2
 GR.HIDE backbar[z0]
 GR.HIDE backtext[z0]
NEXT z0

!amend clock
z9c=(CLOCK()-backclock)+z9c
IF bmenupick=2 THEN
 lives=0
 z9=CLOCK()
 quitnow=1
ELSE
 IF musiccheck=0 THEN AUDIO.PLAY audlist[playsong]
ENDIF

touched=0
PAUSE 300
GR.HIDE fade
backbutton=oldbackbutton

RETURN


!====================================
!error trapping
!====================================
ONERROR:

RETURN
