REM hangman v1.0 for BASIC!  by antonis 

ab$="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
lab=len(ab$)
! pointers to letters and already pressed letters array
dim lobj[lab],pressedletter[lab]
dim word$[1200]

! read word file 
text.open r,filn,"wordsh.txt"
numofwords=0
do 
  text.readln filn,wo$
  numofwords=numofwords+1
  word$[numofwords]=wo$
until wo$="EOF"
text.close filn

dim wf[numofwords] % used to choose different rnd words
wordsplayed=0
gplayed=0
scor=0

! ****here restarts a new game****
begin:
pressedletter$=""
attempt=0
found=0

!go graphics
cls
gr.open 255,255,155,25
gr.color 255,255,255,255,1
gr.orientation 1
gr.screen w,h
if w>h then % exchange values
    z=w
    w=h
    h=z
endif
 
! i use a virtual screen of 480 x 800
! & scale in 3 cases
  if w>=720 & h>=1200 then 
                        myscale = 1.5
  elseif w>=400 & h>= 800 then 
                           myscale = 1
  else
        myscale=0.4
  endif
gr.scale myscale,myscale

gr.bitmap.load  bp1,"kidicon.png" % load kid icon
gr.bitmap.draw  newob1,bp1,300,550
gr.render

! read bitmap for hangman
gr.bitmap.load  bptr1,"androidicon.png"
! create head
gr.bitmap.crop newbptr2,bptr1,60,0,157,90
! create left hand
gr.bitmap.crop newbptr1,bptr1,10,0,53,277
! create right hand
gr.bitmap.crop newbptr3,bptr1,217,0,53,277
! body
gr.bitmap.crop newbptr4,bptr1,61,91,157,130
! feet
gr.bitmap.crop newbptr5,bptr1,61,220,157,57

txtsize=37
gr.text.size txtsize
gr.text.bold 1 
gr.text.draw obj1,5,40,"Hangman v1.0"

! print letters,13 in a row
yoffset=0
j=0
for i=1 to lab
    j=j+1
   if i=14 then % change row
        yoffset=60
        j=1
   endif
  cc$=mid$(ab$,i,1)
  gr.text.draw lobj[i],(j-1)*txtsize,660+yoffset,cc$
next i
gr.render

! draw gallow
gr.rect recobj1,10,300,110,320 % down part
gr.rect recobj2,50,60,70,300 % straight part
gr.rect recobj3,20,60,300,80 % up horizontal part
gr.rect recobj4,260,80,280,130 % small vertical part
gr.render

! choose a new random word
choose:
     trnd=ceil(rnd(t)*numofwords))
     for i=1 to numofwords
            if wf[i]=trnd then goto choose                                                                      
      next i

! print underscores
for i=1 to len(word$[trnd])
   gr.text.draw guessobj,i*5+(i-1)*txtsize+2,500,"_"
   gr.render
next i
w$=word$[trnd]

!*******main*********
do 
  lost=1
  finished=0

start: % ...of pressing letters
numletter=0
! wait for a letter
do
   touched=-1
   gr.touch touched,x,y
until touched>0

if  x>13*txtsize*myscale | x<0 | y<620*myscale | y>740*myscale then goto start

! check pressed coordinates
i=0
ok=0
do
  i=i+1 
  if x<i*txtsize*myscale then   %  found column    
          ok=1
          xler=i   
        endif
until ok=1 
if y<680*myscale then yler=0 else yler=1 % found row

! hide pressed letter
ler=xler+yler*13
gr.hide lobj[ler]
gr.render

! check if letter has been already pressed and return to start
ok=1
g$= mid$(ab$,ler,1)
if len(pressedletter$)>0 then 
      for j=1 to len(pressedletter$)
            if g$=mid$(pressedletter$,j,1) then ok=0                   
      next j
    endif

! letter has been already pressed
if ok=0 then goto start

! new letter pressed, memorize it
pressedletter$=pressedletter$+g$

! check if letter is in  word
for indx=1 to len(w$)
    if mid$(w$,indx,1)=g$ then
     numletter=indx
     found=found+1 
     gr.text.draw obj,indx*5+(indx-1)*txtsize,500,g$
     gr.render
    endif 
next indx
   
! found all letters, won
if found=len(word$[trnd]) then 
                            finished=1        
                            lost=0        
                          endif

! found wrong letter, u have 5 tries to guess word
 if numletter=0 then
             tone 500,500
              attempt=attempt+1
              sw.begin attempt
                 
                sw.case 1 % draw head
                              gr.bitmap.draw bpt1,newbptr2,190,130
                              gr.render
                              sw.break
                sw.case 2 % draw body
                              gr.bitmap.draw bpt2,newbptr4,190,220
                              gr.render
                              sw.break
                sw.case 3 % draw left hand
                              gr.bitmap.draw bpt3,newbptr1,137,130
                              gr.render
                              sw.break
                sw.case 4 % draw right hand
                              gr.bitmap.draw bpt4,newbptr3,346,130
                              gr.render
                              sw.break
                sw.case 5 % draw feet
                              gr.bitmap.draw bpt5,newbptr5,190,350
                              gr.render
                              lost=1
                              finished=1
                              sw.break
            sw.end     
           endif
 
until finished=1

! score:
gr.text.size 30
gr.color 255,0,255,255,1
gplayed=gplayed+1
if lost=0 then scor=scor+1
sc$=str$(scor)
gp$=str$(gplayed)
sc$=replace$(sc$,".0","")
gp$=replace$(gp$,".0","")
f$="Score: "+sc$+"|"+gp$
gr.text.draw obj,280,40,f$

! current game finished
! print won or lost then play or quit
gr.text.size 40
if lost=0 then 
      gr.color 255,0,255,0,1
      gr.text.draw obj,10,600,"YOU WON" 
      gr.render
      gr.color 255,255,255,255,1
      gosub happykid 
  else 
   gr.color 255,255,0,0,1
   gr.text.draw obj,10,600,"YOU LOST"
   gr.render
   gr.color 255,255,255,255,1
   gr.text.size 37
! reveal word, comment out
!  next 3 lines if you don't want to   
   for i=1 to len(w$)
      gr.text.draw obj,i*5+(i-1)*txtsize,500,mid$(w$,i,1)
   next i 
   gosub sadkid
endif

! memorize played word
wordsplayed=wordsplayed+1
wf[wordsplayed]=trnd 

! print play or quit   
gr.text.size 30
gr.color 255,0,255,0,255
gr.text.draw pl, 50,780,"play"
gr.color 255,0,0,255,255
gr.text.draw qu, 300, 780,"quit"
gr.render

! wait for play or quit
touched = -1
do
    gr.touch touched, x, y
until (x>50*myscale & x<170*myscale & y>750*myscale & y<780*myscale) | x>300*myscale & x<420*myscale & y>750*myscale & y<780*myscale)

if x<120*myscale then 
           gr.close
           goto begin % play again
         else
          gr.close  
          print "Thanks for playing Hangman!"             
          end  % quit
      endif

!**** end of main *****

sadkid: % some effect for losing
! fall down
for i=1 to 100 step 10 
  pause 100
  gr.rotate.start -i,300,636
  gr.hide newob1
  gr.bitmap.draw  newob1,bp1,300,550
  gr.render
  gr.rotate.end
next i
audio.load au, "laugh.wav"
audio.play au
pause 1000
audio.stop
return
 
happykid:  % some effect for winning
! do a complete rotation
for i=0 to 360 step 10 
  pause 10
  gr.rotate.start -i,340,593
  gr.hide newob1
  gr.bitmap.draw  newob1,bp1,300,550
  gr.render
  gr.rotate.end
next i
audio.load au, "appl.mp3"
audio.play au
pause 1500
audio.stop
return

