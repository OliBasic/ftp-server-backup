Hangman v1.0 by Antonis
The Classical game of hangman.
You have 5 chances to guess the word.
Includes vocabulary of over 1100 words.
Put files androidicon.png, laugh.wav, appl.mp3, kidicon.png, wordsh.txt
into your "/rfo-basic/data/" directory.
Put hangmanv1.bas into your "/rfo-basic/source/" directory.
Or download and install hangmanv1.apk
Updated Nov 8/2011

