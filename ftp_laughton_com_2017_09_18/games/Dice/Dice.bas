Rem Dice
Rem November 2014
Rem With RFO Basic!
Rem Version 1.00
Rem By Roy Shepherd
Rem

console.title"Dice"
Rem Open Graphics
di_width = 1280 % set to my tablet
di_height = 800

gr.open 255,0,0,0 % Black
gr.orientation 0
pause 1000
gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y
gr.text.size 25
wakelock 3

Rem Globel Vars
dim diceBackGround[1]
dim playerDice[6] 
dim androidDice[6]
dim dangerousScore[6]
dim upDate[11] % 1:Android score 2:Player score 3:Android wins 4:Player wins 5:Message 6:Round 7:winner 8:Sound on  
!				 9:Players dangerous dice 10:Android dangerous dice  11:gr.clip   
          
dim info[4] % 1:Android score 2:Player score 3:Android wins 4:Player wins 
dim mess$[3] % 1:Messeges 2:What round it is 3: The winer
	
dim match[1] %Rounds
dim soundOnOff[4]
soundOnOff[2]=1 % sound on or off 1 holds the sound.png 2:sound on or off 3:buttonClick 4: diceClater
 
Rem Make a bitmap from the dices 2 to 6 and uses the onedice to hols the one to compleat the real
dim diceReal[4] % 1 and 2 player dice real, 3 and 4 android dice real
dim oneDice[2] % number 1 to compleat the real for player & 2 for android
dim playerUpDate[2] % 1 for the real  and 2 for the oneDice
dim androidUpDate[2] % 1 for the real  and 2 for the oneDice
dim startY[2]
startY[1]=1 %so that the real starts from where it left off
startY[2]=1 
tts.init

Rem Start of Functions

fn.def LoadSounds(soundOnOff[])
	Soundpool.open 1
	Soundpool.load soundOnOff[3],"buttonClick.wav"
	Soundpool.load soundOnOff[4],"diceClater.wav"
fn.end

fn.def LoadImages(playerDice[],androidDice[],diceBackGround[],soundOnOff[],dangerousScore[])

	for x=1 to 6
		gr.bitmap.load playerDice[x], "dice0"+int$(x)+".png"
		gr.bitmap.scale playerDice[x], playerDice[x], 120,120
		
		gr.bitmap.load dangerousScore[x],"dice0"+int$(x)+".png"
		gr.bitmap.scale dangerousScore[x], dangerousScore[x], 50,50
	next

	for x=1 to 6 
		gr.bitmap.load androidDice[x],"dice0"+int$(x)+".png"
		gr.bitmap.scale androidDice[x],androidDice[x],120,120
	next

	gr.bitmap.load diceBackGround[1],"DiceBG.png" 
	gr.bitmap.scale diceBackGround[1],diceBackGround[1],1000,800
	gr.bitmap.load soundOnOff[1],"soundOn.png"
	gr.bitmap.scale soundOnOff[1],soundOnOff[1],45,45
fn.end

fn.def MakeDiceReals(diceReal[],playerDice[],androidDice[],oneDice[],upDate[])
	Rem Android's dice real
	gr.bitmap.create diceReal[3], 120,600
	gr.bitmap.drawinto.start diceReal[3]
	y=1
	for x=5 to 1 step - 1
		gr.bitmap.draw d, androidDice[x],1,y
		y+=120
	next
	gr.bitmap.drawinto.end
	
	Rem Player's dice real
	gr.bitmap.create diceReal[1], 120,600
	gr.bitmap.drawinto.start diceReal[1]
	y=1
	for x=5 to 1 step - 1
		gr.bitmap.draw d, playerDice[x],1,y
		y+=120
	next
	gr.bitmap.drawinto.end
	
	Rem Set view port for dice and set reals positions
	gr.clip upDate[11],245,325,1000,445 % View port for dice
	Rem Set up to run reals
	Rem Player's real
	gr.bitmap.draw diceReal[2], diceReal[1], 245,-275 % player's real
	Rem Android's real
	gr.bitmap.draw diceReal[4], diceReal[3], 870,-275 % android's real
	Rem Do the oneDice[]
	gr.bitmap.draw oneDice[1], playerDice[6],245,325 % player's six
	gr.bitmap.draw oneDice[2], androidDice[6],870,325 % android's six
	gr.render
fn.end

fn.def RunAndroidsDice(diceReal[],oneDice[],androidDice[],startY[],soundOnOff[],tx,scale_x,upDate[],info[],mess$[],scale_y)
	pause 200
	flag=0
	ct=1
		do
			for y=startY[2] to 120*6 step 15
				if soundOnOff[2] then Soundpool.play s,soundOnOff[4],0.99,0.99,1,0,1 % Dice clater
				gr.modify diceReal[4],"y",-275+y
				if y> 600 then gr.modify oneDice[2],"y",205+y-600 % six dice
				if y<125 then gr.modify oneDice[2],"y",205+y+120 % six dice to complete the real
				gr.render 
				diceNum = ROUND(y/120, 2,"D")
				if (int(y/120)=diceNum) & ct>1 then 
					r=int(3*rnd())				
					if r=1 then 
						flag=1 
						f_n.break
					endif
				endif
				Rem See if any menu buttons have been tapped
				gr.touch touched,tx,ty
				if touched & tx>160*scale_x & tx<1070*scale_x & ty>670*scale_y & ty<800*scale_y then
					do 
						gr.touch touched,tx,ty
					until !touched
					Menu(soundOnOff[],tx,scale_x,upDate[],info[],mess$[],1)
				endif
			next
			startY[2]=1
			ct++
		until flag=1
		if diceNum=0 then diceNum=6
		startY[2]=y
	fn.rtn diceNum
fn.end

fn.def RunPlayersDice(diceReal[],oneDice[],playerDice[],startY[],soundOnOff[],tx,scale_x,upDate[],info[],mess$[],scale_y)
	flag=0
	ct=1
		do
			for y=startY[1] to 120*6 step 15
				if soundOnOff[2] then Soundpool.play s,soundOnOff[4],0.99,0.99,1,0,1 % Dice clater
				gr.modify diceReal[2],"y",-275+y
				if y> 600 then gr.modify oneDice[1],"y",205+y-600 % six dice
				if y<125 then gr.modify oneDice[1],"y",205+y+120 % six dice to complete the real
				gr.render 
				diceNum = ROUND(y/120, 2,"D")
				if (int(y/120)=diceNum) & ct>1 then 
					r=int(3*rnd())				
					if r=1 then 
						flag=1 
						f_n.break
					endif
				endif
				Rem See if any menu buttons have been tapped
				gr.touch touched,tx,ty
				if touched & tx>160*scale_x & tx<1070*scale_x & ty>670*scale_y & ty<800*scale_y then
					do 
						gr.touch touched,tx,ty
					until !touched
					Menu(soundOnOff[],tx,scale_x,upDate[],info[],mess$[],1)
				endif
			next
			startY[1]=1
			ct++
		until flag=1
		if diceNum=0 then diceNum=6
		startY[1]=y
	fn.rtn diceNum
fn.end

fn.def SetUpPartScreen(diceBackGround[],di_width,di_height,soundOnOff[],upDate[],dangerousScore[])
	call CurvyWindow(1,1,di_width,di_height,8)
	call CurvyWindow(4,4,di_width-4,di_height-4,2)
	gr.bitmap.draw d,diceBackGround[1],120,1
	gr.bitmap.draw upDate[8],soundOnOff[1],594,256 % sound on
	Rem Draw Dangerous Score Dices and then hide them
	gr.bitmap.draw upDate[9],dangerousScore[6],280,525 % players dangerous dice
	gr.bitmap.draw upDate[10],dangerousScore[6],905,525 % Andoid dangerous dice
	gr.hide upDate[9]
	gr.hide upDate[10]
	gr.render
fn.end

fn.def SetUpScreen(upDate[],info[],match[],mess$[],playerDice[],androidDice[],soundOnOff[])	
	Rem Setup scores and Messages
	match[1]=1
	for x=1 to 4 
		info[x]=0
	next
	call Gcol(7,1)
	gr.text.size 25
	gr.text.align 1
	gr.text.draw upDate[1],275,92,int$(info[1]) % Andoid score
	gr.text.draw upDate[2],275,122,int$(info[2]) % player score
	gr.text.draw upDate[3],1014,87,int$(info[3]) % Andoid wins
	gr.text.draw upDate[4],1014,119,int$(info[4]) % player wins
	
	Rem Setup Messeges
	mess$[1]="Tap New Game to Start"
	mess$[2]="Round "+int$(match[1])+ " of 5"
	mess$[3]=""
	call Gcol(0,1)
	gr.text.align 2
	gr.text.size 40
	gr.text.draw update[7],630,550,mess$[3]
	gr.text.align 1
	call Gcol(8,1)
	gr.text.size 30
	gr.text.align 2 
	gr.text.draw upDate[5],610,225,mess$[1] % top message
	gr.text.draw upDate[6],610,360,mess$[2] % Round
	gr.render
	if soundOnOff[2] then tts.speak "Tap New Game to Start"
fn.end

fn.def TopBoxMesseges$(upDate[],messNum,mess$[],soundOnOff[])
	if messNum=1 then mess$[1]="Tap New Game to Start"
	if messNum=2 then mess$[1]="I'll Decide Who Gose First"
	if messNum=3 then mess$[1]="I'll go First"
	if messNum=4 then mess$[1]="You go First" 
	if messNum=5 then mess$[1]="I'll Spin For My Dangerous Score"
	if messNum=6 then mess$[1]="I'll Spin Again"
	if messNum=7 then mess$[1]="I'm Not Stopping Now"
	if messNum=8 then mess$[1]="I'm Feeling Luckey"
	if messNum=9 then mess$[1]="I'll Push My Luck"
	if messNum=10 then mess$[1]="I Lost All My Points In This Round"
	if messNum=11 then mess$[1]="I'll Stick Now"
	
	Rem Players Messeges
	if messNum=12 then mess$[1]="Tap Spin to get Your Dangerous Score"
	if messNum=13 then mess$[1]="Spin or Stick"
	if messNum=14 then mess$[1]="You Lost All Your Points In This Round"
	if messNum=15 then mess$[1]="Your Sticking At That"
	
	gr.modify upDate[5],"text",mess$[1]
	gr.render
	if soundOnOff[2] then Tts.speak mess$[1]
fn.end

fn.def CurvyWindow(x,y,l,h,c)
	call Gcol(c,1)
	gr.rect r,x,y+25,l,h-25 
	gr.rect r,x+25,y,l-25,h 
	gr.circle c, x+25,y+25,25 
	gr.circle c,l-25,y+25,25 
	gr.circle c,x+25,h-25,25
	gr.circle c,l-25,h-25,25
	gr.render
fn.end

fn.def Gcol(c,g)
	Rem g=0 Outline and g=1 Fill
	if c=0 then gr.color 255,0,0,0,g % black
	if c=1 then gr.color 255,255,0,0,g % Red
	if c=2 then gr.color 255,0,255,0,g % green
	if c=3 then gr.color 255,0,0,255,g % blue
	if c=4 then gr.color 255,255,255,0,g % yellow
	if c=5 then gr.color 255,0,255,255,g % cyan
	if c=6 then gr.color 255,255,0,255,g % magenta
	if c=7 then gr.color 255,192,192,192,g % gray
	if c=8 then gr.color 255,255,255,255,g % white
fn.end

fn.def Help(soundOnOff[])
	read.from 1
	h$=""
	while r$<>"EOD"
		read.next r$
		if r$<>"EOD" then h$=h$+r$+chr$(10)
	repeat
	Dialog.Message chr$(9861)+" Help",h$,ok,"OK"
	if soundOnOff[2] then Soundpool.play s,soundOnOff[3],0.99,0.99,1,0,1 % Button clicked
	pause 100
fn.end

fn.def About(soundOnOff[])
	p$=chr$(10)+chr$(10) % Paragraph break
	Dialog.Message chr$(9861)+ ~
		" About"," Dice"+p$ + ~
		" With RFO-BASIC!" +p$+ ~
		" For Android"+p$+ ~
		" November 2014"+p$+ ~
		" By Roy Shepherd",ok,"OK"
	if soundOnOff[2] then Soundpool.play s,soundOnOff[3],0.99,0.99,1,0,1 % Button clicked
	pause 100
fn.end

fn.def Quite(upDate[],soundOnOff[])
	Dialog.Message chr$(9861)+ "Exit","Are you Sure you want to Exit Dice",yn,"Yes","No"
	if yn = 1 then
		if soundOnOff[2] then Soundpool.play s,soundOnOff[3],0.99,0.99,1,0,1 % Button clicked
		gr.modify upDate[11],"left",200,"top",200,"right",1080,"bottom",600
		gr.text.size 70
		gr.text.align 2
		call CurvyWindow(200,200,1080,600,0)
		call CurvyWindow(220,220,1060,580,4)
		call ThreeDtext(0,1,630,300,"Thanks for Playing")
		call ThreeDtext(0,1,630,400,"  D i c e")
		call ThreeDtext(0,1,630,500," Goodbye")
		gr.text.align 1
		pause 5000
		wakelock 5
		soundpool.release
		Tts.stop
		gr.close
		exit	
	endif
	if soundOnOff[2] then Soundpool.play s,soundOnOff[3],0.99,0.99,1,0,1 % Button clicked
	pause 100
fn.end

fn.def ThreeDtext(c1,c2,x,y,t$)
	gr.text.bold 1
	call Gcol(c1,1)
	gr.text.draw d,x,y,t$
	call Gcol(c2,1)
	gr.text.draw d,x+2,y-2,t$
	gr.text.bold 0
	gr.render
fn.end

fn.def ToddleSound(upDate[],soundOnOff[])
	if soundOnOff[2]=1 then 
		soundOnOff[2]=0 
		gr.hide upDate[8] % sound No png
	else 
		soundOnOff[2]=1 
		gr.show upDate[8] % sound No png
	endif
	gr.render
	if soundOnOff[2] then Soundpool.play s,soundOnOff[3],0.99,0.99,1,0,1 % Button clicked
	pause 100
fn.end

fn.def NewGame(upDate[],info[],mess$[],soundOnOff[],running)
	if running then 
		Dialog.Message chr$(9861)+ "New Game","Game in Progress",ok,"OK"
		if soundOnOff[2] then Soundpool.play s,soundOnOff[3],0.99,0.99,1,0,1 % Button clicked
	else
		info[1]=0
		info[2]=0 
		call UpDateScores(upDate[],info[])
		call TopBoxMesseges$(upDate[],2,mess$[],soundOnOff[]) % I'll Decide owe gose first
		pause 1000
		r= int(2 * rnd() + 1)
		if r=1 then 
			call TopBoxMesseges$(upDate[],3,mess$[],soundOnOff[]) % I'll go first
		else 
			call TopBoxMesseges$(upDate[],4,mess$[],soundOnOff[]) % You go First
		endif 
		pause 1000
		gr.modify update[7],"text",""
		fn.rtn r
	endif
fn.end

fn.def AnnounceWinner(upDate[],info[],mess$[],soundOnOff[])
	pause 1000
	if info[1]>info[2] then
		info[3]++
		mess$[3]="I Wins This Game"
	elseif info[2]>info[1] then 
		info[4]++
		mess$[3]="You Wins This Game"
	elseif info[1]=info[2] then 
		mess$[3]="A Draw, No Wins"
	endif
	
	call UpDateScores(upDate[],info[])
	gr.modify update[7],"text",mess$[3]
	if soundOnOff[2] then tts.speak mess$[3]
	call TopBoxMesseges$(upDate[],1,mess$[],soundOnOff[])
fn.end

fn.def UpDateScores(upDate[],info[])
	for s=1 to 4 
		gr.modify upDate[s],"text",int$(info[s]) % Scores and wins
	next
	gr.render
fn.end

fn.def UpDateRound(upDate[],mess$[],soundOnOff[])
	gr.modify upDate[6],"text",mess$[2]
	gr.render
	if soundOnOff[2] then tts.speak mess$[2]
fn.end

fn.def Menu(soundOnOff[],tx,scale_x,upDate[],info[],mess$[],running)
	Rem ty and scale_y must be ok to get here
	flag=0
	if soundOnOff[2] then Soundpool.play s,soundOnOff[3],0.99,0.99,1,0,1 % Button Click
	if tx>160*scale_x & tx<310*scale_x then call Help(soundOnOff[])
	if tx>350*scale_x & tx<500*scale_x then call About(soundOnOff[])
	if tx>540*scale_x & tx<690*scale_x then call ToddleSound(upDate[],soundOnOff[])
	if tx>730*scale_x & tx<880*scale_x then flag = NewGame(upDate[],info[],mess$[],soundOnOff[],running)
	if tx>920*scale_x & tx<1070*scale_x then call Quite(upDate[],soundOnOff[])
	fn.rtn flag
fn.end

fn.def GetTouch(tx,ty)
	do 
		gr.touch touched,tx,ty 
	until touched
	do 
		gr.touch touched,tx,ty 
	until !touched
fn.end

fn.def AndroidRollDice(upDate[],info[],mess$[],diceReal[],oneDice[],androidDice[],startY[],dangerousScore[],soundOnOff[],first,scale_x,scale_y,match[])
	androidSoFar=info[1]
	say=6
	stick=0
	call TopBoxMesseges$(upDate[],5,mess$[],soundOnOff[]) % I'll Spin For My Dangerous Score
	dangerousDice = RunAndroidsDice(diceReal[],oneDice[],androidDice[],startY[],soundOnOff[],tx,scale_x,upDate[],info[],mess$[],scale_y)
	
	info[1]+=dangerousDice 
	call UpDateScores(upDate[],info[])
	gr.show upDate[10] % dangerousScore that was hiden until first run
	gr.modify upDate[10],"bitmap",dangerousScore[dangerousDice]
	do 
		pause 1000
		r=int(4*rnd())
		if first & info[1] > info[2]+r+4 then stick=1
		if !first & info[1] > info[2]+r then stick=1
		if !first & match[1]=5 & info[1]>info[2] then stick = 1
		if !stick then
			call TopBoxMesseges$(upDate[],say,mess$[],soundOnOff[]) % I'll Spin Again
			dice = RunAndroidsDice(diceReal[],oneDice[],androidDice[],startY[],soundOnOff[],tx,scale_x,upDate[],info[],mess$[],scale_y)
			info[1]+=dice 
			call UpDateScores(upDate[],info[])
			say++
			if say=10 then say = 7
		endif
	until dangerousDice = dice | stick
	if dangerousDice = dice then 
		call TopBoxMesseges$(upDate[],10,mess$[],soundOnOff[]) % I Lost All My Points In This Round
		info[1]=androidSoFar 
		call UpDateScores(upDate[],info[])
	else
		call TopBoxMesseges$(upDate[],11,mess$[],soundOnOff[]) % I'll Stick Now
	endif
	pause 1000
fn.end

fn.def PlayerRollDice(upDate[],info[],mess$[],diceReal[],oneDice[],androidDice[],startY[],dangerousScore[],scale_x,scale_y,soundOnOff[])
	tx=0 
	ty=0 
	willStick=0
	playerSoFar=info[2]
	call TopBoxMesseges$(upDate[],12,mess$[],soundOnOff[]) % Spin For Your Dangerous Score
	do
		call GetTouch(&tx,&ty)
		if tx>160*scale_x & tx<1070*scale_x & ty>670*scale_y & ty<800*scale_y then flag = Menu(soundOnOff[],tx,scale_x,upDate[],info[],mess$[],1)
	until tx>170 * scale_x & tx<290 * scale_x & ty>430 * scale_y & ty<500 * scale_y
	if soundOnOff[2] then 
		Soundpool.play s,soundOnOff[3],0.99,0.99,1,0,1 % Button clicked
		pause 100
	endif
	dangerousDice = RunPlayersDice(diceReal[],oneDice[],androidDice[],startY[],soundOnOff[],tx,scale_x,upDate[],info[],mess$[],scale_y)
	info[2]+=dangerousDice 
	call UpDateScores(upDate[],info[])
	gr.show upDate[9] % dangerousScore that was hide
	gr.modify upDate[9],"bitmap",dangerousScore[dangerousDice]
	do 
		call TopBoxMesseges$(upDate[],13,mess$[],soundOnOff[]) % Spin or Stick
		call GetTouch(&tx,&ty)
		if tx>160*scale_x & tx<1070*scale_x & ty>670*scale_y & ty<800*scale_y then flag = Menu(soundOnOff[],tx,scale_x,upDate[],info[],mess$[],1)
		if tx>170 * scale_x & tx<290 * scale_x & ty>430 * scale_y & ty<500 * scale_y then
			if soundOnOff[2] then
				Soundpool.play s,soundOnOff[3],0.99,0.99,1,0,1 % Button clicked
				pause 100 
			endif
			dice = RunPlayersDice(diceReal[],oneDice[],androidDice[],startY[],soundOnOff[],tx,scale_x,upDate[],info[],mess$[],scale_y)
			info[2]+=dice 
			call UpDateScores(upDate[],info[])
		endif
		if tx>320 * scale_x & tx<430 * scale_x & ty>430 * scale_y & ty<500 * scale_y then
			willStick=1
			if soundOnOff[2] then
				Soundpool.play s,soundOnOff[3],0.99,0.99,1,0,1 % Button clicked
				pause 100 
			endif
		endif
	until dangerousDice = dice | willStick
	if dangerousDice = dice then 
		call TopBoxMesseges$(upDate[],14,mess$[],soundOnOff[]) % You Lost All My Points In This Round
		info[2]=playerSoFar 
		call UpDateScores(upDate[],info[])
	else
		call TopBoxMesseges$(upDate[],15,mess$[],soundOnOff[]) % Your Sticking At That
	endif
	pause 1000
fn.end

fn.def PlayDice(playerDice[],androidDice[],soundOnOff[],scale_x,scale_y,upDate[],info[],mess$[],match[],diceReal[],oneDice[],startY[],dangerousScore[])
	do
		tx=0 
		ty=0
		flag=0
		
		call GetTouch(&tx,&ty)
		if tx>160*scale_x & tx<1070*scale_x & ty>670*scale_y & ty<800*scale_y then 
			!flag=1 % One of menu touch
			flag = Menu(soundOnOff[],tx,scale_x,upDate[],info[],mess$[],0)
		endif
		match[1]=1
		mess$[2]="Round "+int$(match[1])+ " of 5"
			if flag=2 then 
				call UpDateRound(upDate[],mess$[],soundOnOff[])
				do
					call PlayerRollDice(upDate[],info[],mess$[],diceReal[],oneDice[],androidDice[],startY[],dangerousScore[],scale_x,scale_y,soundOnOff[])
					call AndroidRollDice(upDate[],info[],mess$[],diceReal[],oneDice[],androidDice[],startY[],dangerousScore[],soundOnOff[],0,scale_x,scale_y,match[])
					match[1]++
					mess$[2]="Round "+int$(match[1])+ " of 5"
					if match[1]<6 then call UpDateRound(upDate[],mess$[],soundOnOff[])
				until match[1]=6
				call AnnounceWinner(upDate[],info[],mess$[],soundOnOff[])
			else if flag = 1 then
				call UpDateRound(upDate[],mess$[],soundOnOff[])
				do
					call AndroidRollDice(upDate[],info[],mess$[],diceReal[],oneDice[],androidDice[],startY[],dangerousScore[],soundOnOff[],1,scale_x,scale_y,match[])
					call PlayerRollDice(upDate[],info[],mess$[],diceReal[],oneDice[],androidDice[],startY[],dangerousScore[],scale_x,scale_y,soundOnOff[])
					match[1]++
					mess$[2]="Round "+int$(match[1])+ " of 5"
					if match[1]<6 then call UpDateRound(upDate[],mess$[],soundOnOff[])
				until match[1]=6
				call AnnounceWinner(upDate[],info[],mess$[],soundOnOff[])
			endif
		
	until 0
fn.end

Rem End Of Functions
call LoadSounds(soundOnOff[])
call LoadImages(playerDice[],androidDice[],diceBackGround[],soundOnOff[],dangerousScore[])
call SetUpPartScreen(diceBackGround[],di_width,di_height,soundOnOff[],upDate[],dangerousScore[])

call SetUpScreen(upDate[],info[],match[],mess$[],playerDice[],androidDice[],soundOnOff[])
call MakeDiceReals(diceReal[],playerDice[],androidDice[],oneDice[],upDate[])
call PlayDice(playerDice[],androidDice[],soundOnOff[],scale_x,scale_y,upDate[],info[],mess$[],match[],diceReal[],oneDice[],startY[],dangerousScore[])
do 
until 0 

onBackKey:
call Quite(upDate[],soundOnOff[])
back.resume

end


read.data"Dice:"
read.data""
read.data"You play against your device."
read.data"There are 5 'rounds' in a game."
read.data"Each round consists of one dice-"
read.data"throwing session (or 'turn') for you"
read.data"and one for your device."
read.data""
read.data"Your device will decide (by random"
read.data"selection) who should start. Let's"
read.data"assume that it's you."
read.data"You start each turn by throwing the"
read.data"dice to create a 'dangerous' score."
read.data""
read.data"You must then decide whether to throw"
read.data"the dice again. You can continue to"
read.data"throw the dice for as long as you like."
read.data"Every time that you throw the dice,"
read.data"including the first time, the face"
read.data"value of the dice is added to your"
read.data"score."
read.data""
read.data"However, the catch is that if you"
read.data"throw the 'dangerous' score, you lose"
read.data"all of the points that you have just"
read.data"won in that round!"
read.data"EOD"


