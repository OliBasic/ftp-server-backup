Traffic

Traffic is a simple traffic quiz.
On screen a traffic sign is shown.
Select the description which fits the sign.
Ten descriptions are offered.
Scroll through them by tapping above or below the central 'answer bar'.
Once you think you have the correct answer in the answer bar, tap on it to select.
You score one point if the selected answer is correct.
The game ends after 10 answers.

At first start you have the option to select 1 of 4 languages.
(English, French, German or Dutch).

A list of scores is maintained. Your best score is saved and compared to others
(if any) and ranked in the list. Max. score is 10 points....

Feel free to add a language! Just copy one of the present language files (*.trl)
to a new language file, having this language as the file name.
Do not change the file layout. Translate all but line 1 and 40 (though not important).
Place the new language file in ../../Traffic/data/ and go......select 'change language'
after finishing a game to set the new language for the next game.
The language has to be supported by your device.
Please, send me copy if you have a new (or better) language file.

Have fun, Aat