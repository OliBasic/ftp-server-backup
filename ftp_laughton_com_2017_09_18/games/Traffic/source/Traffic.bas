! Traffic multilingual
! Aat @2016
GR.OPEN 255,128,128,128,0,1
GR.SCREEN w,h:ScaleX=600:ScaleY=h/w*ScaleX:sx=w/ScaleX:sy=h/ScaleY:GR.SCALE sx,sy
DataPath$="../../Traffic/data/":NumOfSigns=84:NumOfTurns=10:Cfg=0:NumPlayers=0
GR.BiTMAP.LOAD Total,DataPath$+"Total.png"
DIM Numbers[NumOfSigns],ShuffledNumbers[NumOfSigns],DiffNums[NumOfTurns],ShowAnswers[10],Answers$[12]
DIM TheText$[38],TheTopPaint[7],TheCenterPaint[7],TheBottomPaint[7]
FOR i=1 TO NumOfSigns:Numbers[i]=i:NEXT i
FILE.DIR DataPath$,Trans$[]:ARRAY.LENGTH L,Trans$[]
LIST.CREATE S,FNames
FOR i=1 TO L
	IF RIGHT$(Trans$[i],3)="cfg" THEN
		TEXT.OPEN R,Config,DataPath$+"Traffic.cfg"
		TEXT.READLN Config,Language$:TEXT.READLN Config,Sound$:TEXT.READLN Config,Num$:NumPlayers=VAL(Num$)
		DIM Name$[NumPlayers],Score[NumPlayers]
		FOR j=1 TO NumPlayers:TEXT.READLN Config,Name$[j]:TEXT.READLN Config,S$:Score[j]=VAL(S$):NEXT j
		TEXT.CLOSE Config
		Cfg=1:Sound=Val(Sound$)
	ENDIF
	IF RIGHT$(Trans$[i],3)="trl" THEN LIST.ADD FNames,LEFT$(Trans$[i],-4)
NEXT i
FreshStart:
IF !Cfg THEN
	DO:DIALOG.SELECT L,FNames,"TRAFFIC":UNTIL L>0:LIST.GET FNames,L,Language$
ENDIF
GRABFILE Desc$,DataPath$+Language$+".trl"
FOR i=1 TO 38:TheText$[i]=WORD$(Desc$,i+1,CHR$(13)):NEXT i
IF !Cfg THEN
	! Show directions
	HTML$="<html><head><style>body{background-color:green;font-family:Arial;font-size:2.5em;font-weight:bold;color:yellow}#Title{text-align:center;font-size:300%;color:white}"
	HTML$=HTML$+"#text {text-align:center;font-size:100%}#Button{font-size:100%;color:white;background-color:rgb(128,128,128);border:0.1em solid yellow}"
	HTML$=HTML$+"</style><script>function doDataLink(data){Android.dataLink(data);}function doDataLink(data){Android.dataLink(data);}</script></head><body><div id="+CHR$(34)+"Title"+CHR$(34)+">Traffic</div><hr>"
	HTML$=HTML$+TheText$[27]+"<br>":HTML$=HTML$+TheText$[28]+"<br>":HTML$=HTML$+TheText$[29]+"<br>"
	HTML$=HTML$+TheText$[30]+"<br>":HTML$=HTML$+TheText$[31]+"<br>":HTML$=HTML$+TheText$[32]+"<br>"
	HTML$=HTML$+TheText$[33]+"<br>":HTML$=HTML$+TheText$[34]+"<br>":HTML$=HTML$+TheText$[35]
	HTML$=HTML$+TheText$[36]+"<br>"+TheText$[37]+TheText$[38]
	HTML$=HTML$+"<div id="+CHR$(34)+"text"+CHR$(34)+">"
	HTML$=HTML$+"<input id="+CHR$(34)+"Button"+CHR$(34)+" type="+CHR$(34)+"button"+CHR$(34)+" value="+CHR$(34)+" << OK >> "+CHR$(34)+" onclick="+CHR$(34)+"doDataLink('OK')"+CHR$(34)+"></div></body></html>"
	HTML.OPEN:HTML.ORIENTATION 1:HTML.LOAD.STRING HTML$
	DO:HTML.GET.DATALINK data$:UNTIL LEFT$(data$,3)="BAK" | IS_IN("OK",data$):HTML.CLOSE
ENDIF
GR.CLS:FOR i=0 to 140 STEP 5:GR.COLOR 255,128-128/(140/5)*(140-i)/5,128,128-128/(140/5)*(140-i)/5,1:GR.RECT g,0,i,600,i+10:NEXT i
GR.COLOR 255,255,255,0,1:GR.TEXT.SiZE 75:GR.TEXT.ALIGN 2:GR.TEXT.DRAW g,300,100,TheText$[1]:GR.TEXT.ALIGN 1
GR.TEXT.SiZE 30:GR.TEXT.DRAW NumRounds,10,490,TheText$[2]+" "+INT$(1)+" ("+TheText$[3]+" "+INT$(NumOfTurns)+")"
GR.TEXT.DRAW ShowScore,10,660,TheText$[4]+" "+INT$(0)
GR.COLOR 255,0,0,0,1:GR.RECT g,0,140,600,460
GR.TEXT.SiZE 40:GR.TEXT.DRAW g,5,610,"^"
GR.ROTATE.START 180,21,512:GR.TEXT.DRAW g,21,512,"^":GR.ROTATE.END
GR.COLOR 255,255,255,255,1:GR.RECT g,0,155,600,165:GR.RECT g,0,435,600,445
FOR i=1 TO 9:GR.RECT g,(i-1)*70,290,50+(i-1)*70,310:NEXT i
GR.COLOR 92,255,0,0,1:GR.RECT g,0,500,600,540:GR.RECT g,0,580,600,620
GR.COLOR 92,0,0,0,1:GR.RECT g,0,540,600,580
GR.COLOR 255,0,0,0,0:GR.SET.STROKE 1:GR.CIRCLE g,12,560,5:GR.RECT g,0,500,600,620
GR.SET.STROKE 3:GR.RECT g,0,540,600,580
GR.TEXT.SiZE 20:GR.TEXT.ALiGN 2:GR.COLOR 128,0,0,255,1:GR.TEXT.DRAW Top,300,530,TheText$[16]
N=0:FOR i=20 TO 14 STEP -1:GR.TEXT.SiZE i:N=N+1:GR.PAINT.GET TheTopPaint[N]:NEXT i
GR.TEXT.SiZE 20:GR.COLOR 255,0,255,0,1:GR.TEXT.SKEW -0.25:GR.TEXT.DRAW Center,300,570,TheText$[17]
N=0:FOR i=20 TO 14 STEP -1:GR.TEXT.SiZE i:N=N+1:GR.PAINT.GET TheCenterPaint[N]:NEXT i
GR.TEXT.SiZE 20:GR.COLOR 128,255,255,0,1:GR.TEXT.SKEW 0:GR.TEXT.DRAW Bottom,300,610,TheText$[18]
N=0:FOR i=20 TO 14 STEP -1:GR.TEXT.SiZE i:N=N+1:GR.PAINT.GET TheBottomPaint[N]:NEXT i
IF Turn<NumOfTurns THEN
	GR.TEXT.SIZE 200:GR.TEXT.DRAW qg,300,370,"?"
	GR.COLOR 255,255,192,255,1:GR.TEXT.SIZE 40:GR.TEXT.DRAW dg,1000,250,TheText$[19]
	FOR i=670 to ScaleY STEP 5:GR.COLOR 255,128/((ScaleY-670)/5)*(ScaleY-i)/5,128,128/((ScaleY-670)/5)*(ScaleY-i)/5,1:GR.RECT g,0,i,600,i+10:NEXT i
	GR.SET.STROKE 10:GR.COLOR 255,255,0,0,0:GR.RECT g,0,0,600,ScaleY
	FOR i=1 TO LEN(TheText$[19]):GR.MODIFY dg,"x",1000-i*20:GR.SHOW.TOGGLE qg:GR.RENDER:PAUSE 200:NEXT i
	GR.HIDE dg:GR.HIDE qg
ENDIF
PAUSE 2000:GR.COLOR 255,255,255,255,1:GR.TEXT.SIZE 100:GR.RENDER
DO:DIALOG.MESSAGE TheText$[5],TheText$[6],Sound,TheText$[7],TheText$[8]:UNTIL Sound
Start:
! Pick different numbers
ARRAY.COPY Numbers[],ShuffledNumbers[]:ARRAY.SHUFFLE ShuffledNumbers[]
s=FLOOR((NumOfSigns-NumOfTurns)*RND()+1):ARRAY.COPY ShuffledNumbers[s,NumOfTurns],DiffNums[]:CurScore=0
GR.MODIFY ShowScore,"text",TheText$[4]+" "+INT$(CurScore)
FOR Turn=1 TO NumOfTurns
	ShowBar=5:IF Sound=1 THEN TONE 440,100,0
	GR.MODIFY NumRounds,"text",TheText$[2]+" "+INT$(Turn)+" ("+TheText$[3]+" "+INT$(NumOfTurns)+")"
	ARRAY.COPY Numbers[],ShuffledNumbers[]
	! Pick next number
	SignNr=DiffNums[Turn]
	! Get 9 different descriptions
	SWAP ShuffledNumbers[1],ShuffledNumbers[SignNr]:ARRAY.SHUFFLE ShuffledNumbers[2,NumOfSigns-1]
	ARRAY.COPY ShuffledNumbers[1,10],ShowAnswers[]:ARRAY.SHUFFLE ShowAnswers[]
	! Read 10 answers including correct one
	FOR i=2 TO 11:Answers$[i]=WORD$(Desc$,ShowAnswers[i-1]+40,CHR$(13)):NEXT i
	! Read correct answer
	D$=WORD$(Desc$,SignNr+40,CHR$(13))
	! Show 3 answers
	L=LEN(Answers$[ShowBar]):K=INT((L>50)*(L-46)/5)+1:J=INT((K>7)*(L-76)/5):J=K-J
	GR.MODIFY Top,"paint",TheTopPaint[J]:GR.MODiFY Top,"text",Answers$[ShowBar]
	L=LEN(Answers$[ShowBar+1]):K=INT((L>50)*(L-46)/5)+1:J=INT((K>7)*(L-76)/5):J=K-J
	GR.MODIFY Center,"paint",TheCenterPaint[J]:GR.MODiFY Center,"text",Answers$[ShowBar+1]
	L=LEN(Answers$[ShowBar+2]):K=INT((L>50)*(L-46)/5)+1:J=INT((K>7)*(L-76)/5):J=K-J
	GR.MODIFY Bottom,"paint",TheBottomPaint[J]:GR.MODiFY Bottom,"text",Answers$[ShowBar+2]
	GR.BiTMAP.CROP Sign,Total,MOD(SignNr-1,10)*300,iNT((SignNr-1)/10)*300,300,300
	GR.BiTMAP.DRAW Bord,Sign,150,150
	GR.TEXT.DRAW AnswerOK,300,280,TheText$[9]:GR.TEXT.DRAW AnswerWrong,300,280,TheText$[10]
	GR.HIDE AnswerOK:GR.HIDE AnswerWrong:GR.RENDER
	DO:GOSUB GetTouch:CheckedBar=INT((y-500)/40+1)
		IF CheckedBar>0 & CheckedBar<4 THEN
			IF CheckedBar=1 THEN
				IF ShowBar>1 THEN
					IF Sound=1 THEN TONE 740,100,0
					ShowBar=ShowBar-1
				ENDIF
			ENDIF
			IF CheckedBar=3 THEN
				IF ShowBar<10 THEN
					IF Sound=1 THEN TONE 740,100,0
					ShowBar=ShowBar+1
				ENDIF
			ENDIF
		ENDIF
		L=LEN(Answers$[ShowBar]):K=INT((L>50)*(L-46)/5)+1:J=INT((K>7)*(L-76)/5):J=K-J
		GR.MODIFY Top,"paint",TheTopPaint[J]:GR.MODiFY Top,"text",Answers$[ShowBar]
		L=LEN(Answers$[ShowBar+1]):K=INT((L>50)*(L-46)/5)+1:J=INT((K>7)*(L-76)/5):J=K-J
		GR.MODIFY Center,"paint",TheCenterPaint[J]:	GR.MODiFY Center,"text",Answers$[ShowBar+1]
		L=LEN(Answers$[ShowBar+2]):K=INT((L>50)*(L-46)/5)+1:J=INT((K>7)*(L-76)/5):J=K-J
		GR.MODIFY Bottom,"paint",TheBottomPaint[J]:GR.MODiFY Bottom,"text",Answers$[ShowBar+2]
		GR.RENDER
	UNTIL CheckedBar=2
	IF Answers$[ShowBar+1]=D$ THEN
		CurScore=CurScore+1:GR.SHOW AnswerOK:IF Sound=1 THEN TONE 440,300,0:TONE 740,100,0
	ELSE
		GR.SHOW AnswerWrong:IF Sound=1 THEN TONE 740,300,0:TONE 440,100,0
	ENDIF
	GR.MODIFY Bord,"alpha",128:GR.MODIFY ShowScore,"text",TheText$[4]+" "+INT$(CurScore):GR.RENDER
	GR.GETDL Displ[],1:ARRAY.LENGTH L,Displ[]:GR.NEWDL Displ[1,L-3]:ARRAY.DELETE Displ[]
	DO:DIALOG.MESSAGE TheText$[20],D$,g,"<>":UNTIL g
NEXT Turn
! Check score
IF NumPlayers=0 THEN
	DO:INPUT TheText$[21],N$,,Canceled:UNTIL !Canceled
	NumPlayers=1
	DIM Name$[1],Score[1]:Name$[1]=N$:Score[1]=CurScore
ELSE
	ARRAY.COPY Name$[],TempName$[-1]:ARRAY.COPY Score[],TempScore[-1]
	TempName$[1]=TheText$[22]
	DO:DIALOG.SELECT P,TempName$[],TheText$[23]+"/"+TheText$[21]:UNTIL P:P=P-1
	IF P=0 THEN
		DO:INPUT TheText$[21],N$,,Canceled:UNTIL !Canceled
		NumPlayers=NumPlayers+1
		TempName$[1]=N$:TempScore[1]=CurScore
		ARRAY.DELETE Name$[],Score[]
		ARRAY.COPY TempName$[],Name$[]:ARRAY.COPY TempScore[],Score[]
	ELSE
		IF Score[P]<CurScore THEN Score[P]=CurScore
	ENDIF
	ARRAY.DELETE TempName$[],TempScore[]
	! Rank scores
	FOR i=1 TO NumPlayers
		ARRAY.MAX Hi,Score[i,NumPlayers-i+1]
		ARRAY.SEARCH Score[],Hi,M1
		SWAP Name$[M1],Name$[i]
		SWAP Score[M1],Score[i]
	NEXT i
ENDIF
GOSUB MakeConfig:GOSUB ViewScore
DO:DIALOG.MESSAGE TheText$[11],TheText$[12],Answer,TheText$[13],TheText$[14],TheText$[26]:UNTIL Answer
IF Answer=1 THEN GOTO Start
IF Answer=3 THEN 
	DO:DIALOG.SELECT L,FNames,"TRAFFIC":UNTIL L>0:LIST.GET FNames,L,Language$
	GOSUB MakeConfig
	GOTO FreshStart
ENDIF
GR.CLOSE
EXIT
GetTouch:
	DO:GR.TOUCH Touched,x,y:UNTiL Touched
	DO:GR.TOUCH Touched,x,y:UNTiL !Touched
	x/=sx:y/=sy
RETURN
MakeConfig:
TEXT.OPEN W,Config,DataPath$+"Traffic.cfg"
	TEXT.WRITELN Config,Language$
	TEXT.WRITELN Config,INT$(Sound)
	TEXT.WRITELN Config,INT$(NumPlayers)
	FOR i=1 TO NumPlayers
		TEXT.WRITELN Config,Name$[i]
		TEXT.WRITELN Config,INT$(Score[i])
	NEXT i
TEXT.CLOSE Config
RETURN
ViewScore:
	IF NumPlayers=0 THEN RETURN
	HTML$="<html><head><style>body{background-color:green;font-family:Arial;font-size:3em;font-weight:bold;color:yellow}#Title{text-align:center;font-size:100%;color:white}"
	HTML$=HTML$+"#text {text-align:center;font-size:100%}#Button{font-size:100%;color:white;background-color:rgb(128,128,128);border:0.1em solid yellow}"
	HTML$=HTML$+"</style><script>function doDataLink(data){Android.dataLink(data);}</script></head><body><div id="+CHR$(34)+"Title"+CHR$(34)+">Traffic Score</div><hr>"
	HTML$=HTML$+"<table align="+CHR$(34)+"center"+CHR$(34)+" border="+CHR$(34)+"1"+CHR$(34)+" style="+CHR$(34)+"font-size: 100%;background-color:#0000FF;"+CHR$(34)+" cellpadding="+CHR$(34)+"10"+CHR$(34)+">"
	HTML$=HTML$+"<tr><th>#</th><th>"+TheText$[24]+"</th><th>"+TheText$[25]+"</th></tr>"
	HTML$=HTML$+"<tr><td>"+INT$(1)+"</td><td>"+Name$[1]+"</td><td align='center'>"+INT$(Score[1])+"</td></tr>"
	IF NumPlayers>1 THEN
		FOR i=2 TO NumPlayers
			HTML$=HTML$+"<tr><td>"
			IF Score[i]=Score[i-1] THEN
				HTML$=HTML$+INT$(i-1)
			ELSE
				HTML$=HTML$+INT$(i)
			ENDIF
			HTML$=HTML$+"</td><td>"+Name$[i]+"</td><td align='center'>"+INT$(Score[i])+"</td></tr>"
		NEXT i
	ENDIF
	HTML$=HTML$+"</table><div id="+CHR$(34)+"text"+CHR$(34)+"><br><input id="+CHR$(34)+"Button"+CHR$(34)+" type="+CHR$(34)+"button"+CHR$(34)+" value="+CHR$(34)+" << OK >> "+CHR$(34)+" onclick="+CHR$(34)+"doDataLink('OK')"+CHR$(34)+"></div></body></html>"
	HTML.OPEN:HTML.ORIENTATION 1:HTML.LOAD.STRING HTML$
	DO:HTML.GET.DATALINK data$:UNTIL LEFT$(data$,3)="BAK" | IS_IN("OK",data$):HTML.CLOSE
RETURN
ONBACKKEY:
	IF Sound=1 THEN TONE 440,300,0
	DO:DIALOG.MESSAGE TheText$[11],TheText$[15],Answer,TheText$[13],TheText$[14],TheText$[25]:UNTIL Answer
	IF Answer=1 THEN
		IF Cfg THEN GOSUB MakeConfig
		GR.CLOSE:EXIT
	ENDIF
	IF Answer=3 THEN GOSUB ViewScore
BACK.RESUME
