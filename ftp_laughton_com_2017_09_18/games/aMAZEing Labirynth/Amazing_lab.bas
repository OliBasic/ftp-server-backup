REM AMAZING Labyrinth game... @Cassiope34 / 01-17

Fn.def find$(item, no, c[])
for y=1 to 7
  for x=1 to 7
    if c[x,y,item]=no then Fn.rtn int$(x*10+y)
  next
next
Fn.end

Fn.def xy$(x,y)   % 2 digit because x nor y > 9...
 Fn.rtn int$(x*10+y)
Fn.end

Fn.def xfc(c$)    % val 'x' from c$  ( see the 'xy$(x,y)' function )
 Fn.rtn int(val(c$)/10)
Fn.end

Fn.def yfc(c$)    % val 'y' from c$
 Fn.rtn mod(val(c$),10)
Fn.end

Fn.def clr( cl$ )
 gr.color val(word$(cl$,1,",")), val(word$(cl$,2,",")), val(word$(cl$,3,",")), ~
          val(word$(cl$,4,",")), val(word$(cl$,5,","))
Fn.end

Fn.def LastPtr(ptr)    % put the graphic pointer 'ptr' (so the bitmap) UP to all other.
  gr.getDL ndl[],1
  array.length sz, ndl[]
  if !ptr | sz=1 | ndl[sz] =ptr then array.delete ndl[] : Fn.rtn 0
  array.search ndl[],ptr,n
  if n
    for nn=n to sz-1 : ndl[nn] =ndl[nn+1] : next
    ndl[sz] =ptr : gr.newDL ndl[]
  endif
  array.delete ndl[]
Fn.end

Fn.def move(ptr, arx, ary, speed)   % move smoothly the bitmap 'ptr' to arx,ary
LastPtr( ptr )
do
  gr.get.position ptr,dpx,dpy
  ix =int((dpx-arx)/speed)
  iy =int((dpy-ary)/speed)
  if abs(ix)<3 & abs(iy)<3 then D_u.break
  gr.modify ptr, "x",dpx-ix, "y",dpy-iy
  gr.render
until 0
gr.modify ptr, "x",arx, "y",ary   % final position
gr.render
Fn.end

FN.DEF RondRect(x,y, c, lx,ly, fill)
GR.ARC nul, x,     y,     x+c,     y+c,  -90, -90, fill  % top left
GR.ARC nul, x+lx-c,y,     x+lx,    y+c,  -90,  90, fill  % top right
GR.ARC nul, x,     y+ly-c,x+c,     y+ly, -180,-90, fill  % bottom left
GR.ARC nul, x+lx-c,y+ly-c,x+lx,    y+ly, 0,    90, fill  % bottom right
if !fill
  GR.LINE r1, x+c/2, y,     x+lx-c/2,y          % left
  GR.LINE r2, x,     y+c/2, x,       y+ly-c/2   % up
  GR.LINE r3, x+c/2, y+ly,  x+lx-c/2,y+ly       % right
  GR.LINE r4, x+lx,  y+c/2, x+lx,    y+ly-c/2   % down
else
  c/=2.5
  gr.rect nul, x+c, y, x+lx-c, y+ly
  gr.rect nul, x, y+c, x+lx, y+ly-c
endif
FN.END

GR.OPEN 255,0,0,0,0,1    % noir
gr.screen w,h
scx =800
scy =1280
sx  =w/scx
sy  =h/scy
gr.scale sx,sy

File.Root tmp$
IstandAlone =Is_In("rfo-basic",tmp$)  % to know if it's an APK for the way to exit

path$     =""
if IstandAlone then path$ ="Amazing_Lab/"
filename$ ="Amazing.sav"
ARRAY.LOAD diX[] ,0 ,1 ,0 ,-1    % 1 =Nord  2 =Est  3 =Sud  4 =Ouest
ARRAY.LOAD diY[] ,-1,0 ,1 ,0
DIM tm[5], tmp[5], to[12], ob[16], obtc[16], obcc[2], oc[2], obr[2], obj[2], nobtc[2], bpion[5]
DIM c[7,7,5], cr[7,7], vc[7,7], clr$[5], fl[4], jpt[2], pion[2], jx[2], jy[2], mp[2], fle[12]
ptr =1  % pointeur tuile
tui =2  % n° de tuile 1 à 12
che =3  % valeur du chemin (a mix of 1 2 4 8)
pob =4  % pointeur de l'éventuel objet
nob =5  % n° de l'objet 1 à 16
fd  =2  % 0 to 3 screen background
tu  =0  % 0 to 6 tiles background
p1  =1  % 0 to 4 player 1 color
p2  =2  % 0 to 4 player 2 color
ts  =108   % a tile size (square)
chgtu =-1
chgfd =-1
GOSUB CrHelp

DO
 FIN =0 : fin$ ="" : insert =1 : player =1 : obcc[1] =1 : obcc[2] =9
 GOSUB InitScreen : new =0

 DO                   %  -------- principal game loop ---------
  gr.modify mess, "text", word$("Destination ?;Please insert the mobile tile...",insert+1,";")
  if obcc[player]=9+8*(player=2) & xy$(jx[player],jy[player])<>word$("11 77",player)
    gr.modify mess, "text", "Please insert the tile to return to home "+int$(player)+"..."
  endif
  if FIN then gr.modify mess, "text", fin$
  do
    gr.touch touched, x, y
    if !background() then gr.render
  until touched | new | quit
  if new | quit then D_U.break
  if help then help =0 : gr.hide bhelp : d_u.continue
  x/=sx : y/=sy
  tx =int((x-pgx)/ts)+1   % convert in cells
  ty =int((y-pgy)/ts)+1

  do
    gr.touch touched, x, y
  until !touched
  x/=sx : y/=sy
  cx =int((x-pgx)/ts)+1   % convert in cells
  cy =int((y-pgy)/ts)+1

  if cx=5 & (cy=9 | cy=10) & !help        %  display the help screen
    help =1 : gr.show bhelp : LastPtr(bhelp) 
    d_u.continue
    
  elseif cx<3 & (cy=9 | cy=10)       % to change pawn's color.
    if cy =9
      p1+=1-5*(p1=5) : if p1=p2 then p1+=1-5*(p1=5)
      gr.modify pion[1], "bitmap", bpion[p1] : gr.modify jpt[1], "bitmap", bpion[p1] 
    elseif cy =10
      p2+=1-5*(p2=5) : if p2=p1 then p2+=1-5*(p2=5)
      gr.modify pion[2], "bitmap", bpion[p2] : gr.modify jpt[2], "bitmap", bpion[p2]     
    endif

  elseif y<ts & (cx=1 | cx=7)        % change tiles or screen backgrounds...
    GOSUB SaveGame : chgfd =-1 : chgtu =-1
    if cx=7 then chgtu =tu+1-7*(tu=6) else chgfd =fd+1-4*(fd=3)
    GOSUB InitScreen
    
  endif


  if cx & cy & cx<8 & cy<8 & !FIN     %  into the gameboard
    if insert
      GOSUB insertion   % use tx,ty & cx,cy
      if se
        if se=1 | se=3
          nx =cx : ny =val(mid$("0_8",se,1))
        elseif se=2 | se=4
          nx =val(mid$("_8_0",se,1)) : ny =cy
        endif
        move(tm[ptr], pgx+(nx-1)*ts, pgy+(ny-1)*ts, 2)   %  glisser la tuile en position d'insertion.
        if tm[pob] then move(tm[pob], pgx+(cx-0.5)*ts, pgy+(cy-0.5)*ts, 1)
        GOSUB slide
        STARTX =jx[player] : STARTY =jy[player]
        GOSUB linked      % all linked cells...
        GOSUB ShowLinked
        insert =1-insert
      endif
    else
      if IS_IN(xy$(cx,cy), chf$)
        GOSUB Go : insert =1-insert
        if !FIN
          player =3-player         % change player
          LastPtr( jpt[player] ) : LastPtr( cache ) : gr.move cache,,ts*(player=1)-ts*(player=2)
          
          GOSUB AI
          
        endif
      endif
    endif

  elseif cx=7 & (cy=9 | cy=10) & insert      % touch the 'tile to be inserted' to rotate it clockwise...
    tm[tui]++
    if tm[tui]=5 | tm[tui]=9 | tm[tui]=13 then tm[tui] =val(mid$("....1...5...9",tm[tui],1))
    tm[che] =val(word$("11 7 14 13 3 6 12 9 10 5 10 5",tm[tui]))
    gr.modify tm[ptr], "bitmap", to[tm[tui]]

  endif
  if FIN then POPUP"  Bravo!!! player "+int$(player)+" win...!  "
 UNTIL new
UNTIL quit
GOSUB SaveGame
if IstandAlone then END "Bye...!" else EXIT

OnBackKey:
  Dialog.message win$,"       What do you want to do?", ok, " Exit ", " New Game ", " Cancel "
  new =(ok=1 | ok=2) : quit =(ok=1)
Back.Resume

AI:

RETURN

Go:
TARGX =cx : TARGY =cy : GOSUB PathFinder     % the shorter path from STARTX,STARTY to TARGX,TARGY
for cy =1 to 7 :for cx =1 to 7 :gr.hide cr[cx,cy] :next :next
lc  =len(dest$)/3
pas =4
dp  =ts/pas
LastPtr( jpt[player] )
for c=lc-1 to 1 step -1
  cc$ =word$(dest$,c)
  dx =xfc(cc$) : dy =yfc(cc$)
  cc$ =word$(dest$,c+1)
  ax =xfc(cc$) : ay =yfc(cc$)
  if dx=ax          % vertical
    se =1+2*(dy>ay)
  elseif dy=ay      % horizontal
    se =2+2*(dx<=ax)
  endif
  for pp=1 to pas
    gr.move jpt[player], diX[se]*dp, diY[se]*dp : gr.render
  next
next
for cy =1 to 7 :for cx =1 to 7 :gr.hide cr[cx,cy] :next :next
jx[player] =TARGX : jy[player] =TARGY
if obcc[1]=9 & jx[1]=1 & jy[1]=1 | obcc[2]=17 & jx[2]=7 & jy[2]=7     % end of this game
  FIN =1 : fin$ ="  Bravo!!! player "+int$(player)+" win...!  "
else
  if c[TARGX,TARGY,nob] =obcc[player] then GOSUB recup
endif
RETURN

recup:
pox =pgx+182 : poy =scy-5-((player=1)+0.9)*ts : obcc[player]++
move(c[TARGX,TARGY,pob], pox, poy, 3) : gr.hide c[TARGX,TARGY,pob] : c[TARGX,TARGY,nob] =0
gr.modify nobtc[player], "text", int$(8*(player=2)+9-obcc[player])    % nbr of items to collect.
if obcc[player]<=(8*(player=2)+8)
  gr.modify obj[player], "bitmap", ob[obcc[player]]    % show the new item to collect.
else
  clr("60,0,0,0,1") : gr.circle nul, pgx+210, scy-((player=1)+0.7)*ts, 25
  gr.hide obj[player] : clr("255,0,0,255,2") : gr.text.size 46
  gr.text.draw nul, pgx+210, scy-((player=1)+0.55)*ts, int$(player)    % player must return to home.
endif
RETURN

insertion:    % give 'se' with tx,ty & cx,cy
se =0
if cx=tx & cy=ty
  p =IS_IN(xy$(tx,ty),"21 41 61 72 74 76 27 47 67 12 14 16")   % possibles slots
  if p then se =int(p/9)+1   %  se = 1 north, 2 east, 3 south, 4 west.
elseif tx=cx & !mod(tx,2)    %  vertical
  se =1+2*(ty>cy)  : cy =1+6*(se=3)
elseif ty=cy & !mod(ty,2)    %  horizontal
  se =2+2*(tx<=cx) : cx =1+6*(se=2)
endif
c$ =xy$(cx,cy)
if se
  if c$<>oldse$
    oldse  =IS_IN(c$,"21 41 61 72 74 76 27 47 67 12 14 16")
    oldse$ =mid$("27 47 67 12 14 16 21 41 61 72 74 76",oldse,2)
    tohide =val(word$("7 8 9 10 11 12 1 2 3 4 5 6",int(oldse/3)+1))
    for fl=1 to 12 : gr.show fle[fl] : next
    gr.hide fle[tohide]
  else
    popup " Reverse of the precedent move is forbidden! " : se =0
  endif
endif
return

slide:
se =val(mid$("3412",se,1))
d  =1-2*(se=1|se=4)  % direction
vs =1+6*(se=1|se=4)  % départ
ve =1+6*(vs=1)       % arrivée
pas =6 : array.fill mp[], 0
dp =ts/pas
for m=1 to pas
  dx =cx : dy =cy
  gr.move tm[ptr], d*dp*(se=2|se=4), d*dp*(se=1|se=3)
  for t=vs to ve step d
    ldx =dx : ldy =dy
    gr.move c[dx,dy,ptr], d*dp*(se=2|se=4), d*dp*(se=1|se=3)
    if c[dx,dy,nob] then gr.move c[dx,dy,pob], d*dp*(se=2|se=4), d*dp*(se=1|se=3)
    if jx[1]=dx & jy[1]=dy then gr.move jpt[1], d*dp*(se=2|se=4), d*dp*(se=1|se=3) :mp[1] =1
    if jx[2]=dx & jy[2]=dy then gr.move jpt[2], d*dp*(se=2|se=4), d*dp*(se=1|se=3) :mp[2] =1
    dx+=dix[se] : dy+=diy[se]
  next
  gr.render
next
for j=1 to 2   % move a player to the other side if he's ejected from the gameboard.
  if mp[j]
    jx[j]+=diX[se] : jy[j]+=diY[se]
    jx[j] =jx[j]-7*(jx[j]=8)+7*(jx[j]=0)
    jy[j] =jy[j]-7*(jy[j]=8)+7*(jy[j]=0)
    move(jpt[j], pgx+(jx[j]-1)*ts+20+10*(j=2), pgy+(jy[j]-1)*ts, 2)
  endif
next
for m=1 to 5 : tmp[m] =c[ldx,ldy,m] : next  % la tuile qui sort.
d =-d : se =val(mid$("3412",se,1))          % maj des variables tableaux
dx =ldx : dy =ldy
vs =ve : ve =6*(ve=1)+2*(ve=7)
for t=vs to ve step d
  for m=1 to 5 : swap c[dx,dy,m], c[dx+diX[se], dy+diY[se], m] : next
  dx+=diX[se] : dy+=diY[se]
next
for m=1 to 5 : c[cx,cy,m] =tm[m] : tm[m] =tmp[m] : next     % maj tuile insérée & la mobile.
move(tm[ptr], pgx+5.9*ts, scy-1.9*ts, 2)
if tm[pob] then move(tm[pob], pgx+6.4*ts, scy-1.4*ts, 1)
RETURN

linked:
chf$ =xy$(STARTX,STARTY)+" "
nc   =1
do
  n$ =word$(chf$,nc)
  if len(n$)
    xc =xfc(n$) : yc =yfc(n$)
    for i=1 to 4    % 1=North, 2=Est, 3=South, 4=West
      Xnext =xc+diX[i] : Ynext =yc+diY[i]     % neighboring cell in the direction i.
      if Xnext & Ynext & Xnext<8 & Ynext<8    % stay into the gameboard
        d =val(mid$("3412",i,1))              % the opposite direction of i.
        if BAND(c[xc,yc,che],POW(2,i-1)) & BAND(c[Xnext,Ynext,che],POW(2,d-1))     % can go in 'i' direction.
          cc$ =xy$(Xnext,Ynext) : if !IS_IN(cc$,chf$) then chf$+=cc$+" "    % make the string of linked cells.
        endif
      endif
    next
    nc++
  endif
until n$=""
return

ShowLinked:
for y=1 to 7
  for x=1 to 7
    if IS_IN(xy$(x,y),chf$) then gr.show cr[x,y] : LastPtr(cr[x,y])
  next
next
return

InitScreen:   % create tiles bitmaps etc...
GR.CLS
pc  =ts/3
cc  =ts/2
pgx =(scx-7*ts)/2
pgy =160
clr$[1] ="255, 255, 255, 212, 1"  % ivoire
clr$[2] ="255, 0, 220, 20, 2"     % light green
fe  =0
if !new
  File.exists fe, path$+filename$
  if fe then GOSUB LoadGame
endif

if chgtu>-1 then tu =chgtu  % chg tiles background
if chgfd>-1 then fd =chgfd  % chg screen background

if !bmps then gr.bitmap.load bmps, path$+"Amazing_Lab_bmp.png"
if tuile then gr.bitmap.delete tuile
gr.bitmap.crop tuile, bmps, tu*109,102, 108,108

for t=1 to 12
  if to[t] then gr.bitmap.delete to[t]
next

gr.bitmap.create to[1], ts, ts    %  T             Tiles
gr.bitmap.drawinto.start to[1]
gr.bitmap.draw nul, tuile, 0,0
clr( clr$[1] )                    % ivory
RondRect(0,pc, 10, ts,pc, 1)
RondRect(pc,0, 10, pc,pc, 1)

for s=1 to 3
  gr.bitmap.create to[1+s], ts, ts    %  T   rotated
  gr.bitmap.drawinto.start to[1+s]
  gr.rotate.start s*90, cc, cc
  gr.bitmap.draw nul, to[1], 0, 0
  gr.rotate.end
next

gr.bitmap.create to[5], ts, ts    %  L
gr.bitmap.drawinto.start to[5]
gr.bitmap.draw nul, tuile, 0,0
clr( clr$[1] )  % ivory
RondRect(pc,0, 10, pc,pc, 1)
RondRect(pc,pc, 10, 2*pc,pc, 1)

for s=1 to 3
  gr.bitmap.create to[5+s], ts, ts    %  L    rotated
  gr.bitmap.drawinto.start to[5+s]
  gr.rotate.start s*90, cc, cc
  gr.bitmap.draw nul, to[5], 0, 0
  gr.rotate.end
next

gr.bitmap.create to[9], ts, ts    %  ---
gr.bitmap.drawinto.start to[9]
gr.bitmap.draw nul, tuile, 0,0
clr( clr$[1] )  % ivory
RondRect(0,pc, 10, ts,pc, 1)

for s=1 to 3
  gr.bitmap.create to[9+s], ts, ts    %  ---  rotated
  gr.bitmap.drawinto.start to[9+s]
  gr.rotate.start s*90, cc, cc
  gr.bitmap.draw nul, to[9], 0, 0
  gr.rotate.end
next

gr.bitmap.create cache, 330, 85    % to show for who is the turn to play...
gr.bitmap.drawinto.start cache
clr( "255,0,0,0,1" )
RondRect(0,0, 50, 330,85, 1)
gr.bitmap.drawinto.end

if fond then gr.bitmap.delete fond
gr.bitmap.crop fond, bmps, 0, 213+fd*330, 800,330       % screen background
for fnd=1 to 5 : gr.bitmap.draw nul, fond, 0, (fnd-1)*300 : next

gr.set.stroke 4
gr.text.align 2
gr.text.size 60
clr( clr$[2] )  % light green
gr.text.draw nul, scx/2, 80, "AMAZING  LABYRINTH"

for o=1 to 16
  if ob[o] then gr.bitmap.delete ob[o]
next
for o=1 to 8
  gr.bitmap.crop ob[o],   bmps, (o-1)*50,  0, 50, 50    % objects bitmaps
  gr.bitmap.crop ob[8+o], bmps, (o-1)*50, 50, 50, 50
next
for o=1 to 16 : gr.bitmap.scale ob[o], ob[o], 55, 55 : next   % resized...

for f=1 to 4
  if fl[f] then gr.bitmap.delete fl[f]
next
gr.bitmap.crop fl[1], bmps, 647, 2, 49, 11     % insertion yellow arrows
gr.bitmap.crop fl[2], bmps, 700, 1, 11, 49
gr.bitmap.crop fl[3], bmps, 647,38, 49, 11
gr.bitmap.crop fl[4], bmps, 634, 1, 11, 49
if pion1 then gr.bitmap.delete pion1
if pion2 then gr.bitmap.delete pion2
for p=1 to 5 : gr.bitmap.crop bpion[p], bmps, 400+(p-1)*42, 0, 40,70 : next   % 5 colored pawns.
gr.bitmap.crop flrot, bmps, 728,0, 44,100
gr.bitmap.delete bmps : bmps =0

if !fe
  array.fill c[],0
  ca$ ="11 71 77 17 31 51 73 75 37 57 13 15 33 53 35 55"   % the 16 fix tiles
  vc$ ="6785334411222314"
  for t=1 to 16
    x =xfc(word$(ca$,t)) : y =yfc(word$(ca$,t)) : c[x,y,tui] =val(mid$(vc$,t,1))
    gr.bitmap.draw c[x,y,ptr], to[c[x,y,tui]], pgx+(x-1)*ts, pgy+(y-1)*ts 
    c[x,y,che] =val(word$("11 7 14 13 3 6 12 9 10 5 10 5",c[x,y,tui]))
  next
  for t=1 to 33     %  7 x 'T'   +   12 x '--'   +   15 x 'L'   = 33 + 1 tiles to insert...
    do : x =int(rnd()*7)+1 : y =int(rnd()*7)+1 : until !c[x,y,ptr]
    p  =val(mid$("1111111555555555555999999999999999",t,1))
    c[x,y,tui] =p+int(rnd()*4)     % n° of the tile (1 - 12)
    gr.bitmap.draw c[x,y,ptr], to[ c[x,y,tui] ], pgx+(x-1)*ts, pgy+(y-1)*ts
    c[x,y,che] =val(word$("11 7 14 13 3 6 12 9 10 5 10 5", c[x,y,tui] ))     % cell value for path... (mix of 1 2 4 8)
  next
  array.shuffle ob[]
  for f=1 to 16 
    do
      x =int(rnd()*7)+1 : y =int(rnd()*7)+1 : c =10*x+y     %  place items to collect on the gameboard.
    until c<>11 & c<>71 & c<>77 & c<>17 & !c[x,y,nob]
    c[x,y,nob] =f : gr.bitmap.draw c[x,y,pob], ob[f], pgx-5+(x-0.5)*ts, pgy-5+(y-0.5)*ts
  next
else
  for y=1 to 7
    for x=1 to 7
      gr.bitmap.draw c[x,y,ptr], to[ c[x,y,tui] ], pgx+(x-1)*ts, pgy+(y-1)*ts
      c[x,y,che] =val(word$("11 7 14 13 3 6 12 9 10 5 10 5", c[x,y,tui] ))   % cell value for path... (mix of 1 2 4 8)
      if c[x,y,nob] then gr.bitmap.draw c[x,y,pob], ob[c[x,y,nob]], pgx-5+(x-0.5)*ts, pgy-5+(y-0.5)*ts
    next
  next
endif

clr("90, 255, 255, 212, 1")   % transparency
gr.circle nul, pgx+6.4*ts, scy-1.4*ts, 0.85*ts
clr("255, 255, 255, 212, 1")

if !fe then tm[tui] =int(rnd()*12)+1
tm[che] =val(word$("11 7 14 13 3 6 12 9 10 5 10 5",tm[tui]))

gr.bitmap.draw tm[ptr], to[tm[tui]], pgx+5.9*ts, scy-1.9*ts    % the tile to insert...
if tm[nob] then gr.bitmap.draw tm[pob], ob[tm[nob]], pgx+6.4*ts, scy-1.4*ts

clr("255,255,255,255,0") : n =0
for se =1 to 4           %   the 12 arrows to show places of insertion.
  if se=1 | se=3
    for i=1 to 3
      n++ : gr.bitmap.draw fle[n], fl[se], pgx-ts+25+i*2*ts, pgy-5+15*diY[se]+7*ts*(se=3)
    next
  else
    for i=1 to 3
      n++ : gr.bitmap.draw fle[n], fl[se], pgx-5+15*diX[se]+7*ts*(se=2), pgy-ts+25+i*2*ts
    next
  endif
next

gr.set.stroke 3
gr.text.size 44
for j=1 to 2
  clr("50,0,0,0,1")
  gr.circle nul, pgx+(6*(j=2)+0.5)*ts, pgy+(6*(j=2)+0.5)*ts, ts/3
  clr("255, 0, 0, 255, 2")
  gr.text.draw nul, pgx+(6*(j=2)+0.5)*ts, pgy+(6*(j=2)+0.66)*ts, int$(j)   %  player's home...
next

if !fe then jx[1] =1 : jy[1] =1 : jx[2] =7 : jy[2] =7
gr.bitmap.draw jpt[1], bpion[p1], pgx+(jx[1]-1)*ts+15, pgy+(jy[1]-1)*ts     % put players on the game board.
gr.bitmap.draw jpt[2], bpion[p2], pgx+(jx[2]-1)*ts+35, pgy+(jy[2]-1)*ts

clr("20, 0, 255, 0, 1")
for y=1 to 7           % transparent circles to show linked cells...
  for x=1 to 7
    gr.circle cr[x,y], pgx+(x-0.5)*ts, pgy+(y-0.5)*ts, ts/2 : gr.hide cr[x,y]
  next
next

clr("255,250,255,190,1")   % ivoire
gr.bitmap.draw pion[1], bpion[p1], 85, scy-2*ts   % pawn 1
gr.bitmap.draw pion[2], bpion[p2], 85, scy-1*ts   % pawn 2
gr.circle nul, pgx+210, scy-1.7*ts, 35   % for player1's object to collect
gr.circle nul, pgx+210, scy-0.7*ts, 35   % for player2's object to collect
clr("60,0,0,255,1")
gr.circle nul, 352, scy-1.65*ts, ts/3
gr.circle nul, 352, scy-0.65*ts, ts/3
clr("255, 255, 255, 212, 2")
if obcc[1]<9  then gr.bitmap.draw obj[1], ob[obcc[1]], pgx+182, scy-28-1.7*ts    % actual object to collect.
if obcc[2]<17 then gr.bitmap.draw obj[2], ob[obcc[2]], pgx+182, scy-28-0.7*ts
gr.set.stroke 2
gr.text.size 50
gr.text.draw nobtc[1], 350, scy-1.5*ts, int$(9-obcc[1])
gr.text.draw nobtc[2], 350, scy-0.5*ts, int$(17-obcc[2])

gr.bitmap.draw nul, flrot, 5.48*ts, 10*ts

clr( "170,255,255,255,1" )    % set transparency for the rect to show who is playing.
gr.bitmap.draw cache, cache, 70, scy-(1.1+(player=2))*ts

clr( "90, 255, 255, 212, 1")   % transparency
gr.circle nul, 40, 62, 22
gr.circle nul, scx-40, 62, 22
gr.circle nul, pgx+4.4*ts, pgy+9.2*ts, 35

clr( "255,255,255,0,1" )      % yellow text.
gr.text.size 36 : gr.text.draw mess, scx/2, pgy+7.8*ts, ""

gr.text.size 60 : gr.text.draw nul, pgx+4.4*ts, pgy+9.4*ts, "?"
gr.bitmap.draw bhelp, bmphelp, pgx, pgy : gr.hide bhelp
RETURN

PathFinder:         %  ************ RECHERCHE du PLUS COURT CHEMIN *************
xc  =STARTX  % case courante
yc  =STARTY
ppp =0   % distance pour arriver là du parent... ici 0 puisque c'est la case de départ !
success =0
!         la case       son parent         ppp         son cout (sur 3 chiffres)
 lf$ = "_"+right$(format$("%%",STARTX),2)+right$(format$("%%",STARTY),2)~
          +right$(format$("%%",STARTX),2)+right$(format$("%%",STARTY),2)  % liste fermée
 lo$ = ""    % liste ouverte

 DO
   FOR i=1 to 4          % check all 4 directions
     Xnext = xc + diX[i]    % coordonnée de la case voisine dans la direction i.
     Ynext = yc + diY[i]
     if Xnext & Ynext & Xnext<8 & Ynext<8
     d =val(mid$("3412",i,1))
     if BAND(c[xc,yc,che],POW(2,i-1)) & BAND(c[Xnext,Ynext,che],POW(2,d-1))   % can go in 'i' direction.
       ccc = ppp + abs(TARGY-Ynext) + abs(TARGX-Xnext)    % Calcul du Cout de cette case voisine.
       ct$ = "_"+ right$(format$("%%",Xnext),2)+ right$(format$("%%",Ynext),2)  % case à rechercher...
       if Ynext = TARGY & Xnext = TARGX   % arrivée...
          lf$ = lf$ + ct$ +right$(format$("%%",xc),2)+right$(format$("%%",yc),2)  % liste fermée
          success = 1
          F_n.break
       else
          if Is_In(ct$, lf$) = 0   % si la case n'est PAS dans la liste fermée ...
            slo = Is_In(ct$, lo$)  % on la cherche dans la Liste Ouverte lo$
            if slo = 0     % si la case n'est pas dans la liste ouverte : on l'y met et c'est tout...
              lo$= lo$ +ct$ + right$(format$("%%",xc),2)+ right$(format$("%%",yc),2)~
                   + right$(format$("%%%",ppp),3)+ right$(format$("%%%",ccc),3)   % liste ouverte
            else
               ! si elle y est : comparer les 2 coûts
              cclo = val(mid$(lo$, slo + 12, 3))   % cout de cette case déjà présente dans lo$
              if ccc < cclo   % si le cout actuel est inférieur à celui de cette case trouvée dans lo$ :
       ! mise à jour des données de cette case dans la liste ouverte : " parent ppp et cout"
                lt$ = left$(lo$,slo+4)   % partie gauche de lo$ y compris les XXYY de la case à mettre à jour
                lt$ = lt$ + right$(format$("%%",xc),2) + right$(format$("%%",yc),2)~
                          + right$(format$("%%%",ppp),3) + right$(format$("%%%",ccc),3) % mise à jour du parent + cout
                lo$ = lt$ + right$(lo$,len(lo$)-len(lt$))     % reconstruction de lo$
              endif
            endif
          endif
        endif
      endif
      endif
    NEXT
    if success then D_u.break
    nclo = len(lo$)/15  % nbre total de cases dans lo$ (liste ouverte)  (rappel: 1 case = 15 caractères)

    if nclo = 0         % si liste ouverte vide = pas de solution... on arrête.
       D_u.break

    elseif nclo = 1     % s'il n'y a qu'une case dans lo$ alors c'est celle là qui est sélectionnée bien sur...!
       ncppc = 1

    else                % localise dans lo$ le n° de la case qui a le plus petit cout... = ncppc
       mc = val(right$(lo$,3))  % cout référence = celui de la DERNIERE case enregistrée dans lo$
       ncppc = nclo
       for n = nclo to 1 step -1   % puis on remonte case par case dans lo$ en partant de la fin...
         mc2 = val(mid$(lo$, n*15-2, 3))  % cout de la case précédente dans lo$
         if mc2 < mc    % cout inférieur trouvé...
            mc = mc2
            ncppc = n   % ncppc = localisation (base 15) dans lo$ de la case qui a le plus petit cout.
         endif
       next
    endif

   lt$ = mid$(lo$,(ncppc-1)*15+1,15)  % la case choisie dans lo$ est mise dans une chaine temporaire.
   xc  = val(mid$(lt$,2,2))  % devient la nouvelle case courante, (et donc le nouveau parent...)
   yc  = val(mid$(lt$,4,2))
   ppp = val(mid$(lt$,10,3)) + 1  % nouveau cout pour venir jusqu'ici (dans le chemin)

   lf$ = lf$ + left$(lt$,9)       % ajoutée à la liste fermée  (constitution du chemin)

    ! retrait de cette case de lo$ (liste ouverte) :
   lt$ = left$(lo$,(ncppc-1)*15)  % partie gauche de lo$ avant la case choisie
   rst = nclo - (len(lt$)/15+1)
   lo$ = lt$ + right$(lo$,rst*15)  % reconstitution de la Liste Ouverte

 UNTIL success>0   % --------------  FIN de la RECHERCHE du PLUS COURT CHEMIN  ---------------

 dest$ =""
 if success
   pl =0
   i  =Is_In("_"+right$(format$("%%",Xnext),2)+right$(format$("%%",Ynext),2), lf$)
   while i > 1
     ax = val(mid$(lf$,i+5,2))
     ay = val(mid$(lf$,i+7,2))
     i = Is_In("_"+right$(format$("%%",ax),2)+right$(format$("%%",ay),2), lf$)
     dest$+=xy$(ax,ay)+" " : pl++
   Repeat
   dest$ =xy$(TARGX,TARGY)+" "+dest$
 endif
RETURN

CrHelp:
DIM h$[12]
h$[1] ="You must collect all your 8 treasures and"
h$[2] =" return to your home."
h$[3] ="To do that mission, there's a free tile at"
h$[4] =" the bottom right of the screen."
h$[5] ="Touch it to rotate it if you want."
h$[6] ="You must insert that tile into the gameboard"
h$[7] =" but only by the places where you can see"
h$[8] =" smalls yellow arrows."
h$[9] ="Then you can move your pawn trying to collect"
h$[10]=" the treasure shown for you at the bottom"
h$[11]=" left of the screen."
h$[12]="That's all..."
gr.bitmap.create bmphelp, 7*ts, 7*ts
gr.bitmap.drawinto.start bmphelp
gr.color 255,162,80,15,2
RondRect(0,0, 30, 7*ts, 7*ts, 2)
gr.color 255,255,255,0,2
gr.set.stroke 1 : gr.text.size 36
gr.text.draw nul, 2.7*ts, 60, "Your goal"
for li=1 to 12 : gr.text.draw nul, 12, 90+li*50, h$[li] : next
gr.text.draw nul, 5*ts, 90+li*50, "have fun!"
gr.bitmap.drawinto.end
array.delete h$[]
return

LoadGame:
array.fill c[],0 : array.fill tm[],0
grabfile ldg$, path$+filename$
split ln$[], ldg$, "\n"
array.length nl, ln$[]
if nl<>20 then popup " BAD FILE... " : fe =0 : return
fd =val(ln$[1])  % screen background
tu =val(ln$[2])  % tiles background
p1 =val(ln$[3])  % pawn1 color
p2 =val(ln$[4])  % pawn2 color
for y=1 to 7
  n =0
  for x=1 to 7
    n++ : c[x,y,tui] =val(word$(ln$[4+y],n))
  next
next
if len(ln$[12])   % the objects
  n =0
  do
    n++ : o$ =word$(ln$[12],n)
    if len(o$) then x =val(mid$(o$,1,1)) : y =val(mid$(o$,2,1)) : c[x,y,nob] =val(mid$(o$,3))
  until o$=""
endif
jx[1] =val(mid$(ln$[13],1,1)) : jy[1] =val(mid$(ln$[13],2))
jx[2] =val(mid$(ln$[14],1,1)) : jy[2] =val(mid$(ln$[14],2))
obcc[1] =val(ln$[15]) : obcc[2] =val(ln$[16])
tm[tui] =val(ln$[17]) : tm[nob] =val(ln$[18])
FIN     =val(left$(ln$[19],1)) : if FIN then fin$ =mid$(ln$[19],3)
player  =val(ln$[20])
array.delete ln$[]
RETURN

SaveGame:
CLS
?fd     % screen background
?tu     % tiles background
?p1     % player 1 color
?p2     % player 2 color
obj$ =""
for y=1 to 7     % the game board
  t$ =""
  for x=1 to 7
    t$+=int$(c[x,y,tui])+" "
    if c[x,y,nob] then obj$+=int$(10*x+y)+int$(c[x,y,nob])+" "
  next
  print t$
next
print obj$       % where are items on the gameboard.
?xy$(jx[1],jy[1])  % players positions
?xy$(jx[2],jy[2])
?obcc[1]    % actual number of items to collect by players.
?obcc[2]
?tm[tui]    % the mobile tile
?tm[nob]
?int$(FIN)+" "+fin$
?player
Console.Save path$+filename$
pause 200
CLS
RETURN
