REM 8 Américain ou Crazy Eight  @Cassiope34 0217

Fn.def clc(n,ncp)    % card color   1=trèfle, 2=pique, 3=coeur, 4=carreau
 Fn.rtn int(n/(8+0.1+5*(ncp=52)))+1
Fn.end

Fn.def vlc(n,ncp)    % card value
 d =8+5*(ncp=52) : noc =mod(n,d) : noc+=d*(noc=0)
 if ncp=32 then Fn.rtn val(word$("1 7 8 9 10 11 12 13",noc)) else Fn.rtn noc
Fn.end

Fn.def LastPtr(ptr)  % put the graphic pointer 'ptr' UP to all other.
  gr.getDL ndl[],1
  array.length sz, ndl[]
  if !ptr | sz=1 | ndl[sz] =ptr then array.delete ndl[] : Fn.rtn 0
  array.search ndl[],ptr,n
  if n
    for nn=n to sz-1 : ndl[nn] =ndl[nn+1] : next
    ndl[sz] =ptr : gr.newDL ndl[]
  endif
  array.delete ndl[]
Fn.end

Fn.def move(ptr, arx, ary, speed)   % move smoothly the bitmap 'ptr' to arx,ary
LastPtr( ptr )
if speed>1
do
  gr.get.position ptr,dpx,dpy
  ix =int((dpx-arx)/speed)
  iy =int((dpy-ary)/speed)
  if abs(ix)<3 & abs(iy)<3 then D_u.break
  gr.modify ptr, "x",dpx-ix, "y",dpy-iy
  gr.render
until 0
endif
gr.modify ptr, "x",arx, "y",ary
gr.render
Fn.end

Fn.def move2(ptr, arx, ary, pas)   % move smoothly the bitmap 'ptr' to arx,ary
LastPtr( ptr )
if pas>1
  gr.get.position ptr,dpx,dpy
  ex =int((arx-dpx)/pas) : ey =int((ary-dpy)/pas)
  for e=1 to pas-1 : gr.move ptr, ex, ey : gr.render : next
endif
gr.modify ptr, "x",arx, "y",ary : gr.render
Fn.end

FN.DEF RectR(x,y, c, lx,ly, fill)
GR.ARC nul, x, y, x+c, y+c, -90, -90, fill     % top left
GR.ARC nul, x+lx-c,y, x+lx, y+c, -90, 90, fill   % top right
GR.ARC nul, x, y+ly-c,x+c, y+ly, -180, -90, fill   % bottom left
GR.ARC nul, x+lx-c, y+ly-c, x+lx, y+ly, 0, 90, fill  % bottom right
if !fill
  GR.LINE r1, x+c/2, y, x+lx-c/2, y     % left
  GR.LINE r2, x, y+c/2, x, y+ly-c/2     % up
  GR.LINE r3, x+c/2, y+ly, x+lx-c/2, y+ly  % right
  GR.LINE r4, x+lx, y+c/2, x+lx, y+ly-c/2  % down
else
  c*=0.4
  gr.rect nul, x+c, y, x+lx-c, y+ly
  gr.rect nul, x, y+c, x+lx, y+ly-c
endif
FN.END

gr.open 255,90,140,80,0,0    % green & landscape
gr.screen w,h
scx =1280
scy =800
sx =w/scx : sy =h/scy
gr.scale sx, sy

path$ =""
filename$ ="CrazyEight.sav"
np  =1    % nbre de paquets
ncp =32   % nbre de cartes / paquet
ntc =np*ncp    % nbre total de cartes
DIM bc[ncp], cl[4], coul[4], clb[4], dos[6], pqx[2], pqy[2], np[2], p[ntc,2,2]    % <-- les 2 paquets.
DIM c[20,2,4], bmain[4], main[4], act[3], px[4], py[4], nc[4], pts[4,2], nmj[4], sco[4], sc[4]  % <-- les 4 joueurs
! carte n°, 1=ptr, 2=vlr  (pointeur, valeur), joueur (1 à 4)
ptr =1 : vlr =2 : nob =0 : dcx =120 : dcy =dcx*1.46 : ecrt =0.7*dcx
gr.bitmap.load bmps, path$ + "52_cards.png"
GOSUB BmpMaker

Timer.set 10000    %   set time evry 10s !

start =1 : bdos =5 : dj =2   %  'dj' est le joueur qui distribue.

DO
  GOSUB InitGame
  new =0
  
  DO

    if chgtj then chgtj =0 : j =j+1-4*(j=4) : GOSUB AI       % chgt de joueur

    mv   =0
    crt  =0
    eccx =ecrt
    if nc[j] then gr.get.position c[1,ptr,j], posx, posy

    if !pioche & !chxcoul
      GOSUB DefAction
      if action<3 then pioche =action else pioche =0
    endif

    do
      gr.touch touched,x,y
      if !background() then gr.render
    until touched | quit | new
    if quit | new then D_U.break
    
    x/=sx : y/=sy
    if x>posx & y>py[j] & nc[j] & !chxcoul & !help     % une carte du joueur 3 (bas)
      crt =int((x-posx)/eccx)+1      % son numéro de place... (1,2,3,etc...)
      if crt<=nc[j]+1
        if crt=nc[j]+1 then crt =nc[j]
        gr.get.position c[crt,ptr,j], orx, ory : LastPtr( c[crt,ptr,j] )
        deltaX= x-orx : deltaY= y-ory : mv =1
      endif
      
    elseif x>pqx[1] & x<pqx[1]+dcx & y>pqy[1] & y<pqy[1]+dcy & np[1] & pioche & !chxcoul & !help  %  pioche
      pioch=1
      
    endif
    
    do
      gr.touch touched, x, y
      x/=sx : y/=sy
      if mv then gr.modify c[crt,ptr,j], "x", x-deltaX, "y", y-deltaY : gr.render
    until !touched
    
    if mv
      ok =0 : gr.get.position c[crt,ptr,j], lchx, lchy
      if lchy<(pqy[1]+dcy+10) & action=3              %  carte saisie jouée...
        cc  =clc( p[np[2],vlr,2], ncp)    % la carte sur l'écart.
        vc  =vlc( p[np[2],vlr,2], ncp)
        ccj =clc( c[crt,vlr,j], ncp)      % contrôler la validité de la carte jouée avec 'ok' !
        vcj =vlc( c[crt,vlr,j], ncp)
        ok  =(ob=5 & vcj=1)+(ob>0 & ob<5)*(ccj=ob | vcj=8)+(ob=0)*(ccj=cc | vcj=vc | vcj=8)  % respect des règles.
      endif
      if ok
        gr.modify mess, "text", "" : gr.hide bcli
        GOSUB joue
        if nc[j]=0        %  le joueur n'a plus de carte.
          GOSUB MajScores
          GOSUB BoiteMenu
          D_U.break
        endif
        cc =clc( p[np[2],vlr,2], ncp)
        vc =vlc( p[np[2],vlr,2], ncp)
        if vc=1
          ob =5
        elseif vc=8
          for c=1 to 4 : gr.show coul[c] : next : gr.hide bcli
          gr.modify mess, "text", "asked ?"
          chxcoul =1
          gr.modify act[2], "text", "chooses"
          gr.modify act[3], "text", "a color"
          D_U.continue
        endif
        chgtj =1
        
      ELSE
        move( c[crt,ptr,j], orx, ory, 1.5) : GOSUB Reorg     % retourne à sa place !
        
      endif
    
    elseif x<80 & y>scy-80
      help =1 : LastPtr(bhelp) : gr.show bhelp
      
    elseif pioch           %  en train de prendre une ou 2 cartes...
      GOSUB prend : pioche--
      if action =2
        GOSUB prend : pioche =0 : pioch =0 : ob =0 : chgtj =1
      elseif !pioche
        pioch =0
        GOSUB DefAction    % Est-ce que je peux jouer la carte piochée ?
        if action<>3 then chgtj =1
      endif
      
    elseif chxcoul    %  choix de la couleur aprés un 8.
      dm =85
      gr.get.position coul[1], clx, cly
      if x>clx & x<clx+2*dm & y>cly & y<cly+2*dm     % sélection de couleur
        if x>clx+dm
          if y>cly+dm then ob =4 else ob =2
        else
          if y>cly+dm then ob =3 else ob =1
        endif
        for c=1 to 4 : gr.hide coul[c] : next
        gr.modify bcli, "bitmap", clb[ob] : gr.show bcli
        gr.modify mess, "text", "imposed."
        chxcoul =0 : chgtj =1
      endif
      
    elseif x>px[2]    %  change le dos des cartes... en touchant les cartes du joueur 2.
      bdos =bdos+1-6*(bdos=6)
      for jo=1 to 4
        if jo<>3 then for c=1 to nc[jo] : gr.modify c[c,ptr,jo], "bitmap", dos[bdos] : next
      next
      for c=1 to np[1] : gr.modify p[c,ptr,1], "bitmap", dos[bdos] : next
      
    endif
  until new
UNTIL quit         % --------- exit -----------
GOSUB SaveGame
Timer.Clear
END "Bye...!"

OnTimer:
  Time ,,,h$,m$  :  gr.modify hre, "text", h$+" : "+m$
Timer.Resume

OnBackKey:
  if help
    help =0 : gr.hide bhelp
  else
    GOSUB BoiteMenu
  endif
back.resume

BoiteMenu:
  Dialog.message win$,"       What do you want ?", ok, " Exit ", " We continue ? ", " Cancel "
  new  =(ok=1 | ok=2) : quit =(ok=1)
return

MajScores:
ptref$ ="4 2 3 4 5 6 7 35 9 10 1 2 3"   % les points par carte
for jo =1 to 4
  if nc[jo]
    for c=1 to nc[jo]
      v =vlc(c[c,vlr,jo],ncp) : sc[jo]+=val(word$(ptref$,v))    % additionne les cartes restantes par joueur.
      if sc[jo]>=150 then FIN =jo
    next
    gr.modify sco[jo], "text", int$(sc[jo]) : gr.render
  endif
next
if FIN then POPUP " The player "+int$(FIN)+" has lost... " : array.fill sc[], 0 : FIN =0
return

DefAction:     % défini 'action' en fonction de la carte visible ou d'une couleur imposée 'ob'
! action =1 -> tire une carte
! action =2 -> tire 2 cartes
! action =3 -> peut jouer...
cc =clc( p[np[2],vlr,2], ncp)
vc =vlc( p[np[2],vlr,2], ncp)
action =(ob=5)+1
for n=1 to nc[j]
  ccj =clc( c[n,vlr,j], ncp)      % est-ce qu'une carte du joueur peut aller ?
  vcj =vlc( c[n,vlr,j], ncp)
  if (ob=5 & vcj=1)+(ob>0 & ob<5)*(ccj=ob | vcj=8)+(ob=0)*(ccj=cc | vcj=vc | vcj=8) then action =3 : f_n.break
next
gr.get.value nmj[j], "text", nom$
gr.modify act[1], "text", nom$
if action=1
  gr.modify act[2], "text", "take"
  gr.modify act[3], "text", "1 card"
elseif action=2
  gr.modify act[2], "text", "take"
  gr.modify act[3], "text", "2 cards"
elseif action=3
  gr.modify act[2], "text", "can"
  gr.modify act[3], "text", "play"
endif
gr.render
return

Joue:    %  'j' play the card 'crt'
gr.modify c[crt,ptr,j], "bitmap", bc[c[crt,vlr,j]]
d=np[2]*0.33
move( c[crt,ptr,j], pqx[2]+d, pqy[2]-d, 2)
np[2]++ : swap p[np[2],ptr,2], c[crt,ptr,j] : swap p[np[2],vlr,2], c[crt,vlr,j]
if crt<nc[j]
  for c=crt to nc[j]-1 : swap c[c,ptr,j], c[c+1,ptr,j] : swap c[c,vlr,j], c[c+1,vlr,j] : next   % décale à gauche.
endif
nc[j]-- : GOSUB Reorg
RETURN

Prend:     %  'j' take a new card.
if j=3 then gr.modify p[np[1],ptr,1], "bitmap", bc[p[np[1],vlr,1]]
move2( p[np[1],ptr,1], px[j], py[j], 8)
nc[j]++ : swap p[np[1],ptr,1],c[nc[j],ptr,j] : swap p[np[1],vlr,1],c[nc[j],vlr,j] : np[1]-- : t =0
GOSUB Reorg
if !np[1]     % si paquet est vide alors remplir p[1] avec p[2] sauf la carte visible !
  for c=np[2]-1 to 1 step -1
    np[1]++ : swap p[c,ptr,2], p[np[1],ptr,1] : swap p[c,vlr,2], p[np[1],vlr,1]
    swap p[c,ptr,2], p[c+1,ptr,2] : swap p[c,vlr,2], p[c+1,vlr,2] : np[2]--
    gr.modify p[np[1],ptr,1], "bitmap", dos[bdos], "x", pqx[1]+t, "y", pqy[1]-t : t+=0.4 : LastPtr(p[np[1],ptr,1])
  next
  gr.modify p[np[2],ptr,2], "x", pqx[2], "y", pqy[2]
endif
RETURN

AI:
DO
  for m=1 to 4 : gr.hide main[m] : next : gr.show main[j] : gr.render     %  show who is playing...
  cp$ =""
  if nc[j] % & sc[j]<150
  
    GOSUB DefAction    % les actions possibles.

    if action<3        % doit piocher
      GOSUB prend
      if action=2
        GOSUB prend : ob =0
      else
        GOSUB DefAction : if action=3 then pause 500 : goto enc
      endif

    elseif action=3    % 'j' peut jouer
      enc:
      gr.modify mess, "text", "" : gr.hide bcli
      chc$ ="" : cp =0 : crt =0
      for c=1 to nc[j]
        vcj =vlc( c[c,vlr,j], ncp)
        ccj =clc( c[c,vlr,j], ncp)
        if ob=5
          if vcj=1 then chc$+=right$(int$(100+c),2)+" " : cp++    % AS imposé
        elseif ob>0 & ob<5
          if ccj=ob | vcj=8 then chc$+=right$(int$(100+c),2)+" " : cp++   % couleur ou 8 imposé
        elseif ccj=cc | vcj=vc | vcj=8
          chc$+=right$(int$(100+c),2)+" " : cp++                  % couleur ou valeur imposé
        endif
      next
      if cp
        crr$ ="10 9 7 1 8 13 12 11"    %  ordre de priorité
        if ncp =52 then crr$ ="10 9 7 6 5 4 1 8 3 13 12 11 2"
        if nc[1]<3 | nc[2]<3 | nc[3]<3 | nc[4]<3   % si 1 joueur a - de 3 cartes
          crr$ ="1 8 10 9 7 13 12 11" : if ncp =52 then crr$ ="8 1 10 9 7 6 5 4 3 13 12 11 2"
        endif
        crt =0 : e =0
        do
          e++ : vlre =val(word$(crr$,e))
          for c=1 to cp
            nc =val(word$(chc$,c))
            if vlc(c[nc,vlr,j],ncp) =vlre then crt =nc
          next
        until crt
        ob =0 : vcj =vlc( c[crt,vlr,j], ncp)
        if vcj=1        % impose un AS au joueur suivant.
          ob =5
        elseif vcj=8
          ob =clc(c[crt,vlr,j],ncp)       % la couleur du 8 par défaut...
          vm =0
          for c=1 to nc[j]      % cherche la couleur de la carte la + chère en pts sauf le 8.
            vcj =vlc( c[c,vlr,j], ncp)
            vcc =val(word$("4 2 3 4 5 6 7 35 9 10 1 2 3", vcj))
            if vcc<>35 & vcc>vm then vm =vcc : ob =clc( c[c,vlr,j], ncp)
          next
        endif
        GOSUB joue
        if ob>0 & ob<5           %  couleur imposée...
          gr.modify mess, "text", "imposed."
          gr.modify bcli, "bitmap", clb[ob] : gr.show bcli : gr.render 
        endif
        if nc[j]=0        %  si le joueur n'a plus de carte il gagne cette manche...
          GOSUB MajScores
          GOSUB BoiteMenu
          new =1 : d_u.break
        endif
        
      endif
    endif
  endif
  j =j+1-4*(j=4)
UNTIL j =3
for m=1 to 4 : gr.hide main[m] : next : gr.show main[j] : gr.render
RETURN

Reorg:     % re-affiche les cartes de j.
if !nc[j] then return
if j=1 | j=3
  eccx =ecrt  : eccy =0
  posy =py[j] : posx =(scx-dcx-(nc[j]-1)*eccx)/2     %  centrage vertical
else
  eccy =dcy/4 : eccx =0
  posx =px[j] : posy =(scy-dcy-(nc[j]-1)*eccy)/2     %  centrage horizontal
endif
if j=3 & nc[j]>1      % tri automatique selon une chaine de référence...
  cr$ ="8 1 10 9 7 13 12 11" : if ncp=52 then cr$ ="8 1 10 9 7 6 5 4 3 13 12 2 11"
  p =1
  for r=1 to 8+5*(ncp=52)
    vr =val(word$(cr$,r))
    for c=p to nc[j]
      if vlc( c[c,vlr,j], ncp )=vr then swap c[c,ptr,j], c[p,ptr,j] : swap c[c,vlr,j], c[p,vlr,j] : p++
    next
  next
endif
for c=1 to nc[j] : LastPtr( c[c,ptr,j] ) : gr.modify c[c,ptr,j], "x", posx+(c-1)*eccx, "y", posy+(c-1)*eccy : next
return

BmpMaker:
for c=1 to ncp         %  all cards bitmaps.
  cc =clc(c,ncp) : vc =vlc(c,ncp)
  gr.bitmap.crop nul, bmps, (vc-1)*158, (cc-1)*246, 158, 246
  gr.bitmap.scale bc[c], nul, dcx, dcy : gr.bitmap.delete nul  
next
for d=1 to 6
 gr.bitmap.crop nul, bmps, (d-1)*158, 4*246, 158, 246 : gr.bitmap.scale dos[d], nul, dcx, dcy : gr.bitmap.delete nul
next
for c=1 to 4
  gr.bitmap.crop nul, bmps, 950+108*(c>2), 985+123*(c=2 | c=4), 107, 123  % boutons : trèfle, pique, coeur, carreau
  gr.bitmap.scale clb[c], nul, 80, 80 : gr.bitmap.delete nul
next
for m=1 to 4
  gr.bitmap.crop nul, bmps, 1176+122*(m=2 | m=3), 986+120*(m>2), 120, 120    % les mains
  gr.bitmap.scale bmain[m], nul, 70, 70 : gr.bitmap.delete nul
next
gr.bitmap.delete bmps
GOSUB helpEn
RETURN

InitGame:
GR.CLS
pioche =0 : chxcoul =0
pqx[1] =scx/2-1.1*dcx : pqy[1] =scy/2-0.5*dcy   %  positions des paquets
pqx[2] =scx/2+0.1*dcx : pqy[2] =scy/2-0.5*dcy
px[1] =scx/2-0.5*dcx  : py[1]= 20               %  positions des joueurs
px[2] =scx-1.2*dcx    : py[2]= scy/2-0.5*dcy
px[3] =scx/2-0.5*dcx  : py[3]= scy-dcy-20
px[4] =0.2*dcx        : py[4]= scy/2-0.5*dcy
gr.set.stroke 2 : gr.color 255,0,0,0,0 : gr.text.align 1
RectR( pqx[1]-10, pqy[1]-10, 26, 2*dcx+45, dcy+20, 0)
FOR p=1 TO 4 : RectR( px[p]-10, py[p]-10, 26, dcx+20, dcy+20, 0) : NEXT

for cl=1 to 4     %  affiche les 4 boutons du choix d'une couleur.
  gr.bitmap.draw coul[cl], clb[cl], pqx[1]+2.4*dcx+(cl-1-2*(cl>2))*85, scy/2-100+85*(cl>2)
next
gr.bitmap.draw bcli, coul[3], pqx[2]+dcx+40, scy/2-45    % représente la couleur imposée s'il y en a une...
gr.hide bcli

gr.bitmap.draw main[1], bmain[1], scx/2-30, py[1]+dcy+10   %  la main qui indique le joueur en cours.
gr.bitmap.draw main[2], bmain[2], scx-dcx-110, scy/2-35
gr.bitmap.draw main[3], bmain[3], scx/2-30, py[3]-80
gr.bitmap.draw main[4], bmain[4], dcx+40, scy/2-35

gr.color 255,200,200,200,1 : gr.text.size 30
gr.text.draw nul, px[1]+dcx, py[1]+dcy+60, "score"
gr.text.draw nul, px[2]-110, py[2]+dcy-20, "score"
gr.text.draw nul, px[3]+dcx, py[3]-40, "score"
gr.text.draw nul, px[4]+dcx+40, py[4]+dcy-20, "score"

gr.color 255,255,255,0,2     % jaune
Time ,,,h$,m$ : gr.text.draw hre, scx-120, 40, h$+" : "+m$   % heure

File.Exists fe, path$+filename$   %  en cas de sauvegarde ?
if fe & start
  grabfile ldg$, path$+filename$
  split ln$[], ldg$, "\n"
  dj =val(ln$[1])
  bdos =val(ln$[2])
  for jo=1 to 4 : sc[jo] =val(word$(ln$[3],jo)) : next
  array.delete ln$[]
endif
gr.color 255,255,174,0,1     % orange
gr.text.draw nmj[1], px[1]-110, py[1]+dcy+60, "Player 1"
gr.text.draw nmj[2], px[2]-140, py[2]+30, "Player 2"
gr.text.draw nmj[3], px[3]-110, py[3]-40, "Player 3"
gr.text.draw nmj[4], px[4]+dcx+40, py[4]+30, "Player 4"

gr.color 255,255,255,0,1 : gr.text.size 25
gr.text.draw mess, scx/2+1.35*dcx, pqy[1]+dcy+10, int$(np)+" x "+int$(ncp)+" cards..."

gr.color 255,60,190,255,2 : gr.text.size 54 : gr.text.align 2       % chiffre du score actuel / joueur.
gr.text.draw sco[1], px[1]+2.1*dcx, py[1]+dcy+70, int$(sc[1])
gr.text.draw sco[2], px[2]-74, py[2]+1.3*dcy, int$(sc[2])
gr.text.draw sco[3], px[3]+2.1*dcx, py[3]-30, int$(sc[3])
gr.text.draw sco[4], px[4]+dcx+74, py[4]+1.3*dcy, int$(sc[4])
gr.color 255,60,190,255,0 : gr.text.size 70
gr.text.draw nul, 170, 90, "The 150"
gr.text.draw nul, 50, scy-40, "?"

if start
  start =0 : ch$ =""
  for c=1 to ntc   % fabrique le paquet
    do
      n =int(rnd()*ntc)+1
    until !IS_IN(right$(int$(1000+n),3),ch$)   %  unique...
    ch$+=right$(int$(1000+n),3)+" "
    p[c,vlr,1] =n-ncp*(n>ncp)
  next
else
  for jo=1 to 4   % rassemble les cartes restantes des joueurs sur le paquet 2.
    if nc[jo] then for c=1 to nc[jo] : np[2]++ : swap c[c,vlr,jo], p[np[2],vlr,2] :nc[jo]-- : next
  next
  for c=1 to np[1] : np[2]++ : swap p[c,vlr,1], p[np[2],vlr,2] : next   % met le reste du talon sur l'écart.
  for c=1 to np[2] : p[c,vlr,1] =p[c,vlr,2] : p[c,vlr,2] =0 : next      % rempli le talon avec l'écart...
endif
np[1] =ntc : np[2] =0 : t =0
for c=1 to np[1]
 ! gr.bitmap.draw p[c,ptr,1], bc[p[c,vlr,1]], pqx[1], pqy[1]  % dessine tout le paquet 1 (le talon)
  t+=0.4 : gr.bitmap.draw p[c,ptr,1], dos[bdos], pqx[1]+t, pqy[1]-t : LastPtr(p[c,ptr,1])
next

array.fill c[],0 : array.fill nc[],0
for m=1 to 4 : gr.hide main[m] : next : gr.show main[dj] : gr.render     % indique celui qui distribue.

for c=1 to 6        % distribution de 5 cartes/joueur
  for j=1 to 4
    if j=3 then gr.modify p[np[1],ptr,1], "bitmap", bc[p[np[1],vlr,1]]
    move(p[np[1],ptr,1], px[j], py[j], 1) : nc[j]++
    swap c[nc[j],ptr,j], p[np[1],ptr,1] : swap c[nc[j],vlr,j], p[np[1],vlr,1]
    GOSUB Reorg : np[1]-- : gr.render
  next
next
np[2]++ : move(p[np[1],ptr,1], pqx[2], pqy[2], 3)      %  la carte visible
swap p[np[1],ptr,1], p[np[2],ptr,2] : swap p[np[1],vlr,1], p[np[2],vlr,2] : np[1]--
gr.modify p[np[2],ptr,2], "bitmap", bc[p[np[2],vlr,2]]

gr.color 255,255,255,0,1 : gr.text.size 25     %  texte de l'action à faire...
gr.text.draw act[1], pqx[1]-80, pqy[1]+50,  ""
gr.text.draw act[2], pqx[1]-80, pqy[1]+100, ""
gr.text.draw act[3], pqx[1]-80, pqy[1]+150, ""

gr.bitmap.draw bhelp, bmphelp, 10, 40 : gr.hide bhelp : help =0

gr.modify mess, "text", ""
for c=1 to 4 : gr.hide coul[c] : next
ob =0 : if vlc( p[np[2],vlr,2], ncp)=1 then ob =5
if vlc( p[np[2],vlr,2], ncp)=8         %  couleur du 8 imposée...
  ob =clc(p[np[2],vlr,2], ncp)
  gr.modify mess, "text", "imposed."
  gr.modify bcli, "bitmap", clb[ob] : gr.show bcli : gr.render
endif

j =dj : dj =dj+1-4*(dj=4)
if j=2
  j =3 : for m=1 to 4 : gr.hide main[m] : next : gr.show main[j] : gr.render
else
  chgtj =1
endif
RETURN

helpFr:
 "But du jeu: se débarrasser de toutes ses cartes..."
 "tout joueur qui atteint 150 est eliminé, la partie s’arrête quand il ne reste qu'un joueur en course."
 "Le donneur distribue 5 cartes à chacun une par une (sens horaire), pose le《 talon 》au centre du tapis"
 "puis retourne la première carte face visible à côté du talon pour former《 l’écart 》"
 "le 1er joueur après le donneur (sens horaire) commence à jouer sur《 l’écart 》:"
 "- une carte de même couleur (trefle pique coeur carreau) ou de même valeur (AS Roi ... 7) que celle visible."
 "- s'il ne peut pas jouer, il tire une carte et la joue s'il le peut ou passe son tour."
 "- un 8 peut toujours être joué (sauf sur un AS) et permet d'imposer une couleur de son choix au joueur suivant:"
 "  si celui-ci n'a pas la couleur imposée il peut jouer un 8 ou tirer une carte, puis jouer ou passer."
 "- un AS oblige le joueur suivant (uniquement) à jouer un AS ou à tirer 2 cartes et passer son tour."
 "Si le talon est vide, on forme un nouveau talon en retournant l'écart, sauf la carte visible."
 "Si un joueur n'a plus que 1 ou 2 cartes, il doit l'annoncer « 1 carte » ou « 2 cartes »"
 "Dés qu'un joueur n'a plus de carte, on arrête le jeu et on additionne les points des cartes en main des autres"
 "joueurs de la façon suivante : le 8 =35, les 10, 9 et 7 = leur valeur, AS =4, Roi =3, Dame =2, Valet =1 point."
return

helpEn:
DIM h$[14]
gr.bitmap.create bmphelp, scx-20, scy-80
gr.bitmap.drawinto.start bmphelp
!gr.color 255,60,190,255,2
gr.color 255,152,75,0,2
RectR( 0, 0, 50, scx-20, scy-80, 2)
gr.color 255,255,255,0,1 : gr.text.size 46
li =0 : ecl =41 : gr.text.draw nul,scx/2-130,60, "The 150's rules" : gr.text.size 26
h$[ 1] ="The 150 is a variant of the famous CrazyEight cards game. The goal is to get rid of all your cards..."
h$[ 2] ="The dealer distributes 5 cards one by one to all players and returns the next card visible face."
h$[ 3] ="The dealer will be the next player (clockwise) next time."
h$[ 4] ="The next player (clockwise) start to play a card if he can by putting a card on this visible card"
h$[ 5] =" respecting some simple rules:"
h$[ 6] =" - a same color (clover, spade, heart, diamond) or a same value than the visible card."
h$[ 7] =" - a 8 of any color can be played every time except on an Ace. It can impose the color of your choice."
h$[ 8] =" - respecting the visible card's color, an Ace impose only at the next player to play an Ace or to take 2 cards."
h$[ 9] =" - except in this last case, if you can't play you must take a card and then play it if it's possible before"
h$[10] ="   passing the hand to the next player (always clockwise)."
h$[11] ="When a player places his last card then the current game stop and it's time to add up the points in the hand"
h$[12] ="of each of the other players."
h$[13] ="Cards values : 8 =35 points, 10,9,7 =their value, Jack =1, Queen =2, King =3, Ace =4 points."
h$[14] ="Repeat the game until a player reaches or exceeds 150 points."
for li=1 to 14 : gr.text.draw nul, 18, 76+li*ecl, h$[li] : next
gr.text.draw nul, scx-200, 76+li*ecl, "Have fun!"
gr.bitmap.drawinto.end
return

SaveGame:
cls
if nc[1] & nc[2] & nc[3] & nc[4] then dj=dj-1+4*(dj=1)
?dj
?bdos
sc$="" : for jo=1 to 4 : sc$+=int$(sc[jo])+" " : next
?sc$
Console.Save path$ + filename$
cls
RETURN
