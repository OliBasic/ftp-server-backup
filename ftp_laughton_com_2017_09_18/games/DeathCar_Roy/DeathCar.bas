Rem Death Car
Rem For Android
Rem With RFO Basic!
Rem October/November 2016
Rem Version 1.00
Rem By Roy Shepherd

!debug.on

di_height = 672 % set to my Device
di_width = 1152

gr.open 255,86,80,82
gr.orientation 0 % landscape 
pause 1000
WakeLock 3
 
gr.screen screenWidth, screenHeight
scale_x = screenWidth / di_width
scale_y = screenHeight / di_height
gr.scale scale_x, scale_y

gameLoaded = 0
gosub Functions
gosub Initialise

gosub PlayDeathCar

exit

onBackKey:
    if ! gameLoaded then back.resume
    gosub OptionsMenu
    !Debug.dump.scalars
back.resume 

!------------------------------------------------
! Do once at first run of game
!------------------------------------------------
Initialise:
    dim explode[6]
    dim playerCar[4], deathCar[4], redEyes[250], blueEyes[250]
    array.load eyesX[], 25, 75, 125, 175, 225, 275, 325, 375, 425, 475, 525, 575, 625, 675, 725, 775, 825,~
                        875, 925, 975, 1025, 1075, 1125, 25, 75, 125, 175, 225, 275, 325, 375, 425, 475,~
                        525, 575, 625, 675, 725, 775, 825, 875, 925, 975, 1025, 1075, 1125, 25, 75, 125,~
                        175, 225, 275, 325, 375, 425, 475, 525, 575, 625, 675, 725, 775, 825, 875, 925, 975,~
                        1025, 1075, 1125, 25, 75, 125, 175, 225, 275, 325, 375, 425, 475, 525, 575, 625,~
                        675, 725, 775, 825, 875, 925, 975, 1025, 1075, 1125, 25, 75, 125, 175, 225, 275,~
                        325, 375, 425, 475, 525, 575, 625, 675, 725, 775, 825, 875, 925, 975, 1025, 1075,~
                        1125, 25, 75, 125, 175, 225, 925, 975, 1025, 1075, 1125, 25, 75, 125, 175, 225, 925,~
                        975, 1025, 1075, 1125, 25, 75, 125, 175, 225, 275, 325, 375, 425, 475, 525, 575, 625,~
                        675, 725, 775, 825, 875, 925, 975, 1025, 1075, 1125, 25, 75, 125, 175, 225, 275, 325,~
                        375, 425, 475, 525, 575, 625, 675, 725, 775, 825, 875, 925, 975, 1025, 1075, 1125,~
                        25, 75, 125, 175, 225, 275, 325, 375, 425, 475, 525, 575, 625, 675, 725, 775, 825,~
                        875, 925, 975, 1025, 1075, 1125, 25, 75, 125, 175, 225, 275, 325, 375, 425, 475,~
                        525, 575, 625, 675, 725, 775, 825, 875, 925, 975, 1025, 1075, 1125, 25, 75, 125,~
                        175, 225, 275, 325, 375, 425, 475, 525, 575, 625, 675, 725, 775, 825, 875, 925,~
                        975, 1025, 1075, 1125 
    
    
    array.load eyesY[], 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25,~
                        25, 25, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75,~
                        75, 75, 75, 75, 125, 125, 125, 125, 125, 125, 125, 125, 125, 125, 125, 125, 125,~
                        125, 125, 125, 125, 125, 125, 125, 125, 125, 125, 175, 175, 175, 175, 175, 175, 175,~
                        175, 175, 175, 175, 175, 175, 175, 175, 175, 175, 175, 175, 175, 175, 175, 175, 225,~
                        225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225,~
                        225, 225, 225, 225, 225, 275, 275, 275, 275, 275, 275, 275, 275, 275, 275, 325, 325,~
                        325, 325, 325, 325, 325, 325, 325, 325, 375, 375, 375, 375, 375, 375, 375, 375, 375,~
                        375, 375, 375, 375, 375, 375, 375, 375, 375, 375, 375, 375, 375, 375, 425, 425, 425,~
                        425, 425, 425, 425, 425, 425, 425, 425, 425, 425, 425, 425, 425, 425, 425, 425, 425,~
                        425, 425, 425, 475, 475, 475, 475, 475, 475, 475, 475, 475, 475, 475, 475, 475, 475,~
                        475, 475, 475, 475, 475, 475, 475, 475, 475, 525, 525, 525, 525, 525, 525, 525, 525,~
                        525, 525, 525, 525, 525, 525, 525, 525, 525, 525, 525, 525, 525, 525, 525, 575, 575,~
                        575, 575, 575, 575, 575, 575, 575, 575, 575, 575, 575, 575, 575, 575, 575, 575, 575,~
                        575, 575, 575, 575
   
    FALSE = 0 : TRUE = 1
    sound = TRUE : buzz = TRUE : music = TRUE
    BLUE = TRUE : RED = FALSE
    highScore = 0 : gameTime = 60
    DEATH_CAR = 10 : playersCar = 1
    carSongPlaying = 1
    deathPosX = 5 : deathPosY = (di_height / 2)
    carPosX = di_width - 45 : carPosY = (di_height / 2) - 100
    path$="DeathCar/data/"
    
    ! Load and display Splash Screen
    gr.bitmap.load splash, path$ + "DeathCar.png"
    if splash = - 1 then call ImageLoadError("SplashScreen.png")
    gr.bitmap.draw splash, splash, 1, 1 : gr.render
    
    gosub LoadData
    
    ! Load Starter button
    gr.bitmap.load e_start, path$ + "engineStart.png"
    if e_start = - 1 then call ImageLoadError("engineStart.png")
    !gr.bitmap.scale e_start, e_start, 100, 100
    gr.bitmap.size e_start, startSizeX, startSizeY
    
    ! Load Speedo 
    gr.bitmap.load speed, path$ + "playSpeed.png"
    if speed = - 1 then call ImageLoadError("playSpeed.png")
    gr.bitmap.scale speed, speed, 100, 100
    
    ! Load Speedo needle 
    gr.bitmap.load needle, path$ + "needle.png"
    if needle = - 1 then call ImageLoadError("needle.png")
    gr.bitmap.scale needle, needle, 60, 16
    
    ! Load fuel gauge
    gr.bitmap.load gauge, path$ + "fuelGauge.png"
    if gauge = - 1 then call ImageLoadError("fuelGauge.png")
    gr.bitmap.scale gauge, gauge, 180, 95
    
    ! Load fuel gauge needle
    gr.bitmap.load f_needle, path$ + "fuelNeedle.png"
    if f_needle = - 1 then call ImageLoadError("fuelNeedle.png")
    gr.bitmap.scale f_needle, f_needle, 80, 20
    
    ! Load gas pedal 
     gr.bitmap.load gas, path$ + "gasPedal.png"
     if gas = - 1 then call ImageLoadError("gasPedal.png")
     gr.bitmap.scale gas, gas, 50, 75
     
     ! Load break pedal
    gr.bitmap.load break, path$ + "breakPedal.png"
    if break = - 1 then call ImageLoadError("breakPedal.png")
    gr.bitmap.scale break, break, 100, 45
    
    ! Load arrow in. go to centre of maze
    gr.bitmap.load arrowIn, path$ + "arrowIn.png"
    if arrowIn = - 1 then call ImageLoadError("arrowIn.png")
    gr.bitmap.scale arrowIn, arrowIn, 75, 75
    
    ! Load arrow out. go to out side of maze
    gr.bitmap.load arrowOut, path$ + "arrowOut.png"
    if arrowOut = - 1 then call ImageLoadError("arrowOut.png")
    gr.bitmap.scale arrowOut, arrowOut, 75, 75
    
    ! use collision for button taped 
    gr.color 0 : gr.point collision, - 1, - 1 
    
    ! Use for car lane change
    gr.circle laneChange, - 100, - 100, 10 : gr.color 255
    
    gosub DrawRoadWay
    gosub LoadCars
    gosub DrawDashBoard
    gosub DrawMessageBox
    gosub LoadSounds
    gosub DrawOptionsMenu
    gosub DrawDeathCarHelp
    
    ! Load the 6 explosions  
    gr.bitmap.load ex, path$ + "explode.png"
    if ex = - 1 then call ImageLoadError("explode.png")
    
    ! Crop, scale and draw the explosions
    for x = 1 to 6
        gr.bitmap.crop explode[x], ex, (x - 1) * 32, 0,32, 32 
        gr.bitmap.scale explode[x], explode[x], 50, 50
        gr.bitmap.draw explode[x], explode[x], - 110, - 110
    next
    
    gr.hide splash
    gr.bitmap.delete splash
    gameLoaded = TRUE
return

!------------------------------------------------
! Load the sounds for the game
!------------------------------------------------
LoadSounds:
    array.load freq[],261,293,330,349,392,440,494,523,586,660,698,723,786,860,898,923,986,1060,1098,1123
    dim carSong[6]
    soundpool.open 9
    soundpool.load soundCarRunning,path$ + "soundCarRunning.mp3"
    if ! soundCarRunning then call SoundLoadError("soundCarRunning")

    soundpool.load soundCarStart, path$ + "soundCarStart.wav"
    if ! soundCarStart then call SoundLoadError("soundCarStart")
    
    soundpool.load soundCollide, path$ + "soundCollide.wav"
    if ! soundCollide then call SoundLoadError("soundCollide")
    
     soundpool.load soundButtonClick, path$ + "soundButtonClick.wav"
    if ! soundButtonClick then call SoundLoadError("soundButtonClick")
    
    ! Audio loads car songs
    for s = 1 to 6
        audio.load carSong[s], Path$ + "song0" + int$(s) +".mp3" 
        if ! carSong[s] then call SoundLoadError("carSong " + int$(s))
    next
    array.load buzzGame[], 1, 100
    
    gosub StartMusic
return

!------------------------------------------------
! Draw the road, dots to runover, score and high score
!------------------------------------------------
DrawRoadWay:
    w = di_width : h = 600 : r = 50 : inc = 50
    gr.set.stroke 10 : gr.color 255, 50, 227, 4, 0
    gr.rect null, 0, 0, w, h
    gr.rect null, r, r, w - r, h - r
    r += inc
    gr.rect null, r, r, w - r, h - r
    r += inc
    gr.rect null, r, r, w - r, h - r
    r += inc
    gr.rect null, r, r, w - r, h - r
    
    ! Draw maze doors vertical
    gr.color 255,86,80,82, 1 
    gr.rect null, (w / 2)-40, 40, (w / 2) + 40, h - 40
    gr.rect null, 40, (h / 2) - 40, w - 40, (h / 2) + 40
    
    ! Draw last maze wall
    r += inc : gr.color 255, 50, 227, 4, 1
    gr.rect null, r, r, w - r, h - r
    
    gosub DrawCatsEyes
    gosub DrawMazeCollisionWalls
    gosub DrawChangeLaneWalls
    
    ! Draw Score
    gr.text.size 30 : gr.color 255, 255, 255, 255
    gr.text.draw txtScore, r, r + 35, "Score: " + int$(score)
    
    ! Draw High Score
     gr.text.draw txtHighScore, r, r + 85, "High Score: " + int$(highScore)
    
    ! Draw Speedo
    gr.bitmap.draw speedo, speed, (di_width / 2) - 50, (di_height / 2) - 85
    
    ! Draw speedo needle
    needlePosX = (di_width / 2) - 40 : needlePosY = (di_height / 2) - 42
    gr.rotate.start 0, needlePosX, needlePosY, RotSpeedoNeedle
         gr.bitmap.draw speedNeedle, needle, needlePosX, needlePosY
	  gr.rotate.end
    
    ! Draw fuel gauge
    gr.bitmap.draw fuelGauge, gauge, (di_width / 2) + 140, (di_height / 2) - 80
    
    ! Draw fuel gauge needle
    fuelNeedlePosX = 745 : fuelNeedlePosY = 333
    gr.rotate.start 0, fuelNeedlePosX, fuelNeedlePosY, RotFuelNeedle
         gr.bitmap.draw fuelNeedle, f_needle, fuelNeedlePosX, fuelNeedlePosY
	  gr.rotate.end
    
    ! Draw engine start button
    gr.bitmap.draw engineStart, e_start, (di_width / 2) - startSizeX / 2, ((di_height / 2) - startSizeY / 2) - 35
    gr.hide engineStart
 
return

!------------------------------------------------
! Draw the Options Menu
!------------------------------------------------
DrawOptionsMenu:
    dim optMenu[14] % use this to hide, show the menu 

    gr.bitmap.load opt, path$ + "optionsMenu.png"
    if opt = - 1 then call ImageLoadError("optionsMenu.png")
    gr.bitmap.size opt, optSizeX, optSizeY
    gr.bitmap.load tick, path$ + "tick.png"
    if tick = - 1 then call ImageLoadError("tick")
    gr.bitmap.scale tick, tick, 27, 23
    
    ! Gray out screen for menu
    gr.color 180,0,0,0,1
    gr.rect optMenu[1], 0, 0, di_width, di_height
    !Draw the options menu
    gr.color 255,0,0,0,1
    gr.bitmap.draw optMenu[2], opt, (di_width / 2) - (optSizeX / 2), (di_height / 2) - (optSizeY / 2)
    ! Draw the ticks
    gr.bitmap.draw optMenu[3], tick, 625, 175 % music tick
    gr.bitmap.draw optMenu[4], tick, 625, 208 % sound tick
    gr.bitmap.draw optMenu[5], tick, 625, 242 % buzz tick
    
    ! Button
    gr.color 0,255,255,255,0
    gr.rect optMenu[6],  458, 165, 699, 200  % Music
    gr.rect optMenu[7],  458, 201, 699, 234  % Sound
    gr.rect optMenu[8],  458, 235, 699, 268  % Buzz
    
    gr.rect optMenu[9],  458, 276, 699, 320  % Reset High Score
    gr.rect optMenu[10], 458, 320, 699, 365  % Change Song
    gr.rect optMenu[11], 458, 366, 699, 410  % About
    gr.rect optMenu[12], 458, 411, 699, 455  % Help
    gr.rect optMenu[13], 458, 456, 699, 495  % Exit Game
    gr.rect optMenu[14], 458, 496, 699, 545  % Close Menu
   
    for off = 1 to 14
        gr.hide optMenu[off]
    next
  
return

!------------------------------------------------
! Draw message box for when a game is over
!------------------------------------------------
DrawMessageBox:
    dim mess[7]
   
    gr.color 255,0,0,0,1 : gr.set.stroke 1
    gr.rect mess[1], 200, di_height - 80, di_width - 200, di_height
    gr.color 255,255,255,255,0
    gr.rect mess[2], 200, di_height - 80, di_width - 200, di_height
    gr.color 255,255,255,255,1 : gr.text.align 2
    gr.text.draw mess[3], di_width / 2, di_height - 50, ""
    gr.color 255,255,255,255,1
    gr.rect mess[4], 300, di_height - 40, (di_width / 2) - 10, di_height - 5
    gr.rect mess[5], (di_width / 2) + 10, di_height - 40, (di_width) - 300, di_height - 5
    gr.color 255,0,0,0
    gr.text.draw mess[6], (di_width / 2) - 150, di_height - 12, "Play"
    gr.text.draw mess[7], (di_width / 2) + 150, di_height - 12, "Exit"
    
    gr.group messageBox, mess[1], mess[2], mess[3], mess[4], mess[5], mess[6], mess[7]
    gr.hide messageBox
   
return


!------------------------------------------------
! Draw the Help screen
!------------------------------------------------
DrawDeathCarHelp:
    dim help[20] : gr.text.size 25
    y = 50 : x = 50 : ofset = 30 : xoff = 50
    ! Gray out screen for help
    gr.color 200,0,0,0,1 : gr.text.align 1
    gr.rect help[1], 0, 0, di_width, di_height
    gr.color 255,255,255,0

    gr.text.draw help[2], x, y, "You are driving your highly modified jeep. Your job is to collect all the cat's eyes"
    y += ofset 
    gr.text.draw help[3], x, y, "in the maze, by running over them."
    y += ofset
    gr.text.draw help[4], x, y, "Avoid the black truck driver, she is trying to ram you. She dose this by swiching into you lane."
    y += ofset  
    gr.text.draw help[5], x, y, "Once you have collected all the cat's eyes, the maze fills with more cat's eyes and the black"
    y += ofset
    gr.text.draw help[6], x, y, "truck speeds up a little." : y += ofset
    y += ofset / 2
   
    gr.text.draw help[7], x, y, "When you run out of fuel you jeep coasts to a stop and the black truck driver will seek you out and "
    y += ofset
    gr.text.draw help[8], x, y, "ram you. The game is over when you are rammed with the back truck" : y += ofset
    y += ofset / 2
    
    gr.color 255,255,255,255
    gr.text.draw help[9], x, y, "Back Key for options."
    y += ofset :  y += ofset / 2
    
    gr.color 255,0,255,0 : gr.text.size 30
    gr.text.draw help[10], x, y, "Point:" : y += ofset
    gr.text.draw help[11], x + xoff, y, "Cat's Eyes = 10" : y += ofset
    gr.text.draw help[12], x + xoff, y, "All Cat's Eyes = 100" : y += ofset
    
    y += ofset / 2
    gr.color 255,255,0, 0
    gr.text.draw help[13], x, y, "Controls:" : y += ofset
    gr.bitmap.draw help[14], arrowOut, x + xoff, y
    gr.bitmap.draw help[15], arrowIn, x + xoff * 3, y 
    gr.bitmap.draw help[16], break, x + xoff * 5, y
    gr.bitmap.draw help[17], gas, x + xoff * 8, y
    y += ofset * 4
    gr.text.draw help[18], x, y, " Drive out   Drive In   Break        Gas"
    y += ofset * 2
    gr.color 255,0,255,255
    gr.rect help[19],(di_width / 2) - 300, y - 30, (di_width / 2) + 300, y + 10
    gr.color 255,255,0,0 : gr.text.align 2
    gr.text.draw help[20], (di_width / 2), y, "Tap to close Help"
    gr.text.align 1
    
    for x = 1 to 20
        gr.hide help[x]
    next
return

!------------------------------------------------
! Options, sound, music, buzz, reset high score,
! change song, about, help, exit
!------------------------------------------------
OptionsMenu:
    allDone = FALSE
    for on = 1 to 14
       gr.show optMenu[on]
       if on = 3 & ! music then gr.hide optMenu[3]
       if on = 4 & ! sound then gr.hide optMenu[4]
       if on = 5 & ! buzz  then gr.hide optMenu[5]
    next
    gr.render
    do : gr.touch t, tx, ty  : until ! t
    do
        gr.touch t, tx, ty 
        if t then 
            do : gr.touch t, tx, ty  : until ! t
            tx /= scale_x : ty /= scale_y
            gr.modify collision, "x", tx, "y", ty
       
        
            if gr_collision(collision,optMenu[6]) then 
                gosub ModMusic
                
            elseif gr_collision(collision,optMenu[7]) then
                gosub ModSound
                
            elseif gr_collision(collision,optMenu[8]) then
                gosub ModBuzz
            
            elseif gr_collision(collision,optMenu[9]) then
                gosub ResetHighScore
                
            elseif gr_collision(collision,optMenu[10]) then
                gosub SongMenu
                
            elseif gr_collision(collision,optMenu[11]) then
                gosub AboutDeathCar
                
            elseif gr_collision(collision,optMenu[12]) then
                gosub HelpDeathCar
                
            elseif gr_collision(collision,optMenu[13]) then
                gosub ExitDeathCar
                
            elseif gr_collision(collision,optMenu[14]) then 
            if sound then call PlaySound(sound, soundButtonClick)
                allDone = TRUE
                
            endif
        
            gr.modify collision, "x", - 1, "y", - 1
        endif
    until allDone
    for off = 1 to 14
        gr.hide optMenu[off]
    next
    gr.render
return


!------------------------------------------------
! Turn music on and off
!------------------------------------------------
ModMusic:
  if buzz then vibrate buzzGame[], -1
  if sound then call PlaySound(sound, soundButtonClick)
  if music then
    music = FALSE
    gr.hide optMenu[3]
    audio.stop 
  else
    music = TRUE
    gr.show optMenu[3]
    audio.play carSong[carSongPlaying]
    audio.loop 
  endif
  gr.render
return

!------------------------------------------------
! Turn sound on and off
!------------------------------------------------
ModSound:
  if buzz then vibrate buzzGame[], -1
  if sound then call PlaySound(sound, soundButtonClick)
  if sound then
    sound = FALSE
   gr.hide optMenu[4]
  else
    sound = TRUE
    gr.show optMenu[4]
  endif
  gr.render 
return

!------------------------------------------------
! Turn buzz on and off
!------------------------------------------------
ModBuzz:
  if buzz then vibrate buzzGame[], -1
  if sound then call PlaySound(sound, soundButtonClick)
  if buzz then
    buzz = FALSE
    gr.hide optMenu[5]
  else
    buzz = TRUE
   gr.show optMenu[5]
  endif
  gr.render
return

!------------------------------------------------
! Reset the high score to zero 
!------------------------------------------------
ResetHighScore:
  if buzz then vibrate buzzGame[], -1
  if sound then call PlaySound(sound, soundButtonClick)
  do : dialog.message "Reset high Score to Zero?",,yn,"Yes","No" : until yn > 0
  if buzz then vibrate buzzGame[], -1
  if sound then call PlaySound(sound, soundButtonClick)
  if yn = 1 then 
        highScore = 0
        gr.modify txtHighScore, "text", "High Score: " + int$(highScore)
  endif
  gr.render
return

!------------------------------------------------
! About Snapper
!------------------------------------------------
AboutDeathCar:
    if buzz then vibrate buzzGame[], -1
    if sound then call PlaySound(sound, soundButtonClick)
    do
       Dialog.Message " About:\n\n",~
			" 	Death Car for Android\n\n" + ~
			" 	With: RFO BASIC!\n\n" + ~
			" 	October/November 2016\n\n" +~
			" 	Version: 1.00\n\n" + ~ 
			" 	Author: Roy Shepherd\n\n", ~
			OK, "OK"
        if buzz then vibrate buzzGame[], -1
        if sound then call PlaySound(sound, soundButtonClick)
    until OK > 0
    
return

!------------------------------------------------
! How to play Snapper
!------------------------------------------------
HelpDeathCar:
    if buzz then vibrate buzzGame[], -1
    if sound then call PlaySound(sound, soundButtonClick)
    for x = 1 to 20
        gr.show help[x]
    next
    gr.render
    do
    gr.touch t, tx, ty
    if t then 
        tx /= scale_x : ty /= scale_y
        gr.modify collision, "x", tx, "y", ty
    endif
    until gr_collision(help[20], collision)
    if buzz then vibrate buzzGame[], -1
    if sound then call PlaySound(sound, soundButtonClick)
    for x = 1 to 20 
        gr.hide help[x]
    next
    gr.render
   
return

!------------------------------------------------
! Leaving Snapper
!------------------------------------------------
ExitDeathCar:
    if buzz then vibrate buzzGame[], -1
    if sound then call PlaySound(sound, soundButtonClick)
    do
        dialog.message "Exit Death Car",, yn, "Yes", "No"
        if buzz then vibrate buzzGame[], -1
        if sound then call PlaySound(sound, soundButtonClick)
    until yn > 0 
    if yn = 1 then 
        if score > highScore then
            highScore = score 
            popup"New High Score:" + int$(highScore)
            gr.modify txtHigh, "text", "High Score:" + int$(highScore) 
            gr.render
        endif 
        gosub SaveData : pause 200
        exit
    endif
return


!------------------------------------------------
!Draw the red and blue cats eyes
!------------------------------------------------
DrawCatsEyes:

    for x = 1 to 250
        gr.bitmap.load r_eye, path$ + "redCat.png"
        gr.bitmap.draw redEyes[x], r_eye, eyesX[x] - 10, eyesY[x] - 10
        gr.hide redEyes[x]
    next
    
    for x = 1 to 250
        gr.bitmap.load b_eye, path$ + "blueCat.png"
        gr.bitmap.draw blueEyes[x], b_eye, eyesX[x] - 10, eyesY[x] - 10
        gr.hide blueEyes[x]
    next

return

!------------------------------------------------
! Light up ether the red or blue cats eyes
!------------------------------------------------
ReLightCatsEyes:
    ! All out
    for x = 1 to 250
        gr.hide redEyes[x]
        gr.hide redEyes[x]
    next
    ! Light up red or blue cat's eyes
    if RED then
        for x = 1 to 250
            gr.show redEyes[x]
        next
    else
        for x = 1 to 250
            gr.show blueEyes[x]
        next
    endif
    gr.render
return

!------------------------------------------------
!Draw the maze walls for collision testing
!------------------------------------------------
DrawMazeCollisionWalls:
    dim mazeWall[40]
    gr.color 0
    ! Lane 1, outer
    gr.rect mazeWall[1], 0, 0, 42, 5
    gr.rect mazeWall[2], 0, 5, 4, 34
    gr.rect mazeWall[3], 1106, 0, 1145, 5
    gr.rect mazeWall[4], 1150, 4, 1145, 38
    gr.rect mazeWall[5], 0, 549, 5, 594
    gr.rect mazeWall[6], 4, 595, 38, 602
    gr.rect mazeWall[7], 1143, 543, 1150, 595
    gr.rect mazeWall[8], 1103, 591, 1140, 601
    
    ! Lane 2
    gr.rect mazeWall[9], 44, 42, 87, 53
    gr.rect mazeWall[10], 44, 55, 53, 92
    gr.rect mazeWall[11], 1095, 44, 1052, 52
    gr.rect mazeWall[12], 1106, 54, 1097, 90
    gr.rect mazeWall[13], 44, 504, 53, 543
    gr.rect mazeWall[14], 51, 551, 86, 543
    gr.rect mazeWall[15], 1056, 542, 1095, 552
    gr.rect mazeWall[16], 1096, 497, 1104, 543
    
    ! Lane 3
    gr.rect mazeWall[17], 94, 93, 144, 102
    gr.rect mazeWall[18], 94, 104, 104, 134
    gr.rect mazeWall[19], 999, 94, 1044, 103
    gr.rect mazeWall[20], 1045, 104, 1056, 136
    gr.rect mazeWall[21], 94, 452, 102, 492
    gr.rect mazeWall[22], 100, 503, 142, 494
    gr.rect mazeWall[23], 1005, 491, 1055, 501
    gr.rect mazeWall[24], 1046, 455, 1056, 491
    
    ! Lane 4
    gr.rect mazeWall[25], 154, 144, 187, 152
    gr.rect mazeWall[26], 144, 154, 153, 190
    gr.rect mazeWall[27], 953, 145, 994, 153
    gr.rect mazeWall[28], 997, 154, 1004, 183
    gr.rect mazeWall[29], 143, 403, 152, 441
    gr.rect mazeWall[30], 149, 452, 194, 443
    gr.rect mazeWall[31], 993, 444, 954, 452
    gr.rect mazeWall[32], 996, 440, 1004, 401
    
    ! Lane 5
    gr.rect mazeWall[33], 193, 192, 241, 202
    gr.rect mazeWall[34], 194, 204, 203, 237
    gr.rect mazeWall[35], 906, 195, 943, 204
    gr.rect mazeWall[36], 945, 204, 954, 233
    gr.rect mazeWall[37], 192, 355, 204, 390
    gr.rect mazeWall[38], 203, 391, 242, 402
    gr.rect mazeWall[39], 944, 354, 955, 391
    gr.rect mazeWall[40], 902, 393, 947, 402
    !gr.render
return

!------------------------------------------------
! Collision walls for changing lanes
!------------------------------------------------
DrawChangeLaneWalls:
    dim laneGate[8]
    ! Top gates
    gr.rect laneGate[1], 615, 2, 628, 248
    gr.rect laneGate[2], 535, 2, 520, 247
    
    ! Left gates
    gr.rect laneGate[3], 4, 260, 248, 251
    gr.rect laneGate[4], 2, 337, 246, 342
    
    ! Bottom gates
    gr.rect laneGate[5], 535, 347, 522, 591
    gr.rect laneGate[6], 613, 347, 624, 590
    
    ! Right gates
    gr.rect laneGate[7], 900, 250, 1145, 258
    gr.rect laneGate[8], 900, 347, 1146, 333
return

!------------------------------------------------
! Load players car and the death car
!------------------------------------------------
LoadCars:
    ! Load players car
    for x = 1 to 4
        gr.bitmap.load p_car, path$ + "Pcar0" + int$(x) + ".png"
        if p_car = - 1 then call ImageLoadError("Pcar0" + int$(x))
        if x = 1 | x = 3 then gr.bitmap.scale p_car, p_car, 40, 80
        if x = 2 | x = 4 then gr.bitmap.scale p_car, p_car, 80, 40
        gr.bitmap.draw playerCar[x], p_car, - 100, - 100
    next
   ! Load death car
   for x = 1 to 4
        gr.bitmap.load d_car, path$ + "Dcar0" + int$(x) + ".png"
        if d_car = - 1 then call ImageLoadError("Dcar")
        if x = 1 | x = 3 then gr.bitmap.scale d_car, d_car, 40, 80
        if x = 2 | x = 4 then gr.bitmap.scale d_car, d_car, 80, 40
        gr.bitmap.draw deathCar[x], d_car, - 100, - 100
    next
    carSizeX = 40 : carSizeY = 80
    
return

!------------------------------------------------
! Draw gas, break, move in and move out buttons
!------------------------------------------------
DrawDashBoard:
    h = di_height - 65 : c = di_width / 2
    
    gr.bitmap.draw outArrow, arrowOut, c - 250, h
    gr.bitmap.draw inArrow, arrowIn, c - 150, h
    
    gr.bitmap.draw breakPedal, break, c + 100, h + 10
    gr.bitmap.draw gasPedal, gas, c + 250, h
    
return

!------------------------------------------------
!Has the gas or break been taped
!------------------------------------------------
GasOrBreak:
    gr.touch tt, tx, ty
    if ! tt then return
    tx /= scale_x : ty /= scale_y 
    gr.modify collision, "x", tx, "y", ty
    
    if fuelExhausted then return
    
    if gr_collision(collision, breakPedal) & playerSpeed > 5 then 
        if buzz then vibrate buzzGame[], -1
        playerSpeed -= 0.3 : deathSpeed -= 0.2
        if playerSpeed < 5 then playerSpeed = 5
        if deathSpeed < 5 then deathSpeed = 5
        playerTurnSpeed -= playerSpeed / 2.5
        gr.modify RotSpeedoNeedle, "angle", + playerTurnSpeed - 50, "x", needlePosX + 40, "y", needlePosY + 8
    endif
    
    if gr_collision(collision, gasPedal)  & playerSpeed < 20 then 
        if buzz then vibrate buzzGame[], -1
        playerSpeed += 0.3 : deathSpeed += deathIncrease
        playerTurnSpeed += playerSpeed / 2.5
        gr.modify RotSpeedoNeedle, "angle", + playerTurnSpeed - 50, "x", needlePosX + 40, "y", needlePosY + 8
    endif
  
return

!------------------------------------------------
! Update fuel gauge; game over when fuel used up 
!------------------------------------------------
FuelGauge:
    if fuelExhausted & playerSpeed > 0 then 
        playerSpeed -= 0.1
        if playerSpeed < 0 then playerSpeed = 0
        if playerTurnSpeed > 0 then playerTurnSpeed -= playerSpeed / 6
        gr.modify RotSpeedoNeedle, "angle", + playerTurnSpeed - 50, "x", needlePosX + 40, "y", needlePosY + 8
    endif
    
    if fuelExhausted then return
   
    fuelTock = (time() - fuelTick - stopTime) / 1000
    fuelTurnSpeed -= fuelTock / 2000
    ! fuelTurnSpeed -= fuelTock / 20 for testing
    gr.modify RotFuelNeedle, "angle", + fuelTurnSpeed, "x", fuelNeedlePosX + 62, "y", fuelNeedlePosY + 10
    if fuelTurnSpeed < 26 then 
        fuelExhausted = TRUE
    endif
return

!------------------------------------------------
! Do at start of each game
!------------------------------------------------
SetUp:
    deathCollision = FALSE : fuelExhausted = FALSE
    score = 0 : playerLane = 1 : deathCarLane = 1
    p_direction = 1 : d_direction = 1
    playerSpeed = 5 : deathSpeed = 5
    playerTurnSpeed = 5 : fuelTurnSpeed = 0
    gameOver = FALSE : deathIncrease = 0
    catsEyesCount = 0 : fuelTick = time()
    stopTime = 0 : startTime = 0
    tick = time() : deathTick = time()
    deathPosX = 5 : deathPosY = (di_height / 2)
    carPosX = di_width - 45 : carPosY = (di_height / 2) - 100
    gosub RelightCatsEyes
    gr.modify RotSpeedoNeedle, "angle", - 50, "x", needlePosX + 40, "y", needlePosY + 8
    gr.modify RotFuelNeedle, "angle", + 26, "x", fuelNeedlePosX + 62, "y", fuelNeedlePosY + 10
    gr.modify deathCar[d_direction], "x", deathPosX, "y", deathPosY
    gr.modify playerCar[p_direction], "x", carPosX, "y", carPosY
	  gr.render 
    gosub StartEngine
return

!------------------------------------------------
! Start the car's engine
!------------------------------------------------
StartEngine:
    gr.show engineStart : gr.render
    do
        gr.touch t, tx, ty 
        if t then  
            tx /= scale_x : ty /= scale_y 
            gr.modify collision, "x", tx, "y", ty
        endif
    until gr_collision(collision, engineStart)
    call PlaySound(sound, soundCarStart)
    x = (di_width / 2) - startSizeX / 2 : y = ((di_height / 2) - startSizeY / 2) - 35
    ! Shake starter and move fuel gauge to full
    for shake = 1 to 10 
        gr.modify engineStart, "x", x + floor( 10 * rnd() - 5), "y", y + floor( 10 * rnd() - 5)
        gr.render
        gr.modify engineStart, "x", x, "y", y : gr.render
        fuelTurnSpeed += 16 
        gr.modify RotFuelNeedle, "angle", + fuelTurnSpeed, "x", fuelNeedlePosX + 62, "y", fuelNeedlePosY + 10
    next
    gr.hide engineStart 
    ! Speedo and fuel gauge 
    gr.modify RotSpeedoNeedle, "angle", + playerTurnSpeed - 50, "x", needlePosX + 40, "y", needlePosY + 8
    gr.modify RotFuelNeedle, "angle", + fuelTurnSpeed, "x", fuelNeedlePosX + 62, "y", fuelNeedlePosY + 10
return    


!------------------------------------------------
! Main loop of the game
!------------------------------------------------
PlayDeathCar:
    gosub SetUp
    do
        gosub PlayersCar
        
        gosub DeathCar
        
        gr.render
    until gameOver
    gosub SaveData : pause 200
return

!------------------------------------------------
! Check for collision with cats eyes
!------------------------------------------------
CatsEyes:
    
    if BLUE then
        for x = 1 to 250
            if gr_collision(blueEyes[x], playerCar[p_direction]) then
                score += 10 : catsEyesCount ++
                gr.modify txtScore, "text", "Score: " + int$(score)
                gr.hide blueEyes[x]
            endif
        next
    else
        for x = 1 to 250
            if gr_collision(redEyes[x], playerCar[p_direction]) then
                score += 10 : catsEyesCount ++
                gr.modify txtScore, "text", "Score: " + int$(score)
                gr.hide redEyes[x]
            endif
        next
    endif
    
    if catsEyesCount = 250 then 
        catsEyesCount = 0
        if RED then BLUE = TRUE : RED = FALSE else RED = TRUE : BLUE = FALSE
        score += 100
        if deathSpeed < playerSpeed then deathIncrease += 0.1
        gosub ReLightCatsEyes
    endif
    
return

!------------------------------------------------
! Move the players car
! p_direction = 1 car go up, p_direction = 2 car go left
! p_direction = 3 car go down, p_direction = 4 car go right
!------------------------------------------------
PlayersCar:
    gosub EngineNote
    gosub GasOrBreak
    gosub CatsEyes
    gosub PlayerDirectionChange
    gosub PlayerLaneChange
    gosub FuelGauge
    if p_direction = 1 then carPosY = carPosY - playerSpeed
    if p_direction = 2 then carPosX = carPosX - playerSpeed
    
    if p_direction = 3 then carPosY = carPosY + playerSpeed
    if p_direction = 4 then carPosX = carPosX + playerSpeed 
    
    gr.modify playerCar[p_direction], "x", carPosX, "y", carPosY 
return

!------------------------------------------------
! If sound then play the engine note
!------------------------------------------------
EngineNote:
    if ! sound then return
    soundpool.play stream, soundCarRunning,0.9,0.9,0,0,freq[int(playerSpeed) - 4]/800
return

!------------------------------------------------
! Check for collision with maze wall. If so change direction 
!------------------------------------------------
PlayerDirectionChange:
    
    array.load d[], 1, 2, 3, 4
    if playerLane = 1 then 
        array.load w[], 7, 3, 2, 6
        array.load xy[], 1107, 5, 5, 555
        
    elseif playerLane = 2 then
        array.load w[], 16, 11, 10, 14
        array.load xy[], 1055, 57, 57, 503
        array.load d[], 1, 2, 3, 4
        
     elseif playerLane = 3 then
        array.load w[], 24, 19, 18, 22
        array.load xy[], 1007, 107, 107, 453
        array.load d[], 1, 2, 3, 4
        
    elseif playerLane = 4 then
        array.load w[], 32, 27, 26, 30
        array.load xy[], 954, 156, 156, 405
        array.load d[], 1, 2, 3, 4
        
    elseif playerLane = 5 then
        array.load w[], 39, 35,  34, 38
        array.load xy[], 902, 206, 206, 352
        array.load d[], 1, 2, 3, 4
    endif
    
    for x = 1 to 4
        if gr_collision(mazeWall[w[x]], playerCar[p_direction]) then 
            gr.modify playerCar[p_direction], "x", - 100, "y", - 100 
            p_direction = d[x] 
            if p_direction = 1 then carPosX = xy[x] : carPosY -= 80
            if p_direction = 2 then carPosY = xy[x] : carPosX -= 80
            if p_direction = 3 then carPosX = xy[x]
            if p_direction = 4 then carPosY = xy[x]
        endif
    next
    
    array.delete w[], xy[], d[]
   
return

!------------------------------------------------
! Player change lane if at space in maze and arrow pressed 
!------------------------------------------------
PlayerLaneChange:
    if fuelExhausted then return
    
    ! To stop changing more then one lane
    tock = (time() - tick) / 1000
    if tock < 1 then return
    tick = 0
    
    gr.touch t, tx, ty
    if ! t then return
    tx /= scale_x : ty /= scale_y 
    gr.modify collision, "x", tx, "y", ty
    laneJump = 50
    array.load j[], 7, 2, 4, 6
    
    if p_direction = 1 | p_direction = 2 then 
        gr.modify laneChange, "x", carPosX, "y", carPosY
    elseif p_direction = 3 then
        gr.modify laneChange, "x", carPosX, "y", carPosY + 80
    else 
        gr.modify laneChange, "x", carPosX + 80, "y", carPosY
    endif

    if gr_collision(collision, inArrow) & playerLane <> 5 then 
        if buzz then vibrate buzzGame[], -1
        for x = 1 to 4
            if gr_collision(laneGate[j[x]], laneChange) then
                if p_direction = 1 then carPosX -= laneJump : playerLane ++ : tick = time()
                if p_direction = 2 then carPosY += laneJump : playerLane ++ : tick = time()
                if p_direction = 3 then carPosX += laneJump : playerLane ++ : tick = time()
                if p_direction = 4 then carPosY -= laneJump : playerLane ++ : tick = time()
            endif
        next
    endif
    
    if gr_collision(collision, outArrow) & playerLane <> 1 then
        if buzz then vibrate buzzGame[], -1
         for x = 1 to 4
            if gr_collision(laneGate[j[x]], laneChange) then
                if p_direction = 1 then carPosX += laneJump : playerLane -- : tick = time()
                if p_direction = 2 then carPosY -= laneJump : playerLane -- : tick = time()
                if p_direction = 3 then carPosX -= laneJump : playerLane -- : tick = time()
                if p_direction = 4 then carPosY += laneJump : playerLane -- : tick = time()
            endif
        next
    endif
    array.delete j[]
return

!------------------------------------------------
! Move the death car
! p_direction = 1 car go up, p_direction = 2 car go right
! p_direction = 3 car go down, p_direction = 4 car go left
!------------------------------------------------
DeathCar:
    gosub DeathLaneChange
    gosub DeathDirectionChange
    gosub CollisionDeathPlayer
    if d_direction = 1 then deathPosY = deathPosY - deathSpeed
    if d_direction = 2 then deathPosX = deathPosX + deathSpeed
    
    if d_direction = 3 then deathPosY = deathPosY + deathSpeed
    if d_direction = 4 then deathPosX = deathPosX - deathSpeed 
    
    gr.modify deathCar[d_direction], "x", deathPosX, "y", deathPosY 
    
return

!------------------------------------------------
! If death car not on same lane as players car, then change lane if at a space in maze
!------------------------------------------------
DeathLaneChange:

    ! To stop changing more then one lane
    deathTock = (time() - deathTick) / 1000
    if deathTock < 1 then return
    deathTick = 0
    
    laneJump = 50
    array.load j[], 1, 8, 5, 3
    
    if d_direction = 1 | d_direction = 4 then
        gr.modify laneChange, "x", deathPosX, "y", deathPosY
        
    elseif d_direction = 2 then 
        gr.modify laneChange, "x", deathPosX + 80, "y", deathPosY
        
    elseif d_direction = 3 then
        gr.modify laneChange, "x", deathPosX, "y", deathPosY + 80
    endif
    
    if deathCarLane < playerLane then 
        for x = 1 to 4
            if gr_collision(laneGate[j[x]], laneChange) then
                if d_direction = 1 then deathPosX += laneJump : deathCarLane ++ : deathTick = time() 
                if d_direction = 2 then deathPosY += laneJump : deathCarLane ++ : deathTick = time() 
                if d_direction = 3 then deathPosX -= laneJump : deathCarLane ++ : deathTick = time()  
                if d_direction = 4 then deathPosY -= laneJump : deathCarLane ++ : deathTick = time()  
            endif
        next
        
    elseif deathCarLane > playerLane
        for x = 1 to 4
            if gr_collision(laneGate[j[x]], laneChange) then
                if d_direction = 1 then deathPosX -= laneJump : deathCarLane -- : deathTick = time()  
                if d_direction = 2 then deathPosY -= laneJump : deathCarLane -- : deathTick = time()  
                if d_direction = 3 then deathPosX += laneJump : deathCarLane -- : deathTick = time()  
                if d_direction = 4 then deathPosY += laneJump : deathCarLane -- : deathTick = time()  
            endif
        next
    endif
    array.delete j[]
return

!------------------------------------------------
! Check for, death car, collision with maze wall. If so change direction 
!------------------------------------------------
DeathDirectionChange:
    array.load d[], 1, 2, 3, 4
    if deathCarLane = 1 then
        array.load w[], 5, 1, 4, 8
        array.load xy[], 5, 5, 1107, 555
        
    elseif deathCarLane = 2 then
        array.load w[], 13, 9, 12, 15
        array.load xy[], 57, 57, 1055, 503
        
    elseif deathCarLane = 3 then
        array.load w[], 21, 17, 20, 23
        array.load xy[], 107, 107, 1007, 453
        
    elseif deathCarLane = 4 then
        array.load w[], 29, 25, 28, 31
        array.load xy[], 156, 156, 954, 405
        
    elseif deathCarLane = 5 then
        array.load w[], 37, 33, 36, 40
        array.load xy[], 206, 206, 902, 352
    
    endif
    
    for x = 1 to 4
        if gr_collision(mazeWall[w[x]], deathCar[d_direction]) then 
            gr.modify deathCar[d_direction], "x", - 100, "y", - 100 
            d_direction = d[x] 
            if d_direction = 1 then deathPosX = xy[x]
            if d_direction = 2 then deathPosY = xy[x]
            if d_direction = 3 then deathPosX = xy[x]
            if d_direction = 4 then deathPosY = xy[x]
        endif
    next
    
    array.delete w[], xy[], d[]
   
return

!------------------------------------------------
! Have the two cars collided. If so game over
!------------------------------------------------
CollisionDeathPlayer:

    if playerLane <> deathCarLane then return
    if gr_collision(deathCar[d_direction], playerCar[p_direction]) then 
        gr.render
        deathCollision = TRUE
        call PlaySound(sound, soundCollide)
        call Explosion(explode[], p_direction, carPosX, carPosY)
        gosub AnotherGame
    endif
    
return

!------------------------------------------------
! The two cars have collided. If necessary update high Score.
! Ask user if they want another game
!------------------------------------------------
AnotherGame:
    m$ = ""
    if fuelExhausted then m$ = "Fuel Exhausted. "
    if deathCollision then m$ = m$ + "Death Collision. " 
    if score > highScore then 
        highScore = score
        m$ = m$ + "New High Score"
        gr.modify txtHighScore, "text", "High Score: " + int$(highScore)
        gr.render
    endif
    messFlag = FALSE
    gr.show messageBox
    gr.modify mess[3], "text", m$
    gr.render
    do : gr.touch t, tx, ty : until ! t
    do
        gr.touch t, tx, ty
        if t then
            tx /= scale_x : ty /= scale_y 
            gr.modify collision, "x", tx, "y", ty
            if gr_collision(collision, mess[5]) then 
                gameOver = TRUE : messFlag = TRUE
                if buzz then vibrate buzzGame[], -1
                call PlaySound(sound, soundButtonClick)
            endif
            
            if gr_collision(collision, mess[4]) then 
                gr.hide messageBox 
                gr.modify playerCar[p_direction], "x", - 100, "y", - 100
                gr.modify deathCar[d_direction], "x", - 100, "y", - 100
                if buzz then vibrate buzzGame[], -1
                call PlaySound(sound, soundButtonClick)
                BLUE = TRUE : RED = FALSE
                gr.modify explode[6], "x", - 100, "y", - 100 : gr.render
                gosub SetUp 
                messFlag = TRUE
            endif
        endif
    until messFlag
    
return

!-------------------------------------------------
! Start background music at start of game, if music = TRUE
!-------------------------------------------------
StartMusic:
    if music then
        audio.stop
        audio.play carSong[carSongPlaying] 
        audio.loop 
    endif 
return

!------------------------------------------------
! Save the High Score, Music, sound, and Buzz
!------------------------------------------------
SaveData:
    file.exists pathPresent,path$+"GameData.txt"
    if ! pathPresent then file.mkdir path$

    text.open w,hs,Path$+"GameData.txt"
    
    text.writeln hs, int$(highScore)
    text.writeln hs, int$(music)
    text.writeln hs, int$(sound)
    text.writeln hs, int$(buzz)
    text.writeln hs, int$(carSongPlaying)
    text.close hs
return

!------------------------------------------------
! Load the High Score, Music, sound, and Buzz
!------------------------------------------------
LoadData:
    file.exists pathPresent,path$+"GameData.txt"
    if pathPresent then
        text.open r,hs,path$+"GameData.txt"
        
        text.readln hs, highScore$
        text.readln hs, music$
        text.readln hs, sound$
        text.readln hs, buzz$
        text.readln hs, carSongPlaying $
        text.close hs
        highScore = val(highScore$)
        music = val(music$)
        sound = val(sound$)
        buzz = val(buzz$)
        carSongPlaying = val(carSongPlaying$)
    endif
return

!------------------------------------------------
! Menu to pick a new song
!-------------------------------------------------
SongMenu:
	
    array.load menu$[],~
            "Chris Rea - The Road To Hell",~
            "Deep Purple - Highway Star",~
            "Gary Numan - Cars",~
            "R. D. Taylor - Gotta See Jane",~
            "Kylie - This Wheel's On Fire",~
            "Roy Orbison - I Drove All Night"
            
    dialog.select menuSelected, menu$[], "Song Menu"
    array.delete menu$[]
	
    sw.begin menuSelected
		
        sw.case 1
            carSongPlaying = 1
        Sw.break
           
        sw.case 2
            carSongPlaying = 2
        Sw.break
           
        sw.case 3
            carSongPlaying = 3
        sw.break

        sw.case 4
            carSongPlaying = 4
        sw.break

        sw.case 5
            carSongPlaying = 5
        sw.break
          
        sw.case 6 
            carSongPlaying = 6
        sw.break
        
    sw.end
    gosub StartMusic
return

!------------------------------------------------
! Game functions
!------------------------------------------------
Functions:

!------------------------------------------------
! The two cars explode when they collide 
!------------------------------------------------
fn.def Explosion(explode[], direction, ex, ey)
        if direction = 1 then ex -= 5 : ey += 0
        if direction = 2 then ex += 0 : ey -= 5
        if direction = 3 then ex -= 5 : ey += 40
        if direction = 4 then ex += 40 : ey -= 5
        for x = 1 to 6
            gr.modify explode[x], "x", ex, "y", ey
            gr.render
            pause 100
            gr.modify explode[x], "x", - 100, "y", - 100
        next
        gr.modify explode[6], "x", ex, "y", ey : gr.render
fn.end

!------------------------------------------------
! if sound then Play sound (ptr)
!------------------------------------------------
  fn.def PlaySound(sound, ptr)
      if sound then soundpool.play s,ptr,0.99,0.99,1,0,1
  fn.end
  
!------------------------------------------------
! load Sound files not found
!------------------------------------------------
    fn.def SoundLoadError(e$)
        dialog.message "Sound file " + e$ + " not found", "Game will end",OK,"OK" 
        end
    fn.end
    
!------------------------------------------------
! Load image file not found
!------------------------------------------------
    fn.def ImageLoadError(e$)
        dialog.message "Image file " + e$ + " not found", "Game will end",OK,"OK" 
        end
    fn.end

return