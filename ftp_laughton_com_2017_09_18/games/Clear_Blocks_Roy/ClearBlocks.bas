Rem Console Clear Blocks
Rem With RFO Basic!
Rem October 2015
Rem Version 1.00
Rem By Roy Shepherd

di_height = 720 % set to my Device
di_width = 1120
gr.open
gr.orientation 0
pause 1000
WakeLock 3 

gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y

!-------------------------------------------------
! Setup screen for drawing.
!-------------------------------------------------
gr.bitmap.create screen, di_width, di_height
gr.bitmap.draw    nn, screen, 0, 0
gr.bitmap.drawinto.start screen

!-------------------------------------------------
! include ConLib.bas after opening graphics 
! and setting up the bitmap screen and before creating any bundles 
!-------------------------------------------------
include ConLib.bas % include 'ConLib.bas' here

!-------------------------------------------------
! Put bundle's here, after the included ConLib.bas
! 'global' bundle pointer = 2.
!-------------------------------------------------
bundle.create global % bundle pointer = 2.
bundle.put global, "Divice_W", di_width
bundle.put global, "Score", 0 
bundle.put global, "Best", 0
bundle.put global, "Time", 120 % seconed to play
bundle.put global, "Device",0 % 0 = phone. 1 = tablet
bundle.put global, "Help", ""

gosub Functions % Let basic see the functions before they are called

dim blocks[8, 3] % The coloured blocks at the top of the screen
!call AutoDetectDevice()
call PhoneOrTablet()
call ReadHelp()
call LoadBest()
call DrawScreen(blocks[])
call MoveCannon(blocks[])
end

onBackKey:
	call Menu()
back.resume

end

Functions:
!-------------------------------------------------
! Ask the user if they are using a phone or a tablet
! and set bundle.put global, "Device" accordingly 
!-------------------------------------------------
fn.def PhoneOrTablet()
	do : dialog.Message"Are you using a Phone or a Tablet?",,answer,"Phone","Tablet" : until answer > 0
	if answer = 1 then bundle.put 2, "Device", 0  else bundle.put 2, "Device", 1
	fn.end
!-------------------------------------------------
! Setup the screen and load array blocks[] 
!-------------------------------------------------
fn.def DrawScreen(blocks[])
	Con_ChrPerLine(40) : Con_BackgroundName("Blue") : Con_Cls()
	c = 3 : Con_Locate(1,1) : Con_RenderOff()
	for y = 1 to 3
		Con_Locate(1,y)
		for x = 1 to 40 step 5
			Con_Background(c)
			Con_PrintSpc(5)
			if y = 2 then c-- else c++
		next
		c++
	next
	! Set the blocks to 1 
	for y = 1 to 3 
		for x = 1 to 8 
			blocks[x, y] = 1
		next 
	next
	
	Con_BackgroundName("Blue") : Con_RenderOn()
	Con_Scroll(0) : call DoScore(0) : call DoTimeLeft() : call DoBest()
fn.end
!-------------------------------------------------
! Tip device left and right to move the cannon. Tap to fire
!-------------------------------------------------
fn.def MoveCannon(blocks[])
	cannon$ = " i " : missile$ = chr$(46)
	bundle.get 2, "Device", myDevice
	do
		now = int(time() + 120 * 1000) % 2 minits
		oldTime = int(TIME())
		seconedLeft = 120 
		do
			gameOver = 0
			x = Con_GetMaxX() / 2 
			y = Con_GetMaxY()-1
			Con_PrintAt(x, y, cannon$)
			sensors.open 1 % Accelerometer
			do
				flag = 0 
				sensors.read 1, sen1, sen2, sen3 
				
				if myDevice = 1 then % Tablet
					sen1 = int(sen1)
					if sen1 > 0 & x > 0 then x-- : Con_PrintAt(x, y, cannon$)
					if sen1 < 0 & x < Con_GetMaxX()-1 then x++ : Con_PrintAt(x, y, cannon$)
				else % Phone
					sen2 = int(sen2)
					if sen2 > 0 & x < Con_GetMaxX()-1 then x++ : Con_PrintAt(x, y, cannon$)
					if sen2 < 0 & x > 0 then x-- : Con_PrintAt(x, y, cannon$)
				endif
				
				! Test with BlueStakes
				!if Con_GetTouchX() > 0 &  Con_GetTouchX() < 20 & x > 0 then x-- : Con_PrintAt(x, y, cannon$) : fuel -= 0.1 
				!if Con_GetTouchX() > 20  & x < Con_GetMaxX()-1 then x++ : Con_PrintAt(x, y, cannon$) : fuel -= 0.1 
				!if Con_GetTouchY() > 0 & Con_GetTouchY() < 10 then gameOver = LaunchMissile(blocks[],x, y)
				
				if Con_GetTouchY() > 0 then gameOver = LaunchMissile(blocks[],x, y)
				mowTime = int(time()) 
				if mowTime - 1000 > oldTime then 
					seconedLeft--
					bundle.put 2, "Time", seconedLeft
					oldTime = mowTime
					call DoTimeLeft()
				endif
				if seconedLeft < 1 then seconedLeft = 0 : call DoTimeLeft() : gameOver = 3
				
			until BlockRowClear(blocks[], 1) | gameOver = 2 | gameOver = 3
			if gameOver < 2 then 
				call DoScore(100)
				call DrawScreen(blocks[])
			endif
		until gameOver = 2 | gameOver = 3
		if gameOver = 2 then m$ = "That block is alread Cleared" else m$ = "Fuel Exhausted"
		bundle.get 2, "Score", score 
		bundle.get 2, "Best", best 
		if score > best then 
			best = score 
			bundle.put 2, "Best", best 
			call DoBest() : call SaveBest()
		
			do : dialog.message "Another game",m$ + "\nA new Best Score",yn,"Yes","No" : until yn > 0
		else
			do : dialog.message "Another game",m$,yn,"Yes","No" : until yn > 0
		endif
		
		if yn = 1 then 
			bundle.put 2, "Score", 0
			bundle.put 2, "Time", 120
			call DrawScreen(blocks[])
		endif
	until yn = 2 % No
	call GoodBye()
fn.end

!-------------------------------------------------
! Launch a Missile and test for hitting a block
!-------------------------------------------------
fn.def LaunchMissile(blocks[],x, y)
	flag = 0
	for m = y - 1 to 1 step - 1 
		Con_PrintAt(x+1, m, ".")
		Con_PrintAt(x+1, m, " ")
		
		if m = 3 & ! BlockRowClear(blocks[], 3) then % look at third row
			blow = int(x / 5) : blow++
			if blocks[blow, 3] = 1 then 
				launch = (blow * 5) - 4 
				Con_PrintAt(launch, 3,"     ")
				blocks[blow, 3] = 0
				flag = 1
			else 
				flag = 2
			endif 
		endif
		
		if flag then f_n.break
		if m = 2 & ! BlockRowClear(blocks[], 2) then % look at seconed row
			blow = int(x / 5) : blow++
			if blocks[blow, 2] = 1 then 
				launch = (blow * 5) - 4 
				Con_PrintAt(launch, 2,"     ")
				blocks[blow, 2] = 0
				flag = 1
			else 
				flag = 2
			endif 
		endif
		
		if flag then f_n.break
		if m = 1 & ! BlockRowClear(blocks[], 1) then % look at first  row (top of screen)
			blow = int(x / 5) : blow++
			if blocks[blow, 1] = 1 then 
				launch = (blow * 5) - 4 
				Con_PrintAt(launch, 1,"     ")
				blocks[blow, 1] = 0
				flag = 1
			else 
				flag = 2
			endif 
		endif
		if flag then f_n.break
	next
	if flag = 1 then call DoScore(10)
	fn.rtn flag
fn.end

!-------------------------------------------------
! Look at block[] and see if all of row is cleared
!-------------------------------------------------
fn.def BlockRowClear(blocks[],row) 
	flag = 1
	for x = 1 to 8 
		if blocks[x, row] = 1 then flag = 0
	next
	fn.rtn flag
fn.end

!-------------------------------------------------
! Increase and display the score 
!-------------------------------------------------
fn.def DoScore(s)
	bundle.get 2, "Score", score 
	score += s 
	bundle.put 2, "Score", score 
	Con_RenderOff()
	Con_PrintAt(2, Con_GetMaxY(), "Score: " + int$(score))
	Con_RenderOn()
fn.end

!-------------------------------------------------
! Display the best score 
!-------------------------------------------------
fn.def DoBest()
	bundle.get 2, "Best", best 
	b$ ="   Best: " + int$(best) 
	bestLen = len(b$)
	Con_PrintAt(Con_GetMaxX() - bestLen, Con_GetMaxY(), b$)
fn.end

!-------------------------------------------------
! Show remaining time
!-------------------------------------------------
fn.def DoTimeLeft()
	bundle.get 2, "Time", timeLeft
	t$ =" Time: " + int$(timeLeft) + " "
	timeLen = len(t$) / 2
	x = (Con_GetMaxX() / 2) - (len(t$) / 2)
	Con_RenderOff()
	Con_PrintAt(x, Con_GetMaxY(), t$)
	Con_RenderOn()
fn.end

!-------------------------------------------------
! Save best score
!-------------------------------------------------
fn.def SaveBest()
	dataPath$="../../ClearBlocks/data/"
	bundle.get 2, "Best", best
	file.exists BestPresent,dataPath$+"BlocksBest.txt"
	if BestPresent then
		text.open w,hs,dataPath$+"BlocksBest.txt"
			text.writeln hs,int$(best)
		text.close hs
	else
		file.mkdir dataPath$
		text.open w,hs,dataPath$+"BlocksBest.txt"
			text.writeln hs,int$(best)
		text.close hs
	endif
fn.end

!-------------------------------------------------
! Load best score
!-------------------------------------------------
fn.def LoadBest()
	dataPath$="../../ClearBlocks/data/"
	file.exists BestPresent,dataPath$+"BlocksBest.txt"
	if BestPresent then
		text.open r,hs,dataPath$+"BlocksBest.txt"
			text.readln hs,best$
			best = val(best$)
			bundle.put 2, "Best", best
		text.close hs
	endif
fn.end

!-------------------------------------------------
! Read the help from read.data and put in bundle
!-------------------------------------------------
fn.def ReadHelp()
	read.from 1
	while h$ <> "STOP"
		read.next h$ 
		if h$ <> "STOP" then help$ = help$ + h$ + chr$(10)
	repeat
	bundle.put 2, "Help", help$
fn.end

!-------------------------------------------------
! Back key tapped. Call the menu
!-------------------------------------------------
fn.def Menu()

	array.load menu$[],	"Reset Best Score",~
						"About",~
						"Help",~
						"Back to Game",~
						"Exit"
	dialog.select menuSelected, menu$[], "Main"
	array.delete menu$[]
	
	sw.begin menuSelected
			
		sw.case 1
			do : dialog.Message"Reset Best Score to zero?",,yn,"Yes","No" : until yn > 0
			if yn = 1 then 
				bundle.put 2, "Best", 0
				call SaveBest()
				call DoBest()
			endif
		sw.break
		
		sw.case 2
			Dialog.Message " About:\n\n",~
			"   Clear Blocks for Android\n\n" + ~
			"   With: RFO BASIC!\n\n" + ~
			"   Using ConLib.bas include file\n\n" + ~
			"   October 2015\n\n" +~
			"   Version: 1.00\n\n" + ~ 
			"   Author: Roy Shepherd\n\n", ~
			ok, "OK"
		sw.break
		
		sw.case 3
			bundle.get 2, "Help", help$
			dialog.Message " Clear Blocks Help:",help$,ok,"OK"
		sw.break
		
		sw.case 4
			! Back to game
		sw.break
			
		sw.case 5
			do : dialog.Message"Exit Game?",,yn,"Yes","No" : until yn > 0
			if yn=1 then 
				bundle.get 2, "Score", score 
				bundle.get 2, "Best", best 
				if score > best then 
					best = score 
					bundle.put 2, "Best", best 
					call DoBest() : call SaveBest()
					dialog.message "A new Best Score",,ok,"OK"
				endif
				call GoodBye()
			endif
		sw.break
		
	sw.end
fn.end

!-------------------------------------------------
! Leave game
!-------------------------------------------------
fn.def GoodBye()
	Con_ChrPerLine(80) : Con_Cls() : Con_RenderOff()
	x = Con_GetMaxX() : y = Con_GetMaxY()
	Con_ColourName("Yellow") : Con_Bold(1)
	Con_PrintString(x, chr$(9574))
	Con_Locate(1,2)
	Con_PrintString(x, chr$(9577))
	
	Con_PrintString(x, chr$(9618))
	Con_PrintString(x, chr$(9618))
	
	Con_PrintString(x, chr$(9574))
	Con_PrintString(x, chr$(9577))
	
	Con_Locate(1, y - 5) 
	Con_PrintString(x, chr$(9574))
	Con_Locate(1, y - 4) 
	Con_PrintString(x, chr$(9577))
	
	Con_PrintString(x, chr$(9618))
	Con_PrintString(x, chr$(9618))
	
	Con_PrintString(x, chr$(9574))
	Con_PrintString(x, chr$(9577))
	Con_RenderOn()
	Con_ChrPerLine(10) : Con_ColourName("Red")
	Con_BackgroundName("Navy")
	Con_PrintAt(1,2, " Good Bye ")
	pause 5000
	exit
fn.end
 
!-------------------------------------------------
! Fine out what device the user is using
!-------------------------------------------------
fn.def AutoDetectDevice()
	h$="<html>"
	h$+="<script type=\"text/javascript\">"
	h$+="function getRot()"
	h$+="{"
	h$+=" w=screen.width; h=screen.height;"
	h$+=" r = window.orientation;"
	h$+=" if ((r!=0) && (r!=90) && (r!=-90)) r=0;"
	h$+=" r=Math.abs(r);"
	h$+=" if (h > w) o=\"portrait\";"
	h$+=" else    o=\"landscape\";"
	h$+=" n=\"LANDSCAPE\"; if ((r==0 && w<h) || (r==90 && w>h)) n=\"PORTRAIT\";"
	h$+=" Android.dataLink(n+','+o);"
	h$+="};"
	h$+="</script>"
	h$+="<body onload=\"setTimeout(getRot,1000);\">"
	h$+="</body>"
	h$+="</html>"
	   html.open 0
	   Html.orientation 0 % Landscape
	   html.load.string h$
		   do
			 pause 50
			 html.get.datalink data$      % one shot only
		   until data$ <> ""
	   html.close
   type$ = LEFT$(data$,4)
  
   if is_in("LANDSCAPE",data$) then bundle.put 2, "Device", 1 % Tablet
   if is_in("PORTRAIT",data$) then  bundle.put 2, "Device", 0 % Phone
   
   if type$ <> "DAT:" then call PhoneOrTablet() % Failed, so ask user what device there are using.
fn.end

return

!------------------------------------------------
! Help data for the game.
!------------------------------------------------
read.data ""
read.data "Playing Clear Blocks:"
read.data "     Your mission is to clear as many of the coloured blocks as possible."
read.data "You have to clear a row at a time. Clear row three before clearing row two and finally clear row one."
read.data "When you clear all the blocks, you get a new set of blocks."
read.data ""
read.data "Time:"
read.data "		You have two minutes"
read.data ""
read.data "Points:"
read.data "		You gain 10 points each time you clear a block."
read.data "When you clear all the blocks you gain 100 bonus points and a new set of blocks"
read.data ""
read.data "Game Over:"
read.data "		The game is over when you run out of time or you fire at a block that as previously been cleared."
read.data ""
read.data "Controls:"
read.data "     Tip your device left to move the cannon left."
read.data "     Tip your device right to move the cannon right."
read.data "     Tap the screen to fire." 
read.data ""
read.data "Menu:"
read.data "		Tap the Back Key to access the Menu, this also pauses the game."
read.data "		From the menu, you can:"
read.data "		Reset the Best Score to zero."
read.data "		Read About or Help."
read.data "		Go back to the game."
read.data "		Exit the game."
read.data ""
read.data "	------------------------------------------------------------"
read.data "STOP"