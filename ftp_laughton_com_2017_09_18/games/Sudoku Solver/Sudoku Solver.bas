REM Sudoku Solve

apptitel$ = "Sudoku Solver"
appversion$ = "0.30"

CONSOLE.TITLE apptitel$ + " - " + appversion$

CLS

INPUT "Bitte gib die komplette URL ein:",url$

IF url$ = "bsp" THEN
 url$ = " http://www.google.com/goggles/a/sudoku?original=002030907003980000900015380600840739890703065017659040008004203400060500050078406&solved=182436957543987621976215384625841739894723165317659842768594213439162578251378496&solutionOnly=y "
ENDIF

S$="&solved="
x=IS_IN(S$,url$)+LEN(s$)
s$="&solutionOnly=y"
y=IS_IN(S$,url$)
zahlenkette$=MID$(url$,x,y-x)

z = 1

FOR i = 1 TO 9

 z = (i * 9)-8
 komplett$ = MID$(zahlenkette$,z,9)

 reihe$ = MID$(komplett$,1,3) + "|"+MID$(komplett$,4,3)+"|"+MID$(komplett$,7,3)

 PRINT INT$(i) + ". Reihe: |" + reihe$ +"|"

NEXT i

REM http://www.google.com/goggles/a/sudoku?original=002030907003980000900015380600840739890703065017659040008004203400060500050078406&solved=182436957543987621976215384625841739894723165317659842768594213439162578251378496&solutionOnly=y
