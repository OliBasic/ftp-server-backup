! Addicting game 
fn.def buttons$()
GR.SCREEN width, height
whalf = FLOOR(width/2)
hhalf = FLOOR(height/2)
dim pad[2]
array.load b$[],"play again","quit"
sq=height/10

for i=1 to 2
GR.TEXT.DRAW pad[i],whalf,hhalf+hhalf*i*0.3, b$[i]
next i
gr.render
 found=0
do
 FOr i=1 to 2
  GR.GET.POSITION pad[i],sx,sy
  GR.BOUNDED.TOUCH touched, sx-sq, sy-sq, sx+sq, sy+sq
  IF touched
   let found=1
   F_N.BREAK
  ENDIF
 NEXT I
pause 50
until found
fn.rtn b$[i]
fn.end

FN.DEF stats(l,best,av)
 f$="adscores.txt"
 best=0
sum=0
av=0
 FILE.EXISTS isoldfile, f$ 
 IF isoldfile
  TEXT.OPEN r,fh3,f$
  DO
   TEXT.READLN fh3, a$
   IF a$<>"EOF"
    level=VAL(WORD$(a$,1,CHR$(9)))
    t=VAL(WORD$(a$,2,CHR$(9)))
    IF level=l
     best=MAX(best,t)
     sum+=t
     n++
    ENDIF
   ENDIF
  UNTIL a$="EOF"
  IF n>0 THEN av=floor(1000*sum/n)/1000

 ENDIF
 FN.RTN 0

FN.END

ARRAY.LOAD Response$[], "1 - slow","2","3","4","5","6","7- fast"
Nbox=4
Drag=0

DIM bx[nbox],by[nbox]   % box coords
DIM vx[nbox],vy[nbox]   % box velocities
DIM bh[nbox],bw[nbox]   % box dimensions
DIM box[nbox]           % box objects
Games=0

Playagain:
Cantouch=0
Games++

Msg$="select game speed"

DIALOG.SELECT rnum, response$[], msg$

CLS
GR.OPEN 255, 0, 0, 0
PAUSE 500
GR.ORIENTATION 1
GR.SET.ANTIALIAS 0
GR.SCREEN awidth, aheight
Width=awidth
Height=aheight
S= (3*rnum)*(width/480)

Samp=1

whalf = FLOOR(width/2)
hhalf = FLOOR(height/2)
wqtr = FLOOR(width/4)
hqtr = FLOOR(height/4)
hthird= FLOOR(height/3)
wthird=FLOOR(width/3)

sq=width/16

vx[1]=s
vy[1]=s
bx[1]=1
by[1]=1
bw[1]=sq*5
bh[1]=sq*2

vx[2]=-s/2
vy[2]=s/3
bx[2]=width-sq*2
by[2]=1
bw[2]=sq*2
bh[2]=sq*4

vx[3]=s/3
vy[3]=-s
bx[3]=1
by[3]=height-sq*2
bw[3]=sq*2
bh[3]=sq*2

vx[4]=-s/3
vy[4]=-s/2
bx[4]=width-sq*4
by[4]=height-sq*5
bw[4]=sq*4
bh[4]=sq*5


Wallwidth=FLOOR(0.25*sq)
gameend=0



GR.COLOR 255,100,100,100,1  % grey frame
GR.SET.STROKE 0
GR.RECT tt, 1, 1, width, wallwidth
GR.RECT bb, 1, height-wallwidth, width, height
GR.RECT rr, width-wallwidth, 1, width, height
GR.RECT ll, 1, 1, wallwidth, height


GR.SET.STROKE 0

FOR i=1 TO 4
 GR.COLOR 255,0,0,255,1  % blue boxes
 GR.RECT box[i],bx[i],by[i],bx[i]+bw[i], by[i]+bh[i]
 GR.SHOW box[i]
NEXT i

GR.TEXT.TYPEFACE 2
GR.COLOR 255,200,200,00,1
gr.text.size 28
gr.text.bold 1
! GR.TEXT.DRAW info,30,35,"---"


sx=width/2
sy=height/2


! red square player
GR.COLOR 255,255,0,0,1
GR.RECT player , sx-sq, sy-sq, sx+sq, sy+sq
GR.SHOW player

gr.color 0,200,0,0
gr.rect chkPlayerWall, wallwidth+2*sq, wallwidth+2*sq, width-wallwidth-2*sq, height-wallwidth-2*sq


IF games=1
 POPUP "Move the red square and avoid touching the blue rectangles and walls", 0,-hqtr,0
ENDIF

GR.RENDER
DO
 GR.BOUNDED.TOUCH touched, sx, sy, sx+2*sq, sy+2*sq
UNTIL touched

tstart=CLOCK()

Cantouch=1
tic = CLOCK()
!------------------------------------------
DO

! process blue boxes ---------------
 FOR i=1 TO nbox

  GR.GET.POSITION box[i],fx,fy

  IF fx+bw[i] >width  | fx<1 THEN LET vx[I]=-vx[I]
  IF fy+bh[i] >height | fy<1 THEN LET vy[I]=-vy[I] 
  
  GR.MOVE box[i], vx[I], vy[I]
  IF GR_COLLISION(player,box[i]) THEN Tend=CLOCK()

 NEXT i

 !process touch ---------------------
 GR.TOUCH touched,x,y
 IF  touched & !drag                          THEN GOSUB touch
 IF  touched &  drag & ABS(x-ox)+ABS(y-oy) >3 THEN GOSUB hold
 IF !touched &  drag                          THEN Drag=0

 !check collision player <> wall ----
 if !GR_COLLISION(player, chkPlayerWall ) THEN  Tend =CLOCK()

 GR.RENDER

 !timing ---------------
 tocfilt += (toc-tocfilt)*0.025
! GR.MODIFY info, "text", "looptime: "+ int$(round(tocfilt))
 LET toc = CLOCK()-tic
 PAUSE max (33 - toc,1)
 LET tic = CLOCK()

UNTIL tend
 !-------------------------------------


 Newsl=(tend-tstart) /1000

GR.SET.ANTIALIAS 1
 GR.COLOR 255, 0, 255,0,1  % green text
 GR.TEXT.SIZE 30
 GR.TEXT.ALIGN 2
 GR.TEXT.DRAW txtend,whalf,hhalf*0.2,"game over"
 GR.TEXT.ALIGN 2
 GR.TEXT.DRAW txtend,whalf,hhalf*0.4,"speed "+ INT$(rnum)
 T$= STR$(newsl) +" secs"
 GR.TEXT.DRAW txt,whalf,hhalf*0.6, t$


 TEXT.OPEN A, FN1, "adscores.txt"
 TEXT.WRITELN FN1, INT$(rnum) +CHR$(9)+STR$(newsl)
 TEXT.CLOSE FN1
best=0
avg=0
stats(rnum,&best,&avg)
T$= "best:"+STR$(best) +" secs"
 GR.TEXT.DRAW txt,whalf,hhalf*0.7, t$
T$= "avg:"+STR$(avg) +" secs"
 GR.TEXT.DRAW txt,whalf,hhalf*0.8, t$


do
gr.touch touched,x,y
pause 100
until !touched

b$=buttons$()


 
 IF b$="play again" THEN
  GR.CLOSE
pause 300
  Tend=0
  drag=0
  GOTO playagain
 ENDIF

 EXIT

!-----------------------------------
 touch:
 LET ox=x
 LET oy=y
 LET Drag=1
 RETURN
 
 hold:
  gr.move player,x-ox, y-oy
  LET ox=x
  LET oy=y
 RETURN
!-----------------------------------
