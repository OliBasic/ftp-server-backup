! Addicting game 

ARRAY.LOAD Response$[], "1 - slow","2","3","4","5","6","7- fast"
Nbox=4
Drag=0

DIM bx[nbox],by[nbox]   % box coords
DIM vx[nbox],vy[nbox]   % box velocities
DIM bh[nbox],bw[nbox]   % box dimensions
DIM box[nbox]           % box objects
Games=0

Playagain:
Cantouch=0
Games++

Msg$="select game speed"

DIALOG.SELECT rnum, response$[], msg$

CLS
GR.OPEN 255, 0, 0, 0
PAUSE 1000
GR.ORIENTATION 1
GR.SET.ANTIALIAS 0
GR.SCREEN awidth, aheight
Width=awidth
Height=aheight
S= (3*rnum-1)*(width/480)

Samp=1

whalf = FLOOR(width/2)
hhalf = FLOOR(height/2)
wqtr = FLOOR(width/4)
hqtr = FLOOR(height/4)
hthird= FLOOR(height/3)
wthird=FLOOR(width/3)

sq=width/16

vx[1]=s
vy[1]=s
bx[1]=1
by[1]=1
bw[1]=sq*5
bh[1]=sq*2

vx[2]=-s/2
vy[2]=s/3
bx[2]=width-sq*2
by[2]=1
bw[2]=sq*2
bh[2]=sq*4

vx[3]=s/3
vy[3]=-s
bx[3]=1
by[3]=height-sq*2
bw[3]=sq*2
bh[3]=sq*2

vx[4]=-s/3
vy[4]=-s/2
bx[4]=width-sq*4
by[4]=height-sq*5
bw[4]=sq*4
bh[4]=sq*5


Wallwidth=FLOOR(0.25*sq)
gameend=0

GR.TEXT.TYPEFACE 2

GR.COLOR 255,100,100,100,1  % grey frame
GR.SET.STROKE 0
GR.RECT tt, 1, 1, width, wallwidth
GR.RECT bb, 1, height-wallwidth, width, height
GR.RECT rr, width-wallwidth, 1, width, height
GR.RECT ll, 1, 1, wallwidth, height

gr.text.size 28
gr.text.bold 1
GR.TEXT.DRAW info,30,35,"---"


GR.SET.STROKE 0

FOR i=1 TO 4
 GR.COLOR 255,0,0,255,1  % blue boxes
 GR.RECT box[i],bx[i],by[i],bx[i]+bw[i], by[i]+bh[i]
 GR.SHOW box[i]
NEXT i


sx=width/2
sy=height/2


! red square player
GR.COLOR 255,255,0,0,1
GR.RECT player , sx-sq, sy-sq, sx+sq, sy+sq
GR.SHOW player

IF games=1
 POPUP "Move the red square and avoid touching the blue rectangles and walls", 0,-hqtr,0
ENDIF

GR.RENDER
DO
 GR.BOUNDED.TOUCH touched, sx, sy, sx+2*sq, sy+2*sq
UNTIL touched


c1 = width - wallwidth
c2 = height-wallwidth
ww = wallwidth
tstart=CLOCK()

Cantouch=1
tic = CLOCK()
!------------------------------------------
DO

 FOR i=1 TO nbox

  GR.GET.POSITION box[i],fx,fy

  IF fx+bw[i] >width  | fx<1 THEN LET vx[I]=-vx[I]
  IF fy+bh[i] >height | fy<1 THEN LET vy[I]=-vy[I] 
  
  GR.MOVE box[i], vx[I], vy[I]
  IF GR_COLLISION(player,box[i]) THEN Tend=CLOCK()

 NEXT i

 GR.RENDER

 !touchit ---------------------
 GR.TOUCH touched,x,y
 IF  touched & !drag                          THEN GOSUB touchit_1
 IF  touched &  drag & ABS(x-ox)+ABS(y-oy) >3 THEN GOSUB touchit_2
 IF !touched &  drag                          THEN Drag=0


 !collisions ------------------
 GR.GET.POSITION player,sx , sy
 LET Sx=sx+sq
 LET Sy=sy+sq
 IF sx-sq<ww | sx+sq>c1 | sy-sq<ww | sy+sq>c2 THEN  Tend =CLOCK()


 !timing ---------------
 tocfilt += (toc-tocfilt)*0.1
 GR.MODIFY info, "text", "looptime: "+int $(tocfilt)
 LET toc = CLOCK()-tic
 PAUSE max (33 - toc,1)
 LET tic = CLOCK()

UNTIL tend

 !-------------------------------------


 Newsl=(tend-tstart) /1000


 GR.COLOR 255, 0, 255,0,1  % green text
 GR.TEXT.SIZE 30
 GR.TEXT.ALIGN 2
 GR.TEXT.DRAW txtend,whalf,hhalf*0.2,"game over"
 GR.TEXT.ALIGN 2
 GR.TEXT.DRAW txtend,whalf,hhalf*0.4,"speed "+ INT$(rnum)
 T$= STR$(newsl) +" secs"
 GR.TEXT.DRAW txt,whalf,hhalf*0.6, t$
 GR.RENDER

 TEXT.OPEN A, FN1, "adscores.txt"
 TEXT.WRITELN FN1, INT$(rnum) +CHR$(9)+STR$(newsl)
 TEXT.CLOSE FN1

 PAUSE 2000

 DIALOG.MESSAGE , "play again?", c, "yes", "no"

 IF c=1 THEN
  GR.CLOSE
  PAUSE 500
  Tend=0
  GOTO playagain
 ENDIF

 EXIT

 !-----------------------------------
 touchit_1:
 LET Ox=x
 LET Oy=y
 GR.GET.POSITION player,sx , sy

 LET Sx=sx+sq
 LET Sy=sy+sq
 LET Drag=1
 LET Dragx=x
 LET Dragy=y
 LET Bx=sx
 LET By=sy
 RETURN
 !-----------------------------------

 !-----------------------------------
 touchit_2:
 LET Xx=x-dragx
 LET Yy=y-dragy
 LET Sx=bx+ xx
 LET Sy=by+yy
 GR.MODIFY player,"left",sx-sq,"top",sy-sq,"right",sx+sq,"bottom",sy+sq

  LET Ox=x
  LET Oy=y
  RETURN
  !-----------------------------------
