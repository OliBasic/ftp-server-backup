Shifter is based on an old listing I found. The original (text-based) program was named 'Jumping Fallout'.
The rules are very simple:
Try to dock all the coloured balls shown on the top row by letting them fall though the grid.
This is done by left moving the bars of the grid. Tapping << next to the bar will move that bar one
position to the left. This continues untill you release <<.
As soon as the bar does not move anymore, balls above a gap will fall into this gap.
You start with 200 points. Each time you press a << you will get a penalty depending on the row number
(penalty=row number+1) .
Each move to the left will give you a penalty of 1 point. You can earn back 20 points by docking a ball
exactly on its starting horizontal position. Of course the aim of the game is to finish with as high a score as 
possible !

Have fun !


