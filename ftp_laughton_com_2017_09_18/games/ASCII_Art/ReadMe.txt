ASCII_Art

This programme creates so-called ASCII art pictures from a given 'normal' picture.
See the examples.

The operation is simple:
Place a picture in the data directory.
Run the programme.
Choose options and then select the picture.
After finishing (may take a while !) the resulting ASCII art version is placed in the data directory.

The options are:
-Use characters in either Landscape or Portrait (see note)
-Use colour or Black & White

The name of the ASCII art file consists of the original file name plus the addition of
_BW_ART for B&W or _Col_ART for colour.

The programme needs the file GV.txt in the data directory (contains the characters to be used).

Note:
use pictures of size 720x960.
If you have a picture of 960x720 (Landscape), use an external programme to rotate it first and
use Landscape characters (Rotate the result file back afterwards).

Have fun, Aat
