! ASCII art
! Aat Don @2013
DataPath$="../../ASCII_Art/data/"
INPUT "Use P(ortrait) or L(andscape) characters",Ori$,"P"
INPUT "Use C(olour) or B(lack/White)",Col$,"B"
M$="_BW"
IF UPPER$(LEFT$(Col$,1))="C" THEN M$="_Col"
TestScale=1
GR.OPEN 255,255,255,255,0,1
GR.SCREEN w, h
ScreenWidth=720/TestScale
ScreenHeight=960/TestScale
sX=w/ScreenWidth
sY=h/ScreenHeight
GR.SCALE sX,sY
GR.CLS
DIM Grey[256,3]
GR.COLOR 255,0,0,0,1
GR.TEXT.SIZE 27/TestScale
GR.TEXT.TYPEFACE 2
GR.SET.ANTIALIAS 0
GR.TEXT.BOLD 1
! Get file
FILE.EXISTS DirPresent, DataPath$
IF DirPresent=0 THEN
	FILE.MKDIR DataPath$
ENDIF
FILE.DIR DataPath$, Namen$[]
Array.length NumFiles, Namen$[]
IF NumFiles<1 THEN
	PRINT "No files found"
	END
ENDIF
SELECT art, Namen$[], "Select a file"
FName$=Namen$[art]
Array.DELETE Namen$[]
TIME Year$,Month$,Day$,Hour$,Minute$,Second$
PRINT "Start ";Hour$;":";Minute$;":";Second$
GR.BITMAP.LOAD BG,DataPath$+FName$
GR.BITMAP.SIZE BG,Pwidth,Pheight
GR.BITMAP.SCALE BGs,BG,720/TestScale,960/TestScale
GR.BITMAP.DRAW g,BGs,0,0
GR.RENDER

IF UPPER$(LEFT$(Ori$,1))="P" THEN
	! Portrait letters
	yP=936/TestScale
	YSt=24/TestScale
	xP=702/TestScale
	XSt=18/TestScale
	jP=(24-1)/TestScale
	iP=(18-1)/TestScale
	xR=18/TestScale
	yR=24/TestScale
	rG=0
	yT=(24-1)/TestScale
ELSE
	! Landscape letters
	yP=942/TestScale
	YSt=18/TestScale
	xP=696/TestScale
	XSt=24/TestScale
	jP=(18-1)/TestScale
	iP=(24-1)/TestScale
	xR=24/TestScale
	yR=18/TestScale
	rG=90
	yT=0
ENDIF

TEXT.OPEN R,FN1,DataPath$+"GV.txt"
	TEXT.READLN FN1,Regel$
	FOR i=1 TO 256
		TEXT.READLN FN1,Regel$
		Grey[i,1]=VAL(WORD$(Regel$,2))
		Grey[i,2]=VAL(WORD$(Regel$,3))
		Grey[i,3]=VAL(WORD$(Regel$,4))
	NEXT i
TEXT.CLOSE FN1

FOR y=0 TO yP STEP YSt
	FOR x=0 TO xP STEP XSt
		AvgR=0
		AvgG=0
		AvgB=0
		q=0
		FOR j=y TO y+jP
			FOR i=x TO x+iP
				GR.GET.BMPIXEL BGs,i,j,a,r,g,b
				AvgR=(q*AvgR+r)/(q+1)
				AvgG=(q*AvgG+g)/(q+1)
				AvgB=(q*AvgB+b)/(q+1)
				q=q+1
			NEXT i
		NEXT j
		GV=ROUND((AvgR+AvgG+AvgB)/3)
		! Look up parameters for ASCII char
		IF Grey[GV+1,3]=1 THEN
			GR.COLOR 255,0,0,0,1
		ELSE
			GR.COLOR 255,255,255,255,1
		ENDIF
		GR.RECT g,x,y,x+xR,y+yR
		IF M$="_Col" THEN
			GR.COLOR 255,AvgR,AvgG,AvgB,1
		ELSE
			IF Grey[GV+1,3]=1 THEN
				GR.COLOR Grey[GV+1,2],255,255,255,1
			ELSE
				GR.COLOR Grey[GV+1,2],0,0,0,1
			ENDIF
		ENDIF
		GR.ROTATE.START rG,x,y
			GR.TEXT.DRAW g,x,y+yT,CHR$(Grey[GV+1,1])
		GR.ROTATE.END
	NEXT x
	GR.RENDER
NEXT Y
TIME Year$,Month$,Day$,Hour$,Minute$,Second$
PRINT "Ready ";Hour$;":";Minute$;":";Second$
GR.SAVE DataPath$+REPLACE$(FName$,".",M$+"_ART.")
DO
	GR.TOUCH touched,x,y
UNTIL touched
DO
	GR.TOUCH touched,x,y
UNTIL !touched
GR.CLOSE
END