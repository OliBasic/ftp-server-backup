Rem Simon Says
Rem For Android
Rem With RFO Basic!
Rem August 2016
Rem Version 1.00
Rem By Roy Shepherd

di_height = 1152 % set to my Device
di_width = 672

gr.open 255,128,128,255
gr.orientation 1 % portrate 
pause 1000
WakeLock 3
 
gr.screen screenWidth, screenHeight
scale_x = screenWidth / di_width
scale_y = screenHeight / di_height
gr.scale scale_x, scale_y

!-------------------------------------------------
!Let Basic see the functions before they are called
!-------------------------------------------------
gosub Functions

gosub Initialise

gosub SetUp
gosub PlayGame

onBackKey:
    dialog.message "Simon Says",,yn,"Exit","Help","Cancel"
    if yn = 1 then gosub SaveData : exit
    if yn = 2 then gosub ShowHelp
     
back.resume 
end

!------------------------------------------------
! Do once at first run of game
!------------------------------------------------
Initialise:
  !Set some variables 
  FALSE = 0 : TRUE = 1
  path$ = "../../SimonSays/data/"
  sb = di_height - 45  : bb = di_height - 5
  sound = 1 : buzz = 1 : level = 1 : delay = 500
  dim scores[4], highScores[4], h[16]
  for x = 1 to 4 : highScores[x] = 0 : next
 
  ! Load highScores, level, sound and buzz from previous game
  gosub LoadData 
  
  ! List
  list.create n, SimonSays
  
  ! sounds and buzz
  soundpool.open 4
  soundpool.load soundClick,path$ + "buttonClick.wav"
  soundpool.load soundNewHigh, path$ + "newHigh.wav"
  soundpool.load soundNotHigh, path$ + "notHigh.wav"
  soundpool.load cnote, path$ + "c.mp3"
  if cnote = 0 then do : dialog.message "Sound file c.mp3 not found",,ok,"OK" : until ok > 0 : exit
  array.load freq[],261,293,330,349,392,440,494,523,586,660,698
  array.load buzzGame[], 1, 100
  
  gosub Help
  
  !Top and bottom bar
  gr.color 255,79,79,255
  gr.rect null, 0,0,di_width, 55
  gr.rect null, 0,di_height - 50,di_width, di_height
  
  ! Score, High Score and info
  gr.text.size 40 : gr.color 255,255,255,255
  gr.text.draw txtScore, 20, 40, "Score: " + int$(score)
  gr.text.align 3
  gr.text.draw txtHighScore, di_width - 20, 40, "High Score: " + int$(highScores[level])
  gr.text.align 2 : gr.color 255,255,255,36
  gr.text.draw txtInfo, di_width / 2, 90, ""
  gr.text.align 1
   
  !Bottom Buttons
  gr.text.size 27
  gr.color 255,128,128,255
  gr.rect butMusic, 20, sb, 170, bb
  gr.rect butSound, 180, sb, 330, bb
  gr.rect butBuzz, 340, sb, 490, bb
  gr.rect butReset, 500, sb, 650, bb
  
  !Text on bottom buttons
  gr.color 255,200,200,200
  gr.paint.get gray % To gray out level button when it can not be used
  gr.color 255,255,255,255
  gr.paint.get white
  gr.text.align 2 
  lev$ = "Level " + int$(level)
  gr.text.draw txtLevel, 55,bb - 10,lev$
  if sound then s$ = "Sound Off" else s$ = "Sound On"
  gr.text.draw txtSound, 255,bb - 10, s$
  if buzz then b$ ="Buzz Off" else b$ = "Buzz On"
  gr.text.draw txtBuzz, 415,bb - 10,b$
  gr.text.draw txtReset, 575,bb - 10,"Reset Score"
   
  !Using gr_collision for when a button is tapped
  gr.color 0,0,0,0
  gr.point collision, - 1, - 1 
  
 gosub DrawBoard
 gr.render
 gosub Help
return

!------------------------------------------------
! Do at start of each new game
!------------------------------------------------
SetUp:
  gameOver = FALSE
  for x = 1 to 4 : scores[x] = 0 : next
  gameInPlay = FALSE
   
  gr.modify txtLevel, "paint", white : gr.render
  list.clear SimonSays
return

!------------------------------------------------
! Draw the Simon Says game board
!------------------------------------------------
DrawBoard:
  gr.color 255,150,150,255
  gr.rect null, 100,100, di_width - 100, di_height - 100
  Gr.set.stroke 4 : gr.color 255, 0, 0, 0, 0
  gr.paint.get black
  gr.rect null, 100,100, di_width - 100, di_height - 100
  
  ! Red
  gr.color 155,255,85,85, 1
  gr.paint.get paleRed
  
  gr.color 255,255,85,85
  gr.paint.get red
  
  ! Yellow
  gr.color 155,255,255,36
  gr.paint.get paleYellow
  
  gr.color 255,255,255,36
  gr.paint.get yellow
  
  ! Blue
  gr.color 155,118,104,242
  gr.paint.get paleBlue
  
  gr.color 255,118,104,242
  gr.paint.get blue
  
  ! Green
  gr.color 155,0,251,0
  gr.paint.get paleGreen
  
  gr.color 255,0,251,0
  gr.paint.get green
  
  ! Top left colour
  gr.color 255,0,0,0
  gr.rect topLeft, 130,130, (di_width / 2) - 30, (di_height / 2) - 50
  
  gr.rect topLeft, 130,130, (di_width / 2) - 30, (di_height / 2) - 50
  gr.modify topLeft, "paint", paleRed
  gr.rect boarder, 130,130, (di_width / 2) - 30, (di_height / 2) - 50
  gr.modify boarder, "paint", black
 
  ! Top right colour
  gr.color 255,0,0,0
  gr.rect topRight, (di_width / 2) + 30, 130, di_width - 130, (di_height / 2) - 50
  
  gr.rect topRight, (di_width / 2) + 30, 130, di_width - 130, (di_height / 2) - 50
  gr.modify topRight, "paint", paleYellow
  gr.rect boarder, (di_width / 2) + 30, 130, di_width - 130, (di_height / 2) - 50
  gr.modify boarder, "paint", black

  ! Bottom left
  gr.color 255,0,0,0
  gr.rect bottomLeft, 130, (di_height / 2) + 50, (di_width / 2) - 30, di_height - 130
  
  gr.rect bottomLeft, 130, (di_height / 2) + 50, (di_width / 2) - 30, di_height - 130
  gr.modify bottomLeft, "paint", paleGreen
  gr.rect boarder, 130, (di_height / 2) + 50, (di_width / 2) - 30, di_height - 130
  gr.modify boarder, "paint", black
  
  ! Bottom right
  gr.color 255,0,0,0
  gr.rect bottomRight, (di_width / 2) + 30, (di_height / 2) + 50, di_width - 130, di_height - 130
  
  gr.rect bottomRight, (di_width / 2) + 30, (di_height / 2) + 50, di_width - 130, di_height - 130
  gr.modify bottomRight, "paint", paleBlue
  gr.rect boarder, (di_width / 2) + 30, (di_height / 2) + 50, di_width - 130, di_height - 130
  gr.modify boarder, "paint", black
  
  gr.oval go, (di_width / 2) - 60, (di_height / 2) - 40 , (di_width / 2) + 60, (di_height / 2) + 40
  gr.modify go, "paint", red
  gr.oval boarder, (di_width / 2) - 60, (di_height / 2) - 40 , (di_width / 2) + 60, (di_height / 2) + 40
  gr.modify boarder, "paint", black
  gr.text.align 2 : gr.text.size 40 : gr.color 255,255,255,255
  gr.text.draw txtStart, di_width / 2, (di_height / 2) + 10, "Start"

return

!------------------------------------------------
! Play Simon Says
!------------------------------------------------
PlayGame:
  do 
    gr.modify txtStart, "text", "Start" : gr.render
      do
        gr.touch t, tx, ty
        if t then 
          do : gr.touch t, tx, ty : until ! t
          tx /= scale_x : ty /= scale_y
          gr.modify collision, "x", tx, "y", ty : gr.render
          gosub BottomButton
        endif
      until gr_collision(collision, txtStart)
      if buzz then vibrate buzzGame[], -1
      call PlaySound(sound,soundClick)
      gameInPlay = TRUE
      gr.modify txtScore, "text", "Score: " + int$(scores[level]) 
     
      gr.modify txtInfo, "text", ""
      gr.modify txtLevel, "paint", gray : gr.render
      do
        ! Add to SimonSays list
        simon = floor(4 * rnd() + 1)
        list.add SimonSays, simon
        gosub PlaySimonList
        gosub PlayerRepeat
      until gameOver
      gosub SetUp
    until TRUE = FALSE
return

!------------------------------------------------
! Play the colours and sounds in SimonSays list
!------------------------------------------------
PlaySimonList:
  list.size SimonSays, colours
  gr.modify txtStart, "text", "Simon"
  pause 1000
  SimonPlaying = TRUE
  for x = 1 to colours
    list.get SimonSays, x, colourNum
    gosub LightUpColour
  next
  SimonPlaying = FALSE
return

!------------------------------------------------
! Light up the colours and play a note
!------------------------------------------------
LightUpColour:
  if sound then soundpool.play stream, cnote, 0.9, 0.9,0,0,freq[colourNum]/261
    sw.begin colourNum
      sw.case 1
        gr.modify topLeft, "paint", red : gr.render
        pause delay 
        gr.modify topLeft, "paint", paleRed : gr.render
      sw.break
      
       sw.case 2
        gr.modify topRight, "paint", yellow : gr.render
        pause delay 
        gr.modify topRight, "paint", paleYellow : gr.render
      sw.break
      
       sw.case 3
        gr.modify bottomLeft, "paint", green : gr.render
        pause delay 
        gr.modify bottomLeft, "paint", paleGreen : gr.render
      sw.break
      
       sw.case 4
        gr.modify bottomRight, "paint", blue : gr.render
        pause delay 
        gr.modify bottomRight, "paint", paleBlue : gr.render
      sw.break
    sw.end  
    if SimonPlaying then pause 500
  
return

!------------------------------------------------
! Player as to repeat SimonSays list
!------------------------------------------------
PlayerRepeat:
  !dialog.message "Repeat list",,ok,"OK"
  gr.modify txtStart, "text", "Player" : gr.render
  flag = FALSE
  list.size SimonSays, colours
  x = 1
  do
    flag = FALSE
    gr.touch t, tx, ty
    if t then
      do : gr.touch t, tx, ty : until ! t
      tx /= scale_x : ty /= scale_y
      gr.modify collision, "x", tx, "y", ty : gr.render
      if gr_collision(collision,topLeft) then userClick = 1 : flag = TRUE
      if gr_collision(collision,topRight) then userClick = 2 : flag = TRUE
      if gr_collision(collision,bottomLeft) then userClick = 3 : flag = TRUE
      if gr_collision(collision,bottomRight) then userClick = 4 : flag = TRUE
      if flag then if buzz then vibrate buzzGame[], -1
      if ! flag then gosub BottomButton
      if flag then
        list.get SimonSays, x, colourNum
        if colourNum = userClick then 
          gosub LightUpColour : x ++
         
        endif
        if colourNum <> userClick then 
          gameOver = TRUE
          if scores[level] > highScores[level] then
            highScores[level] = scores[level]
            gr.modify txtInfo, "text", "New High Score"
            call PlaySound(sound, soundNewHigh)
            gr.modify txtHighScore, "text", "High Score: " + int$(highScores[level])
            gr.render
          else
           gr.modify txtInfo, "text", "You didn't beat the High Score"
            call PlaySound(sound, soundNotHigh)
          endif
        endif
      endif
    endif
 until x > colours & flag | gameOver
 if ! gameOver then 
  scores[level] ++
  gr.modify txtScore, "text", "Score: " + int$(scores[level])
  gr.render
endif
return

!------------------------------------------------
! Advance the game level
!------------------------------------------------
ModLevel:
  if ! gameInPlay then 
    if buzz then vibrate buzzGame[], -1
    if level < 4 then level ++ else level = 1
    gr.modify txtLevel, "text", "Level: " + int$(level) 
    gr.modify txtHighScore, "text", "High Score: " + int$(highScores[level])
    gr.render
    if level = 1 then delay = 500
    if level = 2 then delay = 250
    if level = 3 then delay = 125
    if level = 4 then delay = 65
  endif
return

!------------------------------------------------
! Turn sound on and off
!------------------------------------------------
ModSound:
  if buzz then vibrate buzzGame[], -1
  call PlaySound(sound,soundClick)
  if sound then
    sound = FALSE
    gr.modify txtSound, "text", "Sound: On"
  else
    sound = TRUE
    gr.modify txtSound, "text", "Sound: Off"
  endif
  gr.render 
return

!------------------------------------------------
! Turn buzz on and off
!------------------------------------------------
ModBuzz:
  if buzz then vibrate buzzGame[], -1
  call PlaySound(sound,soundClick)
  if buzz then
    buzz = FALSE
    gr.modify txtBuzz, "text", "Buzz: On"
  else
    buzz = TRUE
    gr.modify txtBuzz, "text", "Buzz: Off"
  endif
  gr.render
return

!------------------------------------------------
! Reset the player's scores to zero 
!------------------------------------------------
ModReset:
  if buzz then vibrate buzzGame[], -1
  call PlaySound(sound,soundClick)
  do : dialog.message "Reset High Score to Zero?",,yn,"Yes","No" : until yn > 0
  if buzz then vibrate buzzGame[], -1
  call PlaySound(sound,soundClick)
  if yn = 1 then 
    highScores[level] = 0
    gr.modify txtHighScore, "text", "High Score: " + int$(highScores[level])
  endif
  gr.render
return

!------------------------------------------------
! Test to see if a bottom botton has been tapped
!------------------------------------------------
BottomButton:
  gr.touch t, tx, ty 
  tx /= scale_x : ty /= scale_y
  gr.modify collision, "x", tx, "y", ty : gr.render
  if gr_collision(collision,butMusic) then gosub ModLevel
  if gr_collision(collision,butSound) then gosub ModSound
  if gr_collision(collision,butBuzz) then gosub ModBuzz
  if gr_collision(collision,butReset) then gosub ModReset
return


!-------------------------------------------------
!Save High Scores, level, sound and buzz At end of game
!-------------------------------------------------
SaveData:
	Path$="../../SimonSays/data/"
	file.exists pathPresent,Path$+"GameData.txt"
	if ! pathPresent then file.mkdir Path$
  
		text.open w,hs,Path$+"GameData.txt"
    for x = 1 to 4
      text.writeln hs,int$(highScores[x])
    next
    text.writeln hs,int$(level)
		text.writeln hs,int$(sound)
    text.writeln hs,int$(buzz)
		text.close hs
return

!-------------------------------------------------
! Load High Scores, level, sound and buzz. At start of game
!-------------------------------------------------
LoadData:
	Path$="../../SimonSays/data/"
  dim highScoreTemp$[4]
	file.exists pathPresent,Path$+"GameData.txt"
	if pathPresent then
    text.open r,hs,Path$+"GameData.txt"
    for x = 1 to 4
      text.readln hs,highScoreTemp$[x]
    next
    text.readln hs,level$
    text.readln hs,sound$
    text.readln hs,buzz$
    text.close hs
    for x = 1 to 4
      highScores[x] = val(highScoreTemp$[x])
    next
    level = val(level$)
    sound = val(sound$)
    buzz = val(buzz$)
	endif
return

!------------------------------------------------
! The help screen, scane it for later 
!------------------------------------------------
Help:
  
  gr.color 155,128,128,255
  gr.rect h[1], 0, 0, di_width, di_height
  gr.text.align 2 : gr.text.size 40
  gr.color 255,255,255,255
  
  gr.text.draw h[2], di_width / 2, 100, "Simon Memory Game"
  gr.text.align 1
  gr.text.draw h[3], 50, 150, "Play this Simon Says game and"
  gr.text.draw h[4], 50, 200, "have fun testing your memory."
  gr.text.draw h[5], 50, 250, "Follow the pattern of lights and"
  gr.text.draw h[6], 50, 300, "sounds and repeat the same"
  gr.text.draw h[7], 50, 350, "combination to move on to the" 
  gr.text.draw h[8], 50, 400, "next round."

  gr.text.draw h[9], 50, 500, "It starts off easy but it won’t be"
  gr.text.draw h[10], 50, 550, "long before you’re questioning"
  gr.text.draw h[11], 50, 600, "your memory and struggling to"
  gr.text.draw h[12], 50, 650, "remember the pattern. Rack up"
  gr.text.draw h[13], 50, 700, "high scores, improve your memory"
  gr.text.draw h[14], 50, 750, "and enjoy this classic game."

  gr.text.draw h[15], 50, 800, "There are four skill levels."
  gr.text.align 2
  gr.text.draw h[16], di_width / 2, 900, "Tap to close"
  gr.text.align 1
  
  for x = 16 to 1 step - 1 : gr.hide h[x] : next
return

!------------------------------------------------
! Show help screen
!------------------------------------------------
ShowHelp:
  for hx = 1 to 16 : gr.show h[hx] : pause 100 : gr.render : next
  do : gr.touch t, tx, ty : until t
  do : gr.touch t, tx, ty : until ! t
  for hx = 16 to 1 step - 1 : gr.hide h[hx] : pause 100 : gr.render : next
return

!------------------------------------------------
! Functions in this gosub
!------------------------------------------------
Functions:

!------------------------------------------------
! if sound is on. Play sound (ptr)
!------------------------------------------------
  fn.def PlaySound(sound, ptr)
      if sound then Soundpool.play s,ptr,0.99,0.99,1,0,1
  fn.end
  
return