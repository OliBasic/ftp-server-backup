Rem Master Mind
Rem With RFO Basic!
Rem June 2015
Rem Version 1.00
Rem By Roy

di_height = 1230 % set to my Device
di_width = 800

gr.open
gr.set.AntiAlias 0
gr.orientation 1
pause 1000
WakeLock 3 

gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y

!----------------------------------
gr.color         255, 255,255,255, 1
gr.bitmap.create screen, di_width,di_height
gr.bitmap.draw    nn, screen, 0, 0
gr.bitmap.drawinto.start screen
!gr.rect          nn,0, 0, di_width,di_height
!----------------------------------

gr.set.stroke 4
gr.text.size 40
array.load colours[],0, 1, 2, 3, 4, 5, 6, 7
gosub Functions

do	
	win = 0
	gosub DrawBoard
	!gosub ShowColours
	win = PlayGame(di_height, screen, scale_x, scale_y, colours[])
	gosub FinalResults
until 0

onBackKey:
Dialog.Message"Menu",,yn,"Help","Canel","Exit"
if yn = 3 then WakeLock 5 : exit
if yn = 1 then gosub Help
back.resume

end

DrawBoard:
	gosub ClearScreen
	DrawPallet(di_width, di_height,screen)
	Rem Playing Board
	call Gcol(9,0)
	for y=1000 to 300 step - 50 
		for x = 100 to 340 step 75 
			gr.rect t, x, y, x + 75, y + 50  
		next 
	next
	
	Rem Score Board
	call CurvyWindow(438,298,722, 1052, 0)
	call CurvyWindow(440,300,720, 1050, 9)
	for y = 1025 to 325 step - 50 
		for x = 490 to 670 step 60
			call Gcol(7,1) : gr.circle c, x, y, 20 
			call Gcol(0,0) : gr.circle c, x, y, 20 
		next 
	next
	array.shuffle colours[1,8]
	gr.render
return

Help:
     h$="Your device selects a four colour code from the eight available, with no repeat.\n\n"+ ~
	"You workout what code the device as selected, by tapping the colour pallet.\n\n"+~

     "After you have entered you code the device marks as follows:\n"+~
      "A right colour in the right place gets a Green Peg.\n"+~
      "A right colour in the wrong place gets a Red Peg.\n\n"+~

	"If after fifteen failed attempts to workout the code, your device takes pity on you and reverse the code.\n\n" 
	
	Dialog.Message"Help:",h$,ok,"OK"
return

ClearScreen:
	call Gcol(10, 1)
	gr.rect r, 0,0,di_width,di_height 
	gr.color 255,100,100,100,1 
	gr.rect r, 50,50,di_width-50,di_height-100
	gr.color 255,255,255,255,0
	gr.rect r, 50,50,di_width-50,di_height-100
	t$="M A S T E R M I N D" : Gr.text.width w ,t$ : call Gcol(1 , 1)
	gr.set.stroke 1 : gr.text.draw t ,(di_width/2) - (w/2), 40 ,t$ : call Gcol(8,0)
	gr.text.draw t ,(di_width/2) - (w/2), 40 ,t$ : gr.set.stroke 4
	gr.render
return

ShowColours:
	cc=1
	for x = 135 to 375 step 75 
		call Gcol(colours[cc], 1) : cc++
		gr.circle cir, x, 260, 25 
	next 
	call Gcol(4, 1): gr.text.draw d, 100,220, "Here's the Code:"
	gr.render	
return

FinalResults:
	flag = 0 : w$="Well Done!"
	if ! win then gosub ShowColours : w$="Bad Luck!"
	call CurvyWindow(53,53,di_width - 53, 183, 0)
	call CurvyWindow(55,55,di_width - 55, 180, 4)
	gr.text.align 2 
	call Gcol(0,1) : gr.text.draw d, di_width/2,90,"Game Over:  " + w$
	call Gcol(8, 1) : gr.rect r, 100,100, (di_width/2)-5,170
	gr.rect r, (di_width/2)+5,100, di_width - 100, 170
	
	call Gcol(0,1) : gr.text.draw d, (di_width/4)+50,150,"New Game"
	call Gcol(0,1) : gr.text.draw d, di_width - (di_width/4)-50,150,"Exit"
	
	call Gcol(0, 0) : gr.rect r, 105,105, (di_width/2)-10,165
	gr.rect r, (di_width/2)+10,105, di_width - 105, 165
	gr.render
	
	do
		do : gr.touch touched, tx, ty : until touched
		do : gr.touch touched, tx, ty : until ! touched
		tx/=scale_x : ty/=scale_y
		tx = int(tx) : ty = int(ty)
		if ty > 100 & ty < 170 then
			if tx> 100 & tx < (di_width/2)-5 then flag = 1
			if tx> (di_width/2)+5 & tx < di_width - 100 then WakeLock 5 : exit
		endif
	until flag
	gr.text.align 1
return

Functions:

fn.def CurvyWindow(x,y,l,h,c)
	call Gcol(c,1)
	gr.rect r,x,y+25,l,h-25 
	gr.rect r,x+25,y,l-25,h 
	gr.circle c, x+25,y+25,25 
	gr.circle c,l-25,y+25,25 
	gr.circle c,x+25,h-25,25
	gr.circle c,l-25,h-25,25
	gr.render
fn.end

fn.def PlayGame(di_height, screen, scale_x, scale_y,colours[])
	boxX = 150 : boxY = 1025 : win = 0 : c = 1
	array.load userSelect[],-1, -1, -1 ,-1
	do
		do : gr.touch touched, tx, ty : until touched
		do : gr.touch touched, tx, ty : until ! touched
		tx/=scale_x : ty/=scale_y
		tx = int(tx) : ty = int(ty)
		if ty > 1150 & ty < di_height - 9 & tx > 50 & tx < 650 then 
			userSelect[c] = Pixel(tx, ty)
			if ! UsedColour(userSelect[],c) & userSelect[c] <> 9 then
				!gr.bitmap.fill screen, boxX, boxY : gr.render
				call Gcol(userSelect[c],1)
				gr.rect r, boxX - 45, boxY - 21, boxX + 21, boxY + 21 : gr.render
				boxX += 75 : c++
				if boxX > 375 then 
					win = Results(colours[],boxY)
					if win < 4 then win = 0
					boxX = 150 : boxY -= 50 : c = 1
					for r = 1 to 4 : userSelect[r] = -1 : next
				endif
			endif
		endif
	until win | boxY < 325
	array.delete userSelect[]
	fn.rtn win
fn.end

fn.def UsedColour(userSelect[],c)
	flag = 0
	if c > 1 then 
		for x = 1 to c - 1 
			if userSelect[x] = userSelect[c] then 
				flag = 1 : Dialog.message"Colour Used",,ok,"OK"
			endif
		next
	endif
	fn.rtn flag
fn.end

fn.def Results(colours[],boxY)
	dim pixelColour[4]
	flag = 0 : c = 0 : c$ = "" : sx = 490
	for x = 150 to 375 step 75
		c++
		pixelColour[c] = Pixel(x,boxY)
		if pixelColour[c] = colours[c] then 
			flag ++ 
			c$ = c$ + "1" % right colour, right place
		endif
	next
		if flag <4 then
			for x = 1 to 4
				for y = 1 to 4
					if pixelColour[x] = colours[y] & x <> y then c$ = c$ + "2" % right colour, wrong place	
				next 
			next
		endif	
		for m = 1 to len(c$) 
			if mid$(c$,m,1) = "1" then call Gcol(2,1) : gr.circle c, sx, boxY, 20 : sx += 60 % green peg
			if mid$(c$,m,1) = "2" then call Gcol(1,1) : gr.circle c, sx, boxY, 20 : sx += 60 % red peg
		next
		gr.render
	array.delete pixelColour[]
	fn.rtn flag
fn.end

fn.def Gcol(c, style)
	if c=0 then gr.color 255,0,0,0,style % black
	if c=1 then gr.color 255,255,0,0,style % Red
	if c=2 then gr.color 255,0,255,0,style % green
	if c=3 then gr.color 255,0,0,255,style % blue
	if c=4 then gr.color 255,255,255,0,style % yellow
	if c=5 then gr.color 255,0,255,255,style % cyan
	if c=6 then gr.color 255,255,0,255,style % magenta
	if c=7 then gr.color 255,255,255,255,style % white
	if c=8 then gr.color 255,192,192,192,style % gray
	if c=9 then gr.color 255,180,180,180,style % dark gray 
	if c=10 then gr.color 255,218, 143, 255,style % Light magenta
fn.end

fn.def Pixel(xx,yy)
	colour = 0
   gr.get.bmpixel 1,xx, yy, alpha, red, green, blue
   if red = 0 & green = 0 & blue = 0 then colour = 0 % Black
   if red = 255 & green = 0 & blue = 0 then colour = 1 % Red
   if red = 0 & green = 255 & blue = 0 then colour = 2 % Green
   if red = 0 & green = 0 & blue = 255 then colour = 3 % Blue
   if red = 255 & green = 255 & blue = 0 then colour = 4 % Yellow
   if red = 0 & green = 255 & blue = 255 then colour = 5 % Cyan
   if red = 255 & green = 0 & blue = 255 then colour = 6 % Magenta
   if red = 255 & green = 255 & blue = 255 then colour = 7 % White
   if red = 192 & green = 192 & blue = 192 then colour = 8 % Gray
   if red = 180 & green = 180 & blue = 180 then colour = 9 % Dark Gray
   gr.color 255, red, green, blue
   fn.rtn colour
fn.end

fn.def DrawPallet(di_width, di_height,screen)
	c=0 : call Gcol(9, 0)
	for x=50 to 575 step 75
		call Gcol(c, 1) : gr.rect r ,x,di_height - 80, x + 75 , di_height - 5
		call Gcol(9, 0) : gr.rect r ,x,di_height - 80, x + 75 , di_height - 5
		!gr.bitmap.fill screen, x + 50, di_height - 50
		c++
	next
	gr.render
fn.end

return
