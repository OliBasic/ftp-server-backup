REM Start of BASIC! Program
rem tapdots
rem mookiebearapps 2017


FN.DEF setcolor(c)
 SW.BEGIN c
  SW.CASE 1 % red
   GR.COLOR 255,255,0,0
   SW.BREAK
  SW.CASE 2 % green
   GR.COLOR 255,0,255,0
   SW.BREAK
  SW.CASE 3 % blue
   GR.COLOR 255,0,0,255
   SW.BREAK
  SW.CASE 4 % cyan
   GR.COLOR 255,0,255,255
   SW.BREAK
 SW.END
FN.END

d=5 % grid size

GR.OPEN 255,0,0,0
GR.ORIENTATION 1
GR.SCREEN w,h
r=w/(d*2) % circle radius
DIM grid[d,d] % stores objects
DIM c[d,d] % stores color number
DIM ct[4] % count for color
again:
score=0
badtap=0
tap=0
deadline=20000

st=CLOCK()
DO
 ARRAY.FILL ct[],0
 FOR y=1 TO d
  FOR x=1 TO d
   c=FLOOR(RND()*4)+1
   ct[c]++
   c[x,y]=c
   setcolor(c)
gr.circle newc,((x-1)/d)*w+r,((y-1)/d)*w+h/3,r*0.85
   grid[x,y]=newc
   GR.SHOW newc
  NEXT
 NEXT
 GR.RENDER
! draw reference circle
 c=c[FLOOR(RND()*d)+1,FLOOR(RND()*d)+1]
 setcolor(c)
 GR.CIRCLE ww,w/2,h/8,r
 GR.SHOW ww
 GR.RENDER

 tap=0
 DO

  GR.TOUCH t,xx,yy
  IF t
   goodtap=0
   GR.COLOR 0,0,0,0
   GR.CIRCLE tt,xx,yy,3
   GR.SHOW tt
   GR.RENDER
   FOR y=1 TO d
    FOR x=1 TO d
     IF GR_COLLISION(tt,grid[x,y]) & c[x,y]=c

      GR.HIDE grid[x,y]: GR.RENDER
      tap++:score++:goodtap=1
     ENDIF
    NEXT
   NEXT
   IF !goodtap THEN badtap++
   DO
    GR.TOUCH t,xx,yy
   UNTIL !t
  ENDIF
 
e=clock()-st
 UNTIL (e>=deadline) | tap=ct[c]
 GR.CLS
if e<deadline then deadline+=500
UNTIL (e>=deadline)

GR.CLS
setcolor(1)
GR.TEXT.ALIGN 2
GR.TEXT.SIZE h/15
GR.TEXT.DRAW s1,w/2,h/2,"score:"+INT$(score)
GR.TEXT.DRAW s2,w/2,3*h/4, "badtaps:"+INT$(badtap)
GR.RENDER
PAUSE 500
DO
 GR.TOUCH t,x,y
 PAUSE 100
UNTIL !t


DO
 GR.TOUCH t,x,y
 PAUSE 100
UNTIL t
gr.cls
Dialog.message "tapdots","play again?",  c, "yes!","no!"
If c=1 then goto again
GR.CLOSE
EXIT
