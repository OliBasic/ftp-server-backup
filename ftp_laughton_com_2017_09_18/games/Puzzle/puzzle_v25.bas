! puzzle - Vladimir - 2011
! uses gr.crop, so needs RFO BASIC v01.251 at least (not tested with other versions)
! If you have questions, bug reports..., post on http://rfobasic.freeforums.org

! =====================
! Functions
! =====================
fn.def int$(n)
	! returns the integer n converted in a string
	! without decimal point and without initial space characters
	no$ = str$(n)
	p = is_in(".", no$,1)
	no$ = left$(no$, p-1)
	fn.rtn no$
fn.end

fn.def coord2pos(x,y,nbcol)
	fn.rtn (x + y*nbcol + 1) 
fn.end

! =====================
! --- MAIN PROGRAMM ---
! =====================

nbcol = 5
! nb of columns of the puzzle
nblig = 7
! nb of lines of the puzzle

! =====================
! set the directories
! =====================
repBase$="puzzle/"
file.exists ex, repBase$
if ex=0 then
  file.mkdir repBase$
endif
repImage$ = repBase$ + "img/"
! repImage$ contains the .jpg files of the images
! images must be in portrait mode (because it is optimized for ASUS TF101 tablet)
file.exists ex, repImage$
if ex=0 then
	file.mkdir repImage$
endif
repTemp$ = repBase$ + "temp/"
! used to store the list of images (faster when there are a lot of images)
file.exists ex, repTemp$
if ex=0 then
  file.mkdir repTemp$
endif

! =====================
! download images if no images in the directory
! =====================
file.dir repImage$, a$[]
array.length n, a$[]
imageexists = 0
i = 1
while ((imageexists = 0) & (i<=n))
  b$=a$[i]
 if len(b$)>4 then
	if right$(b$,4)=".jpg" then
		imageexists = 1
	endif
 endif
	i = i+1
repeat
undim a$[]
if imageexists = 0 then
	! loads some images from internet
	nom$ = "http://laughton.com/basic/programs/games/PuzzleImages/image"
	for i=1 to 20
		no$ = int$(i)
		if len(no$)<2 then
			no$ = "0" + no$
		endif
		print "load image " + no$ + "/20"
		byte.open r, ficid, nom$ + no$ + ".jpg"
		byte.copy ficid, repImage$  + "image" + no$ + ".jpg"
		byte.close ficid
	next i
endif

! =====================
! arrays declaration
! =====================
dim noimgsel[16]
dim nomimgsel$[16]
dim bitmini[16]
dim nomimg$[1]
dim bitptr[1]
dim objptr[1]
dim pos[1]
dim display[1]

! =====================
! mode selection
! =====================
firstmenu:
mode = 0
array.load nommode$[],"mode 1 = random thumbnails","mode 2 = sorted thumbnails", "mode 3 = random image", "-", "change number of pieces (currently " + int$(nbcol) + "x" + int$(nblig) + ")","-","Quit"
while ((mode=6)|(mode=4)|(mode=0))
	select mode, nommode$[],""
repeat
undim nommode$[]
if mode=7 then
	end
elseif mode<=3 then
	mode = mode - 1
elseif mode = 5 then
	gosub newdimensions
	goto firstmenu
endif

! =====================
! screen preparation
! =====================
gr.open 255, 50,50,50
gr.orientation 1
gr.screen wscreen, hscreen
if wscreen>hscreen then
	! to correct strange behaviour
	! (at least on ASUS TF101)
	if wscreen=1280 then
		wscreen = 800
		hscreen = 1232
	else
		temp = wscreen
		wscreen = hscreen
		hscreen = temp
	endif
endif
imagedisplayed = 0
nbpieceschargees = 0
nbminischargees = 0
gosub calculeNbPieces

! =====================
! retrieve img names list
! =====================
gosub effaceEcran
zzbarText$ = "collect the image files"
zzbarPct = 0
gosub afficheBarre
file.exists ex, repTemp$ + "nomrep.txt"
if ex then
	grabfile nomrep$, repTemp$ + "nomrep.txt"
	if nomrep$<>repImage$ then
		ex = 0
	endif
endif
if ex then
	grabfile liste$,repTemp$ + "listeimg.txt"
	undim nomimg$[]
	split nomimg$[], liste$, "#"
	array.length nbimg, nomimg$[]
else
	file.dir repImage$, a$[]
	array.length n, a$[]
	array.sort a$[]
	undim nomimg$[]
	dim nomimg$[n]
	liste$ =""
	nbimg = 0
	for i=1 to n
		zzbarPct = i/n
		gosub afficheBarre
		b$ = a$[i]
		if right$(b$,4) = ".jpg"then
			nbimg = nbimg+1
			nomimg$[nbimg] = b$
			liste$ = liste$ + "#"+ b$
		endif
	next i
	liste$=mid$(liste$,2)
	undim a$[]
	byte.open w, idfic, repTemp$ + "listeimg.txt"
	byte.write idfic, liste$
	byte.close idfic
	byte.open w, idfic, repTemp$ + "nomrep.txt"
	byte.write idfic, repImage$
	byte.close idfic
endif

nobase=1
! =====================
! MAIN LOOP
! =====================
nouvelleImage:
gosub hideBar
gosub initBarre

if mode>=2 then
	! mode=2: random image, mode=3: selects image noimage
	if mode=2 then
		noimage = floor(rnd()*nbimg)+1
	endif
	nomcomplet$=nomimg$[noimage]
	goto extraitPieces
endif

! =====================
! select 16 files -> nomimgsel$[]
! =====================
zzbarText$ = "select thumbnails"
if nbimg<=16 then
	nbsel=nbimg
	for i=1 to nbsel
		zzbarPct = i/nbsel
		gosub afficheBarre
		nomimgsel$[i] = nomimg$[i]
	next i
elseif mode=0 then
	dejasel$ = ""
	nbsel = 16
	for i=1 to nbsel
		zzbarPct = i/nbsel
		gosub afficheBarre
		do
			n = floor(rnd() *nbimg) + 1
		until is_in("["+str$(n)+"]", dejasel$,1)=0
	dejasel$ = dejasel$ + "[" + str$(n) + "]"
	nomimgsel$[i] = nomimg$[n]
	noimgsel[i] = n
	next i
elseif mode=1 then
	nbsel = nbimg - nobase + 1
	if nbsel>16 then
		nbsel=16
	endif
	for i=1 to nbsel
		nomimgsel$[i]= nomimg$[i + nobase - 1]
		noimgsel[i] = i + nobase - 1
	next i
endif

! =====================
! loads thumbnails -> bitmini[]
! =====================
gosub effacebitmapsmini
zzbarText$ = "load thumbnails"
wmini = wscreen / 4 
hmini = hscreen / 4
for i=1 to nbsel
	zzbarPct = i/nbsel
	gosub afficheBarre
	nomcomplet$ = nomimgsel$[i]
	gr.bitmap.load objptr, repImage$+nomcomplet$
	gr.bitmap.scale bitmini[i], objptr, wmini, hmini
	gr.bitmap.delete objptr
next i
nbminischargees = nbsel
gosub hideBar

! ==========================================
! if image displayed, requires screen touch
! ==========================================
if imagedisplayed then
	gosub waitforscreentouch
	gosub effacebitmapspieces
endif

! =====================
! displays thumbnails
! =====================
gosub effaceEcran
x=0
y=0
for i=1 to nbsel
 gr.bitmap.draw objptr, bitmini[i], x, y
 gr.render
 x = x + wmini
 if x>wscreen-5 then
  x = 0
  y = y + hmini
 endif
next i
popup "choose an image", 0,0,0

! =====================
! thumbnail user selection
! =====================
do
	do
		gr.touch touched, x1, y1
	until touched
	do
		gr.touch touched, x2, y2
	until touched=0
	index = 0
	! index = 1 to 16 : selects the image corresonding to the thumbnail
	! index = 17 : exit (touch screen from bottom right to top left (in portrait mode))
	! index = 18 : reload next (touch screen from right to left)
	! index = 19 : reload previous (touch screen from left to right)
	xx1 = floor(x1 / wmini)
	yy1 = floor(y1 / hmini)
	xx2 = floor(x2 / wmini)
	yy2 = floor(y2 / hmini)
	if ((xx1=xx2)&(yy1=yy2)) then
		index = xx1 + yy1 * 4 + 1
	else
		if (yy1=yy2) then
			if x1>x2 then
				index = 18
			else
				index = 19
			endif
		else
			if ((xx1=3)&(yy1=3)&(xx2=0)&(yy2=0)) then
				index = 17
			endif
		endif
	endif
until index>0
if index=18 then
	nobase = nobase + 16
  if (nobase>(nbimg-15)) then
    nobase = nbimg - 15
  endif
endif
if index=19 then
	nobase = nobase - 16
  if nobase < 1 then
    nobase = 1
  endif
endif
if ((index=18)|(index=19)) then
	imagedisplayed = 0
	goto nouvelleImage
endif
if index=17 then
	exit
endif

! =====================
! displays selected thumbnail
! =====================
gosub effaceEcran
gr.bitmap.draw objptr, bitmini[index], (wscreen-wmini)/2, (hscreen-hmini)/2
nomcomplet$ = nomimgsel$[index]
noimage = noimgsel[index]
gr.render

! =====================
! creates the pieces of the puzzle
! =====================
extraitPieces:
zzbarText$=""
if imagedisplayed then
	gosub hideBar
	gosub waitforscreentouch
	gosub effacebitmapspieces
endif
p = 0
undim bitptr[]
undim objptr[]
undim pos[]
undim display[]
dim bitptr[nbpieces]
dim objptr[nbpieces]
dim pos[nbpieces]
dim display[nbpieces]
zzbarText$ = "creates the pieces"
gr.bitmap.load bitmap1, repImage$+nomcomplet$
gr.bitmap.scale bitmap2, bitmap1, wscreen, hscreen
gr.bitmap.delete bitmap1
x=0
y=0
for i=1 to nbpieces
	zzbarPct = i/nbpieces
	gosub afficheBarre
	pos[i] = i
	gr.bitmap.crop bitptr[i], bitmap2, x*wp, y*hp, wp, hp
	x=x+1
	if x=nbcol then
		x=0
		y=y+1
	endif
next i
gr.bitmap.delete bitmap2
nbpieceschargees = nbpieces
gosub effacebitmapsmini

! =====================
! shuffle and displays puzzle
! =====================
array.shuffle pos[]
gosub affichePuzzle

! =====================
! loop : move pieces
! =====================
nbcoups = 0
t_start = clock()
loopmove:
	do
		gr.touch touched, x1,y1
	until touched
	xp1 = floor(x1/wp)
	if xp1>nbcol-1 then
		xp1 = nbcol-1
	endif
	yp1 = floor(y1/hp)
	if yp1>nblig-1 then
		yp1 = nblig-1
	endif
	offx = x1 - xp1*wp
	offy = y1 - yp1*hp
	pos1 = coord2pos(xp1,yp1,nbcol)
	p1 = pos[pos1]
	temp = display[p1]
	display[p1] = display[nbpieces]
	display[nbpieces] = temp
	gr.newdl display[]
	do
		gr.touch touched, x2,y2
		gr.modify objptr[p1], "x", x2 - offx
		gr.modify objptr[p1], "y", y2 - offy
		gr.render
	until touched=0
	xp2 = floor(x2/wp)
	if xp2>nbcol-1 then
		xp2 = nbcol-1
	endif
	yp2 = floor(y2/hp)
	if yp2>nblig-1 then
		yp2 = nblig-1
	endif
	if((xp1<>xp2)|(yp1<>yp2)) then
		nbcoups = nbcoups+1
		pos2 = coord2pos(xp2,yp2,nbcol)
		p2 = pos[pos2]
		pos[pos1] = p2
		pos[pos2] = p1
		array.sort display[]
		gr.modify objptr[p1], "x", xp2*wp
		gr.modify objptr[p1], "y", yp2*hp
		gr.modify objptr[p2], "x", xp1*wp
		gr.modify objptr[p2], "y", yp1*hp
		gr.render
		goto bienplacees
	else
		goto menuOptions
	endif
goto loopmove
END


! ###############################################
! 			SUBROUTINES
! ###############################################

! =====================
! bienplacees
! =====================
bienplacees:
	t_end = clock()
	nbBienPlacees = 0
	for i=1 to nbpieces
		if pos[i]=i then
			nbBienPlacees = nbBienPlacees + 1
		endif
	next i
	if nbBienPlacees = nbpieces then
		t_duree = floor((t_end - t_start)/1000)
		t_min = floor(t_duree/60)
		t_sec = t_duree - 60*t_min
		if t_min>0 then
			t$ = int$(t_min) + "mn"
		else
			t$ = ""
		endif
		t$ = t$ + int$(t_sec) + "s"
		popup "Congratulations ! Completed in " + int$(nbcoups) + " moves and " + t$, 0, 0, 1
		pause 2000
		popup "Touch the screen to continue", 50, 50, 0
		do
			gr.touch touched, x,y
		until touched
		goto menuOptions
	else
		goto loopmove
	endif
END

! =====================
! menuOptions
! =====================
menuOptions:
	array.load zzselect$[], "back to puzzle", "shift right (or up)", "shift left (or down)", "shift up (or left)", "shift down (or right)"~
	"-","reshuffle","-","new random image", "next image", "previous image", "sorted thumbnails", "random thumbnails"~
	"-", "go to first menu"
	select zzn, zzselect$[],""
	undim zzselect$[]	!
	sw.begin zzn
	sw.case 2
		! shift right
		for y=0 to nblig-1
			po = coord2pos(nbcol-1,y,nbcol)
			piecedroite = pos[po]
			for x=nbcol-1 to 1 step -1
				pos[po] = pos[po-1]
				po = po - 1
			next x
			pos[po] = piecedroite
		next y
		gosub affichePuzzle
		goto bienplacees
		sw.break
	sw.case 3
		! shift left
		for y=0 to nblig-1
			po = coord2pos(0,y,nbcol)
			piecegauche = pos[po]
			for x=1 to nbcol-1
				pos[po] = pos[po+1]
				po = po + 1
			next x
			pos[po] = piecegauche
		next y
		gosub affichePuzzle
		goto bienplacees
		sw.break
	sw.case 4
		! shift up
		for x=0 to nbcol-1
			po = coord2pos(x,0,nbcol)
			piecehaut = pos[po]
			for y=1 to nblig-1
				pos[po] = pos[po+nbcol]
				po = po + nbcol
			next x
			pos[po] = piecehaut
		next y
		gosub affichePuzzle
		goto bienplacees
		sw.break
	sw.case 5
		! shift down
		for x=0 to nbcol-1
			po = coord2pos(x,nblig-1,nbcol)
			piecebas = pos[po]
			for y = nblig-1 to 1 step -1
				pos[po] = pos[po-nbcol]
				po = po - nbcol
			next x
			pos[po] = piecebas
		next y
		gosub affichePuzzle
		goto bienplacees
		sw.break
	sw.case 7
		! reshuffle
		array.shuffle pos[]
		gosub affichePuzzle
		nbcoups = 0
		t_start = clock()
		goto bienplacees
		sw.break
	sw.case 9
		! random image
		mode = 2
		goto nouvelleImage
		sw.break
	sw.case 10
		! next image
		if noimage<nbimg then
			noimage = noimage+1
			mode = 3
			goto nouvelleImage
		else
			popup "it's already the last image", 0, 0, 0
			goto loopmove
		endif
		sw.break
	sw.case 11
		! previous image
		if noimage>1 then
			noimage = noimage-1
			mode = 3
			goto nouvelleImage
		else
			popup "it's already the first image", 0, 0, 0
			goto loopmove
		endif
		sw.break
	sw.case 12
		! sorted thumbnails
		mode = 1
		nobase = noimage - 7
		if nobase<1 then
			nobase = 1
		endif
		goto nouvelleImage
	sw.case 13
		! random thumbnails
		mode = 0
		goto nouvelleImage
	sw.case 15
		! relaunch
		gr.close
		goto firstmenu
	sw.default
		! including choices 0 & 1
		goto loopmove
	sw.end
END

! =====================
! affichePuzzle
! =====================
affichePuzzle:
	gosub effaceEcran
	x=0
	y=0
	for i=1 to nbpieces
		gr.bitmap.draw objptr[i], bitptr[i], x, y
		display[i] = objptr[i]
	next i
	for i=1 to nbpieces
		p = pos[i]
		gr.modify objptr[p], "x", x*wp
		gr.modify objptr[p], "y", y*hp
		x=x+1
		if x=nbcol then
			x=0
			y=y+1
		endif
	next i
	gr.newdl display[]
	gr.render
	imagedisplayed = 1
return

! =====================
! effacebitmapsmini:
! =====================
effacebitmapsmini:
	! deletes thumbnails bitmaps
	! to free memory
	if nbminischargees>0 then
		gosub effaceEcran
		for i=1 to nbminischargees
			gr.bitmap.delete bitmini[i]
		next i
		nbminischargees = 0
	endif
return

! =====================
! effacebitmapspieces
! =====================
effacebitmapspieces:
	! deletes pieces bitmaps
	! to free memory
	if nbpieceschargees>0 then
		gosub effaceEcran
		for i=1 to nbpieceschargees
			gr.bitmap.delete bitptr[i]
		next i
		imagedisplayed = 0
		nbpieceschargees = 0
	endif
return

! =====================
! initGraph
! =====================
initGraph:
	gr.text.align 2
	gr.text.size 15
	gr.color 255, 0, 255, 0, 1
return


! =====================
! initBarre
! =====================
initBarre:
	zzbarw = wscreen - 40
	zzbarx1 = 20
	zzbary1 = hscreen - 30
	zzbary2 = hscreen - 25
	gr.rect zzbaridrect, zzbarx1, zzbary1, zzbarx1, zzbary2
	gr.text.draw zzbaridtxt, wscreen/2, hscreen-35, ""
	gr.render
return

! =====================
! afficheBarre
! =====================
afficheBarre:
	gr.show zzbaridrect
	gr.show zzbaridtxt
	gr.modify zzbaridtxt, "text", zzbarText$
	gr.modify zzbaridrect, "right", zzbarx1 + zzbarw * zzbarPct
	gr.render
return

! =====================
! hideBar
! =====================
hideBar:
	gr.hide zzbaridrect
	gr.hide zzbaridtxt
	gr.render
return

! =====================
! waitforscreentouch
! =====================
waitforscreentouch:
	popup "Touch the screen", 0,0,0
	do
		gr.touch touched,x,y
	until touched
	do
		gr.touch touched,x,y
	until touched = 0
return

! =====================
! effaceEcran
! =====================
effaceEcran:
	gr.cls
	gosub initGraph
	gosub initBarre
return

! =====================
! newdimensions
! =====================
newdimensions:
	do
		input "Enter the number or columns:", a$, int$(nbcol)
		n = val(a$)
	until n>0
	nbcol = n
	do
		input "Enter the number of lines:", a$, int$(nblig)
		n = val(a$)
	until n*nbcol>1
	nblig = n
	gosub calculeNbPieces
return

! =====================
! calculeNbPieces
! =====================
calculeNbPieces:
	nbpieces = nbcol * nblig
	wp = floor(wscreen/nbcol)
	hp = floor(hscreen/nblig)
return

! =====================
! afficheFini
! =====================
afficheFini:
	t_duree = floor((t_end - t_start)/1000)
	t_min = floor(t_duree/60)
	t_sec = t_duree - 60*t_min
	if t_min>0 then
		t$ = int$(t_min) + "mn"
	else
		t$ = ""
	endif
	t$ = t$ + int$(t_sec) + "s"
	popup "Congratulations ! Completed in " + int$(nbcoups) + " moves and " + t$, 0, 0, 0
	popup "Touch the screen to continue", 50, 50, 0
return
