REM Yin & Yang   @Cassiope34  08/2014

Fn.def vc(a,b,v$)   % valeurs affectées aux coups
!  +1 -1 =35
!  +1  0 =30
!   0 -1 =25
!   0 +1 =10
!  -1  0 =5
!  -1 +1 =0 
if a=1 & b=-1
  rs =1
elseif a=1 & b=0
  rs =2
elseif a=0 & b=-1
  rs =3
elseif (a=0 & b=1) | (a=0 & b=0)
  rs =4
elseif a=-1 & b=0
  rs =5
elseif a=-1 & b=1
  rs =6
else
  Fn.rtn -10
endif
Fn.rtn val(word$(v$,rs))
Fn.end

Fn.def lg$()    % give the device language  (v1.82)
 Device b
 Bundle.get b, "Language", lang$
 Fn.rtn lang$
Fn.end

Fn.def c2c(ax, ay, ar, bx, by, br)     % circle to circle collision detect
  ! ax,ay = center of circle a,  ar = its radius
  ! bx,by = center of circle b,  br = its radius
  Fn.rtn (((ar+br)*(ar+br)) > ((ax-bx)*(ax-bx) + (ay-by)*(ay-by)))   % return 0 or 1
Fn.end

Fn.def pnc(ptx ,pty, ccx, ccy, ccr)    % point into circle detect
  ! ptx, pty = coordinate of the point
  ! ccx,ccy = center of circle c,  ccr = its radius
  Fn.rtn (((ptx-ccx)*(ptx-ccx)+(pty-ccy)*(pty-ccy)) <= ccr*ccr)   % return 0 or 1
Fn.end

Fn.def p2r(px, py, rx, ry, rw, rh)     % point into rectangle detect
 ! px,py   = point coord
 ! rx, ry  = upper left coord of rectangle
 ! rw      = width  of rectangle
 ! rh      = height of rectangle
 Fn.rtn ((px>=rx) & (px<=(rx+rw-1))) & ((py>=ry) & (py<=(ry+rh-1)))   % return 0 or 1
Fn.end

Fn.def clr( cl$ )
  gr.color val(word$(cl$,1)), val(word$(cl$,2)), val(word$(cl$,3)), val(word$(cl$,4)), val(word$(cl$,5))
Fn.end

Fn.def movepix( ptr, arx, ary )    % move the bitmap 'ptr' to arx,ary smoothly...
 do
   gr.get.position ptr, px, py
   ix =floor((px-arx)/3)
   iy =floor((py-ary)/3)
   if abs(ix) <3 & abs(iy) <3 then D_u.break
   gr.modify ptr, "x", px-ix
   gr.modify ptr, "y", py-iy
   gr.render
 Until 0
 gr.modify ptr, "x", arx
 gr.modify ptr, "y", ary
 gr.render
Fn.end

Fn.def LastPtr( ptr )  % put the graphic pointer 'ptr' (so the bitmap) UP to all other.
  gr.getDL ndl[],1
  array.length sz, ndl[]
  if !ptr | sz=1 | ndl[sz] =ptr
    array.delete ndl[]
    Fn.rtn 0
  endif
  array.search ndl[],ptr,n
  if n
    for nn=n to sz-1
      ndl[nn] =ndl[nn+1]
    next
    ndl[sz] =ptr
    gr.newDL ndl[]
  endif
  array.delete ndl[]
Fn.end

File.Root tmp$
IstandAlone =Is_In("rfo-basic",tmp$)    % to know if it's an APK for the way to exit.

gr.open 255,82,148,86,0,0
gr.screen w,h
scx =1280
scy =800
sx  =w/scx
sy  =h/scy
gr.scale sx,sy

DIM cl$[4], yy[2], scyy[2], res[2], pyy[16,6], lcp[2], main[2], mbx[2], mby[2]
DIM pile[9,3], dja$[9], nop[9], polx[2], poly[2], rx[2], ry[2], wr[2], hr[2], h$[12]

nptr =1  % n° of its bitmap pointer
nptt =2  % n° of its text pointer
posx =3  % pos x
posy =4  % pos y
poup =5  % hauteur dans une pile (seul =1)
type =6  % 1=bleu, 2=orange

cl$[1] = "255 0 0 0 1"        % noir
cl$[2] = "255 255 255 255 1"  % blanc
cl$[3] = "255 76 173 224 1"   % bleu
cl$[4] = "255 238 139 21 1"   % orange

! the help bitmap
hsx =scx-310
hsy =scy-190
gr.bitmap.create bhelp, hsx, hsy
gr.bitmap.drawinto.start bhelp
call clr("255 82 148 86 1")    % vert
gr.rect nul,0,0,hsx,hsy
gr.set.stroke 3
gr.text.size 55
inl =38
call clr( "255 238 139 21 2" )
gr.text.draw nul, 310, inl+20, "Y i n   &   Y a n g"
gr.set.stroke 2
gr.text.size 45
call clr("255 238 244 82 2")
gr.text.draw nul, 20, inl+20, "Welcome to"
gr.set.stroke 1
gr.text.size 25
h$[1] ="Your mission should you accept it:"
h$[2] ="- Be the first to have 5 of your pieces showing."
h$[3] =""
h$[4] ="The rule:"
h$[5] ="- You may move your own pieces or your opponent's (in play)."
h$[6] ="- You can start a stack or place a piece on top of another."
h$[7] ="- Stacks can only go 3 high."
h$[8] ="- You can move pieces that are already in play, but not the piece that your opponent"
h$[9] ="    just moved."
h$[10] =""
h$[11] ="The first piece you touch will be your camp."
h$[12] ="If you want the computer start: just touch it's piece more than 2 seconds."
for lg=1 to 12
  gr.text.draw nul, 18, 90+lg*inl, h$[lg]
next
gr.text.draw nul, hsx-200, 90+13.2*inl, "have fun."
gr.bitmap.drawinto.end

for j =1 to 2
 gr.bitmap.create yy[j], 780, 780   % crée les 2 bitmaps du YinYang orange et bleu
 gr.bitmap.drawinto.start yy[j]
 call clr( cl$[2+j] )             % orange ou bleu
 gr.circle nul, 390, 390, 390
 call clr( cl$[2] )               % blanc
 gr.arc nul, 0,0,780,780,270,180,1
 gr.circle nul,390,585,195
 call clr( cl$[2+j] )            % orange ou bleu
 gr.circle nul,390,195,195
 gr.circle nul,390,585,45       % petits cercles optionnels: noir
 call clr( cl$[2] )             % blanc
 gr.circle nul,390,195,45
 gr.bitmap.drawinto.end
next

dlx =25
dly =60
gr.bitmap.create los, dlx, dly    % bitmap du losange
 gr.bitmap.drawinto.start los
 call clr( "255 0 0 0 1" )
 list.create N, s1
 list.add s1, dlx/2, 0
 list.add s1, dlx, dly/2
 list.add s1, dlx/2, dly
 list.add s1, 0, dly/2
 gr.poly nul, s1
gr.bitmap.drawinto.end

sqx =scx/6       %  quadrillage or ref. for bases position.
sqy =scy/4
!!
for lx =1 to 6
  gr.line nul, sqx*lx, 0, sqx*lx, scy
next
for ly =1 to 4
  gr.line nul, 0, sqy*ly, scx, sqy*ly
next
!!

gr.bitmap.create dessin, scy, scy    % bitmap du dessin central
gr.bitmap.drawinto.start dessin
 gr.bitmap.draw yinyang, yy[1], 10, 10
 pas =10    %  <---  voir autres valeurs
 for angle =0 to 360-pas step pas
  dx = scy/2+350*sin(toradians(angle))
  dy = scy/2+350*cos(toradians(angle))
  gr.rotate.start -angle, dx, dy
   gr.bitmap.draw nul, los, dx-dlx/2, dy-210
   gr.circle nul, dx-23, dy-90, 13
   gr.bitmap.draw nul, los, dx-dlx/2, dy-dly/2
  gr.rotate.end
  gr.line nul, scy/2, scy/2, dx, dy     % les lignes d'angle
 next
gr.bitmap.drawinto.end

gr.bitmap.draw nul, dessin, scx/2-scy/2, 0     % dessin central
gr.modify nul, "alpha", 30
gr.bitmap.draw helpb, bhelp, (scx-hsx)/2, 2   % bitmap d'aide
gr.hide helpb
! les mains  ( le point de la couleur du joueur qui doit jouer (et qui a joué! )
call clr( "255 0 252 255 1" )   % blue
mbx[1] =sqx
mby[1] =3.8*sqy
gr.circle main[1], mbx[1], mby[1], 12
call clr( "255 255 192 0 1" )   % orange
mbx[2] =5*sqx
mby[2] =3.8*sqy
gr.circle main[2], mbx[2], mby[2], 12

! lignes de camp   ( yellow )
call clr( "255 252 255 0 1" )
gr.line lcp[1], 0, 2.4*sqy, 1.6*sqx, scy
gr.line lcp[2], scx, 2.4*sqy, 4.4*sqx, scy

dr =148    % diamètre d'un pion
rr =dr/2   % son rayon
dcx =floor((sqx-dr)/2)   % décalage pour afficher chiffre au centre
dcy =floor((sqy-dr)/2)

gr.bitmap.scale scyy[1], yy[1], dr, dr   % échelle réduite pour fabriquer les pions
gr.bitmap.scale scyy[2], yy[2], dr, dr

call clr( "255 0 0 0 1" )
gr.text.size 40     % les 16 pions  ( 8 de chaque coté )
gr.text.align 2
dtx =rr
dty =rr+15
for p =1 to 16
  pyy[p,posy] = dcy+(4-1)*sqy
  btm = scyy[1]
  pyy[p,posx] = dcx+(1-1)*sqx
  pyy[p,poup] =p
  pyy[p,type] =1
  if p>8
    btm =scyy[2]
    pyy[p,posx] = dcx+(6-1)*sqx
    pyy[p,poup] =p-8
    pyy[p,type] =2
  endif
  gr.bitmap.draw pyy[p,nptr], btm, pyy[p,posx], pyy[p,posy]
  gr.text.draw pyy[p,nptt], pyy[p,posx]+dtx, pyy[p,posy]+dty, replace$(str$(pyy[p,poup]),".0","")
next

!call clr( "255 170 170 170 0" )
!gr.rect tapi1, dr, dr, scx-dr, scy-2.5*dr              % rectangles virtuels qui doivent
!gr.rect tapi2, 2.2*dr, scy-2.5*dr, scx-2.2*dr, scy-dr  % contenir le centre des pions.

call clr( "255 170 170 170 1" )    % couleur du cache
gr.circle cache, -dr, 0, rr
gr.hide cache

call clr( "255 0 255 255 1" )      % couleur des textes en haut.
gr.text.size 30
gr.text.draw mess, scx/2, 30, ""
gr.text.draw nul, scx-80, 35, "NEW"
gr.text.draw nul, 70, 35, "HELP"

lastp  =0  % dernier pointeur touché
cachok =0  % cache ce qu'il y a dessous 1=oui, 0=non
j      =2  % joueur orange commence, sauf si blue touché en premier...
start  =1
ok     =0

DO         % ------------ boucle principale -------------
 do
   gr.touch touched, x, y
   if !background() then gr.render
 until touched | quit
 ti =clock()
 if quit then D_u.break
 if help then gr.hide helpb
 help =0
 x /=sx
 y /=sy
 pt  =0
 nup =0
 for p=1 to 16    %  quel pion est touché ?
   if pnc(x, y, pyy[p,posx]+rr, pyy[p,posy]+rr, rr)   % si pion touché
     if pyy[p,poup]>=nup      % selectionne toujours le + haut d'une pile...
       pt =p
       nup = pyy[pt,poup]
     endif
   endif
 next
 if start
   if pt =8 then j =1
 endif
 campx = dcx+rr+5*(j=1)*sqx   % pour s'assurer que 'j' touche son camp s'il y a lieu.
 campy = dcy+rr+3*sqy
 if pt & pnc(x,y,campx,campy,rr)=0   %  le joueur a touché un pion : prêt pour slide.
   if pyy[pt,nptr]<>lastp

    if pyy[pt,poup]>1 & cachok     % si nécessaire : cache ce qu'il y a dessous...
      call LastPtr( cache )
      gr.modify cache, "x", pyy[pt,posx]+rr
      gr.modify cache, "y", pyy[pt,posy]+rr
      gr.show cache
    endif

    vdsb = pnc( x, y, dcx+rr+5*(j=2)*sqx, campy, rr )    %  vient de sa base.

    call LastPtr( pyy[pt,nptr] )    % pointeur au dessus des autres.
    gr.get.position pyy[pt,nptr], oldpx, oldpy
    deltaX =oldpx-x
    deltaY =oldpy-y
    m =1
   endif
 endif
 if FIN then m =0
 do
   gr.touch touched, tx, ty
   tx /=sx
   ty /=sy
   if m
     gr.hide pyy[pt,nptt]
     gr.modify pyy[pt,nptr], "x", tx+deltaX
     gr.modify pyy[pt,nptr], "y", ty+deltaY
     gr.render
   endif
   tr =clock()-ti
   incp = pnc(tx, ty, dcx+rr+5*(j=2)*sqx, campy, rr)
 until !touched | (tr>1000 & incp)   % (tr>1500 & start=1)
 if (tr>1000 & incp)
   AImode =1
   gosub comptage
   gosub AI
   m =0
 endif
 if m=1
   gosub majpt    %  ctrl + maj pyy[] + changement de j

   if ok=1 & FIN=0 & AImode
     gr.render
     gosub AI
   endif

 elseif tx>1200 & ty<80     % coin supérieur droit
   gosub new

 elseif tx<100 & ty<80 & !help
   call LastPtr( helpb )
   gr.show helpb
   help =1

 endif
 m =0
 if dja$[1] <> "" then start =0    % au moins 1 pion sur le tapis.

UNTIL quit      % ------------ fin de la boucle principale -------------

if IstandAlone
  END "Bye...!"
else
  EXIT
endif      %  ----------------------  exit  ----------------------

onbackkey:
if help
  gr.hide helpb
  help =0
  Back.resume
endif
lastime =clock()
if lastime-thistime <1000
  quit =1
else
  quit =0
  thistime =lastime
  popup " Presser encore pour quitter ",0,0,0
endif
Back.resume

majpt:     % controle si coup légal & maj pyy[]
 gr.get.position pyy[pt,nptr], dstx, dsty      % l'endroit du laché
 noc =(floor((dstx+rr)/sqx)+1)*10 +floor((dsty+rr)/sqy) +1   % case du laché...
 oldup =pyy[pt,poup]
 ap =0
 nup =0
 for p =1 to 16     % est-ce qu'il touche un autre pion ?
   if p<>pt
     if c2c(dstx+rr, dsty+rr, rr, pyy[p,posx]+rr, pyy[p,posy]+rr, rr)>0 & pyy[p,poup]>=nup
       nup = pyy[p,poup]
       ap =p        % rang (1 à 16) du plus haut pointeur recouvert
     endif
   endif
 next
 ok =1
 if noc=14 | noc=64    % pion posé sur une des 2 bases !
   call movepix( pyy[pt,nptr], oldpx , oldpy )    % ce pion retourne à sa place.
   ok =0

 elseif ap      % si recouvre, pt prendra tout seul la meme place que ap.
   if (pyy[ap,poup]+1)<4 & (pyy[ap,posx]<>oldpx & pyy[ap,posy]<>oldpy)    % coup valide
     call movepix( pyy[pt,nptr], pyy[ap,posx], pyy[ap,posy] )
     pyy[pt,posx] = pyy[ap,posx]
     pyy[pt,posy] = pyy[ap,posy]
     pyy[pt,poup] = pyy[ap,poup] +1    % prend le niveau du pion recouvert +1

   else
     call movepix( pyy[pt,nptr], oldpx , oldpy )    % jamais + de 3 sur une pile...
     ok =0
   endif

 elseif !ap & !vdsb & oldup=1
   pyy[pt,posx] =dstx           % déplace juste un pion seul sans conséquence...
   pyy[pt,posy] =dsty
   ok =0

 endif
 gosub posptxt
 gr.hide cache
 if ok
   if !ap        % par défaut si pas posé sur un autre pion.
     pyy[pt,posx] =dstx
     pyy[pt,posy] =dsty
     pyy[pt,poup] = 1      % nouvelle pile donc niveau 1
   endif

   call LastPtr( main[j] )            %   positionne les mains et change de joueur
   gr.modify main[j], "x", pyy[pt,posx]+dr-5
   gr.modify main[j], "y", pyy[pt,posy]+dr-5
   call LastPtr( main[3-j] )
   gr.modify main[3-j], "x", mbx[3-j]
   gr.modify main[3-j], "y", mby[3-j]
   lastp = pyy[pt,nptr]               % mémorise le dernier pointeur joué
   j = 3-j            % change de joueur

   gosub comptage     % compte les blue & les orange
   if res[1] =5
     FIN =1
     t$ =t$ + "    :  blue win !"
   elseif res[2] =5
     FIN =2
     t$ =t$ + "    :  orange win !"
   endif
   gr.modify mess, "text", t$
 endif

 for pion =1 to 16        % cache le chiffre si 1.
   if pyy[pion,poup]=1 then gr.hide pyy[pion,nptt] else gr.show pyy[pion,nptt]
 next
RETURN

comptage:
UnDim dja$[], pile[], res[], nop[]
DIM dja$[9], pile[9,3], res[2], nop[9]
pil =0
for pl=1 to 16
  cx = pyy[pl,posx]
  cy = pyy[pl,posy]
  noc =(floor((cx+rr)/sqx)+1)*10 +floor((cy+rr)/sqy) +1
  array.search dja$[], str$(cx)+";"+str$(cy), trv
  if noc<>14 & noc<>64
    if !trv      %  c'est donc une nouvelle pile.
      pil++
      dja$[pil] = str$(cx)+";"+str$(cy)       % stocke coordonnées des piles vérifiées
      pile[pil,pyy[pl,poup]] =pyy[pl,type]
      nop[pil] =pl         % et son n° dans pyy[]
    else         %  pile déjà existante
      pile[trv,pyy[pl,poup]] =pyy[pl,type]    % couleur de ce niveau ds cette pile.
      if pyy[pl,poup] > pyy[nop[trv],poup] then nop[trv] =pl
    endif
  endif
next
for pi =1 to pil      % vrai comptage des blue & des orange : seulement de + haut niveau.
  if pile[pi,3]
    res[ pile[pi,3] ] ++
  elseif pile[pi,2]
    res[ pile[pi,2] ] ++
  elseif pile[pi,1]
    res[ pile[pi,1] ] ++
  endif
next
t$ = replace$(str$(res[1])+" blue,   "+str$(res[2])+" orange ",".0","")
return

posptxt:   % fait suivre le chiffre de son niveau ds une pile
 gr.modify pyy[pt,nptt], "text", replace$(str$(pyy[pt,poup]),".0","")
 gr.modify pyy[pt,nptt], "x", pyy[pt,posx]+dtx
 gr.modify pyy[pt,nptt], "y", pyy[pt,posy]+dty
 call LastPtr( pyy[pt,nptt] )    % pointeur texte au dessus des autres.
 gr.show pyy[pt,nptt]
return

new:     %   all pawns goes back to home...
gr.modify main[1], "x", mbx[1]
gr.modify main[1], "y", mby[1]
gr.modify main[2], "x", mbx[2]
gr.modify main[2], "y", mby[2]
gr.hide cache
for pt=1 to 16
  pyy[pt,posy] = dcy+(4-1)*sqy
  pyy[pt,posx] = dcx+(1-1)*sqx
  pyy[pt,poup] = pt
  if pt>8 then pyy[pt,posx] = dcx+(6-1)*sqx
  if pt>8 then pyy[pt,poup] = pt-8
  gr.hide pyy[pt,nptt]
  call LastPtr( pyy[pt,nptr] )
  call movepix( pyy[pt,nptr], pyy[pt,posx], pyy[pt,posy] )
  gosub posptxt
next
res[1] =0
res[2] =0
t$ = replace$(str$(res[1])+" blue,   "+str$(res[2])+" orange.",".0","")
gr.modify mess, "text", t$
UnDim dja$[]
Dim dja$[9]
lastp =0
start =1
j =2
FIN =0
AImode =0
return

gpl:    % génère une position libre sur le tapis: plx,ply
rx[1] =dr
ry[1] =dr           % un grand rectangle en haut dans lequel doit se situer la position.
wr[1] =scx-2*dr
hr[1] =scy-3.5*dr
rx[2] =2.2*dr
ry[2] =scy-2.5*dr   % un plus petit en bas
wr[2] =scx-4.4*dr
hr[2] =1.5*dr
elm   =35           % éloignement mini entre les pions.
for r=1 to 2
 tt =0
 do
  polx[r] =floor(rx[r] +(rnd()*wr[r]))   % qqe part au hasard ds rectangle r
  poly[r] =floor(ry[r] +(rnd()*hr[r]))
  cok =1
  n =1
  while (nop[n]>0 & cok=1)   % teste l'éloignement des autres piles à 'elm' pixels mini...
    if c2c( polx[r], poly[r], rr+elm, pyy[nop[n],posx]+rr, pyy[nop[n],posy]+rr, rr)
      cok=0
    endif
    n++
  repeat
  if !cok then tt++
 until cok | tt=40    % arrête au bout de 40 tentatives par rectangles...
next
plig =floor(rnd()*2)+1   % une des 2 places trouvées
if !cok then plig =1
polx = polx[plig]-rr
poly = poly[plig]-rr
RETURN

AI:
! chaine liste des coups possibles (n° du pion ds pyy[] + destx + desty + valeur)
cp$ =""
va$ ="35 30 25 10 5 0"   % valeurs affectées aux coups... voir fonction vc(mc,ac,va$)

! pil         = le nbre de piles actuellement
! dja$[pil]   = leur coordonnée à l'écran
! pile[pil,3] = couleur de chaque pion des piles par niveau.
! nop[pil]    = n° ds pyy[] des pions en haut de chaque pile
! res[1]      = nbre de blue actuel.
! res[2]      = nbre de orange

! est ce que je dispose d'un pion dans ma base ?
basx =dcx+5*(j=2)*sqx    % coordonnées base de j
basy =dcy+3*sqy
psb  =0          %  pion sur ma base ?
nv   =1
for np =1 to 16
  if pyy[np,posx]=basx & pyy[np,posy]=basy & pyy[np,poup]>=nv   % y a un pion sur ma base.
    psb =np
    nv = pyy[np,poup]
  endif
next

! si psb tester tout ça avec lui.
! puis prendre chaque pion de nop[] sauf si pyy[n,nptr]=lastp
!    un coup = n° du pion dans pyy[] + coord. de destination

oldv =-20   % précédente valeur de coup.
nsol =0
cli  =0
np   =1
do
  pct =0
  if psb
    pct =psb       % pion de ma base en premier
  elseif nop[np]   % puis prend chaque pions du plateau
    pct =nop[np]
    if pyy[pct,poup]>1 then cli =pile[np,pyy[pct,poup]-1] else cli =0  % couleur dessous
    np++
  endif
  if pct then gosub posepion   % et le pose sur chaque position possibles...
  psb =0
until pct=0

! choisir un coup dans cp$ et le jouer.
  pause 800
  chx   = floor(rnd()*nsol)+1
  pt    = val(word$( word$(cp$,chx) ,1 ,";"))
  destx = val(word$( word$(cp$,chx) ,2 ,";"))
  desty = val(word$( word$(cp$,chx) ,3 ,";"))
  call LastPtr( pyy[pt,nptr] )
  oldpx = pyy[pt,posx]
  oldpy = pyy[pt,posy]
  gr.hide pyy[pt,nptt]
  call movepix( pyy[pt,nptr], destx , desty )  % joue le coup
  gosub majpt         %  ctrl + maj pyy[] + changement de j
RETURN

posepion:  % fabrique la chaine des coups avec les dplts possibles de pct, si meilleur...
if pyy[pct,nptr] =lastp then return
mc  =0     % nbre d'augemtation ou diminution de ma couleur.
ac  =0     % nbre d'augemtation ou diminution de la couleur adverse.
omc =0
oac =0

! -créer une nvelle pile sauf si (le pion est de niveau 1 & autre que base).
!   -calculer le resultat
! -poser sur chaque pile existante (sauf si celle-ci =3 ).
!   -calculer le resultat
!   -petit plus si pile de 2

! 9 possibilités : 6 en réalité + 1 inutile (0 0)
!   +1 à mes couleurs
!    0
!   -1
! à mixer avec :
!   +1 à la couleur adverse
!    0
!   -1
! donc on a 6 cas:
!  mc ac =valeur
!  +1 -1 =35
!  +1  0 =30
!   0 -1 =25
!   0 +1 =10
!  -1  0 =5
!  -1 +1 =0

! toutes les places où je peux poser pct...
if cli =j    % s'il y avait un couleur dessous elle a été libérée donc +1
  mc =1
elseif cli =3-j
  ac =1
endif
omc =mc
oac =ac
if psb>0 | pyy[pct,poup]>1    % dépot sur position libre : crée une nvelle pile.
  gosub gpl    % génère coord. position libre
  if psb then mc++
  vlc =vc(mc,ac,va$)
  if psb & vlc>20 then vlc+=6    % pion posé vient de ma base...
  if res[j]+mc =5 then vlc =50   % coup gagnant
  if vlc>oldv
    cp$ =str$(pct)+";"+str$(polx)+";"+str$(poly)+";"+str$(vlc)+" "
    nsol =1
    oldv =vlc
  elseif vlc=oldv
    cp$ =cp$ +str$(pct)+";"+str$(polx)+";"+str$(poly)+";"+str$(vlc)+" "
    nsol++
  endif
endif
for pose =1 to pil    % toutes les piles existantes.
  pto =nop[pose]
  if pyy[pto,poup]<3 & pto<>pct   % pile possible (sauf celle d'ou vient le pion!)
    mc =omc
    ac =oac
    if psb then mc++
    if pyy[pto,type] =j
      mc--
    elseif pyy[pto,type] =3-j
      ac--
    endif
    vlc =vc(mc,ac,va$)    % évalue ce coup...
    if psb & vlc>20 then vlc+=6    % pion posé vient de ma base...
    if res[j]+mc =5 then vlc =50   % coup gagnant
    if vlc>oldv              % fabrique la chaine des coups possibles...
      cp$ =str$(pct)+";"+dja$[pose]+";"+str$(vlc)+" "
      nsol =1
      oldv =vlc
    elseif vlc=oldv
      cp$ =cp$ +str$(pct)+";"+dja$[pose]+";"+str$(vlc)+" "
      nsol++
    endif
  endif
next
RETURN
