! Jumpy game clone
! 
! public domain demo game of BASIC!
!

FN.DEF SetColor(c$)
 GR.COLOR HEX(LEFT$(c$,2)), ~
 HEX(MID$(c$,3,2)), ~
 HEX(MID$(c$,5,2)), ~
 HEX(RIGHT$(c$,2)), 1
FN.END

FN.DEF c()
 FN.RTN RND()*50+50
FN.END

FN.DEF buttons$()
 a=100
 GR.SCREEN aw,ah
 w=720:h=1280
 sx=aw/w:sy=ah/h
 GR.SCALE sx,sy
 whalf=FLOOR(w/2)
 hhalf=FLOOR(h/2)
 DIM pad[2]
 ARRAY.LOAD b$[],"play again","quit"
 sq=h/8

 FOR i=1 TO 2
  GR.TEXT.DRAW pad[i],whalf,hhalf/2+hhalf*i*0.3,b$[i]
 NEXT i
 found=0
 DO
  GR.TOUCH touched,xx,yy
  GR.COLOR 0,0,0,0
  GR.CIRCLE cc,xx/sx,yy/sy,20
  GR.SHOW cc
  GR.RENDER
  FOR i=1 TO 2
   IF GR_COLLISION(cc,pad[i])
    LET found=1
    F_N.BREAK
   ENDIF
   a+=4
   IF a>255 THEN a=100
   GR.MODIFY pad[i],"alpha",a
  NEXT I
  GR.RENDER
  PAUSE 50
 UNTIL found
 FN.RTN b$[i]
FN.END

FN.def play(n)
soundpool.play s,n,0.9,0.9,1,0,2
FN.end

! main

Nbox=8
DIM bx[nbox],by[nbox] % box coords
DIM vx[nbox],vy[nbox] % box velocities
DIM bh[nbox],bw[nbox] % box dimensions
DIM box[nbox] % box objects

POPUP "tap screen anywhere jump on platforms and collect money. get to the top!",0,0,1

soundpool.open 5
soundpool.LOAD whee, "whee.mp3"
soundpool.LOAD boing, "boing.mp3"
soundpool.LOAD bink, "bink.mp3"
soundpool.LOAD plink, "plink.mp3"
audio.load fest,"festivus.mp3"

Playagain:

score=0
start=1
Cantouch=0
Games++

GR.OPEN 255,0,0,0,0,1
PAUSE 1000
GR.SET.ANTIALIAS 1
GR.SCREEN aw,ah
w=720:h=1280
Sx=aw/w:Sy=ah/h
GR.SCALE sx,sy
whalf=FLOOR(w/2)
hhalf=FLOOR(h/2)
FOR i=1 TO nbox
 vx[i]=5+FLOOR(RND()*8)
 IF FLOOR(RND()*2)=1 THEN vx[i]=-vx[i]
 vy[i]=0
 bx[i]=FLOOR(RND()*w)
 by[i]=i*((0.9*h)/nbox)
 bw[i]=(w/8)+(w/80)*FLOOR(RND()*6)
 bh[i]=FLOOR(h/50)
NEXT i

GR.COLOR 0,0,0,0
GR.RECT scr,0,0,w,h
GR.SHOW scr

ncoins=30
DIM coins[ncoins]
setcolor("ff00ff00")

FOR i=1 TO ncoins
gr.text.size (h*rnd())/10

!GR.CIRCLE coins[i],RND()*w,RND()*h,h/100
GR.text.draw coins[i],RND()*w,RND()*h,"$"
 GR.SHOW coins[i]
NEXT

GR.RENDER

gameend=0

setcolor("ff606060") % grey
GR.SET.STROKE 0
score=0
lives=5
GR.SET.STROKE 0


FOR i=1 TO nbox

GR.BITMAP.LOAD box, "box.png"

Gr.bitmap.scale box2, box, bw[i],bh[i]
GR.BITMAP.DRAW box[i], box2,bx[i],by[i]

 !GR.COLOR 255,c(),c(),c()
! GR.RECT box[i],bx[i],by[i],bx[i]+bw[i],by[i]+bh[i]
 GR.SHOW box[i]
NEXT i

GR.TEXT.TYPEFACE 2
setcolor("ffd0d000")
GR.TEXT.SIZE 40
GR.TEXT.BOLD 1
sx=w/2:sy=h
GR.BITMAP.LOAD p_bm_obj, "jumper.png"

GR.BITMAP.DRAW player, p_bm_obj,w/2,h
GR.RENDER                % Render it now
PAUSE 500                % Pause for 1/2 second
GR.HIDE player  % and then hide it
GR.SHOW player

GR.RENDER
DO
 GR.TOUCH touched, x,y
UNTIL touched

audio.stop
audio.play fest
tstart=CLOCK()

Cantouch=1
tic=CLOCK()
!------------------------------------------
DO

 ! process boxes ---------------
 FOR i=1 TO nbox

  GR.GET.POSITION box[i],fx,fy
  GR.MOVE box[i],vx[i],0
  IF fx+bw[i]>w
   Fx=w-bw[i]
   LET vx[i]=-vx[i]


!   GR.MODIFY box[i],"left",fx,"right",fx+bw[i]
!   GR.MODIFY box[i],"top",fy,"bottom",fy+bh[i]
gr.modify box[i],"x",fx
gr.modify box[i],"y",fy
  ELSE if fx<0
   Vx[i]=-vx[i]
   LET fx=0
!   GR.MODIFY box[i],"left",fx,"right",fx+bw[i]
gr.modify box[i],"x",fx
  ENDIF
 ! GR.MODIFY box[i],"top",fy,"bottom",fy+bh[i]
gr.modify box[i],"y",fy

  GR.GET.POSITION player, px, py
  IF GR_COLLISION(player,box[i]) & py<(fy-20) & pvy>0 & !sitting
   Sit=i:Pvy=0:Sitting=1
 PLAY(bink)
 !  TONE 9000,50,0
  ENDIF
 NEXT i
 GR.MOVE player,pvx,pvy
 IF !sitting &!start THEN Pvy+=1
 IF sitting 
  hold=0: Pvx=vx[sit]
 ENDIF

 FOR i=1 TO ncoins
  GR.MOVE coins[i],RND()*3-2,RND()*3-2
  IF GR_COLLISION(player,coins[i])
   score++
PLAY(plink)
!TONE 440,50,0
   GR.HIDE coins[i]
  ENDIF
  IF !GR_COLLISION(coins[i],scr)
   GR.MODIFY coins[i],"x",8*RND()*w,"y",8*RND()*h
  ENDIF

 NEXT 

 !process touch ---------------------
 GR.TOUCH touched,x,y
 IF touched & !hold
  hold=1:  start=0:Pvy=-25:pvx=0:sitting=0
  PLAY(boing)
 ENDIF

 GR.RENDER

 !timing ---------------
 LET toc=CLOCK()-tic
 PAUSE MAX(33-toc,1)
 LET tic=CLOCK()

 IF py>h+150
  lives--
  TONE 400,150,0
  TONE 380,150,0
  pvy=0:pvx=0
  hold=0
  sitting=0
  start=1
  py=h-50
  px=w/2
  GR.MODIFY player,"x",px,"y",py
  GR.RENDER
  cantouch=1
 ENDIF

 IF py<0
  PLAY(whee)
  score+=10
  pvy=0:pvx=0
  hold=0
  sitting=0
  start=1
  py=h-50
  px=w/2
  GR.MODIFY player,"x",px,"y",py
  GR.RENDER
  Cantouch=1
 ENDIF

UNTIL lives=0
!-------------------------------------

GR.SET.ANTIALIAS 1
setcolor("ff00ffff")
GR.TEXT.SIZE 40
GR.TEXT.ALIGN 2
GR.TEXT.DRAW txtend,whalf,hhalf*0.2,"game over"
GR.TEXT.ALIGN 2
GR.TEXT.DRAW txtend,whalf,hhalf*0.4,"score "+INT$(score)
DO
 GR.TOUCH touched,x,y
 PAUSE 100
UNTIL !touched
setcolor("ff00ffff") 
b$=buttons$()

IF b$="play again" THEN
 GR.CLOSE
 PAUSE 300
 Tend=0
 drag=0
 GOTO playagain
ENDIF

EXIT

hold:
GR.MOVE player,pvx,-pvy
GR.GET.POSITION player,px,py
IF py<1 

 score++
 GR.MODIFY player,"y",h
ENDIF
RETURN

!ONERROR:
GR.CLOSE
PRINT geterror()$
