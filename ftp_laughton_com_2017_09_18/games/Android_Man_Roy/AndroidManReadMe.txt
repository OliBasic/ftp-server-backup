Hi All

This app uses a test version of RFO-BASIC which as inkey$ array$[] and is a good test for that command. There is an issue with the Back Key, so this is a test for the Back Key also. Use Back Key for the options menu.

For the full story of this test version see:
http://rfobasic.freeforums.org/inkey-t4534.html 

For the Game:
http://laughton.com/basic/programs/games/Android_Man_Roy/

Android man:
Android man is confined to the 
yellow box. When Red man invades 
Android man box you gain points
by guiding  Android man and 
collide with Red man, sending
him off in another direction and
at a different spead. Each game 
lasts for one minute.

Press the Back Key for the 
options menu.


Point:
	Red men = 1

Controls:
	Touch the screen when you want Android man to run to.
	The arrows on a Blue-tooth keyboard.
	The rocker switch on a Game-pad.

Regards Roy








