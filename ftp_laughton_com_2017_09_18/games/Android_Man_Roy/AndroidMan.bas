Rem Android Man
Rem Demo Using inkey$ array$[]
Rem For Android
Rem With RFO Basic!
Rem October 2016
Rem Version 1.00
Rem By Roy Shepherd

di_height = 672 % set to my Device
di_width = 1152

gr.open 255,81,146,210
gr.orientation 0 % landscape 
pause 1000
WakeLock 3
 
gr.screen screenWidth, screenHeight
scale_x = screenWidth / di_width
scale_y = screenHeight / di_height
gr.scale scale_x, scale_y

gosub Functions
gosub Initialise
gosub SetUp
gosub PlayAndroidMan

exit

onBackKey:
   gosub OptionsMenu
back.resume 

!------------------------------------------------
! Do once at first run of game
!------------------------------------------------
Initialise:
    FALSE = 0 : TRUE = 1
    sound = TRUE : buzz = TRUE : music = TRUE
    Ymove = 20 : Xmove = 20
    highScore = 0 : gameTime = 60
    path$="../../AndroidMan/data/"
    
    ! Load Spash Screen
    gr.bitmap.load splash, path$ + "AndroidSplash.png"
    if splash = - 1 then call ImageLoadError("AndroidSplash")
    gr.bitmap.draw splash, splash, 0,0 : gr.render
    pause 2000 : gr.bitmap.delete splash : gr.cls
    
    !Draw screen rectangles 
    gr.color 255,96,113,196,1
    gr.rect null, 100, 100, di_width - 100, di_height - 100
    gr.color 255,255,255,0,0 : gr.set.stroke 4
    gr.rect androidFance, 100, 100, di_width - 100, di_height - 100
    
    gosub LoadData
    ! Load Android Man and Red Man
    gr.bitmap.load android, path$ + "androidMan.png"
    if android = - 1 then call ImageLoadError("androidMan")
    
    gr.bitmap.load man, path$ + "redMan.png"
    if man = - 1 then call ImageLoadError("redMan")
    
    gr.bitmap.draw android, android, - 50, - 50
    gr.bitmap.draw man, man, - 50, - 50
    
    gr.text.size 40
    gr.color 255,255,255,255, 1

    gr.text.draw txtScore, 100,50, "Score: 0"
    gr.text.align 2
    gr.text.draw txtTime, di_width / 2, 50, "Time : "
    gr.text.align 3
    gr.text.draw txtHighScore, di_width - 100, 50, "High Score : " + int$(highScore)
    gr.text.align 1 : gr.text.size 35
    gr.color 0,0,0,0
    gr.point collision, -1, -1
    
    gosub DrawOptionsMenu
    gosub DrawHelp
    gr.render
    gosub LoadSounds
return

!------------------------------------------------
! Do at start of each new game
!------------------------------------------------
SetUp:
    androidX = di_width / 2
    androidY = di_height / 2
    stopTime = 0 : startTime = 0
    manX = (di_width / 2)
    manY = - 50
    
    xd = 20 : yd = 20
    
    score = 0 : tick = time()
    stopTime = 0 % for when option menu is called
    startTime = 0
    gameOver = FALSE
    
    gr.modify txtScore, "text", "Score: " + int$(score)
    gr.modify txtHighScore, "text", "High Score: " + int$(highScore)
    
    gr.modify android, "x", di_width / 2, "y", di_height / 2
    gr.modify man, "x", manX, "y", manY
    
    gr.render
return

!------------------------------------------------
! Play Android Man using the New inkey$ array$[]
!------------------------------------------------
PlayAndroidMan:
    do
        gosub SetUp
        do
            gosub GamePad 
            gosub ScreenPad
            gosub MoveMan
            gosub AndroidHitMan
            gosub TimeLeft
            gr.render
        until gameOver
        gosub NewGame
    until gameOver
return

!------------------------------------------------
! Move the man bouncing him off walls and android Man 
!------------------------------------------------
MoveMan:
    offWall = FALSE
    manX += xd 
    manY += yd
    gr.modify man, "x", manX, "y", manY
    
    if (manX + 25) > di_width then xd =- xd : offWall = TRUE
        
    if manX < 10 then xd = abs(xd) : offWall = TRUE
        
    if manY + 30 > di_height then yd = - yd : offWall = TRUE
        
    if manY < 10 then yd = abs(yd) : offWall = TRUE
    
    if sound & offWall then call PlaySound(bounce1)
  
return

!------------------------------------------------
! Using collision to detect if Android hits man
!------------------------------------------------
AndroidHitMan:
    if gr_collision(android, man) then
        xd = int(40 * rnd()-20) + 10
        yd = int(40 * rnd()-20) - 10
        score ++
        gr.modify txtScore, "text", "Score: " + int$(score)
        if sound then call PlaySound(bounce2)
    endif
return

!------------------------------------------------
! Control Android man with gamepad or bluetooth keyboard
! Using the new INKEY$ array$[] 
!------------------------------------------------
GamePad:
    androidFlag = FALSE
    inkey$ key$[]
    
    array.search key$[], "", keyNotPressed
    if keyNotPressed then return
    
    array.search key$[], "up", keyUpPressed
    if keyUpPressed & androidY > 100 then
        androidY -= Ymove
        androidFlag = TRUE
    endif
   
    array.search key$[] ,"down", keyDownPressed
    if keyDownPressed & androidY < di_height - 185 then  
        androidY += Ymove
        androidFlag = TRUE
    endif
    
    array.search key$[] ,"left", keyLeftPressed
    if keyLeftPressed & androidX > 100 then 
        androidX -= Xmove 
        androidFlag = TRUE
    endif
    
    array.search key$[] ,"right", keyRightPressed
    if keyRightPressed & androidX < di_width - 175 then  
        androidX += Xmove
        androidFlag = TRUE
    endif
                        
    if androidFlag then gr.modify android, "x", androidX, "y", androidY
    
return

!------------------------------------------------
! Control  Android man with the virtual pad
!------------------------------------------------
ScreenPad:
    if androidFlag then return
    gr.touch t, tx, ty
    if t then 
        tx /= scale_x : ty /= scale_y
        if androidX < di_width - 175 & androidX < tx then androidX += Xmove
            
        if androidX > 100 & androidX > tx then androidX -= Xmove
           
        if androidY < di_height - 185 & androidY < ty then androidY += Ymove
        
        if androidY > 100 & androidY > ty then androidY -= Ymove
        
        gr.modify android, "x", androidX, "y", androidY
        if buzz then vibrate buzzGame[], -1
    
    endif
return

!------------------------------------------------
! Display the remaining time to play
!------------------------------------------------
TimeLeft:
    tock = (time() - tick - stopTime) / 1000
    gr.modify txtTime, "text", "Time: " + int$(gameTime - tock)
    if tock > 59 then gameOver = TRUE
return

!------------------------------------------------
! Time up. Start a New Game or End
!------------------------------------------------
NewGame:
    if score > highScore then
        highScore = score
        gr.modify txtHighScore, "text", "High Score: " + int$(highScore)
        if sound then call PlaySound(newHigh)
        gr.render
        m$ = "Time Up! A New High Score"
    else 
        m$ = "Time Up!"
        if sound then call PlaySound(notHigh)
    endif
    do : dialog.message m$, "New Game", yn, "Yes", "No" : until yn > 0
    if yn = 1 then gameOver = FALSE else gosub SaveData : pause 1000 : exit
return

!------------------------------------------------
! Load Sound files and setup buzz
!------------------------------------------------
LoadSounds:
    soundpool.open 9
    soundpool.load buttonClick,path$ + "buttonClick.wav"
    if ! buttonClick then call SoundLoadError("buttonClick")

    soundpool.load newHigh, path$ + "newHigh.wav"
    if ! newHigh then call SoundLoadError("newHigh")
    
    soundpool.load notHigh, path$ + "notHigh.wav"
    if ! notHigh then call SoundLoadError("notHigh")
    
    soundpool.load bounce2, path$ + "bounce2.wav"
    if ! bounce2 then call SoundLoadError("bounce2")
    
    soundpool.load bounce1, path$ + "bounce1.wav"
    if ! bounce1 then call SoundLoadError("bounce1")
    
    ! Audio loads White Rabbit
    audio.load WhiteRabbit, Path$ + "WhiteRabbit.mp3" 
    if ! WhiteRabbit then call SoundLoadError("WhiteRabbit")
    
    array.load buzzGame[], 1, 100
    
    gosub StartMusic
return

!------------------------------------------------
! Draw the Options Menu
!------------------------------------------------
DrawOptionsMenu:
    dim menu[13] % use this to hide, show the menu 

    gr.bitmap.load opt, path$ + "bmpOptions.png"
    if opt = - 1 then call ImageLoadError("bmpOptions")
    gr.bitmap.load tick, path$ + "bmpTick.png"
    if tick = - 1 then call ImageLoadError("bmpTick")
    gr.bitmap.scale tick, tick, 27, 23
    
    ! Gray out screen for menu
    gr.color 150,0,0,0,1
    gr.rect menu[1], 0, 0, di_width, di_height
    !Draw the options menu
    gr.color 255,0,0,0,1
    gr.bitmap.draw menu[2], opt, (di_width / 2) - 130, (di_height / 2) - 230
    ! Draw the ticks
    gr.bitmap.draw menu[3], tick, 630, 160 % music tick
    gr.bitmap.draw menu[4], tick, 630, 193 % sound tick
    gr.bitmap.draw menu[5], tick, 630, 227 % music tick
    
    ! Button
    gr.color 0,255,255,255,0
    gr.rect menu[6],  458, 151, 699, 182  % Music
    gr.rect menu[7],  458, 188, 699, 217  % Sound
    gr.rect menu[8],  458, 222, 699, 260  % Buzz
    gr.rect menu[9],  458, 276, 699, 325  % Reset High Score
    gr.rect menu[10], 458, 346, 699, 380  % About
    gr.rect menu[11], 458, 383, 699, 414  % Help
    gr.rect menu[12], 458, 417, 699, 460  % Exit Game
    gr.rect menu[13], 458, 489, 699, 540  % Close Menu
    
    for off = 1 to 13
        gr.hide menu[off]
    next
    
    gr.render
    
return

!------------------------------------------------
! Options, sound, music, buzz, reset high score,
! about, help, scoring, exit
!------------------------------------------------
OptionsMenu:
    startTime = time() 
    if sound then call PlaySound(buttonClick)
    allDone = FALSE
    for on = 1 to 13
       gr.show menu[on]
       if on = 3 & ! music then gr.hide menu[3]
       if on = 4 & ! sound then gr.hide menu[4]
       if on = 5 & ! buzz  then gr.hide menu[5]
    next
    gr.render
    do
        gr.touch t, tx, ty 
        if t then 
            do : gr.touch t, tx, ty  : until ! t
            tx /= scale_x : ty /= scale_y
            gr.modify collision, "x", tx, "y", ty
        endif
        
        if gr_collision(collision,menu[6]) then 
            gosub ModMusic
            
        elseif gr_collision(collision,menu[7]) then
            gosub ModSound
            
        elseif gr_collision(collision,menu[8]) then
            gosub ModBuzz
        
        elseif gr_collision(collision,menu[9]) then
            gosub ResetHighScore
            
        elseif gr_collision(collision,menu[10]) then
            gosub About
            
        elseif gr_collision(collision,menu[11]) then
            gosub Help
            
        elseif gr_collision(collision,menu[12]) then
            gosub ExitGame
            
        elseif gr_collision(collision,menu[13]) then 
            if sound then call PlaySound(snapButtonClick)
            allDone = TRUE
            
        endif
        
        gr.modify collision, "x", - 1, "y", - 1
    until allDone
    for off = 1 to 13
        gr.hide menu[off]
    next
    gr.render
    stopTime += time()- startTime
return

!-------------------------------------------------
! Start background music at start of game, if music = TRUE
!-------------------------------------------------
StartMusic:
if music then
    audio.play WhiteRabbit 
    audio.loop 
  endif 
return

!------------------------------------------------
! Turn music on and off
!------------------------------------------------
ModMusic:
  if buzz then vibrate buzzGame[], -1
  if sound then call PlaySound(buttonClick)
  if music then
    music = FALSE
    gr.hide menu[3]
    audio.stop 
  else
    music = TRUE
    gr.show menu[3]
    audio.play WhiteRabbit
    audio.loop 
  endif
  gr.render
return
                                    
!------------------------------------------------
! Turn sound on and off
!------------------------------------------------
ModSound:
  if buzz then vibrate buzzGame[], -1
  if sound then call PlaySound(buttonClick)
  if sound then
    sound = FALSE
   gr.hide menu[4]
  else
    sound = TRUE
    gr.show menu[4]
  endif
  gr.render 
return

!------------------------------------------------
! Turn buzz on and off
!------------------------------------------------
ModBuzz:
  if buzz then vibrate buzzGame[], -1
  if sound then call PlaySound(buttonClick)
  if buzz then
    buzz = FALSE
    gr.hide menu[5]
  else
    buzz = TRUE
   gr.show menu[5]
  endif
  gr.render
return

!------------------------------------------------
! Reset the high score to zero 
!------------------------------------------------
ResetHighScore:
  if buzz then vibrate buzzGame[], -1
  if sound then call PlaySound(buttonClick)
  do : dialog.message "Reset high Score to Zero?",,yn,"Yes","No" : until yn > 0
  if buzz then vibrate buzzGame[], -1
  if sound then call PlaySound(buttonClick)
  if yn = 1 then 
        highScore = 0
        gr.modify txtHighScore, "text", "High Score: " + int$(highScore)
  endif
  gr.render
return

!------------------------------------------------
! About Android Man
!------------------------------------------------
About:
    if buzz then vibrate buzzGame[], -1
    if sound then call PlaySound(buttonClick)
    do
       Dialog.Message " About:\n\n",~
			" 	Android Man for Android\n\n" + ~
			" 	With: RFO BASIC!\n\n" + ~
			" 	October 2016\n\n" +~
			" 	Version: 1.00\n\n" + ~ 
			" 	Author: Roy Shepherd\n\n", ~
			OK, "OK"
        if buzz then vibrate buzzGame[], -1
        if sound then call PlaySound(buttonClick)
    until OK > 0
    
return

!------------------------------------------------
! Draw the Help screen
!------------------------------------------------
DrawHelp:
    dim help[19]
    y = 50 : x = 150 : ofset = 30 : xoff = 50
    ! Gray out screen for help
    gr.color 150,0,0,0,1
    gr.rect help[1], 0, 0, di_width, di_height
    gr.color 255,255,255,0

    gr.text.draw help[2], x, y, "Android man is confined to the"
    y += ofset 
    gr.text.draw help[3], x, y, "yellow box. When Red man invades"
    y += ofset  
    gr.text.draw help[4], x, y, "Android man's box you gain points"
    y += ofset  
    gr.text.draw help[5], x, y, "by guiding  Android man and "
    y += ofset
    gr.text.draw help[6], x, y, "colliding with Red man, sending" 
    y += ofset
    gr.text.draw help[7], x, y, "him off in another direction and"
    y += ofset
    gr.text.draw help[8], x, y, "at a different spead. Each game " 
    y += ofset
    gr.text.draw help[9], x, y, "last for one minute."
    y += ofset :  y += ofset / 2
    gr.text.draw help[10], x, y, "Press the Back Key for the" 
    y += ofset
    gr.text.draw help[11], x, y, "options menu." : y += ofset
    y += ofset / 2
    
    gr.color 255,0,255,0
    gr.text.draw help[12], x, y, "Point:" : y += ofset
    gr.text.draw help[13], x + xoff, y, "Red men = 1" : y += ofset
    y += ofset / 2
    
    gr.text.draw help[14], x, y, "Controls:" : y += ofset
    gr.text.draw help[15], x + xoff, y, "Touch the screen when you want Android man to run too." : y += ofset
    gr.text.draw help[16], x + xoff, y, "The arrows on a Blue-tooth keyboard." : y += ofset
    gr.text.draw help[17], x + xoff, y, "The rocker switch on a Game-pad." : y += ofset
    y += ofset / 2
    gr.color 255,0,255,255
    gr.rect help[18],x + 280, y - 30, 750, y + 10
    gr.color 255,255,0,0
    gr.text.draw help[19], x + 300, y, "Tap to close Help"
    for x = 1 to 19 
        gr.hide help[x]
    next
return

!------------------------------------------------
! How to play Android Man
!------------------------------------------------
Help:
    if buzz then vibrate buzzGame[], -1
    if sound then call PlaySound(snapButtonClick)
    for x = 1 to 19 
        gr.show help[x]
    next
    gr.render
    do
    gr.touch t, tx, ty
    if t then 
        tx /= scale_x : ty /= scale_y
        gr.modify collision, "x", tx, "y", ty
    endif
    until gr_collision(help[18], collision)
    if buzz then vibrate buzzGame[], -1
    if sound then call PlaySound(snapButtonClick)
    for x = 1 to 19 
        gr.hide help[x]
    next
    gr.render

return

!------------------------------------------------
! Exit the game
!------------------------------------------------
ExitGame:
    if buzz then vibrate buzzGame[], -1
    if sound then call PlaySound(snapButtonClick)
    do : dialog.message "Exit Android Man",, yn, "Yes", "No" : until yn > 0
    if buzz then vibrate buzzGame[], -1
    if sound then call PlaySound(snapButtonClick)
    if yn = 1 then
        if score > highScore then highScore = score : popup "New High Score"
        gosub SaveData  
        pause 1000 
        exit
    endif
return

!------------------------------------------------
! Save the High Score, Music, sound, and Buzz
!------------------------------------------------
SaveData:
    file.exists pathPresent,path$ + "GameData.txt"
    if ! pathPresent then file.mkdir path$

    text.open w,hs,Path$ + "GameData.txt"
    text.writeln hs,int$(highScore)
    text.writeln hs,int$(music)
    text.writeln hs,int$(sound)
    text.writeln hs,int$(buzz)
    text.close hs
return

!------------------------------------------------
! Load the High Score, Music, sound, and Buzz
!------------------------------------------------
LoadData:
    file.exists pathPresent,path$  +"GameData.txt"
    if pathPresent then
        text.open r,hs,path$ + "GameData.txt"
        text.readln hs,highScore$
        text.readln hs,music$
        text.readln hs,sound$
        text.readln hs,buzz$
        text.close hs
        highScore = val(highScore$)
        music = val(music$)
        sound = val(sound$)
        buzz = val(buzz$)
    endif
return

!------------------------------------------------
! All game functions
!------------------------------------------------
Functions:

!------------------------------------------------
! if Play sound (ptr)
!------------------------------------------------
  fn.def PlaySound(ptr)
      soundpool.play s,ptr,0.99,0.99,1,0,1
  fn.end
  
!------------------------------------------------
! load Sound files not found
!------------------------------------------------
    fn.def SoundLoadError(e$)
        dialog.message "Sound file " + e$ + " not found", "Game will end",OK,"OK" 
        end
    fn.end
    
!------------------------------------------------
! Load image file not found
!------------------------------------------------
    fn.def ImageLoadError(e$)
        dialog.message "Image file " + e$ + " not found", "Game will end",OK,"OK" 
        end
    fn.end
    
return

