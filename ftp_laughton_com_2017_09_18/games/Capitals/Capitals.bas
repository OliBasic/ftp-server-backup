! Capitals
! Aat Don @2015
GR.OPEN 255,104,192,230,0,0
PAUSE 1000
GR.SCREEN w,h
ScaleY=700
ScaleX=w/h*ScaleY
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
WAKELOCK 3
DataPath$="../../Capitals/data/"
DBPath$="../../Capitals/databases/"
DBName$=DBPath$+"Countries.db"
TableName$="WorldData"
TableTranslate$="Trans"
C1$="WorldPart"
C4$="XGet"
C5$="YGet"
C6$="XWidth"
C7$="YWidth"
C8$="XPut"
C9$="YPut"
ARRAY.LOAD MultiLang$[],"English","Nederlands","Français","Deutsch"
DIM TextLines$[35]
GR.COLOR 255,255,0,0,1
GR.TEXT.ALIGN 2
GR.TEXT.SIZE 50
GR.ROTATE.START -25,250,150
	GR.TEXT.DRAW g,250,150,"Choose your language"
GR.ROTATE.END
GR.ROTATE.START 35,900,150
	GR.TEXT.DRAW g,900,150,"Kies je taal"
GR.ROTATE.END
GR.ROTATE.START 135,900,450
	GR.TEXT.DRAW g,900,470,"Wähle dein Sprache"
GR.ROTATE.END
GR.ROTATE.START -145,250,450
	GR.TEXT.DRAW g,250,470,"Choisissez ta langue"
GR.ROTATE.END
GR.RENDER
DO
	DIALOG.SELECT Language,MultiLang$[],"Language | Taal | Langue | Sprache"
UNTIL Language>0
GOSUB Language,EN,NL,FR,DE
! Get language
SQL.OPEN DB_Ptr,DBName$
SQL.QUERY Cursor,DB_Ptr,TableTranslate$,Columns$
FOR i=1 TO 35
	SQL.NEXT xdone,Cursor,TextLines$[i]
NEXT i
GR.CLS
GR.BITMAP.LOAD Icon,DataPath$+"icon.png"
GR.BITMAP.DRAW g,Icon,0,0
GR.BITMAP.DRAW g,Icon,ScaleX-192,0
GR.TEXT.SIZE 40
GR.TEXT.BOLD 1
GR.COLOR 255,0,0,0,1
GR.TEXT.DRAW g,ScaleX/2,50,TextLines$[1]
GR.TEXT.SIZE 15
GR.TEXT.DRAW g,ScaleX/2,100,TextLines$[2]
GR.TEXT.DRAW g,ScaleX/2,150,TextLines$[3]
GR.TEXT.DRAW g,ScaleX/2,180,TextLines$[4]
GR.TEXT.DRAW g,ScaleX/2,210,TextLines$[5]
GR.TEXT.DRAW g,ScaleX/2,240,TextLines$[6]
GR.TEXT.DRAW g,ScaleX/2,280,TextLines$[7]
GR.TEXT.DRAW g,ScaleX/2,310,TextLines$[8]
GR.TEXT.DRAW g,ScaleX/2,340,TextLines$[9]
GR.TEXT.DRAW g,ScaleX/2,370,TextLines$[10]
GR.TEXT.DRAW g,ScaleX/2,400,TextLines$[11]
GR.TEXT.DRAW g,ScaleX/2,430,TextLines$[12]
GR.TEXT.DRAW g,ScaleX/2,460,TextLines$[13]
GR.RENDER
! Get number of countries
Columns$=C1$+","+C2$+","+C3$+","+C4$+","+C5$+","+C6$+","+C7$+","+C8$+","+C9$
SQL.QUERY Cursor,DB_Ptr,TableName$,Columns$
SQL.QUERY.LENGTH NumOfCountries,Cursor
NumOfCountries=NumOfCountries-1
DIM PickNum[NumOfCountries],ChooseCountry$[9],ChooseCity$[7]
FOR i=1 TO NumOfCountries
	PickNum[i]=i
NEXT i
! Show world map
GR.BITMAP.LOAD Map,DataPath$+"worldmap.png"
GR.BITMAP.SCALE ScaledMap,Map,ScaleX,ScaleY
GR.CLS
GR.BITMAP.DRAW View,ScaledMap,0,0
! Draw transparent overlay
GR.COLOR 128,255,255,255,1
GR.RECT Layer,0,0,ScaleX,ScaleY
GR.COLOR 255,255,255,0,1
GR.TEXT.SIZE 40
GR.TEXT.DRAW g,ScaleX/2,45,TextLines$[1]
! Draw button
GR.SET.STROKE 5
GR.TEXT.SIZE 15
GR.BITMAP.CREATE Knob,200,50
GR.BITMAP.DRAWINTO.START Knob
	GR.COLOR 255,255,255,0,1
	GR.OVAL g,0,0,200,50
	GR.COLOR 255,0,0,255,0
	GR.OVAL g,3,3,197,47
	GR.COLOR 255,0,0,255,1
	GR.TEXT.DRAW g,100,30,TextLines$[14]
GR.BITMAP.DRAWINTO.END
POPUP TextLines$[15],0,0,0
GOSUB GetTouch
DIALOG.MESSAGE TextLines$[16],TextLines$[17],MaxRounds,"5","10","15"
MaxRounds=MaxRounds*5
DIALOG.MESSAGE TextLines$[18],TextLines$[19],Level,TextLines$[20],TextLines$[21],TextLines$[22]
IF Level=1 THEN
	MaxPoints=2
	Level$=TextLines$[20]
ENDIF
IF Level=2 THEN
	MaxPoints=2
	Level$=TextLines$[21]
ENDIF
IF Level=3 THEN
	MaxPoints=5
	Level$=TextLines$[23]
ENDIF
! Draw score
Score=0
GR.RECT g,30,380,110,460
GR.COLOR 255,255,255,0,1
GR.TEXT.DRAW g,80,400,"Score"
GR.TEXT.DRAW Points,80,420,INT$(Score)+"/"+INT$(MaxPoints*MaxRounds)
GR.TEXT.DRAW Percent,80,440,"0 %"
GR.SET.STROKE 0
GR.COLOR 255,255,0,0,1
GR.RECT g,30,380,50,460
GR.COLOR 255,0,255,0,1
GR.RECT Slider,30,380,50,460
GR.COLOR 255,255,0,0,0
GR.RECT g,30,380,50,460
GR.COLOR 255,255,0,0,1
FOR i=1 TO MaxRounds-1
	GR.LINE g,30,380+i*(80/MaxRounds),50,380+i*(80/MaxRounds)
NEXT i
GR.TEXT.DRAW g,100,350,Level$
GR.BITMAP.DRAW Button,Knob,(ScaleX-450)/2+560,(ScaleY-366)/2+100
GR.HIDE Button
! Worldpart indicator
GR.COLOR 255,255,0,0,0
GR.SET.STROKE 5
GR.RECT SelCont,0,0,10,10
GR.HIDE SelCont
GR.RENDER
ARRAY.SHUFFLE PickNum[]
FOR NumOfRounds=1 TO MaxRounds
	! Get a random Country
	n=PickNum[NumOfRounds]
	Where$="_id='"+INT$(n)+"'"
	Columns$="_id,"+C1$+","+C2$+","+C3$+","+C4$+","+C5$+","+C6$+","+C7$+","+C8$+","+C9$
	SQL.QUERY Cursor,DB_Ptr,TableName$,Columns$,Where$
	SQL.NEXT xdone,Cursor,Index$,MapNr$,CountryName$,CityName$,XGet$,YGet$,XSize$,YSize$,XPut$,YPut$
	CorrectCountry$=CountryName$
	CorrectCity$=CityName$
	mn=VAL(MapNr$)
	XFrom=VAL(XGet$)
	YFrom=VAL(YGet$)
	XW=VAL(XSize$)
	YW=VAL(YSize$)
	XTo=VAL(XPut$)
	YTo=VAL(YPut$)
	! Animate worldpart indicator
	GR.SHOW SelCont
	FOR i=1 TO 18
		READ.FROM (MOD(i-1,6))*4+1
		READ.NEXT l,t,r,b
		GR.MODIFY SelCont,"left",l,"top",t,"right",r,"bottom",b
		GR.RENDER
		PAUSE 100
	NEXT i
	READ.FROM (mn-1)*4+1
	READ.NEXT l,t,r,b
	GR.MODIFY SelCont,"left",l,"top",t,"right",r,"bottom",b
	GR.RENDER
	PAUSE 2000
	GR.HIDE SelCont
	! Show map
	GR.BITMAP.LOAD SelMap,DataPath$+"map0"+INT$(mn)+".png"
	GR.BITMAP.SIZE SelMap,W2,H2
	GR.BITMAP.DRAW CurMap,SelMap,(ScaleX-W2)/2,(ScaleY-H2)/2
	! Show country
	GR.BITMAP.LOAD CountryMaps,DataPath$+"cmap0"+INT$(mn)+".png"
	GR.BITMAP.CROP CurCountry,CountryMaps,XFrom,YFrom,XW,YW
	GR.BITMAP.DRAW ShowCountry,CurCountry,XTo+(ScaleX-W2)/2,YTo+(ScaleY-H2)/2
	GR.BITMAP.DELETE CountryMaps
	GR.RENDER
	! Fill array with answer plus 8 random options
	ChooseCountry$[1]=CountryName$
	ChooseCity$[1]=CityName$
	! Get 8 Countries from same map
	Where$="WorldPart='"+INT$(mn)+"'"
	Columns$=C2$+","+C3$
	SQL.QUERY Cursor,DB_Ptr,TableName$,Columns$,Where$
	SQL.QUERY.LENGTH NumOfSels,Cursor
	NumOfSels=NumOfSels-1
	m=FLOOR((NumOfSels-8)*RND()+1)
	FOR i=m TO m+7
		SQL.NEXT xdone,Cursor,CountryName$,CityName$
		IF CountryName$=CorrectCountry$ THEN
			SQL.NEXT xdone,Cursor,CountryName$,CityName$
		ENDIF
		ChooseCountry$[i-m+2]=CountryName$
		IF i<m+6 THEN
			ChooseCity$[i-m+2]=CityName$
		ENDIF
	NEXT i
	ARRAY.SHUFFLE ChooseCountry$[]
	ARRAY.SHUFFLE ChooseCity$[]
	GR.SHOW Button
	GR.RENDER
	DO
		GOSUB GetTouch
	UNTIL x>(ScaleX-450)/2+560 & x<(ScaleX-450)/2+760 & y>(ScaleY-366)/2+100 & y<(ScaleY-366)/2+150
	GR.HIDE Button
	GR.RENDER
	IF Level=1 | Level=3 THEN
		DO
			DIALOG.SELECT SelectedCountry,ChooseCountry$[],TextLines$[24]
			IF SelectedCountry=0 THEN
				GOSUB Question
				IF Q=1 THEN GOTO Quit
			ENDIF
		UNTIL SelectedCountry>0
		IF ChooseCountry$[SelectedCountry]=CorrectCountry$ THEN
			Answer$=TextLines$[25]
			Score=Score+2
		ELSE
			Answer$=TextLines$[26]+CorrectCountry$
		ENDIF
		GR.MODIFY Points,"text",INT$(Score)+"/"+INT$(MaxPoints*MaxRounds)
		GR.MODIFY Percent,"text",INT$(Score*100/(MaxRounds*MaxPoints))+" %"
		GR.RENDER
		DIALOG.MESSAGE TextLines$[27]+ChooseCountry$[SelectedCountry],TextLines$[28]+Answer$,ok,"OK"
	ENDIF
	IF Level=2 | Level=3 THEN
		DO
			DIALOG.SELECT SelectedCity,ChooseCity$[],TextLines$[29]+CorrectCountry$+TextLines$[30]
			IF SelectedCity=0 THEN
				GOSUB Question
				IF Q=1 THEN GOTO Quit
			ENDIF
		UNTIL SelectedCity>0
		IF ChooseCity$[SelectedCity]=CorrectCity$ THEN
			Answer$=TextLines$[25]
			Score=Score+2
			IF Level=3 THEN
				IF ChooseCountry$[SelectedCountry]=CorrectCountry$ THEN Score=Score+1
			ENDIF
		ELSE
			Answer$=TextLines$[26]+CorrectCity$
		ENDIF
		GR.MODIFY Points,"text",INT$(Score)+"/"+INT$(MaxPoints*MaxRounds)
		GR.MODIFY Percent,"text",INT$(Score*100/(MaxRounds*MaxPoints))+" %"
		GR.RENDER
		DIALOG.MESSAGE TextLines$[27]+ChooseCity$[SelectedCity]+TextLines$[31]+CorrectCountry$,TextLines$[28]+Answer$,ok,"OK"
	ENDIF
	GR.MODIFY Slider,"top",380+(NumOfRounds/MaxRounds)*80
	GR.HIDE ShowCountry
	GR.BITMAP.DELETE CurCountry
	GR.HIDE CurMap
	GR.BITMAP.DELETE SelMap
	GR.RENDER
NEXT NumOfRounds
PAUSE 1000
GR.RENDER
PAUSE 1000
GR.HIDE Layer
GR.RENDER
POPUP TextLines$[32],0,0,1
GOSUB GetTouch
WAKELOCK 5
SQL.CLOSE DB_Ptr
GR.CLOSE
EXIT
GetTouch:
	DO
		GR.TOUCH Touched,x,y
	UNTIL Touched
	DO
		GR.TOUCH Touched,x,y
	UNTIL !Touched
	x/=sx
	y/=sy
RETURN
Question:
	Tone 880,200,0
	DIALOG.MESSAGE TextLines$[1],TextLines$[33],Q,TextLines$[34],TextLines$[35]
RETURN
EN:
	C2$="Country"
	C3$="Capital"
	Columns$="English"
RETURN
NL:
	C2$="Natie"
	C3$="Hoofdstad"
	Columns$="Nederlands"
RETURN
FR:
	C2$="Pays"
	C3$="Capitale"
	Columns$="Francais"
RETURN
DE:
	C2$="Land"
	C3$="Hauptstadt"
	Columns$="Deutsch"
RETURN
ONBACKKEY:
	GOSUB Question
	IF Q=1 THEN
		GR.CLOSE
		WAKELOCK 5
		EXIT
	ENDIF
BACK.RESUME
READ.DATA 480,130,720,330
READ.DATA 480,310,740,590
READ.DATA 660,240,810,410
READ.DATA 780,210,1160,690
READ.DATA 10,10,530,430
READ.DATA 240,410,450,690
