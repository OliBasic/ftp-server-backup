REM Carré par @Cassiope34, 02/2016

Fn.def v(c$,p)     % convert character to value p=1 -> x, p=2 -> y
  Fn.rtn ASCII(c$,p)-48
Fn.end

Fn.def v$(x,y)     % convert x,y values to characters
  Fn.rtn chr$(48+x, 48+y)
Fn.end

Fn.def clr( cl$, mode )    % set a color
  gr.color val(word$(cl$,1)), val(word$(cl$,2)), val(word$(cl$,3)), ~
           val(word$(cl$,4)), mode
Fn.end

Fn.def trace(xd,yd,xa,ya)
if xd<>xa
  for px=xd+6 to xa-6
    gr.circle nul, px,yd, 3
  next
else
  for py=yd+6 to ya-6
    gr.circle nul, xd,py, 3
  next
endif
Fn.end

GR.OPEN 255,255,255,255,0,0
LET di_width  =480
LET di_height =320
GR.SCREEN orient_w,orient_h
LET scw =orient_w/di_width
LET sch =orient_h/di_height
GR.SCALE scw,sch
let fileGame$ ="CarreGC.ini"
let ai    =2
let cells =6
file.exists fe, fileGame$
if fe
  Grabfile cps$, fileGame$
  Split cp$[], cps$, "\n"
  cells =val(cp$[1])
  array.delete cp$[]
endif
DIM c[13,13], ncj[2], sol$[3], cl$[18]
array.load diX[],0,1,0,-1  % Nord Est Sud Ouest
array.load diY[],-1,0,1,0
cl$[1] = "255 76 173 224 1"   % bleu       players colors
cl$[2] = "255 238 139 21 1"   % orange

DIM s[7,7,2], cs[7,7,2], nz[7,7]   % tableau Gégé

wakelock 3

DO        % principal loop for new.

ntc  =cells*2+1
nc   =cells+1

GOSUB init

ec   =int((inc*nce)/2)+0.5*(cells=4)  % ??
tchx =posx-int(ec/2)
tchy =posy-int(ec/2)
ncj[1] =0  : jo =0
ncj[2] =0  : jv =0
j      =1
new    =0

do               % loop for a game.
  gr.hide dummyp
  do
    gr.touch touched, dx, dy
    if !background() then gr.render
  until touched | quit
  tch =clock()
  if quit then d_u.break
  do
    gr.touch touched, dx, dy
    ttch =clock()-tch
  until !touched | ttch>1000
  dx/=scw
  dy/=sch
  
  cx =int((dx-tchx)/ec)+1  % conversion en zone touchée.
  cy =int((dy-tchy)/ec)+1
  
  array.sum sum, c[]
  if cx>ntc then gr.show dummyp else gr.hide dummyp
  GR.MODIFY dummyp,"x",dx,"y",dy
  IF GR_COLLISION(dummyp,text3)       % RESET
    new =1
    
  elseif GR_COLLISION(dummyp,text4) & !sum   % Chg grid
    cells =cells+1-4*(cells=6) : new =1  % 3x3, 4x4, 5x5, 6x6 maxi
    
  elseif GR_COLLISION(dummyp,player) & !sum & ttch>1000  % long press "YOU"
    popup " ai contre ai "
    gr.modify player, "text", "AI.gg" : gr.hide dummyp
    j =1
    gosub ai_contre_ai
    
  elseif GR_COLLISION(dummyp,plai) & !sum & ttch>1000  % long press "A.I."
    popup " ai contre ai "
    gr.modify player, "text", "AI.gg" : gr.hide dummyp
    j =2
    gosub ai_contre_ai
    
  elseif (GR_COLLISION(dummyp,text2) | GR_COLLISION(dummyp,plai)) & !sum
    j=3-j
    gr.modify curs, "y", 65+46*(j-1)
    gosub jeu_ai : j=3-j   % AI joue en premier.
    gr.modify curs, "y", 65+46*(j-1)
  
  elseif cx & cy & cx<=ntc & cy<=ntc  % un trait
    gosub drawIt
    
    if drw & !mark then j=3-j    % chgt de joueur
    gr.modify curs, "y", 65+46*(j-1)

    if drw & j=ai then gosub jeu_ai : j=3-j   % AI joue à son tour.
    gr.modify curs, "y", 65+46*(j-1)

  endif
  
until new
gr.bitmap.drawinto.end
UNTIL quit
gosub saveGrid
wakelock 5
END " Bye..."

OnBackKey:
 lastime =clock()
 if lastime-thistime <999
   quit =1
 else
   quit =0
   thistime =lastime
   popup " Appuyer 2 fois pour quitter "
 endif
back.resume

jeu_ai:   % must set cx,cy to play.
gr.render
mark =0 : res$ =""
DO
sol$[1] ="" : sol$[2] ="" : sol$[3] =""
for cy=2 to 2*cells step 2
  for cx=2 to 2*cells step 2   % les cases
    if c[cx,cy]<4
      for o=1 to 4     % les 4 traits autour.
        nx =cx+diX[o] : ny =cy+diY[o] : ok =0
        c$ =right$(int$(100+nx),2)+right$(int$(100+ny),2)
        if !c[nx,ny]   % un trait libre
          ok =2-1*(c[cx,cy]=3)+1*(c[cx,cy]=2)  % ok=1,2,3
          ! vérifier la case derrière ce trait...!?
          if nx+diX[o]>0 & ny+diY[o]>0 & nx+diX[o]<ntc & ny+diY[o]<ntc
            if c[nx+diX[o],ny+diY[o]]=3     % ferme
              ok =1
            elseif c[nx+diX[o],ny+diY[o]]=2  % donne
              ok =3
            elseif ok<>3      % rien...
              ok =2
            endif
          endif
          if !is_in(c$,sol$[ok]) then sol$[ok]+=c$+" "
        endif
      next
    endif
  next
next
c =3   % donne
if len(sol$[1])   % ferme
  c =1
elseif len(sol$[2])   % rien
  c =2
endif
pj =len(sol$[c])/5
if pj
  cai =int(rnd()*pj)+1
  if c=3 then gosub moins   % essaye de donner le moins possible...
  cx =val(left$( word$(sol$[c],cai),2))
  cy =val(right$(word$(sol$[c],cai),2))
  pause 150
  gosub drawIt  % trace le trait cx,cy & maj c[]
  gr.render
endif
UNTIL !mark | ncj[1]+ncj[2]=cells*cells | quit
return

moins:    % test de tous les traits qui donnent, pour donner le moins possible...
small =100 : r$="" : ch$ =""
array.copy c[],cc[]  % sauve état actuel.
for cs=1 to pj
  array.copy cc[],c[]
  cx =val(left$( word$(sol$[3],cs),2))   % joue le trait et crée donc une case à 3.
  cy =val(right$(word$(sol$[3],cs),2))
  cpt =0
  c[cx,cy] =1
  if mod(cx,2) & !mod(cy,2)       % joue trait vertical & maj case(s)
    if cx-1
      c[cx-1,cy]++ : if c[cx-1,cy]=4 then cpt++
    endif
    if cx+1<ntc
      c[cx+1,cy]++ : if c[cx+1,cy]=4 then cpt++
    endif
  else                            % joue trait horizontal & maj case(s)
    if cy-1
      c[cx,cy-1]++ : if c[cx,cy-1]=4 then cpt++
    endif
    if cy+1<ntc
      c[cx,cy+1]++ : if c[cx,cy+1]=4 then cpt++
    endif
  endif
  res$ =right$(int$(100+cx),2)+right$(int$(100+cy),2)+" "
  ! rechercher une case de 3, la fermer, la compter, puis en chercher une autre, etc...
  DO
    ok =0
    for y=2 to ntc-1 step 2
      for x=2 to ntc-1 step 2
        if c[x,y]=3    % une case à 3 traits.
          r$ =""
          for o=1 to 4     % cherche le trait autour qui va la fermer.
            nx =x+diX[o] : ny =y+diY[o]
            if !c[nx,ny]   % ferme la case et crée peut-être une case à 3...
              c[nx,ny] =1 : c[x,y] =4 : ok=1 : cpt++
              if nx+diX[o] & nx+diX[o]<ntc & ny+diY[o] & ny+diY[o]<ntc
                c[nx+diX[o],ny+diY[o]]++
                if c[nx+diX[o],ny+diY[o]] =4 then cpt++
              endif
              r$ =right$(int$(100+nx),2)+right$(int$(100+ny),2)
              f_n.break
            endif
          next
        endif
        if ok then f_n.break
      next
      if ok then f_n.break
    next
    if !is_in(r$,res$) then res$+=r$+" "
  UNTIL !ok | quit
  if cpt=small then small =cpt : ch$+=right$(int$(100+cs),2)+" "
  if cpt<small then small =cpt : ch$=right$(int$(100+cs),2)+" "
 ! if !is_in(r$,res$) then res$+=r$+" " else res$+="-"
  res$+="-"
!  ? res$
next
!?"***********"
pj  =len(ch$)/3
chx =int(rnd()*pj)+1
cai =val(word$(ch$,chx))
!?pj,ch$,cai
array.copy cc[],c[]    % restore grille.
return

drawIt:   % draw the line at cx,cy & eventualy the closed cell.
 clr(cl$[j],1) : drw =0 : mark =0
 if !mod(cx,2) & !mod(cy,2) & c[cx,cy] =3    % a cell to mark.
   for o=1 to 4
     if !c[cx+diX[o],cy+diY[o]]
       cx =cx+diX[o] : cy =cy+diY[o] : f_n.break
     endif
   next
 endif
 if mod(cx,2) & !mod(cy,2) & c[cx,cy]=0       % trait vertical
 !  gr.line nul, posx+(cx-1)*ec, posy+(cy-2)*ec, posx+(cx-1)*ec, posy+cy*ec
   trace( posx+(cx-1)*ec, posy+(cy-2)*ec, posx+(cx-1)*ec, posy+cy*ec )
   c[cx,cy]=j : drw =1
   if cx-1>0
     c[cx-1,cy]++
     if c[cx-1,cy]=4 then gr.circle nul, posx+(cx-2)*ec, posy+(cy-1)*ec, 0.5*ec :ncj[j]++ :mark =1
   endif
   if cx+1<=ntc
     c[cx+1,cy]++
     if c[cx+1,cy]=4 then gr.circle nul, posx+(cx+0)*ec, posy+(cy-1)*ec, 0.5*ec :ncj[j]++ :mark =1
   endif
   
 elseif !mod(cx,2) & mod(cy,2) & c[cx,cy]=0   % trait horizontal
 !  gr.line nul, posx+(cx-2)*ec, posy+(cy-1)*ec, posx+cx*ec, posy+(cy-1)*ec
   trace( posx+(cx-2)*ec, posy+(cy-1)*ec, posx+cx*ec, posy+(cy-1)*ec )
   c[cx,cy]=j : drw =2
   if cy-1>0
     c[cx,cy-1]++
     if c[cx,cy-1]=4 then gr.circle nul, posx+(cx-1)*ec, posy+(cy-2)*ec, 0.5*ec :ncj[j]++ :mark =1
   endif
   if cy+1<=ntc
     c[cx,cy+1]++
     if c[cx,cy+1]=4 then gr.circle nul, posx+(cx-1)*ec, posy+(cy+0)*ec, 0.5*ec :ncj[j]++ :mark =1
   endif
   
 endif
 if drw then gr.modify trait, "x", posx+(cx-1)*ec, "y", posy+(cy-1)*ec
 GR.MODIFY text1, "text", int$(ncj[1])
 GR.MODIFY text2, "text", int$(ncj[2])
return

init:
gr.cls
if bmp1 then gr.bitmap.delete bmp1
gr.bitmap.create bmp1,480,320
gr.bitmap.draw bmp,bmp1,0,0
GR.TEXT.SIZE 28
GR.TEXT.ALIGN 2
clr(cl$[1],1) : GR.TEXT.DRAW text1,390,69, "0" : GR.TEXT.DRAW player,440,69, "You"
clr(cl$[2],1) : GR.TEXT.DRAW text2,390,115,"0" : GR.TEXT.DRAW plai,440,115, "A.I."
GR.COLOR 255,0,0,0,1      : GR.TEXT.DRAW text3,402,184,"RESET"
GR.COLOR 255,56,190,165,1 : GR.TEXT.DRAW text4,402,254,int$(cells)+"x"+int$(cells)
GR.POINT dummyp,-1,-1
gr.hide dummyp
gr.color 255,230,135,0,1  % orange
gr.text.draw curs, 365,65,chr$(8592)   % la flèche
gr.color 255,0,255,0,1
gr.circle trait, -10, 0, 4      % pointe le dernier trait tracé.

gr.bitmap.drawinto.start bmp1
gr.color 255,250,250,250,1
gr.rect rec,0,0,479,319
gr.set.antialias 0
gr.set.stroke 1
gr.color 255,220,220,220,0  % light gray
for lx=1 to 19
  gr.line nul,33+lx*23,0,33+lx*23,319
next
gr.color 30,255,40,90,0  % red
gr.line nul,33,0,33,319
for ly=1 to 58
  if !mod(ly,4) then gr.color 30,255,40,90,0 else gr.color 30,40,255,205
  gr.line nul,0,ly*(23/4),479,ly*(23/4)
next
gr.color 255,120,120,120,1
posx =33+23*(cells=5)
posy =23+23*(cells=5)
inc  =23
nce  =val(word$(". . 4 3 2 2",cells))  % nbre d'inc entre les points...
for i=1 to cells+1
  for j=1 to cells+1
    gr.circle poin, posx+(i-1)*inc*nce, posy+(j-1)*inc*nce, 3
  next
next
array.fill c[],0
gr.set.antialias 1
gr.set.stroke 2
RETURN

saveGrid:
cls
? cells
Console.save fileGame$
cls
return


!############################# AI Gégé ############################
ai_contre_ai:
ncc =cells*cells
nc  =cells+1
DO
  if j=1
    gosub convert : do  :  gosub jeuai  :  until g=2 | ncj[1]+ncj[2]=ncc | quit
  else
    gosub jeu_ai
  endif
  j =3-j  : gr.modify curs, "y", 65+46*(j-1)
  gr.render
UNTIL ncj[1]+ncj[2]=ncc | quit
return

convert:
  for y=2 to ntc step 2
    for x=2 to ntc step 2
      cx =x/2 : cy =y/2 : s[cx,cy,1] =0 : s[cx,cy,2] =c[x,y]
      for o=1 to 4
        if c[x+diX[o],y+diY[o]] then s[cx,cy,1]+=val(mid$("1248",o,1))
      next
    next
  next
  jo =ncj[1] : jv =ncj[2]
return

jeuai:    % jeu AI
  !**************************|
  ! AI cherche case à fermer |
  !**************************
  xd=0 : yd=0 : g=0 : h=0 : v=0
  array.fill nz[],0 : nz=0
  r=0 : rr=0 : sscpt=0 : mx=0 : my=0
  for y=1 to nc-1    % y=dy to fy step sty
     for x=1 to nc-1 % x=dx to fx step stx   
        if     band(s[x,y,1],15)=14
          xd= x : yd= y : h=1 : v=0 : r=1
        elseif band(s[x,y,1],15)=13
          xd= x+1 : yd= y : v=1 : h=0 : r=1
        elseif band(s[x,y,1],15)=11
          xd= x : yd= y+1 : h=1 : v=0 : r=1
        elseif band(s[x,y,1],15)=7
          xd= x : yd= y : v=1 : h=0 : r=1
        endif    
     next
  next
  
  !*******************************|
  ! pas trouvé de  case à fermer  |
  ! recherche 1 trait à jouer h+v |
  !************************(******
  if r=0    % mais rr cases jouables
    r$=""
    for y=1 to nc-1 % dy to fy step sty
      for x=1 to nc-1 %=dx to fx step stx  
        r=0
        if s[x,y,2]<2   % 0/1 trait maxi
          if     band(s[x,y,1],2)=0   % droite
           xd= x+1 : yd= y : v=1 : h=0 : r=1
           if x<nc then r=(s[x+1,y,2]<2)
           if r then gosub aj_rr
          endif
          if band(s[x,y,1],4)=0       % bas
           xd= x   : yd= y+1 : h=1 : v=0 : r=1
           if y<nc then r=(s[x,y+1,2]<2)
           if r then gosub aj_rr
          endif
          if band(s[x,y,1],1)=0       % haut
           xd= x   : yd= y   : h=1 : v=0 : r=1
           if y>1 then r=(s[x,y-1,2]<2)
           if r then gosub aj_rr
          endif
          if band(s[x,y,1],8)=0       % gauche
           xd= x   : yd= y   : v=1 : h=0 : r=1
           if x>1 then r=(s[x-1,y,2]<2)
           if r then gosub aj_rr
          endif
        endif
      next
    next
    if rr
      rr=int(rnd()*rr)+1 : rr=(rr-1)*5+1 : r=1
      xd=val(mid$(r$,rr,1)) : yd=val(mid$(r$,rr+1,1))
      v=val(mid$(r$,rr+2,1)) : h=val(mid$(r$,rr+3,1))
    endif
  endif
  if r=0
     gosub simul2    % pour donner le plus petit secteur
     if sscpt
        xd= ssxd : yd= ssyd : h= ssh : v= ssv
     endif
  endif
  if     h=1
      gosub horizontal
  elseif v=1
      gosub vertical
  endif
  pause 150
return

aj_rr :
 if r=1
   rr++ : r$=r$+int$(xd)+int$(yd)+int$(v)+int$(h)+","
 endif
return

simul2:   % recherche dans chaque case
  for uy=1 to nc-1     
     for ux=1 to nc-1
        if band(s[ux,uy,1],1)=0     % ch1 manque trait en haut
          array.copy s[], cs[]
           cs[ux,uy,1]+=1 : cs[ux,uy,2]+=1
           if uy>1                %  case au dessus
             cs[ux,uy-1,1]+=4 : cs[ux,uy-1,2]+=1
           endif
           gosub simul
           if sscpt=0 | (scpt>0 & sscpt>scpt)
             sscpt=scpt : ssxd= ux : ssyd= uy : ssh=1 : ssv=0
           endif
        endif

        if band(s[ux,uy,1],4)=0     % ch2 manque trait en bas 
           array.copy s[], cs[]
           cs[ux,uy,1]+=4 : cs[ux,uy,2]+=1
           if uy < nc-1             %  case au dessous
             cs[ux,uy+1,1]+=1 : cs[ux,uy+1,2]+=1
           endif
           gosub simul
           if sscpt=0 | (scpt> 0 & sscpt>scpt)
             sscpt=scpt : ssxd= ux : ssyd= uy+1 : ssh=1 : ssv=0
           endif
        endif

        if band(s[ux,uy,1],2)=0    % ch3 manque trait à droite
           array.copy s[], cs[]
           cs[ux,uy,1]+=2 : cs[ux,uy,2]+=1
           if ux < nc-1            % case à droite
             cs[ux+1,uy,1]+=8 : cs[ux+1,uy,2]+=1
           endif
           gosub simul
           if sscpt=0 | (scpt> 0 & sscpt>scpt)
             sscpt=scpt : ssxd= ux+1 : ssyd= uy : ssh=0 : ssv=1
          endif
        endif
          
        if band(s[ux,uy,1],8)=0     % ch4 manque trait à gauche
           array.copy s[], cs[]
           cs[ux,uy,1]+=8 : cs[ux,uy,2]+=1
           if ux > 1            % case à gauche
             cs[ux-1,uy,1]+=2 : cs[ux-1,uy,2]+=1
           endif
           gosub simul
           if sscpt=0 | (scpt> 0 & sscpt>scpt)
             sscpt=scpt : ssxd= ux : ssyd= uy : ssh=0 : ssv=1
          endif 
        endif
        if quit then f_n.break
     next
     if quit then f_n.break
  next
return

simul:
scpt=0
do
 sim=0
 for sy=1 to nc-1
    for sx=1 to nc-1
      if cs[sx,sy,2]=3
        if sim=0 & nz[sx,sy]=0 then nz++
        sim=1 : scpt++
        if band(cs[sx,sy,1],1)=0 % trait en haut 
           if sy > 1             % case au dessus
              cs[sx,sy-1,1]+=4 : cs[sx,sy-1,2]+=1
           endif
        elseif band(cs[sx,sy,1],4)=0 % trait en bas 
           if sy < nc-1          % case au dessous
              cs[sx,sy+1,1]+=1 : cs[sx,sy+1,2]+=1
           endif
        elseif band(cs[sx,sy,1],2)=0 % trait à droite 
           if sx < nc-1          % case à droite
              cs[sx+1,sy,1]+=8 : cs[sx+1,sy,2]+=1
           endif
        elseif band(cs[sx,sy,1],8)=0 % trait à gauche 
           if sx > 1             % case à gauche
               cs[sx-1,sy,1]+=2 : cs[sx-1,sy,2]+=1
           endif
        endif
        cs[sx,sy,1]=15 : cs[sx,sy,2]=4
        if nz[sx,sy]=0
          xx=mg+sx*lg-lg/2-50 : yy=mh+sy*lg-lg/2+50
        !  gr.color 255,255,255,255,1
        !  gr.circle nul, xx,yy ,lg/6
        !  gr.color 255,19,0,20,0
          nz[sx,sy]=nz
        !  gr.text.size 20
        !  gr.text.draw nul,xx-12,yy+10,int$(nz[sx,sy])
        !  gr.circle nul, xx,yy ,lg/7
        !  gr.render
        endif
      endif
    next
 next
until sim=0 | quit     % | (scpt>sscpt & sscpt)
return

horizontal:    % trait horizontal
 if yd>1
   ! *********************! 
   ! case dessus le trait ! 
   ! *********************!
   if band(s[xd,yd-1,1],4)=0
   !  gr.show ptr[xd,yd,1+2*(j=2)]
     s[xd,yd-1,1]+=4 : s[xd,yd-1,2]++ : g=2
     if s[xd,yd-1,1]=15
       xx=mg+xd*lg-lg/2 : yy=mh+(yd-1)*lg-lg/2 : g=1
       gosub ferme
     endif
   endif
 endif
 if yd<nc
   ! **********************! 
   ! case dessous le trait ! 
   ! **********************!
   if band(s[xd,yd,1],1)=0
   !  gr.show ptr[xd,yd,1+2*(j=2)]
     if g<>1 then g=2
     s[xd,yd,1]+=1 : s[xd,yd,2]++
     if s[xd,yd,1]=15
       xx=mg+xd*lg-lg/2 : yy=mh+yd*lg-lg/2 : g=1
       gosub ferme
     endif   
   endif
 endif
 cx =xd*2 : cy =yd*2-1
 gosub drawIt
 gr.render
return

vertical:   % trait vertical
 if xd>1
   ! ***********************! 
   ! case à gauche du trait ! 
   ! ***********************!
   if band(s[xd-1,yd,1],2)=0
    ! gr.show ptr[xd,yd,2+2*(j=2)]
     s[xd-1,yd,1]+=2 : s[xd-1,yd,2]++ : g=2
     if s[xd-1,yd,1]=15
       xx=mg+(xd-1)*lg-lg/2 : yy=mh+yd*lg-lg/2 : g=1
       gosub ferme
     endif
   endif
 endif
 if xd<nc
   ! ***********************!
   ! case à droite du trait !
   ! ***********************!
   if band(s[xd,yd,1],8)=0
   !  gr.show ptr[xd,yd,2+2*(j=2)]
     if g<>1 then g=2
     s[xd,yd,1]+=8 : s[xd,yd,2]++
     if s[xd,yd,1]=15
       xx=mg+xd*lg-lg/2 : yy=mh+yd*lg-lg/2 : g=1
       gosub ferme
     endif
   endif
 endif
 cx =xd*2-1 : cy =yd*2
 gosub drawIt
 gr.render
return

ferme:   % ferme la case
  if j=1 then jo++ else jv++
return
