dialog.message "Dots and Boxes","Tap between the dots to create a line. Try to make squares and don't let AI do the same.\nGood luck!",ok,"OK"

GR.OPEN 255,255,255,255,0,0 : LET di_width=480 : LET di_height=320
GR.SCREEN orient_w,orient_h : LET scale_width=orient_w/di_width : LET scale_height=orient_h/di_height
GR.SCALE scale_width,scale_height : GR.TEXT.SIZE 22 : gr.text.bold 1
gr.color 255,0,200,0,1
gr.text.draw mest,480,300,"YOU ARE FIRST"
GR.TEXT.SIZE 28 : gr.text.bold 0
GR.COLOR 255,0,0,255,1 : GR.TEXT.DRAW text1,430,45,"0" : gr.text.draw yout,330,45,"YOU:"
gr.text.size 20 : gr.text.draw mt1,330,67,"match" : gr.text.draw ms1,430,67,"0"
gr.text.draw allt1,330,87,"all time" : gr.text.draw alls1,430,87,"0"
gr.text.size 28
GR.COLOR 255,255,0,0,1 : GR.TEXT.DRAW text2,430,125,"0" : gr.text.draw ait,330,125,"AI:" 
gr.text.size 20 : gr.text.draw mt2,330,147,"match" : gr.text.draw ms2,430,147,"0"
gr.text.draw allt2,330,167,"all time" : gr.text.draw alls2,430,167,"0"
gr.text.size 28
GR.COLOR 255,0,0,0,1 : GR.TEXT.DRAW text3_text,350,227,"RESET"
GR.POINT dummyp,-1,-1 : gr.bitmap.create bmp1,320,320 : gr.bitmap.draw bmp,bmp1,0,0
cells=6 : size=40 : scorm1=0 : scorm2=0 
text.open r,scorf,"scor.ini"
if scorf=-1 then   %nu e fis
 scora1=0 : scora2=0
else
 text.readln scorf,sir$ : scora1=val(sir$) : text.readln scorf,sir$ : scora2=val(sir$)
 text.close scrof
endif
gr.modify alls1,"text",int$(scora1) : gr.modify alls2,"text",int$(scora2)

fn.def mesaj(mest,msg$)
 gr.modify mest,"x",480 : gr.modify mest,"text",msg$ : gr.render
 for i=480 to 320 step -40
  gr.modify mest,"x",i : gr.render : pause 30
 next i
fn.end

gr.bitmap.drawinto.start bmp1 : gr.set.stroke 5 : GR.RENDER : GOSUB init2
DO : PAUSE 111 : UNTIL 0 : END "goodbye"

umple:
 if (xx<1) | (xx>cells) | (yy<1) | (yy>cells) then return    %asta nu ar trebui sa se intimple
 teren[xx,yy]=teren[xx,yy]+1 : if turn=1 then gr.color 255,0,0,255 else gr.color 255,255,0,0
 if teren[xx,yy]=4 then 
  plin=1 : gr.oval ova,xx*size+5,yy*size+5,(xx+1)*size-5,(yy+1)*size-5
  if turn=1 then scor1++ else scor2++
  gr.modify text1,"text",int$(scor1) : gr.modify text2,"text",int$(scor2)
  if scor1+scor2=cells*cells then     %final joc
   call mesaj(mest,"GAME OVER")
   if scor1>scor2 then      %player wins
    scorm1++ : gr.modify ms1,"text",int$(scorm1) : popup "You win!",0,0,1
    scora1++ : gr.modify alls1,"text",int$(scora1)
   else
    if scor1<scor2 then     %AI wins
     scorm2++ : gr.modify ms2,"text",int$(scorm2) : popup "AI wins!",0,0,1
     scora2++ : gr.modify alls2,"text",int$(scora2)
    else                    %egalitate
     popup "Draw!",0,0,1
    endif
   endif
   text.open w,scorf,"scor.ini"
   text.writeln scorf,int$(scora1) : text.writeln scorf,int$(scora2)
   text.close scorf
  endif    %gata joc
 endif
return

setsegs:
 plin=0
 if seg<=2 then
  result=!hsegs[cx,cy+seg-1]
  if result then
   gr.line lin,cx*size+4,(cy+seg-1)*size,(cx+1)*size-4,(cy+seg-1)*size : hsegs[cx,cy+seg-1]=1
   xx=cx : yy=cy : gosub umple : xx=cx : yy=cy+seg*2-3 : gosub umple
  endif
 else
  result=!vsegs[cx+seg-3,cy]
  if result then
   gr.line lin,(cx+seg-3)*size,cy*size+4,(cx+seg-3)*size,(cy+1)*size-4 : vsegs[cx+seg-3,cy]=1
   xx=cx : yy=cy : gosub umple : xx=cx+seg*2-7 : yy=cy : gosub umple
  endif
 endif
 gr.render
RETURN

evalh:
 vlt=0 
 if cy>1 then : if teren[cx,cy-1]<>2 then : vlt=vlt+teren[cx,cy-1]+int(teren[cx,cy-1]/3)*2 : else : vlt=vlt-2 : endif : endif
 if cy<=cells then : if teren[cx,cy]<>2 then : vlt=vlt+teren[cx,cy]+int(teren[cx,cy]/3)*2 : else : vlt=vlt-2 : endif : endif
 if vlt>vlx then vlx=vlt : hx=cx : hy=cy 
return

evalv:
 vlt=0 
 if cx>1 then : if teren[cx-1,cy]<>2 then : vlt=vlt+teren[cx-1,cy]+int(teren[cx-1,cy]/3)*2 : else : vlt=vlt-2 : endif : endif
 if cx<=cells then : if teren[cx,cy]<>2 then : vlt=vlt+teren[cx,cy]+int(teren[cx,cy]/3)*2 : else : vlt=vlt-2 : endif : endif
 if vlt>vly then vly=vlt : vx=cx : vy=cy 
return

aiturn:
 !turn=2
 call mesaj(mest,"AI TURN")
 do
  hx=1 : hy=1 : vx=1 : vy=1 : vlx=-5 : vly=-5
  for cx=1 to cells : for cy=1 to cells+1
    if !hsegs[cx,cy] then gosub evalh
   next cy : next cx
  for  cx=1 to cells+1 : for cy=1 to cells
    if !vsegs[cx,cy] then gosub evalv
   next cy : next cx
  !turn=2 :
  plin=0
  if vlx+vly=-10 then
   popup "AI cannot move"
  else
   if vlx>vly then
    cx=hx : if hy=cells+1 then cy=hy-1 : seg=2 else cy=hy : seg=1
   else
    cy=vy : if vx=cells+1 then cx=vx-1 : seg=4 else cx=vx : seg=3
   endif
   gosub setsegs
  endif
 until !plin
 turn=1
 if scor1+scor2=cells*cells then call mesaj(mest,"GAME OVER") else : call mesaj(mest,"YOUR TURN")
return

UserScreen_OnTouch:
 if dumx>320 then return    %in afara tablei de joc
 cx=int(dumx/size) : cy=int(dumy/size)
 if cx>cells then cx=cells
 if cy>cells then cy=cells
 if cx<1 then cx=1
 if cy<1 then cy=1
 !if (cx>cells) | (cy>cells) | (cx<1) | (cy<1) then return
 cex=cx*size+int(size/2) : cey=cy*size+int(size/2) : seg=mod(int((atan2((dumx-cex),(dumy-cey))*57.29578+225)/90),4) + 1
 if int(seg/2)=1 then seg=-seg+5
 GOSUB setsegs 
 if !result then
  return
 else
  if !plin then 
   turn=2 : gosub aiturn 
  else 
   if scor1+scor2=cells*cells then call mesaj(mest,"GAME OVER") else : call mesaj(mest,"PLACE AGAIN!")
  endif
 endif 
RETURN

OnGrTouch:
 if turn=2 then Gr.OnGrTouch.Resume
 DO : GR.TOUCH touched,dumx,dumy : UNTIL !touched    %wait lift
 LET dumx=dumx/scale_width : LET dumy=dumy/scale_height : GOSUB UserScreen_OnTouch
 GR.MODIFY dummyp,"x",dumx,"y",dumy : IF GR_COLLISION(dummyp,text3_text) THEN GOSUB init
Gr.OnGrTouch.Resume

init:
dialog.message "Reset game?",,go,"OK","Cancel"
if go<>1 then return
init2:
 turn=1 : array.delete teren[],hsegs[],vsegs[] : dim teren[cells,cells],hsegs[cells,cells+1],vsegs[cells+1,cells]
 gr.color 255,255,255,255,1 : gr.rect rec,0,0,319,319 : gr.color 255,0,0,0
 for i=1 to cells+1 : for j=1 to cells+1 : gr.point poin,i*size,j*size : next j : next i
 !gr.modify mest,"text","YOU ARE FIRST"
 call mesaj(mest,"YOU ARE FIRST")
 scor1=0 : scor2=0
 gr.modify text1,"text","0" : gr.modify text2,"text","0" : gr.render
RETURN
