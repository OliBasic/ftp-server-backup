! Catch.bas
! Aat Don 2014
GR.OPEN 255,255,155,20,0,0
PAUSE 1000
GR.SCREEN w,h
ScaleX=900
ScaleY=h/w*ScaleX
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
WAKELOCK 3
DIM Rope[8],Bar[8],Num[8]
DataPath$="../../Catch/data/"
GR.COLOR 255,255,255,0,1
GR.SET.STROKE 12
GR.TEXT.BOLD 1
GR.TEXT.SIZE 200
GR.TEXT.DRAW g,80,300,"CATCH !"
GR.TEXT.SIZE 40
GR.COLOR 255,0,0,0,1
GR.TEXT.DRAW gx,80,400,"Tap screen to skip intro....."
GR.RENDER
SOUNDPOOL.OPEN 3
SOUNDPOOL.LOAD Begin,DataPath$+"Begin.mp3"
SOUNDPOOL.LOAD Start,DataPath$+"Start.mp3"
SOUNDPOOL.LOAD Stop,DataPath$+"Stop.mp3"
SOUNDPOOL.LOAD Catch,DataPath$+"Catch.mp3"
FILE.EXISTS HSPresent,DataPath$+"HiScore.tct"
IF HSPresent THEN
	TEXT.OPEN R,HS,DataPath$+"HiScore.tct"
		TEXT.READLN HS,HiScore$
	TEXT.CLOSE HS
ELSE
	HiScore$="0"
	TEXT.OPEN W,HS,DataPath$+"HiScore.tct"
		TEXT.WRITELN HS,HiScore$
	TEXT.CLOSE HS
ENDIF
SndVol=0.99
HiScore=VAL(HiScore$)
Sel=0
! Show rules
GR.BITMAP.CREATE rules,810,820
GR.BITMAP.DRAWINTO.START rules
	GR.COLOR 255,0,0,255,1
	FOR i=1 TO 20
		GR.TEXT.UNDERLINE i=1
		READ.NEXT R$
		GR.TEXT.DRAW g,5,i*40,R$
	NEXT i
GR.BITMAP.DRAWINTO.END
GR.BITMAP.DRAW ShowRules,rules,40,900
FOR i=450 TO -800 STEP -4
	GR.MODIFY ShowRules,"y",i
	GR.RENDER
	GR.TOUCH Touched,x,y
	IF Touched THEN F_N.BREAK
	PAUSE 10+(i>-500)*90
NEXT i
GR.HIDE gx
GR.HIDE ShowRules
GR.BITMAP.DELETE rules
GR.RENDER
PAUSE 1000
Amplitude=100
WaveLength=300
Playgame:
Points=0
GR.SET.STROKE 12
FOR i=80 to 820 STEP 10
	y1=Amplitude*SIN(2*PI()*i/WaveLength)
	GR.COLOR 255,255,0,0,1
	GR.LINE g,i,0,i,y1+Amplitude
	GR.COLOR 255,255,255,255,1
	GR.LINE g,i,y1+Amplitude,i,y1+ScaleY-Amplitude
	GR.COLOR 255,0,0,255,1
	GR.LINE g,i,y1+ScaleY-Amplitude,i,ScaleY
	IF Sel=0 THEN GR.RENDER
NEXT i
FOR i=160 TO 820 STEP 83
	GR.SET.STROKE 3
	GR.COLOR 255,0,0,0,1
	GR.LINE Rope[(i-160)/83+1],i,0,i,20
	GR.SET.STROKE 12
	GR.COLOR 255,VAL(LEFT$(RIGHT$("00"+BIN$((i-160)/83),3),1))*128,VAL(MID$(RIGHT$("00"+BIN$((i-160)/83),3),2,1))*128,VAL(RIGHT$(RIGHT$("00"+BIN$((i-160)/83),3),1))*128,1
	GR.LINE Bar[(i-160)/83+1],i,20,i,100
NEXT i
GR.SET.STROKE 3
GR.COLOR 200,0,200,0,1
GR.RECT g,74,420,154,450
FOR i=0 TO 6
	GR.RECT g,166+i*83,420,237+i*83,450
NEXT i
GR.RECT g,747,420,826,450
GR.RECT g,73,450,826,480
GR.COLOR 255,0,200,0,1
GR.TEXT.SIZE 30
GR.TEXT.DRAW g,400,475,"DOCK"
GR.RENDER
FOR i=1 TO 8
	Num[i]=i
NEXT i
ARRAY.SHUFFLE Num[]
GR.COLOR 255,0,255,255,1
GR.SET.STROKE 5
GR.TEXT.SIZE 70
GR.ROTATE.START 90,840,100
GR.TEXT.DRAW g,840,100,"CATCH !"
GR.ROTATE.END
GR.ROTATE.START 270,60,360
GR.TEXT.DRAW g,60,360,"CATCH !"
GR.ROTATE.END
GR.COLOR 200,0,0,0,1
GR.TEXT.SIZE 50
GR.TEXT.DRAW g,120,520,"Score"
GR.TEXT.DRAW score,260,520,USING$("","%d",INT(Points))
GR.TEXT.DRAW g,480,520,"Hi-Score"
GR.TEXT.DRAW highscore,690,520,USING$("","%d",INT(HiScore))
! Show buttons
GR.RECT g,90,200,310,300
GR.RECT g,390,200,730,300
GR.COLOR 255,255,255,0,1
GR.TEXT.DRAW g,125,270,"START"
GR.TEXT.DRAW g,425,270,"SOUND"
GR.COLOR 255,255,50,50,0
GR.RECT g,610,220,720,280
GR.COLOR 255,255,0,0,1
GR.PAINT.GET red
GR.COLOR 255,50,255,50,1
GR.PAINT.GET green
IF SndVol=0 THEN
	GR.TEXT.DRAW sound,620,270,"OFF"
	GR.MODIFY sound,"paint",red
ELSE
	GR.TEXT.DRAW sound,620,270,"ON"
	GR.MODIFY sound,"paint",green
ENDIF
GR.RENDER
DO
	GOSUB GetTouch
	IF x>390 & x<730 & y>200 & y<300 THEN
		IF SndVol=0 THEN
			SndVol=0.99
			GR.MODIFY sound,"text","ON"
			GR.MODIFY sound,"paint",green
			GR.RENDER
		ELSE
			SndVol=0
			GR.MODIFY sound,"text","OFF"
			GR.MODIFY sound,"paint",red
			GR.RENDER
		ENDIF
	ENDIF
UNTIL x>90 & x<310 & y>200 & y<300
SOUNDPOOL.PLAY Id4,Begin,SndVol,SndVol,1,0,1
! Delete last six entries from display list (=buttons)
GR.GETDL NwDspl[]
ARRAY.LENGTH L,NwDspl[]
GR.NEWDL NwDspl[1,L-6]
GR.RENDER
ARRAY.DELETE NwDspl[]
FOR Turn=1 TO 8
	! Wait random time
	Wait=Val(USING$("","%tQ",time()))+RND()*4000
	DO
		GWait=Val(USING$("","%tQ",time()))
	UNTIL GWait>Wait
	SOUNDPOOL.PLAY Id1,Start,SndVol,SndVol,1,0,1
	FOR i=1 TO 3
		GR.HIDE Rope[Num[Turn]]
		GR.RENDER
		GR.SHOW Rope[Num[Turn]]
		GR.RENDER
	NEXT i
	FOR i=20 TO 400 STEP 50
		GR.MODIFY Bar[Num[Turn]],"y1",i
		GR.MODIFY Bar[Num[Turn]],"y2",i+80
		GR.RENDER
		GR.BOUNDED.TOUCH Touched,(67+Num[Turn]*83)*sx,i*sy,(87+Num[Turn]*83)*sx,(i+80)*sy
		IF Touched THEN
			SOUNDPOOL.PLAY Id3,Catch,SndVol,SndVol,1,0,1
			GR.HIDE Bar[Num[Turn]]
			Points=Points+((370-i)/10)*(10-(Turn-1))
			GR.MODIFY score,"text",USING$("","%d",INT(Points))
			GR.RENDER
			F_N.BREAK
		ENDIF
	NEXT i
	IF !Touched THEN SOUNDPOOL.PLAY Id2,Stop,SndVol,SndVol,1,0,1
NEXT Turn
IF Points>HiScore THEN
	SOUNDPOOL.PLAY Id4,Begin,SndVol,SndVol,1,0,1
	GR.COLOR 255,128,128,128,1
	GR.OVAL g,150,20,570,120
	GR.COLOR 255,255,255,255,1
	GR.TEXT.DRAW g,170,90,"NEW HI-SCORE !"
	HiScore=Points
	GR.MODIFY highscore,"text",USING$("","%d",INT(HiScore))
	GR.RENDER
	TEXT.OPEN W,HS,DataPath$+"HiScore.tct"
		TEXT.WRITELN HS,USING$("","%d",INT(HiScore))
	TEXT.CLOSE HS
	PAUSE 2000
ENDIF
DIALOG.MESSAGE "CATCH","Play Again",Sel,"YES","NO"
IF Sel=1 THEN
	GR.CLS
	GOTO Playgame
ENDIF
WAKELOCK 5
GR.CLOSE
SOUNDPOOL.RELEASE
EXIT

GetTouch:
	DO
		GR.TOUCH Touched,x,y
	UNTIL Touched
	DO
		GR.TOUCH Touched,x,y
	UNTIL !Touched
	x/=sx
	y/=sy
RETURN
READ.DATA "CATCH - a simple game written in RFO-Basic!"
READ.DATA ""
READ.DATA "The object of this game is to catch as"
READ.DATA "many bars as you can."
READ.DATA "Eight bars are hanging on a rope."
READ.DATA "At irregular intervals they fall down one"
READ.DATA "at the time."
READ.DATA "The rope of the bar which is about to fall"
READ.DATA "will vibrate first."
READ.DATA "Put your finger on or below the one which"
READ.DATA "is falling to catch it, before it reaches"
READ.DATA "the dock. Mind you, you have to lift off"
READ.DATA "your finger from the screen between catches."
READ.DATA "If you catch a bar you are rewarded with"
READ.DATA "a number of points."
READ.DATA "The less bars are left, the less points"
READ.DATA "you get. The higher you catch the bar,"
READ.DATA "the more points you get."
READ.DATA "Max. number of points to obtain is 1820."
READ.DATA "Wishing you a good CATCH !"
