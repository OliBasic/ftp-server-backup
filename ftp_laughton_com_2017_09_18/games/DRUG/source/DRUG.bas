! DRUG.bas (DRess Up Goofy)
! Aat Don 2013
GR.OPEN 255,255,255,255,0,0
PAUSE 1000
GR.SCREEN w,h
ScaleX=720
ScaleY=h/w*ScaleX
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
WAKELOCK 3
GR.TEXT.SIZE 25
GR.SET.STROKE 3
DataDir$="../../DRUG/data/"
GR.BITMAP.LOAD Choice,DataDir$+"Choice.png"
GR.BITMAP.SIZE Choice,SPicW,SPicH
GR.BITMAP.SCALE ScaledChoice,Choice,SPicW/SPicH*ScaleY,ScaleY
NumOfPics=9
MaxParts=10
Kap=0
Mute=1
DIM T$[12],T[12]
GOSUB ScrollText
DIM Parts[MaxParts],Intro[2]
DIM PartPosition[NumOfPics,MaxParts,6]
DIM NumOfParts[MaxParts],ChosenParts[MaxParts]
READ.FROM 1
FOR k=1 TO NumOfPics
	READ.NEXT PicNumber
	READ.NEXT NumOfParts[k]
	FOR i=1 TO NumOfParts[k]
		FOR j=1 TO 6
			READ.NEXT PartPosition[PicNumber,i,j]
		NEXT j
	NEXT i
NEXT k
! Intro
GR.BITMAP.LOAD Nr01,DataDir$+"Goofy05.png"
GR.BITMAP.SIZE Nr01,PicW,PicH
GR.BITMAP.CROP CroppedNr01,Nr01,0,0,440,500
GR.BITMAP.SCALE ScaledNr01,CroppedNr01,440/PicH*ScaleY,ScaleY
GR.BITMAP.DRAW Pic01,ScaledNr01,0,0
GR.RENDER
SOUNDPOOL.OPEN 4
SOUNDPOOL.LOAD Uh,DataDir$+"UhHugh.mp3"
SOUNDPOOL.LOAD All,DataDir$+"AllSet.mp3"
SOUNDPOOL.LOAD Ctune,DataDir$+"ClosingTune.mp3"
SOUNDPOOL.LOAD Cheer,DataDir$+"Cheer.mp3"
FOR Nr=1 TO 2
	GR.BITMAP.CROP Parts[Nr],Nr01,PartPosition[5,Nr,1],PartPosition[5,Nr,2],PartPosition[5,Nr,3],PartPosition[5,Nr,4]
	GR.BITMAP.SCALE ScaledNr01,Parts[Nr],PartPosition[5,Nr,3]*440/PicH,PartPosition[5,Nr,4]*440/PicH
	GR.BITMAP.DRAW Intro[Nr],ScaledNr01,PartPosition[5,Nr,5]*440/PicH,PartPosition[5,Nr,6]*440/PicH
	GR.HIDE Intro[Nr]
	GR.RENDER
NEXT Nr
GR.COLOR 255,0,0,255,1
FOR i=1 TO 12
	GR.TEXT.DRAW T[i],20,500+(i-1)*25,T$[i]
NEXT i
FOR i=1 TO 30
	IF i=20 THEN SOUNDPOOL.PLAY StrId1,Uh,0.99,0.99,1,0,1
	GR.SHOW Intro[(i/2=FLOOR(i/2))+1]
	GR.HIDE Intro[2-(i/2=FLOOR(i/2))]
	GR.RENDER
	PAUSE 50
	GR.SHOW Intro[2-(i/2=FLOOR(i/2))]
	GR.HIDE Intro[(i/2=FLOOR(i/2))+1]
	GR.RENDER
	PAUSE 50
NEXT i
GR.HIDE Intro[1]
FOR i=1 TO 30
	GR.MODIFY Pic01,"y",-i*15
	IF i=20 THEN SOUNDPOOL.PLAY StrId1,Uh,0.99,0.99,1,0,1
	FOR q=1 TO 12
		GR.MODIFY T[q],"y",500+(q-1)*25-i*15
	NEXT q
	PAUSE 50
	GR.RENDER
NEXT i
GOSUB GetTouch
GR.BITMAP.DELETE Nr01
GR.BITMAP.DELETE CroppedNr01
GR.BITMAP.DELETE ScaledNr01
DO
	GR.CLS
	GR.BITMAP.DRAW Start,ScaledChoice,0,0
	GR.ROTATE.START 90,500,130
		IF Mute=0 THEN
			GR.COLOR 255,255,0,0,1
			GR.TEXT.DRAW g,500,130,"Sound is OFF"
		ELSE
			GR.COLOR 255,0,128,0,1
			GR.TEXT.DRAW g,500,130,"Sound is ON"
		ENDIF
	GR.ROTATE.END
	GR.RENDER
	PicW=SPicW
	PicH=SPicH
	GOSUB GetTouch
	PicNumber=CEIL(x/240)+CEIL(y/240)*3-3
	IF ((PicNumber>0 & PicNumber<5) | (PicNumber>5 & PicNumber<10)) & x<PicW & y<PicH THEN
		IF Mute=1 THEN SOUNDPOOL.PLAY StrId2,All,0.99,0.99,1,0,1
		GOSUB ReadPic
		FOR i=1 TO NumOfParts[PicNumber]
			ChosenParts[i]=0
		NEXT i
		TotalSum=0
		DO
			GOSUB GetTouch
			Nr=0
			FOR j=1 TO NumOfParts[PicNumber]
				IF x>PartPosition[PicNumber,j,1] & x<PartPosition[PicNumber,j,1]+PartPosition[PicNumber,j,3] & y>PartPosition[PicNumber,j,2] & y<PartPosition[PicNumber,j,2]+PartPosition[PicNumber,j,4] THEN
					Nr=j
				ENDIF
			NEXT j
			IF Nr>0 THEN
				IF ChosenParts[Nr]=0 THEN
					IF Mute=1 THEN SOUNDPOOL.PLAY StrId1,Uh,0.99,0.99,1,0,1
					ChosenParts[Nr]=1
					GOSUB DrawPart
					TotalSum=TotalSum+1
				ENDIF
			ENDIF
		UNTIL TotalSum=NumOfParts[PicNumber]
		POPUP "WELL DONE !!!",0,0,1
		IF Mute=1 THEN SOUNDPOOL.PLAY StrId4,Cheer,0.99,0.99,1,0,1
		PAUSE 5000
	ENDIF
	IF x>279 & x<443 & y>249 & y<284 THEN
		IF Mute=1 THEN
			Mute=0
		ELSE
			Mute=1
		ENDIF
	ENDIF
	IF x>279 & x<443 & y>427 & y<462 THEN
		Kap=1
	ENDIF
UNTIL Kap=1
IF Mute=1 THEN SOUNDPOOL.PLAY StrId3,Ctune,0.99,0.99,1,0,1
POPUP "Please, wait for Goofy to exit ...",0,0,1
PAUSE 7000
WAKELOCK 5
GR.CLOSE
EXIT

GetTouch:
	DO
		GR.TOUCH Touched,x,y
	UNTIL Touched
	DO
		GR.TOUCH Touched,x,y
	UNTIL !Touched
	x/=sx
	y/=sy
	x=x*PicW/(PicW/PicH*ScaleY)
	y=y*PicH/ScaleY
RETURN

ReadPic:
	GR.CLS
	FileName$="Goofy"+REPLACE$(FORMAT$("%%",PicNumber)," ","")+".png"
	GR.BITMAP.LOAD Nr01,DataDir$+FileName$
	GR.BITMAP.SIZE Nr01,PicW,PicH
	GR.BITMAP.CREATE MemNr01,PicW,PicH
	GR.BITMAP.DRAWINTO.START MemNr01
		GR.BITMAP.DRAW Pic,Nr01,0,0
	GR.BITMAP.DRAWINTO.END
	GR.BITMAP.SCALE ScaledNr01,MemNr01,PicW/PicH*ScaleY,ScaleY
	GR.BITMAP.DRAW Pic01,ScaledNr01,0,0
	GR.RENDER
	GR.BITMAP.DELETE ScaledNr01
RETURN

DrawPart:
	GR.CLS
	GR.BITMAP.CROP Parts[Nr],Nr01,PartPosition[PicNumber,Nr,1],PartPosition[PicNumber,Nr,2],PartPosition[PicNumber,Nr,3],PartPosition[PicNumber,Nr,4]
	GR.BITMAP.DRAWINTO.START MemNr01
		GR.BITMAP.DRAW g,Parts[Nr],PartPosition[PicNumber,Nr,5],PartPosition[PicNumber,Nr,6]
		GR.COLOR 255,255,255,255,1
		GR.RECT g,PartPosition[PicNumber,Nr,1],PartPosition[PicNumber,Nr,2],PartPosition[PicNumber,Nr,1]+PartPosition[PicNumber,Nr,3],PartPosition[PicNumber,Nr,2]+PartPosition[PicNumber,Nr,4]
	GR.BITMAP.DRAWINTO.END
	GR.BITMAP.SCALE ScaledNr01,MemNr01,PicW/PicH*ScaleY,ScaleY
	GR.BITMAP.DRAW Pic01,ScaledNr01,0,0
	GR.RENDER
	GR.BITMAP.DELETE ScaledNr01
	GR.BITMAP.DELETE Parts[Nr]
RETURN

ONBACKKEY:
	POPUP "Please, finish game first...",0,0,0
BACK.RESUME
ONMENUKEY:
	POPUP "Please, finish game first...",0,0,0
MENUKEY.RESUME

ScrollText:
T$[1]="On the main screen tap on one"
T$[2]="of the eight Goofy pictures."
T$[3]="This will load a picture of"
T$[4]="Goofy fairly silly dressed."
T$[5]="Just tap on the parts on the"
T$[6]="rightside of the screen."
T$[7]="The parts will fly onto"
T$[8]="Goofy, until he is properly"
T$[9]="dressed up."
T$[10]="When all parts have fallen"
T$[11]="into place, choose a new Goofy."
T$[12]="Tap screen to start."
RETURN

READ.DATA 1,10,360,0,61,64,37,10,435,0,93,90,100,51,540,30,19,23,129,113,585,0,115,136,143,71,360,110,109,134,4,164,585,144,105,166,56,190,490,170,47,55,113,137,365,250,216,120,21,319,360,380,133,69,6,423,540,420,135,43,200,392
READ.DATA 2,8,345,0,76,106,158,26,440,40,38,35,202,50,500,20,105,73,197,62,630,20,55,99,269,193,345,130,245,317,3,134,630,170,22,38,151,174,540,450,158,63,131,409,370,450,157,63,40,449
READ.DATA 3,8,380,15,83,55,239,175,480,5,160,111,146,148,660,45,20,21,244,193,380,125,114,116,77,186,530,165,41,40,46,164,615,175,43,24,127,330,435,260,68,115,3,112,550,300,107,38,103,345
READ.DATA 4,8,360,0,62,82,81,23,450,5,88,75,39,131,570,20,92,45,201,108,360,100,158,122,90,184,480,260,200,202,96,266,555,130,122,70,44,406,345,290,129,70,208,410,420,250,21,15,72,181
READ.DATA 5,2,440,0,227,97,102,1,480,110,187,97,140,10
READ.DATA 6,9,360,0,35,49,2,171,435,0,48,61,98,197,510,0,34,72,96,143,570,0,76,81,53,249,370,120,17,13,105,242,440,100,61,70,124,233,540,135,126,118,167,186,400,280,84,47,172,129,570,310,51,70,191,289
READ.DATA 7,8,320,10,42,40,46,27,380,20,33,25,126,64,425,5,106,115,60,35,545,2,75,155,166,59,320,140,222,233,71,9,546,200,172,236,71,200,275,375,138,122,109,302,430,440,183,60,71,437
READ.DATA 8,10,374,35,96,397,14,28,615,95,23,40,275,110,520,5,115,84,115,146,655,15,106,67,90,223,545,100,19,30,166,255,660,90,116,302,253,166,470,140,188,118,58,293,540,270,105,107,170,298,515,415,42,75,266,305,570,430,207,54,196,445
READ.DATA 9,7,360,5,218,103,50,134,580,5,100,74,238,102,605,90,49,120,2,165,385,120,174,130,96,207,415,280,51,52,147,276,505,300,16,13,164,281,570,280,49,28,157,345
