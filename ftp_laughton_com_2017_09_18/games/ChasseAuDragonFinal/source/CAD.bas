!'---------------------------------------------------------------------------------------------------
!'
!'                               CHASSE AU DRAGON, PAR MOUGINO (2012)
!'                    JEU GENERE PAR ANDROID RPG MAKER ET PROPULSE PAR RFO-BASIC!
!'
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Définition des fonctions usuelles
!'---------------------------------------------------------------------------------------------------
Fn.def Stg$(c)
  Fn.rtn Left$(Str$(c),Is_In(".",Str$(c))-1)
Fn.end

Fn.def SetRGB(alpha,RGB$,fill)
  gr.color alpha,Hex(Mid$(RGB$,1,2)),Hex(Mid$(RGB$,3,2)),Hex(Mid$(RGB$,5,2)),fill
Fn.end

Fn.def RainbowColor$(c)
  !' balaie les couleurs de l'arc-en-ciel, de c=0 (bleu) à c=4.3 (rouge)
  zu=c
  i=Floor(zu)
  if i=0 then
    r=255
    v=255*zu
    b=0
  elseif i=1 then
    r=255*(2-zu)
    v=255
    b=0
  elseif i=2 then
    r=0
    v=255
    b=255*(zu-2)
  elseif i=3 then
    r=0
    v=255*(4-zu)
    b=255
  elseif i=4 then
    r=255*(zu-4)
    v=0
    b=255
  elseif i=5 then
    r=255
    v=0
    b=255*(6-zu)
  end if
  e$=Hex$(r)
  while len(e$)<2
    e$="0"+e$
  repeat
  r$=e$
  e$=Hex$(v)
  while len(e$)<2
    e$="0"+e$
  repeat
  r$=r$+e$
  e$=Hex$(b)
  while len(e$)<2
    e$="0"+e$
  repeat
  r$=r$+e$
  Fn.rtn r$
Fn.end

Fn.def IntToStr$(c)
  b2=Mod(c,256)
  b1=(c-b2)/256
  Fn.rtn Chr$(b1)+Chr$(b2)
Fn.end

Fn.def GetIntFrom(e$)
  b1=Ascii(e$)
  e$=Right$(e$,Len(e$)-1)
  b2=Ascii(e$)
  e$=Right$(e$,Len(e$)-1)
  Fn.rtn 256*b1+b2
Fn.end

Fn.def GetAudioLength(file$)
  audio.load snd, file$
  audio.length l, snd
  audio.release snd
  Fn.rtn l
Fn.end

Fn.def Label(x,y,txt$)
  gr.color 255,255,255,255,1
  For dy=-1 to 1
    For dx=-1 to 1
      gr.text.draw o,x+dx,y+dy,txt$
    Next dx
  Next dy
  gr.color 255,0,0,0,1
  gr.text.draw o,x,y,txt$
Fn.end

!' Index Objets Multiples
Fn.def IOM(x,y)
  !' objets de type 49
  if x=213 & y=22 then Fn.rtn 51
  if x=239 & y=27 then Fn.rtn 52
  if x=184 & y=67 then Fn.rtn 53
  if x=177 & y=167 then Fn.rtn 54
  if x=146 & y=184 then Fn.rtn 55
  if x=186 & y=195 then Fn.rtn 56
  !' objets de type 17
  if x=19 & y=27 then Fn.rtn 57
  if x=15 & y=44 then Fn.rtn 58
  if x=19 & y=45 then Fn.rtn 59
  if x=138 & y=50 then Fn.rtn 60
  if x=19 & y=53 then Fn.rtn 61
  if x=19 & y=56 then Fn.rtn 62
  if x=135 & y=67 then Fn.rtn 63
  if x=137 & y=67 then Fn.rtn 64
  if x=136 & y=68 then Fn.rtn 65
  if x=4 & y=91 then Fn.rtn 66
  if x=8 & y=92 then Fn.rtn 67
  if x=31 & y=93 then Fn.rtn 68
  if x=67 & y=196 then Fn.rtn 69
  if x=68 & y=196 then Fn.rtn 70
  if x=65 & y=197 then Fn.rtn 71
  if x=67 & y=197 then Fn.rtn 72
  if x=68 & y=197 then Fn.rtn 73
  if x=64 & y=198 then Fn.rtn 74
  if x=65 & y=198 then Fn.rtn 75
  if x=66 & y=198 then Fn.rtn 76
  if x=67 & y=198 then Fn.rtn 77
  if x=66 & y=199 then Fn.rtn 78
  if x=67 & y=199 then Fn.rtn 79
  if x=63 & y=200 then Fn.rtn 80
  if x=64 & y=200 then Fn.rtn 81
  if x=65 & y=200 then Fn.rtn 82
  if x=66 & y=200 then Fn.rtn 83
  if x=67 & y=200 then Fn.rtn 84
  if x=62 & y=201 then Fn.rtn 85
  if x=65 & y=201 then Fn.rtn 86
  if x=66 & y=201 then Fn.rtn 87
  if x=64 & y=202 then Fn.rtn 88
  !' objets de type 12
  if x=0 & y=32 then Fn.rtn 89
  if x=161 & y=49 then Fn.rtn 90
  if x=126 & y=73 then Fn.rtn 91
  if x=101 & y=75 then Fn.rtn 92
  if x=5 & y=123 then Fn.rtn 93
  if x=5 & y=127 then Fn.rtn 94
  if x=0 & y=155 then Fn.rtn 95
  if x=71 & y=194 then Fn.rtn 96
  if x=70 & y=195 then Fn.rtn 97
  if x=71 & y=195 then Fn.rtn 98
  if x=70 & y=196 then Fn.rtn 99
  if x=71 & y=196 then Fn.rtn 100
  if x=66 & y=197 then Fn.rtn 101
  if x=69 & y=197 then Fn.rtn 102
  if x=70 & y=197 then Fn.rtn 103
  if x=69 & y=198 then Fn.rtn 104
  if x=64 & y=199 then Fn.rtn 105
  if x=65 & y=199 then Fn.rtn 106
  if x=68 & y=199 then Fn.rtn 107
  if x=69 & y=199 then Fn.rtn 108
  if x=68 & y=200 then Fn.rtn 109
  if x=63 & y=201 then Fn.rtn 110
  !' objets de type 50
  if x=157 & y=42 then Fn.rtn 111
  if x=8 & y=125 then Fn.rtn 112
  if x=168 & y=135 then Fn.rtn 113
  if x=161 & y=168 then Fn.rtn 114
  if x=182 & y=182 then Fn.rtn 115
  if x=108 & y=186 then Fn.rtn 116
  if x=169 & y=206 then Fn.rtn 117
  if x=237 & y=208 then Fn.rtn 118
  if x=237 & y=210 then Fn.rtn 119
  if x=237 & y=212 then Fn.rtn 120
  !' objets de type 46
  if x=156 & y=135 then Fn.rtn 121
  if x=183 & y=171 then Fn.rtn 122
  if x=237 & y=191 then Fn.rtn 123
  if x=174 & y=195 then Fn.rtn 124
  !' objets de type 47
  if x=158 & y=136 then Fn.rtn 125
  if x=160 & y=136 then Fn.rtn 126
  if x=167 & y=169 then Fn.rtn 127
  if x=182 & y=170 then Fn.rtn 128
  if x=167 & y=177 then Fn.rtn 129
  if x=160 & y=182 then Fn.rtn 130
  if x=233 & y=187 then Fn.rtn 131
  !' objets de type 45
  if x=164 & y=166 then Fn.rtn 132
  if x=173 & y=181 then Fn.rtn 133
  if x=230 & y=191 then Fn.rtn 134
  if x=160 & y=198 then Fn.rtn 135
  if x=181 & y=199 then Fn.rtn 136
  if x=133 & y=238 then Fn.rtn 137
  if x=133 & y=246 then Fn.rtn 138
  !' objets de type 48
  if x=191 & y=184 then Fn.rtn 139
  if x=150 & y=188 then Fn.rtn 140
  if x=235 & y=195 then Fn.rtn 141
  Fn.rtn 0
Fn.end
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Démarrage
!'---------------------------------------------------------------------------------------------------
Pi=3.14159265
UNDEF=2^16-1
TBC$="00373D" %' Title Background Color
TFC$="FFE7C1" %' Title Foreground Color
LOM$=",12,17,37,38,39,40,45,46,47,48,49,50,"
BgnBloc$=",1,7,8,9,10,11,12,13,14,16,18,19,23,26,29,30,33,34,36,41," %' le héros peut créer des blocs
BgnBloc$=BgnBloc$+"43,44,53,54,55,56,57,58,59,62,65,66,67,68,69,70,"    %' sur ces tiles de paysage
Dim objet[141] %' index max comprenant les objets multiples
_up=0 %' n° des tiles correspondant aux directions des héros
_down=3
_right=6
_left=10
_upBat=0 %' n° des tiles correspondant aux directions du bateau
_downBat=1
_rightBat=2
_leftBat=3
tilew=18
tileh=18
mapw=240
maph=250
sol0=71
obj0=91
chr0=141
nthor=21 %' ALTTP=15 (13 is good)
ntver=13 %' ALTTP=13 (9 is good)
padsize=1.25 %' en tiles
mapWmax=mapw+nthor+3
mapHmax=maph+ntver+3
Dim bgnMap$[mapHmax]
Dim solMap$[mapHmax]
Dim objMap$[mapHmax]
Dim chrMap$[mapHmax]
Dim bgnIni$[mapHmax]
Dim solIni$[mapHmax]
Dim objIni$[mapHmax]
Dim chrIni$[mapHmax]
Dim usdPtr[9]
array.load VibrCrt[], 1, 75
!' Initialisation graphique
gr.open 255,0,0,0
gr.screen real_w, real_h
if Mod(nthor,2)=0 then nthor=nthor - 1
if Mod(ntver,2)=0 then ntver=ntver - 1
if herox<1 then herox=1
if heroy<1 then heroy=1
if herox>mapw then herox=mapw
if heroy>maph then heroy=maph
dx=Ceil(nthor/2)+1
dy=Ceil(ntver/2)+1
di_w=640
di_h=480
scale_w=real_w/di_w
scale_h=real_h/di_h
gr.scale scale_w, scale_h
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Vérif. répertoire du jeu + préférences (langue, mode silencieux, tempos mouvements)
!'---------------------------------------------------------------------------------------------------
File.Exists o, "../prefs.txt"
if o Then
  Text.open r, c, "../prefs.txt"
  Text.readln c, LNG$
  Text.readln c, e$
  mute=VAL(e$)
  Text.close c
End If
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Sélection de la langue
!'---------------------------------------------------------------------------------------------------
if LNG$="" then
  array.load LngLbl$[],"Français","English"
  array.load LngCod$[],"FR","EN"
  l=1
  Call SetRGB(255,"DDDDDD",0)
  gr.set.stroke 10
  gr.arc tmp,87,111,358,279,-177,95,0
  gr.line tmp,86,186,79,164
  gr.line tmp,86,186,108,176
  gr.arc tmp,282,111,552,279,-98,95,0
  gr.line tmp,553,186,562,164
  gr.line tmp,553,186,533,176
  gr.set.stroke 0
  gr.text.size 54
  gr.text.align 2
  gr.text.skew -0.15
  gr.text.draw nul,320,336,LngLbl$[l]
  gr.get.textbounds LngLbl$[l],a1,b1,a2,b2
  gr.text.skew 0
  gr.render
  do
    gr.touch tmp,x,y
    if tmp then
      x0=x
      y0=y
      x=x/scale_w
      y=y/scale_h
      if x>320+a1-20 & x<320+a2+20 & y>336+b1-20 & y<336+b2+20 then %' chose a language
        vibrate VibrCrt[], -1
        for t=0 to 10
          gr.cls
          Call SetRGB(255,"DDDDDD",0)
          gr.text.size 54+2*t
          gr.text.align 2
          gr.text.skew -0.15
          gr.text.draw nul,320,336-15*t,LngLbl$[l]
          gr.text.skew 0
          gr.render
          Pause 20
        next t
        gr.modify nul,"y",171
        gr.modify nul,"text",LngLbl$[l]+" ?"
        gr.set.stroke 10
        Call SetRGB(255,"DD0000",0)
        gr.line tmp,63,374,130,442
        gr.line tmp,63,442,130,374
        Call SetRGB(255,"00DD00",0)
        gr.line tmp,484,389,512,445
        gr.line tmp,510,445,549,342
        gr.line tmp,547,342,561,365
        gr.set.stroke 0
        gr.render
        do
          gr.touch tmp,x,y
          if tmp then
            x=x/scale_w
            y=y/scale_h
            if x>50 & x<150 & y>350 & y<480 then %' cancel
              vibrate VibrCrt[], -1
              Call SetRGB(255,"DDDDDD",0)
              for t=10 to 0 step -1
                gr.cls
                Call SetRGB(255,"DDDDDD",0)
                gr.text.size 54+2*t
                gr.text.align 2
                gr.text.skew -0.15
                gr.text.draw nul,320,336-15*t,LngLbl$[l]
                gr.text.skew 0
                gr.render
                Pause 20
              next t
              c=1
              gr.set.stroke 10
              gr.arc tmp,87,111,358,279,-177,95,0
              gr.line tmp,86,186,79,164
              gr.line tmp,86,186,108,176
              gr.arc tmp,282,111,552,279,-98,95,0
              gr.line tmp,553,186,562,164
              gr.line tmp,553,186,533,176
              gr.set.stroke 0
              gr.render
            elseif x>465 & x<580 & y>320 & y<480 then %' validate
              vibrate VibrCrt[], -1
              LNG$=LngCod$[l]
            end if
          end if
        until c | LNG$<>"" | BackKey | Background()
        if BackKey | Background() then Exit
        c=0
      else %' touch on other part of screen
        Pause 100
        gr.touch tmp,x,y
        if tmp then %' swipe
          if x>x0+50*scale_w then %' next language
            vibrate VibrCrt[], -1
            for t=-Pi/2 to 0 step Pi/40
              gr.modify nul,"x",320*(1+cos(t))
              gr.modify nul,"y",240*(1+sin(t))+336
              gr.render
              Pause 20
            next t
            array.length w,LngLbl$[]
            l=l+1
            if l>w then l=1
            gr.modify nul,"text",LngLbl$[l]
            gr.get.textbounds LngLbl$[l],a1,b1,a2,b2
            for t=-Pi to -Pi/2 step Pi/40
              gr.modify nul,"x",320*(1+cos(t))
              gr.modify nul,"y",240*(1+sin(t))+336
              gr.render
              Pause 20
            next t
          elseif x<x0-50*scale_w then %' previous language
            vibrate VibrCrt[], -1
            for t=-Pi/2 to -Pi step -Pi/40
              gr.modify nul,"x",320*(1+cos(t))
              gr.modify nul,"y",240*(1+sin(t))+336
              gr.render
              Pause 20
            next t
            array.length w,LngLbl$[]
            l=l-1
            if l<1 then l=w
            gr.modify nul,"text",LngLbl$[l]
            gr.get.textbounds LngLbl$[l],a1,b1,a2,b2
            for t=0 to -Pi/2 step -Pi/40
              gr.modify nul,"x",320*(1+cos(t))
              gr.modify nul,"y",240*(1+sin(t))+336
              gr.render
              Pause 20
            next t
          end if
        end if
      end if
    end if
  until LNG$<>"" | BackKey | Background()
  if BackKey | Background() then Exit
  Gosub SavePrefs
end if
Gosub FadeOut
gr.cls
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Splash Screen "développeur présente" + Chargement des ressources 1/2
!'---------------------------------------------------------------------------------------------------
Dim RPGLBL$[808]
File.delete nul,"CAD.EN" %' v1.01
File.delete nul,"CAD.FR" %' v1.01
Text.open r, hr1, "CAD.TRAD."+LNG$
Text.readln hr1, RPGLBL$[1]
MDP$=RPGLBL$[1]
Text.readln hr1, RPGLBL$[2]
for i=0 to 120
  Call SetRGB(255-2*(i+7),"7A0000",1)
  gr.rect nul,320+2*i,0,322+2*i,340
  gr.rect nul,318-2*i,0,320-2*i,340
  gr.rect nul,0,359+i,640,360+i
next i
for i=0 to 120
  Call SetRGB(2*(i+7),"000000",1)
  gr.rect nul,0,239+i,640,240+i
  Call SetRGB(120-i,"C65B7C",1)
  gr.oval nul,320-i,400-i/4,320+i,400+i/4
next i
Call SetRGB(255,"DDDDDD",1)
gr.text.size 48
gr.text.align 2
gr.text.skew -0.15
gr.text.draw nul,320,100,RPGLBL$[2]
gr.text.skew 0
gr.bitmap.load tmpBmp,"mougino.png"
gr.bitmap.scale b1,tmpBmp,120,144
gr.bitmap.delete tmpBmp
gr.bitmap.draw nul,b1,260,180
gr.render
splashTmr=Clock()

!' Chargement des labels
Array.length c, RPGLBL$[]
for i=3 to c
  Text.readln hr1, RPGLBL$[i]
next i
Text.close hr1
array.load Mois$[],RPGLBL$[3],RPGLBL$[4],RPGLBL$[5],RPGLBL$[6],RPGLBL$[7],RPGLBL$[8],RPGLBL$[9]~
RPGLBL$[10],RPGLBL$[11],RPGLBL$[12],RPGLBL$[13],RPGLBL$[14]

!' Chargement de l'ensemble des tiles
for b=1 to 4
  gr.bitmap.load tmpBmp,"hero.form" + Stg$(b) + ".png"
  gr.bitmap.size tmpBmp, w, h
  l=h/tileh * w/tilew
  if b=1 then dim heroTile[5,l]
  for y=0 to h/tileh - 1
    for x=0 to w/tilew - 1
      gr.bitmap.crop heroTile[b, y*w/tilew+x+1], tmpBmp, x*tilew, y*tileh, tilew, tileh
    next x
  next y
  gr.bitmap.delete tmpBmp
next b

dim sansHeaumeBmp[l] %' heaume sweet heaume
for i=1 to l
  sansHeaumeBmp[i]=heroTile[1, i]
next i
dim avecHeaumeBmp[l]
gr.bitmap.load tmpBmp,"hero.form1b.png"
for y=0 to h/tileh - 1
  for x=0 to w/tilew - 1
    gr.bitmap.crop avecHeaumeBmp[y*w/tilew+x+1], tmpBmp, x*tilew, y*tileh, tilew, tileh
  next x
next y
gr.bitmap.delete tmpBmp

gr.bitmap.scale miniHeroBmp,heroTile[1,4],9,9,1 %' miniature héros humain pour carte du monde

gr.bitmap.load tmpBmp,"hero.form5.png" %' bateau
gr.bitmap.size tmpBmp, w, h
for y=0 to h/2 step h/2
  for x=0 to w/2 step w/2
    gr.bitmap.crop heroTile[5, 2*y*2/h+x*2/w+1], tmpBmp, x, y, 3*tilew, 2*tileh
  next x
next y
gr.bitmap.delete tmpBmp

gr.bitmap.load tmpBmp,"background.png"
gr.bitmap.size tmpBmp, w, h
l=h/tileh * w/tilew
dim bgnTile[l]
for y=0 to h/tileh - 1
  for x=0 to w/tilew - 1
    gr.bitmap.crop bgnTile[y*w/tilew+x+1], tmpBmp, x*tilew, y*tileh, tilew, tileh
  next x
next y
gr.bitmap.delete tmpBmp

gr.bitmap.load tmpBmp,"objects.png"
gr.bitmap.size tmpBmp, w, h
l=h/tileh * w/tilew
dim objTile[l]
dim equipement[l]
for y=0 to h/tileh - 1
  for x=0 to w/tilew - 1
    gr.bitmap.crop objTile[y*w/tilew+x+1], tmpBmp, x*tilew, y*tileh, tilew, tileh
  next x
next y
gr.bitmap.delete tmpBmp

gr.bitmap.load tmpBmp,"characters.png"
gr.bitmap.size tmpBmp, w, h
l=h/tileh * w/tilew
dim chrTile[l]
dim perso[l]
dim chrX[l]
dim chrY[l]
for y=0 to h/tileh - 1
  for x=0 to w/tilew - 1
    gr.bitmap.crop chrTile[y*w/tilew+x+1], tmpBmp, x*tilew, y*tileh, tilew, tileh
  next x
next y
gr.bitmap.delete tmpBmp

!' cas spécial tiles consolidées
!' alligator 1 (2x1)
gr.bitmap.create tmpBmp, 2*tilew, tileh
gr.bitmap.drawinto.start tmpBmp
gr.bitmap.draw i, chrTile[61], 0, 0
gr.bitmap.draw i, chrTile[62], tilew, 0
gr.bitmap.drawinto.end
chrTile[61]=tmpBmp
!' alligator 2 (2x1)
gr.bitmap.create tmpBmp, 2*tilew, tileh
gr.bitmap.drawinto.start tmpBmp
gr.bitmap.draw i, chrTile[63], 0, 0
gr.bitmap.draw i, chrTile[64], tilew, 0
gr.bitmap.drawinto.end
chrTile[63]=tmpBmp
!' Seigneur Dragon
gr.bitmap.create tmpBmp, 3*tilew, 2*tileh %' dragon repos (3x2)
gr.bitmap.drawinto.start tmpBmp
for y=0 to 1
  for x=0 to 2
    gr.bitmap.draw i, chrTile[26+10*y+x], x*tilew, y*tileh
  next x
next y
gr.bitmap.drawinto.end
DragonRepos=tmpBmp
gr.bitmap.create tmpBmp, 3*tilew, 2*tileh %' dragon feu (3x2)
gr.bitmap.drawinto.start tmpBmp
for y=0 to 1
  for x=0 to 2
    gr.bitmap.draw i, chrTile[41+10*y+x], x*tilew, y*tileh
  next x
next y
gr.bitmap.drawinto.end
DragonFeu=tmpBmp
gr.bitmap.create tmpBmp, 3*tilew, 2*tileh %' dragon blessé (3x2)
gr.bitmap.drawinto.start tmpBmp
for y=0 to 1
  for x=0 to 2
    gr.bitmap.draw i, chrTile[44+10*y+x], x*tilew, y*tileh
  next x
next y
gr.bitmap.drawinto.end
DragonBlesse=tmpBmp
gr.bitmap.create tmpBmp, 3*tilew, 2*tileh %' dragon mort (3x2)
gr.bitmap.drawinto.start tmpBmp
for y=0 to 1
  for x=0 to 2
    gr.bitmap.draw i, chrTile[47+10*y+x], x*tilew, y*tileh
  next x
next y
gr.bitmap.drawinto.end
DragonMort=tmpBmp

!' miniature bateau pour carte du monde
gr.bitmap.create tmpBmp, 3*tilew, 2*tileh
gr.bitmap.drawinto.start tmpBmp
for y=2 to 3
  for x=3 to 5
    gr.bitmap.draw i, chrTile[y*10+x], (x-3)*tilew, (y-2)*tileh
  next x
next y
gr.bitmap.drawinto.end
gr.bitmap.scale miniBatBmp, tmpBmp,9*1.5,9,1
gr.bitmap.delete tmpBmp

!' Ellipse de pluie générée par la croix de l'eau
Dim rainBmp[2]
gr.bitmap.load rainBmp[1],"rain1.png"
gr.bitmap.load rainBmp[2],"rain2.png"
array.load rainOrder[],1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23
array.load rainX[],-1,0,1,-2,-1,0,1,2,-3,-2,-1,0,1,2,3,-2,-1,0,1,2,-1,0,1
array.load rainY[],-2,-2,-2,-1,-1,-1,-1,-1,0,0,0,0,0,0,0,1,1,1,1,1,2,2,2

!' Salle du paysage changeant dans la pyramide
array.load pyrPaysageOrder[],1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27
array.load pyrPaysageX[],230,230,230,230,230,230,230,230,230,231,231,231,231,231,231,231,231~
232,232,232,232,232,232,233,233,233,233
array.load pyrPaysageY[],206,207,208,209,210,211,212,213,214,206,207,208,209,210,211,212,213~
207,208,209,210,211,212,208,209,210,211

if Clock()-splashTmr<2000 then Pause 2000-Clock()+splashTmr
gr.bitmap.delete b1
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Splash Screen "un jeu Android RPG Maker" + Chargement des ressources 2/2
!'---------------------------------------------------------------------------------------------------
gr.cls
Call SetRGB(4,"6666CC",1)
for i=0 to 120
  gr.circle nul,320,480+10*i,600
next i
Call SetRGB(255,"FFFFFF",1)
gr.text.size 48
gr.text.align 2
gr.text.skew -0.15
gr.text.draw nul,320,100,RPGLBL$[15]
gr.text.skew 0
gr.text.size 22
gr.text.align 3
gr.text.draw nul, 614, 470, "http://android-rpg-maker.com"
gr.bitmap.load tmpBmp,"android_rpg_maker.png"
gr.bitmap.scale b2,tmpBmp,120,144
gr.bitmap.delete tmpBmp
gr.bitmap.draw nul,b2,180,200
gr.text.align 1
gr.text.bold 1
Call SetRGB(255,"A0A0A0",1)
gr.text.size 42
gr.text.draw nul,300,254,"Android"
gr.text.draw nul,300,344,"Maker"
gr.text.size 60
gr.text.draw nul,298,306,"RPG"
gr.text.bold 0
gr.render
splashTmr=Clock()

!' Ecran de titre
gr.bitmap.load tmpBmp,"title.jpg"
gr.bitmap.scale titBmp, tmpBmp, 640, 400, 0
gr.bitmap.delete tmpBmp
gr.bitmap.load menuBmp,"menu.png"
gr.bitmap.crop nul, menuBmp, 0, 0, 36, 36 %' icone Exit
gr.bitmap.scale exitBmp, nul, 46, 55, 0
gr.bitmap.delete nul

!' Création du calque "bougie" (gradient d'obscurité)
gr.bitmap.create lightBmp, nthor*tilew, ntver*tileh
gr.bitmap.drawinto.start lightBmp
gr.bitmap.load tmpBmp, "light.png"
gr.bitmap.size tmpBmp, w, h
for y=0 to h-1
  for x=0 to w-1
    gr.get.bmpixel tmpBmp,x,y,alpha,red,green,blue
    gr.color alpha,0,0,0,1
    up=(Floor(nthor/2)-x)*tilew
    vp=(Floor(ntver/2)-y)*tileh
    um=(Floor(nthor/2)+x)*tilew
    vm=(Floor(ntver/2)+y)*tileh
    gr.rect i,up,vp,up+tilew,vp+tileh
    if y<>0 then gr.rect i,up,vm,up+tilew,vm+tileh
    if x<>0 then gr.rect i,um,vp,um+tilew,vp+tileh
    if x<>0 & y<>0 then gr.rect i,um,vm,um+tilew,vm+tileh
  next x
next y
gr.bitmap.delete tmpBmp
gr.bitmap.drawinto.end

!' Images diverses
gr.bitmap.load coeurBmp,"coeur.png"
gr.bitmap.load tmpBmp,"icon.png"
gr.bitmap.scale gamBmp,tmpBmp,60,72
gr.bitmap.delete tmpBmp
gr.bitmap.load tmpBmp,"mougino.png"
gr.bitmap.scale youBmp,tmpBmp,60,72
gr.bitmap.delete tmpBmp
gr.bitmap.load tmpBmp,"android_rpg_maker.png"
gr.bitmap.scale aRPGmB,tmpBmp,60,72
gr.bitmap.delete tmpBmp
gr.bitmap.load tmpBmp,"basic.png"
gr.bitmap.scale basBmp,tmpBmp,60,72
gr.bitmap.delete tmpBmp
gr.bitmap.load diskBmp,"disk.png"
if LNG$="FR" then gr.bitmap.load sandBmp,"sand-island.png" else gr.bitmap.load sandBmp,"sand-island.en.png"
gr.bitmap.load paramBmp,"settings.png"
gr.bitmap.load shieldBmp,"shield.png"
gr.bitmap.load cheatBmp,"cheat_menu.png"
if LNG$="FR" then gr.bitmap.load tmpBmp,"carte_maritime.png" else gr.bitmap.load tmpBmp,"carte_maritime.en.png"
gr.bitmap.size tmpBmp, w, h
gr.bitmap.create worldMapBmp, w, h
gr.bitmap.drawinto.start worldMapBmp
gr.bitmap.draw tmp,tmpBmp,0,0
gr.bitmap.drawinto.end
gr.bitmap.delete tmpBmp
if LNG$="FR" then gr.bitmap.load tmpBmp,"runes.png" else gr.bitmap.load tmpBmp,"runes.en.png"
gr.bitmap.size tmpBmp, w, h
gr.bitmap.create parchmentBmp, w, h
gr.bitmap.drawinto.start parchmentBmp
gr.bitmap.draw tmp,tmpBmp,0,0
gr.bitmap.drawinto.end
gr.bitmap.delete tmpBmp
if LNG$="FR" then gr.bitmap.load tmpBmp,"carte_labyrinthe.png" else gr.bitmap.load tmpBmp,"carte_labyrinthe.en.png"
gr.bitmap.size tmpBmp, w, h
gr.bitmap.create mazeMapBmp, w, h
gr.bitmap.drawinto.start mazeMapBmp
gr.bitmap.draw tmp,tmpBmp,0,0
gr.bitmap.drawinto.end
gr.bitmap.delete tmpBmp
dim flame[4]
for i=1 to 4
  gr.bitmap.load flame[i],"flame"+Stg$(i-1)+".png"
next i
dim fire[nthor]
gr.bitmap.load tmpBmp,"explo.png"
gr.bitmap.size tmpBmp, w, h
l=h/tileh * w/tilew
dim exploBmp[l]
for y=0 to h/tileh - 1
  for x=0 to w/tilew - 1
    gr.bitmap.crop exploBmp[y*w/tilew+x+1], tmpBmp, x*tilew, y*tileh, tilew, tileh
  next x
next y
gr.bitmap.delete tmpBmp

!' Bruitages
Soundpool.open 2
Soundpool.load sndFbd, "interdit.mp3"
Soundpool.load sndHur, "blesse.mp3"
Soundpool.load sndSel, "select.mp3"
Soundpool.load sndUsd, "bourse.mp3"
Soundpool.load sndPdv, "coeur.wav"
Soundpool.load sndRls, "debloque.mid"
Soundpool.load sndBnc, "bouncing.wav" %' objet mineur rebondit
Soundpool.load sndBlo, "bloc.mp3" %' création bloc de glace
Soundpool.load sndPou, "pousse.mp3" %' pousse bloc de glace
Soundpool.load sndMel, "icemelt.wav" %' bloc de glace fond
Soundpool.load sndFal, "tombe.mp3" %' bloc de glace tombe
Soundpool.load sndPRi, "ressort1.wav" %' enclenche un poussoir-ressort
Soundpool.load sndPRo, "ressort2.wav" %' libère un poussoir-ressort
Soundpool.load sndPlo, "plouf.mp3" %' qqchose tombe dans l'eau
Soundpool.load SndWav, "wave.mp3"
Soundpool.load sndHro0, "marteau_rocher_0.mp3"
Soundpool.load sndHro1, "marteau_rocher_1.mp3"
Soundpool.load sndHic0, "marteau_glace_0.mp3"
Soundpool.load sndHic1, "marteau_glace_1.mp3"
Soundpool.load sndHno, "marteau_rien.mp3"
Soundpool.load sndSwo, "epee.mp3"
Soundpool.load sndShi, "bouclier.mp3"
Soundpool.load sndCan, "bougie.mp3"
Soundpool.load sndTor, "tornade.mp3"
Soundpool.load sndMag, "magic.mp3"
Soundpool.load sndVen, "conque_vent.mp3"
Soundpool.load sndEau, "croix_eau.mp3"
Soundpool.load sndIce, "miroir_glaces.mp3"

if Clock()-splashTmr<2000 then Pause 2000-Clock()+splashTmr
gr.bitmap.delete b2
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Ecran d'init + Chargement des calques BGN (décor), SOL (solidités), OBJ (objets) et CHR (persos)
!'---------------------------------------------------------------------------------------------------
InMenu=0 %' 0=init, 1=titre, 2=nouveau O/N, 3=continuer, 4=difficulté, 5=jeu
Gosub InitEcranDeTitre
Call SetRGB(255,TFC$,1)
gr.text.size 26
gr.text.align 2
gr.text.draw o,320,35,RPGLBL$[16]
gr.render
File.Exists o, "../CAD.BGN.SAV"
if !o then
  byte.open r,map,"CAD.BGN.MAP"
  byte.copy map,"../CAD.BGN.SAV"
end if
byte.open r,map,"../CAD.BGN.SAV"
byte.read.byte map,bgnNasc
for i=1 to mapHmax
  Gosub TestToucheEcranDeTitre
  byte.read.buffer map,mapWmax*bgnNasc,bgnMap$[i]
next i
byte.close map
byte.open r,map,"CAD.BGN.MAP"
byte.read.byte map,i
for i=1 to mapHmax
  Gosub TestToucheEcranDeTitre
  byte.read.buffer map,mapWmax*bgnNasc,bgnIni$[i]
next i
byte.close map

File.Exists o, "../CAD.SOL.SAV"
if !o then
  byte.open r,map,"CAD.SOL.MAP"
  byte.copy map,"../CAD.SOL.SAV"
end if
byte.open r,map,"../CAD.SOL.SAV"
byte.read.byte map,solNasc
for i=1 to mapHmax
  Gosub TestToucheEcranDeTitre
  byte.read.buffer map,mapWmax*solNasc,solMap$[i]
next i
byte.close map
byte.open r,map,"CAD.SOL.MAP"
byte.read.byte map,i
for i=1 to mapHmax
  Gosub TestToucheEcranDeTitre
  byte.read.buffer map,mapWmax*solNasc,solIni$[i]
next i
byte.close map

File.Exists o, "../CAD.OBJ.SAV"
if !o then
  byte.open r,map,"CAD.OBJ.MAP"
  byte.copy map,"../CAD.OBJ.SAV"
end if
byte.open r,map,"../CAD.OBJ.SAV"
byte.read.byte map,objNasc
for i=1 to mapHmax
  Gosub TestToucheEcranDeTitre
  byte.read.buffer map,mapWmax*objNasc,objMap$[i]
next i
byte.close map
byte.open r,map,"CAD.OBJ.MAP"
byte.read.byte map,i
for i=1 to mapHmax
  Gosub TestToucheEcranDeTitre
  byte.read.buffer map,mapWmax*objNasc,objIni$[i]
next i
byte.close map

File.Exists o, "../CAD.CHR.SAV"
if !o then
  byte.open r,map,"CAD.CHR.MAP"
  byte.copy map,"../CAD.CHR.SAV"
end if
byte.open r,map,"../CAD.CHR.SAV"
byte.read.byte map,chrNasc
for i=1 to mapHmax
  Gosub TestToucheEcranDeTitre
  byte.read.buffer map,mapWmax*chrNasc,chrMap$[i]
next i
byte.close map
byte.open r,map,"CAD.CHR.MAP"
byte.read.byte map,i
for i=1 to mapHmax
  Gosub TestToucheEcranDeTitre
  byte.read.buffer map,mapWmax*chrNasc,chrIni$[i]
next i
byte.close map
!'---------------------------------------------------------------------------------------------------

MenuPrincipal:
!'---------------------------------------------------------------------------------------------------
!' Ecran de titre - Menu principal: Son On/Off | Nouveau jeu & niveau de difficulté | Continuer
!'---------------------------------------------------------------------------------------------------
InMenu=1 %' 0=init, 1=titre, 2=nouveau O/N, 3=continuer, 4=difficulté, 5=jeu
if InScores | InApropos then
  Gosub TestToucheEcranDeTitre %' si sous-menu scores ou à propos déjà en fond depuis la phase précédente
else
  Gosub DessineBoutonsMenuPrincipal
end if
do
  Gosub TestToucheEcranDeTitre
  gr.bounded.touch tmp,4*scale_w,2*scale_h,56*scale_w,54*scale_h
  inkey$ k$
  if k$="key 24" | k$="key 25" then tmp=1 %' Touches Vol+/Vol-
  if tmp then %' L'utilisateur a touché l'icône haut-parleur
    mute=1 - mute
    Gosub SavePrefs
    if mute then
      gr.show zr
      gr.show zl
      gr.render
      for i=1 to 3
        vibrate VibrCrt[], -1
        Pause 150
      next i
    else
      gr.hide zr
      gr.hide zl
      gr.render
      Soundpool.play tmp,sndUsd,0.50,0.50,1,0,1
    end if
    do
      gr.touch tmp,i,i
    until !tmp
  end if
  gr.bounded.touch tmp,(200-s/2)*scale_w,10*scale_h,(200+s/2)*scale_w,55*scale_h
  inkey$ k$
  if k$="key 108" & !Sauvegarde then tmp=1 %' Touche Start si pas de sauvegarde = Nouveau jeu
  if tmp then Goto NouveauJeu
  if Sauvegarde then
    gr.bounded.touch tmp,(440-s/2)*scale_w,10*scale_h,(440+s/2)*scale_w,55*scale_h
    inkey$ k$
    if k$="key 108" then tmp=1 %' Touche Start si sauvegarde = Continuer le jeu
    if tmp then Goto Continuer
  end if
until 0
!'---------------------------------------------------------------------------------------------------

OnError:
!' ---------------------------------------------------------------------------------------------------
!' Sous-routine d'attrapage et de traitement des erreurs
!' ---------------------------------------------------------------------------------------------------
if InOnError then %' avoid looping error
  d$=getError$()
  Popup d$,0,0,0
  Exit
end if
InOnError=1
if !mute then audio.stop
gr.cls
gr.render
Popup RPGLBL$[17],0,0,0
Print getError$()
Time y$,m$,d$,h$,mn$,s$
e$="../crashlog_"+y$+m$+d$+"_"+h$+mn$+s$+".txt"
console.save e$
HideSaveProgress=1
if pdv>0 then Gosub SauverPartie
if needSaveMAPs then Gosub SauverCalques
Exit
!'---------------------------------------------------------------------------------------------------

OnBackKey:
!'---------------------------------------------------------------------------------------------------
!' Gestion de la touche Back
!'---------------------------------------------------------------------------------------------------
BackKey=1
Back.resume
!'---------------------------------------------------------------------------------------------------

InitEcranDeTitre:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine d'initialisation de l'écran de titre
!'---------------------------------------------------------------------------------------------------
gr.cls
Call SetRGB(255,TBC$,1)
gr.rect tmp,0,0,di_w,di_h
gr.bitmap.draw e, exitBmp, 594, 0
gr.bitmap.draw tmpPtr, titBmp, 0, 58
gr.text.size 17
gr.text.align 2
gr.text.bold 1
x=160
s=200
Call SetRGB(255,TFC$,1)
gr.arc tmp,x-s/2-3,460,x-s/2+3,478,90,180,1
gr.arc tmp,x+s/2-3,460,x+s/2+3,478,-90,180,1
gr.rect tmp,x-s/2-1,460,x+s/2+1,478
Call SetRGB(255,TBC$,1)
gr.text.draw tmp,x,475,RPGLBL$[18]
x=480
s=200
Call SetRGB(255,TFC$,1)
gr.arc tmp,x-s/2-3,460,x-s/2+3,478,90,180,1
gr.arc tmp,x+s/2-3,460,x+s/2+3,478,-90,180,1
gr.rect tmp,x-s/2-1,460,x+s/2+1,478
Call SetRGB(255,TBC$,1)
gr.text.draw tmp,x,475,RPGLBL$[19]
gr.text.align 1
gr.text.bold 0
gr.render
Return
!'---------------------------------------------------------------------------------------------------

DessineBoutonsMenuPrincipal:
!'---------------------------------------------------------------------------------------------------
!' Dessine les boutons du menu principal de l'écran de titre
!'---------------------------------------------------------------------------------------------------
Gosub InitEcranDeTitre
e$=RPGLBL$[20]
x=200
Gosub MakeButton
btn1=btn
File.Exists Sauvegarde, "../jeu.sav"
e$=RPGLBL$[21]
x=440
KeepSameWidthAsLastButton=1
Gosub MakeButton
if !Sauvegarde then Gosub DeactivateButton
btn2=btn
!' Icône haut-parleur
Call SetRGB(255,TFC$,1)
gr.circle tmp,30,28,26
Call SetRGB(255,TBC$,0)
gr.set.stroke 2
gr.rect tmp,11,22,23,34
gr.line tmp,23,34,37,48
gr.line tmp,37,48,38,48
gr.line tmp,38,48,38,8
gr.line tmp,38,8,37,8
gr.line tmp,37,8,23,22
gr.set.stroke 4
gr.line zr,4,2,56,54
gr.line zl,4,54,56,2
gr.set.stroke 0
if !mute then
  gr.hide zr
  gr.hide zl
end if
gr.render
Return
!'---------------------------------------------------------------------------------------------------

InitEcranScores:
!'---------------------------------------------------------------------------------------------------
!' L'utilisateur presse la touche "Derniers scores"
!'---------------------------------------------------------------------------------------------------
InScores=1 %' (en // de InMenu=.. 0=init, 1=titre, 2=nouveau O/N, 3=continuer, 4=difficulté, 5=jeu)
gr.cls
Call SetRGB(255,TBC$,1)
gr.rect tmp,0,0,di_w,di_h
x=580
e$=RPGLBL$[22]
Gosub MakeButton
Call SetRGB(255,TFC$,1)
gr.text.size 16
gr.text.align 1
File.Exists s, "../scores.txt"
if !s Then
  gr.text.draw tmp,20,80,RPGLBL$[23]
else
  t1=0
  Text.open r, s, "../scores.txt"
  do
    Text.readln s, e$
    if e$<>"EOF" & e$<>"" then t1=t1+1
  until e$="EOF" | e$=""
  Text.close s
  t0=t1-9
  if t0<1 then t0=1
  Text.open r, s, "../scores.txt"
  for i=1 to t0-1
    Text.readln s, e$
  next i
  for i=t0 to t1
    Text.readln s, e$
    Decrypt MDP$, e$, s$
    y=40*(t1-i)+80
    e$=RainbowColor$((y-80)/88.372093)
    Call SetRGB(255,e$,1)
    gr.circle nul,30,y,10
    gr.text.bold 1
    gr.text.underline 1
    gr.text.draw tmp, 60, y, left$(s$, Is_In("-->",s$)-1)
    gr.text.underline 0
    gr.text.bold 0
    y=40*(t1-i)+100
    gr.text.skew -0.15
    gr.text.draw tmp, 80, y, right$(s$, len(s$)-Is_In("-->",s$)+1)
    gr.text.skew 0
  next i
  Text.close s
end if
gr.render
Return
!'---------------------------------------------------------------------------------------------------

InitEcranApropos:
!'---------------------------------------------------------------------------------------------------
!' L'utilisateur presse la touche "A propos du jeu"
!'---------------------------------------------------------------------------------------------------
InApropos=1 %' (en // de InMenu=.. 0=init, 1=titre, 2=nouveau O/N, 3=continuer, 4=difficulté, 5=jeu)
gr.cls
Call SetRGB(255,TBC$,1)
gr.rect tmp,0,0,di_w,di_h
x=580
e$=RPGLBL$[24]
Gosub MakeButton
gr.text.align 1
y=80
Call SetRGB(255,TFC$,0)
gr.rect tmp, 50, y, 130, y+80
gr.bitmap.draw tmp, gamBmp, 60, y+4
gr.rect tmp, 140, y, 580, y+80
gr.text.size 16
gr.text.bold 1
e$=RPGLBL$[25]
gr.text.width w, e$
Call SetRGB(255,TBC$,1)
gr.rect tmp, 145, y-2, 147+w, y+2
Call SetRGB(255,TFC$,1)
gr.text.draw tmp,146,y+4,e$
gr.text.bold 0
gr.text.size 11
gr.text.draw tmp,142,y+20,RPGLBL$[26]
gr.text.draw tmp,142,y+30,RPGLBL$[27]
gr.text.draw tmp,142,y+40,RPGLBL$[28]
gr.text.draw tmp,142,y+50,RPGLBL$[29]
gr.text.draw tmp,142,y+60,RPGLBL$[30]
gr.text.skew -0.25
gr.text.draw tmp,162,y+76,RPGLBL$[31]
gr.text.skew 0
y=180
Call SetRGB(255,TFC$,0)
gr.rect tmp, 50, y, 130, y+80
gr.bitmap.draw tmp, youBmp, 60, y+4
gr.rect tmp, 140, y, 580, y+80
gr.text.size 16
gr.text.bold 1
e$=RPGLBL$[32]
gr.text.width w, e$
Call SetRGB(255,TBC$,1)
gr.rect tmp, 145, y-2, 147+w, y+2
Call SetRGB(255,TFC$,1)
gr.text.draw tmp,146,y+4,e$
gr.text.bold 0
gr.text.size 11
gr.text.draw tmp,142,y+20,RPGLBL$[33]
gr.text.draw tmp,142,y+30,RPGLBL$[34]
gr.text.draw tmp,142,y+40,RPGLBL$[35]
gr.text.draw tmp,142,y+50,RPGLBL$[36]
gr.text.skew -0.25
gr.text.draw tmp,162,y+76,RPGLBL$[37]
gr.text.skew 0
y=280
Call SetRGB(255,TFC$,0)
gr.rect tmp, 50, y, 130, y+80
gr.bitmap.draw tmp, aRPGmB, 60, y+4
gr.rect tmp, 140, y, 580, y+80
gr.text.size 16
gr.text.bold 1
e$=RPGLBL$[38]
gr.text.width w, e$
Call SetRGB(255,TBC$,1)
gr.rect tmp, 145, y-2, 147+w, y+2
Call SetRGB(255,TFC$,1)
gr.text.draw tmp,146,y+4,e$
gr.text.bold 0
gr.text.size 11
gr.text.draw tmp,142,y+20,RPGLBL$[39]
gr.text.draw tmp,142,y+30,RPGLBL$[40]
gr.text.draw tmp,142,y+40,RPGLBL$[41]
gr.text.draw tmp,142,y+50,RPGLBL$[42]
gr.text.draw tmp,142,y+60,RPGLBL$[43]
gr.text.skew -0.25
gr.text.draw tmp,162,y+76,RPGLBL$[44]
gr.text.skew 0
y=380
Call SetRGB(255,TFC$,0)
gr.rect tmp, 50, y, 130, y+80
gr.bitmap.draw tmp, basBmp, 60, y+4
gr.rect tmp, 140, y, 580, y+80
gr.text.size 16
gr.text.bold 1
e$=RPGLBL$[45]
gr.text.width w, e$
Call SetRGB(255,TBC$,1)
gr.rect tmp, 145, y-2, 147+w, y+2
Call SetRGB(255,TFC$,1)
gr.text.draw tmp,146,y+4,e$
gr.text.bold 0
gr.text.size 11
gr.text.draw tmp,142,y+20,RPGLBL$[46]
gr.text.draw tmp,142,y+30,RPGLBL$[47]
gr.text.draw tmp,142,y+40,RPGLBL$[48]
gr.text.draw tmp,142,y+50,RPGLBL$[49]
gr.text.skew -0.25
gr.text.draw tmp,162,y+76,RPGLBL$[50]
gr.text.skew 0
gr.render
Return
!'---------------------------------------------------------------------------------------------------

InitEcranChoixDifficulte:
!'---------------------------------------------------------------------------------------------------
!' L'utilisateur choisit la difficulté
!'---------------------------------------------------------------------------------------------------
Gosub InitEcranDeTitre
for x=170 to 600 step 5 %' Arc-en-ciel
  e$=RainbowColor$((360-x/600*360)/60)
  Call SetRGB(255,e$,1)
  gr.rect nul,x-100,0,x-100+6,55
next i
gr.text.align 2
gr.text.size 23
Call Label(285,20,RPGLBL$[51])
gr.text.size 18
Call Label(100,50,RPGLBL$[52])
Call Label(280,50,RPGLBL$[53])
Call Label(470,50,RPGLBL$[54])
gr.render
Return
!'---------------------------------------------------------------------------------------------------

TestToucheEcranDeTitre:
!'---------------------------------------------------------------------------------------------------
!' Gestion appui touches communes de l'écran de titre principal ou ses sous-menus + touche Back
!' InMenu=.. 0=init, 1=titre, 2=nouveau O/N, 3=continuer, 4=difficulté, (5=jeu)
!'---------------------------------------------------------------------------------------------------
gr.touch tmp,w,w
inkey$ k$
if !tmp & k$="@" & !BackKey & InMenu<5 then Return %' accélère de 50% le chargement des ressources!
oldx=x
oldy=y
oldi=i
if !InScores & !InApropos then
  gr.bounded.touch tmp,57*scale_w,455*scale_h,263*scale_w,real_h %' scores
  inkey$ k$
  if k$="key 84" then tmp=1 %' Touche Search
  if tmp then
    x=160
    s=200
    Call SetRGB(255,TBC$,1)
    gr.rect btn,x-s/2-3,460,x+s/2+3,478
    Gosub PressButton
    Gosub InitEcranScores
    if InMenu=3 then
      e$=RPGLBL$[55]
      Gosub MakeProgressBar
      Gosub RefreshProgressBar
    end if
  end if
  inkey$ k$
  if k$="key 82" then tmp=1 %' Touche Menu
  gr.bounded.touch tmp,377*scale_w,455*scale_h,583*scale_w,real_h %' à propos
  if tmp then
    x=480
    s=200
    Call SetRGB(255,TBC$,1)
    gr.rect btn,x-s/2-3,460,x+s/2+3,478
    Gosub PressButton
    Gosub InitEcranApropos
    if InMenu=3 then
      e$=RPGLBL$[56]
      Gosub MakeProgressBar
      Gosub RefreshProgressBar
    end if
  end if
  gr.bounded.touch tmp,590*scale_w,0,real_w,58*scale_h %' exit
  if BackKey & InMenu<=1 then tmp=1 %' ou back key depuis écrans 0 et 1
  if tmp then
    gr.cls
    gr.render
    Popup RPGLBL$[57],0,0,0
    if pdv>0 then Gosub SauverPartie
    if needSaveMAPs then Gosub SauverCalques
    Pause 500
    Exit
  end if
  if BackKey & InMenu>=2 then Goto MenuPrincipal %' back key depuis sous menu -> retour menu principal

else if InScores | InApropos then
  if InMenu<>0 & InMenu<> 3 then %' ressources ou jeu mis en mémoire => écrans Scores ou A propos bloquants
    do
      if InApropos then
        gr.bounded.touch nul, 50, 80, 580, 160
        if nul then Browse "https://play.google.com/store/search?q=mougino&c=apps"
        gr.bounded.touch nul, 50, 180, 580, 260
        if nul then Browse "http://mougino.free.fr/"
        gr.bounded.touch nul, 50, 280, 580, 360
        if nul then Browse "http://android-rpg-maker.com/"
        gr.bounded.touch nul, 50, 380, 580, 460
        if nul then Browse "http://laughton.com/basic/"
      end if
      gr.bounded.touch tmp,520*scale_w,0,real_w,58*scale_h %' retour
      if BackKey then tmp=1 %' ou back key
    until tmp
  else %' ressources/jeu en cours de mise en mémoire : test bref appui touche puis redonner la main à la mise en mémoire
    if InApropos then
      gr.bounded.touch nul, 50, 80, 580, 160
      if nul then Browse "https://play.google.com/store/search?q=mougino&c=apps"
      gr.bounded.touch nul, 50, 180, 580, 260
      if nul then Browse "http://mougino.free.fr/"
      gr.bounded.touch nul, 50, 280, 580, 360
      if nul then Browse "http://android-rpg-maker.com/"
      gr.bounded.touch nul, 50, 380, 580, 460
      if nul then Browse "http://laughton.com/basic/"
    end if
    gr.bounded.touch tmp,520*scale_w,0,real_w,58*scale_h %' retour
    if BackKey then tmp=1 %' ou back key
  end if
  if tmp then %' appui bouton retour ou touche Back depuis Scores ou A propos
    Gosub PressButton
    BackKey=0
    InScores=0
    InApropos=0
    if InMenu=0 then %' 0=init
      Gosub InitEcranDeTitre
      Call SetRGB(255,TFC$,1)
      gr.text.size 26
      gr.text.align 2
      gr.text.draw nul,320,35,RPGLBL$[58]
      gr.render
    else if InMenu=1 then %' 1=titre
      Gosub DessineBoutonsMenuPrincipal
    else if InMenu=2 then %' 2=nouveau O/N
      Gosub InitEcranDeTitre
      Call SetRGB(255,TFC$,1)
      gr.text.size 22
      gr.text.align 1
      gr.text.bold 1
      gr.text.draw nul,60,20,RPGLBL$[59]
      gr.text.size 20
      gr.text.draw nul,40,40,RPGLBL$[60]
      gr.text.bold 0
      e$=RPGLBL$[61]
      x=380
      Gosub MakeButton
      btn1=btn
      e$=RPGLBL$[62]
      x=480
      Gosub MakeButton
      btn2=btn
      gr.render
    else if InMenu=3 then %' 3=continuer
      Gosub InitEcranDeTitre
      if pct<1 then
        e$=RPGLBL$[63]
        Gosub MakeProgressBar
        Gosub RefreshProgressBar
      end if
    else if InMenu=4 then %' 4=difficulté
      Gosub InitEcranChoixDifficulte
    end if
  end if
end if
x=oldx
y=oldy
i=oldi
Return
!'---------------------------------------------------------------------------------------------------

MakeProgressBar:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine de création d'une barre de progrès dans la barre du haut de l'écran de titre
!'    [Paramètre entrant] e$=label de la barre de progrès
!'---------------------------------------------------------------------------------------------------
Call SetRGB(255,TFC$,1)
gr.text.size 18
gr.text.align 2
gr.text.width t2, e$
gr.text.draw tmp,320,20,e$
gr.arc tmp,320-t2/2-3,30,320-t2/2+3,36,90,180,1
gr.arc tmp,320+t2/2-3,30,320+t2/2+3,36,-90,180,1
Call SetRGB(255,TFC$,0)
gr.rect t1,320-t2/2,30,320+t2/2,36
Call SetRGB(255,TFC$,1)
gr.rect t1,320-t2/2,30,320-t2/2,36
gr.render
Return
!'---------------------------------------------------------------------------------------------------

RefreshProgressBar:
!'---------------------------------------------------------------------------------------------------
!' Rafraichit la barre de progrès créée précédemment pour refléter l'avancement
!'    [Paramètre entrant] pct=pourcentage d'avancement de la barre de progrès (0<pct<1)
!'---------------------------------------------------------------------------------------------------
gr.modify t1,"right",320-t2/2+t2*pct
gr.render
Return
!'---------------------------------------------------------------------------------------------------

MakeButton:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine de création d'un bouton dans la barre du haut de l'écran de titre
!'    [Paramètres entrants] e$=label du bouton ; x=abscisse de centrage du bouton
!'    [Paramètre sortant] btn=référence du bouton (pour appeler DeactivateButton ou PressButton)
!'    Ne contient pas de gr.render : rafraichissez manuellement après avoir créé 1 ou plsrs boutons
!'---------------------------------------------------------------------------------------------------
gr.text.size 26
gr.text.align 2
gr.text.bold 1
if !KeepSameWidthAsLastButton then
  gr.text.width lBtn, e$
  lBtn=lBtn+20
else
  KeepSameWidthAsLastButton=0
end if
Call SetRGB(255,TFC$,1)
gr.arc tmp,x-lBtn/2-6,6,x-lBtn/2+6,51,90,180,1
gr.arc tmp,x+lBtn/2-6,6,x+lBtn/2+6,51,-90,180,1
gr.rect tmp,x-lBtn/2-1,6,x+lBtn/2+1,51
Call SetRGB(255,TBC$,1)
gr.text.draw tmp,x,36,e$
gr.text.bold 0
gr.rect btn,x-lBtn/2-6,6,x+lBtn/2+6,51
gr.hide btn
Return
!'---------------------------------------------------------------------------------------------------

DeactivateButton:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine de désactivation d'un bouton de la barre du haut de l'écran de titre
!'    [Paramètre entrant] btn=numéro du bouton précédemment créé (param sortant de MakeButton)
!'---------------------------------------------------------------------------------------------------
gr.modify btn,"alpha",128
gr.show btn
gr.render
Return
!'---------------------------------------------------------------------------------------------------

PressButton:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine d'effet lors de l'appui sur un bouton de la barre du haut de l'écran de titre
!'    [Paramètre entrant] btn=numéro du bouton précédemment créé (param sortant de MakeButton)
!'---------------------------------------------------------------------------------------------------
if !mute then vibrate VibrCrt[], -1
l=GetAudioLength("Select.mp3")
if !mute then Soundpool.play tmp,sndSel,0.99,0.99,1,0,1
tmr=Clock()
while Clock()-tmr<l
  if mute then vibrate VibrCrt[], -1
  gr.show btn
  gr.render
  Pause 50
  gr.hide btn
  gr.render
  Pause 50
repeat
Return
!'---------------------------------------------------------------------------------------------------

NouveauJeu:
!'---------------------------------------------------------------------------------------------------
!' L'utilisateur presse la touche "Nouveau jeu"
!'---------------------------------------------------------------------------------------------------
InMenu=2 %' 0=init, 1=titre, 2=nouveau O/N, 3=continuer, 4=difficulté, 5=jeu
btn=btn1
Gosub PressButton
if !Sauvegarde then Goto ChoisirDifficulte
!' Sinon : une sauvegarde existe déjà --> l'écraser ?
Gosub InitEcranDeTitre
Call SetRGB(255,TFC$,1)
gr.text.size 22
gr.text.align 1
gr.text.bold 1
gr.text.draw o,60,20,RPGLBL$[64]
gr.text.size 20
gr.text.draw o,40,40,RPGLBL$[65]
gr.text.bold 0
e$=RPGLBL$[66]
x=380
Gosub MakeButton
btn1=btn
e$=RPGLBL$[67]
x=480
Gosub MakeButton
btn2=btn
gr.render

do
  Gosub TestToucheEcranDeTitre
  gr.bounded.touch zu,(380-s/2)*scale_w,0,(380+s/2)*scale_w,55*scale_h
  if zu then %' "OUI" --> Nouveau jeu = réinitialisation totale (la sauvegarde sera écrasée)
    btn=btn1
    Gosub PressButton
    JeuDejaPret=0
    Array.length c, objet[]
    for i=1 to c
      objet[i]=0
    next i
    Array.length c, equipement[]
    for i=1 to c
      equipement[i]=0
    next i
    Array.length c, perso[]
    for i=1 to c
      perso[i]=0
    next i
    !' supprime sauvegarde
    file.delete o, "../jeu.sav"
    !' réinitialise calque de décor
    file.delete o, "../CAD.BGN.SAV"
    byte.open r,map,"CAD.BGN.MAP"
    byte.copy map,"../CAD.BGN.SAV"
    for i=1 to mapHmax
      bgnMap$[i]=bgnIni$[i]
    next i
    !' réinitialise calque de solidité
    file.delete o, "../CAD.SOL.SAV"
    byte.open r,map,"CAD.SOL.MAP"
    byte.copy map,"../CAD.SOL.SAV"
    for i=1 to mapHmax
      solMap$[i]=solIni$[i]
    next i
    !' réinitialise calque des objets
    file.delete o, "../CAD.OBJ.SAV"
    byte.open r,map,"CAD.OBJ.MAP"
    byte.copy map,"../CAD.OBJ.SAV"
    for i=1 to mapHmax
      objMap$[i]=objIni$[i]
    next i
    !' réinitialise calque des persos
    file.delete o, "../CAD.CHR.SAV"
    byte.open r,map,"CAD.CHR.MAP"
    byte.copy map,"../CAD.CHR.SAV"
    for i=1 to mapHmax
      chrMap$[i]=chrIni$[i]
    next i
    Goto ChoisirDifficulte
  end if
  gr.bounded.touch zu,(480-s/2)*scale_w,0,(480+s/2)*scale_w,55*scale_h
  if zu then %' "NON" --> on revient au menu précédent
    btn=btn2
    Gosub PressButton
    Goto MenuPrincipal
  end if
until 0
!'---------------------------------------------------------------------------------------------------

Continuer:
!'---------------------------------------------------------------------------------------------------
!' L'utilisateur presse la touche "Continuer"
!'---------------------------------------------------------------------------------------------------
InMenu=3 %' 0=init, 1=titre, 2=nouveau O/N, 3=continuer, 4=difficulté, 5=jeu
btn=btn2
Gosub PressButton
Gosub ChargerSauvegarde
if heroform=5 then DerDir=_downBat else DerDir=_down %' Dernière dir° du héros=au repos vers le bas
Goto ChargerCarte
!'---------------------------------------------------------------------------------------------------

ChoisirDifficulte:
!'---------------------------------------------------------------------------------------------------
!' Choix de la difficulté
!'---------------------------------------------------------------------------------------------------
InMenu=4 %' 0=init, 1=titre, 2=nouveau O/N, 3=continuer, 4=difficulté, 5=jeu
Gosub InitEcranChoixDifficulte
do
  Gosub TestToucheEcranDeTitre
  gr.bounded.touch zu,70*scale_w,0,506*scale_w,55*scale_h
  if zu Then
    gr.touch zu, x, y
    difficulte=Floor((x/scale_w - 70) / 4.36)
    Popup RPGLBL$[68]+Stg$(difficulte)+"%",0,-160,0
    gr.set.stroke 3
    Call SetRGB(255,TFC$,0)
    gr.rect btn,(x-3)/scale_w,0,(x+3)/scale_w,55
    gr.set.stroke 0
    Gosub PressButton
    gr.hide btn
    gr.render
    Goto InitNouvellePartie
  end if
until 0

InitNouvellePartie:
!'---------------------------------------------------------------------------------------------------
!' Initialisation des variables et des calques pour une nouvelle partie
!'---------------------------------------------------------------------------------------------------
Gosub InitEcranDeTitre
Call SetRGB(255,TFC$,1)
gr.text.size 26
gr.text.align 2
e$=RPGLBL$[69]
gr.text.draw a3,320,35,e$+"0%"
gr.render
herox=UNDEF %' pour éviter des rafraichissements d'écran intempestifs depuis les SetX, DelX, etc.
heroy=UNDEF
!'---------------------------------------------------------------------------------------------------
!' Coordonnées initiales des persos
!'---------------------------------------------------------------------------------------------------
Undim chrX[]
array.load chrX[],3,0,0,0,0,0,30,208,108,101,138,139,140,0,0,0,99,146,29,28,0,0,0,0,0,0,0,0,102,103~
0,0,0,0,0,0,0,0,102,103,0,0,0,0,0,0,200,201,202,202,0,0,0,0,0,0,200,201,202,194,0,0,0,0,0,0,0,0,144~
0,0,0,88,0,0,0,0,0,144,0,0,0,0,0,0,0,0,153,0,0
Undim chrY[]
array.load chrY[],235,0,0,0,0,0,6,83,181,78,57,57,57,0,0,0,71,35,49,72,0,0,0,0,0,0,0,0,1,1,0,0,0~
0,0,0,0,0,2,2,0,0,0,0,0,0,248,248,248,245,0,0,0,0,0,0,249,249,249,30,0,0,0,0,0,0,0,0,42,0,0,0,104~
0,0,0,0,0,43,0,0,0,0,0,0,0,0,228,0,0
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
!' [Script 0][NOUVELLE PARTIE] Placer ici toutes les modifications apportées aux calques initiaux
!'      lors d'une nouvelle partie : changement de décors, solidités, objets, persos
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
!' passage du dauphin pas encore débloqué :
for i=177 to 179 %' 3
  for destx=103 to 107 %' 5 => 3*5=15
    !' passage haut
    desty=i
    b=56
    Gosub SetB
    s=15
    Gosub SetS
    !' passage bas
    desty=i+10
    b=56
    Gosub SetB
    s=15
    Gosub SetS
  next destx
  gr.modify a3,"text",e$+Stg$((i-177)*(destx-103)/9.95)+"%"
  gr.render
next desty
!'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!' île du désert inaccessible sans carte :
c=8 %' sorcière du désert
Gosub DelC
for desty=61 to 88 %' 28
  for destx=181 to 215 %' 35 => 28*35=980
    b=56
    Gosub SetB
    s=15
    Gosub SetS
    Gosub DelO
  next destx
  gr.modify a3,"text",e$+Stg$((30+(desty-61)*(destx-181))/9.95)+"%"
  gr.render
next desty
gr.modify a3,"text",e$+"100%"
gr.render
!' [Fin du Script 0] FinScript0
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
FinScript0:
!'---------------------------------------------------------------------------------------------------
!' Remise à zéro des variables + calques lors d'une nouvelle partie
!'---------------------------------------------------------------------------------------------------
herox=8
heroy=5
heroform=1
pdv=100 %' santé (points de vie)
usd=0   %' argent
ata=0   %' niveau d'attaque
def=1   %' niveau de défense
Array.length o, objet[]
for i=1 to o
  objet[i]=0
next i
neq=0   %' nb d'objets dans l'équipement
Array.length e, equipement[]
neq=0
for i=1 to e
  equipement[i]=0
next i
Array.length c, perso[]
for i=1 to c
  perso[i]=0
next i
batx=40 %' coordonnées du bateau
baty=125
dkns=0 %' darkness
victories=0 %' nb de combats gagnés
!'---------------------------------------------------------------------------------------------------

SceneIntro:
!'---------------------------------------------------------------------------------------------------
!' Scène d'intro lors d'une nouvelle partie
!'---------------------------------------------------------------------------------------------------
di_w=Floor(nthor/2)*tilew
di_h=Floor(ntver/2)*tileh
scale_w=real_w/di_w
scale_h=real_h/di_h
gr.scale scale_w, scale_h
gr.cls
gr.color 255,15,203,255,1
!' il y a le ciel :
gr.rect nul,0,0,di_w,di_h
!' le soleil :
gr.color 255,0,0,0,1
gr.bitmap.create bgnBmp, Floor(nthor/2+1)*tilew, 3*tileh
gr.bitmap.drawinto.start bgnBmp
for y=0 to 2*tileh step tileh
  for x=0 to Floor(nthor/2)*tilew step tilew
    gr.bitmap.draw nul, bgnTile[56], x, y
  next x
next y
gr.bitmap.drawinto.end
gr.bitmap.draw bgnPtr, bgnBmp, 0, Floor(ntver/2-2)*tileh
!' et la mer :
gr.color 20,255,216,0,1
for i=Floor(ntver/2)*tileh to 10 step -5
  gr.circle nul, Floor(nthor/2)*tilew, 0, i
next i
!' mais aussi des nuages :
gr.color 100,255,255,255,1
x=Floor(nthor/2*tilew*Rnd())
y=Floor(2*tilew*Rnd())
gr.oval a1, x, y, x+Ceil(3*Rnd()+3)*tilew, y+Ceil(3*Rnd())*tilew
x=Floor(nthor/2*tilew*Rnd())
y=Floor(2*tilew*Rnd())
gr.oval a2, x, y, x+Ceil(3*Rnd()+3)*tilew, y+Ceil(3*Rnd())*tilew
x=Floor(nthor/2*tilew*Rnd())
y=Floor(2*tilew*Rnd())
gr.oval b1, x, y, x+Ceil(3*Rnd()+3)*tilew, y+Ceil(3*Rnd())*tilew
x=Floor(nthor/2*tilew*Rnd())
y=Floor(2*tilew*Rnd())
gr.oval b2, x, y, x+Ceil(3*Rnd()+3)*tilew, y+Ceil(3*Rnd())*tilew
!' et enfin un radeau et deux amoureux :
gr.color 255,0,0,0,1
gr.bitmap.create tmpBmp, 4*tilew, 3*tileh
gr.bitmap.drawinto.start tmpBmp
for y=0 to 2*tileh step tileh
  for x=0 to 3*tilew step tilew
    gr.bitmap.draw nul, bgnTile[13], x, y
  next x
next y
gr.bitmap.draw nul, chrTile[3], tilew, 0
gr.bitmap.draw nul, heroTile[1,16], 2*tilew, 0
gr.bitmap.drawinto.end
gr.bitmap.create equBmp, 3*tilew, 2*tileh
gr.bitmap.draw tmpPtr, tmpBmp, -4*tilew, (Floor(ntver/2)-2)*tileh
gr.hide tmpPtr
gr.text.size 4
gr.text.align 3
Call Label(di_w,di_h-2,RPGLBL$[70])
gr.text.align 1
!' animation paisible :
zd=0
for z=1 to tilew %' la mer défile tout simplement
  Gosub IntroAnimeDecor
  gr.touch tmp,nul,nul
  inkey$ k$
  if tmp | k$<>"@" then Goto FinIntro
next z
gr.show tmpPtr
for z=-4*tilew to di_w/2-2*tilew step 2 %' le radeau se déplace de la gauche au centre de l'écran
  gr.modify tmpPtr, "x", z
  i=Mod(i+1,2*48)
  gr.modify tmpPtr, "y", (Floor(ntver/2)-2+0.3*Sin(i*Pi/48))*tileh
  Gosub IntroAnimeDecor
  gr.touch tmp,nul,nul
  inkey$ k$
  if tmp | k$<>"@" then Goto FinIntro
next z
for z=di_w/2-2*tilew to di_w/2-tilew step 2 %' le radeau est stable au centre de l'écran
  i=Mod(i+1,2*48)
  gr.modify tmpPtr, "y", (Floor(ntver/2)-2+0.3*Sin(i*Pi/48))*tileh
  Gosub IntroAnimeDecor
  gr.touch tmp,nul,nul
  inkey$ k$
  if tmp | k$<>"@" then Goto FinIntro
next z
!' et soudain c'est le drame :
gr.bitmap.draw ennPtr, DragonRepos, -3*tilew, -tileh
gr.bitmap.draw heroPtr, heroTile[1,2], 5*tilew, 4*tileh
gr.hide heroPtr
gr.rect dknsPtr, 0, 0, di_w, di_h
gr.modify dknsPtr, "alpha", 0
for z=-3*tilew to di_w/2-2.5*tilew step 2
  gr.modify ennPtr, "x", z
  gr.modify ennPtr, "y", 5*tileh*z/(di_w/2+3.5*tilew)-tileh*(di_w/2-11.5*tilew)/(di_w/2+3.5*tilew)
  gr.modify dknsPtr, "alpha", 120*(z+3*tilew)/(di_w/2+0.5*tilew) %' le ciel s'obscurcit
  i=Mod(i+1,2*48)
  gr.modify tmpPtr, "y", (Floor(ntver/2)-2+0.3*Sin(i*Pi/48))*tileh %' le radeau continue de tanguer
  Gosub IntroAnimeDecor
  gr.touch tmp,nul,nul
  inkey$ k$
  if tmp | k$<>"@" then Goto FinIntro
next z
gr.modify ennPtr, "bitmap", DragonFeu
gr.color 255,0,0,0,1
gr.bitmap.drawinto.start tmpBmp
gr.bitmap.draw nul, bgnTile[13], tilew, 0
gr.bitmap.draw nul, bgnTile[13], 2*tilew, 0
gr.bitmap.draw nul, chrTile[1], tilew, 0
gr.bitmap.draw nul, heroTile[1,14], 2*tilew, 0
gr.bitmap.drawinto.end
if !mute then
  audio.load snd, "blesse.mp3"
  audio.stop
  audio.play snd
end if
for z=0 to tilew step 2 %' le radeau tangue tandis que le héros est blessé et sa fiancée pétrifiée
  i=Mod(i+1,2*48)
  gr.modify tmpPtr, "y", (Floor(ntver/2)-2+0.3*Sin(i*Pi/48))*tileh
  Gosub IntroAnimeDecor
  gr.touch tmp,nul,nul
  inkey$ k$
  if tmp | k$<>"@" then Goto FinIntro
next z
gr.modify ennPtr, "bitmap", DragonRepos
for z=2*tileh to 3*tileh step 2 %' le dragon descend récupérer sa proie
  gr.modify ennPtr, "x", di_w/2-2.5*tilew-(z-tileh)/4
  gr.modify ennPtr, "y", z
  i=Mod(i+1,2*48)
  gr.modify tmpPtr, "y", (Floor(ntver/2)-2+0.3*Sin(i*Pi/48))*tileh
  Gosub IntroAnimeDecor
  gr.touch tmp,nul,nul
  inkey$ k$
  if tmp | k$<>"@" then Goto FinIntro
next z
gr.bitmap.drawinto.start equBmp %' il repart avec la statue de la fiancée
gr.bitmap.draw nul, DragonRepos, 0, 0
gr.bitmap.draw nul, chrTile[1], 2*tilew, tileh
gr.bitmap.drawinto.end
gr.modify ennPtr, "bitmap", equBmp
gr.bitmap.drawinto.start tmpBmp %' tandis que le héros regarde la scène, médusé
gr.bitmap.draw nul, bgnTile[13], tilew, 0
gr.bitmap.draw nul, bgnTile[13], 2*tilew, 0
gr.bitmap.draw nul, heroTile[1,1], 2*tilew, 0
gr.bitmap.drawinto.end
for z=3*tileh to tileh step -2
  gr.modify ennPtr, "x", di_w/2-3*tilew-(z-3*tileh)
  gr.modify ennPtr, "y", z
  i=Mod(i+1,2*48)
  gr.modify tmpPtr, "y", (Floor(ntver/2)-2+0.3*Sin(i*Pi/48))*tileh
  Gosub IntroAnimeDecor
  gr.touch tmp,nul,nul
  inkey$ k$
  if tmp | k$<>"@" then Goto FinIntro
next z
gr.bitmap.drawinto.start tmpBmp %' le héros saute sur le dragon !
gr.bitmap.draw nul, bgnTile[13], 2*tilew, 0
gr.bitmap.drawinto.end
gr.show heroPtr
if !mute then
  audio.stop
  audio.release snd
  audio.load snd, "bouncing.wav"
  audio.play snd
end if
for z=tileh to 0 step -2
  gr.modify ennPtr, "x", di_w/2-3*tilew-(z-3*tileh)
  gr.modify ennPtr, "y", z
  gr.modify heroPtr, "y", 4*tileh-3*(tileh-z)
  i=Mod(i+1,2*48)
  gr.modify tmpPtr, "y", (Floor(ntver/2)-2+0.3*Sin(i*Pi/48))*tileh
  Gosub IntroAnimeDecor
  gr.touch tmp,nul,nul
  inkey$ k$
  if tmp | k$<>"@" then Goto FinIntro
next z
gr.bitmap.drawinto.start equBmp %' le héros est accroché au dragon
gr.bitmap.draw nul, DragonRepos, 0, 0
gr.bitmap.draw nul, heroTile[1,2], 0, tileh
gr.bitmap.drawinto.end
gr.hide heroPtr
for z=0 to -2*tileh step -2 %' le dragon continue de prendre de l'altitude
  gr.modify ennPtr, "x", di_w/2-3*tilew-(z-3*tileh)
  gr.modify ennPtr, "y", z
  i=Mod(i+1,2*48)
  gr.modify tmpPtr, "y", (Floor(ntver/2)-2+0.3*Sin(i*Pi/48))*tileh
  Gosub IntroAnimeDecor
  gr.touch tmp,nul,nul
  inkey$ k$
  if tmp | k$<>"@" then Goto FinIntro
next z
for z=di_w/2-2*tilew to di_w step 2 %' zoom out de la caméra : on laisse filer le radeau
  gr.modify tmpPtr, "x", z
  i=Mod(i+1,2*48)
  gr.modify tmpPtr, "y", (Floor(ntver/2)-2+0.3*Sin(i*Pi/48))*tileh
  Gosub IntroAnimeDecor
  gr.touch tmp,nul,nul
  inkey$ k$
  if tmp | k$<>"@" then Goto FinIntro
next z
gr.bitmap.delete equBmp
gr.bitmap.create equBmp, 4*tilew, 4*tileh %' ...on atteint un rivage
gr.bitmap.drawinto.start equBmp
for y=0 to 3*tileh step tileh
  gr.bitmap.draw nul, bgnTile[1], 0, y
  gr.bitmap.draw nul, bgnTile[1], tilew, y
  gr.bitmap.draw nul, bgnTile[29], 2*tilew, y
  gr.bitmap.draw nul, bgnTile[12], 3*tilew, y
next y
gr.bitmap.drawinto.end
gr.modify ennPtr, "bitmap", equBmp
gr.modify ennPtr, "y", (Floor(ntver/2)-2.5)*tileh
for z=-4*tilew to 0 step 2
  gr.modify ennPtr, "x", z
  Gosub IntroAnimeDecor
  gr.touch tmp,nul,nul
  inkey$ k$
  if tmp | k$<>"@" then Goto FinIntro
next z
gr.modify heroPtr, "bitmap", heroTile[1,10]
gr.modify heroPtr, "x", 2*tilew
gr.show heroPtr
if !mute then %' le héros tombe !
  audio.stop
  audio.release snd
  audio.load snd, "tombe.mp3"
  audio.play snd
end if
for z=-tileh to di_h step 4
  gr.modify heroPtr, "y", z
  gr.modify heroPtr, "x", 2*tilew*(1-(z+tileh)/di_h)
  Gosub IntroAnimeDecor
  gr.touch tmp,nul,nul
  inkey$ k$
  if tmp | k$<>"@" then Goto FinIntro
next z
FinIntro:
Gosub FadeOut
if !mute then
  audio.stop
  if snd then audio.release snd
end if
gr.bitmap.delete tmpBmp
gr.bitmap.delete equBmp
equBmp=0
di_w=640
di_h=480
scale_w=real_w/di_w
scale_h=real_h/di_h
gr.scale scale_w, scale_h
DerDir=9 %' Spécial animation nouveau jeu
Goto ChargerCarte
IntroAnimeDecor:
!' anime nuage 1
gr.get.value a1,"left",zl
gr.get.value a1,"right",zr
zr=zr-zl
zl=zl+1
if zl>di_w then zl=-di_w
zr=zr+zl
gr.modify a1, "left", zl
gr.modify a1, "right", zr
!' anime nuage 2
gr.get.value a2,"left",zl
gr.get.value a2,"right",zr
zr=zr-zl
zl=zl+2
if zl>di_w then zl=-di_w
zr=zr+zl
gr.modify a2, "left", zl
gr.modify a2, "right", zr
!' anime nuage 3
gr.get.value b1,"left",zl
gr.get.value b1,"right",zr
zr=zr-zl
zl=zl+3
if zl>di_w then zl=-di_w
zr=zr+zl
gr.modify b1, "left", zl
gr.modify b1, "right", zr
!' anime nuage 4
gr.get.value b2,"left",zl
gr.get.value b2,"right",zr
zr=zr-zl
zl=zl+4
if zl>di_w then zl=-di_w
zr=zr+zl
gr.modify b2, "left", zl
gr.modify b2, "right", zr
!' la mer défile
zd = Mod(zd+tilew-2, tilew)
gr.modify bgnPtr, "x", zd-tilew
if Clock()-tmr>2000 then
  if !mute then Soundpool.play tmp,sndWav,0.99,0.99,1,0,1 %' bruit de ressac
  tmr=Clock()+3000*Rnd()
end if
gr.render
Pause 2
Return
!'---------------------------------------------------------------------------------------------------

ChargerCarte:
!'---------------------------------------------------------------------------------------------------
!' Chargement des calques à l'écran
!'---------------------------------------------------------------------------------------------------
LstEqu=0 %' Dernier équipement utilisé : rien
if JeuDejaPret then Goto DemarrerJeu
Gosub InitEcranDeTitre
e$=RPGLBL$[71]
Gosub MakeProgressBar
!' Créer bitmap de l'inventaire
if !equBmp then
  gr.bitmap.create equBmp, nthor*tilew, ntver*tileh
  gr.bitmap.drawinto.start equBmp
  i=0
  for y=0 to ntver-3 step 2
    for x=0 to nthor-3 step 2
      if y<>0 | x<>nthor-3 then
        gr.color 150,0,0,0,1
        gr.rect tmpPtr,(x+0.1)*tilew,(y+0.1)*tileh,(x+1.9)*tilew,(y+1.9)*tileh
        gr.color 150,255,255,255,0
        gr.rect tmpPtr,(x+0.1)*tilew,(y+0.1)*tileh,(x+1.9)*tilew,(y+1.9)*tileh
        i=i+1
        if i<=neq then
          pct=i / (neq+(nthor+2)*(ntver+2))
          Gosub RefreshProgressBar
          if equipement[i]=7 & perso[81]=7 then  %' crâne purifié du terrible pirate Roberts
            equipement[i]=6
            perso[81]=8
          end if
          gr.color 255,0,0,0,1
          gr.bitmap.scale nul,objTile[equipement[i]],1.8*tilew,1.8*tileh, 0
          gr.bitmap.draw tmpPtr,nul,(x+0.1)*tilew,(y+0.1)*tileh
          gr.bitmap.delete nul
        end if
      end if
    next x
  next y
  gr.bitmap.drawinto.end
end if
!' Générer le fond de la map
if bgnBmp then gr.bitmap.delete bgnBmp
gr.bitmap.create bgnBmp, (nthor+2)*tilew, (ntver+2)*tileh
e$=""
for y=heroy+dy-Floor(ntver/2)-1 to heroy+dy+Floor(ntver/2)+1
  for x=herox+dx-Floor(nthor/2)-1 to herox+dx+Floor(nthor/2)+1
    Gosub TestToucheEcranDeTitre
    Gosub AfficherDecor
    if Mod(x-herox-dx+Floor(nthor/2)+1,4)=0 then
      pct=(neq+(nthor+2)*(y-heroy-dy+Floor(ntver/2)+1)+x-herox-dx+Floor(nthor/2)+1) / (neq+(nthor+2)*(ntver+2))
      Gosub RefreshProgressBar
    end if
  next x
next y
pct=1
Gosub RefreshProgressBar
Call SetRGB(255,TBC$,1)
gr.rect rectPtr, 0, 0, 510, 58
for i=0 to 255 step 15
  gr.modify rectPtr, "alpha", i
  gr.render
  Pause 20
next i
!'---------------------------------------------------------------------------------------------------

DemarrerJeu:
!'---------------------------------------------------------------------------------------------------
!' Démarrage du moteur de jeu
!'---------------------------------------------------------------------------------------------------
InMenu=5 %' 0=init, 1=titre, 2=nouveau O/N, 3=continuer, 4=difficulté, 5=jeu
Gosub TestToucheEcranDeTitre %' si sous-menu scores ou à propos déjà en fond depuis la phase précédente
JeuDejaPret=1
Gosub FadeOut

di_w=nthor * tilew
di_h=ntver * tileh
scale_w=real_w/di_w
scale_h=real_h/di_h
gr.scale scale_w, scale_h
Gosub InitEcranDeJeu
Gosub MusiqueChange %' démarrer la musique
if DerDir=9 then gr.modify heroPtr, "y", -tileh
Gosub FadeIn

if DerDir=9 then %' animation nouveau jeu
  if !mute then Soundpool.play tmp,sndFal,0.99,0.99,1,0,1
  for i=-tileh to Floor(ntver/2-1/4)*tileh step tileh/4
    gr.modify heroPtr, "y", i
    gr.render
    Pause 10
  next i
  if !mute then Soundpool.play tmp,sndBnc,0.99,0.99,1,0,1
  for y=1 to 2.5 step 1/tilew
    gr.modify heroPtr, "y", tileh*(Floor(ntver/2) - Abs(Sin(Pi*2*(y-1)))/y)
    gr.render
    Pause 10
  next y
  gr.modify heroPtr, "y", Floor(ntver/2)*tileh
  gr.render
end if

BouclePrincipale:
do

  inkey$ k$
  gr.bounded.touch zu,real_w*0.15-real_h/10.4,real_h*0.50,real_w*0.15+real_h/10.4,real_h*0.70
  if k$="up" then zu=1
  gr.bounded.touch zd,real_w*0.15-real_h/10.4,real_h*0.80,real_w*0.15+real_h/10.4,real_h
  if k$="down" then zd=1
  gr.bounded.touch zr,real_w*0.24-real_h/10.4,real_h*0.66,real_w*0.24+real_h/10.4,real_h*0.86
  if k$="right" then zr=1
  gr.bounded.touch zl,real_w*0.06-real_h/10.4,real_h*0.66,real_w*0.06+real_h/10.4,real_h*0.86
  if k$="left" then zl=1
  if zu | zd | zr | zl then
    if zu+zd+zr+zl=1 then Goto HerosBouge %' pour éviter les déplacements en diagonale
  end if

  Gosub TestJeuEnArrierePlan

  gr.bounded.touch tmp,real_w-padsize*tilew*2*scale_w,padsize*tileh*0.5*scale_h,real_w-padsize*tilew*0.5*scale_w,padsize*tileh*2*scale_h
  inkey$ k$
  if k$="key 84" then tmp=1 %' Touche Search
  if tmp then Goto Inventaire

  gr.bounded.touch tmp,(di_w-32)*scale_w,(di_h-32)*scale_h,real_w,real_h
  inkey$ k$
  if k$="key 82" then tmp=1 %' Touche Menu
  if tmp then Goto Settings

  gr.bounded.touch tmp,Floor(nthor/2)*tilew*scale_w,Floor(ntver/2)*tileh*scale_h,(Floor(nthor/2)+1)*tilew*scale_w,(Floor(ntver/2)+1)*tileh*scale_h
  inkey$ k$
  if k$="go" then tmp=1 %' Touche playstation (X)
  if tmp then Goto ActionHeros

  if LstEqu then
    gr.bounded.touch tmp,real_w-padsize*tilew*4*scale_w,padsize*tileh*0.5*scale_h,real_w-padsize*tilew*2.5*scale_w,padsize*tileh*2*scale_h
    inkey$ k$
    if k$="key 100" then tmp=1 %' Touche playstation (/\)
    if tmp then
      o=LstEqu
      for i=1 to 3
        vibrate VibrCrt[], -1
      next i
      do
        gr.touch tmp, x, y
      until !tmp
      Goto UtiliseObjet
    end if
  end if

  gr.touch tmp, x, y
  if tmp & objet[25]=1 & heroform=1 then %' Runes élémentales !
    x=x/scale_w
    y=y/scale_h

    !' Conque du vent
    if objet[8]=1 & x<padsize*tilew & y<padsize*tileh then
      e$="schema1.png"
      Gosub FollowTheTrail
      if !red then %' Hors des sentiers battus
        if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
        e$="00FF00" %' voile vert
        Gosub Flash
        Gosub InitEcranDeJeu
        if light then Gosub FiatLux else gr.render
        Goto BouclePrincipale
      else %' L'utilisateur a suivi le chemin de la formule magique comme il convient
        if !mute then
          l=GetAudioLength("conque_vent.mp3")
          tmr=Clock()
          Soundpool.play tmp,sndVen,0.99,0.99,1,0,1
          audio.pause
        else
          for i=1 to 3
            vibrate VibrCrt[], -1
            Pause 150
          next i
        end if
        e$="00FF00" %' voile vert
        Gosub Flash
        Gosub InitEcranDeJeu
        gr.bitmap.draw tmpPtr, objTile[21], Floor(nthor/2)*tilew, Floor(ntver/2)*tileh
        for t=0 to 6*Pi step Pi/20 %' tornade en spirale souffle le sable et la fumée
          w=t*0.5/Pi*cos(t)
          z=t*0.5/Pi*sin(t)
          gr.modify tmpPtr, "x", (Floor(nthor/2)+w)*tilew
          gr.modify tmpPtr, "y", (Floor(ntver/2)+z)*tileh
          if t>=5*Pi then gr.modify tmpPtr, "alpha", 255*(1-(t-5*Pi)/Pi)
          if light then Gosub FiatLux else gr.render
          u=x
          v=y
          x=herox+dx+Floor(w+0.5)
          y=heroy+dy+Floor(z+0.5)
          if u<>x | v<>y then
            Gosub GetS
            if s<>1 then
              Gosub GetB
              if b=37 | b=38 | b=47 | b=48 then %' souffle dunes de sable
                b=41
                Gosub SetB
                s=20
                Gosub SetS
              end if
              Gosub GetO
              if o & o<4 then %' souffle brumes magiques
                Gosub DelO
              end if
            end if
          end if
          Pause 5
        next t
        if Clock()-tmr<l then Pause l-Clock()+tmr
        if !mute & music$<>"" then audio.play musPtr
        Gosub InitEcranDeJeu
        if light then Gosub FiatLux else gr.render
        Goto BouclePrincipale
      end if %' fin rune magique ok/ko

    !' Croix de l'eau
    elseif objet[4]=1 & x<padsize*tilew & y>(di_h-padsize*tileh)/2 & y<(di_h+padsize*tileh)/2 then
      e$="schema2.png"
      Gosub FollowTheTrail
      if !red | !green | !yellow then %' Hors des sentiers battus
        if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
        e$="00FFFF" %' voile bleu ciel
        Gosub Flash
        Gosub InitEcranDeJeu
        if light then Gosub FiatLux else gr.render
        Goto BouclePrincipale
      else %' L'utilisateur a suivi le chemin de la formule magique comme il convient
        if !mute then
          l=GetAudioLength("croix_eau.mp3")
          tmr=Clock()
          Soundpool.play tmp,sndEau,0.99,0.99,1,0,1
          audio.pause
        else
          for i=1 to 3
            vibrate VibrCrt[], -1
            Pause 150
          next i
        end if
        e$="00FFFF" %' voile bleu ciel
        Gosub Flash
        Gosub InitEcranDeJeu
        gr.color 175,0,0,0,1
        gr.bitmap.draw tmpPtr, rainBmp[1], (di_w-126)/2, -154 %' rideau de pluie descend
        w=0
        for y=-154 to 0 step tileh/2
          gr.modify tmpPtr, "y", y
          if light then Gosub FiatLux else gr.render
          Pause 5
        next y
        array.shuffle rainOrder[]
        for t=1 to 23 %' rideau de pluie irrigue le paysage
          w=1-w
          gr.modify tmpPtr, "bitmap", rainBmp[w+1]
          if light then Gosub FiatLux else gr.render
          x=herox+dx+rainX[rainOrder[t]]
          y=heroy+dy+rainY[rainOrder[t]]
          Gosub GetS
          if s<>1 then
            Gosub GetB
            if b=1 | (b>=7 & b<=9) | b=16 | b=18 | b=19 then %' prairie desséchée --> plaine fertile
              if Rnd()<0.2 then
                b=11 %' (...soit tachetée, pour creuser des coeurs en loup) (proportion 20%)
              else
                b=29 %' (...soit plaine normale, bien verte) (proportion 80%)
              end if
              Gosub SetB
            elseif b=20 then %' lave --> +fumée
              if !mute then Soundpool.play tmp,sndMel,0.99,0.99,1,0,1
              o=3
              Gosub SetO
            elseif b=33 then %' marais à sec --> marais irrigué
              b=34
              Gosub SetB
              s=17
              Gosub SetS
            elseif b=59 | b=36 then %' banquise --> ruisseau
              b=70
              Gosub SetB
              Gosub GetS
              if s<>1 then %' protège les têtes de lion à l'entrée du labyrinthe
                s=17
                Gosub SetS
              end if
            elseif b=60 then %' igloo sur banquise --> igloo submergé
              b=46
              Gosub SetB
            end if
          end if
          Pause 12
        next t
        while Clock()-tmr<l-500
          w=1-w
          gr.modify tmpPtr, "bitmap", rainBmp[w+1]
          if light then Gosub FiatLux else gr.render
          Pause 5
        repeat
        if !mute & music$<>"" then audio.play musPtr
        for y=0 to -154 step -tileh %' rideau de pluie remonte
          gr.modify tmpPtr, "y", y
          if light then Gosub FiatLux else gr.render
          Pause 5
        next y
        Gosub InitEcranDeJeu
        if light then Gosub FiatLux else gr.render
        if perso[81]=10 & destx>235 & desty>158 & desty<163 then %' sépulture de Roberts
          Pause 1000
          destx=238
          desty=160
          Gosub DelO
          desty=161
          Gosub DelO
          Pause 1000
          Gosub DebloqueSecretMusique
          destx=236
          desty=160
          c=81
          Gosub DelC
          Gosub Explosion
          o=44
          Gosub SetO
        end if
        Goto BouclePrincipale
      end if %' fin rune magique ok/ko

    !' Miroir des glaces
    elseif objet[26]=1 & x>132 & x<182 & y>2 & y<52 then
      e$="schema3.png"
      Gosub FollowTheTrail
      if !red | !green then %' Hors des sentiers battus
        if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
        e$="FFFF00" %' voile jaune
        Gosub Flash
        Gosub InitEcranDeJeu
        if light then Gosub FiatLux else gr.render
        Goto BouclePrincipale
      else %' L'utilisateur a suivi le chemin de la formule magique comme il convient
        if !mute then
          l=GetAudioLength("miroir_glaces.mp3")
          tmr=Clock()
          Soundpool.play tmp,sndIce,0.99,0.99,1,0,1
          audio.pause
        else
          for i=1 to 3
            vibrate VibrCrt[], -1
            Pause 150
          next i
        end if
        e$="FFFF00" %' voile jaune
        Gosub Flash
        !' création images miroir
        gr.bitmap.create mir1,tilew,tileh
        gr.bitmap.drawinto.start mir1
        gr.rotate.start 270,tilew/2,tileh/2
        gr.bitmap.draw i,objTile[26],0,0
        gr.rotate.end
        gr.bitmap.drawinto.end
        gr.bitmap.create mir2,tilew,tileh
        gr.bitmap.drawinto.start mir2
        gr.rotate.start 90,tilew/2,tileh/2
        gr.bitmap.draw i,objTile[26],0,0
        gr.rotate.end
        gr.bitmap.drawinto.end
        !' craquelure et gel du paysage
        Gosub InitEcranDeJeu
        gr.bitmap.load tmpBmp, "icetrace.png"
        gr.bitmap.size tmpBmp, bb, cc
        gr.modify heroPtr, "bitmap", heroTile[heroform,_left+1]
        gr.bitmap.draw a1, mir1, (Floor(nthor/2)-0.5)*tilew, Floor(ntver/2)*tileh
        gr.color 125,255,255,0,1
        gr.set.stroke tilew-3
        gr.line b1,Floor(nthor/2)*tilew-1,di_h/2,Floor(nthor/2)*tilew-1,-tileh
        a2=0
        b2=0
        gr.set.stroke 0
        if light then Gosub FiatLux else gr.render
        for w=Floor(bb/2) to 0 step -1
          for z=Floor(cc/2) to cc-1
            gr.get.bmpixel tmpBmp,w,z,alpha,red,green,blue
            if red & green then %' yellow
              Gosub GelePaysage
            end if
          next z
        next w
        for w=Floor(bb/2) to 0 step -1
          for z=Floor(cc/2) to 0 step -1
            gr.get.bmpixel tmpBmp,w,z,alpha,red,green,blue
            if red then
              Gosub GelePaysage
            end if
          next z
        next w
        gr.modify heroPtr, "bitmap", heroTile[heroform,_right+1]
        gr.modify a1, "bitmap", mir2
        gr.modify a1, "x", (Floor(nthor/2)+0.5)*tilew
        gr.modify b1, "x1", (Floor(nthor/2)+1)*tilew+1
        gr.modify b1, "x2", (Floor(nthor/2)+1)*tilew+1
        gr.modify b2, "x1", (Floor(nthor/2)+1)*tilew+1
        if light then Gosub FiatLux else gr.render
        for w=Floor(bb/2) to bb-1
          for z=Floor(cc/2) to 0 step -1
            gr.get.bmpixel tmpBmp,w,z,alpha,red,green,blue
            if green then
              Gosub GelePaysage
            end if
          next z
        next w
        for w=Floor(bb/2) to bb-1
          for z=Floor(cc/2) to cc-1
            gr.get.bmpixel tmpBmp,w,z,alpha,red,green,blue
            if blue then
              Gosub GelePaysage
            end if
          next z
        next w
        gr.modify heroPtr, "bitmap", heroTile[heroform,DerDir+1]
        gr.hide a1
        gr.hide a2
        gr.hide b1
        gr.hide b2
        if light then Gosub FiatLux else gr.render
        gr.bitmap.delete tmpBmp %' ice_trace
        gr.bitmap.delete mir1
        gr.bitmap.delete mir2
        if Clock()-tmr<l-500 then Pause l-500-Clock()+tmr
        if !mute & music$<>"" then audio.play musPtr
        Gosub InitEcranDeJeu
        if light then Gosub FiatLux else gr.render
        Goto BouclePrincipale
      end if %' fin rune magique ok/ko

    !' Crâne de la désolation
    elseif objet[34]=1 & x>165 & x<215 & y>195 then
      e$="schema4.png"
      Gosub FollowTheTrail
      if !red | !green then %' Hors des sentiers battus
        if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
        e$="FF6A00" %' voile orange
        Gosub Flash
        Gosub InitEcranDeJeu
        if light then Gosub FiatLux else gr.render
        Goto BouclePrincipale
      else %' L'utilisateur a suivi le chemin de la formule magique comme il convient
        l=GetAudioLength("crane_desolation.mp3")
        tmr=Clock()
        if !mute then
          audio.load snd, "crane_desolation.mp3"
          audio.stop
          audio.play snd
        else
          for i=1 to 3
            vibrate VibrCrt[], -1
            Pause 150
          next i
        end if
        e$="FF6A00" %' voile orange
        Gosub Flash
        Gosub InitEcranDeJeu
        !' création et positionnement tête de dragon
        if DerDir=_right | DerDir=_down then
          if tete then gr.bitmap.delete tete
          gr.bitmap.create tete,tilew,tileh
          gr.bitmap.drawinto.start tete
          gr.bitmap.draw tmp,objTile[34],0,0
          gr.bitmap.drawinto.end
        else
          gr.bitmap.scale tete,objTile[34],-tilew,tileh
        end if
        gr.modify heroPtr, "bitmap", heroTile[heroform,DerDir+3]
        if DerDir=_left then
          w=-1
          gr.bitmap.draw fire[1],flame[1],(di_w+(2*w-1)*tilew)/2,Ceil(ntver/2)*tileh-42
          gr.bitmap.draw tmp, tete, (Floor(nthor/2)-0.5)*tilew, Floor(ntver/2)*tileh
        elseif DerDir=_right then
          w=1
          gr.bitmap.draw fire[1],flame[1],(di_w+(2*w-1)*tilew)/2,Ceil(ntver/2)*tileh-42
          gr.bitmap.draw tmp, tete, (Floor(nthor/2)+0.5)*tilew, Floor(ntver/2)*tileh
        elseif DerDir=_down then
          w=1
          gr.bitmap.draw tmp, tete, Floor(nthor/2)*tilew, (Floor(ntver/2)+0.3)*tileh
          gr.bitmap.draw fire[1],flame[1],Floor(nthor/2)*tilew,(Ceil(ntver/2)+0.5*w)*tileh-42
        elseif DerDir=_up then
          w=-1
          gr.hide heroPtr
          gr.bitmap.draw fire[1],flame[1],Floor(nthor/2)*tilew,(Ceil(ntver/2)+0.5*w)*tileh-42
          gr.bitmap.draw tmp, tete, Floor(nthor/2)*tilew, (Floor(ntver/2)-0.3)*tileh
          gr.bitmap.draw heroPtr,heroTile[1,_up+3],Floor(nthor/2)*tilew,Floor(ntver/2)*tileh
          if GhostMode then gr.modify heroPtr, "alpha", 128
          if tricheZeroEnnemi & heroform<>5 then gr.bitmap.draw tmp,shieldBmp,(Floor(nthor/2)+x)*tilew,Floor(ntver/2)*tileh
        end if
        gr.hide fire[1]
        if light then Gosub FiatLux else gr.render
        Pause 500
        z=1
        !' flammes sortent de la tete du dragon
        gr.show fire[1]
        for t=1 to 40
          gr.modify bgnPtr,"x",-tilew+Floor(3*Rnd())-1
          gr.modify bgnPtr,"y",-tileh+Floor(3*Rnd())-1
          if Mod(t,4)=1 then
            z=Mod(z,4)+1
            gr.modify fire[1],"bitmap",flame[z]
          end if
          if light then Gosub FiatLux else gr.render
          Pause 10
        next t
        !' le paysage va être désolé
        if DerDir=_left | DerDir=_right then
          v=Ceil(ntver/2)*tileh-42
          y=heroy+dy
        else
          u=Floor(nthor/2)*tilew
          x=herox+dx
        end if
        for t=1 to Floor(nthor/2)+0.5 step 0.5 %' création ligne de feu
          gr.modify bgnPtr,"x",-tilew+Floor(3*Rnd())-1
          gr.modify bgnPtr,"y",-tileh+Floor(3*Rnd())-1
          if DerDir=_left | DerDir=_right then
            u=(di_w+(2*w-1)*tilew)/2+w*(t-0.5)*tilew
            x=herox+dx+w*Floor(t)
          else
            v=(Ceil(ntver/2)+0.5*w)*tileh-42+w*(t-0.5)*tileh
            y=heroy+dy+w*Floor(t)
          end if
          gr.bitmap.draw fire[2*t],flame[Ceil(4*Rnd())],u,v
          if light then Gosub FiatLux else gr.render
          if Mod(2*t,2)=0 then %' désolation du paysage
            Gosub GetO
            if o=35 | o=24 then %' blocs de glace fondent ; carte du labyrinthe brûle !!!!!
              if !mute then Soundpool.play tmp,sndMel,0.99,0.99,1,0,1
              Gosub DelO
            end if
            Gosub GetB
            if b=45 & o & o<=3 then %' vide stellaire avec brumes magiques --> sol sacré
              if !mute then Soundpool.play tmp,sndMag,0.99,0.99,1,0,1
              b=43
              Gosub SetB
              s=19 %' solidité "humain seulement"
              Gosub SetS
              Gosub DelO %' supprime la brume
              if destx>226 & desty>200 & desty<220 then %' pyramide des paysages changeants
                solsacre=1
              end if
            end if
            Gosub GetS
            if s<>1 then
              if b & (b<=9 | b=11 | b=13 | (b>=23 & b<=25) | b=29 | (b>=33 & b<=35)) then
                b=20 %' toutes prairies, plaines, forêts --> lave
                Gosub SetB
                s=13
                Gosub SetS
              elseif b=10 then %' pics gelés --> pics dégelés
                if !mute then Soundpool.play tmp,sndMel,0.99,0.99,1,0,1
                b=65
                Gosub SetB
              elseif b=28 then %' montagnes gelées --> rochers dégelés
                if !mute then Soundpool.play tmp,sndMel,0.99,0.99,1,0,1
                b=26
                Gosub SetB
                s=13
                Gosub SetS
              elseif b=14 | b=46 | b=54 | (b>=56 & b<=58) | b>=69 then
                if !mute then Soundpool.play tmp,sndMel,0.99,0.99,1,0,1
                o=3 %' tous types d'eau --> +brume rouge
                Gosub SetO
              elseif b=36 | b=44 | b=59 | b=60 then
                if !mute then Soundpool.play tmp,sndMel,0.99,0.99,1,0,1
                b=16 %' tous types de glace --> terre battue
                Gosub SetB
                s=13
                Gosub SetS
              end if
            end if
          end if
        next t
        !' animation ligne de feu
        do
          for t=0.5 to Floor(nthor/2)+0.5 step 0.5
            gr.modify bgnPtr,"x",-tilew+Floor(3*Rnd())-1
            gr.modify bgnPtr,"y",-tileh+Floor(3*Rnd())-1
            gr.modify fire[2*t],"bitmap",flame[Ceil(4*Rnd())]
          next t
          if light then Gosub FiatLux else gr.render
        until Clock()-tmr>=l-1000
        !' extinction des flammes
        for e=255 to 0 step -25
          gr.modify bgnPtr,"x",-tilew+Floor(3*Rnd())-1
          gr.modify bgnPtr,"y",-tileh+Floor(3*Rnd())-1
          for t=0.5 to Floor(nthor/2)+0.5 step 0.5
            gr.modify fire[2*t], "alpha", e
          next t
          if light then Gosub FiatLux else gr.render
          Pause 2
        next e
        !' Libération des ressources, retour au jeu
        gr.bitmap.delete tete
        Gosub InitEcranDeJeu
        if light then Gosub FiatLux else gr.render
        if !mute then
          audio.stop
          audio.release snd
          if music$<>"" then
            audio.play musPtr
            audio.loop
          end if
        end if
        if solsacre & !perso[81] then
          Pause 2000
          perso[81]=1 %' Apparition du fantôme du Terrible Pirate Roberts !!
          Gosub MusiqueChange
          destx=228
          desty=210
          c=81
          Gosub MovC
          if !mute then Soundpool.play tmp,sndPRo,0.99,0.99,1,0,1
          Gosub Explosion
          destx=229
          s=13
          Gosub SetS
          b=26
          Gosub SetB
          Pause 500
        end if
        Goto BouclePrincipale
      end if %' fin rune magique ok/ko

    end if %' fin runes élémentales
  end if %' fin

until 0
!'---------------------------------------------------------------------------------------------------

GelePaysage:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine d'utilisation du miroir des glaces : gèle le paysage
!'---------------------------------------------------------------------------------------------------
if !a2 then
  u=(Floor(nthor/2)+w-Floor(bb/2)+0.5)*tilew
  v=(Floor(ntver/2)+z-Floor(cc/2)+0.5)*tileh
  gr.oval a2,u-tilew/2,v-tileh/2,u+tilew/2,v+tileh/2
  gr.set.stroke tilew-3
  gr.line b2,Floor(nthor/2)*tilew-1,-tileh,u,v
  gr.set.stroke 0
else
  u=(Floor(nthor/2)+w-Floor(bb/2)+0.5)*tilew
  v=(Floor(ntver/2)+z-Floor(cc/2)+0.5)*tileh
  gr.modify a2, "left", u-tilew/2
  gr.modify a2, "top", v-tileh/2
  gr.modify a2, "right", u+tilew/2
  gr.modify a2, "bottom", v+tileh/2
  gr.modify b2, "x2", u
  gr.modify b2, "y2", v
end if
x=herox+dx+w-Floor(bb/2)
y=heroy+dy+z-Floor(cc/2)
Gosub GetS
if s<>1 then
  Gosub GetB
  if b=56 | b=57 | b=58 | b=69 | b=70 then %' tout type d'eau --> glace
    if Rnd()<0.2 then
      b=36 %' (...soit glace semi-fondue avec prairie en dessous) (proportion 20%)
      Gosub SetB
      if s<>19 then %' protection ile du dragon
        s=13
        Gosub SetS
      end if
    else
      b=59 %' (...soit banquise) (proportion 80%)
      Gosub SetB
      if s<>19 then %' protection ile du dragon
        s=16
        Gosub SetS
      end if
    end if
  else if b=46 then %' igloo submergé --> igloo sur banquise
    b=60
    Gosub SetB
  else if b=34 then %' marais irrigué --> glace semi-fondue avec prairie en dessous
    b=36
    Gosub SetB
    s=13
    Gosub SetS
  else if b=20 then %' lave --> terre battue
    b=16
    Gosub SetB
  else if b=65 then %' pics --> pics gelés
    b=10
    Gosub SetB
  else
    if light then Gosub FiatLux else gr.render
  end if
else
  if light then Gosub FiatLux else gr.render
end if
Pause 2
Return
!'---------------------------------------------------------------------------------------------------

InitEcranDeJeu:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine d'initialisation de l'écran de jeu entier
!'---------------------------------------------------------------------------------------------------
gr.cls %' Call gClear()
coeurPtr=0
flashPtr=0
gr.color 255,0,0,0,1
gr.bitmap.draw bgnPtr,bgnBmp,-tilew,-tileh
gr.bitmap.draw blocPtr,objTile[35],0,0
gr.hide blocPtr
gr.bitmap.draw exploPtr,exploBmp[1],0,0
gr.hide exploPtr
olddestx=destx
olddesty=desty
destx=herox-1
desty=heroy-1
Gosub FromDuskTillDawn
destx=olddestx
desty=olddesty
dkns=dknsTgt
gr.rect dknsPtr, 0, 0, di_w, di_h
gr.modify dknsPtr,"alpha",dkns*(255-light)/255
gr.bitmap.draw lightPtr,lightBmp,0,0
gr.modify lightPtr,"alpha",light*dkns/255
if heroform=5 then x=-1 else x=0
gr.bitmap.draw heroPtr, heroTile[heroform,DerDir+1], (Floor(nthor/2)+x)*tilew, Floor(ntver/2)*tileh
if GhostMode then gr.modify heroPtr, "alpha", 128
if tricheZeroEnnemi & heroform<>5 then gr.bitmap.draw tmp,shieldBmp,(Floor(nthor/2)+x)*tilew,Floor(ntver/2)*tileh
Gosub InitBoutonsEcranDeJeu
Return
!'---------------------------------------------------------------------------------------------------

InitBoutonsEcranDeJeu:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine d'initialisation de l'écran de jeu (boutons)
!'---------------------------------------------------------------------------------------------------
!' Pad directionnel
for i=1 to 2
  if i=1 then
    gr.color 75,255,255,255,1
  else
    gr.color 75,0,0,0,0
    gr.set.stroke 2
  end if
  gr.circle y, padsize*tilew*2.5, di_h - padsize*tileh*4, padsize*tilew %' up
  gr.circle y, padsize*tilew*1, di_h - padsize*tileh*2.5, padsize*tilew %' left
  gr.circle y, padsize*tilew*2.5, di_h - padsize*tileh, padsize*tilew %' down
  gr.circle y, padsize*tilew*4, di_h - padsize*tileh*2.5, padsize*tilew %' right
next i
!' Bouton paramètres
gr.color 155,255,255,255,1
gr.bitmap.draw paramPtr, paramBmp, di_w-36, di_h-36
!' Bouton de l'inventaire
gr.color 75,255,255,255,1
gr.rect y, di_w - padsize*tilew*2, padsize*tileh*0.5, di_w - padsize*tilew*0.5, padsize*tileh*2
gr.color 75,0,0,0,0
gr.rect y, di_w - padsize*tilew*2, padsize*tileh*0.5, di_w - padsize*tilew*0.5, padsize*tileh*2
gr.line y, di_w - padsize*tilew*1.8, padsize*tileh*1, di_w - padsize*tilew*0.7, padsize*tileh*1
gr.line y, di_w - padsize*tilew*1.8, padsize*tileh*1.25, di_w - padsize*tilew*0.7, padsize*tileh*1.25
!' Dernier équipement utilisé
if LstEqu then
  gr.color 75,255,255,255,1
  gr.rect y, di_w - padsize*tilew*4, padsize*tileh*0.5, di_w - padsize*tilew*2.5, padsize*tileh*2
  gr.color 75,0,0,0,0
  gr.rect y, di_w - padsize*tilew*4, padsize*tileh*0.5, di_w - padsize*tilew*2.5, padsize*tileh*2
  gr.color 155,0,0,0,0
  gr.bitmap.draw y, lstEquBmp, di_w - padsize*tilew*4, padsize*tileh*0.5
end if
!' Barre de vie, argent
gr.set.stroke 0
gr.color 175,255,0,0,1
gr.bitmap.draw tmpPtr,coeurBmp,0,0 %' coeur
gr.rect pdvPtr, tilew*1.1, tileh*0.4, tilew*4.1, tileh*0.6
gr.modify pdvPtr, "right", tilew*(1.1+3*pdv/100)
gr.color 175,50,50,50,0
gr.set.stroke 1
gr.rect tmpPtr, tilew*1.1, tileh*0.4, tilew*4.1, tileh*0.6
gr.bitmap.draw tmpPtr,objTile[12],0,tileh %' bourse
gr.text.size 12
gr.text.align 1
gr.color 175,255,255,255,1
For y=-1 to 1
  For x=-1 to 1
    i=3*(y+1)+(x+2)
    if i<>5 then gr.text.draw usdPtr[i], tilew*1.1+x, tileh*1.8+y, Stg$(usd)
  Next x
Next y
gr.color 175,10,10,20,1
gr.text.draw usdPtr[5], tilew*1.1, tileh*1.8, Stg$(usd)
gr.color 255,0,0,0,1
Return
!'---------------------------------------------------------------------------------------------------

TestJeuEnArrierePlan:
!'---------------------------------------------------------------------------------------------------
!' Vérif appui touche Home du téléphone (met le jeu en veille) + Back + Gestion des tiles de dommage
!'---------------------------------------------------------------------------------------------------
if Background() then
  if !mute then audio.pause
  if pdv>0 then Gosub SauverPartie
  if needSaveMAPs then Gosub SauverCalques
  Gosub InitEcranDeJeu
  Notify RPGLBL$[72], RPGLBL$[73], RPGLBL$[74], 0
  do
    Pause 1000 %' wait in background doing nothing
  until !Background()
  gr.render
  if !mute & music$<>"" then audio.play musPtr
end if
if hurtTmr then %' Gestion des dommages sur certains tiles
  if Clock()-hurtTmr>1000 then Gosub HurtMePlenty
end if
if light then Gosub FiatLux %' Gestion de l'obscurité
if BackKey then
  BackKey=0
  if !mute then audio.stop
  if pdv>0 then Gosub SauverPartie
  Gosub FadeOut
  di_w=640
  di_h=480
  scale_w=real_w/di_w
  scale_h=real_h/di_h
  gr.scale scale_w, scale_h
  Goto MenuPrincipal
end if
Return
!'---------------------------------------------------------------------------------------------------

TheEnd:
!'---------------------------------------------------------------------------------------------------
!' Le héros a perdu tous ses PDV au cours de l'aventure
!'---------------------------------------------------------------------------------------------------
di_w=nthor/2*tilew
di_h=ntver/2*tileh
scale_w=real_w/di_w
scale_h=real_h/di_h
gr.scale scale_w, scale_h
Time y$,m$,d$,h$,mn$,s$
score=10*victories*(difficulte+1)*(1+neq/25) %' 25 objets trouvables en tout dans l'aventure
date$=Stg$(val(d$))+" "+Mois$[val(m$)]+" "+y$+RPGLBL$[75]+h$+":"+mn$+":"+s$
e$=Stg$(score)+RPGLBL$[76]+date$+" --> "+e$
Text.open a, nul, "../scores.txt"
Encrypt MDP$, e$, s$
Text.writeln nul, s$
Text.close nul
light=0 %' annule la baisse de luminosité (si la bougie est en train de s'éteindre)
gr.cls %' Call gClear()
gr.color 255,255,255,255,1
gr.bitmap.scale ennBmp1, DragonRepos, 3*tilew*1.5, 2*tileh*1.5, 0
gr.bitmap.scale ennBmp2, DragonFeu, 3*tilew*1.5, 2*tileh*1.5, 0
gr.bitmap.draw ennPtr, ennBmp1, (di_w-3*tilew*1.5)/2, (di_h-2*tileh*1.5)/2
gr.bitmap.draw heroPtr, heroTile[1,_right+1], 0, 0
gr.text.size 10
gr.text.align 2
gr.text.draw i,di_w/2,1.5*tileh,RPGLBL$[77]
gr.render
!' Musique
if !mute then
  audio.load snd, "TheEnd.mid"
  audio.stop
  if musPtr then audio.release musPtr
  audio.play snd
end if
!' Animation des héros morts au combat
for hf=1 to 4
  gr.modify ennPtr, "bitmap", ennBmp1
  gr.modify heroPtr, "bitmap", heroTile[hf,_right+1]
  for x=0 to di_w-tilew step tilew/4
    gr.modify heroPtr, "x", x
    gr.render
    Pause 5
  next x
  gr.modify ennPtr, "bitmap", ennBmp2
  gr.modify heroPtr, "bitmap", heroTile[hf,_down+1]
  for y=0 to di_h-tileh step tileh/4
    gr.modify heroPtr, "y", y
    gr.render
    Pause 5
  next y
  gr.modify ennPtr, "bitmap", ennBmp1
  gr.modify heroPtr, "bitmap", heroTile[hf,_left+1]
  for x=di_w-tilew to 0 step -tilew/4
    gr.modify heroPtr, "x", x
    gr.render
    Pause 5
  next x
  gr.modify ennPtr, "bitmap", ennBmp2
  gr.modify heroPtr, "bitmap", heroTile[hf,_up+1]
  for y=di_h-tileh to 0 step -tileh/4
    gr.modify heroPtr, "y", y
    gr.render
    Pause 5
  next y
  if heroform=1 & HerosEnHeaume then Gosub MettreEnleverLeHeaume
next hf
!' Ecran de continue
gr.cls %' Call gClear()
gr.color 255,255,255,255,1
gr.bitmap.draw ennPtr, ennBmp1, (di_w-3*tilew*1.5)/2, (di_h-2*tileh*1.5)/2
gr.text.draw i,di_w/2,1.5*tileh,RPGLBL$[78]
gr.color 255,255,255,255,1
gr.text.draw w,di_w/2,di_h-tileh,RPGLBL$[79]
x=2*tilew
s=3*tilew
gr.color 255,255,255,255,1
gr.arc tmp,x-s/2-3,0,x-s/2+3,tileh/2,90,180,1
gr.arc tmp,x+s/2-3,0,x+s/2+3,tileh/2,-90,180,1
gr.rect tmp,x-s/2-1,0,x+s/2+1,tileh/2
gr.color 255,0,0,0,1
gr.text.bold 1
gr.text.draw tmp,x,tileh/2-1,RPGLBL$[80]
gr.text.bold 0
gr.bitmap.scale tmpBmp, exitBmp, 0.8*tilew, 0.8*tileh, 0
gr.bitmap.draw nul, tmpBmp, di_w-tilew, 0
gr.render
tmr=Clock()
!' Attendre que l'utilisateur touche l'écran
do
  if Clock()-tmr>2000 then
    gr.modify ennPtr, "bitmap", ennBmp1
    gr.show w
    gr.render
    tmr=Clock()
  elseif Clock()-tmr>1000 then
    gr.modify ennPtr, "bitmap", ennBmp2
    gr.hide w
    gr.render
  end if
  gr.touch tmp, x, y
  inkey$ k$
  if k$<>"@" then tmp=1
until tmp
x=x/scale_w
y=y/scale_h
hurtTmr=0 %' l'éventuelle tile de dommage qui a tué le héros n'a plus d'effet !
LstEqu=0 %' Dernier équipement utilisé : rien
JeuDejaPret=0
if !mute then
  audio.stop
  audio.release snd
end if
!' Appui touche Exit
if x>=di_w-tilew & y<=0.8*tileh then
    vibrate VibrCrt[], -1
    gr.cls
    gr.render
    Popup RPGLBL$[81],0,0,0
    Pause 500
    Exit
!' Appui bouton "Menu titre"
elseif x<=3.5*tilew & y<=tileh/2 then
  gr.color 255,0,0,0,1
  gr.rect btn,0,0,4*tilew,tileh/2
  Gosub PressButton
  Gosub FadeOut
end if
gr.bitmap.delete ennBmp1
gr.bitmap.delete ennBmp2
gr.bitmap.delete tmpBmp
if x<=3.5*tilew & y<=0.8*tileh then
  di_w=640
  di_h=480
  scale_w=real_w/di_w
  scale_h=real_h/di_h
  gr.scale scale_w, scale_h
  Goto MenuPrincipal
end if
!' Appui écran : récupère la dernière sauvegarde et charge les calques
gr.cls %' Call gClear()
gr.color 255,255,255,255,1
gr.bitmap.draw tmp, heroTile[1,15], di_w/2-2*tilew, (di_h-tileh)/2
gr.bitmap.draw tmp, heroTile[2,6], di_w/2-tilew, (di_h-tileh)/2
gr.bitmap.draw tmp, heroTile[3,9], di_w/2, (di_h-tileh)/2
gr.bitmap.draw tmp, heroTile[4,3], di_w/2+tilew, (di_h-tileh)/2
gr.text.draw i,di_w/2,2*tileh,RPGLBL$[82]
gr.text.draw i,di_w/2,di_h-1.5*tileh,RPGLBL$[83]
gr.render
Gosub ChargerSauvegarde
pdv=100 %' le héros vient de mourir, il repart au moins avec tous ses points de vie
gr.text.width b2, RPGLBL$[84]
gr.color 255,255,255,255,1
gr.arc i,di_w/2-b2/2-3,di_h*3/4+10,di_w/2-b2/2+3,di_h*3/4+16,90,180,1
gr.arc i,di_w/2+b2/2-3,di_h*3/4+10,di_w/2+b2/2+3,di_h*3/4+16,-90,180,1
gr.color 255,255,255,255,0
gr.rect a1,di_w/2-b2/2,di_h*3/4+10,di_w/2+b2/2,di_h*3/4+16
gr.color 255,255,255,255,1
gr.rect a2,di_w/2-b2/2,di_h*3/4+10,di_w/2-b2/2,di_h*3/4+16
if bgnBmp then gr.bitmap.delete bgnBmp
gr.bitmap.create bgnBmp, (nthor+2)*tilew, (ntver+2)*tileh
e$=""
for y=heroy+dy-Floor(ntver/2)-1 to heroy+dy+Floor(ntver/2)+1
  for x=herox+dx-Floor(nthor/2)-1 to herox+dx+Floor(nthor/2)+1
    Gosub AfficherDecor
    if Mod(x-herox-dx+Floor(nthor/2)+1,4)=0 then
      pct=((nthor+2)*(y-heroy-dy+Floor(ntver/2)+1)+x-herox-dx+Floor(nthor/2)+1) / ((nthor+2)*(ntver+2))
      gr.modify a2,"right",di_w/2-b2/2+b2*pct
      gr.render
    end if
  next x
next y
gr.modify a2,"right",di_w/2+b2/2 %' barre à 100%
gr.render
if heroform=1 then DerDir=9 else DerDir=_down %' Spécial animation nouveau jeu
Goto DemarrerJeu
!'---------------------------------------------------------------------------------------------------

ActionHeros:
!'---------------------------------------------------------------------------------------------------
!' Routine d'action du héros (l'utilisateur "touche" le héros au centre de l'écran)
!'---------------------------------------------------------------------------------------------------
x=herox+dx
y=heroy+dy
Gosub GetB
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
!' [Script 1][pas de déplacement] Le héros fait une ACTION (l'utilisateur le touche à l'écran)
!'      b=(background) tile de décor sur lequel le héros se tient
!'      destx, desty=coordonnées actuelles du héros
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
if heroform=2 & b=11 then
  Gosub DeterreUnCoeur %' loup creuse un coeur dans la terre meuble de la prairie
  Goto BouclePrincipale
elseif heroform=3 & b=58 then
  Gosub DeterreUnCoeur %' baleine mange le plancton des eaux profondes
  Goto BouclePrincipale
elseif heroform=1 & perso[10]=2 & destx=161 & desty=61 then
  o=22 %' clé de Zoraniscaeos ville (dans la flaque)
  Gosub ObjetMineurRebondit
  perso[10]=3
  Gosub DebloqueSecretMusique
end if
Pause 100
if Clock()-cheatTmr<2000 then
  gr.bounded.touch2 tmp,padsize*tilew*2*scale_w,real_h-padsize*tileh*4.5*scale_h,padsize*tilew*3*scale_w,real_h-padsize*tileh*3.5*scale_h
  if tmp then Goto MenuTriche
end if
!' [Fin du Script 1] FinScript1
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
FinScript1:
Goto BouclePrincipale

Inventaire:
!'---------------------------------------------------------------------------------------------------
!' Routine de consultation de l'équipement
!'---------------------------------------------------------------------------------------------------
gr.cls %' Call gClear()
gr.color 255,0,0,0,1
gr.bitmap.draw bgnPtr,bgnBmp,-tilew,-tileh
gr.rect dknsPtr, 0, 0, di_w, di_h
gr.modify dknsPtr,"alpha",dkns*(255-light)/255
gr.bitmap.draw lightPtr,lightBmp,0,0
gr.modify lightPtr,"alpha",light*dkns/255
if heroform=5 then x=-1 else x=0
gr.bitmap.draw heroPtr, heroTile[heroform,DerDir+1], (Floor(nthor/2)+x)*tilew, Floor(ntver/2)*tileh
if GhostMode then gr.modify heroPtr, "alpha", 128
if tricheZeroEnnemi & heroform<>5 then gr.bitmap.draw tmp,shieldBmp,(Floor(nthor/2)+x)*tilew,Floor(ntver/2)*tileh
!' Bouton de l'inventaire
gr.color 75,255,255,255,1
gr.rect y, di_w - padsize*tilew*2, padsize*tileh*0.5, di_w - padsize*tilew*0.5, padsize*tileh*2
gr.color 75,0,0,0,0
gr.set.stroke 2
gr.rect y, di_w - padsize*tilew*2, padsize*tileh*0.5, di_w - padsize*tilew*0.5, padsize*tileh*2
gr.line y, di_w - padsize*tilew*1.8, padsize*tileh*1, di_w - padsize*tilew*0.7, padsize*tileh*1
gr.line y, di_w - padsize*tilew*1.8, padsize*tileh*1.25, di_w - padsize*tilew*0.7, padsize*tileh*1.25
gr.set.stroke 0
for i=1 to 3
  vibrate VibrCrt[], -1
next i
gr.color 255,0,0,0,1
gr.bitmap.draw tmpPtr, equBmp, di_w, -di_h
for x=di_w-tilew/2 to tilew/2 step -tilew
  y=tileh - x/di_w*di_h
  gr.modify tmpPtr, "x", x
  gr.modify tmpPtr, "y", y
  if light then Gosub FiatLux else gr.render
  Pause 2
next x
do
  gr.touch tmp, x, y
until !tmp

do
  Gosub TestJeuEnArrierePlan
  gr.bounded.touch tmp,real_w-padsize*tilew*2*scale_w,padsize*tileh*0.5*scale_h,real_w-padsize*tilew*0.5*scale_w,padsize*tileh*2*scale_h
  inkey$ k$
  if k$="key 84" then tmp=1 %' Touche Search
  if tmp then %' Ré-appui sur le bouton de l'inventaire : ferme l'inventaire
    vibrate VibrCrt[], -1
    for x=tilew/2 to di_w-tilew/2 step tilew
      y=tileh - x/di_w*di_h
      gr.modify tmpPtr, "x", x
      gr.modify tmpPtr, "y", y
      if light then Gosub FiatLux else gr.render
      if x<di_w then Pause 2
    next x
    Gosub InitEcranDeJeu
    if light then Gosub FiatLux else gr.render
    do
      gr.touch tmp, x, y
    until !tmp
    cheatTmr=Clock()
    Goto BouclePrincipale
  else
    gr.touch tmp, x, y
    if tmp then
      x=Floor(0.5*(x-tilew/2*scale_w)/(tilew*scale_w))
      y=Floor(0.5*(y-tileh/2*scale_h)/(tileh*scale_h))
      if x<0 then x=0
      if x>(nthor-1)/2-1 then x=(nthor-1)/2-1
      if y<0 then y=0
      if y>(ntver-1)/2-1 then y=(ntver-1)/2-1
      if y<>0 | x<>(nthor-3)/2 then
        i=y*(nthor-1)/2+x+1
        if y>0 then i=i-1
        if i<=neq then
          o=equipement[i]
          LstEqu=o
          gr.color 255,0,0,0,1
          if lstEquBmp then gr.bitmap.delete lstEquBmp
          gr.bitmap.scale lstEquBmp,objTile[o],padsize*tilew*1.5,padsize*tileh*1.5, 0
          for i=1 to 3
            vibrate VibrCrt[], -1
          next i
          for i=250 to 0 step -25
            gr.modify tmpPtr, "alpha", i
            if light then Gosub FiatLux else gr.render
            Pause 2
          next i
          Gosub InitEcranDeJeu
          if light then Gosub FiatLux else gr.render
          do
            gr.touch tmp, x, y
          until !tmp
          Goto UtiliseObjet
        end if
        do
          gr.touch tmp, x, y
        until !tmp
      end if
    end if
  end if
until 0
!'---------------------------------------------------------------------------------------------------

UtiliseObjet:
!'---------------------------------------------------------------------------------------------------
!' Routine d'utilisation de l'équipement
!'---------------------------------------------------------------------------------------------------
x=herox + dx
y=heroy + dy
Gosub GetS
z=-3*tileh*scale_h
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
!' [Script 2][pas de déplacement] Le héros UTILISE un objet de son EQUIPEMENT
!'      destx, desty=coordonnées actuelles du héros
!'      o=objet utilisé ; s=solidité du tile de décor courant
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
if o=14 then %' Bague sylvestre (loup)
  Popup RPGLBL$[85],0,z,0
  Pause 2200
  Popup RPGLBL$[86],0,z,0
  Pause 2200

elseif o=15 then %' Bague sulfure (aigle)
  Popup RPGLBL$[87],0,z,0
  Pause 2200
  Popup RPGLBL$[88],0,z,0
  Pause 2200

elseif o=16 then %' Bague sanguine (poisson)
  Popup RPGLBL$[89],0,z,0
  Pause 2200
  Popup RPGLBL$[90],0,z,0
  Pause 2200

elseif o=9 then %' Bouclier
  Popup RPGLBL$[91],0,z,0
  Pause 2200
  Popup RPGLBL$[92],0,z,0
  Pause 2200

elseif o=29 then %' Armure du guerrier
  Popup RPGLBL$[93],0,z,0
  Pause 2200
  Popup RPGLBL$[94],0,z,0
  Pause 2200

elseif o=10 then %' Dague
  Popup RPGLBL$[95],0,z,0
  Pause 2200
  Popup RPGLBL$[96],0,z,0
  Pause 2200

elseif o=32 then %' Masse d'arme
  Popup RPGLBL$[97],0,z,0
  Pause 2200
  Popup RPGLBL$[98],0,z,0
  Pause 2200

elseif o=44 then %' Emeraude
  if !perso[28] then
    Popup RPGLBL$[99],0,z,0
    Pause 2200
    Popup RPGLBL$[100],0,z,0
    Pause 2200
  else
    if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
  end if

elseif o=30 then %' Epée légendaire
  if destx=203 & desty=249 & HerosEnHeaume & !objet[34] then %' face au dragon mort
    if DerDir<>_left then
      if !mute then Soundpool.play tmp,sndHno,0.99,0.99,1,0,1
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+2]
      if light then Gosub FiatLux else gr.render
      Pause 300
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+1]
      if light then Gosub FiatLux else gr.render
    else
      !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      c=9 %' fait disparaitre le nain marin et son bateau
      Gosub DelC
      batx=UNDEF
      baty=UNDEF
      !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      if !mute then Soundpool.play tmp,sndSwo,0.99,0.99,1,0,1
      gr.modify heroPtr,"bitmap",heroTile[heroform,_left+2]
      if light then Gosub FiatLux else gr.render
      Pause 500
      c=49
      Gosub DelC
      c=74
      destx=202
      desty=249
      Gosub MovC
      o=34
      Gosub ObjetMineurRebondit
      e$=RPGLBL$[101]
      Gosub TrouveObjet
    end if
  else
    Popup RPGLBL$[102],0,z,0
    Pause 2200
    Popup RPGLBL$[103],0,z,0
    Pause 2200
  end if

elseif o=28 then %' Oeil de Ra
  Popup RPGLBL$[104],0,z,0
  Pause 2200
  Popup RPGLBL$[105],0,z,0
  Pause 2200
  Popup RPGLBL$[106],0,z,0
  Pause 2200
  Popup RPGLBL$[107],0,z,0
  Pause 2200

elseif o=11 then %' Carte maritime
  if heroform<>1 & heroform<>5 then
    if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
    Popup RPGLBL$[108],0,z,0
    Pause 2200
  else
    Goto DisplayWorldMap
  end if

elseif o=25 then %' Runes élémentales
  if heroform<>1 & heroform<>5 then
    if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
    Popup RPGLBL$[109],0,z,0
    Pause 2200
  else
    Goto DisplayParchment
  end if

elseif o=24 then %' Carte du labyrinthe
  if heroform<>1 & heroform<>5 then
    if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
    Popup RPGLBL$[110],0,z,0
    Pause 2200
  else
    Goto DisplayMazeMap
  end if

elseif o=27 then %' Bougie
  if !mute then Soundpool.play tmp,sndCan,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
  if dkns then
    lightTmr=Clock()
    Gosub FiatLux
  end if

elseif o=6 then %' Crâne purifié du terrible pirate Roberts
  if perso[81]=9 & destx=238 & desty=160 then
    perso[81]=10
    o=6
    Gosub SetO
    Gosub DebloqueSecretMusique
  elseif perso[81]<10 then
    Popup RPGLBL$[111],0,z,0
    Pause 2200
  else
    if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
  end if

elseif o=7 then %' Crâne du terrible pirate Roberts
  Popup RPGLBL$[112],0,z,0
  Pause 2200
  Popup RPGLBL$[113],0,z,0
  Pause 2200

elseif o=8 then %' Conque du vent
  Gosub GetO
  if HerosEnHeaume then
    Popup RPGLBL$[114],0,z,0
    Pause 2200
  elseif o=21 & heroform=1 then %' en humain sur une tornade !
    if destx=205 & desty=64 then %' tornade de l'île-sable
      destx=225
      desty=242
      Goto Tornado
    elseif destx=225 & desty=242 then %' tornade de la forêt du bord du monde
      destx=205
      desty=64
      Goto Tornado
    elseif destx=169 & desty=184 then %' tornade du centre du labyrinthe
      destx=47
      desty=241
      Goto Tornado
    elseif destx=47 & desty=241 then %' tornade de l'île-dragon
      destx=169
      desty=184
      Goto Tornado
    end if
  else %' autre part que sur une tornade, ou pas en humain
    if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
    if heroform<>1 & heroform<>5 then
      Popup RPGLBL$[115],0,z,0
    else
      Popup RPGLBL$[116],0,z,0
    end if
    Pause 2200
  end if

elseif o=4 then %' Croix de l'eau
  if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
  Popup RPGLBL$[117],0,z,0
  Pause 2200

elseif o=26 then %' Miroir des glaces
  if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
  Popup RPGLBL$[118],0,z,0
  Pause 2200

elseif o=34 then %' Crâne de la désolation
  if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
  Popup RPGLBL$[119],0,z,0
  Pause 2200

elseif o=33 then %' Heaume étincelant
  if heroform>1 & heroform<5 then
    if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
    Popup RPGLBL$[120],0,z,0
    Pause 2200
  else if heroform=5 then
    if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
    Popup RPGLBL$[121],0,z,0
    Pause 2200
  else
    Gosub MettreEnleverLeHeaume
    Goto CheckObjet %' vérif si le héros est sur le tile d'objet spécial face à la tête de dragon
  end if

elseif o=18 then %' Sphère du loup
  if heroform=5 then %' tentative de devenir loup dans le bateau
    if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
    Popup RPGLBL$[122],0,z,0
    Pause 2200
  else if heroform=2 then %' loup redevient humain
    if s=13 | s=20 then
      needChangeMusic=0
      heroform=1
      Goto Transformation
    elseif s=16 | s=18 then
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      Popup RPGLBL$[123],0,z,0
      Pause 2200
    else
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      Popup RPGLBL$[124],0,z,0
      Pause 2200
    end if
  else %' humain, poisson ou aigle se transforment en loup
    if s=13 | s=16 | s=18 | s=20 then
      if heroform=3 | heroform=4 then needChangeMusic=1 else needChangeMusic=0
      heroform=2
      Goto Transformation
    elseif s=14 then
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      Popup RPGLBL$[125],0,z,0
      Pause 2200
    elseif s=15 | s=17 then
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      Popup RPGLBL$[126],0,z,0
      Pause 2200
    elseif s=19 | s<5 then
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      Popup RPGLBL$[127],0,z,0
      Pause 2200
    end if
  end if

elseif o=19 then %' Sphère de l'aigle
  if heroform=5 then %' tentative de devenir aigle dans le bateau
    if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
    Popup RPGLBL$[128],0,z,0
    Pause 2200
  else if heroform=4 then %' aigle redevient humain
    Gosub GetC
    Gosub GetO
    if c | o>=35 then 
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      Popup RPGLBL$[129],0,z,0
      Pause 2200
    elseif s=13 | s=17 | s=20 then
      needChangeMusic=1
      heroform=1
      Goto Transformation
    elseif s=14 then
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      Popup RPGLBL$[130],0,z,0
      Pause 2200
    elseif s=16 | s=18 then
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      Popup RPGLBL$[131],0,z,0
      Pause 2200
    else
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      Popup RPGLBL$[132],0,z,0
      Pause 2200
    end if
  else %' humain, poisson ou loup se transforment en aigle
    if s=19 | s<5 then
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      Popup RPGLBL$[133],0,z,0
      Pause 2200
    else
      needChangeMusic=1
      heroform=4
      Goto Transformation
    end if
  end if

elseif o=20 then %' Sphère du poisson
  if heroform=5 then %' tentative de devenir poisson dans le bateau
    if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
    Popup RPGLBL$[134],0,z,0
    Pause 2200
  else if heroform=3 then %' poisson redevient humain
    if s=17 then
      needChangeMusic=1
      heroform=1
      Goto Transformation
    elseif s=15 then
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      Popup RPGLBL$[135],0,z,0
      Pause 2200
    elseif s=18 then
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      Popup RPGLBL$[136],0,z,0
      Pause 2200
    else
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      Popup RPGLBL$[137],0,z,0
      Pause 2200
    end if
  else %' humain, loup ou aigle se transforment en poisson
    if s=15 | s=17 | s=18 then
      needChangeMusic=1
      heroform=3
      Goto Transformation
    elseif s=14 then
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      Popup RPGLBL$[138],0,z,0
      Pause 2200
    elseif s=16 then
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      Popup RPGLBL$[139],0,z,0
      Pause 2200
    else
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      Popup RPGLBL$[140],0,z,0
      Pause 2200
    end if
  end if

elseif o=5 then %' Pointe de givre
  if heroform=5 then
    if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
    Popup RPGLBL$[141],0,z,0
    Pause 2200
  elseif heroform<>1 then
    if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
    Popup RPGLBL$[142],0,z,0
    Pause 2200
  else
    x=herox + dx
    y=heroy + dy
    if DerDir>5 then
      x=x - (DerDir - 8) / 2
    else
      y=y + (DerDir - 1.5) / 1.5
    end if
    Gosub GetB
    Gosub GetO
    Gosub GetC
    if c then %' ne peut pas créer un bloc sur un perso
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+2]
      if light then Gosub FiatLux else gr.render
      Pause 300
      Popup RPGLBL$[143],0,z,0
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+1]
      if light then Gosub FiatLux else gr.render
      Pause 300
    elseif o & b<>65 then %' ne peut pas créer un bloc sur un objet (sauf s'il est sur des pics)
      Popup RPGLBL$[144],0,z,0
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+2]
      if light then Gosub FiatLux else gr.render
      Pause 300
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+1]
      if light then Gosub FiatLux else gr.render
      Pause 300
    elseif b=65 then %' crée un bloc sur des pics : les gèle !
      if !mute then Soundpool.play tmp,sndBlo,0.99,0.99,1,0,1
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+2]
      gr.modify blocPtr, "x", (Floor(nthor/2)+destx-herox+1)*tilew
      gr.modify blocPtr, "y", (Floor(ntver/2)+desty-heroy+1)*tileh
      gr.show blocPtr
      if light then Gosub FiatLux else gr.render
      Pause 300
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+1]
      if light then Gosub FiatLux else gr.render
      Pause 300
      if !mute then Soundpool.play tmp,sndMel,0.99,0.99,1,0,1
      gr.hide blocPtr
      b=10
      Gosub SetB
      s=13
      Gosub SetS
      Gosub Explosion
    elseif b=20 then %' crée un bloc dans de la lave : le bloc fond
      if !mute then Soundpool.play tmp,sndBlo,0.99,0.99,1,0,1
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+2]
      gr.modify blocPtr, "x", (Floor(nthor/2)+destx-herox+1)*tilew
      gr.modify blocPtr, "y", (Floor(ntver/2)+desty-heroy+1)*tileh
      gr.show blocPtr
      if light then Gosub FiatLux else gr.render
      Pause 300
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+1]
      if light then Gosub FiatLux else gr.render
      Pause 300
      if !mute then Soundpool.play tmp,sndMel,0.99,0.99,1,0,1
      gr.hide blocPtr
      Gosub Explosion
    elseif b=45 | b=51 | b=52 | b=61 then %' crée un bloc dans le vide : tombe
      if !mute then Soundpool.play tmp,sndBlo,0.99,0.99,1,0,1
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+2]
      gr.modify blocPtr, "x", (Floor(nthor/2)+destx-herox+1)*tilew
      gr.modify blocPtr, "y", (Floor(ntver/2)+desty-heroy+1)*tileh
      gr.show blocPtr
      if light then Gosub FiatLux else gr.render
      Pause 300
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+1]
      if light then Gosub FiatLux else gr.render
      Pause 300
      if !mute then Soundpool.play tmp,sndFal,0.99,0.99,1,0,1
      gr.hide blocPtr
      Gosub Explosion
    elseif Is_In(","+Stg$(b)+",", BgnBloc$) then %' crée un bloc
      if !mute then Soundpool.play tmp,sndBlo,0.99,0.99,1,0,1
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+2]
      Gosub Explosion
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+1]
      o=35
      Gosub SetO
      Pause 300
      if b=54 | b=56 | b=57 | b=58 | b=69 | b=70 then %' crée bloc au dessus de l'eau : plouf !
        if !mute then Soundpool.play tmp,sndPlo,0.99,0.99,1,0,1
        Pause 300
      end if
    else %' ne peut pas créer de bloc sur ces tiles de paysages
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+2]
      if light then Gosub FiatLux else gr.render
      Pause 300
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+1]
      if light then Gosub FiatLux else gr.render
      Pause 300
    end if
  end if

elseif o=36 then %' Marteau
  if heroform=5 then
    if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
    Popup RPGLBL$[145],0,z,0
    Pause 2200
  elseif heroform<>1 then
    if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
    Popup RPGLBL$[146],0,z,0
    Pause 2200
  else
    x=herox + dx
    y=heroy + dy
    if DerDir>5 then
      x=x - (DerDir - 8) / 2
    else
      y=y + (DerDir - 1.5) / 1.5
    end if
    Gosub GetB
    Gosub GetO
    Gosub GetC
    if c>1 then %' coup de marteau sur un perso=interdit
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+2]
      gr.bitmap.draw tmpPtr,heroTile[heroform,17+Floor(DerDir/3)],Floor(nthor/2)*tilew,Floor(ntver/2)*tileh
      if light then Gosub FiatLux else gr.render
      Pause 300
      Popup RPGLBL$[147],0,z,0
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      Gosub InitEcranDeJeu
      if light then Gosub FiatLux else gr.render
      Pause 300
    elseif c=1 then %' coup de marteau sur la fiancée statufiée=la libère
      snd0=sndHro0
      snd1=sndHro1
      Gosub TapeAuMarteau
      c=2
      Gosub MovC
      Gosub Explosion
      if !mute then Soundpool.play tmp,sndMag,0.99,0.99,1,0,1
      Gosub InitEcranDeJeu
      if light then Gosub FiatLux else gr.render
      Pause 300
    elseif o=43 then %' coup de marteau sur la tombe
      snd0=sndHro0
      if objet[43]<6 then snd1=sndHro1 else snd1=snd0
      objet[43]=objet[43]+2 %' de 2 en 2 car si objet=1 il ne sera plus retourné par GetO !!
      Gosub TapeAuMarteau
      if objet[43]<6 then
        gr.show exploPtr %' choc sur l'objet
        if light then Gosub FiatLux else gr.render
        Pause 100
      else %' destruction au 3ème coup
        o=13
        Gosub SetO
        Gosub Explosion
        if perso[81]=8 then
          perso[81]=9 %' Fait apparaître Roberts près de sa tombe
          c=81
          destx=236
          desty=160
          Gosub MovC
          if !mute then Soundpool.play tmp,sndPRo,0.99,0.99,1,0,1
          Gosub Explosion
        end if
      end if
      Gosub InitEcranDeJeu
      if light then Gosub FiatLux else gr.render
      Pause 300
    elseif o=35 then %' coup de marteau sur un bloc de glace=l'explose
      snd0=sndHic0
      snd1=sndHic1
      Gosub TapeAuMarteau
      Gosub DelO
      Gosub Explosion
      Gosub InitEcranDeJeu
      if light then Gosub FiatLux else gr.render
      Pause 300
    elseif o=49 | o=50 then %' coup de marteau sur un levier d'aiguillage=le bascule
      if !mute then Soundpool.play tmp,sndSwo,0.99,0.99,1,0,1
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+2]
      gr.bitmap.draw tmpPtr,heroTile[heroform,17+Floor(DerDir/3)],Floor(nthor/2)*tilew,Floor(ntver/2)*tileh
      if o=49 then o=50 else o=49
      Gosub SetO
      Pause 300
      Gosub InitEcranDeJeu
      if light then Gosub FiatLux else gr.render
      Gosub EffetsBasculeObjetBinaire
    elseif o>=37 & o<=40 then %' coup de marteau sur un poussoir-ressort + bloc de glace=le libère
      snd0=sndHic0
      snd1=sndHic1
      Gosub TapeAuMarteau
      o=o+8
      Gosub SetO
      if !mute then Soundpool.play tmp,sndPRo,0.99,0.99,1,0,1
      Gosub Explosion
      Gosub InitEcranDeJeu
      if light then Gosub FiatLux else gr.render
      Gosub EffetsBasculeObjetBinaire
    elseif b=15 & perso[69]=45 then %' coup de marteau sur un mur neuf=le fissure
      snd0=sndHro0
      snd1=sndHro1
      Gosub TapeAuMarteau
      b=64
      Gosub SetB
      Gosub Explosion
      Gosub InitEcranDeJeu
      if light then Gosub FiatLux else gr.render
      Pause 300
      !'----------------------------------------------------------------------------
      !' Placer ici tous les événements liés à la vandalisation d'un mur au marteau
      !'----------------------------------------------------------------------------
      !'----------------------------------------------------------------------------
    elseif b=64 & perso[69]=45 then %' coup de marteau sur un mur fissuré=le détruit
      snd0=sndHro0
      snd1=sndHro1
      Gosub TapeAuMarteau
      b=26
      Gosub SetB
      s=13
      Gosub SetS
      Gosub Explosion
      Gosub InitEcranDeJeu
      if light then Gosub FiatLux else gr.render
      Pause 300
      !'----------------------------------------------------------------------------
      !' Placer ici les événements liés à la destruction d'un mur fissuré au marteau
      !'----------------------------------------------------------------------------
      if (destx<34 & desty<100) then %' destruction des murs des cités
        perso[79]=perso[79]+1
      elseif destx<15 & desty>118 & desty<130 then
        perso[79]=perso[79]+1
      elseif (destx>96 & destx<108 & desty>70 & desty<80) then
        perso[79]=perso[79]+1
      end if
      if perso[79]=158 then %' toutes les cités sont détruites
        gr.modify heroPtr,"bitmap",heroTile[heroform,15]
        if light then Gosub FiatLux else gr.render
        Popup RPGLBL$[148],0,z,0
        Pause 2200
        Popup RPGLBL$[149],0,z,0
        Pause 2200
        Popup RPGLBL$[150],0,z,0
        Pause 2200
        Gosub DebloqueSecretMusique
      end if
      !'----------------------------------------------------------------------------
    elseif b=27 then %' coup de marteau sur un éboulis de rochers=ouvre le passage
      snd0=sndHro0
      snd1=sndHro1
      Gosub TapeAuMarteau
      b=26
      Gosub SetB
      s=13
      Gosub SetS
      Gosub Explosion
      Gosub InitEcranDeJeu
      if light then Gosub FiatLux else gr.render
      Pause 300
      !'----------------------------------------------------------------------------
      !' Placer ici tous les événements liés à la destruction d'un rocher au marteau
      !'----------------------------------------------------------------------------
      if destx>132 & destx<141 & desty>218 & desty<228 then %' pyramides de l'ouest
        perso[69]=perso[69]+1 %' géant des montagnes de Zoraniscaeos
        if perso[69]=10 then
          Popup RPGLBL$[151],0,z,0
          Pause 2200
        elseif perso[69]=20 then
          Popup RPGLBL$[152],0,z,0
          Pause 2200
        elseif perso[69]=30 then
          Popup RPGLBL$[153],0,z,0
          Pause 2200
        elseif perso[69]=44 then
          gr.modify heroPtr,"bitmap",heroTile[heroform,15]
          if light then Gosub FiatLux else gr.render
          Popup RPGLBL$[154],0,z,0
          Pause 2200
          Popup RPGLBL$[155],0,z,0
          Pause 2200
          Gosub DebloqueSecretMusique
        end if
      end if
      !'----------------------------------------------------------------------------
    else %' coup de marteau sur n'importe quoi d'autre=coup dans le vide
      if !mute then Soundpool.play tmp,sndHno,0.99,0.99,1,0,1
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+2]
      gr.bitmap.draw tmpPtr,heroTile[heroform,17+Floor(DerDir/3)],Floor(nthor/2)*tilew,Floor(ntver/2)*tileh
      if light then Gosub FiatLux else gr.render
      Pause 300
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+1]
      gr.hide tmpPtr
      if light then Gosub FiatLux else gr.render
      Pause 300
      if !mute then Soundpool.play tmp,sndHno,0.99,0.99,1,0,1
      gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+2]
      gr.show tmpPtr
      if light then Gosub FiatLux else gr.render
      Pause 300
      Gosub InitEcranDeJeu
      if light then Gosub FiatLux else gr.render
      Pause 300
    end if
  end if

end if
!' [Fin du Script 2] FinScript2
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
FinScript2:
Goto BouclePrincipale

EffetsBasculeObjetBinaire:
if !oIndx then Return %' gestion objet multiple en erreur
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
!' [Sous-script 2b] Le héros vient de BASCULER un OBJET MULTIPLE (action utilisateur du Script 2)
!' Ex1: il vient d'ENFONCER ou bien de LIBERER un POUSSOIR-RESSORT grâce à un bloc / au marteau
!' Ex2: il vient de BASCULER un LEVIER D'AIGUILLAGE à droite ou à gauche par un coup de marteau
!'      o=type de l'objet multiple basculé ; oIndx=instance concernée (index) de l'objet multiple
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
if (o>=37 & o<=40) | (o>=45 & o<=48) then %' POUSSOIR-RESSORT
  if !objet[30] then %' dans pyramide de la sorcière
    if objet[IOM(133,238)] & objet[IOM(133,246)] then
      !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      objet[30]=2 %' l'épée légendaire apparait !
      destx=137
      desty=242
      o=30
      Gosub SetO
      !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      Gosub Explosion
      Gosub DebloqueSecretMusique
    end if
  elseif !objet[4] then  %' dans pyramide au sud
    if objet[IOM(233,187)] & objet[IOM(237,191)] & objet[IOM(235,195)] & !objet[IOM(230,191)] then
      !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      objet[4]=2 %' la croix de l'eau apparait !
      destx=234
      desty=191
      o=4
      Gosub SetO
      !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      Gosub Explosion
      Gosub DebloqueSecretMusique
    end if
  elseif !objet[26] then %' banquise sud
    if objet[IOM(160,136)] & !objet[IOM(156,135)] & !objet[IOM(158,136)] then
      !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      objet[26]=2  %' la plateforme pour accéder au miroir des glaces apparait
      destx=198
      desty=149
      b=57
      Gosub SetB
      s=18
      Gosub SetS
      !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      Gosub Explosion
      Gosub DebloqueSecretMusique
    end if
  end if
elseif o>=49 then %' LEVIER D'AIGUILLAGE
  if oIndx=118 | oIndx=119 | oIndx=120 then %' salle du paysage (pyramide centrale)
    array.shuffle pyrPaysageOrder[]
    for t=1 to 27
      destx=pyrPaysageX[pyrPaysageOrder[t]]
      desty=pyrPaysageY[pyrPaysageOrder[t]]
      if t=1 then
        Pause 200
        !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if objet[118]=0 & objet[119]=0 & objet[120]=0 then
          b=56 %' mer
          s=15
        elseif objet[118]=0 & objet[119]=0 & objet[120]=1 then
          b=1 %' prairie desséchée
          s=13
        elseif objet[118]=0 & objet[119]=1 & objet[120]=0 then
          b=59 %' banquise
          s=16
        elseif objet[118]=1 & objet[119]=0 & objet[120]=0 then
          b=23 %' forêt
          s=13
        elseif objet[118]=1 & objet[119]=0 & objet[120]=1 then
          b=20 %' lave
          s=13
        elseif objet[118]=1 & objet[119]=1 & objet[120]=0 then
          b=33 %' marais
          s=13
        elseif objet[118]=1 & objet[119]=1 & objet[120]=1 then
          b=45 %' vide stellaire
          s=14
        end if
        !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if !mute then Soundpool.play tmp,sndMag,0.99,0.99,1,0,1
      end if
      !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      if objet[118]=0 & objet[119]=1 & objet[120]=1 then
        b=37+Mod(destx,2)+10*Mod(desty,2) %' dunes de sable
        s=14
      end if
      Gosub SetB
      Gosub SetS
      !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    next t
  end if
end if
if objet[IOM(8,125)] & objet[IOM(108,186)] & objet[IOM(157,42)] & objet[IOM(168,135)] then
  if !objet[IOM(213,22)] & !objet[IOM(184,67)] & !objet[IOM(239,27)] then
    if objet[IOM(161,168)] & objet[IOM(146,184)] & objet[IOM(169,206)] then
      if !objet[IOM(177,167)] & !objet[IOM(182,182)] & !objet[IOM(186,195)] then
        if objet[IOM(164,166)] & objet[IOM(167,177)] & objet[IOM(160,182)] & objet[IOM(173,181)] & objet[IOM(174,195)] & objet[IOM(160,198)] then
          if !objet[IOM(167,169)] & !objet[IOM(182,170)] & !objet[IOM(183,171)] & !objet[IOM(191,184)] & !objet[IOM(150,188)] & !objet[IOM(181,199)] then
            gr.modify heroPtr,"bitmap",heroTile[1,15]
            if light then Gosub FiatLux else gr.render
            !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            o=21 %' ouverture du passage vers l'île-dragon depuis le labyrinthe
            destx=169
            desty=184
            Gosub SetO
            !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Gosub Explosion
            Gosub DebloqueSecretMusique
          end if
        end if
      end if
    end if
  end if
end if
!' [Fin du sous-script 2b]
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
Return

MusiqueChange:
destx=herox - 1
desty=heroy - 1
!'---------------------------------------------------------------------------------------------------
!' Sous-routine de gestion de la MUSIQUE DE FOND en fonction de la zone sur la carte
!'      destx, desty=coordonnées de destination du héros
!'---------------------------------------------------------------------------------------------------
if heroform=5 then
  music$="EnPleineMer.mid"
elseif heroform=4 then
  music$="Aigle.mp3"
elseif heroform=3 then
  music$="poisson.mid"
elseif destx>176 & destx<214 & desty>10 & desty<39 then
  music$="IleMarais.mid"
elseif perso[40] & destx>181 & destx<215 & desty>61 & desty<88 then %' île-sable
  music$="IleDesert.mid"
elseif destx>216 & desty>185 & desty<218 then %' pyramide paysages changeants
  if perso[81] then music$="" else music$="IleDesert.mid" %' fantôme de Roberts
elseif destx>130 & destx<164 & desty>214 then %' pyramide
  music$="IleDesert.mid"
elseif destx>143 & destx<194 & desty>158 & desty<209 then %' Labyrinthe
  music$="Labyrinthe.mid"
elseif destx<55 & desty>232 then %' île-dragon
  if perso[28] then music$="tornade.mp3" else music$="IleDragon.mid"
elseif destx>85 & destx<165 & desty>29 & desty<89 then %' Zoraniscaeos
  music$="Zoraniscaeos.mid"
elseif destx>233 | (destx>85 & desty>102 & desty<166) then %' banquise
  music$="Banquise.mid"
elseif destx>180 & desty>228 then %' forêt du bord du monde
  music$="foret.mid"
else %' partout ailleurs : musique par défaut = continent de l'ouest
  music$="ContinentOuest.mid"
end if
if !mute then
  audio.stop
  if musPtr then audio.release musPtr
  if music$<>"" then
    audio.load musPtr, music$
    audio.play musPtr
    audio.loop
  end if
end if
Return
!'---------------------------------------------------------------------------------------------------

HurtMePlenty:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine de dommages du héros en fonction du tile où il se tient
!'      b=tile de décor actuel ; s=solidité actuelle
!'---------------------------------------------------------------------------------------------------
if b=44 | b=45 | b=51 | b=52 | b=61 | b=62 then %' vide stellaire
  pdv=0
  gr.modify pdvPtr, "right", tilew*(1.1+3*pdv/100)
  gr.hide heroPtr
  if !mute then Soundpool.play tmp,sndFal,0.99,0.99,1,0,1
  Gosub Explosion
  e$=RPGLBL$[156]
  Goto TheEnd
end if
if heroform >=4 then Return %' en aigle ou en bateau : non impactés
if (heroform=2 & s=20) | b=65 | b=20 then %' loup sur tile "hot", ou toute forme sur pics ou lave
  if !mute then Soundpool.play tmp,sndHur,0.99,0.99,1,0,1
  gr.modify heroPtr, "bitmap", heroTile[heroform,14]
  pdv=pdv-50
  if pdv<0 then pdv=0
  gr.modify pdvPtr, "right", tilew*(1.1+3*pdv/100)
  if light then Gosub FiatLux else gr.render
  Pause 1000
  gr.modify heroPtr, "bitmap", heroTile[heroform,DerDir+1]
  if light then Gosub FiatLux else gr.render
  if pdv=0 then
    if heroform=2 & s=20 then
      e$=RPGLBL$[157]
    else if b=65 then
      e$=RPGLBL$[158]
    else if b=20 then
      e$=RPGLBL$[159]
    end if
    Goto TheEnd
  end if
  hurtTmr=Clock()
else
  hurtTmr=0
end if
Return
!'---------------------------------------------------------------------------------------------------

TapeAuMarteau:
!'---------------------------------------------------------------------------------------------------
!' Animation du héros tapant au marteau sur le tile aux coordonnées destx,desty
!'    [Paramètre entrant] snd0=son à jouer à chaque coup de marteau intermédiaire
!'    [Paramètre entrant] snd1=son à jouer lors du coup de marteau final (qui explose l'objet)
!'---------------------------------------------------------------------------------------------------
gr.bitmap.draw tmpPtr,heroTile[heroform,17+Floor(DerDir/3)],Floor(nthor/2)*tilew,Floor(ntver/2)*tileh
gr.modify exploPtr, "x", (Floor(nthor/2)+destx-herox+1)*tilew
gr.modify exploPtr, "y", (Floor(ntver/2)+desty-heroy+1)*tileh
gr.modify exploPtr, "bitmap", exploBmp[1]
for w=1 to 5
  if w=1 | w=3 then %' le héros abaisse le marteau
    if !mute then Soundpool.play tmp,snd0,0.99,0.99,1,0,1
    gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+2]
    gr.show tmpPtr
    if light then Gosub FiatLux else gr.render
    Pause 50
    gr.show exploPtr %' choc sur l'objet
    if light then Gosub FiatLux else gr.render
    Pause 50
    gr.hide exploPtr
    if light then Gosub FiatLux else gr.render
    Pause 200
  else if w=2 | w=4 then %' le héros relève le marteau
    gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+1]
    gr.hide tmpPtr
    if light then Gosub FiatLux else gr.render
    Pause 300
  else if w=5 then %' coup de marteau final
    if !mute then Soundpool.play tmp,snd1,0.99,0.99,1,0,1
    gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+2]
    gr.show tmpPtr
    if light then Gosub FiatLux else gr.render
  end if
next i
Return
!'---------------------------------------------------------------------------------------------------

Explosion:
!'---------------------------------------------------------------------------------------------------
!' Animation d'une explosion sur le tile aux coordonnées destx,desty
!'---------------------------------------------------------------------------------------------------
z=4*tileh*scale_h
x=destx-herox+1
y=desty-heroy+1
if abs(x)>Floor(nthor/2) | abs(y)>Floor(ntver/2) then %' trop loin pour l'effet visuel : notif !
  e$=RPGLBL$[160]
  if abs(y)>Floor(ntver/2) then
    if y<0 then
      e$=e$+RPGLBL$[161]
    else
      e$=e$+RPGLBL$[162]
    end if
    if abs(x)>Floor(nthor/2) then
      if x<0 then
        e$=e$+RPGLBL$[163]
      else
        e$=e$+RPGLBL$[164]
      end if
    end if
  else if abs(x)>Floor(nthor/2) then
    if x<0 then
      e$=e$+RPGLBL$[165]
    else
      e$=e$+RPGLBL$[166]
    end if
  end if
  Popup e$,0,z,0
  Return
end if
!' Sinon, effet visuel :
Pause 40
gr.modify exploPtr, "x", (Floor(nthor/2)+destx-herox+1)*tilew
gr.modify exploPtr, "y", (Floor(ntver/2)+desty-heroy+1)*tileh
gr.show exploPtr
gr.modify exploPtr, "bitmap", exploBmp[2]
if light then Gosub FiatLux else gr.render
Pause 40
gr.modify exploPtr, "bitmap", exploBmp[3]
if light then Gosub FiatLux else gr.render
Pause 40
gr.modify exploPtr, "bitmap", exploBmp[4]
if light then Gosub FiatLux else gr.render
Pause 40
gr.hide exploPtr
if light then Gosub FiatLux else gr.render
Pause 40
Return
!'---------------------------------------------------------------------------------------------------

FromDuskTillDawn:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine de définition de l'OBSCURITE AMBIANTE en fonction de la zone sur la carte
!'      destx, desty=coordonnées de destination du héros
!'---------------------------------------------------------------------------------------------------
if ToujoursLumineux then
  dknsTgt=0
  Return
end if
if desty<175 & destx>=215 then %' extrème-Est sur la carte
  dknsTgt=16*(destx-215)
  if dknsTgt>255 then dknsTgt=255
elseif destx<55 & desty>232 then %' île-dragon
  if perso[28] then dknsTgt=0 else dknsTgt=255-7*tileh %' faible obscurité si dragon présent
elseif destx>218 & desty>200 & desty<220 then %' intérieur pyramide des paysages qui changent
  dknsTgt=255-7*tileh %' faible obscurité
elseif destx>218 & desty>180 & desty<200 then %' intérieur autre pyramide
  dknsTgt=255-3*tileh %' forte obscurité
elseif destx>128 & destx<168 & desty>212 then %' intérieur autres pyramides
  dknsTgt=255-3*tileh %' forte obscurité
else
  dknsTgt=0 %' obscurité mini, il fait jour (typiquement : à l'extérieur)
end if
Return
!'---------------------------------------------------------------------------------------------------

FiatLux:
!'---------------------------------------------------------------------------------------------------
!' Changement de luminosité (bougie en cours d'extinction)
!'---------------------------------------------------------------------------------------------------
if dkns then %' sauf en plein jour (inutile)
  light=255-16*(Clock()-lightTmr)/1000+64
  if light>255 then light=255
  if light<0 then light=0
  gr.modify dknsPtr,"alpha",dkns*(255-light)/255
  gr.modify lightPtr,"alpha",light*dkns/255
  gr.render
end if
Return
!'---------------------------------------------------------------------------------------------------

MettreEnleverLeHeaume:
!'---------------------------------------------------------------------------------------------------
!' Le héros humain met ou enlève le heaume étincelant
!'---------------------------------------------------------------------------------------------------
HerosEnHeaume=1-HerosEnHeaume
array.length l, avecHeaumeBmp[]
if HerosEnHeaume then 
  for i=1 to l
    heroTile[1,i]=avecHeaumeBmp[i]
  next i
else
  for i=1 to l
    heroTile[1,i]=sansHeaumeBmp[i]
  next i
end if
gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+1]
if light then Gosub FiatLux else gr.render
Return
!'---------------------------------------------------------------------------------------------------

DisplayWorldMap:
!'---------------------------------------------------------------------------------------------------
!' Affichage de la carte du monde + position du héros et du bateau
!'---------------------------------------------------------------------------------------------------
gr.color 255,0,0,0,1
if perso[40] & !croixSurLaCarte then %' île-sable
  gr.bitmap.drawinto.start worldMapBmp
  gr.bitmap.draw i,sandBmp,254,76
  gr.bitmap.drawinto.end
  croixSurLaCarte=1
end if
if objet[8] & !ReliqueUnSurLaCarte then %' Conque du vent
  gr.bitmap.drawinto.start worldMapBmp
  gr.bitmap.draw i,objTile[8],211,215
  gr.bitmap.drawinto.end
  ReliqueUnSurLaCarte=1
end if
if objet[4] & !ReliqueDeuxSurLaCarte then %' Croix de l'eau
  gr.bitmap.drawinto.start worldMapBmp
  gr.bitmap.draw i,objTile[4],235,215
  gr.bitmap.drawinto.end
  ReliqueDeuxSurLaCarte=1
end if
if objet[26] & !ReliqueTroisSurLaCarte then %' Miroir des glaces
  gr.bitmap.drawinto.start worldMapBmp
  gr.bitmap.draw i,objTile[26],259,215
  gr.bitmap.drawinto.end
  ReliqueTroisSurLaCarte=1
end if
if objet[34] & !ReliqueQuatreSurLaCarte then %' Crâne de la désolation
  gr.bitmap.drawinto.start worldMapBmp
  gr.bitmap.draw i,objTile[34],283,213
  gr.bitmap.drawinto.end
  ReliqueQuatreSurLaCarte=1
end if
gr.bitmap.create tmpBmp, nthor*tilew, ntver*tileh
gr.bitmap.drawinto.start tmpBmp
gr.bitmap.draw tmpPtr, worldMapBmp, 0, 0
if heroform=5 then
  if heroy<=210 then gr.bitmap.draw tmpPtr, miniBatBmp, herox*tilew/15 +41 -7, heroy*tileh/25.45 +40 -4
else
  if baty<=210 then gr.bitmap.draw tmpPtr, miniBatBmp, batx*tilew/15 +41 -7, baty*tileh/25.45 +40 -4
end if
if heroy<=210 then gr.bitmap.draw tmpPtr, miniHeroBmp, herox*tilew/15 +41 -4, heroy*tileh/25.45 +40 -4
gr.bitmap.drawinto.end
gr.cls %' Call gClear()
gr.bitmap.draw bgnPtr,bgnBmp,-tilew,-tileh
gr.rect dknsPtr, 0, 0, di_w, di_h
gr.modify dknsPtr,"alpha",dkns*(255-light)/255
gr.bitmap.draw lightPtr,lightBmp,0,0
gr.modify lightPtr,"alpha",light*dkns/255
if heroform=5 then x=-1 else x=0
gr.bitmap.draw heroPtr, heroTile[heroform,DerDir+1], (Floor(nthor/2)+x)*tilew, Floor(ntver/2)*tileh
if GhostMode then gr.modify heroPtr, "alpha", 128
if tricheZeroEnnemi & heroform<>5 then gr.bitmap.draw tmp,shieldBmp,(Floor(nthor/2)+x)*tilew,Floor(ntver/2)*tileh
for i=1 to 3
  vibrate VibrCrt[], -1
next i
gr.bitmap.draw tmpPtr, tmpBmp, 0, di_h
for y=di_h to 0 step -tileh
  gr.modify tmpPtr, "y", y
  if light then Gosub FiatLux else gr.render
  Pause 2
next y
do
  gr.touch tmp, x, y
until !tmp
do
  Gosub TestJeuEnArrierePlan
  gr.bounded.touch tmp,0,0,real_w,real_h  %' Toucher la carte=retour au jeu
  inkey$ k$
  if k$<>"@" then tmp=1
  if tmp then
    vibrate VibrCrt[], -1
    for y=0 to di_h step tileh
      gr.modify tmpPtr, "y", y
      if light then Gosub FiatLux else gr.render
      Pause 2
    next y
    gr.bitmap.delete tmpBmp
    Gosub InitEcranDeJeu
    if light then Gosub FiatLux else gr.render
    Goto BouclePrincipale
  end if
until 0
!'---------------------------------------------------------------------------------------------------

DisplayParchment:
!'---------------------------------------------------------------------------------------------------
!' Affichage du parchemin des runes élémentales
!'---------------------------------------------------------------------------------------------------
gr.color 255,0,0,0,1
gr.cls %' Call gClear()
gr.bitmap.draw bgnPtr,bgnBmp,-tilew,-tileh
gr.rect dknsPtr, 0, 0, di_w, di_h
gr.modify dknsPtr,"alpha",dkns*(255-light)/255
gr.bitmap.draw lightPtr,lightBmp,0,0
gr.modify lightPtr,"alpha",light*dkns/255
if heroform=5 then x=-1 else x=0
gr.bitmap.draw heroPtr, heroTile[heroform,DerDir+1], (Floor(nthor/2)+x)*tilew, Floor(ntver/2)*tileh
if GhostMode then gr.modify heroPtr, "alpha", 128
if tricheZeroEnnemi & heroform<>5 then gr.bitmap.draw tmp,shieldBmp,(Floor(nthor/2)+x)*tilew,Floor(ntver/2)*tileh
for i=1 to 3
  vibrate VibrCrt[], -1
next i
gr.bitmap.draw tmpPtr, parchmentBmp, -di_w, 0
for x=-di_w to 0 step tilew
  gr.modify tmpPtr, "x", x
  if light then Gosub FiatLux else gr.render
  Pause 2
next x
do
  gr.touch tmp, x, y
until !tmp
do
  Gosub TestJeuEnArrierePlan
  gr.bounded.touch tmp,0,0,real_w,real_h  %' Toucher la carte=retour au jeu
  inkey$ k$
  if k$<>"@" then tmp=1
  if tmp then
    vibrate VibrCrt[], -1
    for x=0 to -di_w step -tilew
      gr.modify tmpPtr, "x", x
      if light then Gosub FiatLux else gr.render
      Pause 2
    next x
    Gosub InitEcranDeJeu
    if light then Gosub FiatLux else gr.render
    Goto BouclePrincipale
  end if
until 0
!'---------------------------------------------------------------------------------------------------

DisplayMazeMap:
!'---------------------------------------------------------------------------------------------------
!' Affichage de la carte du labyrinthe + position du héros dans le labyrinthe
!'---------------------------------------------------------------------------------------------------
gr.color 255,0,0,0,1
gr.bitmap.create tmpBmp, nthor*tilew, ntver*tileh
gr.bitmap.drawinto.start tmpBmp
gr.bitmap.draw tmpPtr, mazeMapBmp, 0, 0
if herox>143 & herox<194 & heroy>158 & heroy<209 then
  gr.bitmap.draw tmpPtr,miniHeroBmp,(herox-143)*3.42+92,(heroy-158)*2.6666+59
end if
if perso[7]>=3 then %' sorcière de l'ouest
  Call SetRGB(255,"8E8E8D",1)
  gr.text.align 2
  gr.text.size 8
  gr.text.draw nul,di_w/2+20,206,RPGLBL$[167]
  gr.text.draw nul,di_w/2+20,214,RPGLBL$[168]
  gr.text.draw nul,di_w/2-20,222,RPGLBL$[169]
  gr.text.draw nul,di_w/2-20,230,RPGLBL$[170]
end if
gr.bitmap.drawinto.end
gr.cls %' Call gClear()
gr.bitmap.draw bgnPtr,bgnBmp,-tilew,-tileh
gr.rect dknsPtr, 0, 0, di_w, di_h
gr.modify dknsPtr,"alpha",dkns*(255-light)/255
gr.bitmap.draw lightPtr,lightBmp,0,0
gr.modify lightPtr,"alpha",light*dkns/255
if heroform=5 then x=-1 else x=0
gr.bitmap.draw heroPtr, heroTile[heroform,DerDir+1], (Floor(nthor/2)+x)*tilew, Floor(ntver/2)*tileh
if GhostMode then gr.modify heroPtr, "alpha", 128
if tricheZeroEnnemi & heroform<>5 then gr.bitmap.draw tmp,shieldBmp,(Floor(nthor/2)+x)*tilew,Floor(ntver/2)*tileh
for i=1 to 3
  vibrate VibrCrt[], -1
next i
gr.bitmap.draw tmpPtr, tmpBmp, 0, di_h
for y=di_h to 0 step -tileh
  gr.modify tmpPtr, "y", y
  if light then Gosub FiatLux else gr.render
  Pause 2
next y
do
  gr.touch tmp, x, y
until !tmp
do
  Gosub TestJeuEnArrierePlan
  gr.bounded.touch tmp,0,0,real_w,real_h  %' Toucher la carte=retour au jeu
  inkey$ k$
  if k$<>"@" then tmp=1
  if tmp then
    vibrate VibrCrt[], -1
    for y=0 to di_h step tileh
      gr.modify tmpPtr, "y", y
      if light then Gosub FiatLux else gr.render
      Pause 2
    next y
    gr.bitmap.delete tmpBmp
    Gosub InitEcranDeJeu
    if light then Gosub FiatLux else gr.render
    Goto BouclePrincipale
  end if
until 0
!'---------------------------------------------------------------------------------------------------

Settings:
!'---------------------------------------------------------------------------------------------------
!' Barre des paramètres en bas à droite de l'écran
!'---------------------------------------------------------------------------------------------------
gr.cls %' Call gClear()
gr.color 255,0,0,0,1
gr.bitmap.draw bgnPtr,bgnBmp,-tilew,-tileh
gr.rect dknsPtr, 0, 0, di_w, di_h
gr.modify dknsPtr,"alpha",dkns*(255-light)/255
gr.bitmap.draw lightPtr,lightBmp,0,0
gr.modify lightPtr,"alpha",light*dkns/255
if heroform=5 then x=-1 else x=0
gr.bitmap.draw heroPtr, heroTile[heroform,DerDir+1], (Floor(nthor/2)+x)*tilew, Floor(ntver/2)*tileh
if GhostMode then gr.modify heroPtr, "alpha", 128
if tricheZeroEnnemi & heroform<>5 then gr.bitmap.draw tmp,shieldBmp,(Floor(nthor/2)+x)*tilew,Floor(ntver/2)*tileh
gr.rect w,0,0,di_w,di_h
for i=1 to 3
  vibrate VibrCrt[], -1
next i
gr.bitmap.draw tmpPtr, menuBmp, di_w-36, di_h
for y=di_h to di_h-216 step -24
  gr.modify tmpPtr, "y", y
  gr.modify w, "alpha", (di_h-y)/1.5
  if light then Gosub FiatLux else gr.render
  Pause 2
next y
gr.color 155,255,255,255,1
gr.bitmap.draw paramPtr, paramBmp, di_w-36, di_h-36
if light then Gosub FiatLux else gr.render
do
  gr.touch tmp, x, y
until !tmp
z=3*tileh*scale_h

do
  Gosub TestJeuEnArrierePlan

  !' Icône Exit
  gr.bounded.touch i,(di_w-36)*scale_w,(di_h-45*4-36)*scale_h,real_w,(di_h-45*4)*scale_h
  if i then
    vibrate VibrCrt[], -1
    if !mute then audio.stop
    gr.cls %' Call gClear()
    light=0 %' annule la baisse de luminosité (bougie en train de s'éteindre) sinon il y a 1 BUG
    gr.render
    Popup RPGLBL$[171],0,0,0
    if pdv>0 then Gosub SauverPartie
    if needSaveMAPs then Gosub SauverCalques
    Pause 500
    Exit
  end if

  !' Icône Back ou appui touche Back : retour au menu de titre
  gr.bounded.touch i,(di_w-36)*scale_w,(di_h-45*3-36)*scale_h,real_w,(di_h-45*3)*scale_h
  if i | BackKey then
    vibrate VibrCrt[], -1
    if !mute then audio.stop
    if pdv>0 then Gosub SauverPartie
    Gosub FadeOut
    di_w=640
    di_h=480
    scale_w=real_w/di_w
    scale_h=real_h/di_h
    gr.scale scale_w, scale_h
    Goto MenuPrincipal
  end if

  !' Icône haut-parleur (ou appui touches volumes)
  gr.bounded.touch i,(di_w-36)*scale_w,(di_h-45*2-36)*scale_h,real_w,(di_h-45*2)*scale_h
  inkey$ k$
  if k$="key 24" | k$="key 25" then i=1 %' Touches Vol+/Vol-
  if i then
    vibrate VibrCrt[], -1
    mute=1-mute
    Gosub SavePrefs
    if mute then
      audio.stop
      if musPtr then audio.release musPtr
    else
      if music$<>"" then
        audio.load musPtr, music$
        audio.stop
        audio.play musPtr
        audio.loop
      end if
    end if
    Gosub InitEcranDeJeu
    if light then Gosub FiatLux else gr.render
    if mute then Popup RPGLBL$[172],0,z,0 else Popup RPGLBL$[173],0,z,0
    Goto BouclePrincipale
  end if

  !' Icône sauver le jeu
  gr.bounded.touch i,(di_w-36)*scale_w,(di_h-45-36)*scale_h,real_w,(di_h-45)*scale_h
  if i then
    vibrate VibrCrt[], -1
    Gosub InitEcranDeJeu
    gr.hide paramPtr
    Gosub SauverPartie
    Gosub SauverCalques
    Gosub InitEcranDeJeu
    if light then Gosub FiatLux else gr.render
    Popup RPGLBL$[174],0,z,0
    Goto BouclePrincipale
  end if

  !' Bouton Settings ou appui touche Menu = retour au jeu
  gr.bounded.touch i,(di_w-36)*scale_w,(di_h-36)*scale_h,real_w,real_h
  inkey$ k$
  if k$="key 82" then i=1 %' Touche Menu
  if i then
    vibrate VibrCrt[], -1
    gr.hide paramPtr
    for y=di_h-216 to di_h step 24
      gr.modify tmpPtr, "y", y
      gr.modify w, "alpha", (di_h-y)/1.5
      if light then Gosub FiatLux else gr.render
      Pause 2
    next y
    Gosub InitEcranDeJeu
    if light then Gosub FiatLux else gr.render
    do
      gr.touch tmp, x, y
    until !tmp
    Goto BouclePrincipale
  end if

  !' Zone à gauche de la barre de menu = retour au jeu
  gr.bounded.touch i,0,0,(di_w-36)*scale_w,real_h
  if i then
    vibrate VibrCrt[], -1
    Gosub InitEcranDeJeu
    if light then Gosub FiatLux else gr.render
    Pause 200
    Goto BouclePrincipale
  end if

until 0
!'---------------------------------------------------------------------------------------------------

MenuTriche:
!'---------------------------------------------------------------------------------------------------
!' Menu de triche (activé en touchant le héros + la touche Haut)
!'---------------------------------------------------------------------------------------------------
if !mute then audio.pause
gr.cls %' Call gClear()
gr.color 255,0,0,0,1
gr.bitmap.draw bgnPtr,bgnBmp,-tilew,-tileh
gr.rect dknsPtr, 0, 0, di_w, di_h
gr.modify dknsPtr,"alpha",dkns*(255-light)/255
gr.bitmap.draw lightPtr,lightBmp,0,0
gr.modify lightPtr,"alpha",light*dkns/255
if heroform=5 then x=-1 else x=0
gr.bitmap.draw heroPtr, heroTile[heroform,DerDir+1], (Floor(nthor/2)+x)*tilew, Floor(ntver/2)*tileh
if GhostMode then gr.modify heroPtr, "alpha", 128
if tricheZeroEnnemi & heroform<>5 then gr.bitmap.draw tmp,shieldBmp,(Floor(nthor/2)+x)*tilew,Floor(ntver/2)*tileh
for i=1 to 3
  vibrate VibrCrt[], -1
next i
gr.bitmap.scale tmpBmp, exitBmp, 36, 36, 0
gr.bitmap.draw t1, tmpBmp, di_w-36, di_h-36
gr.color 255,255,0,0,1
gr.text.size 30
gr.text.draw t2, di_w-7, di_h-6, "!"
gr.bitmap.draw tmpPtr, cheatBmp, 0, di_h
for y=di_h to 12 step -6
  gr.modify tmpPtr, "y", y
  if light then Gosub FiatLux else gr.render
next y
do
  gr.touch tmp, x, y
until !tmp
z=3*tileh*scale_h

do
  Gosub TestJeuEnArrierePlan

  !' Icône santé max
  gr.bounded.touch i,13*scale_w,12*scale_h,48*scale_w,48*scale_h
  if i then
    vibrate VibrCrt[], -1
    gr.bitmap.delete tmpBmp
    Gosub InitEcranDeJeu
    if light then Gosub FiatLux else gr.render
    for i=1 to 10
      destx=herox+2*(Floor(2*Rnd())-0.5)*Floor(nthor*Rnd()/2)
      desty=heroy+2*(Floor(2*Rnd())-0.5)*Floor(ntver*Rnd()/2)
      Gosub DeterreUnCoeur
    next i
    Popup RPGLBL$[175],0,z,0
    if !mute & music$<>"" then audio.play musPtr
    cheatTmr=Clock() %' si besoin d'enchaîner un autre code triche
    Goto BouclePrincipale
  end if

  !' Icône argent infini
  gr.bounded.touch i,58*scale_w,12*scale_h,93*scale_w,48*scale_h
  if i then
    vibrate VibrCrt[], -1
    gr.bitmap.delete tmpBmp
    Gosub InitEcranDeJeu
    if light then Gosub FiatLux else gr.render
    Pause 200
    if !mute then Soundpool.play tmp,sndUsd,0.50,0.50,1,0,1
    usd=8888
    Gosub RefreshUsd
    Popup RPGLBL$[176],0,z,0
    if !mute & music$<>"" then audio.play musPtr
    cheatTmr=Clock() %' si besoin d'enchaîner un autre code triche
    Goto BouclePrincipale
  end if

  !' Icône pas d'ennemi
  gr.bounded.touch i,103*scale_w,12*scale_h,138*scale_w,48*scale_h
  if i then
    tricheZeroEnnemi=1-tricheZeroEnnemi
    vibrate VibrCrt[], -1
    gr.bitmap.delete tmpBmp
    Gosub InitEcranDeJeu
    if light then Gosub FiatLux else gr.render
    Pause 200
    if tricheZeroEnnemi then
      Popup RPGLBL$[177],0,z,0
    else
      Popup RPGLBL$[178],0,z,0
    end if
    if !mute then
      if tricheZeroEnnemi then
        audio.load snd, "Victory.mp3"
        if musPtr then audio.release musPtr
        audio.stop
        audio.play snd
        do
          Audio.isdone i
        until i
        audio.stop
        audio.release snd
        if music$<>"" then audio.load musPtr, music$
      else
        Soundpool.play tmp,sndHur,0.50,0.50,1,0,1
        Pause 1000
      end if
    else %' mute
      for i=1 to 3
        vibrate VibrCrt[], -1
        Pause 150
      next i
    end if
    if !mute & music$<>"" then
      audio.play musPtr
      audio.loop
    end if
    cheatTmr=Clock() %' si besoin d'enchaîner un autre code triche
    Goto BouclePrincipale
  end if

  !' Icône restaurer le paysage
  gr.bounded.touch i,148*scale_w,12*scale_h,183*scale_w,48*scale_h
  if i then
    vibrate VibrCrt[], -1
    gr.hide tmpPtr
    gr.hide t1
    gr.hide t2
    gr.bitmap.draw b3,heroTile[4,7],0,0
    RefreshOff=1
    for desty=heroy-Floor(ntver/2)-1 to heroy+Floor(ntver/2)-1
      for destx=herox-Floor(nthor/2)-1 to herox+Floor(nthor/2)-1
        gr.modify b3,"x",(destx-herox+Floor(nthor/2)+2)*tilew
        gr.modify b3,"y",(desty-heroy+Floor(ntver/2)+1)*tileh
        gr.modify b3,"bitmap",heroTile[4,7+Mod(destx,3)]
        Gosub RazB
        Gosub RazS
        Gosub RazO
        Gosub AfficherDecor
        if light then Gosub FiatLux else gr.render
      next destx
    next desty
    RefreshOff=0
    gr.bitmap.delete tmpBmp
    Gosub InitEcranDeJeu
    if light then Gosub FiatLux else gr.render
    if !mute & music$<>"" then audio.play musPtr
    cheatTmr=Clock() %' si besoin d'enchaîner un autre code triche
    Goto BouclePrincipale
  end if

  !' Icône tornade de téléportation
  gr.bounded.touch i,193*scale_w,12*scale_h,228*scale_w,48*scale_h
  if i then
    gr.bitmap.delete tmpBmp
    gr.color 255,0,0,0,1
    gr.cls %' Call gClear()
    gr.bitmap.draw bgnPtr,bgnBmp,-tilew,-tileh
    gr.rect dknsPtr, 0, 0, di_w, di_h
    gr.modify dknsPtr,"alpha",dkns*(255-light)/255
    gr.bitmap.draw lightPtr,lightBmp,0,0
    gr.modify lightPtr,"alpha",light*dkns/255
    if heroform=5 then x=-1 else x=0
    gr.bitmap.draw heroPtr, heroTile[heroform,DerDir+1], (Floor(nthor/2)+x)*tilew, Floor(ntver/2)*tileh
    if GhostMode then gr.modify heroPtr, "alpha", 128
    if tricheZeroEnnemi & heroform<>5 then gr.bitmap.draw tmp,shieldBmp,(Floor(nthor/2)+x)*tilew,Floor(ntver/2)*tileh
    gr.bitmap.draw tmpPtr, worldMapBmp, 0, 0
    if heroy<=210 then gr.bitmap.draw tmpPtr, miniHeroBmp, herox*tilew/15 +41 -4, heroy*tileh/25.45 +40 -4
    if light then Gosub FiatLux else gr.render
    vibrate VibrCrt[], -1
    do
      gr.touch tmp, x, y
    until !tmp
    do
      gr.touch tmp, x, y
    until tmp
    x=x/scale_w
    y=y/scale_h
    vibrate VibrCrt[], -1
    if x>=40 & y>=40 & x<=328 & y<=190 then
      destx=Floor((x-41)*15/tilew-1)
      if destx<0 then destx=0
      if destx>mapw-1 then destx=mapw-1
      desty=Floor((y-40)*25.45/tileh-1)
      if desty<0 then desty=0
      if desty>maph-1 then desty=maph-1
      Goto Tornado
    else
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
    end if
    Gosub InitEcranDeJeu
    if light then Gosub FiatLux else gr.render
    if !mute & music$<>"" then audio.play musPtr
    cheatTmr=Clock() %' si besoin d'enchaîner un autre code triche
    Goto BouclePrincipale
  end if

  !' Icône passe-muraille
  gr.bounded.touch i,283*scale_w,12*scale_h,319*scale_w,48*scale_h
  if i then
    GhostMode=1-GhostMode
    vibrate VibrCrt[], -1
    gr.bitmap.delete tmpBmp
    Gosub InitEcranDeJeu
    if light then Gosub FiatLux else gr.render
    Pause 200
    if GhostMode then
      Popup RPGLBL$[179],0,z,0
      if !mute then Soundpool.play tmp,sndPRi,0.50,0.50,1,0,1
    else
      Popup RPGLBL$[180],0,z,0
      if !mute then Soundpool.play tmp,sndPRo,0.50,0.50,1,0,1
    end if
    if mute then
      for i=1 to 3
        vibrate VibrCrt[], -1
        Pause 150
      next i
    else
      Pause 1000
      if music$<>"" then
        audio.play musPtr
        audio.loop
      end if
    end if
    cheatTmr=Clock() %' si besoin d'enchaîner un autre code triche
    Goto BouclePrincipale
  end if

  !' Icône luminosité
  gr.bounded.touch i,328*scale_w,12*scale_h,364*scale_w,48*scale_h
  if i then
    ToujoursLumineux=1-ToujoursLumineux
    Gosub FromDuskTillDawn
    dkns=dknsTgt
    vibrate VibrCrt[], -1
    gr.bitmap.delete tmpBmp
    if !mute then Soundpool.play tmp,sndCan,0.50,0.50,1,0,1
    if ToujoursLumineux then
      Popup RPGLBL$[181],0,z,0
      light=0
    else
      Popup RPGLBL$[182],0,z,0
    end if
    Gosub InitEcranDeJeu
    if light then Gosub FiatLux else gr.render
    Pause 200
    if mute then
      for i=1 to 3
        vibrate VibrCrt[], -1
        Pause 150
      next i
    else
      Pause 1000
      if music$<>"" then
        audio.play musPtr
        audio.loop
      end if
    end if
    cheatTmr=Clock() %' si besoin d'enchaîner un autre code triche
    Goto BouclePrincipale
  end if

  !' Spécial quitter sans rien sauver (coin bas droite de l'écran de triche)
  gr.bounded.touch i,real_w-36*scale_w,real_h-36*scale_h,real_w,real_h
  if i then
    Exit
  else

  !' Zone sous la barre du menu triche = retour au jeu
    gr.bounded.touch i,0,60*scale_h,real_w,real_h
    if i then
      vibrate VibrCrt[], -1
      gr.bitmap.delete tmpBmp
      Gosub InitEcranDeJeu
      if light then Gosub FiatLux else gr.render
      Pause 200
      if !mute & music$<>"" then audio.play musPtr
      cheatTmr=Clock() %' si besoin d'enchaîner un autre code triche
      Goto BouclePrincipale
    end if
  end if

until 0
!'---------------------------------------------------------------------------------------------------

Transformation:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine du Script 2 : le héros se transforme
!'---------------------------------------------------------------------------------------------------
if needChangeMusic & !mute then audio.stop
gr.color 0,0,0,0,0
gr.bitmap.draw tmpPtr, heroTile[heroform,DerDir+1], Floor(nthor/2)*tilew, Floor(ntver/2)*tileh
for i=0 to 255 step 5
  gr.modify heroPtr, "alpha", 255-i
  gr.modify tmpPtr, "alpha", i
  if light then Gosub FiatLux else gr.render
  Pause 2
next i
if HerosEnHeaume then Gosub MettreEnleverLeHeaume
Gosub InitEcranDeJeu
if light then Gosub FiatLux else gr.render
if needChangeMusic then Gosub MusiqueChange
Goto CheckObjet
!'---------------------------------------------------------------------------------------------------

DeterreUnCoeur:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine : déterre un coeur depuis le tile (destx,desty) et récupère des points de vie
!'---------------------------------------------------------------------------------------------------
if !mute then Soundpool.play tmp,sndPdv,0.99,0.99,1,0,1
gr.color 255,0,0,0,1
if !coeurPtr then
  gr.bitmap.draw coeurPtr, coeurBmp, (di_w - tilew)/2, (di_h - tileh)/2
else
  gr.show coeurPtr
end if
for y=0 to tileh
  gr.modify coeurPtr, "x", (di_w - tilew)/2 + 0.25*tilew*Cos(Pi*4*y/tileh) + (destx-herox+1)*tilew
  gr.modify coeurPtr, "y", (di_h - tileh)/2 - y + (desty-heroy+1)*tileh
  if light then Gosub FiatLux else gr.render
  Pause 2
next y
gr.hide coeurPtr
!' Mise à jour des points de vie
pdv=pdv + 10
if pdv>100 then pdv=100
gr.modify pdvPtr, "right", tilew*(1.1+3*pdv/100)
if light then Gosub FiatLux else gr.render
Return
!'---------------------------------------------------------------------------------------------------

HerosBouge:
!'---------------------------------------------------------------------------------------------------
!' Routine de déplacement du héros
!'---------------------------------------------------------------------------------------------------
tmp=DerDir
if heroform=5 then  %' Dernière Direction du héros
  DerDir=zu*_upBat + zd*_downBat + zr*_rightBat + zl*_leftBat
else
  DerDir=zu*_up + zd*_down + zr*_right + zl*_left
end if
if tmp<>DerDir then
  gr.modify heroPtr, "bitmap", heroTile[heroform,DerDir+1]
  if light then Gosub FiatLux else gr.render
  Pause 100
  Goto BouclePrincipale
end if

!' Héros descend du bateau
!'-------------------------
if heroform=5 & zd then
  y=heroy+dy+1
  for x=herox+dx-1 to herox+dx+1
    Gosub GetB %' vérifie que le bateau est aligné sur des pilotis
    if b<>14 then Goto DescendPasDuBateau
  next x
  if !mute then audio.stop
  !' Prépare le nouveau décor (bords de carte mis à jour pendant le déplacement)
  gr.color 255,0,0,0,1
  gr.bitmap.create tmpBmp, (nthor+2)*tilew, (ntver+2)*tileh
  gr.bitmap.drawinto.start tmpBmp
  gr.bitmap.draw nul, bgnBmp, 0, 0
  gr.bitmap.drawinto.end
  gr.modify bgnPtr, "bitmap", tmpBmp
  gr.modify bgnPtr,"x",-tilew
  gr.modify bgnPtr,"y",-tileh
  herox=herox+1
  gr.bitmap.crop nul, bgnBmp, tilew, 0, (nthor+1)*tilew, (ntver+2)*tileh
  gr.bitmap.delete bgnBmp
  gr.bitmap.create bgnBmp, (nthor+2)*tilew, (ntver+2)*tileh
  gr.bitmap.drawinto.start bgnBmp
  gr.bitmap.draw tmpPtr, nul, 0, 0
  gr.bitmap.drawinto.end
  gr.bitmap.delete nul
  !' Décentrage du bateau sur l'écran
  x=herox+dx+Floor(nthor/2)+1
  y=heroy+dy-Floor(ntver/2)-2
  for w=1 to ntver+2
    y=y+1
    Gosub AfficherDecor
    gr.modify heroPtr,"x",Floor(nthor/2-1)*tilew - Floor(w*tilew/(ntver+2))
    gr.modify bgnPtr,"x",-tilew-Floor(w*tilew/(ntver+2))
    if Mod(w,Floor(ntver/3.25))=0 then
      if light then Gosub FiatLux else gr.render
    end if
  next w
  gr.bitmap.delete tmpBmp
  gr.modify bgnPtr,"bitmap",bgnBmp
  gr.modify bgnPtr,"x",-tilew
  gr.modify bgnPtr,"y",-tileh
  if light then Gosub FiatLux else gr.render
  heroform=1
  DerDir=_down
  batx=herox-3
  baty=heroy-1
  !' Dessine le bateau à quai par dessus le décor
  gr.color 255,0,0,0,1
  gr.bitmap.drawinto.start bgnBmp
  for y=heroy+dy to heroy+dy+1
    for x=herox+dx-2 to herox+dx
      Gosub GetC %' décor "bateau vide" (en fait perso) sous le bateau actuel
      if c>0 then gr.bitmap.draw tmpPtr, chrTile[c], (x-herox-dx+Floor(nthor/2)+1)*tilew, (y-heroy-dy+Floor(ntver/2)+1)*tileh
    next x
  next y
  gr.bitmap.drawinto.end
  Gosub InitEcranDeJeu
  !' Prépare le nouveau décor (bords de carte mis à jour pendant le déplacement)
  gr.color 255,0,0,0,1
  gr.bitmap.create tmpBmp, (nthor+2)*tilew, (ntver+2)*tileh
  gr.bitmap.drawinto.start tmpBmp
  gr.bitmap.draw nul, bgnBmp, 0, 0
  gr.bitmap.drawinto.end
  gr.modify bgnPtr, "bitmap", tmpBmp
  gr.modify bgnPtr,"x",-tilew
  gr.modify bgnPtr,"y",-tileh
  heroy=heroy+1
  gr.bitmap.crop nul, bgnBmp, 0, tileh, (nthor+2)*tilew, (ntver+1)*tileh
  gr.bitmap.delete bgnBmp
  gr.bitmap.create bgnBmp, (nthor+2)*tilew, (ntver+2)*tileh
  gr.bitmap.drawinto.start bgnBmp
  gr.bitmap.draw tmpPtr, nul, 0, 0
  gr.bitmap.drawinto.end
  gr.bitmap.delete nul
  !' Héros descend l'échelle
  x=herox+dx-Floor(nthor/2)-2
  y=heroy+dy+Floor(ntver/2)+1
  w=1
  for z=1 to nthor+2
    x=x+1
    Gosub AfficherDecor
    if z>=0.25*(nthor+2) & w=1 then
      w=2
      gr.modify heroPtr,"bitmap",heroTile[heroform,_up+2]
    elseif z>=0.5*(nthor+2) & w=2 then
      w=3
      gr.modify heroPtr,"bitmap",heroTile[heroform,_up+3]
    elseif z>=0.75*(nthor+2) & w=3 then
      w=4
      gr.modify heroPtr,"bitmap",heroTile[heroform,_up+2]
    end if
    gr.modify bgnPtr,"y",-tileh-Floor(z*tileh/(nthor+2))
    if Mod(z,Floor(nthor/3.25))=0 then
      if light then Gosub FiatLux else gr.render
    end if
  next z
  gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+1]
  gr.bitmap.delete tmpBmp
  gr.modify bgnPtr,"bitmap",bgnBmp
  gr.modify bgnPtr,"x",-tilew
  gr.modify bgnPtr,"y",-tileh
  if light then Gosub FiatLux else gr.render
  !' Change la musique
  Gosub MusiqueChange
  Goto BouclePrincipale
end if
DescendPasDuBateau:

!' Rencontre avec un perso
!'------------------------
if heroform=5 then %' Rencontre en bateau
  for y=heroy+dy+zd-zu to heroy+dy+zd-zu+1
    for x=herox+dx+zr-zl-1 to herox+dx+zr-zl+1
      Gosub GetC
      if c then F_n.break
    next y
    if c then F_n.break
  next x
  if !c then Goto FinScript3 %' aucun perso
  if (zu | zd) then i=zd+1 else i=zl+3
else %' Rencontre à pied
  x=herox + dx + zr - zl
  y=heroy + dy + zd - zu
  Gosub GetC
  if !c then Goto FinScript3 %' aucun perso
  if zu | zd then i=_up*zu+_down*zd+1 else i=_right*zr+_left*zl+1
end if
gr.modify heroPtr,"bitmap",heroTile[heroform,i]
if light then Gosub FiatLux else gr.render

!' Rencontre avec le bateau et son capitaine : le héros monte à bord
if c=70 & heroform=1 & DerDir=_up then
  if HerosEnHeaume then
    z=-3*tileh*scale_h
    Popup RPGLBL$[183],0,z,0
  else
    if !mute then audio.stop
    !' Prépare le nouveau décor (bords de carte mis à jour pendant le déplacement)
    gr.color 255,0,0,0,1
    gr.bitmap.create tmpBmp, (nthor+2)*tilew, (ntver+2)*tileh
    gr.bitmap.drawinto.start tmpBmp
    gr.bitmap.draw nul, bgnBmp, 0, 0
    gr.bitmap.drawinto.end
    gr.modify bgnPtr, "bitmap", tmpBmp
    gr.modify bgnPtr,"x",-tilew
    gr.modify bgnPtr,"y",-tileh
    heroy=heroy-1
    gr.bitmap.crop nul, bgnBmp, 0, 0, (nthor+2)*tilew, (ntver+1)*tileh
    gr.bitmap.delete bgnBmp
    gr.bitmap.create bgnBmp, (nthor+2)*tilew, (ntver+2)*tileh
    gr.bitmap.drawinto.start bgnBmp
    gr.bitmap.draw tmpPtr, nul, 0, tileh
    gr.bitmap.drawinto.end
    gr.bitmap.delete nul
    !' Héros grimpe l'échelle
    x=herox+dx-Floor(nthor/2)-2
    y=heroy+dy-Floor(ntver/2)-1
    w=1
    for z=1 to nthor+2
      x=x+1
      Gosub AfficherDecor
      if z>=0.25*(nthor+2) & w=1 then
        w=2
        gr.modify heroPtr,"bitmap",heroTile[heroform,_up+2]
      elseif z>=0.5*(nthor+2) & w=2 then
        w=3
        gr.modify heroPtr,"bitmap",heroTile[heroform,_up+3]
      elseif z>=0.75*(nthor+2) & w=3 then
        w=4
        gr.modify heroPtr,"bitmap",heroTile[heroform,_up+2]
      end if
      gr.modify bgnPtr,"y",-tileh+Floor(z*tileh/(nthor+2))
      if Mod(z,Floor(nthor/3.25))=0 then
        if light then Gosub FiatLux else gr.render
      end if
    next z
    gr.modify heroPtr,"bitmap",heroTile[heroform,_up+1]
    gr.bitmap.delete tmpBmp
    gr.modify bgnPtr,"bitmap",bgnBmp
    gr.modify bgnPtr,"x",-tilew
    gr.modify bgnPtr,"y",-tileh
    if light then Gosub FiatLux else gr.render
    heroform=5
    DerDir=_rightBat
    !' Efface le bateau à quai (redessine le décor derrière)
    gr.color 255,0,0,0,1
    gr.bitmap.drawinto.start bgnBmp
    for y=heroy+dy to heroy+dy+1
      for x=herox+dx-2 to herox+dx
        Gosub GetB %' tile de décor initial sous le bateau à quai
        if b>0 then gr.bitmap.draw tmpPtr, bgnTile[b], (x-herox-dx+Floor(nthor/2)+1)*tilew, (y-heroy-dy+Floor(ntver/2)+1)*tileh
      next x
    next y
    gr.bitmap.drawinto.end
    Gosub InitEcranDeJeu
    !' Prépare le nouveau décor (bords de carte mis à jour pendant le déplacement)
    gr.color 255,0,0,0,1
    gr.bitmap.create tmpBmp, (nthor+2)*tilew, (ntver+2)*tileh
    gr.bitmap.drawinto.start tmpBmp
    gr.bitmap.draw nul, bgnBmp, 0, 0
    gr.bitmap.drawinto.end
    gr.modify bgnPtr, "bitmap", tmpBmp
    gr.modify bgnPtr,"x",-tilew
    gr.modify bgnPtr,"y",-tileh
    herox=herox-1
    gr.bitmap.crop nul, bgnBmp, 0, 0, (nthor+1)*tilew, (ntver+2)*tileh
    gr.bitmap.delete bgnBmp
    gr.bitmap.create bgnBmp, (nthor+2)*tilew, (ntver+2)*tileh
    gr.bitmap.drawinto.start bgnBmp
    gr.bitmap.draw tmpPtr, nul, tilew, 0
    gr.bitmap.drawinto.end
    gr.bitmap.delete nul
    !' Centrage du bateau sur l'écran
    x=herox+dx-Floor(nthor/2)-1
    y=heroy+dy-Floor(ntver/2)-2
    for w=1 to ntver+2
      y=y+1
      Gosub AfficherDecor
      gr.modify heroPtr,"x",Floor(nthor/2-2)*tilew + Floor(w*tilew/(ntver+2))
      gr.modify bgnPtr,"x",-tilew+Floor(w*tilew/(ntver+2))
      if Mod(w,Floor(ntver/3.25))=0 then
        if light then Gosub FiatLux else gr.render
      end if
    next w
    gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+1]
    gr.bitmap.delete tmpBmp
    gr.modify bgnPtr,"bitmap",bgnBmp
    gr.modify bgnPtr,"x",-tilew
    gr.modify bgnPtr,"y",-tileh
    if light then Gosub FiatLux else gr.render
    !' Change la musique
    Gosub MusiqueChange
    Goto BouclePrincipale
  end if
end if

z=-3*tileh*scale_h
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
!' [Script 3][AVANT déplacement] Le héros veut se déplacer sur un tile contenant un PERSONNAGE
!'      c=perso rencontré ; zu=(up) le héros l'aborde par le bas (il se déplace vers le haut)
!'      zd=(down) il l'aborde par le haut ; zr (right)=par la gauche ; zl (left)=par la droite
!'      destx, desty=coordonnées de destination du héros
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
if heroform=4 then Goto FinScript3 %' l'aigle survole tous les personnages
if c=11 | c=33 | c=34 | c=35 | c=80 then Goto FinScript3 %' tiles de perso "transparents"
!' Autres personnages
if c=7 then
  e$=RPGLBL$[184]
  if heroform<>1 then
    Popup "...",0,z,0
    Pause 2200
  elseif zu | zd | zr then
    Popup RPGLBL$[185],0,z,0
    Pause 2200
  elseif !perso[c] then
    Popup e$+RPGLBL$[186],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[187],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[188],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[189],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[190],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[191],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[192],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[193],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[194],0,z,0
    Pause 2200
    perso[c]=1
    o = 22
    Gosub ObjetMineurRebondit
    Gosub DebloqueSecretMusique
  elseif !objet[14] then %' pas de bague sylvestre
    Popup e$+RPGLBL$[195],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[196],0,z,0
    Pause 2200
  elseif !objet[18] then %' pas de sphère du loup
    Popup e$+RPGLBL$[197],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[198],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[199],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[200],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[201],0,z,0
    Pause 2200
  elseif !objet[16] then %' pas de bague sanguine (poisson)
    Popup e$+RPGLBL$[202],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[203],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[204],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[205],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[206],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[207],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[208],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[209],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[210],0,z,0
    Pause 2200
  elseif !objet[36] then %' pas de marteau
    Popup e$+RPGLBL$[211],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[212],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[213],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[214],0,z,0
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    tmr=Clock()
    for desty=41 to 42
      for destx=144 to 152
        Gosub RazO
      next destx
    next desty
    if Clock()-tmr<2000 then Pause 2200-Clock()+tmr
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Popup e$+RPGLBL$[215],0,z,0
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    tmr=Clock()
    for desty=43 to 44
      for destx=144 to 152
        Gosub RazO
      next destx
    next desty
    if Clock()-tmr<2000 then Pause 2200-Clock()+tmr
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Popup e$+RPGLBL$[216],0,z,0
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    tmr=Clock()
    desty=45
    for destx=144 to 152
      Gosub RazO
    next destx
    if Clock()-tmr<2000 then Pause 2200-Clock()+tmr
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Gosub DebloqueSecretMusique
  elseif !objet[5] then %' pas de pointe de givre
    Popup e$+RPGLBL$[217],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[218],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[219],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[220],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[221],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[222],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[223],0,z,0
    Pause 2200
  elseif !objet[8] then %' pas de conque du vent
    Popup e$+RPGLBL$[224],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[225],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[226],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[227],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[228],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[229],0,z,0
    Pause 2200
  elseif !objet[25] then %' pas de runes élémentales
    Popup e$+RPGLBL$[230],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[231],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[232],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[233],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[234],0,z,0
    Pause 2200
  elseif perso[c]=2 then %' carte du labyrinthe puis discussion avec nain marin
    Popup e$+RPGLBL$[235],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[236],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[237],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[238],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[239],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[240],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[241],0,z,0
    Pause 2200
    perso[c]=3
  elseif perso[c]=3 then
    Popup e$+RPGLBL$[242],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[243],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[244],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[245],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[246],0,z,0
    Pause 2200
    perso[c]=4
    Gosub DebloqueSecretMusique
  elseif perso[c]=4 then
    Popup e$+RPGLBL$[247],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[248],0,z,0
    Pause 2200
  else
    Popup e$+RPGLBL$[249],0,z,0
    Pause 2200
  end if

elseif c=19 then %' garde jaune du continent de l'ouest
  e$=RPGLBL$[250]
  if heroform<>1 then
    Popup "...",0,z,0
    Pause 2200
  else if zd then
    Popup RPGLBL$[251],0,z,0
    Pause 2200
  else if perso[c]=3 then
    Popup e$+RPGLBL$[252],0,z,0
    Pause 2200
  else if perso[c]=2 then
    Popup e$+RPGLBL$[253],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[254],0,z,0
    Pause 2200
  else if perso[20]=1 then
    Popup e$+RPGLBL$[255],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[256],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[257],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[258],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[259],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[260],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[261],0,z,0
    Pause 2200
    o=23 %' os préféré du chien
    Gosub ObjetMineurRebondit
    perso[72]=1
    perso[19]=2
    perso[20]=2
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    destx=4 %' fait apparaitre le chien de garde
    desty=108
    c=72
    Gosub MovC
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Gosub DebloqueSecretMusique
  else
    Popup e$+RPGLBL$[262],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[263],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[264],0,z,0
    Pause 2200
    perso[c]=1
  end if

elseif c=20 then %' garde orange du continent de l'ouest
  e$=RPGLBL$[265]
  if heroform<>1 then
    Popup "...",0,z,0
    Pause 2200
  else if zd then
    Popup RPGLBL$[266],0,z,0
    Pause 2200
  else if objet[29] then
    Popup e$+RPGLBL$[267],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[268],0,z,0
    Pause 2200
  else if perso[c]=3 then
    Popup e$+RPGLBL$[269],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[270],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[271],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[272],0,z,0
    Pause 2200
  else if perso[c]=2 then
    Popup e$+RPGLBL$[273],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[274],0,z,0
    Pause 2200
  else if perso[19]=1 then
    Popup e$+RPGLBL$[275],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[276],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[277],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[278],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[279],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[280],0,z,0
    Pause 2200
    o=23 %' os préféré du chien
    Gosub ObjetMineurRebondit
    perso[72]=1
    perso[19]=2
    perso[20]=2
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    destx=4 %' fait apparaitre le chien de garde
    desty=108
    c=72
    Gosub MovC
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Gosub DebloqueSecretMusique
  else
    Popup e$+RPGLBL$[281],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[282],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[283],0,z,0
    Pause 2200
    perso[c]=1
  end if

elseif c=72 then
  e$=RPGLBL$[284]
  if heroform<>2 then
    Popup e$+RPGLBL$[285],0,z,0
    Pause 2200
  else if if zu | zd | zr then
    Popup RPGLBL$[286],0,z,0
    Pause 2200
  else
    Popup e$+RPGLBL$[287],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[288],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[289],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[290],0,z,0
    Pause 2200
    perso[c]=2
  end if

elseif c=9 then
  e$=RPGLBL$[291]
  if heroform<>1 then
    Popup "...",0,z,0
    Pause 2200
  elseif !perso[c] & !objet[34] then %' tout début (pas encore le crâne de la désolation)
    Popup e$+RPGLBL$[292],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[293],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[294],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[295],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[296],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[297],0,z,0
    Pause 2200
    if usd<48 then
      Popup e$+RPGLBL$[298],0,z,0
      Pause 2200
    else
      Popup e$+RPGLBL$[299]+Stg$(usd)+RPGLBL$[300],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[301],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[302],0,z,0
      Pause 2200
      perso[c]=1
      usd=usd - 48
      Gosub RefreshUsd
      Gosub DebloqueSecretMusique
    end if
  else if perso[c]=1 & destx=108 & desty=181 then
      Popup e$+RPGLBL$[303],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[304],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[305],0,z,0
      Pause 2200
  elseif !perso[c] & !objet[24] then %' presque fin (pas encore la carte du labyrinthe)
    Popup e$+RPGLBL$[306],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[307],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[308],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[309],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[310],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[311],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[312],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[313],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[314],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[315],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[316],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[317],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[318],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[319],0,z,0
    Pause 2200
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    destx=102 %' fait apparaitre la carte du labyrinthe
    desty=248
    o=24
    Gosub SetO
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Gosub DebloqueSecretMusique
  elseif !perso[c] & perso[7]<2 then %' le héros vient de récupérer la carte du labyrinthe
    Popup e$+RPGLBL$[320],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[321],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[322],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[323],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[324],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[325],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[326],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[327],0,z,0
    Pause 2200
    perso[7]=2 %' sorcière de l'ouest
  elseif !perso[c] then
    Popup e$+RPGLBL$[328],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[329],0,z,0
    Pause 2200
  end if

elseif c=23 | c=24 | c=25 then %' bateau vide à quai
  if zu & heroform=1 then
    if !perso[9] then
      Popup RPGLBL$[330],0,z,0
      Pause 2200
      Popup RPGLBL$[331],0,z,0
      Pause 2200
    end if
  end if

elseif c=71 & zu & heroform=1 then %' bateau + nain marin à quai
  e$=RPGLBL$[332]
  if HerosEnHeaume then
    i=Ceil(3*Rnd())
    if i=1 then
      Popup e$+RPGLBL$[333],0,z,0
      Pause 2200
    elseif i=2 then
      Popup e$+RPGLBL$[334],0,z,0
      Pause 2200
    elseif i=3 then
      Popup e$+RPGLBL$[335],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[336],0,z,0
      Pause 2200
    end if
  else
    i=Ceil(10*Rnd())
    if i=1 then
      Popup e$+RPGLBL$[337],0,z,0
      Pause 2200
    elseif i=2 then
      Popup e$+RPGLBL$[338],0,z,0
      Pause 2200
    elseif i=3 then
      Popup e$+RPGLBL$[339],0,z,0
      Pause 2200
    elseif i=4 & batx>45 then
      Popup e$+RPGLBL$[340],0,z,0
      Pause 2200
    elseif i=5 then
      Popup e$+RPGLBL$[341],0,z,0
      Pause 2200
    elseif i=6 then
      Popup e$+RPGLBL$[342],0,z,0
      Pause 2200
    elseif i=7 & objet[10] then %' dague
      Popup e$+RPGLBL$[343],0,z,0
      Pause 2200
    elseif i=8 & objet[25] then %' runes
      Popup e$+RPGLBL$[344],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[345],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[346],0,z,0
      Pause 2200
    elseif i=9 & objet[10] then %' dague
      Popup e$+RPGLBL$[347],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[348],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[349],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[350],0,z,0
      Pause 2200
    elseif i=10 & objet[36] then %' marteau
      Popup e$+RPGLBL$[351],0,z,0
      Pause 2200
    else
      Popup e$+RPGLBL$[352],0,z,0
      Pause 2200
    end if
  end if

elseif c=18 then %' garde bleu de Zoraniscaeos (exilé sur la pointe nord-est)
  e$=RPGLBL$[353]
  if heroform<>1 then
    Goto BouclePrincipale
  else if !perso[10] then
    Popup e$+RPGLBL$[354],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[355],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[356],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[357],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[358],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[359],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[360],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[361],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[362],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[363],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[364],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[365],0,z,0
    Pause 2200
    perso[c]=1
    perso[10]=2
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    destx=103 %' garde bleu secouru se retrouve sur l'île
    desty=79
    c=18
    Gosub MovC
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    destx=102 %' garde rouge ne bloque plus l'entrée de la ville
    desty=79
    c=10
    Gosub MovC
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Gosub DebloqueSecretMusique
    Goto BouclePrincipale
  else if perso[10]=1 then
    Popup e$+RPGLBL$[366],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[367],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[368],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[369],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[370],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[371],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[372],0,z,0
    Pause 2200
    perso[c]=1
    perso[10]=2
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    destx=103 %' garde bleu secouru se retrouve sur l'île
    desty=79
    c=18
    Gosub MovC
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    destx=102 %' garde rouge ne bloque plus l'entrée de la ville
    desty=79
    c=10
    Gosub MovC
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Gosub DebloqueSecretMusique
    Goto BouclePrincipale
  else
    Popup e$+RPGLBL$[373],0,z,0
    Pause 2200
    if objet[11] then
      Popup e$+RPGLBL$[374],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[375],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[376],0,z,0
      Pause 2200
    end if
  end if

elseif c=10 then %' garde rouge de Zoraniscaeos (grand ami du garde bleu exilé)
  e$=RPGLBL$[377]
  if heroform<>1 then
    Popup e$+RPGLBL$[378],0,z,0
    Pause 2200
  else if !perso[c] then
    Popup e$+RPGLBL$[379],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[380],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[381],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[382],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[383],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[384],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[385],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[386],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[387],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[388],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[389],0,z,0
    Pause 2200
    perso[c]=1
  else if perso[c]=1 then
    Popup e$+RPGLBL$[390],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[391],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[392],0,z,0
    Pause 2200
  else if perso[c]=2 then
    Popup e$+RPGLBL$[393],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[394],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[395],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[396],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[397],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[398],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[399],0,z,0
    Pause 2200
  else if perso[c]=3 then
    Popup e$+RPGLBL$[400],0,z,0
    Pause 2200
    if objet[11] then
      Popup e$+RPGLBL$[401],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[402],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[403],0,z,0
      Pause 2200
    end if
  end if

elseif c=17 then %' garde vert (fait travailler les bucherons)
  e$=RPGLBL$[404]
  if heroform<>1 then
    Popup e$+"...",0,z,0
    Pause 2200
  else if !perso[12] then
    Popup e$+RPGLBL$[405],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[406],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[407],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[408],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[409],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[410],0,z,0
    Pause 2200
  else if perso[12]=1 then
    if !objet[10] then
      Popup e$+RPGLBL$[411],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[412],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[413],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[414],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[415],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[416],0,z,0
      Pause 2200
    else
      Popup e$+RPGLBL$[417],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[418],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[419],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[420],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[421],0,z,0
      Pause 2200
      perso[12]=2
    end if
  else if perso[12]>=2 then
    Popup e$+RPGLBL$[422],0,z,0
    Pause 2200
  end if

elseif c=12 then %' bucherons de la forêt de Zoraniscaeos
  e$=RPGLBL$[423]
  if heroform<>1 then
    Popup e$+RPGLBL$[424],0,z,0
    Pause 2200
  else if !perso[c] then
    Popup e$+RPGLBL$[425],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[426],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[427],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[428],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[429],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[430],0,z,0
    Pause 2200
    perso[c]=1
  else if perso[c]=1 then
    Popup e$+RPGLBL$[431],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[432],0,z,0
    Pause 2200
  else if perso[c]=2 then
    Popup e$+RPGLBL$[433],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[434],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[435],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[436],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[437],0,z,0
    Pause 2200
    perso[c]=3
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    destx=128 %' tronc d'arbre dégagé par les bucherons
    desty=54
    b=23
    Gosub SetB
    s=13
    Gosub SetS
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Gosub Explosion
    Gosub DebloqueSecretMusique
  else if perso[c]=3 then
    Popup e$+RPGLBL$[438],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[439],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[440],0,z,0
    Pause 2200
  end if

elseif c=69 | c=79 then %' géant des montagnes de Zoraniscaeos
  e$=RPGLBL$[441]
  if heroform<>1 then
    Popup e$+RPGLBL$[442],0,z,0
    Pause 2200
  else if perso[79]=159 then
    Popup e$+RPGLBL$[443],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[444],0,z,0
    Pause 2200
  else if perso[79]=158 then
    Popup e$+RPGLBL$[445],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[446],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[447],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[448],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[449],0,z,0
    Pause 2200
    perso[79]=159
    o=32
    e$=RPGLBL$[450]
    Gosub TrouveObjet
  else if perso[69]=45 then
    Popup e$+RPGLBL$[451],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[452],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[453],0,z,0
    Pause 2200
  else if perso[69]=44 then
    Popup e$+RPGLBL$[454],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[455],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[456],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[457],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[458],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[459],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[460],0,z,0
    Pause 2200
    perso[69]=45
    destx=herox-1
    desty=heroy-1
    Gosub Explosion
    Gosub DebloqueSecretMusique
  else if perso[69] & perso[69]<44 then
    Popup e$+RPGLBL$[461],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[462],0,z,0
    Pause 2200
  else
    Popup e$+RPGLBL$[463],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[464],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[465],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[466],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[467],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[468],0,z,0
    Pause 2200
    if usd>=34 then
      Popup e$+RPGLBL$[469],0,z,0
      Pause 2200
      perso[69]=1
      usd=usd - 34
      Gosub RefreshUsd
      o=36
      e$=RPGLBL$[470]
      Gosub TrouveObjet
    else
      Popup e$+RPGLBL$[471],0,z,0
      Pause 2200
    end if
  end if

elseif c=30 | c=40 then %' navigateur sur son radeau (mer du nord)
  e$=RPGLBL$[472]
  if heroform=3 then
    Popup e$+"...",0,z,0
    Pause 2200
  else if !objet[11] then %' carte maritime
    Popup e$+RPGLBL$[473],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[474],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[475],0,z,0
    Pause 2200
  else if !perso[40] then
    c=8 %' sorcière du désert
    destx=208
    desty=83
    Gosub MovC
    Popup e$+RPGLBL$[476],0,z,0
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    tmr=Clock()
    for desty=61 to 65
      for destx=181 to 215 %' 35
        Gosub RazB
        Gosub RazS
        Gosub RazO
      next destx
    next desty
    if Clock()-tmr<2000 then Pause 2200-Clock()+tmr
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Popup e$+RPGLBL$[477],0,z,0
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    tmr=Clock()
    for desty=66 to 70
      for destx=181 to 215 %' 35
        Gosub RazB
        Gosub RazS
        Gosub RazO
      next destx
    next desty
    if Clock()-tmr<2000 then Pause 2200-Clock()+tmr
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Popup e$+RPGLBL$[478],0,z,0
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    tmr=Clock()
    for desty=71 to 75
      for destx=181 to 215 %' 35
        Gosub RazB
        Gosub RazS
        Gosub RazO
      next destx
    next desty
    if Clock()-tmr<2000 then Pause 2200-Clock()+tmr
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Popup e$+RPGLBL$[479],0,z,0
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    tmr=Clock()
    for desty=76 to 80
      for destx=181 to 215 %' 35
        Gosub RazB
        Gosub RazS
        Gosub RazO
      next destx
    next desty
    if Clock()-tmr<2000 then Pause 2200-Clock()+tmr
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Popup e$+RPGLBL$[480],0,z,0
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    tmr=Clock()
    for desty=81 to 85
      for destx=181 to 215 %' 35
        Gosub RazB
        Gosub RazS
        Gosub RazO
      next destx
    next desty
    if Clock()-tmr<2000 then Pause 2200-Clock()+tmr
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Popup e$+RPGLBL$[481],0,z,0
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    tmr=Clock()
    for desty=86 to 90
      for destx=181 to 215 %' 35
        Gosub RazB
        Gosub RazS
        Gosub RazO
      next destx
    next desty
    if Clock()-tmr<2000 then Pause 2200-Clock()+tmr
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Popup e$+RPGLBL$[482],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[483],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[484],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[485],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[486],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[487],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[488],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[489],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[490],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[491],0,z,0
    perso[40]=1
    Gosub DebloqueSecretMusique
  else if perso[40]=1 then
    Popup e$+RPGLBL$[492],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[493],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[494],0,z,0
    Pause 2200
  else if perso[40]=2 then
    if !objet[8] then %' conque du vent
      Popup e$+RPGLBL$[495],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[496],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[497],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[498],0,z,0
      Pause 2200
    else if !objet[25] then %' runes élémentales
      Popup e$+RPGLBL$[499],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[500],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[501],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[502],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[503],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[504],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[505],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[506],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[507],0,z,0
      Pause 2200
    else if !objet[4] then %' croix de l'eau
      Popup e$+RPGLBL$[508],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[509],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[510],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[511],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[512],0,z,0
      Pause 2200
    else if !objet[26] then %' miroir des glaces
      Popup e$+RPGLBL$[513],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[514],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[515],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[516],0,z,0
      Pause 2200
    else if !objet[7] then %' crâne de la désolation
      Popup e$+RPGLBL$[517],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[518],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[519],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[520],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[521],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[522],0,z,0
      Pause 2200
    else
      Popup e$+RPGLBL$[523],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[524],0,z,0
      Pause 2200
    end if
  end if

elseif c=8 then %' sorcière du désert
  e$=RPGLBL$[525]
  if heroform<>1 then
    Popup e$+"...",0,z,0
    Pause 2200
  else if !perso[c] then
    Popup e$+RPGLBL$[526],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[527],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[528],0,z,0
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    tmr=CLock()
    destx=201 %' entrées des pyramides de l'île-sable
    desty=82
    s=13
    Gosub SetS
    destx=194
    desty=85
    s=13
    Gosub SetS
    if Clock()-tmr<2000 then Pause 2200-Clock()+tmr
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Popup e$+RPGLBL$[529],0,z,0
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    tmr=CLock()
    destx=188 %' entrées des pyramides de l'île-sable
    desty=81
    s=13
    Gosub SetS
    destx=188
    desty=78
    s=13
    Gosub SetS
    if Clock()-tmr<2000 then Pause 2200-Clock()+tmr
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Popup e$+RPGLBL$[530],0,z,0
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    tmr=CLock()
    destx=196 %' entrées des pyramides de l'île-sable
    desty=74
    s=13
    Gosub SetS
    destx=190
    desty=67
    s=13
    Gosub SetS
    if Clock()-tmr<2000 then Pause 2200-Clock()+tmr
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Popup e$+RPGLBL$[531],0,z,0
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    tmr=CLock()
    destx=192 %' entrées des pyramides de l'île-sable
    desty=66
    s=13
    Gosub SetS
    if Clock()-tmr<2000 then Pause 2200-Clock()+tmr
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Popup e$+RPGLBL$[532],0,z,0
    Pause 2200
    perso[c]=1
    Gosub DebloqueSecretMusique
  else if !objet[5] then %' pointe de givre
    Popup e$+RPGLBL$[533],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[534],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[535],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[536],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[537],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[538],0,z,0
    Pause 2200
  else if !objet[20] then %' sphère du poisson
    Popup e$+RPGLBL$[539],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[540],0,z,0
    Pause 2200
    if !objet[16] then %' bague sanguine
      Popup e$+RPGLBL$[541],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[542],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[543],0,z,0
      Pause 2200
    end if
  else if !objet[15] then %' bague sulfure
    Popup e$+RPGLBL$[544],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[545],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[546],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[547],0,z,0
    Pause 2200
  else if !objet[8] then %' conque du vent
    Popup e$+RPGLBL$[548],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[549],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[550],0,z,0
    Pause 2200
  else if !objet[25] then %' runes élémentales
    Popup e$+RPGLBL$[551],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[552],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[553],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[554],0,z,0
    Pause 2200
  else if !objet[4] then %' croix de l'eau
    Popup e$+RPGLBL$[555],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[556],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[557],0,z,0
    Pause 2200
  else if !objet[26] then %' miroir des glaces
    Popup e$+RPGLBL$[558],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[559],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[560],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[561],0,z,0
    Pause 2200
  else if !objet[19] then %' sphère de l'aigle
    Popup e$+RPGLBL$[562],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[563],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[564],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[565],0,z,0
    Pause 2200
  else if !objet[34] then %' crâne de la désolation
    if !perso[50] then %' nain blond
      Popup e$+RPGLBL$[566],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[567],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[568],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[569],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[570],0,z,0
      Pause 2200
    else if !perso[60] then %' nain forgeron
      Popup e$+RPGLBL$[571],0,z,0
      Pause 2200
      if !objet[33] then %' heaume
        Popup e$+RPGLBL$[572],0,z,0
        Pause 2200
        Popup e$+RPGLBL$[573],0,z,0
        Pause 2200
        Popup e$+RPGLBL$[574],0,z,0
        Pause 2200
        Popup e$+RPGLBL$[575],0,z,0
        Pause 2200
        Popup e$+RPGLBL$[576],0,z,0
        Pause 2200
      end if
    else
      Popup e$+RPGLBL$[577],0,z,0
      Pause 2200
    end if
  else if !objet[24] then %' carte du labyrinthe
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    c=9 %' déplace le nain marin sur la crique du bord du monde
    perso[c]=0
    destx=108
    desty=181
    Gosub MovC
    batx=95
    baty=183
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Popup e$+RPGLBL$[578],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[579],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[580],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[581],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[582],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[583],0,z,0
    Pause 2200
  else if perso[7]<3 then %' aide de la sorcière de l'ouest
    Popup e$+RPGLBL$[584],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[585],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[586],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[587],0,z,0
    Pause 2200
  else
    Popup e$+RPGLBL$[588],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[589],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[590],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[591],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[592],0,z,0
    Pause 2200
  end if

elseif c=73 then %' dauphin
  e$=RPGLBL$[593]
  if heroform=3 then
    if !perso[c] then
      Popup e$+RPGLBL$[594],0,z,0
      !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      tmr=CLock()
      for desty=177 to 179 %' 3
        for destx=103 to 107 %' 5 => 3*5=15
          Gosub RazB %' passage haut
          Gosub RazS
        next destx
      next desty
      if Clock()-tmr<2000 then Pause 2200-Clock()+tmr
      !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      Popup e$+RPGLBL$[595],0,z,0
      !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      tmr=CLock()
      for desty=187 to 189 %' 3
        for destx=103 to 107 %' 5 => 3*5=15
          Gosub RazB %' passage bas
          Gosub RazS
        next destx
      next desty
      if Clock()-tmr<2000 then Pause 2200-Clock()+tmr
      !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      Popup e$+RPGLBL$[596],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[597],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[598],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[599],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[600],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[601],0,z,0
      Pause 2200
      perso[c]=1
      Gosub DebloqueSecretMusique
    else
      Popup e$+RPGLBL$[602],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[603],0,z,0
      Pause 2200
    end if
  end if

elseif c=88 then %' Bender
  e$=RPGLBL$[604]
  if Rnd()<0.5 then
    Popup e$+RPGLBL$[605],0,z,0
    Pause 2200
  else
    Popup e$+RPGLBL$[606],0,z,0
    Pause 2200
  end if


elseif c=60 then %' nain forgeron sur l'île-marais
  e$=RPGLBL$[607]
  if heroform<>1 then
    Popup e$+"...",0,z,0
    Pause 2200
  else if !perso[50] then %' nain blond
    Popup e$+RPGLBL$[608],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[609],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[610],0,z,0
    Pause 2200
  else if !perso[c] then
    if usd<170 then
      Popup e$+RPGLBL$[611],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[612],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[613],0,z,0
      Pause 2200
    else
      Popup e$+RPGLBL$[614],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[615],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[616],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[617],0,z,0
      Pause 2200
      usd=usd - 170
      Gosub RefreshUsd
      AttenteFabricationDuHeaume=1
      perso[c]=1
      Gosub DebloqueSecretMusique
    end if
  else if AttenteFabricationDuHeaume then
    Popup e$+RPGLBL$[618],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[619],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[620],0,z,0
    Pause 2200
  else if !objet[33] then %' heaume
    Popup e$+RPGLBL$[621],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[622],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[623],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[624],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[625],0,z,0
    Pause 2200
    destx=152
    desty=33
    o=33 %' heaume
    Gosub SetO
    objet[33]=2
    Gosub DebloqueSecretMusique
  else if objet[33]=2 then %' heaume
    Popup e$+RPGLBL$[626],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[627],0,z,0
    Pause 2200
  else if HerosEnHeaume then
    Popup e$+RPGLBL$[628],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[629],0,z,0
    Pause 2200
  else if perso[81]<6 then
    Popup e$+RPGLBL$[630],0,z,0
    Pause 2200
  else if perso[81]=6 then
    Popup e$+RPGLBL$[631],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[632],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[633],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[634],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[635],0,z,0
    Pause 2200
    perso[81]=7
    AttenteCraneRoberts=1
    Gosub DebloqueSecretMusique
  else if AttenteCraneRoberts then
    Popup e$+RPGLBL$[636],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[637],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[638],0,z,0
    Pause 2200
  else if perso[81]>=8 then
    Popup e$+RPGLBL$[639],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[640],0,z,0
    Pause 2200
    if chrX[81]=142 & chrY[81]=144 then
      Pause 1000
      destx=192
      desty=31
      c=81
      Gosub MovC
      if !mute then Soundpool.play tmp,sndPRo,0.99,0.99,1,0,1
      Gosub Explosion
    end if
  end if

elseif c=50 then %' nain blond de la forêt du bord du monde
  e$=RPGLBL$[641]
  if heroform<>1 then
    Popup e$+"...",0,z,0
    Pause 2200
  else if !perso[c] then
    Popup e$+RPGLBL$[642],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[643],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[644],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[645],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[646],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[647],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[648],0,z,0
    Pause 2200
    perso[c]=1
  else if !perso[60] then %' nain forgeron
    Popup e$+RPGLBL$[649],0,z,0
    Pause 2200
  else if HerosEnHeaume then
    Popup e$+RPGLBL$[650],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[651],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[652],0,z,0
    Pause 2200
    if !objet[30] then %' épée légendaire
      Popup e$+RPGLBL$[653],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[654],0,z,0
      Pause 2200
    end if
  else if !objet[34] then %' crâne de la désolation
    Popup e$+RPGLBL$[655],0,z,0
    Pause 2200
  else
    Popup e$+RPGLBL$[656],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[657],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[658],0,z,0
    Pause 2200
  end if

elseif c=81 then %' fantôme du terrible pirate Roberts
  e$=RPGLBL$[659]
  if heroform<>1 then
    Popup e$+RPGLBL$[660],0,z,0
    Pause 2200
  else if perso[c]=3 & objet[43]=3 then
    Popup e$+RPGLBL$[661],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[662],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[663],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[664],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[665],0,z,0
    Pause 2200
    Gosub DelC
    Gosub Explosion
  else if perso[c]=3 then
    Popup e$+RPGLBL$[666],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[667],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[668],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[669],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[670],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[671],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[672],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[673],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[674],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[675],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[676],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[677],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[678],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[679],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[680],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[681],0,z,0
    Pause 2200
    perso[c]=4
  else if perso[c]=4 & victories<40 then
    Popup e$+RPGLBL$[682],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[683],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[684],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[685]+Stg$(40-victories)+RPGLBL$[686],0,z,0
    Pause 2200
  else if perso[c]=4 then
    Popup e$+RPGLBL$[687],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[688]+Stg$(victories)+RPGLBL$[689],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[690],0,z,0
    Pause 2200
    perso[c]=5
  else if perso[c]=5 then
    Popup e$+RPGLBL$[691],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[692],0,z,0
    Pause 2200
    if !objet[7] then
      Popup e$+RPGLBL$[693],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[694],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[695],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[696],0,z,0
      Pause 2200
    else
      Popup e$+RPGLBL$[697],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[698],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[699],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[700],0,z,0
      Pause 2200
      Popup e$+RPGLBL$[701],0,z,0
      Pause 2200
    end if
  else if perso[c]=6 | perso[c]=7 then
    Popup e$+RPGLBL$[702],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[703],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[704],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[705],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[706],0,z,0
    Pause 2200
  else if perso[c]=8 then
    Popup e$+RPGLBL$[707],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[708],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[709],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[710],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[711],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[712],0,z,0
    Pause 2200
  else if perso[c]>=9 then
    Popup e$+RPGLBL$[713],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[714],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[715],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[716],0,z,0
    Pause 2200
    Popup e$+RPGLBL$[717],0,z,0
    Pause 2200
  end if

elseif c=2 then %' fiancée venant d'être libérée
  gr.modify heroPtr,"bitmap",heroTile[1,16]
  c=3
  Gosub MovC
  if !perso[c] then
    perso[c]=1
    Goto SceneDeFin
  else
    Popup RPGLBL$[718],0,z,0
    Pause 2200
  end if

elseif c=3 then %' fiancée souriante
  Popup RPGLBL$[719],0,z,0
  Pause 2200

end if
!' [Fin du Script 3] FinScript3
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
if c then Goto BouclePrincipale
FinScript3:

bloque=0
!' Bloque sur les bords de la carte :
if (zl & herox=1) | (zr & herox=mapw) | (zu & heroy=1) | (zd & heroy=maph) then bloque=1
if GhostMode then Goto FinScript4 %' mode passe-muraille du menu triche
!' Bateau ne peut naviguer qu'en mer, eaux profondes, et s'aligner sur des pilotis :
if heroform=5 then
  for x=herox+dx+zr-zl-1 to herox+dx+zr-zl+1
    for y=heroy+dy+zd-zu to heroy+dy+zd-zu+1
      Gosub GetB
      if b<>56 & b<>58 & b<>14 then bloque=1
    next y
  next x
  Goto FinScript4
else %' A pied
  x=herox + dx + zr - zl
  y=heroy + dy + zd - zu
  Gosub GetB
  Gosub GetS
  Gosub GetO
  destb=b %' pour éviter de recalculer après chaque script (où b/s/o peuvent changer)
  dests=s
  desto=o
end if
z=-3*tileh*scale_h
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
!' [Script 4][AVANT déplacement] La SOLIDITE du tile de décor de destination est testée
!'      b=(background) tile du décor de destination ; s=solidité de ce tile
!'      o=éventuel objet présent à destination
!'      "bloque=1" stoppe le déplacement à venir du héros
!'      destx, desty=coordonnées de destination du héros
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
if s=1 then
  bloque=1
elseif s=2 & (!objet[14] | hereform<>1) then %' fixed v1.01 seul l'humain peut se déplacer dans les brumes magiques
  bloque=1
elseif s=3 & (!objet[15] | hereform<>1) then %' fixed v1.01 seul l'humain peut se déplacer dans les brumes magiques
  bloque=1
else if s=4 & (!objet[16] | hereform<>1) then %' fixed v1.01 seul l'humain peut se déplacer dans les brumes magiques
  bloque=1
elseif s=13 & heroform=3 then
  bloque=1
elseif s=14 & heroform<>4 then
  bloque=1
elseif s=15 & heroform<>3 & heroform<>4 then
  bloque=1
elseif s=16 & heroform<>2 & heroform<>4 then
  bloque=1
elseif s=17 & heroform=2 then
  bloque=1
elseif s=18 & heroform=1 then
  bloque=1
elseif s=19 & heroform<>1 then
  bloque=1
elseif s=20 & heroform=3 then
  bloque=1
end if

!' loup creuse un coeur au pied des troncs d'arbre en forêt
if heroform=2 & b=24 then
  gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+2]
  if light then Gosub FiatLux else gr.render
  Pause 300
  gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+1]
  if light then Gosub FiatLux else gr.render
  Pause 300
  gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+2]
  if light then Gosub FiatLux else gr.render
  Pause 300
  gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+1]
  if light then Gosub FiatLux else gr.render
  Pause 300
  Gosub DeterreUnCoeur
  Goto BouclePrincipale
end if

!' Prise en compte blocs de glace, poussoirs-ressorts, et leviers d'aiguillage
pousseBloc=0
if !bloque & heroform<=3 then %' l'aigle vole par dessus tous ces objets
  if o>=37 & o<=40 then %' destination du héros=poussoir-ressort enfoncé par bloc de glace
    bloque=1
  elseif o>=45 & o<=50 then %' destination=poussoir-ressort ou levier d'aiguillage
    bloque=1
  elseif o=35 then %' destination du héros=bloc de glace seul
    x=x+zr-zl
    y=y+zd-zu
    Gosub GetB
    Gosub GetO
    Gosub GetC
    if c then
      bloque=1 %' le héros ne peut pas pousser un bloc sur un perso
    elseif o & (o<45 | o>48) then
      bloque=1 %' le héros ne peut pas pousser un bloc sur un autre objet (hors poussoirs-ressorts)
      if b=65 then bloque=0 %' sauf si ledit objet est sur des pics auquel cas il peut
    elseif (o=45 & !zl) | (o=46 & !zr) | (o=47 & !zu) | (o=48 & !zd) then
      bloque=1 %' le héros ne peut pousser un bloc sur un poussoir-ressort que dans la bonne direction
    elseif !Is_In(","+Stg$(b)+",", BgnBloc$) then %' ne peut pas pousser de bloc sur ces tiles de décor
      bloque=1
    end if
    if !bloque then pousseBloc=1
    bb=b %' propriétés du tile de destination du *BLOC DE GLACE*
    oo=o
    cc=c
    b=destb %' propriétés du tile de destination du *HEROS*
    o=desto
  end if
end if
!' [Fin du Script 4] FinScript4
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
FinScript4:

!'---------------------------------------------------------------------------------------------------
!' Change l'obscurité courante
!'---------------------------------------------------------------------------------------------------
Gosub FromDuskTillDawn
dkns=dknsTgt
gr.modify dknsPtr,"alpha",dkns

!'---------------------------------------------------------------------------------------------------
!' Héros en mouvement
!'---------------------------------------------------------------------------------------------------
if !bloque then
  if pousseBloc then %' le héros pousse un bloc de glace
    destx=herox-1+zr-zl
    desty=heroy-1+zd-zu
    Gosub DelO %' supprime du décor le bloc de glace à pousser puis le redessine en surimpression
    gr.modify blocPtr, "x", (Floor(nthor/2)+zr-zl)*tilew
    gr.modify blocPtr, "y", (Floor(ntver/2)+zd-zu)*tileh
    gr.show blocPtr
    if !mute then
      if heroform=3 then
        Soundpool.play tmp,sndWav,0.99,0.99,1,0,1 %' son ressac
      else
        Soundpool.play tmp,sndPou,0.99,0.99,1,0,1 %' son pousse bloc sur terre ferme
      end if
    end if
  end if
end if
!' prépare le nouveau décor (bords de carte mis à jour pendant le déplacement)
gr.color 255,0,0,0,1
gr.bitmap.create tmpBmp, (nthor+2)*tilew, (ntver+2)*tileh
gr.bitmap.drawinto.start tmpBmp
gr.bitmap.draw nul, bgnBmp, 0, 0
gr.bitmap.drawinto.end
gr.modify bgnPtr, "bitmap", tmpBmp
gr.modify bgnPtr,"x",-tilew
gr.modify bgnPtr,"y",-tileh
if !bloque then
  herox=herox + zr - zl
  heroy=heroy + zd - zu
  gr.bitmap.crop nul, bgnBmp, tilew*zr, tileh*zd, (nthor+1+zu+zd)*tilew, (ntver+1+zl+zr)*tileh
  gr.bitmap.delete bgnBmp
  gr.bitmap.create bgnBmp, (nthor+2)*tilew, (ntver+2)*tileh
  gr.bitmap.drawinto.start bgnBmp
  gr.bitmap.draw tmpPtr, nul, tilew*zl, tileh*zu
  gr.bitmap.drawinto.end
  gr.bitmap.delete nul
end if
!' déplacement horizontal
if zr | zl then
  if heroform=5 then gr.modify heroPtr,"bitmap",heroTile[heroform,3+zl]
  x=herox+dx+(zr-zl)*(Floor(nthor/2)+1)
  y=heroy+dy-Floor(ntver/2)-2
  z=1
  for w=1 to ntver+2
    y=y+1
    Gosub AfficherDecor
    if heroform<>5 then
      if w>=0.25*(ntver+2) & z=1 then
        z=2
        gr.modify heroPtr,"bitmap",heroTile[heroform,_right*zr+_left*zl+2]
      elseif w>=0.5*(ntver+2) & z=2 then
        z=3
        gr.modify heroPtr,"bitmap",heroTile[heroform,_right*zr+_left*zl+3]
      elseif w>=0.75*(ntver+2) & z=3 then
        z=4
        gr.modify heroPtr,"bitmap",heroTile[heroform,_right*zr+_left*zl+2]
      end if
    end if
    if !bloque then gr.modify bgnPtr,"x",-tilew+Floor(w*tilew/(ntver+2))*(zl-zr)
    if Mod(w,Floor(ntver/3.25))=0 then
      if light then Gosub FiatLux else gr.render
    end if
  next w
  if heroform<>5 then gr.modify heroPtr,"bitmap",heroTile[heroform,_right*zr+_left*zl+1]
  gr.bitmap.delete tmpBmp
  gr.modify bgnPtr,"bitmap",bgnBmp
  gr.modify bgnPtr,"x",-tilew
  gr.modify bgnPtr,"y",-tileh
  if light then Gosub FiatLux else gr.render
!' déplacement vertical
elseif zu | zd then
  if heroform=5 then gr.modify heroPtr,"bitmap",heroTile[heroform,1+zd]
  x=herox+dx-Floor(nthor/2)-2
  y=heroy+dy+(zd-zu)*(Floor(ntver/2)+1)
  w=1
  for z=1 to nthor+2
    x=x+1
    Gosub AfficherDecor
    if heroform<>5 then
      if z>=0.25*(nthor+2) & w=1 then
        w=2
        gr.modify heroPtr,"bitmap",heroTile[heroform,_down*zd+_up*zu+2]
      elseif z>=0.5*(nthor+2) & w=2 then
        w=3
        gr.modify heroPtr,"bitmap",heroTile[heroform,_down*zd+_up*zu+3]
      elseif z>=0.75*(nthor+2) & w=3 then
        w=4
        gr.modify heroPtr,"bitmap",heroTile[heroform,_down*zd+_up*zu+2]
      end if
    end if
    if !bloque then gr.modify bgnPtr,"y",-tileh+Floor(z*tileh/(nthor+2))*(zu-zd)
    if Mod(z,Floor(nthor/3.25))=0 then
      if light then Gosub FiatLux else gr.render
    end if
  next z
  if heroform<>5 then gr.modify heroPtr,"bitmap",heroTile[heroform,_down*zd+_up*zu+1]
  gr.bitmap.delete tmpBmp
  gr.modify bgnPtr,"bitmap",bgnBmp
  gr.modify bgnPtr,"x",-tilew
  gr.modify bgnPtr,"y",-tileh
  if light then Gosub FiatLux else gr.render
end if
!' Mise à jour du décor lorsque le héros pousse un bloc de glace
if !bloque then
  if pousseBloc then
    Gosub InitEcranDeJeu
    destx=destx + zr - zl
    desty=desty + zd - zu
    if oo>=45 & oo<=48 then %' le héros a poussé le bloc sur un poussoir-ressort
      if !mute then Soundpool.play tmp,sndPRi,0.99,0.99,1,0,1
      o=oo-8
      Gosub SetO
      Gosub EffetsBasculeObjetBinaire
    elseif bb=65 then %' le héros a poussé le bloc sur des pics
      if !mute then Soundpool.play tmp,sndMel,0.99,0.99,1,0,1
      b=10
      Gosub SetB
      Gosub Explosion
    elseif bb=20 then %' le héros a poussé le bloc dans la lave
      if !mute then Soundpool.play tmp,sndMel,0.99,0.99,1,0,1
      Gosub Explosion %' ne rien faire d'autre, not. ne pas recréer le bloc
    elseif bb=45 then %' le héros a poussé le bloc dans le vide
      if !mute then Soundpool.play tmp,sndFal,0.99,0.99,1,0,1
      Gosub Explosion %' ne pas recréer le bloc
    elseif bb=54 | bb=56 | bb=57 | bb=58 | bb=69 | bb=70 then %' a poussé le bloc dans l'eau
      if heroform<>3 then
        if !mute then Soundpool.play tmp,sndPlo,0.99,0.99,1,0,1 %' plouf!
      end if
      o=35 %' recréer le bloc poussé qui change juste de place
      Gosub SetO
    else
      o=35 %' recréer le bloc poussé qui change juste de place
      Gosub SetO
    end if
    b=destb %' pour calculer le tile de dommage SOUS LE HEROS (et pas sous le bloc)
    o=desto %' pour détecter la présence d'un objet sur le tile d'arrivée du HEROS (et pas du bloc)
  end if
  pousseBloc=0 %' le héros a fini de pousser le bloc de glace
end if

!'---------------------------------------------------------------------------------------------------
!' Gestion des dommages en fonction du tile de destination
!'---------------------------------------------------------------------------------------------------
if !bloque | hurtTmr then Gosub HurtMePlenty

CheckObjet:
!' Trouve un objet ?
x=herox + dx
y=heroy + dy
Gosub GetO
if !o then Goto FinScript6 %' aucun objet
if bloque then  %' héros bloqué : récupérer les coord. du tile où le héros voulait aller initialement
  destx=destx + zr - zl
  desty=desty + zd - zu
end if
z=4*tileh*scale_h
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
!' [Script 6][APRES déplacement] Le héros est arrivé sur un tile CONTENANT un OBJET
!'      o=objet du tile de destination (si première rencontre, jamais ramassé)
!'      destx, desty=coordonnées de destination du héros
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
if heroform=3 & o=31 & destx>100 & desty>110 & desty<162 then %' plaques de glace (eau gelée)
  Popup RPGLBL$[720],0,z,0
  Pause 2200
  Popup RPGLBL$[721],0,z,0
  Pause 2200
end if

if o=31 then %' tile d'objet transparente pour actions spéciales

  if heroform=1 & destx=18 & (desty=25 | desty=26) & perso[7]=1 then %' ouvre cité nord cont.ouest
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    desty=25 %' panneau supérieur de la porte de la cité du nord
    b=19
    Gosub SetB
    s=13
    Gosub SetS
    Gosub Explosion
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    desty=26 %' panneau inférieur de la porte de la cité du nord
    b=18
    Gosub SetB
    s=13
    Gosub SetS
    Gosub Explosion
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  else if heroform=1 & destx=4 & (desty=124 | desty=125) & perso[72]=3 then %' ouvre cité sud c.ouest
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    desty=124 %' panneau supérieur de la porte de la cité du sud
    b=19
    Gosub SetB
    s=13
    Gosub SetS
    Gosub Explosion
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    desty=125 %' panneau inférieur de la porte de la cité du sud
    b=18
    Gosub SetB
    s=13
    Gosub SetS
    Gosub Explosion
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  else if heroform=1 & destx=104 & desty=75 & perso[10]=3 then %' ouvre cité Zoraniscaeos (carte)
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    b=18 %' porte de la carte dans Zoraniscaeos ville
    Gosub SetB
    s=13
    Gosub SetS
    Gosub Explosion
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  else if heroform<3 & destx=23 & desty=108 & perso[72]=2 then %' pousse bloc de pierre chien de garde
    o=22 %' clé de la cité sud du continent de l'ouest
    Gosub ObjetMineurRebondit
    perso[19]=3
    perso[20]=3
    perso[72]=3
    Gosub DebloqueSecretMusique

  else if (destx=168 | destx=170) & desty=141 then %' entrée du passage vers le labyrinthe
    Popup RPGLBL$[722],0,z,0
    Pause 2200

  else if (destx=7 | destx=8) & desty>=236 & !perso[28] then %' combat final Vs le Seigneur Dragon
    Goto FinalFight

  else if destx=229 & desty=210 & perso[81]=1 then %' tile qui repousse le fantôme de Roberts
    perso[81]=2
    destx=225
    c=81
    Gosub MovC
    if !mute then Soundpool.play tmp,sndPRo,0.99,0.99,1,0,1
    Gosub Explosion

  else if destx=225 & (desty=209 | desty=211) & perso[81]=2 then %' repousse le fantôme de Roberts
    perso[81]=3
    destx=223
    desty=210
    c=81
    Gosub MovC
    if !mute then Soundpool.play tmp,sndPRo,0.99,0.99,1,0,1
    Gosub Explosion

  else if destx=203 & desty=249 & !objet[34] then %' face au dragon mort (toutes formes du héros)
    if !HerosEnHeaume then
      if !mute then Soundpool.play tmp,sndBnc,0.99,0.99,1,0,1
      gr.modify heroPtr,"bitmap",heroTile[heroform,_right+1] %' regarde à droite
      if light then Gosub FiatLux else gr.render
      Pause 10
      gr.modify heroPtr,"bitmap",heroTile[heroform,_left+1] %' regarde à gauche
      if light then Gosub FiatLux else gr.render
      Pause 500
      if !mute then Soundpool.play tmp,sndHur,0.99,0.99,1,0,1
      gr.modify heroPtr, "bitmap", heroTile[heroform,14] %' héros blessé
      pdv=0
      gr.modify pdvPtr, "right", tilew*(1.1+3*pdv/100)
      if light then Gosub FiatLux else gr.render
      Pause 2200
      e$=RPGLBL$[723]
      Goto TheEnd
    end if

  else if heroform<3 & destx=201 & desty=82 then %' (île-sable) ENTREE pyramide proche sorcière
    destx=137
    desty=249
    NewDir=_up
    Goto EntreSortBatiment
  else if heroform<3 & destx=137 & desty=249 then %' SORTIE
    destx=201
    desty=82
    NewDir=_down
    Goto EntreSortBatiment

  else if heroform<3 & destx=188 & desty=81 then %' (île-sable) ENTREE basse 2 pyramides ouest
    destx=136
    desty=230
    NewDir=_up
    Goto EntreSortBatiment
  else if heroform<3 & destx=136 & desty=230 then %' SORTIE
    destx=188
    desty=81
    NewDir=_down
    Goto EntreSortBatiment

  else if heroform<3 & destx=188 & desty=78 then %' (île-sable) ENTREE haute 2 pyramides ouest
    destx=136
    desty=216
    NewDir=_down
    Goto EntreSortBatiment
  else if heroform<3 & destx=136 & desty=216 then %' SORTIE
    destx=188
    desty=78
    NewDir=_left
    Goto EntreSortBatiment

  else if heroform<3 & destx=190 & desty=67 then %' (île-sable) ENTREE basse 2 pyramides nord
    destx=156
    desty=239
    NewDir=_up
    Goto EntreSortBatiment
  else if heroform<3 & destx=156 & desty=239 then %' SORTIE
    destx=190
    desty=67
    NewDir=_left
    Goto EntreSortBatiment

  else if heroform<3 & destx=192 & desty=66 then %' (île-sable) ENTREE haute 2 pyramides nord
    destx=163
    desty=229
    NewDir=_left
    Goto EntreSortBatiment
  else if heroform<3 & destx=163 & desty=229 then %' SORTIE
    destx=192
    desty=66
    NewDir=_down
    Goto EntreSortBatiment

  else if heroform<3 & destx=196 & desty=74 then %' (île-sable) ENTREE pyramide centrale
    destx=233
    desty=217
    NewDir=_up
    Goto EntreSortBatiment
  else if heroform<3 & destx=233 & desty=217 then %' SORTIE
    destx=196
    desty=74
    NewDir=_down
    Goto EntreSortBatiment

  else if heroform<3 & destx=194 & desty=85 then %' (île-sable) ENTREE pyramide au sud
    destx=233
    desty=198
    NewDir=_up
    Goto EntreSortBatiment
  else if heroform<3 & destx=233 & desty=198 then %' SORTIE
    destx=194
    desty=85
    NewDir=_down
    Goto EntreSortBatiment

  end if

elseif heroform<>1 then %' seul le héros sous sa forme humaine peut ramasser les objets
  o=0
  Goto FinScript6
elseif o=17 then %' pièce d'or (OBJET MULTIPLE)
  argent=1
  Gosub RamasseArgent
elseif o=12 then %' bourse (OBJET MULTIPLE)
  argent=10
  Gosub RamasseArgent
elseif o=14 then %' bague sylvestre (loup)
  e$=RPGLBL$[724]
  Gosub TrouveObjet
elseif o=18 then %' sphère du loup
  e$=RPGLBL$[725]
  Gosub TrouveObjet
elseif o=9 then %' bouclier
  e$=RPGLBL$[726]
  Gosub TrouveObjet
elseif o=11 then %' carte maritime
  e$=RPGLBL$[727]
  Gosub TrouveObjet
elseif o=10 then %' dague
  e$=RPGLBL$[728]
  Gosub TrouveObjet
elseif o=16 then %' bague sanguine (poisson)
  e$=RPGLBL$[729]
  Gosub TrouveObjet
elseif o=27 then %' bougie
  e$=RPGLBL$[730]
  Gosub TrouveObjet
elseif o=15 then %' bague sulfure (aigle)
  e$=RPGLBL$[731]
  Gosub TrouveObjet
elseif o=5 then %' pointe de givre
  e$=RPGLBL$[732]
  Gosub TrouveObjet
elseif o=28 then %' oeil de ra
  e$=RPGLBL$[733]
  Gosub TrouveObjet
elseif o=29 then %' armure du guerrier
  e$=RPGLBL$[734]
  Gosub TrouveObjet
elseif o=30 then %' épée légendaire
  e$=RPGLBL$[735]
  Gosub TrouveObjet
elseif o=19 then %' sphère de l'aigle
  e$=RPGLBL$[736]
  Gosub TrouveObjet
elseif o=33 then %' heaume étincelant
  e$=RPGLBL$[737]
  Gosub TrouveObjet
elseif o=25 then %' runes élémentales
  e$=RPGLBL$[738]
  Gosub TrouveObjet
elseif o=8 then %' conque du vent (relique élémentale n°1)
  e$=RPGLBL$[739]
  Gosub TrouveObjet
elseif o=4 then %' croix de l'eau (relique élémentale n°2)
  e$=RPGLBL$[740]
  Gosub TrouveObjet
elseif o=26 then %' miroir des glaces (relique élémentale n°3)
  e$=RPGLBL$[741]
  Gosub TrouveObjet
elseif o=44 then %' émeraude de Roberts
  e$=RPGLBL$[742]
  Gosub TrouveObjet
elseif o=7 then %' crâne du terrible pirate Roberts
  e$=RPGLBL$[743]
  Gosub TrouveObjet
  if perso[81]=5 then %' en mission pour Roberts
    Pause 1000
    perso[81]=6
    c=81
    destx=142
    desty=144
    Gosub MovC
    if !mute then Soundpool.play tmp,sndPRo,0.99,0.99,1,0,1
    Gosub Explosion
  end if
elseif o=24 then %' carte du labyrinthe
  e$=RPGLBL$[744]
  Gosub TrouveObjet
elseif o=20 then %' sphère du poisson
  !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  c=29 %' déplace le navigateur au radeau près de l'île-sable
  destx=213
  desty=77
  Gosub MovC
  c=30
  destx=214
  desty=77
  Gosub MovC
  c=39
  destx=213
  desty=78
  Gosub MovC
  c=40
  destx=214
  desty=78
  Gosub MovC
  !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  perso[40]=2
  e$=RPGLBL$[745]
  Gosub TrouveObjet
end if
!' [Fin du Script 6] FinScript6
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
if o then Goto BouclePrincipale
FinScript6:

if tricheZeroEnnemi then Goto BouclePrincipale
if heroform>=4 then Goto BouclePrincipale %' pas de combat en aigle ou en bateau
if bloque then Goto BouclePrincipale %' pas de combat si bloqué
if !combat then combat=4 + (130 - difficulte) / 10 + Ceil(10 * Rnd()) / 3
tour=tour + 1
if tour<combat then Goto BouclePrincipale %' pas encore sur un tour "rencontre d'ennemi"
tour=0 %' on est sur un tour "rencontre d'ennemi" -> on ràz les compteurs
combat=0
if 100 * Rnd()>difficulte then Goto BouclePrincipale %' rencontre trop fréquente Vs difficulté choisie
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
!' [Script 7][APRES déplacement] La routine de rencontre aléatoire des ENNEMIS a décidé d'un COMBAT !
!'      b=(background) tile de décor sur lequel le héros se tient
!'      destx, desty=coordonnées actuelles du héros
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
if b=1 | b=7 then %' plaine du continent de l'ouest
  if Rnd()<0.5 then
    e$=RPGLBL$[746]
    ennBmp1=chrTile[4] %' araignée
    ennBmp2=chrTile[5]
    ennPdv=20+20*Rnd()
    ennAta=1
    ennDef=1
  else
    e$=RPGLBL$[747]
    ennBmp1=chrTile[14] %' serpent
    ennBmp2=chrTile[15]
    ennPdv=30+20*Rnd()
    ennAta=2
    ennDef=1
  end if

elseif b=12 then %' plage
  e$=RPGLBL$[748]
  ennBmp1=chrTile[77] %' crabe
  ennBmp2=chrTile[78]
  ennPdv=20+20*Rnd()
  ennAta=1
  ennDef=1

elseif b=26 | b=28 then %' montagnes de Zoraniscaeos
  if Rnd()<0.5 then
    e$=RPGLBL$[749]
    ennBmp1=chrTile[75] %' chauve-souris
    ennBmp2=chrTile[76]
    ennPdv=30+30*Rnd()
    ennAta=2
    ennDef=1
  else
    e$=RPGLBL$[750]
    ennBmp1=chrTile[86] %' rat
    ennBmp2=chrTile[87]
    ennPdv=30+30*Rnd()
    ennAta=2
    ennDef=1
  end if

elseif b=56 then %' pleine mer
  if Rnd()<0.5 then
    e$=RPGLBL$[751]
    ennBmp1=chrTile[6] %' requin
    ennBmp2=chrTile[16]
    ennPdv=60+50*Rnd()
    ennAta=6
    ennDef=1
  else
    e$=RPGLBL$[752]
    ennBmp1=chrTile[82] %' pieuvre
    ennBmp2=chrTile[83]
    ennPdv=40+60*Rnd()
    ennAta=3
    ennDef=2
  end if

elseif b=41 then %' désert de l'île-sable et de l'île au dragon
  if Rnd()<0.5 then
    e$=RPGLBL$[753]
    ennBmp1=chrTile[21] %' momie
    ennBmp2=chrTile[31]
    ennPdv=60+20*Rnd()
    ennAta=3
    ennDef=2
  else
    e$=RPGLBL$[754]
    ennBmp1=chrTile[22] %' squelette
    ennBmp2=chrTile[32]
    ennPdv=60+20*Rnd()
    ennAta=3
    ennDef=3
  end if

elseif b=33 | b=34 then %' île-marais
  if Rnd()<0.5 then
    e$=RPGLBL$[755]
    ennBmp1=chrTile[61] %' alligator
    ennBmp2=chrTile[63]
    ennPdv=80+20*Rnd()
    ennAta=5
    ennDef=4
  else
    e$=RPGLBL$[756]
    ennBmp1=chrTile[84] %' plante carnivore
    ennBmp2=chrTile[85]
    ennPdv=50+40*Rnd()
    ennAta=2
    ennDef=3
  end if

elseif b=57 | b=59 | b=36 then %' plaques de glace (eau gelée) et banquise
  if Rnd()<0.5 then
    e$=RPGLBL$[757]
    ennBmp1=chrTile[65] %' pingouin tueur
    ennBmp2=chrTile[66]
    ennPdv=70+10*Rnd()
    ennAta=6
    ennDef=3
  else
    e$=RPGLBL$[758]
    ennBmp1=chrTile[67] %' gnome des glaces
    ennBmp2=chrTile[68]
    ennPdv=70+10*Rnd()
    ennAta=7
    ennDef=4
  end if

else
  Goto BouclePrincipale %' pas d'ennemi sur ce type de tile de décor
end if
!' [Fin du Script 7] FinScript7
!'¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
FinScript7:
Goto FightClub
!'---------------------------------------------------------------------------------------------------

TrouveObjet:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine du Script 6 lorsque le héros trouve un objet qui enrichit son équipement
!'---------------------------------------------------------------------------------------------------
z=4*tileh*scale_h
destx=herox - 1
desty=heroy - 1
Popup e$,0,z,4
if !mute then
  audio.load snd, "TrouveObjet.mp3"
  audio.stop
  audio.play snd
end if
!' Redessine le décor en enlevant l'objet
gr.modify heroPtr,"bitmap",heroTile[heroform,15]
Gosub DelO
!' Animation : l'objet s'élève dans les airs
gr.bitmap.draw tmpPtr, objTile[o], (di_w - tilew)/2, (di_h - tileh)/2
for y=(di_h - 2*tileh)/2 to padsize*tileh*0.5 Step -1
  gr.modify tmpPtr, "y", y
  if light then Gosub FiatLux else gr.render
  Pause 2
next y
if !mute then
  do
    Audio.isdone i
  until i
  audio.stop
  audio.release snd
  if music$<>"" then
    audio.play musPtr %' Reprendre la musique de fond depuis le début
    audio.loop
  end if
end if
DerDir=_down %' héros regarde vers le bas
gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+1]
!' Animation : l'objet regagne l'inventaire
for x=(di_w - tilew)/2 to di_w - padsize*tilew*1.25 Step tilew/2
  gr.modify tmpPtr, "x", x
  if light then Gosub FiatLux else gr.render
  Pause 2
next y
Gosub InitEcranDeJeu
if light then Gosub FiatLux else gr.render
!' Mise à jour de l'inventaire
objet[o]=1
neq=neq + 1
equipement[neq]=o
if neq<=(nthor-3)/2 then
  x=neq-1
  y=0
else
    w=1+(nthor-3)/2
    x=Mod(neq,w)
    y=(neq-x)/w
end if
gr.color 255,0,0,0,1
gr.bitmap.scale c,objTile[o],1.8*tilew,1.8*tileh, 0
gr.bitmap.drawinto.start equBmp
gr.bitmap.draw i,c,(2*x+0.1)*tilew,(2*y+0.1)*tileh
gr.bitmap.drawinto.end
gr.bitmap.delete c
Return
!'---------------------------------------------------------------------------------------------------

ObjetMineurRebondit:
!'---------------------------------------------------------------------------------------------------
!' Le héros trouve un objet mineur qui rebondit, typiquement donné par un personnage
!'      destx, desty=coordonnées où se trouve l'objet
!'---------------------------------------------------------------------------------------------------
if !mute then Soundpool.play tmp,sndBnc,0.99,0.99,1,0,1
gr.color 255,0,0,0,1
gr.bitmap.draw tmpPtr, objTile[o], (di_w - tilew)/2, (di_h - tileh)/2
for x=(destx-herox+1)*tilew to (destx-herox+2.5)*tilew %' Animation
  y=x/tilew+herox-destx
  gr.modify tmpPtr, "x", di_w/2 + x - tilew/2
  gr.modify tmpPtr, "y", di_h/2 - 3*tileh*Abs(Sin(Pi*2*(y-1)))/y + (desty-heroy+1)*tileh
  if light then Gosub FiatLux else gr.render
  Pause 16
next x
Pause 250
Gosub InitEcranDeJeu
if light then Gosub FiatLux else gr.render
Return
!'---------------------------------------------------------------------------------------------------

RamasseArgent:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine du Script 6 lorsque le héros ramasse de l'argent
!'---------------------------------------------------------------------------------------------------
objet[oIndx]=1
if !mute then Soundpool.play tmp,sndUsd,0.50,0.50,1,0,1
Gosub DelO
!' Animation
gr.bitmap.draw tmpPtr, objTile[o], (di_w - tilew)/2, (di_h - tileh)/2
for x=(di_w - tilew)/2 to padsize*tilew*1.25 Step -tilew/2
  y=x/di_w*di_h
  gr.modify tmpPtr, "x", x
  gr.modify tmpPtr, "y", y
  if light then Gosub FiatLux else gr.render
  Pause 2
next y
gr.hide tmpPtr
if light then Gosub FiatLux else gr.render
!' Mise à jour de la bourse du héros
usd=usd + argent
if usd>=2^16 then usd=2^16-1
Gosub RefreshUsd
Return
!'---------------------------------------------------------------------------------------------------

FightClub:
!'---------------------------------------------------------------------------------------------------
!' Sous-routine du Script 7 lorsque le héros tombe sur un ennemi et engage un combat !
!'---------------------------------------------------------------------------------------------------
gr.cls %' Call gClear()
coeurPtr=0
flashPtr=0
gr.color 255,0,0,0,1
ennAta=Floor(ennAta*(1+difficulte/100)+0.5)
ennDef=Floor(ennDef*(1+difficulte/100)+0.5)
ennPdv=Floor(ennPdv*(1+difficulte/100)+0.5)
ennMaxPdv=ennPdv
if !mute then audio.stop
gr.bitmap.draw bgnPtr,bgnBmp,-tilew,-tileh
gr.rect dknsPtr, 0, 0, di_w, di_h
gr.modify dknsPtr,"alpha",dkns*(255-light)/255
gr.bitmap.draw lightPtr,lightBmp,0,0
gr.modify lightPtr,"alpha",light*dkns/255
if objet[28] then %' oeil de Ra
  gr.bitmap.draw tmpPtr, objTile[28], (di_w-tilew)/2, (di_h-tileh)/2
  gr.hide tmpPtr
end if
gr.bitmap.draw heroPtr, heroTile[heroform,_down+1], Floor(nthor/2)*tilew, Floor(ntver/2)*tileh
if GhostMode then gr.modify heroPtr, "alpha", 128
gr.render
Pause 1000
if !mute then Soundpool.play tmp,sndBnc,0.99,0.99,1,0,1 %' regarde à droite
gr.modify heroPtr, "bitmap", heroTile[heroform,_right+1]
gr.render
Pause 500
if !mute then Soundpool.play tmp,sndBnc,0.99,0.99,1,0,1 %' regarde à gauche
gr.modify heroPtr, "bitmap", heroTile[heroform,_left+1]
gr.render
Pause 500
gr.modify heroPtr, "bitmap", heroTile[heroform,_down+1]
!' Animation : ennemi tombe sur le héros
gr.bitmap.draw ennPtr, ennBmp1, (di_w - tilew)/2, -tileh
for y=-tileh to (di_h - 3*tileh)/2
  gr.modify ennPtr, "y", y
  if Mod(y,2*tileh)=tileh then
    gr.modify ennPtr, "bitmap", ennBmp1
  elseif Mod(y,2*tileh)=0 then
    gr.modify ennPtr, "bitmap", ennBmp2
  end if
  gr.render
  Pause 4
next y
!' Animation : héros touché ou pas ?
gr.modify ennPtr, "bitmap", ennBmp2
if objet[28] then %' oeil de ra
  if !mute then Soundpool.play tmp,sndShi,0.99,0.99,1,0,1 %' premier coup évité !
  gr.show tmpPtr
  gr.modify heroPtr, "bitmap", heroTile[heroform,_up+2]
  for y=(di_h - 3*tileh)/2 to (di_h - tileh)/2
    gr.modify heroPtr, "y", y+tileh
    gr.render
    Pause 4
  next y
  Pause 400
  gr.modify heroPtr, "y", (di_h - tileh)/2
  if !mute then Soundpool.play tmp,sndSwo,0.99,0.99,1,0,1 %' 1er coup de l'ennemi retourné contre lui!
  e$="FF0000" %' voile rouge
  Gosub Flash
  fighter=1 %' héros
  Pause 1000
  gr.hide tmpPtr
else
  if !mute then Soundpool.play tmp,sndHur,0.99,0.99,1,0,1 %' l'ennemi a porté le premier coup !
  gr.modify heroPtr, "bitmap", heroTile[heroform,14]
  for y=(di_h - 3*tileh)/2 to (di_h - 2*tileh)/2
    gr.modify ennPtr, "y", y
    gr.modify heroPtr, "y", y+tileh
    gr.render
    Pause 4
  next y
  fighter=0 %' ennemi
  Pause 2200
end if
!' Musique de combat !
if !mute then
  audio.stop
  if musPtr then audio.release musPtr
  audio.load musPtr, "ennemi"+Stg$(Mod((victories-Mod(victories,10))/10,8))+".mid"
  audio.play musPtr
  audio.loop
end if
!' Animation : héros et ennemi chacun vers un coin d'écran
gr.modify heroPtr, "bitmap", heroTile[heroform,_left+1]
for x=0 to di_w/2 - 1.5 * tilew step tilew/6
  y=x / (di_w - tilew) * (di_h - tileh)
  alpha=180*x/(di_w/2-1.5*tilew)
  if alpha<dkns*(255-light)/255 then alpha=dkns*(255-light)/255
  gr.modify dknsPtr, "alpha", alpha
  gr.modify ennPtr, "x", (di_w-tilew)/2 - x
  gr.modify ennPtr, "y", (di_h-tileh)/2 - y
  gr.modify heroPtr, "x", (di_w-tilew)/2 + x
  gr.modify heroPtr, "y", (di_h-tileh)/2 + y
  gr.render
  Pause 4
next x
!' Préparation de l'écran de combat
gr.cls %' Call gClear()
flashPtr=0
gr.color 255,0,0,0,1
gr.bitmap.draw bgnPtr,bgnBmp,-tilew,-tileh
gr.rect dknsPtr, 0, 0, di_w, di_h
gr.modify dknsPtr,"alpha",alpha
light=0 %' annule la baisse de luminosité (si la bougie est en train de s'éteindre)
!' Barre de PDV + ATA/DEF du HEROS
gr.color 175,255,0,0,1
gr.bitmap.draw tmpPtr,coeurBmp,di_w-tilew*5,0 %' coeur
gr.rect pdvPtr, di_w-tilew*3.9, tileh*0.4, di_w-tilew*0.9, tileh*0.6
gr.modify pdvPtr, "right", di_w-tilew*(3.9-3*pdv/100)
gr.color 175,200,200,200,0
gr.rect tmpPtr, di_w-tilew*3.9, tileh*0.4, di_w-tilew*0.9, tileh*0.6
if heroform=1 then
  ata=0
  if objet[10] then ata=ata+3
  if objet[30] then ata=ata+5
  def=1
  if objet[9] then def=def+3
  if objet[29] then def=def+4
elseif heroform=2 then
  ata=6
  def=4
elseif heroform=3 then
  ata=2
  def=3
end if
nbCoupsAta=2 + (victories - Mod(victories,10))/10
coupsDoubles=1
gr.text.size 8
Call Label(di_w-tilew*4.7,1.4*tileh,Stg$(nbCoupsAta)+RPGLBL$[759])
gr.color 255,255,255,255,1
gr.circle dbl1, di_w-tilew*0.3, 1.1*tileh, 0.3*tilew
gr.color 255,0,0,0,1
gr.text.size 8
gr.text.align 2
gr.text.draw dbl2, di_w-tilew*0.3, 1.2*tileh, "x2"
gr.text.align 1
gr.hide dbl1
gr.hide dbl2
gr.text.size 14
Call Label(di_w-tilew*5,2.6*tileh,"ATA: +"+Stg$(ata))
Call Label(di_w-tilew*5,3.6*tileh,"DEF: x"+Stg$(def))
if heroform=1 then
  if objet[10] then gr.bitmap.draw tmp, objTile[10], di_w-2.2*tilew, 1.7*tileh
  if objet[30] then gr.bitmap.draw tmp, objTile[30], di_w-1.1*tilew, 1.7*tileh
  if objet[9] then gr.bitmap.draw tmp, objTile[9], di_w-2.2*tilew, 2.8*tileh
  if objet[29] then gr.bitmap.draw tmp, objTile[29], di_w-1.1*tilew, 2.8*tileh
end if
!' Icône "Oeil de Ra"
raUsed=0
if OeilRaBmp then gr.bitmap.delete OeilRaBmp
if objet[28] then
  gr.color 75,255,255,255,1
  gr.rect zu, di_w-padsize*tilew*6, padsize*tileh, di_w-padsize*tilew*4.5, padsize*tileh*2.5
  gr.color 75,0,0,0,0
  gr.set.stroke 2
  gr.rect zd, di_w-padsize*tilew*6, padsize*tileh, di_w-padsize*tilew*4.5, padsize*tileh*2.5
  gr.set.stroke 1
  gr.color 155,0,0,0,0
  gr.bitmap.scale OeilRaBmp,objTile[28],padsize*tilew*1.5,padsize*tileh*1.5, 0
  gr.bitmap.draw zr, OeilRaBmp, di_w-padsize*tilew*6, padsize*tileh
  gr.set.stroke 0
  gr.hide zu
  gr.hide zd
  gr.hide zr
end if
!' Barre de PDV + ATA/DEF de l'ENNEMI
gr.color 175,72,0,255,1
gr.bitmap.draw tmpPtr,coeurBmp,0,di_h-3.5*tileh %' coeur
gr.rect ennPdvPtr, tilew*1.1, di_h-3.1*tileh, tilew*4.1, di_h-2.9*tileh
gr.modify ennPdvPtr, "right", tilew*(1.1+3*ennPdv/ennMaxPdv)
gr.color 175,200,200,200,0
gr.rect tmpPtr, tilew*1.1, di_h-3.1*tileh, tilew*4.1, di_h-2.9*tileh
gr.text.size 14
Call Label(tilew*0.5,di_h-1.7*tileh,"ATA: "+Stg$(ennAta))
if heroform=1 & objet[32] then gr.text.strike 1 %' masse d'arme en humain
Call Label(tilew*0.5,di_h-0.7*tileh,"DEF: x"+Stg$(ennDef))
if heroform=1 & objet[32] then %' masse d'arme en humain
  gr.text.width w, "DEF: x"+Stg$(ennDef))
  gr.text.strike 0
  gr.color 255,255,255,255,1
  gr.rect tmpPtr, tilew*0.8+w, di_h-1.5*tileh, tilew*2+w, di_h-0.3*tileh
  gr.bitmap.draw tmpPtr, objTile[32], tilew*0.9+w, di_h-1.4*tileh
  Call Label(tilew*2.2+w,di_h-0.7*tileh,"x1")
  ennDef=1
end if
!' Zone tactile pour les combats
gr.color 100,238,116,255,1
gr.circle t, 0, 0, padsize*tilew
gr.hide t
gr.circle tt, 0, 0, padsize*tilew
gr.hide tt
gr.color 100,255,255,255,1
gr.circle u, 0, 0, padsize*tilew
gr.hide u
gr.circle uu, 0, 0, padsize*tilew
gr.hide uu
gr.color 100,255,0,0,1
gr.circle v, 0, 0, padsize*tilew
gr.hide v
!' Labels du nb de coups portés
gr.color 255,255,255,255,1
gr.text.bold 1
gr.text.draw t1,0,0,""
gr.hide t1
gr.text.draw t2,0,0,""
gr.hide t2
gr.text.draw t3,0,0,""
gr.hide t3
gr.text.underline 1
gr.text.draw t4,0,0,""
gr.text.underline 0
gr.hide t4
gr.text.bold 0
!' Agrandissement des combattants
gr.color 255,0,0,0,1
if ennBigBmp then gr.bitmap.delete ennBigBmp
gr.bitmap.size ennBmp1, ennw, ennh
gr.bitmap.scale ennBigBmp, ennBmp1, ennw*4, ennh*4, 0
gr.bitmap.draw ennPtr, ennBigBmp, tilew/2, tileh/2
if heroBigBmp then gr.bitmap.delete heroBigBmp
gr.bitmap.scale heroBigBmp, heroTile[heroform,_left+1], tilew*4, tileh*4, 0
gr.bitmap.draw heroPtr, heroBigBmp, di_w-4.5*tilew, di_h-4.5*tileh
if GhostMode then gr.modify heroPtr, "alpha", 128
gr.render

!' Combat !!
do
  if fighter then %' C'est au héros d'attaquer
    Popup RPGLBL$[764],0,0,0
    Pause 3000
    coups=0
    for i=1 to nbCoupsAta * coupsDoubles
      y=padsize*tilew+Floor(6*Rnd())*(ntver*tileh-2*padsize*tilew)/5
      x=0.5*(di_h-y)*di_w/di_h*Rnd() + padsize*tilew
      gr.modify u,"x",x
      gr.modify u,"y",y
      gr.modify v,"x",x
      gr.modify v,"y",y
      gr.show u
      gr.render
      tmr=Clock()
      do
        Gosub TestCombatEnArrierePlan
        gr.bounded.touch tmp,(x-0.8*padsize*tilew)*scale_w,(y-0.8*padsize*tilew)*scale_h,(x+0.8*padsize*tilew)*scale_w,(y+0.8*padsize*tilew)*scale_h
      until tmp | Clock()-tmr>500
      if tmp then
        if !mute then Soundpool.play tmp,sndSwo,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
        gr.hide u
        gr.show v
        gr.render
        coups=coups+1
        Pause 200
      end if
      gr.hide u
      gr.hide v
      gr.render
    next i
    if coups then %' L'ennemi est touché
      gr.modify t1,"text",Stg$(coups)+RPGLBL$[760]
      gr.modify t1,"x",5*tilew
      gr.show t1
      for y=di_h+tileh to 5*tileh step -tileh/2
        gr.modify t1, "y", y
        gr.render
        Pause 4
      next y
      Pause 500
      gr.modify t2,"text","+"+Stg$(ata)+RPGLBL$[761]
      gr.modify t2,"x",5*tilew
      gr.show t2
      for y=di_h+0.25*tileh to 6.25*tileh step -tileh/2
        gr.modify t2, "y", y
        gr.render
        Pause 4
      next y
      Pause 500
      gr.modify t3,"text","/"+Stg$(ennDef)+RPGLBL$[762]
      gr.modify t3,"x",5*tilew
      gr.show t3
      for y=di_h+tileh to 7.5*tileh step -tileh/2
        gr.modify t3, "y", y
        gr.render
        Pause 4
      next y
      Pause 500
      gr.modify t4,"text","="+Stg$(10*(coups+ata)/ennDef)+RPGLBL$[763]+Stg$(ennPdv)
      gr.modify t4,"x",5*tilew
      gr.show t4
      for y=di_h+0.75*tileh to 8.75*tileh step -tileh/2
        gr.modify t4, "y", y
        gr.render
        Pause 4
      next y
      Pause 1000
      gr.hide t1
      gr.hide t2
      gr.hide t3
      gr.hide t4
      e$="FF0000" %' voile rouge
      Gosub Flash
      ennPdv=Floor(ennPdv-10*(coups+ata)/ennDef)
      if ennPdv<=0 then
        ennPdv=0
        gr.hide ennPtr
      end if
      gr.modify ennPdvPtr, "right", tilew*(1.1+3*ennPdv/ennMaxPdv)
      gr.render
      if ennPdv=0 then
        victories=victories+1 %' une nouvelle victoire ! :o)
        if heroform=1 then
          gr.bitmap.delete heroBigBmp
          gr.bitmap.scale heroBigBmp, heroTile[heroform,15], tilew*4, tileh*4, 0
          gr.modify heroPtr, "bitmap", heroBigBmp
          gr.render
        end if
        if !mute then
          audio.load snd, "Victory.mp3"
          audio.stop
          audio.release musPtr
          audio.play snd
          do
            Audio.isdone i
          until i
          audio.stop
          audio.release snd
        else
          for i=1 to 3
            vibrate VibrCrt[], -1
            Pause 150
          next i
        end if
        Pause 200
        gr.bitmap.delete heroBigBmp
        gr.bitmap.scale heroBigBmp, heroTile[heroform,DerDir+1], tilew*4, tileh*4, 0
        gr.modify heroPtr, "bitmap", heroBigBmp
        gr.render
        Pause 200
      end if
    else %' L'ennemi n'a pas été touché
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1
      gr.modify t1,"text",RPGLBL$[773]
      gr.modify t1,"x",5*tilew
      gr.show t1
      for y=di_h+tileh to 6*tileh step -tileh/2
        gr.modify t1, "y", y
        gr.render
        Pause 4
      next y
      Pause 1000
      gr.hide t1
      gr.render
    end if
    fighter=1-fighter
    coupsDoubles=1
    gr.hide dbl1
    gr.hide dbl2
    !' Utilisation de l'oeil de ra ?
    if ennPdv>0 & objet[28] & !raUsed then
      for i=1 to 8
        if Mod(i,2)=1 then
          gr.show zu
          gr.show zd
          gr.show zr
        else
          gr.hide zu
          gr.hide zd
          gr.hide zr
        end if
        gr.render
        tmr=Clock()
        do
          Gosub TestCombatEnArrierePlan
          gr.bounded.touch tmp, (di_w-padsize*tilew*6)*scale_w, padsize*tileh*scale_h, (di_w-padsize*tilew*4.5)*scale_w, padsize*tileh*2.5*scale_h
        until tmp | Clock()-tmr>500
        if tmp then
          gr.show zu
          gr.show zd
          gr.show zr
          gr.render
          if !mute then Soundpool.play tmp,sndSwo,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
          Pause 150
          if !mute then Soundpool.play tmp,sndSwo,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
          Pause 150
          raUsed=1
          fighter=1-fighter %' Encore au héros de frapper !
          Pause 500
          gr.hide zu
          gr.hide zd
          gr.hide zr
          gr.render
          Pause 500
          F_n.break
        end if
      next i
    end if

  else %' C'est à l'ennemi d'attaquer et au héros de se défendre
    Popup RPGLBL$[765],0,0,0
    Pause 3000
    coups=0
    for i=1 to Floor(ennAta)
      a1=(4+padsize)*tilew + 0.5*(di_w - 2*(4+padsize)*tilew)*Rnd()
      b1=(2+padsize)*tilew + 0.5*(di_h - 2*(2+padsize)*tilew)*Rnd()
      a2=a1 + padsize*tilew + 0.5*(di_w - 2*(4+padsize)*tilew)*Rnd()
      b2=b1 + padsize*tilew + 0.5*(di_h - 2*(2+padsize)*tilew)*Rnd()
      gr.modify t,"x",a1
      gr.modify t,"y",b1
      gr.modify u,"x",a1
      gr.modify u,"y",b1
      gr.modify tt,"x",a2
      gr.modify tt,"y",b2
      gr.modify uu,"x",a2
      gr.modify uu,"y",b2
      gr.show t
      gr.show tt
      gr.render
      tmr=Clock()
      do
        Gosub TestCombatEnArrierePlan
        gr.bounded.touch  x,(a1-padsize*tilew)*scale_w,(b1-padsize*tilew)*scale_h,(a1+padsize*tilew)*scale_w,(b1+padsize*tilew)*scale_h
        gr.bounded.touch2 y,(a2-padsize*tilew)*scale_w,(b2-padsize*tilew)*scale_h,(a2+padsize*tilew)*scale_w,(b2+padsize*tilew)*scale_h
      until (x & y) | Clock()-tmr>1000
      if x & y then
        if !mute then Soundpool.play tmp,sndShi,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
        gr.hide t
        gr.show u
        gr.hide tt
        gr.show uu
        gr.render
        Pause 200
      else
        coups=coups+1
      end if
      gr.hide t
      gr.hide u
      gr.hide tt
      gr.hide uu
      gr.render
    next i
    if coups then %' Le héros est touché
      gr.modify t1,"text",Stg$(coups)+RPGLBL$[769]
      gr.modify t1,"x",7*tilew
      gr.show t1
      for y=0 to 6*tileh step tileh/2
        gr.modify t1, "y", y
        gr.render
        Pause 4
      next y
      Pause 500
      gr.modify t2,"text","/"+Stg$(def)+RPGLBL$[770]
      gr.modify t2,"x",7*tilew
      gr.show t2
      for y=-0.75*tileh to 7.25*tileh step tileh/2
        gr.modify t2, "y", y
        gr.render
        Pause 4
      next y
      Pause 500
      gr.modify t3,"text","="+Stg$(10*coups/def)+RPGLBL$[771]+Stg$(pdv)
      gr.modify t3,"x",7*tilew
      gr.show t3
      for y=-0.5*tileh to 8.5*tileh step tileh/2
        gr.modify t3, "y", y
        gr.render
        Pause 4
      next y
      Pause 1000
      gr.hide t1
      gr.hide t2
      gr.hide t3
      gr.bitmap.delete heroBigBmp
      gr.bitmap.scale heroBigBmp, heroTile[heroform,14], tilew*4, tileh*4, 0
      gr.modify heroPtr, "bitmap", heroBigBmp
      if !mute then Soundpool.play tmp,sndHur,0.99,0.99,1,0,1
      gr.render
      Pause 1000
      gr.bitmap.delete heroBigBmp
      gr.bitmap.scale heroBigBmp, heroTile[heroform,_left+1], tilew*4, tileh*4, 0
      gr.modify heroPtr, "bitmap", heroBigBmp
      gr.render
      pdv=Floor(pdv-10*coups/def)
      if pdv<=0 then
        pdv=0
        gr.hide heroPtr
      end if
      gr.modify pdvPtr, "right", di_w-tilew*(3.9-3*pdv/100)
      gr.render
    else %' Le héros a esquivé tous les coups
      gr.modify t1,"text",RPGLBL$[772]
      gr.modify t1,"x",5*tilew
      gr.show t1
      for y=0 to 6*tileh step tileh/2
        gr.modify t1, "y", y
        gr.render
        Pause 4
      next y
      Pause 500
      gr.modify t2,"text",RPGLBL$[780]
      gr.modify t2,"x",5*tilew
      gr.show t2
      gr.modify t3,"text",RPGLBL$[781]
      gr.modify t3,"x",5*tilew
      gr.show t3
      for y=-0.25*tileh to 7.75*tileh step tileh/2
        gr.modify t2, "y", y
        gr.modify t3, "y", y+1.25*tileh
        gr.render
        Pause 4
      next y
      if !mute then Soundpool.play tmp,sndSwo,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      Pause 150
      if !mute then Soundpool.play tmp,sndSwo,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      Pause 150
      coupsDoubles=2
      gr.show dbl1
      gr.show dbl2
      Pause 1000
      gr.hide t1
      gr.hide t2
      gr.hide t3
      gr.render
    end if
    Pause 1000
    fighter=1-fighter
  end if
until !ennPdv | !pdv
!' fin du combat : on nettoie le champ de bataille et on rend la main
if pdv=0 then
  Goto TheEnd
else if Mod(victories,10)=0 then %' montée de level !!
  gr.modify t1,"text",RPGLBL$[782]+Stg$(victories)+RPGLBL$[783]
  gr.modify t1,"x",5*tilew
  gr.show t1
  for y=di_h+tileh to 4*tileh step -tileh/2
    gr.modify t1, "y", y
    gr.render
    Pause 4
  next y
  Pause 500
  gr.modify t2,"text",RPGLBL$[784]
  gr.modify t2,"x",5*tilew
  gr.show t2
  for y=di_h+tileh to 5*tileh step -tileh/2
    gr.modify t2, "y", y
    gr.render
    Pause 4
  next y
  Pause 500
  gr.modify t3,"text",RPGLBL$[785]+Stg$(2+(victories-Mod(victories,10))/10)
  gr.modify t3,"x",3*tilew
  gr.show t3
  for y=di_h+tileh to 6*tileh step -tileh/2
    gr.modify t3, "y", y
    gr.render
    Pause 4
  next y
  if !mute then
    audio.load snd, "debloque.mid"
    audio.stop
    audio.release musPtr
    audio.play snd
    do
      Audio.isdone i
    until i
    audio.stop
    audio.release snd
  else
    for i=1 to 3
      vibrate VibrCrt[], -1
      Pause 150
    next i
  end if
  Pause 1000
  gr.hide t1
  gr.hide t2
  gr.hide t3
  gr.render
end if
if !mute & music$<>"" then
  audio.load musPtr, music$
  audio.play musPtr
  audio.loop
end if
Gosub InitEcranDeJeu
gr.render
Goto BouclePrincipale
!'---------------------------------------------------------------------------------------------------

TestCombatEnArrierePlan:
!'---------------------------------------------------------------------------------------------------
!' Vérif appui touche Home du téléphone (pause le combat) ou touche Back (tentative de fuite)
!'---------------------------------------------------------------------------------------------------
if Background() then
  if !mute then audio.pause
  if pdv>0 then Gosub SauverPartie
  if needSaveMAPs then Gosub SauverCalques
  Gosub InitEcranDeJeu
  Notify RPGLBL$[766], RPGLBL$[767], RPGLBL$[768], 0
  do
    Pause 1000 %' wait in background doing nothing
  until !Background()
  gr.render
  if !mute then audio.play musPtr %' musique de combat
end if
if BackKey then %' Tentative de fuite !
  BackKey=0
  gr.hide t
  gr.hide u
  gr.hide v
  gr.hide tt
  gr.hide uu
  gr.hide t1
  gr.hide t2
  gr.hide t3
  gr.hide t4
  if !mute then audio.stop
  gr.bitmap.delete heroBigBmp
  gr.bitmap.scale heroBigBmp, heroTile[heroform,_right+3], tilew*4, tileh*4, 0
  gr.modify heroPtr, "bitmap", heroBigBmp
  if !mute then Soundpool.play tmp,sndBnc,0.99,0.99,1,0,1
  for x=di_w-4.5*tilew to di_w step tilew
    gr.modify heroPtr, "x", x
    gr.render
    Pause 50
  next x
  Pause 500
  gr.bitmap.delete ennBigBmp
  gr.bitmap.size ennBmp2, ennw, ennh
  gr.bitmap.scale ennBigBmp, ennBmp2, ennw*4, ennh*4, 0
  gr.modify ennPtr, "bitmap", ennBigBmp
  if !mute then Soundpool.play tmp,sndBnc,0.99,0.99,1,0,1
  z=1
  for x=tilew/2 to di_w+3*tilew/2 step 2*tilew
    pct=(x-tilew/2)/(di_w+tilew)
    if (pct>=0.25 & z=1) | (pct>=0.75 & z=3) then
      gr.bitmap.delete ennBigBmp
      gr.bitmap.size ennBmp1, ennw, ennh
      gr.bitmap.scale ennBigBmp, ennBmp1, ennw*4, ennh*4, 0
      gr.modify ennPtr, "bitmap", ennBigBmp
      z=z+1
    elseif pct>=0.5 & z=2 then
      gr.bitmap.delete ennBigBmp
      gr.bitmap.size ennBmp2, ennw, ennh
      gr.bitmap.scale ennBigBmp, ennBmp2, ennw*4, ennh*4, 0
      gr.modify ennPtr, "bitmap", ennBigBmp
      z=z+1
    end if
    y=tileh/2+(di_h-5*tileh)*pct
    gr.modify ennPtr, "x", x
    gr.modify ennPtr, "y", y
    gr.render
    Pause 50
  next x
  Pause 500
  gr.modify t1,"text",RPGLBL$[797]
  gr.modify t1,"x",7*tilew
  gr.show t1
  for y=0 to 6*tileh step tileh/2
    gr.modify t1, "y", y
    gr.render
    Pause 4
  next y
  Pause 500
  gr.modify t2,"text",RPGLBL$[798]
  gr.modify t2,"x",7*tilew
  gr.show t2
  for y=-0.75*tileh to 7.25*tileh step tileh/2
    gr.modify t2, "y", y
    gr.render
    Pause 4
  next y
  Pause 500
  ata=30+Floor(41*Rnd())
  gr.modify t3,"text",RPGLBL$[799]+Stg$(ata)+RPGLBL$[800]+Stg$(pdv)
  gr.modify t3,"x",7*tilew
  gr.show t3
  for y=-0.5*tileh to 8.5*tileh step tileh/2
    gr.modify t3, "y", y
    gr.render
    Pause 4
  next y
  Pause 1000
  gr.hide t1
  gr.hide t2
  gr.hide t3
  if !mute then Soundpool.play tmp,sndHur,0.99,0.99,1,0,1
  pdv=pdv-ata
  if pdv<=0 then
    pdv=0
  end if
  gr.modify pdvPtr, "right", di_w-tilew*(3.9-3*pdv/100)
  gr.render
  Pause 1000
  gr.bitmap.delete ennBigBmp
  gr.bitmap.delete heroBigBmp
  if pdv=0 then Goto TheEnd
  if !mute & music$<>"" then
    audio.load musPtr, music$
    audio.play musPtr
    audio.loop
  end if
  Gosub InitEcranDeJeu
  gr.render
  Goto BouclePrincipale
end if
Return
!'---------------------------------------------------------------------------------------------------

FinalFight:
!'---------------------------------------------------------------------------------------------------
!' Routine du combat final contre le Seigneur Dragon !
!'---------------------------------------------------------------------------------------------------
!' Animation : héros s'arrête, contrôles disparaissent
gr.cls %' Call gClear()
coeurPtr=0
flashPtr=0
gr.color 255,0,0,0,1
light=0 %' annule la baisse de luminosité (si la bougie est en train de s'éteindre)
ennAta=Floor(9*(1+difficulte/100)+0.5)
ennDef=Floor(5*(1+difficulte/100)+0.5)
ennPdv=Floor(333*(1+difficulte/100)+0.5)
ennMaxPdv=ennPdv
gr.bitmap.draw bgnPtr,bgnBmp,-tilew,-tileh
gr.modify bgnPtr,"alpha",255-dkns
if objet[44] then %' Emeraude
  gr.bitmap.draw tmpPtr, objTile[44], (di_w-tilew)/2, (di_h-tileh)/2
  gr.hide tmpPtr
end if
gr.bitmap.draw heroPtr, heroTile[heroform,_down+1], Floor(nthor/2)*tilew, Floor(ntver/2)*tileh
if GhostMode then gr.modify heroPtr, "alpha", 128
if Background() then Exit else gr.render
!' Sauve avant d'affronter le boss final
herox=herox+1
if pdv>0 then Gosub SauverPartie
if needSaveMAPs then Gosub SauverCalques
gr.color 255,0,0,0,1
gr.bitmap.create tmpBmp, (nthor+2)*tilew, (ntver+2)*tileh
gr.bitmap.drawinto.start tmpBmp
gr.bitmap.draw nul, bgnBmp, 0, 0
gr.bitmap.drawinto.end
gr.modify bgnPtr,"bitmap",tmpBmp
gr.bitmap.crop nul, bgnBmp, tilew, 0, (nthor+1)*tilew, (ntver+2)*tileh
gr.bitmap.delete bgnBmp
gr.bitmap.create bgnBmp, (nthor+2)*tilew, (ntver+2)*tileh
gr.bitmap.drawinto.start bgnBmp
gr.bitmap.draw i, nul, 0, 0
gr.bitmap.drawinto.end
gr.bitmap.delete nul
x=herox+dx+Floor(nthor/2)+1
y=heroy+dy-Floor(ntver/2)-2
for w=1 to ntver+2
  y=y+1
  Gosub AfficherDecor
next w
!' Suite de l'animation
if !mute then Soundpool.play tmp,sndBnc,0.99,0.99,1,0,1 %' regarde à gauche
gr.modify heroPtr, "bitmap", heroTile[heroform,_left+1]
if Background() then Exit else gr.render
Pause 500
if !mute then Soundpool.play tmp,sndBnc,0.99,0.99,1,0,1 %' regarde à droite
gr.modify heroPtr, "bitmap", heroTile[heroform,_right+1]
if Background() then Exit else gr.render
Pause 500
gr.modify heroPtr, "bitmap", heroTile[heroform,_up+1]
!' Animation : ennemi tombe sur le héros
gr.bitmap.draw ennPtr, DragonRepos, (di_w - 3*tilew)/2, -2*tileh
for y=-2*tileh to (di_h - 5*tileh)/2 step 2
  gr.modify ennPtr, "y", y
  if Background() then Exit else gr.render
  Pause 4
next y
if objet[44] then %' Emeraude
  if !mute then Soundpool.play tmp,sndUsd,0.99,0.99,1,0,1
  gr.show tmpPtr
  gr.modify heroPtr, "bitmap", heroTile[heroform,_up+2]
  for y=(di_h - 3*tileh)/2 to (di_h - tileh)/2
    gr.modify heroPtr, "y", y+tileh
    if Background() then Exit else gr.render
    Pause 4
  next y
  Pause 500
  if !mute then audio.stop
  Pause 2000
  gr.modify ennPtr, "x", (di_w - 4*tilew)/2
  gr.modify ennPtr, "y", (di_h - 4*tileh)/2
  if !mute then Soundpool.play tmp,sndBnc,0.99,0.99,1,0,1 %' dragon à gauche
  if Background() then Exit else gr.render
  Pause 800
  gr.modify ennPtr, "x", (di_w - 3*tilew)/2
  gr.modify ennPtr, "y", (di_h - 5*tileh)/2
  if !mute then Soundpool.play tmp,sndBnc,0.99,0.99,1,0,1 %' dragon au milieu
  if Background() then Exit else gr.render
  Pause 800
  gr.modify ennPtr, "x", (di_w - 2*tilew)/2
  gr.modify ennPtr, "y", (di_h - 4*tileh)/2
  if !mute then Soundpool.play tmp,sndBnc,0.99,0.99,1,0,1 %' dragon à droite
  if Background() then Exit else gr.render
  Pause 800
  gr.modify ennPtr, "x", (di_w - 3*tilew)/2
  gr.modify ennPtr, "y", (di_h - 5*tileh)/2
  if !mute then Soundpool.play tmp,sndBnc,0.99,0.99,1,0,1 %' dragon au milieu
  if Background() then Exit else gr.render
  Pause 800
  gr.modify ennPtr, "x", (di_w - 4*tilew)/2
  gr.modify ennPtr, "y", (di_h - 4*tileh)/2
  if !mute then Soundpool.play tmp,sndBnc,0.99,0.99,1,0,1 %' dragon à gauche
  if Background() then Exit else gr.render
  Pause 2000
  gr.bitmap.drawinto.start DragonRepos
  gr.bitmap.draw nul, objTile[44], 2*tilew, tileh
  gr.bitmap.drawinto.end
  gr.hide tmpPtr
  perso[28]=1 %' le Seigneur Dragon s'en va avec l'émeraude
  Gosub MusiqueChange
  for x=(di_w - 4*tilew)/2 to di_w step 2
    y=(di_h - 4*tileh)/2-(x - (di_w - 4*tilew)/2)/di_w*di_h/2
    gr.modify ennPtr, "x", x
    gr.modify ennPtr, "y", y
    if Background() then Exit else gr.render
    Pause 4
  next x
  Pause 3000
  if Background() then Exit else Gosub FadeOut
  gr.cls %' Call gClear()
  gr.bitmap.delete tmpBmp
  Gosub InitEcranDeJeu
  if Background() then Exit else Gosub FadeIn
  if pdv>0 then Gosub SauverPartie
  Pause 2000
  Goto BouclePrincipale
end if
if !mute then Soundpool.play tmp,sndPRo,0.99,0.99,1,0,1
gr.modify heroPtr, "bitmap", heroTile[heroform,_up+2]
for y=(di_h - 3*tileh)/2 to (di_h - tileh)/2
  gr.modify ennPtr, "y", y-tileh
  gr.modify heroPtr, "y", y+tileh
  if Background() then Exit else gr.render
  Pause 4
next y
Pause 1000
!' Animation : héros et dragon chacun vers un coin d'écran
gr.modify heroPtr, "bitmap", heroTile[heroform,_left+1]
for x=2*tilew to di_w/2 - 1.5 * tilew step tilew/6
  y=x / (di_w - tilew) * (di_h - tileh)
  gr.modify ennPtr, "x", (di_w-tilew)/2 - x
  gr.modify ennPtr, "y", (di_h-tileh)/2 - y
  gr.modify heroPtr, "x", (di_w-tilew)/2 + x
  gr.modify heroPtr, "y", (di_h-tileh)/2 + y
  if Background() then Exit else gr.render
  Pause 4
next x
!' Préparation de l'écran de combat
gr.cls %' Call gClear()
flashPtr=0
gr.color 255,0,0,0,1
gr.bitmap.draw bgnPtr,tmpBmp,-tilew,-tileh
gr.modify bgnPtr,"alpha",60
if tilesBmp then gr.bitmap.delete tilesBmp
gr.bitmap.create tilesBmp,di_w,di_h
gr.bitmap.draw tilesPtr,tilesBmp,0, 0
gr.hide tilesPtr
!' Barre de PDV + ATA/DEF du HEROS
gr.color 175,255,0,0,1
gr.bitmap.draw i,coeurBmp,di_w-tilew*5,0 %' coeur
gr.rect pdvPtr, di_w-tilew*3.9, tileh*0.4, di_w-tilew*0.9, tileh*0.6
gr.modify pdvPtr, "right", di_w-tilew*(3.9-3*pdv/100)
gr.color 175,200,200,200,0
gr.rect i, di_w-tilew*3.9, tileh*0.4, di_w-tilew*0.9, tileh*0.6
if heroform=1 then
  ata=0
  if objet[10] then ata=ata+3
  if objet[30] then ata=ata+5
  def=1
  if objet[9] then def=def+3
  if objet[29] then def=def+4
elseif heroform=2 then
  ata=6
  def=4
elseif heroform=3 then
  ata=2
  def=3
end if
nbCoupsAta=2 + (victories - Mod(victories,10))/10
gr.text.size 8
Call Label(di_w-tilew*4.7,1.4*tileh,Stg$(nbCoupsAta)+RPGLBL$[769])
gr.text.size 14
Call Label(di_w-tilew*5,2.6*tileh,"ATA: +"+Stg$(ata))
Call Label(di_w-tilew*5,3.6*tileh,"DEF: x"+Stg$(def))
if heroform=1 then
  if objet[10] then gr.bitmap.draw tmp, objTile[10], di_w-2.2*tilew, 1.7*tileh
  if objet[30] then gr.bitmap.draw tmp, objTile[30], di_w-1.1*tilew, 1.7*tileh
  if objet[9] then gr.bitmap.draw tmp, objTile[9], di_w-2.2*tilew, 2.8*tileh
  if objet[29] then gr.bitmap.draw tmp, objTile[29], di_w-1.1*tilew, 2.8*tileh
end if
!' Barre de PDV + ATA/DEF du DRAGON
gr.color 175,72,0,255,1
gr.bitmap.draw i,coeurBmp,0,di_h-3.5*tileh %' coeur
gr.rect ennPdvPtr, tilew*1.1, di_h-3.1*tileh, tilew*4.1, di_h-2.9*tileh
gr.modify ennPdvPtr, "right", tilew*(1.1+3*ennPdv/ennMaxPdv)
gr.color 175,200,200,200,0
gr.rect i, tilew*1.1, di_h-3.1*tileh, tilew*4.1, di_h-2.9*tileh
gr.text.size 14
Call Label(tilew*0.5,di_h-1.7*tileh,"ATA: "+Stg$(ennAta))
if heroform=1 & objet[32] then gr.text.strike 1 %' masse d'arme en humain
Call Label(tilew*0.5,di_h-0.7*tileh,"DEF: x"+Stg$(ennDef))
if heroform=1 & objet[32] then %' masse d'arme en humain
  gr.text.width w, "DEF: x"+Stg$(ennDef))
  gr.text.strike 0
  gr.color 255,255,255,255,1
  gr.rect i, tilew*0.8+w, di_h-1.5*tileh, tilew*2+w, di_h-0.3*tileh
  gr.bitmap.draw i, objTile[32], tilew*0.9+w, di_h-1.4*tileh
  Call Label(tilew*2.2+w,di_h-0.7*tileh,"x1")
  ennDef=1
end if
!' Zone tactile pour les combats
gr.color 100,238,116,255,1
gr.circle t, 0, 0, padsize*tilew
gr.hide t
gr.circle tt, 0, 0, padsize*tilew
gr.hide tt
gr.color 100,255,255,255,1
gr.circle u, 0, 0, padsize*tilew
gr.hide u
gr.circle uu, 0, 0, padsize*tilew
gr.hide uu
gr.color 100,255,0,0,1
gr.circle v, 0, 0, padsize*tilew
gr.hide v
!' Agrandissement des combattants
gr.color 255,0,0,0,1
if ennBigBmp then gr.bitmap.delete ennBigBmp
gr.bitmap.size DragonRepos, ennw, ennh
gr.bitmap.scale ennBigBmp, DragonRepos, ennw*4, ennh*4, 0
gr.bitmap.draw ennPtr, ennBigBmp, 0, 0
if heroBigBmp then gr.bitmap.delete heroBigBmp
gr.bitmap.scale heroBigBmp, heroTile[heroform,_left+1], tilew*4, tileh*4, 0
gr.bitmap.draw heroPtr, heroBigBmp, di_w-4.5*tilew, di_h-4.5*tileh
if GhostMode then gr.modify heroPtr, "alpha", 128
if Background() then Exit else gr.render
!' Labels du nb de coups portés
gr.color 255,255,255,255,1
gr.text.bold 1
gr.text.draw t1,0,0,""
gr.hide t1
gr.text.draw t2,0,0,""
gr.hide t2
gr.text.draw t3,0,0,""
gr.hide t3
gr.text.underline 1
gr.text.draw t4,0,0,""
gr.text.underline 0
gr.hide t4
gr.text.bold 0
!' Voiles de couleur pour les sorts magiques du Dragon
Call SetRGB(255,"FFFF00",1) %' voile jaune
gr.rect yfp,0,0,di_w,di_h
gr.hide yfp
Call SetRGB(255,"FFFFFF",1) %' voile blanc
gr.rect wfp,0,0,di_w,di_h
gr.hide wfp
Call SetRGB(255,"FF6A00",1) %' voile orange
gr.rect ofp,0,0,di_w,di_h
gr.hide ofp
Call SetRGB(255,"000000",1) %' voile noir
gr.rect bfp,0,0,di_w,di_h
gr.hide bfp
!' Bandit manchot (attaque du héros)
if !vegasBmp then
  gr.bitmap.create vegasBmp,4*tilew+4,2*tileh+4
  gr.bitmap.drawinto.start vegasBmp
  gr.set.stroke 4
  gr.color 255,127,51,0,1
  gr.rect nul,2,2,4*tilew+2,2*tileh+2
  gr.color 255,255,106,0,0
  gr.rect nul,2,2,4*tilew+2,2*tileh+2
  gr.set.stroke 0
  gr.line nul,(4*tilew+4)/3,0,(4*tilew+4)/3,2*tileh+4
  gr.line nul,(4*tilew+4)*2/3,0,(4*tilew+4)*2/3,2*tileh+4
  gr.color 255,127,0,0,1
  gr.oval nul,4*tilew/6-tilew/2+2,1.8*tileh-tileh/5,4*tilew/6+tilew/2+2,1.8*tileh+tileh/5
  gr.oval nul,4*tilew/2-tilew/2+2,1.8*tileh-tileh/5,4*tilew/2+tilew/2+2,1.8*tileh+tileh/5
  gr.oval nul,4*tilew*5/6-tilew/2+2,1.8*tileh-tileh/5,4*tilew*5/6+tilew/2+2,1.8*tileh+tileh/5
  gr.bitmap.drawinto.end
end if
gr.bitmap.draw vPtr0,vegasBmp,11*tilew-2,1.5*tileh-2
gr.hide vPtr0
gr.color 255,255,106,0,1
gr.rect vPtr4,11*tilew,1.5*tileh,(11*tilew+4*tilew/3),3.5*tileh
gr.hide vPtr4
gr.rect vPtr5,(11*tilew+4*tilew/3),1.5*tileh,(11*tilew+4*tilew*2/3),3.5*tileh
gr.hide vPtr5
gr.rect vPtr6,(11*tilew+4*tilew*2/3),1.5*tileh,(11*tilew+4*tilew),3.5*tileh
gr.hide vPtr6
gr.bitmap.draw vPtr1,heroTile[1,1],11*tilew+4*tilew/6-tilew/2,2*tileh-4
gr.hide vPtr1
gr.bitmap.draw vPtr2,heroTile[1,1],11*tilew+4*tilew/2-tilew/2,2*tileh-4
gr.hide vPtr2
gr.bitmap.draw vPtr3,heroTile[1,1],11*tilew+4*tilew*5/6-tilew/2-2,2*tileh-4
gr.hide vPtr3
!' Effets reliques élémentales
gr.color 255,0,0,0,1
if !tornadoBmp then gr.bitmap.scale tornadoBmp, objTile[21], tilew*2, tileh*2, 0
gr.bitmap.draw tornadoPtr, tornadoBmp, 0, 0
gr.hide tornadoPtr
gr.bitmap.draw rainPtr, rainBmp[1], 6.5*tilew-63, -154
gr.modify rainPtr,"alpha",175
gr.hide rainPtr
if !mirBmp then
  gr.bitmap.create nul,tilew,tileh
  gr.bitmap.drawinto.start nul
  gr.rotate.start 270,tilew/2,tileh/2
  gr.bitmap.draw i,objTile[26],0,0
  gr.rotate.end
  gr.bitmap.drawinto.end
  gr.bitmap.scale mirBmp, nul, tilew*2, tileh*2, 0
  gr.bitmap.delete nul
end if
gr.bitmap.draw mirPtr, mirBmp, di_w-5*tilew, di_h-3*tileh
gr.hide mirPtr
gr.color 125,255,255,0,1
gr.oval mirOval, 0, 0, 0, 0
gr.hide mirOval
gr.set.stroke 2*(tilew-3)
gr.line mirLine1, di_w-4*tilew, di_h-2*tileh, di_w-4*tilew, -tileh
gr.hide mirLine1
gr.line mirLine2, di_w-4*tilew, -tileh, 0, 0
gr.hide mirLine2
gr.set.stroke 0
gr.color 255,0,0,0,1
if !teteBmp then
  gr.bitmap.scale teteBmp,objTile[34],-2.5*tilew,2.5*tileh, 0
  dim flameBmp[4]
  for i=1 to 4
    gr.bitmap.scale flameBmp[i],flame[i],2*tilew,2*tileh*7/3, 0
  next i
end if
gr.bitmap.draw tetePtr, teteBmp, di_w-5.5*tilew, di_h-3.5*tileh
gr.hide tetePtr
gr.bitmap.draw flamePtr, flameBmp[1], 0, 0
gr.hide flamePtr

!' Combat !!
fighter=0
do
  if fighter=0 then %' ATTAQUE DU SEIGNEUR DRAGON
    coups=0
    if Rnd()<0.5 then %' ATTAQUE CONVENTIONNELLE A COUPS DE GRIFFE (PEUT DONNER DES COEURS)
      Popup RPGLBL$[774],0,0,0
      Pause 3000
      for i=1 to Floor(ennAta)
        a1=di_w-(padsize*tilew+Floor(6*Rnd())*(ntver*tileh-2*padsize*tilew)/5)
        b1=di_h-(0.5*(di_h-y)*di_w/di_h*Rnd() + padsize*tilew)
        gr.modify t,"x",a1
        gr.modify t,"y",b1
        gr.modify u,"x",a1
        gr.modify u,"y",b1
        gr.show t
        if Background() then Exit else gr.render
        tmr=Clock()
        do
          if Background() then Exit
          gr.bounded.touch  tmp,(a1-padsize*tilew)*scale_w,(b1-padsize*tilew)*scale_h,(a1+padsize*tilew)*scale_w,(b1+padsize*tilew)*scale_h
        until tmp | Clock()-tmr>500
        if tmp then
          if !mute then Soundpool.play tmp,sndShi,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
          gr.hide t
          gr.show u
          if Background() then Exit else gr.render
          Pause 200
        else
          coups=coups+1
        end if
        gr.hide t
        gr.hide u
        if Background() then Exit else gr.render
      next i
      if coups then %' Le héros est touché
        e$=RPGLBL$[775]
        gr.modify t1,"text",Stg$(coups)+RPGLBL$[769]
        gr.modify t1,"x",7*tilew
        gr.show t1
        for y=0 to 6*tileh step tileh/2
          gr.modify t1, "y", y
          if Background() then Exit else gr.render
          Pause 4
        next y
        Pause 500
        gr.modify t2,"text","/"+Stg$(def)+RPGLBL$[770]
        gr.modify t2,"x",7*tilew
        gr.show t2
        for y=-0.75*tileh to 7.25*tileh step tileh/2
          gr.modify t2, "y", y
          if Background() then Exit else gr.render
          Pause 4
        next y
        Pause 500
        gr.modify t3,"text","="+Stg$(10*coups/def)+RPGLBL$[771]+Stg$(pdv)
        gr.modify t3,"x",7*tilew
        gr.show t3
        for y=-0.5*tileh to 8.5*tileh step tileh/2
          gr.modify t3, "y", y
          if Background() then Exit else gr.render
          Pause 4
        next y
        Pause 1000
        gr.hide t1
        gr.hide t2
        gr.hide t3
        gr.bitmap.delete heroBigBmp
        gr.bitmap.scale heroBigBmp, heroTile[heroform,14], tilew*4, tileh*4, 0
        gr.modify heroPtr, "bitmap", heroBigBmp
        if !mute then Soundpool.play tmp,sndHur,0.99,0.99,1,0,1
        if Background() then Exit else gr.render
        Pause 1000
        gr.bitmap.delete heroBigBmp
        gr.bitmap.scale heroBigBmp, heroTile[heroform,_left+1], tilew*4, tileh*4, 0
        gr.modify heroPtr, "bitmap", heroBigBmp
        if Background() then Exit else gr.render
        pdv=Floor(pdv-10*coups/def)
        if pdv<=0 then
          pdv=0
          gr.hide heroPtr
        end if
        gr.modify pdvPtr, "right", di_w-tilew*(3.9-3*pdv/100)
        if Background() then Exit else gr.render
      else %' Le héros a esquivé tous les coups
        gr.modify t1,"text",RPGLBL$[772]
        gr.modify t1,"x",5*tilew
        gr.show t1
        for y=0 to 6*tileh step tileh/2
          gr.modify t1, "y", y
          if Background() then Exit else gr.render
          Pause 4
        next y
        Pause 500
        gr.modify t2,"text",RPGLBL$[801]
        gr.modify t2,"x",5*tilew
        gr.show t2
        for y=-0.25*tileh to 7.75*tileh step tileh/2
          gr.modify t2, "y", y
          if Background() then Exit else gr.render
          Pause 4
        next y
        Pause 1000
        destx=herox+Floor(nthor/2)-5
        desty=heroy+Floor(ntver/2)-3
        Gosub DeterreUnCoeur
        destx=herox+Floor(nthor/2)-5
        desty=heroy+Floor(ntver/2)-4
        Gosub DeterreUnCoeur
        Pause 1000
        gr.hide t1
        gr.hide t2
        if Background() then Exit else gr.render
      end if
      Pause 1000
      fighter=1-fighter

    else %' attaque spéciale changement de paysage
      z=Ceil(4*Rnd())
      gr.bitmap.delete ennBigBmp
      gr.bitmap.scale ennBigBmp, DragonFeu, ennw*4, ennh*4, 0
      gr.modify ennPtr, "bitmap", ennBigBmp
      if Background() then Exit else gr.render
      if z=1 then
        a1=41 %' sables mouvants
        flashPtr=yfp %' yellow flash pointer
      elseif z=2 then
        a1=59 %' glace
        flashPtr=wfp %' white flash pointer
      elseif z=3 then
        a1=20 %' lave
        flashPtr=ofp %' orange flash pointer
      elseif z=4 then
        a1=45 %' vide stellaire
        flashPtr=bfp %' black flash pointer
      end if
      Popup RPGLBL$[776],0,0,0
      Pause 2000
      Gosub Flash
      Pause 500
      gr.bitmap.delete tilesBmp
      gr.bitmap.create tilesBmp,di_w,di_h
      gr.modify tilesPtr,"bitmap",tilesBmp
      gr.modify tilesPtr,"alpha",255
      gr.show tilesPtr
      if Background() then Exit else gr.render
      Pause 500
      if !mute then Soundpool.play tmp,sndMag,0.99,0.99,1,0,1
      gr.bitmap.drawinto.start tilesBmp
      for x=11 to 15
        y=x-6
        gr.bitmap.draw nul,bgnTile[a1],x*tilew,y*tileh
        gr.bitmap.draw nul,bgnTile[a1],x*tilew,(y+1)*tileh
        if Background() then Exit else gr.render
        Pause 100
      next x
      x=16
      for y=9 to 11
        gr.bitmap.draw nul,bgnTile[a1],x*tilew,y*tileh
      next y
      if Background() then Exit else gr.render
      Pause 100
      for x=17 to 19
        for y=8 to 12
          gr.bitmap.draw nul,bgnTile[a1],x*tilew,y*tileh
        next y
      next x
      if Background() then Exit else gr.render
      Pause 100
      x=20
      for y=9 to 11
        gr.bitmap.draw nul,bgnTile[a1],x*tilew,y*tileh
      next y
      if Background() then Exit else gr.render
      gr.bitmap.drawinto.end
      pause 500
      gr.bitmap.delete ennBigBmp
      gr.bitmap.scale ennBigBmp, DragonRepos, ennw*4, ennh*4, 0
      gr.modify ennPtr, "bitmap", ennBigBmp
      if Background() then Exit else gr.render
      coups=0
      e$="3"
      gr.modify t1,"text",e$
      gr.modify t1,"x",di_w-padsize*tilew*5
      gr.modify t1,"y",padsize*tileh*2
      gr.show t1
      if Background() then Exit else gr.render
      tmr=Clock()
      do
        if Background() then Exit
        gr.touch tmp, x, y
        if tmp then
          x=x/scale_w
          y=y/scale_h
          tmp=0
          if x<padsize*tilew & y<padsize*tileh then tmp=1
          if x<padsize*tilew & y>(di_h-padsize*tileh)/2 & y<(di_h+padsize*tileh)/2 then tmp=1
          if x>132 & x<182 & y>2 & y<52 then tmp=1
          if x>165 & x<215 & y>195 then tmp=1
        end if
        if Clock()-tmr>500 & Clock()-tmr<1000 & e$<>"" then
          e$=""
          gr.modify t1,"text",e$
          if Background() then Exit else gr.render
        elseif Clock()-tmr>1000 & Clock()-tmr<1500 & e$<>"2" then
          e$="2"
          gr.modify t1,"text",e$
          if Background() then Exit else gr.render
        elseif Clock()-tmr>1500 & Clock()-tmr<2000 & e$<>"" then
          e$=""
          gr.modify t1,"text",e$
          if Background() then Exit else gr.render
        elseif Clock()-tmr>2000 & Clock()-tmr<2500 & e$<>"1" then
          e$="1"
          gr.modify t1,"text",e$
          if Background() then Exit else gr.render
        elseif Clock()-tmr>2500 & Clock()-tmr<3000 & e$<>"" then
          e$=""
          gr.modify t1,"text",e$
          if Background() then Exit else gr.render
        end if
      until tmp | Clock()-tmr>3000
      gr.hide t1
      if Background() then Exit else gr.render
      if !tmp then
        coups=1
        e$=RPGLBL$[777]
      else
        !'---------------------------- VENT ---------------------------------
        if x<padsize*tilew & y<padsize*tileh then 
          e$="schema1.png"
          Gosub FollowTheTrail
          if !red then %' Hors des sentiers battus
            coups=1
            e$=RPGLBL$[778]
          else %' L'utilisateur a suivi le chemin de la formule magique comme il convient
            if !mute then
              l=GetAudioLength("conque_vent.mp3")
              tmr=Clock()
              Soundpool.play tmp,sndVen,0.99,0.99,1,0,1
            else
              for i=1 to 3
                vibrate VibrCrt[], -1
                Pause 150
              next i
            end if
            gr.modify tornadoPtr,"alpha",255
            gr.show tornadoPtr
            t=0
            while Clock()-tmr<l
              t=t+Pi/20 %' tornade en spirale souffle le sable
              gr.modify tornadoPtr, "x", di_w-(4+2*cos(t))*tilew
              gr.modify tornadoPtr, "y", di_h-(4+2*sin(t))*tileh
              if z=1 then
                alpha=255*(6*Pi-t)/(6*Pi)
                if alpha<0 then alpha=0
                gr.modify tilesPtr,"alpha",alpha
              end if
              if Background() then Exit else gr.render
              Pause 5
            repeat
            alpha=255
            while alpha>0
              t=t+Pi/20 %' tornade en spirale souffle le sable
              alpha=alpha-10
              if alpha<0 then alpha=0
              gr.modify tornadoPtr,"alpha",alpha
              gr.modify tornadoPtr, "x", di_w-(4+2*cos(t))*tilew
              gr.modify tornadoPtr, "y", di_h-(4+2*sin(t))*tileh
              if Background() then Exit else gr.render
              Pause 5
            repeat
            gr.hide tornadoPtr
            if Clock()-tmr<l then Pause l-Clock()+tmr
            if z<>1 then %' protège uniquement de l'attaque des sables
              coups=1
              e$=RPGLBL$[779]
            end if
          end if %' fin rune magique Vent ok/ko
        !'---------------------------- EAU ---------------------------------
        elseif x<padsize*tilew & y>(di_h-padsize*tileh)/2 & y<(di_h+padsize*tileh)/2 then
          e$="schema2.png"
          Gosub FollowTheTrail
          if !red | !green | !yellow then %' Hors des sentiers battus
            coups=1
            e$=RPGLBL$[778]
          else %' L'utilisateur a suivi le chemin de la formule magique comme il convient
            if !mute then
              l=GetAudioLength("croix_eau.mp3")
              tmr=Clock()
              Soundpool.play tmp,sndEau,0.99,0.99,1,0,1
            else
              for i=1 to 3
                vibrate VibrCrt[], -1
                Pause 150
              next i
            end if
            gr.show rainPtr
            for y=-154 to 0 step tileh/2 %' rideau de pluie descend
              gr.modify rainPtr, "y", y
              if Background() then Exit else gr.render
              Pause 5
            next y
            a2=0
            while Clock()-tmr<l-500
              a2=a2+1
              gr.modify rainPtr, "bitmap", rainBmp[Mod(a2,2)+1]
              if z=2 then
                alpha=255-4*a2
                if alpha<0 then alpha=0
                gr.modify tilesPtr,"alpha",alpha
              end if
              if Background() then Exit else gr.render
              Pause 5
            repeat
            for y=0 to -154 step -tileh/2 %' rideau de pluie remonte
              gr.modify rainPtr, "y", y
              if Background() then Exit else gr.render
              Pause 5
            next y
            gr.hide rainPtr
            if z<>2 then %' protège uniquement de l'attaque de la glace
              coups=1
              e$=RPGLBL$[779]
            end if
          end if %' fin rune magique Eau ok/ko
        !'-------------------------- GLACES --------------------------------
        elseif x>132 & x<182 & y>2 & y<52 then
          e$="schema3.png"
          Gosub FollowTheTrail
          if !red | !green then %' Hors des sentiers battus
            coups=1
            e$=RPGLBL$[778]
          else %' L'utilisateur a suivi le chemin de la formule magique comme il convient
            if !mute then
              l=GetAudioLength("miroir_glaces.mp3")
              tmr=Clock()
              Soundpool.play tmp,sndIce,0.99,0.99,1,0,1
            else
              for i=1 to 3
                vibrate VibrCrt[], -1
                Pause 150
              next i
            end if
            gr.show mirPtr
            gr.show mirLine1
            gr.show mirLine2
            gr.show mirOval
            b1=9*tilew
            a1=-1
            a2=0
            while Clock()-tmr<l
              a2=a2+1
              b1=b1+2*a1
              if b1<=4*tilew | b1>=9*tilew then a1=-a1
              y=(4*sin(a2*Pi/20)+4.5)*tileh
              gr.modify mirOval, "left", b1-tilew
              gr.modify mirOval, "top", y-tilew
              gr.modify mirOval, "right", b1+tilew
              gr.modify mirOval, "bottom", y+tilew
              gr.modify mirLine2, "x2", b1
              gr.modify mirLine2, "y2", y
              if z=3 then
                alpha=255-4*a2
                if alpha<0 then alpha=0
                gr.modify tilesPtr,"alpha",alpha
              end if
              if Background() then Exit else gr.render
              Pause 5
            repeat
            gr.hide mirPtr
            gr.hide mirLine1
            gr.hide mirLine2
            gr.hide mirOval
            if z<>3 then %' protège uniquement de l'attaque de la glace
              coups=1
              e$=RPGLBL$[779]
            end if
          end if %' fin rune magique Glaces ok/ko
        !'------------------------ DESOLATION ------------------------------
        elseif x>165 & x<215 & y>195 then
          e$="schema4.png"
          Gosub FollowTheTrail
          if !red | !green then %' Hors des sentiers battus
            coups=1
            e$=RPGLBL$[778]
          else %' L'utilisateur a suivi le chemin de la formule magique comme il convient
            if !mute then
              l=GetAudioLength("conque_vent.mp3") %' car "crane_desolation.mp3" trop long
              tmr=Clock()                         %' -> impliquerait de stopper la musique
              Soundpool.play tmp,sndVen,0.99,0.99,1,0,1
            else
              for i=1 to 3
                vibrate VibrCrt[], -1
                Pause 150
              next i
            end if
            gr.bitmap.delete heroBigBmp
            gr.bitmap.scale heroBigBmp, heroTile[heroform,_left+2], tilew*4, tileh*4, 0
            gr.modify heroPtr, "bitmap", heroBigBmp
            gr.show tetePtr
            gr.modify flamePtr,"alpha",255
            gr.show flamePtr
            t=0
            a1=0
            a2=0
            while Clock()-tmr<l
              t=t+Pi/20 %' flammes du crâne de la désolation
              a1=Mod(a1,4)+1
              a2=a2+1/80
              gr.modify flamePtr, "x", di_w-(4+(2+a2)*cos(t))*tilew
              gr.modify flamePtr, "y", di_h-(4-(3+a2)*sin(t)+4/3)*tileh
              gr.modify flamePtr, "bitmap", flameBmp[a1]
              if z=4 then
                alpha=255*1.5*(5*Pi-t)/(6*Pi)
                if alpha<0 then alpha=0
                gr.modify tilesPtr,"alpha",alpha
              end if
              if Background() then Exit else gr.render
              Pause 5
            repeat
            alpha=255
            while alpha>0
              t=t+Pi/20 %' flammes du crâne de la désolation
              alpha=alpha-10
              if alpha<0 then alpha=0
              a1=Mod(a1,4)+1
              a2=a2-1/10
              gr.modify flamePtr, "x", di_w-(4-(2+a2)*cos(t))*tilew
              gr.modify flamePtr,"alpha",alpha
              gr.modify flamePtr, "y", di_h-(4-(3+a2)*sin(t)+4/3)*tileh
              gr.modify flamePtr, "bitmap", flameBmp[a1]
              if Background() then Exit else gr.render
              Pause 5
            repeat
            gr.hide flamePtr
            gr.hide tetePtr
            gr.bitmap.delete heroBigBmp
            gr.bitmap.scale heroBigBmp, heroTile[heroform,_left+1], tilew*4, tileh*4, 0
            gr.modify heroPtr, "bitmap", heroBigBmp
            if z<>4 then %' protège uniquement de l'attaque du vide stellaire
              coups=1
              e$=RPGLBL$[779]
            end if
          end if %' fin rune magique Désolation ok/ko
        end if
        !'------------------------------------------------------------------
      end if
      if coups then %' Raté ou mauvaise rune ou trop tard : le héros est touché
        if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
        gr.modify t3,"text",e$
        gr.modify t3,"x",7*tilew
        gr.show t3
        for y=di_h+tileh to 10*tileh step -tileh/2
          gr.modify t3, "y", y
          if Background() then Exit else gr.render
          Pause 4
        next y
        Pause 200
        gr.hide t3
        if z=1 then
          e$=RPGLBL$[786]
        elseif z=2 then
          e$=RPGLBL$[787]
        elseif z=3 then
          e$=RPGLBL$[788]
        elseif z=4 then
          e$=RPGLBL$[789]
        end if
        gr.bitmap.delete heroBigBmp
        gr.bitmap.scale heroBigBmp, heroTile[heroform,14], tilew*4, tileh*4, 0
        gr.modify heroPtr, "bitmap", heroBigBmp
        if !mute then Soundpool.play tmp,sndHur,0.99,0.99,1,0,1
        if Background() then Exit else gr.render
        Pause 1000
        gr.bitmap.delete heroBigBmp
        gr.bitmap.scale heroBigBmp, heroTile[heroform,_left+1], tilew*4, tileh*4, 0
        gr.modify heroPtr, "bitmap", heroBigBmp
        if Background() then Exit else gr.render
        Pause 500
        Gosub Flash
        for alpha=255 to 0 step -32
          gr.modify tilesPtr, "alpha", alpha
          if Background() then Exit else gr.render
          Pause 5
        next alpha
        gr.hide tilesPtr
        if Background() then Exit else gr.render
        Pause 500
        coups=3*(1+z)*(1+difficulte/100)
        gr.modify t3,"text",Stg$(10*coups/def)+RPGLBL$[771]+Stg$(pdv)
        gr.modify t3,"x",7*tilew
        gr.show t3
        for y=-0.5*tileh to 10*tileh step tileh/2
          gr.modify t3, "y", y
          if Background() then Exit else gr.render
          Pause 4
        next y
        pdv=Floor(pdv-10*coups/def)
        if pdv<=0 then
          pdv=0
          gr.hide heroPtr
        end if
        gr.modify pdvPtr, "right", di_w-tilew*(3.9-3*pdv/100)
        if Background() then Exit else gr.render
        Pause 1000
        gr.hide t3
        if Background() then Exit else gr.render
      else %' Le héros a tracé la rune qu'il fallait pour contrer l'attaque magique
        gr.hide tilesPtr
        if !mute then Soundpool.play tmp,sndSwo,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
        Pause 150
        if !mute then Soundpool.play tmp,sndSwo,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
        gr.modify t3,"text",RPGLBL$[802]
        gr.modify t3,"x",7*tilew
        gr.show t3
        for y=di_h+tileh to 10*tileh step -tileh/2
          gr.modify t3, "y", y
          if Background() then Exit else gr.render
          Pause 4
        next y
        Pause 1000
        gr.hide t3
        gr.bitmap.delete ennBigBmp
        gr.bitmap.scale ennBigBmp, DragonBlesse, ennw*4, ennh*4, 0
        gr.modify ennPtr, "bitmap", ennBigBmp
        if !mute then Soundpool.play tmp,sndPRo,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
        if Background() then Exit else gr.render
        Pause 1000
        gr.bitmap.delete ennBigBmp
        gr.bitmap.scale ennBigBmp, DragonRepos, ennw*4, ennh*4, 0
        gr.modify ennPtr, "bitmap", ennBigBmp
        if Background() then Exit else gr.render
      end if
      Pause 1000
      fighter=1-fighter
    end if

  else %' ATTAQUE DU HEROS
    Popup RPGLBL$[790],0,0,0
    Pause 3000
    coups=0
    gr.bitmap.delete tilesBmp
    gr.bitmap.create tilesBmp,di_w,di_h
    gr.modify tilesPtr,"bitmap",tilesBmp
    gr.modify tilesPtr,"alpha",255
    gr.show tilesPtr
    gr.show vPtr0
    gr.modify t1,"text",RPGLBL$[803]+Stg$(1)+"/"+Stg$(nbCoupsAta)
    gr.modify t1,"x",10.5*tilew
    gr.show t1
    for y=-tileh to 5*tileh step tileh/2
      gr.modify t1, "y", y
      if Background() then Exit else gr.render
      Pause 4
    next y
    gr.show vPtr1
    gr.show vPtr2
    gr.show vPtr3
    for i=1 to nbCoupsAta
      ht1=0
      ht2=0
      ht3=0
      gr.modify t1,"text",RPGLBL$[803]+Stg$(i)+"/"+Stg$(nbCoupsAta)
      do
        gr.bounded.touch tmp,11*tilew*scale_w,1.5*tileh*scale_h,(11*tilew+4*tilew/3)*scale_w,3.5*tileh*scale_h
        if tmp & !ht1 then
          vibrate VibrCrt[], -1
          gr.show vPtr4
          ht1=1
        end if
        gr.bounded.touch tmp,(11*tilew+4*tilew/3)*scale_w,1.5*tileh*scale_h,(11*tilew+4*tilew*2/3)*scale_w,3.5*tileh*scale_h
        if tmp & !ht2 then
          vibrate VibrCrt[], -1
          gr.show vPtr5
          ht2=1
        end if
        gr.bounded.touch tmp,(11*tilew+4*tilew*2/3)*scale_w,1.5*tileh*scale_h,(11*tilew+4*tilew)*scale_w,3.5*tileh*scale_h
        if tmp & !ht3 then
          vibrate VibrCrt[], -1
          gr.show vPtr6
          ht3=3
        end if
        if !ht1 then
          hr1=Floor(4*Rnd())+1
          gr.modify vPtr1,"bitmap",heroTile[hr1,_right+1]
        end if
        if !ht2 then
          hr2=Floor(4*Rnd())+1
          gr.modify vPtr2,"bitmap",heroTile[hr2,_right+1]
        end if
        if !ht3 then
          hr3=Floor(4*Rnd())+1
          gr.modify vPtr3,"bitmap",heroTile[hr3,_right+1]
        end if
        if Background() then Exit else gr.render
        Pause 50
      until ht1 & ht2 & ht3
      if hr1<>hr2 & hr2<>hr3 & hr1<>hr3 then %' 3 FIGURES DIFFERENTES
        ht1=0
      else %' 2 OU 3 FIGURES IDENTIQUES
        if hr2=hr3 then swap hr1,hr2
        if hr1=1 then %' humain = puissance 2
          ht1=2
        elseif hr1=2 then %' loup = puissance 3
          ht1=3
        elseif hr1=3 then %' poisson = puissance 1
          ht1=1
        elseif hr1=4 then %' aigle = puissance 4
          ht1=4
        end if
      end if
      if hr1=hr2 & hr2=hr3 then ht1=2*ht1 %' 3 FIGURES IDENTIQUES = DEMULTIPLIE LES COUPS
      if ht1=0 then %' 3 FIGURES DIFFERENTES
        gr.modify t1,"text","X-("
        if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      else %' 2 OU 3 FIGURES IDENTIQUES
        gr.modify t1,"text","= "+Stg$(ht1)+RPGLBL$[804]
        if !mute then Soundpool.play tmp,sndSwo,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      end if
      if Background() then Exit else gr.render
      Pause 500
      coups=coups+ht1
      gr.bitmap.drawinto.start tilesBmp
      gr.color 255,255,106,0,1
      gr.rect nul,(11.5-0.5*Mod(i,2))*tilew+4*tilew/6-0.75*tilew,di_h-i*tileh,(11.5-0.5*Mod(i,2))*tilew+4*tilew*5/6+0.75*tilew,di_h-(i-1)*tileh
      gr.bitmap.draw nul,heroTile[hr1,_right+1],(11.5-0.5*Mod(i,2))*tilew+4*tilew/6-tilew/2,di_h-i*tileh
      gr.bitmap.draw nul,heroTile[hr2,_right+1],(11.5-0.5*Mod(i,2))*tilew+4*tilew/2-tilew/2,di_h-i*tileh
      gr.bitmap.draw nul,heroTile[hr3,_right+1],(11.5-0.5*Mod(i,2))*tilew+4*tilew*5/6-tilew/2-2,di_h-i*tileh
      gr.text.bold 1
      gr.text.draw nul,(11.5-0.5*Mod(i,2))*tilew+4*tilew*5/6+0.75*tilew,di_h-(i-1)*tileh-3,"+"+Stg$(ht1)
      gr.text.bold 0
      gr.bitmap.drawinto.end
      gr.hide t1
      gr.hide vPtr1
      gr.hide vPtr2
      gr.hide vPtr3
      gr.hide vPtr4
      gr.hide vPtr5
      gr.hide vPtr6
      if !mute then Soundpool.play tmp,sndPRo,0.99,0.99,1,0,1 %' bandit manchot fini -> empilé
      if Background() then Exit else gr.render
      gr.show t1
      gr.show vPtr1
      gr.show vPtr2
      gr.show vPtr3
      Pause 500
    next i
    gr.hide tilesPtr
    gr.hide vPtr0
    gr.hide vPtr1
    gr.hide vPtr2
    gr.hide vPtr3
    if coups then %' L'ennemi est touché
      gr.modify t1,"text",Stg$(coups)+RPGLBL$[805]
      gr.modify t1,"x",7*tilew
      gr.show t1
      for y=di_h+0.75*tileh to 8.75*tileh step -tileh/2
        gr.modify t1, "y", y
        if Background() then Exit else gr.render
        Pause 4
      next y
      Pause 500
      gr.modify t2,"text","+"+Stg$(ata)+RPGLBL$[806]
      gr.modify t2,"x",7*tilew
      gr.show t2
      for y=di_h+tileh to 10*tileh step -tileh/2
        gr.modify t2, "y", y
        if Background() then Exit else gr.render
        Pause 4
      next y
      Pause 500
      gr.modify t3,"text","/"+Stg$(ennDef)+RPGLBL$[807]
      gr.modify t3,"x",7*tilew
      gr.show t3
      for y=di_h+0.25*tileh to 11.25*tileh step -tileh/2
        gr.modify t3, "y", y
        if Background() then Exit else gr.render
        Pause 4
      next y
      Pause 500
      gr.modify t4,"text","="+Stg$((4-2*difficulte/100)*(coups+ata)/ennDef)+RPGLBL$[763]+Stg$(ennPdv)
      gr.modify t4,"x",7*tilew
      gr.show t4
      for y=di_h+0.5*tileh to 12.5*tileh step -tileh/2
        gr.modify t4, "y", y
        if Background() then Exit else gr.render
        Pause 4
      next y
      Pause 1000
      gr.hide t1
      gr.hide t2
      gr.hide t3
      gr.hide t4
      gr.bitmap.delete ennBigBmp
      gr.bitmap.scale ennBigBmp, DragonBlesse, ennw*4, ennh*4, 0
      gr.modify ennPtr, "bitmap", ennBigBmp
      if Background() then Exit else gr.render
      for i=1 to 3
        if !mute then Soundpool.play tmp,sndPRo,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
        Pause 150
      next i
      Pause 500
      gr.bitmap.delete ennBigBmp
      gr.bitmap.scale ennBigBmp, DragonRepos, ennw*4, ennh*4, 0
      gr.modify ennPtr, "bitmap", ennBigBmp
      ennPdv=Floor(ennPdv-(4-2*difficulte/100)*(coups+ata)/ennDef)
      if ennPdv<=0 then ennPdv=0
      gr.modify ennPdvPtr, "right", tilew*(1.1+3*ennPdv/ennMaxPdv)
      if Background() then Exit else gr.render
    else %' L'ennemi n'a pas été touché
      if !mute then Soundpool.play tmp,sndFbd,0.99,0.99,1,0,1
      gr.modify t1,"text",RPGLBL$[808]
      gr.modify t1,"x",7*tilew
      gr.show t1
      for y=di_h+tileh to 10*tileh step -tileh/2
        gr.modify t1, "y", y
        if Background() then Exit else gr.render
        Pause 4
      next y
      Pause 1000
      gr.hide t1
      if Background() then Exit else gr.render
    end if
    fighter=1-fighter
  end if

until !ennPdv | !pdv
!' fin du combat : on nettoie le champ de bataille et on rend la main
if pdv=0 then
  Goto TheEnd
else if ennPdv=0 then
  perso[28]=1 %' le Seigneur Dragon est mort !
  Gosub MusiqueChange
  gr.bitmap.delete ennBigBmp
  gr.bitmap.scale ennBigBmp, DragonBlesse, ennw*4, ennh*4, 0
  gr.modify ennPtr, "bitmap", ennBigBmp
  gr.show tilesPtr
  gr.modify tilesPtr,"alpha",128
  gr.bitmap.drawinto.start tilesBmp
  hr1=-1
  for ht2=1 to 2
    for ht1=0 to 2*4.3 step 4.3/20
      if ht1<=4.3 then ht3=ht1 else ht3=2*4.3-ht1
      hr1=hr1+1
      if Mod(hr1,16)=0 then
        if !mute then Soundpool.play tmp,sndPRo,0.99,0.99,1,0,1 else vibrate VibrCrt[], -1
      end if
      e$=RainbowColor$(ht3)
      Call SetRGB(255,e$,1)
      gr.rect nul,0,0,nthor*tilew,ntver*tileh
      if Background() then Exit else gr.render
      Pause 4
    next ht1
  next ht2
  gr.bitmap.drawinto.end
  gr.hide tilesPtr
  if Background() then Exit else gr.render
  Pause 1000
  gr.bitmap.delete ennBigBmp
  gr.bitmap.scale ennBigBmp, DragonMort, ennw*4, ennh*4, 0
  gr.modify ennPtr, "bitmap", ennBigBmp
  if Background() then Exit else gr.render
  Pause 3000
  if Background() then Exit else Gosub FadeOut
end if
gr.cls %' Call gClear()
if Background() then Exit else gr.render
gr.bitmap.delete tmpBmp
gr.bitmap.delete tilesBmp
gr.bitmap.delete ennBigBmp
gr.bitmap.delete heroBigBmp
c=47 %' dragon mort sur l'île
destx=6
desty=241
gosub MovC
c=48
destx=7
desty=241
gosub MovC
c=49
destx=8
desty=241
gosub MovC
c=57
destx=6
desty=242
gosub MovC
c=58
destx=7
desty=242
gosub MovC
c=59
destx=8
desty=242
gosub MovC
Gosub InitEcranDeJeu
if Background() then Exit else Gosub FadeIn
if pdv>0 then Gosub SauverPartie
Pause 2000
c=2
destx=3
desty=235
Goto BouclePrincipale
!'---------------------------------------------------------------------------------------------------

SceneDeFin:
!'---------------------------------------------------------------------------------------------------
!' Scène de fin du jeu
!'---------------------------------------------------------------------------------------------------
if pdv>0 then Gosub SauverPartie
if !mute then
  audio.stop
  if musPtr then audio.release musPtr
  audio.load musPtr, "finished.mid"
  audio.play musPtr
end if
Pause 2000
gr.color 255,255,255,255,1
gr.rect rectPtr, 0, 0, di_w, di_h
for i=0 to 255 step 15
  gr.modify rectPtr, "alpha", i
  gr.render
  Pause 20
next i
gr.cls %' Call gClear()
gr.color 255,15,203,255,1
!' il y a le ciel :
gr.rect nul,0,0,di_w,di_h
!' le soleil :
gr.color 255,0,0,0,1
gr.bitmap.create bgnBmp, (nthor+1)*tilew, 4*tileh
gr.bitmap.drawinto.start bgnBmp
for y=0 to 3*tileh step tileh
  for x=0 to nthor*tilew step tilew
    gr.bitmap.draw nul, bgnTile[56], x, y
  next x
next y
gr.bitmap.drawinto.end
gr.bitmap.draw bgnPtr, bgnBmp, 0, di_h-3.8*tileh
!' et la mer :
gr.color 20,255,216,0,1
for i=Floor(ntver/2)*tileh to 10 step -5
  gr.circle nul, Floor(nthor/2)*tilew, 0, i
next i
!' mais aussi des nuages :
gr.color 100,255,255,255,1
x=Floor(nthor/2*tilew*Rnd())
y=Floor(6*tilew*Rnd())
gr.oval a1, x, y, x+Ceil(3*Rnd()+3)*tilew, y+Ceil(4*Rnd())*tilew
x=Floor(nthor/2*tilew*Rnd())
y=Floor(6*tilew*Rnd())
gr.oval a2, x, y, x+Ceil(3*Rnd()+3)*tilew, y+Ceil(4*Rnd())*tilew
x=Floor(nthor/2*tilew*Rnd())
y=Floor(6*tilew*Rnd())
gr.oval b1, x, y, x+Ceil(3*Rnd()+3)*tilew, y+Ceil(4*Rnd())*tilew
x=Floor(nthor/2*tilew*Rnd())
y=Floor(6*tilew*Rnd())
gr.oval b2, x, y, x+Ceil(3*Rnd()+3)*tilew, y+Ceil(4*Rnd())*tilew
!' et enfin un radeau et deux amoureux :
gr.color 255,0,0,0,1
gr.bitmap.draw ennPtr, objTile[1], -2*tilew, (ntver-4)*tileh
gr.bitmap.create tmpBmp, 4*tilew, 3*tileh
gr.bitmap.drawinto.start tmpBmp
for y=0 to 2*tileh step tileh
  for x=0 to 3*tilew step tilew
    gr.bitmap.draw nul, bgnTile[13], x, y
  next x
next y
gr.bitmap.draw nul, chrTile[3], tilew, 0
gr.bitmap.draw nul, heroTile[1,16], 2*tilew, 0
gr.bitmap.drawinto.end
gr.bitmap.draw tmpPtr, tmpBmp, -4*tilew, (ntver-2)*tileh
gr.hide tmpPtr
!' ...et des amis pour la fin alternative ;o)
zz=0
if objet[44] then
  if tilesBmp then gr.bitmap.delete tilesBmp
  gr.bitmap.create tilesBmp, 3*tilew, 2*tileh
  gr.bitmap.drawinto.start tilesBmp
  gr.bitmap.draw nul, chrTile[89], 0, 0
  gr.bitmap.draw nul, chrTile[24], tilew, 0
  gr.bitmap.draw nul, chrTile[90], 2*tilew, 0
  gr.bitmap.draw nul, chrTile[33], 0, tileh
  gr.bitmap.draw nul, chrTile[34], tilew, tileh
  gr.bitmap.draw nul, chrTile[35], 2*tilew, tileh
  gr.bitmap.drawinto.end
  gr.bitmap.draw tilesPtr, tilesBmp, -4*tilew, (ntver-2)*tileh
  zz=2.2*tilew
end if
!' score final
e$=RPGLBL$[791]
Time y$,m$,d$,h$,mn$,s$
score=10*victories*(difficulte+1)*(1+neq/25) %' 25 objets trouvables en tout dans l'aventure
date$=Stg$(val(d$))+" "+Mois$[val(m$)]+" "+y$+RPGLBL$[792]+h$+":"+mn$+":"+s$
e$=Stg$(score)+RPGLBL$[793]+date$+" --> "+e$
Text.open a, nul, "../scores.txt"
Encrypt MDP$, e$, s$
Text.writeln nul, s$
Text.close nul
gr.text.size 14
gr.text.align 3
e$=RPGLBL$[794]+Stg$(score)
Call Label(di_w-tilew,tileh,e$)
gr.text.skew -0.15
gr.text.align 2
!' animation paisible :
zd=0
for z=1 to tilew %' la mer défile tout simplement
  Gosub OutroAnimeDecor
next z
gr.show tmpPtr
for z=-4*tilew to di_w/2-2*tilew step 2 %' le radeau se déplace de la gauche au centre de l'écran
  gr.modify tmpPtr, "x", z-zz
  if objet[44] then gr.modify tilesPtr, "x", z+zz
  Gosub OutroAnimeDecor
next z
for z=0 to 4*tilew step 2 %' le radeau est stable au centre de l'écran
  Gosub OutroAnimeDecor
next z
hr1=2
hr2=0
do
  if !hr2 then
    Audio.isdone hr3
    if hr3 then
      gr.text.size 36
      Call Label(di_w/2,5*tileh,RPGLBL$[795])
      hr2=1
    end if
  elseif hr2=1 then
    gr.text.size 16
    Call Label(di_w/2,7*tileh,RPGLBL$[796])
    hr2=2
  end if
  if hr1=1 then
    ht1=1
    ht2=19
  elseif hr1=2 then
    ht1=29
    ht2=7
  elseif hr1=3 then
    ht1=11
    ht2=20
  elseif hr1=4 then
    ht1=7
    ht2=72
  elseif hr1=5 then
    ht1=70
    ht2=73
  elseif hr1=6 then
    ht1=28
    ht2=69
  elseif hr1=7 then
    ht1=9
    ht2=18
  elseif hr1=8 then
    ht1=24
    ht2=12
  elseif hr1=9 then
    ht1=16
    ht2=10
  elseif hr1=10 then
    ht1=41
    ht2=8
  elseif hr1=11 then
    ht1=33
    ht2=50
  elseif hr1=12 then
    ht1=0
    ht2=40
  elseif hr1=13 then
    ht1=45
    ht2=60
  elseif hr1=14 then
    ht1=43
    ht2=88
  elseif hr1=15 then
    ht1=59
    ht2=81
  elseif hr1=16 then
    ht1=20
    ht2=27
  end if
  gr.bitmap.delete equBmp
  gr.bitmap.create equBmp, 4*tilew, 4*tileh
  gr.bitmap.drawinto.start equBmp
  for y=0 to 3*tileh step tileh
    for x=0 to 3*tilew step tilew
      if ht1 then gr.bitmap.draw nul, bgnTile[ht1], x, y
    next x
  next y
  gr.bitmap.draw nul, chrTile[ht2], (2-Mod(hr1,2))*tilew, 1.5*tileh
  if ht2=69 then
    gr.bitmap.draw nul, chrTile[79], (2-Mod(hr1,2))*tilew, 2.5*tileh
  elseif ht2=12 then
    gr.bitmap.draw nul, chrTile[11], (1-Mod(hr1,2))*tilew, 1.5*tileh
    gr.bitmap.draw nul, chrTile[13], (3-Mod(hr1,2))*tilew, 1.5*tileh
  elseif ht2=40 then
    gr.bitmap.draw nul, chrTile[39], (1-Mod(hr1,2))*tilew, 1.5*tileh
    gr.bitmap.draw nul, chrTile[30], (2-Mod(hr1,2))*tilew, 0.5*tileh
    gr.bitmap.draw nul, chrTile[29], (1-Mod(hr1,2))*tilew, 0.5*tileh
  elseif ht2=27 then
    gr.bitmap.draw nul, chrTile[26], (1-Mod(hr1,2))*tilew, 1.5*tileh
    gr.bitmap.draw nul, chrTile[28], (3-Mod(hr1,2))*tilew, 1.5*tileh
    gr.bitmap.draw nul, chrTile[36], (1-Mod(hr1,2))*tilew, 2.5*tileh
    gr.bitmap.draw nul, chrTile[37], (2-Mod(hr1,2))*tilew, 2.5*tileh
    gr.bitmap.draw nul, chrTile[38], (3-Mod(hr1,2))*tilew, 2.5*tileh
  end if
  gr.bitmap.drawinto.end
  gr.modify ennPtr, "bitmap", equBmp
  hr1=Mod(hr1,16)+1
  if Mod(hr1,2) then
    ht1=-4*tilew
    ht2=0
    ht3=2
  else
    ht1=nthor*tilew
    ht2=(nthor-4)*tilew
    ht3=-2
  end if
  for z=ht1 to ht2 step ht3 %' le rivage apparait
    gr.modify ennPtr, "x", z
    Gosub OutroAnimeDecor
  next z
  for z=0 to 4*tilew step 2 %' le radeau est stable au centre de l'écran
    Gosub OutroAnimeDecor
  next z
  for z=ht2 to ht1 step -ht3 %' le rivage disparait
    gr.modify ennPtr, "x", z
    Gosub OutroAnimeDecor
  next z
  for z=0 to 4*tilew step 2 %' le radeau est stable au centre de l'écran
    Gosub OutroAnimeDecor
  next z
until 0
OutroAnimeDecor:
!' radeau oscille au gré des vagues
i=Mod(i+1,2*48)
gr.modify tmpPtr, "y", (ntver-3+0.3*Sin(i*Pi/48))*tileh
if objet[44] then gr.modify tilesPtr, "y", (ntver-3-0.3*Sin(i*Pi/48))*tileh
!' anime nuage 1
gr.get.value a1,"left",zl
gr.get.value a1,"right",zr
zr=zr-zl
zl=zl+1
if zl>di_w then zl=-di_w
zr=zr+zl
gr.modify a1, "left", zl
gr.modify a1, "right", zr
!' anime nuage 2
gr.get.value a2,"left",zl
gr.get.value a2,"right",zr
zr=zr-zl
zl=zl+2
if zl>di_w then zl=-di_w
zr=zr+zl
gr.modify a2, "left", zl
gr.modify a2, "right", zr
!' anime nuage 3
gr.get.value b1,"left",zl
gr.get.value b1,"right",zr
zr=zr-zl
zl=zl+3
if zl>di_w then zl=-di_w
zr=zr+zl
gr.modify b1, "left", zl
gr.modify b1, "right", zr
!' anime nuage 4
gr.get.value b2,"left",zl
gr.get.value b2,"right",zr
zr=zr-zl
zl=zl+4
if zl>di_w then zl=-di_w
zr=zr+zl
gr.modify b2, "left", zl
gr.modify b2, "right", zr
!' la mer défile
zd = Mod(zd+tilew-2, tilew)
gr.modify bgnPtr, "x", zd-tilew
if Background() then Exit else gr.render
Pause 2
Return
!'---------------------------------------------------------------------------------------------------

EntreSortBatiment:
!'---------------------------------------------------------------------------------------------------
!' Routine d'entrée/sortie dans 1 bâtiment (équivaut à une téléportation aux coordonnées destx,desty)
!'---------------------------------------------------------------------------------------------------
gr.cls %' Call gClear()
coeurPtr=0
flashPtr=0
gr.color 255,0,0,0,1
light=0 %' annule la baisse de luminosité (si la bougie est en train de s'éteindre)
Gosub FromDuskTillDawn %' définit l'obscurité de destination
gr.bitmap.create tmpBmp, (nthor+2)*tilew, (ntver+2)*tileh
gr.bitmap.drawinto.start tmpBmp
gr.bitmap.draw tmpPtr,bgnBmp,0,0
gr.bitmap.drawinto.end
gr.bitmap.draw bgnPtr,tmpBmp,-tilew,-tileh
gr.rect dknsPtr, 0, 0, di_w, di_h
gr.modify dknsPtr,"alpha",dkns
gr.color 255,255,255,255,1
gr.rect z,0,0,di_w,di_h
gr.modify z,"alpha",0
gr.bitmap.draw heroPtr, heroTile[heroform,DerDir+1], Floor(nthor/2)*tilew, Floor(ntver/2)*tileh
if GhostMode then gr.modify heroPtr, "alpha", 128
if tricheZeroEnnemi & heroform<>5 then gr.bitmap.draw tmp,shieldBmp,(Floor(nthor/2)+x)*tilew,Floor(ntver/2)*tileh
gr.render
herox=destx+1
heroy=desty+1
w=0
for y=heroy+dy-Floor(ntver/2)-1 to heroy+dy+Floor(ntver/2)+1
  if Mod(y,2)=0 then
    w=Mod(w+1,2)
    gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+2+w]
  end if
  for x=herox+dx-Floor(nthor/2)-1 to herox+dx+Floor(nthor/2)+1
    Gosub AfficherDecor
    if Mod(x-herox-dx,5)=0 then
      if dknsTgt=0 then %' le héros ressort en plein jour : voile blanc d'éblouissement
        alpha=255*((nthor*(y-heroy-dy+Floor(ntver/2)+1)+x-herox-dx+Floor(nthor/2)+1)/((nthor+2)*(ntver+2)))^0.3
        gr.modify z,"alpha",alpha
      else %' le héros va vers un environnement plus sombre : on baisse graduellement la luminosité
        alpha=dkns+(255-dkns)*((nthor*(y-heroy-dy+Floor(ntver/2)+1)+x-herox-dx+Floor(nthor/2)+1)/((nthor+2)*(ntver+2)))^0.3
        gr.modify dknsPtr,"alpha",alpha
      end if
      gr.render
    end if
  next x
next y
gr.bitmap.delete tmpBmp
DerDir=NewDir
gr.cls %' Call gClear()
gr.color 255,0,0,0,1
gr.bitmap.draw bgnPtr,bgnBmp,-tilew,-tileh
gr.rect dknsPtr, 0, 0, di_w, di_h
gr.modify dknsPtr,"alpha",dknsTgt
if dknsTgt<>0 then gr.modify dknsPtr,"alpha",255 %' évol de nuit noire vers l'env. + sombre
gr.color 255,255,255,255,1
gr.rect z,0,0,di_w,di_h
if dknsTgt<>0 then gr.modify z,"alpha",0 %' en ce cas pas d'effet flash blanc
gr.bitmap.draw heroPtr, heroTile[heroform,DerDir+1], (Floor(nthor/2))*tilew, Floor(ntver/2)*tileh
if GhostMode then gr.modify heroPtr, "alpha", 128
if tricheZeroEnnemi & heroform<>5 then gr.bitmap.draw tmp,shieldBmp,(Floor(nthor/2)+x)*tilew,Floor(ntver/2)*tileh
Gosub InitBoutonsEcranDeJeu
gr.render
!' Héros en mouvement
zu=0
zd=0
zr=0
zl=0
if DerDir=_up then
  zu=1
elseif DerDir=_down then
  zd=1
elseif DerDir=_right then
  zr=1
elseif DerDir=_left then
  zl=1
end if
herox=herox + zr - zl
heroy=heroy + zd - zu
if zr | zl then
  for x=1 to tilew
    gr.modify bgnPtr,"x",-tilew + x*(zl - zr)
    if Floor(4*x/tilew)<>Floor(4*(x-1)/tilew) then
      if Floor(4*x/tilew)=1 then
        gr.modify heroPtr,"bitmap",heroTile[heroform,_right*zr+_left*zl+2]
      elseif Floor(4*x/tilew)=2 then
        gr.modify heroPtr,"bitmap",heroTile[heroform,_right*zr+_left*zl+3]
      elseif Floor(4*x/tilew)=3 then
        gr.modify heroPtr,"bitmap",heroTile[heroform,_right*zr+_left*zl+2]
      elseif Floor(4*x/tilew)=4 then
        gr.modify heroPtr,"bitmap",heroTile[heroform,_right*zr+_left*zl+1]
      end if
    end if
    if dknsTgt=0 then
      gr.modify z,"alpha",255*(1-x/tilew) %' le voile blanc d'éblouissement se dissipe
    else
      gr.modify dknsPtr,"alpha",255-(255-dknsTgt)*x/tilew %' nuit noire -> obscurité - prononcée
    end if
    gr.render
    Pause 2
  next x
elseif zu | zd then
  for y=1 to tileh
    gr.modify bgnPtr,"y",-tileh + y*(zu - zd)
    if Floor(4*y/tileh)<>Floor(4*(y-1)/tileh) then
      if Floor(4*y/tileh)=1 then
        gr.modify heroPtr,"bitmap",heroTile[heroform,_up*zu+_down*zd+2]
      elseif Floor(4*y/tileh)=2 then
        gr.modify heroPtr,"bitmap",heroTile[heroform,_up*zu+_down*zd+3]
      elseif Floor(4*y/tileh)=3 then
        gr.modify heroPtr,"bitmap",heroTile[heroform,_up*zu+_down*zd+2]
      elseif Floor(4*y/tileh)=4 then
        gr.modify heroPtr,"bitmap",heroTile[heroform,_up*zu+_down*zd+1]
      end if
    end if
    if dknsTgt=0 then
      gr.modify z,"alpha",255*(1-y/tileh) %' le voile blanc d'éblouissement se dissipe
    else
      gr.modify dknsPtr,"alpha",255-(255-dknsTgt)*y/tileh %' nuit noire -> obscurité - prononcée
    end if
    gr.render
    Pause 2
  next y
end if
!' Décalage de la carte
gr.color 255,0,0,0,1
gr.bitmap.crop tmpBmp, bgnBmp, tilew*zr, tileh*zd, (nthor+1+zu+zd)*tilew, (ntver+1+zl+zr)*tileh
gr.bitmap.delete bgnBmp
gr.bitmap.create bgnBmp, (nthor+2)*tilew, (ntver+2)*tileh
gr.bitmap.drawinto.start bgnBmp
gr.bitmap.draw tmpPtr, tmpBmp, tilew*zl, tileh*zu
gr.bitmap.drawinto.end
gr.bitmap.delete tmpBmp
gr.modify bgnPtr,"x",-tilew
gr.modify bgnPtr,"y",-tileh
gr.modify bgnPtr,"bitmap",bgnBmp
!' Mise à jour des bords de la carte
if zr | zl then
  x=herox+dx+(zr-zl)*(Floor(nthor/2)+1)
  for y=heroy+dy-Floor(ntver/2)-1 to heroy+dy+Floor(ntver/2)+1
    Gosub AfficherDecor
  next y
elseif zu | zd then
  y=heroy+dy+(zd-zu)*(Floor(ntver/2)+1)
  for x=herox+dx-Floor(nthor/2)-1 to herox+dx+Floor(nthor/2)+1
    Gosub AfficherDecor
  next x
end if
dkns=dknsTgt
Gosub InitEcranDeJeu
gr.render
bloque=1 %' pour éviter les ennemis à la sortie de la tornade
Goto CheckObjet %' au cas où l'on atterrit sur un objet
!'---------------------------------------------------------------------------------------------------

Tornado:
!'---------------------------------------------------------------------------------------------------
!' Routine de téléportation aux coordonnées destx,desty
!'---------------------------------------------------------------------------------------------------
if !mute then
  audio.stop
  audio.load snd, "tornade.mp3"
  audio.stop
  audio.play snd
end if
DerDir=_down
gr.cls %' Call gClear()
coeurPtr=0
flashPtr=0
gr.color 255,0,0,0,1
light=0 %' annule la baisse de luminosité (si la bougie est en train de s'éteindre)
Gosub FromDuskTillDawn %' définit l'obscurité de destination
gr.bitmap.create tmpBmp, (nthor+2)*tilew, (ntver+2)*tileh
gr.bitmap.drawinto.start tmpBmp
gr.bitmap.draw tmpPtr,bgnBmp,0,0
gr.bitmap.drawinto.end
gr.bitmap.draw bgnPtr,tmpBmp,-tilew,-tileh
gr.rect dknsPtr, 0, 0, di_w, di_h
gr.modify dknsPtr,"alpha",dkns
gr.color 255,255,255,255,1
gr.rect z,0,0,di_w,di_h
gr.modify z,"alpha",0
gr.bitmap.draw heroPtr, heroTile[heroform,DerDir+1], (Floor(nthor/2))*tilew, Floor(ntver/2)*tileh
if GhostMode then gr.modify heroPtr, "alpha", 128
gr.render
herox=destx+1
heroy=desty+1
w=0
for y=heroy+dy-Floor(ntver/2)-1 to heroy+dy+Floor(ntver/2)+1
  for x=herox+dx-Floor(nthor/2)-1 to herox+dx+Floor(nthor/2)+1
    Gosub AfficherDecor
    if Mod(x-herox-dx,5)=0 then
      w=Mod(w+30,360)
      gr.hide heroPtr
      gr.rotate.start w, di_w/2, di_h/2
      gr.bitmap.draw heroPtr, heroTile[heroform,DerDir+1], Floor(nthor/2)*tilew, Floor(ntver/2)*tileh
      if GhostMode then gr.modify heroPtr, "alpha", 128
      gr.rotate.end
      alpha=255*((nthor*(y-heroy-dy+Floor(ntver/2)+1)+x-herox-dx+Floor(nthor/2)+1) / ((nthor+2)*(ntver+2)))^0.3
      gr.modify z,"alpha",alpha
      gr.render
    end if
  next x
next y
gr.bitmap.delete tmpBmp
dkns=dknsTgt
gr.cls %' Call gClear()
gr.color 255,0,0,0,1
gr.bitmap.draw bgnPtr,bgnBmp,-tilew,-tileh
gr.rect dknsPtr, 0, 0, di_w, di_h
gr.modify dknsPtr,"alpha",dkns
gr.color 255,255,255,255,1
gr.rect z,0,0,di_w,di_h
gr.bitmap.draw heroPtr, heroTile[heroform,DerDir+1], (Floor(nthor/2))*tilew, Floor(ntver/2)*tileh
if GhostMode then gr.modify heroPtr, "alpha", 128
Gosub InitBoutonsEcranDeJeu
gr.render
!' Héros continue de tournoyer
for x=1 to tilew
  w=Mod(w+30,360)
  gr.hide heroPtr
  gr.rotate.start w, di_w/2, di_h/2
  gr.bitmap.draw heroPtr, heroTile[heroform,DerDir+1], Floor(nthor/2)*tilew, Floor(ntver/2)*tileh
  if GhostMode then gr.modify heroPtr, "alpha", 128
  gr.rotate.end
  gr.modify z,"alpha",255*(1-x/tilew)
  gr.render
  Pause 50
next x
Gosub InitEcranDeJeu
gr.render
if !mute then
  audio.stop
  audio.release snd
end if
Gosub MusiqueChange
bloque=1 %' pour éviter les ennemis à la sortie de la tornade
Goto CheckObjet %' au cas où l'on atterrit sur un objet
!'---------------------------------------------------------------------------------------------------

FollowTheTrail:
!'---------------------------------------------------------------------------------------------------
!' Routine de vérification d'un tracé à l'écran sur une piste définie (tracé des runes élémentales)
!'---------------------------------------------------------------------------------------------------
vibrate VibrCrt[], -1
gr.bitmap.load trailBmp,e$
red=0
green=0
yellow=0
do
  gr.touch tmp, x, y
  x=x/scale_w
  y=y/scale_h
  gr.get.bmpixel trailBmp,x,y,alpha,red,i,blue
  if i=255 then
    if red=255 then
      yellow=1
      red=0
    else
      green=1
    end if
  end if
until red | !tmp | !alpha
gr.bitmap.delete trailBmp
Return
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Sous-routines "GOSUB" diverses
!'---------------------------------------------------------------------------------------------------
FadeIn:
gr.color 255,0,0,0,1
gr.rect rectPtr, 0, 0, di_w, di_h
for i=255 to 0 step -15
  gr.modify rectPtr, "alpha", i
  gr.render
  Pause 20
next i
gr.hide rectPtr
gr.render
Return

FadeOut:
gr.color 255,0,0,0,1
gr.rect rectPtr, 0, 0, di_w, di_h
for i=0 to 255 step 15
  gr.modify rectPtr, "alpha", i
  gr.render
  Pause 20
next i
gr.hide rectPtr
Return

Flash:
if !flashPtr then
  Call SetRGB(255,e$,1)
  gr.rect flashPtr,0,0,di_w,di_h
else
  gr.show flashPtr
end if
for i=0 to 255*2 step 32
  if i<256 then alpha=i else alpha=255*2-i
  gr.modify flashPtr, "alpha", alpha
  if light then Gosub FiatLux else gr.render
  Pause 5
next i
gr.hide flashPtr
Return

DebloqueSecretMusique:
if heroform=1 then gr.modify heroPtr,"bitmap",heroTile[heroform,15]
if light then Gosub FiatLux else gr.render
if !mute then
  l=GetAudioLength("debloque.mid")
  Soundpool.play tmp,sndRls,0.99,0.99,1,0,1
  audio.pause
  Pause l
  if music$<>"" then audio.play musPtr
else
  for i=1 to 3
    vibrate VibrCrt[], -1
    Pause 150
  next i
end if
gr.modify heroPtr,"bitmap",heroTile[heroform,DerDir+1]
if light then Gosub FiatLux else gr.render
Return

RefreshUsd:
For i=1 to 9
  gr.modify usdPtr[i], "text", Stg$(usd)
Next i
gr.render
Return

SauverCalques:
if !HideSaveProgress then
  x=di_w-23
  y=di_h-28
  gr.text.align 2
  gr.text.size 8
  gr.text.bold 1
  gr.color 255,0,0,0,1
  gr.bitmap.draw a3,diskBmp,di_w-39,di_h-39
  gr.text.draw b3,x,y,"0%"
  gr.text.bold 0
  gr.text.align 1
end if
!' Enregistre le calque des décors
byte.open w,map,"../CAD.BGN.SAV"
byte.write.byte map,bgnNasc
for i=1 to mapHmax
  byte.write.buffer map,bgnMap$[i]
  if !Mod(i,20) & !Background() & !HideSaveProgress then
    e$=Stg$(i/mapHmax*25)+"%"
    gr.modify b3,"text",e$
    if light then Gosub FiatLux else gr.render
  end if
next i
byte.close map
!' Enregistre le calque des solidités
byte.open w,map,"../CAD.SOL.SAV"
byte.write.byte map,solNasc
for i=1 to mapHmax
  byte.write.buffer map,solMap$[i]
  if !Mod(i,20) & !Background() & !HideSaveProgress then
    e$=Stg$(25+i/mapHmax*25)+"%"
    gr.modify b3,"text",e$
    if light then Gosub FiatLux else gr.render
  end if
next i
byte.close map
!' Enregistre le calque des objets
byte.open w,map,"../CAD.OBJ.SAV"
byte.write.byte map,objNasc
for i=1 to mapHmax
  byte.write.buffer map,objMap$[i]
  if !Mod(i,20) & !Background() & !HideSaveProgress then
    e$=Stg$(50+i/mapHmax*25)+"%"
    gr.modify b3,"text",e$
    if light then Gosub FiatLux else gr.render
  end if
next i
byte.close map
!' Enregistre le calque des persos
byte.open w,map,"../CAD.CHR.SAV"
byte.write.byte map,chrNasc
for i=1 to mapHmax
  byte.write.buffer map,chrMap$[i]
  if !Mod(i,20) & !Background() & !HideSaveProgress then
    e$=Stg$(75+i/mapHmax*25)+"%"
    gr.modify b3,"text",e$
    if light then Gosub FiatLux else gr.render
  end if
next i
byte.close map
needSaveMAPs=0
Return

SavePrefs:
Text.open w, tmp, "../prefs.txt"
Text.writeln tmp, LNG$
Text.writeln tmp, Stg$(mute)
Text.close tmp
Return

SauverPartie:
e$=IntToStr$(difficulte)
e$=e$+IntToStr$(herox)
e$=e$+IntToStr$(heroy)
e$=e$+IntToStr$(heroform)
e$=e$+IntToStr$(pdv)
e$=e$+IntToStr$(usd)
e$=e$+IntToStr$(ata)
e$=e$+IntToStr$(def)
Array.length w, objet[]
for i=1 to w
  e$=e$+IntToStr$(objet[i])
next i
e$=e$+IntToStr$(neq)
Array.length w, equipement[]
for i=1 to w
  e$=e$+IntToStr$(equipement[i])
next i
Array.length w, perso[]
for i=1 to w
  e$=e$+IntToStr$(perso[i])
next i
e$=e$+IntToStr$(batx)
e$=e$+IntToStr$(baty)
e$=e$+IntToStr$(dkns)
e$=e$+IntToStr$(victories)
for i=1 to w
  e$=e$+IntToStr$(chrX[i])
next i
for i=1 to w
  e$=e$+IntToStr$(chrY[i])
next i
e$=e$+music$
Byte.open w, w, "../jeu.sav"
Byte.write.buffer w, e$
Byte.close w
Sauvegarde=1
Return

ChargerSauvegarde:
File.size i, "../jeu.sav"
Byte.open r, o, "../jeu.sav"
Byte.read.buffer o, i, e$
Byte.close o
difficulte=GetIntFrom(&e$)
herox=GetIntFrom(&e$)
heroy=GetIntFrom(&e$)
heroform=GetIntFrom(&e$)
pdv=GetIntFrom(&e$)
usd=GetIntFrom(&e$)
ata=GetIntFrom(&e$)
def=GetIntFrom(&e$)
Array.length o, objet[]
for i=1 to o
  objet[i]=GetIntFrom(&e$)
next i
neq=GetIntFrom(&e$)
Array.length e, equipement[]
for i=1 to e
  equipement[i]=GetIntFrom(&e$)
next i
Array.length c, perso[]
for i=1 to c
  perso[i]=GetIntFrom(&e$)
next i
batx=GetIntFrom(&e$)
baty=GetIntFrom(&e$)
dkns=GetIntFrom(&e$)
victories=GetIntFrom(&e$)
for i=1 to c
  chrX[i]=GetIntFrom(&e$)
next i
for i=1 to c
  chrY[i]=GetIntFrom(&e$)
next i
music$=e$
Return
!'---------------------------------------------------------------------------------------------------

AfficherDecor:
!'---------------------------------------------------------------------------------------------------
!' Routine d'affichage des calques (affiche décor+objet+perso aux coordonnées x,y)
!'---------------------------------------------------------------------------------------------------
gr.color 255,0,0,0,1
gr.bitmap.drawinto.start bgnBmp
olddestx=destx
olddesty=desty
old=b
Gosub GetB
if b then
  gr.bitmap.draw nul, bgnTile[b], (x-herox-dx+Floor(nthor/2)+1)*tilew, (y-heroy-dy+Floor(ntver/2)+1)*tileh
else
  gr.rect nul,(x-herox-dx+Floor(nthor/2)+1)*tilew,(y-heroy-dy+Floor(ntver/2)+1)*tileh,(x-herox-dx+Floor(nthor/2)+2)*tilew,(y-heroy-dy+Floor(ntver/2)+2)*tileh
end if
b=old
old=o
Gosub GetO
if o then gr.bitmap.draw nul, objTile[o], (x-herox-dx+Floor(nthor/2)+1)*tilew, (y-heroy-dy+Floor(ntver/2)+1)*tileh
o=old
old=c
Gosub GetC
if c then gr.bitmap.draw nul, chrTile[c], (x-herox-dx+Floor(nthor/2)+1)*tilew, (y-heroy-dy+Floor(ntver/2)+1)*tileh
c=old
destx=olddestx
desty=olddesty
gr.bitmap.drawinto.end
Return
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' (B=background) Opérations sur le tile de DECOR aux coordonnées destx, desty
!'---------------------------------------------------------------------------------------------------
!' GET B=RECUPERATION DU DECOR @destx,desty
GetB:
destx=x-dx-1
desty=y-dy-1
b=0
for i=1 to bgnNasc
  b=b + Ascii(Mid$(bgnMap$[y],bgnNasc*(x-1)+i,1))*256^(bgnNasc-i)
next i
Return
!' SET B=MODIFICATION DU DECOR @destx,desty
SetB:
needSaveMAPs=1
x=destx+dx+1
y=desty+dy+1
b$=chr$(b)
while Len(b$)<bgnNasc
  b$=chr$(0) + b$
repeat
bgnMap$[y]=Left$(bgnMap$[y], bgnNasc*(x-1)) + b$ + Right$(bgnMap$[y], Len(bgnMap$[y])-bgnNasc*x)
if x>=herox+dx-Floor(nthor/2)-1 & x<=herox+dx+Floor(nthor/2)+1 then
  if y>=heroy+dy-Floor(ntver/2)-1 & y<=heroy+dy+Floor(ntver/2)+1 then
    Gosub AfficherDecor
    if light then Gosub FiatLux else gr.render
  end if
end if
Return
!' RAZ B=REMISE A ZERO DU DECOR @destx,desty (reprend la valeur par défaut telle qu'au début du jeu)
RazB:
needSaveMAPs=1
x=destx+dx+1
y=desty+dy+1
bgnMap$[y]=Left$(bgnMap$[y],bgnNasc*(x-1))+Mid$(bgnIni$[y],bgnNasc*x,bgnNasc)+Right$(bgnMap$[y],Len(bgnMap$[y])-bgnNasc*x)
if !RefreshOff then
  if x>=herox+dx-Floor(nthor/2)-1 & x<=herox+dx+Floor(nthor/2)+1 then
    if y>=heroy+dy-Floor(ntver/2)-1 & y<=heroy+dy+Floor(ntver/2)+1 then
      Gosub AfficherDecor
      if light then Gosub FiatLux else gr.render
    end if
  end if
end if
Return
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' (S=solidité) Opérations sur la SOLIDITE aux coordonnées destx, desty
!'---------------------------------------------------------------------------------------------------
!' GET S=RECUPERATION DE LA SOLIDITE @destx,desty
GetS:
destx=x-dx-1
desty=y-dy-1
s=0
for i=1 to solNasc
  s=s + Ascii(Mid$(solMap$[y],solNasc*(x-1)+i,1))*256^(solNasc-i)
next i
Return
!' SET S=MODIFICATION DE LA SOLIDITE @destx,desty
SetS:
needSaveMAPs=1
x=destx+dx+1
y=desty+dy+1
s$=chr$(s)
while Len(s$)<solNasc
  s$=chr$(0) + s$
repeat
solMap$[y]=Left$(solMap$[y], solNasc*(x-1)) + s$ + Right$(solMap$[y], Len(solMap$[y])-solNasc*x)
Return
!' RAZ S=REMISE A ZERO DE LA SOLIDITE @destx,desty
RazS:
needSaveMAPs=1
x=destx+dx+1
y=desty+dy+1
solMap$[y]=Left$(solMap$[y],solNasc*(x-1))+Mid$(solIni$[y],solNasc*x,solNasc)+Right$(solMap$[y],Len(solMap$[y])-solNasc*x)
Return
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' (O=objet) Opérations sur le tile d'OBJET aux coordonnées destx, desty
!'---------------------------------------------------------------------------------------------------
!' SET EOB = DEFINIR l'ETAT d'un OBJET BINAIRE en fonction de son aspect sur carte
SetEOB:
oIndx=IOM(destx,desty)
if !oIndx then Return %' gestion objet multiple en erreur
if o>=45 & o<=48 then %' poussoir-ressort libéré
  objet[oIndx]=0
elseif (o>=37 & o<=40) then %' poussoir-ressort enfoncé
  objet[oIndx]=1
elseif o=50 then %' levier d'aiguillage à gauche (position initiale)
  objet[oIndx]=0
elseif o=49 then %' levier d'aiguillage à droite (basculé)
  objet[oIndx]=1
end if
Return
!' GET O=RECUPERATION DE L'OBJET @destx,desty
GetO:
destx=x-dx-1
desty=y-dy-1
o=0
for i=1 to objNasc
  o=o + Ascii(Mid$(objMap$[y],objNasc*(x-1)+i,1))*256^(objNasc-i)
next i
if !o then Return
if Is_In(","+Stg$(o)+",", LOM$) then %' objet multiple
  Gosub SetEOB
else %' objet unique
  if objet[o]=1 then o=0 %' objet déjà trouvé
  oIndx=o
end if
Return
!' SET O=MODIFICATION DE L'OBJET @destx,desty
SetO:
needSaveMAPs=1
x=destx+dx+1
y=desty+dy+1
o$=chr$(o)
while Len(o$)<objNasc
  o$=chr$(0) + o$
repeat
objMap$[y]=Left$(objMap$[y], objNasc*(x-1)) + o$ + Right$(objMap$[y], Len(objMap$[y])-objNasc*x)
if x>=herox+dx-Floor(nthor/2)-1 & x<=herox+dx+Floor(nthor/2)+1 then
  if y>=heroy+dy-Floor(ntver/2)-1 & y<=heroy+dy+Floor(ntver/2)+1 then
    Gosub AfficherDecor
    if light then Gosub FiatLux else gr.render
  end if
end if
if Is_In(","+Stg$(o)+",", LOM$) then Gosub SetEOB
Return
!' DEL O=SUPPRESSION DE L'OBJET @destx,desty
DelO:
needSaveMAPs=1
x=destx+dx+1
y=desty+dy+1
o$=chr$(0)
while Len(o$)<objNasc
  o$=chr$(0) + o$
repeat
objMap$[y]=Left$(objMap$[y], objNasc*(x-1)) + o$ + Right$(objMap$[y], Len(objMap$[y])-objNasc*x)
if x>=herox+dx-Floor(nthor/2)-1 & x<=herox+dx+Floor(nthor/2)+1 then
  if y>=heroy+dy-Floor(ntver/2)-1 & y<=heroy+dy+Floor(ntver/2)+1 then
    Gosub AfficherDecor
    if !pousseBloc then
      if light then Gosub FiatLux else gr.render
    end if
  end if
end if
Return
!' RAZ O=REMISE A ZERO DE L'OBJET @destx,desty
RazO:
needSaveMAPs=1
x=destx+dx+1
y=desty+dy+1
objMap$[y]=Left$(objMap$[y],objNasc*(x-1))+Mid$(objIni$[y],objNasc*x,objNasc)+Right$(objMap$[y],Len(objMap$[y])-objNasc*x)
if !RefreshOff then
  if x>=herox+dx-Floor(nthor/2)-1 & x<=herox+dx+Floor(nthor/2)+1 then
    if y>=heroy+dy-Floor(ntver/2)-1 & y<=heroy+dy+Floor(ntver/2)+1 then
      Gosub AfficherDecor
      if light then Gosub FiatLux else gr.render
    end if
  end if
end if
Return
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' (C=character) Opérations sur le tile de PERSONNAGE aux coordonnées destx, desty
!'---------------------------------------------------------------------------------------------------
!' GET C=RECUPERATION DU PERSO @destx,desty
GetC:
bx=x-batx-dx
by=y-baty-dy
if heroform<>5 & bx>=1 & bx<=3 & by>=1 & by<=2 then
  if perso[9]=1 then
    perso[9]=2
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    oldx=x
    oldy=y
    c=9
    Gosub DelC %' nain à bord de son bateau
    x=oldx
    y=oldy
    !'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  end if
  c=22+10*(by-1)+bx
  if perso[9]=2 then
    if bx=1 & by=1 then
      c=71
    elseif bx=3 & by=1 then
      c=70
    elseif bx=3 & by=2 then
      c=80
    end if
  end if
else
  c=0
  for i=1 to chrNasc
    c=c + Ascii(Mid$(chrMap$[y],chrNasc*(x-1)+i,1))*256^(chrNasc-i)
  next i
end if
destx=x-dx-1
desty=y-dy-1
Return
!' DEL C=SUPPRESSION DU PERSO c DE LA CARTE
DelC:
if chrX[c]=UNDEF | chrY[c]=UNDEF then Return
needSaveMAPs=1
x=chrX[c] + dx + 1
y=chrY[c] + dy + 1
c$=chr$(0)
while Len(c$)<chrNasc
  c$=chr$(0) + c$
repeat
chrMap$[y]=Left$(chrMap$[y], chrNasc*(x-1)) + c$ + Right$(chrMap$[y], Len(chrMap$[y])-chrNasc*x)
if x>=herox+dx-Floor(nthor/2)-1 & x<=herox+dx+Floor(nthor/2)+1 then
  if y>=heroy+dy-Floor(ntver/2)-1 & y<=heroy+dy+Floor(ntver/2)+1 then
    Gosub AfficherDecor
    if light then Gosub FiatLux else gr.render
  end if
end if
chrX[c]=UNDEF
chrY[c]=UNDEF
Return
!' MOV C=DEPLACEMENT DU PERSO c DE SA POS° ACTUELLE VERS (destx, desty)
MovC:
needSaveMAPs=1
Gosub DelC
x=destx+dx+1
y=desty+dy+1
c$=chr$(c)
while Len(c$)<chrNasc
  c$=chr$(0) + c$
repeat
chrMap$[y]=Left$(chrMap$[y], chrNasc*(x-1)) + c$ + Right$(chrMap$[y], Len(chrMap$[y])-chrNasc*x)
chrX[c]=destx
chrY[c]=desty
if x>=herox+dx-Floor(nthor/2)-1 & x<=herox+dx+Floor(nthor/2)+1 then
  if y>=heroy+dy-Floor(ntver/2)-1 & y<=heroy+dy+Floor(ntver/2)+1 then
    Gosub AfficherDecor
    if light then Gosub FiatLux else gr.render
  end if
end if
Return
!'---------------------------------------------------------------------------------------------------

