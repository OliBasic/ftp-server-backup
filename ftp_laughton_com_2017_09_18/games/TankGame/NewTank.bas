! Tank Game

GOSUB game_functions
GOSUB game_layout
GOSUB game_initiate
GOSUB game_main

! --- Main Loop ---

game_main:
	DO
		% Check for screen touch
		gr.touch touched, tx, ty
		gr.touch2 touched2, tx2, tx2
		
		GOSUB game_player
		GOSUB game_cpu
		GOSUB game_rules
		gr.render
	UNTIL 0
return

! --- Game Player ---

game_player:
	% Subprogram for controls check
	GOSUB game_player_control
	
	% Subprogram for player movement
	GOSUB game_player_movement
	
	% Subprogram for player_bomb movement
	GOSUB game_player_bomb
	
	gr.modify Player, "x", Player_X
	gr.modify Player, "y", Player_Y
	
	gr.modify PBomb, "x", PBomb_X
	gr.modify PBomb, "y", PBomb_Y
return

game_player_control:
	IF touched | touched2
		x = tx
		y = ty
		GOSUB game_control
		button_1 = BPressed
	
		x = tx2
		y = ty2
		GOSUB game_control
		button_2 = BPressed
	ENDIF
	
	GR.GET.POSITION PBOMB, PBX, PBY
	PRINT PBX;"|";PBY
	
	IF button_1 = 1 | button_2 = 1
		PlayerD = 1
	ELSEIF button_1 = 2 | button_2 = 2
		PlayerD = 2
	ELSEIF button_1 = 3 | button_2 = 3
		PlayerD = 3
	ELSEIF button_1 = 4 | button_2 = 4
		PlayerD = 4
	ENDIF
	
	IF button_1 = 5 | button_2 = 5
		IF Player_Ammo = 1
			Player_Ammo = 0
		ENDIF
		IF Player_Ammo = 0
			PBombD = PlayerD
		ENDIF
	ENDIF
	
	button_1 = 0
	button_2 = 0
return

game_player_movement:
	IF touched
		IF PlayerD = 1 & Player_X > GameS_x1
			Player_X -= MoveSpeed
		ELSEIF PlayerD = 2 & Player_Y > GameS_y1
			Player_Y -= MoveSpeed
		ELSEIF PlayerD = 3 & Player_X < GameS_x2
			Player_X += MoveSpeed
		ELSEIF PlayerD = 4 & Player_Y < GameS_y2
			Player_Y += MoveSpeed
		ENDIF
	ENDIF
return

game_player_bomb:
	IF Player_Ammo = 0
		IF PBomb_X < GameS_x1 | PBomb_Y < GameS_y1~
		| PBomb_X > GameS_x2 | PBomb_Y > GameS_Y2
			Player_Ammo = 1
		ELSEIF PBombD = 1
			PBomb_X -= BombSpeed
		ELSEIF PBombD = 2
			PBomb_Y -= BombSpeed
		ELSEIF PBombD = 3
			PBomb_X += BombSpeed
		ELSEIF PBombD = 4
			PBomb_Y += BombSpeed
		ENDIF
	ELSE
		PBomb_X = Player_X
		PBomb_Y = Player_Y
	ENDIF
return

! --- Game CPU ---

game_cpu:
return

! --- Game Rules ---

game_rules:
return

! --- Game Layout ---

game_layout:
	% Black background | Portrait oriented
	GR.OPEN 255, 0, 0, 0
	GR.ORIENTATION 1
	
	% Subprogram that resizes it to fit any device
	GOSUB game_layout_scale
	
	% Subprogram to draw the layout
	GOSUB game_layout_draw
return

game_layout_draw:
	% Set draw color
	GR.COLOR  255, 255, 255, 255, 0
	
	% Draw game screen
	GR.RECT GameS, GameS_x1, GameS_y1, GameS_x2, GameS_y2
	
	% Draw fire button
	GR.RECT FireB, FireB_x1, FireB_y1, FireB_x2, FireB_y2
	
	% Draw directionals
	GR.RECT LeftB, LeftB_x1, LeftB_y1, LeftB_x2, LeftB_y2
	GR.RECT UpB, UpB_x1, UpB_y1, UpB_x2, UpB_y2
	GR.RECT RightB, RightB_x1, RightB_y1, RightB_x2, RightB_y2
	GR.RECT DwnB, DwnB_x1, DwnB_y1, DwnB_x2, DwnB_y2
return

game_layout_scale:
	% Get screen resolution
	GR.SCREEN screen_W, screen_H
	
	% Here I will divide the screen in 20x40 squares
	square_W = screen_W/20
	square_H = screen_H/40
	
	% Here I create some arrays that will hold
	% the square positions x and y
	DIM PosX[20]
	DIM PosY[40]
	
	% Finally I write the square positions into the arrays
	FOR i = 1 TO 20
		PosX[i] = i*square_W
	NEXT
	FOR i = 1 TO 40
		PosY[i] = i*square_H
	NEXT	
		
	% Game screen coordinates
	GameS_x1 = PosX[2]
	GameS_x2 = PosX[18]
	GameS_y1 = PosY[2]
	GameS_y2 = PosY[22]
	
	% Fire button coordinates
	FireB_x1 = PosX[2]
	FireB_x2 = PosX[6]
	FireB_y1 = PosY[26]
	FireB_y2 = PosY[30]
	
	% Left button coordinates
	LeftB_x1 = PosX[6]
	LeftB_x2 = PosX[10]
	LeftB_y1 = PosY[30]
	LeftB_y2 = PosY[34]
	
	% Up button coordinates
	UpB_x1 = PosX[10]
	UpB_x2 = PosX[14]
	UpB_y1 = PosY[26]
	UpB_y2 = PosY[30]
	
	% Right button coordinates
	RightB_x1 = PosX[14]
	RightB_x2 = PosX[18]
	RightB_y1 = PosY[30]
	RightB_y2 = PosY[34]
	
	% Down button coordinates
	DwnB_x1 = PosX[10]
	DwnB_x2 = PosX[14]
	DwnB_y1 = PosY[34]
	DwnB_y2 = PosY[38]
return

! --- Game Functions ---

game_functions:
	% This function returns the number of the button pressed
	% 1 = left | 2 = up | 3 = right | 4 = down
	% 5 = fire | 0 = none
	
	game_control:
		% Button left
		IF x >= LeftB_x1 & x <= LeftB_x2
			IF y >= LeftB_y1 & y <= LeftB_y2
				BPressed =  1
			ELSE
				BPressed =  0
			ENDIF
		
		% Button up
		ELSEIF x >= UpB_x1 & x <= UpB_x2 & y <= UpB_y2
			IF y >= UpB_y1 & y <= UpB_y2
				BPressed =  2
			ELSE
				BPressed =  0
			ENDIF
		
		% Button right
		ELSEIF x >= RightB_x1 & x <= RightB_x2
			IF y >= RightB_y1 & y <= RightB_y2
				BPressed =  3
			ELSE
				BPressed =  0
			ENDIF
			
		% Button down
		ELSEIF x >= DwnB_x1 & x <= DwnB_x2
			IF y >= DwnB_y1 & y <= DwnB_y2
				BPressed =  4
			ELSE
				BPressed =  0
			ENDIF
		
		% Button fire
		ELSEIF x >= FireB_x1 & x <= FireB_x2
			IF y >= FireB_y1 & y <= FireB_y2
				BPressed =  5
			ELSE
				BPressed =  0
			ENDIF
		ELSE
			BPressed =  0
		ENDIF
	return
return

! --- Game Initiate ---

game_initiate:
	Player_X = PosX[17]
	Player_Y = PosY[21]
	Player_Ammo = 1
	PlayerD = 0
	PBomb_X = Player_X
	PBomb_Y = Player_Y
	
	CPU_X = PosX[3]
	CPU_Y = PosY[3]
	CBomb_X = CPU_X
	CBomb_Y = CPU_Y
	
	MoveSpeed = 10
	BombSpeed = 10

	gr.color 255, 0, 255, 0, 1
	gr.circle Player, Player_X, Player_Y, square_W
	gr.color 255, 255, 255, 255, 1
	gr.circle PBomb, PBomb_X, PBomb_Y, square_W/4
	
	gr.color 255, 255, 0, 0, 1
	gr.circle CPU, CPU_X, CPU_Y, square_W
return		