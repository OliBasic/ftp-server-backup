
Rem Rearrange Number
Rem For Android
Rem With RFO Basic!
Rem July 2016
Rem Version 1.00
Rem By Roy Shepherd

di_height = 672 % set to my Device
di_width = 1152

gr.open 255,251,49,155
gr.orientation 2 % Landscape 
pause 1000
WakeLock 3
 
gr.screen screenWidth, screenHeight
scale_x = screenWidth / di_width
scale_y = screenHeight / di_height
gr.scale scale_x, scale_y

!-------------------------------------------------
!Let Basic see the functions before they are called
!-------------------------------------------------
gosub Functions

gosub Initialise
gosub ShowNumbers

gosub SolvePuzzel

exit

onBackKey: 
  if buzz then vibrate buzzGame[], -1
  if sound then soundpool.play stream, cnote, 0.9, 0.9,0,0,freq[1]/261
  
  do : dialog.message "Quit Puzzle?",,yn,"Yes","No" : until yn > 0
  if yn = 1 then gosub SaveData : exit
	back.resume 
end


!-------------------------------------------------
! Do this stuff once
!-------------------------------------------------
Initialise:
    Path$="../../RearrangeNumber/data/"
    soundpool.open 1
    soundpool.load cnote,Path$ + "c.mp3"
    if cnote = 0 then do : dialog.message "Sound file c.mp3 not found",,ok,"OK" : until ok > 0 : exit
    array.load freq[],261,293,330,349,392,440,494,523,586,660,698
    array.load buzzGame[], 1, 100
    
    score = 0 : lowest = 0 : sound = 1 : buzz = 1 : playWithFire = 1
    gosub LoadData
    array.load numbers[], 0,1,2,3,4,5,6,7,8,9
    array.shuffle numbers[]
    gr.text.size 40
    gr.color 255,220,12,168 
    gr.rect top, 0,0,di_width,60
    gr.rect bot,0,di_height - 60,di_width,di_height
    gr.color 255,245,227,3
    gr.text.draw trys, 50,40,"Trys: " + int$(score)
    gr.text.align 3
    gr.text.draw low, di_width - 50,40,"Lowest: " + int$(lowest)
    
    gr.text.align 2
    gr.text.draw d1, di_width / 2, 450, "Rearrange the Numbers above to '0123456789'"
    gr.text.draw d2, di_width / 2, 500, "by tapping a number to swap it with the end number."
    
    gr.set.stroke 5
    gr.text.align 2
    gr.text.size 200
    gr.text.bold 
    gr.color  255,232,0,0
    gr.text.draw numIner, di_width / 2, di_height / 2, "0123456789"
    gr.color 255,0,0,0,0
    gr.text.draw numOuter, di_width / 2, di_height / 2, "0123456789"
    gr.render
    gr.text.size 30
    gr.set.stroke 1
    gr.text.bold
    gr.text.align 2
    
    dh = di_height-55 : b = di_height - 5
     ! Background music Button
    call Gcol(8,255,1) : gr.rect ptrBackgroundBut, 50,dh, 200, b
    call Gcol(4,255,1) 
    if playWithFire then 
      gr.text.draw prtBackgroundTxt, 125,b - 10, "Music OFF"
    else
      gr.text.draw prtBackgroundTxt, 125,b - 10, "Music ON"
    endif
    
    ! Sound Button
    call Gcol(8,255,1) 
    gr.rect ptrSoundBut, 250,dh, 400, b
    call Gcol(4,255,1) 
    if sound then 
      gr.text.draw prtSoundTxt, 325,b - 10, "Sound OFF"
    else
      gr.text.draw prtSoundTxt, 325,b - 10, "Sound ON"
    endif
    
    ! Buzz Button
    call Gcol(8,255,1) : gr.rect ptrBuzzBut, 450,dh, 600, b
    call Gcol(4,255,1) 
    if buzz then
      gr.text.draw prtBuzzTxt, 525,b - 10, "Buzz OFF"
    else
     gr.text.draw prtBuzzTxt, 525,b - 10, "Buzz ON"
    endif
    
    ! Zero Lowest Score button
    call Gcol(8,255,1) : gr.rect ptrLowBut, 650,dh, 800, b
    call Gcol(4,255,1) :  gr.text.draw prtLowTxt, 725,b - 10, "Zero Low"
    
    ! Exit Button
    call Gcol(8,255,1) : gr.rect ptrExitBut, 950,dh, 1100, b
    call Gcol(4,255,1) : gr.text.draw prtExitTxt, 1025,b - 10, "Exit"
    
    gr.render
    
    call Gcol(9,0,1)
    gr.point collision, - 1, - 1 
    
    ! Loop sound
    audio.load WithFire, Path$ + "PlayWithFire.mp3" 
	  if WithFire = 0 then e$ = GETERROR$() : ?e$ : end
    !call LoopSound(playWithFire, WithFire)
    gosub StartMusic
return

!-------------------------------------------------
! Show the order of the bag numbers
!-------------------------------------------------
ShowNumbers:
    n$ = ""
    for x = 1 to 10
        n$ = n$ + int$(numbers[x])
    next
    
    gr.modify numIner, "text",n$
    gr.modify numOuter, "text",n$
    gr.render
return

!-------------------------------------------------
! Solve the puzzle
!-------------------------------------------------
SolvePuzzel:
    d = di_width / 10
    do
        do
            flag = 0
            do 
                gr.touch t, tx, ty
                if t then 
                    do : gr.touch t, tx, ty : until ! t : flag = 1
                    tx /= scale_x : ty /= scale_y
                    tx /= d : tx += 1
                   if ty >350 | ty < 160 then flag = 0
                   if ! flag then gosub ButtonTest
                endif
                
            until flag & tx < 10
            if buzz then vibrate buzzGame[], -1
            if sound then soundpool.play stream, cnote, 0.9, 0.9,0,0,freq[tx]/261
            swap numbers[tx], numbers[10]
            score ++
            gr.modify trys, "text", "Trys:" + int$(score)
            gosub ShowNumbers
        until NumberSorted(numbers[])
        result$ = ""
        if score < lowest | lowest = 0 then 
            result$ = "A New low Score"
            lowest = score
            gr.modify low, "text", "Lowest " + int$(lowest) : gr.render
        endif
        do
            dialog.message "Puzzle Solved",result$ + "\nAnother go?",yn,"Yes","No"
        until yn > 0
        if yn = 1 then 
            array.shuffle numbers[]
            score = 0 : gr.modify trys, "text", "Trys: " + int$(score)
            gosub ShowNumbers
        endif
    until yn = 2
return

!-------------------------------------------------
! Look to see if a button as been tapped
!-------------------------------------------------
ButtonTest:
  gr.touch t, tx, ty 
  tx /= scale_x : ty /= scale_y
   gr.modify collision, "x", tx, "y", ty : gr.render
    if gr_collision(collision,ptrSoundBut) then gosub ButSound
    if gr_collision(collision,ptrBuzzBut) then gosub butBuzz
    if gr_collision(collision,ptrLowBut) then gosub ButLowest
    if gr_collision(collision,ptrExitBut) then gosub ButExit
    if gr_collision(collision,ptrBackgroundBut) then gosub ButBackgroundSound
return

!-------------------------------------------------
! Turn sound on and off
!-------------------------------------------------
ButSound:
  if buzz then vibrate buzzGame[], -1
  if sound then soundpool.play stream, cnote, 0.9, 0.9,0,0,freq[1]/261
  
  if sound then
    gr.modify prtSoundTxt, "text", "Sound ON" : sound = 0
  else 
    gr.modify prtSoundTxt, "text", "Sound OFF" : sound = 1
  endif
  gr.render
return

!-------------------------------------------------
! Turn background sound on and off
!-------------------------------------------------
ButBackgroundSound:
  if buzz then vibrate buzzGame[],  -1
  if playWithFire then
    audio.stop 
    gr.modify prtBackgroundTxt, "text", "Music ON" : playWithFire = 0
  else
    audio.play WithFire 
    audio.loop 
    gr.modify prtBackgroundTxt, "text", "Music OFF" : playWithFire = 1
  endif
  gr.render
return

!-------------------------------------------------
! Start background music if playWithFire = 1
!-------------------------------------------------
StartMusic:
if playWithFire then
    audio.play WithFire 
    audio.loop 
  endif 
return

!-------------------------------------------------
! Turn buzz on and off
!-------------------------------------------------
Butbuzz:
  if buzz then vibrate buzzGame[], -1
  if sound then soundpool.play stream, cnote, 0.9, 0.9,0,0,freq[1]/261
  
  if buzz then
    gr.modify prtBuzzTxt, "text", "Buzz ON" : buzz = 0
  else 
    gr.modify prtBuzzTxt, "text", "Buzz OFF" : buzz = 1
  endif
  gr.render
return

!-------------------------------------------------
! Reset the lowest score to zero
!-------------------------------------------------
ButLowest:
  if buzz then vibrate buzzGame[], -1
  if sound then soundpool.play stream, cnote, 0.9, 0.9,0,0,freq[1]/261
  
  do : dialog.message "Reset Lowest to Zero?",,yn,"Yes","No" : until yn > 0
  if yn = 1 then lowest = 0 : gr.modify low, "text", "Lowest " + int$(lowest)
  gr.render
return

!-------------------------------------------------
! Exit the puzzle 
!-------------------------------------------------
butExit:
  if buzz then vibrate buzzGame[], -1
  if sound then soundpool.play stream, cnote, 0.9, 0.9,0,0,freq[1]/261
  
  do : dialog.message "Quit Puzzle?",,yn,"Yes","No" : until yn > 0
  if yn = 1 then gosub SaveData : exit
return

!-------------------------------------------------
!Save sound, buzz and lowest score. At end of game
!-------------------------------------------------
SaveData:
	Path$="../../RearrangeNumber/data/"
	file.exists pathPresent,Path$+"GameData.txt"
	if ! pathPresent then file.mkdir Path$
  
		text.open w,hs,Path$+"GameData.txt"
    text.writeln hs,int$(lowest)
		text.writeln hs,int$(sound)
    text.writeln hs,int$(buzz)
    text.writeln hs,int$(playWithFire)
		text.close hs
return

!-------------------------------------------------
! Load sound, buzz and lowest score . At start of game
!-------------------------------------------------
LoadData:
	Path$="../../RearrangeNumber/data/"
	file.exists pathPresent,Path$+"GameData.txt"
	if pathPresent then
    text.open r,hs,Path$+"GameData.txt"
    text.readln hs,lowest$
    text.readln hs,sound$
    text.readln hs,buzz$
    text.readln hs,playWithFire$
    text.close hs
    lowest = val(lowest$)
    sound = val(sound$)
    buzz = val(buzz$)
    playWithFire = val(playWithFire$)
	endif
return

!-------------------------------------------------
! Start of functions
!-------------------------------------------------
Functions:

!-------------------------------------------------
! Test to see if the puzzle is Solved
!-------------------------------------------------
fn.def NumberSorted(numbers[])
    flag = 1
    for test = 0 to 9
        if numbers[test + 1] <> test then flag = 0
    next
    fn.rtn flag
fn.end

!-------------------------------------------------
! Some Colours
!-------------------------------------------------
fn.def Gcol(c, alpha, style)
	if c=0 then gr.color alpha,20,20,20,style % black (not quite)
	if c=1 then gr.color alpha,255,0,0,style % Red
	if c=2 then gr.color alpha,0,255,0,style % green
	if c=3 then gr.color alpha,0,0,255,style % blue
	if c=4 then gr.color alpha,255,255,0,style % yellow
	if c=5 then gr.color alpha,0,255,255,style % cyan
	if c=6 then gr.color alpha,255,0,255,style % magenta
	if c=7 then gr.color alpha,192,192,192,style % gray
  !if c=8 then gr.color alpha,255,128,0,style % brown 
  if c=8 then gr.color alpha,255,50,255,style % For the buttons 
  if c=9 then gr.color alpha,255,255,255,style % white
	if c=10 then gr.color alpha,218, 143, 255,style % Light magenta
  if c=11 then gr.color alpha, 128, 128, 128, style % Gray 
fn.end
return