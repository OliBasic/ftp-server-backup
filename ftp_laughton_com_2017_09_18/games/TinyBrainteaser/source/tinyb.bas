!! Tiny Brainteaser
See DrawInfoBox for game rules.
----
Data files are expected in {data/}tinyb/[img/|snd/] unless running as APK.
In the latter case the project folder TinyBrainteaser is checked and files are found in root.
Change BASE_DIR$, IMG_DIR$ and SND_DIR$ to match your setup/preferences.
----
The following names are used in more than one subroutine.
----
BackKey
BASE_DIR$, IMG_DIR$, SND_DIR$
BestScore, Drops, Hints, RightHits, WrongHits
Bitmaps
BoardTap, BoardTouch
ButtonClick, ButtonDown
Buttons[]
BTN_DOWN, BTN_UP
Confirm, ConfirmBox, ConfirmText, Prompt$
InfoBox
IsDisplayingMessage, Message$, MessageBox, MessageText
IsPlaying
Pattern, BinPattern$, PatternMasks[]
SBOX_LEFT, SBOX_TOP, SBOX_WIDTH, SBOX_HEIGHT
ScaleHeight, ScaleWidth
Scores[]
Solutions$[]
Sounds[], SoundVolume, Volumes[]
SND_APPLAUSE, SND_BUTTON_DOWN, SND_BUTTON_UP, SND_CLICK, SND_DELUSION, SND_PLACE, SND_REMOVE, SND_VOLUME
Stones[]
VirtualHeight, VirtualWidth
VirtualX, VirtualY
----
!!

FILE.EXISTS IS_APK, "../../TinyBrainteaser/"
IF IS_APK THEN
 BASE_DIR$ = ""
 IMG_DIR$ = ""
 SND_DIR$ = ""
ELSE
 BASE_DIR$ = "tinyb/"
 IMG_DIR$ = BASE_DIR$ + "img/"
 SND_DIR$ = BASE_DIR$ + "snd/"
ENDIF

List.create N, Bitmaps

GOSUB SetupScreen
GOSUB DrawStones
GOSUB DrawButtons
GOSUB DrawScores
GOSUB DrawVolumeControls
GOSUB DrawInfoBox
GOSUB DrawConfirmBox
GOSUB DrawMessageBox
GOSUB LoadSounds
GOSUB LoadSolutions

ARRAY.LOAD PatternMasks[], 27, 7, 54, 73, 186, 292, 216, 448, 432
! 000011011, 000000111, 000110110, 001001001, 010111010, 100100100, 011011000, 111000000, 110110000

GOSUB StartNewGame

DO
 GOSUB GetTouchOrKey
 IF BackKey THEN
  ButtonClick = 4 % quit
 ELSE
  GOSUB CheckButtonDown
  IF !ButtonDown & IsPlaying THEN GOSUB CheckBoardTouch
  GOSUB GetReleasePosition
  IF ButtonDown THEN
   GOSUB CheckButtonClick
  ELSEIF BoardTouch THEN
   GOSUB CheckBoardTap
  ENDIF
 ENDIF
 IF ButtonClick = 1 THEN
  GOSUB GiveHint
 ELSEIF ButtonClick = 2 THEN
  GOSUB ShowInfoDialog
 ELSEIF ButtonClick = 3 THEN
  IF IsPlaying THEN
   Prompt$ = "Start new game?"
   GOSUB ConfirmDialog
  ELSE
   Confirm = 1
  ENDIF
  IF Confirm THEN GOSUB StartNewGame
 ELSEIF ButtonClick = 4 THEN
  Prompt$ = "Quit the game?"
  GOSUB ConfirmDialog
  IF Confirm THEN D_U.BREAK
 ELSEIF BoardTap THEN
  GOSUB UpdateScores
  Pattern = BXOR(Pattern, PatternMasks[BoardTap])
  GOSUB UpdateBoard
  IF Pattern = 0 THEN
   GOSUB DisplayGameOver
  ELSEIF Pattern = 495 THEN
   GOSUB DisplayScoreDone
  ENDIF
 ENDIF
UNTIL 0

! CleanUp
SOUNDPOOL.RELEASE

LIST.TOARRAY Bitmaps, tmp[]
ARRAY.LENGTH bmps, tmp[]
FOR i = 1 TO bmps
 GR.BITMAP.DELETE tmp[i]
NEXT

GOSUB SaveBestScore

EXIT

LoadBestScore:
FILE.EXISTS found, BASE_DIR$ + "score.txt"
IF !found THEN
 TEXT.OPEN w, tmp, BASE_DIR$ + "score.txt"
 TEXT.WRITELN tmp, "0"
 TEXT.CLOSE tmp
ENDIF
TEXT.OPEN r, tmp, BASE_DIR$ + "score.txt"
IF tmp = -1 THEN
 BestScore = 0
ELSE
 TEXT.READLN tmp, b$
 TEXT.CLOSE tmp
 BestScore = VAL(b$)
ENDIF
RETURN

SaveBestScore:
TEXT.OPEN w, tmp, BASE_DIR$ + "score.txt"
b$ = REPLACE$(FORMAT$("########%", BestScore), " ", "")
TEXT.WRITELN tmp, b$
TEXT.CLOSE tmp
RETURN

SetupScreen:
GR.OPEN 255, 0, 0, 0, 1, 0
PAUSE 1000
VirtualWidth = 800
VirtualHeight = 480
GR.SCREEN aw, ah
ScaleWidth = aw / VirtualWidth
ScaleHeight = ah / VirtualHeight
GR.SCALE ScaleWidth, ScaleHeight
GR.BITMAP.LOAD bmp, IMG_DIR$ + "gameboard.jpg"
List.add Bitmaps, bmp
GR.BITMAP.DRAW tmp, bmp, 0, 0
GR.TEXT.TYPEFACE 4
GR.TEXT.ALIGN 2
SBOX_LEFT = 250
SBOX_TOP = 160
SBOX_WIDTH = 550
SBOX_HEIGHT = 320
RETURN

DrawStones:
GR.BITMAP.LOAD tmp, IMG_DIR$ + "stone.png"
GR.BITMAP.SCALE bmp, tmp, 90, 90
GR.BITMAP.DELETE tmp
List.add Bitmaps, bmp
DIM Stones[9]
! Checkered board at 90, 90, squares of 100.
FOR row = 0 TO 2
 y = 95 + 100 * row
 FOR col = 0 TO 2
  x = 95 + 100 * col
  GR.BITMAP.DRAW Stones[row * 3 + col + 1], bmp, x, y
 NEXT
NEXT
RETURN

DrawButtons:
BTN_DOWN = 1
BTN_UP = 2
GR.BITMAP.LOAD bmp_dn, IMG_DIR$ + "button_down.png"
List.add Bitmaps, bmp_dn
GR.BITMAP.LOAD bmp_up, IMG_DIR$ + "button_up.png"
List.add Bitmaps, bmp_up
DIM Buttons[2, 4]
GR.TEXT.SIZE 32
FOR i = 0 TO 3
 y = 45 + 100 * i
 n = i + 1
 GR.COLOR 128, 255, 255, 255, 1
 GR.BITMAP.DRAW Buttons[BTN_DOWN, n], bmp_dn, 500, y
 GR.BITMAP.DRAW Buttons[BTN_UP, n], bmp_up, 500, y
 GR.HIDE Buttons[BTN_DOWN, n]
 GR.COLOR 255, 255, 255, 255, 1
 GR.TEXT.DRAW tmp, 550, 105 + 100 * i, REPLACE$(MID$("HintInfoNew Quit", i * 4 + 1, 4), " ", "")
NEXT
RETURN

DrawScores:
DIM Scores[3]
GOSUB LoadBestScore
GR.TEXT.SIZE 48
GR.COLOR 255, 0, 255, 0, 1
GR.TEXT.DRAW Scores[1], 700, 110, "0"
GR.COLOR 255, 255, 0, 0, 1
GR.TEXT.DRAW Scores[2], 700, 210, "0"
GR.COLOR 255, 255, 255, 255, 1
GR.TEXT.DRAW Scores[3], 700, 410, REPLACE$(FORMAT$("########%", BestScore), " ", "")
RETURN

UpdateScores:
Drops += 1
tapped_on$ = REPLACE$(FORMAT$("#", BoardTap), " ", "")
IF IS_IN(tapped_on$, Solutions$[Pattern]) > 0 THEN
 IF Hints = 0 THEN
  RightHits += 1
  gosub UpdateRightHits
 ELSE
  Hints -= 1
  Hints = MAX(0, Hints)
 ENDIF
ELSE
 WrongHits += 1
 gosub UpdateWrongHits
ENDIF
RETURN

UpdateRightHits:
GR.MODIFY Scores[1], "text", REPLACE$(FORMAT$("########%", RightHits), " ", "")
RETURN

UpdateWrongHits:
GR.MODIFY Scores[2], "text", REPLACE$(FORMAT$("########%", WrongHits), " ", "")
RETURN

DrawVolumeControls:
SoundVolume = 0.5
DIM Volumes[4]
DIM bmp[4]
GR.COLOR 128, 255, 255, 255, 1
FOR i = 1 TO 4
 GR.BITMAP.LOAD tmp, IMG_DIR$ + "vol" + REPLACE$(FORMAT$("%", i - 1), " ", "") + ".png"
 GR.BITMAP.SCALE bmp[i], tmp, 50, 50
 GR.BITMAP.DELETE tmp
 List.add Bitmaps, bmp[i]
 GR.BITMAP.DRAW Volumes[i], bmp[i], 675, 270
 GR.HIDE Volumes[i]
NEXT
GR.SHOW Volumes[3]
UNDIM bmp[]
RETURN

UpdateVolumeControls:
SOUNDPOOL.PLAY tmp, Sounds[SND_VOLUME], SoundVolume, SoundVolume, 1, 0, 1
GR.HIDE Volumes[1]
GR.HIDE Volumes[2]
GR.HIDE Volumes[3]
GR.HIDE Volumes[4]
IF SoundVolume = 0 THEN
 GR.SHOW Volumes[1]
ELSEIF SoundVolume < 0.33 THEN
 GR.SHOW Volumes[2]
ELSEIF SoundVolume < 0.66 THEN
 GR.SHOW Volumes[3]
ELSE
 GR.SHOW Volumes[4]
ENDIF
GR.RENDER
RETURN

DrawInfoBox:
ARRAY.LOAD txt$[], "Tiny Brainteaser", "", ~
"The goal is to place a stone on each square of a 3x3 board except on the central one.", ~
"By tapping on the board you add or remove a stone to or from the touched square and", ~
"to or from adjacent squares.", "", ~
"In particular, tapping on", ~
"1. the central square you will change also the adjacent edges;", ~
"2. an edge you will change also the adjacent corners;", ~
"3. a corner you will change also the adjacent edges and the central square.", "", ~
"The game is over if a move gives a completely empty board."
ARRAY.LENGTH lines, txt$[]
larger = 72
factor = 1
DO
 ok = 1
 size = larger / factor
 GR.TEXT.SIZE size
 IF factor = 6 THEN D_U.BREAK
 factor += 1
 FOR i = 1 TO lines
  GR.TEXT.WIDTH w, txt$[i]
  IF w > VirtualWidth THEN
   ok = 0
   F_N.BREAK
  ENDIF
 NEXT
UNTIL ok
GR.BITMAP.CREATE bmp, VirtualWidth, VirtualHeight
GR.BITMAP.DRAWINTO.START bmp
GR.COLOR 255, 0, 0, 0, 1
GR.RECT tmp, 0, 0, VirtualWidth, VirtualHeight
GR.COLOR 255, 255, 255, 255, 1
space = (VirtualHeight - (lines * size * 0.5)) / lines
y = space * 2
FOR i = 1 TO lines
 GR.TEXT.DRAW tmp, 400, y, txt$[i]
 y += space
NEXT
GR.BITMAP.DRAWINTO.END
List.add Bitmaps, bmp
GR.COLOR 198, 0, 0, 0, 1
GR.BITMAP.DRAW InfoBox, bmp, 0, 0
GR.HIDE InfoBox
UNDIM txt$[]
RETURN

ShowInfoDialog:
GR.SHOW InfoBox
GR.RENDER
GOSUB GetTouchOrKey
IF !BackKey THEN GOSUB GetReleasePosition
GR.HIDE InfoBox
GR.RENDER
RETURN

DrawConfirmBox:
GR.COLOR 255, 255, 255, 255, 1
GR.BITMAP.CREATE bmp, SBOX_WIDTH, SBOX_HEIGHT
GR.BITMAP.DRAWINTO.START bmp
GR.RECT tmp, 0, 0, SBOX_WIDTH - SBOX_LEFT, SBOX_HEIGHT - SBOX_TOP
GR.BITMAP.LOAD bmp_ok, IMG_DIR$ + "ok_button.png"
GR.BITMAP.DRAW tmp, bmp_ok, 350 - SBOX_LEFT, 210 - SBOX_TOP
GR.BITMAP.DELETE bmp_ok
GR.BITMAP.DRAWINTO.END
List.add Bitmaps, bmp
GR.COLOR 198, 0, 0, 0, 1
GR.BITMAP.DRAW ConfirmBox, bmp, 250, 160 % 0, 0
GR.TEXT.DRAW ConfirmText, 400, 200, ""
GR.HIDE ConfirmBox
GR.HIDE ConfirmText
RETURN

ConfirmDialog:
Confirm = 0
GR.MODIFY ConfirmText, "text", Prompt$
GR.SHOW ConfirmBox
GR.SHOW ConfirmText
GR.RENDER
GOSUB GetTouchOrKey
IF !BackKey THEN
 IF VirtualX > 350 & VirtualX < 450 & VirtualY > 190 & VirtualY < 290 THEN
  Confirm = 1
 ENDIF
 GOSUB GetReleasePosition
 IF Confirm & VirtualX > 350 & VirtualX < 450 & VirtualY > 210 & VirtualY < 310 THEN
  SOUNDPOOL.PLAY tmp, Sounds[SND_CLICK], SoundVolume, SoundVolume, 1, 0, 1
  Confirm = 1
 ELSE
  Confirm = 0
 ENDIF
ENDIF
GR.HIDE ConfirmBox
GR.HIDE ConfirmText
GR.RENDER
RETURN

DrawMessageBox:
GR.COLOR 198, 255, 255, 255, 1
GR.RECT MessageBox, 250, 160, 550, 320
GR.TEXT.SIZE 24
GR.COLOR 200, 0, 0, 0, 1
GR.TEXT.DRAW MessageText, 400, 250, ""
GR.HIDE MessageBox
GR.HIDE MessageText
RETURN

OpenMessagePopup:
WHILE IsDisplayingMessage
 PAUSE 20
REPEAT
IsDisplayingMessage = 1
GR.SHOW MessageBox
GR.SHOW MessageText
GR.MODIFY MessageText, "text", Message$
GR.RENDER
TIMER.SET 1000
RETURN

CloseMessagePopup:
TIMER.CLEAR
GR.HIDE MessageText
GR.HIDE MessageBox
GR.RENDER
IsDisplayingMessage = 0
RETURN

LoadSounds:
SOUNDPOOL.OPEN 1
ARRAY.LOAD files$[], "applause.mp3", "button_cdown.mp3", "button_cup.mp3", "click.mp3", ~
"delusion.mp3", "place.mp3", "remove.mp3", "volume_control.mp3"
READ.DATA 1, 2, 3, 4, 5, 6, 7, 8
READ.NEXT SND_APPLAUSE, SND_BUTTON_DOWN, SND_BUTTON_UP, SND_CLICK, SND_DELUSION, SND_PLACE, SND_REMOVE, SND_VOLUME
ARRAY.LENGTH count, files$[]
DIM Sounds[count]
FOR i = 1 to count
 SOUNDPOOL.LOAD Sounds[i], SND_DIR$ + files$[i]
NEXT
UNDIM files$[]
RETURN

StartNewGame:
DO
 Pattern = FLOOR(511 * RND() + 1)
UNTIL Pattern <> 495
gosub UpdateBoard
Drops = -LEN(Solutions$[Pattern])
Hints = 0
RightHits = 0
gosub UpdateRightHits
WrongHits = 0
gosub UpdateWrongHits
IsPlaying = 1
GR.SHOW Buttons[BTN_UP, 1]
Message$ = "New game started!"
GOSUB OpenMessagePopup
RETURN

UpdateBoard:
BinPattern$ = RIGHT$("000000000" + BIN$(Pattern), 9)
FOR i = LEN(BinPattern$) TO 1 STEP -1
 IF MID$(BinPattern$, i, 1) = "1" THEN
  GR.SHOW Stones[10 - i]
 ELSE
  GR.HIDE Stones[10 - i]
 ENDIF
NEXT
RETURN

GetTouchOrKey:
BackKey = 0
ButtonDown = 0
ButtonClick = 0
BoardTouch = 0
BoardTap = 0
GR.RENDER
DO
 IF BACKGROUND() THEN
  PAUSE 1000
 ELSE
  INKEY$ k$
  IF k$ = "key 24" THEN
   SoundVolume += 0.1
   SoundVolume = min(SoundVolume, 0.99)
   GOSUB UpdateVolumeControls
  ELSEIF k$ = "key 25" THEN
   SoundVolume -= 0.1
   SoundVolume = max(SoundVolume, 0)
   GOSUB UpdateVolumeControls
  ELSE
   GR.TOUCH touched, x, y
  ENDIF
 ENDIF
UNTIL touched | BackKey
IF touched THEN
 VirtualX = x / ScaleWidth
 VirtualY = y / ScaleHeight
ENDIF
RETURN

GetReleasePosition:
 DO
  GR.TOUCH touched, x, y
 UNTIL !touched
 VirtualX = x / ScaleWidth
 VirtualY = y / ScaleHeight
RETURN

CheckButtonDown:
FOR m = 1 TO 4
 GR.GET.VALUE Buttons[BTN_UP, m], "x", left
 GR.GET.VALUE Buttons[BTN_UP, m], "y", top
 IF VirtualX > left & VirtualX < left + 100 & VirtualY > top & VirtualY < top + 100 THEN
  IF m > 1 | IsPlaying = 1 THEN
   GR.HIDE Buttons[BTN_UP, m]
   GR.SHOW Buttons[BTN_DOWN, m]
   SOUNDPOOL.PLAY tmp, Sounds[SND_BUTTON_DOWN], SoundVolume, SoundVolume, 1, 0, 1
   ButtonDown = m
   GR.RENDER
  ENDIF
  F_N.BREAK
 ENDIF
NEXT
RETURN

CheckButtonClick:
GR.HIDE Buttons[BTN_DOWN, ButtonDown]
GR.SHOW Buttons[BTN_UP, ButtonDown]
SOUNDPOOL.PLAY tmp, Sounds[SND_BUTTON_UP], SoundVolume, SoundVolume, 1, 0, 1
GR.RENDER
GR.GET.VALUE Buttons[BTN_UP, ButtonDown], "x", left
GR.GET.VALUE Buttons[BTN_UP, ButtonDown], "y", top
IF VirtualX > left & VirtualX < left + 100 & VirtualY > top & VirtualY < top + 100 THEN
 ButtonClick = ButtonDown
ENDIF
RETURN

CheckBoardTouch:
IF VirtualX > 90 & VirtualX < 390 & VirtualY > 90 & VirtualY < 390 THEN
 FOR row = 0 TO 2
  FOR col = 0 TO 2
   idx = row * 3 + col + 1
   GR.GET.VALUE Stones[idx], "x", left
   GR.GET.VALUE Stones[idx], "y", top
   IF VirtualX > left - 5 & VirtualX < left + 95 & VirtualY > top - 5 & VirtualY < top + 95 THEN
    IF MID$(BinPattern$, 10 - idx, 1) = "1" THEN
     SOUNDPOOL.PLAY tmp, Sounds[SND_REMOVE], SoundVolume, SoundVolume, 1, 0, 1
    ELSE
     SOUNDPOOL.PLAY tmp, Sounds[SND_PLACE], SoundVolume, SoundVolume, 1, 0, 1
    ENDIF
    BoardTouch = idx
    F_N.BREAK
   ENDIF
  NEXT
 NEXT
ENDIF
RETURN

CheckBoardTap:
GR.GET.VALUE Stones[BoardTouch], "x", left
GR.GET.VALUE Stones[BoardTouch], "y", top
IF VirtualX > left - 5 & VirtualX < left + 95 & VirtualY > top - 5 & VirtualY < top + 95 THEN
 BoardTap = BoardTouch
ENDIF
RETURN

GiveHint:
Hints += 1
idx = VAL(LEFT$(Solutions$[Pattern], 1))
IF MID$(BinPattern$, 10 - idx, 1) = "1" THEN
 ARRAY.LOAD seq[], 0, 1, 0, 1
ELSE
 ARRAY.LOAD seq[], 1, 0, 1, 0
ENDIF
FOR i = 1 TO 4
 IF seq[i] THEN
  GR.SHOW Stones[idx]
 ELSE
  GR.HIDE Stones[idx]
 ENDIF
 GR.RENDER
 PAUSE 20
NEXT
UNDIM seq[]
RETURN

DisplayGameOver:
SOUNDPOOL.PLAY tmp, Sounds[SND_DELUSION], SoundVolume, SoundVolume, 1, 0, 1
Message$ = "Game Over!"
GOSUB OpenMessagePopup
IsPlaying = 0
GR.HIDE Buttons[BTN_UP, 1]
RETURN

DisplayScoreDone:
SOUNDPOOL.PLAY tmp, Sounds[SND_APPLAUSE], SoundVolume, SoundVolume, 1, 0, 1
score = FLOOR(MAX(0, RightHits - WrongHits) * 10 / MAX(1, Drops))
IF score > BestScore THEN BestScore = score
GR.MODIFY Scores[3], "text", REPLACE$(FORMAT$("########%", BestScore), " ", "")
Message$ = "You made " + REPLACE$(FORMAT$("########%", score), " ", "") + " point"
IF FLOOR(score) <> 1 THEN
 Message$ = Message$ + "s"
ENDIF
 Message$ = Message$ + "."
GOSUB OpenMessagePopup
IsPlaying = 0
GR.HIDE Buttons[BTN_UP, 1]
RETURN

ONTIMER:
If IsDisplayingMessage THEN GOSUB CloseMessagePopup
TIMER.RESUME

ONBACKGROUND:
IF BACKGROUND() THEN GOSUB SaveBestScore
BACKGROUND.RESUME

ONBACKKEY:
BackKey = 1
BACK.RESUME

LoadSolutions:
ARRAY.LOAD Solutions$[], "5689", "138", "24578", "4578", "1238", "25689", "1346789", "167", "23456", "1249"~
"3579", "23579", "149", "3456", "1267", "13579", "249", "123456", "67", "267", "13456", "49", "123579"~
"124578", "38", "15689", "2346789", "346789", "125689", "238", "14578", "349", "12579", "2367", "1456"~
"12456", "367", "1579", "2349", "28", "134578", "46789", "1235689", "135689", "246789", "1234578", "8"~
"235689", "146789", "34578", "128", "18", "234578", "1246789", "35689", "456", "12367", "2579", "1349"~
"12349", "579", "1367", "2456", "2356", "1467", "34579", "129", "19", "234579", "12467", "356", "45689"~
"1236789", "2578", "1348", "12348", "578", "136789", "245689", "348", "12578", "236789", "145689", "1245689"~
"36789", "1578", "2348", "29", "134579", "467", "12356", "1356", "2467", "1234579", "9", "13578", "248"~
"12345689", "6789", "26789", "1345689", "48", "123578", "124579", "39", "156", "23467", "3467", "1256"~
"239", "14579", "123467", "56", "139", "24579", "4579", "1239", "256", "13467", "16789", "2345689", "1248"~
"3578", "23578", "148", "345689", "126789", "279", "13459", "46", "123567", "13567", "246", "123459", "79"~
"3478", "1258", "23689", "1456789", "12456789", "3689", "158", "23478", "456789", "123689", "258", "13478"~
"123478", "58", "13689", "2456789", "23567", "146", "3459", "1279", "179", "23459", "1246", "3567", "1689"~
"23456789", "12478", "358", "2358", "1478", "3456789", "12689", "12346", "567", "1379", "2459", "459"~
"12379", "2567", "1346", "12459", "379", "1567", "2346", "346", "12567", "2379", "1459", "1358", "2478"~
"123456789", "689", "2689", "13456789", "478", "12358", "12458", "378", "156789", "234689", "34689"~
"1256789", "2378", "1458", "1359", "2479", "1234567", "6", "26", "134567", "479", "12359", "16", "234567"~
"12479", "359", "2359", "1479", "34567", "126", "1234689", "56789", "1378", "2458", "458", "12378"~
"256789", "134689", "4567", "1236", "259", "13479", "123479", "59", "136", "24567", "2356789", "14689"~
"3458", "1278", "178", "23458", "124689", "356789", "278", "13458", "4689", "12356789", "1356789"~
"24689", "123458", "78", "3479", "1259", "236", "14567", "124567", "36", "159", "23479", "1245", "37"~
"15679", "23469", "3469", "125679", "237", "145", "13589", "24789", "12345678", "68", "268", "1345678"~
"4789", "123589", "168", "2345678", "124789", "3589", "23589", "14789", "345678", "1268", "123469"~
"5679", "137", "245", "45", "1237", "25679", "13469", "45678", "12368", "2589", "134789", "1234789"~
"589", "1368", "245678", "235679", "1469", "345", "127", "17", "2345", "12469", "35679", "27", "1345"~
"469", "1235679", "135679", "2469", "12345", "7", "34789", "12589", "2368", "145678", "1245678", "368"~
"1589", "234789", "2789", "134589", "468", "1235678", "135678", "2468", "1234589", "789", "347", "125"~
"2369", "145679", "1245679", "369", "15", "2347", "45679", "12369", "25", "1347", "12347", "5", "1369"~
"245679", "235678", "1468", "34589", "12789", "1789", "234589", "12468", "35678", "169", "2345679"~
"1247", "35", "235", "147", "345679", "1269", "123468", "5678", "13789", "24589", "4589", "123789"~
"25678", "13468", "124589", "3789", "15678", "23468", "3468", "125678", "23789", "14589", "135", "247"~
"12345679", "69", "269", "1345679", "47", "1235", "23568", "14678", "345789", "1289", "189", "2345789"~
"124678", "3568", "4569", "123679", "257", "134", "1234", "57", "13679", "24569", "34", "1257", "23679"~
"14569", "124569", "3679", "157", "234", "289", "1345789", "4678", "123568", "13568", "24678", "12345789"~
"89", "1357", "24", "1234569", "679", "2679", "134569", "4", "12357", "1245789", "389", "1568", "234678"~
"34678", "12568", "2389", "145789", "1234678", "568", "1389", "245789", "45789", "12389", "2568", "134678"~
"1679", "234569", "124", "357", "2357", "14", "34569", "12679", "1234679", "569", "13", "2457", "457"~
"123", "2569", "134679", "1678", "234568", "12489", "35789", "235789", "1489", "34568", "12678", "135789"~
"2489", "1234568", "678", "2678", "134568", "489", "1235789", "12457", "3", "1569", "234679", "34679"~
"12569", "23", "1457", "3489", "125789", "23678", "14568", "124568", "3678", "15789", "23489", "2"~
"13457", "4679", "123569", "13569", "24679", "123457", "", "23569", "14679", "3457", "12", "1", "23457"~
"124679", "3569", "4568", "123678", "25789", "13489", "123489", "5789", "13678", "24568"
RETURN
