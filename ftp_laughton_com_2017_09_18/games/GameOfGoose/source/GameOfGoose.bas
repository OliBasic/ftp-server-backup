! Goose+
! Aat @2017
FN.DEF Beep()
	TONE 659.25,200,0
	TONE 440,300,0
FN.END
GR.OPEN 255,0,0,0,0,0
GR.SCREEN w,h
ScaleX=1200
ScaleY=h/w*ScaleX
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
GR.CLS
DataPath$="../../GameOfGoose/data/"
DIM Dice[6],P[4],Pawn[4],CurPlace[4],PawnStatus[4],DisplN[4],gSt[6]
ARRAY.LOAD Place[],238,562,320,563,387,564,460,564,532,564,608,560,689,563,758,559,825,523,890,484,936,430,967,390,983,337,986,271,976,205,950,153,911,107,842,57,736,23,665,20,595,20,527,20,453,23,380,20,314,17,240,17,168,36,109,69,57,112,33,163,10,228,12,288,33,349,54,397,110,442,176,467,252,480,319,484,385,487,457,485,529,488,608,485,688,486,759,465,806,421,847,362,864,313,860,265,844,216,791,136,737,122,635,120,537,118,454,116,378,115,308,119,237,121,151,176,133,265,151,326,187,367,249,393,264,253
ARRAY.LOAD ChoosePl$[],"1","2","3","4"
GR.BITMAP.LOAD Bg,DataPath$+"Board.png"
GR.BITMAP.LOAD Goose,DataPath$+"Goose.png"
GR.BITMAP.LOAD WinGoose,DataPath$+"Goose01.png"
GR.BITMAP.LOAD LooseGoose,DataPath$+"Goose02.png"
FOR i=1 TO 4
	GR.BITMAP.LOAD P[i],DataPath$+"Goose"+INT$(i)+".png"
NEXT i
GR.BITMAP.LOAD Pic,DataPath$+"Dice.png"
LIST.CREATE S,NS:LIST.CREATE N,SC
Items=43:DIM Taal$[Items,4]
SndVol=0.99:Lang=1
GR.BITMAP.CREATE Flag1,100,60
GR.BITMAP.DRAWINTO.START Flag1
	GR.COLOR 255,170,0,0,1:GR.RECT g,0,0,100,20
	GR.COLOR 255,255,255,255,1:GR.RECT g,0,20,100,40
	GR.COLOR 255,0,0,160,1:GR.RECT g,0,40,100,60
GR.BITMAP.DRAWINTO.END
GR.BITMAP.CREATE Flag2,100,60
GR.BITMAP.DRAWINTO.START Flag2
	GR.COLOR 255,0,0,110,1:GR.RECT g,0,0,100,60
	GR.SET.STROKE 10:GR.COLOR 255,255,255,255,1:GR.LINE g,0,0,100,60:GR.LINE g,0,60,100,0
	GR.SET.STROKE 4:GR.COLOR 255,220,0,0,1:GR.LINE g,0,1,40,25:GR.LINE g,98,0,60,24:GR.LINE g,2,60,40,37:GR.LINE g,100,59,60,36
	GR.SET.STROKE 15:GR.COLOR 255,255,255,255,1:GR.LINE g,0,30,100,30:GR.LINE g,50,0,50,60
	GR.SET.STROKE 8:GR.COLOR 255,220,0,0,1:GR.LINE g,0,30,100,30:GR.LINE g,50,0,50,60
GR.BITMAP.DRAWINTO.END
GR.BITMAP.CREATE Flag3,100,60
GR.BITMAP.DRAWINTO.START Flag3
	GR.COLOR 255,0,0,230,1:GR.RECT g,0,0,33,60
	GR.COLOR 255,255,255,255,1:GR.RECT g,33,0,66,60
	GR.COLOR 255,255,50,50,1:GR.RECT g,66,0,100,60
GR.BITMAP.DRAWINTO.END
GR.BITMAP.CREATE Flag4,100,60
GR.BITMAP.DRAWINTO.START Flag4
	GR.COLOR 255,0,0,0,1:GR.RECT g,0,0,100,20
	GR.COLOR 255,255,0,0,1:GR.RECT g,0,20,100,40
	GR.COLOR 255,200,200,0,1:GR.RECT g,0,40,100,60
GR.BITMAP.DRAWINTO.END
FOR i=1 TO 4
	FOR j=1 TO 3
		READ.NEXT A$
		Taal$[j,i]=A$
	NEXT j
NEXT i
GR.COLOR 255,64,64,64,1:GR.RECT g,0,0,1200,150
GR.COLOR 255,255,255,0,0:GR.SET.STROKE 0:GR.TEXT.SIZE 25
GR.TEXT.DRAW g,310,25,"Nederlands":GR.TEXT.DRAW g,480,25,"English":GR.TEXT.DRAW g,630,25,"Français":GR.TEXT.DRAW g,780,25,"Deutsch"
GR.SET.STROKE 5:GR.RECT L,320,45,430,115:GR.SET.STROKE 0
GR.TEXT.ALIGN 2:GR.TEXT.DRAW tSnd,600,190,Taal$[1,1]:GR.TEXT.DRAW SetSndOn,600,275,Taal$[2,1]
GR.TEXT.DRAW SetSndOff,600,275,Taal$[3,1]:GR.HIDE SetSndOff:GR.TEXT.ALIGN 1
GR.BITMAP.DRAW g,Flag1,325,50:GR.BITMAP.DRAW g,Flag2,475,50:GR.BITMAP.DRAW g,Flag3,625,50:GR.BITMAP.DRAW g,Flag4,775,50
GR.COLOR 255,255,255,255,0:GR.RECT g,550,200,650,250:GR.RECT g,555,205,645,245
GR.COLOR 255,192,192,192,1:GR.RECT g,560,210,640,240
GR.COLOR 255,0,255,0,1:GR.RECT Green,600,207,643,243:GR.COLOR 255,255,0,0,1:GR.RECT Red,557,207,600,243:GR.HIDE Red
GR.COLOR 255,0,0,0,0:GR.RECT g,560,210,640,240
GR.BITMAP.DRAW g,Goose,450,300:GR.COLOR 255,215,215,215,1:GR.OVAL g,380,300,480,355:GR.OVAL g,480,340,510,350:GR.OVAL g,520,355,540,365
GR.COLOR 255,0,0,0,0:GR.TEXT.SIZE 50:GR.TEXT.DRAW g,395,345,"OK":GR.RENDER
DO
	GOSUB GetTouch
	IF y>50 & y<110 THEN
		IF x>325 & x<425 THEN
			GR.MODIFY L,"left",320,"right",430
			Lang=1
		ENDIF
		IF x>475 & x<575 THEN
			GR.MODIFY L,"left",470,"right",580
			Lang=2
		ENDIF
		IF x>625 & x<725 THEN
			GR.MODIFY L,"left",620,"right",730
			Lang=3
		ENDIF
		IF x>775 & x<875 THEN
			GR.MODIFY L,"left",770,"right",880
			Lang=4
		ENDIF
		GR.MODIFY tSnd,"text",Taal$[1,Lang]:GR.MODIFY SetSndOn,"text",Taal$[2,Lang]:GR.MODIFY SetSndOff,"text",Taal$[3,Lang]
	ENDIF
	IF x>550 & y>200 & x<650 & y<250 THEN
		IF SndVol=0.99 THEN
			GR.HIDE Green:GR.SHOW Red:GR.HIDE SetSndOn:GR.SHOW SetSndOff
			SndVol=0
		ELSE
			GR.SHOW Green:GR.HIDE Red:GR.SHOW SetSndOn:GR.HIDE SetSndOff
			SndVol=0.99
		ENDIF
	ENDIF
	GR.RENDER
UNTIL x>380 & y>300 & x<480 & y<355
GR.CLS
READ.FROM 13+(Lang-1)*(Items-3)
FOR i=4 TO Items
	READ.NEXT A$
	Taal$[i,Lang]=A$
NEXT i
FILE.EXISTS FE,DataPath$+"Score.txt"
IF !FE THEN
	TEXT.OPEN W,FS,DataPath$+"Score.txt"
		TEXT.WRITELN FS,"EOF"
	TEXT.CLOSE FS
ENDIF
TEXT.OPEN R,FS,DataPath$+"Score.txt"
	DO
		TEXT.READLN FS,Name$
		IF IS_IN(",",Name$) THEN
			LIST.ADD NS,WORD$(Name$,1,",")
			LIST.ADD SC,VAL(WORD$(Name$,2,","))
		ELSE
			LIST.ADD NS,Name$
		ENDIF
	UNTIL Name$="EOF"
TEXT.CLOSE FS
LIST.SIZE NS,NumNames
LIST.REPLACE NS,NumNames,Taal$[4,Lang]
q=0:FOR i=0 TO 150 STEP 150
	FOR j=0 TO 300 STEP 150
		q=q+1:GR.BITMAP.CROP Dice[q],Pic,j,i,150,150
	NEXT j
NEXT i
GR.BITMAP.DELETE Pic
GR.BITMAP.CREATE Rules,620,670
GR.BITMAP.DRAWINTO.START Rules
	GR.COLOR 255,0,0,255,1:GR.RECT g,0,0,620,670
	GR.COLOR 255,255,255,0,0:GR.RECT g,10,10,610,660
	GR.TEXT.SIZE 20:GR.COLOR 255,255,255,255,1
	GR.TEXT.UNDERLINE 1:GR.TEXT.DRAW g,100,40,Taal$[7,Lang]:GR.TEXT.UNDERLINE 0
	GR.TEXT.DRAW g,250,40,Taal$[8,Lang]
	TEXT.OPEN R,GameRules,DataPath$+"Rules.txt"
	NumOfLines=30
	TEXT.POSITION.SET GameRules,(Lang-1)*NumOfLines+1
	FOR i=1 TO NumOfLines
		TEXT.READLN GameRules,A$
		GR.TEXT.DRAW g,20,50+i*20,A$
	NEXT i
	TEXT.CLOSE GameRules
GR.BITMAP.DRAWINTO.END
GR.COLOR 255,255,255,0,1:GR.TEXT.SIZE 80:GR.TEXT.DRAW g,400,65,Taal$[9,Lang]
GR.COLOR 255,255,255,255,1:GR.TEXT.SIZE 40:GR.TEXT.DRAW g,100,50,Taal$[10,Lang]
GR.COLOR 255,255,255,0,1:FOR i=0 TO 2:GR.RECT g,900,i*100,1000,i*100+100:NEXT i
GR.COLOR 255,170,0,0,1:GR.RECT g,900,3*100,1000,3*100+100
GR.COLOR 255,255,255,0,1:FOR i=4 TO 5:GR.RECT g,900,i*100,1000,i*100+100:NEXT i
GR.COLOR 255,170,0,0,1:GR.RECT g,900,6*100,1000,6*100+100
GR.SET.STROKE 10:GR.COLOR 255,255,255,255,0
FOR i=0 TO 6:GR.RECT g,900,i*100,1000,i*100+100:NEXT i
GR.SET.STROKE 0:GR.BITMAP.DRAW g,Goose,50,300
GR.COLOR 255,0,255,0,1:GR.ARC Anim1,250,300,350,400,0,360,1:GR.COLOR 255,0,255,0,0:GR.CIRCLE g,300,350,50
GR.COLOR 255,255,0,0,1:GR.ARC Anim2,400,500,500,600,0,360,1:GR.COLOR 255,255,0,0,0:GR.CIRCLE g,450,550,50
GR.COLOR 255,0,0,255,1:GR.ARC Anim3,700,400,800,500,0,360,1:GR.COLOR 255,0,0,255,0:GR.CIRCLE g,750,450,50
GR.COLOR 255,255,255,255,1
GR.RENDER
SOUNDPOOL.OPEN 1
SOUNDPOOL.LOAD Snd0,DataPath$+"Dice.mp3"
SOUNDPOOL.LOAD Snd1,DataPath$+"Goose1.mp3"
SOUNDPOOL.LOAD Snd2,DataPath$+"Goose2.mp3"
SOUNDPOOL.LOAD Snd3,DataPath$+"Goose3.mp3"
! GET NumOfPlayers, Names, Hiscores
DO
	DIALOG.SELECT NumOfPlayers,ChoosePl$[],Taal$[11,Lang]
UNTIL NumOfPlayers>0
DIM PlayName$[NumOfPlayers],Score[NumOfPlayers]
FOR i=1 TO NumOfPlayers
	DO
		DO
			DIALOG.SELECT Pick,NS,Taal$[12,Lang]
		UNTIL Pick>0
		LIST.GET NS,Pick,N$
		! Do not allow same name
		Dubble=0
		FOR j=1 TO NumOfPlayers
			IF PlayName$[j]=N$ THEN Dubble=1
		NEXT j
	UNTIL !Dubble
	IF Pick=NumNames THEN
		DO
			INPUT Taal$[13,Lang],N$,"",IsCancel
		UNTIL !IsCancel & N$<>""
		! Make name unique
		FOR j=1 TO NumNames
			LIST.GET NS,j,N1$
			IF N$=N1$ THEN N$=N$+"*"
		NEXT j
		LIST.INSERT NS,1,N$
		N=-1
		LIST.INSERT SC,1,N
		NumNames=NumNames+1
		! Save new file
		TEXT.OPEN W,FS,DataPath$+"Score.txt"
			FOR j=1 TO NumNames-1
				LIST.GET NS,j,N1$
				LIST.GET SC,j,N1
				TEXT.WRITELN FS,N1$+","+INT$(N1)
			NEXT j
		TEXT.CLOSE FS
	ELSE
		LIST.GET NS,Pick,N$
		LIST.GET SC,Pick,N
	ENDIF
	PlayName$[i]=N$
	Score[i]=N
	GR.BITMAP.DRAW g,P[i],10,(i+1)*50-40
	GR.TEXT.DRAW DisplN[i],80,(i+1)*50,Taal$[14,Lang]+" "+INT$(i)+" = "+PlayName$[i]+" (Score = "+INT$(Score[i])+")"
	GR.RENDER
NEXT i
IF NumOfPlayers>1 THEN
	DO
		DIALOG.MESSAGE Taal$[15,Lang],"",YN,Taal$[2,Lang],Taal$[3,Lang]
	UNTIL YN>0
	IF YN=1 THEN
		ARRAY.SHUFFLE PlayName$[]
		FOR i=1 TO NumOfPlayers
			FOR j=1 TO NumNames-1
				! Get correct score
				LIST.GET NS,j,N$
				LIST.GET SC,j,N
				IF PlayName$[i]=N$ THEN Score[i]=N
			NEXT j
		NEXT i
		SOUNDPOOL.PLAY SndStr01,Snd0,SndVol,SndVol,1,0,1
		FOR i=360 TO 0 STEP -20
			GR.MODIFY Anim1,"sweep_angle",i:GR.MODIFY Anim2,"sweep_angle",i:GR.MODIFY Anim3,"sweep_angle",i
			PAUSE 50:GR.RENDER
		NEXT i
		FOR i=0 TO 360 STEP 20
			GR.MODIFY Anim1,"sweep_angle",i:GR.MODIFY Anim2,"sweep_angle",i:GR.MODIFY Anim3,"sweep_angle",i
			PAUSE 50:GR.RENDER
		NEXT i
		FOR i=1 TO NumOfPlayers
			IF Score[i]>-1 THEN
				X$=INT$(Score[i])
			ELSE
				X$="XXX"
			ENDIF
			GR.MODIFY DisplN[i],"text",Taal$[14,Lang]+" "+INT$(i)+" = "+PlayName$[i]+" (Score = "+X$+")"
		NEXT i
	ENDIF
	GR.RENDER
ENDIF
DIALOG.MESSAGE Taal$[16,Lang],,QQ,"OK"
GR.CLS
GR.BITMAP.DRAW g,Bg,0,0
GR.COLOR 255,0,64,0,1:GR.OVAL g,375,220,825,430
GR.SET.STROKE 10:GR.COLOR 255,255,0,0,0:GR.OVAL g,375,220,825,430
GR.COLOR 255,60,60,192,1:GR.CIRCLE B1,600,325,90
GR.COLOR 255,0,0,0,0:GR.CIRCLE B2,600,325,90
GR.COLOR 255,0,0,0,1:GR.CIRCLE B3,600,325,30:GR.GROUP Button,B1,B2,B3
GR.BITMAP.DRAW D1,Dice[1],440,250:GR.HIDE D1
GR.BITMAP.DRAW D2,Dice[1],610,250:GR.HIDE D2
FOR i=1 TO NumOfPlayers
	CurPlace[i]=0
	PawnStatus[i]=1
NEXT i
NoTrhow=0:Turn=1:DRW=0:Player=0
FOR i=1 TO NumOfPlayers
	GR.BITMAP.DRAW Pawn[i],P[i],10+i*40,540+i*10
NEXT i
GR.TEXT.SIZE 120
GR.COLOR 255,255,255,0,1:GR.TEXT.DRAW nSc,1000,110,INT$(Thrown):GR.HIDE nSc
! Status screen
GR.TEXT.SIZE 20
GR.SET.STROKE 0:LIST.CREATE N,StList
GR.COLOR 255,0,0,255,1:GR.RECT g,250,0,870,670:LIST.ADD StList,g
GR.COLOR 255,255,255,0,0:GR.RECT g,260,10,860,660:LIST.ADD StList,g
GR.TEXT.SIZE 20:GR.COLOR 255,255,255,255,1
GR.TEXT.UNDERLINE 1:GR.TEXT.DRAW g,350,40,"Status":GR.TEXT.UNDERLINE 0:LIST.ADD StList,g
GR.TEXT.DRAW g,500,40,Taal$[8,Lang]:LIST.ADD StList,g
GR.BITMAP.DRAW g,Goose,580,300:LIST.ADD StList,g
GR.TEXT.DRAW g,280,80,Taal$[17,Lang]+" = "+INT$(NumOfPlayers):LIST.ADD StList,g
FOR i=1 TO NumOfPlayers
	IF Score[i]>-1 THEN
		PrtScore$=INT$(Score[i])
	ELSE
		PrtScore$="XXX"
	ENDIF
	GR.TEXT.DRAW g,370,90+i*20,Taal$[14,Lang]+" "+INT$(i)+" = "+PlayName$[i]+"    (HI-SCORE "+PrtScore$+")":LIST.ADD StList,g
NEXT i
FOR i=1 TO NumOfPlayers
	GR.TEXT.DRAW gSt[i],280,190+i*20,"":LIST.ADD StList,gSt[i]
NEXT i
GR.TEXT.DRAW gSt[5],700,80,"":LIST.ADD StList,gSt[5]
GR.TEXT.DRAW gSt[6],280,310,"":LIST.ADD StList,gSt[6]
GR.GROUP.LIST ShowStats,StList
GR.BITMAP.DRAW ShowRules,Rules,250,0:GR.HIDE ShowRules:GR.HIDE ShowStats
GR.TEXT.SIZE 30:GR.TEXT.DRAW cPl,930,640,""
GR.RENDER
SOUNDPOOL.PLAY SndStr01,Snd1,SndVol,SndVol,1,0,1
DO
	GR.HIDE nSc:GR.HIDE cPl:GR.RENDER
	IF NoTrhow=0 THEN
		GR.SHOW Button:GR.HIDE D1:GR.HIDE D2:GR.RENDER
		IF Player=NumOfPlayers THEN
			Player=0
			Turn=Turn+1
		ENDIF
		Player=Player+1
		IF PawnStatus[Player]=2 THEN
			DIALOG.MESSAGE PlayName$[Player]+" "+Taal$[18,Lang],,QQ,"OK"
			PawnStatus[Player]=0
			IF Player=NumOfPlayers THEN
				Player=1
			ELSE
				Player=Player+1
			ENDIF
		ENDIF
		DO
			IF PawnStatus[Player]>2 THEN
				DIALOG.MESSAGE PlayName$[Player]+" "+Taal$[19,Lang],,QQ,"OK"
				IF Player=NumOfPlayers THEN
					Player=1
				ELSE
					Player=Player+1
				ENDIF
			ENDIF
		UNTIL PawnStatus[Player]<3
		GR.MODIFY cPl,"text",PlayName$[Player]:GR.SHOW cPl:GR.RENDER
		DO
			DIALOG.MESSAGE PlayName$[Player]+" "+Taal$[20,Lang],Taal$[27,Lang]+" "+INT$(Turn),QQ,"OK",Taal$[7,Lang],"Status"
			IF QQ=2 THEN GR.SHOW ShowRules:GR.RENDER:GOSUB GetTouch:GR.HIDE ShowRules:GR.RENDER
			IF QQ=3 THEN
				! Update status
				FOR i=1 to NumOfPlayers
					IF PawnStatus[i]=0 THEN X$=Taal$[21,Lang]+" "+INT$(CurPlace[i])
					IF PawnStatus[i]=1 THEN X$=Taal$[22,Lang]
					IF PawnStatus[i]=2 THEN X$=Taal$[23,Lang]
					IF PawnStatus[i]=3 THEN X$=Taal$[24,Lang]
					IF PawnStatus[i]=4 THEN X$=Taal$[25,Lang]
					IF i=Player THEN X$=X$+" "+Taal$[26,Lang]
					GR.MODIFY gSt[i],"text",PlayName$[i]+" "+X$
				NEXT i
				GR.MODIFY gSt[5],"text",Taal$[27,Lang]+" "+INT$(Turn)
				Hi=0:FOR i=1 to NumOfPlayers
					IF CurPlace[i]>Hi THEN Hi=CurPlace[i]:L1=i
				NEXT i
				IF L1>0 THEN
					GR.MODIFY gSt[6],"text",PlayName$[L1]+" "+Taal$[28,Lang]
				ELSE
					GR.MODIFY gSt[6],"text",""
				ENDIF
				GR.SHOW ShowStats:GR.RENDER:GOSUB GetTouch:GR.HIDE ShowStats:GR.RENDER
			ENDIF
		UNTIL QQ=1
		GOSUB ThrowDice
	ELSE
		NoTrhow=0
	ENDIF
	GR.RENDER:Temp1=0
	! Remember previous place
	PrevPlace=CurPlace[Player]
	! Move pawn
	IF CurPlace[Player]+Thrown<64 THEN
		GOSUB PutPawn
	ELSE
		Temp1=Thrown:Thrown=63-CurPlace[Player]:Temp2=Thrown:GOSUB PutPawn
		Thrown=Temp1-Temp2:GOSUB PawnBack
	ENDIF
	! Special
	IF CurPlace[Player]=6 THEN
		! Bridge -> Goto 12
		SOUNDPOOL.PLAY SndStr01,Snd2,SndVol,SndVol,1,0,1
		DIALOG.MESSAGE PlayName$[Player]+" "+Taal$[29,Lang],,QQ,"OK"
		FOR Qi=6 TO 11
			GR.MODIFY Pawn[Player],"x",Place[Qi*2+1]+(Player-1)*10,"y",Place[Qi*2+2]+(Player-1)*5:GR.RENDER
			PAUSE 100
		NEXT Qi
		CurPlace[Player]=12
	ENDIF
	IF CurPlace[Player]=42 THEN
		! Maze -> Goto 37
		SOUNDPOOL.PLAY SndStr01,Snd2,SndVol,SndVol,1,0,1
		DIALOG.MESSAGE PlayName$[Player]+" "+Taal$[30,Lang],,QQ,"OK"
		FOR Qi=41 TO 36 STEP -1
			GR.MODIFY Pawn[Player],"x",Place[Qi*2+1]+(Player-1)*10,"y",Place[Qi*2+2]+(Player-1)*5:GR.RENDER
			PAUSE 100
		NEXT Qi
		CurPlace[Player]=37
	ENDIF
	! PawnStatus[4] 0=OK 1-at Start 2=Wait one turn 3=Wait in pit 4=Wait in prison
	IF CurPlace[Player]=19 THEN
		! Inn -> Wait one turn
		PawnStatus[Player]=2
		SOUNDPOOL.PLAY SndStr01,Snd2,SndVol,SndVol,1,0,1
		DIALOG.MESSAGE PlayName$[Player]+" "+Taal$[31,Lang],,QQ,"OK"
	ENDIF
	IF CurPlace[Player]=31 THEN
		! Pit -> Wait for relay
		R$=""
		FOR i=1 TO 4
			IF i<>Player & PawnStatus[i]=3 THEN
				R$=", "+Taal$[32,Lang]+" "+PlayName$[i]+" "+Taal$[33,Lang]
				PawnStatus[i]=0
			ENDIF
		NEXT i
		SOUNDPOOL.PLAY SndStr01,Snd2,SndVol,SndVol,1,0,1
		DIALOG.MESSAGE PlayName$[Player]+" "+Taal$[24,Lang]+R$,,QQ,"OK"
		PawnStatus[Player]=3
	ENDIF
	IF CurPlace[Player]=52 THEN
		! Prison -> Wait for relay
		R$=""
		FOR i=1 TO 4
			IF i<>Player & PawnStatus[i]=4 THEN
				R$=", "+Taal$[32,Lang]+" "+PlayName$[i]+" "+Taal$[33,Lang]
				PawnStatus[i]=0
			ENDIF
		NEXT i
		SOUNDPOOL.PLAY SndStr01,Snd2,SndVol,SndVol,1,0,1
		DIALOG.MESSAGE PlayName$[Player]+" "+Taal$[25,Lang]+R$,,QQ,"OK"
		PawnStatus[Player]=4
	ENDIF
	IF NumOfPlayers=1 & (PawnStatus[1]=3 | PawnStatus[1]=4) THEN
		! Player is in either Pit or Prison -> it's over
		DRW=1
	ENDIF
	IF NumOfPlayers=2 & PawnStatus[1]+PawnStatus[2]=7 THEN
		! Both players are in Pit and Prison -> it's a draw
		DRW=2
	ENDIF
	! Check if occupied
	FOR i=1 TO NumOfPlayers
		IF Player<>i & CurPlace[i]=CurPlace[Player] & CurPlace[i]<>0 & CurPlace[i]<>31 & CurPlace[i]<>52 THEN
			SOUNDPOOL.PLAY SndStr01,Snd2,SndVol,SndVol,1,0,1
			DIALOG.MESSAGE PlayName$[Player]+" "+Taal$[34,Lang]+" "+PlayName$[i],Taal$[35,Lang]+" "+INT$(PrevPlace),QQ,"OK"
			PAUSE 1000
			! Back to START?
			IF PrevPlace=0 THEN
				GR.MODIFY Pawn[Player],"x",10+Player*40,"y",540+Player*10:GR.RENDER
				CurPlace[Player]=PrevPlace
				PawnStatus[Player]=1
			ELSE
				GR.MODIFY Pawn[Player],"x",Place[(PrevPlace-1)*2+1]+(Player-1)*10,"y",Place[(PrevPlace-1)*2+2]+(Player-1)*5:GR.RENDER
				CurPlace[Player]=PrevPlace
				PawnStatus[Player]=0
			ENDIF
		ENDIF
	NEXT i
UNTIL CurPlace[1]=63 | CurPlace[2]=63 | CurPlace[3]=63 | CurPlace[4]=63 | DRW>0
GR.HIDE D1:GR.HIDE D2:GR.RENDER
GR.TEXT.SIZE 40:GR.TEXT.ALIGN 2
SOUNDPOOL.PLAY SndStr01,Snd1,SndVol,SndVol,1,0,1
! Check winner or over (2 players) or over (1 player)
IF DRW=0 THEN
	! New HiScore?
	If Score[Player]=-1 | Score[Player]>Turn THEN
		! Save new file
		TEXT.OPEN W,FS,DataPath$+"Score.txt"
			FOR j=1 TO NumNames-1
				LIST.GET NS,j,N1$
				LIST.GET SC,j,N1
				IF N1$=PlayName$[Player] THEN
					N1=Turn
					NS$=" !!!! HI_SCORE !!!!!"
				ELSE
					NS$=""
				ENDIF
				TEXT.WRITELN FS,N1$+","+INT$(N1)
			NEXT j
		TEXT.CLOSE FS
	ENDIF
	GR.BITMAP.DRAW g,WinGoose,350,100
	GR.COLOR 192,255,255,255,1
	GR.RECT g,0,350,1200,470:GR.COLOR 255,0,0,0,1
	GR.TEXT.DRAW g,600,400,PlayName$[Player]+" "+Taal$[36,Lang]
	GR.TEXT.DRAW g,600,450,Taal$[27,Lang]+" "+INT$(Turn)+NS$
	GR.RENDER
ENDIF
IF DRW=1 THEN
	GR.BITMAP.DRAW g,LooseGoose,350,100
	GR.COLOR 192,255,255,255,1
	GR.RECT g,0,350,1200,470:GR.COLOR 255,0,0,0,1
	GR.TEXT.DRAW g,600,400,PlayName$[1]+" "+WORD$(Taal$[37,Lang],1,",")
	GR.TEXT.DRAW g,600,450,WORD$(Taal$[37,Lang],2,",")
	GR.RENDER
ENDIF
IF DRW=2 THEN
	GR.BITMAP.DRAW g,LooseGoose,350,100
	GR.COLOR 192,255,255,255,1
	GR.RECT g,0,350,1200,470:GR.COLOR 255,0,0,0,1
	GR.TEXT.DRAW g,600,400,PlayName$[1]+" "+Taal$[38,Lang]+" "+PlayName$[2]
	GR.TEXT.DRAW g,600,450,Taal$[39,Lang]
	GR.RENDER
ENDIF
POPUP MID$(Taal$[8,Lang],2,LEN(Taal$[8,Lang])-2),,,1
GOSUB GetTouch
GR.CLS:GR.COLOR 255,255,255,0,1:GR.TEXT.SIZE 60
LIST.CREATE N,Thanks
GR.TEXT.DRAW g,600,600,Taal$[43,Lang]:LIST.ADD Thanks,g
FOR i=1 TO NumOfPlayers
	GR.TEXT.DRAW g,600,600+i*60,PlayName$[i]:LIST.ADD Thanks,g
NEXT i
GR.GROUP.LIST ShowThanks,Thanks
GR.COLOR 0,0,0,0,1:GR.RECT Fade,200,100,1000,500
FOR i=1 TO 20:GR.MOVE ShowThanks,0,-20:GR.RENDER:PAUSE 150:NEXT i
SOUNDPOOL.PLAY SndStr01,Snd1,SndVol,SndVol,1,0,1
FOR i=25 TO 255 STEP 20:GR.MODIFY Fade,"alpha",i:GR.RENDER:PAUSE 200:NEXT i
PAUSE 1000
EXIT

GetTouch:
	DO
		GR.TOUCH Touched,x,y
	UNTIL Touched
	DO
		GR.TOUCH Touched,x,y
	UNTIL !Touched
	x/=sx:y/=sy
RETURN
ThrowDice:
	SOUNDPOOL.PLAY SndStr01,Snd0,SndVol,SndVol,1,0,1
	PAUSE 2000
	GR.HIDE Button:GR.SHOW D1:GR.SHOW D2
	Nt1=FLOOR(10*RND()+10):Nt2=FLOOR(10*RND()+10)
	Nr1=FLOOR(6*RND()+1):Nr2=FLOOR(6*RND()+1)
	DO
		IF Nt1>1 THEN
			GR.MODIFY D1,"bitmap",Dice[FLOOR(6*RND()+1)]
			Nt1=Nt1-1
		ELSE
			GR.MODIFY D1,"bitmap",Dice[Nr1]
		ENDIF
		IF Nt2>1 THEN
			GR.MODIFY D2,"bitmap",Dice[FLOOR(6*RND()+1)]
			Nt2=Nt2-1
		ELSE
			GR.MODIFY D2,"bitmap",Dice[Nr2]
		ENDIF
		GR.RENDER
		PAUSE 500/Nt1
	UNTIL Nt1=1 & Nt2=1
	GR.MODIFY D1,"bitmap",Dice[Nr1]:GR.MODIFY D2,"bitmap",Dice[Nr2]:GR.RENDER
!Nr1=2:Nr2=4
	Thrown=Nr1+Nr2
	IF SndVol THEN Beep()
	GR.MODIFY nSc,"text",INT$(Thrown):GR.SHOW nSc:GR.RENDER
	PAUSE 1000
RETURN
PutPawn:
	IF PawnStatus[Player]<>1 | Thrown<>9 THEN
		FOR Qi=CurPlace[Player] TO CurPlace[Player]+Thrown-1
			GR.MODIFY Pawn[Player],"x",Place[Qi*2+1]+(Player-1)*10,"y",Place[Qi*2+2]+(Player-1)*5:GR.RENDER
			PAUSE 300
		NEXT Qi
		CurPlace[Player]=CurPlace[Player]+Thrown
		PawnStatus[Player]=0
	ENDIF
	IF PawnStatus[Player]=1 & ((Nr1=4 & Nr2=5 ) | (Nr1=5 & Nr2=4 )) THEN
		! From Start 4 + 5 -> Goto 53
		SOUNDPOOL.PLAY SndStr01,Snd3,SndVol,SndVol,1,0,1
		DIALOG.MESSAGE Taal$[40,Lang],,QQ,"OK"
		s=560
		FOR Qi=120 TO 500 STEP 50
			s=s-50
			GR.MODIFY Pawn[Player],"x",Qi+(Player-1)*10,"y",s+(Player-1)*5:GR.RENDER
			PAUSE 100
		NEXT Qi
		CurPlace[Player]=53
		GR.MODIFY Pawn[Player],"x",Place[105]+(Player-1)*10,"y",Place[106]+(Player-1)*5:GR.RENDER
		PawnStatus[Player]=0
	ENDIF
	IF PawnStatus[Player]=1 & ((Nr1=3 & Nr2=6 ) | (Nr1=6 & Nr2=3 )) THEN
		! From Start 3 + 6 -> Goto 26
		SOUNDPOOL.PLAY SndStr01,Snd3,SndVol,SndVol,1,0,1
		DIALOG.MESSAGE Taal$[41,Lang],,QQ,"OK"
		s=560
		FOR Qi=80 TO 230 STEP 20
			s=s-60
			GR.MODIFY Pawn[Player],"x",Qi+(Player-1)*10,"y",s+(Player-1)*5:GR.RENDER
			PAUSE 100
		NEXT Qi
		CurPlace[Player]=26
		GR.MODIFY Pawn[Player],"x",Place[51]+(Player-1)*10,"y",Place[52]+(Player-1)*5:GR.RENDER
		PawnStatus[Player]=0
	ENDIF
	IF CurPlace[Player]=58 THEN
		! Death -> Goto 0
		SOUNDPOOL.PLAY SndStr01,Snd1,SndVol,SndVol,1,0,1
		DIALOG.MESSAGE PlayName$[Player]+" "+Taal$[42,Lang],,QQ,"OK"
		CurPlace[Player]=0:PawnStatus[Player]=1
		GR.MODIFY Pawn[Player],"x",50+(Player-1)*40,"y",550+(Player-1)*10:GR.RENDER
	ENDIF
	! Goose?
	IF CurPlace[Player]>0 & CurPlace[Player]+Thrown<63 & (CurPlace[Player]/9=INT(CurPlace[Player]/9) | (CurPlace[Player]+4)/9=INT((CurPlace[Player]+4)/9)) THEN
		SOUNDPOOL.PLAY SndStr01,Snd2,SndVol,SndVol,1,0,1
		POPUP Taal$[5,Lang]+" "+Taal$[6,Lang]:PAUSE2000
		GOTO PutPawn
	ENDIF
	IF CurPlace[Player]>0 & CurPlace[Player]+Thrown>=63 & ((CurPlace[Player]/9=INT(CurPlace[Player]/9) | (CurPlace[Player]+4)/9=INT((CurPlace[Player]+4)/9)) & CurPlace[Player]<>63) THEN
		SOUNDPOOL.PLAY SndStr01,Snd2,SndVol,SndVol,1,0,1
		POPUP Taal$[5,Lang]+" "+Taal$[6,Lang]:PAUSE2000
		NoTrhow=1
	ENDIF
RETURN
PawnBack:
	IF CurPlace[Player]-Thrown-1<1 THEN
		Thrown=Thrown-CurPlace[Player]
	ENDIF
	FOR Qi=CurPlace[Player]-2 TO CurPlace[Player]-Thrown-1 STEP -1
		GR.MODIFY Pawn[Player],"x",Place[Qi*2+1]+(Player-1)*10,"y",Place[Qi*2+2]+(Player-1)*5:GR.RENDER
		PAUSE 300
	NEXT Qi
	CurPlace[Player]=CurPlace[Player]-Thrown
	IF CurPlace[Player]=58 THEN
		! Death -> Goto 0
		DIALOG.MESSAGE PlayName$[Player]+" "+Taal$[42,Lang],,QQ,"OK"
		CurPlace[Player]=0:PawnStatus[Player]=1
		GR.MODIFY Pawn[Player],"x",50+(Player-1)*40,"y",550+(Player-1)*10:GR.RENDER
	ENDIF
	! Goose?
	IF CurPlace[Player]>0 & CurPlace[Player]<63 & (CurPlace[Player]/9=INT(CurPlace[Player]/9) | (CurPlace[Player]+4)/9=INT((CurPlace[Player]+4)/9)) THEN
		IF Temp1>0 THEN Thrown=Temp1:Temp1=0
		SOUNDPOOL.PLAY SndStr01,Snd2,SndVol,SndVol,1,0,1
		POPUP Taal$[5,Lang]+" "+Taal$[6,Lang]:PAUSE2000
		GOTO PawnBack
	ENDIF
	! Back to Start?
	IF CurPlace[Player]=1 THEN
		SOUNDPOOL.PLAY SndStr01,Snd1,SndVol,SndVol,1,0,1
		CurPlace[Player]=0
		GR.MODIFY Pawn[Player],"x",50+(Player-1)*40,"y",550+(Player-1)*10:GR.RENDER
	ENDIF
RETURN

! NL, UK, FR, DE
READ.DATA "Geluid","Ja","Nee","Sound","Yes","No","Son","Oui","Non","Ton","Ja","Nein"
! NL
READ.DATA "Voeg een NIEUWE NAAM toe","Een GANS! Ga het aantal gegooide","ogen nog een keer verder!"
READ.DATA "Spelregels","(tik op het scherm om te sluiten)","Ganzenbord","SPELER(S)"
READ.DATA "Kies het aantal SPELERS","Kies Naam van Speler","Voer de nieuwe NAAM in","Speler"
READ.DATA "Wil je de spelersvolgorde loten?","OK, de spelers zijn gekozen, we kunnen beginnen"
READ.DATA "Aantal Spelers","moet deze beurt overslaan","wacht op bevrijding","is aan de beurt"
READ.DATA "staat op vakje","staat op START","is in de HERBERG (beurt overslaan)","zit in de PUT"
READ.DATA "zit in de GEVANGENIS","en is aan de beurt","Beurt","is het verst!"
READ.DATA "is op de BRUG - Ga naar vakje 12","is verdwaald in het DOOLHOF - Terug naar 37"
READ.DATA "is op bezoek in de HERBERG - 1 beurt overslaan!","maar","is bevrijd"
READ.DATA "helaas is deze plek bezet door","JE MOET TERUG NAAR JE VORIGE PLEK, VAKJE"
READ.DATA "heeft gewonnen!","je zit vast, het spel is voorbij","en","jullie zitten vast, het spel is voorbij"
READ.DATA "Een 4 en een 5 -> Ga naar vakje 53","Een 3 en een 6 -> Ga naar vakje 26"
READ.DATA "je ontmoet de dood -> Ga terug naar START","BEDANKT"
! UK
READ.DATA "Add NEW NAME","A GOOSE! Move again","the number thrown"
READ.DATA "Rules","(tap screen to exit)","Goose","PLAYER(S)"
READ.DATA "Choose number of PLAYERS","Choose name of Player","Enter a new NAME","Player"
READ.DATA "Would you like to shuffle the order?","OK, the players have been chosen, we can start"
READ.DATA "Number of Players","has to skip this turn","is waiting for liberation","takes this turn"
READ.DATA "is located on box","is at START","stays in the INN (skip next turn)","is in the PIT"
READ.DATA "is in PRISON","and throws next","Turn","is the furthest!"
READ.DATA "is on the BRIDGE - Go to box 12","got lost in the MAZE - Back to 37"
READ.DATA "is visiting the INN - skip 1 turn!","but","has been set free"
READ.DATA "unfortunately this place is occupied by","YOU HAVE TO GO BACK TO YOUR PREVIOUS PLACE, BOX"
READ.DATA "has won!","you're stuck, game is over","and","you're both stuck, game is over"
READ.DATA "A 4 and a 5 -> Goto to box 53","A 3 and a 6 -> Goto to box 26"
READ.DATA "you meet the death -> Go back to START","Thanks"
! FR
READ.DATA "Ajoute un AUTRE NOM","Une OIE! Avancez encore une fois","le nombre obtenu"
READ.DATA "Règles","(Touchez l’écran pour fermer)","Jeu de l'oie","JOUEUR(S)"
READ.DATA "Choisissez le nobre de JOUEURS","Choisissez le nom d'un Joueur","Entrez le nouveau nom","Joueur"
READ.DATA "Vous voulez tirer l’ordre?","OK, les joueurs sont choisis, nous pouvons commencer"
READ.DATA "Nombre de Joueurs","devra passer une fois son tour","est en attente pour la libération","prend cet tour"
READ.DATA "est située à la case","est à START","est dans l’AUBERGE (passer une fois le tour)","est dans le PUITS"
READ.DATA "est dans le PRISON","et a le prochain tour","Tour","est le plus éloigné!"
READ.DATA "est sur le PONT - Avancer à la case 12","se perd dans le LABYRINTHE - Retourner à la case 37"
READ.DATA "a visité l’AUBERGE - passer 1 fois le tour!","mais","a été libéré"
READ.DATA "malheureusement cette place occupée par","VOUS DEVEZ ALLER À VOTRE DERNIER LIEU, CASE"
READ.DATA "a gagné!","vous êtes coincé, le jeu est terminé","et","vous êtes coincé, le jeu est terminé"
READ.DATA "Un 4 et un 5 -> Avancer à la case 53","Un 3 et un 6 -> Avancer à la case 26"
READ.DATA "rencontrer la mort -> Retourner à START","Merci"
! DE
READ.DATA "Melde ein NEUE NAME an","Eine GANS! Gehe nochmals","das geworfen Anzahl"
READ.DATA "Regeln","(Tippe am schirm zum schliessen)","Gänsespiel","SPIELER"
READ.DATA "Wähle die Anzahl der SPIELER","Wähle den Namen eines Spielers","Gebe den neuen Namen","Spieler"
READ.DATA "Möchten Sie die Reihenfolge zu schütteln?","OK, die Spieler werden ausgewählt, wir können anfangen"
READ.DATA "Anzahl der Spieler","wird einmal ausgesetzt","wartet auf Befreiung","ist an der Reihe"
READ.DATA "befindet sich auf Feld","ist auf START","ist in der HERBERGE (einmal ausgesetzt)","ist in der BRUNNEN"
READ.DATA "ist im GEFÄNGNIS","und ist an der Reihe","Reihe","ist am weitesten!"
READ.DATA "ist auf der Brücke - Geh nach Feld 12","ist im IRRGARTEN verloren - Zurück nach 37"
READ.DATA "besucht die HERBERGE - einmal ausgesetzt!","aber","ist befreit"
READ.DATA "leider ist dieser Platz besetst durch","SIE MÜSSEN ZURÜCK ZU IHREM FRÜHEREN PLATZ, FELD"
READ.DATA "hat gewonnen!","du steckt in der Klemme, das Spiel ist vorbei","und","sie stecken in der Klemme, das Spiel ist vorbei"
READ.DATA "Ein 4 und ein 5 -> Geh nach Feld 53","Ein 3 und ein 6 -> Geh nach Feld 26"
READ.DATA "Sie treffen den Tod -> Geh zurück nach START","Danke"
