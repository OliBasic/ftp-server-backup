! Space Panic

gr.open 255,0,0,0,1,1
pause 200
gr.screen r_w, r_h
oy =46
dc =90
dw= 820
dh= 1205
scw= r_w/dw
sch= r_h/dh
gr.scale scw,sch

path$= "SpacePanic/"
DIM lvPtr[4], num[11], clef[4], ft[3], fla[3]
DIM mc$[9], flbmp[6], mstr[7], mb[4], mbst[4]
array.load mpx[],5,6,7,7,6,5     % moves of the monster room4
array.load mpy[],2,2,2,1,1,1

! all possibilities of the robot
U_ok$= "14 24 34 64 84 94 16 76 86"
L_ok$= "23 33 42 52 53 73 83 24 34 44 54 64 74 84 94 55 85 95 26 36 46 56 66 75 76 86 96"
R_ok$= "13 23 32 43 63 73 14 24 34 44 54 64 74 84 45 65 75 16 26 36 46 56 66 76 85 86"
D_ok$= "13 23 33 63 83 93 15 65 75 85"

soundpool.open 2
soundpool.load vies, path$+"Vies.ogg"
soundpool.load shoot,path$+"shoot.ogg"
soundpool.load doors,path$+"Porte.ogg"
soundpool.load tic,  path$+"Tic.ogg"
soundpool.load gagne,path$+"Gagne.ogg"
soundpool.load perdu,path$+"Perdu.ogg"
soundpool.load aoooh,path$+"Aoooh.ogg"

! load bitmap
gr.bitmap.load bckgrd,  path$+"Panique_SpatialeM.png"  % 292x420
gr.bitmap.load numBmp,  path$+"Num_Mm.png"   % 268x39
gr.bitmap.load eleBmp,  path$+"ElementsT.png"

gr.bitmap.crop robotBmp ,eleBmp,361,209,76,88  % robot
gr.bitmap.crop spship,   eleBmp,2,204,181,96   % ship
gr.bitmap.crop clefBmp , eleBmp,467,244,51,21  % key

! room1
gr.bitmap.crop braHbmp, eleBmp, 0,0,89,89    % arms
gr.bitmap.crop braLbmp, eleBmp, 0,90,89,89
! room2
gr.bitmap.crop flbmp[1], eleBmp, 180,0,89,89
gr.bitmap.crop flbmp[2], eleBmp, 90,0,89,89
gr.bitmap.crop flbmp[3], eleBmp, 270,0,89,89
gr.bitmap.crop flbmp[4], eleBmp, 180,90,89,89
gr.bitmap.crop flbmp[5], eleBmp, 90,90,89,89
gr.bitmap.crop flbmp[6], eleBmp, 270,90,89,89
! room3
gr.bitmap.crop mstr[1], eleBmp, 360,0,89,89
gr.bitmap.crop mstr[2], eleBmp, 360,90,89,89
gr.bitmap.crop mstr[3], eleBmp, 360,0,89,89
gr.bitmap.crop mstr[4], eleBmp, 360,90,89,89
gr.bitmap.crop mstr[5], eleBmp, 450,0,89,89
gr.bitmap.crop mstr[6], eleBmp, 450,90,89,89
gr.bitmap.crop mstr[7], eleBmp, 540,0,89,178  % the big monster
! room4
gr.bitmap.crop monster, eleBmp,268,213,62,85    % ghost room4 (up right)
! time display
for n=1 to 11
  gr.bitmap.crop num[n],numBmp,(n-1)*24,0,24,39      % digital numbers
next
gr.bitmap.crop nfond,bckgrd,39,48,211,322

gosub helpScreen

gr.bitmap.delete bckgrd    % to free memory
gr.bitmap.delete eleBmp
gr.bitmap.delete numBmp
! size ajustement
gr.bitmap.scale scfond, nfond ,r_w/scw+10, r_h/sch-oy-10
gr.bitmap.scale robotsc,robotBmp,dc-25,dc-18
gr.bitmap.scale clefBmp, clefBmp, 65, 35

! make lifters bitmap
lg =dc-10
gr.bitmap.create lift,lg,6*dc
gr.bitmap.drawinto.start lift
 gr.color 255,0,0,0,1
 gr.rect nul,0,0,lg,10
 gr.rect nul,0,2*dc,lg,2*dc+10
 gr.rect nul,0,4*dc,lg,4*dc+10
gr.bitmap.drawinto.end

score =0
File.Exists ScGame, "SpacePanic.ini"
if ScGame
  grabfile ScGame$, "SpacePanic.ini"
  score =val(ScGame$)
endif

StartNew:        % start new game   ----------------------------
GR.CLS
gr.bitmap.draw fond, scfond, 0,oy

gr.color 255,0,255,255,1   % message text
gr.text.size 35
gr.text.align 2
gr.text.draw mess, r_w/2/scw, 625, "you must reach the red button !"

gr.color 255,90,90,90,1
gr.text.draw sco, r_w/2/scw, 1155, "Best score = "+replace$(str$(score),".0","")

gr.bitmap.draw clef, clefBmp, 65+5*dc, 122
gr.bitmap.draw clef[1], clefBmp, 40, oy+60+dc
gr.bitmap.draw clef[2], clefBmp, 40, oy+68+3*dc
gr.bitmap.draw clef[3], clefBmp, 750, oy+50+4*dc
gr.bitmap.draw clef[4], clefBmp, 640, oy+30+dc
gr.color 255,255,0,0,0
gr.set.stroke 3
gr.text.size 35
gr.text.draw nclef, 98+5*dc, 122, "0"   % number of keys collected.
gr.hide clef
gr.hide nclef

! lifters
ylfL = 48+oy
ylfR = 48+oy+dc
gr.bitmap.draw liftL,lift,24+3*dc,ylfL
gr.bitmap.draw liftR,lift,24+4*dc,ylfR
gr.set.stroke 4
gr.color 100,128,128,128,0
gr.rect nul,55+3*dc,oy+30,55+3*dc+20,oy+50+5*dc
gr.rect nul,55+4*dc,oy+30,55+4*dc+20,oy+50+5*dc
for f=1 to 5
  gr.line nul,55+3*dc, oy+f*dc, 55+3*dc+10, oy+f*dc+14
  gr.line nul,55+3*dc+10, oy+f*dc+14, 55+3*dc+20, oy+f*dc
  gr.line nul,55+4*dc, oy+f*dc, 55+4*dc+10, oy+f*dc-14
  gr.line nul,55+4*dc+10, oy+f*dc-14, 55+4*dc+20, oy+f*dc
next
gr.set.stroke 6
gr.line nul, 16+8*dc, oy+4*dc-45, 9*dc-16, oy+4*dc-45

! trapdoors
gr.set.stroke 8
gr.color 255,0,0,0,1
gr.line trd2, 16+8*dc, oy+5*dc-15, 16+8*dc, oy+5*dc+35
gr.line trd1, 16+8*dc, oy+3*dc-45, 9*dc-16, oy+3*dc-45

gr.set.stroke 1
gr.bitmap.draw dm, num[1], 620,100
gr.bitmap.draw um, num[1], 650,100
gr.bitmap.draw pp, num[11],680,100    % display the timer
gr.bitmap.draw ds, num[1], 710,100
gr.bitmap.draw us, num[1], 740,100

gr.color 255,255,0,0,1
gr.circle explos, 45+8*dc, oy+65+dc, 12   % red button room4
gr.render
pause 500
dpx =-181
dpy =-96
arx =12
ary =oy+34
gr.bitmap.draw ship, spship, dpx, dpy
do
  gr.get.position ship,px,py
  ix =floor((px-arx)/6)
  iy =floor((py-ary)/6)
  if abs(ix)<5 & abs(iy)<5 then D_u.break
  gr.modify ship, "x", px-ix
  gr.modify ship, "y", py-iy
  gr.render
  pause 20
until 0
gr.modify ship, "x", arx
gr.modify ship, "y", ary
gr.render
gr.color 255,0,255,0,1
gr.circle lvPtr[1],90, oy+60,5   % 4 starting lives (green points)
gr.circle lvPtr[2],90, oy+80,5
gr.circle lvPtr[3],70, oy+70,5
gr.circle lvPtr[4],110,oy+70,5

rcx =3    % robot start position
rcy =2
gr.bitmap.draw rbptr,robotsc,30+(rcx-1)*dc,16+(rcy-1)*dc  % robot  (you !)

pmr =1
gr.bitmap.draw mstr, monster, 36+mpx[pmr]*dc, oy+50+mpy[pmr]*dc  % ghost room4

bmh =0
bml =2
gr.bitmap.draw braH, braHbmp, 24+(bmh-1)*dc, oy+60+1*dc   % arms room1
gr.bitmap.draw braL, braLbmp, 24+(bml-1)*dc, oy+40+2*dc

fm  =2
ind =1
gr.bitmap.draw lf, flbmp[2], 34+(fm-1)*dc, oy+40+3*dc    % flamethrower room2
gr.bitmap.draw flam, flbmp[5], 34+(fm-1)*dc, oy+32+4*dc
gr.hide flam

ms =1          %  room3  (down right)
gr.bitmap.draw mb[1], mstr[2], 36+5*dc, oy+41+4*dc   % little down left monster
gr.bitmap.draw mb[2], mstr[4], 36+5*dc, oy+43+3*dc   % little up left monster
gr.bitmap.draw mb[3], mstr[6], 6+8*dc, oy+41+3*dc    % little up right monster
gr.bitmap.draw mb[4], mstr[7], 50+6*dc, oy+55+3*dc   % the big monster
for m =1 to 4
  gr.hide mb[m]  % no visible at start
next

destruction =0
clefs =0
lives =4
win =0
lpL =0
lpR =0
lfs =0
tm  =0   % minutes
ts  =0   % seconds
tt     =clock()
tlift  =tt
troom1 =tt
troom2 =tt
troom3 =tt
redB   =tt
stroom3 =0
mkilled =0
UnDim mbst[]
DIM mbst[4]
c$= replace$(format$("#",rcx)+format$("#",rcy)," ","")   % where is the robot
DO
  do
    gr.touch touched,x,y
    gosub anim_ctrl
  until touched | !lives | destruction >=12
  x/=scw
  y/=sch
  if x>540     % right
    bt_R=1
  elseif x<310  % left
    bt_L=1
  elseif x>310
    if y>870    % down
      bt_D=1
    elseif y<870   % up
      bt_U=1
    endif
  endif
  gosub moveCtrl
  if !lives | destruction >=12 then D_u.break
  do
    gr.touch touched,x,y
    gosub anim_ctrl
  until !touched
UNTIL !lives | win | destruction >=12
gr.show ship
if win
  soundpool.play nul,gagne,0.99,0.99,1,0,1
  gr.modify mess,"text", "B R A V O  !!"
  gr.hide rbptr
  gr.render
else
  soundpool.play nul,perdu,0.99,0.99,1,0,1
  gr.modify mess,"text", "You lost !"
  gr.render
endif
arx =-181
ary =-96
do
  gr.get.position ship,px,py
  ix =floor((px-arx)/6)
  iy =floor((py-ary)/6)
  if abs(ix)<5 & abs(iy)<5 then D_u.break
  gr.modify ship, "x", px-ix
  gr.modify ship, "y", py-iy
  gr.render
  pause 20
until 0
gr.modify ship, "x", arx
gr.modify ship, "y", ary
gr.render
pause 2500
if win
  li$=replace$(str$(lives),".0","")
  t$ =mid$(str$(100+tm),2,2)+":"+ mid$(str$(100+ts),2,2)
  scoreG = 240-60*tm+ts+100*lives
  if scoreG <0 then scoreG =0
  score$ =replace$(str$(scoreG),".0","")
  gr.modify mess,"text","Your score  =  "+score$+".    Touch the screen."
  if scoreG>score
    score =scoreG
    gr.modify sco,"text", "Best score = "+replace$(str$(score),".0","")
    cls
    print scoreG
    console.save "SpacePanic.ini"
    cls
  endif
else
  gr.modify mess,"text","Touch the screen to go again if you're brave !"
endif
gr.render
do
  gr.touch touched,x,y
until touched
goto StartNew

moveCtrl:     % move the robot if possible
  if bt_U & Is_In(c$,U_ok$)        % UP except into lifter
    rcy--
    mv=1
    if c$ ="94" & trd1
      rcy++
      mv =0
    endif
  elseif bt_L & Is_In(c$,L_ok$)    % LEFT
    rcx--
    mv=1
    if (rcx+1=6 & lpR) | (rcx+1=5 & lpR=lpL)   % lifter nok
      rcx++
      mv=0
    elseif c$ ="75" & (mbst[2]=1 | mbst[2]=2)
      soundpool.play nul,shoot,0.99,0.99,1,0,1
      mkilled++
      gr.hide mb[2]
      mbst[2] =5
    elseif c$ ="76" & (mbst[1]=1 | mbst[1]=2)
      soundpool.play nul,shoot,0.99,0.99,1,0,1
      mkilled++
      gr.hide mb[1]
      mbst[1] =5
      rcx++
      mv=0
    endif
    if c$ ="75"
      rcx++
      mv=0
    endif
  elseif bt_R & Is_In(c$,R_ok$)    % RIGHT
    rcx++
    mv=1
    if (rcx-1=3 & !lpL) | (rcx-1=4 & lpR=lpL)  % lifter nok
      rcx--
      mv=0
    elseif (c$ ="86" & trd2) %| (c$ ="84" & trd1)   % trapdoors closed !
      rcx--
      mv=0
    elseif c$ ="56" & !stroom3  % robot come into the room3
      troom3 =clock()
      stroom3 =1
    elseif c$ ="85" & (mbst[3]=1 | mbst[3]=2)
      soundpool.play nul,shoot,0.99,0.99,1,0,1
      mkilled++
      gr.hide mb[3]
      mbst[3] =5
    endif
    if c$ ="85"
      rcx--
      mv=0
    endif
  elseif bt_D & Is_In(c$,D_ok$)   % DOWN except into lifter
    rcy++
    mv=1
  endif
  if mv then gosub moveRobot     % move the robot
  bt_U=0
  bt_D=0
  bt_L=0
  bt_R=0
return

anim_ctrl:      %   ---- Space Station animations ----
  if clock()-troom3 >=3000    % room3
    troom3 =clock()
    gosub room3
  elseif clock()-tt >=1000     % timer display
    tt =clock()
    gosub dispTime
    gosub room4
    if destruction
      soundpool.play nul,tic,0.99,0.99,1,0,0.6
      destruction++
      d$ =replace$(str$(13-destruction),".0","")
      gr.modify mess, "text", "You have "+d$+" seconds to get out !!!"
    endif
  elseif clock()-troom1 >=800  % room1 (arms)
    troom1 =clock()
    gosub room1
  elseif clock()-tlift >=700   % lifters speed
    tlift =clock()
    gosub lifters
  elseif clock()-troom2 >=600   % room2
    troom2 =clock()
    gosub room2
  elseif clock()-redB >=400 & destruction     % the red point
    redB =clock()
    clign = 1-1*(clign=1)
    if clign
      gr.hide explos
      gr.show ship
    else
      gr.show explos
      gr.hide ship
    endif
  endif
  if (mkilled =3 | mbst[4]) & trd2
    gr.hide trd2    % open the door of the last key room3 (down right)
    trd2 =0
    soundpool.play nul,doors,0.99,0.99,1,0,1
  endif
  if Is_In(c$,"13 15 83 96")  % catch keys
    gosub catchkey
  elseif c$ ="93" & !destruction  % you've start self-destruction countdown... go away !
    destruction++
    d$ =replace$(str$(13-destruction),".0","")
    gr.modify mess, "text", "You have "+d$+" seconds to get out !!!"
  endif
  gr.render
  for mc =1 to 9     % control bad cells for robot !!
    if c$ =mc$[mc]
      gosub lostlife
      F_n.break
    endif
  next
  if c$ ="32" & destruction>0 & lives>0   % you WIN
    win =1
  endif
return

room1:
  br =1-1*(br=1)
  if br
    bmh =bmh+1-4*(bmh=3)
    gr.modify braH, "x", 24+(bmh-1)*dc
  else
    bml =bml+1-4*(bml=3)
    gr.modify braL, "x", 24+(bml-1)*dc
  endif
  mc$[1] = replace$(format$("#",bmh)+"3"," ","")
  mc$[2] = replace$(format$("#",bml)+"4"," ","")
return

room2:
  if fm =4
    fm =3
    ind =-1
    gr.show flam
  elseif fm =0
    fm =1
    ind =1
    gr.hide flam
  endif
  gr.modify lf, "bitmap", flbmp[fm]
  gr.modify flam, "bitmap", flbmp[fm+3]
  gr.modify flam, "x", 34+(fm-1)*dc
  fm +=ind
  mc$[3] ="00"
  if ind =-1 then mc$[3] = replace$(format$("#",fm+1)+"6"," ","")
return

room3:
  if mkilled =3 then return   % all monsters killed : this room is free.
  rob= Is_In(c$,"75 76 85 86")  % is robot is into this room3 ?
  array.sum mbsum, mbst[]
  if mbsum>=15
    mbst[4]= mbst[4]+1-2*(mbst[4]=1)   % 0 or 1 :big monster appear or not
    if mbst[4]=0 then gr.hide mb[4] else gr.show mb[4]
  elseif mbsum<15
    do
      m =floor(rnd()*3)+1
      if mbst[m]>2 | (m =1 & rob =0) then m =0
    until m
    mbst[m]++
    if mbst[m] =3
      mbst[1]=5
      mbst[2]=5
      mbst[3]=5
      mbst[4]=1
      gr.hide mb[1]
      gr.hide mb[2]
      gr.hide mb[3]
      gr.show mb[4]
    else
      idm =val(word$("1 0",mbst[m]))
      gr.modify mb[m], "bitmap", mstr[2*m-idm]
      gr.show mb[m]
    endif
  endif
  if mbst[4]=1
    mc$[5] ="66"
    mc$[6] ="76"
    mc$[7] ="86"
    mc$[8] ="75"
    mc$[9] ="85"
  else
    mc$[5] ="00"
    mc$[6] ="00"
    mc$[7] ="00"
    mc$[8] ="00"
    mc$[9] ="00"
  endif
return

room4:           % the monster up right  (room 4)
  dr =floor(rnd()*2)
  if dr then pmr =pmr+1-6*(pmr=6) else pmr =pmr-1+6*(pmr=1)
  gr.modify mstr, "x", 36+mpx[pmr]*dc
  gr.modify mstr, "y", oy+50+mpy[pmr]*dc
  mc$[4] = replace$(format$("#", mpx[pmr]+1 )+format$("#", mpy[pmr]+2 )," ","")
return

catchkey:
  cf=0
  if c$ ="13" & clef[1]
    cf =1
  elseif c$ ="15" & clef[2]
    cf =2
  elseif c$ ="96" & clef[3]
    cf =3
  elseif c$ ="83" & clef[4] & mc$[4]<>c$
    cf =4
  endif
  if cf
    clefs++
    gr.show clef
    gr.show nclef
    gr.hide clef[cf]
    gr.modify nclef, "text", replace$(str$(clefs),".0","")
    soundpool.play nul,tic,0.99,0.99,1,0,0.5
    clef[cf] =0
    if clefs =4   % now you can go to the computer room to start the self-destruction
      if trd1
        gr.hide trd1
        gr.set.stroke 8
        gr.color 255,0,0,0,1
        gr.line nul, 16+8*dc, oy+4*dc-45, 9*dc-16, oy+4*dc-45
        trd1 =0
        soundpool.play nul,doors,0.99,0.99,1,0,1
      endif
    endif
    gr.render
  endif
return

lifters:
  lfs =1-1*(lfs=1)    % alternate lifter left / right
  if lfs
    lpL =1-1*(lpL=1)
    gr.modify liftL,"y",ylfL+lpL*dc  % left lifter
    if rcx=4  % robot into left lifter
      rcy++
      if rcy>6  % stay into lifte
        rcx++
        rcy-=2
      endif
      gosub moveRobot
    endif
  else
    lpR =1-1*(lpR=1)
    gr.modify liftR,"y",ylfR-lpR*dc   % right lifter
    if rcx=5   % robot into right lifter
      rcy--
      if rcy<2  % stay into lifte
        rcx--
        rcy++
      endif
      gosub moveRobot
    endif
  endif
return

moveRobot:
  gr.modify rbptr,"x",30+(rcx-1)*dc
  gr.modify rbptr,"y",16+(rcy-1)*dc
  gr.render
  c$= replace$(format$("#",rcx)+format$("#",rcy)," ","")   % where is the robot
  mv=0
return

lostlife:
  soundpool.play nul,aoooh,0.8,0.8,1,0,1
  for r =255 to 0 step -5
    gr.modify rbptr, "alpha", r
    gr.render
    pause 8
  next
  gr.hide lvPtr[lives]
  lives--
  rcx =3
  rcy =2
  mv =0
  gr.modify rbptr, "alpha", 255
  ts+=10   % penality: 10 second to time
  if ts>59
    ts=ts-59
    tm++
  endif
  gosub dispTime
  gosub moveRobot
return

dispTime:
  ts =ts+1-60*(ts>=59)       % count seconds
  if ts=0 then tm =tm+1-60*(tm=59)     % count minutes
  gr.modify dm,"bitmap",num[ floor(tm/10) +1]
  gr.modify um,"bitmap",num[ mod(tm,10) +1]    % display they right bitmaps
  gr.modify ds,"bitmap",num[ floor(ts/10) +1]
  gr.modify us,"bitmap",num[ mod(ts,10) +1]
return

helpScreen:
gr.bitmap.scale help, bckgrd, r_w/scw+20, r_h/sch+120
gr.bitmap.draw nul, help, -10, oy-20
gr.color 255,90,90,90,1
gr.rect nul, 40, oy+20, r_w/scw-40, r_h/sch-40
gr.color 255,240,68,0,0
gr.set.stroke 6
gr.text.size 60
gr.text.align 2
gr.text.draw nul, r_w/scw/2, oy+100, "S P A C E   P A N I C"
gr.color 255, 0,255,0,1
gr.set.stroke 2
gr.line nul, 50, oy+130, r_w/scw-50, oy+130
gr.color 255,255,200,0,1
gr.text.align 1
gr.text.size 37
el =60
gr.text.draw nul, 60, oy+150+1*el, "  You must trigger the self-destruction of"
gr.text.draw nul, 60, oy+150+2*el, "this old rusty spacial base full of hostile"
gr.text.draw nul, 60, oy+150+3*el, "mechanisms and monsters."
gr.text.draw nul, 60, oy+150+4*el, "- To accomplish this mission you must first"
gr.text.draw nul, 60, oy+150+5*el, "retrieve the keys from every rooms before"
gr.text.draw nul, 60, oy+150+6*el, "going to press the countdown red button."
gr.text.draw nul, 60, oy+150+7*el, "- Take care to not let monsters to grow at"
gr.text.draw nul, 60, oy+150+8*el, "their third stage of evolution."
gr.text.draw nul, 60, oy+150+9*el, "Kill them first !"
gr.text.draw nul, 60, oy+150+10*el, "- Take care of your lives : you have only 4 !"
gr.text.draw nul, 200, oy+140+16*el, "Touch the screen if you're brave !"
gr.bitmap.draw nul, mstr[7], 450, oy+120+12*el
gr.render
do
  gr.touch touched,x,y
until touched
gr.cls
return

ONBACKKEY:
  gr.close
END "Bye..."
