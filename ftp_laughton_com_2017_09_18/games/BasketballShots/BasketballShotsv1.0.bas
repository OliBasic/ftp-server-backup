! basket v1.0
! basic 1.68
! Oct.29.2012
! by Antonis [tony_gr]

ARRAY.LOAD pos[],266,0,466,0,466,200 % kid positions
GR.OPEN 255,0,0,0,0,1
GR.ORIENTATION 1
PAUSE 1000

GR.TEXT.SIZE 20
GR.TEXT.BOLD 1
GR.SCREEN w, h
Sx=w/800
Sy=h/1232
GR.SCALE sx,sy
success=0
start:

game=1
success=0
StartX=0 % of ball,kid
eEndX=800 % of ball movement
height=0.1 % ball max height parameter
kidStartY=266 % kid Y
StartY=kidStartY % ball Y
sh=0 % shots on target
tot=0 % total
GR.BITMAP.LOAD g,"ground.jpg"
GR.BITMAP.SCALE gr,g,w/sx,h/sy
GR.BITMAP.DRAW ground,gr,0,0
GR.BITMAP.DELETE g
GR.BITMAP.LOAD b,"ball.png"
GR.BITMAP.DRAW ball,b,StartX,StartY
GR.BITMAP.LOAD r,"rim.png"
GR.BITMAP.DRAW rim,r, 665,100
GR.BITMAP.LOAD k1,"kid.png"
GR.BITMAP.DRAW kid,k1,StartX, kidStartY
GR.MODIFY ball,"y",StartY
GR.BITMAP.LOAD k2,"kid2.png"
GR.BITMAP.DRAW kid2,k2,StartX,kidStartY
GR.HIDE kid2
GR.TEXT.DRAW Tx, 270, 1230,"Basketball shots v0.1 by [Antonis]"
GR.RENDER

GR.COLOR 255,255,255,255,1
GR.SET.STROKE 4
GR.TEXT.SIZE 40
GR.COLOR 255,255,0,0,1
GR.RECT bcol1,690,1000,790,1100
GR.TEXT.DRAW s1, 670, 920,"Shoot!"
GR.TEXT.DRAW plus1, 220, 980,"+"
GR.TEXT.DRAW minus1, 220, 1170,"-"
GR.TEXT.DRAW powerTxt, 5, 1050,REPLACE$(STR$( ROUND(eEndX/10) ),".0","")
GR.TEXT.DRAW p1, 70, 920,"power"

GR.COLOR 255,155,55,255,1
GR.TEXT.DRAW plus2, 610, 980,"+"
GR.TEXT.DRAW minus2, 610, 1170,"-"
GR.TEXT.DRAW heightTxt, 340, 1050,REPLACE$(STR$( ROUND( height*1000 )/100 ) ,".0","")
GR.TEXT.DRAW d1, 410, 920,"direction"

GR.TEXT.SIZE 70
GR.TEXT.BOLD 1
GR.COLOR 255,0,0,0,1
GR.TEXT.DRAW target, 400, 75,"0/0"
GR.COLOR 255,85,107,47,0
GR.SET.STROKE 8
GR.ARC arcobj,410,1000,600,1010,0,-180,0
GR.SET.STROKE 36
GR.LINE lobj,60,1100,210,1100

GR.RENDER
DO

 DO
  PAUSE 100
  GR.TOUCH touched,x,y
 UNTIL touched
 IF (x>=220*sx & y>=930*sy & x<=190*sx & y<=990*sy)  & eEndX<1200 THEN
  eEndX=eEndX+50
  GR.MODIFY powerTxt,"text",REPLACE$(STR$( ROUND(eEndX/10) ),".0","")
  GR.MODIFY lobj,"x2",60+ eEndX/6
  GR.RENDER
 ENDIF
 IF (x>=220*sx & y>=1120*sy & x<=190*sx & y<=1180*sy)  & eEndX>510 THEN
  eEndX=eEndX-50
  GR.MODIFY powerTxt,"text",REPLACE$(STR$(ROUND(eEndX/10)),".0","")
  GR.MODIFY lobj,"x2",60+ eEndX/6
  GR.RENDER
 ENDIF

 IF (x>=610*sx & y>=930*sy & x<=670*sx & y<=990*sy) &  height<2 THEN
  height=height+0.1
  f$= REPLACE$(STR$( ROUND( height*1000 )/100 ) ,".0","")
  GR.MODIFY heightTxt,"text", f$
  GR.MODIFY arcobj,"bottom",1000+ height*200
  GR.RENDER
 ENDIF

 IF (x>=610*sx & y>=1120*sy & x<=670*sx & y<=1180*sy)  &  height>0.12 THEN
  height=height-0.1
  f$= REPLACE$( STR$( ROUND( height*1000 )/100 ),".0","")
  GR.MODIFY heightTxt,"text",f$
  GR.MODIFY arcobj,"bottom",1000+ height*200
  GR.RENDER
 ENDIF

 IF x>=690*sx & y>=1000*sy & x<=790*sx & y<=1100*sy THEN
  GOSUB shot
  tot=tot+1
  GR.MODIFY target,"text",REPLACE$(STR$(sh),".0","")+ "/"+ REPLACE$(STR$(tot),".0","")
  GR.RENDER

  IF tot=12 THEN
   POPUP "Game Over!",0,0,4
   TONE 800,500
   PAUSE 2000
   END
  ENDIF

  IF MOD(tot,4)=0  THEN
   POPUP "Next Position",0,0,0
   game=game+2
   StartX=pos[game+1]
   kidStartY= pos[game]
   StartY=kidStartY
   PAUSE 2000
  ENDIF
  GR.MODIFY ball,"x",StartX
  GR.MODIFY ball,"y",StartY
  GR.MODIFY kid,"x",StartX
  GR.MODIFY kid,"y", kidStartY
  GR.MODIFY kid2,"x",StartX
  GR.MODIFY kid2,"y", kidStartY

  GR.RENDER

 ENDIF

UNTIL 0

shot:
success=0
FOR x=-1 TO 2.5 STEP 0.05
 ! calc ball parabola
 x1=x
 y1=height*x*x
 x1=x1*(eEndX-StartX)*0.5+(eEndX-StartX)*0.5+StartX
 y1=y1*StartY+(1-height)*StartY
 GR.MODIFY ball,"x",x1
 GR.MODIFY ball,"y",y1

 ! case 1
 IF x1>605 & x1<660 & y1>198 & y1<250 THEN
  F_N.BREAK
 ENDIF
 ! case 2
 IF x1>=716 & y1>70 & y1<250 THEN
  F_N.BREAK
 ENDIF
 ! case 3
 IF x1>=716 THEN
  success=-1 % 
  flag=1
 ENDIF
 ! case 4
 IF y1>630  THEN % 
  success=-1
  flag=2
  F_N.BREAK
 ENDIF

 GR.RENDER
 GR.HIDE kid
 GR.SHOW kid2
 IF x1>=645 & x1<785 & y1>240 & y1<265 THEN
  success=1
  POPUP "Very good!",0,0,0
  GR.MODIFY ball,"x",690
  GR.MODIFY ball,"y",265
  GOSUB updatekid
  PAUSE 2000
  sh=sh+1
  RETURN
 ENDIF
NEXT x

GR.RENDER

! ball returns or goes out
IF success=-1 THEN
 POPUP "Out!",0,0,0
 TONE 800,800
 GOSUB updateKid
 RETURN
ENDIF
IF x1>705 & y1>170 & y1<225 THEN
 success=1
ENDIF

! now invert ball movement
x2=x1
y2=y1
limit=34

IF success=1 THEN limit=16

FOR x=0 TO limit STEP 2
 PAUSE x+1
 x2=x2-x
 y2=y2+height*x

 GR.MODIFY ball,"x",x2
 GR.MODIFY ball,"y",y2
 GR.RENDER
 IF success=1 & x2<672 THEN

  sh=sh+1
  GR.MODIFY ball,"x",690
  GR.MODIFY ball,"y",265
  GR.RENDER
  POPUP "Good",0,0,0
  GOSUB updatekid
  PAUSE 2000
  RETURN
 ENDIF
NEXT x
PAUSE 1000
GOSUB updateKid
RETURN

updateKid:
GR.HIDE kid2
GR.SHOW kid
GR.RENDER
RETURN
