REM copy of Aat's game : CodeWar.bas    @05/2016 by Cassiope34.

Fn.def toCr$(v)  % to set a value to 1 character more easy to store and find (is_in())
  Fn.rtn chr$(v+64)
Fn.end

Fn.def toValue(c$)  % restore a character to a value.
  Fn.rtn ascii(c$)-64
Fn.end

Fn.def clr( cl$ )
  gr.color val(word$(cl$,1)), val(word$(cl$,2)), val(word$(cl$,3)), ~
           val(word$(cl$,4)), val(word$(cl$,5))
Fn.end

Fn.def LastPtr(ptr)  % put the graphic pointer 'ptr' (so the bitmap) UP to all other.
  gr.getDL ndl[],1
  array.length sz, ndl[]
  if !ptr | sz=1 | ndl[sz] =ptr then array.delete ndl[] : Fn.rtn 0
  array.search ndl[],ptr,n
  if n
    for nn=n to sz-1 : ndl[nn] =ndl[nn+1] : next
    ndl[sz] =ptr : gr.newDL ndl[]
  endif
  array.delete ndl[]
Fn.end

Fn.def move(ptr, arx, ary, speed)   % move smoothly the bitmap 'ptr' to arx,ary
LastPtr( ptr )
do
  gr.get.position ptr,dpx,dpy
  ix =int((dpx-arx)/speed)
  iy =int((dpy-ary)/speed)
  if abs(ix)<3 & abs(iy)<3 then D_u.break
  gr.modify ptr, "x",dpx-ix, "y",dpy-iy
  gr.render
until 0
gr.modify ptr, "x",arx, "y",ary
gr.render
Fn.end

Fn.def moveDBox(ptgr, ptr, py, down, speed, dbt1, t1$, dbt2, t2$, noc, cl$[])
    % ptgr is the group that contents all the dialogBox objects.
if down
  gr.modify dbt1,"text",t1$
  clr( cl$[3] ) : gr.paint.get clr  
  gr.modify dbt2,"text",t2$, "paint", clr
  if t2$=""
    gr.modify dbt1,"text",t1$,"y",-180
  elseif noc
    gr.modify dbt1,"text",t1$,"y",-205
    clr( cl$[1+4*(noc>13)] ) : gr.paint.get clr
    t$ =word$("black red", 1+(noc>13))+" "
    t$+=word$("ACE 2 3 4 5 6 7 8 9 10 JACK QUEEN KING", noc-13*(noc>13))
    gr.modify dbt2, "text", t$, "paint", clr
  endif
endif
LastPtr( ptr ) : LastPtr( dbt1 ) : LastPtr( dbt2 )
do
  gr.get.position ptr, dpx, dpy
  if down
    iy =int((dpy-py)/speed)  : dy =abs(iy)
  else
    iy =int((-py-dpy)/speed) : dy =-1*abs(iy)
  endif
  gr.move ptgr,,dy
  gr.render
until abs(iy)<2
Fn.end

gr.open 255,116,212,129,0,0   % vert clair
gr.screen w,h
scx =1280
scy =800
sx  =w/scx
sy  =h/scy
gr.scale sx,sy

wakelock 3

DIM co[26], back[2], noc[2], yy[2], sco[2], scoptr[4], pr$[26]
DIM cl$[5], act$[8], g[13,6,4]   % the all screen grid is 13x6 cells...
array.load sp[],1,6    % row of players
! for 3e param of g[x,y,n] -> n= :
! 1 =pointer for bitmap
! 2 =value 1-13 = black, 14-26 = red : they are 26 cards shuffled at the screen center.
! 3 =true value to sort : if >13 then true value =value-13.5
! 4 =visible no=0, yes=1
fpo$ ="5 5 5 5 4 4 3 3 2 2"   % first place by number of player's cards (noc[p]).
cre$ =""  :  for c=1 to 26 :cre$+=toCr$(c) :next
ext$ =""  :  for c=1 to 13 :ext$+=toCr$(c+13)+toCr$(c) :next
blk  =1  % or 0  for black cards  (pique ou trèfle)
red  =2  % or 3  for red    (coeur ou carreau)
p    =2  % player -> 1 =AI , 2 =you
cl$[1] ="255 0 0 0 1"        % noir
cl$[2] ="255 255 255 255 1"  % blanc
cl$[3] ="255 76 173 224 1"   % bleu
cl$[4] ="255 238 139 21 1"   % orange
cl$[5] ="255 255 0 0 1"      % rouge
act$[1] ="Tap one of my card and guess its value..."
act$[2] ="Which value do you guess for this card ?"
act$[3] ="I will guess the value of one of your card"
act$[4] ="I will make another guess"
act$[5] ="Do you want to continue"
act$[6] ="I pick a card"
act$[7] ="Please, pick a new card"
act$[8] ="I guess this card is a"

Font.load ft, "Batavia.ttf"
gr.text.setFont ft

gr.bitmap.load cards, "52_cards.png"    % origin size = 2056x1230
gosub yinyang    % other bitmaps maker...
gosub makeCards  % cards bitmaps maker depend of "blk" & "red" values. (will can change)

start =1
Timer.set 60000  % set time evry minute !

p =int(rnd()*2)+1  :whostart =p    % p = the current player

DO               % principal loop
gosub init
if p=1 then gosub AI4Cards :p=3-p   % if the AI start
fin =0  :  new =0

do               % loop of a game.
  if noc[p]<4 then mess$ ="You must pick your first 4 cards."
  gr.modify mess, "y", (1.6+3*(p=2))*ccy, "text", mess$
  do
    gr.touch touched,x,y
    if !background() then gr.render
  until touched | quit | new
  tm =clock()
  if new | quit then D_u.break
  if help then move(ghelp, 0, -scy, 3) : help =0 : D_u.continue
  
  if db then gr.hide curs :move(ThePanel, 5, -200, 2) :db=0    % Debug UP
  
  do
    gr.touch touched,x,y
    pause 10
  until !touched | clock()-tm>1500
  x/=sx   :ncx =int((x-orx)/ccx)+1 :ncx =max(1,ncx) :ncx=min(13,ncx)
  y/=sy   :ncy =int(y/ccy)+1       :ncy =max(1,ncy) :ncy=min(6,ncy)
  
  if clock()-tm>1500 & (ncy=1 | ncy=6) & noc[p] & !fin then gosub chgcard : D_u.Continue
  
  if ncx>11 & ncy=5
    move(ghelp, 0, 0, 3) : help =1
  
  elseif ncy=6
    if g[ncx,ncy,2] & !g[ncx,ncy,4] then gosub DBg :db=1    % just for debug
    
  elseif noc[p]<4     % player must pick his first 4 cards.
    if g[ncx,ncy,1] & (ncy=3 | ncy=4)
      gosub addcard
      if noc[p]=4 & noc[1]<4 then gosub AI4Cards :p =2
      if noc[p]=4
        mess$ =act$[5+p] : if whostart =1 then p =1 :gosub AIplay
      endif
    endif
    
  else    
    if choice    % from the panel
      if ncy=3 | ncy=4
        gr.hide curs :move(ThePanel, 5, -200, 2)   % panel UP
        chx =ncx+13*(cl=red)
        if g[choice,1,2]=chx
          gr.modify mess, "y", (1.6+3*(p=2))*ccy, "text", "good choice !"
          sco[p]++ : gosub affScore : gr.render
          pause 1000
          g[choice,1,4]=1
          gr.modify g[choice,1,1], "bitmap", co[g[choice,1,2]], "alpha", 140
          gr.render  :  choice =0
          gosub ctrlFin
          if !fin
            moveDBox(gbox, dbox, 255, 1, 3, dbt1, act$[5], dbt2, "guessing ?", 0, cl$[])
            mess$   =""
            request =2
          else
            gosub gameOver
          endif
        else
          p =3-p  :  choice =0  :  g[cc2,6,4]=1
          gr.modify g[cc2,6,1], "alpha", 60    % my card is reversed.
          gr.modify mess, "y", (1.6+3*(p=2))*ccy, "text", "Sorry, bad choice !"
          gr.render
          pause 1000
          gosub AIplay
        endif
      endif
      
    elseif request   % from the dialog box
      if ncy=4 & (ncx=5 | ncx=6 | ncx=8 | ncx=9)    % only "Yes" or "No" possible.
        gr.hide curs
        mess$ =act$[5+p]
        moveDBox(gbox, dbox, 305, 0, 2, dbt1, t1$, dbt2, "", 0, cl$[])   % dbox UP
        if request=2
          request =0
          if ncx>7 then p=3-p : gosub AIplay
          
        elseif request=1   % AI's guessing... 
          if (ncx<7 & g[lcg,6,2]<>ch1) | (ncx>7 & g[lcg,6,2]=ch1)   % verif. cheating !!!
            moveDBox(gbox, dbox, 255, 1, 2, dbt1, "It ' s not nice to lie !!!", dbt2, "_", ch1, cl$[])
            ncx =lcg  :  gosub ArrowShow
          else
            request =0
            if ncx<7         % "Yes"  the AI have right.
              g[lcg,6,4]=1    : gr.modify g[lcg,6,1], "alpha", 140
              p =1 : sco[p]++ : gosub affScore
              gosub ctrlFin
              if !fin then gosub AIplay else gosub gameOver
            else             % AI must discover its last card picked & pass the hand.
              g[cc1,1,4]=1 : gr.modify g[cc1,1,1], "bitmap", co[g[cc1,1,2]], "alpha", 60
              p=3-p        : mess$ =act$[5+p]
              pr$[g[lcg,6,2]] =replace$(pr$[g[lcg,6,2]], toCr$(ch1), "")  % delete this value ( ch1 )
            endif
          endif
        endif
      endif
      
    elseif p=2 & guess & ncy=1 & g[ncx,ncy,1] & !g[ncx,ncy,4]  % player choice an AI's card to guess.
      gosub ChxCard
      mess$ =act$[2]
      choice =ncx
      guess  =0
            
    elseif p=2 & noc[p]<10 & !guess & g[ncx,ncy,1] & (ncy=3 | ncy=4)  % player have picked a card
      gosub addcard      
      mess$ =act$[1]
      guess =1
      cc2   =pli   % last picked card.
      
    elseif noc[p]=10
      gosub gameOver
      
    endif
  endif
until new
p =3-whostart    % the other player will start.
UNTIL quit
Timer.clear
wakelock 5
END

gameOver:
fpc =val(word$(fpo$,noc[1])) 
for xc=fpc to fpc+noc[1]-1    % show all A.I. cards
  if g[xc,1,1] & !g[xc,1,4] then gr.modify g[xc,1,1], "bitmap", co[g[xc,1,2]] :gr.render
next
if (sco[1]+sco[2])>0
  win$ ="              A.I.  win !" :mess$ ="A.I.  win"
  if sco[2]>=sco[1] then win$ ="              You  win !" :mess$ ="You  win"
endif
pause 1000  :  fin =1  :  gosub finish
return

OnBackKey:
  gosub finish
back.resume

OnTimer:
  Time ,,,h$,m$  :  gr.modify hre, "text", h$+":"+m$
Timer.resume

finish:
  Dialog.message win$,"       What do you want ?", ok, " Exit ", " New Game ", " Cancel "
  new  =(ok=1 | ok=2) : quit =(ok=1)
return

ctrlFin:
for j=1 to 2
  fpc =val(word$(fpo$,noc[j])) :f =0
  for cr=fpc to fpc+noc[j]-1   :f+=(g[cr,sp[j],4]>0)  :next
  if f=noc[j] then fin =3-j    :F_n.break
next
return

AIplay:
if noc[1]<10      % AI pick a card from center.
  gr.modify mess, "y", (1.6+3*(p=2))*ccy, "text", act$[5+p]
  gr.render  :  pause 500
  
  do : ncx =int(rnd()*13+1) : ncy =int(rnd()*2)+3 :until g[ncx,ncy,2]   % pick from center
  gosub addcard   % set "pli" : place of insertion.
  cc1 =pli
          
  if noc[1]>=noc[2] | noc[2]>5
    mess$ =act$[3]
    gr.modify mess, "y", (1.6+3*(p=2))*ccy, "text", mess$
    gr.render  :  pause 500
    
    gosub AI : gosub AI2  % set 'ch1' & 'ncx'
    
    moveDBox(gbox, dbox, 255, 1, 2, dbt1, act$[8], dbt2, "_", ch1, cl$[])
    gosub ArrowShow   % show ncx
    request =1
  else
    gr.modify mess, "y", (1.6+3*(p=2))*ccy, "text", "I pass the hand..."
    gr.render  :  pause 1000
    p =3-p  :  mess$ =act$[5+p]
  endif
else
  gosub gameOver
endif
RETURN

repl:
  if len(c$)>1 then c$ =replace$(c$,ca$,"")
return

AI:    % best choice for the AI...!?!
fp1 =val(word$(fpo$,noc[1]))
fpc =val(word$(fpo$,noc[2]))
dcp =fpc+noc[2]-1    % cell of the last card.
for crd=fpc to dcp   % AI check card by card of the player.
  vd =0 : vg =0
  if !g[crd,6,4]    % not a visible card.
    cl2 =1+(g[crd,6,2]>13)   %  1=black, 2=red  color of the player's card analysed.
    c$  =pr$[ g[crd,6,2] ]   %  Possibilities.
    
    if crd < dcp     %  see at right side.
      t =dcp : vd =26
      do
        if g[t,6,4]    % visible card
          vd =IS_IN(toCr$(g[t,6,2]),ext$) : t--
        elseif (g[t,6,2]>13)=(mod(vd,2)>0)  % same color ?
          t--
        endif
        if t>crd then vd--
      until t=crd
    endif

    if crd > fpc    % see at left side.
      t =fpc : vg =1
      do
        if g[t,6,4]    % visible card
          vg =IS_IN(toCr$(g[t,6,2]),ext$) : t++
        elseif (g[t,6,2]>13)=(mod(vg,2)>0)  % same color
          t++
        endif
        if t <crd then vg++
      until t=crd
    endif
    
    for a =1 to 26
       if (vg & a<=vg) | (vd & a>=vd) then ca$ =mid$(ext$,a,1) : gosub repl
       if a>=fp1 & a<fp1+noc[1]
         if cl2=1+(g[a,1,2]>13) then ca$ =toCr$(g[a,1,2]) : gosub repl
       endif
    next
    pr$[ g[crd,6,2] ] =c$

  else
    pr$[ g[crd,6,2] ] =""    % a visible card.
    
  endif
next
return

AI2:   %  must choose the best in all shorter pr$[]<>""...
td$ ="" : lm=26 : mt =0
for crd=fpc to dcp
  lp =len(pr$[g[crd,6,2]]) :mt+=lp
  if lp>0 & lp<lm
    td$ =toCr$(crd) :lm =lp
  elseif lp=lm
    td$+=toCr$(crd)
  endif
next
lt =len(td$) :gr.modify tcc, "text", int$(100-round(mt*100/((dcp-fpc)*13),,"HD"))+" %"
if lt
  ncx =toValue(mid$(td$, int(rnd()*lt)+1, 1))
  lg  =len(pr$[g[ncx,6,2]])
  ca$ =mid$(pr$[g[ncx,6,2]], int(rnd()*lg)+1, 1)
  ch1 =toValue(ca$)   % value to be ask into the DBox by the A.I.
  lcg =ncx   % shown by the arrow & to store the last card played by AI
endif
RETURN

affScore:
gr.modify scoptr[p],   "text", int$(sco[p])
gr.modify scoptr[p+2], "text", int$(sco[p])
return

DBg:     % create (and show) the panel with current card's color.
if panl then gr.bitmap.delete panl
gr.bitmap.create panl, scx-8, 205
gr.bitmap.drawinto.start panl
clr("100 80 80 80 1")    % gray
gr.rect nul,10,10,scx-8,205
clr("255 250 246 185 1")    % ivoire
gr.rect nul,0,0,scx-20,190
cl =blk :if g[ncx,6,2]>13 then cl =red
for c=1 to 13
  if IS_IN(toCr$(c+13*(cl=red)), pr$[g[ncx,6,2]])
    gr.bitmap.crop nul, cards, (c-1)*158, cl*246, 158, 246
    gr.bitmap.scale co[c+13*(cl=red)], nul, lc, hc
    gr.bitmap.delete nul
    gr.bitmap.draw nul, co[c+13*(cl=red)], 10+(c-1)*ccx, 30
  endif
next
gr.bitmap.drawinto.end
gr.modify ThePanel, "bitmap", panl    % created before in "init"
move(ThePanel, 5, 300, 4)
list.replace lcurs, 2, ory+3.4*ccy+35
list.replace lcurs, 4, ory+3.4*ccy+35
list.replace lcurs, 5, orx+(ncx-0.5)*ccx
list.replace lcurs, 6, ory+5*ccy-5
gr.modify curs, "list", lcurs
gr.show curs
!mess$ ="carte "+int$(ncx-fpc+1)+" ->  pr$["+int$(g[ncx,6,2])+"] = "+pr$[g[ncx,6,2]]
return

ArrowShow:    % big arrow adapted to the panel or DBox to point to a card...
list.replace lcurs, 2, ory+2*ccy+35+1.4*ccy*(p=1)
list.replace lcurs, 4, ory+2*ccy+35+1.4*ccy*(p=1)
list.replace lcurs, 5, orx+(ncx-0.5)*ccx
list.replace lcurs, 6, ory+(1+4*(p=1))*ccy-5
gr.modify curs, "list", lcurs
gr.show curs
return

addcard:   % add a card at side of 'p' (player)
ny =2+3*(p=2)   % who ?
ix =orx+6*ccx : iy =ory+(ny-1)*ccy
move(g[ncx,ncy,1], ix, iy, 2)
if p=2 then gr.modify g[ncx,ncy,1], "bitmap", co[g[ncx,ncy,2]]  % see player's card.
for s=1 to 4 : swap g[ncx,ncy,s], g[7,ny,s] :next
iy =ory+(sp[p]-1)*ccy     % then insert the card at the right place.
if g[7,ny,1]
  noc[p]++  :  fpc =val(word$(fpo$,noc[p]))   % first place of cards
  pli =fpc  :  pas =-1  :  a =0  :  deb =fpc+noc[p]-2
  if noc[p]=5 | noc[p]=7 | noc[p]=9 then pas =1 : a =1 : deb =fpc
  while g[pli+a,sp[p],1] & g[pli+a,sp[p],3]<g[7,ny,3] : pli++ : repeat
  if (pli>fpc & a) | (pli<fpc+noc[p]-1 & !a)
    for xin=deb to pli-a step pas
      ix =orx+(xin-a)*ccx
      move(g[xin+a,sp[p],1], ix, iy, 1.2)
      for s=1 to 4 :swap g[xin+a,sp[p],s], g[xin+1-a,sp[p],s] :next    
    next
  endif
  ix =orx+(pli-1)*ccx
  move(g[7,ny,1], ix, iy, 1.4)
  for s=1 to 4 :swap g[7,ny,s],g[pli,sp[p],s] :next
  if p=2 then pr$[g[pli,6,2]] =mid$(cre$,1+13*(g[pli,6,2]>13),13)  % for the AI
  if noc[1]+noc[2]>=8 then gosub AI
endif
return

AI4Cards:   % the AI pick it's first 4 cards.
p =1
gr.modify mess, "y", (1.6+3*(p=2))*ccy, "text", "I pick my first 4 cards."
do
  do : ncx =int(rnd()*13+1) : ncy =int(rnd()*2)+3 :until g[ncx,ncy,2]
  gosub addcard
until noc[p]=4
return

ChxCard:     % create (and show) the panel with current card's color.
if panl then gr.bitmap.delete panl
gr.bitmap.create panl, scx-8, 205
gr.bitmap.drawinto.start panl
clr("100 80 80 80 1")    % gray
gr.rect nul,10,10,scx-8,205
clr("255 250 246 185 1")    % ivoire
gr.rect nul,0,0,scx-20,190
cl =blk :if g[ncx,1,2]>13 then cl =red
for c=1 to 13
  gr.bitmap.crop nul, cards, (c-1)*158, cl*246, 158, 246
  gr.bitmap.scale co[c+13*(cl=red)], nul, lc, hc
  gr.bitmap.delete nul
  gr.bitmap.draw nul, co[c+13*(cl=red)], 10+(c-1)*ccx, 30
next
gr.bitmap.drawinto.end
gr.modify ThePanel, "bitmap", panl    % created before in "init"
move(ThePanel, 5, 300, 4)
gosub ArrowShow
return

makeCards:
lc =90   % 158  too big !
hc =130  % 246
x =0 : cl =blk
for c=1 to 26
  gr.bitmap.crop nul, cards, x*158, cl*246, 158, 246    % copy from origin bitmap.
  gr.bitmap.scale co[c], nul, lc, hc
  gr.bitmap.delete nul
  x++ : if x=13 then x=0 : cl =red
next
gr.bitmap.crop nul, cards, 0,   4*246, 158, 246
gr.bitmap.scale back[1], nul, lc, hc
gr.bitmap.delete nul
gr.bitmap.crop nul, cards, 158, 4*246, 158, 246
gr.bitmap.scale back[2], nul, lc, hc
gr.bitmap.delete nul
RETURN

chgcard:   % chg Spades for Clubs or Hearts for Diamonds, depend of the card touched...
if g[ncx,ncy,1]
  clr =1+(g[ncx,ncy,2]>13)  % color of this card ? 1=black, 2=red
  if clr=1
    blk =1-blk  : cl =blk   % 0 or 1
  else
    red =5-red  : cl =red   % 2 or 3
  endif
  for n=1 to 13
    gr.bitmap.delete co[n+13*(clr=2)]
    gr.bitmap.crop nul, cards, (n-1)*158, cl*246, 158, 246   % copy from origin bitmap.
    gr.bitmap.scale co[n+13*(clr=2)], nul, lc, hc
    gr.bitmap.delete nul
  next
  for j=1 to 2
    fpc =val(word$(fpo$,noc[j]))   % first place of 'j' cards
    for c=fpc to fpc+noc[j]-1
      if g[c,sp[j],1] & ((clr=1 & g[c,sp[j],2]<14) | (clr=2 & g[c,sp[j],2]>13))
        if j=2 | (j=1 & g[c,1,4]) then gr.modify g[c,sp[j],1], "bitmap", co[g[c,sp[j],2]]
      endif
    next
  next
endif
return

init:     % init the screen for a new game.
GR.CLS
gr.bitmap.draw nul, dessin, scx/2-scy/2, 0     % dessin central
gr.modify nul, "alpha", 30
gr.bitmap.draw nul, scyy1, scx-150, 30    % small yinyang
gr.modify nul, "alpha", 50
gr.bitmap.draw nul, scyy2, scx-150, 680
gr.modify nul, "alpha", 50
gr.bitmap.draw nul, scyy2, 30, 30      % small yinyang
gr.modify nul, "alpha", 50
gr.bitmap.draw nul, scyy1, 30, 680
gr.modify nul, "alpha", 50
if !hlptr
  gr.screen.to_bitmap nul
  gr.bitmap.scale hlptr,nul,scx,scy   :  gosub makeHelp
  gr.bitmap.delete nul
endif
array.fill g[],  0    % reinit the grid
array.fill noc[],0
array.fill sco[],0
array.fill pr$[],""
ccx  =lc+6   % a cell size  (the all screen grid = 13x6)
ccy  =hc+2
orx  =int((scx-13*ccx)/2)
ory  =int(scy/2)-3*ccy
win$ =""   :  ini$ =""
for ncd=1 to 26
  do : ch =int(rnd()*26)+1 : until !IS_IN(toCr$(ch),ini$)
  ini$ +=toCr$(ch)
  do : cx =int(rnd()*13)+1 : cy =int(rnd()*2)+3 :until !g[cx,cy,2]
  gr.bitmap.draw ptr, back[1+(ch>13)], orx+(cx-1)*ccx, ory+(cy-1)*ccy   % debug
!  gr.bitmap.draw ptr, co[ch], orx+(cx-1)*ccx, ory+(cy-1)*ccy
  g[ cx, cy, 1] =ptr  :  g[ cx, cy, 2] =ch
  g[ cx, cy, 3] =ch-13.5*(ch>13)  % the value to sort...
  gr.render
next

gr.bitmap.draw dbox, dbx, (scx-600)/2, -300    % the oval dialog box
clr( cl$[3] )
gr.text.align 2
gr.text.draw dbt1,scx/2,-205,""
gr.text.draw dbt2,scx/2,-150,""
gr.group gbox, dbox, dbt1, dbt2     % create the group to be move all together.

gr.text.size 48
gr.set.stroke 3
clr("100 80 80 80 2")    % gray
gr.text.draw scoptr[1], 40, 170,     int$(sco[1])   % scores
gr.text.draw scoptr[2], 40, scy-120, int$(sco[2])
clr( "255 238 139 21 2" )
gr.text.draw scoptr[3], 35, 165,     int$(sco[1])   % scores
gr.text.draw scoptr[4], 35, scy-125, int$(sco[2])

gr.text.size 38
gr.set.stroke 2
clr("100 80 80 80 1")    % gray
gr.text.draw nul, scx-112, 200, "CardWar"   % tittle
clr( cl$[4] )
gr.text.draw nul, scx-115, 196, "CardWar"
gr.text.draw nul, scx-115, scy-170, "Help"
clr( cl$[3] )
Time ,,,h$,m$
gr.text.draw hre, scx-105, 50, h$+":"+m$

gr.text.draw tcc, scx-105, scy-54, "0 %"

gr.text.size 38
gr.color 255,255,255,0,1
gr.text.draw mess, scx/2-60, 1.6*ccy, ""

gr.bitmap.draw ThePanel, scyy2, 5, -200   % the panel

clr("255 250 246 185 1")    % ivoire for the big arrow
List.Create N, lcurs
List.add lcurs, scx/2-60, ory+2*ccy+35, scx/2+40, ory+2*ccy+35, orx+6.5*ccx, ory+ccy-10
gr.poly curs, lcurs
gr.hide curs
gr.render

gr.bitmap.draw ghelp, hlptr, 0, -scy   % the Help screen
gr.set.stroke 2
RETURN

yinyang:         % bitmaps creator
for j =1 to 2
 gr.bitmap.create yy[j], 780, 780   % crée les 2 bitmaps du YinYang orange et bleu
 gr.bitmap.drawinto.start yy[j]
 call clr( cl$[2+j] )             % orange ou bleu
 gr.circle nul, 390, 390, 390
 call clr( cl$[2] )               % blanc
 gr.arc nul, 0,0,780,780,270,180,1
 gr.circle nul,390,585,195
 call clr( cl$[2+j] )            % orange ou bleu
 gr.circle nul,390,195,195
 gr.circle nul,390,585,45       % petits cercles optionnels: noir
 call clr( cl$[2] )             % blanc
 gr.circle nul,390,195,45
 gr.bitmap.drawinto.end
next
gr.bitmap.scale scyy1, yy[1], 100, 100   % yinyang à échelle réduite.
gr.bitmap.scale scyy2, yy[2], 100, 100

dlx =25
dly =60
gr.bitmap.create los, dlx, dly    % bitmap du losange
 gr.bitmap.drawinto.start los
 call clr( "255 0 0 0 1" )
 list.create N, s1
 list.add s1, dlx/2, 0
 list.add s1, dlx, dly/2
 list.add s1, dlx/2, dly
 list.add s1, 0, dly/2
 gr.poly nul, s1
gr.bitmap.drawinto.end

gr.bitmap.create dessin, scy, scy    % bitmap du dessin central
gr.bitmap.drawinto.start dessin
 gr.bitmap.draw yinyang, yy[1], 10, 10
 pas =10    %  <---  voir autres valeurs
 for angle =0 to 360-pas step pas
  dx =scy/2+350*sin(toradians(angle))
  dy =scy/2+350*cos(toradians(angle))
  gr.rotate.start -angle, dx, dy
   gr.bitmap.draw nul, los, dx-dlx/2, dy-210
   gr.circle nul, dx-23, dy-90, 13
   gr.bitmap.draw nul, los, dx-dlx/2, dy-dly/2
  gr.rotate.end
  gr.line nul, scy/2, scy/2, dx, dy     % les lignes d'angle
 next
gr.bitmap.drawinto.end

gr.text.size 36
gr.text.align 2
gr.bitmap.create dbx,600,300    %  create the oval dialog box bitmap
gr.bitmap.drawinto.start dbx
 clr("255 250 246 185 1")    % ivoire
 gr.oval nul,0,0,600,200
 gr.oval nul,20,190,280,280
 gr.oval nul,330,190,580,280
 clr( cl$[3] )
 gr.text.draw on,150,250,"Yes"
 gr.text.draw no,460,250,"No"
gr.bitmap.drawinto.end
RETURN

makeHelp:
gr.bitmap.drawinto.start hlptr
gr.color 255,255,255,0,1   % yellow
gr.text.size 40
gr.text.align 2
gr.text.draw nul, scx/2-30, 55, "•••   C A R D  W A R   •••"
gr.text.size 30
l = 45  : m = 55
gr.text.draw nul, scx/2-10, 25+2*l, "( It's a copy of the game ' CodeWar ' offered by Aat )"
gr.text.align 1
gr.text.draw nul, m, 25+3*l, "Your goal is to discover the Android cards."
gr.text.draw nul, m, 25+4*l, "Obviously Android will do the same with your cards... witout cheating !"
gr.text.draw nul, m, 25+6*l, "First of all the 2 players must pick they first 4 cards."
gr.text.draw nul, m, 25+7*l, "During the game the cards will be sorted. If there are duplicate"
gr.text.draw nul, m, 25+8*l, "value the red will always be placed to the left of the black."
gr.text.draw nul, m, 25+10*l, "At each turn the player must pick a new card from the center :"
gr.text.draw nul, m, 25+11*l, " then he must tap an opponent card to guess :"
gr.text.draw nul, m, 25+12*l, "  - if the player guess the card he can continue guessing or not !"
gr.text.draw nul, m, 25+13*l, "  - if the player DON ' T guess the card then the card he pick first"
gr.text.draw nul, m, 25+14*l, "    will be discovered. (and this card will become VERY transparent)"
gr.text.draw nul, m, 25+16*l, "End at 10 cards or all a player ' s cards discovered...      Have fun !"
gr.text.size 20
gr.text.draw nul, 420, 25+17*l, "Keep your finger on a player 's card more than 3 sec. to see some change."
gr.bitmap.drawinto.end
gr.text.size 36
return
