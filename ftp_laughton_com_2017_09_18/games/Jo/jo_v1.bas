rem Jo the robot, v1.0
rem (C) Antonis
rem Apr-May 2012

dim b[6],p[6],map[21],map2[17],map3[13],f[10],bb[3],face[6],lift[4],lf[4],prison[6],line[23]
! num,x1,y1,x2,y2 / map level [1]
array.load rc[],1,50,100,200,150,2,250,50,300,150,3,350,100,700,200~
4,100,200,150,550,5,200,200,250,350,6,300,250,700,300~
7,200,400,250,550,8,300,350,400,550,9,450,350,550,550~
10,600,350,700,550,11,100,600,200,800,12,250,600,450,650~
13,500,600,700,650,14,250,700,450,750,15,500,700,700,750~
16,250,750,300,800,17,350,750,400,800,18,550,750,600,800~
19,650,750,700,800,20,100,850,350,900,21,400,850,700,900

! map level [2]
array.load rc2[],1,100,100,200,250,2,250,100,550,150,3,600,100,700,450~
4,250,200,550,250,5,100,300,150,500,6,200,300,550,350~
7,300,400,550,450,8,100,550,150,700,9,200,400,250,700~
10,300,500,350,700,11,400,500,550,550,12,400,600,550,700~
13,600,500,700,900,14,100,750,300,800,15,350,750,550,800~
16,100,850,350,900,17,400,850,550,900

! map level [3]
array.load rc3[],1,100,100,200,300,2,250,100,550,200,3,600,100,700,300~
4,250,250,550,300,5,100,400,650,450,6,100,500,250,550~
7,350,500,400,650,8,500,500,650,550,9,100,600,150,900~
10,200,600,250,900,11,300,700,450,900,12,500,600,550,900~
13,600,600,650,900


! faces coordinates level [1]
array.load fx1[],50,150,400,50,700,300,300,750,600,750,300,550
! faces coordinates level [2]
array.load fx2[],200,150,550,300,350,550,700,700,100,800,100,900
! faces coordinates level [3]
array.load fx3[],250,200,350,350,350,650,150,700,700,650,300,900
! lifts level 1,2,3
array.load lf1[],700,700,200,650,700,100,50,250
array.load lf2[],700,750,200,800,700,150,350,150
array.load lf3[],550,800,150,800,550,100,100,50
! load bonus1 coordinates
array.load bn1[],150,250,450,800
array.load bn2[],550,750,600,50
array.load bn3[],550,200,300,450

 version$="Version 1.0 (3 levels)"
 Device dev$
 
reinput:
input "Enter your Nickname",nick$
if nick$="" then goto reinput

gr.open 255,0,25,0,0,1
gr.orientation 1
gr.text.size 39
pause 1000
gr.screen w,h
sx=w/800
sy=h/1232
gr.scale sx,sy

! load audio
audio.load beep1,"beep1.wav" % snake collision
audio.load beep2,"beep2.wav" % get a face
audio.load beep3,"beepbonus.wav" % bonus
audio.load beep4,"beeplift.wav" % lift
audio.load beep5,"beepface.wav" % face

! load boards & controls
gr.bitmap.load ct,"control.png"
gr.bitmap.load jb,"joboard.png"
gr.bitmap.load jb2,"joboard2.png"
gr.bitmap.load jb3,"joboard3.png"
!  load jo sprites
gr.bitmap.load b[1], "ar1b.png"
gr.bitmap.load b[2], "ar2b.png"
gr.bitmap.load b[3], "ar3b.png"
! load faces / level [1]?
for i=1 to 6
 gr.bitmap.load f[i], "robo"+ replace$(str$(i),".0","")+ ".png"
next i
for i=1 to 4
 gr.bitmap.load lf[i], "lift"+ replace$(str$(i),".0","")+ ".png"
next i
 ! load  snakes,bonus,prison
 gr.bitmap.load sn,"snaken1.png"
 gr.bitmap.load snb,"snaken2.png"
 gr.bitmap.load pr,"prison.png"
 gr.bitmap.load bn,"bonus.png"
 gr.bitmap.load bn1,"bonus1.png"
 decrypt "1,2,3,4,5,6","QfbMt81iCV2Cmo3UVvZTxw==",s1$

! intro screen 
gr.text.size 50
gr.color 255,0,255,255,0
gr.text.draw tex, 80,50,">>> [Jo the robot, v1.0] <<<"
gr.color 255,255,255,0,0
gr.text.draw tex, 50,100,"[Play] [Show Hall of Fame] [Quit]"
gr.color 255,255,255,255,0
for i=1 to 4
 gr.bitmap.draw bb,b[1], 80, 200+(i-1)*300
 gr.bitmap.draw bb,b[1], 380, 200+(i-1)*300
 gr.bitmap.draw bb,b[1], 680, 200+(i-1)*300
next i 
gr.text.size 40
gr.render

retouchme:
do
  gr.touch touched,x,y
  until touched
 do
   gr.touch touched,x,y
  until !touched

 ! goto play the game
  if x>0 & y>50*sy & x<180*sx & y<130*sy then 
 gr.cls
 goto begin
endif

if x>181*sx & y>50*sy & x<630*sx & y<130*sy then
! show hall of fame
 graburl hf$,"http://antonis.getenjoyment.net/hall_of_fame.txt"
 hf$=replace$(hf$,chr$(13),"")
 array.delete hfame$[] 
 Split hfame$[], hf$, chr$(10)
 array.length lhf,hfame$[]
 j=0
 for i=1 to lhf
 j=j+1
 if j=24 then
	               popup "touch screen to continue..",0,0,0	  
	               do
                     gr.touch touched,x,y
				   until touched
  for ii=1 to 23
   gr.hide line[ii]
  next ii
  j=1  
  endif
 gr.hide line[j]
 gr.text.draw line[j],10,150+j*45,hfame$[i]
 gr.render
  next i
 popup "End: touch screen to continue..",0,0,0	  
	               do
                     gr.touch touched,x,y
				   until touched
for ii=1 to 23
   gr.hide line[ii]
  next ii	
 gr.render  
 endif

! Quit Game
if x>631*sx & y>50*sy & x<800*sx & y<130*sy then end
  
goto retouchme 

begin:

gr.render
level=0
lives=3   % jo lives
points=0 % points
s=2    % speed of jo
clok=clock()

! next level

nexlevel:
level=level+1
points=points+200
gr.text.size 39 
gr.color 255,255,0,0,1
gr.set.stroke 2


! initialize
i=1  % sprite num of jo, don't touch!
x1=50 % start position x of jo
y1=50  % start position y of jo
xx1=700 % start position x of snake
yy1=900  % start position y of snake
rot=0     % rotation of snake 0|1
caught=0   %  faces collected
xx2=700     % start position x of snakeb
yy2=100      % start position y of snakeb
freerider=0   % don't pass through bricks

! draw score & level
scor$=replace$(str$(points),".0","")
gr.text.draw score,5,1040,"Points: "+scor$
level$=replace$(str$(level),".0","")
gr.text.draw levl,5,1090,"Player: "+nick$+"  "+"Level: "+level$

! draw map rects
if level=1 then 
 for ii=1 to 21
  gr.rect map[ii], rc[ii*5-3]+1,rc[ii*5-2]+1,rc[ii*5-1]-1,rc[ii*5]-1
 next ii
 elseif level=2
 for ii=1 to 17
  gr.rect map2[ii], rc2[ii*5-3]+1,rc2[ii*5-2]+1,rc2[ii*5-1]-1,rc2[ii*5]-1
 next ii
 else
 for ii=1 to 13
  gr.rect map3[ii], rc3[ii*5-3]+1,rc3[ii*5-2]+1,rc3[ii*5-1]-1,rc3[ii*5]-1
 next ii
endif
! draw board
if level=1 then gr.bitmap.draw jo,jb,0,0 
if level=2 then gr.bitmap.draw jo2,jb2,0,0
if level=3 then gr.bitmap.draw jo3,jb3,0,0

! draw lives
for ii=1 to lives
 gr.bitmap.draw bb[ii],b[1],+60*ii,1180
next ii 
! draw controls
gr.bitmap.draw con,ct,500,1010

! draw faces & bonus
if level=1 then
gr.bitmap.draw bonus,bn,300,300
gr.bitmap.draw bonus1,bn1,bn1[1],bn1[2]
gr.bitmap.draw bonus2,bn1,bn1[3],bn1[4]
for ii=1 to 6
 gr.bitmap.draw face[ii],f[ii],fx1[2*(ii-1)+1],fx1[2*(ii-1)+2]
 gr.bitmap.draw prison[ii],pr,fx1[2*(ii-1)+1],fx1[2*(ii-1)+2]
next ii
endif
if level=2 then
gr.bitmap.draw bonus,bn,300,250
gr.bitmap.draw bonus1,bn1,bn2[1],bn2[2]
gr.bitmap.draw bonus2,bn1,bn2[3],bn2[4]
for ii=1 to 6 
 gr.bitmap.draw face[ii],f[ii],fx2[2*(ii-1)+1],fx2[2*(ii-1)+2]
 gr.bitmap.draw prison[ii],pr,fx2[2*(ii-1)+1],fx2[2*(ii-1)+2]
next ii
endif
if level=3 then
gr.bitmap.draw bonus1,bn1,bn3[1],bn3[2]
gr.bitmap.draw bonus2,bn1,bn3[3],bn3[4]
gr.bitmap.draw bonus,bn,300,300
for ii=1 to 6 
 gr.bitmap.draw face[ii],f[ii],fx3[2*(ii-1)+1],fx3[2*(ii-1)+2]
 gr.bitmap.draw prison[ii],pr,fx3[2*(ii-1)+1],fx3[2*(ii-1)+2]
next ii
endif

! draw lifts
if level=1 then
for ii=1 to 4 
 gr.bitmap.draw lift[ii],lf[ii],lf1[2*(ii-1)+1],lf1[2*(ii-1)+2]
next ii
endif
if level=2 then
for ii=1 to 4 
 gr.bitmap.draw lift[ii],lf[ii],lf2[2*(ii-1)+1],lf2[2*(ii-1)+2]
next ii
endif
if level=3 then
for ii=1 to 4 
 gr.bitmap.draw lift[ii],lf[ii],lf3[2*(ii-1)+1],lf3[2*(ii-1)+2]
next ii
endif

! draw snakes somewhere
 gr.bitmap.draw snake,sn,700,700
 gr.bitmap.draw snakeb,snb,700,700
 
! draw jo sprites
for ii=1 to 3
 gr.bitmap.draw p[ii],b[ii],50,50
next ii
gr.hide p[2]
gr.hide p[3]

gr.render
xflag=0
t=clock()

do
if clock()-t>3000 & freerider=0
  xflag=!xflag
  if xflag=0 then gr.hide bonus else gr.show bonus
  t=clock()
endif
gosub snakecollision
 rot=!rot
! if rot=1 then 
 if xx1>52 then xx1=xx1-2 else xx1=650
 if yy1>52 then yy1=yy1-2 else yy1=900
 gr.modify snake,"x",xx1
 gr.modify snake,"y",yy1
! else
 if xx2>52 then xx2=xx2-2 else xx2=600
 if yy2<898 then yy2=yy2+2 else yy2=100
 gr.modify snakeb,"x",xx2
 gr.modify snakeb,"y",yy2
! endif
 
 gr.touch touched,x,y
 
 if  touched>0  then % move, first one
 
  ! up arrow
 if x>600*sx & x<700*sx & y>1010*sy & y<1080*sy & y1>50 then 
   y1=y1-s
   gr.modify p[i],"y",y1 
   if freerider=0 then  
   gosub check
   if ok=0 then 
     y1=y1+s 
     gr.modify p[i],"y",y1 
   endif	
  endif
 endif 
 ! down arrow
 if x>600*sx & x<700*sx & y>1160*sy & y1<900 then
   y1=y1+s
   gr.modify p[i],"y",y1  
   if freerider=0 then
   gosub check    
   if ok=0 then 
    y1=y1-s
	  gr.modify p[i],"y",y1 
	 endif
  endif
 endif
 ! left arrow
 if x>500*sx & x<590*sx & y>1080*sy & y<1150*sy & x1>50 then 
   x1=x1-s
   gr.modify p[i],"x",x1  
   if freerider=0 then
   gosub check
   if ok=0 then 
     x1=x1+s
	 gr.modify p[i],"x",x1  
	 endif
   endif
 endif 
 ! right arrow
 if x>720*sx & x<800*sx & y>1080*sy & y<1150*sy & x1<700 then
   x1=x1+s
   gr.modify p[i],"x",x1  
   if freerider=0 then
   gosub check
   if ok=0 then 
     x1=x1-s
	 gr.modify p[i],"x",x1  
	 endif
   endif
 endif 

 if x>591*sx & x<719*sx & y>1081*sy & y<1149*sy then 
  gosub catch
  gosub checklift
  gosub checkbonus1_2
  if freerider=0 then gosub checkbigface
 endif

! if ok>0 | freerider then  jo must move
 if ok>0 | freerider=1 then 
  i=i+1 
  if i=4 then i=1
  if i=3 then
  gr.show p[3]
  gr.hide p[1]
  gr.hide p[2]
  elseif i=2 then
  gr.show p[2]
  gr.hide p[1]
  gr.hide p[3]
  else 
  gr.show p[1]
  gr.hide p[2]
  gr.hide p[3]
 endif
 
 gr.modify p[i],"x",x1  
 gr.modify p[i],"y",y1  

 endif 
 
endif % touched

gr.render
until 0  % first one

end

! check for collision with map 
check:
a1=0
if level=1 then
for l=1 to 21
  a1=gr_collision(p[i],map[l]) 
  if a1>0 then 
    ok=0
	return
  endif	
 next l
ok=1
endif

if level=2 then
 for l=1 to 17
  a1=gr_collision(p[i],map2[l]) 
  if a1>0 then 
    ok=0
	return
  endif	
 next l
ok=1
endif

if level=3 then
 for l=1 to 13
  a1=gr_collision(p[i],map3[l]) 
  if a1>0 then 
    ok=0
	return
  endif	
 next l
ok=1
endif

return

! get a face 
catch:
for l=1 to 6
b1=gr_collision(p[i],face[l])
if b1=1 then
  audio.stop
  audio.play beep2
  gr.modify face[l],"x",(l-1)*53+3
  gr.modify face[l],"y",1110
  gr.hide prison[l]
  gr.render
  points=points+50
  caught=caught+1
  gosub showscore
  
  if caught=6 & level=1 then
   popup "Get ready..",0,0,4
   pause 4000
   gr.cls
   goto nexlevel
  endif
  if caught=6 & level=2 then
    popup "Get ready..",0,0,4
    pause 4000
    gr.cls
    goto nexlevel
  endif
  if caught=6 & level=3 then
    timbonus=900-round((clock()-clok)/1000) % 15 min to take timebonus
    decrypt "123456789","EyXsfgw0Zts=",s$
	if timbonus<0 then timbonus=0
	points=points+timbonus
	gosub showscore
    tone 800,1000
	popup "You entered The Hall Of Fame!",0,0,2
	pause 2000
	if timbonus>0 then 
	 popup "Time Bonus: "+replace$(str$(timbonus),".0",""),0,0,2
	 pause 2000
	endif  
	gosub writescore
	end
  endif
endif
next l

return
 
snakecollision:

 b1=gr_collision(p[i],snake)
 b2=gr_collision(p[i],snakeb)
 if b1=1 | b2=1 then
   if b1=1 then
     xx1=650
     yy1=850
     gr.modify snake,"x",xx1
     gr.modify snake,"y",yy1
     gr.render
   endif
   audio.stop
   audio.play beep1
   pause 1500
   x1=50
   y1=50  
   gr.modify p[i],"x",x1
   gr.modify p[i],"y",y1
     
   gr.hide bb[lives]
   gr.render
   lives=lives-1
   points=points-50
   gosub showscore
   if lives=0 then
    tone 800,800
	popup "End of game",0,0,4
	pause 4000
	end
   endif
  endif
return
 
checklift:
 for m=1 to 4
  b1=gr_collision(p[i],lift[m])
  if b1=1 then
  audio.stop
  audio.play beep4
  points=points-10
  gosub showscore
    if m=1 then
      x1=50
      y1=50
    endif
   if m=2 then
      x1=700
      y1=50
    endif
   if m=3 then
      x1=50
      y1=900
    endif
   if m=4 then
      x1=700
      y1=900
    endif
   !gr.modify p[i],"x",x1
   !gr.modify p[i],"y",y1
  endif
 next m
return

! check collision with big face
checkbigface:
  b1=gr_collision(p[i],bonus)
  if b1=1 then 
  audio.stop
  audio.play beep3
  freerider=1
  points=points+100  
  gosub showscore
  gr.modify bonus,"x",320
  gr.modify bonus,"y",1180
  else 
  freerider=0
  endif
return

checkbonus1_2:
  b1=gr_collision(p[i],bonus1)
  b2=gr_collision(p[i],bonus2)
  if b1=1 then 
   audio.stop
   audio.play beep5
   points=points+100  
   gosub showscore
   gr.modify bonus1,"x",380
   gr.modify bonus1,"y",1180
  endif
  if b2=1 then 
   points=points+100  
   gosub showscore
   gr.modify bonus2,"x",440
   gr.modify bonus2,"y",1180
  endif
return

writescore:
! cl=clock()
popup "Updating score..",0,0,0
ftp.open "antonis.getenjoyment.net",21,s1$,s$
ftp.get "/antonis.getenjoyment.net/hall_of_fame.txt","hall_of_fame.txt"
Time Year$, Month$, Day$, Hour$, Minute$, Second$
text.open a,filn3,"hall_of_fame.txt"
text.writeln filn3,"User={"+nick$+"}"
text.writeln filn3,version$
text.writeln filn3,"Points="+replace$(str$(points),".0","")
text.writeln filn3,"Date="+year$+"|"+month$+"|"+day$+"|"+hour$+":"+minute$
text.writeln filn3,"[Device]"
text.writeln filn3,Dev$
text.writeln filn3,"--------------------"
text.close filn3 
ftp.put "hall_of_fame.txt","/antonis.getenjoyment.net/hall_of_fame.txt"
ftp.close
popup "Thanks for playing!",0,0,0
! print ceil((clock()-cl)/1000)
return
 
showscore:
 scor$=replace$(str$(points),".0","")
 gr.modify score,"text","Points: "+scor$
 gr.render
return

OnError:
ftp.close
popup "Terminated...",0,0,0
end
 
