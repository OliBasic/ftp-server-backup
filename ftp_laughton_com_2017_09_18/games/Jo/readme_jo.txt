[Jo the robot, v1.0]
[(C) Antonis ]
[Apr.-May 2012]

Jo is a very kind & good robot, but its friends are imprisoned!
Help it to free its friends and you will enter Jo's Hall of Fame!

[The Homepage}
Jo has an internet homepage where scores of those who finished the
games are logged: 
http://antonis.getenjoyment.net/
http://antonis.getenjoyment.net/hall_of_fame.txt
If you finish the game you enter Jo's Hall of Fame and your
nickname, points, time, device model, will be stored on Jo's homepage.

[The Game]
Use controls to move around the screen. Use mid control to free robots
or get something else.
There are 2 tri-snakes flying around trying to harm you...
Use the 4 lifts(arrows) to quickly get transferred to an opposite corner.
Get the big happy face to pass through walls. It's visible every 5 seconds.
Get the Bonus(B icon) to win more points.
There are 3 levels (for now!).
You have 3 lives.
Freeing up the 5 robots gets you to the next level.

[Points]
You get:
+200 points for every level
+50 points for the happy face
-10 for using the lift
-50 when losing a life
+100 for the bonus icon
if you finish the game within 15 mins you get an extra time bonus

All bitmaps & sounds are homemade ;)
