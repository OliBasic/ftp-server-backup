Rem Guess the Number
Rem October 2014
Rem Version 1.00
Rem By Roy
Rem

console.title"Guess the Number"
Rem Open Graphics
di_width = 800 % set to my tablet
di_height = 1280

gr.open 255,0,0,0 % Black
gr.orientation 1
pause 1000
gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y
!gr.text.typeface 1
gr.text.size 30
wakelock 3
Rem Globel Vars
array.load guessNum$[],"Ten","Nine","Eight","Seven","Six","Five","Four","Three","Two","One"
dim trys[1]

Rem Start of Functions

fn.def Gcol(c,g)
	Rem g=0 Outline and g=1 Fill
	if c=0 then gr.color 255,0,0,0,g % black
	if c=1 then gr.color 255,255,0,0,g % Red
	if c=2 then gr.color 255,0,255,0,g % green
	if c=3 then gr.color 255,0,0,255,g % blue
	if c=4 then gr.color 255,255,255,0,g % yellow
	if c=5 then gr.color 255,0,255,255,g % cyan
	if c=6 then gr.color 255,255,0,255,g % magenta
	if c=7 then gr.color 255,192,192,192,g % gray
	if c=8 then gr.color 255,255,255,255,g % white
	if c=9 then gr.color 255,92,92,92,g % dark gray
fn.end

fn.def CurvyWindow(x,y,l,h,c)
	call Gcol(c,1)
	gr.rect r,x,y+25,l,h-25 
	gr.rect r,x+25,y,l-25,h 
	gr.circle c, x+25,y+25,25 
	gr.circle c,l-25,y+25,25 
	gr.circle c,x+25,h-25,25
	gr.circle c,l-25,h-25,25
	gr.render
fn.end

fn.def SetUp(di_width,di_height,trys[],guess$)
	gr.cls
	call CurvyWindow(1,1,di_width,di_height,8)
	call CurvyWindow(4,4,di_width-3,di_height-3,2)
	gr.text.size 50
	g$="Guess The number"
	gr.text.width w,g$
	call ThreeDtext(0,1,di_width/2-w/2,50,g$)
	gr.text.size 30
	gr.text.align 2
	call Gcol(3,0)
	gr.text.draw trys[1],400,100,guess$
	gr.text.align 1
	gr.render
fn.end

fn.def ThreeDtext(c1,c2,x,y,t$)
	gr.text.bold 1
	call Gcol(c1,1)
	gr.text.draw d,x,y,t$
	call Gcol(c2,1)
	gr.text.draw d,x+2,y-2,t$
	gr.text.bold 0
	gr.render
fn.end

fn.def DrawNumbersKeyboard()
	num=1
	call Gcol(0,0)
	gr.set.stroke 2
	for y=1050 to 1150 step 100
		for x=50 to 550 step 100
			if x<550 then 
				call Gcol(0,1)
				gr.rect r,x,y,x+100,y+100
				call Gcol(8,0)
				gr.rect r,x,y,x+100,y+100
				call Gcol(8,1)
				gr.text.draw d,x+43,y+60,int$(num)
				num++
				if num=10 then num=0
			else
				call Gcol(0,1)
				gr.rect r,x,y,x+200,y+100
				call Gcol(8,0)
				gr.rect r,x,y,x+200,y+100
				call Gcol(8,1)
				if y=1050 then 
					gr.text.draw d,x+30,y+60,"Backspace"
				else
					gr.text.draw d,x+43,y+60,"Enter"
				endif
			endif
		next
	next
	Rem TextBox
	call Gcol(7,1)
	gr.rect r,300,950,500,1000
	call Gcol(0,0)
	gr.rect r,300,950,500,1000
	Rem Exit and Help Button
	call CurvyWindow(98,948,252,1002,0)
	call CurvyWindow(100,950,250,1000,4) %Exit
	
	call CurvyWindow(548,948,702,1002,0)
	call CurvyWindow(550,950,700,1000,4) %Help
	call Gcol(0,1)
	gr.text.draw d,150,985,"Exit"
	gr.text.draw d,595,985,"Help"
	gr.render
fn.end

fn.def GuessesLeft(GuessNum$[],trys[],guess$,guess)
	if guess<10 then
		guess$="You have "+guessNum$[guess]+" guesses left"
	else
		guess$="You have just "+guessNum$[guess]+" guess left"
	endif
	gr.modify trys[1],"text",guess$
	gr.render
	guess++
fn.end

fn.def Cursor(x,y)
		call Gcol(0,1)
		gr.text.draw d,x,y+1,"_"
		gr.render
		pause 50
		call Gcol(7,1)
		gr.text.draw d,x,y+3,chr$(9602)
		gr.render
		pause 50
fn.end

fn.def Help()
	do
		Dialog.Message "Help:",~
		"I've thought of a number between 1 and 100. " + ~
		"You have to try and guess it in 10 or less guesses." + ~
		" I will give useful advice after each guess!.", ~
		ok, "OK"
		if ok=0 then tone 300,300
	until ok=1
fn.end

fn.def Quit()
	do
		Dialog.Message "Exit:", "Are you sure you want to leave the Game", yn, "Yes","No"
		if yn=0 then tone 300,300
	until yn=1 | yn=2
	if yn=1 then 
		gr.close
		wakelock 5
		exit"Thanks for Playing Guess the Number"
	endif
fn.end

fn.def DoError()
	tone 500,100
	do
		Dialog.Message "Error:", "Please Enter a Number Between 1 and 100", ok, "OK"
		if ok=0 then tone 300,300
	until ok=1
fn.end

fn.def PlaySound(mySound)
	Rem Number found
	if mySound = 1 then
		  for s=500 to 1000 step 200
			tone s,100
		  next
	endif

	Rem Number not found
	if mySound = 2 then
      for s=550 to 200 step -50
        tone s,100
      next
	endif
fn.end

fn.def GrayKey(x,y,c,col)
	call Gcol(col,1)
	gr.rect r,x+2,y+2,x+98,y+98
	call Gcol(8,1)
	gr.text.draw d,x+43,y+60,int$(c)
fn.end

fn.def GrayBackkey(x,y,col)
	call Gcol(col,1)
	gr.rect r,x+2,y+2,x+198,y+98
	call Gcol(8,1)
	gr.text.draw d,x+30,y+60,"Backspace"
	gr.render
fn.end

fn.def GrayKeyEnter(x,y,col)
	call Gcol(col,1)
	gr.rect r,x+2,y+2,x+198,y+98
	call Gcol(8,1)
	gr.text.draw d,x+43,y+60,"Enter"
	gr.render
fn.end

fn.def GetNumber(scale_x,scale_y,numLen)
	num$=""
	enter=0
	c=1
	call Gcol(0,1)
	gr.text.draw t,305,985,num$
	gr.render
	col=7
	do
		do
			Gr.text.width w,num$
			call Cursor(305+w,985)
			gr.touch touched,tx,ty 
		until touched 
		if tx>550*scale_x & ty>950*scale_y & tx<700*scale_x & ty<1000*scale_y then call Help()

		if tx>100*scale_x & ty>950*scale_y & tx<250*scale_x & ty<1000*scale_y then call Quit()
		c=1
		for y=1050 to 1150 step 100
			for x=50 to 450 step 100
				if ty>y*scale_y & ty< (y+100)*scale_y then 
					if tx>x*scale_x & tx< (x+100)*scale_x then 
						if c=10 then c=0
						if len(num$)<numLen then 
							num$+=int$(c)
							gr.modify t,"text",num$
							call GrayKey(x,y,c,9)
							gr.render
							tone 300,100
							pause 10
							call GrayKey(x,y,c,0)
						endif
					endif
				endif
				c++
			next 
		next
		Rem Backspace
		if ty>1050*scale_y & ty<1150*scale_y then 
			if tx>550*scale_x & tx<750*scale_x then 
				num$=left$(num$,len(num$)-1)
				gr.modify t,"text",num$
				call GrayBackkey(550,1050,9)
				gr.render
				tone 300,100
				pause 10
				call GrayBackkey(550,1050,0)
			endif 
		endif
		Rem Enter
		if ty>1150*scale_y & ty<1250*scale_y then 
			if tx>550*scale_x & tx<750*scale_x then 
				call GrayKeyEnter(550,1150,9)
				if num$="" then 
					call DoError()
				elseif num$<>"" & val(num$)>100 then 
					call DoError()
				elseif num$<>"" & val(num$)<1 then 
					call DoError()
				else
					enter=1
					tone 300,100
				endif
				call GrayKeyEnter(550,1150,0)
			endif 
		endif
	until enter
	n=val(num$)
	num$=""
	gr.modify t,"text",num$
	fn.rtn n
fn.end

fn.def ShowResults(Yplace,androidNum,playerNum,di_width,guess)
	if playerNum>androidNum then g$=int$(playerNum)+" Is to Hight"
	if playerNum<androidNum then g$=int$(playerNum)+" Is to Low"
	if playerNum=androidNum then 
		g$=int$(playerNum)+" Is the Right Guess. Well Done"
		call PlaySound(1)
	endif
	if playerNum<>androidNum & guess=11 then 
		g$="You didn't guess the Number, it was: "+int$(androidNum)
		call PlaySound(2)
	endif
	call Gcol(0,1)
	gr.text.width w,g$
	gr.text.draw d,di_width/2-w/2,Yplace,g$
	gr.render
	Yplace+=50
fn.end

Rem End of Functions

Rem Main
do
	guess=1
	Yplace=150
	playerNum=0
	call SetUp(di_width,di_height,trys[],guess$)
	call DrawNumbersKeyboard()
	androidNum=int(100 * rnd()+1)
	!gr.text.draw d,50,50,int$(androidNum)
		do
			if playerNum<>androidNum then GuessesLeft(guessNum$[],trys[],guess$,&guess)
			playerNum=GetNumber(scale_x,scale_y,3)
			call ShowResults(&Yplace,androidNum,playerNum,di_width,guess)
		until playerNum=androidNum | guess=11
		pause 5000
		do
			Dialog.Message , "Another Game?" , yn, "Yes", "No"
			if yn=0 then tone 300,300
		until yn=1 | yn=2
		pause 100
until yn=2

onBackKey:

wakelock 5

!OnError:

exit"Thanks for Playing Guess the Number"
