! By gege_95c & Cassiope34
! game / jeu Atome (version 1.0)

FN.DEF stg$(c)
 FN.RTN LEFT$(STR$(c),IS_IN(".",STR$(c))-1)
FN.END

Fn.def lang$()
SYSTEM.OPEN
SYSTEM.WRITE "getprop"
PAUSE 100
DO
  SYSTEM.READ.LINE l$
  SYSTEM.READ.READY ready
  fd= is_in("language",l$)
UNTIL !ready | fd
SYSTEM.CLOSE
if fd then Fn.rtn mid$(word$(l$,2,":"),3,2)
Fn.end

GR.OPEN 255,255,255,255,1,0

! global var
DIM cell[14,12,2]  % cell[x,y,atome]
DIM atome[17]  % 17 formes atomes
DIM mur[5]     % 5 choix de murs
DIM murPtr[168]
DIM lang$[2]   % words in diff. languages
DIM BGDcol$[6] % diff. background colors
DIM score[30]  % number of move in a level.
DIM l$[12]     % number of line
dim skin[3]    % 3 skin color
dim skinPtr[3]
dim skin2[3]  % skin color -
dim skin2Ptr[3]
dim skin3[3]  % skin color +
dim skin3Ptr[3]
dim mur2[5]   % skin mur - 
dim mur2Ptr[5]
dim mur3[5]   % skin mur +
dim mur3Ptr[5]
DIM Ison[2]   % icône son
DIM Isonptr[2]
DIM son[2]    % état du son
dim tt$[9] 
dim ttPtr[9]
dim uxSel[200]
dim uySel[200]
dim udx[200]
dim udy[200]
dim utori[200]

ARRAY.LOAD dix[],1,0,-1,0  % 1 Est  2 Sud 
ARRAY.LOAD diy[],0,1,0,-1  % 3 Ouest 4 Nord

! constants
path$ = "Atome/"
dcell =  56 % cellule size
   ox = 282 % left border
   oy =  32 % top border 28

! si lang$()="fr" => 2=French sinon 1=English
langage = 2-1*(lang$()<>"fr")

lang$[1] = "Wall,Level,Sound,Reset,action(s),Skin,  Well done !  the molecule is ok ...  "
lang$[2] = "Mur,Niveau,Son,RAZ,action(s),Fond,  Bravo !  la molecule est conforme...  "
BGDcol$[1] = "0 128 0 124 250 168"    % 2 color
BGDcol$[2] = "26 102 118 116 210 232" % of the
BGDcol$[3] = "250 138 2 250 218 92"   % Backgd

! init variables
wall  = 1  % type de mur 1 à 5
son   = 1  % sound on/off
BGcol = 1  % colors of Backgroung
Level = 1
SavedGame = 0
chgBkgd = 0   % indicateur chgt couleur de fond.

!  sounds !
soundpool.open 3
soundpool.load appl,path$+"Applause.wav"
soundpool.load boing,path$+"Boing.wav"

!  load all levels
GRABFILE LdLvl$, path$+"AtomeLevels.txt"
SPLIT Levels$[], LdLvl$, "\n"    % CHR$(10)  end of line as separator.

GOSUB loadGame  % set SavedGame=1 if game restored

! LOAD AND SCALE GRAPHICS !

GR.SCREEN real_w, real_h
di_w = 26*dcell - ox
di_h = 13*dcell - oy
scale_w = real_w/di_w
scale_h = real_h/di_h
GR.SCALE scale_w, scale_h

! create bitmaps

GR.BITMAP.LOAD tmpBmp,path$+"atome.png"

! Son 
GR.BITMAP.CROP son[1],tmpBmp,464,dcell,dcell,dcell
GR.BITMAP.CROP son[2],tmpBmp,464+dcell,dcell,dcell,dcell

! mur
FOR wal=1 TO 5
 GR.BITMAP.CROP mur[wal],tmpBmp,(wal-1)*dcell,dcell,dcell,dcell
 GR.BITMAP.CROP mur2[wal],tmpBmp,(wal-1)*dcell,dcell,dcell,dcell
 GR.BITMAP.CROP mur3[wal],tmpBmp,(wal-1)*dcell,dcell,dcell,dcell
NEXT

! atomes
FOR a=1 TO 17
 GR.BITMAP.CROP atome[a],tmpBmp,(a-1)*dcell,1,dcell-1,dcell-1
NEXT

! dessin @tome (gauche)
GR.BITMAP.CROP icone,tmpBmp,12.35*dcell,dcell,4. 68*dcell,1.35*dcell

! menu icones 6 7 8 9 10
GR.BITMAP.CROP Iexit,tmpBmp,5*dcell,dcell,1.2*dcell,1.2*dcell
GR.BITMAP.CROP Ison[2],tmpBmp,5*dcell+65,dcell,1.2 *dcell,1.2*dcell
GR.BITMAP.CROP Ison[1],tmpBmp,5*dcell+130,dcell,1.2*dcell,1.2*dcell
GR.BITMAP.CROP Info,tmpBmp,5*dcell+196,dcell,1.2*dcell-3,1.2*dcell
GR.BITMAP.CROP Iundo,tmpBmp,5*dcell+260,dcell,1.2*dcell,1.2*dcell
GR.BITMAP.CROP Iraz,tmpBmp,5*dcell+325,dcell,1.2*dcell,1.2*dcell

! original bitmap is not useful -> free memory
GR.BITMAP.DELETE tmpBmp

! new level
New:
und = 0  % raz ou nvelle partie
FOR n=1 TO 12
 l$[n] = WORD$(Levels$[Level],n+1,",")
NEXT
levelName$ = WORD$(Levels$[Level],17,",")
mol$ = WORD$(Levels$[Level],18,",")
top  = val(word$(Levels$[Level],19,","))
IF !SavedGame THEN moved = 0

! the screen
GR.CLS

gosub bgd % 123
GR.RECT nul,0,0,real_w, real_h

! sauvegarde couleur actuelle
BGcol2 = BGcol

! icone coul -1
BGcol = BGcol-1*(BGcol>1)+2*(BGcol=1)
gosub bgd % 123
GR.RECT nul, 0.1*dcell,7.9*dcell,1.1*dcell,8.9*dcell

gosub grid % 456
GR.RECT nul, 0.3*dcell,8.1*dcell,0.9*dcell,8.7*dcell

! icone coul +2
BGcol = 3*(BGcol=1)+1*(BGcol=2)+2*(BGcol=3)
gosub bgd % 123
GR.RECT nul, 3.9*dcell,7.9*dcell,4.9*dcell,8.9*dcell

gosub grid % 456
GR.RECT nul, 4.1*dcell,8.1*dcell,4.7*dcell,8.7*dcell

! retour couleur actuelle
BGcol = BGcol2
GR.COLOR 255,255,255,255,1   % text color white
GR.TEXT.SIZE 30
GR.TEXT.ALIGN 2

! ajout 2 traits
GR.line nul,0.1*dcell,5.2*dcell,4.9*dcell,5.2*dcell
GR.line nul,0.1*dcell,9.1*dcell,4.9*dcell,9.1*dcell

          ! menu partie droite Ecran
! exit
GR.BITMAP.DRAW IexitPtr,Iexit,ox+14.5*dcell,1.2*dcell

! Raz
GR.BITMAP.DRAW IrazPtr,Iraz,ox+14.5*dcell,2.9*dcell

! son
GR.BITMAP.DRAW IsonPtr[1],Ison[1],ox+14.5*dcell,4.5*dcell
GR.BITMAP.DRAW IsonPtr[2],Ison[2],ox+14.5*dcell,4.5*dcell
gr.hide IsonPtr[2-son]

! info jeu mode emploi
GR.BITMAP.DRAW InfoPtr,Info,ox+14.5*dcell,6.1*dcell

! undo
GR.BITMAP.DRAW IundoPtr,Iundo,ox+14.5*dcell,7.7*dcell % 4.5 6.1 7.7

! date et heure
Time time(),Year$, Month$, Day$, Hour$, Minute$,, dow
GR.TEXT.DRAW IhmnPtr,ox+15*dcell,10*dcell, hour$+":"+minute$
days$= "Sun Mon Tue Wed Thu Fri Sat Dim Lun Mar Mer Jeu Ven Sam"
GR.TEXT.DRAW IdatePtr,ox+15*dcell,11*dcell, word$(days$,dow+7*(langage-1))
GR.TEXT.DRAW IanPtr,ox+15*dcell,12*dcell, day$+"/"+month$

     ! Infos partie gauche écran
       


! nom + formule molecule
GR.TEXT.DRAW molPtr,2.6*dcell,4.7*dcell,levelName$ +" - "+mol$ % +")"

! icone chgt. - niveau +
GR.RECT nul, 0.1*dcell,5.5*dcell,1.1*dcell,6.5*dcell
GR.RECT nul, 3.9*dcell,5.5*dcell,4.9*dcell,6.5*dcell
GR.TEXT.DRAW namePtr,2.5*dcell, 6.1*dcell,WORD$(lang$[langage],2,",")+" "+stg$(level)

GR.COLOR 255,0,0,0,1   % text color: black
GR.TEXT.SIZE 65
GR.TEXT.DRAW nul,0.6*dcell,6.4*dcell,"-"
GR.TEXT.SIZE 45
GR.TEXT.DRAW nul,4.4*dcell,6.3*dcell,"+"

! couleur affichage
GR.COLOR 255,255,255,255,1   % text color: white
GR.TEXT.SIZE 30

! affiche mur -1
GR.BITMAP.DRAW murPtr[wall-1*(wall>1)+4*(wall=1)],mur[wall-1*(wall>1)+4*(wall=1)],0.1*dcell,6.7*dcell

! affiche <<mur/wall>>
GR.TEXT.DRAW skinPtr,2.5*dcell,7.3*dcell, WORD$(lang$[langage],1,",")

! affiche mur +1
GR.BITMAP.DRAW murPtr[1+wall*(wall<5)],mur[ 1+wall*(wall<5)],3.9*dcell,6.7*dcell

! affiche <<fond/skin>>
GR.TEXT.DRAW nul,2.5*dcell,8.6*dcell,WORD$(lang$[langage],6,",")

! affiche dessin @tome
GR.BITMAP.DRAW iconePtr,icone,6,9.4*dcell

! affiche Move(s) / Best / Top
GR.TEXT.DRAW scorePtr,2.4*dcell,11.4*dcell, "Score / Best / Top"

! affiche nb deplacements / Meilleur / Top
GR.TEXT.DRAW mov1Ptr,1*dcell,12.1*dcell,stg$(moved)
gr.text.draw mov2Ptr,2.7*dcell,12.1*dcell,stg$(score[Level])
gr.text.draw nul,4.1*dcell,12.1*dcell,stg$(top)

! grid color 456
gosub grid
GR.RECT nul, ox, oy, ox+14*dcell, oy+12*dcell

! background color 123
gosub bgd

w=0
FOR y=1 TO 12
 FOR x=1 TO 14
  ll = IS_IN(MID$(l$[y],x,1),"BCDEFGHIJKLMNOPQR12")
  IF ll = 19  % external
   GR.RECT nul, ox+(x-1)*dcell-1, oy+(y-1)*dcell-1, ox+x*dcell+2, oy+y*dcell+2
  ELSEIF ll = 18  % wall
   w++
   GR.BITMAP.DRAW murPtr[w],mur[wall],ox+(x-1)*dcell,oy+(y-1)*dcell
  ELSE
   IF SavedGame | chgBkgd THEN ll= cell[x,y,1]
   IF ll
    GR.BITMAP.DRAW atomePtr,atome[ll],ox+(x-1)*dcell,oy+(y-1)*dcell
    cell[x,y,2] = atomePtr
   ENDIF
  ENDIF
  IF !SavedGame & !chgBkgd THEN cell[x,y,1] = ll
 NEXT
NEXT

! draw molecule
if molPtr then gr.bitmap.delete molPtr
gr.bitmap.create molPtr, 5*dcell, 4*dcell
gr.bitmap.drawinto.start molPtr
lenMol= LEN(WORD$(Levels$[Level],15,","))
offset= FLOOR((ox-lenMol*dcell)/2)-1
FOR ym=1 TO 3
 wrd$= WORD$(Levels$[Level],13+ym,",")
 IF !IS_IN("X",wrd$)
  ltm=LEN(wrd$)
  FOR xm=1 TO ltm
   llm =IS_IN(MID$(wrd$,xm,1),"BCDEFGHIJKLMNOPQR")
   IF llm THEN GR.BITMAP.DRAW molecPtr,atome[llm],offset+(xm-1)*dcell,ym*dcell
  NEXT
 ENDIF
NEXT
gr.bitmap.drawinto.end
gr.bitmap.draw mPtr, molPtr, 0, 0

gosub initinfo

!GR.RENDER
SavedGame = 0
chgBkgd = 0

DO
  DO
    GR.TOUCH touched,ttx,tty
    PAUSE 5
    Time ,,, Hour$, Minute$,
    GR.modify IhmnPtr, "text",hour$+":"+minute$
    gr.render
   UNTIL touched
   if deltaX then GOSUB RestoreMol
  ttx/=scale_w
  tty/=scale_h
  xSel=FLOOR((ttx-ox)/dcell)+1
  ySel=FLOOR((tty-oy)/dcell)+1
  deltaX=0
  IF xSel>0 & xSel<15 & ySel>0 & ySel<13
    IF !cell[xSel,ySel,1] | cell[xSel,ySel,1]>17 THEN xSel=0  % it's not an atome
  ELSEIF xSel<=0 & ySel<4    % touch the molecule
    deltaX=ttx
    deltaY=tty
  ENDIF
   DO
     GR.TOUCH touched,tx,ty
     tx/=scale_w
     ty/=scale_h
     if deltaX   % the molecule follow the finger
       gr.modify mPtr, "x", tx-deltaX
       gr.modify mPtr, "y", ty-deltaY
       gr.render
     endif
   UNTIL !touched

 IF tx>ox+14*dcell    
 ! "Menu" droite
 
   IF ty<2.4*dcell       % exit
      Gosub ONBACKKEY
   ELSEIF ty<4*dcell     % Reset level
     FIN = 0
     GOTO New
   ELSEIF ty<5.6*dcell % "sound" button
     gr.hide IsonPtr[son+1]
     son=1-son
     gr.show IsonPtr[son+1]
     GR.RENDER
  ELSEIF ty<7.2*dcell    % info jeu.
    if sh=0
      gosub show
     else 
      gosub hide
     endif
     sh1=1
  ELSEIF ty< 8.8*dcell   % undo
    if und then gosub undo1
  ENDIF
  
 ELSEIF tx>ox
   ! clic écran jeu (ty>oy & tx<ox+14*dcell)
   tx=FLOOR((tx-ox)/dcell)+1
   ty=FLOOR((ty-oy)/dcell)+1
   
   ! move from xSel,ySel to target tx,ty
   IF xSel & (tx=xSel | ty=ySel) & !(tx=xSel & ty=ySel) & !FIN  
     GOSUB moveatome
     GOSUB ctrlEOGame   % test end level
     IF FIN
       IF !score[Level] | moved<score[Level] THEN score[Level] = moved
       GOSUB majscore
       POPUP WORD$(lang$[langage],7,","),0,0,0
       if son then soundpool.play nul,appl,0.99,0.99,1,0,1
     ENDIF
    ENDIF
    
 ! zone tx < ox (gauche ecran jeu) 
 ELSEIF ty > 5.4*dcell & ty < 6.6*dcell
  ! chgt niveau/level
   IF tx<1.2*dcell
     Level= Level -1 + 30*(Level=1)
     FIN = 0
     GOTO New
   ELSEIF tx>3.9
     Level = Level+1-30*(Level=30)
     FIN = 0
     GOTO New
   ENDIF

 ELSEIF ty>6.5*dcell & ty<7.7*dcell
   ! chgt mur/wall
   IF     tx<1.2*dcell   % mur -
     wall=wall-1+5*(wall=1)
     GOSUB affmur
     GOTO New
   ELSEIF tx>3.9*dcell   % mur +
     wall=wall+1-5*(wall=5)
     GOSUB affmur
     GOTO New
   ENDIF

  ELSEIF ty<8.9*dcell & ty>7.7*dcell
   ! chgt coul fond
   IF     tx<1.2*dcell   % skin -
     BGcol = BGcol-1*(BGcol>1)+2*(BGcol=1)
     chgBkgd = 1
     GOTO New
   ELSEIF tx>3.9*dcell   % skin +
     BGcol = BGcol+1-3*(BGcol=3)
     chgBkgd = 1
     GOTO New
   ENDIF
 
 ! ELSEIF ty>12.3*dcell & tx<dcell/5
   ! (acces caché) chgt langue français/anglais
   ! langage=2-1*(langage=2)
   ! GOTO New
 ENDIF
 if sh & sh1=0
   gosub hide
 else
   sh1=0
 endif
UNTIL 0
GR.CLOSE
END

RestoreMol:
destX=0
destY=0
do
  gr.get.position mPtr, px, py
  ix= floor((px-destX)/2)
  iy= floor((py-destY)/2)
  if abs(ix)<5 & abs(iy)<5 then D_u.break
  gr.modify mPtr, "x", px-ix
  gr.modify mPtr, "y", py-iy
  gr.render
  pause 5
until 0
gr.modify mPtr, "x", destX
gr.modify mPtr, "y", destY
gr.render
RETURN

majscore:
  GR.modify mov1Ptr,"text",stg$(moved)
  gr.modify mov2Ptr,"text",stg$(score[Level])
  gr.render
RETURN

undo1:     % (infos undo dans moveatome:)
! si und>1 retour arr possible
fin = 0
tori = utori[und] % inverse tori 1 a 4
dx = udx[und]-1   % utx = xSel
dy = udy[und]-1   % utyc= ySel
xSel = uxSel[und] % uxSel = dx
ySel = uySel[und] % uySel = dy
GR.MODIFY cell[xSel,ySel,2], "x", ox+dx*dcell
GR.MODIFY cell[xSel,ySel,2], "y", oy+dy*dcell
   
 swap cell[udx[und],udy[und],1],cell[xSel,ySel,1]  % dest
 swap cell[udx[und],udy[und],2],cell[xSel,ySel,2]

 ! maj cell (av et aprés) et compteur 
 FIN=0
 moved--
 und--
 gosub majscore
RETURN

moveatome: % Move atome in vert / horiz
if son then soundpool.play nul,boing,0.99,0.99,1,0,1

! at least tx=xSel vertical or ty=ySel horizontal
distX = xSel - tx
distY = ySel - ty
IF ABS(distX) > 0 & distY = 0 THEN
 IF distX> 0 THEN tori=3 ELSE tori=1  % LeftRight
ELSEIF distX = 0 & ABS(distY) > 0
 IF distY > 0 THEN tori=4 ELSE tori=2  % up|down
ENDIF
dx= xSel
dy= ySel
while (dx+dix[tori])>0 & (dx+dix[tori])<15 & (dy+diy[tori])>0 & (dy+diy[tori])<13
  if cell[dx+dix[tori],dy+diy[tori],1] then W_r.break
  dx+= dix[tori]
  dy+= diy[tori]
repeat
if dx<>xSel | dy<>ySel
  destx= ox+(dx-1)*dcell
  desty= oy+(dy-1)*dcell
  do
    gr.get.position cell[xSel,ySel,2], px, py
    ix= floor((px-destx)/2)
    iy= floor((py-desty)/2)
    if abs(ix)<5 & abs(iy)<5 then D_u.break
    GR.MODIFY cell[ xSel,ySel,2], "x", px-ix
    GR.MODIFY cell[ xSel,ySel,2], "y", py-iy
    GR.RENDER
    PAUSE 5
  until 0
  GR.MODIFY cell[ xSel,ySel,2], "x", destx
  GR.MODIFY cell[ xSel,ySel,2], "y", desty
  swap cell[dx,dy,1],cell[xSel,ySel,1]  % arrivée
  swap cell[dx,dy,2],cell[xSel,ySel,2]
  moved++
  gosub majscore
endif

! infos undo
und++
udx[und] =xSel
udy[und] =ySel
uxSel[und] =dx
uySel[und] =dy
utori[und] = 1*(tori=3)+ 2*(tori=4) +3*(tori=1) +4*(tori=2)
RETURN

ctrlEOGame:
! Control End Of Game: if molecule built FIN=0
L1$=WORD$(Levels$[Level],14,",")
L2$=WORD$(Levels$[Level],15,",")  % the molecule
L3$=WORD$(Levels$[Level],16,",")
ly=1
DO
 ly++
 lr$ = ""
 FOR lx=1 TO 14
  lr$=lr$+MID$("0BCDEFGHIJKLMNOPQR12",cell[lx,ly,1]+1,1)
 NEXT
 lr$=REPLACE$(lr$,"1","0")
 lr$=REPLACE$(lr$,"2","0")
 r2=IS_IN(L2$,lr$)
UNTIL ly>11 | r2
IF r2
 IF IS_IN(L1$,"X") & IS_IN(L3$,"X")
  FIN=1
 ELSE
  lr$ = ""
  ly--
  FOR lx=1 TO 14
   lr$=lr$+MID$("0BCDEFGHIJKLMNOPQR12",cell[lx,ly,1]+1,1)
  NEXT
  lr$=REPLACE$(lr$,"1","0")
  lr$=REPLACE$(lr$,"2","0")
  r1=IS_IN(L1$,lr$)
  IF r1
   lr$ = ""
   ly += 2
   FOR lx=1 TO 14
    lr$=lr$+MID$("0BCDEFGHIJKLMNOPQR12",cell[lx,ly,1]+1,1)
   NEXT
   lr$=REPLACE$(lr$,"1","0")
   lr$=REPLACE$(lr$,"2","0")
   r3=IS_IN(L3$,lr$)
   IF r3=r2 & r2=r1 THEN FIN=1
  ENDIF
 ENDIF
ENDIF
RETURN

affmur:
m=0
FOR yy=1 TO 12
 FOR xx=1 TO 14
  IF cell[xx,yy,1]=18
   m++
   GR.MODIFY murPtr[m],"bitmap", mur[wall]
  ENDIF
 NEXT
NEXT
GR.RENDER
RETURN

ONBACKKEY:
GOSUB saveGame
POPUP "Goodbye",0,0,0
PAUSE 500
OnError:
GR.CLOSE
END

saveGame:
cls
print STR$(Level)+" "+STR$(moved)+" "+STR$(wall)+" "+STR$(son)+" "+STR$(BGcol)+" "+STR$(langage)
FOR sc=1 TO 30
 score$ = score$+stg$(score[sc])+" "
NEXT
print score$
FOR cy = 1 TO 12
 L$ = ""
 FOR cx = 1 TO 14
  L$ = L$ + STR$(cell[cx,cy,1])+" "+STR$(cell[cx,cy,2]) + " "
 NEXT
 print L$
NEXT
console.save path$+"Atome.ini"
cls
RETURN

LoadGame:
FILE.EXISTS LdGame, path$+"Atome.ini"
IF LdGame
 GRABFILE LdGame$, path$+"Atome.ini"
 SPLIT Ln$[], LdGame$, "\n" % CHR$(10)   % end of line as separator
 Level = VAL(WORD$(Ln$[1],1))
 moved = VAL(WORD$(Ln$[1],2))
 wall  = VAL(WORD$(Ln$[1],3))
 son   = VAL(WORD$(Ln$[1],4))
 BGCol = VAL(WORD$(Ln$[1],5))
 langage= VAL(WORD$(Ln$[1],6))
 FOR sc=1 TO 30
  score[sc] = VAL(WORD$(Ln$[2],sc))
 NEXT
 FOR cy=1 TO 12
  c=1
  FOR cx=1 TO 14
   cell[cx,cy,1] = VAL(WORD$(Ln$[cy+2],c))
   cell[cx,cy,2] = VAL(WORD$(Ln$[cy+2],c+1))
   c+=2
  NEXT
 NEXT
 POPUP " Last game... ",0,0,0
 SavedGame = 1
 ARRAY.DELETE Ln$[]
ENDIF
RETURN

grid:
R = VAL(WORD$(BGDcol$[BGcol],4))
V = VAL(WORD$(BGDcol$[BGcol],5))  % grid color
B = VAL(WORD$(BGDcol$[BGcol],6))
GR.COLOR 255,R,V,B,1
return

bgd:
R = VAL(WORD$(BGDcol$[BGcol],1))
V = VAL(WORD$(BGDcol$[BGcol],2))  % background
B = VAL(WORD$(BGDcol$[BGcol],3))
GR.COLOR 255,R,V,B,1
return

initinfo:
GR.COLOR 220,55,55,55,1   % text color yellow
gr.rect tinfo,1.5*dcell,2*dcell,19.5*dcell,12*dcell

GR.COLOR 200,255,255,55,1   % text color white
GR.TEXT.SIZE 30
GR.TEXT.ALIGN 1

   if langage  = 2
tt$[1]= "      jeu Atome (version 1.1  07/2013)"
tt$[2]= "but du jeu: reconstitue la molécule comme celle en haut à gauche"
tt$[3]= "déplace les atomes en les glissant (4 directions possibles)"
tt$[4]="l'atome s'arrêtera au 1er obstacle dans la direction donnée"
tt$[5]= "attention: les atomes de même type ont des rattachements différents"
tt$[6]= "score par niveau: Action(s)  / (ton) Best / Top Score"
tt$[7]= "le meilleur record obtenu = le minimum de déplacements"
tt$[8]= "astuce: déplace la molécule sur le jeu pour confirmer un emplacement"
tt$[9]= "30 tableaux à résoudre.    bon amusement ..."
   else
tt$[1]= "     game Atome (version 1.0  06/2013)"
tt$[2]= "your job is to build the molecule same as the top left model"
tt$[3]= "touch and move the atomes, one by one, in the direction expected"
tt$[4]= "the atome will stop at the first end of way in this direction"
tt$[5]= "pay attention: same color atomes may have differents connections"
tt$[6]= "score by level:  Moves / (your) Best / Top score"
tt$[7]= "the best score equal a minimum of moves"
tt$[8]= "trick: move the molecule on the screen to confirm a place available"
tt$[9]= "you have to resolve 30 scenes...    have fun"
   endif
gr.hide tinfo
for n=1 to 9  
 gr.text.draw ttPtr[n],2*dcell,(n+2)*dcell,tt$[n]
 gr.hide ttPtr[n]
next
RETURN

show:
gr.show tinfo
for n=1 to 9
 gr.show ttPtr[n]
next
sh=1
RETURN

hide:
gr.hide tinfo
for n=1 to 9
 gr.hide ttPtr[n]
next
sh=0
RETURN

