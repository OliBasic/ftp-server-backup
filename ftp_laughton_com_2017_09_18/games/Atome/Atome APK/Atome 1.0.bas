! By gege_95c & Cassiope34
! game / jeu Atome (version 1.0)

FN.DEF stg$(c)
 FN.RTN LEFT$(STR$(c),IS_IN(".",STR$(c))-1)
FN.END

FN.DEF lang$()
 SYSTEM.OPEN
 SYSTEM.WRITE "getprop"
 PAUSE 100
 DO
  SYSTEM.READ.LINE l$
  SYSTEM.READ.READY ready
  fd= IS_IN("language",l$)
 UNTIL !ready | fd
 SYSTEM.CLOSE
 IF fd THEN Fn.rtn MID$(WORD$(l$,2,":"),3,2)
FN.END

GR.OPEN 255,255,255,255,1,0

DIM cell[14,12,2]       % cell[x,y,atome]
DIM atome[17]           % 17 formes atomes
DIM mur[5],murPtr[168]  % 5 choix de murs
DIM lang$[2]     % words in diff. languages
DIM BGDcol$[6]   % diff. background colors
DIM score[30]    % best number of move by level
DIM l$[12]       % number of line
DIM skin[3],skinPtr[3]  % 3 skin color 
DIM tt$[10],ttPtr[10]   % 10 lignes aide
DIM uxSel[200],uySel[200]
DIM udx[200],udy[200]

ARRAY.LOAD dix[],1,0,-1,0  % 1 Est  2 Sud
ARRAY.LOAD diy[],0,1,0,-1  % 3 West 4 North

  ! constants
dcell = 56  % cellule size
ox = 282    % left border
oy =  32    % top border 28
  ! french/english
langage  = 2-1*(lang$()<>"fr")
lang$[1] = "Wall,Level,Sound,Reset,action(s),Skin,  Well done !  the molecule is ok ...  "
lang$[2] = "Mur,Niveau,Son,RAZ,action(s),Fond,  Bravo !  la molecule est conforme...  "

BGDcol$[1] = "0 128 0 124 250 168"    % 2 color
BGDcol$[2] = "26 102 118 116 210 232" % of the
BGDcol$[3] = "250 138 2 250 218 92"   % Backgd

  ! init variables
wall  = 1  % type de mur 1 à 5
son   = 1  % sound on/off
BGcol = 1  % colors of Backgroung
Level = 1
SavedGame = 0
chgBkgd   = 0   % indicateur chgt couleur fond

  ! sounds !
soundpool.open 3
soundpool.load appl,"Applause.wav"
soundpool.load boing,"Boing.wav"

  ! load all levels
grabfile LdLvl$,"TopLevels.lvl"
split Levels$[], LdLvl$, "\n"
  ! CHR$(10)  end of line as separator.

  ! SavedGame=1 if game restored
 GOSUB loadGame

  ! LOAD AND SCALE GRAPHICS !
GR.SCREEN real_w, real_h
di_w = 1174        % 26*dcell - ox
di_h = 696         % 13*dcell - oy
scale_w = real_w/di_w
scale_h = real_h/di_h
GR.SCALE scale_w, scale_h

  ! create bitmaps from BMP source
gr.bitmap.load tmpBmp,"atomeBmp.png"

  ! mur     % x= 952 à 1176
FOR wal=1 TO 5
 GR.BITMAP.CROP mur[wal],tmpBmp,(wal+16)*56,0,56,56
next

  ! atomes  % x= 0 à 896
FOR a=1 TO 17
 GR.BITMAP.CROP atome[a],tmpBmp,(a-1)*56,0,56,56
NEXT

  ! dessin @tome (gauche)
GR.BITMAP.CROP icone,tmpBmp,952,56,261,74

  ! menu icones droite
GR.BITMAP.CROP Iexit,tmpBmp,0,  55,95,95
GR.BITMAP.CROP Iraz, tmpBmp,94, 55,95,95
GR.BITMAP.CROP Ison, tmpBmp,188,55,95,95
GR.BITMAP.CROP Info, tmpBmp,282,55,95,95
GR.BITMAP.CROP Iundo,tmpBmp,376,55,95,95
GR.BITMAP.CROP Bbest,tmpBmp,470,56,88,88
GR.BITMAP.CROP Btop, tmpBmp,470,56,88,88
GR.BITMAP.CROP BlevM,tmpBmp,558,56,57,57
GR.BITMAP.CROP BlevP,tmpBmp,615,56,57,57

  ! original bitmap is not useful -> free memory
GR.BITMAP.DELETE tmpBmp


New:     % new level

FOR n=1 TO 12   % charge les données du niveau
 l$[n] = WORD$(Levels$[Level],n+1,",")
NEXT
levName$ = WORD$(Levels$[Level],17,",")
mol$ = WORD$(Levels$[Level],18,",")
top  = VAL(WORD$(Levels$[Level+30],1))

  GR.CLS   % the screen
GOSUB bgd % 123
GR.RECT nul,0,0,1200,800  % real_w, real_h

  ! sauvegarde couleur actuelle
BGcol2 = BGcol

  ! icone coul -1
BGcol = BGcol-1+3*(BGcol=1)
GOSUB bgd % 123
GR.RECT nul,5,442,61,498     % 56x56
GOSUB grid % 456
GR.RECT nul,16,453,51,487    % 35x35

  ! icone coul +2
BGcol = BGcol-1+3*(BGcol=1)
GOSUB bgd % 123
GR.RECT nul,218,442,274,498  % 56x56
GOSUB grid % 456
! GR.RECT nul,230,453,263,487
GR.RECT nul,229,453,264,487  % 35x35 

  ! retour couleur actuelle
BGcol = BGcol2
GR.COLOR 255,255,255,255,1   % text color white
GR.TEXT.SIZE 30
GR.TEXT.ALIGN 2

  ! 2 traits/ (sur Hp et Undo) pour desactiver
! GR.LINE IsonoPtr  ,1094,314,1145,263
! GR.LINE IundonoPtr,1094,504,1145,453


  ! menu droite 5 buttons+date+time
GR.SET.STROKE 1
GR.COLOR 255,255,255,255,1
  ! exit ox 282
GR.BITMAP.DRAW IexitPtr,Iexit,1073, 48
  ! Raz
GR.BITMAP.DRAW IrazPtr ,Iraz ,1073,144
  ! son
GR.BITMAP.DRAW IsonPtr ,Ison ,1073,240
  ! info jeu mode emploi
GR.BITMAP.DRAW InfoPtr ,Info ,1073,336
  ! undo
GR.BITMAP.DRAW IundoPtr,Iundo,1073,432

  ! traits /(rouge) => desactiver son et undo
GR.COLOR 255,255,25,25,1
GR.SET.STROKE 6
GR.LINE IsonoPtr,  1095,313,1145,263
GR.LINE IundonoPtr,1095,504,1145,453

if son=1 then GR.HIDE IsonoPtr
if moved>0 & moved<201 then GR.HIDE IundonoPtr

  ! normalisé stroke et couleur
GR.SET.STROKE 1
GR.COLOR 255,255,255,255,1

  ! date et heure
TIME TIME(),Year$, Month$, Day$, Hour$, Minute$,, dow
GR.TEXT.DRAW IhmnPtr,1122,672, hour$+":"+minute$

days$= "Sun Mon Tue Wed Thu Fri Sat Dim Lun Mar Mer Jeu Ven Sam"

GR.TEXT.DRAW IdatePtr,1122,626,WORD$(days$,dow+7*(langage-1))

if langage=1
  GR.TEXT.DRAW IanPtr  ,1122,580, month$+"/"+day$ 
else
  GR.TEXT.DRAW IanPtr  ,1122,580,day$+"/"+month$
endif
        ! Infos partie gauche ecran

  ! nom + formule molecule
if levName$="Eau" & langage=1 then levName$ ="Water"
GR.TEXT.DRAW molPtr,146,263, levName$ +" - "+mol$ % +")"

  ! icone chgt. - niveau +
GR.BITMAP.DRAW nul,BlevM,5,308
GR.BITMAP.DRAW nul,BlevP,218,308
GR.TEXT.DRAW namePtr,140,341,WORD$(lang$[langage],2,",")+" "+stg$(level)

  ! couleur affichage texte
GR.COLOR 255,255,255,255,1  % text color: white
GR.TEXT.SIZE 30

  ! affiche texte <mur/wall>
GR.TEXT.DRAW nul,140,409, WORD$(lang$[langage],1,",")

  ! affiche mur -1
wl= wall-1+5*(wall=1)
GR.BITMAP.DRAW nul,mur[wl],6,375

  ! affiche mur +1
wl=1+wall*(wall<5)
GR.BITMAP.DRAW nul,mur[wl],218,375

  ! affiche <<fond/skin>>
GR.TEXT.DRAW nul,140,481,WORD$(lang$[langage],6,",")

  ! ajout 2 traits à gauche
GR.LINE nul,6,291,274,291
GR.LINE nul,6,509,274,509

  ! affiche dessin @tome
GR.BITMAP.DRAW nul,icone,6,526

  ! 2 boutons bbest et btop
GR.COLOR 140,255,O,25.5,1   % fond transparent
GR.BITMAP.DRAW BbestPtr,Bbest,95,605
GR.BITMAP.DRAW BtopPtr,Btop, 190,605

GOSUB shtopbest % montre/cache boutons top/best

GR.COLOR 255,255,255,255,1   % text color white

  ! affiche titre Move(s) / Best / Top
GR.TEXT.DRAW scorePtr,134,638,"Score   Best     Top"

  ! affiche nb deplacements
GR.TEXT.DRAW mov1Ptr,56, 677,stg$(moved)
GR.TEXT.DRAW mov2Ptr,140,677,stg$(score[Level])
gr.text.draw topPtr, 235,677,stg$(top)

  ! grid color 456 grille de jeu
GOSUB grid
GR.RECT nul, ox, oy,1066,704

  ! background color 123
GOSUB bgd

  ! affiche murs+decor
w=0
FOR y=1 TO 12
 FOR x=1 TO 14
  ll = IS_IN(MID$(l$[y],x,1),"BCDEFGHIJKLMNOPQR12")
  IF ll = 19  % external
   GR.RECT nul, ox+(x-1)*dcell-1, oy+(y-1)*dcell-1, ox+x*dcell+2, oy+y*dcell+2
  ELSEIF ll = 18  % wall
   w++
   GR.BITMAP.DRAW murPtr[w],mur[wall],ox+(x-1)*dcell,oy+(y-1)*dcell
   cell[x,y,1] = 18
  ELSE
   IF SavedGame | chgBkgd THEN ll= cell[x,y,1]
   IF ll
    GR.BITMAP.DRAW atomePtr,atome[ll],ox+(x-1)*dcell,oy+(y-1)*dcell
    cell[x,y,2] = atomePtr
   ENDIF
  ENDIF
  IF !SavedGame & !chgBkgd THEN cell[x,y,1] = ll
 NEXT
NEXT

  ! affiche/draw molecule
GR.BITMAP.CREATE molPtr,280,224
GR.BITMAP.DRAWINTO.START molPtr
lenMol= LEN(WORD$(Levels$[Level],15,","))
offset= FLOOR((ox-lenMol*dcell)/2)-1
FOR ym=1 TO 3
 wrd$= WORD$(Levels$[Level],13+ym,",")
 IF !IS_IN("X",wrd$)
  ltm=LEN(wrd$)
  FOR xm=1 TO ltm
   llm =IS_IN(MID$(wrd$,xm,1),"BCDEFGHIJKLMNOPQR")
   IF llm THEN GR.BITMAP.DRAW molecPtr,atome[llm],offset+(xm-1)*dcell,ym*dcell
  NEXT
 ENDIF
NEXT
GR.BITMAP.DRAWINTO.END
GR.BITMAP.DRAW mPtr,molPtr, 0, 0

GOSUB infojeu

  ! appui sur top/best
IF topbest>0 & !chgBkgd THEN GOSUB vueSoluce

  ! boucle du jeu
SavedGame = 0
chgBkgd = 0

DO
 DO
  GR.TOUCH touched,ttx,tty
  PAUSE 5
  TIME ,,, Hour$, Minute$,
  GR.MODIFY IhmnPtr, "text",hour$+":"+minute$
  GR.RENDER
 UNTIL touched
 IF deltaX THEN GOSUB RestoreMol
 ttx/=scale_w
 tty/=scale_h
 xSel=FLOOR((ttx-ox)/dcell)+1
 ySel=FLOOR((tty-oy)/dcell)+1
 deltaX=0
 IF xSel>0 & xSel<15 & ySel>0 & ySel<13
  IF !cell[xSel,ySel,1] | cell[xSel,ySel,1]>17 THEN xSel=0  % it's not an atome

 ELSEIF xSel<=0 & ySel<4    % touch the molecule
  deltaX=ttx
  deltaY=tty
 ENDIF

 DO
  GR.TOUCH touched,tx,ty
  tx/=scale_w
  ty/=scale_h
  IF deltaX      % molecule follow the finger
   GR.MODIFY mPtr, "x", tx-deltaX
   GR.MODIFY mPtr, "y", ty-deltaY
   GR.RENDER
  ENDIF
 UNTIL !touched
 
 IF deltaX
   gr.get.position mPtr, mx, my
   if floor ((mx-deltaX)/56)>0
     GR.MODIFY mPtr, "x", ox+round((mx-ox)/56)*56 -28*(ltm=4)
     GR.MODIFY mPtr, "y", oy+round((my-oy)/56)*56
   else
     GR.MODIFY mPtr, "x",0
     GR.MODIFY mPtr, "y",0
   endif
   GR.RENDER
   
 ELSEIF shinfo=1       % cache aide 
  GOSUB hideinfo
  
 ELSEIF tx>1066        % "Menu" droite
  IF     ty<130            % 134 exit
   GOSUB ONBACKKEY
  ELSEIF ty<240        % 224 Reset level
   FIN = 0
   topbest=0
   moved = 0
   GOTO New
  ELSEIF ty<336        % 313 "sound" button
   son=1-son
   if son=1
     GR.HIDE IsonoPtr
   else
     GR.SHOW IsonoPtr
   endif
   GR.RENDER
  ELSEIF ty<432        % 403 info jeu
    IF shinfo=0
      GOSUB showinfo
    ENDIF
  ELSEIF ty<528        % 493 undo
   IF moved>0 & moved <201 THEN gosub undo1
  ENDIF

 ELSEIF tx>ox          % écran jeu
  if abs(ttx-tx)>abs(tty-ty)
    tx= FLOOR((tx-ox)/56)+1
    ty= ySel
  elseif abs(ttx-tx)<abs(tty-ty)
    ty= FLOOR((ty-oy)/56)+1
    tx= xSel
  endif

    ! move from xSel,ySel to target tx,ty
  IF xSel & (tx=xSel | ty=ySel) & !(tx=xSel & ty=ySel) & !FIN
   GOSUB moveatome
   GOSUB ctrlEOGame   % test if end level
   IF FIN
    ! empeche acquérir score Best via solution
    IF !score[Level] | (moved < score[Level] & topbest = 0)
     score[Level] = moved
     IF moved<top
      CLS
      soluce$ = STG$(moved)+" - "
      FOR m=1 TO moved
       soluce$+= CHR$(64+udx[m])+CHR$(64+udy[m])+CHR$(64+uxSel[m])+CHR$(64+uySel[m])
      NEXT
      PRINT soluce$
      console.save "Top"+right$(FORMAT$("%%",Level),2)+".top"
     ENDIF
     GOSUB shtopbest % maj 2 boutons top/best  
    ENDIF
    GOSUB majscore    
    POPUP WORD$(lang$[langage],7,","),0,0,0
    IF son THEN soundpool.play nul,appl,0.99,0.99,1,0,1
   ENDIF
  ELSEIF fin
   POPUP WORD$(lang$[langage],7,","),0,0,0
  ENDIF
  
  ! zone tx < ox (gauche ecran jeu)
 ELSEIF ty>302 & ty<369    % chgt niveau/level
  IF tx<67
   Level= Level-1+30*(Level=1)
   FIN = 0
   vuesol=0  % si chgt niv ou raz
   topbest=0
   moved = 0
   GOTO New
  ELSEIF tx>218
   Level= Level+1-30*(Level=30)
   FIN = 0
   vuesol=0  % si chgt niv ou raz
   topbest=0
   moved = 0
   GOTO New
  ENDIF

 ELSEIF ty>369 & ty<431    % chgt mur/wall
  IF     tx<67    % mur -
    wall = wall-1+5*(wall=1)
    GOSUB affmur
    chgBkgd = 1
    GOTO New
  ELSEIF tx>218   % mur +
   wall = wall+1-5*(wall=5)
   GOSUB affmur
   chgBkgd = 1
   GOTO New
  ENDIF

 ELSEIF ty>431 & ty<498    % chgt coul fond
  IF     tx<67    % skin -
    BGcol = BGcol-1+3*(BGcol=1)
    chgBkgd = 1
    GOTO New
  ELSEIF tx>218   % skin +
    BGcol = BGcol+1-3*(BGcol=3)
    chgBkgd = 1
    GOTO New
  ENDIF

 ELSEIF ttx>115 & ttx<182 & tty>616
  IF vuesol=2  % vue <solution best> (joueur)
    topbest=2
    GOTO New
  ENDIF
   ELSEIF ttx>202 & ttx<269 & tty>616
  IF vuesol> 0   % vue <solution top>
    topbest=1
    GOTO New
  ENDIF
 ENDIF
UNTIL 0

shtopbest:  % montre/cache 2 boutons top/ best
 vuesol=0
 GR.HIDE bbestPtr
 GR.HIDE btopPtr
 cs$="Top"+right$(FORMAT$("%%",Level),2)+".top"
 file.exists fe,cs$

 IF score[Level]>0
  GR.SHOW btopPtr
  vuesol=1
  IF score[Level] < top & fe > 0
    GR.SHOW bbestPtr
    vuesol=2
  ENDIF 
 ENDIF
RETURN

vueSoluce:     % acces demo
IF     topbest=1     % demo top
  cs$ = WORD$(Levels$[Level+30],3)
ELSEIF topbest=2     % demo best
  grabfile demobest$, "Top"+right$(FORMAT$("%%",Level),2)+".top"
  cs$ = WORD$(demobest$,3)
ENDIF

if cs$<>""
  lenA = LEN(cs$)
  moved= 0
  GR.RENDER
  FOR m=1 TO lenA STEP 4
    PAUSE 300
    xSel= ASCII( MID$(cs$,m,1) )-64
    ySel= ASCII( MID$(cs$,m+1,1) )-64
    dx  = ASCII( MID$(cs$,m+2,1) )-64
    dy  = ASCII( MID$(cs$,m+3,1) )-64
    GOSUB actionMove
    moved++
    uxSel[moved]=dx
    uySel[moved]=dy
    udx[moved]=xSel
    udy[moved]=ySel
    GOSUB majscore
  NEXT
  PAUSE 300
  fin=1
endif
RETURN

RestoreMol:
destX=0
destY=0
DO
 GR.GET.POSITION mPtr, px, py
 ix= FLOOR((px-destX)/2)
 iy= FLOOR((py-destY)/2)
 IF ABS(ix)<5 & ABS(iy)<5 THEN D_U.BREAK
 GR.MODIFY mPtr, "x", px-ix
 GR.MODIFY mPtr, "y", py-iy
 GR.RENDER
 PAUSE 5
UNTIL 0
GR.MODIFY mPtr, "x", destX
GR.MODIFY mPtr, "y", destY
GR.RENDER
RETURN

majscore:
GR.MODIFY mov1Ptr,"text",stg$(moved)
GR.MODIFY mov2Ptr,"text",stg$(score[Level])

if moved=0 | moved> 200
  GR.SHOW IundonoPtr
else
  GR.HIDE IundonoPtr
endif
gr.render
RETURN

undo1:     % (infos undo dans moveatome:)
fin = 0
dx = udx[moved]     % utx = xSel
dy = udy[moved]     % utyc= ySel
xSel = uxSel[moved] % uxSel = dx
ySel = uySel[moved] % uySel = dy
GOSUB actionMove  % maj cellule+compteur
moved--
GOSUB majscore
RETURN

moveatome: % Move atome in vertical / horizontal
IF son THEN soundpool.play nul,boing,0.99,0.99,1,0,1
! at least tx=xSel vertical or ty=ySel horizontal
distX = xSel - tx
distY = ySel - ty
IF ABS(distX) > 0 & distY = 0
  IF distX> 0 THEN tori=3 ELSE tori=1  % LeftRight
ELSEIF ABS(distY) > 0 & distX = 0
  IF distY > 0 THEN tori=4 ELSE tori=2  % up|down
ENDIF
dx= xSel
dy= ySel
WHILE (dx+dix[tori])>0 & (dx+dix[tori])<15 & (dy+diy[tori])>0 & (dy+diy[tori])<13
  IF cell[dx+dix[tori],dy+diy[tori],1] THEN W_R.BREAK
  dx+= dix[tori]
  dy+= diy[tori]
REPEAT
IF dx<>xSel | dy<>ySel
  GOSUB actionMove
  moved++
ENDIF
IF moved <201
  udx[moved] =xSel
  udy[moved] =ySel
  uxSel[moved] =dx
  uySel[moved] =dy
ENDIF 
GOSUB majscore
if moved=180 | moved=185 | (moved>189 & moved<200)
  if langage=2
   POPUP "retour arriere interdit apres 200 deplacements",-240,-250,0
  else
    POPUP "back turn will be forbiden up to 200 moves",-240,-250,0
  endif
endif
RETURN

actionMove:     % from xSel,ySel to dx,dy
destx= ox+(dx-1)*56
desty= oy+(dy-1)*56
DO
 GR.GET.POSITION cell[xSel,ySel,2], px, py
 ix= FLOOR((px-destx)/2)
 iy= FLOOR((py-desty)/2)
 IF ABS(ix)<5 & ABS(iy)<5 THEN D_u.break
 GR.MODIFY cell[ xSel,ySel,2], "x", px-ix
 GR.MODIFY cell[ xSel,ySel,2], "y", py-iy
 GR.RENDER
 PAUSE 2
UNTIL 0
GR.MODIFY cell[ xSel,ySel,2], "x", destx
GR.MODIFY cell[ xSel,ySel,2], "y", desty
SWAP cell[dx,dy,1],cell[xSel,ySel,1] % destination
SWAP cell[dx,dy,2],cell[xSel,ySel,2]
RETURN

ctrlEOGame:     % Control if the molecule is built.
L1$=WORD$(Levels$[Level],14,",")
L2$=WORD$(Levels$[Level],15,",")  % the molecule
L3$=WORD$(Levels$[Level],16,",")
ly=1
DO
 ly++
 lr$ = ""
 FOR lx=1 TO 14
  lr$=lr$+MID$("0BCDEFGHIJKLMNOPQR12",cell[lx,ly,1]+1,1)
 NEXT
 lr$=REPLACE$(lr$,"1","0")
 lr$=REPLACE$(lr$,"2","0")
 r2=IS_IN(L2$,lr$)
UNTIL ly>11 | r2
IF r2
 IF IS_IN(L1$,"X") & IS_IN(L3$,"X")
  FIN=1
 ELSE
  lr$ = ""
  ly--
  FOR lx=1 TO 14
   lr$=lr$+MID$("0BCDEFGHIJKLMNOPQR12",cell[lx,ly,1]+1,1)
  NEXT
  lr$=REPLACE$(lr$,"1","0")
  lr$=REPLACE$(lr$,"2","0")
  r1=IS_IN(L1$,lr$)
  IF r1
   lr$ = ""
   ly += 2
   FOR lx=1 TO 14
    lr$=lr$+MID$("0BCDEFGHIJKLMNOPQR12",cell[lx,ly,1]+1,1)
   NEXT
   lr$=REPLACE$(lr$,"1","0")
   lr$=REPLACE$(lr$,"2","0")
   r3=IS_IN(L3$,lr$)
   IF r3=r2 & r2=r1 THEN FIN=1
  ENDIF
 ENDIF
ENDIF
RETURN

affmur:
m=0
FOR yy=1 TO 12
 FOR xx=1 TO 14
  IF cell[xx,yy,1]=18
   m++
   GR.MODIFY murPtr[m],"bitmap", mur[wall]
  ENDIF
 NEXT
NEXT
GR.RENDER
RETURN

ONBACKKEY:
GOSUB saveGame
POPUP "Goodbye",0,0,0
PAUSE 500
!ONERROR:
GR.CLOSE
EXIT    % END "Bye, see you !"

saveGame:
CLS
L3$=""
PRINT STR$(Level)+" "+STR$(moved)+" "+STR$(wall)+" "+STR$(son)+" "+STR$(BGcol)+" "+STR$(langage) +" "+STR$(Fin)
FOR sc=1 TO 30
 score$ = score$+stg$(score[sc])+" "
NEXT
PRINT score$
FOR cy = 1 TO 12   % save actual position of atomes
 FOR cx = 1 TO 14
  if cell[cx,cy,1] & cell[cx,cy,1]<18
    L3$+=chr$(64+cx)+chr$(64+cy)+right$(replace$(str$(100+cell[cx,cy,1]),".0",""),2)+" "
  endif
 NEXT
NEXT
print L3$
L4$=""     % save the undo
while moved
  L4$ = chr$(64+udx[moved])+ chr$(64+udy[moved])+ chr$(64+uxSel[moved])+ chr$(64+uySel[moved])+L4$
  moved--
repeat
print L4$
console.save "Atome.ini"
RETURN

LoadGame:
file.exists LdGame,"Atome.ini"
IF LdGame
 grabfile LdGame$,"Atome.ini"
 SPLIT Ln$[], LdGame$, "\n" % CHR$(10)
 Level  = VAL(WORD$(Ln$[1],1))
 moved  = VAL(WORD$(Ln$[1],2))
 wall   = VAL(WORD$(Ln$[1],3))
 son    = VAL(WORD$(Ln$[1],4))
 BGCol  = VAL(WORD$(Ln$[1],5))
 langage= VAL(WORD$(Ln$[1],6))
 Fin    = VAL(WORD$(Ln$[1],7))
 FOR sc=1 TO 30
  score[sc] = VAL(WORD$(Ln$[2],sc))
 NEXT
 FOR lg=1 TO LEN(Ln$[3])/5  % where are atomes
   rs$ = WORD$(Ln$[3],lg)
   cell[ASCII(MID$(rs$,1,1))-64,ASCII(MID$(rs$,2,1))-64,1] = VAL(MID$(rs$,3,2))
 NEXT
 if moved              % restore the undo
   for ud=1 to moved
     wud$ = mid$(Ln$[4],1+((ud-1)*4),4)
     udx[ud]= ASCII(mid$(wud$,1,1))-64
     udy[ud]= ASCII(mid$(wud$,2,1))-64
     uxSel[ud]= ASCII(mid$(wud$,3,1))-64
     uySel[ud]= ASCII(mid$(wud$,4,1))-64
   next
   ud--
 endif
 popup " Last game...", 0, 0, 0
 SavedGame = 1
 ARRAY.DELETE Ln$[]
ENDIF
RETURN

grid:
R = VAL(WORD$(BGDcol$[BGcol],4))
V = VAL(WORD$(BGDcol$[BGcol],5))  % grid color
B = VAL(WORD$(BGDcol$[BGcol],6))
GR.COLOR 255,R,V,B,1
RETURN

bgd:
R = VAL(WORD$(BGDcol$[BGcol],1))
V = VAL(WORD$(BGDcol$[BGcol],2))  % background
B = VAL(WORD$(BGDcol$[BGcol],3))
GR.COLOR 255,R,V,B,1
RETURN

infojeu:
GR.COLOR 220,55,55,55,1   % text color yellow
GR.RECT tinfo,6,84,1075,672

GR.COLOR 200,255,255,55,1   % text color white
GR.TEXT.SIZE 30
GR.TEXT.ALIGN 1

IF langage=2
 tt$[1]="jeu Atome (version 1.2  08/2013)"
 tt$[2]="- but du jeu:  reconstituer la molecule comme le modele en haut a gauche."
 tt$[3]="  ce modele peut etre glisse sur l'ecran pour confirmer un emplacement valide."
 tt$[4]="- deplacement:  toucher et glisser un atome dans l'une des 4 directions."
 tt$[5]="  l'atome s'arretera au 1er obstacle rencontre dans la direction donnee."
 tt$[6]="- attention: les atomes de meme type ont des rattachements differents."
 tt$[7]="- scores par niveau: Action(s)  / (ton score) Best / Top Score"
 tt$[8]="- voir la demo (Top): un bouton apparait des que tu reussis le niveau une fois"
 tt$[9]="- voir ta demo (Best): accessible si ton score est inferieur au Top"
 tt$[10]="il y a 30 tableaux a resoudre, bon amusement ...."
ELSE
 tt$[1]= "game Atome (version 1.2  08/2013)"
 tt$[2]= "- your job:  build the molecule same as the top left model."
 tt$[3]= "  drag the model on the screen can help to confirm a valid building place."
 tt$[4]= "- move:  touch and drag an atome in the direction expected."
 tt$[5]= "  the atome will stop at the first end of way in this direction."
 tt$[6]= "- pay attention, same color atomes may have differents connections"
 tt$[7]= "- scores by level :  Moves / (your) Best / Top score"
 tt$[8]= "- Top demo access: a button will appear if you finish the level once."
 tt$[9]= "- Best demo access: you'll have to realise your (Best) score lower than Top."
 tt$[10]="there are 30 scenes to resolve,  have fun...."
ENDIF
GR.HIDE tinfo
FOR n=1 TO 10
 GR.TEXT.DRAW ttPtr[n],(0.3+5*(n=1 | n=10))*56,(n+1.5)*56,tt$[n]
 GR.HIDE ttPtr[n]
NEXT
RETURN

showinfo:
GR.SHOW tinfo
FOR n=1 TO 10
 GR.SHOW ttPtr[n]
NEXT
shinfo =1
RETURN

hideinfo:
GR.HIDE tinfo
FOR n=1 TO 10
 GR.HIDE ttPtr[n]
NEXT
shinfo=0
RETURN
