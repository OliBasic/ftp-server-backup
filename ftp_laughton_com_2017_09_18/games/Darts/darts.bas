! Darts
! Aat Don @2013
GR.OPEN 255, 210, 105, 30
GR.ORIENTATION 1
GR.SCREEN w,h
sx=w/720
sy=h/960
GR.SCALE sx,sy
GR.TEXT.SIZE 60
GR.COLOR 255,255,255,255,1
GR.TEXT.DRAW g,15,480,"Initializing..."
GR.RENDER
cX=360
cY=480
ARRAY.LOAD BoardNum$[],"6","10","15","2","17","3","19","7","16","8","11","14","9","12","5","20","1","18","4","13"
ARRAY.LOAD Pattern[], 1,200
ARRAY.LOAD a[],0,200,150,0,330,0,420,120,600,0,720,240,450,400,300,280,90,400
DIM SegAngles[20]
DIM Dart[3]
DIM HiScore$[4,2]
pi=3.1415
! Circle radius
OuterRing=303
Ring1Rad=246
Ring2Rad=231
Ring3Rad=150
Ring4Rad=135
Ring5Rad=24
Ring6Rad=12
GOSUB ReadScores
GOSUB MakeBMP

! Start of game
GOSUB DrawBoard
GR.BITMAP.DRAW OK,Well,0,330
GR.HIDE OK
GR.RENDER
GameScore=301
Turns=0
GR.TEXT.SIZE 60
GR.COLOR 255,255,255,255,1
GR.TEXT.DRAW g,15,75,"0.Score 0 Left 301"
GR.RENDER
DO
	TurnScore=0
	EindDbl=0
	Turns=Turns+1
	GR.COLOR 255,0,0,0,1
	GR.OVAL g,0,780,200,870
	GR.TEXT.SIZE 60
	GR.COLOR 255,255,255,255,1
	GR.TEXT.DRAW g,45,840,"0"
	GR.RENDER
	For Nr=1 TO 3
		GOSUB ThrowDart
		GOSUB EvalPos
		TurnScore=TurnScore+Score
		IF R$="Double" | R$="Bull's Eye" THEN EindDbl=1
		GR.COLOR 255,0,0,0,1
		GR.OVAL g,0,780,200,870
		GR.TEXT.SIZE 60
		GR.COLOR 255,255,255,255,1
		T$=REPLACE$(FORMAT$("##%",TurnScore)," ","")
		GR.TEXT.DRAW g,45,840,T$
		GR.RENDER
		PAUSE 1000
	NEXT Nr
	IF TurnScore>=100 THEN
		GR.SHOW OK
		GR.RENDER
		TONE 440,200
		PAUSE 2000
		GR.HIDE OK
	ENDIF
	GR.HIDE Dart[1]
	GR.HIDE Dart[2]
	GR.HIDE Dart[3]
	! Show score
	IF (GameScore-TurnScore)>1 | (GameScore-TurnScore=0 & EindDbl=1) THEN
		GameScore=GameScore-TurnScore
	ENDIF
	GR.COLOR 255,0,0,0,1
	GR.RECT g,0,0,720,90
	GR.TEXT.SIZE 60
	GR.COLOR 255,255,255,255,1
	T$=REPLACE$(FORMAT$("###",Turns)," ","")
	TS$=REPLACE$(FORMAT$("##%",TurnScore)," ","")
	G$=REPLACE$(FORMAT$("##%",GameScore)," ","")
	GR.TEXT.DRAW g,15,75,T$+".Score "+TS$+" Left "+G$
	GR.RENDER
UNTIL GameScore=0 & EindDbl=1
GR.SHOW OK
GR.RENDER
TONE 440,200
PAUSE 2000
GR.HIDE OK
GR.RENDER
Rank=0
FOR i=3 TO 1 STEP-1
	IF Turns<VAL(HiScore$[i,2]) THEN
		Rank=i
	ENDIF
NEXT i
GR.FRONT 0
IF Rank>0 THEN
	INPUT "New High Score, please enter your name",N$
	FOR i=4 TO Rank+1 STEP -1
		HiScore$[i,1]=HiScore$[i-1,1]
		HiScore$[i,2]=HiScore$[i-1,2]
	NEXT i
	HiScore$[Rank,1]=N$
	HiScore$[Rank,2]=REPLACE$(FORMAT$("##",Turns)," ","")
	TEXT.OPEN W,F,"DartHS.txt"
		FOR i=1 TO 3
			TEXT.WRITELN F,HiScore$[i,1]
			TEXT.WRITELN F,HiScore$[i,2]
		NEXT i
	TEXT.CLOSE F
ENDIF
! Show Hi scores
PRINT "Hall of Fame"
PRINT "------------"
PRINT "best 3 players to date are :"
PRINT "1.";HiScore$[1,1];" took ";HiScore$[1,2];" turns"
PRINT "2.";HiScore$[2,1];" took ";HiScore$[2,2];" turns"
PRINT "3.";HiScore$[3,1];" took ";HiScore$[3,2];" turns"
PAUSE 5000
GR.FRONT 1
GR.COLOR 255,0,0,0,1
GR.RECT g,0,390,720,540
GR.TEXT.SIZE 120
GR.COLOR 255,255,255,0,1
GR.TEXT.DRAW g,15,510,"GAME OVER"
GR.RENDER
TONE 700,200
PAUSE 5000
GR.CLOSE
EXIT

ReadScores:
	FILE.EXISTS t,"DartHS.txt"
	IF t=0 THEN
		! Create new file
		TEXT.OPEN W,F,"DartHS.txt"
			FOR i=1 TO 3
				TEXT.WRITELN F,"NoName"
				TEXT.WRITELN F,STR$(100)
			NEXT i
		TEXT.CLOSE F
	ENDIF
	TEXT.OPEN R,F,"DartHS.txt"
		FOR i=1 TO 3
			TEXT.READLN F,HiScore$[i,1]
			TEXT.READLN F,HiScore$[i,2]
		NEXT i
	TEXT.CLOSE F
RETURN

MakeBMP:
	! Draw Dart
	GR.BITMAP.CREATE Arrow,63,63
	GR.BITMAP.DRAWINTO.START Arrow
	GR.COLOR 255,255,255,255,0
	GR.OVAL o,0,21,60,39
	GR.OVAL o,21,0,39,60
	GR.COLOR 255,255,0,255,1
	GR.OVAL o,0,21,60,39
	GR.OVAL o,21,0,39,60
	GR.COLOR 255,0,0,0,1
	GR.CIRCLE c,31,31,3
	GR.BITMAP.DRAWINTO.END
	! Draw callout
	GR.BITMAP.CREATE Well,720,400
	GR.BITMAP.DRAWINTO.START Well
	LIST.CREATE n, List1
	LIST.ADD.ARRAY List1, a[]
	GR.SET.STROKE 12
	GR.COLOR 255,255,255,0,1
	GR.POLY pt,List1,0,0
	GR.COLOR 255,0,0,0,0
	GR.POLY pt,List1,0,0
	GR.TEXT.SIZE 90
	GR.COLOR 255,0,0,255,0
	GR.TEXT.DRAW c,90,240,"WELL DONE !"
	GR.BITMAP.DRAWINTO.END
RETURN

DrawBoard:
	GR.CLS
	! Background
	GR.SET.STROKE 6
	GR.COLOR 255,0,0,0,0
	FOR i=30 TO 720 STEP 120
		GR.LINE g,i,0,i,960
	NEXT i
	! Random dots
	i=0
	DO
		x=RND()*720
		y=RND()*960
		IF ROUND(SQR((x-cX)^2+(y-cY)^2))>OuterRing THEN
			i=i+1
			GR.CIRCLE c,x,y,3
		ENDIF
	UNTIL i=100
	GR.COLOR 255,210,105,30,1
	GR.RECT g,0,cY+480,720,cY+528
	GR.COLOR 255,0,0,0,0
	GR.RECT g,0,cY+480,720,cY+528
	GR.TEXT.SIZE 90
	GR.SET.STROKE 3
	GR.COLOR 255,0,0,0,1
	GR.RECT g,0,0,720,90
	GR.COLOR 255,255,255,0,0
	GR.TEXT.DRAW g,15,165,"Let's play Darts !"
	! Draw board
	GR.COLOR 255,255,255,255,0
	GR.CIRCLE c,cX,cY,OuterRing
	GR.COLOR 255,0,0,0,1
	GR.CIRCLE c,cX,cY,OuterRing-1
	! Segments
	FOR angle=9 TO 333 STEP 36
	! Red / Black
	GR.COLOR 255,255,0,0,1
	GR.ARC ga,cX-Ring1Rad,cY-Ring1Rad,cX+Ring1Rad,cY+Ring1Rad,angle,18,1
	GR.COLOR 255,0,0,0,1
	GR.ARC ga,cX-Ring2Rad,cY-Ring2Rad,cX+Ring2Rad,cY+Ring2Rad,angle,18,1
	GR.COLOR 255,255,0,0,1
	GR.ARC ga,cX-Ring3Rad,cY-Ring3Rad,cX+Ring3Rad,cY+Ring3Rad,angle,18,1
	GR.COLOR 255,0,0,0,1
	GR.ARC ga,cX-Ring4Rad,cY-Ring4Rad,cX+Ring4Rad,cY+Ring4Rad,angle,18,1
	! Green / White
	GR.COLOR 255,0,255,0,1
	GR.ARC ga,cX-Ring1Rad,cY-Ring1Rad,cX+Ring1Rad,cY+Ring1Rad,angle+18,18,1
	GR.COLOR 255,255,255,191,1
	GR.ARC ga,cX-Ring2Rad,cY-Ring2Rad,cX+Ring2Rad,cY+Ring2Rad,angle+18,18,1
	GR.COLOR 255,0,255,0,1
	GR.ARC ga,cX-Ring3Rad,cY-Ring3Rad,cX+Ring3Rad,cY+Ring3Rad,angle+18,18,1
	GR.COLOR 255,255,255,191,1
	GR.ARC ga,cX-Ring4Rad,cY-Ring4Rad,cX+Ring4Rad,cY+Ring4Rad,angle+18,18,1
	NEXT angle
	! Segment lines
	GR.COLOR 255,128,128,128,0
	l=Ring1Rad
	FOR angle=9 TO 351 STEP 18
		x=l*COS(angle*pi/180)+cX
		y=l*SIN(angle*pi/180)+cY
		GR.LINE g,cX,cY,x,y
		! save segment boundaries
		! Find angle of x,y w.r.t. center
		SegAngles[angle/18+1]=ROUND(ATAN2(y-cY,x-cX)*180/pi)
	NEXT angle
	! Numbers
	GR.TEXT.SIZE 35
	GR.COLOR 255,255,255,255,0
	l=280
	FOR angle=0 TO 342 STEP 18
		! use radians for position
		x=l*COS(angle*pi/180)+cX
		y=l*SIN(angle*pi/180)+cY
		! use degrees for rotation
		GR.ROTATE.START angle+90,x,y
		GR.TEXT.DRAW g,x-15,y+15,BoardNum$[angle/18+1]
		GR.ROTATE.END
	NEXT angle
	! Circles
	GR.COLOR 255,128,128,128,0
	GR.CIRCLE c,cX,cY,Ring1Rad
	GR.CIRCLE c,cX,cY,Ring2Rad
	GR.CIRCLE c,cX,cY,Ring3Rad
	GR.CIRCLE c,cX,cY,Ring4Rad
	GR.CIRCLE c,cX,cY,Ring5Rad+3
	! Bull
	GR.COLOR 255,0,255,0,1
	GR.CIRCLE c,cX,cY,Ring5Rad
	GR.COLOR 255,255,0,0,1
	GR.CIRCLE c,cX,cY,Ring6Rad
	GR.RENDER
RETURN

ThrowDart:
	GR.TEXT.SIZE 60
	GR.COLOR 255,0,0,0,1
	GR.RECT g,0,870,720,960
	GR.COLOR 255,255,255,255,1
	GR.TEXT.DRAW g,15,945,"Throw dart"
	GR.BITMAP.DRAW Dart[Nr],Arrow,540,885
	GR.RENDER
	DO
		DO
			GR.TOUCH touched,x,y
		UNTIL touched
		StrtX=ROUND(x/sx)
		StrtY=ROUND(y/sy)
	UNTIL (StrtX>720-720/4) & (StrtY>870)
	StrtX=StrtX+31.5
	StrtY=StrtY-31.5
	DO
		GR.TOUCH touched,x,y
	UNTIL !touched
	HitX=ROUND(x/sx)
	HitY=ROUND(y/sy)
	! Calculate line from start to end
	dY=HitY-StrtY
	dX=HitX-StrtX
	IF dX=0 THEN dX=0.01
	IF dY=0 THEN dY=0.01
	m=dY/dX
	b=StrtY-m*StrtX
	! Make steps based on distance
	d=SQR((StrtX-HitX)^2+(StrtY-HitY)^2)
	IF d<1 THEN d=1
	InSteps=(StrtY-HitY)/(d/85)+3
	FOR Ny=StrtY TO HitY STEP -InSteps
		Nx=ROUND((Ny-b)/m)
		GR.MODIFY Dart[Nr],"x",Nx-31.5
		GR.MODIFY Dart[Nr],"y",Ny-31.5
		GR.RENDER
		PAUSE 5
	NEXT Ny
	GR.MODIFY Dart[Nr],"x",ROUND(Nx)-31.5
	GR.MODIFY Dart[Nr],"y",ROUND(Ny)-31.5
	GR.RENDER
	HitX=ROUND(Nx)
	HitY=ROUND(Ny)
	TONE 200,200
	VIBRATE Pattern[], -1
RETURN

EvalPos:
	! Avoid 0,0
	IF HitX=cX THEN HitX=cX+1
	IF HitY=cY THEN HitY=cY+1
	Hoek=ROUND(ATAN2(HitY-cY,HitX-cX)*180/pi)
	Afstand=ROUND(SQR((HitX-cX)^2+(HitY-cY)^2))
	FOR i=1 to 9
		IF Hoek>SegAngles[i] & Hoek<=SegAngles[i+1] THEN
			Score=Val(BoardNum$[i+1])
		ENDIF
	NEXT i
	IF Hoek>SegAngles[10] & Hoek<=180 THEN
		Score=Val(BoardNum$[11])
	ENDIF
	IF Hoek>-180 & Hoek<=SegAngles[11] THEN
		Score=Val(BoardNum$[11])
	ENDIF
	FOR i=11 to 19
		IF Hoek>SegAngles[i] & Hoek<=SegAngles[i+1] THEN
			Score=Val(BoardNum$[i+1])
		ENDIF
	NEXT i
	IF Hoek>SegAngles[20] & Hoek<=0 THEN
		Score=Val(BoardNum$[1])
	ENDIF
	IF Hoek>0 & Hoek<=SegAngles[1] THEN
		Score=Val(BoardNum$[1])
	ENDIF

	R$="Single"
	IF Afstand>Ring1Rad THEN
		R$="MISSED"
		Score=0
	ENDIF
	IF Afstand<=Ring1Rad & Afstand>=Ring2Rad THEN
		R$="Double"
		Score=Score*2
	ENDIF
	IF Afstand<=Ring3Rad & Afstand>=Ring4Rad THEN
		R$="Triple"
		Score=Score*3
	ENDIF
	IF Afstand<=Ring5Rad & Afstand>=Ring6Rad THEN
		R$="Bull"
		Score=25
	ENDIF
	IF Afstand<=Ring6Rad THEN
		R$="Bull's Eye"
		Score=50
	ENDIF
	S$=REPLACE$(FORMAT$("##%",Score)," ","")
	GR.TEXT.SIZE 60
	GR.COLOR 255,0,0,0,1
	GR.RECT g,0,870,720,960
	GR.COLOR 255,255,255,255,1
	GR.TEXT.DRAW g,15,945,R$+" : "+S$+" points"
	GR.RENDER
RETURN