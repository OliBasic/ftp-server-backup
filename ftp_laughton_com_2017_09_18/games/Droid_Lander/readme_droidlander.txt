Droid Lander v0.9
by [Antonis]
created on Feb.24.2012

Touch screen when ready to begin landing.
Use left or right part of the screen to change direction.
Avoid passing to close to planets, because you get a crash and stay on the
same level.
Try to collect as many points as possible in 3 minutes.
After 3 minutes -and the time required to complete your last landing-
the game ends & restarts.
Position of points changes in every level, and is visible after you release
droid from the starship ;)
The first 5 levels speed will increase.
Next 5 levels will be harder to turn...
 
