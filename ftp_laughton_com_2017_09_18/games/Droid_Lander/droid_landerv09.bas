REM Droid Lander! v0.9
REM Feb. 23 2012
REM by Antonis
! use left or right
! part of screen
! as controls

dim t[10],n[10],interval[9]
gr.open 255,0,25,0
gr.color 255,255,255,255,0
gr.orientation 1
pause 1000
gr.screen w,h
sx=w/800
sy=h/1232
gr.scale sx,sy
array.load word$[]," +100 "," -100 "," -50 "," +150 "," +200 "~
" -150 "," +50 "," -200 "
array.load colors[],255,255,0, 255,0,255, 0,255,255, 255,65,55~
0,0,255, 0,255,0, 255,155,55, 155,255,55

! first load bitmaps, audio
gr.bitmap.load bptr4,"../../rfo-droidland/data/starr.png"
gr.bitmap.load  bptr2,"../../rfo-droidland/data/lship.png"
gr.bitmap.load  bptr,"../../rfo-droidland/data/droid.png"
gr.bitmap.load bptr3,"../../rfo-droidland/data/planet.png"
audio.load au,"../../rfo-droidland/data/appear.mp3"

! new game
begin:
for i=1 to 70
   gr.bitmap.draw star,bptr4, rnd()*790,150+rnd()*960
next i
gr.color 255,255,255,255,1
gr.line lin,0,1120,799,1120
gr.bitmap.draw plnt1,bptr3, 0,600
gr.bitmap.draw plnt2,bptr3, 685,600
gr.bitmap.draw plnt3,bptr3, 330,400
gr.text.size 15
gr.text.draw mylogo, 590,1228, "Droid Lander v0.9, by [Antonis]"
gr.text.size 39
gr.text.draw tx,10,1210,"Level:  1"
gr.color 255,0,255,0,1
gr.text.draw ttx,200,1210,"Time: 0 s."
gr.color 255,255,0,0,1
gr.text.draw pnt,500,1210,"Points:  0"

speed=1
level=1
difficulty=10
mypoints=0 
timdgame=clock()

start:
! new level

gr.color 255,255,255,255,1
gr.modify tx,"text","Level: "+left$(str$(level),len(str$(level))-2)
gr.bitmap.draw lship,bptr2,0,0
gr.bitmap.draw droid,bptr,50,20
gr.render
pause 1000

! then begin animation of starship
! waiting first touch
audio.stop
audio.play au
myx=0
myy=0
do
 f=clock()
 do
  gr.touch touched,x,y
 until touched>0 | (clock()-f>25)
 if clock()-f>25 & myx+200<800 then 
  myx=myx+3
  gr.modify droid, "x",myx+50
  gr.modify lship, "x",myx
 endif
 if clock()-f>25 & myx+200>=800 then 
  myx=0
  gr.modify droid, "x",myx+50
  gr.modify lship, "x",myx
 endif
 audio.isdone k
 if k>0 then 
    audio.stop
    audio.play au
 endif
 gr.render
until touched>0
gr.color 255,0,25,0,1
gr.modify droid, "x",myx+50
pause 1000

! now fall & play
Array.shuffle  Word$[]
xpos=0
for i=1 to 8
  gr.color 255,colors[3*(i-1)+1] ,colors[3*(i-1)+2], colors[3*(i-1)+3],1
  gr.text.draw t[i],xpos,1160,word$[i]
  gr.text.width n[i], word$[i]
  xpos=xpos+n[i]
  interval[i]=xpos
  gr.render
next i
mx=myx+50
i=1
do
f=clock()
  gr.touch touched,x,y
 a1=gr_collision (droid,plnt1)
 a2=gr_collision (droid,plnt2)
 a3=gr_collision (droid,plnt3)
 if a1<>0 | a2<>0 | a3<>0 then
  ! goto same level
  popup "OUCH..",0,0,0
  tone 800,1000
  points=val(word$[1])
  gr.color 255,255,0,0,1
  secs$=str$((clock()-timdgame)/1000)
  gr.color 255,0,255,0,1
  gr.modify ttx,"text","Time: "+left$(secs$,len(secs$)-2)+" s."
  gr.render
  pause 1000
  for i=1 to 8
   gr.hide t[i]
  next i
  gr.hide droid
  gr.hide lship
  goto start
 ENDIF
 if touched>0 then 
     if x>399*sx & mx<700 then mx=mx+difficulty+speed-1
     if x<=399*sx & mx>0 then  mx=mx-difficulty-speed+1  
 endif
 gr.modify droid, "x",mx
 gr.modify droid, "y",i*3+20
 gr.render
 i=i+speed
 
until i*3>=999

gr.modify droid, "y",1020
gr.render
pause 2000
gr.hide lship
gr.hide droid
 
! goto next level, increase speed or difficulty
level=level+1

if speed<5 then speed=speed+1
if speed>=5 & difficulty>5 then difficulty=difficulty-1

points=val(word$[1])
for i=2 to 8
 if mx+25<=interval[i] & mx+25>interval[i-1] then points=val(word$[i])
next i
mypoints=mypoints+points
gr.color 255,255,0,0,1
gr.modify pnt,"text","Points: "+left$(str$(mypoints),len(str$(mypoints))-2)
secs$=str$((clock()-timdgame)/1000)
gr.color 255,0,255,0,1
gr.modify ttx,"text","Time: "+left$(secs$,len(secs$)-2)+" s."
gr.render
pause 1000
for i=1 to 8
   gr.hide t[i]
next i
if clock()-timdgame>=180000 then 
        popup "G.O. you scored: " +left$(str$(mypoints), len(str$(mypoints))- 2),0,0,4  
		tts.init 
		tts.speak "game over"
 tone 800,800
 pause 5000
 gr.cls
 goto begin
 ! new game
endif
goto start
! new level

end
 
