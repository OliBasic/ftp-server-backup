Rem Running Man
Rem With RFO Basic!
Rem June 2015
Rem Version 1.00
Rem By Roy

di_height = 1230 % set to my Device
di_width = 800

gr.open
gr.set.AntiAlias 1
gr.orientation 1
pause 1000
WakeLock 3 

gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y

gr.set.stroke 2
gr.text.size 40
gosub Functions 
gosub Initialise 
gosub LoadData 
do
	gosub SetUp 
	call LoadBoard(board[]) 
	gosub DrawBoard
	gosub PlayGame
until CurvyWindow(scale_x,scale_y,190,1020,610,1220,4,mess,newBest) = 2

exit

onBackKey:
	dialog.Message"Exit Game?",,yn,"Yes","No"
	if yn=1 then gosub LeaveRunningMan
back.resume
end

DrawBoard:
	call Gcol(1,1) : call Box(0,0,di_width,di_height)
	call Gcol(7,1) : call Box(20,20,di_width - 20, di_height - 20)
	call Gcol(1,1) : call box(0,di_width - 22, di_width,di_width - 2)

	call Gcol(0, 0) 
	for y = 20 to 780 - 63 step 63
		for x = 20 to 780 - 63 step 63
			call Box(x,y,x + 63, y + 63)
		next
	next
	gr.text.align 1
	gosub PutNumber
	gr.bitmap.draw a, arrows,200,di_height - 430
	gr.text.draw sc,50,900,"Score"
	gr.text.draw myScore,50,950, int$(score)
	gr.text.align 3 
	gr.text.draw be,di_width - 50,900,"Best"
	gr.text.draw myBest,di_width - 50,950, int$(best)
	gr.render
return

LoadData:
	gr.bitmap.load arrows , "arrow.png"
	gr.bitmap.scale arrows, arrows, di_width/2, di_width/2
	
	gr.bitmap.load smile , "smile.png"
	gr.bitmap.scale smile, smile,55,55
	
	gr.bitmap.load eval , "eval.png"
	gr.bitmap.scale eval, eval, 55,55
	
	gr.bitmap.load man , "man.png"
	gr.bitmap.scale man, man, 55,55
	
	gr.bitmap.load eaten3 , "eaten3.png"
	gr.bitmap.scale eaten3, eaten3, 55,55
return

PutNumber:
	bx = 1 : by = 1 : call Gcol(1,1)
	for y = 65 to 780 - 20 step 63
		for x = 40 to 780 - 30 step 63
			if board[bx, by] < 7 then gr.text.draw d, x, y, int$(board[bx, by])
			if board[bx, by] = 7 then gr.bitmap.draw s, smile , x-16, y-42
			if board[bx, by] = 8 then gr.bitmap.draw e, eval , x-16, y-42
			if board[bx, by] = 9 then gr.bitmap.draw myMan, man , x-16, y-42
			bx++
		next
		by++ : bx = 1
	next
	gr.render
return

PlayGame:
	call DrawManPath(myMan) : call BringToFront(myMan) : gr.render
	do
		oldScore = score
		do : gr.touch t ,tx, ty : until t
		if buzz then vibrate buzzTouch[], - 1
		call PlaySound(sound, click)
		do : gr.touch t ,tx, ty : until !t
		tx/=(scale_x) : ty/=(scale_y)
		tx = int(tx) : ty = int(ty)
		
		if tx > 340 & tx < 460 & ty > 940 & ty < 1050 then gosub MainMenu
		
		if tx> 200 & tx < 320 & ty > 800 & ty < 910 then gosub GoUpLeft
		if tx> 350 & tx < 450 & ty > 800 & ty < 910 then gosub GoUp
		if tx> 480 & tx < 600 & ty > 800 & ty < 910 then gosub GoUpRight
		
		if tx> 200 & tx < 320 & ty > 940 & ty < 1050 then gosub GoLeft
		if tx> 480 & tx < 600 & ty > 940 & ty < 1050 then gosub GoRight
		
		if tx> 200 & tx < 320 & ty > 1075 & ty < 1190 then gosub GoDownLeft
		if tx> 350 & tx < 450 & ty > 1075 & ty < 1190 then gosub GoDown
		if tx> 480 & tx < 600 & ty > 1075 & ty < 1190 then gosub GoDownRight
		
		call BringToFront(myMan)
		if ! gameOver & BoardFull(board[],MAN_PATH) then 
			call PlaySound(sound, cheer) : pause 1000 : cls
			call LoadBoard(board[]) : gosub DrawBoard : score += 1000
		endif
		
		if gameOver & mess = 1 then 
			gosub GetPosXY
			gr.bitmap.draw e, eaten3, manX, manY
			call BringToFront(eaten3)			
		endif
		if gameOver & score > best then 
			best = score : newBest = TRUE : call SaveBestScore(score) 
			gr.modify myBest, "text", int$(best) : gr.render
		else 
			newBest = FALSE
		endif
	until gameOver
	if sound then gosub GameOverSounds
return

GameOverSounds:
	if mess = 1 then PlaySound(sound, eat)
	if mess = 2 then PlaySound(sound, scream)
	if mess = 3 then PlaySound(sound, huh)
	pause 1000
	if newBest then PlaySound(sound, cheer)
return

GetPosXY:
	bx = 1 : by = 1 : posX = 0 : posY = 0
	for y = 1 to 12
		for x = 1 to 12
			if board[x, y] = MAN_RUNNING then posX = x : posY = y : f_n.break
		next
		if posX > 0 then f_n.break
	next
	gr.get.position myMan, manX, manY
return

GoUpLeft:
	gosub GetPosXY 
	if posX > 1 & posY > 1 then 
		manMove = board[posX - 1 , posY - 1]
		if board[posX - 1 , posY - 1] =  M_SMILE then manMove = 1
		board[posX, posY] = MAN_PATH
		for m = 1 to manMove
			call PlaySound(sound, manRun)
			gr.modify myMan, "x",manX - m * 63 , "y",manY - m * 63 : gr.render
			gameOver = Look(board[], posX - m , posY - m, &score , &mess , gameOver, sound, wow, myScore)
			if ! gameOver & m < manMove & posY - m - 1 < 1 then mess = 2 : gameOver = 1
			if ! gameOver & m < manMove & posX - m - 1 < 1 then mess = 2 : gameOver = 1
			!if ! gameOver & m < manMove & posY - m  < 1 then mess = 2 : gameOver = 1
			!if ! gameOver & m < manMove & posX - m  < 1 then mess = 2 : gameOver = 1
			
			if gameOver & mess = 2 then 
				call DrawManPath(myMan)
				gr.modify myMan, "x",manX - (m + 1) * 53 , "y",manY - (m + 1) * 53 : gr.render
			endif
			
			if gameOver then f_n.break
			board[posX - m, posY - m] = MAN_PATH : call DrawManPath(myMan)
		next
		if posY - manMove >0 & posX - manMove > 0 then board[posX - manMove , posY - manMove] = MAN_RUNNING 
	endif
return

GoUp:
	gosub GetPosXY
	if posY > 1 then
		manMove = board[posX , posY - 1]
		if board[posX , posY - 1] =  M_SMILE then manMove = 1
		board[posX, posY] = MAN_PATH
		for m = 1 to manMove
			call PlaySound(sound, manRun)
			gr.modify myMan, "y",manY - m * 63 : gr.render
			gameOver = Look(board[], posX , posY - m, &score , &mess , gameOver, sound, wow, myScore)
			if ! gameOver & m < manMove & posY - m - 1 < 1 then 
				mess = 2 : gameOver = 1
				call DrawManPath(myMan)
				gr.modify myMan, "y", 0 : gr.render
			endif
			if gameOver then f_n.break
			board[posX, posY - m] = MAN_PATH : call DrawManPath(myMan)
		next
		if posY - manMove >0 then board[posX , posY - manMove] = MAN_RUNNING 
	endif
return

GoUpRight:
	gosub GetPosXY 
	if posX < 12 & posY > 1 then 
		manMove = board[posX + 1 , posY - 1]
		if board[posX + 1 , posY - 1] =  M_SMILE then manMove = 1
		board[posX, posY] = MAN_PATH
		for m = 1 to manMove
			call PlaySound(sound, manRun)
			gr.modify myMan, "x",manX + m * 63 , "y",manY - m * 63 : gr.render
			gameOver = Look(board[], posX + m , posY - m, &score , &mess , gameOver, sound, wow, myScore)
			if ! gameOver & m < manMove & posY - m - 1 < 1 then mess = 2 : gameOver = 1
			if ! gameOver & m < manMove & posX + m + 1 > 12 then mess = 2 : gameOver = 1
			if gameOver & mess = 2 then 
				call DrawManPath(myMan)
				gr.modify myMan, "x",manX + (m + 1) * 53 , "y",manY - (m + 1) * 53 : gr.render
			endif
			if gameOver then f_n.break
			board[posX + m, posY - m] = MAN_PATH : call DrawManPath(myMan)
			
		next
		if posY - manMove >0 & posX + manMove < 13 then board[posX + manMove , posY - manMove] = MAN_RUNNING 
	endif
	
return

GoLeft:
	gosub GetPosXY
	if posX > 1 then
		manMove = board[posX - 1 , posY]
		if board[posX - 1 , posY] =  M_SMILE then manMove = 1
		board[posX, posY] = MAN_PATH
		for m = 1 to manMove
			call PlaySound(sound, manRun)
			gr.modify myMan, "x", manX - m * 63 : gr.render
			gameOver = Look(board[], posX - m, posY, &score , &mess , gameOver, sound, wow, myScore)
			if ! gameOver & m < manMove & posX - m - 1 < 1 then 
				mess = 2 : gameOver = 1
				call DrawManPath(myMan)
				gr.modify myMan, "x", 0 : call BringToFront(myMan) : gr.render
			endif
			board[posX - m, posY] = MAN_PATH : call DrawManPath(myMan)
			if gameOver then f_n.break
		next
		if posX - manMove >0 then board[posX - manMove, posY] = MAN_RUNNING
	endif
return

GoRight:
	gosub GetPosXY
	if posX < 12 then
		manMove = board[posX + 1 , posY]
		if board[posX + 1 , posY] =  M_SMILE then manMove = 1
		board[posX, posY] = MAN_PATH
		for m = 1 to manMove 
			call PlaySound(sound, manRun)
			gr.modify myMan, "x",manX + m * 63 : gr.render
			gameOver = Look(board[], posX + m, posY , &score , &mess , gameOver, sound, wow, myScore)
			if ! gameOver & m < manMove & posX + m + 1 > 12 then 
				mess = 2 : gameOver = 1
				call DrawManPath(myMan)
				gr.modify myMan, "x", 750 : gr.render
			endif
			board[posX + m, posY] = MAN_PATH : call DrawManPath(myMan)
			if gameOver then f_n.break
		next
		if posX + manMove < 13 then board[posX + manMove, posY] = MAN_RUNNING 
	endif
return

GoDownLeft:
	gosub GetPosXY 
	if posX > 1 & posY < 12 then 
		manMove = board[posX - 1 , posY + 1]
		if board[posX - 1 , posY + 1] =  M_SMILE then manMove = 1
		board[posX, posY] = MAN_PATH
		for m = 1 to manMove
			call PlaySound(sound, manRun)
			gr.modify myMan, "x", manX - m * 63 , "y", manY + m * 63 : gr.render
			gameOver = Look(board[], posX - m , posY + m, &score , &mess , gameOver, sound, wow, myScore)
			if ! gameOver & m < manMove & posY + m + 1 > 12 then
				mess = 2 : gameOver = 1
				call DrawManPath(myMan)
				gr.modify myMan, "y", 740 : gr.render
			endif
			if ! gameOver & m < manMove & posX - m - 1 < 1 then
				mess = 2 : gameOver = 1
				call DrawManPath(myMan)
				gr.modify myMan, "x", 0 : gr.render
			endif
			
			if gameOver then f_n.break
			board[posX - m, posY + m] = MAN_PATH : call DrawManPath(myMan)
		next
		if posY + manMove < 13 & posX - manMove > 0 then board[posX - manMove , posY + manMove] = MAN_RUNNING 
	endif
return

GoDown:
	gosub GetPosXY
	if posY < 12 then
		manMove = board[posX , posY + 1]
		if board[posX , posY + 1] =  M_SMILE then manMove = 1
		board[posX, posY] = MAN_PATH
		for m = 1 to manMove
			call PlaySound(sound, manRun)
			gr.modify myMan, "y", manY + m * 63 : gr.render
			gameOver = Look(board[], posX , posY + m, &score , &mess , gameOver, sound, wow, myScore)
			if ! gameOver & m < manMove & posY + m + 1 > 12 then 
				mess = 2 : gameOver = 1
				call DrawManPath(myMan)
				gr.modify myMan, "y" ,740 : gr.render
			endif
			board[posX, posY + m] = MAN_PATH : call DrawManPath(myMan)
			if gameOver then f_n.break
		next
		if posY + manMove < 13 then board[posX , posY + manMove] = MAN_RUNNING 
	endif
return

GoDownRight:
	gosub GetPosXY 
	if posX < 12 & posY < 12 then 
		manMove = board[posX + 1 , posY + 1]
		if board[posX + 1 , posY + 1] =  M_SMILE then manMove = 1
		board[posX, posY] = MAN_PATH
		for m = 1 to manMove
			call PlaySound(sound, manRun)
			gr.modify myMan, "x",manX + m * 63 , "y",manY + m * 63 : gr.render
			gameOver = Look(board[], posX + m , posY + m, &score , &mess , gameOver, sound, wow, myScore)
			if ! gameOver & m < manMove & posY + m + 1 > 12 then
				mess = 2 : gameOver = 1
				call DrawManPath(myMan)
				gr.modify myMan, "y", 740 : gr.render
			endif
			if ! gameOver & m < manMove & posX + m + 1 > 12 then 
				mess = 2 : gameOver = 1 
				call DrawManPath(myMan)
				gr.modify myMan, "x", 750 : gr.render
			endif
			
			if gameOver then f_n.break
			board[posX + m, posY + m] = MAN_PATH : call DrawManPath(myMan)
		next
		if posY + manMove < 13 & posX + manMove < 13 then board[posX + manMove , posY + manMove] = MAN_RUNNING 
	endif
return

Initialise:
	dim board[12,12]
	array.load buzzTouch[], 1, 100
	
	MAN_PATH = 10
	M_SMILE = 7 
	M_EVIL = 8
	MAN_RUNNING = 9
	
	TRUE = 1
	FALSE = 0
	best = LoadBestScore()
	newBest = 0
	sound = TRUE
	buzz = TRUE
	
	soundpool.open 7
	soundpool.load  click,"click.wav"
	soundpool.load  eat,"eat.wav"
	soundpool.load  huh,"huh.wav"
	soundpool.load  cheer,"best.wav"
	soundpool.load  manRun,"manRun.wav"
	soundpool.load  scream,"scream.wav"
	soundpool.load  wow,"wow.wav"
	
	help$ = "Running Man:\n\n" +~

	"Use the Arrow Pad to run your man in the direction of the arrow tapped.\n\n" +~

	"As your man runs over the numbers he gains you points.\n\n" +~

	"Run over a smiley face and gain 100 points.\n\n" +~

	"Clear the board and gain 1000 points and a new board\n\n" +~

	"The game is over if you run your man into an evil monster or he crosses " +~
	"his own path or he runs into the outer perimeter electric fence.\n\n" +~

	"Tap the button in the centre of the Arrow Pad for the Menu.\n\n"
	

return

SetUp:
	gr.cls
	mess = 0
	score = 0 
	gameOver = FALSE
return

MainMenu:
	if sound then sound$="Sound On" else sound$="Sound Off"
	if buzz then vibration$="Vibration On" else vibration$="Vibration Off"
	if buzz then vibrate buzzTouch[], -1
	
	array.load menu$[], sound$, vibration$, "About"~
	"Rules","Reset Best Score", "Exit"
	dialog.select menuSelected, menu$[], "Main Menu"
	array.delete menu$[]
	call PlaySound(sound, click)
	
	Sw.begin menuSelected
		
		Sw.case 1
			if sound then sound = 0 else sound = 1		
			call PlaySound(sound, click)
			Sw.break
		Sw.case 2
			if buzz then buzz = 0 else buzz = 1
			call PlaySound(sound, click)
			Sw.break
		Sw.case 3
			Dialog.Message " About:\n\n",~
			" 	Running Man: for Android\n\n" + ~
			" 	With RFO Basic\n\n" + ~
			" 	June/July 2015\n\n" +~
			" 	Version 1.00\n\n" + ~ 
			" 	Author: Roy Shepherd\n\n", ~
			ok, "OK"
			call PlaySound(sound, click)
			Sw.break
		
		sw.case 4
			dialog.Message " Rules:",help$,ok,"OK"
			call PlaySound(sound, click)
			sw.break
		sw.case 5 
			dialog.Message"Reset Best Score to 0",,yn,"Yes","No"
			if yn=1 then best = 0 : call SaveBestScore(best)
			gr.modify myBest, "text", int$(best) : gr.render
			call PlaySound(sound, click)
			sw.break
		sw.case 6
			dialog.Message"Exit Game?",,yn,"Yes","No"
			call PlaySound(sound, click)
			if yn=1 then gosub LeaveRunningMan
			sw.break
	sw.end
return	

SoundWow:
	if sound then soundpool.play s,wow,0.99,0.99,1,0,1
return

LeaveRunningMan:
	soundpool.release
	wakelock 5
	gr.close
	exit
return

Functions:
fn.def Look(board[], posX , posY, score , mess , gameOver, sound, wow, myScore)
	MAN_PATH = 10
	M_SMILE = 7 
	M_EVIL = 8
	
	if board[posX , posY] = M_SMILE then score += 100 : gosub SoundWow
	if board[posX , posY] = M_EVIL then mess = 1 : gameOver = 1
	if board[posX ,posY] = MAN_PATH then mess = 3 : gameOver = 1
	if ! gameOver & ! board[posX , posY] = M_SMILE then score +=  board[posX , posY]
	gr.modify myScore, "text", int$(score) : gr.render
	fn.rtn gameOver
fn.end

fn.def BoardFull(board[], MAN_PATH)
	M_EVIL = 8
	flag = 1
	for y = 1 to 12
		for x = 1 to 12
			if(board[x, y]) <> MAN_PATH & (board[x, y]) <> M_EVIL then flag = 0 : f_n.break
		next
		if ! flag then f_n.break
	next
	fn.rtn flag
fn.end

fn.def Gcol(c, style)
	if c=0 then gr.color 255,0,0,0,style % black
	if c=1 then gr.color 255,255,0,0,style % Red
	if c=2 then gr.color 255,0,255,0,style % green
	if c=3 then gr.color 255,0,0,255,style % blue
	if c=4 then gr.color 255,255,255,0,style % yellow
	if c=5 then gr.color 255,0,255,255,style % cyan
	if c=6 then gr.color 255,255,0,255,style % magenta
	if c=7 then gr.color 255,255,255,255,style % white
	if c=8 then gr.color 255,192,192,192,style % gray
	if c=9 then gr.color 255,180,180,180,style % dark gray 
	if c=10 then gr.color 255,218, 143, 255,style % Light magenta
fn.end

fn.def Box(x,y,xx,yy)
	gr.rect r , x, y, xx, yy
fn.end

fn.def LoadBoard(board[])
	for y = 1 to 12 
		for x = 1 to 12 
			board[x, y] = floor(6 * rnd() + 1)
		next
	next
	flag = 0 
	!Smile
	do 
		x = floor(12 * rnd() + 1) 
		y = floor(12 * rnd() + 1)
		if board[x, y] < 7 then board[x, y] = 7 : flag++ 
	until flag = 4 
	flag = 0
	!Eval
	do 
		x = floor(12 * rnd() + 1) 
		y = floor(12 * rnd() + 1)
		if board[x, y] < 7 then board[x, y] = 8 : flag++ 
	until flag = 4 
	flag = 0
	!Running Man
	do 
		x = floor(12 * rnd() + 1) 
		y = floor(12 * rnd() + 1)
		if board[x, y] < 7 then board[x, y] = 9 : flag++ 
	until flag = 1
	
fn.end

fn.def CurvyWindow(scale_x,scale_y,x,y,l,h,c1,mess,mess2)
	choice = 0 : call Gcol(0,1) : gr.text.align 1
	gr.rect r,x,y+25,l,h-25 
	gr.rect r,x+25,y,l-25,h 
	gr.circle c, x+25,y+25,25 
	gr.circle c,l-25,y+25,25 
	gr.circle c,x+25,h-25,25
	gr.circle c,l-25,h-25,25
	x += 2 : y += 2 : l -= 2 : h -= 2 : call Gcol(c1,1)
	gr.rect r,x,y+25,l,h-25 
	gr.rect r,x+25,y,l-25,h 
	gr.circle c, x+25,y+25,25 
	gr.circle c,l-25,y+25,25 
	gr.circle c,x+25,h-25,25
	gr.circle c,l-25,h-25,25
	if mess = 1 then m$ = "Eaten by Evil"
	if mess = 2 then m$ = "Electrocuted by Fence"
	if mess = 3 then m$ = "Crossed own Path"
	call Gcol(0,1) : gr.text.width w , m$
	gr.text.draw d, 400 - (w / 2),1080,m$
	if mess2 = 1 then call Gcol(1,1) : gr.text.draw d, 235,1125,"A New Best Score!"
	Call Gcol(7,1) :  gr.rect r, 200,1140,390,1200 : gr.rect r, 410,1140,600,1200 
	Call Gcol(0,0) :  gr.rect r, 200,1140,390,1200 : gr.rect r, 410,1140,600,1200 
	call Gcol(0,1) : gr.text.draw d,255,1185,"New" : gr.text.draw d,470,1185,"Exit"
	gr.render
	do
		do : gr.touch t, tx, ty : until t : do : gr.touch t, tx, ty : until !t
		tx/=(scale_x) : ty/=(scale_y)
		tx = int(tx) : ty = int(ty)
		if tx > 200 & ty > 1140 & tx < 390 & ty < 1200 then choice = 1
		if tx > 410 & ty > 1140 & tx < 600 & ty < 1200 then choice = 2
	until choice > 0
	fn.rtn choice
fn.end

fn.def DrawManPath(myMan)
	gr.get.position myMan, x,y 
	gr.rect r, x, y, x + 55 ,y + 55
fn.end

fn.def BringToFront( ptr )  % ptr is the pointer to become the last one into dl.
  gr.getdl ndl[],1
  array.length sz, ndl[]
  if !ptr | sz =1 | ndl[sz] =ptr
    array.delete ndl[]
    fn.rtn 0
  endif
  array.search ndl[],ptr,n
  if n
    if ndl[n] =ptr
      for nn=n to sz-1
        ndl[nn] = ndl[nn+1]
      next
      ndl[sz] = ptr
      gr.newdl ndl[]
    endif
  endif
  array.delete ndl[]
fn.end

fn.def PlaySound(sound, gameSound)
	if sound then soundpool.play s,gameSound,0.99,0.99,1,0,1
fn.end

fn.def SaveBestScore(score)
	BestScorePath$="../../RunningMan/data/"
	file.exists BestPresent,BestScorePath$+"BestScore.txt"
	if BestPresent then
		text.open w,hs,BestScorePath$+"BestScore.txt"
			text.writeln hs,int$(score)
		text.close hs
	else
		file.mkdir BestScorePath$
		text.open w,hs,BestScorePath$+"BestScore.txt"
			text.writeln hs,int$(score)
		text.close hs
	endif
fn.end

fn.def LoadBestScore()
	BestScorePath$="../../RunningMan/data/"
	file.exists BestPresent,BestScorePath$+"BestScore.txt"
	if BestPresent then
		text.open r,hs,BestScorePath$+"BestScore.txt"
			text.readln hs,b$
			best=val(b$)
		text.close hs
	else
		best = 0
	endif
	fn.rtn best
fn.end

return
