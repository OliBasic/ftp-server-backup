!Start of game
!This is my sensor-based game, Piano Man.

! Open the acclerometer sensor, pause for accuracy
sensors.open 1
pause 500
sensors.read 1, y,x,z

restart:
! These will be my device neutral sizes for height and width:
di_height = 480
di_width = 800

! Open Graphics
gr.open 255, 145, 77, 105
gr.screen actual_w, actual_h
scale_width = actual_w/di_width
scale_height = actual_h/di_height
gr.scale scale_width, scale_height
gr.orientation 0

!Load all of the images we will be using
gr.bitmap.load background, "background.png"
gr.bitmap.load guy1, "guy1.png"
gr.bitmap.load guy2, "guy2.png"
gr.bitmap.load guy3, "guy3.png"
gr.bitmap.load piano, "piano1.png"

!Initial image values/starting positions
gr.bitmap.draw b1pi, background,  1, 1
gr.bitmap.draw guypointer, guy1,  20, ypos
gr.bitmap.draw pianopointer, piano, 50, 1
gr.hide pianopointer 
gr.hide guypointer
gr.render

!Initial variable values
floorlevel = 390
ypos = floorlevel
fallingspeed = 50
ptime = 50
pianox = rnd() * 799
pianoy = 1
pianofallspeed = 20
maxpianofallspeed = 44
pianoshow = 1
whereami = 20
gameover = 0
scoretobeat = 0
calbcount = 0
aaa$ = ">"
touchscreenok = 1
BlockWalk = 0

!Make sure our guy is in the correct starting position
gr.modify guypointer, "x", whereami
gr.modify guypointer, "y", ypos

! Get screen size paramaters
!gr.screen w,h

calibrate:
gr.text.size 25
gr.text.draw message, 1, 250, "Please hold device normally while we calculate a horizontal axis.  "
gr.render
pause 500
gr.modify message, "text", "Please hold device normally while we calculate a horizontal axis.. "
gr.render
pause 500
gr.modify message, "text", "Please hold device normally while we calculate a horizontal axis..."
gr.render
pause 500
sensors.read 1,y,x,z 

! In order to fix a glitch where certain phones either don't detect gravity sensors or
! invert the x and y sensor (weird, right?), I added the following block of code:
! This code still needs work, because phones are all different.
if y >-2 & y < 2 then axis$ = "y"
if x >-2 & x < 2 then axis$ = "x"
if x=0 & y=0 then 
 calbcount = calbcount + 1 
 if calbcount <= 5 then goto calibrate
end if
if x=0 & y=0 & calbcount >= 5 then
 gr.modify message, "text", "Your device apparently doesn't support gravity detection."
 gr.render
 pause 4000
 gr.modify message, "text", "Maybe you should buy an asus eee pad or something...     "
 gr.render
 pause 4000
 gr.modify message, "text", "Ah well, you can still use the touchscreen to move.      "
 gr.render
 pause 4000
end if
gr.modify message, "text", "Please hold device normally while we calculate a horizontal axis...."
gr.render
calbcount = 5
pause 1000


!Now on to the game's code:
levelone:
!11111111111111111111111111111111111111111111111111
scoretobeat=20

gr.modify message, "text", "        LEVEL ONE, Dodge 20 pianos.        "
gr.render
pause 3500
gr.show guypointer
gr.show pianopointer
Gosub mainloop
if Gameover = 1 then goto gameover

gr.hide message
gr.hide guypointer
gr.hide pianopointer
gr.text.draw message, 100, 250, "LEVEL ONE Done. But it's about to get harder."
gr.render
pause 2000
!11111111111111111111111111111111111111111111111111

leveltwo:
!22222222222222222222222222222222222222222222222222
scoretobeat=40
pianofallspeed=20
maxpianofallspeed = 60
gr.bitmap.load background2, "background2.png"
gr.modify b1pi, "bitmap", background2

gr.modify message, "text", "        LEVEL TWO, Dodge 20 more pianos.      "
gr.modify message, "y", 50
gr.render
pause 3500
gr.show guypointer
gr.show pianopointer
Gosub mainloop
!if Gameover = 1 then goto gameover

gr.hide message
gr.hide guypointer
gr.hide pianopointer
gr.text.draw message, 100, 250, "LEVEL TWO Done. Now get ready for a challenge."
gr.render
pause 2000
!222222222222222222222222222222222222222222222222222

levelthree:
!333333333333333333333333333333333333333333333333333
scoretobeat=55
pianofallspeed=20
maxpianofallspeed = 60
gr.bitmap.load background3, "background3.png"
gr.modify b1pi, "bitmap", background3

gr.modify message, "text", "        LEVEL THR33, Dodge 15 more pianos.    "
gr.modify message, "y", 50
gr.render
pause 3500
gr.show guypointer
gr.show pianopointer
Gosub mainloop
if Gameover = 1 then goto gameover

gr.hide message
gr.hide guypointer
gr.hide pianopointer
gr.text.draw message, 100, 50, "LEVEL THREE Done. Enough of the easy stuff."
gr.render
pause 2000
!333333333333333333333333333333333333333333333333333

levelfour:
!444444444444444444444444444444444444444444444444444
scoretobeat=70
pianofallspeed=20
maxpianofallspeed = 60
BlockWalk = 1
gr.bitmap.load background4, "background4.png"
gr.modify b1pi, "bitmap", background4

gr.modify message, "text", "        LEVEL Four, Dodge fifteen pianos.       "
gr.render
pause 3500
gr.modify message, "text", "        Help the guy out of the mud, if you can."
gr.render
pause 3500

gr.show guypointer
gr.show pianopointer
Gosub mainloop
if Gameover = 1 then goto gameover

gr.hide message
gr.hide guypointer
gr.hide pianopointer
gr.text.draw message, 100, 250, "LEVEL Four Done. Now get ready for a challenge."
gr.render
pause 2000
!444444444444444444444444444444444444444444444444444

gr.cls
gr.text.draw page1, 100, 250, "More levels coming soon."
gr.render

pause 3500
gr.close
end


! The main program loop ******************************************************
mainloop:
do
mainloopin:

! Test to see if we've touched the screen
if touchscreenok = 1 then gosub touchscreencheck

! Read the acclerometer
sensors.read 1,y,x,z 
! If their gravity sensors are sideways, we'll correct the x/y axis thing
if axis$ = "x" then y=x
! (or should this be "y=x*-1"??? I need android phones to test this for me!!!)

!use the sensor readings to determine our guy's speed...
if y > 7         then aaa$ = "<<<"
if y > 5 & y < 7 then aaa$ = "<<"
if y > 1 & y < 5 then aaa$ = "<"
if y >-1 & y < 1 then aaa$ = "-"
if y <-1 & y >-5 then aaa$ = ">"
if y <-5 & y >-7 then aaa$ = ">>"
if y <-7         then aaa$ = ">>>"

! Some levels (like the mud level) won't let the guy move without a touchscreen. 
if BlockWalk = 1 then aaa$ = "-"

!...and then move him according to the speed
if aaa$ = ">>>" then 
 if whereami < 750 then whereami = whereami + 50
end if
if aaa$ = ">>" then 
 if whereami < 750 then whereami = whereami + 40
end if
if aaa$ = ">" then 
 if whereami < 750 then whereami = whereami + 20
end if
if aaa$ = "<<<" then 
 if whereami > 50 then whereami = whereami - 50
end if
if aaa$ = "<<" then 
 if whereami > 40 then whereami = whereami - 40
end if
if aaa$ = "<" then 
 if whereami > 20 then whereami = whereami - 20
end if

if pianoshow = 1 then
 gosub pianostep
end if

!Make sure we are walking, then flip the frame for our guy's animation
if !(aaa$ = "-") then
 frame = frame + 1
 if frame = 3 then frame = 0
 if !(falling =1) then animatethis=1
end if

!If we are falling, move our guy down at the fallingspeed
if falling = 1
 if ypos < floorlevel then ypos = ypos + fallingspeed
 if ypos >= floorlevel then 
  falling = 0
  ypos = floorlevel
 end if
!Now animate the fall:
 aaa$="|"
 animatethis=1
end if

!take a little break so it doesn't all happen too fast
pause ptime

!if anything has changed, we draw it
if animatethis = 1 then gosub animate

until (Gameover=1 | score >= scoretobeat)
return
!*******************************************************************************



! Here is the sub where we animate everything, we update the x and y positions
! for the piano and guy, then render them.
! This is also the sub where we check for collision detection.
animate:

if pianoshow = 1 then
 gr.modify pianopointer, "y", pianoy
 gr.modify pianopointer, "x", pianox
end if

!If we're walking, make the guy take a step
if !(aaa$ = "-") then
 if frame = 0 then
  gr.modify guypointer, "bitmap", guy1
 end if
 if frame = 1 then
  gr.modify guypointer, "bitmap", guy2
 end if
 if frame = 2 then
  gr.modify guypointer, "bitmap", guy3
 end if
!Now adjust his x/y coordinates
 gr.modify guypointer, "x", whereami
 gr.modify guypointer, "y", ypos
end if

gr.render
if gr_collision(guypointer, pianopointer) then Gameover = 1

animatethis=0 
return



! This is the sub that updates the piano's y-position depending on the piano's fall speed.
pianostep:

if pianoy + pianofallspeed < floorlevel
  pianoy = pianoy + pianofallspeed
  animatethis=1
end if

!if the piano has hit the floor:
if pianoy + pianofallspeed >= floorlevel then
  pianoy = pianoy + pianofallspeed
  gosub animate  
  pianoy = 1
  pianox = rnd() * 799  
  score = score + 1
  pianofallspeed = pianofallspeed + 1  
  if pianofallspeed > maxpianofallspeed then pianofallspeed = maxpianofallspeed

!here is our random piano generator, a higher score will increase
!likelihood of a bigger piano falling
  ch = round(rnd() * 10) 
  if score > 10 then ch = round (rnd()*8)+2
  if score > 20 then ch = round (rnd()*6)+4
  if score > 40 then ch = round (rnd()*5)+5

  if ch >0 & ch <= 3 then
   gr.bitmap.load piano, "piano1.png"
   gr.modify pianopointer, "bitmap", piano
   animatethis=1
  end if
  if ch > 3 & ch <=6 then
   gr.bitmap.load piano, "piano2.png"
   gr.modify pianopointer, "bitmap", piano
   animatethis=1
  end if
  if ch > 6 & ch < 9 then
   gr.bitmap.load piano, "piano3.png"
   gr.modify pianopointer, "bitmap", piano
   animatethis=1
  end if
  if ch = 9 then
   gr.bitmap.load piano, "pianos.png"
   gr.modify pianopointer, "bitmap", piano
   animatethis=1
  end if
  if ch = 10 then
   gr.bitmap.load piano, "piano4.png"
   gr.modify pianopointer, "bitmap", piano
   animatethis=1
  end if
end if

!if the character is touching the piano...
gameover1=gr_collision (guypointer, pianopointer)

return



! This sub checks to see if the screen is being touched, and if so, if the character is
! being dragged or not. Right now, the touch has to be within 50 pixels of the character's
! x and y coordinates to be considered close enough to drag him.
touchscreencheck:

do
!see if we have a screen touch or not
 gr.touch flag, fingx, fingy

!if yes,...
 if flag then
!Test to see if we're close enough to pick up our guy
  closeenough=0
  if (whereami > ((fingx/scale_width) - 50) & whereami < ((fingx/scale_width) + 50) & ypos > (fingy/scale_height)-50 & ypos < (fingy/scale_height)+50) then
!if we are close enough:
   closeenough=1
   aaa$="0"
   whereami = fingx / scale_width
   ypos = fingy / scale_height
   if pianoshow = 1 then gosub pianostep 
   pause ptime
   gosub animate
   falling = 1
  end if
!if we're not close enough to pick him up, but we did touch the screen,
!we turn the flag off so the game can continue
  if closeenough=0 & flag = 1 then 
   flag = 0
  end if 
 end if
 
!repeat until we aren't touching the screen anymore 
until (flag=0|gameover=1|score>=scoretobeat)

return



! This sub is how we'll handle when a piano hits our guy.
! Technically, it's not a sub right now because it doesn't return, it just immediately
! goes back to level one, but one step at a time for now. :)
gameover:
if Gameover=1 then
 gr.close
 cls
 Print "You have been killed by a falling piano."
 Print "Youre score is: "; (Totalscore + score)
 pause 4000
 score = 0
 goto restart
end if



! this needs work too, but I'll get to it later:
on back key:
pause 750

gr.modify message, "text", "PAUSED, (pause function is still under construction)"
gr.modify message, "y", 25
gr.render
pause 1000
gr.close
goto restart


! unique error message:
!on error:
gr.close
print "There has been an error of some sort."
print "Likely due to solar flares."
print "We are looking into it. Thanks and sorry for the"
print "inconvenience."
pause 5000
end
