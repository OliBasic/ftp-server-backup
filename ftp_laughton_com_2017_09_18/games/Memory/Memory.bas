! Memory
! Aat @2016
FN.DEF GetStartNum(Pics[],n)
	ARRAY.SUM Num,Pics[1,n]:Num=Num-Pics[n]+1:FN.RTN Num
FN.END
FN.DEF AnimatePic(Pics[],Series[],SN)
	s=GetStartNum(Pics[],SN):n=Pics[SN]+s-1:NumTimes=INT(40/Pics[SN])
	FOR j=1 TO NumTimes
		FOR i=s TO n
			GR.SHOW Series[i,1]:GR.SHOW Series[i,2]:GR.RENDER
			GR.HIDE Series[i,1]:GR.HIDE Series[i,2]:PAUSE 100
		NEXT i
	NEXT j
	GR.SHOW Series[s,1]:GR.SHOW Series[s,2]:GR.RENDER
FN.END
FN.DEF HidePic(Pics[],Series[],SN)
	s=GetStartNum(Pics[],SN):n=Pics[SN]+s-1
	FOR i=s TO n
		GR.HIDE Series[i,1]:GR.HIDE Series[i,2]
	NEXT i
FN.END
GR.OPEN 255,0,0,255,0,0
GR.SCREEN w,h:ScaleX=1280:ScaleY=h/w*ScaleX:sx=w/ScaleX:sy=h/ScaleY:GR.SCALE sx,sy
ARRAY.LOAD NumberOfPics[],4,8,5,7,4,2,8,8,4,8,12,2,7,6
ARRAY.LOAD FName$[],"Beaver","Car","Girl","Gorilla","Alien","Angel","Castle","Clown","Horses","Kid","Panda","Queen","Storch","Toucan"
ARRAY.SUM Total,NumberOfPics[]
DIM PicPointers[Total],PicSeries[Total,2],M[28],Tiles[28],Positions[28]
DataPath$="../../Memory/data/"
GR.TEXT.BOLD 1
GR.BITMAP.CREATE Cover,160,150
GR.BITMAP.DRAWINTO.START Cover
	GR.COLOR 255,255,255,0,1:GR.RECT g,0,0,160,145
	GR.COLOR 255,0,255,0,1:	GR.RECT g,20,20,140,125
	GR.COLOR 255,255,0,0,0
	FOR j=23 TO 123 STEP 20
		FOR i=20 TO 140 STEP 20
			GR.CIRCLE g,i,j,10
		NEXT i
	NEXT j
	GR.COLOR 255,0,128,255,1:GR.TEXT.SIZE 100
	GR.TEXT.DRAW g,35,110,"M"
GR.BITMAP.DRAWINTO.END
GR.BITMAP.SAVE Cover,DataPath$+"icon.png"
GR.BITMAP.CREATE Button,70,60
GR.BITMAP.DRAWINTO.START Button
	GR.COLOR 255,155,155,0,1:GR.RECT g,2,2,68,58
	GR.COLOR 255,255,255,0,0:GR.RECT g,0,0,70,60
	GR.COLOR 225,0,255,0,1:GR.TEXT.SIZE 20:GR.TEXT.DRAW g,10,40,"cheat"
GR.BITMAP.DRAWINTO.END
GR.BITMAP.DRAW CBtn,Button,740,20:GR.HIDE CBtn
NumPnt=0:MakeCheat=0:Sound=1:Snd$=" OFF"
FOR j=1 TO 14
	FOR i=1 TO NumberOfPics[j]
		NumPnt=NumPnt+1
		GR.BITMAP.LOAD PicPointers[NumPnt],DataPath$+FName$[j]+REPLACE$(FORMAT$("%%",i)," ","")+".png"
	NEXT i
NEXT j
GR.SET.STROKE 20:GR.COLOR 255,255,0,0,1:GR.LINE g,-10,80,100,-10
GR.SET.STROKE 0:GR.COLOR 255,0,0,0,1:GR.TEXT.SIZE 12
GR.ROTATE.START -40,10,70:GR.TEXT.DRAW g,10,70,"RFO-BASIC GAME":GR.ROTATE.END:GR.TEXT.SIZE 20
GR.COLOR 255,255,255,0,1:GR.TEXT.DRAW g,420,80,"Matches":GR.TEXT.DRAW g,845,80,"Turns":GR.TEXT.DRAW g,1020,80,"Sound is"
GR.TEXT.DRAW ShowSnd,1100,80," ON"
GR.TEXT.SIZE 80:GR.TEXT.DRAW g,90,80,"Memory"
GR.TEXT.DRAW Score,500,80,"0/14"
GR.TEXT.DRAW Clicks,900,80,"0"
GR.COLOR 255,200,200,255,1
FOR i=0 TO 3
	FOR j=0 TO 6
		GR.RECT Tiles[i*7+j+1],j*165+5,i*150+100,j*165+165,i*150+245
	NEXT j
NEXT i
FOR i=1 TO 28:Positions[i]=i:NEXT i
ARRAY.SHUFFLE Positions[]:NumPnt=0
FOR q=1 TO 14
	XPos1=MOD((Positions[q*2-1]-1),7)+1:YPos1=INT((Positions[q*2-1]-1)/7)+1
	XPos2=MOD((Positions[q*2]-1),7)+1:YPos2=INT((Positions[q*2]-1)/7)+1
	GR.BITMAP.SIZE PicPointers[NumPnt+1],BmpW,BmpH:XOffSet=(165-BmpW)/2:YOffSet=(150-BmpH)/2
	FOR i=1 TO NumberOfPics[q]
		NumPnt=NumPnt+1
		GR.BITMAP.DRAW PicSeries[NumPnt,1],PicPointers[NumPnt],XOffSet+(XPos1-1)*165+5,YOffSet+(YPos1-1)*150+100
		GR.BITMAP.DRAW PicSeries[NumPnt,2],PicPointers[NumPnt],XOffSet+(XPos2-1)*165+5,YOffSet+(YPos2-1)*150+100
		GR.HIDE PicSeries[NumPnt,1]:GR.HIDE PicSeries[NumPnt,2]
	NEXT i
	GR.SHOW PicSeries[GetStartNum(NumberOfPics[],q),1]
	GR.SHOW PicSeries[GetStartNum(NumberOfPics[],q),2]
NEXT q
! Draw covers
FOR i=0 TO 3
	FOR j=0 TO 6
		GR.BITMAP.DRAW M[i*7+j+1],Cover,j*165+5,i*150+100
	NEXT j
NEXT i
GR.RENDER:NumCorrect=0:Kliks=0
DO
	POPUP "TAP ON THE FIRST CARD":IF Sound THEN TONE 330,200,0
	DO
		IF MakeCheat=1 THEN GR.SHOW CBtn:GR.RENDER:Cheat=1
		GOSUB GetTouch
		IF MakeCheat=1 THEN Cheat=0:GR.HIDE CBtn
		Cover1Num=(x-1)+(y-1)*7+1
		ARRAY.SEARCH Positions[],Cover1Num,P1
	UNTIL P1>0
	GR.HIDE M[Cover1Num]
	Kliks=Kliks+1:GR.MODIFY Clicks,"text",INT$(Kliks):GR.RENDER
	POPUP "TAP ON THE SECOND CARD":IF Sound THEN TONE 440,200,0
	DO
		GOSUB GetTouch
		Cover2Num=(x-1)+(y-1)*7+1
		ARRAY.SEARCH Positions[],Cover2Num,P2
	UNTIL Cover1Num<>Cover2Num & P2>0
	GR.HIDE M[Cover2Num]:GR.RENDER:Hit=0
	IF P1/2<>INT(P1/2) THEN
		IF Positions[P1+1]=Cover2Num THEN
			Hit=P2/2:Positions[P1]=0:Positions[P1+1]=0
		ENDIF
	ELSE
		IF Positions[P1-1]=Cover2Num THEN
			Hit=P1/2:Positions[P1]=0:Positions[P1-1]=0
		ENDIF
	ENDIF
	IF !Hit THEN
		POPUP "SORRY, NOT MATCHING":IF Sound THEN TONE 550,200,0
		PAUSE 3000:GR.SHOW M[Cover1Num]:GR.SHOW M[Cover2Num]:GR.RENDER
	ELSE
		POPUP "YESSSS, A MATCHING PAIR":IF Sound THEN TONE 660,200,0
		NumCorrect=NumCorrect+1
		GR.MODIFY Score,"text",INT$(NumCorrect)+"/14"
		AnimatePic(NumberOfPics[],PicSeries[],Hit):PAUSE 1000
		GR.HIDE Tiles[Cover1Num]:GR.HIDE Tiles[Cover2Num]
		HidePic(NumberOfPics[],PicSeries[],Hit)
		GR.GETDL R[],1:ARRAY.LENGTH L,R[]:ARRAY.SEARCH R[],M[Cover1Num],S:SWAP R[S],R[L-1]
		ARRAY.SEARCH R[],M[Cover2Num],S:SWAP R[S],R[L]:GR.NEWDL R[]:ARRAY.DELETE R[]
		GR.SHOW M[Cover1Num]:GR.SHOW M[Cover2Num]
		Ep=1250:Sp1=5+MOD(Cover1Num-1,7)*165:s1=(Ep-Sp1)/10
		Sp2=5+MOD(Cover2Num-1,7)*165:s2=(Ep-Sp2)/10
		FOR i=1 TO 10
			Sp1=SP1+s1:Sp2=SP2+s2:GR.MODIFY M[Cover1Num],"x",SP1:GR.MODIFY M[Cover2Num],"x",Sp2:GR.RENDER:PAUSE 100
		NEXT i
		GR.MODIFY M[Cover1Num],"y",NumCorrect*40:GR.MODIFY M[Cover2Num],"y",NumCorrect*40:GR.RENDER
	ENDIF
UNTIL NumCorrect=14
DIALOG.MESSAGE "YOU USED "+INT$(Kliks)+" TURNS TO FINISH THIS GAME !","Tap outside this box to quit.",x
EXIT
GetTouch:
	DO:GR.TOUCH Touched,x,y:UNTIL Touched
	DO:GR.TOUCH Touched,x,y:UNTIL !Touched
	x/=sx:y/=sy
	IF x>740 & x<810 & y>20 & y<80 & Cheat THEN
		FOR i=0 TO 3:FOR j=0 TO 6:GR.HIDE M[i*7+j+1]:NEXT j:NEXT i:GR.RENDER:PAUSE 3000
		FOR i=0 TO 3:FOR j=0 TO 6:GR.SHOW M[i*7+j+1]:NEXT j:NEXT i:GR.RENDER
	ELSE
		IF x<1155 & y>100 THEN
			x=INT((x-5)/165)+1:y=INT((y-100)/150)+1
		ENDIF
	ENDIF
RETURN
ONBACKKEY:
	DIALOG.MESSAGE "Memory","Please, select",Answer1,"Options","QUIT","Cancel"
	IF Answer1=1 THEN
		DIALOG.MESSAGE "Memory Options","Please, select",Answer2,"Set/ReSet Cheat Button","Turn sound"+Snd$,"Cancel"
		IF Answer2=1 THEN
			IF MakeCheat THEN
				MakeCheat=0:Cheat=0:GR.HIDE CBtn:GR.RENDER
			ELSE
				MakeCheat=1:Cheat=1:GR.SHOW CBtn:GR.RENDER
			ENDIF
		ENDIF
		IF Answer2=2 THEN
			GR.MODIFY ShowSnd,"text",Snd$:GR.RENDER
			IF Sound THEN
				Sound=0:Snd$=" ON"
			ELSE
				Sound=1:Snd$=" OFF":TONE 440,200,0
			ENDIF
		ENDIF
	ENDIF
	IF Answer1=2 THEN EXIT
BACK.RESUME
