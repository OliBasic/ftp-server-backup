Rem Name that Dog breed
Rem For Android
Rem With RFO Basic!
Rem Febuary 2017
Rem Version 1.00
Rem Author Roy Shepherd

PORTRAIT = 1 : LANDSCAPE = 0
orientation = PORTRAIT
now = time()
if orientation = LANDSCAPE then
    di_height = 672
    di_width = 1150
elseif orientation = PORTRAIT then
    di_height = 1150
    di_width = 672
endif

gr.open 255,117,117,255
gr.color 255, 255, 255, 255
gr.text.size 30
gr.orientation orientation
pause 1000
WakeLock 3
 
gr.screen screenWidth, screenHeight
scale_x = screenWidth / di_width
scale_y = screenHeight / di_height
gr.scale scale_x, scale_y

gosub Functions
gosub Initialise
gosub DrawDropDownMenu
gosub DrawAboutBox

gosub Setup
gosub PlayNameDogs

end

!------------------------------------------------
! Double tap back key to end
!------------------------------------------------
onBackKey:
    backKeyHit = BackKey(backKeyHit)
back.resume

!------------------------------------------------
! Touch interrupt
!------------------------------------------------
OnGrTouch:
    call TouchButton()
Gr.onGrTouch.resume

!------------------------------------------------
! Do once at first run of app
!------------------------------------------------
Initialise:
    TRUE = 1 : FALSE = 0
    sound = TRUE : buzz = TRUE
    roundNumber = 1 : numberDog = 0
    backTick = 0 % for double tap back key
    array.load breedPos[], 1, 2, 3 : dim randomDog[2], dog[20]
    array.load dogOrder[], 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, ~
                            12, 13, 14, 15, 16, 17, 18, 19, 20
    path$ = "NameDogs/data/"
    gosub LoadData
    
    ! Heading 
    gr.color 255, 100, 100, 100
    gr.rect null , 0, 0, di_width, 50 
    gr.color 255, 255, 0, 0
    gr.line null , 0, 50, di_width, 50
    gr.color 255, 255, 255, 255 : gr.text.size 30
    gr.text.align 2 : gr.text.draw null, di_width / 2, 30, "Name Dog Breed"
    
    ! Load and display the options button
    gr.bitmap.load option, path$ + "optionList.png"
    call ImageLoadError(option, "optionsList.png")
    gr.bitmap.draw menu, option, di_width - 50, 0
    
    ! Load the dogs 
    for x = 1 to 20
        gr.bitmap.load d, path$ + "dog0" + int$(x) + ".jpg"
        call ImageLoadError(d, "dog0" + int$(x) + ".jpg")
        gr.bitmap.scale dog, d, di_width, 500
        gr.bitmap.draw dog[x], dog, 0, 130
        gr.bitmap.delete d
    next
    
    ! Group all dogs
    gr.group allDogs, dog[1], dog[2], dog[3], dog[4], dog[5], dog[6], dog[7], dog[8], dog[9], dog[10], ~
                      dog[11], dog[12], dog[13], dog[14], dog[15], dog[16], dog[17], dog[18], dog[19], dog[20]
    gr.hide allDogs
    
    gr.text.align 2
    ! Draw Select breed Buttons and text
    ! Setup colours for buttons
    gr.color 255, 255, 255, 255 : gr.paint.get white
    gr.color 255, 0, 0, 0       : gr.paint.get black
    gr.color 255, 255, 0, 0     : gr.paint.get red
    gr.color 255, 0, 255, 0     : gr.paint.get green
    gr.color 255, 0, 0, 255     : gr.paint.get blue
    gr.color 255, 255, 255, 0   : gr.paint.get yellow
    
    dim butBreed[3], txtBreed[3]
    
    y = di_height - 450 : x = di_width / 2
    for breed = 1 to 3
        ! Buttons
        gr.rect butBreed[breed], x - 200, y - 50, x + 200, y + 30
        gr.modify butBreed[breed], "paint", yellow
        ! Text
        gr.text.draw txtBreed[breed], x, y, "English Cocker Spaniel"
        gr.modify txtBreed[breed], "paint", black
        y += 90
    next
    
    ! Next dog and New Round buttons and texts
    y += 50
    gr.rect butNextDog, x - 200, y - 50, x + 200, y + 30
    gr.rect butNextRound, x - 200, y - 50, x + 200, y + 30
    gr.modify butNextDog, "paint", black
    gr.modify butNextRound, "paint", black
    gr.text.draw txtNextDog, x, y, "Next Dog"
    gr.text.draw txtNextRound, x, y, "Start Next Round"
    gr.modify txtNextDog, "paint", white
    gr.modify txtNextRound, "paint", white
    gr.group nextDogButton, butNextDog, txtNextDog
    gr.hide nextDogButton
    gr.hide butNextRound : gr.hide txtNextRound
    
    
    ! Draw Score, Menu and Best text
    gr.text.align 1 : gr.text.draw txtScore, 30, 100, "Score: " + int$(score)
    gr.text.align 2 : gr.text.draw txtDogs, x, 100, "Dog: " + int$(numberDog)
    gr.text.align 3 : gr.text.draw txtWrong, di_width - 30, 100, "Wrong: " + int$(wrong)
    gr.text.align 1 : gr.color 255, 255, 255, 255
    
    ! Setup for collision for buttons tapper
    gr.color 0
    gr.point collision, -1, -1
    gr.color 255
    
    gosub LoadDogBreedNames
    
    ! Load sounds
    soundpool.open 3
    soundpool.load bark, path$ + "bark.wav"
    call SoundLoadError(bark, "bark")
    soundpool.load whimper, path$ + "whimper.wav"
    call SoundLoadError(whimper, "whimper")
    soundpool.load howling, path$ + "howling.wav"
    call SoundLoadError(howling, "howling")

return

!------------------------------------------------
! Do at start of each round
!------------------------------------------------
Setup:
    score = 0
    wrong = 0
    array.shuffle dogOrder[]
    gosub UpDateScore
    gosub UpDateDogNumber
return

!------------------------------------------------
! Load the dog breed names
!------------------------------------------------
LoadDogBreedNames:
    grabfile d$, path$ + "DogBreeds.txt"
    if d$ = "" then ?"Can't load Dog Breeds" : end
    split dogBreed$[], d$, "\n"
return

!------------------------------------------------
! Play game
!------------------------------------------------
PlayNameDogs:
    do
        goodDog = 0
        showNextDog = FALSE
        !thisDog  = floor(20 * rnd()) + 1
        thisDog = dogOrder[numberDog]
        gr.show dog[thisDog] 
        array.shuffle breedPos[]
        
        do
            do
                randomDog[1] = floor(20 * rnd()) + 1
                randomDog[2] = floor(20 * rnd()) + 1
            until randomDog[1] <> randomDog[2] 
        until thisDog <> randomDog[1] & thisDog <> randomDog[2]
        
        gr.modify txtBreed[breedPos[1]], "text", dogBreed$[thisDog]
        gr.modify butBreed[breedPos[1]], "paint", yellow
        gr.modify txtBreed[breedPos[2]], "text", dogBreed$[randomDog[1]]
        gr.modify butBreed[breedPos[2]], "paint", yellow
        gr.modify txtBreed[breedPos[3]], "text", dogBreed$[randomDog[2]]
        gr.modify butBreed[breedPos[3]], "paint", yellow
        call Render()
        do : pause 10 : until goodDog
        if goodDog then 
            gosub UpDateScore
            
            if numberDog = 10 then 
                if sound then pause 500
                call PlaySound(sound, howling)
                gosub EndOfRound
            endif
            
            gr.show nextDogButton
            call Render()
            do : pause 10 : until showNextDog
            gr.hide dog[thisDog]
            gr.hide nextDogButton
            showNextDog = FALSE
            gosub UpDateDogNumber
        endif
    until 0
return

!------------------------------------------------
! Update the score or wrong
!------------------------------------------------
UpDateScore:
    gr. modify txtScore, "text", "Score: " + int$(score)
    gr. modify txtWrong, "text", "Wrong: " + int$(wrong)
return

!------------------------------------------------
! Update the dog number
!------------------------------------------------
UpDateDogNumber:
    numberDog ++
    gr. modify txtDogs, "text", "Dog: " + int$(numberDog)
return

!------------------------------------------------
! Show results for this round
!------------------------------------------------
EndOfRound:
    call Render()
    do
        dialog.message "End of Round: " + int$(roundNumber), int$(score) + " out of 10", OK,"OK"
    until OK > 0
    roundNumber ++ : numberDog = - 1
    gosub Setup
    call Render()
return

!------------------------------------------------
! Draw Drop Down Menu
!------------------------------------------------
DrawDropDownMenu:
    dropX = 450
    dropY = 50
    dropText$="Help-About-Breed Info-Toggle Sound-Toggle Buzz"
    
    textX = dropX + 10 : textY = dropY + 30 : wordLen = 0
    lineY = dropY + 40 :  rectY = dropY
    split dropMenu$[], dropText$ , "-"
    array.length menuItems, dropMenu$[]

    wordLen = len(dropMenu$[1])
    for m = 1 to menuItems
        if m > 1 then
            if len(dropMenu$[m]) > wordLen then wordLen = len(dropMenu$[m])
        endif
    next

    menuWidth = wordLen * 20 : menuDrop = menuItems * 40
    dim optionsPtr[ (menuItems * 2) + 1]
    gr.color 255, 255, 255, 255
    gr.rect optionsPtr[1], dropX, dropY, dropX + menuWidth, dropY + menuDrop
    gr.color 255,0,0,0 : gr.text.align 1 : gr.text.size 30
    
    for m = 1 to menuItems
        gr.text.draw optionsPtr[m + 1], textX, textY, dropMenu$[m]
        textY += 40
    next
   
    gr.color 255,187,187,187
   
    for m = 1 to menuItems - 1
        gr.line optionsPtr[menuItems + m + 2], dropX, lineY, dropX + menuWidth, lineY
        lineY += 40
    next
    
    gr.color 255,0, 0, 0, 0
    ! Collision rectangles for menu items
    dim menuSelect[menuItems]
    for m = 1 to menuItems
        gr.rect menuSelect[m], dropX, rectY, dropX + menuWidth, rectY + 40
        rectY += 40
    next
    
    gr.color 255, 0, 0, 0
    for m = 1 to (menuItems * 2) + 1
        gr.hide optionsPtr[m]
        if m <= menuItems then gr.hide menuSelect[m]
    next
    
return

!------------------------------------------------
! Draw About box
!------------------------------------------------
DrawAboutBox:
    aboutText$="Name Dog Breed-Febuary 2017-Version 1.00-Author: Roy Shepherd-OK"
    dWidth = di_width / 2 : dHeight = (di_height / 2) - 200
    
    split aboutMenu$[], aboutText$ , "-"
    array.length aboutItems, aboutMenu$[]

    wordLen = len(aboutMenu$[1])
    for m = 1 to aboutItems
        if m > 1 then
            if len(aboutMenu$[m]) > wordLen then wordLen = len(aboutMenu$[m])
        endif
    next

    menuWidth = wordLen * 20 : menuDrop = aboutItems * 40
    dim aboutPtr[ aboutItems + 2]
    gr.color 255, 255, 255, 255, 1
    gr.rect aboutPtr[1],dWidth - (menuWidth / 2), dHeight - (menuDrop / 2), dWidth + (menuWidth / 2), dHeight + (menuDrop / 2)
    gr.color 255,0,0,0 : gr.text.align 2 : gr.text.size 30
    
    textY = dHeight - (menuDrop / 2) + 30
    for m = 1 to aboutItems
        gr.text.draw aboutPtr[m + 1], dWidth, textY, aboutMenu$[m]
        textY += 40
    next
   
    gr.color 255,187,187,187
   
    gr.line aboutPtr[aboutItems + 2], dWidth - (menuWidth / 2), textY - 70, dWidth + (menuWidth / 2), textY - 70
    
    gr.color 255, 0, 0, 0
    for m = 1 to aboutItems + 2
        gr.hide aboutPtr[m]
    next
  
return

!------------------------------------------------
! Save sound and Buzz
!------------------------------------------------
SaveData:
    file.exists pathPresent,path$ + "GameData.txt"
    if ! pathPresent then file.mkdir path$
    text.open w,hs,Path$ + "GameData.txt"
    text.writeln hs,int$(sound)
    text.writeln hs,int$(buzz)
    text.close hs
return

!------------------------------------------------
! Load sound, and Buzz
!------------------------------------------------
LoadData:
    file.exists pathPresent,path$ + "GameData.txt"
    if pathPresent then
        text.open r,hs,path$ + "GameData.txt"
        text.readln hs,sound$
        text.readln hs,buzz$
        text.close hs
        sound = val(sound$)
        buzz = val(buzz$)
    endif
return
       
!------------------------------------------------
! All functions below
!------------------------------------------------
Functions:

!------------------------------------------------
! Show Drop Down Menu
!------------------------------------------------
fn.def ShowDropMenu()
    call Buzzer(buzz)
    flag = 0
    for m = 1 to (menuItems * 2) + 1
        gr.show optionsPtr[m]
        if m <= menuItems then gr.show menuSelect[m]
    next
    
    call Render()
    do : gr.touch t, tx, ty : until ! t
    do 
        gr.touch t, tx, ty
        if t then 
            do : gr.touch t, tx, ty : until ! t
            flag = 1
            tx /= scale_x : ty /= scale_y
            gr.modify collision, "x", tx, "y", ty
            if gr_collision(collision, menuSelect[1]) then call Help()
            if gr_collision(collision, menuSelect[2]) then call About()
            if gr_collision(collision, menuSelect[3]) then call BreedInfo()
            if gr_collision(collision, menuSelect[4]) then 
                if sound then 
                    call Buzzer(buzz)
                    sound = FALSE : popup "Sound Off"
                else
                    call Buzzer(buzz)
                    sound = TRUE : popup "Sound On"
                endif
            endif
            
            if gr_collision(collision, menuSelect[5]) then 
                if buzz then
                    call Buzzer(buzz)
                    buzz = FALSE : popup "Buzz Off"
                else 
                    call Buzzer(buzz)
                    buzz = TRUE : popup "Buzz On"
                endif
            endif
        endif
    until flag
    for m = 1 to (menuItems * 2) + 1
        gr.hide optionsPtr[m]
        if m <= menuItems then gr.hide menuSelect[m]
    next
    call Render()
fn.end

!------------------------------------------------
! Display the About box
!------------------------------------------------
fn.def About()
    for m = 1 to aboutItems + 2
        gr.show aboutPtr[m]
    next
    call Render()
    call Buzzer(buzz)
    do : gr.touch t, tx, ty : until ! t
    do : gr.touch t, tx, ty : until t
    do : gr.touch t, tx, ty : until ! t
    call Buzzer(buzz)
    for m = 1 to aboutItems + 2
        gr.hide aboutPtr[m]
    next
    call Render()
fn.end

!------------------------------------------------
! Display info About the dog breed on display
!------------------------------------------------
fn.def BreedInfo()
    call Buzzer(buzz)
    grabfile d$, path$ + "DogsInfo.txt"
    if d$ = "" then ?"Can't load Dog Info" : end
    split dogInfo$[], d$, "#"
    array.load info$[],dogInfo$[thisDog]
    dialog.select null, info$[]
    array.delete info$[]
    array.delete dogInfo$[]
fn.end

!------------------------------------------------
! Has a button been tapped 
!------------------------------------------------
    fn.def TouchButton()
        if time() - now < 5000 then fn.rtn 0
        gr.touch t, tx, ty 
        tx /= scale_x : ty /= scale_y
        gr.modify collision, "x", tx, "y", ty
        if gr_collision(collision, menu) then 
            call ShowDropMenu()
            call Render()
        endif
        if ! goodDog then
            if gr_collision(collision, butBreed[breedPos[1]]) then 
                gr.modify butBreed[breedPos[1]], "paint", green
                call PlaySound(sound, bark) : call Buzzer(buzz)
                goodDog = 1 : score ++
            endif
            
            if gr_collision(collision, butBreed[breedPos[2]]) then 
                gr.modify butBreed[breedPos[2]], "paint", red
                gr.modify butBreed[breedPos[1]], "paint", green
                call PlaySound(sound, whimper) : call Buzzer(buzz)
                goodDog = 2 : wrong ++
            endif
            
            if gr_collision(collision, butBreed[breedPos[3]]) then 
                gr.modify butBreed[breedPos[3]], "paint", red
                gr.modify butBreed[breedPos[1]], "paint", green
                call PlaySound(sound, whimper) : call Buzzer(buzz)
                goodDog = 2 : wrong ++
            endif
        endif
        
        if gr_collision(collision, butNextDog) then 
            showNextDog = TRUE : call Buzzer(buzz)
        endif
        call Render()
        
    fn.end
    
!----------------------------------------------------
! Display game Help
!----------------------------------------------------
    fn.def Help()
        call Buzzer(buzz)
        grabfile help$, path$ + "DogHelp.txt"
        if help$ = "" then ?"Can't load help" : end
        array.load info$[],help$
        dialog.select null, info$[]
        array.delete info$[]
    fn.end
    
!------------------------------------------------
! Render if not in background
!------------------------------------------------
    fn.def Render()
        if ! background() then gr.render
    fn.end
    
!------------------------------------------------
! if sound then Play sound (ptr)
!------------------------------------------------
    fn.def PlaySound(sound, ptr)
        if sound then soundpool.play s,ptr,0.99,0.99,1,0,1
    fn.end
    
!------------------------------------------------
! if buzz then  vibrate
!------------------------------------------------
    fn.def Buzzer(buzz)
        if buzz then 
            array.load buzzGame[], 1, 100
            vibrate buzzGame[], -1
            array.delete buzzGame[]
        endif
    fn.end
  
!------------------------------------------------
! Load Sound files not found
!------------------------------------------------
    fn.def SoundLoadError(ptr, e$)
        if ptr = 0 then
            dialog.message "Sound file' " + e$ + " 'not found", "Game will end",OK,"OK" 
            end
        endif
    fn.end
    
!------------------------------------------------
! Load image file not found
!------------------------------------------------
    fn.def ImageLoadError(ptr, e$)
        if ptr =- 1 then
            dialog.message "Image file' " + e$ + " 'not found", "Game will end",OK,"OK" 
            end
        endif
    fn.end

!------------------------------------------------
! Double tap Back Key with in three seconds to end
!------------------------------------------------
    fn.def BackKey(backKeyHit)
        inTime = 3 % Three seconds 
        if backKeyHit = 3 then backKeyHit = 0
        if backKeyHit = 1 then 
            outTime = time() - backTick 
            outTime /= 1000 
            if outTime > inTime then backKeyHit = 0
        endif
        backKeyHit ++
        if backKeyHit = 1 then 
            backTick = time() 
            popup "Press again to Exit",, + (screenHeight / 2)
        endif
        
        if backKeyHit = 2 then 
            backTock = time() - backTick
            backTock /= 1000
            if backTock <= inTime then gosub SaveData : exit
        endif
        fn.rtn backKeyHit
    fn.end

return
