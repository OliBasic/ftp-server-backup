! BrokenWords requires RFO-Basic v1.88
! Aat Don @2015
GR.OPEN 255,255,255,255,0,1
GR.SCREEN w,h
ScaleX=600
ScaleY=h/w*ScaleX
IF ScaleY<820 THEN ScaleY=820
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
WAKELOCK 3
DataPath$="../../BrokenWords/data/"
Game$="Broken Words"
DIM Sector[12],SectorFill[12],SectorData[12],SectorAngles[12],SectorPresent[12],eX[108],eY[108],LineHolder[6],PrLine$[6],DisplayLine$[6],SqTrns[10]
DIM Proverbs$[10],Expl$[10],NumGames[20]
DIM FR[5]
FOR i=1 TO 5
	LIST.CREATE N,FR[i]
NEXT i
LIST.ADD FR[1],0,0,59,0,68,35,83,57,109,72,149,149,92,105,66,99,42,75,0,58
LIST.ADD FR[2],136,0,136,29,125,61,149,149,168,97,168,67,224,0
LIST.ADD FR[3],300,26,232,89,214,90,149,149,180,128,216,142,251,138,300,148
LIST.ADD FR[4],300,300,300,249,283,213,221,202,178,158,149,149,191,235,219,252,221,300
LIST.ADD FR[5],0,300,0,219,72,195,79,194,79,173,149,149,107,183,108,196,66,232,39,300
ARRAY.LOAD N$[],"first","second","third","fourth","fifth","sixth","seventh","eighth","ninth","tenth"
AUDIO.LOAD Crash,DataPath$+"Glass.mp3"
AUDIO.LOAD GlOut,DataPath$+"Glass_Out.mp3"
AUDIO.LOAD Beep,DataPath$+"Beep.mp3"
AUDIO.LOAD Correct,DataPath$+"Correct.mp3"
AUDIO.LOAD Wrong,DataPath$+"Wrong.mp3"
GR.TEXT.SIZE 50
GR.TEXT.BOLD 1
GR.SET.STROKE 16
GR.BITMAP.CREATE Button,300,150
GR.BITMAP.DRAWINTO.START Button
	GR.COLOR 255,255,255,255,0
	GR.CIRCLE g,75,75,67
	GR.CIRCLE g,225,75,67
	GR.RECT g,75,8,225,142
	GR.COLOR 255,215,140,215,1
	GR.CIRCLE g,75,75,60
	GR.CIRCLE g,225,75,60
	GR.RECT g,75,15,225,135
	GR.COLOR 255,230,195,230,1
	GR.CIRCLE g,65,50,30
	GR.CIRCLE g,235,50,30
	GR.RECT g,65,20,235,80
	GR.COLOR 255,0,0,0,1
	GR.TEXT.DRAW g,47,95,"ANSWER"
GR.BITMAP.DRAWINTO.END
GR.SET.STROKE 2
GR.TEXT.BOLD 0
GR.TEXT.SIZE 25
GR.BITMAP.CREATE Pic,300,300
GR.BITMAP.DRAWINTO.START Pic
	GR.COLOR 255,0,0,0,1
	GR.TEXT.DRAW g,30,90,"Don't count your"
	GR.TEXT.DRAW g,30,170,"chickens before they"
	GR.TEXT.DRAW g,30,240,"are hatched"
	GR.COLOR 255,215,205,195,1
	FOR i=1 TO 5
		GR.POLY g,FR[i]
	NEXT i
	GR.COLOR 255,0,0,0,0
	GR.RECT g,1,1,299,299
	GR.COLOR 192,255,255,255,1
	GR.RECT g,0,0,300,300
GR.BITMAP.DRAWINTO.END
FOR i=1 TO 12
	LIST.CREATE N,SectorData[i]
NEXT i
! Intro
GR.COLOR 255,0,255,255,0
GR.RECT g,1,1,ScaleX-1,ScaleY-1
GR.BITMAP.DRAW g,Pic,10,10
GR.COLOR 92,255,0,0,1
GR.TEXT.SIZE 30
GR.TEXT.DRAW g,100,630,"(LONG) TAP SCREEN TO START"
GR.COLOR 92,0,255,0,1
GR.TEXT.SIZE 100
GR.ROTATE.START -45,110,600
	GR.TEXT.DRAW g,110,600,Game$
	GR.COLOR 255,255,255,255,1
	GR.RECT g,110,520,400,550
	GR.RECT g,310,585,600,605
	GR.RECT g,720,550,730,600
GR.ROTATE.END
GR.RECT g,430,140,470,200
GR.RECT g,330,250,370,280
GR.TEXT.SIZE 25
GR.COLOR 255,0,0,0,1
GR.TEXT.UNDERLINE 1
LIST.CREATE N,IntroTxt
FOR i=1 TO 21
	IF i=4 THEN GR.TEXT.UNDERLINE 0
	READ.NEXT T$
	GR.TEXT.DRAW g,10,ScaleY+i*40,T$
	LIST.ADD IntroTxt,g
NEXT i
GR.GROUP.LIST Intro,IntroTxt
GR.RENDER
DO
	FOR i=1 TO 120
		GR.TOUCH touched,x,y
		PAUSE 150
		IF touched THEN F_N.BREAK
		GR.MOVE Intro,,-10
		GR.RENDER
		PAUSE 150
	NEXT i
	PAUSE 500
	GR.MOVE INTRO,,1200
UNTIL touched
DIALOG.MESSAGE "SOUND during play","ON or OFF ?",SndOnOff,"ON","OFF"
GR.CLS
GR.COLOR 192,220,210,200,1
GR.RECT Window,0,0,600,600
GR.COLOR 255,127,127,127,1
GR.RECT g,0,770,600,ScaleY
GR.COLOR 255,0,0,0,1
GR.RECT g,0,600,600,775
GR.TEXT.ALIGN 2
GR.TEXT.SIZE 100
GR.COLOR 255,255,0,255,1
GR.TEXT.DRAW g,295,715,"B W"
GR.COLOR 255,0,255,0,1
GR.TEXT.DRAW g,300,720,"B W"
GR.TEXT.SIZE 40
GR.TEXT.UNDERLINE 1
GR.COLOR 255,255,255,255,1
GR.TEXT.DRAW g,463,813,Game$
GR.COLOR 255,0,0,0,1
GR.TEXT.DRAW g,460,810,Game$
GR.TEXT.UNDERLINE 0
GR.TEXT.TYPEFACE 2,2
FOR i=1 TO 6
	GR.TEXT.DRAW LineHolder[i],ScaleX/2,ScaleX/2-140+(i-1)*60,""
NEXT i
GR.TEXT.ALIGN 1
GR.TEXT.TYPEFACE 1,2
GR.SET.STROKE 5
GR.COLOR 255,255,0,0,0
GR.RECT g,180,766,400,778
GR.LINE g,290,778,305,773
GR.LINE g,290,778,305,783
GR.COLOR 255,0,0,0,1
GR.LINE g,190,766,390,766
GR.SET.STROKE 2
GR.COLOR 255,255,255,0,1
GR.TEXT.SIZE 10
GR.TEXT.DRAW g,220,750,"GAME NUMBER (1-20) ="
GR.TEXT.DRAW CurGame,330,750,"1"

GR.TEXT.SIZE 20
GR.TEXT.DRAW g,190,787,"reshuffle..."
GR.COLOR 255,255,255,0,0
FOR i=0 TO 19
	GR.RECT g,190+i*10,764,200+i*10,768
NEXT i
GR.COLOR 255,0,0,0,1
FOR i=0 TO 19
	GR.RECT NumGames[i+1],191+i*10,765,199+i*10,767
NEXT i
GR.BITMAP.DRAW ButPic,Button,150,610
GR.HIDE ButPic
GR.COLOR 255,255,255,255,1
GR.CIRCLE g,20,800,15
GR.RECT g,23,785,105,815
GR.CIRCLE g,106,800,15
GR.COLOR 255,0,255,0,1
GR.CIRCLE SOn,20,800,12
GR.COLOR 255,255,0,0,1
GR.CIRCLE SOff,106,800,12
GR.COLOR 255,0,0,0,1
GR.TEXT.DRAW g,40,805,"SND"
IF SndOnOff=1 THEN
	GR.HIDE SOff
ELSE
	GR.HIDE SOn
ENDIF
GR.COLOR 192,192,128,128,1
GR.RECT GreyOut,5,785,122,815
GR.HIDE GreyOut
GR.TEXT.SIZE 40
GR.COLOR 255,255,255,0,1
GR.TEXT.DRAW g,10,630,"Score"
GR.TEXT.DRAW g,460,630,"Total"
GR.TEXT.ALIGN 2
GR.TEXT.DRAW CurScore,60,675,"100"
GR.TEXT.DRAW TotalScore,510,675,""
GR.TEXT.DRAW HiScore,550,755,INT$(ScoreHi)
GR.TEXT.ALIGN 1
GR.TEXT.SIZE 20
GR.TEXT.DRAW g,40,760,"Turns"
GR.COLOR 255,255,255,0,0
GR.RECT g,5,705,140,765
GR.TEXT.SIZE 10
FOR i=1 TO 10
	GR.COLOR 255,255,255,0,1
	GR.TEXT.DRAW g,i*12,720,INT$(i)
	GR.COLOR 255,128,128,128,1
	GR.RECT SqTrns[i],i*12,725,i*12+10,735
	GR.COLOR 255,255,255,0,0
	GR.RECT g,i*12,725,i*12+10,735
NEXT i
GR.COLOR 255,0,0,0,1
GR.PAINT.GET PaintBlack
GR.COLOR 255,255,0,0,1
GR.PAINT.GET PaintRed
GR.COLOR 255,0,255,0,1
GR.PAINT.GET PaintGreen
GR.COLOR 255,128,128,128,1
GR.PAINT.GET PaintGrey
GR.COLOR 255,255,255,255,1
GR.PAINT.GET PaintWhite
GR.TEXT.SIZE 20
GR.COLOR 255,255,255,0,1
GR.ROTATE.START -20,410,769
	GR.TEXT.DRAW g,410,769,"HI-SCORE"
GR.ROTATE.END
GR.TEXT.SIZE 15
GR.TEXT.DRAW g,460,700,"Max score=2000"
! Create ball
GR.COLOR 255,210,105,55,1
GR.CIRCLE Ball,0,-33,30
! Score boxes
GR.COLOR 255,255,255,0,0
GR.RECT g,20,640,100,680
GR.RECT g,470,640,550,680
GR.RECT g,503,720,597,760
! Hi Score text
GR.TEXT.SIZE 40
GR.BITMAP.CREATE HiBox,600,170
GR.BITMAP.DRAWINTO.START HiBox
	GR.COLOR 192,0,0,0,1
	GR.RECT g,0,0,600,170
	GR.COLOR 255,255,255,0,1
	GR.TEXT.DRAW g,20,100,"Well done ! A NEW HiScore !!!!"
GR.BITMAP.DRAWINTO.END
GR.BITMAP.DRAW BoxHi,HiBox,0,600
GR.HIDE BoxHi

! Start game
GoAgain:
AccScore=0
GR.MODIFY TotalScore,"text","0"
FOR i=1 TO 10
	GR.MODIFY SqTrns[i],"paint",PaintGrey
NEXT i
! Read encoded data
FILE.SIZE FL,DataPath$+"Proverbs.dat"
BYTE.OPEN R,EncrDat,DataPath$+"Proverbs.dat"
	BYTE.READ.BUFFER EncrDat,FL,EncrTF$
BYTE.CLOSE EncrDat
! Decode data
DecrBuf$=DECODE$("DECRYPT",,EncrTF$)
! Split proverbs
SPLIT  NS$[],DecrBuf$,CHR$(13)+CHR$(10)
! Read 1st line
NumOfProv=VAL(WORD$(NS$[1],1,","))
Part=VAL(WORD$(NS$[1],2,","))
ScoreHi=VAL(WORD$(NS$[1],3,","))
Pwd$=WORD$(NS$[1],4,",")
FOR i=0 TO 19
	GR.MODIFY NumGames[i+1],"paint",PaintBlack
NEXT i
FOR i=0 TO INT(Part/10)
	GR.MODIFY NumGames[i+1],"paint",PaintGreen
NEXT i
GR.MODIFY CurGame,"text",INT$(Part/10+1)
ARRAY.LENGTH L,NS$[]
! Reshuffle if all have been used
IF Part=0 THEN ARRAY.SHUFFLE NS$[2,L-1]
! Keep 10 proverbs
FOR i=1 TO 10
	Proverbs$[i]=WORD$(NS$[i+Part+1],1,"\t")
	Expl$[i]=WORD$(NS$[i+Part+1],2,"\t")
NEXT i
! Reassemble data
Part=Part+10
IF Part=INT(NumOfProv/10)*10 THEN Part=0
NS$[1]=INT$(NumOfProv)+","+INT$(Part)+","+INT$(ScoreHi)+","+Pwd$
JOIN.ALL NS$[],DecrBuf$,CHR$(13)+CHR$(10)
! Encode and save data
EncrTF$=ENCODE$("ENCRYPT",,DecrBuf$)
BYTE.OPEN W,EncrDat,DataPath$+"Proverbs.dat"
	BYTE.WRITE.BUFFER EncrDat,EncrTF$
BYTE.CLOSE EncrDat
UNDIM NS$[]
GR.MODIFY HiScore,"text",INT$(ScoreHi)
FOR Turn=1 TO 10
	GR.MODIFY CurScore,"text","100"
	GR.MODIFY SqTrns[Turn],"paint",PaintWhite
	cX=INT(ScaleX/2+50-RND()*100)
	cY=INT(ScaleX/2+50-RND()*100)
	FOR i=1 TO 6
		GR.MODIFY LineHolder[i],"x",cX
	NEXT i
	Line$=Proverbs$[Turn]
	Expl$=Expl$[Turn]
	NumWords=VAL(LEFT$(Line$,1))
	Line$=RIGHT$(Line$,-1)
	Answer$=Line$
	Line$=LOWER$(Line$)
	Line$=UPPER$(LEFT$(Line$,1))+RIGHT$(Line$,-1)
	DO
		Q=IS_IN("$",Line$)
		Line$=REPLACE$(Line$,"$"+MID$(LINE$,Q+1,1),UPPER$(MID$(LINE$,Q+1,1)))
	UNTIL Q=0
	Answer$=REPLACE$(Answer$,"$","")
	Answer$=REPLACE$(Answer$,"'","")
	Answer$=REPLACE$(Answer$,"?","")
	Answer$=REPLACE$(Answer$,"!","")
	Answer$=REPLACE$(Answer$,"-","")
	Answer$=REPLACE$(Answer$,","," ")
	! Distribute the Proverb
	FOR i=1 TO 6
		PrLine$[i]=""
		DisplayLine$[i]=""
	NEXT i
	! First break up in words
	SPLIT TheWords$[],Line$
	! Reassemble to max. 20 chars.
	ARRAY.LENGTH WL,TheWords$[]
	NumLines=1
	PrLine$[1]=TheWords$[1]+" "
	FOR i=2 TO WL
		IF LEN(PrLine$[NumLines])+LEN(TheWords$[i])<21 THEN
			PrLine$[NumLines]=PrLine$[NumLines]+TheWords$[i]+" "
		ELSE
			PrLine$[NumLines]=TRIM$(PrLine$[NumLines])
			NumLines=NumLines+1
			PrLine$[NumLines]=TheWords$[i]+" "
		ENDIF
	NEXT i
	PrLine$[NumLines]=TRIM$(PrLine$[NumLines])
	UNDIM TheWords$[]
	SW.BEGIN NumLines
		SW.CASE 1
			Place1=FLOOR(6*RND()+1)
			SWAP PrLine$[1],DisplayLine$[Place1]
			SW.BREAK
		SW.CASE 2
			Place1=FLOOR(3*RND()+1)
			SWAP PrLine$[1],DisplayLine$[Place1]
			Place2=FLOOR((6-Place1)*RND()+1)+Place1
			SWAP PrLine$[2],DisplayLine$[Place2]
			SW.BREAK
		SW.CASE 3
			Place1=FLOOR(2*RND()+1)
			SWAP PrLine$[1],DisplayLine$[Place1]
			Place2=FLOOR((4-Place1)*RND()+1)+Place1
			SWAP PrLine$[2],DisplayLine$[Place2]
			Place3=FLOOR((6-Place2)*RND()+1)+Place2
			SWAP PrLine$[3],DisplayLine$[Place3]
			SW.BREAK
		SW.CASE 4
			Place1=FLOOR(2*RND()+1)
			SWAP PrLine$[1],DisplayLine$[Place1]
			Place2=FLOOR((3-Place1)*RND()+1)+Place1
			SWAP PrLine$[2],DisplayLine$[Place2]
			Place3=FLOOR((4-Place2)*RND()+1)+Place2
			SWAP PrLine$[3],DisplayLine$[Place3]
			Place4=FLOOR((6-Place3)*RND()+1)+Place3
			SWAP PrLine$[4],DisplayLine$[Place4]
			SW.BREAK
		SW.CASE 5
			Place1=FLOOR(2*RND()+1)
			SWAP PrLine$[1],DisplayLine$[Place1]
			Place2=FLOOR((3-Place1)*RND()+1)+Place1
			SWAP PrLine$[2],DisplayLine$[Place2]
			Place3=FLOOR((4-Place2)*RND()+1)+Place2
			SWAP PrLine$[3],DisplayLine$[Place3]
			Place4=FLOOR((5-Place3)*RND()+1)+Place3
			SWAP PrLine$[4],DisplayLine$[Place4]
			Place5=FLOOR((6-Place4)*RND()+1)+Place4
			SWAP PrLine$[5],DisplayLine$[Place5]
			SW.BREAK
	SW.END
	FOR i=1 TO 6
		GR.MODIFY LineHolder[i],"text",DisplayLine$[i]
		GR.HIDE LineHolder[i]
	NEXT i
	GR.COLOR 255,255,255,255,0
	Cnt=0
	FOR angle=0 TO 330 STEP 30
		! First point (center)
		Cnt=Cnt+1
		eX[Cnt]=cX+5-RND()*10
		eY[Cnt]=cY+5-RND()*10
		FOR cR=100 TO 460 STEP 60
			LotX=(FLOOR(RND()*3)-1)*RND()*30
			LotY=(FLOOR(RND()*3)-1)*RND()*30
			x=cR*COS(angle*PI()/180)+cX+LotX
			y=cR*SIN(angle*PI()/180)+cY+LotY
			IF x<0 THEN x=0
			IF x>600 THEN x=600
			IF y<0 THEN y=0
			IF y>600 THEN y=600
			Cnt=Cnt+1
			eX[Cnt]=x
			eY[Cnt]=y
		NEXT cR
		! Last point has to be on edge
		cR=700
		x=cR*COS(angle*PI()/180)+cX
		y=cR*SIN(angle*PI()/180)+cY
		IF x<0 THEN x=0
		IF x>600 THEN x=600
		IF y<0 THEN y=0
		IF y>600 THEN y=600
		Cnt=Cnt+1
		eX[Cnt]=x
		eY[Cnt]=y
	NEXT angle
	PAUSE 1000
	FOR i=1 TO 108 STEP 9
		FOR j=i TO i+8
			LIST.ADD SectorData[(i-1)/9+1],eX[j],eY[j]
		NEXT j
		! Reverse previous lines
		IF i=1 THEN
			FOR j=108 TO 100 STEP-1
				LIST.ADD SectorData[(i-1)/9+1],eX[j],eY[j]
			NEXT j
		ELSE
			FOR j=i-1 TO i-9 STEP -1
				LIST.ADD SectorData[(i-1)/9+1],eX[j],eY[j]
			NEXT j
		ENDIF
	NEXT i
	! Create list of angles to detect sector number
	FOR i=1 TO 12
		LIST.GET SectorData[i],1,NCx
		LIST.GET SectorData[i],2,NCy
		LIST.GET SectorData[i],9,Nx
		LIST.GET SectorData[i],10,Ny
		SectorAngles[i]=MOD(ATAN2((Ny-NcY),(Nx-NcX))*180/PI()+360,360)
		! Keep track of segments
		SectorPresent[i]=1
	NEXT i
	GR.SHOW Window
	GR.RENDER
	POPUP "Let's go find the "+N$[Turn]+" proverb !",,-200
	PAUSE 2000
	! Show ball
	Strt=RND()*600
	dX=(cX-Strt)/((33+cY)/15)
	Cnt=0
	FOR i=-33 TO cY STEP 15
		PAUSE 30
		Cnt=Cnt+1
		Strt=Strt+dX
		GR.MODIFY Ball,"x",Strt
		GR.MODIFY Ball,"y",i
		GR.MODIFY Ball,"radius",30-Cnt
		GR.RENDER
	NEXT i
	IF SndOnOff=1 THEN
		AUDIO.STOP
		AUDIO.PLAY Crash
	ENDIF
	PAUSE 100
	! Ball out of sight
	GR.MODIFY Ball,"y",-33
	GR.RENDER
	FOR i=1 TO 12
		c=RND()*50
		GR.COLOR 255,125+c,125+c,125+c,0
		GR.POLY Sector[i],SectorData[i]
		c=RND()*10
		GR.COLOR 255,225-c,215-c,205-c,1
		GR.POLY SectorFill[i],SectorData[i]
	NEXT i
	GR.SHOW ButPic
	GR.RENDER
	GR.HIDE Window
	FOR i=1 TO 6
		GR.SHOW LineHolder[i]
	NEXT i
	SectorsCleared=0
	YourAnswer$=""
	Score=100
	DO
		! Get user's choice
		IF x>0 THEN POPUP "Tap a fragment OR tap Answer....",,-200
		GOSUB GetTouch
		! Answer or sector ?
		IF x>150 & x<450 & y>610 & y<760 THEN
			IF SndOnOff=1 THEN
				AUDIO.STOP
				AUDIO.PLAY Beep
			ENDIF
			! Get answer
			GR.SHOW GreyOut
			GR.RENDER
			DO
				INPUT "Which proverb do you think ?",YourAnswer$,,IsCancel
			UNTIL !IsCancel
			GR.HIDE GreyOut
			GR.RENDER
		ELSE
			IF y<600 THEN
				! Which sector has been chosen ?
				TouchAngle=MOD(ATAN2((y-cY),(x-cX))*180/PI()+360,360)
				FOR Choice=2 TO 12
					IF TouchAngle<SectorAngles[Choice] THEN
						F_N.BREAK
					ENDIF
				NEXT Choice
				IF Choice=13 THEN
					Choice=1
					IF SectorAngles[12]-SectorAngles[1]>300 THEN
						DelAngle=((SectorAngles[1]-360)+SectorAngles[12])/2
						IF DelAngle<0 THEN DelAngle=360+DelAngle
					ELSE
						DelAngle=(SectorAngles[1]+SectorAngles[12])/2
					ENDIF
				ELSE
					IF SectorAngles[Choice-1]-SectorAngles[Choice]>300 THEN
						DelAngle=((SectorAngles[Choice]-360)+SectorAngles[Choice-1])/2
						IF DelAngle<0 THEN DelAngle=360+DelAngle
					ELSE
						DelAngle=(SectorAngles[Choice]+SectorAngles[Choice-1])/2
					ENDIF
				ENDIF
				! Sector still present ?
				IF SectorPresent[Choice]=1 THEN
					! Remove Sector
					SectorPresent[Choice]=0
					IF SndOnOff=1 THEN
						AUDIO.STOP
						AUDIO.PLAY GlOut
					ENDIF
					FOR r=20 TO 100 STEP 20
						xm=r*COS(DelAngle*PI()/180)
						ym=r*SIN(DelAngle*PI()/180)
						GR.MOVE Sector[Choice],xm,ym
						GR.MOVE SectorFill[Choice],xm,ym
						GR.RENDER
						PAUSE 100
					NEXT r
					GR.HIDE Sector[Choice]
					GR.HIDE SectorFill[Choice]
					SectorsCleared=SectorsCleared+1
					! Show new score
					Score=ABS(Score-(SectorsCleared-1)-3)
					GR.MODIFY CurScore,"text",INT$(Score)
					GR.RENDER
				ENDIF
			ENDIF
		ENDIF
	UNTIL SectorsCleared=12 | YourAnswer$<>""
	GR.MODIFY CurScore,"text",""
	GR.HIDE ButPic
	GR.RENDER
	! Check answer
	IF SectorsCleared=12 THEN
		IF SndOnOff=1 THEN
			AUDIO.STOP
			AUDIO.PLAY Wrong
		ENDIF
		GR.MODIFY SqTrns[Turn],"paint",PaintRed
		GR.SHOW GreyOut
		GR.RENDER
		DIALOG.MESSAGE "","NO answer given ! You've scored 0 points",OK,"OK"
		GR.HIDE GreyOut
		GR.RENDER
	ELSE
		POPUP "You entered: "+YourAnswer$,,-200,1
		! Get answered words
		YourAnswer$=REPLACE$(YourAnswer$,"$","")
		YourAnswer$=REPLACE$(YourAnswer$,"'","")
		YourAnswer$=REPLACE$(YourAnswer$,"?","")
		YourAnswer$=REPLACE$(YourAnswer$,"!","")
		YourAnswer$=REPLACE$(YourAnswer$,"-","")
		YourAnswer$=REPLACE$(YourAnswer$,","," ")
		SPLIT YourWords$[],YourAnswer$
		ARRAY.LENGTH NW,YourWords$[]
		! Is answer correct ?
		Match=0
		FOR i=1 TO NW
			IF IS_IN(UPPER$(YourWords$[i]),Answer$) THEN Match=Match+1
		NEXT i
		UNDIM YourWords$[]
		IF Match>=NumWords THEN
			AccScore=AccScore+Score
			GR.MODIFY TotalScore,"text",INT$(AccScore)
			GR.RENDER
			GR.MODIFY SqTrns[Turn],"paint",PaintGreen
			IF SndOnOff=1 THEN
				AUDIO.STOP
				AUDIO.PLAY Correct
			ENDIF
			GR.SHOW GreyOut
			GR.RENDER
			DO
				DIALOG.MESSAGE "","Your answer is CORRECT ! You've scored "+INT$(Score)+" points.",OK,"OK"
			UNTIL OK
			GR.HIDE GreyOut
			GR.RENDER
		ELSE
			GR.MODIFY SqTrns[Turn],"paint",PaintRed
			IF SndOnOff=1 THEN
				AUDIO.STOP
				AUDIO.PLAY Wrong
			ENDIF
			GR.SHOW GreyOut
			GR.RENDER
			DO
				DIALOG.MESSAGE "","Your answer is WRONG ! You've scored 0 points",OK,"OK"
			UNTIL OK
			GR.HIDE GreyOut
			GR.RENDER
		ENDIF
	ENDIF
	! Clean display list
	FOR i=1 TO 12
		GR.GETDL CurDispl[],1
		ARRAY.LENGTH DL,CurDispl[]
		GR.NEWDL CurDispl[1,DL-2]
		GR.RENDER
		UNDIM CurDispl[]
		! Clear data for Sectors as well
		LIST.CLEAR SectorData[13-i]
	NEXT i
	GR.SHOW GreyOut
	GR.RENDER
	DO
		DIALOG.MESSAGE "The proverb:"+CHR$(13)+CHR$(10)+Line$,"has this meaning:"+CHR$(13)+CHR$(10)+Expl$,OK,"OK"
	UNTIL OK
	GR.HIDE GreyOut
	GR.RENDER
	PAUSE 2000
NEXT Turn
FOR i=1 TO 6
	GR.HIDE LineHolder[i]
NEXT i
GR.RENDER
! Check for Hi-Score
IF AccScore>ScoreHi THEN
	! New Hi-Score
	GR.SHOW BoxHi
	GR.RENDER
	! Read encoded data
	FILE.SIZE FL,DataPath$+"Proverbs.dat"
	BYTE.OPEN R,EncrDat,DataPath$+"Proverbs.dat"
		BYTE.READ.BUFFER EncrDat,FL,EncrTF$
	BYTE.CLOSE EncrDat
	! Decode data
	DecrBuf$=DECODE$("DECRYPT",,EncrTF$)
	SPLIT  NS$[],DecrBuf$,CHR$(13)+CHR$(10)
	! Add hew Hi Score
	NS$[1]=INT$(NumOfProv)+","+INT$(Part)+","+INT$(AccScore)+","+Pwd$
	JOIN.ALL NS$[],DecrBuf$,CHR$(13)+CHR$(10)
	! Encode and save data
	EncrTF$=ENCODE$("ENCRYPT",,DecrBuf$)
	BYTE.OPEN W,EncrDat,DataPath$+"Proverbs.dat"
		BYTE.WRITE.BUFFER EncrDat,EncrTF$
	BYTE.CLOSE EncrDat
	UNDIM NS$[]
	PAUSE 2000
	GR.HIDE BoxHi
	ScoreHi=AccScore
	GR.MODIFY HiScore,"text",INT$(ScoreHi)
	GR.RENDER
	UNDIM NS$[]
ENDIF
! Has the player seen all proverbs?
IF Part=0 THEN
	GR.MODIFY LineHolder[1],"text","The next time you"
	GR.MODIFY LineHolder[2],"text","play this game you"
	GR.MODIFY LineHolder[3],"text","will see proverbs"
	GR.MODIFY LineHolder[4],"text","you have seen before"
	FOR i=1 TO 4
		GR.SHOW LineHolder[i]
	NEXT i
	GR.RENDER
ENDIF
GR.SHOW GreyOut
GR.RENDER
DO
	DIALOG.MESSAGE "Game ENDED","Do you want to play again ?",OK,"YES","NO"
UNTIL OK>0
GR.HIDE GreyOut
GR.RENDER
IF OK=1 THEN GOTO GoAgain
POPUP "THANKS FOR PLAYING Broken Words !"
WAKELOCK 5
GR.CLOSE
EXIT

GetTouch:
	DO
		GR.TOUCH touched,x,y
	UNTIL touched
	DO
		GR.TOUCH touched,x,y
	UNTIL !touched
	x=ROUND(x/sx)
	y=ROUND(y/sy)
RETURN
ONGRTOUCH:
	GR.TOUCH touched,x,y
	x=ROUND(x/sx)
	y=ROUND(y/sy)
	IF x>5 & x<120 & y>785 & y<815 THEN
		IF SndOnOff=1 THEN
			GR.SHOW SOff
			GR.HIDE SOn
			SndOnOff=2
		ELSE
			AUDIO.STOP
			AUDIO.PLAY Beep
			GR.SHOW SOn
			GR.HIDE SOff
			SndOnOff=1
		ENDIF
		GR.RENDER
	ENDIF
GR.ONGRTOUCH.RESUME
ONBACKKEY:
	IF SndOnOff=1 THEN
		AUDIO.STOP
		AUDIO.PLAY Beep
	ENDIF
	GR.SHOW GreyOut
	GR.RENDER
	DO
		DIALOG.MESSAGE Game$,"What's your plan ?",OK,"Cheat","Quit","Cancel"
	UNTIL OK>0
	GR.HIDE GreyOut
	GR.RENDER
	IF OK=1 THEN
		! If password is OK show screen with data
		DO
			INPUT "Enter the CHEAT Password",YourPwd$,,IsCancel
		UNTIL !IsCancel
		IF YourPwd$=Pwd$ THEN
			! Show data
			DIM FakeSel$[16]
			FakeSel$[1]="Number of proverbs "+INT$(NumOfProv)
			FakeSel$[2]="Hi Score = "+INT$(ScoreHi)
			FakeSel$[3]="Busy on part "+INT$(Part/10)
			FakeSel$[4]="Containing these proverbs:"
			FOR i=5 to 14
				SL$=Proverbs$[i-4]
				SL$=RIGHT$(SL$,-1)
				SL$=LOWER$(SL$)
				SL$=UPPER$(LEFT$(SL$,1))+RIGHT$(SL$,-1)
				FakeSel$[i]=INT$(i-4)+" - "+SL$
			NEXT i
			FakeSel$[16]="TAP to return to the game"
			DIALOG.SELECT OK,FakeSel$[],"Cheating makes live easy"
			UNDIM FakeSel$[]
		ELSE
			POPUP "SORRY, this password doesn't sound familiar  !",,-200
		ENDIF
	ENDIF
	IF OK=2 THEN
		POPUP "THANKS FOR PLAYING Broken Words !"
		WAKELOCK 5
		GR.CLOSE
		EXIT
	ENDIF
BACK.RESUME
READ.DATA "Broken Words",""
READ.DATA "Guess the proverb behind the broken glass window.",""
READ.DATA "In this game you start with a broken window."
READ.DATA "By tapping on a glass fragment you remove it."
READ.DATA "(try to hit a fragment close to its center)"
READ.DATA "If you are lucky a part of the words of a proverb"
READ.DATA "is revealed. By tapping on a fragment you decrease"
READ.DATA "the score you can get from the current proverb."
READ.DATA "As soon as you think you know which proverb is"
READ.DATA "hidden behind the glass fragments, tap on the"
READ.DATA "answer button. When prompted for type in your"
READ.DATA "guess. As accurate as possible; typo's are"
READ.DATA "seen as incorrect !"
READ.DATA "If your answer is correct your score will be added"
READ.DATA "to the total score."
READ.DATA "If your answer is wrong your score will be zero."
READ.DATA "You have to guess 10 proverbs per game.",""
READ.DATA "Have fun and good luck."