! Schnick Schnack Schnuck
! Rock Paper Scissors



!GrabURL Result$ , "http://www.xyz.de/medien/append_txt_from_get_request.php?txt=hi_paul_that_works_great"
!print Result$

array.load menu$[], " - SchnickSchnackSchnuck - ", "Start new Game...","End Program"

array.load player$[],"Player 1","Player 2"

array.load round$[], "Stein/stone","Papier/paper", "Schere/scissor", "Brunnen/fountain", "Feuer/fire"

mainmenu:
select sel1, menu$[], "Select something..."

if sel1 = 0 then goto mainmenu
if sel1 = 1 then goto mainmenu
if sel1 = 2 then goto start
if sel1 = 3 then goto ex

start:
print" Start selected"
select sel2, player$[],"Select a player: Pl.1 with forname first..."

if sel2 = 1 then player$ = "1"
if sel2 = 2 then player$ = "2"
print "You are player ";player$
if player$ = "1" then opponent$ = "2"
if player$ = "2" then opponent$ = "1"

GrabURL Result$ , "http://medienreservierung.12hp.de/sss_move_"+player$+".php?new=1"

print "performing request for new data field..."

move = 0
points = 0

goto round

round:
move = move + 1
select sel3, round$[],"Select an item to play..."
! if sel3 = 1 then print"Stein"
! if sel3 = 2 then print"Papier" 

goto setquery

setquery:
GrabURL Result$ , "http://medienreservierung.12hp.de/sss_move_"+player$+".php?player=:"+player$+"_&move="+right$(format$("%%",move),2)+"_&item="+left$(str$(sel3),1)
mSelf = val(right$(Result$,1))
print "Query No. "+left$(right$(Result$,4),2)+" from Player "+left$(right$(Result$,6),1)+" is "+right$(Result$,1)


reqloop:
popup "Waiting for other player...", 50, 50, 0
GrabURL Result$ , "http://medienreservierung.12hp.de/sss_move_"+opponent$+".php"
v$ = left$(right$(Result$,4),2)
print v$

if v$ = "__"
pause 2000
goto reqloop
else
if move = val(v$)
goto yo
else
pause 1000
goto reqloop
endif
endif

yo:
print "Query No. "+left$(right$(Result$,4),2)+" from Player "+left$(right$(Result$,6),1)+" is "+right$(Result$,1)

mOther = val(right$(Result$,1))

print "Now comparing..."

if mSelf = 1 & mOther = 2
print "Papier wickelt Stein ein"
print "You loose!"
goto ergebnis
endif
if mSelf = 2 & mOther = 1
print "Papier wickelt Stein ein"
print "You win!"
goto ergebnis_win
endif
if mSelf = 1 & mOther = 3
print "Stein schleift Schere"
print "You win!"
goto ergebnis_win
endif
if mSelf = 3 & mOther = 1
print "Stein schleift Schere"
print "You loose!"
goto ergebnis
endif
if mSelf = 1 & mOther = 4
print "Stein faellt in Brunnen"
print "You loose!"
goto ergebnis
endif
if mSelf = 4 & mOther = 1
print "Stein faellt in Brunnen"
print "You win!"
goto ergebnis_win
endif
if mSelf = 1 & mOther = 5
print "Stein zerspringt im Feuer"
print "You loose!"
goto ergebnis
endif
if mSelf = 5 & mOther = 1
print "Stein zerspringt im Feuer"
print "You win!"
goto ergebnis_win
endif

if mSelf = 2 & mOther = 3
print "Schere schneidet Papier"
print "You loose!"
goto ergebnis
endif
if mSelf = 3 & mOther = 2
print "Schere schneidet Papier"
print "You win!"
goto ergebnis_win
endif

if mSelf = 2 & mOther = 4
print "Papier deckt Brunnen ab"
print "You win!"
goto ergebnis_win
endif
if mSelf = 4 & mOther = 2
print "Papier deckt Brunnen ab"
print "You loose!"
goto ergebnis
endif
if mSelf = 2 & mOther = 5
print "Papier verbrennt im Feuer"
print "You loose!"
goto ergebnis
endif
if mSelf = 5 & mOther = 2
print "Papier verbrennt im Feuer"
print "You win!"
goto ergebnis_win
endif

if mSelf = 3 & mOther = 4
print "Schere faellt in Brunnen"
print "You loose!"
goto ergebnis
endif
if mSelf = 4 & mOther = 3
print "Schere faellt in Brunnen"
print "You win!"
goto ergebnis_win
endif
if mSelf = 3 & mOther = 5
print "Feuer schmilzt Schere"
print "You loose!"
goto ergebnis
endif
if mSelf = 5 & mOther = 3
print "Feuer schmilzt Schere"
print "You win!"
goto ergebnis_win
endif

if mSelf = 4 & mOther = 5
print "Brunnen loescht Feuer"
print "You win!"
goto ergebnis_win
endif
if mSelf = 5 & mOther = 4
print "Brunnen loescht Feuer"
print "You loose!"
goto ergebnis
endif




print" Equal situation!"

ergebnis:
! vgl items und anzeige ergebnis
pause 5500
print"Points: " +str$(points)
GrabURL Result$ , "http://medienreservierung.12hp.de/sss_move_"+opponent$+".php"
v$ = left$(right$(Result$,4),2)
! print v$

if v$ = "__"
CLS
print " You loose the game!"
pause 6000
goto ex
endif

pause 4000
goto round

ergebnis_win:
points = points +1
print"Points: " +str$(points)
if points >= 10 then goto winning
pause 7000
goto round

winning:
CLS
GrabURL Result$ , "http://medienreservierung.12hp.de/sss_move_"+player$+".php?new=1"

print" You win the game!"
pause 10000


ex:
end