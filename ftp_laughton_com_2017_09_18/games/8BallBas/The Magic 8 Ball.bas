REM The Magic 8 Ball
REM 18th April 2013
	
% MAIN
gosub SetUps
gosub Hello

do
  ! gosub Logic
   gosub UpdateScreen
 !   gosub Logic
  ! gosub GamerInput
until GameOver

Setups:
% Open graphics with white screen.
% gr.open 255, 255, 255, 255
gr.open 255, 102,205,170
% Force landscape on device.
gr.orientation 0

% Get screen width and height.
gr.screen ScreenWidth, ScreenHeight
% Set up variables
Offset = 10
Vibe = 200
VibePause = 200
Flag = 0
% Set up arrays
Dim Answer[20]
array.load VibeTwice[], 1, Vibe, VibePause, Vibe, VibePause
array.load Answer$[], "A0", "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "A11", "A12", "A13", "A14", "A15", "A16", "A17", "A18", "A19"
% Load sprites
gr.bitmap.load BallBottom, "/B.png"
gr.bitmap.size BallBottom, BallBottomWidth, BallBottomHeight
for i = 1 to 20
   gr.bitmap.load Answer[i], "/" + Answer$[i] + ".png"
next i
   gr.bitmap.size Answer[1], AnswerWidth, AnswerHeight
return

Hello:
gr.bitmap.load BallTop, "/T.png"
gr.bitmap.size BallTop, BallTopWidth, BallTopHeight
gr.bitmap.draw BallTopBitmap, BallTop, (ScreenWidth - BallTopWidth) / 2 , (ScreenHeight - BallTopHeight) / 2
gr.show BallTopBitmap
gr.render
popup "need help? or advice? ask!", 0, ScreenHeight, 0
pause 3000
return

GamerInput:
% Wait until user taps screen
do
   gr.touch FingerOn, x, y
until FingerOn
% Touch detected, now wait for finger lifted 
do 
   gr.touch FingerOff, x, y
until !FingerOff
return

UpdateScreen:
% Show sprites
gr.bitmap.draw BallBottomBitmap, BallBottom, (ScreenWidth - BallBottomWidth) / 2 , (ScreenHeight - BallBottomHeight) / 2
gr.show BallBottomBitmap
if Flag = 0 then popup "think...and then tap screen", 0, 0, ScreenWidth
! ,ScreenHeight
gr.render
gosub GamerInput
gosub Logic
gr.bitmap.draw AnswerBitmap, Answer[Index], (ScreenWidth - AnswerWidth) / 2 , ((ScreenHeight - AnswerHeight) + Offset) / 2
% Refresh screen
gr.render
% Wait 4 seconds
pause 4000
% Refresh screen again to hide answer
gr.hide AnswerBitmap
gr.render
return

logic:
Flag = 1
% Choose a number between 1 and 20
Index = ceil(rnd()*20)
vibrate VibeTwice[], -1
return

onerror:
end

