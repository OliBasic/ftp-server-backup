! MasterMind
! AatDon @2013
GR.OPEN 255,0,0,0,0,1
GR.SCREEN w,h
ScaleX=960
ScaleY=h/w*ScaleX
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
! Intro
DIM GA[8]
M$="M a s t e r M i n d"
FOR i=0 TO 360 step 20
	GR.CLS
	GR.COLOR 255,0,255,0,1
	GR.TEXT.SIZE 5*i/20+5
	GR.ROTATE.START i,480,640
	GR.TEXT.DRAW g,50,640,M$
	GR.ROTATE.END
	GR.RENDER
	PAUSE 100
NEXT i
TONE 800,300
GR.CLS
GR.COLOR 255,0,255,0,0
FOR i=1 TO 10
	GR.TEXT.DRAW g,50+i*10,640+i*10,M$
	GR.RENDER
	PAUSE 100
NEXT i
GR.COLOR 255,0,0,0,0
FOR i=1 TO 10
	GR.TEXT.DRAW g,50+i*10,640+i*10,M$
	GR.RENDER
	PAUSE 100
NEXT i
FOR j=1 to 3
	GR.COLOR 255,0,0,0,0
	GR.TEXT.DRAW g,50+i*10,640+i*10,M$
	GR.RENDER
	PAUSE 50
	GR.COLOR 255,0,255,0,0
	GR.TEXT.DRAW g,50+i*10,640+i*10,M$
	GR.RENDER
	PAUSE 50
NEXT j
! Use Software or Hardware keys ?
GOSUB DrwBut
GR.TEXT.DRAW GA[5],80,900,"Use D-PAD"
GR.TEXT.DRAW GA[7],100,980,"KEYS"
GR.TEXT.DRAW GA[6],590,900,"Use 'Soft'"
GR.TEXT.DRAW GA[8],610,980,"KEYS"
GR.RENDER
GOSUB GetAnswer
FOR i=1 TO 8
	GR.HIDE GA[i]
NEXT i
GR.RENDER
IF x<480*sx THEN
	Soft=0
ELSE
	Soft=1
ENDIF
! Unique colours or doubles allowed ?
GOSUB DrwBut
GR.TEXT.DRAW GA[5],80,900,"Use unique"
GR.TEXT.DRAW GA[7],100,980,"colours"
GR.TEXT.DRAW GA[6],575,900,"Allow multi"
GR.TEXT.DRAW GA[8],610,980,"colours"
GR.RENDER
GOSUB GetAnswer
FOR i=1 TO 8
	GR.HIDE GA[i]
NEXT i
GR.RENDER
IF x<480*sx THEN
	AllowD=0
ELSE
	AllowD=1
ENDIF
! Pegs
DIM Pegs[6]
DIM Code[4]
IF AllowD=0 THEN
	ARRAY.LOAD CodeS[],1,2,3,4,5,6
ELSE
	ARRAY.LOAD CodeD[],1,2,3,4,5,6,1,2,3,4,5,6
ENDIF
DIM TestCode[4]
DIM Result[4]
DIM TWin[8]
DIM TLoose[6]
IF Soft=1 THEN
	DIM ak[6]
	! Make arrow keys
	l=41
	t=310
	r=589
	b=410
	ARRAY.LOAD a[],l,t+(b-t)/2,l+70,t+5,l+70,t+30,l+180,t+30,l+180,t+70,l+70,t+70,l+70,t+95
	LIST.CREATE n, Arrow1
	LIST.ADD.ARRAY Arrow1, a[]
	ARRAY.LOAD b[],r,t+(b-t)/2,r-70,t+5,r-70,t+30,r-180,t+30,r-180,t+70,r-70,t+70,r-70,t+95
	LIST.CREATE n, Arrow2
	LIST.ADD.ARRAY Arrow2, b[]
	ARRAY.LOAD c[],l+(r-l)/2,b,l+(r-l)/3+5,b-70,l+(r-l)/3+50,b-70,l+(r-l)/3+50,t,r-(r-l)/3-50,t,r-(r-l)/3-50,b-70,r-(r-l)/3-5,b-70
	LIST.CREATE n, Arrow3
	LIST.ADD.ARRAY Arrow3, c[]
ENDIF
! coloured
GR.SET.STROKE 4
FOR i=1 TO 6
	GR.COLOR 255,0,0,0,0
	GR.BITMAP.CREATE Pegs[i],60,60
	GR.BITMAP.DRAWINTO.START Pegs[i]
		C$=RIGHT$("000"+BIN$(i),3)
		GR.COLOR 255,VAL(MID$(C$,1,1))*255,VAL(MID$(C$,2,1))*255,VAL(MID$(C$,3,1))*255,1
		GR.CIRCLE g,30,30,30
		GR.COLOR 90,255,255,255,1
		GR.CIRCLE g,27,27,15
		GR.COLOR 255,0,0,0,0
		GR.CIRCLE g,30,30,30
	GR.BITMAP.DRAWINTO.END
NEXT i
GR.SET.STROKE 10
PAUSE 500
GR.RENDER
Main:
! Board
GR.CLS
GR.COLOR 255,240,180,30,1
GR.RECT g,0,50,960,1230
GR.RECT g,50,0,910,1280
GR.CIRCLE g,50,50,50
GR.CIRCLE g,910,50,50
GR.CIRCLE g,910,1230,50
GR.CIRCLE g,50,1230,50
GR.COLOR 255,110,150,0,0
GR.TEXT.SIZE 70
GR.TEXT.DRAW g,200,80,M$
GR.COLOR 255,210,150,0,0
GR.RECT g,40,110,590,200
! Show coloured pegs
FOR i=1 TO 6
	GR.BITMAP.DRAW g,Pegs[i],85*i,125
NEXT i
! Draw 'peg cursor'
GR.COLOR 255,0,0,0,0
GR.RECT cursor,80,120,150,190
! black / white
GR.COLOR 255,210,150,0,0
GR.RECT g,630,110,920,200
GR.COLOR 255,0,0,0,1
GR.CIRCLE g,700,155,20
GR.COLOR 255,255,255,255,1
GR.CIRCLE g,840,155,20
! Holes
FOR i=1 TO 4
	FOR j=1 TO 12
		GR.COLOR 200,0,0,0,1
		GR.CIRCLE g,i*90+100,j*70+200,20
		GR.COLOR 150,0,0,0,0
		GR.CIRCLE g,i*90+100,j*70+200,25
	NEXT j
NEXT i
GR.COLOR 255,210,150,0,0
GR.RECT g,40,220,590,1100
FOR i=1 TO 4
	FOR j=1 TO 12
		GR.COLOR 255,0,0,0,0
		GR.CIRCLE g,i*70+595,j*70+200,15
	NEXT j
NEXT i
GR.COLOR 255,210,150,0,0
GR.RECT g,630,220,920,1100
! Screws
GR.COLOR 255,80,80,80,1
GR.CIRCLE g,55,55,40
GR.CIRCLE g,905,55,40
GR.CIRCLE g,905,1225,40
GR.CIRCLE g,55,1225,40
GR.COLOR 255,30,30,30,1
GR.ROTATE.START 20,55,55
GR.RECT g,35,47,75,63
GR.ROTATE.END
GR.ROTATE.START 120,905,55
GR.RECT g,885,47,925,63
GR.ROTATE.END
GR.ROTATE.START 220,905,1225
GR.RECT g,885,1217,925,1233
GR.ROTATE.END
GR.ROTATE.START 320,55,1225
GR.RECT g,35,1217,75,1233
GR.ROTATE.END
! Make code
IF AllowD=0 THEN
	ARRAY.SHUFFLE CodeS[]
	FOR i=1 TO 4
		Code[i]=CodeS[i]
	NEXT i
ELSE
	ARRAY.SHUFFLE CodeD[]
	FOR i=1 TO 4
		Code[i]=CodeD[i]
	NEXT i
ENDIF
FOR i=1 TO 4
	GR.BITMAP.DRAW g,Pegs[Code[i]],90*i+70,1150
NEXT i
! Code cover
GR.COLOR 255,0,0,0,1
GR.RECT cg,100,1130,550,1230
GR.COLOR 200,255,0,0,1
GR.TEXT.SIZE 50
GR.TEXT.DRAW ct,250,1200,"C O D E"
GR.RENDER
GR.TEXT.BOLD 1
IF Soft=1 THEN
	! Draw arrow keys
	GR.COLOR 255,0,0,0,0
	GR.RECT ak[1],l,t,r,b
	GR.LINE ak[2],l+(r-l)/3,t,l+(r-l)/3,b
	GR.LINE ak[3],l+2*(r-l)/3,t,l+2*(r-l)/3,b
	GR.COLOR 200,255,0,0,1
	GR.POLY ak[4], Arrow1
	GR.POLY ak[5], Arrow2
	GR.COLOR 200,255,255,255,1
	GR.POLY ak[6], Arrow3
	FOR i=1 TO 6
		GR.HIDE ak[i]
	NEXT i
ENDIF
CurPos=1
Turn=0
DO
	FoundAll=0
	Turn=Turn+1
	FOR j=1 TO 4
		TestCode[j]=Code[j]
	NEXT j
	GR.COLOR 255,0,0,0,1
	GR.TEXT.DRAW g,50,Turn*70+220,FORMAT$("##",Turn)
	IF Soft=1 THEN
		GR.MODIFY ak[1],"top",t+(Turn-1)*70
		GR.MODIFY ak[1],"bottom",b+(Turn-1)*70
		Gr.SHOW ak[1]
		FOR i=2 TO 3
			GR.MODIFY ak[i],"y1",t+(Turn-1)*70
			GR.MODIFY ak[i],"y2",b+(Turn-1)*70
			GR.SHOW ak[i]
		NEXT i
		FOR i=4 TO 6
			GR.MODIFY ak[i],"y",(Turn-1)*70
			GR.SHOW ak[i]
		NEXT i
	ENDIF
	GR.RENDER
	FOR j=1 TO 4
		IF Soft=0 THEN
			DO
				DO
					INKEY$ K$
				UNTIL K$="left" | K$="right" | K$="go"
				IF K$="left" THEN
					CurPos=CurPos-1
					IF CurPos=0 THEN CurPos=6
				ENDIF
				IF K$="right"THEN
					CurPos=CurPos+1
					IF CurPos=7 THEN CurPos=1
				ENDIF
				GR.MODIFY cursor,"left",CurPos*85-5
				GR.MODIFY cursor,"right",CurPos*85-5+70
				GR.RENDER
			UNTIL K$="go"
		ELSE
			DO
				DO
					GR.TOUCH touched,x,y
				UNTIL touched
				DO
					GR.TOUCH touched,x,y
				UNTIL !touched
				IF x>l*sy & x<(l+(r-l)/3)*sy & y>(t+(Turn-1)*70)*sy & y<(b+(Turn-1)*70)*sy THEN
					CurPos=CurPos-1
					IF CurPos=0 THEN CurPos=6
				ENDIF
				IF x>(r-(r-l)/3)*sy & x<r*sy & y>(t+(Turn-1)*70)*sy & y<(b+(Turn-1)*70)*sy THEN
					CurPos=CurPos+1
					IF CurPos=7 THEN CurPos=1
				ENDIF
				GR.MODIFY cursor,"left",CurPos*85-5
				GR.MODIFY cursor,"right",CurPos*85-5+70
				GR.RENDER
			UNTIL x>(l+(r-l)/3)*sy & x<(r-(r-l)/3)*sy & y>(t+(Turn-1)*70)*sy & y<(b+(Turn-1)*70)*sy
		ENDIF
		! Place peg
		TONE 440,300
		GR.BITMAP.DRAW g,Pegs[CurPos],90*j+70,Turn*70+170
		GR.RENDER
		Result[j]=CurPos
	NEXT j
	IF Soft=1 THEN
		FOR i=1 TO 6
			GR.HIDE ak[i]
			GR.RENDER
		NEXT i
	ENDIF
	! Result
	ResPlace=1
	FOR j=1 TO 4
		IF Result[j]=TestCode[j] THEN
			PAUSE 100
			! Right colour + right place --> black peg
			GR.COLOR 255,0,0,0,1
			GR.CIRCLE g,ResPlace*70+595,Turn*70+200,15
			ResPlace=ResPlace+1
			TestCode[j]=0
			Result[j]=8
			FoundAll=FoundAll+1
			TONE 550,300
		ENDIF
	NEXT j
	FOR j=1 TO 4
		FOR k=1 TO 4
			IF Result[k]=TestCode[j] THEN
				PAUSE 100
				! Right colour, wrong place --> white peg
				GR.COLOR 255,255,255,255,1
				GR.CIRCLE g,ResPlace*70+595,Turn*70+200,15
				ResPlace=ResPlace+1
				TestCode[j]=0
				Result[k]=7
				TONE 550,300
			ENDIF
		NEXT k
	NEXT j
	GR.RENDER
UNTIL FoundAll=4 | Turn=12
GR.COLOR 180,255,255,255,1
GR.OVAL EoG1,630,200,950,1000
GR.COLOR 255,255,255,255,0
GR.OVAL EoG2,630,200,950,1000
GR.COLOR 255,255,0,0,1
GR.TEXT.SIZE 60
PAUSE 1000
IF FoundAll=4 THEN
	! Guessed in 'Turn' times
	GR.TEXT.DRAW TWin[1],700,400,"YES,"
	GR.TEXT.DRAW TWin[2],710,460,"You"
	GR.TEXT.DRAW TWin[3],680,520,"cracked"
	GR.TEXT.DRAW TWin[4],710,580,"the"
	GR.TEXT.DRAW TWin[5],700,640,"code"
	GR.TEXT.DRAW TWin[6],730,700,"in"
	GR.TEXT.DRAW TWin[7],700,760,FORMAT$("##",Turn)
	GR.TEXT.DRAW TWin[8],680,820," turns !"
ELSE
	! Not guessed
	GR.TEXT.DRAW TLoose[1],700,400,"Too"
	GR.TEXT.DRAW TLoose[2],690,460,"bad !"
	GR.TEXT.DRAW TLoose[3],690,580,"Code"
	GR.TEXT.DRAW TLoose[4],720,640,"not"
	GR.TEXT.DRAW TLoose[5],680,700,"cracked"
	GR.TEXT.DRAW TLoose[6],720,760,"......"
ENDIF
GR.RENDER
PAUSE 3000
IF FoundAll=4 THEN
	FOR i=1 TO 8
		GR.HIDE Twin[i]
	NEXT i
ELSE
	FOR i=1 TO 6
		GR.HIDE TLoose[i]
	NEXT i
ENDIF
GR.HIDE EoG1
GR.HIDE EoG2
GR.COLOR 255,0,0,0,1
GR.TEXT.DRAW TG,1,1110,"The CODE will be now be revealed"
GR.RENDER
PAUSE 2000
! Show Code
FOR i=255 TO 0 STEP -25.5
	! Code uncover
	GR.MODIFY cg,"alpha",i
	GR.MODIFY ct,"alpha",i
	GR.RENDER
	PAUSE 300
NEXT i
GR.HIDE TG
GR.RENDER
PAUSE 2000
! Play again ?
GOSUB DrwBut
GR.TEXT.DRAW g,100,950,"Play again"
GR.TEXT.DRAW g,680,950,"QUIT"
GR.RENDER
GOSUB GetAnswer
IF x<480*sx THEN
	GOTO Main
ENDIF
GR.CLOSE
POPUP "Goodbye !",0,0,0
PAUSE 2000
EXIT
DrwBut:
GR.COLOR 200,255,255,255,1
GR.RECT GA[1],50,850,430,1000
GR.RECT GA[2],560,850,930,1000
GR.COLOR 255,255,0,0,0
GR.RECT GA[3],50,850,430,1000
GR.RECT GA[4],560,850,930,1000
GR.TEXT.SIZE 60
RETURN
GetAnswer:
DO
	DO
		GR.TOUCH touched,x,y
	UNTIL touched
	DO
		GR.TOUCH touched,x,y
	UNTIL !touched
UNTIL y>850*sy & y<1000*sy
TONE 550,300
RETURN
ONBACKKEY:
GOSUB DrwBut
GR.TEXT.DRAW GA[5],100,950,"Continue"
GR.TEXT.DRAW GA[6],680,950," STOP"
GR.RENDER
GOSUB GetAnswer
IF x<480*sx THEN
	FOR i =1 TO 6
		GR.HIDE GA[i]
	NEXT i
	GR.RENDER
	BACK.RESUME
ENDIF
GR.CLOSE
POPUP "Goodbye !",0,0,0
PAUSE 2000
EXIT
