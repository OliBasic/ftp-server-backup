Droid Rally v0.9
with optional sensor gaming mode
by [Antonis]
created on Mar.5.2012

Use Left or  Right green part of screen to turn.
Use speed+ or speed- to change speed (1 or 2)
Avoid crashes with cow signs (-10 p.), androids (-50 p.),
street limits (-5 p.). Get as much gas as possible (30 p.)
After 2 min. game ends & restarts remembering highest score.

If you choose sensor gaming, after you made your choice, 
do not move your device till you see the game screen, because
a "calibration" is in progress! (you can turn the car by moving 
your device left or right and towards you or opposite, for changing speed) 
