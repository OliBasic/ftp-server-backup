REM droid rally
REM v0.9
REM by [Antonis]
! controls are:
! left blue part of screen
! & right blue part of screen 

gr.open 255,0,45,0
gr.color 255,255,255,255,0
gr.orientation 1
pause 1000
gr.screen w,h
sx=w/800
sy=h/1232
gr.scale sx,sy
x_sensitivity=0.05
y_sensitivity=0.1
! list my sensors
sR=0
sensors.list list$[]
array.length size, list$[]
for index = 1 to size
if left$(list$[index],2)="Ro" then sR=1
next index
pause 500

! let user decide type of game
if sR=1 then
 gr.text.size 40
 gr.text.draw t1,0,75,"Rotational Sensor detected!"
 gr.text.draw t2,0,150,"Enable Sensor Gaming?         [YES]     [NO]"
 ! gr.rect r1,490,100,630,180
 ! gr.rect r2,630,100,760,180
 gr.render
 do
  gr.touch touched,x,y
 until touched>0 & x>490*sx & x<760*sy & y>100*sy y<180*sy
 if x<630*sx then sR=1 else sR=2
 gr.cls 
endif

! load colors & bitmaps, initialize
array.load colors[],255,255,0, 255,0,255, 0,255,255, 255,65,55~
0,0,255, 0,255,0, 255,155,55, 155,255,55

gr.bitmap.load  bptr1,"redcar.png"
gr.bitmap.load  bptr2,"gas.png"
gr.bitmap.load  bptr3,"cow.png" 
gr.bitmap.load  bptr4,"droidrally.png" 
gr.bitmap.load  bptr5,"speedplus.png" 
gr.bitmap.load  bptr6,"speedminus.png" 

game=0
hiscore=0

begin:

! sensors.open rotation vector
if sR=1 then 
   sensors.open 11       
   sensors.read 11, p1, p2, fake
   pause 500
   sensors.read 11, p1, p2, fake
   r1=p1
   r2=p2
endif

! first draw street & load bitmaps
gr.color 255,0,0,100,1
gr.rect rect1,-5,-5,299,1285
gr.rect rect2,499,-5,801,1285
gr.color 255,255,255,255,0
gr.text.size 20
gr.text.draw mylogo,610,100,"Droid Rally v0.9"
gr.text.draw mylogo,610,120,"  by [Antonis]"
gr.text.size 39
game=game+1

! set parameters of new game
difficulty=1
speed=3 
difficulty=5
points=0
timdgame=clock()

! new level, positioning of car-obstacles
start:
gr.color 255,255,255,255,1
gr.bitmap.draw cow1,bptr3,361,200
gr.bitmap.draw cow2,bptr3,361,500
gr.bitmap.draw cow3,bptr3,361,810

gr.bitmap.draw gas1,bptr2,300,0
gr.bitmap.draw gas2,bptr2,450,308
gr.bitmap.draw gas3,bptr2,300,616
gr.bitmap.draw gas4,bptr2,450,924

gr.bitmap.draw droid1,bptr4,300,100
gr.bitmap.draw droid2,bptr4,450,408
gr.bitmap.draw droid3,bptr4,300,716
gr.bitmap.draw droid4,bptr4,450,1024

gr.bitmap.draw car,bptr1,375,1132

gr.bitmap.draw speedplus,bptr5,500,1112
gr.bitmap.draw speedminus,bptr6,0,1112
gr.render
pause 2000

! then begin redcar from 375,1132 
mx=375 
my=1132
gy1=0    % gas 1
gy2=308  % gas 2
gy3=616  % gas 3
gy4=924  % gas 4

gy5=200 % cow 1
gy6=500 % cow 2
gy7=810 % cow 3

gy8=100   % droid 1
gy9=408   % droid 2
gy10=716  % droid 3
gy11=1024 % droid 4

gr.text.draw ttm,1,100,"Time: 0 s."
gr.text.draw tx,1,900,"Game #"+left$(str$(game),len(str$(game))-2)
gr.text.draw spd,1,500,"Speed: "+left$(str$(speed),len(str$(speed))-2)
gr.text.draw pnt,1,300,"Points: 0"
gr.text.draw hsc,1,700,"Hi Score: "+left$(str$(hiscore),len(str$(hiscore))-2)

! now play 
g=clock()
do % loop whole game 
  gr.touch touched,x,y
   if clock()-g>=100 then 
     g=clock()
     gr.color 255,255,0,0,1
     secs$=str$((clock()-timdgame)/1000)
     gr.color 255,0,255,0,1
     gr.modify ttm,"text","Time: "+left$(secs$,len(secs$)-2)+" s."
     gr.color 255,0,255,0,1
     gr.modify pnt,"text","Points: "+left$(str$(points),len(str$(points))-2)
     if clock()-timdgame>120000 then % end of game
         gr.color 255,0,0,0,1
         gr.rect rct, 200,300,600,500
         gr.color 255,255,0,0,1
         gr.text.draw poi,250,400,"Points: "+left$(str$(points),len(str$(points))-2)
         gr.render
         pause 4000
         gr.cls
         if points>hiscore then hiscore=points
         sensors.close
         goto begin

     endif
     if speed=5 then sp=2 else sp=1
     gr.modify spd,"text","Speed: "+left$(str$(sp),1)
   endif
   
! check for collision
a1=gr_collision (car,gas1)
a2=gr_collision (car,gas2)
a3=gr_collision (car,gas3)
a4=gr_collision (car,gas4)
a5=gr_collision (car,rect1)
a6=gr_collision (car,rect2)
a7=gr_collision (car,cow1)
a8=gr_collision (car,cow2)
a9=gr_collision (car,cow3)
a10=gr_collision (car,droid1)
a11=gr_collision (car,droid2)
a12=gr_collision (car,droid3)
a13=gr_collision (car,droid4)

if a1<>0 | a2<>0 | a3<>0 | a4<>0 then
    ! gas, take 30 points
   points=points+30   
endif
if a5<>0 | a6<>0 then
    ! road crash, lose 5 points
   points=points-5
   endif
if a7<>0 | a8<>0 | a9<>0 then
    ! cow crash, lose 10 points
   points=points-10
   endif
if a10<>0 | a11<>0 | a12<>0 | a13<>0 then
    ! droid crash, lose 50 points
   points=points-50
   endif

   ! if screen touched or sensor changed, change coordinates of car
 if touched>0 then 
     if x<300*sx & y<1112*sy & mx>=300 then  mx=mx-difficulty    
     if x>600*sx & y<1112*sy & mx<=450 then mx=mx+difficulty 
	 if x<300*sx & y>=1112*sy & speed=5 then speed=3
	 if x>600*sx & y>=1112*sy  & speed=3 then speed=5
 endif
 if sR=1 then
        sensors.read 11, p1, p2, p3
        if p1-r1>x_sensitivity & mx>=300 then mx=mx-difficulty    
        if r1-p1>x_sensitivity & mx<=450 then mx=mx+difficulty
	     	if p2-r2>y_sensitivity  & speed=3 then speed=5    
        if r2-p2>y_sensitivity & speed=5 then speed=3
 endif
 
 ! turn car & in every case avance gas till 1232-50
 gr.modify car, "x",mx
 
 gy1=gy1+speed % y of gas 1
 gy2=gy2+speed % y of gas 2
 gy3=gy3+speed % y of gas 3
 gy4=gy4+speed % y of gas 4
 gy5=gy5+speed % y of cow 1
 gy6=gy6+speed % y of cow 2
 gy7=gy7+speed % y of cow 3
 gy8=gy8+speed % y of droid1
 gy9=gy9+speed % y of droid2
 gy10=gy10+speed % y of droid3
 gy11=gy11+speed % y of droid4

 if gy1>1232 then gy1 = 0  % end of screen for gas1
 if gy2>1232 then gy2 = 0  % end of screen for gas2
 if gy3>1232 then gy3 = 0  % end of screen for gas3
 if gy4>1232 then gy4 = 0  % end of screen for gas4
 if gy5>1232 then gy5 = 0  % end of screen for cow1
 if gy6>1232 then gy6 = 0  % end of screen for cow2
 if gy7>1232 then gy7 = 0  % end of screen for cow3
 if gy8>1232 then gy8 = 0  % end of screen for droid1
 if gy9>1232 then gy9 = 0  % end of screen for droid2
 if gy10>1232 then gy10 = 0  % end of screen for droid3
 if gy11>1232 then gy11 = 0  % end of screen for droid4

 gr.modify gas1, "y", gy1
 gr.modify gas2, "y", gy2
 gr.modify gas3, "y", gy3
 gr.modify gas4, "y", gy4
 gr.modify cow1, "y", gy5
 gr.modify cow2, "y", gy6
 gr.modify cow3, "y", gy7
 gr.modify droid1, "y", gy8
 gr.modify droid2, "y", gy9
 gr.modify droid3, "y", gy10
 gr.modify droid4, "y", gy11
 gr.render

until 0 % loop whole game for ever :)
 
 
