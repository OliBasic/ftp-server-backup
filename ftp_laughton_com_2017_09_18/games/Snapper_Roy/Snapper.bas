Rem Snapper
Rem For Android
Rem With RFO Basic!
Rem September/October 2016
Rem Version 1.00
Rem By Roy Shepherd

!Debug.on

di_height = 672 % set to my Device
di_width = 1152

gr.open 255,128,128,255
gr.orientation 0 % landscape 
pause 1000
WakeLock 3
 
gr.screen screenWidth, screenHeight
scale_x = screenWidth / di_width
scale_y = screenHeight / di_height
gr.scale scale_x, scale_y

!-------------------------------------------------
!Let Basic see the functions before they are called
!-------------------------------------------------
gosub Functions

gosub Initialise
gosub SetUp
gosub PlaySnapper

end

onBackKey:
    gosub ExitSnapper
back.resume 


!------------------------------------------------
! Do once at first run of game
!------------------------------------------------
Initialise:
    !Set some variables 
    !path$ = "Snapper/data/"
    path$ = "../../Snapper/data/"
    FALSE = 0 : TRUE = 1
    sound = TRUE : buzz = TRUE : music = TRUE
    highScore = 0 : IN_CAVE = 40
    gosub LoadData
    
    snapLevel = 1 : snapLives = 3
    score = 0 : totalPills = 475
    ghostReverse = FALSE
    snapX = 427 : snapY = 483
    
    fruitX = 427 : fruitY = 483
   
    snapStep_X = 8 : snapStep_Y = 8
    dim eyesRunning[4]
    dim eyesPathX[4, 350], eyesPathY[4, 350]
    dim backToCave[4]
    list.create n, eyePosX : list.create n, eyePosY
    
    gosub DrawScenery
    gosub DrawDotsInMaze
    gosub LoadSnapper
    gosub LoadGhosts
    gosub LoadFruits
    gosub DrawControler
    gosub DrawCollisionBarsInMaze
    gosub LoadSounds
    
    ! Hide snapper when she comes into the maze from the right
    gr.color 255,128,128,255,1
    gr.rect null, 880, (di_height / 2) - 50, 940, (di_height / 2) + 50 
    
     ! Options button
    gr.color 255,135,145,250
    gr.rect null, 890, 260, di_width - 10, 305
    gr.color 100,10,10,10, 0
    gr.rect butOptions, 890, 260, di_width - 10, 305
    
    !Options text
    gr.text.size 35 : gr.color 255,255,255,255, 1
    gr.text.bold 1 : gr.text.draw null, 940,292, "Options"
    gr.color 255,255,0,0,0 : gr.text.draw txtOptions, 940,292, "Options"
    gr.color 255,255,255,255,1 : gr.text.bold 0
    gr.text.size 30

    ! Collision for when a button is tapped
    ! and to control and detact when snapper collides 
    gr.color 0,0,0,0,0
    gr.circle testCollision, - 12, - 12, 15 % Look for opening in maze
    gr.circle bump, - 5, - 5, 5 % snapper bumps in wall maze
    
    gr.point collision, - 1, - 1 % Control the snapper
    
    gr.circle ghostBox, - 5, - 5, 30 % ghosts bumps in wall maze
    gosub DrawOptionsMenu
    gosub DrawSnapperHelp
    gr.render
return

!------------------------------------------------
! Draw the Options Menu
!------------------------------------------------
DrawOptionsMenu:
    dim menu[13] % use this to hide, show the menu 

    gr.bitmap.load opt, path$ + "btmOptions.png"
    if opt = - 1 then call ImageLoadError("btmOptions")
    gr.bitmap.load tick, path$ + "btmTick.png"
    if tick = - 1 then call ImageLoadError("btmTick")
    gr.bitmap.scale tick, tick, 27, 23
    
    ! Gray out screen for menu
    gr.color 150,0,0,0,1
    gr.rect menu[1], 0, 0, di_width, di_height
    !Draw the options menu
    gr.color 255,0,0,0,1
    gr.bitmap.draw menu[2], opt, (di_width / 2) - 137, (di_height / 2) - 237
    ! Draw the ticks
    gr.bitmap.draw menu[3], tick, 632, 160 % music tick
    gr.bitmap.draw menu[4], tick, 632, 193 % sound tick
    gr.bitmap.draw menu[5], tick, 632, 227 % music tick
    
    ! Button
    gr.color 0,255,255,255,0
    gr.rect menu[6],  458, 151, 699, 182  % Music
    gr.rect menu[7],  458, 188, 699, 217  % Sound
    gr.rect menu[8],  458, 222, 699, 260  % Buzz
    gr.rect menu[9],  458, 276, 699, 325  % Reset High Score
    gr.rect menu[10], 458, 346, 699, 380  % About
    gr.rect menu[11], 458, 383, 699, 414  % Help
    gr.rect menu[12], 458, 417, 699, 460  % Exit Snapper
    gr.rect menu[13], 458, 489, 699, 540  % Close Menu
    
    for off = 1 to 13
        gr.hide menu[off]
    next
    
    gr.render
    
return

!------------------------------------------------
! Draw the Help screen
!------------------------------------------------
DrawSnapperHelp:
    dim help[20]
    y = 50 : x = 50 : ofset = 30 : xoff = 50
    ! Gray out screen for help
    gr.color 150,0,0,0,1
    gr.rect help[1], 0, 0, di_width, di_height
    gr.color 255,255,255,0

    gr.text.draw help[2], x, y, "Guide Snapper through the maze eating pills and fruit, that may appear for a"
    y += ofset 
    gr.text.draw help[3], x, y, "short time, while trying to avoid the ghosts from the cave. Eat the red faced"
    y += ofset  
    gr.text.draw help[4], x, y, "men to turn the ghosts blue and make them edible, once eaten their eyes return"
    y += ofset  
    gr.text.draw help[5], x, y, "to the cave. Uneaten ghost soon start to flash which means their regeneration is"
    y += ofset
    gr.text.draw help[6], x, y, "imminent, so watch out!" : y += ofset
    y += ofset / 2
   
    gr.text.draw help[7], x, y, "When all the pills are eaten you tackle a new screenful, but the ghost get"
    y += ofset
    gr.text.draw help[8], x, y, "progressively faster. You have three lives." : y += ofset
    y += ofset / 2
    gr.color 255,0,255,0
    gr.text.draw help[9], x, y, "Point:" : y += ofset
    gr.text.draw help[10], x + xoff, y, "Pills = 10" : y += ofset
    gr.text.draw help[11], x + xoff, y, "Red Faced men = 50" : y += ofset
    gr.text.draw help[12], x + xoff, y, "Fruits = 100" : y += ofset
    gr.text.draw help[13], x + xoff, y, "Ghosts = 150" : y += ofset
    gr.text.draw help[14], x + xoff, y, "New screen = 1000" : y += ofset
    y += ofset / 2
    
    gr.text.draw help[15], x, y, "Controls:" : y += ofset
    gr.text.draw help[16], x + xoff, y, "The arrows on the screen." : y += ofset
    gr.text.draw help[17], x + xoff, y, "The arrows on a Blue-tooth keyboard." : y += ofset
    gr.text.draw help[18], x + xoff, y, "The rocker switch on a Game-pad." : y += ofset
    y += ofset / 2
    gr.color 255,0,255,255
    gr.rect help[19],x + 300, y - 30, 700, y + 10
    gr.color 255,255,0,0
    gr.text.draw help[20], x + 300, y, "Tap to close Snapper Help"
    for x = 1 to 20 
        gr.hide help[x]
    next
return

!------------------------------------------------
! Do at start of each new game
!------------------------------------------------
SetUp:
    score = 0 : pillsEaten = 0 : snapDead = FALSE
    levelOver = FALSE : gameOver = False
    SNAP_UP = FALSE : SNAP_DOWN = FALSE : SNAP_RIGHT = FALSE : SNAP_LEFT = TRUE

    snapAnimation = 1
    fruitOn = FALSE : fruitOff = FALSE
    allBlue = FALSE
    gameOver = FALSE
    snapDead = FALSE
    snapLives = 3
    snapLevel = 1
    gr.modify txtLevel, "text", "Level: " + int$(snapLevel)
    for x = 1 to 4
        ghostStepX[x] = snapStep_X + snapLevel - 1
        ghostStepY[x] = snapStep_Y + snapLevel - 1
    next
return

!------------------------------------------------
! Revive Snapper from the dead, unless all live are used 
!------------------------------------------------
NewSnapper:
    SNAP_UP = FALSE : SNAP_DOWN = FALSE : SNAP_RIGHT = FALSE : SNAP_LEFT = TRUE
    snapLives -- 
    if  snapLives > 0 then gr.hide snapLife[snapLives]
    if snapLives = 0 then gosub SnapperOver : return
    snapDead = FALSE
    gosub ResetSnapGhost
return
  
!------------------------------------------------
! Reset Snapper and ghost stuff
!------------------------------------------------  
ResetSnapGhost:    
    pillsEaten = 0 
    snapX = 427 : snapY = 483
    gr.hide snapFruit
    for x = 1 to 475
        !set GHOST_UP[x] to false so no ghost will run then can test snapper
        if x < 5 then 
            ghostNum[x] = 1 
            gr.show redBalls[x]
            GHOST_UP[x] = TRUE     : GHOST_DOWN[x] = FALSE 
            GHOST_RIGHT[x] = FALSE : GHOST_LEFT[x] = FALSE
            ghostNum[x] = 1 : eyesRunning[x] = FALSE
        endif
        gr.show pillPtr[x]
    next
    !Reset ghosts
    ghostX[1] = blinky_X : ghostX[2] = pinky_X : ghostX[3] = clyde_X : ghostX[4] = inky_X
    ghostY[1] = blinky_Y : ghostY[2] = pinky_Y : ghostY[3] = clyde_Y : ghostY[4] = inky_Y
    
    SNAP_UP = FALSE : SNAP_DOWN = FALSE : SNAP_RIGHT = FALSE : SNAP_LEFT = TRUE
    gr.hide snapUp : gr.Hide SnapDown : gr.hide snapLeft : gr.hide snapRight
    gr.show redBalls[1] : gr.show redBalls[2]
    gr.modify snapperLeft[1], "x", snapX, "y", snapY
    gr.show snapperLeft[1]
    
    allBlue = FALSE
    ghostReverse = FALSE
    gr.modify ghost[1], "bitmap", bmp_prt_ghost1
    gr.modify ghost[2], "bitmap", bmp_prt_ghost2
    gr.modify ghost[3], "bitmap", bmp_prt_ghost3
    gr.modify ghost[4], "bitmap", bmp_prt_ghost4
    
    gr.modify ghost[blinky], "x", blinky_X, "y", blinky_Y
    gr.modify ghost[pinky], "x", pinky_X, "y", pinky_Y
    gr.modify ghost[clyde], "x", clyde_X, "y", clyde_Y
    gr.modify ghost[inky], "x", inky_X, "y", inky_Y
    
    gr.show ready
    gr.render
    if sound then call PlaySound(snapBeginning)
    pause 4000
    gr.hide ready
return

!------------------------------------------------
! All pills eaten. Go up a level. Ghosts move faster 
!------------------------------------------------
MoveUpLevel:
    ghostReverse = FALSE : if sound then gosub StopIntermission
    pause 2000
    snapLevel ++ : levelOver = FALSE
    gr.modify txtLevel, "text", "Level: " + int$(snapLevel)
    score += 1000
    gr.modify txtScore, "text", "Score: " + int$(score)
    ! Any faster would be to fast
    if snapLevel < 6 then
        for x = 1 to 4
            ghostStepX[x] = snapStep_X + snapLevel - 1
            ghostStepY[x] = snapStep_Y + snapLevel - 1
        next
    endif
   gosub ResetSnapGhost
return

!------------------------------------------------
! All lives used. Reset High Score if need be 
! and an opption to start a new game
!------------------------------------------------
SnapperOver:
    gameOver = TRUE
    s$="Game Over"
    if score > highScore then
        s$="Game Over. New High Score"
        highScore = score 
        gr.modify txtHigh, "text", "High Score: " + int$(highScore)
        gr.render
    endif
    
    do : dialog.message s$, "New Game" ,yn, "Yes", "No" : until yn > 0
    if yn = 1 then 
        gosub setup
        gr.show snapLife[1] : gr.show snapLife[2]
        gosub ResetSnapGhost
    else 
        gosub SaveData : pause 1000 
        exit
    endif
return

!------------------------------------------------
! Load and draw the game maze
! Draw Score and High Score
!------------------------------------------------
DrawScenery:

    ! Load and display splash screen
    gr.bitmap.load snapperSplash, path$ + "snapperSplash.png"
    if snapperSplash = - 1 then call ImageLoadError("snapperSplash")
    gr.bitmap.draw splash, snapperSplash, 0, 0 
    gr.render : pause 2000
    gr.cls
    gr.bitmap.delete snapperSplash
    
    !Load the background maze
    gr.bitmap.load snapperMaze, path$ + "snapperBG.png"
    if snapperMaze = - 1 then call ImageLoadError("snapperBG")
    gr.bitmap.draw snapperMaze, snapperMaze, 0, 0
    
    ! Draw score, and high score at top of screen
    gr.color 255,135,145,250
    gr.rect null, 890, 0, di_width - 10, 45
    gr.color 100,10,10,10, 0
    gr.rect null, 890, 0, di_width - 10, 45
    
    gr.text.size 35 : gr.color 255,255,255,255, 1
    gr.text.bold 1 : gr.text.draw null, 940,32, "Snapper"
    gr.color 255,255,0,0,0 : gr.text.draw null, 940,32, "Snapper"
    gr.color 255,255,255,255,1 : gr.text.bold 0
    gr.text.size 30
    gr.text.draw txtScore, 885, 80,  "Score: " + int$(score)
    gr.text.draw txtHigh, 885, 120,  "High Score: " + int$(highScore)
    gr.text.draw txtLevel, 885, 160, "Level: " + int$(snapLevel)
    gr.text.draw txtLives, 885, 200, "Lives: "
    gr.text.draw txtFruit, 885, 240, "Fruits: "
    
    gr.text.bold 1 :  gr.text.size 40
    gr.color 255,255,255,0,1 : gr.text.draw txtReady1, 350,248, "R E A D Y !"
    gr.color 255,255,0,0, 0  : gr.text.draw txtReady2, 350,248, "R E A D Y !"
    gr.text.bold 0 : gr.color 255,255,255,255, 1
    
    gr.group ready, txtReady1, txtReady2
return

!------------------------------------------------
! Load the snapper's left, right and up, images
!------------------------------------------------
LoadSnapper:
    dim snapperLeft[3], snapperRight[3], snapperUp[3], snapperDown[3] 
    dim snapLife[2]
    gr.bitmap.load snap, path$ + "snappers.png"
    if snap = - 1 then call ImageLoadError("snappers")
    
    for x = 1 to 12
        gr.bitmap.crop s, snap, (x - 1) * 40 , 0, 40, 40
        if x < 3 then  gr.bitmap.draw snapLife[x], s, - 50, - 50
        if x < 4 then gr.bitmap.draw snapperLeft[x], s, snapX, snapY
        if x > 3 & x < 7 then gr.bitmap.draw snapperRight[x - 3], s, snapX, snapY
        if x > 6 & x < 10 then gr.bitmap.draw snapperUp[x - 6], s, snapX, snapY
        if x > 9 then gr.bitmap.draw snapperDown[x - 9], s, snapX, snapY
    next
    gr.modify snapLife[1], "x", 970, "y", 170
    gr.modify snapLife[2], "x", 1010, "y", 170
   
    gr.bitmap.delete snap
    
   gr.group snapLeft, snapperLeft[1], snapperLeft[2], snapperLeft[3]
   gr.group snapRight, snapperRight[1], snapperRight[2], snapperRight[3]
   gr.group snapUp, snapperUp[1], snapperUp[2], snapperUp[3]
   gr.group snapDown, snapperDown[1], snapperDown[2], snapperDown[3]
   
   gr.hide snapRight : gr.hide snapUp : gr.hide snapDown  : gr.hide snapLeft
return

!------------------------------------------------
! Load the Ghosts and place them in the ghost cave
! Blinky, Pinky, Inky, Clyde and when they're running
! scared Bluey and Whitey
!------------------------------------------------
LoadGhosts:
    blinky_X = 427 : blinky_Y = 250
    pinky_X = 427 : pinky_Y = 320
    clyde_X = 485 : clyde_Y = 320
    inky_X = 370 : inky_Y = 320
    
    bluey_X = 370 : bluey_Y = 380
    whitey_X = 485 : whitey_Y = 380
    
    eyes_X = 427 : eyes_Y = 380
    
    blinky = 1 : pinky = 2 : clyde = 3 : inky = 4 : bluey = 5 : whitey = 6 : eyes = 7
    
    array.load ghost[], blinky, pinky, clyde, inky, bluey, whitey, eyes
    array.load ghostX[], blinky_X, pinky_X, clyde_X, inky_X, bluey_X, whitey_X, eyes_X
    array.load ghostY[], blinky_Y, pinky_Y, clyde_Y, inky_Y, bluey_Y, whitey_Y, eyes_Y
    
    array.load ghostStartX[], blinky_X, pinky_X, clyde_X, inky_X, bluey_X, whitey_X, eyes_X
    array.load ghostStartY[], blinky_Y, pinky_Y, clyde_Y, inky_Y, bluey_Y, whitey_Y, eyes_Y
    
    array.load ghostStepX[], snapStep_X + snapLevel - 1, snapStep_X + snapLevel - 1, snapStep_X + snapLevel - 1, snapStep_X + snapLevel - 1
    array.load ghostStepY[], snapStep_Y + snapLevel - 1, snapStep_Y + snapLevel - 1, snapStep_Y + snapLevel - 1, snapStep_Y + snapLevel - 1
        
    gr.bitmap.load ghost, path$ + "ghosts.png"
    if ghost = - 1 then call ImageLoadError("ghosts")
    for x = 1 to 6
        gr.bitmap.crop g, ghost, (x - 1) * 30 , 0, 30, 40
        if x = 1 then gr.bitmap.draw ghost[blinky], g, ghostX[blinky], ghostY[blinky]
        if x = 2 then gr.bitmap.draw ghost[pinky], g, ghostX[pinky], ghostY[pinky]
        if x = 3 then gr.bitmap.draw ghost[clyde], g, ghostX[clyde], ghostY[clyde]
        if x = 4 then gr.bitmap.draw ghost[inky], g, ghostX[inky], ghostY[inky]
        if x = 5 then gr.bitmap.draw ghost[bluey], g, ghostX[bluey], ghostY[bluey]
        if x = 6 then gr.bitmap.draw ghost[whitey], g, ghostX[whitey], ghostY[whitey]
    next
    gr.bitmap.delete ghost
    gr.bitmap.load e, path$ + "ghostEyes.png"
    if e = - 1 then ImageLoadError("ghostEyes")
    gr.bitmap.draw ghost[eyes], e, ghostX[eyes], ghostY[eyes]
   
    gr.hide ghost[bluey] : gr.hide ghost[whitey] : gr.hide ghost[eyes]
    gr.get.value ghost[1], "bitmap", bmp_prt_ghost1 % Blinky
    gr.get.value ghost[2], "bitmap", bmp_prt_ghost2 % Pinky
    gr.get.value ghost[3], "bitmap", bmp_prt_ghost3 % Clyde
    gr.get.value ghost[4], "bitmap", bmp_prt_ghost4 % Inky
    gr.get.value ghost[5], "bitmap", bmp_prt_ghost5 % Bluey
    gr.get.value ghost[6], "bitmap", bmp_prt_ghost6 % Whitey
    gr.get.value ghost[7], "bitmap", bmp_prt_ghost7 % Eyes
    
    array.load ghostBmpPrt[], bmp_prt_ghost1, bmp_prt_ghost2,bmp_prt_ghost3, bmp_prt_ghost4,~
                              bmp_prt_ghost5, bmp_prt_ghost6, bmp_prt_ghost7
   gosub GuidingGhosts
return

!------------------------------------------------
! Arrays for guiding the ghosts through the maze
!------------------------------------------------
GuidingGhosts:
    ! blinky  pinky clyde inky
    G_UP = 1 : G_DOWN = 2 : G_LEFT = 3 : G_RIGHT = 4
    
    dim GHOST_UP[4], GHOST_DOWN[4], GHOST_LEFT[4], GHOST_RIGHT[4]
    for x = 1 to 4 
        GHOST_UP[x] = TRUE :    GHOST_DOWN[x] = FALSE
        GHOST_LEFT[x] = FALSE : GHOST_RIGHT[x] = FALSE
    next
    dim ghostNum[4]
    array.fill ghostNum[], 1
    
    !The maze walls and gets where the ghost turn
    dim mazeNum[4,29]
    array.load loadNums[],19,46,18,47,14,48,1,49,7,2,50,51,1,2,7,22,30,52,21,53,1,1,1,1,1,1,1,1,1, ~
                          19,54,18,55,16,48,1,56,9,6,57,58,1,6,9,24,31,59,23,53,1,1,1,1,1,1,1,1,1, ~
                          26,66,19,24,60,61,31,67,44,62,40,69,42,5,10,29,38,62,40,39,4,5,45,63,42,5,10,59,68, ~
                          26,65,19,22,71,72,30,73,34,74,37,76,32,3,8,29,38,74,37,39,4,3,35,75,32,3,8,52,70
    array.copy loadNums[], mazeNum[]
    
    ! The left, right, up and down turns of the four ghosts
    dim direction[4,29]
    array.load d[],3,1,3,1,4,1,3,2,3,1,4,1,3,2,4,2,4,1,4,1,0,0,0,0,0,0,0,0,0, ~
                   4,1,4,1,3,1,4,2,4,1,3,1,4,2,3,2,3,1,3,1,0,0,0,0,0,0,0,0,0, ~
                   3,1,4,2,4,2,4,2,3,2,4,1,4,1,3,2,4,2,3,2,4,1,3,1,4,1,3,1,4, ~
                   4,1,3,2,3,2,3,2,4,2,3,1,3,1,4,2,3,2,4,2,3,1,4,1,3,1,4,1,3
    array.copy d[],direction[] 
   
   array.delete d[], loadNum[]
return

!------------------------------------------------
! Load the bonus fruits for snapper to eat
!------------------------------------------------
LoadFruits:
    dim bonusFruit[4]
    gr.bitmap.load fruit, path$ + "fruits.png"
    if fruit = - 1 then ImageLoadError("fruit")
    for x = 1 to 4
        gr.bitmap.crop f, fruit, (x - 1) * 40 , 0, 40, 40
        gr.bitmap.draw bonusFruit[x], f, fruitX, fruitY
    next
    gr.group snapFruit, bonusFruit[1], bonusFruit[2], bonusFruit[3], bonusFruit[4]
    gr.hide snapFruit
    gr.bitmap.draw f, fruit, 980, 210
return
!------------------------------------------------
! Draw the dots and the red balls on snapper's mazes
!------------------------------------------------
DrawDotsInMaze:
    dim redBalls[4]
    for x = 1 to 4
        gr.bitmap.load redBalls[x], path$ + "redBall.png"
        if redBalls[x] = - 1 then call ImageLoadError("redBall")
    next
    ! Pills are 1 spaces are 0
    dim pillPtr[476] % 475 pills
    array.load pills[],1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1, ~
                       1,0,0,0,1,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0,1, ~
                       0,0,0,0,1,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,1,0,0,0,0, ~
                       1,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,1, ~
                       1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1, ~
                       1,0,0,0,1,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0,1, ~
                       1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1, ~
                       0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0, ~
                       0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0, ~
                       0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0, ~
                       0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0, ~
                       0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0, ~
                       1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1, ~
                       1,0,0,0,1,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0,1, ~
                       0,1,1,0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,1,1,0, ~
                       0,0,1,0,0,0,1,0,1,0,0,0,0,0,0,0,1,0,1,0,0,0,1,0,0, ~
                       1,1,1,1,1,1,1,0,1,1,1,1,0,1,1,1,1,0,1,1,1,1,1,1,1, ~
                       1,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,1, ~
                       1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                       
                      
    gr.color 255,50,255,50
    xx = 1 : ballX = 1 : ballY = 1
    
    for y = 33 to 652 step 33.8
         for x = 35 to 860 step 33.8
            if pills[xx] = 1 then gr.circle cirPrt, x, y, 3
            pillPtr[xx] = cirPrt
            if ballX = 1 & ballY = 3 then gr.bitmap.draw redBalls[1], redBalls[1], x - 10, y - 10
            if ballX = 25 & ballY = 3 then gr.bitmap.draw redBalls[2], redBalls[2], x - 10, y - 10
            if ballX = 1 & ballY = 15 then gr.bitmap.draw redBalls[3], redBalls[3], x - 10, y - 10
            if ballX = 25 & ballY = 15 then gr.bitmap.draw redBalls[4], redBalls[4], x - 10, y - 10
        
            xx ++ : ballX ++
        next
        ballX = 1 : ballY ++
    next
    gr.render
return
!------------------------------------------------
! Load and draw and snapper's controler
!------------------------------------------------
DrawControler:
    ! Load, scale and draw the snapper controler 
    w = di_width : h = di_height : c = 40
     
    gr.bitmap.load snapperArrows, path$ + "snapperArrows.png"
    if snapperArrows  = -1 then call ImageLoadError("snapperArrows")
    gr.bitmap.scale snapperArrows, snapperArrows, 270, 290
    
    gr.bitmap.size snapperArrows, ax, ay
    gr.bitmap.draw snapperArrows,snapperArrows, (w - ax) - 1, (h  - ay) - 50
    
    gr.color 0,0,0,0,0
    gr.rect butUp, 964, 333, 1070, 420
    gr.rect butLeft, 885, 426, 970, 522
    gr.rect butDown, 965, 530, 1065, 620
    gr.rect butRight, 1060, 426, 1150, 524
    gr.render
    
return

!------------------------------------------------
! Draw the collision rectangles on the maze
!------------------------------------------------
DrawCollisionBarsInMaze:
    dim mazeBars[80]
    gr.color 0,255,255,255, 0
    gr.rect mazeBars[1], 0, 0, 875, 5
    gr.rect mazeBars[2], 0, 0, 7, 268
    gr.rect mazeBars[3], 0, 358, 7, 667
    gr.rect mazeBars[4], 0, 670, 875, 662
    gr.rect mazeBars[5], 871, 393, 877, 668
    gr.rect mazeBars[6], 871, 0, 877, 274
    gr.rect mazeBars[7], 0, 257, 212, 310
    gr.rect mazeBars[8], 0, 360, 212, 412
    gr.rect mazeBars[9], 669, 255, 877, 312
    gr.rect mazeBars[10], 670, 358, 877, 413
    gr.rect mazeBars[11], 430, 0, 450, 73
    gr.rect mazeBars[12], 60, 53, 145, 141
    gr.rect mazeBars[13], 196, 54, 280, 141
    gr.rect mazeBars[14], 274, 54, 381, 75
    gr.rect mazeBars[15], 599, 54, 686, 142
    gr.rect mazeBars[16], 499, 54, 603, 75
    gr.rect mazeBars[17], 736, 54, 822, 141
    gr.rect mazeBars[18], 330, 121, 551, 141
    gr.rect mazeBars[19], 430, 134, 450, 209
    gr.rect mazeBars[20], 59, 189, 144, 211
    gr.rect mazeBars[21], 195, 189, 382, 207
    gr.rect mazeBars[22], 261, 205, 281, 310
    gr.rect mazeBars[23], 499, 189, 686, 210
    gr.rect mazeBars[24], 600, 204, 619, 312
    gr.rect mazeBars[25], 737, 189, 821, 212
    gr.rect mazeBars[26], 330, 256, 550, 411
    gr.rect mazeBars[27], 262, 357, 280, 413
    gr.rect mazeBars[28], 601, 357, 619, 411
    gr.rect mazeBars[29], 430, 404, 449, 478
    gr.rect mazeBars[30], 193, 460, 381, 482
    gr.rect mazeBars[31], 499, 460, 686, 482
    gr.rect mazeBars[32], 58, 460, 143, 479
    gr.rect mazeBars[33], 126, 460, 146, 545
    gr.rect mazeBars[34], 125, 527, 211, 547
    gr.rect mazeBars[35], 3, 529, 76, 548
    gr.rect mazeBars[36], 261, 525, 280, 595
    gr.rect mazeBars[37], 56, 594, 380, 620
    gr.rect mazeBars[38], 328, 528, 552, 547
    gr.rect mazeBars[39], 430, 543, 449, 615
    gr.rect mazeBars[40], 499, 595, 822, 618
    gr.rect mazeBars[41], 601, 526, 618, 592
    gr.rect mazeBars[42], 736, 460, 821, 480
    gr.rect mazeBars[43], 735, 459, 754, 547
    gr.rect mazeBars[44], 668, 527, 753, 547
    gr.rect mazeBars[45], 802, 527, 872, 547
    
    ! Ghost 1 Pinky
    gr.rect mazeBars[46], 360, 207, 380, 255 % 1
    gr.rect mazeBars[47], 263, 140, 277, 190 % 2
    gr.rect mazeBars[48], 428, 70, 450, 120  % 3
    gr.rect mazeBars[49], 125, 2, 148, 51    % 4
    gr.rect mazeBars[50], 4, 119, 57, 140    % 5
    gr.rect mazeBars[51], 195, 140, 214, 187 % 6
    gr.rect mazeBars[52], 328, 410, 345, 458 % 7
    gr.rect mazeBars[53], 429, 205, 447, 251 % 8
    
    ! Ghost 2
    gr.rect mazeBars[54], 497, 208, 513, 252  % 9
    gr.rect mazeBars[55], 598, 138, 615, 188 % 10
    gr.rect mazeBars[56], 734, 3, 753, 53    % 11
    gr.rect mazeBars[57], 820, 137, 873, 124 % 12
    gr.rect mazeBars[58], 669, 139, 683, 189 % 13
    gr.rect mazeBars[59], 530, 409, 545, 458 % 14
    
    ! Ghost 3
    gr.rect mazeBars[60], 600, 355, 550, 370
    gr.rect mazeBars[61], 668, 309, 690, 357
    gr.rect mazeBars[62], 598, 480, 615, 525
    gr.rect mazeBars[63], 732, 544, 754, 593
    gr.rect mazeBars[64], 597, 308, 550, 292
    gr.rect mazeBars[65], 467, 283, 485, 325 % Ghost 4
    gr.rect mazeBars[66], 415, 283, 398, 325
    gr.rect mazeBars[67], 734, 458, 752, 410
    gr.rect mazeBars[68], 550, 290, 598, 305
    gr.rect mazeBars[69], 801, 545, 819, 594
    
    ! Ghost 4
    gr.rect mazeBars[70], 279, 307, 329, 295
    gr.rect mazeBars[71], 278, 355, 329, 373
    gr.rect mazeBars[72], 210, 309, 188, 358
    gr.rect mazeBars[73], 123, 410, 143, 458
    gr.rect mazeBars[74], 259, 523, 280, 481
    gr.rect mazeBars[75], 124, 542, 145, 590
    gr.rect mazeBars[76], 75, 545, 54, 591
return


!------------------------------------------------
! Main loop for Playing Snapper
!------------------------------------------------
PlaySnapper:
    gr.color 255,255,255,255,0
    if sound then call PlaySound(snapBeginning)
    gr.show snapperLeft[1] : gr.render : pause 4000
    gr.hide ready : gr.render
    
    do
        now = time()
        do
            gosub ScreenPad
            gosub GamePad
            gosub MoveGhosts
            gosub MoveSnapper
            gosub FruitMaybe
            if ghostReverse then gosub TurnGhostBlue : gosub TurnGhostBack
            if SNAP_UP = FALSE & SNAP_DOWN = FALSE & SNAP_RIGHT = FALSE & SNAP_LEFT = FALSE then gosub AnimateSnap
            gr.render
        until snapDead | levelOver
        if snapDead then gosub NewSnapper
        if levelOver then gosub MoveUpLevel
    until gameOver  
return

!------------------------------------------------
! Control Snapper with the virtual pad
!------------------------------------------------
ScreenPad:
    gr.touch t, tx, ty
    if ! t then return 
    tx /= scale_x : ty /= scale_y
    gr.modify collision, "x", tx, "y", ty
    
    ! Snapper go up
    if gr_collision(collision,butUp) & ! SNAP_UP then
        if buzz then vibrate buzzGame[], -1
        gosub SnapGoUp
       
    ! Snapper go down
    elseif gr_collision(collision,butDown) & ! SNAP_DOWN then
        if buzz then vibrate buzzGame[], -1
        gosub SnapGoDown
       
    ! Snapper go right
    elseif gr_collision(collision,butRight) & ! SNAP_RIGHT then
        if buzz then vibrate buzzGame[], -1
        gosub SnapGoRight
        
     ! Snapper go left   
    elseif gr_collision(collision,butLeft) & ! SNAP_LEFT then
        if buzz then vibrate buzzGame[], -1
        gosub SnapGoLeft
    
    ! The menu
    elseif gr_collision(collision,butOptions) then
        if buzz then vibrate buzzGame[], -1
        gosub OptionsMenu
        
    endif
    
return
!!
!------------------------------------------------
! Control Snapper with gamepad or bluetooth keyboard
! Using the new INKEY$ keys$[] 
!------------------------------------------------
GamePad:
    inkey$ key$[]
    ! if ! key$[1] then return
    array.search key$[], "up", keyUpPressed
    if keyUpPressed & ! SNAP_UP then gosub SnapGoUp : return
    
    array.search key$[] ,"down", keyDownPressed
    if keyDownPressed & ! SNAP_DOWN then  gosub SnapGoDown : return
    
    array.search key$[] ,"left", keyLeftPressed
    if keyLeftPressed & ! SNAP_LEFT then  gosub SnapGoLeft : return
    
    array.search key$[] ,"right", keyRightPressed
    if keyRightPressed & ! SNAP_RIGHT then  gosub SnapGoRight : return
   
return
!!
!------------------------------------------------
! this with using  inkey$ key$
! Control Snapper with gamepad or bluetooth keyboard
!------------------------------------------------
GamePad:
    inkey$ key$
    if key$ = "@" then return
    
    if key$ = "up" & ! SNAP_UP then
        gosub SnapGoUp
       
    elseif key$ = "down" & ! SNAP_DOWN then 
        gosub SnapGoDown
        
    elseif key$ = "right" & ! SNAP_RIGHT then 
        gosub SnapGoRight
       
    elseif key$ = "left" & ! SNAP_LEFT then
        gosub SnapGoLeft
       
    endif
    
return

!------------------------------------------------
! Bonus fruits may appear for a short time
!------------------------------------------------
FruitMaybe:
    if pillsEaten < 20 then return
    if floor(rnd() * 500) = 1 then
        if ! fruitOn then fruitOn = TRUE : gr.show bonusFruit[floor(rnd() * 4) + 1]
    endif
    
    if floor(rnd() * 500) = 1 then fruitOn = FALSE : gr.hide snapFruit
    
 return
   
!------------------------------------------------
! Prepare Snapper to run left
!------------------------------------------------
SnapGoLeft:
    if SNAP_UP then   if MazeCollision(mazeBars[], snapperRight[], snapX, snapY + 30, testCollision) then return
    if SNAP_DOWN then if MazeCollision(mazeBars[], snapperRight[], snapX - 20, snapY + 15, testCollision) then return
    SNAP_UP = FALSE : SNAP_DOWN = FALSE : SNAP_RIGHT = FALSE : SNAP_LEFT = TRUE
    gr.hide snapRight : gr.hide snapUp : gr.hide snapDown
return

!------------------------------------------------
! Prepare Snapper to run right
!------------------------------------------------
SnapGoRight:
    if SNAP_UP then   if MazeCollision(mazeBars[], snapperRight[], snapX + 35, snapY + 30, testCollision) then return
    if SNAP_DOWN then if MazeCollision(mazeBars[], snapperRight[], snapX + 40, snapY + 15, testCollision) then return
    SNAP_UP = FALSE : SNAP_DOWN = FALSE : SNAP_RIGHT = TRUE : SNAP_LEFT = FALSE
    gr.hide snapLeft : gr.hide snapUp : gr.hide snapDown
return

!------------------------------------------------
! Prepare Snapper to run up
!------------------------------------------------
SnapGoUp:
    if SNAP_LEFT   then if MazeCollision(mazeBars[], snapperUp[], snapX + 25, snapY, testCollision) then return
    if SNAP_RIGHT  then if MazeCollision(mazeBars[], snapperUp[], snapX + 15, snapY, testCollision) then return
    SNAP_UP = TRUE : SNAP_DOWN = FALSE : SNAP_RIGHT = FALSE : SNAP_LEFT = FALSE
    gr.hide snapLeft : gr.hide snapRight : gr.hide snapDown
return

!------------------------------------------------
! Prepare Snapper to run down
!------------------------------------------------
SnapGoDown:
    if SNAP_LEFT then  if MazeCollision(mazeBars[], snapperDown[], snapX + 25, snapY + 45, testCollision) then return
    if SNAP_RIGHT then if MazeCollision(mazeBars[], snapperDown[], snapX + 15, snapY + 45, testCollision) then return
    SNAP_UP = FALSE : SNAP_DOWN = TRUE : SNAP_RIGHT = FALSE : SNAP_LEFT = FALSE
    gr.hide snapLeft : gr.hide snapRight : gr.hide snapUp
return


!------------------------------------------------
! Move Snapper left
!------------------------------------------------
SnapLeft:
    if MazeCollision(mazeBars[], snapperLeft[], snapX - 5, snapY + 20, bump) then OLD_SNAP = 1 : SNAP_LEFT = FALSE : return
    result = PillEatenLR(pillPtr[],snapperLeft[], redBalls[], &snapY, &pillsEaten, bonusFruit[], &fruitOn, &ghostReverse)
    score += result : gosub SnapSound
    
    if pillsEaten = 212 then levelOver = TRUE : return
    gr.modify txtScore, "text", "Score: " + int$(score)
    snapX -= snapStep_X : if snapX < - 30 then snapX = 880
    gr.modify snapperLeft[snapAnimation], "x", snapX, "y", snapY
   
    gr.hide snapLeft
    gr.show snapperLeft[snapAnimation]
    snapAnimation ++ : if snapAnimation = 4 then snapAnimation = 1
   
return

!------------------------------------------------
! Move Snapper right
!------------------------------------------------
SnapRight:
    if MazeCollision(mazeBars[], snapperRight[], snapX + 40, snapY + 20, bump) then OLD_SNAP = 2 : SNAP_RIGHT = FALSE : return
    result = PillEatenLR(pillPtr[],snapperRight[], redBalls[],&snapY, &pillsEaten, bonusFruit[], &fruitOn, &ghostReverse)
    score += result : gosub SnapSound
    
    if pillsEaten = 212 then levelOver = TRUE : return
    gr.modify txtScore, "text", "Score: " + int$(score)
    snapX += snapStep_X : if snapX > 900 then snapX = - 30
    gr.modify snapperRight[snapAnimation], "x", snapX, "y", snapY
    
    gr.hide snapRight
    gr.show snapperRight[snapAnimation]
    snapAnimation ++ : if snapAnimation = 4 then snapAnimation = 1
return

!------------------------------------------------
! Move Snapper up
!------------------------------------------------
SnapUp:
    if MazeCollision(mazeBars[], snapperUp[], snapX + 18, snapY - 2, bump) then OLD_SNAP = 3 : SNAP_UP = FALSE : return
    result = PillEatenUD(pillPtr[],snapperUp[], redBalls[], &snapX, &pillsEaten, &ghostReverse)
    score += result : gosub SnapSound
    
    if pillsEaten = 212 then levelOver = TRUE : return
    gr.modify txtScore, "text", "Score: " + int$(score)
    snapY -= snapStep_Y
    gr.modify snapperUp[snapAnimation], "x", snapX, "y", snapY
    
    gr.hide snapUp
    gr.show snapperUp[snapAnimation]
    snapAnimation ++ : if snapAnimation = 4 then snapAnimation = 1
return

!------------------------------------------------
! Move Snapper Down
!------------------------------------------------
SnapDown:
    if MazeCollision(mazeBars[], snapperDown[], snapX + 15, snapY + 40, bump) then OLD_SNAP = 4 : SNAP_DOWN = FALSE : return
    result = PillEatenUD(pillPtr[],snapperDown[], redBalls[], &snapX, &pillsEaten, &ghostReverse) 
    score += result : gosub SnapSound
    
    if pillsEaten = 212 then levelOver = TRUE : return
    gr.modify txtScore, "text", "Score: " + int$(score)
    snapY += snapStep_Y
    gr.modify snapperDown[snapAnimation], "x", snapX, "y", snapY
   
    gr.hide snapDown
    gr.show snapperDown[snapAnimation]
    snapAnimation ++ : if snapAnimation = 4 then snapAnimation = 1
return

!------------------------------------------------
! If sound is TRUE then play Snapper's sounds
!------------------------------------------------
SnapSound:
    if ! sound then return
    if result = 10 then 
        call PlaySound(snapChomp)
    elseif result = 50 then
        gosub StartIntermission
    elseif result = 100 then
        PlaySound(snapEatfruit)
    endif
return


!------------------------------------------------
! Snappers as stoped at a maze wall; keep hir animated until she moves on
!------------------------------------------------
AnimateSnap:
    for x = 1 to 3
        gr.modify snapperLeft[x], "x", snapX, "y", snapY
        gr.modify snapperRight[x], "x", snapX, "y", snapY
        gr.modify snapperUp[x], "x", snapX, "y", snapY
        gr.modify snapperDown[x], "x", snapX, "y", snapY
    next
    
    gr.hide snapUp : gr.hide snapDown : gr.hide snapLeft : gr.hide snapRight
    if OLD_SNAP = 1 then gr.show snapperLeft[snapAnimation]
   
    if OLD_SNAP = 2 then gr.show snapperRight[snapAnimation]
    
    if OLD_SNAP = 3 then gr.show snapperUp[snapAnimation]
    
    if OLD_SNAP = 4 then gr.show snapperDown[snapAnimation]
    
    snapAnimation ++ : if snapAnimation = 4 then snapAnimation = 1
return

!------------------------------------------------
! Snapper in collision with a ghost 
! Snapper floats of top of screen
!------------------------------------------------
SnapperDead:
    if sound then call PlaySound(snapDeath)
    OLD_SNAP = 3
    do
        gosub AnimateSnap
        snapY -= snapStep_Y
        gr.render
    until snapY < - 60
    OLD_SNAP = FALSE
    snapDead = TRUE
return

!------------------------------------------------
! ghostReverse is TRUE. Snapper as killed a ghost
!------------------------------------------------
DeadGhost:
    if sound then call PlaySound(snapEatghost)
    score += 150 
    gr.modify txtScore, "text", "Score: " + int$(score)

    deadX = ghostX[ghostFlag] : ghostX[ghostFlag] = ghostStartX[ghostFlag]
    deadY = ghostY[ghostFlag] : ghostY[ghostFlag] = ghostStartY[ghostFlag]
        
    ghostNum[ghostFlag] = 1 
    GHOST_UP[ghostFlag] = TRUE
    
    !gr.modify txtSnapY, "text", int$(ghostFlag)
    GET_EYE_PATH = TRUE
    
    list.clear eyePosX : list.clear eyePosY
    do
        gosub MoveTheGhosts
        list.add eyePosX, ghostX[ghostFlag]
        list.add eyePosY, ghostY[ghostFlag]
    
    until deadX = ghostX[ghostFlag] & deadY = ghostY[ghostFlag]
    
    list.size eyePosX, backToCave[ghostFlag]

    call LoadEyesArrayX(eyesPathX[],ghostFlag,eyePosX)
    call LoadEyesArrayY(eyesPathY[],ghostFlag,eyePosY)
    
    GET_EYE_PATH = FALSE
    
    gr.modify ghost[ghostFlag], "bitmap", bmp_prt_ghost7 % eyes
    eyesRunning[ghostFlag] = TRUE
  
return

!------------------------------------------------
! Move the eyes back to the cave
!------------------------------------------------
MoveEyes:
    if backToCave[ghostFlag] < 1 then 
        eyesRunning[ghostFlag] = FALSE 
        gr.modify ghost[ghostFlag], "bitmap", ghostBmpPrt[ghostFlag]
        ghostNum[ghostFlag] = IN_CAVE % out of range
        if ! ghostTime then ghostNum[ghostFlag] = 1
        GHOST_UP[ghostFlag] = TRUE
        ghostX[ghostFlag] = ghostStartX[ghostFlag] : ghostY[ghostFlag] = ghostStartY[ghostFlag] 
        gr.modify ghost[ghostFlag], "x", ghostX[ghostFlag], "y", ghostY[ghostFlag]
    else
        gr.modify ghost[ghostFlag], "x", eyesPathX[ghostFlag,backToCave[ghostFlag]], "y", eyesPathY[ghostFlag,backToCave[ghostFlag]]
        backToCave[ghostFlag] -= 1
    endif
return

!------------------------------------------------
! Find out which way Snapper is going jump to the subroutine 
!------------------------------------------------
MoveSnapper:
    if SNAP_LEFT then
        gosub SnapLeft
    elseif SNAP_RIGHT then 
        gosub snapRight
    elseif SNAP_UP then
        gosub SnapUp
    elseif SNAP_DOWN then
        gosub SnapDown
    endif
return
 
!------------------------------------------------
! Move the ghosts, blinky, pinky, clyde and inky
!------------------------------------------------
MoveGhosts:
    ghostStart = (time() - now) / 1000
    ghostFlag = 0
    if ghostStart > 3 then ghostFlag = 1 : gosub MoveTheGhosts
    if ghostStart > 6 then ghostFlag = 3 : gosub MoveTheGhosts
    if ghostStart > 9 then ghostFlag = 4 : gosub MoveTheGhosts
    if ghostStart > 12 then ghostFlag = 2 : gosub MoveTheGhosts
return

!------------------------------------------------
! Set the ghosts to go Up, Down, Left or Right
!------------------------------------------------
MoveTheGhosts:
    if eyesRunning[ghostFlag] then gosub MoveEyes : return % Eyes running back to cave
    if ghostNum[ghostFlag] = IN_CAVE then return % ghost in cave
    
    ! Ghost go up
    if GHOST_UP[ghostFlag] = TRUE then
        ghostY[ghostFlag] -= ghostStepY[ghostFlag]
        gr.modify ghost[ghostFlag], "x", ghostX[ghostFlag], "y", ghostY[ghostFlag]
        gr.modify ghostBox, "x",ghostX[ghostFlag] + 15, "y", ghostY[ghostFlag] + 17
        gosub GhostAdvance 
       
    ! Ghost go down
    elseif  GHOST_DOWN[ghostFlag] = TRUE then
        ghostY[ghostFlag] += ghostStepY[ghostFlag]
        gr.modify ghost[ghostFlag], "x", ghostX[ghostFlag], "y", ghostY[ghostFlag]
        gr.modify ghostBox, "x",ghostX[ghostFlag] + 15, "y", ghostY[ghostFlag] + 17
        gosub GhostAdvance
       
    ! Ghost go right
    elseif  GHOST_RIGHT[ghostFlag] = TRUE then
        ghostX[ghostFlag] += ghostStepX[ghostFlag]
        gr.modify ghost[ghostFlag], "x", ghostX[ghostFlag], "y", ghostY[ghostFlag]
        gr.modify ghostBox, "x",ghostX[ghostFlag] + 15, "y", ghostY[ghostFlag] + 17
        gosub GhostAdvance  
       
     ! Ghost go left   
    elseif GHOST_LEFT[ghostFlag] = TRUE then
        ghostX[ghostFlag] -= ghostStepX[ghostFlag]
        gr.modify ghost[ghostFlag], "x", ghostX[ghostFlag], "y", ghostY[ghostFlag]
        gr.modify ghostBox, "x",ghostX[ghostFlag] + 15, "y", ghostY[ghostFlag] + 17
        gosub GhostAdvance
       
    endif
        
        if GET_EYE_PATH then return
        
        if SnapToGhost(snapX, snapY, 10, ghostX[ghostFlag], ghostY[ghostFlag], 10) then
            if ! ghostReverse then gosub SnapperDead else gosub DeadGhost
        endif
             
return
 
!------------------------------------------------
! Ghost go Forward trying to get snapper
!------------------------------------------------
GhostAdvance:
    flag = FALSE
    
    if ghostNum[ghostFlag] = 1 & gr_collision(mazeBars[mazeNum[ghostFlag,1]], ghostBox) then
        gosub GhostWay : flag = TRUE
    
     elseif ghostNum[ghostFlag] = 2 & gr_collision(mazeBars[mazeNum[ghostFlag,2]], ghostBox) then
        gosub GhostWay : flag = TRUE
    
    elseif ghostNum[ghostFlag] = 3 & gr_collision(mazeBars[mazeNum[ghostFlag,3]], ghostBox) then
        gosub GhostWay : flag = TRUE
    
    elseif ghostNum[ghostFlag] = 4 & gr_collision(mazeBars[mazeNum[ghostFlag,4]], ghostBox) then
        gosub GhostWay : flag = TRUE
    
    elseif ghostNum[ghostFlag] = 5 & gr_collision(mazeBars[mazeNum[ghostFlag,5]], ghostBox) then
        gosub GhostWay : flag = TRUE
    
    elseif ghostNum[ghostFlag] = 6 & gr_collision(mazeBars[mazeNum[ghostFlag,6]], ghostBox) then
       gosub GhostWay : flag = TRUE
   
    elseif ghostNum[ghostFlag] = 7 & gr_collision(mazeBars[mazeNum[ghostFlag,7]], ghostBox) then
       gosub GhostWay : flag = TRUE
   
    elseif ghostNum[ghostFlag] = 8 & gr_collision(mazeBars[mazeNum[ghostFlag,8]], ghostBox) then
        gosub GhostWay : flag = TRUE
   
     elseif ghostNum[ghostFlag] = 9 & gr_collision(mazeBars[mazeNum[ghostFlag,9]], ghostBox) then
        gosub GhostWay : flag = TRUE
    
    elseif ghostNum[ghostFlag] = 10 & gr_collision(mazeBars[mazeNum[ghostFlag,10]], ghostBox) then
       gosub GhostWay : flag = TRUE
   
    elseif ghostNum[ghostFlag] = 11 & gr_collision(mazeBars[mazeNum[ghostFlag,11]], ghostBox) then
        gosub GhostWay : flag = TRUE
    
    elseif ghostNum[ghostFlag] = 12 & gr_collision(mazeBars[mazeNum[ghostFlag,12]], ghostBox) then
        gosub GhostWay : flag = TRUE
    
    elseif ghostNum[ghostFlag] = 13 & gr_collision(mazeBars[mazeNum[ghostFlag,13]], ghostBox) then
        gosub GhostWay : flag = TRUE
    
    elseif ghostNum[ghostFlag] = 14 & gr_collision(mazeBars[mazeNum[ghostFlag,14]], ghostBox) then
        gosub GhostWay : flag = TRUE
    
    elseif ghostNum[ghostFlag] = 15 & gr_collision(mazeBars[mazeNum[ghostFlag,15]], ghostBox) then
        gosub GhostWay : flag = TRUE
   
    elseif ghostNum[ghostFlag] = 16 & gr_collision(mazeBars[mazeNum[ghostFlag,16]], ghostBox) then
        gosub GhostWay : flag = TRUE
    
    elseif ghostNum[ghostFlag] = 17 & gr_collision(mazeBars[mazeNum[ghostFlag,17]], ghostBox) then
       gosub GhostWay : flag = TRUE
    
    elseif ghostNum[ghostFlag] = 18 & gr_collision(mazeBars[mazeNum[ghostFlag,18]], ghostBox) then
        gosub GhostWay : flag = TRUE
    
    elseif ghostNum[ghostFlag] = 19 & gr_collision(mazeBars[mazeNum[ghostFlag,19]], ghostBox) then
        gosub GhostWay : flag = TRUE
    
    elseif ghostNum[ghostFlag] = 20 & gr_collision(mazeBars[mazeNum[ghostFlag,20]], ghostBox) then
        gosub GhostWay : flag = TRUE
       
    elseif ghostNum[ghostFlag] = 21 & gr_collision(mazeBars[mazeNum[ghostFlag,21]], ghostBox) then
        gosub GhostWay : flag = TRUE
       
    elseif ghostNum[ghostFlag] = 22 & gr_collision(mazeBars[mazeNum[ghostFlag,22]], ghostBox) then
        gosub GhostWay : flag = TRUE
       
    elseif ghostNum[ghostFlag] = 23 & gr_collision(mazeBars[mazeNum[ghostFlag,23]], ghostBox) then
        gosub GhostWay : flag = TRUE
       
    elseif ghostNum[ghostFlag] = 24 & gr_collision(mazeBars[mazeNum[ghostFlag,24]], ghostBox) then
        gosub GhostWay : flag = TRUE
       
    elseif ghostNum[ghostFlag] = 25 & gr_collision(mazeBars[mazeNum[ghostFlag,25]], ghostBox) then
        gosub GhostWay : flag = TRUE
       
    elseif ghostNum[ghostFlag] = 26 & gr_collision(mazeBars[mazeNum[ghostFlag,26]], ghostBox) then
        gosub GhostWay : flag = TRUE
       
    elseif ghostNum[ghostFlag] = 27 & gr_collision(mazeBars[mazeNum[ghostFlag,27]], ghostBox) then
        gosub GhostWay : flag = TRUE
       
    elseif ghostNum[ghostFlag] = 28 & gr_collision(mazeBars[mazeNum[ghostFlag,28]], ghostBox) then
        gosub GhostWay : flag = TRUE
       
    elseif ghostNum[ghostFlag] = 29 & gr_collision(mazeBars[mazeNum[ghostFlag,29]], ghostBox) then
        gosub GhostWay : flag = TRUE
       
    endif
    
        if flag then ghostNum[ghostFlag] ++ 
        if ghostFlag < 3 & ghostNum[ghostFlag] > 20 then ghostNum[ghostFlag] = 3
        if ghostFlag > 2 & ghostNum[ghostFlag] > 29  then ghostNum[ghostFlag] = 6
    
return

!------------------------------------------------
! Set all ghost moves to false then set the way to go to true
!------------------------------------------------
GhostWay:
    GHOST_UP[ghostFlag] = FALSE    : GHOST_DOWN[ghostFlag] = FALSE 
    GHOST_RIGHT[ghostFlag] = FALSE : GHOST_LEFT[ghostFlag] = FALSE
    if direction[ghostFlag,ghostNum[ghostFlag]] = G_UP then
        GHOST_UP[ghostFlag] = TRUE
    elseif direction[ghostFlag,ghostNum[ghostFlag]] = G_DOWN then
        GHOST_DOWN[ghostFlag] = TRUE
    elseif direction[ghostFlag,ghostNum[ghostFlag]] = G_LEFT then
        GHOST_LEFT[ghostFlag] = TRUE
    elseif direction[ghostFlag,ghostNum[ghostFlag]] = G_RIGHT then
        GHOST_RIGHT[ghostFlag] = TRUE
    endif
   
return

!------------------------------------------------
! Turn all ghost blue; they can now be chased and eaten by Snapper
!------------------------------------------------
TurnGhostBlue:
    if allBlue then return
    g_now = time()
    allBlue = TRUE
    oldGhostReveres = 0
    gosub AllGhostsBlue
    ghostTime = TRUE
return

!------------------------------------------------
! Turn all ghost blue
!------------------------------------------------
AllGhostsBlue:
    for b = 1 to 4
        if ! eyesRunning[b] then
            gr.modify ghost[b], "bitmap", bmp_prt_ghost5
        endif
    next
return

!------------------------------------------------
! Turn all ghost back to how they where, they now chass and kill Snapper
!------------------------------------------------
TurnGhostBack:
    ghostBlue = (time() - g_now) / 1000
    if ghostBlue > ghostReverse then
        allBlue = FALSE
        ghostReverse = FALSE
        ghostTime = FALSE
        for x = 1 to 4
            if ghostNum[x] = IN_CAVE then ghostNum[x] = 1
        next
        for b = 1 to 4
            if ! eyesRunning[b] then
                gr.modify ghost[b], "bitmap", ghostBmpPrt[b]
            endif
           
        next
        
        if ghostNum[ghostFlag] = IN_CAVE then ghostNum[ghostFlag] = 1
    endif
    if ghostReverse > oldGhostReveres then gosub AllGhostsBlue
    oldGhostReveres = ghostReverse
    if ! ghostReverse & sound then gosub StopIntermission
    if ! ghostReverse then return
    if ghostBlue < ghostReverse - 5 then return else gosub flashGhosts : return
    
return

!------------------------------------------------
! Flash the ghosts blue and white for the last 10 secons
!------------------------------------------------
flashGhosts:
    for y = ghostReverse - 5 to ghostReverse step 2
        for x = 1 to 4
            if round(ghostBlue,, "HE") = y then 
                if ! eyesRunning[x] then 
                    if ghostNum[x] <> IN_CAVE then gr.modify ghost[x], "bitmap", bmp_prt_ghost6
                endif
            endif
            if round(ghostBlue,, "HE") = y + 1 then 
                if ! eyesRunning[x] then 
                    if ghostNum[x] <> IN_CAVE then gr.modify ghost[x], "bitmap", bmp_prt_ghost5
                endif
           endif
        next
    next
    
return

!------------------------------------------------
! Options, sound, music, buzz, reset high score,
! about, help, scoring, exit
!------------------------------------------------
OptionsMenu:
    if sound then call PlaySound(snapButtonClick)
    allDone = FALSE
    for on = 1 to 13
       gr.show menu[on]
       if on = 3 & ! music then gr.hide menu[3]
       if on = 4 & ! sound then gr.hide menu[4]
       if on = 5 & ! buzz  then gr.hide menu[5]
    next
    gr.render
    do
        gr.touch t, tx, ty 
        if t then 
            do : gr.touch t, tx, ty  : until ! t
            tx /= scale_x : ty /= scale_y
            gr.modify collision, "x", tx, "y", ty
        endif
        
        if gr_collision(collision,menu[6]) then 
            gosub ModMusic
            
        elseif gr_collision(collision,menu[7]) then
            gosub ModSound
            
        elseif gr_collision(collision,menu[8]) then
            gosub ModBuzz
        
        elseif gr_collision(collision,menu[9]) then
            gosub ResetHighScore
            
        elseif gr_collision(collision,menu[10]) then
            gosub AboutSnapper
            
        elseif gr_collision(collision,menu[11]) then
            gosub HelpSnapper
            
        elseif gr_collision(collision,menu[12]) then
            gosub ExitSnapper
            
        elseif gr_collision(collision,menu[13]) then 
            if sound then call PlaySound(snapButtonClick)
            allDone = TRUE
            
        endif
        
        gr.modify collision, "x", - 1, "y", - 1
    until allDone
    for off = 1 to 13
        gr.hide menu[off]
    next
    gr.render
return

!------------------------------------------------
! Load Sound files and setup buzz
!------------------------------------------------
LoadSounds:
    soundpool.open 9
    soundpool.load snapButtonClick,path$ + "snapButtonClick.wav"
    if ! snapButtonClick then call SoundLoadError("snapButtonClick")

    soundpool.load snapNewHigh, path$ + "snapNewHigh.wav"
    if ! snapNewHigh then call SoundLoadError("snapNewHigh")
    
    soundpool.load snapNotHigh, path$ + "snapNotHigh.wav"
    if ! snapNotHigh then call SoundLoadError("snapNotHigh")
    
    !Snapper sounds
    soundpool.load snapBeginning, path$ + "snapBeginning.wav"
    if ! snapBeginning then call SoundLoadError("snapBeginning")
    
    soundpool.load snapChomp, path$ + "snapChomp.wav"
    if ! snapChomp then call SoundLoadError("snapChomp")
    
    soundpool.load snapDeath, path$ + "snapDeath.wav"
    if ! snapDeath then call SoundLoadError("snapDeath") 
    
    soundpool.load snapEatfruit, path$ + "snapEatfruit.wav"
    if ! snapEatFruit then call SoundLoadError("snapEatFruit")
    
    soundpool.load snapEatghost, path$ + "snapEatghost.wav"
    if ! snapEatghost then call SoundLoadError("snapEatghost")
   
    soundpool.load snapExtrasnap, path$ + "snapExtrasnap.wav"
    if ! snapExtrasnap then call SoundLoadError("snapExtrasnap")
    
    ! Audio loads CallMe and snapIntermission
    audio.load CallMe, Path$ + "CallMe.mp3" 
    if ! CallMe then call SoundLoadError("CallMe")
    
    audio.load snapIntermission, Path$ + "snapIntermission.mp3" 
    if ! snapIntermission then call SoundLoadError("snapIntermission")
    
    array.load buzzGame[], 1, 100
    
    gosub StartMusic
return

!------------------------------------------------
! Turn music on and off
!------------------------------------------------
ModMusic:
  if buzz then vibrate buzzGame[], -1
  if sound then call PlaySound(snapButtonClick)
  if music then
    music = FALSE
    gr.hide menu[3]
    audio.stop 
  else
    music = TRUE
    gr.show menu[3]
    audio.play CallMe
    audio.loop 
  endif
  gr.render
return

!------------------------------------------------
! Turn sound on and off
!------------------------------------------------
ModSound:
  if buzz then vibrate buzzGame[], -1
  if sound then call PlaySound(snapButtonClick)
  if sound then
    sound = FALSE
   gr.hide menu[4]
  else
    sound = TRUE
    gr.show menu[4]
  endif
  gr.render 
return

!------------------------------------------------
! Turn buzz on and off
!------------------------------------------------
ModBuzz:
  if buzz then vibrate buzzGame[], -1
  if sound then call PlaySound(snapButtonClick)
  if buzz then
    buzz = FALSE
    gr.hide menu[5]
  else
    buzz = TRUE
   gr.show menu[5]
  endif
  gr.render
return

!------------------------------------------------
! Reset the high score to zero 
!------------------------------------------------
ResetHighScore:
  if buzz then vibrate buzzGame[], -1
  if sound then call PlaySound(snapButtonClick)
  do : dialog.message "Reset high Score to Zero?",,yn,"Yes","No" : until yn > 0
  if buzz then vibrate buzzGame[], -1
  if sound then call PlaySound(soundClick)
  if yn = 1 then 
        highScore = 0
        gr.modify txtHigh, "text", "High Score: " + int$(highScore)
  endif
  gr.render
return

!------------------------------------------------
! About Snapper
!------------------------------------------------
AboutSnapper:
    if buzz then vibrate buzzGame[], -1
    if sound then call PlaySound(snapButtonClick)
    do
       Dialog.Message " About:\n\n",~
			" 	Snapper for Android\n\n" + ~
			" 	With: RFO BASIC!\n\n" + ~
			" 	September/October 2016\n\n" +~
			" 	Version: 1.00\n\n" + ~ 
			" 	Author: Roy Shepherd\n\n", ~
			OK, "OK"
        if buzz then vibrate buzzGame[], -1
        if sound then call PlaySound(snapButtonClick)
    until OK > 0
    
return

!------------------------------------------------
! How to play Snapper
!------------------------------------------------
HelpSnapper:
    if buzz then vibrate buzzGame[], -1
    if sound then call PlaySound(snapButtonClick)
    for x = 1 to 20 
        gr.show help[x]
    next
    gr.render
    do
    gr.touch t, tx, ty
    if t then 
        tx /= scale_x : ty /= scale_y
        gr.modify collision, "x", tx, "y", ty
    endif
    until gr_collision(help[19], collision)
    if buzz then vibrate buzzGame[], -1
    if sound then call PlaySound(snapButtonClick)
    for x = 1 to 20 
        gr.hide help[x]
    next
    gr.render
return

!------------------------------------------------
! Leaving Snapper
!------------------------------------------------
ExitSnapper:
    if buzz then vibrate buzzGame[], -1
    if sound then call PlaySound(snapButtonClick)
    do
        dialog.message "Exit Snapper",, yn, "Yes", "No"
        if buzz then vibrate buzzGame[], -1
        if sound then call PlaySound(snapButtonClick)
    until yn > 0 
    if yn = 1 then 
        if score > highScore then
            highScore = score 
            popup"New High Score:" + int$(highScore)
            gr.modify txtHigh, "text", "High Score:" + int$(highScore) 
            gr.render
        endif 
        gosub SaveData : pause 2000 
        exit
    endif
return

!-------------------------------------------------
! Start background music at start of game, if music = TRUE
!-------------------------------------------------
StartMusic:
if music then
    audio.play CallMe 
    audio.loop 
  endif 
return

!-------------------------------------------------
! Start Snapper's Intermission sound, this when the ghost are blue
!-------------------------------------------------
StartIntermission:
    audio.stop
    audio.play snapIntermission 
    audio.loop 
return

!-------------------------------------------------
! Stop Snapper's Intermission sound,
! this is when the ghost turn back to get Snapper mode
!-------------------------------------------------
StopIntermission:
    audio.stop
    gosub StartMusic
return

!------------------------------------------------
! Save the High Score, Music, sound, and Buzz
!------------------------------------------------
SaveData:
    !path$="Snapper/data/"
    file.exists pathPresent,path$+"GameData.txt"
    if ! pathPresent then file.mkdir path$

    text.open w,hs,Path$+"GameData.txt"
    
    text.writeln hs,int$(highScore)
    text.writeln hs,int$(music)
    text.writeln hs,int$(sound)
    text.writeln hs,int$(buzz)
    text.close hs
return

!------------------------------------------------
! Load the High Score, Music, sound, and Buzz
!------------------------------------------------
LoadData:
    !path$="Snapper/data/"
    file.exists pathPresent,path$+"GameData.txt"
    if pathPresent then
        text.open r,hs,path$+"GameData.txt"
        
        text.readln hs,highScore$
        text.readln hs,music$
        text.readln hs,sound$
        text.readln hs,buzz$
        text.close hs
        highScore = val(highScore$)
        music = val(music$)
        sound = val(sound$)
        buzz = val(buzz$)
    endif
return

!-------------------------------------------------
! All the functions for the game start here
!-------------------------------------------------
Functions:

!-------------------------------------------------
! Get the eyes X path from list and load array LoadEyesArrayX
!-------------------------------------------------
fn.def LoadEyesArrayX(eyes[],field,listPtr)
list.size listPtr, size

for x = 1 to size
    list.get listPtr, x, eyeNum
    eyes[field,x] = eyeNum
next

fn.end

!-------------------------------------------------
! Get the eyes Y path from list and load array LoadEyesArrayY
!-------------------------------------------------
fn.def LoadEyesArrayY(eyes[],field,listPtr)
list.size listPtr, size

for y = 1 to size
    list.get listPtr, y, eyeNum
    eyes[field,y] = eyeNum
next

fn.end

!-------------------------------------------------
! For Snapper and the ghosts
! Set the y for the middle of the lains in the maze
! Set the row for Snappers calculations, not needed for the ghosts
!-------------------------------------------------
fn.def SetY(snapY, row)
    offSet = 17
    if snapY > 9   - offSet & snapY < 9   + offSet then snapY = 9   : row = 1   % 1
    if snapY > 76  - offSet & snapY < 76  + offSet then snapY = 76  : row = 51  % 3
    if snapY > 145 - offSet & snapY < 145 + offSet then snapY = 145 : row = 101 % 5
    if snapY > 212 - offSet & snapY < 212 + offSet then snapY = 212 : row = 151 % 7
    if snapY > 314 - offSet & snapY < 314 + offSet then snapY = 314 : row = 226 % 10
    if snapY > 415 - offSet & snapY < 415 + offSet then snapY = 415 : row = 301 % 13
    if snapY > 483 - offSet & snapY < 483 + offSet then snapY = 483 : row = 351 % 15
    if snapY > 550 - offSet & snapY < 550 + offSet then snapY = 550 : row = 401 % 17
    if snapY > 618 - offSet & snapY < 618 + offSet then snapY = 618 : row = 450 % 19  

fn.rtn snapY
fn.end 

!-------------------------------------------------
! For Snapper and the ghosts
! Set the x for the middle of the lains in the maze
! Set the col for Snappers calculations, not needed for the ghosts
!-------------------------------------------------
fn.def SetX(snapX, col)
    offSet = 17
    if snapX > 15  - offSet & snapX < 15  + offSet then snapX = 15  : col = 1 % 1
    if snapX > 81  - offSet & snapX < 81  + offSet then snapX = 81  : col = 3 % 3
    if snapX > 150 - offSet & snapX < 150 + offSet then snapX = 150 : col = 5 % 5
    if snapX > 217 - offSet & snapX < 217 + offSet then snapX = 217 : col = 7 % 7
    
    if snapX > 285 - offSet & snapX < 285 + offSet then snapX = 285 : col = 10 % 10
    if snapX > 385 - offSet & snapX < 385 + offSet then snapX = 385 : col = 12 % 12
    if snapX > 455 - offSet & snapX < 455 + offSet then snapX = 455 : col = 14 % 14
    if snapX > 555 - offSet & snapX < 555 + offSet then snapX = 555 : col = 17 % 17
    
    if snapX > 625 - offSet & snapX < 625 + offSet then snapX = 625 : col = 19 % 19
    if snapX > 692 - offSet & snapX < 692 + offSet then snapX = 692 : col = 21 % 21
    if snapX > 760 - offSet & snapX < 760 + offSet then snapX = 760 : col = 23 % 23
    if snapX > 825 - offSet & snapX < 825 + offSet then snapX = 825 : col = 25 % 25

fn.rtn snapX
fn.end

!-------------------------------------------------
! Snapper eats the pills, gining point.
! Snapper eats red ball, gining point and turns the ghosts blue
! When the ghost are blue Snapper can eat them, gining point
!-------------------------------------------------
fn.def PillEatenLR(pillPtr[], snapper[], redBalls[], snapY, pillsEaten, bonusFruit[], fruitOn, ghostReverse)
    flag = 0 : row = 0 
    snapY = SetY(snapY, &row)
    
   if row > 0 then
        for x = row to row + 25
            if x = 376 & gr_collision(redBalls[4], snapper[1]) then gr.hide redBalls[4] : flag = 50 : pillsEaten ++
            if x = 351 & gr_collision(redBalls[3], snapper[1]) then gr.hide redBalls[3] : flag = 50 : pillsEaten ++
            if gr_collision(pillPtr[x], snapper[1]) then 
                gr.hide pillPtr[x] : flag = 10 : pillsEaten ++ : f_n.break
            elseif gr_collision(pillPtr[x], snapper[2]) then
                gr.hide pillPtr[x] : flag = 10 : pillsEaten ++ : f_n.break
            elseif gr_collision(pillPtr[x], snapper[3]) then
                gr.hide pillPtr[x] : flag = 10 : pillsEaten ++ : f_n.break
            endif
            if row = 351 then 
                if gr_collision(bonusFruit[1], snapper[1]) then 
                    gr.hide bonusFruit[1] : flag = 100 : fruitOn = 0
                elseif gr_collision(bonusFruit[2], snapper[1]) then 
                    gr.hide bonusFruit[2] : flag = 100 : fruitOn = 0
                elseif gr_collision(bonusFruit[3], snapper[1]) then 
                    gr.hide bonusFruit[3] : flag = 100 : fruitOn = 0
                elseif gr_collision(bonusFruit[4], snapper[1]) then 
                    gr.hide bonusFruit[4] : flag = 100 : fruitOn = 0
                endif
            endif
        next
    endif
    if flag = 50 then ghostReverse += 20
fn.rtn flag
fn.end

!-------------------------------------------------
! Snapper eats the pills, gining point.
! Snapper eats red ball, gining point and turns the ghosts blue
! When the ghost are blue Snapper can eat them, gining point
!-------------------------------------------------
fn.def PillEatenUD(pillPtr[], snapper[], redBalls[], snapX, pillsEaten, ghostReverse)
    flag = 0 : col = 0 
    snapX = SetX(snapX, &col)
  
    if col > 0 then
        for x = col to 475 step 25
            if x = 400 & gr_collision(redBalls[4], snapper[1]) then gr.hide redBalls[4] : flag = 50 : pillsEaten ++ 
            if x = 351 & gr_collision(redBalls[3], snapper[1]) then gr.hide redBalls[3] : flag = 50 : pillsEaten ++
            if x = 75  & gr_collision(redBalls[2], snapper[1]) then gr.hide redBalls[2] : flag = 50 : pillsEaten ++ 
            if x = 51  & gr_collision(redBalls[1], snapper[1]) then gr.hide redBalls[1] : flag = 50 : pillsEaten ++
            if gr_collision(pillPtr[x], snapper[1]) then
                gr.hide pillPtr[x] : flag = 10 : pillsEaten ++ : f_n.break 
            elseif gr_collision(pillPtr[x], snapper[2]) then 
                gr.hide pillPtr[x] : flag = 10 : pillsEaten ++ : f_n.break 
            elseif gr_collision(pillPtr[x], snapper[3]) then
                gr.hide pillPtr[x] : flag = 10 : pillsEaten ++ : f_n.break 
            endif
        next
    endif
   if flag = 50 then ghostReverse += 20
fn.rtn flag
fn.end

!------------------------------------------------
! Test to see if Snapper is at a gap in the maze wall
! if so return FALSE (snapper can turn) else return TRUE
! Also test to see if snapper as bumped into a maze wall, if so return TRUE
!------------------------------------------------
fn.def MazeCollision(mazeBars[], snapper[], snapX, snapY, testCollision)
    flag = 0
    gr.modify testCollision, "x", snapX, "y", snapY
    for c = 1 to 45
        if gr_collision(mazeBars[c], testCollision) then  flag = 1 : f_n.break
    next
fn.rtn flag
fn.end

!------------------------------------------------
! Thanks to Gilles for this collision detection function
!------------------------------------------------
Fn.def SnapToGhost(PlanetX, PlanetY, PlanetR, shipX, ShipY, ShipR)
  Fn.rtn (((PlanetR + ShipR) * (PlanetR + ShipR)) > ((PlanetX - shipX) * (PlanetX - shipX) + (PlanetY - ShipY) * (PlanetY - ShipY)))
Fn.End

!------------------------------------------------
! if Play sound (ptr)
!------------------------------------------------
  fn.def PlaySound(ptr)
      soundpool.play s,ptr,0.99,0.99,1,0,1
  fn.end
  
!------------------------------------------------
! load Sound files not found
!------------------------------------------------
    fn.def SoundLoadError(e$)
        dialog.message "Sound file " + e$ + " not found", "Game will end",OK,"OK" 
        end
    fn.end
    
!------------------------------------------------
! Load image file not found
!------------------------------------------------
    fn.def ImageLoadError(e$)
        dialog.message "Image file " + e$ + " not found", "Game will end",OK,"OK" 
        end
    fn.end
 
return



