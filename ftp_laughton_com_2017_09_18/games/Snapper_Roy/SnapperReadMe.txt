Hi All

Anyone remember Pac-man and all the clones. Well one such clone was Snapper for the BBC Micro. Launch in 1982 the game ran very smoothly, making it a popular release of the time. As it was written in machine code, rather than BASIC. It was one of my favorite games.

Here is my take on the game for Android. 

When using the arrows on a Blue-tooth keyboard or the rocker switch on a Game-pad. The game uses inkey$ array$[] to control Snapper through the maze. This is not yet part of Basic. Hopefully it will be in the next release. For the full story see:
http://rfobasic.freeforums.org/inkey-t4534.html 

Snapper:
Guide Snapper through the maze eating
pills and fruit, that may appear for a 
short time, while trying to avoid the
ghosts from the cave. Eat the red faced 
men to turn the ghosts blue and make
them edible-once eaten their eyes return 
to the cave. Uneaten ghost soon start to
flash which means their regeneration is
imminent, so watch out!


When all the pills are eaten you tackle
a new screenful, but the ghost get
progressively faster. You have three
lives.

Point:
	Pills = 10
	Red Faced men = 50
	Fruits = 100
	Ghosts = 150
	New screen = 1000

Controls:
	The arrows on the screen.
	The arrows on a Blue-tooth keyboard.
	The rocker switch on a Game-pad.

Regards Roy








