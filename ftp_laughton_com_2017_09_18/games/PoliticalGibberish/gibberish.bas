REM Start of BASIC! Program
! political gibberish
! generator
!
! by mookiebearapps
!

! return word of type$
FN.DEF wd$(type$)
 GRABFILE f$,"userwords.txt"
f$+=":"
i=is_in(":"+type$,f$)
i=is_in("\n",f$,i)+1
e=is_in(":",f$,i)-1
w$=trim$(mid$(f$,i,e-i+1))
 SPLIT db$[],w$,CHR$(10)
 ARRAY.LENGTH l,db$[]
  LET x=random(l)
 FN.RTN trim$(db$[x])+" "
FN.END

FN.DEF isold(f$) 
 FILE.EXISTS o,f$ 
 FN.RTN o
FN.END

FN.DEF writeln(f$,msg$) 
 TEXT.OPEN w,h,f$
 TEXT.WRITELN h,msg$
 TEXT.CLOSE h 
FN.END

FN.DEF waitclick$()
 DO
  PAUSE 100
  HTML.GET.DATALINK data$
  IF BACKGROUND() 
cs=clock()
do
pause 100
until (clock()-cs)>5000|!background()
if background() then EXIT
endif
 UNTIL data$ <> ""
 ! popup data$,0,0,0
 IF IS_IN("BAK:",data$)=1
  EXIT
 ELSEIF IS_IN("DAT:",data$)=1
  data$=MID$(data$,5) %' User link
 ELSEIF IS_IN("LNK:file:///",data$)=1&IS_IN("?",data$) %' Submit link
  i=IS_IN("?",data$)
  data$="SUBMIT&"+MID$(data$,i+1)+"&"
 ENDIF
 FN.RTN data$
FN.END



FN.DEF htmledit$(p$,s$) 
 HTML.OPEN 0
 w$="<html>"
 w$+="<head >"
 w$+="<meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\"/>"
 w$+="<title >edit text</title>"
 w$+="</head>"
 w$+="<script type=\"text/javascript\">"
 w$+="function doDataLink(data) {"
 w$+="Android.dataLink(data);"
 w$+="}"
 w$+="</script>"
 w$+="<body bgcolor=\"black\" >"
 w$+="<div align=\"left\">"
 w$+="<h2 style=\"color:#999999\">###prompt</h2>"
 w$+=""
 w$+="<form id='main' method='get' action='FORM'>"
 w$+="<input type='submit' style=\"float:left;\" name='submit' value='Done'/>"
 w$+="<TEXTAREA NAME=\"id\"  ROWS=15 COLS=34>"
 w$+="###edit"
 w$+="</TEXTAREA>"
 w$+="</form>"
 w$+="</div>"
 w$+="</body>"
 w$+="</html>"

 w$ =REPLACE$(w$,"###prompt",p$)
 w$ = REPLACE$(w$,"###edit", TRIM$(s$))
 HTML.LOAD.STRING w$
 HTML.LOAD.URL "javascript:DODataLink(document.getElementById('id'))h
 r$=waitclick$()
 ! popup "please wait..."
 s$= DECODE$("URL","UTF-8",r$)
 s$=REPLACE$(s$,"SUBMIT&submit=Done&id=","")
 s$=LEFT$(s$,LEN(s$)-1)
html.close
 FN.RTN s$
FN.END 


FN.DEF SENDMAIL(s$)
s$=replace$(s$,chr$(10),"\n")
 FILE.EXISTS Isold, "email.txt"
 IF Isold
  TEXT.OPEN r, f,"email.txt"
  TEXT.READLN f, em$
  TEXT.CLOSE f
 ELSE
  INPUT "enter default email to send data to",em$,em$,emcancel
  IF emcancel THEN FN.RTN 0
 writeln("email.txt", em$)

 ENDIF
 TIME Year$, Month$, Day$, Hour$, Minute$, Second$, WeekDay, isDST
 Today$=month$+"-"+day$+"-"+year$

 DO
  DIALOG.MESSAGE , "email to " +em$, c, "yes", "edit", "no"
  IF c=2
   INPUT "change email", em$, em$, emcancel
   IF emcancel THEN  D_U.BREAK
   writeln("email.txt", em$)
  ENDIF

 UNTIL c<>2
 IF emcancel THEN FN.RTN  0

 IF c=3 THEN FN.RTN 0

 s$+="\n\ncreated by Political Gibberish Generator for Android\nby mookiebearapps"
 EMAIL.SEND em$, "gibberish "+today$, s$

 POPUP "sent!"
FN.END

! returns true if users answers yes to prompt m$
FN.DEF cask(m$)
 DIALOG.MESSAGE,m$,c,"yes","no"
 IF c=1 THEN FN.RTN 1
 FN.RTN 0
FN.END
TTS.INIT

FN.DEF random(r)
 FN.RTN FLOOR(RND()*r)+1
FN.END


!
!intros
FN.DEF intro$()
 FN.RTN wd$("intro")
FN.END

! singular pronouns
FN.DEF pronouns$()
 FN.RTN wd$("singular pronoun")
FN.END

! plural pronouns
FN.DEF pronounp$()
 FN.RTN wd$("plural pronoun")
FN.END

!singular nouns
FN.DEF nouns$()
 FN.RTN wd$("singular noun")
FN.END

! plural nouns
FN.DEF nounp$()
 FN.RTN wd$("plural noun")
FN.END

! pronouns acting as objects
FN.DEF objpronoun$()
 FN.RTN wd$("object pronoun")
FN.END

! nouns used as objects
FN.DEF objnoun$()
 FN.RTN wd$("object noun")
FN.END

! adjectives
FN.DEF adj$()
 FN.RTN wd$("adjective")
FN.END

! adverbs
FN.DEF adv$()
 FN.RTN wd$("adverb")
FN.END

! singular verbs
FN.DEF verbs$()
 FN.RTN wd$("singular verb")
FN.END

! plural verbs
FN.DEF verbp$()
 FN.RTN wd$("plural verb")
FN.END

! future tense verbs
FN.DEF fverb$()
 FN.RTN wd$("future verb")
FN.END

! singular verbs with no object
FN.DEF verbsno$()
 FN.RTN wd$("singular verb no object")
FN.END

! plural verbs with no object
FN.DEF verbpno$()
 FN.RTN wd$("plural verb no object")
FN.END

!singular verbs that precede other verbs
FN.DEF verbints$()
 FN.RTN wd$("singular verb for verb")
FN.END

!plural verbs that precede other verbs
FN.DEF verbintp$()
 FN.RTN wd$("plural verb for verb")
FN.END

! conjunctions
FN.DEF conj$()
 FN.RTN wd$("conjunction")
FN.END

! the's 
FN.DEF the$()
 FN.RTN wd$("the")
FN.END

! optional strings
FN.DEF opt$(s$)
 IF random(2)=1 
  FN.RTN s$
 ELSE
  FN.RTN ""
 ENDIF
FN.END

FN.DEF opt2$(s$)
 IF random(8)=1 
  FN.RTN s$
 ELSE
  FN.RTN ""
 ENDIF
FN.END

FN.DEF opt3$(s$)
 IF random(4)=1 
  FN.RTN s$
 ELSE
  FN.RTN ""
 ENDIF
FN.END


FN.DEF pick$(s1$,s2$)
 IF random(2)=1
  FN.RTN s1$
 ELSE
  FN.RTN s2$
 ENDIF
FN.END


! sentence singular subject
FN.DEF subjects$()
 IF random(4)>1
  FN.RTN opt$(the$())+opt$(adj$())+nouns$()
 ELSE
  FN.RTN pronouns$()
 ENDIF
FN.END

! plural sentence subject
FN.DEF subjectp$()
 IF random(4)>1
  FN.RTN opt$(the$())+opt$(adj$())+nounp$()
 ELSE
  FN.RTN pronounp$()
 ENDIF
FN.END

! object 
FN.DEF object$()
 IF random(2)=1
  FN.RTN opt$(the$())+opt$(adj$())+objnoun$()
 ELSE
  FN.RTN objpronoun$()
 ENDIF
FN.END

! singular or plural sentence
FN.DEF sentences$()
 c=random(3)
 IF c=1
  FN.RTN subjects$()+opt2$(cl$())+verbs$()+object$()+opt2$(cl$())+opt$(adv$())
 ELSE if c=2
  FN.RTN subjects$()+opt2$(cl$())+verbsno$()+opt$(adv$())
 ELSE
  FN.RTN subjects$()+opt2$(cl$())+verbints$()+fverb$()+object$()+opt$(adv$())
 ENDIF
FN.END
FN.DEF sentencep$()
 c= random(3)
 IF c=1
  FN.RTN subjectp$()+opt2$(cl$())+verbp$()+object$()+opt2$(cl$())+opt$(adv$())
 ELSE if c=2
  FN.RTN subjectp$()+opt2$(cl$())+verbpno$()+opt$(adv$())
 ELSE
  FN.RTN subjectp$()+opt2$(cl$())+verbintp$()+fverb$()+object$()+opt$(adv$())
 ENDIF
FN.END

! return sing or plural sentence
FN.DEF sentence1$()
 FN.RTN opt3$(intro$())+pick$(sentences$(),sentencep$())
FN.END

FN.DEF sentencenocl$()
 IF random(2)=1
  s1$=subjects$()+verbs$()+object$()+opt$(adv$())
  s2$=subjectp$()+verbp$()+object$()+opt$(adv$())
  FN.RTN pick$(s1$,s2$)
 ELSE
  s1$=subjects$()+verbsno$()+opt$(adv$())
  s2$=subjectp$()+verbpno$()+opt$(adv$())
  FN.RTN pick$(s1$,s2$)
 ENDIF
FN.END

! clause 
FN.DEF cl$()
 FN.RTN "which "+sentencenocl$()
FN.END

! sentences joined
FN.DEF sentence2$()
 FN.RTN sentence1$()+opt$(conj$()+sentence1$())
FN.END

! format it
FN.DEF frm$()
 s$=TRIM$(sentence2$())+"."
 l=LEN(s$)
 s$=UPPER$(LEFT$(s$,1))+MID$(s$,2,l)
 FN.RTN s$
FN.END


!
!
! main

n=20
if !isold("userwords.txt")
GRABFILE ff$,"words.txt"
writeln("userwords.txt",ff$)
endif

sp=cask("play speech?")
if cask("change options?")
INPUT "how much gibberish?",n,20

IF cask("reset to  defaults?")
 GRABFILE ff$,"words.txt"
 writeln("userwords.txt",ff$)
ENDIF

if cask("edit database?")
GRABFILE s$,"userwords.txt"
s$=htmledit$("edit db",s$)
writeln("userwords.txt",s$)
endif
endif

FOR i=1 TO n
 s$= frm$()
 PRINT s$
 IF sp THEN TTS.SPEAK s$
 all$+=s$+"\n\n"
NEXT i

TEXT.INPUT all$,all$
clipboard.put all$
popup "copied to clipboard"
sendmail(all$)
EXIT
