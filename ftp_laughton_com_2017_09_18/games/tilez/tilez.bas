! Tilez - tile invaders

!pastel colors

FN.DEF col()
 FN.RTN 50+FLOOR(RND()*100)
FN.END

GR.OPEN 255,0,0,0,0,1

restart:
Gr.cls
!Tts.init
GR.ORIENTATION 1
GR.SET.ANTIALIAS 0
GR.SCREEN aw,ah
Dw=720:Dh=1280
Sx=aw/dw:Sy=ah/dh
Gr.scale sx,sy
LIST.CREATE n,l
LIST.CREATE n,lvy
GR.SET.ANTIALIAS 0
GR.RECT edge,0,dh,dw,dh+70
GR.RECT gone,0,-40,dw,0
boxh=dh/20:boxw=dw/4:rows=40:win=0
n=0:score=0:lose=0:mw=dw/14:mv=dh/40

FOR i=1 TO rows
 x=FLOOR(RND()*4)
 FOR j=0 TO 3
  IF x>1
   n++
   GR.COLOR 255,col(),col(),col()
   GR.RECT b,j*boxw+3,i*boxh-boxh*rows,(j+1)*boxw-3,(i+1)*boxh-3-boxh*rows
   GR.SHOW b
   LIST.ADD l,b
   LIST.ADD lvy,2+RND()*5
  ENDIF
 NEXT
NEXT
Hit=1
! put missile off screen until launched
GR.RECT m,-30,dh,-1,dh+30
GR.HIDE m
LIST.SIZE l,z
DO
let p=clock()
 
 FOR i=1 TO z
  GR.TOUCH t,x,y
 
  IF t & hit  
X/=sx:y/=sy
   let Hit=0:GR.MODIFY m,"left",x-mw,"top",dh,"right",x+mw,"bottom",dh+30
   GR.SHOW m
  ENDIF
  LIST.GET l,i,q:LIST.GET lvy,i,dy:GR.MOVE q,0,dy
  IF GR_COLLISION(q,edge) THEN let Lose=1:F_N.BREAK
  IF GR_COLLISION(m,gone) THEN let hit=1:GR.HIDE m
  IF GR_COLLISION(m,q)
   GR.HIDE q:GR.HIDE m:let hit=1:score++
   !tts.speak int$(score),0
   IF score=n THEN let win=1
  ENDIF
 NEXT
 IF !hit THEN GR.MOVE m,0,-mv
 GR.RENDER

pause max(1,1-(clock()-p))
UNTIL lose | win
IF win 
s$="Congratulations, you won!\n"
else
 S$="Sorry, you lost.\n"
endif
LIST.CLEAR l:LIST.CLEAR lvy
S$+= "Your final score is "+INT$(score)
s$+="\nPlay again?"
!Tts.speak s$,1
DO
 DIALOG.MESSAGE "game over", s$, c ,"yes!","no"
UNTIL c>0
IF c=1 THEN GOTO restart
EXIT
