!debug.on
!debug.echo.on
!--------------------------
!functions------------
!------------------------
fn.def Choose(index,x1,x2,x3,x4)
	if index=1 then 
		fn.rtn x1
	elseif index=2 then
		fn.rtn x2
	elseif index=3 then
		fn.rtn x3
	elseif index=4 then
		 fn.rtn x4			
	endif
fn.end

fn.def ChsSix(index,x1,x2,x3,x4,x5,x6)
	if index=1 then 
		fn.rtn x1
	elseif index=2 then
		fn.rtn x2
	elseif index=3 then
		fn.rtn x3
	elseif index=4 then
		fn.rtn x4			
	elseif index=5 then
		fn.rtn x5
	elseif index=6 then
		fn.rtn x6	
	else
		fn.rtn 0		
	endif
fn.end

!--------------------------
!end functions------------
!------------------------

Dim BlocksX[4,2000]
Dim BlocksY[4,2000]
Dim BlocksZ[4,2000]
Dim BlockColor[2000]
Dim vpath[10,10]
Dim List[2000]

!--------------------------
!Constants
!--------------------------
keyleft=1
keyup=2
keyright=3
keydown=4
fog=7

progress=0
gosub showprogress
!-----------------------
init:



!load tiles
For i = 1 To 10
	gosub loadmap
   For k = 1 To 10
   	Vpath[k, i] = tmparray[k]
   next
   array.delete tmparray[]
   gosub showprogress
next   
vi = 74
For i = 1 To 74
	gosub loadpoly
	BlocksX[1,i] = tmparray[1]
	BlocksZ[1,i] = tmparray[2]
	BlocksY[1,i] = 0.250
	BlocksX[2,i] = tmparray[3]
	BlocksZ[2,i] = tmparray[4]
	BlocksY[2,i] = 0.250
	BlocksX[3,i] = tmparray[3]
	BlocksZ[3,i] = tmparray[4]
	BlocksY[3,i] = -0.250
	BlocksX[4,i] = tmparray[1]
	BlocksZ[4,i] = tmparray[2]
	BlocksY[4,i] = -0.250 
	BlockColor[i]=tmparray[5]
	list.create n,List[i]
	array.delete tmparray[]
   gosub showprogress	
Next


Print vi, " Nodes"
Print "Initializing Screen"
pause 500

gr.open 255, 192, 192, 192
gr.Orientation 0
gr.Screen mx, my
gr.set.stroke 3
stack.create N, nStack

mxt = mx / 2
myt = my / 2
gr.Color 255, 0, 0, 0, 0

!focal length
zp = mx*2/3

mxt=mx/2
myt=my/2
mytxt=my/10
mxtxt=mx/4
PI = Atan(1) * 4

RenderCall=1
tempx =0 
tempy=0
tempz=0
dim tmpx[4]
dim tmpy[4]
dim tmpz[4]
touchflag =0
touchx=0
touchy=0

Dim Zpoly[3,vi] 
Dim PreSort[vi]
Dim Cpoly[vi] 
Sortgap = 0

Do
     Sortgap = Sortgap * 3 + 1
Until Sortgap > vi


Dim Omap[mx+1]


CameraX = -1
CameraY = 2
oldCamX = -1
oldCamY = 2
Goalx= -4
Goaly = -3
vdstepx = 0
vdstepy = 0
ANIMATION = 0
RANIMATION = 0
RotLR = 0.5
oldRotLR = 0.5
RotHPI = RotLR * PI
COSRPI = Cos(RotHPI)
SINRPI = Sin(RotHPI)

EOG = 0
gosub MakeClock
!----------------------------------------------
!----------------------------------------------
!----------------------------------------------
!mainloop
!----------------------------------------------

!debug.echo.on
do 
	if rendercall=1  then
		rendercall=0
		gosub render
	else
		gosub gettouch
	endif
until 0


!OnError:
gr.Close
End
!-------------------------------------------
!------------subroutines------------------
!-------------------------------------------
! 3d Render
render:

	RotHPI = (oldRotLR * RANIMATION + (1 - RANIMATION) * RotLR) * PI
	COSRPI = Cos(RotHPI)
	SINRPI = Sin(RotHPI)
	
	!stopwatch=clock()
	
	gosub getRotate
	!stopwatch2=clock() 
	gosub ShellSort
	!stopwatch3=clock()
	
	!print stopwatch2-stopwatch,stopwatch3-stopwatch2
	
	!=================
	!Occlusion Mapping 
	!-----------------
	!For j = 1 To mx+1
	!    Omap[j] = 0
	!Next
	!================
	i = 1

	While i <= vi
	    Zpoly[3, i] = 0
	    tmpval = Zpoly[2, i]
	    If Zpoly[1, i] > 0 & Zpoly[1, i] < fog Then
	    	  	gosub ClipCheck
	        	If rClipCheck Then 
	        		Zpoly[3, i] = 1
	        	!=================================
	        	!Occlusion Mapping 
	        	!-----------------
	        	!   stepper = 1
				!	list.get List[tmpval],1,X1
				!	list.get List[tmpval],3,X2
	        	!    If x2 > x1 Then stepper = -1
	         !   
	         !   For j = x2 To x1 Step stepper
  	         !      If Omap[j+1] = 0 Then
	         !           Omap[j+1] = 1
	         !          Zpoly[3, i] = 1
	         !       End If
	         !   Next
	         !===============================  
				End If
	       	
	    End If
	    
	    i = i + 1
	repeat

	i = vi
	gosub HideClock
	gr.cls
	While i > 0
	    tmpval = Zpoly[2, i]
	    If Zpoly[3, i] = 1 Then
			fade=Zpoly[1, i]/fog
			if fade>1 then fade=1
			gcolor=192*fade
			vcolor=Cpoly[tmpval]*(1-fade)+fade*192
			gr.color 255,gcolor,gcolor,vcolor,1
			gr.poly pt, List[tmpval]
			gr.color 255,gcolor,gcolor,gcolor,0
			gr.poly pt, List[tmpval]
	    End If
	    i = i - 1
	    
	repeat
	gr.render

  
	If RANIMATION > 0 Then 
		rendercall=1
		popup "turning..",0,0,0
		RANIMATION = RANIMATION - 0.5 
		If RANIMATION < 0.003 Then RANIMATION = 0
	endif
	
	If ANIMATION > 0 Then
		rendercall=1
		ANIMATION = ANIMATION - 1
		If ANIMATION <0.003 Then ANIMATION = 0
	End If
	If CameraX = Goalx & CameraY = Goaly Then
		gr.render
	    popup "WINNER!!!!!!",0,0,0
	    EOG = 1
	End If
return
!---------------------------------------
!---------------------------------------
ClipCheck:

    list.get List[tmpval],1,X1
    list.get List[tmpval],2,y1 
    list.get List[tmpval],3,X2
    list.get List[tmpval],4,y2
    
    !not on screen
    If (X1 < 0 & X2 < 0) | (X1 > mx & X2 > mx) Then
        rClipCheck = 0
        goto Xitsub_ClipCheck
    Else
        rClipCheck = 1
    End If

    !==========================
    
    If X1 < 0 Then
        
        y3 = y1 - ((y2 - y1) / (X2 - X1)) * X1
        list.replace List[tmpval],1, 0
        list.replace List[tmpval],2, y3
        list.replace List[tmpval],7, 0
        list.replace List[tmpval],8, 2 * myt - y3
        X1 = 0
        y1 = y3
    End If
    


    If X2 < 0 Then
        
        y3 = y1 - ((y2 - y1) / (X2 - X1)) * X1
        list.replace List[tmpval],3, 0
        list.replace List[tmpval],4, y3
        list.replace List[tmpval],5, 0
        list.replace List[tmpval],6, 2 * myt - y3
    End If
    
    If X1 > mx Then
        X1 = X1 - mx
        X2 = X2 - mx
        
        y3 = y1 - ((y2 - y1) / (X2 - X1)) * X1
        list.replace List[tmpval],1, mx
        list.replace List[tmpval],2, y3
        list.replace List[tmpval],7, mx
        list.replace List[tmpval],8, 2 * myt - y3
    End If
    
    
    If X2 > mx Then
        X1 = X1 - mx
        X2 = X2 - mx
        
        y3 = y1 - ((y2 - y1) / (X2 - X1)) * X1
        list.replace List[tmpval],3, mx
        list.replace List[tmpval],4, y3
        list.replace List[tmpval],5, mx
        list.replace List[tmpval],6,2 * myt - y3
    End If
Xitsub_ClipCheck:
return
!----------------------------------------------
!-----------------------------------------------
!-----------------------------------------------
!Rotate and 3D plot routine INPUT:I
getRotate:
stack.push nstack, x

precamx=(CameraX * (1 - ANIMATION) + oldCamX * ANIMATION)
precamy=(CameraY * (1 - ANIMATION) + oldCamY* ANIMATION)
	
for i = 1 to vi
	gosub DrawClock
   
   Cpoly[i] = BlockColor[i]
   avgz = 0
   avgx = 0
   For x = 1 To 4
       tempx = BlocksX[x, i] - precamx
       tempz = BlocksZ[x, i] - precamy
       tempx = tempx - 0.5
       tempz = tempz - 0.5
       tmpx[x] = COSRPI * tempx - SINRPI * tempz
       tmpz[x] = COSRPI * tempz + SINRPI * tempx
       tmpy[x] = BlocksY[x, i]
       avgz = avgz + tmpz[x] * 0.25
       avgx = avgx + tmpx[x] * 0.25       
   Next
   
	IF avgz>= 0 then
		Zpoly[1, i] = sqr(avgx * avgx + avgz * avgz) 
	else
		Zpoly[1, i] =0-sqr(avgx * avgx + avgz * avgz) 
	endif   
	
   If Zpoly[1, i] > 0 & Zpoly[1, i] < fog Then
	   gosub ClipCheckZ
	   list.clear List[i]
	   For x = 1 To 4
	
	       list.add List[i],  mxt - tmpx[x] * zp / (tmpz[x] + 0.01)
	       list.add List[i], myt - tmpy[x] * zp / (tmpz[x] + 0.01)
	   Next
	endif
	
next
	 stack.pop nstack, x
RETURN
!---------------------------------------
!---------------------------------------
!Check floor clipping 

ClipCheckZ:
    Z1 = tmpz[1]
    Z2 = tmpz[2]
    X1 = tmpx[1]
    X2 = tmpx[2]
    
    !not on screen
    If (Z1 < 0 & Z2 < 0) Then
        goto Xitsub_ClipCheckZ
    End If
!==========================
    If Z1 < 0 Then
        
        X3 = X1 - ((X2 - X1) / (Z2 - Z1)) * Z1
        tmpx[1] = X3
        tmpz[1] = 0
        tmpx[4] = X3
        tmpz[4] = 0
    End If
    
    If Z2 < 0 Then
        
        X3 = X1 - ((X2 - X1) / (Z2 - Z1)) * Z1
        tmpx[2] = X3
        tmpz[2] = 0
        tmpx[3] = X3
        tmpz[3] = 0
    End If
Xitsub_ClipCheckZ:

return
!------------------------------
!-----------------------------
!Touch Screen Routine

gettouch:
 
If EOG Then goto Xitsub

gr.touch touchflag,touchx,touchy

if touchflag=0 then goto Xitsub

KeyCode=ChsSix(floor(touchx * 3 / mx )+ floor(touchy * 2 / my )*3+ 1,1,2,3,1,4,3)

tRotLR = RotLR + 0.25
If tRotLR >= 2 Then tRotLR = tRotLR - 2
tRotLR = floor(tRotLR * 2) + 1

If ANIMATION = 0 & RANIMATION = 0 Then
    sw.begin KeyCode
        sw.Case keyUp
        		
            vdstepy = Choose(tRotLR, -1, 0, 1, 0)
            vdstepx = Choose(tRotLR, 0, 1, 0, -1)
            If vdstepx <> 0 then 
            		
            		x=CameraX + vdstepx
            		y=CameraY
                  gosub checkmap 
            		if rcheckmap = 1 Then
							popup "forward" ,0,0,1
							oldCamX = CameraX
							oldCamY= CameraY
							ANIMATION = 0
							rendercall=1
							CameraX = CameraX + vdstepx
						else
							popup "can't walk through walls!" ,0,0,0
						endif                    
            elseif vdstepy <> 0 then
            		
					x=CameraX 
					y=CameraY - vdstepy            
					gosub checkmap 
            	if rcheckmap = 1 Then
						popup "forward" ,0,0,1
						oldCamX = CameraX
						oldCamY= CameraY
						ANIMATION =0
						rendercall=1
						CameraY = CameraY - vdstepy
					else
						popup "can't walk through walls!" ,0,0,0						
					endif               
            End If
            sw.break
        sw.Case  keyDown
       		 
            vdstepx = Choose(tRotLR, 0, -1, 0, 1)
            vdstepy = Choose(tRotLR, 1, 0, -1, 0)
            If vdstepx <> 0 then
					
					x=CameraX + vdstepx
					y=CameraY            
					gosub checkmap 
            	if rcheckmap = 1 Then
            		popup "backward" ,0,0,1
						oldCamX = CameraX
						oldCamY= CameraY
						ANIMATION =0
						rendercall=1
						CameraX = CameraX + vdstepx
					else
						popup "can't walk through walls!" ,0,0,0							
					endif               
            elseif vdstepy <> 0 then
					
					x=CameraX 
					y=CameraY - vdstepy             
					gosub checkmap 
            	if rcheckmap Then
						popup "backward" ,0,0,1            	
						oldCamX = CameraX
						oldCamY= CameraY
						ANIMATION =0
						rendercall=1
						CameraY = CameraY - vdstepy
					else
						popup "can't walk through walls!" ,0,0,0							
               endif
            End If
            sw.break
        sw.Case  keyRight
        		popup "right" ,0,0,1
            oldRotLR = RotLR
            RANIMATION =0.5
            rendercall=1
            RotLR = RotLR - 0.5
            If RotLR <= 0 Then
                RotLR = RotLR + 2
                oldRotLR = oldRotLR + 2
            End If
            sw.break
        sw.Case  keyLeft
        		popup "left" ,0,0,1
            oldRotLR = RotLR
            RANIMATION =0.5
            rendercall=1
            RotLR = RotLR + 0.5
            If RotLR >= 2 Then
                RotLR = RotLR - 2
                oldRotLR = oldRotLR - 2
            End If
            sw.break
    sw.end
EndIf


Xitsub:
return

!---------------------------------------
!---------------------------------------

checkmap:
!inputs:x,y		output: rcheckmap

If x < -4 | x > 5 | y < -4 | y > 5 Then
    rcheckmap = 0
    goto Xitsub_checkmap
End If

rcheckmap = Vpath[x+5, y+5]

Xitsub_checkmap:
return



!---------------------------------------
!---------------------------------------
ShellSort:
	stack.push nStack,i
	For i = 1 To vi
	    Zpoly[2,i] = i
	Next
	gap = sortgap
	Do
	    gap = floor(gap / 3)
	    For i = (gap + 1) To vi
	        CurPos = i
	        tempval = Zpoly[1,i]
	        tempval2 = Zpoly[2,i]
	        
	        skip=0
	        
	        do 
	        		if (Zpoly[1,CurPos - gap] > tempval) then
		            Zpoly[1,CurPos] = Zpoly[1,CurPos - gap]
		            Zpoly[2,CurPos] = Zpoly[2,CurPos - gap]
		            CurPos = CurPos - gap
		         else
		         	skip=1
	        		endif
		        If (CurPos - gap) < 1 Then skip=1
		        
		     until skip=1

	        Zpoly[1,CurPos] = tempval
	        Zpoly[2,CurPos] = tempval2
			  gosub DrawClock
	    Next
	  	   
	Until gap = 1
	stack.pop nStack,i
return

!--------------------------------------------
!--------------------------------------------
!Draw Clock while processing
MakeClock: 

gr.color 255,255,255,255,1
gr.circle DCO1,mx/2,my/2,20
gr.color 255,0,0,0,0
gr.circle DCO2,mx/2,my/2,20 
DCR=mod(clock,1000)/1000 
gr.line DCO3, mx/2,my/2, cos(DCR * pi)*20+mx/2, sin(DCR * pi)*20+my/2
DCSkip=0
gr.hide DCO3
gr.hide DCO2
gr.hide DCO1

return

HideClock:

gr.hide DCO3
gr.hide DCO2
gr.hide DCO1
DCSkip=1
return


return

DrawClock:
if DCSKip=1 then goto XitSub_DrawClock
gr.show DCO3
gr.show DCO2
gr.show DCO1
DCR=mod(clock(),1000)/500 
gr.modify DCO3,"x2",cos(DCR * pi)*20+mx/2
gr.modify DCO3,"y2",sin(DCR * pi)*20+my/2
gr.render

XitSub_DrawClock:
return 
 
!-------------------------
showprogress:
	Cls
	Print ""
	Print "Fog 3D by TechnoRobbo"
	Print " "
	Print "TechnoRobbo's 3D Basic! Engine V1.50"
	Print ""
	Print "Initializing 3D Graphics";   
	progress=progress+1
	for l=1 to progress
		Print".";
	next
	Print "" 
	pause 15
return


!--------------------------------------------
!map loader

loadmap:
    sw.begin i-1
        sw.case 0
            array.load tmparray[],0,0,0,0,0,0,0,0,0,0
            sw.break
        sw.case 1
            array.load tmparray[],1,1,0,1,1,1,1,1,1,0
            sw.break
        sw.case 2
            array.load tmparray[],0,1,1,1,0,0,0,0,1,0
            sw.break
        sw.case 3
            array.load tmparray[],0,0,0,0,0,1,1,1,1,0
            sw.break
        sw.case 4
            array.load tmparray[],0,1,1,1,1,1,0,1,1,0
            sw.break
        sw.case 5
            array.load tmparray[],0,1,0,0,0,1,0,0,0,0
            sw.break
        sw.case 6
            array.load tmparray[],0,1,0,1,1,1,1,1,1,0
            sw.break
        sw.case 7
            array.load tmparray[],0,0,0,1,0,0,1,0,1,0
            sw.break
        sw.case 8
            array.load tmparray[],0,1,1,1,1,0,1,0,1,0
            sw.break
        sw.case 9
            array.load tmparray[],0,0,0,0,0,0,0,0,0,0
            sw.break
    sw.end
return

!---------------------------------------------------
!load polygons
loadpoly:
	sw.begin i-1
		sw.case 0
			array.load tmparray[],-4,-3.55,-3,-3.55,128

			sw.break
		sw.case 1
			array.load tmparray[],-3,-3.55,-2,-3.55,128

			sw.break
		sw.case 2
			array.load tmparray[],-1.55,-3.55,-1.55,-3,240
			sw.break
		sw.case 3
			array.load tmparray[],-2,-3.55,-1,-3.55,128

			sw.break
		sw.case 4
			array.load tmparray[],-1,-3.55,0,-3.55,128

			sw.break
		sw.case 5
			array.load tmparray[],0,-3.55,1,-3.55,128

			sw.break
		sw.case 6
			array.load tmparray[],1,-3.55,2,-3.55,128

			sw.break
		sw.case 7
			array.load tmparray[],2,-3.55,3,-3.55,128

			sw.break
		sw.case 8
			array.load tmparray[],3,-3.55,4,-3.55,128

			sw.break
		sw.case 9
			array.load tmparray[],4,-3.55,5,-3.55,128

			sw.break
		sw.case 10
			array.load tmparray[],5.45,-3.55,5,-3.55,128
			sw.break
		sw.case 11
			array.load tmparray[],5.45,-3.55,5.45,-3,240

			sw.break
		sw.case 12
			array.load tmparray[],-1.55,-3,-1.55,-2,240

			sw.break
		sw.case 13
			array.load tmparray[],5.45,-3,5.45,-2,240

			sw.break
		sw.case 14
			array.load tmparray[],-3.55,-2,-3.55,-1,240

			sw.break
		sw.case 15
			array.load tmparray[],0.45,-1.55,0.45,-1,240
			sw.break
		sw.case 16
			array.load tmparray[],0.45,-1.55,1,-1.55,128

			sw.break
		sw.case 17
			array.load tmparray[],1,-1.55,2,-1.55,128

			sw.break
		sw.case 18
			array.load tmparray[],2,-1.55,3,-1.55,128

			sw.break
		sw.case 19
			array.load tmparray[],3,-1.55,4,-1.55,128

			sw.break
		sw.case 20
			array.load tmparray[],5.45,-2,5.45,-1,240

			sw.break
		sw.case 21
			array.load tmparray[],-3.55,-1,-3.55,0,240
			sw.break
		sw.case 22
			array.load tmparray[],-3.55,-0.55,-3,-0.55,128

			sw.break
		sw.case 23
			array.load tmparray[],-3,-0.55,-2,-0.55,128

			sw.break
		sw.case 24
			array.load tmparray[],-2,-0.55,-1,-0.55,128

			sw.break
		sw.case 25
			array.load tmparray[],-1,-0.55,0,-0.55,128

			sw.break
		sw.case 26
			array.load tmparray[],0.45,-0.55,0,-0.55,128
			sw.break
		sw.case 27
			array.load tmparray[],0.45,-0.55,0.45,-1,240

			sw.break
		sw.case 28
			array.load tmparray[],5.45,-1,5.45,0,240

			sw.break
		sw.case 29
			array.load tmparray[],-3.55,0,-3.55,1,240

			sw.break
		sw.case 30
			array.load tmparray[],2.45,0,2.45,1,240

			sw.break
		sw.case 31
			array.load tmparray[],5.45,0,5.45,1,240

			sw.break
		sw.case 32
			array.load tmparray[],-3.55,1,-3.55,2,240

			sw.break
		sw.case 33
			array.load tmparray[],-1.55,1.45,-1.55,2,240
			sw.break
		sw.case 34
			array.load tmparray[],-1.55,1.45,-1,1.45,128

			sw.break
		sw.case 35
			array.load tmparray[],-1,1.45,0,1.45,128

			sw.break
		sw.case 36
			array.load tmparray[],0,1.45,1,1.45,128

			sw.break
		sw.case 37
			array.load tmparray[],2.45,1.45,2.45,1,240
			sw.break
		sw.case 38
			array.load tmparray[],2.45,1.45,3,1.45,128

			sw.break
		sw.case 39
			array.load tmparray[],3,1.45,4,1.45,128

			sw.break
		sw.case 40
			array.load tmparray[],4,1.45,5,1.45,128

			sw.break
		sw.case 41
			array.load tmparray[],5.45,1.45,5,1.45,128
			sw.break
		sw.case 42
			array.load tmparray[],5.45,1,5.45,2,240

			sw.break
		sw.case 43
			array.load tmparray[],-3.55,2,-3.55,3,240

			sw.break
		sw.case 44
			array.load tmparray[],-1.55,2,-1.55,3,240

			sw.break
		sw.case 45
			array.load tmparray[],5.45,2,5.45,3,240

			sw.break
		sw.case 46
			array.load tmparray[],-3.55,3,-3.55,4,240
			sw.break
		sw.case 47
			array.load tmparray[],-3.55,3.45,-3,3.45,128

			sw.break
		sw.case 48
			array.load tmparray[],-3,3.45,-2,3.45,128

			sw.break
		sw.case 49
			array.load tmparray[],-1.55,3.45,-2,3.45,128
			sw.break
		sw.case 50
			array.load tmparray[],-1.55,3.45,-1.55,3,240

			sw.break
		sw.case 51
			array.load tmparray[],0,3.45,1,3.45,128

			sw.break
		sw.case 52
			array.load tmparray[],1.45,3.45,1,3.45,128
			sw.break
		sw.case 53
			array.load tmparray[],1.45,3.45,1.45,4,240

			sw.break
		sw.case 54
			array.load tmparray[],3.45,3,3.45,4,240

			sw.break
		sw.case 55
			array.load tmparray[],5.45,3,5.45,4,240

			sw.break
		sw.case 56
			array.load tmparray[],-3.55,4,-3.55,5,240

			sw.break
		sw.case 57
			array.load tmparray[],1.45,4,1.45,5,240

			sw.break
		sw.case 58
			array.load tmparray[],3.45,4,3.45,5,240

			sw.break
		sw.case 59
			array.load tmparray[],5.45,4,5.45,5,240

			sw.break
		sw.case 60
			array.load tmparray[],-3.55,5.45,-3.55,5,240
			sw.break
		sw.case 61
			array.load tmparray[],-3.55,5.45,-3,5.45,128

			sw.break
		sw.case 62
			array.load tmparray[],-3,5.45,-2,5.45,128

			sw.break
		sw.case 63
			array.load tmparray[],-2,5.45,-1,5.45,128

			sw.break
		sw.case 64
			array.load tmparray[],-1,5.45,0,5.45,128

			sw.break
		sw.case 65
			array.load tmparray[],0,5.45,1,5.45,128

			sw.break
		sw.case 66
			array.load tmparray[],1,5.45,2,5.45,128
			sw.break
		sw.case 67
			array.load tmparray[],1.45,5.45,1.45,5,240

			sw.break
		sw.case 68
			array.load tmparray[],2,5.45,3,5.45,128

			sw.break
		sw.case 69
			array.load tmparray[],3,5.45,4,5.45,128
			sw.break
		sw.case 70
			array.load tmparray[],3.45,5.45,3.45,5,240

			sw.break
		sw.case 71
			array.load tmparray[],4,5.45,5,5.45,128

			sw.break
		sw.case 72
			array.load tmparray[],5.45,5.45,5,5.45,128
			sw.break
		sw.case 73
			array.load tmparray[],5.45,5.45,5.45,5,240

			sw.break
	sw.end
return
