!!
---------------------------------
Draw Poker by Rob Tauler (TechnoRobbo)
---------------------------------
!!
!debug.on

!debug.echo.on

gr.open 255,0, 128, 0, 0
gr.Orientation 0
gr.Screen mx, my
mxt = mx / 10
myt = mxt*96/71

gr.Color 255, 0, 128, 0, 0
dim deck[52]
dim card[5]
dim bmpcard[5]
dim cval[5]
dim pick[5]
dim pval[5]
dim bmppick[5]
dim suit[5]
dim face[5]
silent=1

bet=0
array.load bets$[], " 1 CREDIT", " 2 CREDITS" , " 3 CREDITS", " 4 CREDITS", " 5 CREDITS"
msg$="Place You Bet"

!Load and scale graphics

gr.bitmap.load tmp,"DrawPoker.PNG"
gr.bitmap.size tmp,bx,by
gr.bitmap.scale gfx,tmp, mxt*23,myt*5
gr.bitmap.delete tmp

!splash Screen
gr.bitmap.crop tmp,gfx,mxt*13,0,mxt*5,myt*3
gr.bitmap.scale bmpsplash,tmp,mxt*8,my
gr.bitmap.delete tmp
gr.bitmap.draw splash,bmpsplash ,(mx-mxt*8)/2,0
gr.render
pause 1000
popup "Tap to Start",0,0,0
do
	gr.touch touched,x,y
	pause 10
until touched
gr.cls

gr.bitmap.delete bmpsplash

!set up card locations
array.load mxs[], mx/2-mxt*4.5, mx/2-mxt*2.5, mx/2-mxt*0.5,mx/2+mxt*1.5,mx/2+mxt*3.5

for I =1 to 5
   gr.bitmap.crop bmpcard[I],gfx,0,4*myt,mxt,myt
   gr.bitmap.draw card[i], bmpcard[I] ,mxs[I],myt
next


for I =1 to 5
   gr.bitmap.crop bmppick[I],gfx,mxt,4*myt,mxt,myt
   gr.bitmap.draw pick[i], bmppick[I] ,mxs[I],myt*0.5
   pval[I]=1
next

gr.bitmap.crop btnup,gfx,mxt*3,4*myt,mxt*3,myt
gr.bitmap.crop btndwn,gfx,mxt*6,4*myt,mxt*3,myt
gr.bitmap.draw xbtn, btnup, mx/2-mxt*1.5, myt*2.25

gr.bitmap.crop bmpLEd,gfx,mxt*9,4*myt,mxt*3,myt
gr.bitmap.draw LEDL, bmpLEd, mxt*0.25, my-myt
gr.bitmap.draw LEDR, bmpLEd, mx-mxt*3.25, my-myt

gr.bitmap.crop bmpLogo,gfx,mxt*12,4*myt,mxt,myt
gr.bitmap.draw Logo, bmpLogo, (mx-mxt)/2, my-myt

gr.color 255,255,255,128,1
gr.text.size myt/3
gr.text.align 2
gr.text.draw btntxt, mx/2, myt*2.75 , "-wait-"
gr.text.draw Cdttxt,mxt*1.75,myt* 3.25 , "Credits"
gr.text.draw Bettxt,mx-mxt*1.75,myt* 3.25 , "Bet/Payout"

gr.color 255,192,192,96,1
gr.text.size myt/4
gr.text.draw inftxt, mx/2, myt* 3.25 , "Starting"

gr.color 255,255,0,0,1
gr.text.size myt/3
gr.text.align 3
gr.text.draw Cdtval,mxt*3,myt* 4 , format$("######%",20)
gr.text.draw Betval,mx-mxt*0.5,myt* 4 , format$("######%",0)

gr.bitmap.crop bmpmenu,gfx,mxt*18,0,mxt*5,myt*3 
gr.bitmap.draw menu,bmpmenu,(mx-mxt*5)/2,(my-myt*3)/2
gr.hide menu
gr.render

gosub deckshuffle

!poker hands
RoyalFlush=9
Str8Flush=8
FourKind=7
FullHouse=6
Flush=5
Str8=4
ThreeKind=3
TwoPair=2
JacksPlus=1
Bust=0
array.load WinMsg$[],"Jacks or Better","Two Pairs","Three of a Kind"~
"Straight","Flush","Full House","Four of a Kind","Straight Flush","Royal Flush"
array.load Payout[],1,2,3,4,6,9,25,50,250
Score=Bust

!token constants
makebet=1
DeelOut=2
pickdraw=3
draw=4
uwon=5
ulost=6
bank=7
idle=8
PlayAgain=9
Chk4Win=10

token=makebet
cardpoint=1
Credits=20

!!
-----------------------------------
program loop
-----------------------------------
!!

Do
	if token=makebet then
		if cardpoint >42 then
		  gosub deckshuffle
		endif	
 		gosub ClrPicks
   silent=0 
		gr.modify btntxt,"text","BET"
		gr.modify inftxt,"text","Press to Bet"
		gr.render
		gosub chkbutton
		gosub getbet
		gr.modify Betval,"text",format$("######%",bet)
		gr.render
				
		token=DeelOut

	elseif token=pickdraw then

		gr.modify btntxt,"text","DEAL"
		gr.modify inftxt,"text","Touch Cards to Hold"
		gr.render
		gosub chkpick
		gosub deal
		token=Chk4Win

	elseif token=Chk4Win then
		gr.modify btntxt,"text","-"
		gr.modify inftxt,"text","Checking"
		gr.render
		gosub CardBrkDn
		if Score>0 then  
         msg$="Winner : "  +  WinMsg$[Score] 
	      gr.modify inftxt,"text",msg$
			bet=payout[Score]*bet
         if bet =1250 then bet=4000
         gr.render
         for I =1 to 3
             tone 800,125
             pause 125
         next
		else
  		 gr.modify inftxt,"text","LOST" 
       gr.render
  		 bet=(-bet)
       tone 400,2000 
      endif
     
		Credits=Credits+bet
		gr.modify Cdtval,"text",format$("######%",Credits)
		gr.modify Betval,"text",format$("######%",bet)      
      
      gr.render
		pause 1000
		token=PlayAgain

	elseif token=PlayAgain then
	
		gr.modify btntxt,"text","Again!"
		gr.render
		gosub chkbutton
		token = makebet

	elseif token = Deelout then

		gr.modify inftxt,"text","Dealing!"
		gr.render
		for I =1 to 5
		    pval[i]=0
		next
		gosub deal
		token=pickdraw
		pause 10

	elseif token=idle then
	
		pause 10
		
	endif
until 0


OnError:
gr.Close
End

!!
-----------------------------
Subroutines
----------------------------
!!
deckshuffle:
	gr.modify inftxt,"text","Shuffling!"
	gr.render
	for I=1 to 52
	  deck[i]=I-1
	next
	array.shuffle deck[]
	cardpoint=1
Return
!-----------------------------
deal:
for I = 1 to 5
   pause 10
   if pval[I] =0 then
     tone 200,5 
     tmp=bmpcard[I]
     cval[i]=deck[cardpoint]
     cardpoint=cardpoint+1
     gr.bitmap.crop bmpcard[I],gfx,mod(cval[i],13)*mxt, floor(cval[i]/13) *myt,mxt,myt
     gr.bitmap.delete tmp
     gr.modify card[i],"bitmap",bmpcard[I]

     !pick clear
     tmp=bmppick[i]
     pval[i]=0
     gr.bitmap.crop bmppick[I],gfx,mxt,4*myt,mxt,myt
     gr.bitmap.delete tmp
     gr.modify pick[i],"bitmap",bmppick[I]
 
  endif
  gr.render
next
return
!-----------------------------
chkbutton:
	do
		gr.bounded.touch touched,mx/2-mxt*1.5,myt*2.5 ,mx/2+mxt*1.5,myt*3.5
	until touched

	gr.modify xbtn,"bitmap",btndwn
	gr.render
	do
		gr.bounded.touch touched, mx/2-mxt*1.5,myt*2.5 ,mx/2+mxt*1.5,myt*3.5
		pause 2
	until touched=0
	gr.modify xbtn,"bitmap",btnup
	gr.render
return
!-----------------------------
chkpick:
	do
		for i=1 to 5
			gr.bounded.touch touched, mxs[i],myt*0.5,mxs[i]+mxt,myt*2
			if touched then
				do
					gr.bounded.touch touched, mxs[i],myt*0.5,mxs[i]+mxt,myt*2
				until touched=0
				pval[i]=abs(pval[i]-1)
				tmp=bmppick[I]
				if pval[i]=0 then
					gr.bitmap.crop bmppick[I],gfx,mxt,4*myt,mxt,myt
				else
					gr.bitmap.crop bmppick[I],gfx,mxt*2,4*myt,mxt,myt
				endif
				gr.modify pick[i],"bitmap",bmppick[I]
				gr.bitmap.delete tmp
				gr.render
			endif
		next
		gr.bounded.touch touched, mx/2-mxt*1.5,myt*2.5 ,mx/2+mxt*1.5,myt*3.5
	until touched

	gr.modify xbtn,"bitmap",btndwn
	gr.render

	do
		gr.bounded.touch touched, mx/2-mxt*1.5,myt*2.5 ,mx/2+mxt*1.5,myt*3.5
		pause 2
	until touched=0

	gr.modify xbtn,"bitmap",btnup
	gr.render

return
!-----------------------------
ClrPicks:
for I = 1 to 5
   pause 5
   tmp=bmpcard[I]
   gr.bitmap.crop bmpcard[I],gfx,0,4*myt,mxt,myt
   gr.bitmap.delete tmp
   gr.modify card[i],"bitmap",bmpcard[I]
   pval[i]=0
   tmp=bmppick[i]
   gr.bitmap.crop bmppick[I],gfx,mxt,4*myt,mxt,myt
   gr.bitmap.delete tmp
   gr.modify pick[i],"bitmap",bmppick[I]
  gr.render
  if silent=0 then tone 200,5
next
Return
!-----------------------------
CardBrkDn:
	Score=Bust
	for i = 1 to 5
		suit[i]=floor(cval[i]/13)
	next

	for i = 1 to 5
		face[i]=mod(cval[i],13)
	next
	array.sort face[]

	!check flush
   if suit[1]=suit[2] & suit[2]=suit[3] & suit[3]=suit[4] & suit[4]=suit[5] then
   	flsh=1
   else
   	flsh=0
   endif
   !---------------
   if face[1]=0 & face[2]=9 & face[3]=10 & face[4]=11 & face[5]=12 & flsh then
   	Score=RoyalFlush
   	goto leave
   endif  
   !---------------
   if (face[1]+1=face[2]) & (face[2]+1=face[3]) & (face[3]+1=face[4]) & (face[4]+1=face[5]) then
   	if flsh then
   		Score=Str8Flush
			goto leave
   	endif

   endif
   !---------------
   if (face[1]=face[2]) & (face[2]=face[3]) & (face[3]=face[4]) then
   	Score=FourKind
   	goto leave
   endif

   if (face[2]=face[3]) & (face[3]=face[4]) & (face[4]=face[5]) then
   	Score=FourKind
   	goto leave
   endif
   !---------------
   if (face[1]=face[2]) & (face[2]=face[3]) then
    	if (face[4]=face[5]) then
    	   Score=FullHouse
	   	goto leave
   	endif
   endif
   if (face[3]=face[4]) & (face[4]=face[5]) then
    	if (face[1]=face[2]) then
    	   Score=FullHouse
	   	goto leave
   	endif
   endif
   !---------------
   if flsh=1 then
      Score=Flush
   	goto leave
   endif
   !---------------
   if (face[1]+1=face[2]) & (face[2]+1=face[3]) & (face[3]+1=face[4]) & (face[4]+1=face[5]) then
  		Score=Str8
		goto leave
   endif
   if face[1]=0 & face[2]=9 & face[3]=10 & face[4]=11 & face[5]=12 then
   	Score=Str8
   	goto leave
   endif 
   !---------------
   if (face[1]=face[2]) & (face[2]=face[3]) then
   	Score=ThreeKind
   	goto leave
   endif

   if (face[2]=face[3]) & (face[3]=face[4])  then
   	Score=ThreeKind
   	goto leave
   endif

   if (face[3]=face[4]) & (face[4]=face[5])  then
   	Score=ThreeKind
   	goto leave
   endif
   !---------------
   if (face[1]=face[2]) & (face[3]=face[4]) then
   	Score=TwoPair
   	goto leave
   endif

   if (face[1]=face[2]) & (face[4]=face[5])  then
   	Score=TwoPair
   	goto leave
   endif

   if (face[2]=face[3]) & (face[4]=face[5])  then
   	Score=TwoPair
   	goto leave
   endif
   !---------------
   if (face[1]=face[2]) & (face[1]=0) then
   	Score=JacksPlus
   	goto leave
   endif
	if (face[1]=face[2]) & (face[1]>9) then
   	Score=JacksPlus
   	goto leave
   endif
	if (face[2]=face[3]) & (face[2]>9) then
   	Score=JacksPlus
   	goto leave
   endif
	if (face[3]=face[4]) & (face[3]>9) then
   	Score=JacksPlus
   	goto leave
   endif
	if (face[4]=face[5]) & (face[4]>9) then
   	Score=JacksPlus
   	goto leave
   endif
leave:

Return

getbet:
gr.show menu
gr.render
bet=0
left=(mx-mxt*5)/2
top=(my-myt*3)/2 
do
	do 
	   gr.touch touched,x,y
     pause 10
	until touched
	x=(x-left)/mxt
	y=(y-top)/myt
	x=floor(x)
	y=floor(y)
	if x>=0 & y>=0 then
		if x=0 & y=0 then
			bet=1 
		elseif x=1 & y=0 then
			bet=1 			
		elseif x=3 & y=0 then
			bet=2 
		elseif x=4 & y=0 then
			bet=2 
	   elseif x=2 & y=1
	   	bet=3 
		elseif x=0 & y=2 then
			bet=4 
		elseif x=1 & y=2 then
			bet=4			
		elseif x=3 & y=2 then
			bet=5 	
		elseif x=4 & y=2 then
			bet=5 			
     endif
	endif
until bet>0
gr.hide menu
gr.render
return
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
