REM Start of BASIC! Program
REM Color Picker
REM Slide on the 3 Color Boxes
REM Tablet 800 x 480

 gr.open  255, 255, 255, 255
 gr.orientation 0
 r=0
 g=0
 b=0

 gr.text.size 30   % Exit button
 gr.color 255,180,180,180,1 %fill
 gr.rect btn ,700,420,790,470
 gr.color 255,0,0,0,0
 gr.text.draw tbtn,720,455,"Exit"
 gr.rect btnbox ,700,420,790,470 
 
 gr.text.size 40

 gr.color 255,255,0,0,1 %red fill 
 gr.rect colrect,50,100,325,150
 gr.color 255,0,255,0,1 %green fill
 gr.rect colrect,50,200,325,250
 gr.color 255,0,0,255,1 %blue fill
 gr.rect colrect,50,300,325,350
 
 gr.color 255,0,0,0,0 % outline boxs
 gr.rect colrect,50,100,325,150
 gr.rect colrect,50,200,325,250
 gr.rect colrect,50,300,325,350 
 gr.rect fet ,500,100,750,350 %paint box
 gr.rect fet1,499,99,751,351  %double line
 gr.rect redsld,50,90,70,160 %slider
 gr.rect grnsld,50,190,70,260
 gr.rect blusld,50,290,70,360
  
 gr.text.draw red1,335,150,str$(r)
 gr.text.draw green1,335,250,str$(g)
 gr.text.draw blue1,335,350,str$(b)
 gr.render % draw setup
 
do    %main loop
 gr.touch flag, x,y
 x= ceil(x)
 ifx>50 &y>100 &y<150&x< 305 then r= x -50
 ifx>50 &y>200 &y<250&x< 305 then g= x -50
 ifx>50 &y>300 &y<350&x< 305 then b= x -50
  
 gr.color 255,r,g,b,1 %fill
 gr.rect fet ,500,100,750,350 %paint box
 
 gr.modify red1,"text",str$(r) %slider
 gr.modify redsld,"left",r+50
 gr.modify redsld,"right",r+70

 gr.modify green1,"text",str$(g)
 gr.modify grnsld,"left",g+50
 gr.modify grnsld,"right",g+70 
 
 gr.modify blue1,"text",str$(b) 
 gr.modify blusld,"left",b+50
 gr.modify blusld,"right",b+70 
 
 gr.render  %update screen
 
until x> 700 &y>420 &flag % exit button clicked

end
