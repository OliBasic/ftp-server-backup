REM Start of BASIC! Program
REM touch top or bottom of screen to move ship
gr.open 255,0,0,0
gr.orientation 1
gr.color 255,255,5,5,1
gr.text.size 30
gr.rotate.start 90,450,680
gr.text.draw txtc,450,680,""
gr.rotate.end

    % 1st bitmap screen
ARRAY.LOAD a[], 30,0,50,300,110,470,80,580,120,750,30,800,0,800,0,0
LIST.CREATE n, List1
LIST.ADD.ARRAY List1, a[]

 gr.bitmap.create l1,140,800
 gr.bitmap.drawinto.start l1
 gr.color 255,0,255,0,1
 gr.poly pt, List1
 gr.bitmap.drawinto.end 

      % 2nd bitmap screen
ARRAY.LOAD b[],30,0,40,200,10,220,20,300,50,340,60,460,90,510,50,560,90,660,30,770,30,800~
10,960,30,1010,10,1030,40,1070,20,1100,100,1240,140,1510,30,1600,0,1600,0,0
LIST.CREATE n, List2
LIST.ADD.ARRAY List2, b[]

 gr.bitmap.create l2,140,1600
 gr.bitmap.drawinto.start l2
 gr.color 255,0,255,0,1 
 gr.poly pt, List2
 gr.bitmap.drawinto.end 

gr.set.stroke 1
gr.bitmap.create ship,12,40
gr.bitmap.drawinto.start ship
gr.color 255,0,150,250,1
gr.line L,12,4,12,9
gr.line L,11,4,11,9
gr.line L,10,0,10,12
gr.line L,9,0,9,12
gr.color 255,255,0,100,1
gr.line L,8,0,8,15
gr.line L,7,0,7,15
gr.line L,6,0,6,28
gr.line L,5,0,5,28
gr.line L,4,0,4,40
gr.color 255,255,255,255,1
gr.line L,3,0,3,40
gr.line L,2,4,2,20
gr.line L,1,4,1,20
gr.bitmap.drawinto.end 

 gr.bitmap.draw rd11,l1,0,0
 gr.bitmap.draw rd12,l2,0,1600

gr.circle ss,400,650,30
gr.bitmap.draw ship1,ship,100,200
spd=1
start:
tt=tt+35+spd  %speed

gr.modify rd11, "y",0-tt
gr.modify rd12,"y",800-tt
if tt>=1600
  gr.modify rd11,"y",2400-tt
spd++
end if

 gr.modify ship1,"x",100-yy

 gr.touch flag ,zx,zy        % ship up or down
if zx< 240 &flag
 yy=yy+20
if yy>60 then yy=60
 else if zx>240&flag 
 yy=yy-20
if yy<-340 then yy=-340
endif

if tt>= 2400 then tt =0

! checkcollision:    remove collision code for faster screens
if 100-yy<=140           % checks if ship is over bitmaps for speed up
gr.get.pixel 100-yy ,240,aa,rr,gg,bb %290
if gg<>0
  gr.modify txtc,"text","Crash!"            % goto crash
  gr.modify ship1,"x",100
  gr.render
  pause 500   %pause time to read screen
  spd=1           %reduce speed after crash
  gr.modify txtc,"text",""
end if
end if                %end of collision code

gr.render
goto start

crash:
!crash code goes here
goto start
