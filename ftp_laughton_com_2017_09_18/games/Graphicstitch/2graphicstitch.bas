REM Start of BASIC! Program
gr.open 255,0,0,0
gr.orientation 1
gr.color 255,255,5,5,1
gr.text.size 30
gr.rotate.start 90,450,680
gr.text.draw txtc,450,680,""
gr.rotate.end

 gr.bitmap.create l1,140,800     % 1st bitmap screen
gr.bitmap.drawinto.start l1
gr.color 255,0,255,0,1
gr.line ls1 ,30,1,50,300
gr.line ls1 ,50,300,110,470
gr.line ls1 ,110,470,80,580
gr.line ls1 ,80,580,40,700
gr.line ls1 ,40,700,120,750
gr.line ls1 ,120,750,30,800
gr.bitmap.drawinto.end 

 gr.bitmap.create l2,140,800     %2nd bitmap screen
gr.bitmap.drawinto.start l2
 gr.color 255,0,255,0,1 
gr.line ls2 ,30,1,40,200
gr.line ls2 ,40,200,10,220
gr.line ls2 ,10,220,20,300
gr.line ls2 ,20,300,50,340
gr.line ls2 ,50,340,60,460
gr.line ls2 ,60,460,90,510
gr.line ls2 ,90,510,50,560
gr.line ls2 ,50,560,90,660
gr.line ls2 ,90,660,30,770
gr.line ls2 ,30,770,30,800
gr.bitmap.drawinto.end 

 gr.bitmap.create l3,140,800      %3rd bitmap screen
gr.bitmap.drawinto.start l3
 gr.color 255,0,255,0,1 
gr.line ls3 ,30,1,10,160
gr.line ls3 ,10,160,30,210
gr.line ls3 ,30,210,10,230
gr.line ls3 ,10,230,40,270
gr.line ls3 ,40,270,20,300
gr.line ls3 ,20,300,100,440
gr.line ls3 ,100,440,140,710
gr.line ls3 ,140,710,30,800
gr.bitmap.drawinto.end 


 gr.bitmap.create ship,12,40
gr.bitmap.drawinto.start ship
gr.color 255,0,150,250,1
gr.line L,12,4,12,9
gr.line L,11,4,11,9
gr.line L,10,0,10,12
gr.line L,9,0,9,12
gr.color 255,255,0,100,1
gr.line L,8,0,8,15
gr.line L,7,0,7,15
gr.line L,6,0,6,28
gr.line L,5,0,5,28
gr.line L,4,0,4,40
gr.color 255,255,255,255,1
gr.line L,3,0,3,40
gr.line L,2,4,2,20
gr.line L,1,4,1,20
gr.bitmap.drawinto.end 

 gr.bitmap.draw rd11,l1,0,0
 gr.bitmap.draw rd12,l2,0,800
 gr.bitmap.draw rd13,l3,0,1600

gr.circle ss,400,650,30                  % moon
gr.bitmap.draw ship1,ship,100,200
spd=1

start:

tt=tt+35+spd             %speed     35 = speed of screen movement

gr.modify rd11, "y",0-tt
gr.modify rd12,"y",800-tt
gr.modify rd13,"y",1600-tt
if tt>=1600
  gr.modify rd11,"y",2400-tt
  spd=spd+1                  % increase speed for every loop of bitmaps
end if

gr.modify ship1,"x",100-yy


 gr.touch flag ,zx,zy         %checks for up down of ship
 if zx< 240 &flag
  yy=yy+20
  if yy>60 then yy=60
   else if zx>240&flag 
    yy=yy-20
  if yy<-340 then yy=-340
endif

if tt>= 2400 then tt =0     % starts bitmap draw from begining

! checkcollision:         collision does not always work because of 35 pixel movement
gr.get.pixel 100-yy ,290,aa,rr,gg,bb %290
if gg<>0
 gr.modify txtc,"text","Crash!"  
% goto crash                       !unrem if you have crash code
  gr.render
  pause 500
  gr.modify txtc,"text",""       % clears crash text
end if
gr.render
goto start

crash:
! where crash code goes
goto start
