REM Start of BASIC! Program
! no check for collisions
! touch top or bottom of screen to move ship
! Open graphics
gr.open 255,0,0,0
gr.orientation 1

gr.bitmap.load scrn1,"s1.jpg"
gr.bitmap.load scrn2,"s2.jpg"
gr.bitmap.load scrn3,"s3.jpg"

 gr.bitmap.load blp1,"blp1r.jpg"
 gr.bitmap.load blp2,"blp2r.jpg"
 gr.bitmap.load blp3,"blp3r.jpg" 

gr.bitmap.draw scrn11,scrn1,0,0
gr.bitmap.draw scrn12,scrn2,0,800
gr.bitmap.draw scrn13,scrn3,0,1600

 gr.bitmap.draw b2,blp1,100,200 
 gr.bitmap.draw b3,blp2,100,200 
 gr.bitmap.draw b4,blp3,100,200 

Start:
tt=tt+35      % speed

gr.modify scrn11, "y",0-tt
gr.modify scrn12,"y",800-tt
gr.modify scrn13,"y",1600-tt
if tt>=1600 then gr.modify scrn11,"y",2400-tt

 if cy= 1        % switches between 1 of 3 ships
 gr.show b2
 gr.modify b2,"x",100-yy
 gr.hide b4
 cy=2
 else if cy= 2 
 gr.show b3
  gr.modify b3,"x",100-yy
  gr.hide b2
  cy=3
 else 
 gr.show b4
  gr.modify b4,"x",100-yy
  gr.hide b3
  cy=1
  endif

 gr.touch flag ,zx,zy          %ship up or down
if zx< 240 &flag
yy=yy+20
if yy>60 then yy=60
else if zx>240&flag 
yy=yy-20
if yy<-340 then yy=-340
endif

if tt>= 2400 then tt =0       % checks to see if screen needs to restart

gr.render

goto Start

