Rem Picture Slide
Rem With RFO Basic!
Rem November 2015
Rem Version 1.00
Rem By Roy Shepherd

di_height = 672 % set to my Device
di_width = 1152

gr.open 
gr.orientation 0 % Landscape
pause 1000
WakeLock 3 
gr.text.size 40

gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y

bundle.create global % 'global' bundle pointer = #1

gosub Functions % Let basic see the function before they are called
!-------------------------------------------------
! Global variables
!-------------------------------------------------
setg("Divice_W", di_width)
setg("Divice_H", di_height)
setg("Scale_X", scale_x) 
setg("Scale_Y", scale_y)

!-------------------------------------------------
! Global arrays
!-------------------------------------------------
dim pic[16], picNum[16]

!-------------------------------------------------
! Call once 
!-------------------------------------------------
call ShowBackground()
call Initialise(picNum[])
gosub LoadPictures 

!-------------------------------------------------
! Main loop
!-------------------------------------------------
do
	gosub SelectPicture
	call ScramblePuzzle(pic[], picNum[],thisPicture$)
	call DoPuzzle(pic[], picNum[])
until 0

!-------------------------------------------------
! Back key for menu
!-------------------------------------------------
onBackKey: 
	call Menu(pic[], picNum[])
back.resume
end

!-------------------------------------------------
! Gosubs
!-------------------------------------------------
!-------------------------------------------------
! Load the pictures 
!-------------------------------------------------
LoadPictures:
	myPic = 2 : play = 0
	path$="../../PictureSlide/pictures/"
	file.dir path$, drawing$[]
	array.length numOfPictures, drawing$[]
	if numOfPictures=1 & !len(drawing$[1]) then Dialog.Message , "No Pictures", ok, "OK" : end
	dim GetNumOfPic[numOfPictures]
	for x = 1 to numOfPictures 
		gr.bitmap.load GetNumOfPic[x] ,path$ + drawing$[x]
		gr.bitmap.scale GetNumOfPic[x], GetNumOfPic[x], 445,385
		gr.bitmap.draw GetNumOfPic[x], GetNumOfPic[x],15, 15
		gr.hide GetNumOfPic[x]
	next
	gr.show GetNumOfPic[myPic]
	gr.render
return 

!-------------------------------------------------
! User selects a pictures 
!-------------------------------------------------
SelectPicture:
	gr.modify getg("PlayModNum"), "text", "Play" : gr.render
	myPic = 2 : play = 0
	setg("Trys", 0) : call Trys(0)
	for x = 1 to 16 : picNum[x] = x : next
	for x = 1 to numOfPictures : gr.hide GetNumOfPic[x] : next :  gr.show GetNumOfPic[myPic] :gr.render
	do
		oldPic = myPic
		do : gr.touch t,tx,ty : until t 
		call Buzz()
		do : gr.touch t,tx,ty : until ! t 
		tx/=getg("Scale_X") : ty/=getg("Scale_Y")
		if tx > 34 & tx < 435 & ty > 410 & ty < 460 then call PlaySound(getg("Button"))
		if tx > 34 & tx < 110 & ty > 410 & ty < 460 then myPic-- % Left Arrow
		if tx > 350 & tx < 435 & ty > 410 & ty < 460 then myPic++ % Right Arrow
		if tx > 120 & tx < 350 & ty > 410 & ty < 460 then play = 1 % Play tapped
		
		if myPic < 1 then myPic = numOfPictures
		if myPic > numOfPictures then myPic = 1
		
		gr.hide GetNumOfPic[oldPic]
		gr.show GetNumOfPic[myPic]
		gr.render
	until play
	gr.modify getg("PlayModNum"), "text", "New": gr.render
	thisPicture$ = drawing$[myPic]
return

!-------------------------------------------------
! Functions
!-------------------------------------------------
Functions: % Let basic see the function before they are called

!-------------------------------------------------
! Do this stuff once
!-------------------------------------------------
fn.def Initialise(picNum[])
	dataPath$="../../PictureSlide/data/"
	setg("Sound", 1) : setg("Buzz", 1)
	setg("Best", 0) : setg("Trys", 0)
	call LoadBest()
	call ReadHelp()
	for x = 1 to 16 
		picNum[x] = x
	next
	gr.modify getg("TryModNum"), "text", "Trys: "+int$(getg("Trys"))
	gr.modify getg("BestModNum"), "text", "Best: " +int$(getg("Best"))
	
	! Load sounds and give each sound a global name
	Soundpool.open 5
	Soundpool.load  error, dataPath$ + "error.wav"        : setg("Error", error)
	Soundpool.load  button, dataPath$ + "buttonClick.wav" : setg("Button", button)
	Soundpool.load  newBest, dataPath$ + "newBest.wav"    : setg("NewBest", newBest)
	Soundpool.load  swapTile, dataPath$ + "swaping.wav"   : setg("SwapTile", swapTile)
	Soundpool.load  goodBye, dataPath$ + "goodbye.wav"    : setg("GoodBye", goodBye)
	
fn.end

!------------------------------------------------
! if sound is on. Play sound (ptr)
!------------------------------------------------
fn.def PlaySound(ptr)
	if getg("Sound") then Soundpool.play s,ptr,0.99,0.99,1,0,1
fn.end
!-------------------------------------------------
! Read the help from read.data and make it global. 
!-------------------------------------------------
fn.def ReadHelp()
	read.from 1
	while h$ <> "STOP"
		read.next h$ 
		if h$ <> "STOP" then help$ = help$ + h$
	repeat
	setg$("Help", help$)
fn.end

!-------------------------------------------------
! Load and display the background
!-------------------------------------------------
fn.def ShowBackground()
	dataPath$="../../PictureSlide/data/"
	gr.bitmap.load bg,dataPath$ + "PicBackground2.png"
	if bg < 0 then dialog.message "Back Ground Image, Not Found",,ok,"OK" : end
	gr.bitmap.scale bg, bg, getg("Divice_W"), getg("Divice_H") % scale background
	gr.bitmap.draw bg,bg, 0, 0 
	gr.bitmap.delete bg  
	
	gr.text.draw tryModNum, 60, 560,"Trys: "  : setg("TryModNum", tryModNum)
	gr.text.draw bestModNum, 60, 610,"Best:"  : setg("BestModNum", bestModNum)
	gr.text.draw playModNum, 190, 450, "Play" : setg("PlayModNum",playModNum)
	gr.text.draw pleaseWait,540, 300, "Please Wait Shuffling Tiles"
	setg("PleaseWait", pleaseWait)
	gr.hide pleaseWait
	gr.render
fn.end
 
!-------------------------------------------------
! Crop the picture into 16 tiles then scramble the tiles.
!-------------------------------------------------
 fn.def ScramblePuzzle(pic[], picNum[], thisPicture$)
	gr.show getg("PleaseWait")
	gr.render : p = 1
	path$="../../PictureSlide/pictures/"
	gr.bitmap.load pic ,path$ + thisPicture$
	gr.bitmap.scale pic,pic,670, 650
	gr.bitmap.size pic, width, height
	setg("PicWidth", width) : setg("PicHeight", height)
	
	for y = 0 to height - (height / 4) step height / 4
		for x = 0 to width - (width / 4) step width / 4
			gr.bitmap.crop pic[p],pic, x, y, width / 4, height / 4
			p++
		next
	next
	
	setg("FirstPic", pic[1]) 
	setg("LastPic", pic[16])
	first = getg("FirstPic") : last = getg("LastPic")
	do
		flag = 0 : wellScrambled = 0
		for s = 1 to 16 
			if pic[s] = last then spc = s
		next
		! Look to see if legal move
		x = floor(rnd() * 4) + 1
		y = floor(rnd() * 4) + 1
		num = x 
		if y = 2 : num += 4
			elseif y = 3 : num += 8 
			elseif y = 4 : num += 12
		endif
		if x < 4 & num + 1 = spc then flag = 1
		if x > 1 & num - 1 = spc then flag = 1
		if num + 4 = spc then flag = 1
		if num - 4 = spc then flag = 1
		if flag then swap pic[num], pic[spc] : swap picNum[num], picNum[spc]
		
		p = 1 
		for s = first to last
			if pic[p] = s then wellScrambled++
			p++
		next
		
	until wellScrambled < 2
	
	gr.bitmap.delete pic 
	gr.hide getg("PleaseWait")
	gr.render
 fn.end
 
!-------------------------------------------------
! User to unscramble the tiles to reveal the original picture.
!-------------------------------------------------
 fn.def DoPuzzle(pic[], picNum[])
	scale_x = getg("Scale_X") : scale_y = getg("Scale_Y")
	height = getg("PicHeight") : width = getg("PicWidth") 
	last = getg("LastPic") : newGame = 0
		p = 1
		for y = 0 to height - (height / 4) step height / 4
			for x = 0 to width - (width / 4) step width / 4
				if pic[p] <> last then gr.bitmap.draw pic[p],  pic[p], x + 475, y + 10
				p++
			next
		next
		
		gr.render
		
		do
			do 
				do : gr.touch t,tx,ty : until t 
				call Buzz()
				do : gr.touch t,tx,ty : until ! t 
				tx/=scale_x : ty/=scale_y
				if tx > 120 & tx < 350 & ty > 410 & ty < 460 then newGame = 1 % New tapped
				tx - = 480 : tx / = (width/4) : tx++
				ty -= 10 : ty/=(height/4) : ty+=1
				tx = int(tx) : ty = int(ty)
			until tx > 0 & tx < 5 & ty > 0 & ty < 5 | newGame
	
			if ! newGame then 
				num = tx 
				if ty = 2 : num += 4
					elseif ty = 3 : num += 8 
					elseif ty = 4 : num += 12
				endif
				if pic[num] <> last then call SwapTiles(pic[], picNum[], num, tx) : else call PlaySound(getg("Error"))
			endif
			
		until PuzzleIsDone(picNum[]) | newGame
		if ! newGame then
			if Best() then 
				m$ = "A New Best\nAnother Game"
				call PlaySound(getg("NewBest"))
			else
				m$ = "Another Game"
			endif
			do : dialog.message "Game over",m$,yn,"Yes","No" : until yn > 0
			if yn = 2 then call GoodBye()
		endif
		
		for x= 1 to 16
			if pic[x] <> last then gr.hide pic[x] 
		next 
		gr.render
 fn.end
 
!-------------------------------------------------
! Check move is legal then swap the tiles
!-------------------------------------------------
fn.def SwapTiles(pic[],picNum[], num, x)
	last = getg("LastPic") : flag = 0
	for s = 1 to 16 
		if pic[s] = last then spc = s
	next
	! Look to see if legal move
	if x < 4 & num + 1 = spc then flag = 1
	if x > 1 & num - 1 = spc then flag = 1
	if num + 4 = spc then flag = 1
	if num - 4 = spc then flag = 1
	if ! flag then call PlaySound(getg("Error"))
	
	if flag then 
		PlaySound(getg("SwapTile"))
		swap pic[num], pic[spc] : swap picNum[num], picNum[spc] : p = 1
		height = getg("PicHeight") : width = getg("PicWidth") : p = 1
		for y = 0 to height - (height / 4) step height / 4
			for x = 0 to width - (width / 4) step width / 4
				if pic[p] <> last then gr.modify pic[p], "x", x + 475, "y", y + 10
				p++
			next
		next
	call Trys(1)
	gr.render
	endif 
fn.end 

!-------------------------------------------------
! Check to see if puzzel is over
!-------------------------------------------------
fn.def PuzzleIsDone(picNum[])
	flag = 1
	for p = 1 to 16
		if picNum[p] <> p then flag = 0 : f_n.break
	next
	fn.rtn flag
fn.end

!-------------------------------------------------
! Record and display the lowest trys ever to complete the puzzle.
!------------------------------------------------- 
fn.def best()
	flag = 0
	trys = getg("Trys")
	best = getg("Best")
	if best = 0 then best = trys : flag = 1
	if trys < best then best = trys : flag = 1
	if flag
		setg("Best", best)
		gr.modify getg("BestModNum"), "text", "Best: "+int$(getg("Best"))
		gr.render
	endif 
	fn.rtn flag
fn.end

!-------------------------------------------------
! Record and display the number of trys to complete the puzzle.
!-------------------------------------------------
fn.def Trys(t)
	try = getg("Trys")
	try += t 
	setg("Trys", try) 
	gr.modify getg("TryModNum"), "text", "Trys: "+int$(getg("Trys"))
fn.end

!-------------------------------------------------
! if buzz is on then vibrate when user taps screen
!-------------------------------------------------
fn.def Buzz()
	if getg("Buzz") then 
		array.load buzzGame[], 1, 100
		vibrate buzzGame[], -1
		array.delete buzzGame[]
	endif
fn.end

!-------------------------------------------------
! Loads lowest trys, sound and vibration
!-------------------------------------------------
fn.def LoadBest()
	dataPath$="../../PictureSlide/data/"
	file.exists BestPresent,dataPath$+"PictureBest.txt"
	if BestPresent then
		text.open r,hs,dataPath$+"PictureBest.txt"
			text.readln hs,best$
			best = val(best$)
			
			text.readln hs,best$
			sound = val(best$)
			
			text.readln hs,best$
			vibration = val(best$)
			
			setg("Best", best)
			setg("Sound", sound)
			setg("Buzz", vibration)
		text.close hs
	endif
fn.end

!-------------------------------------------------
! Saves lowest trys, sound and vibration
!-------------------------------------------------
fn.def SaveBest()
	dataPath$="../../PictureSlide/data/"
	best = getg("Best")
	sound = getg("Sound")
	vibration = getg("Buzz")
	file.exists BestPresent,dataPath$+"PictureBest.txt"
	if ! BestPresent then file.mkdir dataPath$
	text.open w,hs,dataPath$+"PictureBest.txt"
		text.writeln hs,int$(best)
		text.writeln hs,int$(sound)
		text.writeln hs,int$(vibration)
	text.close hs
fn.end

!-------------------------------------------------
! Saves lowest trys, sound and vibration and leave the puzzle
!-------------------------------------------------
fn.def GoodBye()
	call SaveBest()
	call PlaySound(getg("GoodBye"))
	gr.color 50, 0, 0, 0, 1
	gr.rect fade, 0, 0, getg("Divice_W"), getg("Divice_H")
	for a = 60 to 250 step 10 
		gr.modify fade, "alpha", a
		pause 50
		gr.render
	next
	gr.color 0,255,255,255,1 
	gr.text.size 200 
	gr.text.draw bye, 100, 400, "Good Bye" 
	for a = 1 to 255 step 5
		gr.modify bye, "alpha", a
		pause 50
		gr.render
	next
	pause 1000
	soundpool.release
	exit
fn.end

!-------------------------------------------------
! Back key tapped. Call the menu
!-------------------------------------------------
fn.def Menu(pic[], picNum[])
	sound = getg("Sound")
	vibration = getg("Buzz")
	if sound then sound$="Sound On" else sound$="Sound Off"
	if vibration then vibration$="Vibration On" else vibration$="Vibration Off"
	
	array.load menu$[],	sound$,~
						vibration$,~
						"Reset Best Score",~
						"About",~
						"Help",~
						"Back to Game",~
						"Exit"
	dialog.select menuSelected, menu$[], "Main"
	
	sw.begin menuSelected
		sw.case 1
			if sound then sound = 0 else sound = 1	
			setg("Sound", sound)
		sw.break
			
		sw.case 2
			if vibration then vibration = 0 else vibration = 1
			setg("Buzz", vibration)
		Sw.break
		
		sw.case 3
			do : dialog.Message"Reset Best Score to zero?",,yn,"Yes","No" : until yn > 0
			if yn = 1 then 
				setg("Best", 0)
				call SaveBest()
				call Best()
			endif
		sw.break
		
		sw.case 4
			Dialog.Message " About:\n\n",~
			"   Picture Slide for Android\n\n" + ~
			"   With: RFO BASIC!\n\n" + ~
			"   November 2015\n\n" + ~
			"   Version: 1.00\n\n" + ~ 
			"   Author: Roy Shepherd\n\n", ~
			ok, "OK"
		sw.break
		
		sw.case 5
			help$ = getg$("Help") 
			dialog.Message " Picture Slide Help:",help$,ok,"OK"
		sw.break
		
		sw.case 6
			! Back to game
		sw.break
			
		sw.case 7
			do : dialog.Message"Exit Game?",,yn,"Yes","No" : until yn > 0
			if yn=1 then call GoodBye()
		sw.break
	sw.end
fn.end

!----------------------------------------------------
! These functions give simulate Global variables
! Thanks to Mougino for the functions
!----------------------------------------------------
FN.DEF SETG(var$, n) % Set global-numeric-variable
  BUNDLE.PUT 1, var$, STR$(n)
FN.END

FN.DEF SETG$(var$, s$) % Set string-numeric-variable
  BUNDLE.PUT 1, var$, s$
FN.END

FN.DEF GETG(var$) % Get global-numeric-variable
  BUNDLE.GET 1, var$, s$
  FN.RTN VAL(s$)
FN.END

FN.DEF GETG$(var$) % Get string-numeric-variable
  BUNDLE.GET 1, var$, s$
  FN.RTN s$
FN.END

return

!------------------------------------------------
! Help data for the puzzle.
!------------------------------------------------
read.data ""
read.data "Playing Picture Slide:\n\n"
read.data "Picture Slide is a sliding puzzle game.\n"
read.data "Tap the arrows to cycle through the pictures and tap 'Play' too shuffle the selected picture."
read.data "The picture will be divided into fifteen tiles and placed in random order with one tile missing."
read.data "The object of this game is to place the tiles in order by tapping tiles next to the empty space to swap them over.\n\n"
read.data "Try solving the puzzle in the less number of trys.\n\n"
read.data "Once you have tapped 'Play' it changes to 'New', you can tap 'New' at any time to select a different picture.\n"
read.data "\n"
read.data "Menu:\n"
read.data "     Tap the Back Key to access the Menu.\n"
read.data "     From the menu, you can:\n"
read.data "     Turn Sound On and Off.\n"
read.data "     Turn Vibration On and Off.\n"
read.data "     Reset the Best Score to zero.\n"
read.data "     Read About and Help.\n"
read.data "     Go back to the game.\n"
read.data "     Exit the game.\n"
read.data "\n"
read.data "	------------------------------------------------------------"
read.data "STOP"