!
! SpaceMax on Android V1.0
! With RFO Basic!
! space.max@free.fr
! Version 3.0 released at 20/10/2014
!

! -------------------------------------------------------------------------------------
! -- User Functions Déclaration
! -------------------------------------------------------------------------------------
Debug.on

fn.def fps(time0,time1,framerate)
if framerate = 0 then
	framerate = 1
end if
laps = time1 - time0
ttw =  (1000 /  framerate ) - laps
if ttw >0 then pause  ttw

fn.rtn int((framerate/laps)*framerate)
fn.end

fn.def gr_color(color$)

sw.begin color$
  sw.case "white"
    gr.color 255,255,255,255,0
    sw.break
  sw.case "blue"
    gr.color 255,0,0,255,0
    sw.break
  sw.case "mediumblue"
    gr.color 255,205,0,0,0
    sw.break
  sw.case "royalblue"
    gr.color 255,65,105,225,0
    sw.break
  sw.case "yellow"
    gr.color 255,255,255,0,0
    sw.break
  sw.case "gold"
    gr.color 255,255,215,0,0
    sw.break
  sw.case "lime"
    gr.color 255,0,255,0,0
    sw.break
sw.case "springgreen"
    gr.color 255,0,250,154,0
    sw.break
  sw.case "red"
    gr.color 255,255,0,0,0
    sw.break
  sw.case "crimson" % rouge pale
    gr.color 255,220,20,60,0
    sw.break
sw.end
fn.end

! -------------------------------------------------------------------------------------
! -- Set the display
! -------------------------------------------------------------------------------------
start:
screenw=480
screenh=320

! -- Open the display in black mode
gr.open 0,0,0,0
! -- Set landscape mode
! -- gr.orientation 0 
gr.color 255,255,0,0,0
gr.screen actual_screenw,actual_screenh
gr.set.AntiAlias 1

! -- Scale for independent  device
x_scale = actual_screenw/screenw
y_scale = actual_screenh/screenh

gr.scale x_scale , y_scale

! -------------------------------------------------------------------------------------
! -- Variable Déclaration
! -------------------------------------------------------------------------------------

title$="Spacemax V3.0"
email$="space.max@free.fr"
path$="."

! -- Framerate per second
fpsec = 20

! -- start:
life=3
level=1
power=1
score=0
game_end=0

! -------------------------------------------------------------------------------------
! -- Alien section
! -------------------------------------------------------------------------------------

! -- Here to split the screen in order to put alien without intersection
number_of_position_per_line = (actual_screenw / x_scale)  / 32  - 1

! -- This array will store all the position of each alien when they will be created
dim array_pos[number_of_position_per_line]

! -- The minimum laps before an alien can shoot
alien_min_laps_before_shoot = 3000

! -- The random time added to the previous variable
alien_rnd_laps_shoot = 3000

!-- Alien's images bitmap
dim alien_img[10]
! -- the max alien to display
max_alien = 5

!-- Create the list of pointer's bundle_alien
list.create N,list_bundle_alien

! -------------------------------------------------------------------------------------
! -- Bullet section
! -------------------------------------------------------------------------------------

! -- Create the list of pointer bundle_bullet
list.create N,list_bundle_bullet_ship

! -- bullets for the ship
nb_bullet = 0
dim bullet_img[5]

! -- Bullets for the aliens
dim bullet_alien_img[5]
list.create N,list_bundle_alien_shoot

! -------------------------------------------------------------------------------------
! -- Ship section
! -------------------------------------------------------------------------------------

dim ship_img[18]

!-- Create the bundle for the ship
bundle.create ptr_to_bundle_ship
bundle.put ptr_to_bundle_ship, "ship_posx", ( screenw   / 2 ) - 16
bundle.put ptr_to_bundle_ship, "ship_posy", ( screenh  )  -32
bundle.put ptr_to_bundle_ship, "index_img", 1
bundle.put ptr_to_bundle_ship,"ship_touched",0
bundle.put ptr_to_bundle_ship,"ship_invincibility",0

! -------------------------------------------------------------------------------------
! -- Load sounds
! -------------------------------------------------------------------------------------
Soundpool.open 10
soundpool.load ship_explose,  "sounds/explode.ogg"
soundpool.load alien_explose,  "sounds/expl1.ogg"

! -------------------------------------------------------------------------------------
! -- Load images 
! -------------------------------------------------------------------------------------

! -- Background for presentation
gr.bitmap.load bg01,  "images/spacemax/bg01.png" 

! -- Aliens
gr.bitmap.load alien_img[1],  "images/spacemax/space1.1.png" 
gr.bitmap.load alien_img[2],  "images/spacemax/space1.2.png" 
gr.bitmap.load alien_img[3],  "images/spacemax/space1.3.png" 
gr.bitmap.load alien_img[4],  "images/spacemax/space1.4.png" 
gr.bitmap.load alien_img[5],  "images/spacemax/space1.5.png" 

! -- Alien explosion
gr.bitmap.load alien_img[6],  "images/spacemax/alien_expl1.png" 
gr.bitmap.load alien_img[7],  "images/spacemax/alien_expl2.png" 
gr.bitmap.load alien_img[8],  "images/spacemax/alien_expl3.png" 
gr.bitmap.load alien_img[9],  "images/spacemax/alien_expl4.png" 
gr.bitmap.load alien_img[10],  "images/spacemax/alien_expl5.png" 

! -- Ship
gr.bitmap.load ship_img[1],  "images/spacemax/ship01.png" 
gr.bitmap.load ship_img[2],  "images/spacemax/ship02.png" 
gr.bitmap.load ship_img[3],  "images/spacemax/ship03.png" 
gr.bitmap.load ship_img[4],  "images/spacemax/ship04.png" 
gr.bitmap.load ship_img[5],  "images/spacemax/ship05.png" 

! -- Ship explosion
for i = 1 to 13
	gr.bitmap.load ship_img[i + 5],  "images/spacemax/ship_expl"  + int$(i) + ".png" 
next i
! -- Bullet for the ship
gr.bitmap.load bullet_img[1],  "images/spacemax/bullet01.png"
! -- Bullet for the aliens
gr.bitmap.load bullet_alien_img[1],  "images/spacemax/bullet_alien01.png"

gosub presentation
gr.cls
gr.text.align 1
first_touched = 0

! -----------------------------------------------------------------------------------
! -- Add ship object bitmap in the object list
! -----------------------------------------------------------------------------------

bundle.get ptr_to_bundle_ship,"ship_posx",ship_posx
bundle.get ptr_to_bundle_ship,"ship_posy",ship_posy
gr.bitmap.draw ptr_to_ship_bitmap,ship_img[1],ship_posx,ship_posy
! -----------------------------------------------------------------------------------
! --  Preparing the bullets for the ship
! -----------------------------------------------------------------------------------

line_board = 16
gosub board

! -------------------------------------------------------------------------------------
! -- This is the main loop
! -------------------------------------------------------------------------------------

do
	 
	nb_alien_to_shoot = 1
	 
	 do 

		! -----------------------------------------------------------------------------------
		! -- Create the bullet for the ship
		! ----------------------------------------------------------------------------------

		 for nb_bullet = 1 to 2 + nb_alien_to_shoot
			gosub create_bullet_ship
		 next nb_bullet
		 
		! -----------------------------------------------------------------------------------
		! -- Create the alien in regarding the level
		! ----------------------------------------------------------------------------------
		
		! -- Initialise array of position for the Aliens
		
		for posx = 1 to (number_of_position_per_line) 
		  array_pos [posx] = posx * 32
		next posx

		for nb_alien=1 to nb_alien_to_shoot
		  gosub create_alien
		  ! -- print "nb_alien";nb_alien
		next nb_alien
		  
		 ! -- the index of the active bullet for tjhe ship
		 index_bullet_ship = 0
		 ! -- gosub debug
		 do 
		    t0 = clock()
			! -----------------------------------------------------------------------------------
			! -- The information board
			! -----------------------------------------------------------------------------------
			
			gosub board_update
			
			! -- Test for the fisrt touch. Until it done, the ship don't move
			gr.touch touched,x,y  
			
			if touched then
			  first_touched = 1
			end if
			
			! -----------------------------------------------------------------------------------
			! -- Move the ship
			! -----------------------------------------------------------------------------------
			
			if x < ship_posx +16 & ship_posx > 0 & first_touched then
			  ship_posx - = 12
			elseif ( x > ship_posx + 32) & ship_posx < (actual_screenw / x_scale)  - 32 & first_touched  then
			  ship_posx + = 12
			  !print x;" ";ship_posx;" ",actual_screenw
			end if
			  
			bundle.put ptr_to_bundle_ship,"ship_posx",ship_posx
			gr.modify ptr_to_ship_bitmap,"x",ship_posx
			
			! -- Retrieve the number of bullet ship
			list.size list_bundle_bullet_ship, list_bundle_bullet_ship_size
			
			! -----------------------------------------------------------------------------------
			! -- Move the bullets of the ship
			! -----------------------------------------------------------------------------------
			
			if index_bullet_ship < list_bundle_bullet_ship_size  then
			  index_bullet_ship = index_bullet_ship + 1
			  gosub move_bullet_ship
			else
			  index_bullet_ship =  list_bundle_bullet_ship_size
			  gosub move_bullet_ship
			end if
			
			! -----------------------------------------------------------------------------------
			! -- Move the bullets of the aliens
			! -----------------------------------------------------------------------------------
			
			! -- Retrieve the number of bullet alien
			list.size list_bundle_alien_shoot, list_bundle_alien_shoot_size
			gosub move_shoot_alien
			
			! -- Retrieve the number of Alien created
			list.size list_bundle_alien, list_bundle_alien_size
			
			! -----------------------------------------------------------------------------------
			! -- Move the aliens
			! -----------------------------------------------------------------------------------
			
			index_alien=1
			 while index_alien <= list_bundle_alien_size
			  list.get list_bundle_alien,index_alien,ptr_to_bundle_alien
			  gosub move_alien
			  index_alien+=1
			repeat
			speed=fps(t0,clock(),fpsec)
			gr.render
		 until life =0 | game_end = 1  | list_bundle_alien_size = 0
			 
		nb_alien_to_shoot = nb_alien_to_shoot + 1
		! -- Hide the current bullet of the ship
		for bullet = 1 to list_bundle_bullet_ship_size
			list.get list_bundle_bullet_ship,bullet,ptr_to_bundle_bullet
			bundle.get ptr_to_bundle_bullet,"ptr_to_bullet_bitmap",ptr_to_bullet_bitmap
			gr.hide ptr_to_bullet_bitmap
		next nb_bullet
		list.clear list_bundle_bullet_ship
	until life = 0 | game_end = 1 | nb_alien_to_shoot  > max_alien
	level = level + 1
	game_end = 1
until life = 0 | game_end = 1 | level = 5
if life > 0 then
	popup "Congratualations, you have win",0,0,1
else 
	popup "Spacemax is stronger than you",0,0,1
end if

array.delete array_pos[],alien_img[],bullet_img[],bullet_alien_img[],ship_img[]

list.clear list_bundle_alien
list.clear list_bundle_bullet_ship
! --list.clear list_bundle_alien_shoot
bundle.clear ptr_to_bundle_ship
bundle.clear bundle_alien
bundle.clear bundle_alien_shoot
bundle.clear bundle_bullet

gr.close
goto start

end

!__________________________________________________________________________________________________________

! -------------------------------------------------------------------------------------
! -- The first screen Presentation
! -------------------------------------------------------------------------------------

presentation:

do
  gr.bitmap.draw bg01,bg01,0,0
  
  a:
  a=int(rnd()*255)
  if a<125 then goto a
  b:
  b=int(rnd()*255)
  if b<125 then goto b
  c:
  c=int(rnd()*255)
  if a<125 then goto c
  
  gr.text.size 40
  gr.color 255,b,c,d,0
  
  gr.text.align 2
  gr.text.draw title,screenw/2,220,title$
  !gr.modify testid,"y",226
  gr.text.size 20
  gr.color 255,255,255,255,0
  gr.text.draw email,screenw/2,260,email$
  gr.text.size 14
  gr.color 255,255,255,0,0
  gr.text.draw email,screenw/2,300,"Touch the screen to start"
  gr.render
  pause 500
  gr.cls
  Gr.touch touched,x,y
until touched
!--gr.bitmap.delete bg01 % to free memory
!print "You have touched the screen at",x,y

return

!__________________________________________________________________________________________________________

create_alien:
! --------------------------------------------------------------------------
! -- Create an alien and add it into list_bundle_alien
! --------------------------------------------------------------------------

! -- Add object bitmap in the object list
gr.bitmap.draw ptr_to_alien_bitmap,alien_img[1],0,0

bundle.create bundle_alien

! -- random selection for position without intersection
pos:
pos = int(rnd()*number_of_position_per_line) + 1
if array_pos[ pos ] = 0 then goto pos
posx = array_pos [ pos ]
array_pos [ pos] = 0

bundle.put bundle_alien,"posx",posx
bundle.put bundle_alien,"posy",int(rnd()* (actual_screenh / y_scale) /2)+32
let rnd = int(rnd()*2)+1
if rnd = 1 then
  bundle.put bundle_alien,"sensx",1
else
  bundle.put bundle_alien,"sensx",-1
end if

bundle.put bundle_alien,"index_img",0
bundle.put bundle_alien,"touched",0
! -- laps before shooting
bundle.put bundle_alien, "laps", int(rnd() * alien_rnd_laps_shoot ) + alien_min_laps_before_shoot
! -- the date of born !
bundle.put bundle_alien, "clock",clock()

! --------------------------------------------------------------------------
! -- Add the ptr_to_alien_bitmap in the bundle
! --------------------------------------------------------------------------

bundle.put bundle_alien,"ptr_to_alien_bitmap",ptr_to_alien_bitmap

! -- Add the pointer bundle_alien to bunble_list
list.add list_bundle_alien,bundle_alien

return

!__________________________________________________________________________________________________________

move_alien:
! --------------------------------------------------------------------------
! -- Move an alien in using the pointer to bundle_alien
! --------------------------------------------------------------------------

bundle.get ptr_to_bundle_alien, "posx", posx
bundle.get ptr_to_bundle_alien, "posy", posy
bundle.get ptr_to_bundle_alien, "sensx", sensx
bundle.get ptr_to_bundle_alien, "index_img", index_img
bundle.get ptr_to_bundle_alien, "touched", alien_touched
bundle.get ptr_to_bundle_alien, "laps", laps
bundle.get ptr_to_bundle_alien, "clock", clock

! -- handle the movement
if posx > (actual_screenw  / x_scale) -32  | posx<0 then
  sensx*=-1
end if
posx+=sensx * 8

! --------------------------------------------------------------------------
! Update all the position attribute  stored in the bundle
! --------------------------------------------------------------------------

bundle.put ptr_to_bundle_alien, "posx",posx
bundle.put ptr_to_bundle_alien, "posy",posy
bundle.put ptr_to_bundle_alien, "sensx",sensx


! -- Retrieve the ptr_to_alien_bitmap stored in the bundle
bundle.get ptr_to_bundle_alien, "ptr_to_alien_bitmap",ptr_to_alien_bitmap

! --------------------------------------------------------------------------
! -- Alien is alive
! --------------------------------------------------------------------------
if alien_touched = 0 then
	! -- Move the coordonate of the bitmap
	gr.modify  ptr_to_alien_bitmap,"x",posx
	gr.modify  ptr_to_alien_bitmap,"y",posy

	! -- Rotate the image for giving an movement
	! -- Image 1 to 5 : Alien alive
	! -- Image 6 to 6 : Alien explosion

	if index_img < 5 then
	  index_img+=1
	  gr.modify ptr_to_alien_bitmap, "bitmap", alien_img[index_img]
	  bundle.put ptr_to_bundle_alien, "index_img",index_img
	else
	  index_img=1.0
	  bundle.put ptr_to_bundle_alien, "index_img",index_img
	end if


	! --------------------------------------------------------------------------
	! -- Making Alien's shoot
	! --------------------------------------------------------------------------

	if  clock() - clock > laps then
	  gr.bitmap.draw ptr_to_alien_bullet_img,bullet_alien_img[1],-10,-10 % not visiible here....
	  
	  bundle.create bundle_alien_shoot
	  bundle.put bundle_alien_shoot,"posx_shoot", posx + 16
	  bundle.put bundle_alien_shoot,"posy_shoot", posy + 40
	  bundle.put bundle_alien_shoot,"ptr_to_alien_bullet_img", ptr_to_alien_bullet_img
	  
	  ! -- Add bundle into the list
	  list.add list_bundle_alien_shoot, bundle_alien_shoot
	  bundle.put ptr_to_bundle_alien,"clock",clock()
	end if
else
! --------------------------------------------------------------------------
! -- Alien is dead and explode
! --------------------------------------------------------------------------
	if index_img < 10 then
		  index_img+=1
		  gr.modify ptr_to_alien_bitmap, "bitmap", alien_img[index_img]
		  bundle.put ptr_to_bundle_alien, "index_img",index_img
	else
		list.remove list_bundle_alien, index_alien
		list.size  list_bundle_alien , list_bundle_alien_size
		gr.hide ptr_to_alien_bitmap
	end if
end if

return


!__________________________________________________________________________________________________________
! ----------------------------------------------------------------------------------------------
! -- Create an bullet for the ship  and add it into bundle_bullet_list
! ----------------------------------------------------------------------------------------------
create_bullet_ship:

! -- Retrieve the ship's position

bundle.get ptr_to_bundle_ship,"ship_posx",ship_posx 
bundle.get ptr_to_bundle_ship,"ship_posy",ship_posy 

bundle.create bundle_bullet
bullet_posx = ship_posx + 14 % Center to ship
bundle.put bundle_bullet,"bullet_posx",bullet_posx 

bullet_posy = ship_posy + 7
bundle.put bundle_bullet,"bullet_posy",bullet_posy

bundle.put bundle_bullet,"bullet_index",nb_bullet 


! -- Add object bitmap in the object list
gr.bitmap.draw ptr_to_bullet_bitmap,bullet_img[1],bullet_posx , bullet_posy 

! --------------------------------------------------------------------------
! -- Add the pointer_to_bullet_bitmap in the bundle
! --------------------------------------------------------------------------
bundle.put bundle_bullet,"ptr_to_bullet_bitmap",ptr_to_bullet_bitmap
list.add list_bundle_bullet_ship, bundle_bullet

return

!__________________________________________________________________________________________________________
move_bullet_ship:
! --------------------------------------------------------------------------
! -- Move an bullet and add it into bundle_bullet_list
! --------------------------------------------------------------------------

for i = 1 to index_bullet_ship

  list.get list_bundle_bullet_ship, i, ptr_to_bundle_bullet
  bundle.get ptr_to_bundle_bullet, "bullet_posx",bullet_posx
  bundle.get ptr_to_bundle_bullet, "bullet_posy",bullet_posy
  bundle.get ptr_to_bundle_bullet, "bullet_index",bullet_index
  bundle.get ptr_to_bundle_bullet,"ptr_to_bullet_bitmap",ptr_to_bullet_bitmap

  if bullet_posy >= 32 then

    bullet_posy = bullet_posy - 16
    bundle.put ptr_to_bundle_bullet, "bullet_posy",bullet_posy
    gr.modify ptr_to_bullet_bitmap,"y",bullet_posy
    
  else

    bundle.get ptr_to_bundle_ship,"ship_posx",ship_posx 
    bundle.get ptr_to_bundle_ship,"ship_posy",ship_posy
    
    bullet_posx = ship_posx + 14 % Center to ship
    bullet_posy = ship_posy + 7

    gr.modify ptr_to_bullet_bitmap,"x",bullet_posx 
    gr.modify ptr_to_bullet_bitmap,"y",bullet_posy
    
    bundle.put ptr_to_bundle_bullet, "bullet_posx",bullet_posx
    bundle.put ptr_to_bundle_bullet, "bullet_posy",bullet_posy

  end if
  
  ! -- Test collision with alien
  j = 1
  list.size  list_bundle_alien , list_bundle_alien_size
  while j <= list_bundle_alien_size
	list.get list_bundle_alien, j, ptr_to_bundle_alien
	bundle.get ptr_to_bundle_alien , "ptr_to_alien_bitmap", ptr_to_alien_bitmap
   bundle.get ptr_to_bundle_alien,"touched",alien_touched
	if alien_touched = 0 then
		target_touched = gr_collision (ptr_to_bullet_bitmap, ptr_to_alien_bitmap)
		if target_touched = 1 then
			soundpool.play sound2,alien_explose,0.9, 0.9, 1, 0, 1
			! -- Update alien's attributes
			bundle.put ptr_to_bundle_alien , "touched", 1
			bundle.put ptr_to_bundle_alien , "index_img", 5
			score = score + 1 * level
		end if
   end if
	j = j + 1
repeat

next i

return

!__________________________________________________________________________________________________________
move_shoot_alien:
! -----------------------------------------------------------------------------------
! -- Handle the alien's shoot
! -----------------------------------------------------------------------------------

list.size list_bundle_alien_shoot, list_bundle_alien_shoot_size
index_shoot_alien = 1
while index_shoot_alien <= list_bundle_alien_shoot_size
	list.get list_bundle_alien_shoot,index_shoot_alien, ptr_to_bundle_alien_shoot
	bundle.get ptr_to_bundle_alien_shoot, "posx_shoot", posx_shoot
	bundle.get ptr_to_bundle_alien_shoot, "posy_shoot", posy_shoot
	bundle.get ptr_to_bundle_alien_shoot, "ptr_to_alien_bullet_img", ptr_to_alien_bullet_img

	if posy _shoot < (actual_screenh / y_scale) then
		posy_shoot = posy_shoot + 8
		gr.modify ptr_to_alien_bullet_img,"x", posx_shoot
		gr.modify ptr_to_alien_bullet_img,"y", posy_shoot
		bundle.put ptr_to_bundle_alien_shoot,"posy_shoot", posy_shoot
		
		! -- Test collision with Ship only if ship is not touched !
		
		bundle.get ptr_to_bundle_ship,"ship_touched",ship_touched
		if ship_touched = 0 then
			ship_touched = gr_collision (ptr_to_alien_bullet_img, ptr_to_ship_bitmap)
			if ship_touched = 1 then
				bundle.put ptr_to_bundle_ship,"ship_touched",ship_touched
				! -- gr.modify ptr_to_bundle_alien_shoot,"y",actual_screenh + 16 ici
				soundpool.play sound1,ship_explose,0.9, 0.9, 1, 0, 1
				life-=1
				ship_touched = 0
				bundle.put ptr_to_bundle_ship,"ship_touched",ship_touched
				gr.hide ptr_to_alien_bullet_img
				list.remove list_bundle_alien_shoot, index_shoot_alien
				list.size list_bundle_alien_shoot, list_bundle_alien_shoot_size
			end if
		end if
	else
		list.remove list_bundle_alien_shoot, index_shoot_alien
		list.size list_bundle_alien_shoot, list_bundle_alien_shoot_size
	end if
	index_shoot_alien + = 1
repeat

return

!__________________________________________________________________________________________________________

board:
! -----------------------------------------------------------------------------------
! -- The information board
! -----------------------------------------------------------------------------------

! -- Erase the information boad

gr.text.typeface 1
gr.text.size 14
gr_color("royalblue")
gr.text.draw game_level,1,line_board,"Level :" + format$("%%",level)
gr_color("white")
gr.text.draw game_life,80,line_board,"Life:" + format$("%%",life)
gr_color("crimson")
gr.text.draw game_bullet,150,line_board,"Bullet :" + format$("%%",list_bundle_alien_shoot_size)
gr_color("springgreen")
gr.text.draw game_score,230,line_board,"Score :" + format$("%%",score)
gr_color("gold")
gr.text.draw game_speed,340,line_board,"FPS :" + format$("%%",speed)

return

!__________________________________________________________________________________________________________
board_update:
gr.modify game_level, "text", "Level :" + format$("%%",level)
gr.modify game_life, "text" , "Life:" + format$("%%",life)
gr.modify game_bullet , "text" ,"Bullet:" + format$("%%",list_bundle_alien_shoot_size)
gr.modify game_score, "text", "Score:" + format$("%%%%",score)
gr.modify game_speed, "text", "FPS:" + format$("%%",speed)
return

debug:
debug.dump.list list_bundle_alien
debug.dump.list list_bundle_bullet_ship
debug.dump.list list_bundle_alien_shoot
debug.dump.bundle ptr_to_bundle_ship
debug.dump.bundle bundle_alien
debug.dump.bundle bundle_alien_shoot
debug.dump.bundle bundle_bullet
return

!ontimer:
! zz=0
!timer.resume
 





