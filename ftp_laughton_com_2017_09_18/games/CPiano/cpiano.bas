REM Start of BASIC! Program
REM color piano

SOUNDPOOL.OPEN 4
SOUNDPOOL.LOAD cnote,"c.mp3"

FN.DEF r()
 FN.RTN RND()*100+80
FN.END

FN.DEF drawkeys(keys[],n$[])
 GR.SCREEN w,h
 ARRAY.LENGTH n,keys[]
 GR.TEXT.SIZE h/20
 GR.TEXT.ALIGN 2
 FOR i=0 TO n-1
  GR.COLOR 130,r(),r(),r()
  GR.RECT keys[i+1],i*w/n,1,(i+1)*w/n,h
  GR.SHOW keys[i+1]
  GR.COLOR 255,255,255,255
  GR.TEXT.DRAW nm,(i+0.5)*w/n,h*0.8,n$[i+1]
  GR.SHOW nm
 NEXT i
 GR.RENDER
FN.END

FN.DEF getkey(keys[])
 ARRAY.LENGTH n,keys[]
 GR.TOUCH t,x,y
if !t then fn.rtn 0
 GR.COLOR 0,0,0,0
 GR.CIRCLE c,x,y,20
 GR.SHOW c:GR.RENDER
 FOR i=1 TO n
  IF GR_COLLISION(c,keys[i])
   let k=i:F_N.CONTINUE
  ENDIF
 NEXT i
 FN.RTN k

FN.END

! main

GR.OPEN 255,0,0,0,0,0

ARRAY.LOAD freq[],261,293,330,349,392,440,494,523,586,660,698
ARRAY.LOAD n$[],"C","D","E","F","G","A","B","C","D","E","F"
ARRAY.LENGTH nkeys,freq[]
DIM keys[nkeys]
drawkeys(&keys[],n$[])

DO
 let k=getkey(&keys[])
 IF k
  GR.MODIFY keys[k],"alpha",255:GR.RENDER
  SOUNDPOOL.PLAY stream, cnote,0.9,0.9,0,0,freq[k]/261
  ! pause 200
  DO 
   !pause 50
   !TONE freq[k],250,1
   let nk=getkey(&keys[])
  UNTIL nk<>k
  GR.MODIFY keys[k],"alpha",130:GR.RENDER
 ENDIF
if background() then exit
UNTIL 0

GR.CLOSE
