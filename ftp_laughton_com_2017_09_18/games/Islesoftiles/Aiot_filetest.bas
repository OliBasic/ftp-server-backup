
REM ISLES OF TILES
REM 2014 by Addyish

gr.open 255,255,255,255

!_!_!_!_!_!_!_!_!_!_!_!_!__!!_!
!-----Constants------
!_!_!_!_!_!_!_!_!_!__!_!_

Gr.Screen w,h 
GW = floor(w/32) %gridwidth 
GH = floor(h/16)  %gridheight 
mw = floor(w/32)  %map Grid width
mh= floor(h/16)  %map Grid height

dim Menu[9]
DIM SAVE[3,32,16] % this will hold our map tile cordinance for saving
DIM BANK[32,16] % this will hold our tiles
DIM Gallery[32,16] %this will hold our gallery
DIM MAP[3,32,16] %this will hold our map layers

!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_
!__________________________---LOAD TILES INTO MEMORY-------
!_!_!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!
GR.bitmap.LOAD temp_storage_address, "/pixel/tileb.png" %load graphic image to a memory storage address and save the address under temp_storage_address

gr.bitmap.SCALE scaled_image, temp_storage_address, w/2,h % "scaled_image" is another memory storage address

gr.bitmap.DELETE temp_storage_address % clean up after ourselves

gr.bitmap.SIZE scaled_image, bw1,bh1

BMGW = bw1/16 %BitMap Grid Width
BMGH = bh1/16 %            Height



!- put first tileset into the tile bank!

FOR col = 1 to 16 
FOR row = 1 to 16
GR.BITMAP.CROP BANK[col,row], scaled_image, (col-1)*bmgw, (row-1)*bmgh,bmgw,bmgh 

next
next

GR.BITMAP.DELETE scaled_image %get rid of scaled bitmap


!------load next tileset------!


GR.bitmap.LOAD temp2_ptr, "/pixel/roof.png"

GR.bitmap.SCALE sbm2_ptr, temp2_ptr, w/2, h

GR.bitmap.DELETE temp2_ptr

GR.bitmap.SIZE sbm2_ptr, bw2,bh2   

bmgw2 = bw2/16 %BitMap Grid Width
bmgh2 = bh2/16 %            Height

!-load second half of gallery---!
!text.open w, gal, "gallery.txt"
for col = 17 to 32 
for row = 1 to 16
                     
	gr.bitmap.CROP BANK[COL,ROW], sbm2_ptr, (col-17)*bmgw, (row-1)*bmgh,bmgw,bmgh
!text.writeln gal, bank[col,row]

next
next
!text.close gal
GR.BITMAP.DELETE sbm2_ptr
!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!
!------                                   -MAIN LOOP                                           ------!
!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!__!_!_!_!_!__!_!_!_!_!__!_!_!_!___!

!!

App States:
 
Menu =     0   user activates the menu
New =      1   user activates a New map
Load =     2   user loads a map
Save? =    2.5 user is asked if they want to save
Save =     3   user saves a map
Gallery =  4   user activates the gallery
MapDraw =  5   user is viewing and adding tiles to the map
Quit?      6   user is asked if they want to quit
MapDIM?    7   user is asked what dimensions he would like the map to be

Map States:
MapNew     0   map is New and un-drawn
MapUnsaved 1   map is drawn on and unsaved
MapSaved   2   map is drawn on and saved
!!

app_state = 0
map_state = 0

DO 


if app_state = 0 then gosub menu 
			
if app_state = 1 then
        print "App_state = ";app_state
        if Map_state = 1 then app_state = 2.5 else gosub new % (if map is drawn on and unsaved from last draw, ask to save)
        endif
        
if app_state = 2 then 
         print "App_state = ";app_state
			       gosub load
      endif
if app_state = 2.5 then
         print "App_state = ";app_state
			    endif
if app_state = 3 then
       gosub save
			 print "App_state = ";app_state
			endif
if app_state = 4
       gosub gallery
			 print "App_state = ";app_state
			endif
if app_state = 5 then gosub MapDraw
			! print "App_state = ";app_state
			 !endif
if app_state = 6
			 print "App_state = ";app_state
       D_u.break
			endif
if app_state = 7
      print "app_state = ";app_state
      endif		
until 0
End

!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!__!_!_!_!_!__!_!_!_!_!__!_!_!_!_!_!_!__!_!_!_!_!__!_!_!_!_!_
Menu:                        %              MENU
!_!_!_!_!_!_!_!_!_!__!_!_!_!_!__!_!_!_!_!__!_!_!_!_!__!_!_!_!_!_!__!_!_!_!_!_!__!_!_!_!__!_!_
do    %necessary! wait for user to release previous touch
  gr.touch touched, tx, ty
until !touched
MGH = h/6
MGC = MGH/2
hmgc = mgc/2

gr.color 255,0,0,0,1
gr.text.size 88
gr.text.draw Menu[1], gw*8 ,mgh , "Isles of Tiles"
gr.text.size 45

!"New" Button
gr.color 255,0,255,0,1
gr.rect Menu[2], gw*8,(mgc*4)-hmgc,gw*24,(mgc*4)+hmgc
Gr.color 255,0,0,0,1
Gr.text.draw Menu[3], gw*15, mgc*4+(hmgc/2), "New"

!"Load" Button
gr.color 255, 0,140,255,1
gr.rect Menu[4], gw*8,mgc*6-hmgc,gw*24,mgc*6+hmgc
gr.color 255,0,0,0,1
gr.text.draw Menu[5], gw*15, mgc*6+(hmgc/2), "Load"

!"Save" Button
gr.color 150,255,0,255,1
Gr.rect Menu[6], gw*8,mgc*8-hmgc,gw*24,mgc*8+hmgc
gr.color 255,0,0,0,1
gr.text.draw Menu[7], gw*15, mgc*8+(hmgc/2), "Save"
gr.color 200,255,0,0,1

!"Quit" button
gr.color 150,0,255,0,1
gr.rect menu[8], gw*8,mgc*10-hmgc,gw*24,mgc*10+hmgc
gr.color 255,0,0,0,1
gr.text.draw Menu[9], gw*15.2, mgc*10+(hmgc/2), "Quit"
gr.color 255,255,0,0,1

left = gw*8
right = gw*24
gr.newdl menu[]
gr.render

DO

  

  gr.bounded.touch new, left,mgc*4-hmgc,right,mgc*4+hmgc
   if new 
				app_state = 1 
				D_U.Break
	endif 

  gr.bounded.touch load,left,mgc*6-hmgc,right,mgc*6+hmgc
  if load
				app_state = 2
				D_U.break
	endif 

  gr.bounded.touch save,left,mgc*8-hmgc,right,mgc*8+hmgc
  if save 
				app_state = 3
				D_U.Break
	endif 

  gr.bounded.touch quit, left,mgc*10-hmgc,right,mgc*10+hmgc
  if quit 
				app_state = 6
				D_U.Break
	endif
  if backkey = 1 then 
       backkey = 0
       app_state = 5
       D_U.Break
  endif
until 0
Return
!_!_!__!_!_!_!_!_!_!_!_!__!_!_!_!_!__!_!_!_!_!_!__!_!_!_!_!_!__!_!_!_!_!_!__!_!_!_!__!_!_!_!_!__!_!_
!-----------------------------------------------NEW------------------------------------------------!
!_!_!_!_!_!__!_!_!_!_!_!__!_!_!_!_!__!_!_!_!_!_!__!_!_!_!_!__!_!_!__!_!_!_!_!_!__!_!_!_!_!__!_!_!_!_

New:
!initialize the map with grass in 1st layer, nothing in 2nd
for col = 1 to 32
  for row = 1 to 16
    gr.bitmap.DRAW MAP[1,col,row], Bank[1,1], (col-1)*mw, (row-1)*mh % fill the empty map array with grass
    Save[1,col,row] = Bank[1,1]
    MAP[2,col,row] = 0
    save[2,col,row]=0
    map[3,col,row] = 0
    save[3,col,row] =0
  next
next
app_state = 5
Tilebrush = Bank[1,1]
Return

!_!_!_!_!_!_!__!_!_!_!_!__!_!_!_!_!_!__!_!_!_!_!_!_!__!_!_!__!_!_!_!_!_!__!_!_!_!_!_!_!__!_!_!_!_!
!--------------------------------------------Map/Draw-----------------------------------------!
!_!_!_!_!__!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!__!_!_!_!_!_

MapDraw:

GR.NEWDL MAP[]
gr.render
do    %necessary! wait for user to release previous touch
  gr.touch touched, tx, ty
until !touched

!-----Menu Button------!

top = mh*15
bottom = mh*16
gr.color 255,0,0,0,1
gr.rect map[1,1,16], 0,top,mw,bottom


!-----Menu Button------!

top_m = mh*15
bottom_m =mh*16
gr.rect map[1,32,16], mw*31,top_m,mw*32,bottom_m

gr.render
DO
      do
        gr.touch touched, tx, ty
        if !background() then gr.render
      until touched

      rx = floor(tx/mw)+1
      ry = floor(ty/mh)+1
      
      if rx = 1 & ry = 16 then 
         app_state = 4
         D_U.Break
      endif
      
      if rx = 32 & ry = 16 then
         app_state = 0
         D_U.break
      endif
     if map[2,RX,RY] <> 0 then
        if Map[3,rx,ry] <> 0 then 
        
            gr.bitmap.draw MAP[2,Rx,ry], Tilebrush, (rx-1)*mw,(ry-1)*mh
            save[2,rx,ry]=tilebrush
            endif
           
        gr.bitmap.draw map[3,rx,ry],tilebrush,(rx-1)*mw,(ry-1)*mh
     
        save[3,rx,ry] = tilebrush
     
     else
     
         gr.bitmap.draw MAP[2,Rx,ry], Tilebrush, (rx-1)*mw,(ry-1)*mh
      
         Save[2,rx,ry] = Tilebrush
      
      endif
UNTIL 0
Return

!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!
!______________________________________________________GALLERY__________________________________________!
!_!_!__!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!

Gallery:

IF !gallery[1,1]
    For col = 1 to 32
      for row = 1 to 16
         GR.BITMAP.Draw gallery[col,row], BANK[col,row], (col-1)*mw, (row-1)*mh
      next
    next
ENDIF

GR.NEWDL gallery[]


Gr.render

DO %touch release
  
  gr.touch touched, tx, ty

UNTIL !touched

DO
  gr.touch touched, tx, ty
  if !background() then GR.render %if user reinitializes the app, then refresh the screen
until touched


!--------Awesome Touch Location Equation--------!

              rx = floor(tx/gw)+1
              ry = floor(ty/gh)+1

Tilebrush = Bank[rx,ry]

app_state = 5

Return
!_!_!_!_!_!_!_!_!_!_!_!_!_!__________!!_!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!_!!!!
!_        _______________________________SAVE MAP    ____________________________________!
!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!_!_!_!


SAVE:
input "File Name:",f$,".map"
text.open w, file, f$
gr.bitmap.CREATE save_map,w,h
gr.bitmap.DRAWINTO.Start save_map
For layers = 1 to 3
for col = 1 to 32
for row = 1 to 16 
if row = 16 then 
text.writeln file, save[layers,col,row]
else
text.writeln file, save[layers,col,row];
text.writeln file, ",";
endif
!if save[layers,col,row] = -1 then byte.write.byte file, 3
!byte.write.byte file,","
if save[layers,col,row] = 0 then f_n.continue
gr.bitmap.draw save,SAVE[layers,col,row] , (col-1)*mw, (row-1)*mh
next
next
next
text.close file
gr.bitmap.Drawinto.END
gr.bitmap.save save_map, "sat.png"
popup "FILE SAVED.",0,0,1
app_state = 0
return 

Onbackkey:
if app_state = 0  then backkey = 1

back.resume

!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!_!_!__!_!_!
!___________________________LOAD_MAP_!_!_!_!_!_!_!_!_!_!_!_!__!_!
!_!_!_!__!_!!__!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!!_!
load:
dim line$[16]
input "Filename:",l$,".map"

text.open r, file, l$
For layers = 1 to 3
ll++
if ll> 3 then ll = 1
for col = 1 to 32
cc = (cc + 1)
if cc > 32 then cc = 1
!for row = 1 to 16

text.readln file, line$
array.delete line$[]
split line$[], line$, ","

for pl = 1 to 16
tick = val(line$[pl])
if tick = 0 then f_n.continue
if tick > 255 then tick = tick -5
tock = mod(tick,16)-2
if tick > 255 then tock = tock + 3
bong = round(tick/16,,"u")
print "val,x,y",tick,tock,bong
gr.bitmap.draw map[layers,col,pl],bank[bong,tock],(col-1)*mw,(pl-1)*mh 
!print tick, layers,col,pl
next pl

!if s = 256  then f_n.continue
!if s = 0 then f_n.continue


next
next
!next
text.close file
!save[] = map[]
app_state= 5
return

fn.def tick()
fn.rtn
fn.end
