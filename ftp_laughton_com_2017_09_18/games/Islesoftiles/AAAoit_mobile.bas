
REM ISLES OF TILES
REM 2014 by Addyish

gr.open 0,255,255,255
layer = 2

!-----Constants------
!_!_!_!_!_!_!_!_!_!__!_!_

Gr.Screen w,h 
GW = floor(w/32) %gridwidth 
GH = floor(h/16)  %gridheight 
mw = floor(w/16)  %map Grid width
mh= floor(h/8)  %map Grid height

dim Menu[10]
DIM SAVE[3,16,8] % this will hold our map tile cordinance for saving
DIM MSBank[32,16]
DIM BANK[32,16] % this will hold our tiles
DIM Gallery[32,16] %this will hold our gallery
DIM MAP[3,16,8] %this will hold our map layers

!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_
!__________________________---LOAD TILES INTO MEMORY-------
!_!_!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!
GR.bitmap.LOAD temp_storage_address, "/pixel/tile1.png" %load graphic image toi a memory storage address and save the address under temp_storage_address

gr.bitmap.SCALE scaled_image, temp_storage_address, w/2,h % "scaled_image" is another memory storage address
gr.bitmap.Scale mapscale, temp_storage_address, w,h*2
gr.bitmap.DELETE temp_storage_address % clean up after ourselves

gr.bitmap.SIZE scaled_image, bw1,bh1
gr.bitmap.SIZE mapscale, msw,msh
print "msw,msh",msw,msh
BMGW = bw1/16 %BitMap Grid Width
BMGH = bh1/16 %            Height
msgw = msw/16
msgh = msh/16
print "msgw,msgh",msgw,msgh


!- put first tileset into the tile bank!

FOR col = 1 to 16 
FOR row = 1 to 16

GR.BITMAP.CROP BANK[col,row], scaled_image, (col-1)*bmgw, (row-1)*bmgh,bmgw,bmgh 
gr.bitmap.CROP msBank[col,row], mapscale, (col-1)*msgw, (row-1)*msgh, msgw,msgh
next
next

GR.BITMAP.DELETE scaled_image %get rid of scaled bitmap
gr.bitmap.delete mapscale

!------load next tileset------!


GR.bitmap.LOAD temp2_ptr, "/pixel/tile2.png"

GR.bitmap.SCALE Galscale, temp2_ptr, w/2, h
gr.bitmap.SCALE Mapscale, temp2_ptr, w,h*2

GR.bitmap.DELETE temp2_ptr

GR.bitmap.SIZE galscale, bw2,bh2   
gr.bitmap.SIZE mapscale, mw2,mh2

bmgw2 = bw2/16 %BitMap Grid Width
bmgh2 = bh2/16 %            Height
msgw2 = mw2/16
msgh2 = mh2/16
!-load second half of gallery---!

for col = 17 to 32 
for row = 1 to 16
                     
	gr.bitmap.CROP BANK[COL,ROW], galscale, (col-17)*bmgw, (row-1)*bmgh,bmgw,bmgh
  gr.bitmap.crop MSBank[col,row], mapscale,(col-17)*msgw2, (row-1)*msgh2, msgw,msgh

next
next

GR.BITMAP.DELETE galscale
Gr.BITMAP.DELETE mapscale
!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!
!------                                   -MAIN LOOP                                           ------!
!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!__!_!_!_!_!__!_!_!_!_!__!_!_!_!___!

!!

App States:
 
Menu =     0   user activates the menu
New =      1   user activates a New map
Load =     2   user loads a map
Save? =    2.5 user is asked if they want to save
Save =     3   user saves a map
Gallery =  4   user activates the gallery
MapDraw =  5   user is viewing and adding tiles to the map
Quit?      6   user is asked if they want to quit
MapDIM?    7   user is asked what dimensions he would like the map to be

Map States:
MapNew     0   map is New and un-drawn
MapUnsaved 1   map is drawn on and unsaved
MapSaved   2   map is drawn on and saved
!!

app_state = 0
map_state = 0

DO 


if app_state = 0 then gosub menu 
			
if app_state = 1 then
        print "App_state = ";app_state
        if Map_state = 1 then app_state = 2.5 else gosub new % (if map is drawn on and unsaved from last draw, ask to save)
        endif
        
if app_state = 2 then print "App_state = ";app_state
			
if app_state = 2.5 then
         print "App_state = ";app_state
			    endif
if app_state = 3 then
       gosub save
			 print "App_state = ";app_state
			endif
if app_state = 4
       gosub gallery
			 print "App_state = ";app_state
			endif
if app_state = 5 then gosub MapDraw
			! print "App_state = ";app_state
			 !endif
if app_state = 6
			 print "App_state = ";app_state
       D_u.break
			endif
if app_state = 7
      print "app_state = ";app_state
      endif		
until 0
End

!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!__!_!_!_!_!__!_!_!_!_!__!_!_!_!_!_!_!__!_!_!_!_!__!_!_!_!_!_
Menu:                        %              MENU
!_!_!_!_!_!_!_!_!_!__!_!_!_!_!__!_!_!_!_!__!_!_!_!_!__!_!_!_!_!_!__!_!_!_!_!_!__!_!_!_!__!_!_
do    %necessary! wait for user to release previous touch
  gr.touch touched, tx, ty
until !touched
MGH = h/6
MGC = MGH/2
hmgc = mgc/2

gr.bitmap.load splash, "/pixel/tile1.png"
gr.bitmap.scale splashs, splash, w,h

gr.color 255,255,255,255,1
gr.text.size 88
gr.bitmap.draw MENU[1],splashs, 0,0
gr.text.draw Menu[2], gw*6,mgh , "Isles of Tiles"
gr.text.size 45

!"New" Button
gr.color 255,0,255,0,1
gr.rect Menu[3], gw*8,(mgc*4)-hmgc,gw*24,(mgc*4)+hmgc
Gr.color 255,0,0,0,1
Gr.text.draw Menu[4], gw*15, mgc*4+(hmgc/2), "New"

!"Load" Button
gr.color 255, 0,140,255,1
gr.rect Menu[5], gw*8,mgc*6-hmgc,gw*24,mgc*6+hmgc
gr.color 255,0,0,0,1
gr.text.draw Menu[6], gw*15, mgc*6+(hmgc/2), "Load"

!"Save" Button
gr.color 150,255,0,255,1
Gr.rect Menu[7], gw*8,mgc*8-hmgc,gw*24,mgc*8+hmgc
gr.color 255,0,0,0,1
gr.text.draw Menu[8], gw*15, mgc*8+(hmgc/2), "Save"
gr.color 200,255,0,0,1

!"Quit" button
gr.color 150,0,255,0,1
gr.rect menu[9], gw*8,mgc*10-hmgc,gw*24,mgc*10+hmgc
gr.color 255,0,0,0,1
gr.text.draw Menu[10], gw*15.2, mgc*10+(hmgc/2), "Quit"
gr.color 255,255,0,0,1

left = gw*8
right = gw*24
gr.newdl menu[]
gr.render

DO

        
        if !background() then gr.render
      

  

  gr.bounded.touch new, left,mgc*4-hmgc,right,mgc*4+hmgc
   if new 
				app_state = 1 
				D_U.Break
	endif 

  gr.bounded.touch load,left,mgc*6-hmgc,right,mgc*6+hmgc
  if load
				app_state = 2
				D_U.break
	endif 

  gr.bounded.touch save,left,mgc*8-hmgc,right,mgc*8+hmgc
  if save 
				app_state = 3
				D_U.Break
	endif 

  gr.bounded.touch quit, left,mgc*10-hmgc,right,mgc*10+hmgc
  if quit 
				app_state = 6
				D_U.Break
	endif
  if backkey = 1 then 
       backkey = 0
       app_state = 5
       D_U.Break
  endif
until 0
Return
!_!_!__!_!_!_!_!_!_!_!_!__!_!_!_!_!__!_!_!_!_!_!__!_!_!_!_!_!__!_!_!_!_!_!__!_!_!_!__!_!_!_!_!__!_!_
!-----------------------------------------------NEW------------------------------------------------!
!_!_!_!_!_!__!_!_!_!_!_!__!_!_!_!_!__!_!_!_!_!_!__!_!_!_!_!__!_!_!__!_!_!_!_!_!__!_!_!_!_!__!_!_!_!_

New:
!initialize the map with grass in 1st layer, nothing in 2nd
for col = 1 to 16
  for row = 1 to 8
    gr.bitmap.DRAW MAP[1,col,row], MSBank[1,10], (col-1)*mw, (row-1)*mh % fill the empty map array with grass
    Save[1,col,row] = MsBank[1,10]
    MAP[2,col,row] = 0
    map[3,col,row] = 0
  next
next
app_state = 5
Tilebrush = msBank[1,1]
Return

!_!_!_!_!_!_!__!_!_!_!_!__!_!_!_!_!_!__!_!_!_!_!_!_!__!_!_!__!_!_!_!_!_!__!_!_!_!_!_!_!__!_!_!_!_!
!--------------------------------------------Map/Draw-----------------------------------------!
!_!_!_!_!__!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!__!_!_!_!_!_

MapDraw:

GR.NEWDL MAP[]
gr.render
do    %necessary! wait for user to release previous touch
  gr.touch touched, tx, ty
until !touched

!-----Menu Button------!

top = mh*7
bottom = mh*8
gr.color 255,0,0,0,1

gr.rect map[1,1,8], 0,top,mw,bottom
gr.color 255,255,255,255,1
gr.text.draw map[2,1,8], 10,mh*8-10,"P"

!-----Menu Button------!
gr.color 255,0,0,0,1
top_m = mh*7
bottom_m =mh*8
gr.rect map[1,3,8], mw*3,top_m,mw*4,bottom_m
gr.color 255,255,255,255,1
gr.text.draw map[2,4,8],mw*3+10,mh*8-10, "3"

top_m = mh*7
bottom_m =mh*8
gr.color 255,0,0,0,1
gr.rect map[1,16,8], mw*15,top_m,mw*16,bottom_m
gr.color 255,255,255,255,1
gr.text.draw map[2,16,8],mw*15+10,mh*8-10, "M"
gr.color 255,0,0,0,1
top_m = mh*7
bottom_m =mh*8
gr.rect map[1,3,8], mw*2,top_m,mw*3,bottom_m
gr.color 255,255,255,255,1
gr.text.draw map[2,4,8],mw*2+10,mh*8-10, "2"
gr.render
DO
      do
        gr.touch touched, tx, ty
        if !background() then gr.render
      until touched

      rx = floor(tx/mw)+1
      ry = floor(ty/mh)+1
      
      if rx = 1 & ry = 8 then 
         app_state = 4
         D_U.Break
      endif
      
      if rx = 16 & ry = 8 then
         app_state = 0
         D_U.break
      endif

      if rx = 3 & ry = 8
         layer = 2
         d_u.break
      endif
      
      if rx = 4 & ry = 8
        layer = 3
        d_u.break
      endif
      if rx > 16 then D_U.continue
      if ry > 8 then D_U.continue
     
gr.bitmap.draw MAP[layer,Rx,ry], Tilebrush, (rx-1)*mw,(ry-1)*mh
Save[layer,rx,ry] = Tilebrush
UNTIL 0
Return

!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!
!______________________________________________________GALLERY__________________________________________!
!_!_!__!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!

Gallery:

IF !gallery[1,1]
    For col = 1 to 32
      for row = 1 to 16
         GR.BITMAP.Draw gallery[col,row], BANK[col,row], (col-1)*gw, (row-1)*gh
      next
    next
ENDIF

GR.NEWDL gallery[]


Gr.render

DO %touch release
  
  gr.touch touched, t, ty

UNTIL !touched

DO
  gr.touch touched, tx, ty
  if !background() then GR.render %if user reinitializes the app, then refresh the screen
until touched


!--------Awesome Touch Location Equation--------!

              rx = floor(tx/gw)+1
              ry = floor(ty/gh)+1

Tilebrush = msBank[rx,ry]

app_state = 5

Return
!_!_!_!_!_!_!_!_!_!_!_!_!_!__________!!_!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!_!!!!
!_        _______________________________SAVE MAP    ____________________________________!
!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!_!_!_!
! Save oppertates on the Menu DL.

SAVE:
input "File Name:",f$,".png"
gr.bitmap.CREATE save_map,w,h
gr.bitmap.DRAWINTO.Start save_map
For layers = 1 to 3
for col = 1 to 16
for row = 1 to 8
if save[layers,col,row] = 0 then f_n.continue
gr.bitmap.draw save,SAVE[layers,col,row] , (col-1)*mw, (row-1)*mh
next
next
next
gr.bitmap.Drawinto.END
gr.bitmap.save save_map, f$
popup "FILE SAVED.",0,0,1
app_state = 0
return 

Onbackkey:
if app_state = 0  then backkey = 1

back.resume
!_@_@_@_@_@_@_@_@_@_@_@_@_@_@_@_@_@_@_@_@_@_@_@_@_@_@_@_@__@_@@__@_@!
!____________________SAVE FILE___________________________________!_!_!
!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_
SaveFile:
input "File Name:",f$,".map"


!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!__!_!_!_!_!_!_!_!_!_!_!_!__!_!_!
!___________________________LOAD_MAP_!_!_!_!_!_!_!_!_!_!_!_!__!_!
!_!_!_!__!_!!__!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!_!!_!
Load:

for layers = 1 to 2
  for col = 1 to 16
    for row = 1 to 8
!read fisrt/next msBank[fx,fy] from file, 
!gr.bitmap.draw map[layers,col,row], msbank[fx,fy], (col-1)*mw, (row-1)*mh
!save[layers,col,row] = bank[fx,fy] 
    next
  next
next
LoadImage:
gr.bitmap.load "
