Rem Falling Blocks
Rem For Android
Rem With RFO Basic!
Rem June 2016
Rem Version 1.00
Rem By Roy Shepherd

di_height = 1152 % set to my Device
di_width = 672

gr.open 255,255,128,255
gr.orientation 1 % Portrait
pause 1000
WakeLock 3
 
gr.screen screenWidth, screenHeight
scale_x = screenWidth / di_width
scale_y = screenHeight / di_height
gr.scale scale_x, scale_y

Rem Let Basic see the functions before they are called
gosub Functions

gosub Initialise

gosub PlayGame

end

!-------------------------------------------------
! Back key for menu
!-------------------------------------------------
onBackKey: 
	gosub Menu
	back.resume 
end

!-------------------------------------------------
! Do this stuff once
! Draw score, high score, start button
!-------------------------------------------------
Initialise:

    score = 0 : highScore = 0 : buzz = 1 : sound = 1
    gosub LoadData
    call Gcol(0, 255, 1)
    gr.text.size 40
    gr.text.align 1
    gr.text.draw prtScore, 50,50, "Score " + int$(score)
    gr.text.align 3
    gr.text.draw prtHighScore, di_width - 100,50, "High Score " + int$(highScore)
    
    gr.text.size 60
    gr.text.align 2
    call Gcol(1, 255, 1)
    gr.text.draw prtNewHigh, di_width / 2,di_height / 2,"New High Score"
    gr.hide prtNewHigh
    gr.text.size 40
    Rem Draw start button
    call Gcol(9, 250, 1)
    w = di_width / 2 : h = di_height
    gr.rect prtStartButton, w - 100, h - 100, w + 100, h - 50 
    call Gcol(0,255,1)
    
    gr.text.draw prtStart, w, h - 60, "Start"
    gr.render
    gr.color 0, 0, 0, 0
    
    soundpool.open 1
    soundpool.load cnote,"c.mp3"
    if cnote = 0 then do : dialog.message "Sound file c.mp3 not found",,ok,"OK" : until ok > 0 : exit
    array.load freq[],261,293,330,349,392,440,494,523,586,660,698
    array.load buzzGame[], 1, 100
    call Gcol(1,255,1)
    gr.rect recLeft, 100,-200,200,0
    call Gcol(2,255,1)
    gr.rect recRight, di_width - 200,-200,di_width - 100,0
    call Gcol(0,255,1)
    gr.point collision, - 1, - 1 
    gr.render
return

!-------------------------------------------------
! Wait for user to tap the start button
!-------------------------------------------------
StartButtonOn_Off:
    sideLeft=-200
    accelerateLeft  = 10 + floor(5 * rnd())
    sideRight =-200
    accelerateRight  = 10 + floor(5 * rnd())
    hitLeft = 0 : hitRight = 0 : flagEnd = 0 : playNote = 0
    if score > highScore then 
        highScore = score
        gr.modify prtHighScore,"text","High Score " + int$(highScore)
        gr.show prtNewHigh
    endif
    gr.modify recLeft,"top",sideLeft,"bottom",200+sideLeft
    gr.modify recRight,"top",sideRight,"bottom",200+sideRight
    gr.show prtStartButton
    gr.show prtStart
    gr.render
    do 
         gr.touch rs, tx, ty
         if rs then
             tx /= scale_x : ty /= scale_y
             gr.modify collision, "x", tx, "y", ty : gr.render
         endif
    until gr_collision(collision, prtStartButton)
    if buzz then vibrate buzzGame[], -1
    do : gr.touch t, tx, ty : until ! t
    if sound then soundpool.play stream, cnote, 0.9, 0.9,0,0,freq[1]/261
    gr.hide prtStartButton
    gr.hide prtStart
    gr.hide prtNewHigh
    score = 0 : gr.modify prtScore,"text","Score:" + int$(score)
    gr.render
return

!-------------------------------------------------
! Play the game
!-------------------------------------------------
PlayGame:
    gosub StartButtonOn_Off
   
    do
        gr.modify recLeft,"top",sideLeft,"bottom",200+sideLeft
        gr.modify recRight,"top",sideRight,"bottom",200+sideRight
        gr.render
        sideLeft += accelerateLeft
        sideRight += accelerateRight
        
        if sideLeft > di_height then
            if hitLeft = 0 then flagEnd = 2
            sideLeft = - 200 : hitLeft = 0
            if accelerateLeft < 50 then accelerateLeft += floor(10 * rnd())
       endif
        
        if sideRight > di_height then 
            if hitRight = 0 then flagEnd = 3
            sideRight = - 200  : hitRight = 0
            if accelerateRight < 50 then accelerateRight += floor(10 * rnd())
        endif
        
        gr.touch t, tx, ty
        if t then 
            tx /= scale_x : ty /= scale_y
            gr.modify collision, "x", tx, "y", ty : gr.render
            
           if ! gr_collision(collision, recLeft) & ! gr_collision(collision, recRight)then flagEnd = 1
            if buzz then vibrate buzzGame[], -1
        endif
        
        if ! hitLeft & gr_collision(collision, recLeft) then
           playNote ++ : hitLeft = 1 : score += 10
           if playNote > 11 then playNote = 1
            if sound then soundpool.play stream, cnote, 0.9, 0.9,0,0,freq[playNote]/261
            gr.modify prtScore,"text","Score:" + int$(score)
        endif
        
        if ! hitRight & gr_collision(collision, recRight) then
            playNote ++ : hitRight = 1 : score += 10
            if playNote > 11 then playNote = 1
            if sound then soundpool.play stream, cnote, 0.9, 0.9,0,0,freq[playNote]/261
            gr.modify prtScore,"text","Score:" + int$(score)
        endif
        gr.modify collision, "x", -1, "y", -1 : gr.render
        if flagEnd > 0 then  gosub StartButtonOn_Off
    until  0
return

!-------------------------------------------------
!Save sound, buzz and high score. At end of game
!-------------------------------------------------
SaveData:
	Path$="../../FallingBlocks/data/"
	file.exists pathPresent,Path$+"GameData.txt"
	if ! pathPresent then file.mkdir Path$
  
		text.open w,hs,Path$+"GameData.txt"
    text.writeln hs,int$(highScore)
		text.writeln hs,int$(sound)
    text.writeln hs,int$(buzz)
		text.close hs
return

!-------------------------------------------------
! Load sound, buzz and high score . At start of game
!-------------------------------------------------
LoadData:
	Path$="../../FallingBlocks/data/"
	file.exists pathPresent,Path$+"GameData.txt"
	if pathPresent then
    text.open r,hs,Path$+"GameData.txt"
    text.readln hs,highScore$
    text.readln hs,sound$
    text.readln hs,buzz$
    text.close hs
    highScore = val(highScore$)
    sound = val(sound$)
    buzz = val(buzz$)
	endif
return

!-------------------------------------------------
! Back key tapped, menu called
!-------------------------------------------------
Menu:

	if buzz then vibration$="Vibration On" else vibration$="Vibration Off"
	if sound then sound$ = "Sound On" else sound$ = "Sound Off"
	array.load menu$[],~
						vibration$,~
            sound$,~
						"Zero High Score",~
						"About",~
						"Help",~
						"Back to Game",~
						"Exit"
	dialog.select menuSelected, menu$[], "Main"
	
	sw.begin menuSelected

		sw.case 1
			if buzz then buzz = 0 else buzz = 1
    
		Sw.break
    
    sw.case 2
       if sound then sound = 0 else sound = 1
    sw.break
    
    sw.case 3
        do : dialog.Message"Set High Score to Zero?",,yn,"Yes","No" : until yn > 0
        if yn = 1 then highScore = 0
        gr.modify prtHighScore,"text","High Score " + int$(highScore) : gr.render
    sw.break
        
		sw.case 4
			Dialog.Message " About:\n\n",~
			"   Falling Blocks\n\n" + ~
      "   for Android\n\n" + ~
			"   With: RFO BASIC!\n\n" + ~
			"   July 2016 \n\n" + ~
			"   Version: 1.00\n\n" + ~ 
			"   Author: Roy Shepherd\n\n", ~
			ok, "OK"
		sw.break
		
		sw.case 5
       dialog.message " Falling Blocks Help:\n\n", ~
       "Tap the start button, then tap the coloured blocks as they " + ~
       "fall down the left and right sides of the screen. A note is sounded each time you tap a block\n\n" + ~
       "At first they move slowly but soon start to move faster and faster.\n\n" + ~
       "The game is over when a block goes off the screen without being tapped " + ~
       "or you miss a block and tap the background. Back Key for menu.\n\n", ~
       ok, "OK"
		sw.break
    
     sw.case 6
        rem Back to game
        
    sw.break
		
		sw.case 7
			do : dialog.Message"Exit Game?",,yn,"Yes","No" : until yn > 0
			if yn=1 then gosub SaveData : exit
		sw.break
	sw.end
return


!-------------------------------------------------
! Start of Functions
!-------------------------------------------------
Functions: 


!-------------------------------------------------
! Colours use in the game
!-------------------------------------------------
fn.def Gcol(c, alpha, style)
	if c=0 then gr.color alpha,20,20,20,style % black (not quite)
	if c=1 then gr.color alpha,255,0,0,style % Red
	if c=2 then gr.color alpha,0,255,0,style % green
  if c=9 then gr.color alpha,255,255,255,style % white
fn.end

return
