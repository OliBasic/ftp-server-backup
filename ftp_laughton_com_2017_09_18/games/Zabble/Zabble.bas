REM Zabble is a like Zatre. @Cassiope34 10/16
! french : http://jeuxsoc.fr/jeu/zatre 
! english : http://boardgamegeek.com/boardgame/2938/zatre

Fn.def xg(noc)
  if !mod(noc,13) then Fn.rtn 13 else Fn.rtn mod(noc,13)
Fn.end

Fn.def yg(noc)
  if !mod(noc,13) then Fn.rtn int(noc/13) else Fn.rtn int(noc/13)+1
Fn.end

Fn.def noc(x,y)   % n° de case en 1 chiffre.
  Fn.rtn (y-1)*13+x
Fn.end

Fn.def clr( cl$ )
  gr.color val(word$(cl$,1,",")), val(word$(cl$,2,",")), val(word$(cl$,3,",")), ~
           val(word$(cl$,4,",")), val(word$(cl$,5,","))
Fn.end

Fn.def LastPtr(ptr)  % put the graphic pointer 'ptr' (so the bitmap) UP to all other.
  gr.getDL ndl[],1
  array.length sz, ndl[]
  if !ptr | sz=1 | ndl[sz] =ptr then array.delete ndl[] : Fn.rtn 0
  array.search ndl[],ptr,n
  if n
    for nn=n to sz-1 : ndl[nn] =ndl[nn+1] : next
    ndl[sz] =ptr : gr.newDL ndl[]
  endif
  array.delete ndl[]
Fn.end

Fn.def move(ptr, arx, ary, speed)   % move smoothly the bitmap 'ptr' to arx,ary
LastPtr( ptr )
do
  gr.get.position ptr,dpx,dpy
  ix =int((dpx-arx)/speed)
  iy =int((dpy-ary)/speed)
  if abs(ix)<3 & abs(iy)<3 then D_u.break
  gr.modify ptr, "x",dpx-ix, "y",dpy-iy
  gr.render
until 0
gr.modify ptr, "x",arx, "y",ary
gr.render
Fn.end

Fn.def align(dx, dy, vd, g[])   % additionne vd sur nc + les dés autour de nc
ARRAY.LOAD diX[] ,0 ,1 ,0 ,-1    % 1 =Nord  2 =Est  3 =Sud  4 =Ouest
ARRAY.LOAD diY[] ,-1,0 ,1 ,0
ho =vd : ve =ho
for di=1 to 4
  xs =dx+diX[di]  :  ys =dy+diY[di]
  while (xs & xs<14 & ys & ys<14)    % ds la grille
    if g[xs,ys,2]   % case occupée
      if di=2 | di=4 then ho+=g[xs, ys, 2] else ve+=g[xs, ys, 2]
    else
      w_r.break
    endif
    xs+=diX[di]  :  ys+=diY[di]
  repeat
next
if (ho>12 | ve>12)
  Fn.rtn 0
elseif ho<10 & ve<10
  Fn.rtn 1
else
  Fn.rtn 2
endif
Fn.end

File.Root tmp$
IstandAlone =Is_In("rfo-basic",tmp$)  % to know if it's an APK for the way to exit
Device b : Bundle.get b, "Language", lang$
lg =2 : if left$(lower$(lang$),2)="fr" then lg =1    % 2=English or 1=French
alg =(lg-1)*18

GR.OPEN 255,100,100,100,0,0  % green & landscape
gr.screen w,h
!scy =800 : scx =w/h*scy
scx =1280 : scy =800
sx =w/scx : sy =h/scy
gr.scale sx,sy
wakelock 3

path$ ="Zabble/"
file$ ="Zabble.ini"
DIM dbmp[6], db[6], dj[6], sac[121], cp[121], p[5], coul[2]     % pions in 5 diff. colors !!!
DIM g[13,13,2], gr[13,13], de[9,2], dp[9], sc[6,26,2], scp[6,20]    % tableau grille & score
DIM play$[5], prio[4], mnus[2], total[2], mess$[36]
ARRAY.LOAD diX[] ,0 ,1 ,0 ,-1    % 1 =Nord  2 =Est  3 =Sud  4 =Ouest
ARRAY.LOAD diY[] ,-1,0 ,1 ,0
coul[1] =3
coul[2] =1
gosub zabblebkgrd
Timer.set 10000    % set time evry 10s !

DO

  GOSUB zabbleinit
  gr.text.draw mess, mg+dc,40,""     % debug
  nok =0 : mns =0
  
  DO            % ============= boucle principale de jeu =============
    GOSUB CPossible    % fabrique la chaine des cases jouables...cp$  pour j.
    if !len(cp$) & cp & !Fin
      if nok
        Fin =1
      else
        tt =0
        for d=1 to 9
          if de[d,j] then tt++
        next
        if tt=9 then nok =1    % pas de case jouable pour ce joueur : on doit passer à l'autre joueur.
        gr.render : pause 1000 : GOSUB chgPlayer : GOSUB Tirage
        if j=2 then gosub AI
        d_u.continue
      endif
    endif
    if Fin
      POPUP mess$[alg+1]  % partie terminée
      if !mns
        mns =1
        for d=1 to 9
          mnus[j]+=de[d,j]
          mnus[3-j]+=de[d,3-j]
        next
        if total[1]-mnus[1]>record then record =total[1]-mnus[1]
        if total[2]-mnus[2]>record then record =total[2]-mnus[2]
        if total[1]-mnus[1]>=total[2]-mnus[2] then j=1 else j=2
        gosub affscores
      endif
    endif
    GOSUB affronds     % indique les cases jouables...
    do
      gr.touch touched, x,y
      if !background() then gr.render
    until touched | new | quit
    if new | quit then d_u.break
    if help then help =0 : move(hlp, 30, -700, 2) : d_u.continue   % enlever l'affichage de la règle.
    x/=sx  :  y/=sy
    cx =int((x-mg)/dc)+1
    cy =int((y-mg)/dc)+1
    m  =0 : vue =0
    if (cx=14 | cx=15) & cy>2 & cy<12 & !Fin      % prend un dé pour le jouer.
      if de[cy-2,j]
        LastPtr( curs )
        gr.get.position dp[cy-2],ox,oy
        deltx =ox-x : delty =oy-y
        gr.modify curs, "bitmap", dj[de[cy-2,j]], "x",ox, "y",oy  : gr.show curs
        gr.hide dp[cy-2]
        m =1
      endif
    elseif cx>=15
      vue =1 : gr.hide curs : GOSUB chgPlayer : gr.render    % affiche scores & dés de l'autre joueur...
    elseif cx=1 & cy=13 & !help
      help =1 : move(hlp, 30, 80, 2) : d_u.continue
    endif
    
   ! if !m then popup int$(cx)+","+int$(cy)+"   "+int$(noc(cx,cy))   % just for debug...
    
    do
      gr.touch touched, tx,ty
      tx/=sx  :  ty/=sy
      if m then gr.modify curs, "x",tx+deltx, "y",ty+delty : gr.render
    until !touched
    if vue then GOSUB chgPlayer : d_u.continue     % restaure le score & dés du joueur en cours...
    gx =int((tx-mg)/dc)+1
    gy =int((ty-mg)/dc)+1
    bon=0 : ok =0 : no =0
    if m
      if gx>13      % dépose du dé ailleurs que ds la grille: il retournera d'oú il vient...
        no =1
      elseif !g[gx,gy,2] & gr[gx,gy] & gr[gx,gy]<3    % dépose du dé sur une case vide & ds la grille.
        gr.modify g[gx,gy,1], "bitmap", db[de[cy-2,j]] : gr.show g[gx,gy,1]
        g[gx,gy,2] =de[cy-2,j] : de[cy-2,j] =0 : gr.hide curs
        cp++ : cp[cp] =noc(gx,gy)    % mémorise le nbre de coups et les cases jouées.
        if cp>1 then GOSUB ctrl      % si coup illégal il sera annulé. (bon=0)
      else
        no =1
      endif
    endif
    if no
      move(curs, xt+6, mg+(cy-1)*dc, 2)     % retour au stock car pas autorisé.
      if de[cy-2,j] then gr.show dp[cy-2]
    elseif bon
      tt =0
      for d=1 to 9    %  tt= nbre de dés au stock de j.
        if de[d,j] then tt++
      next 
      if !tt
        gr.render : pause 1000 : GOSUB chgPlayer : GOSUB Tirage   % change de joueur.
        if j=2 then gosub AI
      endif
    endif
  UNTIL new
UNTIL quit
gosub SaveGame
wakelock 5
Timer.clear
if IstandAlone
  END "Bye...!"
else
  EXIT
endif

OnBackKey:
  if help
    help =0 : move(hlp, 40, -700, 2)
  else
    if lg=1
      Dialog.message win$,"       Que veux-tu faire ?", ook, " Quitter ", " Nouvelle partie ", " Rien ! "
    else
      Dialog.message win$,"       What do you want to do ?", ook, " Exit ", " New game ", " Nothing ! "
    endif
    new  =(ook=1 | ook=2) : quit =(ook=1)
  endif
back.resume

OnTimer:
  Time ,,,h$,m$  :  gr.modify hre, "text", h$+" : "+m$
Timer.resume

AI:        %  l'AI joue...
enc:
array.fill play$[],""
GOSUB CPossible   % fabrique cp$
if !len(cp$) then return
GOSUB affronds
gr.render : pause 1000
ca =1
do
  c$ =word$(cp$,ca)  % pour chaque case jouable :
  if len(c$)
    for d=1 to 9     % pour chaque dé sur cette case : combien ça rapporte ?
      if de[d,j]
        x =xg(val(c$)) : y =yg(val(c$)) : vd =de[d,j] : gosub hovecalc   % calc ho & ve
        if ho<13 & ve<13
         hv$ =right$(int$(100+ho),2)+right$(int$(100+ve),2)
         if gr[x,y] =2 & (ho>9 | ve>9)
          play$[1] +=int$(d)+hv$+right$("00"+c$,3)+" "    %  case grise
         endif
         if ho=12 | ve=12
          play$[4] +=int$(d)+hv$+right$("00"+c$,3)+" "    %  n° du dé + hove$ + n° de case + " "
         endif
         if ho=11 | ve=11
          play$[3] +=int$(d)+hv$+right$("00"+c$,3)+" "    %      1    +   4   +     3      +  1 caractères
         endif
         if ho=10 | ve=10
          play$[2] +=int$(d)+hv$+right$("00"+c$,3)+" "    %  ce qui fait 9 caractères pour un coup.
         endif
         if ho<10 & ve<10
          play$[5] +=int$(d)+hv$+right$("00"+c$,3)+" "    %  le reste....
         endif
        endif
      else
        f_n.break
      endif
    next
  endif
  ca++
until c$=""
GOSUB prio
ch$=""        %  choisir & jouer le dé....
for p=1 to 4
  pr =val(mid$(prio$,p,1))
  if len(play$[pr]) then ch$ =play$[pr] : f_n.break
next
if len(ch$)   % prendre le + haut ho+ve contenu ds ch$
  coup$ ="" : c =1 : vm =0
  do
    c$ =word$(ch$,c)
    if len(c$)
      v =val(mid$(c$,2,2))+val(mid$(c$,4,2))  % somme de ho + ve
      if v>vm then vm =v : coup$ =c$
    endif
    c++
  until c$=""
else
  ch$ =play$[5] : ncp =int(len(ch$)/9) : nch =int(rnd()*ncp)+1   % au hasard si aucun coup valable...
  coup$ =word$(ch$,nch)
endif
if len(coup$)
  gx =xg(val(mid$(coup$,6))) : gy =yg(val(mid$(coup$,6)))  % la case jouée.
  d  =val(left$(coup$,1))   %  n° du dé
  gr.get.position dp[d],ox,oy
  gr.modify curs, "bitmap", dj[de[d,j]], "x",ox, "y",oy  : gr.show curs  : gr.hide dp[d]
  move(curs, mg+(gx-1)*dc, mg+(gy-1)*dc , 6)
  gr.modify g[gx,gy,1], "bitmap", db[de[d,j]] : gr.show g[gx,gy,1] : gr.hide curs
  g[gx,gy,2] =de[d,j] : de[d,j] =0
  if gr[gx,gy]=2 then grise =1 else grise =0
  ho =val(mid$(coup$,2,2)) : ve =val(mid$(coup$,4,2))
  GOSUB marque : gr.render : cp++ : cp[cp] =noc(gx,gy)    % mémorise le nbre de coups et les cases jouées.
endif
td =0
for d=1 to 9
  if de[d,j] then td++
next
if td & !new then goto enc
pause 1500 : GOSUB chgPlayer : GOSUB Tirage    % rend la main.
RETURN

hovecalc:   % additionne vd + les dés autour de gx,gy
ho =vd : ve =ho
for di=1 to 4
  xs =x+diX[di]  :  ys =y+diY[di]
  while (xs & xs<14 & ys & ys<14)    % ds la grille
    if g[xs, ys, 2]    % case occupée
      if (di=2 | di=4) then ho+=g[xs, ys, 2] else ve+=g[xs, ys, 2]
    else
      w_r.break
    endif
    xs+=diX[di]  :  ys+=diY[di]
  repeat
next
return

prio:      %  regarde les colonnes de score pour savoir quelle est la priorité à jouer.
array.fill prio[],0
for li=1 to 26
  for p=1 to 4
    if sc[p,li,j] then prio[p]++
  next
next
prio$ =""
for p=1 to 4
  array.min m,prio[]
  for r=1 to 4
    pr =val(mid$("1432",r,1))   % ordre à prioriser si tout est équivalent : doubleur, 12, 11, 10.
    if prio[pr]=m then prio$ +=int$(pr) : prio[pr] =30 : f_n.break
  next
next
if cp<20 then prio$ ="1432"
return

Tirage:      %  always minimum 2 dice to play.
tt =0 : ti =2 : if (cp=0 | cp=3) then ti =3
for d=1 to 9
  if de[d,j] then tt++    %  tt= nbre de dés (affichés) au stock.
next
if tt then ti=1
if !tt | ti=1
  nt =0
  for t=1 to 9
    if !de[t,j]
      if ta=121 then Fin =1 : f_n.break
      ta++ : de[t,j] =sac[ta] : nt++
      if nt=ti then f_n.break
    endif
  next
  gr.modify sac, "text", int$(121-ta)
endif
RETURN

trides:           %  regroupe les dés du stock et les classe en ordre décroissant.
for d=1 to 8
  if !de[d,j]
    for d2=d+1 to 9 : swap de[d2-1,j], de[d2,j] : next : f_n.break
  endif
next
for i=1 to 9      %  tri le stock de dés en ordre décroissant...
  for i2=i+1 to 9
    if de[i,j]<de[i2,j] then swap de[i,j], de[i2,j]
  next
  gr.hide dp[i] : if de[i,j] then gr.modify dp[i], "bitmap", dj[de[i,j]] : gr.show dp[i]
next
return

CPossible:        %  fabrique la chaine des cases possibles...cp$ avec leur n°.
gosub trides
cp$ =""
for y=1 to 13
  for x=1 to 13
    if gr[x,y] & gr[x,y]<3 & !g[x,y,2]   % case vide & ds la grille
      gr.hide g[x,y,1]
      for s=1 to 4      % 1 voisin minimum.
        rx =x+diX[s] : ry =y+diY[s]
        if rx & rx<14 & ry & ry<14
          if g[rx,ry,2] then cp$+=int$(noc(x,y))+" " : f_n.break   % il y a au moins 1 dé autour de rx,ry.
        endif
      next
    endif
  next
next
if len(cp$)
  ca =1 : cn$ ="" : cg$ ="" : cge$ =""
  do
    c$ =word$(cp$,ca)    % pour chaques cases jouables (ayant au moins un voisin)
    if len(c$)
      x =xg(val(c$)) : y =yg(val(c$))
      if !align(x,y,1,g[])      % si impossible de jouer au moins "1" alors on ferme cette case.
        gr.modify g[x,y,1], "bitmap", cross : gr.show g[x,y,1] : gr[x,y] =3 : gr.render
      else
        for d=1 to 9     % teste chaque dé du joueur j
          if de[d,j]
            a =align(x,y, de[d,j], g[])
            if a
              if gr[x,y]=1
                cn$+=c$+" " : f_n.break
              else
                if a=2
                  cg$+=c$+" " : f_n.break
                else
                  cge$+=c$+" "
                endif
              endif
            endif
          endif
        next
        if len(cn$+cg$) then cge$ =""
      endif
    endif
    ca++
  until c$=""
  cp$ =cn$ + cg$ + cge$
else
  if cp then Fin =1     % plus aucune case jouable !
endif
if cp=0
  cp$ ="72 98 84 85 86 "
  if de[1,j]+de[2,j]+de[3,j]<13 then cp$+="59 83 87 111"
elseif cp=2 & !g[7,7,2]
  cp$ ="85"
endif
RETURN

affronds:       % mets des petits ronds pour montrer les cases jouables...
if len(cp$)
  ca =1
  do
    c$ =word$(cp$,ca)
    if len(c$) then x =xg(val(c$)) : y =yg(val(c$)) : gr.modify g[x,y,1], "bitmap", point : gr.show g[x,y,1]
    ca++
  until c$=""
endif
return

chgPlayer:    % changement de joueur  (stock + score)
j=3-j
gr.modify pion, "bitmap", p[coul[j]]
GOSUB affscores
GOSUB trides
return

sethove:   % additionne g[gx,gy,2] + les dés autour de gx,gy
ho =g[gx,gy,2] : ve =ho
for di=1 to 4
  xs =gx+diX[di]  :  ys =gy+diY[di]
  while (xs & xs<14 & ys & ys<14)    % ds la grille
    if g[xs, ys, 2]    % case occupée
      if (di=2 | di=4) then ho+=g[xs, ys, 2] else ve+=g[xs, ys, 2]
    else
      w_r.break
    endif
    xs+=diX[di]  :  ys+=diY[di]
  repeat
next
return

Ctrl:    % controle la pose de de[cy-2,j] sur g[gx,gy,2] & marque le score.
GOSUB sethove       % additionne g[gx,gy,2] + les dés autour de gx,gy
if cp<4     % case rouge doit être occupée.
  v =0
  for s=1 to 4     % juste s'assurer du voisinage.
    xs =gx+diX[s] : ys =gy+diY[s]
    if g[xs,ys,2] then v =1 : f_n.break
  next
  if !v then POPUP mess$[alg+2],,-(scy/2)*sy : pause 500 : GOSUB Undo
 ! td =0 : for d=1 to 3 : td +=de[d,j] : next
  if (cp=3 & !g[7,7,2]) | (ho>12 | ve>12)  % & !td      % si les 3 dés ont été joués et case centrale vide.
    POPUP mess$[alg+5] : pause 500
    for u=1 to 3 : GOSUB Undo : next
    read.from 1    % au cas oú il y aurait eu des croix.
    for y=1 to 13
      for x=1 to 13
        read.next gr[x,y] : gr.hide g[x,y,1]
      next
    next
    array.fill sc[],0 : array.fill total[],0 : gr.hide tot
    for li=1 to 2     % efface les scores potentiel...
      for cl=1 to 6
        if cl<>5 then gr.hide scp[cl,li]
      next
    next
    gosub trides
    return
  endif
else
  if !IS_IN(int$(noc(gx,gy)),cp$) then POPUP mess$[alg+2],,-(scy/2)*sy :pause 500 : GOSUB Undo  % case qui n'est pas ds cp$
endif
grise =0
if (ho>12 | ve>12)
  POPUP mess$[alg+3] : GOSUB Undo  % Alignement >12 non autorisé
  return
elseif gr[gx,gy]=2     % si case grise s'assurer que l'alignement fait 10, 11 ou 12 ou pas d'autre choix.
  if ho>9 | ve>9
    grise =1
  else      % cherche s'il y a une autre possibilité pour ce dé
    ca =1
    do
      c$ =word$(cp$,ca)   % parmi toutes les cases jouables
      if len(c$)
        x =xg(val(c$)) : y =yg(val(c$))
        if gr[x,y]=1 & align(x,y,g[gx,gy,2],g[]) then d_u.break    % 1 case non grise possible.
      endif
      ca++
    until c$=""
    if gr[x,y]=1 & (gx<>7 & gy<>7) then POPUP mess$[alg+4] : GOSUB Undo    % Case grise autorisée si la somme >9
  endif
endif
GOSUB marque
return

marque:
if (ho=12 | ve=12)    % ferme les 2 cases aux extrémités de l'alignement de 12.
  for di=1 to 4
    xs =gx+diX[di]  :  ys =gy+diY[di]
    while (xs & xs<14 & ys & ys<14)
      if gr[xs,ys] & gr[xs,ys]<3     %  autorisée ds la grille
        if !g[xs, ys, 2]
          if ( ve=12 & (di=1 | di=3) ) | ( ho=12 & (di=2 | di=4) )
            gr.modify g[xs,ys,1], "bitmap", cross : gr.show g[xs,ys,1]
            gr[xs,ys] =3  :  w_r.break   % case interdite maintenant.
          endif
        endif
      else
        w_r.break
      endif
      xs +=diX[di]  :  ys +=diY[di]
    repeat
  next
endif
bon =1 : nok =0
if ho>9 | ve>9     % inscrit le score (compte 26 lignes de score)
  if grise
    for li=1 to 26
      if !sc[1,li,j] then sc[1,li,j]=1 : f_n.break    % x2
    next
  endif
  cl1 =0 : li1 =0
  if ho>9 & ho<13
    cl1 =val(word$("4 3 2",13-ho))
    for li1=1 to 26
      if !sc[cl1,li1,j] then sc[cl1,li1,j] =val(word$("0 1 2 4",cl1)) : f_n.break
    next
  endif
  if ve>9 & ve<13
    cl =val(word$("4 3 2",13-ve))
    for li=1 to 26
      if !sc[cl,li,j]
        if cl =cl1 then li =li1
        sc[cl,li,j] +=val(word$("0 1 2 4",cl)) : f_n.break
      endif
    next
  endif
  for li=1 to 26     % somme par ligne.
    sc[6,li,j] = sc[2,li,j]+sc[3,li,j]+sc[4,li,j]
    if sc[2,li,j]>0 & sc[3,li,j]>0 & sc[4,li,j]>0 then sc[6,li,j]+=sc[5,li,j]  % ligne complète : +bonus
    if sc[1,li,j] then sc[6,li,j]=2*sc[6,li,j]    % si doubleur : x2
  next
  GOSUB affscores
endif
RETURN

affscores:
for li=1 to 20    % seulement 20 lignes affichables...
  for cl=1 to 6
    if sc[cl,li,j]
      if cl=1 then xaf$ ="x" else xaf$ =int$(sc[cl,li,j])
      if cl<>5 then gr.modify scp[cl,li], "text", xaf$ : gr.show scp[cl,li]
    else
      gr.hide scp[cl,li]
    endif
  next
next
total[j] =0 : for li=1 to 26 :total[j]+=sc[6,li,j] :next
gr.modify mnu, "text", int$(mnus[j])
gr.modify tot, "text", int$(total[j]-mnus[j])
if total[j] then gr.show tot else gr.hide tot
gr.modify rec, "text", int$(record)
return

Undo:
xr =xg(cp[cp]) : yr =yg(cp[cp]) : cp[cp] =0 : cp--
p =0 : do : p++ :until !de[p,j]         % la première place libre au stock.
gr.get.position g[xr,yr,1], px, py
gr.modify curs, "bitmap", dj[g[xr,yr,2]], "x",px, "y",py  : gr.show curs  : gr.hide g[xr,yr,1]
move(curs, xt+6, mg+dc+p*dc, 3)
gr.modify dp[p], "bitmap" ,dj[g[xr,yr,2]]  : gr.show dp[p]  : gr.hide curs
de[p,j] =g[xr,yr,2] : g[xr,yr,2] =0
RETURN

zabblebkgrd:    % fabrique le bitmap de l'écran du jeu entier.
gr.bitmap.load dbmp, path$+"6D.png"   % dice
dc =60      % dimension d'une case.
for d=1 to 6
  gr.bitmap.crop dbmp1, dbmp, (d-1)*64, 0, 64,64
  gr.bitmap.scale db[d], dbmp1, dc,dc   % white dice bitmap
  gr.bitmap.scale dj[d], dbmp1, dc,dc   % yellow dice bitmap
!  gr.bitmap.crop db[d], dbmp, (d-1)*dc, 0, dc,dc
!  gr.bitmap.crop dj[d], dbmp, (d-1)*dc, 0, dc,dc   % avec "6D_vrais.png"
  gr.bitmap.drawinto.start dj[d]
    clr("100,255,255,178,1")
    gr.rect nul, 5,5,dc-6,dc-6
  gr.bitmap.drawinto.end
next
gr.bitmap.load pions, path$+"pions1.png"
for p=1 to 5
  gr.bitmap.crop pio, pions, (p-1)*128, 0, 128, 212
  gr.bitmap.scale p[p], pio, 55, 90
  gr.bitmap.delete pio
next
read.data 0,0,0,1,1,2,0,2,1,1,0,0,0,~
          0,2,1,1,1,1,1,1,1,1,1,2,0,~
          0,1,2,1,1,1,1,1,1,1,2,1,0,~
          1,1,1,2,1,1,1,1,1,2,1,1,1,~
          1,1,1,1,2,1,1,1,2,1,1,1,1,~
          2,1,1,1,1,2,1,2,1,1,1,1,2,~
          0,1,1,1,1,1,2,1,1,1,1,1,0,~
          2,1,1,1,1,2,1,2,1,1,1,1,2,~
          1,1,1,1,2,1,1,1,2,1,1,1,1,~
          1,1,1,2,1,1,1,1,1,2,1,1,1,~
          0,1,2,1,1,1,1,1,1,1,2,1,0,~
          0,2,1,1,1,1,1,1,1,1,1,2,0,~
          0,0,0,1,1,2,0,2,1,1,0,0,0
gr.bitmap.create fond, scx, scy
gr.bitmap.drawinto.start fond
gr.set.stroke 2
clr( "255,210,210,210,2" )
mg =(scy-13*dc)/2
for y=1 to 13            % la grille de jeu.
  for x=1 to 13
    read.next c  :  clr( "255,168,168,168,2" )
    if c=1 then gr.rect nul, mg+3+(x-1)*dc, mg+3+(y-1)*dc, mg+1+x*dc, mg+1+y*dc
    clr( "255,210,210,210,"+int$(c-1) )
    if c then gr.rect nul, mg+(x-1)*dc, mg+(y-1)*dc, mg+x*dc, mg+y*dc
  next
next
clr( "80,255,0,0,2" )
gr.rect nul, mg+6*dc, mg+6*dc, mg+7*dc, mg+7*dc
clr( "255,170,170,170,2" )
xt =2*mg+13*dc
gr.rect nul, xt, mg+2*dc-28, xt+70, scy-mg-78
gr.rect nul, xt+80, 100, scx-mg, scy-mg
clr( "255,160,0,250,0" )       % violet
gr.rect nul, xt+80, 100, scx-mg, scy-mg
e =36
e =30
for li=1 to 22 %18
  gr.line nul, xt+80, 100+(li-1)*e, scx-mg, 100+(li-1)*e     % tableau des scores
next
e =70
for li=1 to 5
  gr.line nul, xt+120+(li-1)*e, 100, xt+120+(li-1)*e, scy-mg-60
next
clr( "255,200,200,200,2" )
font.load ft, path$+"Batavia.ttf"
gr.text.setfont ft
gr.text.size 70
gr.text.align 2
gr.text.draw nul, scx-206, 78, "ZABBLE"
clr( "255,0,0,0,1" )
gr.text.size 17
gr.text.draw nul, xt+100, 124, "x2"
gr.text.draw nul, xt+155, 124, "10 (1)"
gr.text.draw nul, xt+225, 124, "11 (2)"
gr.text.draw nul, xt+295, 124, "12 (4)"
gr.text.draw nul, xt+365, 124, "Bonus"
gr.text.draw nul, xt+435, 124, "Total"
gr.text.draw nul, xt+140, 154+20*30, "Record"
gr.text.draw nul, xt+358, 154+20*30, "Minus   -"
gr.text.draw nul, xt+336, 150+21*30, "Grand Total"
clr( "255,0,96,255,1" )      % blue
gr.text.size 18
e =30  :  b =2
for li=1 to 20
  if !mod(li-1,4) then b++
  gr.text.draw nul, xt+365, 124+li*e, int$(b)   % affiche la valeur du bonus par ligne.
next
clr("60,0,192,255,1")   % bleu
gr.circle nul, xt+34, scy-44, 36   %  sous le nbre de dés restants ds le sac.
clr("165,255,160,0,1")  %  orange
gr.text.size 50
gr.text.draw nul, 35, scy-16, "?"
gr.bitmap.drawinto.end  % fond

gr.bitmap.create cursor, dc, dc   % bitmap mobile qui sert à bouger un dé.
gr.bitmap.create point, dc, dc    % bitmap du petit point jaune.
gr.bitmap.drawinto.start point
  clr("255,255,252,172,1")
  gr.circle nul, dc/2, dc/2, 5
gr.bitmap.drawinto.end
gr.bitmap.create cross, dc, dc    % bitmap de la croix qui ferme les cases.
gr.bitmap.drawinto.start cross
!clr( "255,0,96,255,1" )      % blue
!clr( "255,12,184,210,1" )    % blue clair
clr( "255,0,154,10,1" )      % vert
!clr( "255,144,211,12,1" )    % vert clair
!clr( "255,255,85,0,1" )      % orange
!clr( "255,255,0,0,1" )       % rouge
gr.set.stroke 2
gr.line nul,10,10,dc-10,dc-10
gr.line nul,10,dc-10,dc-10,10
gr.bitmap.drawinto.end   % cross

mess$[1]  ="  Partie terminée...  "
mess$[2]  ="   non autorisé   "
mess$[3]  =" Alignement >12 non autorisé... "
mess$[4]  =" Case blanche autorisée si la somme >9 "
mess$[5]  =" La case rouge doit être occupée... "
mess$[6]  ="Au départ 121 dés sont mélangés dans un sac (21x #1, 20x #2, ..., 20x #6)."
mess$[7]  ="Chaque joueur doit poser tous les dés qu ' il reçoit (2 en général) tant que c ' est possible :"
mess$[8]  =" - le joueur qui commence doit jouer un de ces dés sur la case rouge centrale."
mess$[9]  =" - un dé ne peut être posé qu ' à coté d ' un dé déjà présent sur le plateau."
mess$[10] =" - tout alignement dont la somme dépasse 12 est interdit."
mess$[11] =" - les cases blanches permettent de doubler une ligne de score mais leur utilisation n ' est"
mess$[12] ="   possible que si l ' alignement produit est >9 ou s'il n ' est pas possible de jouer ailleurs."
mess$[13] ="Le calcul du score est plus complexe : seuls les alignements totalisant 10, 11 ou 12"
mess$[14] ="  rapportent des points et le bonus ne sera ajouté qu ' à une ligne de score complète..." 
mess$[15] ="  mais heureusement : les ordinateurs ont été inventés !"
mess$[16] ="( touchez la table de score pour voir celle de l ' AI... )"
mess$[17] ="      Zabble est une e-adaptation par @Cassiope34 du jeu 'ZATRE' de Manfred Schüling."

mess$[19] ="  Game over...  "
mess$[20] ="   not authorized   "
mess$[21] =" Sum higher than 12 is not authorized. "
mess$[22] ="White cell is only authorized if the sum is higher than 9."
mess$[23] ="The red cell has to be played"
mess$[24] ="To start with, 121 dices are shuffled in a bag (21x #1, 20x #2, ..., 20x #6)."
mess$[25] ="Each turn and if possible, the player has to play all his dices (usually 2 dices):"
mess$[26] =" - the first player must play one of his dices on the central red cell,"
mess$[27] =" - a dice can only be played next to another one on the board,"
mess$[28] =" - a sum higher than 12 on one alignment of dices is not authorized,"
mess$[29] =" - the white cells can only be played on if the sum in the alignment is higher than 9,"
mess$[30] ="   or if it is the only option. It doubles the points of one line of the score table."
mess$[31] ="The score calculation is a bit complex, fortunately computers have been invented!"
mess$[32] =" - the only way to score is to make alignments of 10, 11, 12,"
mess$[33] =" - a completed line in the score table gives a bonus."
mess$[34] ="( touch the score table to see the AI ' s one )
mess$[35] ="Zabble is an e-adaptation by Cassiope34 of the Manfred Schüling ' s game ' ZATRE ' "

gr.bitmap.create hptr, 1220, 640
gr.bitmap.drawinto.start hptr
clr("255,96,186,102,1")  % fond vert clair
gr.rect nul, 0,0,1220,640
clr("255,255,160,0,1")   %  orange
gr.text.size 40
gr.text.draw nul, scx/2.2, 55, "------------       "+word$("Règles du jeu-Game rule",lg,"-")+"       -------------"
gr.text.align 1
gr.text.size 23
clr("255,255,252,172,1")  % texte jaune clair
for li=1 to 13
  gr.text.draw nul, 24, 80+li*45, mess$[alg+5+li]
next
gr.bitmap.drawinto.end

gr.text.align 2
gr.bitmap.delete pions
gr.bitmap.delete dbmp1
gr.bitmap.delete dbmp
RETURN

zabbleinit:        %  init or re-init une partie.
j =1
gr.cls
gr.bitmap.draw grid, fond, 0,0
gr.bitmap.draw curs,cursor,0,0
for d=1 to 9         % crée les 9 objets graphiques des dés du stock des joueurs.
  gr.bitmap.draw dp[d], curs, xt+6, mg+dc+d*dc : gr.hide dp[d]
next
clr( "255,0,96,255,1" )      % texte blue
array.fill cp[],0
array.fill sc[],0
array.fill scp[],0
array.fill de[],0
array.fill  g[],0
array.fill gr[],0
array.fill mnus[],0
read.from 1
for y=1 to 13        % crée tous les objets graphiques de la grille de jeu.
  for x=1 to 13
    read.next gr[x,y]
    if gr[x,y] then gr.bitmap.draw g[x,y,1], curs, mg+(x-1)*dc, mg+(y-1)*dc : gr.hide g[x,y,1]
  next
next
b =2
for li =1 to 20            % maj de la valeur du bonus par ligne
  if !mod(li-1,4) then b++
  sc[5,li,1] =b : sc[5,li,2] =b
next
sac[1] =1 : n=1
for ds=2 to 121 : n =n+1-6*(n=6) : sac[ds]=n : next    % 6x (1 to 6) + 1 = 121 dés ds le sac.
array.shuffle sac[]
gr.text.size 30
clr("255,12,184,210,1")    %  bleu
Time ,,,h$,m$  :  gr.text.draw hre, 90, dc-12, h$+" : "+m$   % heure
gr.text.size 20
clr("255,255,160,0,1")  %  orange
gr.text.draw mnu, xt+436, scy-46, ""
gr.text.draw rec, xt+140, scy-20, int$(record)
clr("255,255,252,172,1")  %  jaune clair
for li=1 to 20       % crée tous les objets graphiques du tableau des scores.
  for cl=1 to 6
    if cl>1 then xaf =xt+12+cl*70 else xaf =xt+100
    if cl<>5 then gr.text.draw scp[cl,li], xaf, 124+li*30, int$(sc[cl,li,j]) : gr.hide scp[cl,li]
  next
next
gr.text.draw sac, xt+34, scy-36, "121"    % nbre de dés au départ ds le sac.
gr.text.draw tot, xt+436, scy-20, ""  : gr.hide tot
new =0
ta  =0
cp  =0
Fin =0
gosub LoadGame
gosub affscores
gr.bitmap.draw pion, p[coul[j]], xt-20, 5   % le gros pion en haut...
gr.bitmap.draw hlp, hptr, 20, -700      % le bitmap de la règle du jeu
RETURN

SaveGame:
cls : ?ta,cp,total[j],j,record
ch$ ="" : for y=1 to 13 : for x=1 to 13 : ch$+=int$(g[x,y,2]) : ch1$+=int$(gr[x,y]) : next : next : ?ch$ : ?ch1$
ch$ ="" : for li=1 to 26 : for cl=1 to 6 : ch$+=right$(int$(100+sc[cl,li,1]),2)+" " : next : next : ?ch$
ch$ ="" : for li=1 to 26 : for cl=1 to 6 : ch$+=right$(int$(100+sc[cl,li,2]),2)+" " : next : next : ?ch$
ch$ ="" : ch1$ ="" : for d=1 to 9 : ch$+=int$(de[d,1]) :ch1$+=int$(de[d,2]) : next : ?ch$ : ?ch1$
mkdir path$
console.save path$+file$
cls
RETURN

LoadGame:
File.exists fe, path$+file$
if fe
  grabfile ldg$, path$+file$
  split ln$[], ldg$, "\n"
  ta =val(word$(ln$[1],1,",")) : gr.modify sac, "text", int$(121-ta)
  cp =val(word$(ln$[1],2,","))
  j  =val(word$(ln$[1],4,","))
  total[j] =val(word$(ln$[1],3,",")) : gr.modify tot, "text", int$(total[j])
  record =val(word$(ln$[1],5,","))
  c  =0
  for y=1 to 13         %  le plateau de jeu
    for x=1 to 13
      c++ : ch =val(mid$(ln$[2],c,1))
      if ch then g[x,y,2] =ch : gr.modify g[x,y,1], "bitmap", db[ch] : gr.show g[x,y,1]
      gr[x,y] =val(mid$(ln$[3],c,1))
      if gr[x,y]=3 then gr.modify g[x,y,1], "bitmap", cross : gr.show g[x,y,1]
    next
  next
  c =0
  for li=1 to 26      % maj des scores des joueurs
    for cl=1 to 6
      c++ : sc[cl,li,1] =val(word$(ln$[4],c)) : sc[cl,li,2] =val(word$(ln$[5],c))
    next
  next
  GOSUB affscores     % du joueur courant.
  c =0
  for d=1 to 9        % les dés du stock du joueur 2 (AI)
    c++ : de[d,1] =val(mid$(ln$[6],d,1)) : de[d,2] =val(mid$(ln$[7],d,1))
    if de[d,j] then gr.modify dp[d], "bitmap", dj[de[d,j]] : gr.show dp[d]   % affiche les dés du stock du joueur j.
  next
  file.delete fe, path$+file$
else
  GOSUB Tirage
endif
RETURN
