REM computeropolis board game v1.0
REM Created by Antonis


DIM propr1[28], propr2[28]
! cards
ARRAY.load card$[],"You Win 20$","Go to Stop! for 2 rounds","You Lose 20$"~
"Play Again Once!","You Win 50$","Don't play for 1 round!"~
"Play Again Twice!","Go to Stop! for 1 round","You Lose 50$"~
"Go to Free Area"

!coordinates for movement
!display is 1280x800
ARRAY.load cx[],713,550,472,393,315,240,158,68~
0,0,0,0,0,0,0~
160,235,317,392,470,548,627~
713,713,713,713,713,713

ARRAY.load cy[],680,680,680,680,680,680,680,680~
535,456,383,306,234,155~
3,3,3,3,3,3,3,3~
152,230,302,378,453,528

!buy cost
ARRAY.load cost[],0,20,20,0,25,0,25,0,30,30,0~
35,0,35,0,40,0,40,0,45,45,0,50,0,50,0,60,60

!rent cost
ARRAY.load rent[],0,10,10,0,12,0,12,0,15,15,0~
17,0,17,0,20,0,20,0,22,22,0,25,0,25,0,30,50

GR.open 255, 0, 0, 0

!*************
restart:
!*************
!go graphics
GR.color 255, 255, 255, 255, 1
GR.text.size 67
GR.orientation 1
GR.screen w, h
IF w>h THEN % exchange values
    z = w
    w = h
    h = z
ENDIF
IF w = 752 THEN w = 800
scalex = w / 800
scaley = h / 1280

GR.bitmap.load bptr, "board.jpg"
GR.bitmap.load bp1, "pe1.png"
GR.bitmap.load bp2, "pe2.png"
GR.bitmap.load bip1, "pl1.png"
GR.bitmap.load bip2, "pl2.png"
GR.bitmap.load bip3, "help.png"
GR.bitmap.load bip4, "star.png"
GR.bitmap.load bip5, "help2.png"
GR.bitmap.load bip6, "gametype.png"
GR.scale scalex, scaley

GOSUB chooseplay

GR.bitmap.DRAW optr, bptr, 0, 0
GR.bitmap.DRAW op2, bp2, 8 + cx[1], 8 + cy[1]
GR.bitmap.DRAW op1, bp1, cx[1], cy[1]
GR.color 255, 255, 255, 255, 0
GR.rect robj3, 710, 820, 750, 885
GR.rect robj4, 759, 820, 799, 885
GR.text.DRAW comobj2, 8, 1180, "press dice to roll dices"
GR.color 255, 255, 255, 255, 1
GR.text.DRAW obj1, 710, 878, "6"
GR.text.DRAW obj2, 759, 878, "6"
GR.text.DRAW obj3, 710, 878, "6"
GR.text.DRAW obj4, 759, 878, "6"
GR.text.DRAW cptr3, 290, 980, "250"
GR.text.DRAW cptr4, 290, 1040, "250"
GR.RENDER

!***initilize everything****

money1 = 250
money2 = 250
stop1 = 0
stop2 = 0
playerturn = 1
loseround1 = 0
loseround2 = 0
diceok = 0
pos1 = 1
pos2 = 1
bonustart = 20
plagain1 = 0
plagain2 = 0
win=600
FOR i = 1 TO 28
   propr1[i] = 0
   propr2[i] = 0
NEXT i
computerdice=0
 
GR.text.DRAW cptr, 8, 880, "Dice Buy Next Hlp Quit"
GR.color 255, 0, 255, 255, 1
IF computergame=0 THEN plr1$= "Player 1" ELSE plr1$= "Human  "
IF computergame=0 THEN plr2$= "Player 2" ELSE plr2$= "Android "
GR.text.DRAW cptr1, 8, 980, plr1$+":"
GR.text.DRAW cptr2, 8, 1040, plr2$+":"

GOSUB showmoney

GR.color 255, 255, 255, 255, 0
GR.rect robj1, 8, 1060, 791, 1120
GR.rect robj2, 8, 1130, 791, 1190
GR.color 255, 255, 255, 255, 1
GR.text.DRAW comobj,8,1110,"Plays player: "+plr1$

GR.RENDER

! ****** main ******
start:

! wait for command:
! dice, buy, help, next, quit

GOSUB touchme

! ****** choosed dice *****
IF choosed$="d" & diceok=1 THEN 
         GR.HIDE comobj2
         GR.text.DRAW comobj2,8,1180,"you already rolled dices"
         GR.RENDER
         goto start
     ENDIF
 IF choosed$="d" & diceok=0 THEN        
   GOSUB rolldice
   !advance player
   IF playerturn = 1 THEN pos1 = pos1 + dice1 + dice2 ELSE pos2 = pos2 + dice1 + dice2
   IF playerturn=1 & pos1>28 THEN
             pos1 = pos1 - 28
             money1 = money1 + bonustart
     ENDIF
   IF playerturn=2 & pos2>28 THEN
             pos2 = pos2 - 28
             money2 = money2 + bonustart
     ENDIF

 ! position drawing
 IF playerturn = 1 THEN
      TONE 400,200
     GR.HIDE op1
     GR.bitmap.DRAW op1, bp1, cx[pos1], cy[pos1]
	 GR.RENDER
 ENDIF
 IF playerturn = 2 THEN
     TONE 800,200
     GR.HIDE op2
     GR.bitmap.DRAW op2, bp2, cx[pos2] + 5, cy[pos2] + 5
	 GR.RENDER
 ENDIF
 GR.RENDER

! update money for passing start
 GOSUB showmoney
 GOSUB checkmoney

! check square for getmoney
  IF playerturn = 1 THEN
    IF pos1 = 6 THEN money1 = money1 + 20
    IF pos1 = 13 THEN money1 = money1 + 30
    IF pos1 = 17 THEN money1 = money1 + 40
    IF pos1 = 24 THEN money1 = money1 + 50
    IF pos1 = 6 | pos1 = 13 | pos1 = 17 | pos1 = 24 THEN
                                                  GOSUB coins
												                            GOSUB showmoney
                                                  GOSUB checkmoney
                                               ENDIF
 ENDIF
    IF playerturn = 2 THEN
         IF pos2 = 6 THEN money2 = money2 + 20
         IF pos2 = 13 THEN money2 = money2 + 30
         IF pos2 = 17 THEN money2 = money2 + 40
         IF pos2 = 24 THEN money2 = money2 + 50
         IF pos2=6 | pos2=13 | pos2=17 | pos2=24 THEN
                                                    GOSUB coins
													                            GOSUB showmoney
                                                    GOSUB checkmoney
                                                   ENDIF
 ENDIF

! check square for back
 IF playerturn=1 & pos1=22 THEN
      pos1 = pos1 - dice1 - dice2
	    PAUSE 1000
      TONE 400,200
      GR.HIDE op1
      GR.bitmap.DRAW op1, bp1, cx[pos1], cy[pos1]
	    GR.RENDER
 ENDIF
 IF playerturn=2 & pos2=22 THEN
     pos2 = pos2 - dice1 - dice2
	   PAUSE 1000
     TONE 800,200
     GR.HIDE op2
     GR.bitmap.DRAW op2, bp2, cx[pos2] + 5, cy[pos2]+ 5
	   GR.RENDER
 ENDIF

! check square for card, Player1
 IF playerturn=1 & (pos1=4 | pos1=11 | pos1=19 | pos1=26) THEN
  ! pick random card
  GOSUB effect
  crd = CEIL(RND() * 10)
  GR.color 255, 255, 155, 25, 1
  GR.HIDE comobj2
  GR.text.DRAW comobj2, 8, 1180, card$[crd]
  GR.RENDER 
  GR.color 255, 255, 255, 255, 1
 IF crd = 1 THEN
         money1 = money1 + 20
		 GOSUB showmoney
         GOSUB checkmoney
       ENDIF
 IF crd = 2 THEN
            PAUSE 600
            loseround1 = loseround1+2
            pos1 = 8
            money1 = money1 - 50
            GOSUB showmoney
			GOSUB checkmoney
            TONE 400,200
            GR.HIDE op1
            GR.bitmap.DRAW op1, bp1, cx[pos1], cy[pos1]
			GR.RENDER
         ENDIF
 IF crd = 3 THEN
             money1 = money1 - 20
             GOSUB showmoney
			       GOSUB checkmoney
           ENDIF
 IF crd = 4 THEN plagain1 = plagain1+1
 IF crd = 5 THEN
            money1 = money1 + 50
		        GOSUB showmoney
            GOSUB checkmoney
          ENDIF
 IF crd = 6 THEN loseround1 = loseround1+1
 IF crd = 7 THEN plagain1 = plagain1+2
 IF crd = 8 THEN
        PAUSE 600
        loseround1 = loseround1+2
        pos1 = 8
        money1 = money1-50
	      	GOSUB showmoney
        GOSUB checkmoney
        TONE 400,200
        GR.HIDE op1
        GR.bitmap.DRAW op1, bp1, cx[pos1], cy[pos1]
	     	GR.RENDER
      ENDIF
 IF crd = 9 THEN
    money1 = money1 - 50
	GOSUB showmoney
    GOSUB checkmoney
    ENDIF
 IF crd = 10 THEN
              PAUSE 600
              pos1 = 15
              TONE 400,200
              GR.HIDE op1
              GR.bitmap.DRAW op1, bp1, cx[pos1], cy[pos1]
			         GR.RENDER
            ENDIF
 GR.RENDER 
 ENDIF % *** for card, player1 ***

! check square for card, Player2
 IF playerturn=2 & (pos2=4 | pos2=11 | pos2=19 | pos2=26) THEN
  ! pick random card
  GOSUB effect
  crd = CEIL(RND() * 10)
  GR.color 255, 255, 155, 25, 1
  GR.HIDE comobj2
  GR.text.DRAW comobj2, 8, 1180, card$[crd]
  GR.RENDER 
  GR.color 255, 255, 255, 255, 1
 IF crd = 1 THEN
         money2 = money2 + 20
		     GOSUB showmoney
         GOSUB checkmoney
         
       ENDIF
 IF crd = 2 THEN
            PAUSE 600
            TONE 800,200
            loseround2 = loseround2+2
            pos2 = 8
            money2 = money2 - 50
			       GOSUB showmoney
            GOSUB checkmoney
            GR.HIDE op2
            GR.bitmap.DRAW op2, bp2, cx[pos2]+5, cy[pos2]+5
		        GR.RENDER
         ENDIF
 IF crd = 3 THEN
             money2 = money2 - 20
		        	 GOSUB showmoney
             GOSUB checkmoney
           ENDIF
 IF crd = 4 THEN plagain2 = plagain2+1
 IF crd = 5 THEN
            money2 = money2 + 50
			      GOSUB showmoney
            GOSUB checkmoney
          ENDIF
 IF crd = 6 THEN loseround2 = loseround2+1
 IF crd = 7 THEN plagain2 = plagain2+2
 IF crd = 8 THEN
         PAUSE 600
         TONE 800,200
         loseround2 = loseround2+2
         pos2 = 8
         money2 = money2 - 50   
		    GOSUB showmoney
	      GOSUB checkmoney
         GR.HIDE op2
         GR.bitmap.DRAW op2, bp2, cx[pos2]+5, cy[pos2]+5
		     GR.RENDER
      ENDIF
 IF crd = 9 THEN
      money2 = money2 - 50
	    GOSUB showmoney
      GOSUB checkmoney
    ENDIF
 IF crd = 10 THEN
              PAUSE 600
              pos2 = 15
              TONE 400,200
              GR.HIDE op2
              GR.bitmap.DRAW op2, bp2, cx[pos2]+5, cy[pos2]+5
		          GR.RENDER
            ENDIF
 GR.RENDER 

 ENDIF % *** for card, player2 ***


! check square IF other's player property
! check players money
 IF playerturn = 1 THEN
        IF propr2[pos1]> 0 THEN
		            GOSUB checkdoubleproperty
                    money1 = money1 - factor*rent[pos1]
                    money2=money2+factor*rent[pos1]
                    GOSUB showmoney
                    GOSUB checkmoney
               ENDIF
     ENDIF
 IF playerturn = 2 THEN
        IF propr1[pos2]>0  THEN
		                GOSUB checkdoubleproperty
                    money2 = money2 - factor*rent[pos2]
                    money1=money1+factor*rent[pos2]
                    GOSUB showmoney
                    GOSUB checkmoney
               ENDIF
     ENDIF

ENDIF % for dice player 1 or 2

! buy
IF choosed$="b" & diceok=0 THEN 
         GR.HIDE comobj2
         GR.text.DRAW comobj2,8,1180,"you must roll dices"
         GR.RENDER
         goto start
     ENDIF
IF choosed$="b" & diceok=1 & playerturn=1 THEN 
                  buyok=1
                  PAUSE 300	 
                  IF money1<cost[pos1] THEN buyok=0
		          IF pos1=1 | pos1=4 | pos1=6 | pos1=8 | pos1=11 THEN buyok=0
   	              IF pos1=13 | pos1=15 | pos1=17 | pos1=19 | pos1=22  | pos1=24 | pos1=26 THEN buyok=0
                  IF propr1[pos1]>0 | propr2[pos1]>0 THEN buyok=0
                  IF pos1=2 & propr2[3]>0 THEN buyok=0
                  IF pos1=3 & propr2[2]>0 THEN buyok=0
                  IF pos1=5 & propr2[7]>0 THEN buyok=0
                  IF pos1=7 & propr2[5]>0 THEN buyok=0
                  IF pos1=9 & propr2[10]>0 THEN buyok=0
                  IF pos1=10 & propr2[9]>0 THEN buyok=0
                  IF pos1=12 & propr2[14]>0 THEN buyok=0
                  IF pos1=14 & propr2[12]>0 THEN buyok=0
                  IF pos1=16 & propr2[18]>0 THEN buyok=0
                  IF pos1=18 & propr2[16]>0 THEN buyok=0
                  IF pos1=20 & propr2[21]>0 THEN buyok=0
                  IF pos1=21 & propr2[20]>0 THEN buyok=0
                  IF pos1=23 & propr2[25]>0 THEN buyok=0
                  IF pos1=25 & propr2[23]>0 THEN buyok=0
                  IF pos1=27 & propr2[28]>0 THEN buyok=0
                  IF pos1=28 & propr2[27]>0 THEN buyok=0    
          IF buyok=0 THEN
           GR.HIDE comobj2
           GR.color 255,0,255,0,1
           GR.text.DRAW comobj2, 8, 1180, "U can't buy that property!"
           GR.RENDER
           GR.color 255,255,255,255,1 
	       ELSE 
              money1=money1-cost[pos1]
              propr1[pos1]=1 
              GR.HIDE comobj2
              GR.color 255,0,255,0,1
              GR.text.DRAW comobj2, 8, 1180, "U bought a new property!"
    	         bposx=cx[pos1]
	            bposy=cy[pos2]
			  IF pos1>0 & pos1<8 THEN 
									bposx=cx[pos1]+30
									bposy=cy[pos1]+10
								 ENDIF
			  IF pos1>8 & pos1<15 THEN
									bposx=cx[pos1]+80
									bposy=cy[pos1]+50
								 ENDIF
			  IF pos1>15 & pos1<22 THEN
									bposx=cx[pos1]+30
									bposy=cy[pos1]+70
								 ENDIF
			  IF pos1>22 & pos1<29 THEN
                 bposx=cx[pos1]-20
									bposy=cy[pos1]+40
								 ENDIF
              GR.bitmap.DRAW bop1,bip1,bposx,bposy
              GOSUB showmoney
			  GR.RENDER
       GR.color 255,255,255,255,1 
     ENDIF
choosed$=""
ENDIF %here

!buy        
IF choosed$="b" & diceok=1 & playerturn=2 THEN 	
    PAUSE 300
    buyok=1	 
    IF money2<cost[pos2] THEN buyok=0
	IF pos2=1 | pos2=4 | pos2=6 | pos2=8 | pos2=11 THEN buyok=0
   	IF pos2=13 | pos2=15 | pos2=17 | pos2=19 | pos2=22  | pos2=24 | pos2=26 THEN buyok=0
    IF propr2[pos2]>0 | propr1[pos2]>0 THEN buyok=0
    IF pos2=2 & propr1[3]>0 THEN buyok=0
    IF pos2=3 & propr1[2]>0 THEN buyok=0
    IF pos2=5 & propr1[7]>0 THEN buyok=0
    IF pos2=7 & propr1[5]>0 THEN buyok=0
    IF pos2=9 & propr1[10]>0 THEN buyok=0
    IF pos2=10 & propr1[9]>0 THEN buyok=0
    IF pos2=12 & propr1[14]>0 THEN buyok=0
    IF pos2=14 & propr1[12]>0 THEN buyok=0
    IF pos2=16 & propr1[18]>0 THEN buyok=0
    IF pos2=18 & propr1[16]>0 THEN buyok=0
    IF pos2=20 & propr1[21]>0 THEN buyok=0
    IF pos2=21 & propr1[20]>0 THEN buyok=0
    IF pos2=23 & propr1[25]>0 THEN buyok=0
    IF pos2=25 & propr1[23]>0 THEN buyok=0
    IF pos2=27 & propr1[28]>0 THEN buyok=0
    IF pos2=28 & propr1[27]>0 THEN buyok=0
                  
    IF buyok=0 THEN
                GR.HIDE comobj2
                GR.color 255,0,255,0,1 
                GR.text.DRAW comobj2, 8, 1180, "U can't buy that property"            
				GR.RENDER
                GR.color 255,255,255,255,1 
	        ELSE 
              money2=money2-cost[pos2]
              propr2[pos2]=1 
              GR.HIDE comobj2
              GR.color 255,0,255,0,1 
              GR.text.DRAW comobj2, 8, 1180, "U bought a new property!"
              bposx=cx[pos1]
			        bposy=cy[pos2]
			  IF pos2>0 & pos2<8 THEN 
									bposx=cx[pos2]+30
									bposy=cy[pos2]+10
								 ENDIF
			  IF pos2>8 & pos2<15 THEN
									bposx=cx[pos2]+80
									bposy=cy[pos2]+50
								 ENDIF
			  IF pos2>15 & pos2<22 THEN
									bposx=cx[pos2]+30
									bposy=cy[pos2]+70
								 ENDIF
			  IF pos2>22 & pos21<29 THEN
                bposx=cx[pos2]-20
									bposy=cy[pos2]+40
								 ENDIF
              GR.bitmap.DRAW bop2,bip2,bposx,bposy
			  GR.RENDER
              GR.color 255,255,255,255,1 
             GOSUB showmoney
       ENDIF
       GR.RENDER
    
ENDIF %1

! not choosed Dice yet
IF choosed$="d" & diceok=0 THEN 
         GR.HIDE comobj2
         GR.text.DRAW comobj2,8,1180,"you must roll dices"
         GR.RENDER
         goto start
     ENDIF

! Choosed Next	 
IF choosed$="n" & diceok=1 THEN
                   diceok=0
                   IF playerturn=1 THEN playerturn=2 ELSE playerturn=1
				   IF loseround1>0 & loseround2>0 & loseround1>=loseround2 THEN 
				        loseround1=loseround1-loseround2
						loseround2=0
				   ENDIF
				   IF loseround1>0 & loseround2>0 & loseround1<loseround2 THEN 
				        loseround2=loseround2-loseround1
					    	loseround1=0
				   ENDIF
				   IF playerturn=1 & loseround1>0 & plagain1>0 & plagain1>=loseround1 THEN
				      plagain1=plaigain1-loseround1
				  	  loseround1=0
				   ENDIF
				   IF playerturn=1 & loseround1>0 & plagain1>0 & plagain1<loseround1 THEN
				      loseround1=loseround1-plagain1
					    plagain1=0
				   ENDIF
				   IF playerturn=2 & loseround2>0 & plagain2>0 & plagain2>=loseround2 THEN
				      plagain2=plaigain2-loseround2
					   loseround2=0
				   ENDIF
				     IF playerturn=2 & loseround2>0 & plagain2>0 & plagain2<loseround2 THEN
				      loseround2=loseround2-plagain2
					   plagain2=0
				   ENDIF  
						
				   IF playerturn=1 & loseround1>0 THEN
                                 loseround1=loseround1-1
				                          playerturn=2
     				ENDIF
		IF playerturn=2 & loseround2>0 THEN
                                loseround2=loseround2-1
	                              playerturn=1
				ENDIF
		IF playerturn=1 & plagain2>0 THEN 
                                playerturn=2
                                plagain2=plagain2-1
		      ENDIF
		IF playerturn=2 & plagain1>0 THEN 
                                playerturn=1
                                plagain1=plagain1-1 
		      ENDIF
		GR.HIDE comobj
        IF playerturn=1 THEN plays$=plr1$ ELSE plays$=plr2$
           GR.text.DRAW comobj,8,1110,"Plays Player: "+plays$
           GR.RENDER
           GOSUB showmoney
        ENDIF
! help
IF choosed$="h" THEN
        PAUSE 300
        GR.bitmap.DRAW  op3,bip3, 90,90
        GR.RENDER
   DO
     touched = -1
     GR.touch touched, x, y
  UNTIL touched>0
  PAUSE 200
  GR.HIDE op3
  GR.RENDER
  GR.bitmap.DRAW  op5,bip5, 90,90
  GR.RENDER
  DO
   touched = -1
   GR.touch touched, x, y
  UNTIL touched>0
  GR.HIDE op5
  GR.RENDER
choosed$=""
ENDIF

!quit
IF choosed$="q" THEN
        PAUSE 300
        CLS
        GR.close
        print "Thanks for playing!"
        end
ENDIF
GOTO start

!******* end of main *******

! *** Subroutines ***

! ****************************
! TOUCHME
! selects player's choice
touchme:
retouch:
choosed$ = ""
IF playerturn=2 & computergame=1  & computerdice=0 THEN
                choosed$="d"
                computerdice=1
                RETURN
              ENDIF
IF playerturn=2 & computergame=1 & computerdice=1 THEN               
                GOSUB buyornot
                choosed$="n"  
                computerdice=0             
                RETURN
              ENDIF
DO
   touched = -1
   GR.touch touched, x, y
UNTIL touched>0
IF y<830*scaley | y>890*scaley | x>700*scalex THEN goto retouch
IF x <= 145 * scalex THEN choosed$ = "d"                    
IF x>145*scalex & x<280*scalex THEN choosed$="b"
IF x>=280*scalex & x<430*scalex THEN choosed$="n"
IF x>=430*scalex & x<555*scalex THEN choosed$="h"  
IF x>=555*scalex & x<700*scalex THEN choosed$="q"
RETURN

! **************************** 
! THROW DICE
rolldice:
GR.color 255, 255, 255, 255, 1
dice1=CEIL(rnd()*6))
dice2=CEIL(rnd()*6))
     FOR i = 1 TO 10
        GR.HIDE obj1
        GR.HIDE obj2
        GR.HIDE obj3
        GR.HIDE obj4
        fakedice1=CEIL(rnd()*6))
        fakedice2=CEIL(rnd()*6))
        f1$ = replace$(STR$(fakedice1), ".0", "")
        f2$ = replace$(STR$(fakedice2), ".0", "")
        GR.color 255, 255, 255, 255, 1
        GR.text.DRAW obj1, 710, 878, f1$
        GR.text.DRAW obj2, 759, 878, f2$
        GR.RENDER
        TONE 800,200
        PAUSE 100
        GR.HIDE obj1
        GR.HIDE obj2
      NEXT i
d1$ = replace$(STR$(dice1), ".0", "")
d2$ = replace$(STR$(dice2), ".0", "")
GR.color 255, 255, 0, 0, 1
GR.HIDE obj3
GR.HIDE obj4
GR.text.DRAW obj3, 710, 878, d1$
GR.text.DRAW obj4, 759, 878, d2$
GR.RENDER
diceok=1
PAUSE 600
RETURN
 
! **************************** 
!SHOWMONEY
showmoney:
  GR.HIDE cptr3
  GR.HIDE cptr4
  GR.color 255, 0, 0, 255, 1
  m$ = replace$(STR$(money1), ".0", "")
  GR.text.DRAW cptr3, 290, 980, m$
  m$ = replace$(STR$(money2), ".0", "")
  GR.text.DRAW cptr4, 290, 1040, m$
  GR.RENDER
  GR.color 255, 255, 255, 255, 1
RETURN

 
! ****************************
!CHECKMONEY IF > win or < 0 THEN gameend

checkmoney:
IF (playerturn=1 & money1 >= win) | (playerturn=2 & money2 <= 0) THEN
  GOSUB showmoney
  GR.HIDE comobj2
  GR.text.DRAW comobj2,8,1180, " **** "+ Plr1$+" WINS ****"
  TTS.init
  TTS.speak "player one wins"
  GR.HIDE comobj
  GR.text.DRAW comobj,8,1110,"Touch Screen to continue"
  GR.RENDER
  DO
   touched = -1
   GR.touch touched, x, y
UNTIL touched>0
  GOSUB clearscreen
  goto restart
  ENDIF
IF (playerturn=1 & money1 <= 0) | (playerturn=2 & money2 >= win) THEN
  GOSUB showmoney
  GR.HIDE comobj2
  GR.text.DRAW comobj2, 8, 1180, " **** "+ Plr2$+" WINS ****"
  TTS.init
  TTS.speak "player two wins"
  GR.HIDE comobj
  GR.text.DRAW comobj,8,1110,"Touch Screen to continue"
  GR.RENDER
  DO
   touched = -1
   GR.touch touched, x, y
  UNTIL touched>0
  GOSUB clearscreen
  goto restart
  ENDIF
RETURN

! ****************************
! effect for card 
effect: 
 FOR i=1 to 4
    GR.bitmap.DRAW op4,bip4,330,465
    GR.RENDER
    PAUSE 300
    GR.HIDE op4
    GR.RENDER
    PAUSE 300
 NEXT i
RETURN

! ****************************
! coins sound
coins:
AUDIO.load au1, "coins.wav"
AUDIO.play au1
PAUSE 800
AUDIO.stop
RETURN 

! ****************************
! strategy for buy or not, first check
buyornot:
buyok=1
IF money2<cost[pos2] THEN buyok=0
		  IF pos2=1 | pos2=4 | pos2=6 | pos2=8 | pos2=11 THEN buyok=0
   	  IF pos2=13 | pos2=15 | pos2=17 | pos2=19 | pos2=22  | pos2=24 | pos2=26 THEN buyok=0
                  IF propr2[pos2]>0 | propr1[pos2]>0 THEN buyok=0
                  IF pos2=2 & propr1[3]>0 THEN buyok=0
                  IF pos2=3 & propr1[2]>0 THEN buyok=0
                  IF pos2=5 & propr1[7]>0 THEN buyok=0
                  IF pos2=7 & propr1[5]>0 THEN buyok=0
                  IF pos2=9 & propr1[10]>0 THEN buyok=0
                  IF pos2=10 & propr1[9]>0 THEN buyok=0
                  IF pos2=12 & propr1[14]>0 THEN buyok=0
                  IF pos2=14 & propr1[12]>0 THEN buyok=0
                  IF pos2=16 & propr1[18]>0 THEN buyok=0
                  IF pos2=18 & propr1[16]>0 THEN buyok=0
                  IF pos2=20 & propr1[21]>0 THEN buyok=0
                  IF pos2=21 & propr1[20]>0 THEN buyok=0
                  IF pos2=23 & propr1[25]>0 THEN buyok=0
                  IF pos2=25 & propr1[23]>0 THEN buyok=0
                  IF pos2=27 & propr1[28]>0 THEN buyok=0
                  IF pos2=28 & propr1[27]>0 THEN buyok=0
                  			  
				 trnd=CEIL(rnd()*2)	
			  
        IF (buyok=1 & money2>=250) | (buyok=1 & money2<250 & money2>=150 & trnd=1 ) THEN
                
              money2=money2-cost[pos2]
              propr2[pos2]=1 
              GR.HIDE comobj2
              GR.color 255,0,255,0,1
              GR.text.DRAW comobj2, 8, 1180, "android has new property"
              GR.color 255,255,255,255,1
              bposx=cx[pos1]
		       	  bposy=cy[pos2]
			  IF pos2>0 & pos2<8 THEN 
									bposx=cx[pos2]+30
									bposy=cy[pos2]+10
								 ENDIF
			  IF pos2>8 & pos2<15 THEN
									bposx=cx[pos2]+80
									bposy=cy[pos2]+50
								 ENDIF
			  IF pos2>15 & pos2<22 THEN
									bposx=cx[pos2]+30
									bposy=cy[pos2]+70
								 ENDIF
			  IF pos2>22 & pos21<29 THEN
                 bposx=cx[pos2]-20
								  bposy=cy[pos2]+40
								 ENDIF

   GR.bitmap.DRAW bop2,bip2,bposx,bposy 
   GR.RENDER
   GOSUB showmoney
 ENDIF
RETURN

! ****************************
! choose type of game 
chooseplay:

 GR.bitmap.DRAW op6, bip6, 88,200
 GR.text.draw objc1, 10,1000," created by [Antonis]"
 GR.text.draw objc2, 10,1100, "    on Nov.2011"
 GR.RENDER
 retouc:
 DO
   touched = -1
   GR.touch touched, x, y
 UNTIL touched>0
 IF x < 160*scalex | x>600*scalex | y<310*scaley | y>450*scaley THEN goto retouc
 IF y<380*scaley THEN computergame=1 ELSE computergame=0 
  GR.set.stroke 13
 TTS.init
 IF computergame=1 THEN 
             GR.line  lobj1,630, 370, 650, 340 
             GR.line  lobj2,620, 350, 635, 370 
             GR.render       
             TTS.speak "player against android selected"
			 GR.HIDE lobj1
			 GR.HIDE lobj2
          ELSE 
           GR.line  lobj3,630, 440, 650, 410 
           GR.line  lobj4,620, 420, 635, 440 
           GR.render
           TTS.speak "player against player selected"
		   GR.HIDE lobj3
		   GR.HIDE lobj4
ENDIF

 GR.HIDE objc1
 GR.HIDE objc2
 GR.set.stroke 0
 GR.hide op6
 GR.RENDER
RETURN
 
! ****************************
clearscreen:
  GR.CLS
  GR.RENDER
RETURN

! ****************************
! checkdoubleproperty & pay double
checkdoubleproperty:
IF playerturn=1 THEN 
   factor=1  
   IF (pos1=3 & propr2[2]>0) |  (pos1=5 & propr2[7]>0) THEN factor=2
   IF (pos1=7 & propr2[5]>0) |  (pos1=9 & propr2[10]>0) THEN factor=2
   IF (pos1=10 & propr2[9]>0) | (pos1=12 & propr2[14]>0)  THEN factor=2
   IF (pos1=14 & propr2[12]>0) | (pos1=16 & propr2[18]>0) THEN factor=2
   IF (pos1=18 & propr2[16]>0) | (pos1=20 & propr2[21]>0) THEN factor=2
   IF (pos1=21 & propr2[20]>0) | (pos1=23 & propr2[25]>0) THEN factor=2
   IF (pos1=25 & propr2[23]>0) | (pos1=27 & propr2[28]>0) THEN factor=2
   IF pos1=28 & propr2[27]>0 THEN factor=2  
ENDIF
IF playerturn=2 THEN 
   factor=1  
   IF (pos2=3 & propr1[2]>0) |  (pos2=5 & propr1[7]>0) THEN factor=2
   IF (pos2=7 & propr1[5]>0) |  (pos2=9 & propr1[10]>0) THEN factor=2
   IF (pos2=10 & propr1[9]>0) | (pos2=12 & propr1[14]>0)  THEN factor=2
   IF (pos2=14 & propr1[12]>0) | (pos2=16 & propr1[18]>0) THEN factor=2
   IF (pos2=18 & propr1[16]>0) | (pos2=20 & propr1[21]>0) THEN factor=2
   IF (pos2=21 & propr1[20]>0) | (pos2=23 & propr1[25]>0) THEN factor=2
   IF (pos2=25 & propr1[23]>0) | (pos2=27 & propr1[28]>0) THEN factor=2
   IF pos2=28 & propr1[27]>0 THEN factor=2  
ENDIF 
RETURN
! ****************************
 
 
 
 
 
 
