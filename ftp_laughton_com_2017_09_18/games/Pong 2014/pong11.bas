REM Start of BASIC! Program

soundpool.open 4
soundpool.load schl, "blips.mp3"
soundpool.load bande, "wsp.mp3"
soundpool.load out, "klaxons.mp3"
soundpool.load wand, "boing.mp3"
soundpool.load won, "won.mp3"

audio.load birds, "birds.mp3"
audio.play birds
audio.volume 0,0
audio.loop

gr.open 255, 255, 255, 255
gr.orientation 0
gr.color 255,255,0,0,1
gr.Text.align 2
gr.Text.size 40
gr.set.stroke 10

di_height=680
di_width=1270
gr.screen actual_w,actual_h
sw= actual_w/ di_width
sh= actual_h/ di_height*sw
gr.scale sw, sh

gr.bitmap.load feld, "feldrfo.jpg"
gr.bitmap.load pong,"rfopng.png"
gr.bitmap.load schlager, "schlagertr.png"
gr.bitmap.load ball, "balltr.png"
gr.bitmap.load tic, "tic.gif"

array.load go[], 10*sw,340*sh,80*sw,380*sh
array.load pref[], 10*sw,140*sh,80*sw,180*sh
array.load music[], 10*sw,440*sh,80*sw,480*sh
array.load pause[], 10*sw,540*sh,80*sw,580*sh
array.load stop[], 10*sw,640*sh,80*sw,680*sh
array.load score[], 10*sw,240*sh,80*sw,280*sh

gr.bitmap.draw img1, feld, 0, 0
s = 292
v = 0.9
ytic=450

gr.bitmap.draw imgp, pong, 0, 0
gr.render

do
gr.touch touched,x,y
until touched
lef=0
rig=0
For a=255 to 1 step -4
gr.modify imgp,"alpha",a
gr.render
lef=lef+0.01
rig=rig+0.01
audio.volume 0+lef,0+rig
next a

audio.volume 0.9,0.9
gr.hide imgp
gr.bitmap.draw img2, schlager, 1125, s
gr.bitmap.draw img3, ball, 625, 326
gr.render
gr.bitmap.delete pong

do

w = 625
sk = 326
n = 10  % speed x
g = 10
m = 0  % speed y
p = 0
punkte = 0
points = 0
z = 0  % 10er Punkte
grenze = 1060

gr.Text.draw points,50,70,"0"
gr.render


do 

gr.bounded.touch stop,stop[1],stop[2],stop[3],stop[4]
gr.bounded.touch go,go[1], go[2],go[3],go[4]
gr.bounded.touch pref,pref[1],pref[2],pref[3],pref[4]
gr.bounded.touch score,score[1],score[2],score[3],score[4]
gr.bounded.touch music,music[1],music[2],music[3],music[4]

sw.begin 1
sw.case music
pause 100
if v = 0.9
v=0
audio.volume 0,0
gr.line q, 10,440,80,480
gr.render
elseif v = 0
audio.volume 0.9,0.9
v=0.9
gr.hide q
gr.render
endif
sw.break

sw.case stop
exit
sw.break

sw.case pref
gr.bitmap.load pref, "pref.gif"
gr.bitmap.create empty,1680,660
gr.bitmap.drawinto.start empty

gr.bitmap.draw img5, pref, 280, 110

gr.bitmap.delete pref

gr.bitmap.drawinto.end
gr.bitmap.draw scrptr,empty,0,0
gr.render


gr.bitmap.load tic, "tic.gif"
if bi=1
ytic=450
elseif ea=1
ytic=370
elseif ha=1
ytic=290
elseif ci=1
ytic=210
elseif no=1
ytic=530
endif
gr.bitmap.draw img6, tic, 318, ytic
gr.render

do 
gr.bounded.touch ok, 700*sw,460*sh,850*sw ,560*sh
gr.bounded.touch city,318*sw,205*sh,378*sw,265*sh
gr.bounded.touch hart,318*sw,285*sh,378*sw,345*sh
gr.bounded.touch eagle,318*sw,365*sh,378*sw,425*sh
gr.bounded.touch birds,318*sw,445*sh,378*sw,505*sh
gr.bounded.touch none,318*sw,525*sh,378*sw,585*sh

if birds = 1
bi=1
audio.stop
audio.load birds, "birds.mp3"
audio.play birds
audio.loop
gr.hide img6
gr.bitmap.draw img6, tic, 318, 450
gr.render
birds = 0

elseif eagle = 1
ea=1
audio.stop
audio.load eagle, "eagle.mp3"
audio.play eagle
audio.loop
gr.hide img6
gr.bitmap.draw img6, tic, 318, 370
gr.render
eagle = 0

elseif hart = 1
ha=1
audio.stop
audio.load hart, "hart.mp3"
audio.play hart
audio.loop
gr.hide img6
gr.bitmap.draw img6, tic, 318, 290
gr.render
hart = 0

elseif city = 1
ci=1
audio.stop
audio.load city, "city.mp3"
audio.play city
audio.loop
gr.hide img6
gr.bitmap.draw img6, tic, 318, 210
gr.render
city = 0

elseif none = 1
no=1
audio.stop
gr.hide img6
gr.bitmap.draw img6, tic, 318, 530
gr.render
none = 0
endif

until ok

gr.hide img6
gr.bitmap.delete tic
gr.hide scrptr
gr.render
sw.break

sw.case score
gr.bitmap.load dialog, "dialog.png"
gr.bitmap.create empty,1680,660
gr.bitmap.drawinto.start empty

gr.bitmap.draw img4, dialog, 280, 110
gr.bitmap.delete dialog

gr.Text.align 3
gr.Text.size 30

Text.open r,scor,"score.txt"
nl=0
list.create s,liste

do
nl=nl+1
Text.readln scor,line$
list.add liste,line$
until line$ = "EOF"

list.toarray liste,lines$[]
array.sort lines$[]
array.reverse lines$[]

if nl > 10 then nl = 11

for i = 2 to nl
ma=ma+1
print lines$[i]
gr.Text.draw img4,845,173+(ma*41),lines$[i]
next

ma = 0
undim lines$[]
gr.Text.align 2
gr.Text.size 40

gr.bitmap.drawinto.end
gr.bitmap.draw scrptr,empty,0,0
gr.render
score = 0

do 
gr.bounded.touch touched, 280,110,1030,610
until touched

gr.hide scrptr
gr.render

sw.break
sw.end

until go = 1

do

gr.touch sch, x, s
s /= sh
x /= sw

gr.bounded.touch pause,pause[1],pause[2],pause[3],pause[4]

gr.bounded.touch music,music[1],music[2],music[3],music[4]

gr.bounded.touch stop,stop[1],stop[2],stop[3],stop[4]

w = w + n
sk = sk + m

if w >= grenze & sk < s-118
grenze = 1280

elseif w >= grenze & sk > s+68
grenze = 1280

elseif m = 0
m = round(rnd()*(-2)+1)

elseif w >= grenze
soundpool.play streamID1,schl,v,v,1,0,1
p = p + 1
punkte = p + z
m = round((sk+35-s)/4)
n = -g-round(punkte/2)
gr.modify points,"text",int$(punkte)

elseif stop = 1
exit

elseif pause = 1
do
gr.bounded.touch go,go[1], go[2],go[3],go[4]
until go = 1

elseif music = 1 & v = 0.9
v = 0
audio.volume 0,0
gr.line q, 10,440,80,480

elseif music = 1 & v = 0
v = 0.9
audio.volume 0.9,0.9
gr.hide q

elseif p = 10
z = z + 10
soundpool.play streamID1,won,v,v,1,0,1
p = 0

elseif w < 115
n = -n
soundpool.play streamID1,wand,v,v,1,0,1

elseif sk <= 5 | sk >= 650
m = -m
soundpool.play streamID1,bande, v,v,1,0,1

elseif w < 114 & sk > 649
print w
print sk

endif

if s>650 then s=650
if s<68 then s=68

gr.modify img3, "x", w, "y", sk
gr.modify img2, "y", s-68
gr.render

until w > 1280
if stop = 1
D_U.break
endif
soundpool.play streamID1,out, v,v,1,0,1

TIME y$,m$,d$,h$,min$,s$
result$ = format$("##%",punkte)

Text.open a, scor,"score.txt"
Text.writeln scor,result$"             "y$"-"m$"-"d$"     "h$" : "min$
Text.close scor

Pause 100
gr.modify img2, "y", 292
gr.modify img3,"y",326,"x",625
gr.modify points,"text",""
gr.render

until stop=1
exit
