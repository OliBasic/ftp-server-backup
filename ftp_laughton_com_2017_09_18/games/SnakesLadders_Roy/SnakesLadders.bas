Rem Snakes and Ladders 
Rem For Android
Rem With RFO Basic!
Rem July / August 2016
Rem Version 1.01
Rem By Roy Shepherd

di_height = 1152 % set to my Device
di_width = 672

gr.open 255,128,128,255
gr.orientation 1 % portrate 
pause 1000
WakeLock 3
 
gr.screen screenWidth, screenHeight
scale_x = screenWidth / di_width
scale_y = screenHeight / di_height
gr.scale scale_x, scale_y

!-------------------------------------------------
!Let Basic see the functions before they are called
!-------------------------------------------------
gosub Functions

gosub Initialise
gosub SetUp
gosub OneOrTwoPlayer

gosub PlayGame
gosub SaveData
exit

onBackKey:
    dialog.message "Exit",,yn,"Yes","No"
    if yn = 1 then gosub SaveData : exit
back.resume 
end

!------------------------------------------------
! Do once at first run of game
!------------------------------------------------
Initialise:
!Set some variables 
  FALSE = 0 : TRUE = 1
    path$ = "../../SnakesLadders/data/"
    sb = di_height - 45  : bb = di_height - 5
    music = 1 : sound = 1 : buzz = 1
    player1 = 0 : player2 = 0
    onePlayer = 1 : delay = 200
   gosub LoadData
   
    !Sound and Buzz
    soundpool.open 8
    soundpool.load soundClick,path$ + "buttonClick.wav"
    soundpool.load soundDice, path$ + "diceClater.wav"
    soundpool.load soundUp, path$ + "runUpLadder.wav"
    soundpool.load soundSix, path$ + "sixScored.wav"
    soundpool.load soundCounter, path$ + "slide.wav"
    soundpool.load soundDown, path$ + "slideDownSnake.wav"
    soundpool.load soundTooMeny, path$ + "tooMany.wav"
    soundpool.load soundWinner, path$ + "winner.wav"
  
    array.load buzzGame[], 1, 100
    
    audio.load TheDays, Path$ + "TheDays.mp3" 
	  if TheDays = FALSE then e$ = GETERROR$() : ?e$ : end
    
    !Top and bottom bar
    gr.color 255,79,79,255
    gr.rect null, 0,0,di_width, 55
    gr.rect null, 0,di_height - 50,di_width, di_height
    
    !Bottom Buttons
    gr.text.size 27
    gr.color 255,128,128,255
    gr.rect butMusic, 20, sb, 170, bb
    gr.rect butSound, 180, sb, 330, bb
    gr.rect butBuzz, 340, sb, 490, bb
    gr.rect butReset, 500, sb, 650, bb
    
    !Player one and two buttons
    gr.color 255,79,79,255
    gr.rect butPlayer1, 10, 900,230,950
    gr.rect butPlayer2, (di_width / 2) + 110, 900,di_width - 10,950
    
    !Text on bottom buttons
    gr.text.align 2 : gr.color 255,255,255,255
    if music then m$ = "Music: OFF" else m$ = "Music On"
    gr.text.draw txtMusic, 95,bb - 10,m$
    if sound then s$ = "Sound Off" else s$ = "Sound On"
    gr.text.draw txtSound, 255,bb - 10, s$
    if buzz then b$ ="Buzz Off" else b$ = "Buzz On"
    gr.text.draw txtBuzz, 415,bb - 10,b$
    gr.text.draw txtReset, 575,bb - 10,"Reset Wins"
    
    !Text on one and two buttons
    gr.text.align 1 : gr.text.size 30
    gr.text.draw txtPlayer1, 20,938,"Player1 Throw"
    gr.text.draw txtPlayer2, (di_width / 2) + 120,938, "Player2 Throw"
    
    !Player one and two text for scores at top of screen
    gr.text.align 1 : gr.text.size 35
    gr.text.draw txtPlayer1Score, 10,40, "Player One: " + int$(player1)
    gr.text.align 3
    gr.text.draw txtPlayer2Score, di_width - 10,40, "Player Two: " + int$(player2)
    
    !Load and scale the Snakes and Ladders board
    gr.bitmap.load bg, path$ + "bg.jpg"
    gr.bitmap.scale bg, bg, di_width, di_width
    gr.bitmap.draw bg, bg, 0, 50
    
    !Load and scale board counters
    gr.bitmap.load c1, path$ + "c1.png"
    gr.bitmap.load c2, path$ + "c2.png"
    gr.bitmap.scale c1, c1,80, 80
    gr.bitmap.scale c2, c2,80, 80
    gr.bitmap.draw c1, c1, 70, 750
    gr.bitmap.draw c2, c2, 530, 750
    
    !Load and scale the dice
    dim dice[6]
    for d = 1 to 6
      gr.bitmap.load dd, path$ + "dice0"+int$(d)+".png"
      gr.bitmap.scale dice[d], dd, 200,200
      gr.bitmap.draw dice[d],dice[d],(di_width / 2) - 100,750
    next
    
    !Draw info box
    gr.color 255,255,255,255,0
    gr.rect null, 100,970,di_width - 100, 1100
    gr.color 255,79,79,255,1
    gr.rect null, 300,954,di_width - 300, 984
    
    gr.text.align 2 : gr.color 255,255,255,255,1
    gr.text.draw null, di_width / 2, 980, "Info"
    gr.text.draw txtInfo,di_width / 2, 1020, "One or Two Player Game?"
    
    gr.color 255,79,79,255,1
    gr.rect butLeft, 110,1050, (di_width / 2) - 10, 1090
    gr.rect butRight, (di_width / 2) + 10, 1050, di_width - 110, 1090
    
    gr.text.align 2 : gr.color 255,255,255,255,1
    gr.text.draw txtLeft, 220, 1080, "One"
    gr.text.draw txtRight,450, 1080, "Two"
    
    
    !Using gr_collision  for when a button is tapped
    gr.color 0,0,0,0
    gr.point collision, - 1, - 1 
    gr.render
    gosub CounterList : gosub LadderList
    gosub SnakeList : gosub StartMusic
return

!------------------------------------------------
! Make the lists for the counters to move around the board
!------------------------------------------------
CounterList:
    list.create n, xPos
    list.create n, yPos
    
    list.add xPos, 50, 160, 280, 385, 500, 620, ~
                  620, 500, 385, 280, 160, 50, ~
                  50, 160, 280, 385, 500, 620, ~
                  620, 500, 385, 280, 160, 50, ~
                  50, 160, 280, 385, 500, 620
                  
    list.add yPos, 650, 650, 650, 650, 650, 650, ~
                   515, 515, 515, 515, 515, 515, ~
                   380, 380, 380, 380, 380, 380, ~
                   250, 250, 250, 250, 250, 250, ~
                   110, 110, 100, 100, 100, 100
return

!------------------------------------------------
! Make the lists for the counters climb the ladders
!------------------------------------------------
LadderList:
  list.create n, xLad1
  list.create n, yLad1
  
  list.create n, xLad2
  list.create n, yLad2
  
  list.create n, xLad3
  list.create n, yLad3
  
  list.create n, xLad4
  list.create n, yLad4
  
  list.add xLad1, 280, 280, 280, 280
  list.add yLad1, 650, 515, 380, 250
  
  list.add xLad2, 500, 500
  list.add yLad2, 650, 515
  
  list.add xLad3, 160, 160, 160, 160
  list.add yLad3, 515, 380, 250, 110
  
  list.add xLad4, 500, 500
  list.add yLad4, 250, 100
return

!------------------------------------------------
! Make the lists for the counters to slide down the snaks
!------------------------------------------------
SnakeList:
  list.create n, xSnake1
  list.create n, ySnake1
  
  list.create n, xSnake2
  list.create n, ySnake2
  
  list.create n, xSnake3
  list.create n, ySnake3
  
  list.create n, xSnake4
  list.create n, ySnake4
  
  list.add xSnake1, 515, 515, 485, 440, 390, 370,385
  list.add ySnake1, 350, 440, 515, 500, 515,570,650
  
  list.add xSnake2, 620, 645, 600, 620, 620
  list.add ySnake2, 250, 320, 380, 450, 515
  
  list.add xSnake3, 365, 375, 385, 335, 280, 270, 290, 385
  list.add ySnake3, 250, 310, 380, 400, 380, 450, 490, 515
  
  list.add xSnake4, 280, 280, 260, 220, 160, 100, 50, 50, 40, 30, 40, 40, 50
  list.add ySnake4, 110,180, 240, 270, 250, 230, 250, 315, 380, 445, 515, 580, 650
return

!------------------------------------------------
! Do at start of each new game
!------------------------------------------------
SetUp:
  gameOver = FALSE
  counter1 = 0 : counter2 = 0
  oldCounter1 = 0 : oldCounter2 = 0
  player1Six = 0 : player2Six = 0
 
return

!------------------------------------------------
! If a one player game then the device will play as player two
!------------------------------------------------
OneOrTwoPlayer:
  flag = 0
  do 
    gr.touch t, tx, ty
    if t then 
      do : gr.touch t, tx, ty : until ! t
      tx /= scale_x : ty /= scale_y
      gr.modify collision, "x", tx, "y", ty : gr.render
      if gr_collision(collision, butLeft) then 
        onePlayer = 1 : flag = 1
        if buzz then vibrate buzzGame[], -1
        call PlaySound(sound,soundClick)
      endif
      if gr_collision(collision, butRight) then 
        onePlayer = 0 : flag = 1
        if buzz then vibrate buzzGame[], -1
        call PlaySound(sound,soundClick)
      endif
      if ! flag then gosub BottomButton
    endif
  until flag 
  gr.hide txtLeft : gr.hide txtRight
  gr.hide butLeft : gr.hide butRight
return

!------------------------------------------------
! Play the game until ether player one or player two 
! get to the end of the board (place 30)
!------------------------------------------------
PlayGame:
  do
    gosub PlayerOneTurn
    if onePlayer & ! gameOver then
      gosub DeviceTurn
    else
     if ! gameOver then gosub PlayerTwoTurn
    endif
    if gameOver then gosub AnnounceWinner
  until gameOver

return

!------------------------------------------------
! Player one throws the dice
!------------------------------------------------
PlayerOneTurn:
    gr.hide txtPlayer2
    gr.show txtPlayer1
    if ! player1Six then
      gr.modify txtInfo, "text", "Player one throw a six"
    else
      gr.modify txtInfo, "text", "Player one throw"
    endif
    gr.render
    flag=0
    do 
    gr.touch t, tx, ty
    if t then 
      do : gr.touch t, tx, ty : until ! t
      tx /= scale_x : ty /= scale_y
      gr.modify collision, "x", tx, "y", ty : gr.render
      if gr_collision(collision, txtPlayer1) then 
        if buzz then vibrate buzzGame[], -1
        call PlaySound(sound,soundClick)
        diceNum = ThrowDice(dice[],player1Six, sound, soundDice)
        if diceNum <> 6 then flag = 1
         
        if player1Six & counter1 + diceNum < 31 then 
          gosub MoveCounterOne
          gosub OnLadder
          gosub OnSnake
        elseif player1Six & counter1 + diceNum > 30 then
          gosub BackTrackCounterOne
          gosub OnLadder
          gosub OnSnake
        endif
        
        if ! player1Six & diceNum = 6 then
          gosub Counter1ToBoard
          gr.modify txtInfo, "text", "Player one throw"
          gr.render
        endif
        if diceNum <> 6 then flag = 1
        if counter1 = 30 then flag = 1 : gameOver = TRUE
      endif
      if ! flag then gosub BottomButton
    endif
  until flag
return

!------------------------------------------------
! If a one player game then plaer one plays the device
! the device throws the dice
!------------------------------------------------
DeviceTurn:
  gr.hide txtPlayer1
  gr.show txtPlayer2
  pause 1000 : flag = 0
    
  gr.modify txtPlayer2, "text", "Device Throw"
  if ! player2Six then
     gr.modify txtInfo, "text", "I'll try to throw a six"
  else
     gr.modify txtInfo, "text", "I'll throw now"
  endif
  gr.render
  do
    diceNum = ThrowDice(dice[], player2Six, sound, soundDice) 
    !if player2Six & counter2 + diceNum > 30 then call PlaySound(sound, soundTooMeny)
    if player2Six & counter2 + diceNum < 31 then
      gosub MoveCounterTwo
      gosub OnLadder
      gosub OnSnake
    elseif player2Six & counter2 + diceNum > 30 then
          gosub BackTrackCounterTwo
          gosub OnLadder
          gosub OnSnake
    endif
    
    if ! player2Six & diceNum = 6 then 
      gosub Counter2ToBoard 
      gr.modify txtInfo, "text", "I'll throw again"
      gr.render : pause 1000
    endif
    if diceNum <> 6 then flag = 1
    if counter2 = 30 then flag = 1 : gameOver = TRUE
  until flag
return

!------------------------------------------------
! If a two player game then player two throws the dice. 
!------------------------------------------------
PlayerTwoTurn:
  gr.hide txtPlayer1
  gr.show txtPlayer2
    if ! player2Six then 
      gr.modify txtInfo, "text", "Player two throw a six"
    else
      gr.modify txtInfo, "text", "Player two throw"
    endif
    gr.render
    flag=0
    do 
    gr.touch t, tx, ty
    if t then 
      do : gr.touch t, tx, ty : until ! t
      tx /= scale_x : ty /= scale_y
      gr.modify collision, "x", tx, "y", ty : gr.render
      if gr_collision(collision, txtPlayer2) then 
        if buzz then vibrate buzzGame[], -1
        call PlaySound(sound,soundClick)
        diceNum = ThrowDice(dice[], player2Six,sound, soundDice)
       ! if player2Six & counter2 + diceNum > 30 then call PlaySound(sound, soundTooMeny)
        if player2Six & counter2 + diceNum < 31 then 
          gosub MoveCounterTwo
          gosub OnLadder
          gosub OnSnake
       elseif player2Six & counter2 + diceNum > 30 then
          gosub BackTrackCounterTwo
          gosub OnLadder
          gosub OnSnake
        endif
        
        if ! player2Six & diceNum = 6 then 
          gosub Counter2ToBoard 
          gr.modify txtInfo, "text", "Player two throw"
          gr.render
        endif
        if diceNum <> 6 then flag = 1
        if counter2 = 30 then flag = 1 : gameOver = TRUE
      endif
      if ! flag then gosub BottomButton
    endif
  until flag 
return

!------------------------------------------------
! Move player one counter onto the board
!------------------------------------------------
Counter1ToBoard:
  call PlaySound(sound, soundSix)
  player1Six = TRUE
  list.get xPos, 1, sx
  list.get yPos, 1, sy
  gr.modify c1, "x", sx - 20, "y", sy - 20
  gr.render
  counter1 = 1 : oldCounter1 = 1
return

!------------------------------------------------
! Move player Two counter onto the board
!------------------------------------------------
Counter2ToBoard:
  call PlaySound(sound, soundSix)
  player2Six = TRUE
  list.get xPos, 1, sx
  list.get yPos, 1, sy
  gr.modify c2, "x", sx - 35, "y", sy - 35
  gr.render
  counter2 = 1 : oldCounter2 = 1
return

!------------------------------------------------
! Move player one's counter along the board
!------------------------------------------------
MoveCounterOne:
  if diceNum = 6 then call PlaySound(sound, soundSix)
  for m = oldCounter1 to oldCounter1 + diceNum 
    call PlaySound(sound, soundCounter)
    list.get xPos, m, sx
    list.get yPos, m, sy
    gr.modify c1, "x", sx - 20, "y", sy - 20
    gr.render
    pause delay
  next
 
  counter1 += diceNum : oldCounter1 = counter1
return

!------------------------------------------------
! Move player one's counter along the board to 30 then back track
!------------------------------------------------
BackTrackCounterOne:
  if diceNum = 6 then call PlaySound(sound, soundSix)
  boardEnd = 30 - counter1
  for m = oldCounter1 to oldCounter1 + boardEnd 
    call PlaySound(sound, soundCounter)
    list.get xPos, m, sx
    list.get yPos, m, sy
    gr.modify c1, "x", sx - 20, "y", sy - 20
    gr.render
    pause delay
  next
  oldCounter1 = 30
  goBack = diceNum - boardEnd
  
  for m = oldCounter1 to oldCounter1 - goback step - 1 
    call PlaySound(sound, soundCounter)
    list.get xPos, m, sx
    list.get yPos, m, sy
    gr.modify c1, "x", sx - 20, "y", sy - 20
    gr.render
    pause delay
  next
 
  counter1 = 30 - goBack : oldCounter1 = counter1
return

!------------------------------------------------
! Move player two's counter along the board to 30 then back track
!------------------------------------------------
BackTrackCounterTwo:
  if diceNum = 6 then call PlaySound(sound, soundSix)
  boardEnd = 30 - counter2
  for m = oldCounter2 to oldCounter2 + boardEnd 
    call PlaySound(sound, soundCounter)
    list.get xPos, m, sx
    list.get yPos, m, sy
    gr.modify c2, "x", sx - 35, "y", sy - 35
    gr.render
    pause delay
  next
  oldCounter2 = 30
  goBack = diceNum - boardEnd
 
  for m = oldCounter2 to oldCounter2 - goback step - 1 
    call PlaySound(sound, soundCounter)
    list.get xPos, m, sx
    list.get yPos, m, sy
    gr.modify c2, "x", sx - 35, "y", sy - 35
    gr.render
    pause delay
  next
 
  counter2 = 30 - goBack : oldCounter2 = counter2
return

!------------------------------------------------
! Move player two's counter along the board
!------------------------------------------------
MoveCounterTwo:
  if diceNum = 6 then call PlaySound(sound, soundSix)
  for m = oldCounter2 to oldCounter2 + diceNum 
    call PlaySound(sound, soundCounter)
    list.get xPos, m, sx
    list.get yPos, m, sy
    gr.modify c2, "x", sx - 35, "y", sy - 35
    gr.render
    pause delay
  next
  counter2 += diceNum : oldCounter2 = counter2
return

!------------------------------------------------
! If a counter stops at the bottom of a ladder, the counter climb up the ladder
!------------------------------------------------
OnLadder:
  counterFlag = 0
  if counter1 = 3 | counter1 = 5 | counter1 = 11 | counter1 = 20 then
    counter = counter1 : counterFlag = 1
  endif
  
  if counter2 = 3 | counter2 = 5 | counter2 = 11 | counter2 = 20 then
    counter = counter2 : counterFlag = 2
  endif
  
  if counterFlag then
    call PlaySound(sound, soundUp)
    sw.begin counter
      sw.case 3
        for upLadder = 1 to 4
          list.get xLad1, upLadder, ladX
          list.get yLad1, upLadder, ladY
          if counterFlag = 1 then 
            gr.modify c1, "x", ladX - 20, "y", ladY - 20
          else
            gr.modify c2, "x", ladX - 35, "y", ladY - 35
          endif
          gr.render
          pause delay
        next
        topLadder = 22
      sw.break
      
      sw.case 5
        for upLadder = 1 to 2
          list.get xLad2, upLadder, ladX
          list.get yLad2, upLadder, ladY
          if counterFlag = 1 then 
            gr.modify c1, "x", ladX - 20, "y", ladY - 20
          else
            gr.modify c2, "x", ladX - 35, "y", ladY - 35
          endif
          gr.render
          pause delay
        next
        topLadder = 8
      sw.break
      
      sw.case 11
        for upLadder = 1 to 4
          list.get xLad3, upLadder, ladX
          list.get yLad3, upLadder, ladY
         if counterFlag = 1 then 
            gr.modify c1, "x", ladX - 20, "y", ladY - 20
          else
            gr.modify c2, "x", ladX - 35, "y", ladY - 35
          endif
          gr.render
          pause delay
        next
        topLadder = 26
      sw.break
      
      sw.case 20
        for upLadder = 1 to 2
          list.get xLad4, upLadder, ladX
          list.get yLad4, upLadder, ladY
          if counterFlag = 1 then 
            gr.modify c1, "x", ladX - 20, "y", ladY - 20
          else
            gr.modify c2, "x", ladX - 35, "y", ladY - 35
          endif
          gr.render
          pause delay
        next
        topLadder = 29
      sw.break
    sw.end
    
    if counterFlag = 1 then
      counter1 = topLadder : oldCounter1 = counter1
    else
      counter2 = topLadder : oldCounter2 = counter2
    endif
    
  endif
return

!------------------------------------------------
! If a counter stops at a snakes head, the counter slides down the stake
!------------------------------------------------
OnSnake:
  counterFlag = 0 
  if counter1 = 17 | counter1 = 19 | counter1 = 21 | counter1 = 27 then
    counter = counter1 : counterFlag = 1
  endif
  
  if counter2 = 17 | counter2 = 19 | counter2 = 21 | counter2 = 27 then
    counter = counter2 : counterFlag = 2
  endif
  
  if counterFlag then
    call PlaySound(sound, soundDown)
    sw.begin counter
      sw.case 17
        for downSnake = 1 to 7
          list.get xSnake1, downSnake, snakeX
          list.get ySnake1, downSnake, snakeY
          if counterFlag = 1 then 
            gr.modify c1, "x", snakeX - 20, "y", snakeY - 20
          else
            gr.modify c2, "x", snakeX - 35, "y", snakeY - 35
          endif
          gr.render
          pause delay
        next
        bottomSnake = 4
      sw.break
      
      sw.case 19
        for downSnake = 1 to 5
          list.get xSnake2, downSnake, snakeX
          list.get ySnake2, downSnake, snakeY
          if counterFlag = 1 then 
            gr.modify c1, "x", snakeX - 20, "y", snakeY - 20
          else
            gr.modify c2, "x", snakeX - 35, "y", snakeY - 35
          endif
          gr.render
          pause delay
        next
        bottomSnake = 7
      sw.break
      
      sw.case 21
        for downSnake = 1 to 8
          list.get xSnake3, downSnake, snakeX
          list.get ySnake3, downSnake, snakeY
          if counterFlag = 1 then 
            gr.modify c1, "x", snakeX - 20, "y", snakeY - 20
          else
            gr.modify c2, "x", snakeX - 35, "y", snakeY - 35
          endif
          gr.render
          pause delay
        next
        bottomSnake = 9
      sw.break
      
      sw.case 27
        for downSnake = 1 to 13
          list.get xSnake4, downSnake, snakeX
          list.get ySnake4, downSnake, snakeY
          if counterFlag = 1 then 
            gr.modify c1, "x", snakeX - 20, "y", snakeY - 20
          else
            gr.modify c2, "x", snakeX - 35, "y", snakeY - 35
          endif
          gr.render
          pause delay
        next
        bottomSnake = 1
      sw.break
    sw.end
    
    if counterFlag = 1 then
      counter1 = bottomSnake : oldCounter1 = counter1
    else
      counter2 = bottomSnake : oldCounter2 = counter2
    endif
    
  endif
return

!------------------------------------------------
! Announce the winner and ask if another game is required 
!------------------------------------------------
AnnounceWinner:
  call PlaySound(sound, soundWinner)
  if counter1 = 30 then 
    winer = 1 : player1 ++
    gr.modify txtPlayer1Score, "text", "Player One: " + int$(player1)
  else 
    winer = 2 : player2 ++
    gr.modify txtPlayer2Score, "text", "Player Two: " + int$(player2)
  endif
  gr.render
  do
    dialog.message "Game Over Player " + int$(winer) + " Wins","Another Game?",yn,"Yes","No"
  until yn > 0
  if yn = 1 then 
    gr.modify c1, "x", 70, "y", 750
    gr.modify c2, "x", 530, "y", 750
    gosub SetUp : gr.render
  endif
return

!------------------------------------------------
! Test to see if a bottom botton has been tapped
!------------------------------------------------
BottomButton:
  gr.touch t, tx, ty 
  tx /= scale_x : ty /= scale_y
  gr.modify collision, "x", tx, "y", ty : gr.render
  if gr_collision(collision,butMusic) then gosub ModMusic
  if gr_collision(collision,butSound) then gosub ModSound
  if gr_collision(collision,butBuzz) then gosub ModBuzz
  if gr_collision(collision,butReset) then gosub ModReset
return

!-------------------------------------------------
! Start background music at start of game, if music = TRUE
!-------------------------------------------------
StartMusic:
if music then
    audio.play TheDays 
    audio.loop 
  endif 
return

!------------------------------------------------
! Turn music on and off
!------------------------------------------------
ModMusic:
  if buzz then vibrate buzzGame[], -1
  call PlaySound(sound,soundClick)
  if music then
    music = FALSE
    audio.stop 
    gr.modify txtMusic, "text", "Music: On"
  else
    music = TRUE
    audio.play TheDays 
    audio.loop 
    gr.modify txtMusic, "text", "Music: Off"
  endif
  gr.render
return

!------------------------------------------------
! Turn sound on and off
!------------------------------------------------
ModSound:
  if buzz then vibrate buzzGame[], -1
  call PlaySound(sound,soundClick)
  if sound then
    sound = FALSE
    gr.modify txtSound, "text", "Sound: On"
  else
    sound = TRUE
    gr.modify txtSound, "text", "Sound: Off"
  endif
  gr.render 
return

!------------------------------------------------
! Turn buzz on and off
!------------------------------------------------
ModBuzz:
  if buzz then vibrate buzzGame[], -1
  call PlaySound(sound,soundClick)
  if buzz then
    buzz = FALSE
    gr.modify txtBuzz, "text", "Buzz: On"
  else
    buzz = TRUE
    gr.modify txtBuzz, "text", "Buzz: Off"
  endif
  gr.render
return

!------------------------------------------------
! Reset the player's scores to zero 
!------------------------------------------------
ModReset:
  if buzz then vibrate buzzGame[], -1
  call PlaySound(sound,soundClick)
  do : dialog.message "Reset Players Scores to Zero?",,yn,"Yes","No" : until yn > 0
  if buzz then vibrate buzzGame[], -1
  call PlaySound(sound,soundClick)
  if yn = 1 then 
    player1 = 0 : player2 = 0 
    gr.modify txtPlayer1Score, "text", "Player One: " + int$(player1)
    gr.modify txtPlayer2Score, "text", "Player Two: " + int$(player2)
  endif
  gr.render
return


!-------------------------------------------------
!Save music, sound, buzz and score. At end of game
!-------------------------------------------------
SaveData:
	Path$="../../SnakesLadders/data/"
	file.exists pathPresent,Path$+"GameData.txt"
	if ! pathPresent then file.mkdir Path$
  
		text.open w,hs,Path$+"GameData.txt"
    text.writeln hs,int$(player1)
    text.writeln hs,int$(player2)
    text.writeln hs,int$(music)
		text.writeln hs,int$(sound)
    text.writeln hs,int$(buzz)
		text.close hs
return

!-------------------------------------------------
! Load music, sound, buzz and scores . At start of game
!-------------------------------------------------
LoadData:
	Path$="../../SnakesLadders/data/"
	file.exists pathPresent,Path$+"GameData.txt"
	if pathPresent then
    text.open r,hs,Path$+"GameData.txt"
    text.readln hs,player1$
    text.readln hs,player2$
    text.readln hs,music$
    text.readln hs,sound$
    text.readln hs,buzz$
    text.close hs
    player1 = val(player1$)
    player2 = val(player2$)
    music = val(music$)
    sound = val(sound$)
    buzz = val(buzz$)
	endif
return


!------------------------------------------------
! Functions inside this gosub
!------------------------------------------------
Functions:

!------------------------------------------------
! if sound is on. Play sound (ptr)
!------------------------------------------------
fn.def PlaySound(sound, ptr)
    if sound then Soundpool.play s,ptr,0.99,0.99,1,0,1
fn.end

!------------------------------------------------
! Throw the dice and return the result 
!------------------------------------------------
  fn.def ThrowDice(dice[],six, sound, soundDice)
    oldThrow = 0
    
    for loop = 1 to 10
      do
        newThrow = FLOOR(6 * RND() + 1)
      until newThrow <> oldThrow
      call PlaySound(sound, soundDice)
      oldThrow = newThrow
      if ! six & loop = 10 then 
        if newThrow = 1 | newThrow = 3 then newThrow = 6
      endif
      for show = 1 to 6
        if show = newThrow then BringToFront(dice[newThrow])
        gr.render : pause 50
      next
    next
    
    fn.rtn newThrow
  fn.end
  
!-------------------------------------------------
! Thanks to Gilles for this function
! Bring a bitmap to the front 
!-------------------------------------------------
Fn.def BringToFront( ptr )  % ptr is the pointer to become the last one into DL.
  gr.getdl ndl[],1
  array.length sz, ndl[]
  if !ptr | sz = 1 | ndl[sz] = ptr then array.delete ndl[] : Fn.rtn 0
  array.search ndl[],ptr,n
  if n
    if ndl[n] =ptr
      for nn=n to sz-1
        ndl[nn] = ndl[nn+1]
      next
      ndl[sz] = ptr
      GR.NEWDL ndl[]
    endif
  endif
  array.delete ndl[]
Fn.end
  
return


