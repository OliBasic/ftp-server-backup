! ColourMe.bas
! Aat Don @2013
GR.OPEN 255,0,0,0,0,0
GR.SCREEN w,h
ScaleX=720
ScaleY=w/h*ScaleX
sx=h/ScaleX
sy=w/ScaleY
GR.SCALE sx,sy
WAKELOCK 3
DataDir$="../../ColourMe/data/"
GR.BITMAP.LOAD Choice,DataDir$+"Choice.png"
AUDIO.LOAD Intro,DataDir$+"waitsong.mp3"
AUDIO.LOAD ColPress,DataDir$+"MagicWand.mp3"
AUDIO.LOAD Finito,DataDir$+"Kids.mp3"
ARRAY.LOAD S[],0,19,3,16,8,20,24,0,28,0,10,28,6,28
LIST.CREATE n,L1
LIST.ADD.ARRAY L1,S[]
DIM PicDone[9]
FILE.DIR DataDir$,F$[]
ARRAY.LENGTH l,F$[]
FOR i=1 TO l
  fst=ASCII(LEFT$(F$[i],1))-48
  IF fst>0 & fst<10 THEN
    PicDone[fst]=fst
  ENDIF
NEXT i
UNDIM F$[]
MakeSnd=1

DO
  GR.CLS
  GR.BITMAP.DRAW g,Choice,0,0
  GR.COLOR 255,255,0,0,1
  FOR i=1 TO 9
    IF PicDone[i]>0 THEN GR.POLY sign,L1,(MOD(PicDone[i]-1,3)+1)*240-30,(FLOOR((PicDone[i]-1)/3)+1)*240-30
  NEXT i
  GR.COLOR 200,0,0,0,1
  GR.SET.STROKE 5
  GR.LINE Cr1,460,250,504,286
  GR.LINE Cr2,460,286,504,250
  GR.SET.STROKE 0
  UNDIM Spot[]
  READ.FROM 1
  IF MakeSnd=1 THEN
    GR.HIDE Cr1
    GR.HIDE Cr2
  ELSE
    GR.SHOW Cr1
    GR.SHOW Cr2
  ENDIF
  GR.RENDER
  PicNr=5
  DO
    DO
      DO
        GR.TOUCH touched,x,y
      UNTIL touched
      DO
        GR.TOUCH touched,x,y
      UNTIL !touched
      x=ROUND(x/sx)
      y=ROUND(y/sy)
    UNTIL x>0 & x<720 & y>0 & y<720
    If x>280 & x<444 & y>250 & y<285 THEN
      IF MakeSnd=0 THEN
        MakeSnd=1
        GR.HIDE Cr1
        GR.HIDE Cr2
        GR.RENDER
      ELSE
        MakeSnd=0
        GR.SHOW Cr1
        GR.SHOW Cr2
        GR.RENDER
      ENDIF
    ENDIF
    If x>280 & x<444 & y>427 & y<462 THEN
      IF makeSnd THEN AUDIO.STOP
      GR.CLOSE
      WAKELOCK 5
      EXIT
    ENDIF
    PicNr=CEIL(x/240)+CEIL(y/240)*3-3
  UNTIL PicNr<>5
  IF makeSnd THEN
    AUDIO.STOP
    AUDIO.PLAY Intro
  ENDIF
  SW.BEGIN PicNr
    SW.CASE 1
      Pic$="HappyFrog.dat"
    SW.BREAK
    SW.CASE 2
      Pic$="Smurfs.dat"
    SW.BREAK
    SW.CASE 3
      Pic$="Nijntje.dat"
    SW.BREAK
    SW.CASE 4
      Pic$="Bashful.dat"
    SW.BREAK
    SW.CASE 6
      Pic$="Calimero.dat"
    SW.BREAK
    SW.CASE 7
      Pic$="Disney.dat"
    SW.BREAK
    SW.CASE 8
      Pic$="Robin.dat"
    SW.BREAK
    SW.CASE 9
      Pic$="Pooh.dat"
    SW.BREAK
  SW.END
  PicDone[PicNr]=PicNr
  GR.CLS
  GR.SET.ANTIALIAS 0
  GR.COLOR 255,255,255,0,1
  GR.TEXT.SIZE 50
  GR.TEXT.DRAW g,20,350,"Creating picture......"
  GR.RENDER
  GR.TEXT.SIZE 30
  GR.COLOR 255,255,255,255,1
  GR.RECT g,0,0,720,720
  ! Read Sheet
  ARRAY.DELETE Seg$[]
  GRABFILE Parts$,DataDir$+Pic$
  SPLIT Seg$[],Parts$,"#"
  ARRAY.LENGTH SheetLen,Seg$[]
  DIM Spot[SheetLen-2,2]
  SPLIT Segment$[],Seg$[1]
  ARRAY.LENGTH ArLen,Segment$[]
  FOR i=1 TO ArLen STEP 2
      Spot[(i+1)/2,1]=VAL(Segment$[i])
      Spot[(i+1)/2,2]=VAL(Segment$[i+1])
  NEXT i
  ARRAY.DELETE Segment$[]
  GR.COLOR 255,0,0,0,1
  ! Draw outline
  Part=2
  GOSUB DrawPart
  FOR i=0 TO 7
    READ.NEXT r,g,b
    GR.COLOR 255,r,g,b,1
    GR.RECTANGLE g,740,i*80,820,75+i*80
    READ.NEXT r,g,b
    GR.COLOR 255,r,g,b,1
    GR.RECTANGLE g,830,i*80,910,75+i*80
    GR.SET.STROKE 5
    GR.COLOR 255,255,255,255,0
    GR.RECTANGLE g,740,i*80,820,75+i*80
    GR.RECTANGLE g,830,i*80,910,75+i*80
    GR.SET.STROKE 0
  NEXT i
  GR.TEXT.DRAW TapTxt,740,680,"TAP COLOUR"
  GR.HIDE TapTxt
  GR.RENDER

  FOR i=3 TO SheetLen
    ! Show target cross (recreate to keep on top)
    GOSUB DrawTarget
    GR.BITMAP.DRAW Pnt,Target,Spot[i-2,1]-30,Spot[i-2,2]-30
    GR.SHOW TapTxt
    GR.RENDER
    ! Choose colour
    DO
      DO
        GR.TOUCH touched,x,y
      UNTIL touched
      DO
        GR.TOUCH touched,x,y
      UNTIL !touched
      x=ROUND(x/sx)
      y=ROUND(y/sy)
    UNTIL x>740 & x<910 & y>0 & y<635
    TONE 330,200
    IF makeSnd THEN 
      AUDIO.STOP
      AUDIO.PLAY ColPress
    ENDIF
    IF x>740 & x<825 THEN Col=1
    IF x>825 & x<910 THEN Col=2
    Row=CEIL(y/80)
    Pick=(Col+Row*2-3)*3+1
    READ.FROM Pick
    READ.NEXT r,g,b
    GR.COLOR 255,r,g,b,1
    GR.HIDE Pnt
    GR.BITMAP.DELETE Target
    GR.HIDE TapTxt
    ! Draw filled
    Part=i
    GOSUB DrawPart
  NEXT i
  IF makeSnd THEN 
    AUDIO.STOP
    AUDIO.PLAY Finito
  ENDIF
  GR.COLOR 255,255,255,255,1
  GR.TEXT.DRAW TapTxt,740,680,"THE END!!!"
  GR.RENDER
  Pic$=REPLACE$(FORMAT$("#",PicNr)," ","")+"_"+LEFT$(Pic$,Len(Pic$)-3)+".png"
  GR.SAVE DataDir$+Pic$
  GR.BITMAP.LOAD SP,DataDir$+Pic$
  GR.BITMAP.CROP SN,SP,0,0,h,h
  GR.BITMAP.SAVE SN,DataDir$+Pic$
  GR.BITMAP.DELETE SP
  GR.BITMAP.DELETE SN
  PAUSE 5000
UNTIL 0

READ.DATA 255,0,0,0,255,0,0,0,255,255,255,0,255,0,255,0,255,255,128,0,0,0,128,0,0,0,128,128,128,0,128,0,128,0,128,128,192,0,0,0,192,0,0,0,192,192,192,192

DrawPart:
  SPLIT Segment$[],Seg$[Part]
  ARRAY.LENGTH ArLen,Segment$[]
  FOR q=1 TO ArLen STEP 4
    x1=VAL(Segment$[q])-1
    y1=VAL(Segment$[q+1])
    x2=VAL(Segment$[q+2])+1
    y2=VAL(Segment$[q+3])
    GR.LINE g,x1,y1,x2,y2
  NEXT q
  GR.RENDER
  ARRAY.DELETE Segment$[]
RETURN

DrawTarget:
GR.BITMAP.CREATE Target,60,60
GR.BITMAP.DRAWINTO.START Target
  GR.SET.STROKE 5
  GR.COLOR 255,255,0,0,0
  GR.LINE g,0,29,59,29
  GR.LINE g,29,0,29,59
  GR.CIRCLE g,29,29,20
  GR.CIRCLE g,29,29,10
  GR.COLOR 255,255,0,0,1
  GR.CIRCLE g,29,29,3
GR.BITMAP.DRAWINTO.END
GR.SET.STROKE 0
RETURN