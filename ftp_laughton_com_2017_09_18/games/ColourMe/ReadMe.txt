This colouring picture app is meant for very young children !
One out of eight pictures can be choosen to colourize.
Sound can be muted.
A visor shows which part can be coloured. Tap a colour swatch
to paint with that colour.
A finished picture is saved to your SD-card in ColourMe/data.
The initial shows an indicator on the pictures alraedy done,
choosing one again overwrites the previously saved.

Have fun !