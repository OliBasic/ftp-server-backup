!!

Little game by Didier Mermod

!!
! Open the acclerometer sensor
SENSORS.OPEN 1

! Open Graphics
GR.OPEN 255,0,0,0
GR.ORIENTATION 1 % 0

! Get screen size paramters
GR.SCREEN w,h

! Calculate screen center x, y
cxx = w/2
cyy = h/2


nx=floor(w/20)+1
ny=floor(h/20)+1
dim fe[ny+2]
dim ff[ny+2,2]

! some obstacles...
for j=1 to ny step 4


ff[j,1]=0
ff[j,2]=(0.7+(rnd()*0.2))*w

ff[j+2,1]= (0.1+(rnd()*0.3))*w 
ff[j+2,2]= w

gr.color 255,255,0,0,1
gr.rect fe[j], ff[j,1],j*20, ff[j,2],(j*20)+20
gr.rect fe[j+2], ff[j+2,1],(j+2)*20, ff[j+2,2],((j+2)*20)+20

next

! sphere coords
cx=0
cy=0

level=1

! The main program loop
DO

zz=4
 for j=1 to ny step 4

gr.color 255,255,0,0,1
gr.rect fe[j], ff[j,1],j*20, ff[j,2],(j*20)+(zz)
gr.rect fe[j+2], ff[j+2,1],(j+2)*20, ff[j+2,2],((j+2)*20)+zz
zz=zz+3
if zz>20 then zz=20
next
 



 !Read the acclerometer

sensors.read 1,x,y,z

 !Amplify the magnitudes

x=-x*(2+speed)
y=y*(2+speed)

 cx=cx+x

if cx<10 then cx=10
if cx>w-10 then cx=w-10

 cy=cy+y

if cy<10 then cy=10

! reached level exit
if cy>h-10 

score=score+100
speed=speed+0.25
cx=0
cy=0
pause 500

 gr.cls
gr.color 255,255,255,0,1

gr.text.draw ttt,20, -60+h/2,"Level "+left$(str$(level),len(str$(level))-2) + " successfully mastered."
level=level+1 
gr.text.draw ttt,20, -30+h/2,"Well done!!!"
gr.text.draw ttt,20,h/2, "Tilt top down to start next Level!"
gr.render 

yyy=10
while yyy>0
 sensors.read 1,xxx,yyy,z 
pause 50
if raus=1 then yyy=0
repeat
endif


!busted?


gr.get.pixel cx,cy, alpha,red,green,blue

if (red<>0)
gr.cls
gr.color 255,255,255,0,1
gr.text.draw ttt,20, -20+h/2,"You're busted!"
gr.text.draw ttt,20,h/2, "Tilt top down to restart!"
gr.render
level=1
score=0
speed=0
pause 500
cx=0
cy=0


 yyy=10
while yyy>0
 sensors.read 1,xxx,yyy,z 
pause 50
if raus=1 then yyy=0
repeat 

endif


 ! Daw the Green sphere etc.
 GR.COLOR 255,0,255,0,1

gr.circle r,cx,cy,10
gr.text.size 20
gr.text.bold 1
gr.color 255,0,255,255,1
gr.text.draw tt, w-120,h-4, "Score: "+left$(str$(score), len(str$(score))-2)
gr.text.draw tt, w-200,h-4, "Level: "+left$(str$(level), len(str$(level))-2)
 

 ! Now show what we have drawn
 GR.RENDER

 ! Pause for a moment
 PAUSE 5

 ! Clear the screen and repeat
 GR.CLS

UNTIL raus=1
gr.close
end ""

onbackkey:
raus=1
back.resume



