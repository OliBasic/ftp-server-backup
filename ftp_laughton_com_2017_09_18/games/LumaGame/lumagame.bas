REM lumagame.bas
REM color matching game
REM mookiebearapps 



FN.DEF n()
 ! random factor for plane waves
 x=RND()*0.2
 IF RND()<0.5 THEN x=-x
 FN.RTN x
FN.END

FN.DEF caskyn(p$)
 DIALOG.MESSAGE "luma",p$,c,"yes","no"
 FN.RTN (c=1)
FN.END


FN.DEF info(p$)
 DIALOG.MESSAGE "luma",p$,c,"ok"
 FN.RTN (c=1)
FN.END


FN.DEF flick(g)
 ! shrink rectangle a little
 GR.GET.VALUE g,"left",l,"top",t,"right",r,"bottom",b
 GR.MODIFY g,"left",l+20,"top",t+20,"right",r-20,"bottom",b-20
 GR.RENDER
FN.END

FN.DEF unflick(g)
 ! unshrink rectangle
 GR.GET.VALUE g,"left",l,"top",t,"right",r,"bottom",b
 GR.MODIFY g,"left",l-20,"top",t-20,"right",r+20,"bottom",b+20
 GR.RENDER
FN.END


GR.OPEN 255,0,0,0,0,0
WAKELOCK 2
GR.SCREEN w,h

info("A color gradient and then a second screen with swaps will appear.  Try to figure out which pairs of squares have been swapped.")

LIST.CREATE s, dmenu
LIST.ADD dmenu, "2","3","4","5","6"
DIALOG.SELECT c, dmenu,"difficulty?"
LIST.GET dmenu,c,c$
IF IS_NUMBER(c$)
 difficulty=VAL(c$)
ELSE
 difficulty=3
ENDIF



bx=FLOOR(w/10) % box size
hh=80 % brightness
wx=FLOOR(w/bx):wy=FLOOR(h/bx)
DIM grid[wx,wy]
DIM pos[wx*wy]
GR.SET.ANTIALIAS 0
GR.COLOR 255,255,255,255
d=0.1
FOR y=1 TO wy
 FOR x=1 TO wx
  pos[(y-1)*wx+x]=(y-1)*wx+x
  GR.RECT grid[x,y],(x-1)*bx,(y-1)*bx,x*bx,y*bx
 NEXT
NEXT
GR.RENDER
again:
ag=0

loop:


xb=w*n()
yb=-w*n()
xr=w*n()
yr=w*n()
xg=w*n()
yg=w*n()

% create gradient
FOR y=1 TO wy
 FOR x=1 TO wx
  xc=x/w:yc=y/h
  bb=SIN(xb*xc-yb*yc)+1
  rr=SIN(xr*xc-yr*yc)+1
  gg=SIN(xg*xc+yg*yc)+1
  GR.COLOR 255,hh*rr,hh*gg,hh*bb
  GR.PAINT.GET p:GR.MODIFY grid[x,y],"paint",p
  GR.MODIFY grid[x,y],"alpha",255
 NEXT
NEXT

GR.RENDER


PAUSE 3000

GOSUB mix % shuffle colors

! have user pick pairs

DO
 GOSUB pick
 px1=px
 py1=py
 gg=grid[px,py]
 flick(g)
 DO
  GOSUB pick
  px2=px
  py2=py
 UNTIL grid[px,py]<>gg
 flick(grid[px,py])

 GOSUB Swp % swap colors 

 unflick(grid[px1,py1])
 unflick(grid[px2,py2])
 GOSUB check

 IF bad>0 THEN POPUP INT$(bad)+" out of place"
 IF !Bad 
  POPUP "You win!"
  FOR a=255 TO 1 STEP -8
   FOR y=1 TO wy
    FOR x=1 TO wx
     GR.MODIFY grid[x,y],"alpha",a
    NEXT
   NEXT
   GR.RENDER
  NEXT
  PAUSE 1000
 ENDIF

UNTIL !bad

IF caskyn("play again?")
 GOTO loop
ELSE
 GR.CLOSE
 EXIT
ENDIF

pick:
DO
 GR.TOUCH tt,xx,yy
UNTIL tt

IF tt
 GR.COLOR 0,0,0,0
 GR.CIRCLE c,xx,yy,2
 GR.SHOW c
 GR.RENDER

 f=0
 FOR y=1 TO wy
  FOR x=1 TO wx
   g=grid[x,y]
   IF GR_COLLISION(c,g) 
    px=x
    py=y

    f=1
   ENDIF
   IF f THEN F_N.BREAK
  NEXT
  IF f THEN F_N.BREAK
 NEXT
ENDIF
DO
 GR.TOUCH tt,xx,yy
UNTIL !tt

RETURN


swp: % swap square colors
GR.GET.VALUE grid[px1,py1],"paint",p1
GR.GET.VALUE grid[px2,py2],"paint",p2
GR.MODIFY grid[px1,py1],"paint",p2
GR.MODIFY grid[px2,py2],"paint",p1
GR.RENDER

! keep index in array to see if we have won
SWAP pos[(py1-1)*wx+px1],pos[(py2-1)*wx+px2]

RETURN

mix:
FOR i=1 TO difficulty
 px1=FLOOR(RND()*wx)+1
 px2=FLOOR(RND()*wx)+1
 py1=FLOOR(RND()*wy)+1
 py2=FLOOR(RND()*wy)+1
 GOSUB swp
NEXT
RETURN

! if index array is sorted then we won
check:
ARRAY.COPY pos[],ck[]
ARRAY.SORT ck[]
ARRAY.LENGTH z,ck[]
bad=0
FOR i=1 TO z
 IF pos[i]<>ck[i] THEN bad++
NEXT
RETURN
