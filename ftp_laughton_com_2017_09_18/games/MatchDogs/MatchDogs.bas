Rem Match Dogs
Rem For Androud
Rem Version 1.01
Rem November 2014
Rem Roy
Rem

Rem Open Graphics
di_width = 1280
di_height = 720 

console.title"Match the Dogs"
gr.open 255,0,0,0 % Black
gr.orientation 0
pause 1000
gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y
gr.text.typeface 1
gr.text.size 40

Rem Globel Vars

dim board[5,4]
dim dogs[13] % Ten dogs, number 11 is CardBack.jpg, 12 is NewGame.jpg and 13 is bg.jpg Background
matched=0
trys=0
dim upDate[2] % mached and Guesses
helpb = 0 % For Help
bhelp = 0 % For Help
sound = 1 % for sound on or off

Rem Start of Functions

fn.def SetUpHelp(helpb, bhelp)
	gr.bitmap.create bhelp, 1200, 750    % help screen
	gr.bitmap.drawinto.start bhelp
	gr.color 255,192,192,192,1 % gray
	gr.rect r,180,80,1020,670
	call Gcol(8,1)
	gr.rect r,200,100,1000,650
	
	call Gcol(0,1) % Black text
	x=610
	y=150
	inc=40
	gr.text.align 2
	gr.text.draw d,x,y,"Match the Dogs"
	Y+=inc+20
	gr.text.draw d,x,y,"This is a little memory test"
	y+=inc
	gr.text.draw d,x,y,"game. There are ten dogs"
	y+=inc
	gr.text.draw d,x,y,"playing under the cards."
	y+=inc+20
	gr.text.draw d,x,y,"Tap two cards to turn them"
	y+=inc
	gr.text.draw d,x,y,"over, and if they match they"
	y+=inc
	gr.text.draw d,x,y,"stay right side up, if not they"
	y+=inc
	gr.text.draw d,x,y,"turn back over."
	y+=inc+20

	gr.text.draw d,x,y,"Match all the matching pairs in"
	y+=inc
	gr.text.draw d,x,y,"as few guesses as possible."
	y+=inc+20
	call Gcol(1,1)
	gr.text.draw d,x,y,"Tap to go on"
	gr.text.align 1
	gr.bitmap.drawinto.end
fn.end

fn.def BootBBC()
	gr.cls
	call Gcol(8,1)
	tone 400,300
	tone 600,250
	gr.text.draw d,100,100,"B B C  C o m p u t e r  3 2 K"
	gr.text.draw d, 100,150,"A c o r n  D F S"
	gr.text.draw d,100,200,"B A S I C"
	call Cursor(100,280,2)
	
fn.end

fn.def Cursor(x,y,t)
	gr.text.draw d,x,y,">"
	for n=1 to t
		call Gcol(8,1)
		gr.text.draw d,x+20,y,"__"
		gr.render
		pause 700
		call Gcol(0,1)
		gr.text.draw d,x+20,y,chr$(9608)+chr$(9608)
		gr.render
		pause 700
	next
fn.end

fn.def TypeBBC(x,y,t$)
	call Gcol(8,1)
	s$=""
	for n=1 to len(t$)
	tone 50,100
		s$+=mid$(t$,n,1)+" "
		gr.text.draw d,x,y,s$
		gr.render
		pause 100
	next
	call Cursor(100,y+40,2)
	gr.render
fn.end

fn.def WaitForTouch(scale_x,scale_y)
	do
		gr.touch touched,tx,ty 
	until touched & tx>200*scale_x &ty>100*scale_y &tx<1000*scale_x &ty<650*scale_y
	do
		gr.touch touched,tx,ty 
	until !touched
fn.end

fn.def ShowHelp(helpb, bhelp, scale_x,scale_y)
	gr.bitmap.draw helpb, bhelp, (scale_x)/2, (scale_y)/2
	gr.render
	call WaitForTouch(scale_x,scale_y)
	gr.hide helpb
	gr.render
fn.end

fn.def Gcol(c,g)
	Rem g=0 Outline
	Rem g=1 Fill
	if c=0 then gr.color 255,0,0,0,g % black
	if c=1 then gr.color 255,255,0,0,g % Red
	if c=2 then gr.color 255,0,255,0,g % green
	if c=3 then gr.color 255,0,0,255,g % blue
	if c=4 then gr.color 255,255,255,0,g % yellow
	if c=5 then gr.color 255,0,255,255,g % cyan
	if c=6 then gr.color 255,255,0,255,g % magenta
	if c=7 then gr.color 255,192,192,192,g % gray
	if c=8 then gr.color 255,255,255,255,g % white
fn.end

fn.def DoScore(matched,trys,upDate[])
	call Gcol(0,1)
	gr.text.bold 1
	gr.text.draw upDate[1],50,50,"Matched:"+int$(matched)
	gr.text.draw upDate[2],1000,50,"Guesses:"+int$(trys)
	gr.text.bold 0
	gr.render
fn.end

fn.def UpDateScore(matched,trys,upDate[])
	gr.modify upDate[1],"text","Matched:"+int$(matched)
	gr.modify upDate[2],"text","Guesses:"+int$(trys)
	gr.render
fn.end

Rem load Dog Picturs into dogs[] array and scale them
fn.def LoadDogPic(dogs[])
	for x=1 to 10
		gr.bitmap.load dogs[x], "dog0"+int$(x)+".jpg"
		gr.bitmap.scale dogs[x], dogs[x], 100,100
	next
	gr.bitmap.load dogs[11], "CardBack.jpg"
	gr.bitmap.scale dogs[11], dogs[11], 100,100
	
	gr.bitmap.load dogs[12], "NewGame.jpg"
	gr.bitmap.scale dogs[12], dogs[12], 550,400
	
	gr.bitmap.load dogs[13],"bg.jpg"
	gr.bitmap.scale dogs[13],dogs[13],1280,720
fn.end

fn.def DrawBoard(matched,trys,dogs[],upDate[],sound)
	gr.cls
	!Board
	gr.bitmap.draw d,dogs[13],1,1 % Background
	call Gcol(0,1)
	gr.rect r,90,90,610,510
	gr.color 255,255,255,255,0
	gr.rect r,100,100,600,500
	
	!Top Row
	Call Gcol(1,1)
	call DoScore(matched,trys,upDate[])
	gr.text.bold 1
	call Gcol(1,1)
	gr.text.draw d,450,50,"Match the Dogs"
	gr.text.bold 0
	!Help Buttons
	call Gcol(1,1)
	gr.rect r,200,600,400,650
	call Gcol(0,1)
	gr.text.draw d,260,638,"Help"
	!Sound Button
	call Gcol(1,1)
	gr.rect r,880,600,1080,650
	call Gcol(0,1)
	gr.text.draw d,900,638,"Sound"
	call Gcol(2,1)
	if !sound then call Gcol(0,1)
	gr.circle c,1045,625,20
	gr.render
fn.end

fn.def FillWithCardBacks(board[],dogs[])
	for y=1 to 4
		for x=1 to 5
			if board[x,y]<>0 then gr.bitmap.draw d,dogs[11],x*100,y*100
		next
	next
	gr.render
fn.end

fn.def MixBoard(board[])
bc=1
	for y=1 to 4
		for x=1 to 5
			board[x,y]=bc
			bc++
			if bc=11 then bc=1 
		next
	next
	for mix=1 to 40
		x1=int(5 * rnd()+1)
		y1=int(4 * rnd()+1)
		x2=int(5 * rnd()+1)
		y2=int(4 * rnd()+1)
		swap board[x1,y1],board[x2,y2]
	next
fn.end

fn.def MatchOrNot(board[],ftx,fty,stx,sty)
	m=0
	if board[ftx,fty]=board[stx,sty] then
		board[ftx,fty]=0
		board[stx,sty]=0
		m=1
	endif
	fn.rtn m
fn.end

fn.def PlaySound(sound,mySound)
	if sound then
		!Matched
		if mySound = 1 then
			  for s=500 to 1000 step 200
				tone s,100
			  next
		endif

		!Not Matched
		if mySound = 2 then
		  for s=550 to 200 step -50
			tone s,100
		  next
		endif
	endif
fn.end

fn.def ToddleSound(sound)
	if sound then 
		sound=0 
		call Gcol(0,1)
		gr.circle c,1045,625,20
	else 
		sound=1 
		call Gcol(2,1)
		gr.circle c,1045,625,20
	endif
	gr.render
	fn.rtn sound
fn.end

fn.def PlayMatchDogs(board[],dogs[],matched,trys,di_width,scale_x,scale_y,bhelp,helpb,upDate[],sound)
	firstTouch=0
	secondTouch=0
	ftx=0
	fty=0
	tx=0
	ty=0
	do
		do 
			gr.touch touched,tx,ty
		until touched
	
		do
			gr.touch touched,tx,ty
		until !touched
		if tx>200*scale_x & ty>600*scale_y & tx<400*scale_x & ty<650*scale_y then call ShowHelp(bhelp,helpb,scale_x,scale_y)
		!880,600,1080,650
		if tx>880*scale_x & ty>600*scale_y & tx<1080*scale_x & ty<650*scale_y then sound = ToddleSound(sound)
			
		dx=1 
		dy=1
		!tone 300,500
			for y=100 to 400 step 100
				for x=100 to 500 step 100
					if tx>x*scale_x & ty>y*scale_y & tx<(x+100)*scale_x & ty<(y+100)*scale_y then
						if board[dx,dy]<>0 & firstTouch=1 then
							if dx<>ftx | dy<>fty then
								gr.bitmap.draw d,dogs[board[dx,dy]],x,y
								gr.render
								stx=dx
								sty=dy
								secondTouch=1
							endif
						endif
						if board[dx,dy]<>0 & secondTouch=0 then								
							gr.bitmap.draw d,dogs[board[dx,dy]],x,y
							gr.render
							ftx=dx
							fty=dy
							firstTouch=1
						endif
					endif
					
					dx++
				next
				dx=1
				dy++
			next
			if secondTouch then 
				!tone 500,500
				match = MatchOrNot(board[],ftx,fty,stx,sty)
				if match then 
					matched++
					Call PlaySound(sound,1)
					else
					call PlaySound(sound,2)
				endif
				if !match then 
					pause 2000
					call FillWithCardBacks(board[],dogs[])
				endif
				trys++
				call UpDateScore(matched,trys,upDate[])
				firstTouch=0
				secondTouch=0
				ftx=0
				fty=0
			endif
	until matched=10
fn.end

fn.def NewGame(dogs[],matched,trys,di_width,scale_x,scale_y,helpb,bhelp)
	flag=0
	pause 100
	for x=di_width to di_width-550 step-20
		gr.bitmap.draw d,dogs[12],x,100
		gr.render
	next

	do
		gr.touch touched,tx,ty
		if touched & tx>800*scale_x & ty>400*scale_y & tx<980*scale_x & ty<470*scale_y then flag=1 %Yes New Game
		if touched & tx>1060*scale_x & ty>400*scale_y & tx<1210*scale_x & ty<470*scale_y then flag=2 %No end Geame
		if touched & tx>500*scale_x & ty>600*scale_y & tx<700*scale_x & ty<650*scale_y then
			do
				gr.touch touched,tx,ty 
			until !touched
			call ShowHelp(helpb,bhelp,scale_x,scale_y)
		endif
	until flag
	matched=0
	trys=0
	fn.rtn flag
fn.end

Rem End of Functions

Rem Main

call LoadDogPic(dogs[])
call SetUpHelp(&helpb, &bhelp)
call BootBBC()
call TypeBBC(120,280,"LOAD"+chr$(34)+"MATCH_DOGS"+chr$(34))
call TypeBBC(120,330,"RUN")

do
	call DrawBoard(matched,trys,dogs[],upDate[],&sound)

	call MixBoard(board[])
	
	call FillWithCardBacks(board[],dogs[])

	call PlayMatchDogs(board[],dogs[],matched,trys,di_width,scale_x,scale_y,helpb,bhelp,upDate[],&sound)	

until NewGame(dogs[],&matched,&trys,di_width,scale_x,scale_y,helpb,bhelp)=2

gr.close

onBackKey:

!onError:

exit"Thanks for playing Match the Dogs"
