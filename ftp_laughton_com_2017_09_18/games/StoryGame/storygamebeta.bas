! This is my program, "Story Game," by Adam Tidwell
! (Art886@gmail.com) 
!
! Basic Description: This is a simple text-based game, where each page is written by
! users and uploaded to an online database.
!
! This is currently an experimental game. Tips and comments are welcomed. :-)
!
! Super special thanks to Rev. Jonathan C. Watt for writing the word wrap function and
! saving me a ton of time!

! Open Graphics Screen
gr.open 255, 255, 255, 255
gr.orientation -1
gr.front 1
gr.screen screenwidth, screenheight
gr.color 255, 0, 0, 0, 0

! Pagedata$[] is the string array that holds data for each page
! from the online database.
dim pagedata$[10]

! Options$[] is the string array that holds the values for each of the
! page's options
dim options$[5]

! Load Functions
Gosub functions

! This is where the program restarts from each time a new page has been added
thestart:
gr.cls

! Pregame Variables
TextSize = 40
gr.text.size TextSize
list.create S,  goingonline
array.delete options$[]
dim options$[5]
Options$[1] = "Continue"
Options$[2] = "Continue"
Options$[3] = "Continue"
Options$[4] = "Continue"
Options$[5] = "Continue"

! Just one option for now
NumberOfOptions=1

! I know I need to splurge and buy my own website, but until then,
! my friend has been kind enough to let me put my php code on his site.
url$ = "http://www.sftheatre.net/StoryGame/TheGame.php"

Body$ = ""
Fpath$ = "Begin"
Filename$ = ""
PageText$=" Welcome to 'Continue the story, Beta.' An internet connection is required for this version to work properly. Press continue when you are ready."

! Main Loop ****************************************************************************

do

! We take the page's text and break it into strings that fit on the screen
bookmark2:
ww=wordwrap(PageText$,screenwidth)
list.add ww, " "        % add a line break
list.add ww, " "        % add another line break
list.size ww, Pagesize  % find out how many lines are in the list

! Clear the screen and draw as many lines as will fit
! "PagePoint" is the line from which to start. When the user scrolls down,
! the line from which to start displaying the page text increases
gr.cls                  
for i = PagePoint+1 to Pagesize
 list.get ww, i, pageline$
 gr.text.draw tmppointer, 1, textsize*(i-PagePoint), pageline$
 if i-pagepoint > round(screenheight/textsize)-1 then i = pagesize
next i

! Just a random variable to account for how much extra space we get from
! making options buttons with larger texts bigger.
extraspace = 0

! Draw all of the options buttons and fill them with text:
for f = 1 to numberofoptions
  gr.color 255, 0, 0, 0, 0

  ww2=wordwrap(options$[f],screenwidth/2)
  list.size ww2, buttontextsize

  extraspace = extraspace + buttontextsize  

  left=   screenwidth/4
  top=    (textsize)*((pagesize-pagepoint)+(f-1)+(extraspace-buttontextsize))
  right=  (screenwidth/4)*3
  bottom= ((textsize)*(pagesize-pagepoint)+(textsize*f)) + textsize*(extraspace-1)

  gr.rect Button1, left, top, right, bottom  

  for i = 1 to buttontextsize
   list.get ww2, i, buttonline$
   gr.text.draw tmppointer, left, bottom-2-(textsize*(buttontextsize-i)), buttonline$
  next i

next f

! If there weren't any options to draw, make a The End button
if numberofoptions = 0 then
 f=1
 if (PageSize - PagePoint) + 2 < (ScreenHeight / TextSize) then
  gr.color 255, 0, 0, 0, 0
  left=   (screenwidth/4)
  top=    ((textsize)*(pagesize-pagepoint)+textsize*(2*f-1))
  right=  ((screenwidth/4)*3)
  bottom= ((textsize)*(pagesize-pagepoint)+(textsize*(2*f)))
  gr.rect Button1, left, top, right, bottom
  gr.text.draw tmppointer, left, bottom-2, "The End"
 end if
end if

! Now draw the settings bar

! First, the zoom out button:
gr.color 255, 100, 100, 100, 1
left=   0
top=    (screenheight-10-textsize*4)
right=  screenwidth
bottom= Screenheight
gr.rect Button1, left, top, right, bottom
gr.color 255, 0, 0, 0, 1
left=   1
top=    (screenheight-4-textsize*2)
right=  (screenwidth/4)-1
bottom= (Screenheight-2)
gr.rect Button1, left, top, right, bottom
gr.color 255, 255, 255, 255, 1
gr.text.draw tmppointer, left, bottom-2, "Zoom -"

! Then the zoom in button:
gr.color 255, 0, 0, 0, 1
top=    top - textsize*2 -4
bottom= bottom - textsize*2 -4
gr.rect Button1, left, top, right, bottom
gr.color 255, 255, 255, 255, 1
gr.text.draw tmppointer, left, bottom-2, "Zoom +"

! Go back button:
gr.color 255, 0, 0, 0, 1
left=   (screenwidth/4)
top=    (screenheight-4-textsize*2)
right=  (screenwidth/4)*2-1
bottom= (Screenheight-2)
gr.rect Button1, left, top, right, bottom
gr.color 255, 255, 255, 255, 1
gr.text.draw tmppointer, left, bottom-2, "Back <-"

! The report page button
! (This is an elevator close button right now... that is, it doesn't do anything)
gr.color 255, 0, 0, 0, 1
top=    top - textsize*2 -4
bottom= bottom - textsize*2 -4
gr.rect Button1, left, top, right, bottom
gr.color 255, 255, 255, 255, 1
gr.text.draw tmppointer, left, bottom-2, "Report"

! Dislike button (elevator close button)
gr.color 255, 0, 0, 0, 1
left=   (screenwidth/4)*2
top=    (screenheight-4-textsize*2)
right=  (screenwidth/4)*3-1
bottom= (Screenheight-2)
gr.rect Button1, left, top, right, bottom
gr.color 255, 255, 255, 255, 1
gr.text.draw tmppointer, left, bottom-2, "Dislike"

! Like button (elevator close)
gr.color 255, 0, 0, 0, 1
top=    top - textsize*2 -4
bottom= bottom - textsize*2 -4
gr.rect Button1, left, top, right, bottom
gr.color 255, 255, 255, 255, 1
gr.text.draw tmppointer, left, bottom-2, "Like"

! Scroll down button
gr.color 255, 0, 0, 0, 1
left=   (screenwidth/4)*3
top=    (screenheight-4-textsize*2)
right=  screenwidth-2
bottom= (Screenheight-2)
gr.rect Button1, left, top, right, bottom
gr.color 255, 255, 255, 255, 1
gr.text.draw tmppointer, left, bottom-2, "Scroll \/"

! Scroll up button
gr.color 255, 0, 0, 0, 1
top=    top - textsize*2 -4
bottom= bottom - textsize*2 -4
gr.rect Button1, left, top, right, bottom
gr.color 255, 255, 255, 255, 1
gr.text.draw tmppointer, left, bottom-2, "Scroll ^"

! Render the settings bar
gr.render



! Inner loop; Test for and react to touches
do

! Test if the screen has been touched at all,
! because if not, why bother, right?
gr.touch touched, Touchx, Touchy
if touched then

! if it's not a continuation button, test for other buttons:

! React to Zoom out Button being pressed
left=   1
top=    (screenheight-4-textsize*2)
right=  (screenwidth/4)-1
bottom= (Screenheight-2)
gr.bounded.touch TouchButton1, left, top, right, bottom

if TouchButton1=1 then
 gr.color 255, 50, 50, 50, 1
 gr.rect Button1, left, top, right, bottom
 gr.color 255, 0, 0, 0, 1
 gr.render
 pause 100

 textsize=textsize-8
 gr.text.size TextSize
 floop = 1
end if

! React to Zoom in Button being pressed 
top=    top - textsize*2 -4
bottom= bottom - textsize*2 -4
gr.bounded.touch TouchButton1, left, top, right, bottom

if TouchButton1=1 then
 gr.color 255, 50, 50, 50, 1
 gr.rect Button1, left, top, right, bottom
 gr.color 255, 0, 0, 0, 1
 gr.render
 pause 100

 textsize=textsize+8
 gr.text.size TextSize
 floop = 1
end if

! React to back Button being pressed
left=   (screenwidth/4)
top=    (screenheight-4-textsize*2)
right=  (screenwidth/4)*2-1
bottom= (Screenheight-2)
gr.bounded.touch TouchButton1, left, top, right, bottom

if TouchButton1=1 then
 gr.color 255, 50, 50, 50, 1
 gr.rect Button1, left, top, right, bottom
 gr.color 255, 0, 0, 0, 0
 gr.render
 pause 100

 if makingnews = 1 then makingnews = 0

 FileName$ =  left$(FileName$, (len(FileName$)-1))
 if FileName$ = "" then
  gr.cls
  gr.close
  end
  exit
 end if
 floop = 1
 gosub bookmark
end if

! React to report button being pressed 
top=    top - textsize*2 -4
bottom= bottom - textsize*2 -4
gr.bounded.touch TouchButton1, left, top, right, bottom

if TouchButton1=1 then
 gr.color 255, 50, 50, 50, 1
 gr.rect Button1, left, top, right, bottom
 gr.color 255, 0, 0, 0, 1
 gr.render
 pause 100

 popup "This page has been reported. Thank you.", 0, 0, 0
 floop = 1
end if

! React to dislike Button being pressed
left=   (screenwidth/4)*2
top=    (screenheight-4-textsize*2)
right=  (screenwidth/4)*3-1
bottom= (Screenheight-2)
gr.bounded.touch TouchButton1, left, top, right, bottom

if TouchButton1=1 then
 gr.color 255, 50, 50, 50, 1
 gr.rect Button1, left, top, right, bottom
 gr.color 255, 0, 0, 0, 0
 gr.render
 pause 100

 popup "You dislike this page.", 0, 0, 0
 floop = 1
end if

! React to like button being pressed 
top=    top - textsize*2 -4
bottom= bottom - textsize*2 -4
gr.bounded.touch TouchButton1, left, top, right, bottom

if TouchButton1=1 then
 gr.color 255, 50, 50, 50, 1
 gr.rect Button1, left, top, right, bottom
 gr.color 255, 0, 0, 0, 1
 gr.render
 pause 100

 popup "You like this page.", 0, 0, 0
 floop = 1
end if

! React to scroll down Button being pressed
left=   (screenwidth/4)*3
top=    (screenheight-4-textsize*2)
right=  screenwidth-2
bottom= (Screenheight-2)
gr.bounded.touch TouchButton1, left, top, right, bottom

if TouchButton1=1 then
 gr.color 255, 50, 50, 50, 1
 gr.rect Button1, left, top, right, bottom
 gr.color 255, 0, 0, 0, 0
 gr.render
 pause 100

 pagepoint = pagepoint + 1
 floop = 1
end if

! React to scroll up button being pressed 
top=    top - textsize*2 -4
bottom= bottom - textsize*2 -4
gr.bounded.touch TouchButton1, left, top, right, bottom

if TouchButton1=1 then
 gr.color 255, 50, 50, 50, 1
 gr.rect Button1, left, top, right, bottom
 gr.color 255, 0, 0, 0, 1
 gr.render
 pause 100

 if pagepoint > 0 then pagepoint = pagepoint - 1
 floop = 1
end if



! Get ready to test for continue option buttons
numberofoptions2=numberofoptions
if numberofoptions = 0 then numberofoptions2 = 1

extraspace = 0

! First make sure the user wasn't trying to press a setting (zoom,scroll,etc) button
if touchy < (screenheight-9-textsize*4) then     

for f = 1 to numberofoptions2
 ! Test if a Continue button was pressed **********************************
 
 if numberofoptions > 0 then
  cls
  ww3=wordwrap(options$[f],screenwidth/2)
  list.size ww3, buttontextsize
  extraspace = extraspace + buttontextsize  
 end if

 left=   screenwidth/4
 top=    (textsize)*((pagesize-pagepoint)+(f-1)+(extraspace-buttontextsize))
 right=  (screenwidth/4)*3
 bottom= ((textsize)*(pagesize-pagepoint)+(textsize*f)) + textsize*(extraspace-1)
 if numberofoptions = 0 then
  left=   (screenwidth/4)
  top=    ((textsize)*(pagesize-pagepoint)+textsize*(2*f-1))
  right=  ((screenwidth/4)*3)
  bottom= ((textsize)*(pagesize-pagepoint)+(textsize*(2*f)))
 end if

 gr.bounded.touch TouchButton1, left, top, right, bottom

 ! React to Continue Button being pressed
 if TouchButton1=1 then
  gr.color 255, 50, 50, 50, 1
  gr.rect Button1, left, top, right, bottom
  gr.color 255, 0, 0, 0, 0
  gr.render
  pause 250

  MyChoice = f
  Gosub Continue
  f = numberofoptions2
  gr.front 1
  floop = 1
 end if

next f
end if


end if 


! And just in case the screen has been turned, we can switch the orientation and redraw
! buttons/options/settings bar to fit.
gr.screen screenwidth2, screenheight

if screenwidth2 <> screenwidth then
 screenwidth = screenwidth2
 floop = 1
end if

until floop = 1
! End of Inner Loop
   

! Outer loop. Loop forever
floop = 0
touched = 0
until 0

!**************************************************************************************



!CONTINUE ------------------------------
! If a continuation option button was pressed
Continue:

gr.cls

pagepoint = 0

! If the user pressed The End, we restart from the beginning
if numberofoptions = 0 then 
 gr.front 1
 gr.cls
 floop = 1
 goto thestart
end if

! In case the user just pressed Make New Page...
if makingnews = 1 then goto makenewpage

! We make the Filename unique to correspond to the newest page
Filename$ = Filename$ + format$("#", MyChoice)
Filename$ = Replace$(Filename$, " ", "")

bookmark:

! Check to see if the next page has been written yet.
thetext$ = ""
pause 250
graburl thetext$, "http://www.sftheatre.net/StoryGame/Begin/" + Filename$ + ".dat"
thetext$ = Replace$(thetext$, "\"", "'")
thetext$ = Replace$(thetext$, "\'", "'")

! Set up to make a new page!
if thetext$ = "not written yet" then
 thetext$ = " The next page of this story has yet to be written. Please help us continue this story by writing in your own continuation. After, you will be taken to your new page. Press Make Page when you are ready to go! pbreak! 1 pbreak! Make Page pbreak! 1 pbreak! 0"
 makingnews = 1
end if

! PageData$[] is the list file you get from the online txt files. It contains
! the page's text, the number of continuations, the continuation options,
! a number of page views (I don't use this yet), and a rating (also not used yet)

array.delete PageData$[]

split PageData$[], thetext$, "pbreak!"

array.length PageDataLength, PageData$[]

PageText$ = PageData$[1]
NumberofOptions = val(PageData$[2])

array.delete Options$[]
if NumberofOptions > 0 then Dim options$[NumberofOptions]
if NumberofOptions = 0 then Dim options$[1]

if NumberofOptions > 0 then
 for i = 1 to NumberofOptions
  Options$[i] = PageData$[i+2]
 next i
end if

PageViews = val(PageData$[NumberofOptions + 3])
PageRating = val(PageData$[NumberofOptions + 4])

MyChoice = 0

return

!CONTINUE CONTINUE CONTINUE CONTINUE CONTINUE CONTINUE CONTINUE CONTINUE CONTINUE






! Here's where we put together the screen and options for letting our user
! (hopefully) write their own new page.

MakeNewPage:

makingnews = 0

gr.cls

PagePoint = 0

bookmark3:
makingnews = 0

gr.front 0

text.input PageText$, "New Page:"

array.delete Options$[]
pause 50
dim Options$[6]
Options$[1] = "One Continuation"
Options$[2] = "Two Continuations"
Options$[3] = "Three Continuations"
Options$[4] = "Four Continuations"
Options$[5] = "Five Continuations"
Options$[6] = "No continuations, (This is an ending)"

do
select hmtc, Options$[], "How many choices for continuations?"
until hmtc > 0

if hmtc = 6 then hmtc = 0
numberofoptions = hmtc

if hmtc <> 0 then
for t = 1 to hmtc
 text.input Options$[t], "Option "+Format$("#",t)+": " 
next t
end if

body$ = PageText$ + "pbreak!" + str$(numberofOptions) + "pbreak!"
if NumberofOptions > 0 then
 for i = 1 to NumberofOptions
  body$ = body$ + Options$[i] + "pbreak!"
 next i
end if

PageViews=1
body$ = body$ + str$(PageViews) + "pbreak!0"

ff$ = "Begin/" + Filename$
list.add goingonline, "file", ff$
list.add goingonline, "noo", format$("#", NumberofOptions)
list.add goingonline, "body", body$

http.post url$, goingonline, result$

list.clear goingonline


return













Functions:

! Special thanks to Rev. Jonathan C. Watt for the brilliant word wrap function!
Fn.def wordwrap ( t$, w ) 

List.create S, ww % create the list to return.
list.clear ww

s = is_in( " ", t$ ) % find the first space if s=0 then fn.rtn = 0 % can't be processed

l$ = left$( t$, s ) % get string up to this point 
gr.text.width l, l$ % get the lenght in pixels

while s < len(t$) % process the string 
s1 = is_in( " ", t$, s+1 ) 
if s1 = 0 then s1 = len(t$)

lln = len(l$) % save the current length 
l$ = l$ + mid$( t$, s+1, s1-s ) 
gr.text.width l, l$ 
if l > w % is it too long? 
 list.add ww, left$(l$, lln ) 
 l$ = mid$( t$, s+1, s1-s ) 
endif 

s = s1 % look at the next section 
repeat

if len (l$)>0 then % don't forget what's left 
 list.add ww, l$
end if
fn.rtn ww % return the list 
fn.end

return




! Pretty standard error catch
on error:

gr.cls
gr.close
end
exit

