! Yucatan   @cassiope34  02/08/2014

Fn.def mvs4ch(ch, lstmv, j, pos[])
if ch =1
  mv = pos[3-j]-pos[j]+1
  mv = max(mv,1)
elseif ch =2
  mv =lstmv
else
  mv = ch-2
endif
Fn.rtn mv
Fn.end

Fn.def LastPtr( ptr )  % put the graphic pointer 'ptr' (so the bitmap) UP to all other.
  gr.getDL ndl[],1
  array.length sz, ndl[]
  if !ptr | sz=1 | ndl[sz] =ptr
    array.delete ndl[]
    Fn.rtn 0
  endif
  array.search ndl[],ptr,n
  if n
    for nn=n to sz-1
      ndl[nn] =ndl[nn+1]
    next
    ndl[sz] =ptr
    gr.newDL ndl[]
  endif
  array.delete ndl[]
Fn.end

Fn.def movepix( arx, ary, ptr )
 Call LastPtr( ptr )
 do
   gr.get.position ptr,px,py
   ix =floor((px-arx)/2)
   iy =floor((py-ary)/2)
   if abs(ix) <5 & abs(iy) <5 then D_u.break
   gr.modify ptr, "x", px-ix
   gr.modify ptr, "y", py-iy
   gr.render
 Until 0
 gr.modify ptr, "x", arx
 gr.modify ptr, "y", ary
 gr.render
Fn.end

FN.DEF roundCornerRect (b, h, r)
 half_pi       = 3.14159 / 2
 dphi          = half_pi / 8
 LIST.CREATE   N, S1
 mx            = -b/2+r
 my            = -h/2+r
 FOR phi       = 0 TO half_pi STEP dphi
  LIST.ADD     s1, mx-COS(phi)*r, my-sin (phi)*r
 NEXT
 mx            = b/2-r
 my            = -h/2+r
 FOR phi       = 0 TO half_pi STEP dphi
  LIST.ADD     s1, mx+SIN(phi)*r, my- cos (phi)*r
 NEXT
 mx            = b/2-r
 my            = h/2-r
 FOR phi       = 0 TO half_pi STEP dphi
  LIST.ADD     s1, mx+COS(phi)*r, my+sin (phi)*r
 NEXT
 mx            = -b/2+r
 my            =  h/2-r
 FOR phi       = 0 TO half_pi STEP dphi
  LIST.ADD     s1, mx-SIN(phi)*r, my+cos (phi)*r
 NEXT
 FN.RTN s1
FN.END

File.Root tmp$
IstandAlone =Is_In("rfo-basic",tmp$)    % to know if it's an APK for the way to exit.

gr.open 255,254,255,196,0,0
gr.screen w,h
scx =1280
scy =800
sx =w/scx
sy =h/scy
gr.scale sx,sy

dc =90
ncx =14
ncy =7
dpx =(scx-dc*ncx)/2
dpy =(scy-dc*ncy)/2
fileSav$ ="Yucata.ini"

dim c[ncx,ncy,3], pj[2], bl[2], chx[7,2], tched[7,2], h$[18]
dim ch[7], lspt[2], pts[2,2], pos[2], ps[2,2], res[2]

dw =scx-20
dh =scy-10
gr.bitmap.create bhelp, dw, dh     % pion 1
gr.bitmap.drawinto.start bhelp
poly =roundCornerRect(dw,dh,30)
gr.color 255,58,170,50,1
gr.poly nul, poly, dw/2, dh/2
gr.set.stroke 2
gr.text.size 45
inl =38
gr.color 255,255,186,0,2
gr.text.draw nul, 600, inl+15, "Y U C A T A'"
gr.color 255,238,244,82,2
gr.text.draw nul, 320, inl+15, "Welcome to "
gr.text.size 25
gr.set.stroke 1
h$[1] ="The goal :  pick up the least green marbles as possible and reach the red one !"
h$[2] = "Rules  :  each player at his turn choose a way to move his pawn among the choices left to him."
h$[3] = "When a player reach the red marble : points are counted...!"
h$[4] = "    '"+chr$(9996) +"' ->  place your pawn just ahead the most advanced player. If it's you : move one cell. "
h$[5] = "     '='   ->  you move in the same way as the previous player. So it can't be your first choice !"
h$[6] = "     1,2,3,4,5 are simply the number of cell(s) of the move."
h$[7] = "You can't choose the same last move of the previous player, except if you haven't choice."
h$[8] = "After 7 rounds all choices were used. If no one reaches the red marble all choices are available again (except the"
h$[9] = "last one)."
h$[10] = "Scoring :   count the yellow marbles."
h$[11] = "   for 1 green marble you must remove 1 yellow marble"
h$[12] = "   for 2 green marbles you must remove 1+2 = 3 yellow marbles"
h$[13] = "   for 3 green marbles you must remove 1+2+3 = 6 yellow marbles"
h$[14] = "   for 4 green marbles you must remove 1+2+3+4 = 10 yellow marbles"
h$[15] = "   etc...  Obviously your score can be negative !"
h$[16] = "••  The player who reaches the red marble remove 1 green marble (if he don't have: no effect)."
h$[17] = "  The distribution of green marbles is random but neither the first nor the last marble before the red can be green."
h$[18] = "  If you want the computer to play : touch its pawn (on top) more than 2 seconds."
for lg =1 to 18
  gr.text.draw nul, 10, 65+lg*inl, h$[lg]
next
gr.text.draw nul, dw-200, dh-15, "Have fun !"
gr.bitmap.drawinto.end

gr.bitmap.create amul1,40,70     % pion 1
gr.bitmap.drawinto.start amul1
gr.set.stroke 3
clr =120
for r=12 to 1 step -1
  gr.color 255,clr,clr,220,0
  gr.circle nul, 20, 15, r
  clr+=6
next
clr =120
for r=18 to 1 step -1
  gr.color 255,clr,clr,220,0
  gr.circle nul, 20, 45, r
  clr+=6
next
gr.bitmap.drawinto.end

gr.bitmap.create amul2,40,70     % pion 2
gr.bitmap.drawinto.start amul2
gr.set.stroke 3
clr =40
for r=12 to 1 step -1
  gr.color 255,clr,clr,clr,0
  gr.circle nul, 20, 15, r
  clr+=6
next
clr =40
for r=18 to 1 step -1
  gr.color 255,clr,clr,clr,0
  gr.circle nul, 20, 45, r
  clr+=6
next
gr.bitmap.drawinto.end

gr.bitmap.create bmenu,560,130       % bitmap du menu
gr.bitmap.drawinto.start bmenu
poly =roundCornerRect(2*dc-5,1.5*dc-5,24)
gr.set.stroke 6
gr.color 255,100,100,100,1
gr.poly nul, poly, 90, 65
gr.poly nul, poly, 270, 65
gr.poly nul, poly, 450, 65
gr.color 255,170,170,170,0
gr.poly nul, poly, 90, 65
gr.poly nul, poly, 270, 65
gr.poly nul, poly, 450, 65
gr.text.align 2
gr.text.size 60
gr.color 255, 0,174,238,1
gr.text.draw nul,90,85,"Exit"
gr.text.draw nul,270,85,"New"
gr.text.draw nul,450,85,"Help"
gr.bitmap.drawinto.end

gr.bitmap.create square,dc,dc       % carré gris
gr.bitmap.drawinto.start square
poly =roundCornerRect(dc-5,dc-5,18)
gr.set.stroke 5
gr.color 100,190,190,190,1
gr.poly nul, poly, dc/2-2, dc/2
gr.color 255,160,160,160,0
gr.poly nul, poly, dc/2, dc/2
gr.bitmap.drawinto.end

gr.bitmap.create boule1,64,64      % boule1 jaune
gr.bitmap.drawinto.start boule1
gr.set.stroke 3
clr =30
for r=30 to 1 step -1
  gr.color 255,210,220,clr,0
  gr.circle nul, 32, 32, r
  clr+=4
next
gr.bitmap.drawinto.end

gr.bitmap.create boule2,64,64      % boule2 verte
gr.bitmap.drawinto.start boule2
gr.set.stroke 3
clr =60
for r=30 to 1 step -1
  gr.color 255,clr,200,clr,0
  gr.circle nul, 32, 32, r
  clr+=4
next
gr.bitmap.drawinto.end

gr.bitmap.create boule3,64,64      % boule3 rouge
gr.bitmap.drawinto.start boule3
gr.set.stroke 3
clr =80
for r=30 to 1 step -1
  gr.color 255,233,clr,clr,0
  gr.circle nul, 32, 32, r
  clr+=4
next
gr.bitmap.drawinto.end

gr.text.align 2
gr.set.stroke 1
gr.text.size 60

for c=1 to 7
  gr.bitmap.create ch[c],dc,dc      % bitmaps des 7 choix
  gr.bitmap.drawinto.start ch[c]
  gr.color 165,255,196,16,1      % orange
  gr.circle nul,dc/2,dc/2,dc/2-2
  gr.color 255,120,120,120,2     % gris
  if c=1
    c$ = chr$(9996)
  elseif c=2
    c$ = "="
  else
    c$ = replace$(str$(c-2),".0","")
  endif
  gr.text.draw nul, dc/2, dc/2+22, c$
  gr.bitmap.drawinto.end
next
j    =2
AI   =0

New:    % --------------------------------------
UnDim ps[]
dim ps[2,2]
gr.cls
gr.bitmap.draw helpb, bhelp, 10, 5
gr.hide helpb
gr.bitmap.draw menub, bmenu, 370, 335
gr.hide menub

for c=1 to 7     % les 7 choix
  gr.bitmap.draw chx[c,1], ch[c], dpx, dpy+(c-1)*dc
  tched[c,1] =1
  gr.bitmap.draw chx[c,2], ch[c], dpx+13*dc, dpy+(c-1)*dc
  tched[c,2] =1
next

gr.bitmap.draw nul, amul1, dpx+24,  dpy-dc+12
gr.bitmap.draw nul, amul2, dpx+24+dc*13, dpy-dc+12

gr.set.stroke 1
gr.text.size 55
gr.text.draw lspt[1], dpx+1.3*dc,  dpy-30, chr$(9756)   % les mains
gr.text.draw lspt[2], dpx+12.7*dc, dpy-30, chr$(9758)
gr.hide lspt[1]
gr.hide lspt[2]

!  le plateau de jeu

gr.color 40,255,120,0,1      % rectangle du plateau orange clair
gr.rect nul, dpx+2*dc-10, dpy-10, dpx+12*dc+10, dpy+7*dc+10
gr.set.stroke 3
gr.color 255,255,120,0,2
gr.text.size 50
gr.text.draw nul, scx/2+10, dpy+dc*2+50, "Y U C A T A '"
gr.set.stroke 1
gr.text.size 30
gr.text.draw lspcrx, dpx+1.3*dc,  dpy-30, "X"     % croix rouge
gr.hide lspcrx

ref$ ="031 032 033 034 035 036 037 047 057 067 077 087 097 107 117 127 126 125 124 123 "+~
      "122 121 111 101 091 081 071 061 051 052 053 054 055 065 075 085 095 105 104 103"

for sq =1 to 40
  nx =val(left$(word$(ref$,sq),2))
  ny =val(right$(word$(ref$,sq),1))
  gr.bitmap.draw c[nx,ny,1], square, dpx+dc*(nx-1), dpy+dc*(ny-1)
next

gr.bitmap.draw nul, boule1, dpx+13+dc*0,  dpy+13+dc*7
gr.bitmap.draw nul, boule2, dpx+13+dc*1,  dpy+13+dc*7
gr.bitmap.draw nul, boule1, dpx+13+dc*12, dpy+13+dc*7
gr.bitmap.draw nul, boule2, dpx+13+dc*13, dpy+13+dc*7
gr.color 255,0,0,255,1
gr.text.size 30
gr.text.draw pts[1,1], dpx+dc/2+dc*0,  dpy+dc/2+11+dc*7, "0"   % nbre de boules j1
gr.text.draw pts[2,1], dpx+dc/2+dc*1,  dpy+dc/2+11+dc*7, "0"
gr.text.draw pts[1,2], dpx+dc/2+dc*12, dpy+dc/2+11+dc*7, "0"   %    " j2
gr.text.draw pts[2,2], dpx+dc/2+dc*13, dpy+dc/2+11+dc*7, "0"

gr.text.draw res[1], dpx+dc/2+2*dc,  dpy+dc/2+11+dc*7, "0"
gr.text.draw res[2], dpx+dc/2+11*dc, dpy+dc/2+11+dc*7, "0"
gr.hide res[1]
gr.hide res[2]

pbm$ =""
for bm=1 to 9    % tirage au sort répartition des boules vertes
  do
    tr =floor(rnd()*31)+7
  until !is_in("-"+str$(tr),pbm$)
  pbm$ += "-"+str$(tr)
next
for n =6 to 40
  nx =val(left$(word$(ref$,n),2))
  ny =val(right$(word$(ref$,n),1))
  if n =40
    gr.bitmap.draw c[nx,ny,2], boule3, dpx+13+dc*(nx-1), dpy+13+dc*(ny-1)
    c[nx,ny,3] =3
  elseif is_in("-"+str$(n),pbm$)
    gr.bitmap.draw c[nx,ny,2], boule2, dpx+13+dc*(nx-1), dpy+13+dc*(ny-1)
    c[nx,ny,3] =2
  else
    gr.bitmap.draw c[nx,ny,2], boule1, dpx+13+dc*(nx-1), dpy+13+dc*(ny-1)
    c[nx,ny,3] =1
  endif
next

!  place les pions au départ.
pos[1] =1
px =val(left$(word$(ref$,pos[1]),2))
py =val(right$(word$(ref$,pos[1]),1))
gr.bitmap.draw pj[1], amul1, dpx+5+dc*(px-1),  dpy+12+dc*(py-1)
pos[2] =1
px =val(left$(word$(ref$,pos[1]),2))
py =val(right$(word$(ref$,pos[1]),1))
gr.bitmap.draw pj[2], amul2, dpx+46+dc*(px-1), dpy+12+dc*(py-1)

help =0
quit =0
menu =0
FIN  =0
lstpl=0
lstmv=0
if AI then j =3-AI
!AI   =0
gr.modify lspt[j], "y", dpy+-30     % who play hand
gr.show lspt[j]

gr.text.draw mess, scx/2, 30, ""   % just for debug

gosub loadgame

DO           %  ---------- boucle principale -------------
do
  gr.touch touched, tx, ty
  tx/=sx
  ty/=sy
  if !background() then gr.render
until touched | quit
ti =clock()
if quit then D_U.break
do
  gr.touch touched, tx, ty
  tx/=sx
  ty/=sy
  tr =clock()-ti
  tcy =floor((ty-dpy)/dc)+1
until !touched | (!tcy & tr>600)

tcx =floor((tx-dpx)/dc)+1

if menu & !help
  gr.hide menub
  menu =0
  if tcy =4
    if tcx=5 | tcx=6    % exit
      quit =1

    elseif tcx=7 | tcx=8   % new
      quit =1
      if pos[1]=1 & pos[2]=1 then quit =0 else menu =1

    elseif tcx=9 | tcx=10  % help
      help =1
      call lastPtr( helpb )
      gr.show helpb

    endif
  endif

elseif help
  help =0
  gr.hide helpb

elseif tcy =0 & (tcx=1 | tcx =14)     % chose player to start
  if pos[1]+pos[2]=2
    if tcx=1 then j=1 else j=2
  endif
  gr.modify lspt[j], "y", dpy-30     % les mains
  gr.show lspt[j]
  gr.hide lspt[3-j]
  if tr>600 % & !AI
    AI =j
    gosub AI
  endif

elseif tcy>0 & tcy<8 & (tcx=1 | tcx=14)
  if FIN
    gosub animFin

  elseif tched[tcy,j] & ((tcx=1 & j=1) | (tcx=14 & j=2))
    array.sum nt, tched[]    % somme des choix restants...
    if tcy = lstpl & nt >2
      for cli =1 to 2
        pause 150
        gr.hide lspt[3-j]
        gr.render
        pause 150
        gr.show lspt[3-j]
        gr.render
      next
      D_U.continue

    elseif tcy =2 & pos[1]+pos[2] =2    % '=' can't be the first choice
      D_U.continue

    endif
    gosub action   % play tcy

    if AI & !FIN then gosub AI     %  AI...?!

  endif

elseif tcy>0 & tcy<8 & tcx>2 & tcx<13    % for device that don't have a 'menu' button.
  menu =1-menu
  if menu
    call lastPtr( menub )
    gr.show menub
  endif
endif

UNTIL quit

if menu then goto New

gosub savegame

if IstandAlone
  END "Bye...!"
else
  EXIT
endif      %  ----------------------  exit  ----------------------

onbackkey:
if menu | help
  menu =0
  help =0
  gr.hide menub
  gr.hide helpb
  gr.render
  Back.resume
endif
lastime =clock()
if lastime-thistime <1000
  quit =1
else
  quit =0
  thistime =lastime
  popup " Presser encore pour quitter ",0,0,0
endif
Back.resume

onMenuKey:
menu =1
call lastPtr( menub )
gr.show menub
MenuKey.resume

action:       % execute le choix de j 'tcy', change j, maj l'affichage.
tched[tcy,j] =0
gr.modify chx[tcy,j], "alpha", 60       % ce choix grisé
gr.modify lspt[j], "y", dpy+dc*tcy-30   % main sur le choix.
gr.show lspt[j]
gr.hide lspt[3-j]
gr.render
pause 300

gosub move    % move j to pos[j] + tcy depend of tcy
lstpl =tcy
array.sum nt, tched[]   % somme des choix restants...
if nt=0       % reinit les choix au bout de 7 coups.
  gr.render
  pause 300
  for n=1 to 7
    tched[n,1] =1
    if AI<>1 then gr.modify chx[n,1], "alpha", 255
    tched[n,2] =1
    if AI<>2 then gr.modify chx[n,2], "alpha", 255
  next
  nt =7
endif
gr.hide lspcrx
if tched[lstpl,3-j] & nt >2       % la croix rouge
  gr.modify lspcrx, "x", dpx+(1.3*(j=2)+12.7*(j=1))*dc
  gr.modify lspcrx, "y", dpy+dc*lstpl-30
  gr.show lspcrx
endif

j =3-j     %  player change
gr.modify lspt[j], "y", dpy-30    % mains
gr.show lspt[j]
RETURN

move:    % move j to pos[j]+tcy depend of tcy
if tcy =1
  rmv = pos[3-j]-pos[j]+1
  rmv = max(rmv,1)
elseif tcy =2
  rmv =lstmv
else
  rmv = tcy-2
endif
for r =1 to rmv
  if FIN then F_n.break    % pour ne pas dépasser la bille rouge.
  pos[j]++
  px =val(left$(word$(ref$,pos[j]),2))     % read the destination cell.
  py =val(right$(word$(ref$,pos[j]),1))
  call movepix( dpx+5+41*(j=2)+dc*(px-1), dpy+12+dc*(py-1), pj[j] )    % move pawn
  if c[px,py,3]
    if c[px,py,3] =1         % bille jaune
      ps[1,j]++
      gr.modify pts[1,j], "text", replace$( str$(ps[1,j]), ".0", "")
      gr.hide c[px,py,2]

    elseif c[px,py,3] =2     % bille verte
      ps[2,j]++
      gr.modify pts[2,j], "text", replace$( str$(ps[2,j]), ".0", "")
      gr.hide c[px,py,2]

    elseif c[px,py,3] =3     % bille rouge (se soustrait une bille verte)
      if ps[2,j]
        ps[2,j]--
        gr.modify pts[2,j], "text", replace$( str$(ps[2,j]), ".0", "")
      endif
      FIN =1

    endif
    c[px,py,3] =0
  endif
  gr.render
  pause 200
next
lstmv =rmv
if FIN
  s$ ="1 3 6 10 15 21 28 36 45"
  re1 =ps[1,1]
  if ps[2,1] then re1 = re1 -val(word$(s$,ps[2,1]))
  gr.modify res[1], "text", replace$(str$(re1),".0","")
  gr.show res[1]
  re2 =ps[1,2]
  if ps[2,2] then re2 = re2 -val(word$(s$,ps[2,2]))
  gr.modify res[2], "text", replace$(str$(re2),".0","")
  gr.show res[2]
  gosub animFin
endif
return

animFin:
 duce =0
 if re1>re2       % animation...
   ptrg =pj[1]
   jw =1
 elseif re2>re1
   ptrg =pj[2]
   jw =2
 else
   duce =1
 endif
 if !duce
  POPUP "Player  "+replace$(str$(jw),".0","")+"  is the winner !",0,-185,0
  gr.get.position ptrg, x, y
  for b =1 to 10
    call movepix(x,y-dc,ptrg)
    pause 20
    gr.modify ptrg, "y", y
    gr.render
    pause 20
  next
 endif
return

AI:
! Recencer les choix restants (en intégrant l'interdit du dernier coup adverse).
!  '=' interdit si premier coup du joueur qui commence.
for c =1 to 7
  gr.modify chx[c,j], "alpha", 60     % don't show computer choices left...
next
gr.show lspt[j]   % hand
gr.render
array.sum nt, tched[]   % somme des choix restants...
chx$ =""
for ch=1 to 7
  if tched[ch,j]
    if ch =lstpl & nt >2 then F_N.continue
    if ch=2 & (pos[1]+pos[2])=2 then F_N.continue
    chx$ =chx$+str$(ch) +str$(mvs4ch(ch,lstmv,j,pos[])) +" "    % + nbre de cases du mvt.
  endif
next
chx$ =replace$(chx$,".0","")

! si choix obligé :   jouer sans réflexion

if len(chx$)=3           % un seul choix possible.
  tcy =val(left$(chx$,1))

else

!  sinon
!   trouver le choix qui avance le moins: si égaux préférer les choix à chiffre.

  nchx  =len(chx$)/3    % nbre de choix possibles.
  tcym  =0
  chmax =val(left$( word$(chx$,nchx),1))  % dernier choix des possibles.
  bchm  =val(right$(word$(chx$,nchx),1))  % son nbre de case...
  bch   =40
  for ch =1 to nchx
    nmv = val(right$(word$(chx$,ch),1))
    if nmv <=bch                    %   + petit mouvement...
      tcy  =val(left$(word$(chx$,ch),1))
      bch  =nmv
    endif
    if nmv >bchm                    %   + grand mouvement...
      tcym =val(left$(word$(chx$,ch),1))
      bchm =nmv
    endif
  next
  if tcym then chmax =val(left$(word$(chx$,tcym),1))

!  si BEAUCOUP de place avant la première bille verte: ex: 9 ou 10 ?
!        grosse avance possible...?
  seuil =9  % >= à
  d =0      % nbre de cases jusqu'à une bille verte.
  for p =pos[j]+1 to 38
    px =val(left$( word$(ref$,p),2))
    py =val(right$(word$(ref$,p),1))
    if c[px,py,3] =2 then F_n.break     % bille verte rencontrée.
    d++
  next
  bj =0
  bv =0
  seuil2 =10
  for p =pos[j]+1 to min(38,pos[j]+seuil2)  % combien de verte sur cette distance ?
    px =val(left$( word$(ref$,p),2))
    py =val(right$(word$(ref$,p),1))
    if c[px,py,3] =1        % bille jaune
      bj++
    elseif c[px,py,3] =2    % bille verte
      bv++
    endif
  next

! ou pas de bille verte jusqu'à l'arrivée
  if d>=seuil | d>=(38-pos[j]) | (bj>2 & bv=1) then tcy =chmax

!  gérer l'accès à la bille rouge...
  if (pos[j] + bchm) >=39 then tcy =chmax

endif

!gr.modify mess, "text", chx$ +"   -  "+str$(tcy)+"     "+str$( (pos[j] + nmv) )
!gr.render
pause 800
gosub action
RETURN

loadgame:
File.exists fe, fileSav$
if fe
  GRABFILE ldgame$, fileSav$
  split ln$[], ldgame$, "\n"
  j       =val(word$(ln$[1],1,","))
  pos[1]  =val(word$(ln$[1],2,","))
  pos[2]  =val(word$(ln$[1],3,","))
  px =val(left$( word$(ref$,pos[1]),2))
  py =val(right$(word$(ref$,pos[1]),1))
  gr.modify pj[1], "x", dpx+5+dc*(px-1)
  gr.modify pj[1], "y", dpy+12+dc*(py-1)
  px =val(left$( word$(ref$,pos[2]),2))
  py =val(right$(word$(ref$,pos[2]),1))
  gr.modify pj[2], "x", dpx+46+dc*(px-1)
  gr.modify pj[2], "y", dpy+12+dc*(py-1)
  ps[1,1] =val(word$(ln$[1],4,","))
  ps[2,1] =val(word$(ln$[1],5,","))
  ps[1,2] =val(word$(ln$[1],6,","))
  ps[2,2] =val(word$(ln$[1],7,","))
  gr.modify pts[1,1], "text", replace$( str$(ps[1,1]), ".0", "")
  gr.modify pts[2,1], "text", replace$( str$(ps[2,1]), ".0", "")
  gr.modify pts[1,2], "text", replace$( str$(ps[1,2]), ".0", "")
  gr.modify pts[2,2], "text", replace$( str$(ps[2,2]), ".0", "")
  lstpl   =val(word$(ln$[1],8,","))
  lstmv   =val(word$(ln$[1],9,","))
  AI      =val(word$(ln$[1],10,","))

  gr.modify lspt[j], "y", dpy-30
  gr.show lspt[j]
  gr.modify lspt[3-j], "y", dpy+dc*lstpl-30
  gr.show lspt[3-j]
  for c=1 to 7
    tched[c,1] =val(word$(ln$[2],c,","))
    if !tched[c,1] | AI =1 then gr.modify chx[c,1], "alpha", 60
    tched[c,2] =val(word$(ln$[3],c,","))
    if !tched[c,2] | AI =2 then gr.modify chx[c,2], "alpha", 60
  next
  for u=6 to 39
    px =val(left$( word$(ref$,u),2))
    py =val(right$(word$(ref$,u),1))
    nv =val(mid$(ln$[4],u-5,1))
    if !nv
      gr.hide c[px,py,2]
    elseif c[px,py,3]<>nv
      if nv =1
        gr.modify c[px,py,2], "bitmap", boule1
      elseif nv =2
        gr.modify c[px,py,2], "bitmap", boule2
      endif
    endif
    c[px,py,3] =nv
  next
  array.sum nt, tched[]        % la croix rouge...?
  if tched[lstpl,j] & nt >2
    gr.modify lspcrx, "x", dpx+(1.3*(j=1)+12.7*(j=2))*dc
    gr.modify lspcrx, "y", dpy+dc*lstpl-30
    gr.show lspcrx
  endif
  array.delete ln$[]
  File.delete fe, fileSav$    % delete last game file
endif
return

savegame:
cls
if !FIN & (pos[1]>1 | pos[2]>1)
  print j, pos[1], pos[2], ps[1,1], ps[2,1], ps[1,2], ps[2,2], lstpl, lstmv, AI
  print tched[1,1],tched[2,1],tched[3,1],tched[4,1],tched[5,1],tched[6,1],tched[7,1]
  print tched[1,2],tched[2,2],tched[3,2],tched[4,2],tched[5,2],tched[6,2],tched[7,2]
  for u=6 to 39
    px =val(left$( word$(ref$,u),2))
    py =val(right$(word$(ref$,u),1))
    si$ = si$ +replace$(str$(c[px,py,3]),".0","")
  next
  print si$
  console.save fileSav$
  cls
endif
gr.close
return
