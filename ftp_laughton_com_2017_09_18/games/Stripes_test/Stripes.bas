! Cassiope's stripes game

! --------------------------------------------------------
! Variables to make the code faster and better for reading
! Boolean values
true = 1
false = 0
null = -1

! Numbers
zero = 0
one = 1
two = 2
four = 4
six = 6
eight = 8
eighteen = 18
twenty = 20
ninety = 90
two_hundred_seventy = 270

! Resources path
resourcesPath$ = "user/Stripes/"

! Text align values
left = 1
center = 2
right = 3

! Tilesheet constants
TILE_SIZE = 32
NUM_ROWS = 6
NUM_COLUMNS = 6

! Directions
! 	N
! W + E
! 	S
west = 0
north = 1
east = 2
south = 3

! Side values
horizontal = 0
vertical = 1
color_red = 2
color_blue = 3

! Inventory actions
remove = 0
place_back = 1

! Inventory owners
player = 0
cpu = 1

! --------------------------------------------------------
! Game data
DIM PLAYER_INVENTORY_TILES[6,4]
DIM CPU_INVENTORY_TILES[6,4]

READ.DATA -1, -1, -1, 18, -1, -1, 19, 20, -1, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35
FOR i = 1 TO 6
	FOR j = 1 TO 4
		READ.NEXT tempIndex
		PLAYER_INVENTORY_TILES[i,j] = tempIndex
	NEXT
NEXT

READ.DATA 0, -1, -1, -1, 1, 2, -1, -1, 3, 4, 5, -1, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17
FOR i = 1 TO 6
	FOR j = 1 TO 4
		READ.NEXT tempIndex
		CPU_INVENTORY_TILES[i,j] = tempIndex
	NEXT
NEXT

! --------------------------------------------------------
! Open the graphics
GR.OPEN 255, 255, 215, 0
GR.ORIENTATION 0
GR.SCREEN screenWidth, screenHeight

! Set the measure unit
tileSize = FLOOR(screenWidth / 15)
halfTile = tileSize / two

! Board and inventorys status
DIM playerInventory[6,4]
DIM cpuInventory[6,4]
DIM gameBoard[6,6,2]

! Loading scene initials
loading_bar_x1 = tileSize
loading_bar_y1 = (screenHeight / two) - (halfTile / two)
loading_bar_x2 = loading_bar_x1
loading_bar_y2 = loading_bar_y1 + halfTile
loading_bar_final_width = screenWidth - (tileSize * two)

! Set the component locations
player_inventory_x1 = screenWidth - (four * tileSize)
player_inventory_y1 = tileSize
cpu_inventory_x1 = zero
cpu_inventory_y1 = tileSize
game_board_x1 = tileSize * 4.5
game_board_y1 = tileSize

! Set game board and player inventory limits for touch purposes
player_inventory_x2 = player_inventory_x1 + (four * tileSize)
player_inventory_y2 = player_inventory_y1 + (six * tileSize)
game_board_x2 = game_board_x1 + (six * tileSize)
game_board_y2 = game_board_y1 + (six * tileSize)

! Load the tilesheet and empty tiles
GR.BITMAP.LOAD tileSheet, resourcesPath$ + "tilesheet.png"
GR.BITMAP.LOAD tempPtr, resourcesPath$ + "player_empty_tile.png"
GR.BITMAP.SCALE inventoryEmptyTile, tempPtr, tileSize, tileSize
GR.BITMAP.DELETE tempPtr
GR.BITMAP.LOAD tempPtr, resourcesPath$ + "board_empty_tile.png"
GR.BITMAP.SCALE boardEmptyTile, tempPtr, tileSize, tileSize
GR.BITMAP.DELETE tempPtr

! Create returnable bitmaps for the functions
GR.BITMAP.CREATE returned_bitmapPtr, tileSize, tileSize
GR.BITMAP.CREATE returned_playerInventoryPtr, tileSize * four, tileSize * eight
GR.BITMAP.CREATE returned_cpuInventoryPtr, tileSize * four, tileSize * eight
GR.BITMAP.CREATE returned_gameBoardPtr, tileSize * six, tileSize * six

! Create the moveable piece (utility bitmap)
GR.BITMAP.CREATE moveableBitmap, tileSize, tileSize

! Go to the main loop
GOSUB mainLoop

! --------------------------------------------------------
! Loading screen functions
! Create loading scene
! +++ --- +++ --- +++
function_createLoading:
	GR.COLOR 255, 255, 255, 255, 1
	GR.RECT loadingBarPtr, loading_bar_x1, loading_bar_y1, loading_bar_x2, loading_bar_y2
	GR.TEXT.SIZE tileSize
	GR.TEXT.ALIGN center
	GR.TEXT.DRAW loadingTextPtr, screenWidth / two, screenHeight / two - tileSize, "STRIPES"
RETURN

! Update loading scene
! Parameters
	param_totalUpdates = null
! +++ --- +++ --- +++
function_updateLoading:
	updateCount += 1
	GR.MODIFY loadingBarPtr, "right", loading_bar_x2 + (one / param_totalUpdates * updateCount) * loading_bar_final_width
	GR.RENDER
RETURN

! Destroy loading scene
! +++ --- +++ --- +++
function_destroyLoading:
	GR.CLS
	updateCount = zero
RETURN

! --------------------------------------------------------
! Function to create each player's inventory
! +++ --- +++ --- +++
function_createPlayersInventory:
	
	param_deleteOld = true
	GR.BITMAP.DELETE returned_playerInventoryPtr
	GR.BITMAP.DELETE returned_cpuInventoryPtr
	GR.BITMAP.CREATE returned_playerInventoryPtr, tileSize * four, tileSize * eight
	GR.BITMAP.CREATE returned_cpuInventoryPtr, tileSize * four, tileSize * eight
	
	% Player inventory
	GR.BITMAP.DRAWINTO.START returned_playerInventoryPtr
		FOR i = 1 TO 6
			FOR j = 1 TO 4	
				param_pieceIndex = PLAYER_INVENTORY_TILES[i,j]
				
				IF param_pieceIndex <> null
					param_direction = west
					GOSUB function_getBitmap
					
					x1 = (j - one) * tileSize 
					y1 = (i - one) * tileSize 
					GR.BITMAP.DRAW tempPtr, returned_bitmapPtr, x1, y1
				ELSE
				
					x1 = (j - one) * tileSize 
					y1 = (i - one) * tileSize 
					GR.BITMAP.DRAW tempPtr, inventoryEmptyTile, x1, y1
				ENDIF
				
				% Update the loading bar
				GOSUB function_updateLoading
			NEXT
		NEXT
	GR.BITMAP.DRAWINTO.END
	
	% CPU inventory
	GR.BITMAP.DRAWINTO.START returned_cpuInventoryPtr
		FOR i = 1 TO 6
			FOR j = 1 TO 4
				param_pieceIndex = CPU_INVENTORY_TILES[i,j]
				
				IF param_pieceIndex <> null
					param_direction = west
					GOSUB function_getBitmap
					
					x1 = (j - one) * tileSize 
					y1 = (i - one) * tileSize 
					GR.BITMAP.DRAW tempPtr, returned_bitmapPtr, x1, y1
				ELSE
				
					x1 = (j - one) * tileSize 
					y1 = (i - one) * tileSize 
					GR.BITMAP.DRAW tempPtr, inventoryEmptyTile, x1, y1
				ENDIF
				
				% Update the loading bar
				GOSUB function_updateLoading
			NEXT
		NEXT
	GR.BITMAP.DRAWINTO.END
RETURN

! --------------------------------------------------------
! Function to create the game board
! +++ --- +++ --- +++
function_createGameBoard:
	GR.BITMAP.DELETE returned_gameBoardPtr
	GR.BITMAP.CREATE returned_gameBoardPtr, tileSize * six, tileSize * six
	GR.BITMAP.DRAWINTO.START returned_gameBoardPtr
		FOR i = 1 TO 6
			FOR j = 1 TO 6
				x1 = (j - one) * tileSize
				y1 = (i - one) * tileSize
				GR.BITMAP.DRAW tempPtr, boardEmptyTile, x1, y1
				
				% Update the loading bar
				GOSUB function_updateLoading
			NEXT
		NEXT
	GR.BITMAP.DRAWINTO.END
RETURN

! --------------------------------------------------------
! Function to rotate a bitmap
! Parameters
	param_deleteOld = true
	param_pieceIndex = null
	param_direction = west
	returned_bitmapPtr = null
! Note: Every piece is west oriented by default
! +++ --- +++ --- +++
function_getBitmap:
	% Delete the old bitmap
	IF param_deleteOld = true THEN GR.BITMAP.DELETE returned_bitmapPtr

	% Locate and crop the piece by its index
	x1 = MOD(param_pieceIndex, NUM_ROWS) * TILE_SIZE
	y1 = FLOOR(param_pieceIndex / NUM_ROWS) * TILE_SIZE
	GR.BITMAP.CROP cropPtr, tileSheet, x1, y1, TILE_SIZE, TILE_SIZE
	
	% Change the scale and direction of the bitmap
	IF param_direction = west THEN
		GR.BITMAP.SCALE returned_bitmapPtr, cropPtr, tileSize, tileSize
		
	ELSE IF param_direction = north THEN
		GR.BITMAP.CREATE returned_bitmapPtr, tileSize, tileSize
		GR.BITMAP.DRAWINTO.START returned_bitmapPtr
			GR.ROTATE.START ninety, halfTile, halfTile
				GR.BITMAP.SCALE tempPtr, cropPtr, tileSize, tileSize
				GR.BITMAP.DRAW tempPtr, tempPtr, zero, zero
			GR.ROTATE.END
		GR.BITMAP.DRAWINTO.END
		
	ELSE IF param_direction = east THEN
		GR.BITMAP.SCALE returned_bitmapPtr, cropPtr, tileSize, -tileSize
		
	ELSE IF param_direction = south THEN
		GR.BITMAP.CREATE returned_bitmapPtr, tileSize, tileSize
		GR.BITMAP.DRAWINTO.START returned_bitmapPtr
			GR.ROTATE.START two_hundred_seventy, halfTile, halfTile
				GR.BITMAP.SCALE tempPtr, cropPtr, tileSize, tileSize
				GR.BITMAP.DRAW tempPtr, tempPtr, zero, zero
			GR.ROTATE.END
		GR.BITMAP.DRAWINTO.END
		
	ENDIF
	
	% Collect the garbage left
	GR.BITMAP.DELETE cropPtr
	
RETURN % returned_bitmapPtr

! --------------------------------------------------------
! Function to remove/replace a tile in a player inventory
! Parameters
	param_inventoryOwner = null
	param_inventoryAction = null
	param_inventoryRow = null
	param_inventoryColumn = null
! +++ --- +++ --- +++
function_changeInventory:
	IF param_inventoryOwner = player
		IF param_inventoryAction = remove
			GR.BITMAP.DRAWINTO.START returned_playerInventoryPtr
				GR.BITMAP.DRAW tempPtr, inventoryEmptyTile, (param_inventoryColumn - one) * tileSize,~
				(param_inventoryRow - one) * tileSize
			GR.BITMAP.DRAWINTO.END
		ELSE
			GR.BITMAP.DRAWINTO.START returned_playerInventoryPtr				
				GR.BITMAP.DRAW tempPtr, returned_bitmapPtr, (param_inventoryColumn - one) * tileSize,~
				(param_inventoryRow - one) * tileSize
			GR.BITMAP.DRAWINTO.END
		ENDIF
		
		% Modify the old inventory bitmap
		GR.MODIFY playerInventoryPtr, "bitmap", returned_playerInventoryPtr
	ELSE
		
	ENDIF
RETURN

! --------------------------------------------------------
! Function to change the game board
! Parameters
	param_boardRow = null
	param_boardColumn = null
! +++ --- +++ --- +++
function_changeGameBoard:
	GR.BITMAP.DRAWINTO.START returned_gameBoardPtr
		GR.BITMAP.DRAW tempPtr, returned_bitmapPtr, (param_boardColumn - one) * tileSize,~
		(param_boardRow - one) * tileSize
	GR.BITMAP.DRAWINTO.END
	
	% Change the bitmap of the drawed board
	GR.MODIFY gameBoardBoardPtr, "bitmap", returned_gameBoardPtr
RETURN

! --------------------------------------------------------
! Function to start a new game
! +++ --- +++ --- +++
function_startNewGame:
	% Load the game resources
	param_totalUpdates = 84
	GOSUB function_createLoading
	GOSUB function_createPlayersInventory
	GOSUB function_createGameBoard
	GOSUB function_destroyLoading
	
	% Draw all the components
	GR.BITMAP.DRAW playerInventoryPtr, returned_playerInventoryPtr, player_inventory_x1, player_inventory_y1
	GR.BITMAP.DRAW cpuInventoryPtr, returned_cpuInventoryPtr, cpu_inventory_x1, cpu_inventory_y1
	GR.BITMAP.DRAW gameBoardBoardPtr, returned_gameBoardPtr, game_board_x1, game_board_y1
	GR.BITMAP.DRAW moveablePiecePtr, moveableBitmap, zero, zero
	
	% Hide the moveable piece at first
	GR.HIDE moveablePiecePtr
	
	% Set the first move flags
	playerStartMoveOk = false
	cpuStartMoveOk = false
RETURN

! --------------------------------------------------------
! Touch screen handler
! +++ --- +++ --- +++
touchHandler:
	GR.TOUCH touched, touch_x, touch_y
	
	IF touched = true
		IF holdingPiece = true
			
			% Update the position of the piece being held
			GR.MODIFY moveablePiecePtr, "x", touch_x - halfTile
			GR.MODIFY moveablePiecePtr, "y", touch_y - halfTile
			
			% Check if the touch is inside the game board
			IF touch_x > game_board_x1 &~
			touch_x < game_board_x2 &~
			touch_y > game_board_y1 &~
			touch_y < game_board_y2
				
				touchedOverBoard = true
			ELSE
				touchedOverBoard = false
			ENDIF
			
		ELSE
			% Check if the touch is inside the player inventory
			IF touch_x > player_inventory_x1 &~
			touch_x < player_inventory_x2 &~
			touch_y > player_inventory_y1 &~
			touch_y < player_inventory_y2
				
				% Check if the player already did the first move
				IF playerStartMoveOk = true
					% Get the piece index in the touched location
					param_inventoryRow = CEIL((touch_y - player_inventory_y1) / tileSize)
					param_inventoryColumn = CEIL((touch_x - player_inventory_x1) / tileSize)
					param_pieceIndex = PLAYER_INVENTORY_TILES[param_inventoryRow, param_inventoryColumn]
				ELSE
					% Force the player to pick the first piece
					param_inventoryRow = one
					param_inventoryColumn = four
					param_pieceIndex = eighteen
				ENDIF
				
				% Check if this isn't an empty space to proceed
				IF param_pieceIndex <> null
				
					% Get the bitmap of the touched piece
					param_direction = west
					GOSUB function_getBitmap
					
					% Remove the piece from the inventory
					param_inventoryOwner = player
					param_inventoryAction = remove
					GOSUB function_changeInventory
					
					% Update the piece being held
					GR.SHOW moveablePiecePtr
					GR.MODIFY moveablePiecePtr, "x", touch_x - halfTile
					GR.MODIFY moveablePiecePtr, "y", touch_y - halfTile
					GR.MODIFY moveablePiecePtr, "bitmap", returned_bitmapPtr
					
					holdingPiece = true
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF holdingPiece = true
			IF touchedOverBoard = true
				param_boardRow = CEIL((touch_y - game_board_y1) / tileSize)
				param_boardColumn = CEIL((touch_x - game_board_x1) / tileSize)
				GOSUB function_changeGameBoard
				
				playerStartMoveOk = true

				% Give the piece back to the inventory
				param_inventoryOwner = player
				param_inventoryAction = place_back
				GOSUB function_changeInventory

				holdingPiece = false
			ENDIF
			
			% Hide the moveable piece
			GR.HIDE moveablePiecePtr
		ENDIF
	ENDIF
RETURN

! --------------------------------------------------------
! Main loop
! +++ --- +++ --- +++
mainLoop:
	% Create a new game
	GOSUB function_startNewGame
	
	DO
		IF currentTurn = player
			GOSUB touchHandler
		ELSE
			% CPU turn
		ENDIF
		
		GR.RENDER
		PAUSE twenty
	UNTIL false
RETURN