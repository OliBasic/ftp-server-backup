! --- GRAPHICS INITIALIZATION ---

GR.OPEN 255, 0, 0, 0
GR.ORIENTATION 0
GR.SCREEN sWd, sHe
GR.COLOR 255, 255, 255, 255, 0

! --- GAME CONSTANTS ---

path$ = "user/sideScroller/"

% Numbers:
One = 1
Two = 2
Three = 3
Four = 4
Five = 5

oneSecond = 1000
frameDelay = oneSecond/6

blockSize = sHe/8	% blockSize is the measure unit here
block99 = blockSize-1
block75 = blockSize/1.5
block50 = blockSize/2
block25 = blockSize/4
block24 = block25-1
block10 = blockSize/10

midOTSh = sWd/2	% 1/2 of the Screen Horizontally
midOTSv = sHe/2	% 1/2 Vertically
oneTscr = sWd/3	% 1/3 H
twoTscr = (sWd/3)*2 	% 2/3 H

gForce = sHe/32

! --- HERO DESCRIPTION ---

DIM heroSpr[2,3,4]
heroFace = Two
wFrame = One

% 1 - Standing
% 2 - Moving
% 3 - Jumping

heroPosX = 3*blockSize
heroPosY = 6*blockSize

heroStageX = heroPosX

heroSpeedX = 0	% Hero move direction X
heroSpeedY = 0	% Hero move direction Y
moveSpeed = blockSize/4

! --- HERO TEMPORARY SPRITE ---

GR.BITMAP.LOAD temp_ptr, path$+"heroSprSheet.png"
GR.BITMAP.CROP tempCrop_ptr, temp_ptr, 176, 32, 16, 16
GR.BITMAP.SCALE heroSpr[2,1,1], tempCrop_ptr, blockSize, blockSize
GR.BITMAP.SCALE heroSpr[1,1,1], tempCrop_ptr, -blockSize, blockSize
GR.BITMAP.DELETE tempCrop_ptr

GR.BITMAP.CROP tempCrop_ptr, temp_ptr, 80, 32, 16, 16
GR.BITMAP.SCALE heroSpr[2,2,1], tempCrop_ptr, blockSize, blockSize
GR.BITMAP.SCALE heroSpr[1,2,1], tempCrop_ptr, -blockSize, blockSize
GR.BITMAP.DELETE tempCrop_ptr

GR.BITMAP.CROP tempCrop_ptr, temp_ptr, 96, 32, 16, 16
GR.BITMAP.SCALE heroSpr[2,2,2], tempCrop_ptr, blockSize, blockSize
GR.BITMAP.SCALE heroSpr[1,2,2], tempCrop_ptr, -blockSize, blockSize
GR.BITMAP.DELETE tempCrop_ptr

GR.BITMAP.CROP tempCrop_ptr, temp_ptr, 112, 32, 16, 16
GR.BITMAP.SCALE heroSpr[2,2,3], tempCrop_ptr, blockSize, blockSize
GR.BITMAP.SCALE heroSpr[1,2,3], tempCrop_ptr, -blockSize, blockSize
GR.BITMAP.DELETE tempCrop_ptr

GR.BITMAP.CROP tempCrop_ptr, temp_ptr, 96, 32, 16, 16
GR.BITMAP.SCALE heroSpr[2,2,4], tempCrop_ptr, blockSize, blockSize
GR.BITMAP.SCALE heroSpr[1,2,4], tempCrop_ptr, -blockSize, blockSize
GR.BITMAP.DELETE tempCrop_ptr

GR.BITMAP.CROP tempCrop_ptr, temp_ptr, 144, 32, 16, 16
GR.BITMAP.SCALE heroSpr[2,3,1], tempCrop_ptr, blockSize, blockSize
GR.BITMAP.SCALE heroSpr[1,3,1], tempCrop_ptr, -blockSize, blockSize
GR.BITMAP.DELETE tempCrop_ptr
GR.BITMAP.DELETE temp_ptr

! --- TEMPORARY MAP DRAW ---

DIM mapGrid[8,30]
DIM mapTile[2]

mapGrid[3, 5] = 1
mapGrid[3, 6] = 1
mapGrid[4, 14] = 1
mapGrid[5, 10] = 2
mapGrid[5, 14] = 1
mapGrid[5, 15] = 1
mapGrid[5, 19] = 1
mapGrid[6, 1] = 1
mapGrid[6, 10] = 2
mapGrid[6, 18] = 1
mapGrid[6, 19] = 1

FOR i = 26 TO 30
	mapGrid[6,i] = 2
	IF i > 26 THEN mapGrid[5,i] = 2
	IF i > 27 THEN mapGrid[4,i] = 2
	IF i > 28 THEN mapGrid[3,i] = 2
NEXT

FOR i = 1 TO 2
	FOR j = 1 TO 30
		mapGrid[6+i,j] = 1
	NEXT
NEXT

GR.BITMAP.LOAD temp_ptr, path$+"TilesetSMB.png"

GR.BITMAP.CROP tempCrop_ptr, temp_ptr, 0, 0, 16, 16
GR.BITMAP.SCALE mapTile[1], tempCrop_ptr, blockSize, blockSize
GR.BITMAP.DELETE tempCrop_ptr

GR.BITMAP.CROP tempCrop_ptr, temp_ptr, 0, 16, 16, 16
GR.BITMAP.SCALE mapTile[2], tempCrop_ptr, blockSize, blockSize
GR.BITMAP.DELETE tempCrop_ptr
GR.BITMAP.DELETE temp_ptr

GR.BITMAP.CREATE stage_ptr, blockSize*30, blockSize*8
GR.BITMAP.DRAWINTO.START stage_ptr
FOR i = 1 TO 8
	FOR j = 1 TO 30
		tileNum = mapGrid[i,j]
		IF tileNum > Zero
			GR.BITMAP.DRAW temp_ptr, mapTile[tileNum], (j-1)*blockSize, (i-1)*blockSize
		ENDIF
	NEXT
NEXT

!!
GR.COLOR 255, 255, 200, 0, 0
FOR i = 1 TO 8
	GR.LINE line, Zero, i*blockSize, 30*blockSize, i*blockSize
	FOR j = 1 TO 30
		GR.LINE line, j*blockSize, Zero, j*blockSize, 8*blockSize
	NEXT
NEXT
!!

GR.BITMAP.DRAWINTO.END

stageLimitX = -(blockSize*30)+sWd

! --- MAIN GAME LOOP ---

fpsB = 1
DO
	fpsA = CLOCK()
	GR.TOUCH touchEvent, evX, evY
	IF touchEvent THEN GOSUB inGame_Touch

	GR.CLS
	
	GOSUB hero_Movement
	GOSUB stage_Scrolling
	GOSUB hero_Animation

!!
	GR.COLOR 255, 0, 255, 255, 1

             	GR.CIRCLE pointA, heroPosX-block24, heroPosY-blockSize, Five
	GR.CIRCLE pointB, heroPosX+block25, heroPosY-blockSize, Five
	GR.CIRCLE pointC, heroPosX-block24, heroPosY, Five
	GR.CIRCLE pointD, heroPosX+block25, heroPosY, Five

	GR.COLOR 255, 255, 255, 255, 1
	GR.TEXT.SIZE block25
	GR.TEXT.DRAW text1, blockSize, block75, "stageX: "+STR$(heroStageX/blockSize)
	GR.TEXT.DRAW text2, blockSize, blockSize, "stageY: "+STR$(heroPosY/blockSize)
!!

	GR.COLOR 255, 255, 255, 255, 1
	GR.TEXT.DRAW fps, blockSize, block50, "FPS :"+FORMAT$("##.##",oneSecond/fpsB)

	GR.RENDER
	fpsB = CLOCK()-fpsA
UNTIL 0

! --- GAME CONTROLS ---

inGame_Touch:
	IF evX > twoTscr
		heroSpeedX = moveSpeed
	ELSEIF evX < oneTscr
		heroSpeedX = -moveSpeed
	ENDIF

	IF evX > oneTscr & evX < twoTscr
		heroSpeedY = -block50
	ENDIF
RETURN

! --- HERO MOVEMENT ---

hero_Movement:
	IF !touchEvent
		heroSpeedX = Zero
	ENDIF

	IF heroSpeedY < gForce
		heroSpeedY += block10
	ENDIF

	heroStageX += heroSpeedX
	heroPosY += heroSpeedY

	heroGridXa = CEIL((heroStageX-block24)/blockSize)
	heroGridXb = CEIL((heroStageX+block25)/blockSize)
	heroGridYa = CEIL((heroPosY-block99)/blockSize)
	heroGridYb = CEIL((heroPosY)/blockSize)

	IF heroPosY > blockSize
		heroPa = mapGrid[heroGridYa,heroGridXa]
		heroPb = mapGrid[heroGridYa,heroGridXb]
		heroPc = mapGrid[heroGridYb,heroGridXa]
		heroPd = mapGrid[heroGridYb,heroGridXb]
	ENDIF

	IF heroSpeedY < Zero
		IF heroPa > Zero & heroPb > Zero
			heroPosY = heroGridYb*blockSize
		ELSEIF heroPa > Zero & heroPb = Zero
			heroStageX = heroGridXa*blockSize+block25
		ELSEIF heroPa = Zero & heroPb > Zero
			heroStageX = heroGridXa*blockSize-block25
		ENDIF
		heroLand = Zero
	ELSEIF heroSpeedY > Zero
		IF heroPc > Zero | heroPd > Zero
			heroPosY = heroGridYa*blockSize
			heroLand = One
			heroPc = Zero
			heroPd = Zero
		ENDIF
	ENDIF

	IF heroSpeedX < Zero
		IF heroPa > Zero | heroPc > Zero
			heroStageX = heroGridXa*blockSize+block25
		ENDIF
		heroFace = One
	ELSEIF heroSpeedX > Zero
		IF heroPb > Zero | heroPd > Zero
			heroStageX = heroGridXa*blockSize-block25
		ENDIF
		heroFace = Two
	ENDIF
RETURN
 
! --- HERO ANIMATION ---

hero_Animation:
	IF heroLand = One
		IF heroSpeedX = Zero
			GR.BITMAP.DRAW heroSprite, heroSpr[heroFace,One,One], heroPosX-block50,~
			heroPosY-blockSize
		ELSE
			walkDelay += fpsB
			IF walkDelay >= frameDelay
				wFrame ++
				walkDelay = Zero
			ENDIF
			IF wFrame > Four THEN wFrame = One
			GR.BITMAP.DRAW heroSprite, heroSpr[heroFace,Two,wFrame], heroPosX-block50,~
			heroPosY-blockSize
		ENDIF
	ELSE
		GR.BITMAP.DRAW heroSprite, heroSpr[heroFace,Three,One], heroPosX-block50,~
		heroPosY-blockSize
	ENDIF
RETURN

! --- STAGE SCROLLING ---

stage_Scrolling:
	IF heroSpeedX > Zero & heroStageX+stagePosX > midOTSh
		stageScroll = One
	ELSEIF heroSpeedX < Zero & heroStageX+stagePosX < midOTSh
		stageScroll = One
	ELSE
		stageScroll = Zero
	ENDIF

	IF stageScroll
		IF stagePosX >= Zero & heroSpeedX < Zero
			stageScroll = Zero
			stagePosX = Zero
		ELSEIF stagePosX <= stageLimitX & heroSpeedX > Zero
			stageScroll = Zero
			stagePosX = stageLimitX
		ELSE
			stagePosX = midOTSh-heroStageX
		ENDIF
	ENDIF

	heroPosX = heroStageX+stagePosX	% The real position of the Hero on the screen

	GR.BITMAP.DRAW gameScreen, stage_ptr, stagePosX, Zero
RETURN
	