! Aat @2017
GR.OPEN 255,0,0,0,0,0
GR.SCREEN w,h
ScaleX=1400
ScaleY=h/w*ScaleX
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
DataPath$="../../LearnClock/data/"
SOUNDPOOL.OPEN 1:SOUNDPOOL.LOAD Beep1,DataPath$+"beep1.mp3":SOUNDPOOL.LOAD Beep2,DataPath$+"beep2.mp3"
LIST.CREATE N,DrwDat
LIST.ADD DrwDat,145,254,180,250,218,217,258,169,273,76,296,62,330,83,343,128,334,187,472,192,490,208,494,236,483,255,390,255,483,255
LIST.ADD DrwDat,492,276,493,286,476,318,390,318,476,318,481,342,479,350,471,374,454,382,390,382,454,382,451,412,442,428,419,447,209,448,176,427,145,425
GR.BITMAP.CREATE Flag1,100,60
GR.BITMAP.DRAWINTO.START Flag1
	GR.COLOR 255,255,0,0,1:GR.RECT g,0,0,100,20
	GR.COLOR 255,255,255,255,1:GR.RECT g,0,21,100,40
	GR.COLOR 255,0,0,160,1:GR.RECT g,0,41,100,60
GR.BITMAP.DRAWINTO.END
GR.BITMAP.CREATE Flag2,100,60
GR.BITMAP.DRAWINTO.START Flag2
	GR.COLOR 255,0,0,110,1:GR.RECT g,0,0,100,60
	GR.SET.STROKE 10:GR.COLOR 255,255,255,255,1:GR.LINE g,0,0,100,60:GR.LINE g,0,60,100,0
	GR.SET.STROKE 4:GR.COLOR 255,220,0,0,1:GR.LINE g,0,1,40,25:GR.LINE g,98,0,60,24:GR.LINE g,2,60,40,37:GR.LINE g,100,59,60,36
	GR.SET.STROKE 15:GR.COLOR 255,255,255,255,1:GR.LINE g,0,30,100,30:GR.LINE g,50,0,50,60
	GR.SET.STROKE 8:GR.COLOR 255,220,0,0,1:GR.LINE g,0,30,100,30:GR.LINE g,50,0,50,60
GR.BITMAP.DRAWINTO.END
GR.SET.STROKE 20:GR.SET.ANTIALIAS 0
GR.BITMAP.CREATE MakeHand,512,452
GR.BITMAP.DRAWINTO.START MakeHand
	GR.COLOR 255,255,255,255,1:GR.RECT g,15,230,145,445
	GR.COLOR 255,0,255,0,1:GR.POLY g,DrwDat,0,0
	GR.COLOR 255,0,0,255,0:GR.RECT g,15,230,145,445:GR.POLY g,DrwDat,0,0
GR.BITMAP.DRAWINTO.END
GR.BITMAP.SCALE Hand2,MakeHand,512,-452
GR.COLOR 255,255,0,0,1:GR.BITMAP.FILL Hand2,250,220:GR.SET.ANTIALIAS 1
GR.BITMAP.CREATE MakeHour,21,125
GR.BITMAP.DRAWINTO.START MakeHour
	GR.SET.STROKE 21
	GR.COLOR 255,255,0,0,1:GR.LINE g,10,25,10,125
	GR.COLOR 255,255,128,0,1:GR.CIRCLE g,10,15,13
	GR.SET.STROKE 5:GR.LINE g,10,0,10,125
GR.BITMAP.DRAWINTO.END
GR.BITMAP.CREATE MakeMinute,21,175
GR.BITMAP.DRAWINTO.START MakeMinute
	GR.SET.STROKE 13
	GR.COLOR 255,0,128,0,1:GR.LINE g,10,25,10,175
	GR.COLOR 255,0,128,255,1:GR.CIRCLE g,10,15,13
	GR.SET.STROKE 5:GR.LINE g,10,0,10,175
GR.BITMAP.DRAWINTO.END
GR.BITMAP.CREATE Mouth,230,135
GR.BITMAP.DRAWINTO.START Mouth
	GR.SET.STROKE 7
	GR.COLOR 255,0,0,0,0:GR.ARC g,3,50,220,80,0,180,0:GR.ARC g,3,0,220,130,0,180,0
	GR.LINE g,3,60,0,80:GR.LINE g,220,60,220,90
	GR.SET.STROKE 3:GR.COLOR 255,255,255,255,1:GR.BITMAP.FILL Mouth,100,100
GR.BITMAP.DRAWINTO.END
TTS.INIT:OK=0:Lang=1
GR.COLOR 255,255,255,0,0
GR.TEXT.SIZE 25
GR.TEXT.DRAW g,560,25,"nederlands":GR.TEXT.DRAW g,730,25,"english":GR.RECT L,570,45,680,115
GR.BITMAP.DRAW g,Flag1,575,50:GR.BITMAP.DRAW g,Flag2,725,50
GR.COLOR 255,255,255,255,1:GR.RECT g,575,130,655,230:GR.RECT g,660,130,740,230:GR.RECT g,745,130,825,230
GR.COLOR 255,0,0,255,0:GR.RECT g,575,130,655,230:GR.RECT g,660,130,740,230:GR.RECT g,745,130,825,230
GR.TEXT.SIZE 60:GR.TEXT.DRAW g,600,200,"1":GR.TEXT.DRAW g,685,200,"2":GR.TEXT.DRAW g,770,200,"3"
GR.COLOR 255,255,255,255,1:GR.TEXT.SIZE 40:GR.TEXT.DRAW NL,300,200,"Start met Nivo":GR.TEXT.DRAW EN,850,200,"Start at Level"
GR.HIDE EN:GR.TEXT.UNDERLINE 1:GR.TEXT.DRAW g,20,295,"Uitleg":GR.TEXT.DRAW g,720,295,"Explanation":GR.TEXT.UNDERLINE 0
GR.TEXT.SIZE 35:GR.TEXT.DRAW g,20,335,"Leer je kind klokkijken.":GR.TEXT.DRAW g,720,335,"Teach your child to tell the time."
GR.TEXT.DRAW g,20,375,"Probeer eerst de beginselen uit te leggen.":GR.TEXT.DRAW g,720,375,"Try to explain the principles first."
GR.TEXT.DRAW g,20,415,"Gebruik deze app om de voortgang te testen.":GR.TEXT.DRAW g,720,415,"Use this app to test the progress."
GR.TEXT.DRAW g,20,455,"Nivo 1 test alleen hele uren.":GR.TEXT.DRAW g,720,455,"Level 1 only tests the hours."
GR.TEXT.DRAW g,20,495,"Nivo 2 voegt kwartieren toe.":GR.TEXT.DRAW g,720,495,"Level 2 adds quarters."
GR.TEXT.DRAW g,20,535,"Nivo 3 werkt per 5 minuten.":GR.TEXT.DRAW g,720,535,"Level 3 operates on 5 minute intervals."
GR.TEXT.DRAW g,20,575,"Als 5/5 vragen goed zijn beantwoord":GR.TEXT.DRAW g,720,575,"If 5/5 questions are answered correcty"
GR.TEXT.DRAW g,20,615,"wordt het nivo verhoogd.":GR.TEXT.DRAW g,720,615,"the level is increased."
GR.TEXT.DRAW g,20,655,"Tik op de blauwe knoppen om de tijd":GR.TEXT.DRAW g,720,655,"Tap the blue buttons to hear the time"
GR.TEXT.DRAW g,20,695,"gesproken te horen.":GR.TEXT.DRAW g,720,695,"being spoken."
GR.TEXT.DRAW g,20,735,"Tik op het vakje ernaast om":GR.TEXT.DRAW g,720,735,"Tap the box next to the button"
GR.TEXT.DRAW g,20,775,"het goede antwoord te selecteren.":GR.TEXT.DRAW g,720,775,"to select the correct answer."
GR.COLOR 255,0,0,0,1:GR.RECT HL,720,240,1350,ScaleY
GR.RENDER
DO
	GOSUB GetTouch
	IF x>575 & X<675 & y>50 & y<110 THEN
		Lang=1:GR.MODIFY L,"left",570,"right",680:GR.MODIFY HL,"left",720,"right",1350:GR.SHOW NL:GR.HIDE EN
		GR.RENDER:TTS.SPEAK "nederlands"
	ENDIF
	IF x>725 & X<825 & y>50 & y<110 THEN
		Lang=2:GR.MODIFY L,"left",720,"right",830:GR.MODIFY HL,"left",20,"right",719:GR.SHOW EN:GR.HIDE NL
		GR.RENDER:TTS.SPEAK "english"
	ENDIF
	IF x>575 & X<825 & y>130 & y<230 THEN
		Level=INT((x-575)/83.3)+1
		OK=1
	ENDIF
UNTIL OK
GR.CLS
GR.COLOR 255,92,128,255,1:GR.CIRCLE g,380,120,30:GR.CIRCLE g,820,120,30
GR.COLOR 255,255,128,92,1:GR.CIRCLE g,460,220,140:GR.CIRCLE g,740,220,140
GR.SET.STROKE 8:GR.COLOR 255,0,0,0,0:GR.CIRCLE g,600,400,295
GR.COLOR 255,92,128,255,1:GR.SET.STROKE 80:GR.LINE g,470,650,410,750:GR.LINE g,730,650,790,750
GR.SET.STROKE 1:GR.COLOR 255,255,255,92,1:GR.CIRCLE g,600,400,285
GR.COLOR 255,255,255,255,1:GR.OVAL g,500,250,600,420:GR.COLOR 255,64,64,64,0:GR.OVAL g,500,250,600,420
GR.COLOR 255,0,0,0,1:GR.CIRCLE g,550,350,20
GR.ROTATE.START 10,650,335
	GR.COLOR 255,255,255,255,1:GR.OVAL g,600,250,700,420
	GR.COLOR 255,64,64,64,0:GR.OVAL g,600,250,700,420
	GR.COLOR 255,0,0,0,1:GR.CIRCLE g,650,330,20
GR.ROTATE.END
GR.ROTATE.START -10,600,510,MoveMouth
	GR.BITMAP.DRAW g,Mouth,490,430
GR.ROTATE.END
GR.COLOR 255,92,128,92,0
FOR i=0 TO 354 STEP 6
	GR.ROTATE.START i,600,400
		GR.LINE g,600,120,600,135
	GR.ROTATE.END
NEXT i
GR.SET.STROKE 20
FOR i=0 TO 354 STEP 30
	GR.ROTATE.START i,600,400
		GR.LINE g,600,120,600,155
	GR.ROTATE.END
NEXT i
GR.CIRCLE g,600,400,285
GR.SET.STROKE 3:GR.COLOR 255,255,255,0,1:GR.TEXT.ALIGN 2:GR.TEXT.SIZE 40:GR.TEXT.BOLD 1:GR.COLOR 255,64,64,64,1
FOR i=1 TO 12
	GR.ROTATE.START i*30,600,420
		GR.ROTATE.START -i*30,600,200
			GR.TEXT.DRAW g,600,200,INT$(i)
		GR.ROTATE.END
	GR.ROTATE.END
NEXT i
GR.TEXT.ALIGN 1
GR.ROTATE.START 0,600,400,MHand
	GR.BITMAP.DRAW g,MakeMinute,590,200
GR.ROTATE.END
GR.ROTATE.START 0,600,400,HHand
	GR.BITMAP.DRAW g,MakeHour,590,275
GR.ROTATE.END
GR.COLOR 255,92,92,92,1:GR.CIRCLE g,600,400,25:GR.COLOR 255,0,0,0,0:GR.CIRCLE g,600,400,25:GR.COLOR 255,0,0,0,1:GR.CIRCLE g,600,400,5
GR.COLOR 255,255,255,255,1
IF Lang=1 THEN
	GR.TEXT.DRAW g,900,100,"Hoe laat is het nu?"
ELSE
	GR.TEXT.DRAW g,900,100,"What time is it now?"
ENDIF
GR.TEXT.ALIGN 2
IF Lang=1 THEN
	GR.TEXT.DRAW g,150,150,"NIVO"
ELSE
	GR.TEXT.DRAW g,150,150,"LEVEL"
ENDIF
IF Lang=1 THEN
	GR.TEXT.DRAW g,150,350,"VRAAG"
ELSE
	GR.TEXT.DRAW g,150,350,"QUESTION"
ENDIF
IF Lang=1 THEN
	GR.TEXT.DRAW g,150,550,"GOED"
ELSE
	GR.TEXT.DRAW g,150,550,"CORRECT"
ENDIF
GR.TEXT.DRAW Nivo,150,225,"1":GR.TEXT.DRAW Vraag,150,425,"1":GR.TEXT.DRAW GOED,150,625,"0"
GR.TEXT.SIZE 70
GR.ROTATE.START 10,1100,700
	IF Lang=1 THEN
		GR.COLOR 255,255,0,0,1:GR.TEXT.DRAW g,1100,700,"Klok kijken"
		GR.COLOR 255,255,255,0,0:GR.TEXT.DRAW g,1100,700,"Klok kijken"
	ELSE
		GR.COLOR 255,255,0,0,1:GR.TEXT.DRAW g,1100,700,"Telling time"
		GR.COLOR 255,255,255,0,0:GR.TEXT.DRAW g,1100,700,"Telling time"
	ENDIF
GR.ROTATE.END
GR.OVAL g,50,75,250,250:GR.OVAL g,30,275,280,450:GR.OVAL g,40,475,260,650
GR.TEXT.SIZE 20
DIM NM[60],Choice[4],Red[4],Green[4],s[4],Hr$[12],Mn2$[4],Mn3$[12],PickHr$[4],PickMn2$[4]
ARRAY.LOAD NH[],0,1,2,3,4,5,6,7,8,9,10,11
FOR i=1 TO 60:NM[i]=i-1:NEXT i
FOR i=1 TO 4
	GR.COLOR 255,255,0,0,1:GR.RECT Red[i],1300,60+i*100,1380,156+i*100:GR.HIDE Red[i]
	GR.COLOR 255,0,255,0,1:GR.RECT Green[i],1300,60+i*100,1380,156+i*100:GR.HIDE Green[i]
	GR.COLOR 255,0,0,255,1:GR.RECT g,920,60+i*100,1280,156+i*100
	GR.COLOR 255,255,255,255,0:GR.RECT g,920,60+i*100,1280,156+i*100:GR.RECT g,1300,60+i*100,1380,156+i*100
	GR.COLOR 255,255,255,255,1:GR.TEXT.DRAW Choice[i],1100,120+i*100,""
	s[i]=i
NEXT i
GR.TEXT.ALIGN 1
FOR i=1+(Lang-1)*24 TO 12+(Lang-1)*24
	READ.FROM i
	READ.NEXT H$
	Hr$[i-(Lang-1)*24]=H$
	READ.FROM i+12
	READ.NEXT M$
	Mn3$[i-(Lang-1)*24]=M$
NEXT i
FOR i=13+(Lang-1)*24 TO 16+(Lang-1)*24
	READ.FROM i
	READ.NEXT M$
	Mn2$[i-12-(Lang-1)*24]=M$
NEXT i
GR.MODIFY Nivo,"text",INT$(Level)
GR.BITMAP.DRAW OK,MakeHand,400,100:GR.HIDE OK
GR.BITMAP.DRAW NotOK,Hand2,400,100:GR.HIDE NotOK
GR.COLOR 255,64,0,0,1:GR.CIRCLE g1,0,ScaleY,140:GR.CIRCLE g2,280,ScaleY,140:GR.CIRCLE g3,560,ScaleY,140:GR.CIRCLE g4,840,ScaleY,140:GR.CIRCLE g5,1120,ScaleY,140:GR.CIRCLE g6,1400,ScaleY,140
GR.COLOR 255,92,0,0,1:GR.RECT F,0,0,1400,ScaleY
GR.TEXT.SIZE 100:GR.COLOR 220,255,255,0,1
IF Lang=1 THEN
	GR.TEXT.DRAW T,450,1600,"Klok Kijken"
ELSE
	GR.TEXT.DRAW T,450,1600,"Telling time"
ENDIF
FOR i=ScaleY TO -145 STEP -10
	GR.MODIFY F,"bottom",i
	GR.MODIFY g1,"y",i:GR.MODIFY g2,"y",i:GR.MODIFY g3,"y",i:GR.MODIFY g4,"y",i:GR.MODIFY g5,"y",i:GR.MODIFY g6,"y",i
	GR.MODIFY T,"y",i-300:GR.RENDER:PAUSE 10
NEXT i
IF Lang=1 THEN
	TTS.SPEAK "We gaan klok kijken"
ELSE
	TTS.SPEAK "Let's go tell the time"
ENDIF
DO
	Correct=0:GR.MODIFY Goed,"text",INT$(Correct)
	GR.MODIFY Nivo,"text",INT$(Level)
	FOR Game=1 TO 5
		GR.MODIFY Vraag,"text",INT$(Game)
		GR.MODIFY HHand,"angle",0
		GR.MODIFY MHand,"angle",0
		GR.MODIFY MoveMouth,"angle",-10
		FOR i=1 TO 4
			GR.MODIFY Choice[i],"text",""
		NEXT i
		GR.RENDER
		FOR i=1 TO 4
			PickHr$[i]="x"
			PickMn2$[i]="x"
		NEXT i
		NumHours=NH[FLOOR(12*RND()+1)]
		NumMinutes=NM[FLOOR(60*RND()+1)]
		PAUSE 2000
		IF Level=1 THEN NumMinutes=0
		IF Level=2 THEN	NumMinutes=INT(NumMinutes/15)*15
		IF Level=3 THEN NumMinutes=INT(NumMinutes/5)*5
		FOR i=0 TO NumHours
			GR.MODIFY HHand,"angle",i*30
			GR.MODIFY MoveMouth,"angle",-10+(i/(NumHours+1))*20
			GR.RENDER
			PAUSE 100
		NEXT i
		RollMinute=NumMinutes
		IF NumMinutes=0 THEN RollMinute=60
		FOR i=0 TO RollMinute
			GR.MODIFY MHand,"angle",i*6
			IF NumMinutes>0 THEN GR.MODIFY HHand,"angle",NumHours*30+i/2
			GR.MODIFY MoveMouth,"angle",10-(i/(RollMinute+1))*20
			GR.RENDER
			PAUSE 10
		NEXT i
		IF Lang=1 THEN
			TTS.SPEAK "Hoe laat is het nu?"
		ELSE
			TTS.SPEAK "What time is it now?"
		ENDIF
		IF Level=1 THEN
			READ.FROM NumHours+1+(Lang-1)*24
			READ.NEXT H$
			IF Lang=1 THEN
				Sp$="Het is nu "+H$+" uur"
			ELSE
				Sp$="It is now "+H$+" o'clock"
			ENDIF
			ARRAY.SHUFFLE Hr$[]
			P=1:cP=1
			DO
				IF Hr$[cP]<>H$ THEN
					PickHr$[P]=Hr$[cP]
					P=P+1
				ENDIF
				cP=cP+1
			UNTIL P>3
			PickHr$[4]=H$
			ARRAY.SHUFFLE PickHr$[]
			FOR i=1 TO 4
				IF Lang=1 THEN
					GR.MODIFY Choice[i],"text","Het is nu "+PickHr$[i]+" uur"
				ELSE
					GR.MODIFY Choice[i],"text","It is now "+PickHr$[i]+" o'clock"
				ENDIF
			NEXT i
		ENDIF
		IF Level=2 THEN GOSUB Lvl2
		IF Level=3 THEN
			IF NumMinutes/15=INT(NumMinutes/15) THEN
				GOSUB Lvl2
			ELSE
				IF NumMinutes>15 & NumMinutes<30 & Lang=2 THEN
					Tm=1
				ELSE
					Tm=0
				ENDIF
				IF NumMinutes<(15+Tm*15) THEN
					READ.FROM NumMinutes/5+16+(Lang-1)*(24-Tm)
					READ.NEXT M$
					READ.FROM NumHours+1+(Lang-1)*24
					READ.NEXT H$
				ELSE
					IF NumHours+1=12 THEN
						READ.FROM 1+(Lang-1)*24
					ELSE
						READ.FROM NumHours+2+(Lang-1)*24
					ENDIF
					READ.NEXT H$
					READ.FROM NumMinutes/5+15-INT((NumMinutes-20)/15)+(Lang-1)*24
					READ.NEXT M$
				ENDIF
				GOSUB Lvl3
			ENDIF
		ENDIF
		GR.RENDER
		Answer=0
		DO
			GOSUB GetTouch
			C=INT((y-60)/100)
			IF x>920 & x<1280 THEN
				IF C>0 & C<5 THEN
					FOR i=1 TO 4:GR.HIDE Choice[i]:NEXT i:GR.RENDER
					! play speech
					IF Level=1 THEN
						IF Lang=1 THEN
							TTS.SPEAK "Het is nu "+PickHr$[C]+" uur"
						ELSE
							TTS.SPEAK "It is now "+PickHr$[C]+" o'clock"
						ENDIF
					ENDIF
					IF Level>1 THEN
						Q$="Het is nu "+PickMn2$[C]+" "+PickHr$[C]
						IF PickMn2$[C]="" THEN Q$=Q$+" uur"
						TTS.SPEAK Q$
					ENDIF
					FOR i=1 TO 4:GR.SHOW Choice[i]:NEXT i:GR.RENDER
				ENDIF
			ENDIF
			IF x>1300 & x<1380 THEN
				IF C>0 & C<5 THEN
					! place checkmark
					IF PickHr$[C]=H$ THEN
						Answer=2
						SOUNDPOOL.PLAY ST1,Beep1,0.99,0.7,1,0,0.5
						GR.SHOW Green[C]
						GR.SHOW OK
					ELSE
						Answer=1
						SOUNDPOOL.PLAY ST2,Beep2,0.99,0.7,1,0,0.5
						GR.SHOW Red[C]
						GR.SHOW NotOK
					ENDIF
				ENDIF
			ENDIF
		GR.RENDER
		UNTIL Answer
		IF Answer=2 THEN Correct=Correct+1
		GR.MODIFY Goed,"text",INT$(Correct):GR.RENDER
		PAUSE 500
		IF Lang=1 THEN
			TTS.SPEAK "Het goede antwoord is "
		ELSE
			TTS.SPEAK "The correct answer is "
		ENDIF
		PAUSE 1000:TTS.SPEAK Sp$
		GR.HIDE Green[C]:GR.HIDE Red[C]:GR.HIDE OK:GR.HIDE NotOK
	NEXT Game
	GR.RENDER
	IF LAng=1 THEN
		TTS.SPEAK "Heel goed gedaan"
	ELSE
		TTS.SPEAK "Very well done"
	ENDIF
	PAUSE 1000
	IF Correct=5 THEN
		IF Level=1 THEN
			IF Lang=1 THEN
				TTS.SPEAK "het wordt nu iets moeilijker!"
			ELSE
				TTS.SPEAK "it will become slightly more difficult now!"
			ENDIF
			Level=Level+1
		ELSE
			IF Level=2 THEN
				IF Lang=1 THEN
					TTS.SPEAK "het wordt nu nog moeilijker!"
				ELSE
					TTS.SPEAK "it will become even more difficult now!"
				ENDIF
				Level=Level+1
			ELSE
				IF Level=3 THEN
					IF Lang=1 THEN
						TTS.SPEAK "Bravo....je hebt alles goed beantwoord!",1
					ELSE
						TTS.SPEAK "Bravo....you have given all the right answers!",1
					ENDIF
					FOR i=1 TO 5:GR.SHOW OK:GR.RENDER:PAUSE 300:GR.HIDE OK:GR.RENDER:PAUSE 300:NEXT i
					IF Lang=1 THEN
						TTS.SPEAK "Je kunt al heel goed klok kijken!"
					ELSE
						TTS.SPEAK "You can tell the time very well!",1
					ENDIF
					Kap=1
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF Lang=1 THEN
			TTS.SPEAK "we doen het nog een keer"
		ELSE
			TTS.SPEAK "let's try again"
		ENDIF
	ENDIF
UNTIL Kap
Quit:
IF Lang=1 THEN
	TTS.SPEAK "We gaan er nu mee stoppen"
ELSE
	TTS.SPEAK "We will stop now"
ENDIF
FOR i=-145 TO ScaleY STEP 10
	GR.MODIFY F,"bottom",i
	GR.MODIFY g1,"y",i:GR.MODIFY g2,"y",i:GR.MODIFY g3,"y",i:GR.MODIFY g4,"y",i:GR.MODIFY g5,"y",i:GR.MODIFY g6,"y",i
	GR.MODIFY T,"y",i-300:GR.RENDER:PAUSE 10
NEXT i
IF Lang=1 THEN
	TTS.SPEAK "tot ziens"
ELSE
	TTS.SPEAK "see you soon"
ENDIF
EXIT

ONBACKKEY:
IF OK THEN
	GOTO Quit
ELSE
	EXIT

GetTouch:
	DO
		GR.TOUCH Touched,x,y
	UNTIL Touched
	DO
		GR.TOUCH Touched,x,y
	UNTIL !Touched
	x/=sx:y/=sy
RETURN

Lvl2:
	READ.FROM NumMinutes/15+13+(Lang-1)*24
	READ.NEXT M$
	IF Lang=1 THEN
		MTest=2
	ELSE
		MTest=3
	ENDIF
	IF NumMinutes/15<MTest THEN
		READ.FROM NumHours+1+(Lang-1)*24
	ELSE
		IF NumHours+1=12 THEN
			READ.FROM 1+(Lang-1)*24
		ELSE
			READ.FROM NumHours+2+(Lang-1)*24
		ENDIF
	ENDIF
	READ.NEXT H$
Lvl3:
	IF Lang=1 THEN
		Sp$="Het is nu "+M$+" "+H$
		IF M$="" THEN Sp$=Sp$+" uur"
	ELSE
		Sp$="It is now "+M$+" "+H$
		IF M$="" THEN Sp$=Sp$+" o'clock"
	ENDIF
	ARRAY.SHUFFLE Hr$[]
	IF Level=2 THEN
		ARRAY.SHUFFLE Mn2$[]
	ELSE
		ARRAY.SHUFFLE Mn3$[]
	ENDIF
	P=1:cP=1
	DO
		IF Hr$[cP]<>H$ THEN
			PickHr$[P]=Hr$[cP]
			IF Level=2 THEN
				PickMn2$[P]=Mn2$[cP]
			ELSE
				PickMn2$[P]=Mn3$[cP]
			ENDIF
			P=P+1
		ENDIF
		cP=cP+1
	UNTIL P>3
	PickHr$[4]=H$:PickMn2$[4]=M$
	ARRAY.COPY PickHr$[],PickHrCopy$[]
	ARRAY.COPY PickMn2$[],PickMn2Copy$[]
	ARRAY.SHUFFLE s[]
	FOR i=1 TO 4
		PickHr$[s[i]]=PickHrCopy$[i]
		PickMn2$[s[i]]=PickMn2Copy$[i]
	NEXT i
	FOR i=1 TO 4
		IF Lang=1 THEN
			Q$="Het is nu "+PickMn2$[i]+" "+PickHr$[i]
			IF PickMn2$[i]="" THEN Q$=Q$+" uur"
		ELSE
			Q$="It is now "+PickMn2$[i]+" "+PickHr$[i]
			IF PickMn2$[i]="" THEN Q$=Q$+" o'clock"
		ENDIF
		GR.MODIFY Choice[i],"text",Q$
	NEXT i
RETURN

READ.DATA "twaalf","één","twee","drie","vier","vijf","zes","zeven","acht","negen","tien","elf"
READ.DATA "","kwart over","half","kwart voor"
READ.DATA "vijf over","tien over","tien voor half","vijf voor half","vijf over half","tien over half","tien voor","vijf voor"
READ.DATA "twelve","one","two","three","four","five","six","seven","eight","nine","ten","eleven"
READ.DATA "","quarter past","half past","quarter to"
READ.DATA "five past","ten past","twenty past","twenty five past","twenty five to","twenty to","ten to","five to"
