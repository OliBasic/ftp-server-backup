REM Space Hunting v0.1
REM by Antonis [tony_gr]
REM Jan.25,2015, basic! v1.72
REM This programs is free
REM for personal use
! *****************
! * Space Hunting *
! *****************

DIM turn[6],robo[6],cir[6]
GR.OPEN 255,0,0,0,0,1
GR.ORIENTATION 1
PAUSE 1000 %
GR.SCREEN w,h %
sx=w/800 %
sy=h/1232 %
GR.SET.STROKE 4
GR.SCALE sx,sy

! load colors & bitmaps, initialize
GR.BITMAP.LOAD  s0,"spacehunt.jpg"
GR.BITMAP.LOAD bptr10,"alien.png"

SOUNDPOOL.OPEN 3
SOUNDPOOL.LOAD shooted, "invhitted.mp3"
SOUNDPOOL.LOAD au3,"invufo.mp3"

hiscore=0

restartSpaceHunting:

GR.TEXT.SIZE 34
! first draw street & load bitmaps
GR.BITMAP.SCALE s1,s0,800,1232
GR.BITMAP.DRAW space,s1,0,0
GR.COLOR 255,255,0,0,1
GR.TEXT.DRAW jo,0,30,"Space Hunting"
GR.COLOR 255,255,255,0,1
GR.TEXT.DRAW spd,240,30,"Aliens: 0"
GR.COLOR 255,15,155,255,1
GR.TEXT.DRAW ttm,420,30,"Time: 120"
GR.COLOR 255,255,0,255,1
GR.TEXT.DRAW pnt,590,30,"Points: 0"
GR.COLOR 255,0,255,255,1
GR.TEXT.SIZE 19
GR.TEXT.DRAW tx,150,1225,"Space Hunting v0.1 by tony_gr"
GR.SET.STROKE 4
GR.COLOR 255,0,255,255,0
GR.TEXT.SIZE 39

! set parameters of new game
speed=3
difficulty=5
points=0
selcted=5

! draw resources
GR.BITMAP.DRAW robo[1], bptr10, 55, 120
GR.BITMAP.DRAW robo[2], bptr10, 685, 135
GR.BITMAP.DRAW robo[3], bptr10, 430, 390
GR.BITMAP.DRAW robo[4], bptr10, 335, 80
GR.BITMAP.DRAW robo[5], bptr10, 131, 345
GR.BITMAP.DRAW robo[6], bptr10, 235, 610

! draw circles
GR.COLOR 105,155,0,0,1
GR.CIRCLE cir[1],  78,145,30
GR.CIRCLE cir[2], 708,163,30
GR.CIRCLE cir[3], 455,418,30
GR.CIRCLE cir[4], 358,105,30
GR.CIRCLE cir[5], 156,368,30
GR.CIRCLE cir[6], 257,635,30

FOR i=1 TO 6
 GR.HIDE cir[i]
NEXT i

GR.RENDER

! initialize
initilize:
! hide all
FOR i=1 TO 6
 GR.HIDE robo[i]
 turn[i]=0
NEXT i
! choose 4 out of six to show
FOR i=1 TO 4
 choose_random:
 h=CEIL(RND()*6)
 IF turn[h]=0 THEN turn[h]=1 ELSE GOTO choose_random
NEXT i
! show the 4 ones
FOR i=1 TO 6
 h=turn[i]
 IF h=1 THEN GR.SHOW robo[i]
NEXT i
GR.RENDER
PAUSE 1000
g=CLOCK()
g1=g
timdgame=g
found=1
aliens=0
! end of initialize


! loop whole game
startnewSpaceHunting:

found=0

GOSUB timshow

GR.TOUCH touched,x,y
IF CLOCK()-g>=500 THEN
 FOR i=1 TO 6
  GR.HIDE cir[i]
 NEXT i
 g=CLOCK()
 GOSUB choose_element
ENDIF

! if screen touched
IF touched>0 THEN

 g=CLOCK()
 ! first element is on 60,120
 IF x>60*sx & x<110*sx & y>120*sy & y<170*sy  THEN
  ! got it!!!
  IF turn[1]=1 THEN
   found=1
   GOSUB shouCircle
   points=points+20
   GR.HIDE robo[1]
   GOSUB UpdateScore_SpaceHunting
   GOSUB sprite_hitted
   turn[1]=0
   GOSUB RedoIt_SpaceHunting
  ENDIF
 ENDIF
 ! second element is on 690, 135
 IF x>690*sx & x<740*sx & y>135*sy & y<185*sy  THEN
  ! got it!!!
  IF turn[2]=1 THEN
   found=2
   GOSUB shouCircle
   points=points+20
   GOSUB UpdateScore_SpaceHunting
   GR.HIDE robo[2]
   turn[2]=0
   GOSUB sprite_hitted
   GOSUB RedoIt_SpaceHunting
  ENDIF
 ENDIF
 ! third element is on 435, 390
 IF x>435*sx & x<485*sx & y>390*sy & y<440*sy  THEN
  ! got it!!!
  IF turn[3]=1 THEN
   found=3
   GOSUB shouCircle
   points=points+20
   GOSUB UpdateScore_SpaceHunting
   GR.HIDE robo[3]
   turn[3]=0
   GOSUB sprite_hitted
   GOSUB RedoIt_SpaceHunting
  ENDIF
 ENDIF

 ! fourth element is on  335, 80
 IF x>335*sx & x<385*sx & y>80*sy & y<130*sy  THEN
  ! got it!!!
  IF turn[4]=1 THEN
   found=4
   GOSUB shouCircle
   points=points+20
   GOSUB UpdateScore_SpaceHunting
   GR.HIDE robo[4]
   turn[4]=0
   GOSUB sprite_hitted
   GOSUB RedoIt_SpaceHunting
  ENDIF
 ENDIF

 ! fifth element is on  131, 340
 IF x>131*sx & x<181*sx & y>340*sy & y<390*sy  THEN
  ! got it!!!
  IF turn[5]=1 THEN
   found=5
   GOSUB shouCircle
   points=points+20
   GOSUB UpdateScore_SpaceHunting
   GR.HIDE robo[5]
   turn[5]=0
   GOSUB sprite_hitted
   GOSUB RedoIt_SpaceHunting
  ENDIF
 ENDIF

 ! sixth element is on  240, 610
 IF x>240*sx & x<300*sx & y>610*sy & y<660*sy  THEN
  ! got it!!!
  IF turn[6]=1 THEN
   found=6
   GOSUB shouCircle
   points=points+20
   GOSUB UpdateScore_SpaceHunting
   GR.HIDE robo[6]
   turn[6]=0
   GOSUB sprite_hitted
   GOSUB RedoIt_SpaceHunting
  ENDIF
 ENDIF
 g2=CLOCK()
 DO
  GR.TOUCH touched,x,y
  GOSUB timshow
  IF found=0 THEN
   SOUNDPOOL.PLAY missed, au3 , 0.9, 0.9, 1, 0, 1
   points=points-10
   found=-1
   GOSUB UpdateScore_SpaceHunting
  ENDIF
  IF CLOCK()-g2>500 THEN
   GOSUB choose_element
   g2=CLOCK()
  ENDIF
  ! gr.render
 UNTIL !touched

ENDIF

GOTO startnewSpaceHunting % loop whole game for ever :)

! ***************
! * Subroutines *
! ***************
choose_element:
hid_now=CEIL(RND()*6) % random hide 1 out of 6
h=turn[hid_now] % take h element of robos
IF h=0 | hid_now=found THEN GOTO choose_element % robo h is hidden or just hidden
GR.HIDE robo[hid_now] % hide it
turn[hid_now]=0 % mark it
found=hid_now

RedoIt_SpaceHunting:
sho_now=CEIL(RND()*6) % choose new to show
IF turn[sho_now]=1 | sho_now=found THEN GOTO RedoIt_SpaceHunting
GR.SHOW robo[sho_now] % show it
turn[sho_now]=1 % mark
found=sho_now
GR.RENDER % render changes
RETURN

shouCircle:
FOR i=1 TO 6
 GR.HIDE cir[i]
NEXT i
GR.SHOW cir[found]
GR.RENDER
RETURN

UpdateScore_SpaceHunting:
IF found<>-1 THEN aliens=aliens+1
GR.MODIFY pnt,"text","Points :" +REPLACE$(STR$(ROUND(points)),".0","")
GR.MODIFY spd,"text","Aliens : " +REPLACE$(STR$(ROUND(aliens)),".0","")
! IF aliens=0 THEN GOTO GameEnd
GR.RENDER
RETURN

timshow:
secs$=STR$(ROUND(120-(CLOCK()-timdgame)/1000))
a=IS_IN(".",secs$)
sc$=LEFT$(secs$,a-1)
IF LEFT$(secs$,1)="-" THEN sc$="0"
GR.MODIFY ttm,"text","Time: "+sc$
IF CLOCK()-timdgame>120000 THEN
 finished=1
 GOTO GameEnd
ENDIF
RETURN

GameEnd:
GR.RENDER
GR.TEXT.SIZE 100
GR.TEXT.BOLD 1
GR.TEXT.ALIGN 2
GR.COLOR 255,0,255,255,1
GR.TEXT.DRAW tx1,400,615,"Game Over!"
if hiscore<points then hiscore=points
GR.TEXT.DRAW tx2,400,715,"Hi Score:"+ REPLACE$(STR$(ROUND(hiscore)),".0","")
GR.RENDER
GR.TEXT.ALIGN 1
GR.RENDER
DO
 GR.TOUCH touched,x,y
UNTIL touched
DO
 GR.TOUCH touched,x,y
UNTIL !touched
GR.CLS
rerun=1
GOTO restartSpaceHunting

sprite_hitted:
SOUNDPOOL.PLAY shooted2, shooted , 0.9, 0.9, 1, 0, 1
RETURN
