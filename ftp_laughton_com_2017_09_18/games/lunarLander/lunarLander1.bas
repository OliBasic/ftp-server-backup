gr              = 22
GR.OPEN           255, gr,gr,gr,0,1
GR.COLOR          255,255,255,255,1
gr.screen         rw,rh

cx              = rw/2 
cy              = rh/2


GR.COLOR          255, 255,55,55,1
GR.CIRCLE         cr, rw*7/8,rh*1/10, 20
GR.COLOR          255, 55,255,55,1
GR.CIRCLE         cl, rw*1/8,rh*1/10, 20


GR.BITMAP.LOAD    bship, "lander1.png"
GR.BITMAP.SIZE    bship,sbx,sby
GR.BITMAP.CROP    bship,bship,0,0,sbx,sby-5

GR.ROTATE.START   0,px,py, rotShip
GR.BITMAP.DRAW    ship,bship, cx-sbx/2,cy-sby/2
GR.ROTATE.END 

px               = cx 
py               = cy 
oShip            = 1.4
gravi            = 0.08
ro               = 1.2 * gravi 

dtdes            = 38
!-------------------
DO

 tx1            =  0
 tx2            =  0
 GR.TOUCH          t1, tx1,ty1
 GR.TOUCH2         t2, tx2,ty2

 IF                t1 & tx1>cx | t2&tx2>cx THEN tr=1 ELSE tr=0
 IF                t1 & tx1<cx | t2&tx2<cx THEN tl=1 ELSE tl=0

 IF tr             THEN rr=ro : GR.MODIFY cr,"alpha",255 ELSE rr =0 : GR.MODIFY cr,"alpha",25
 IF tl             THEN lr=ro : GR.MODIFY cl,"alpha",255 ELSE lr =0 : GR.MODIFY cl,"alpha",25


 omega          +=         (-rr + lr ) / oship
 phi            +=         omega

 vy             += gravi - ( rr + lr ) * COS(TORADIANS(phi))
 vx             +=         ( rr + lr ) * SIN(TORADIANS(phi))
 px             += vx
 py             += vy

 GR.MODIFY         ship , "x" , px-sbx/2 ,  "y" , py-sby/2 
 GR.MODIFY      rotShip , "x" , px       ,  "y" , py      ,  "angle" , phi 


 GR.RENDER

 !PRINT             toc
 toc             = CLOCK()-tic
 PAUSE             MAX(dtdes-toc,1)
 tic             = CLOCK()

UNTIL              0
!-------------------
