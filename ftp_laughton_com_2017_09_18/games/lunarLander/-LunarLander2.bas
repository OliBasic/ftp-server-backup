! @08/2015 by brochi,  modified by Cassiope34...

gr              = 22
GR.OPEN           255, gr,gr,gr,0,0
GR.COLOR          255,255,255,255,1
gr.screen         rw,rh

cx              = rw/2 
cy              = rh/2

GR.BITMAP.LOAD    bship, "lander2.png"   % new bitmap
GR.BITMAP.SIZE    bship,sbx,sby
!GR.BITMAP.CROP    bship,bship,0,0,sbx,sby-5

gr.bitmap.create btship, sbx, sbx      % ship (center in a square bitmap)
gr.bitmap.drawinto.start btship
GR.COLOR  255, 255,250,228,0
gr.rect nul, 0,0,sbx,sbx   % just for debug
gr.bitmap.draw nul, bship, 0, sbx/2-sby/2
gr.bitmap.drawinto.end

gr.bitmap.create crship, sbx, sbx      % the 2 reactors...
gr.bitmap.drawinto.start crship
GR.COLOR  255, 255,250,228,0
gr.rect nul, 0,0,sbx,sbx   % just for debug
GR.COLOR  255, 255,250,228,1
GR.CIRCLE nul, sbx-10, sbx/2+18, 10
gr.bitmap.drawinto.end

gr.bitmap.create clship, sbx, sbx
gr.bitmap.drawinto.start clship
GR.COLOR  255, 255,250,228,0
gr.rect nul, 0,0,sbx,sbx   % just for debug
GR.COLOR  255, 255,250,228,1
GR.CIRCLE nul, 10, sbx/2+18, 10
gr.bitmap.drawinto.end

GR.ROTATE.START   0, px+sbx/2, py+sbx/2, rotShip
GR.BITMAP.DRAW    ship, btship, px, py

GR.ROTATE.START   0, px+sbx/2, py+sbx/2, rotcr
GR.BITMAP.DRAW    cr,  crship, px, py

GR.ROTATE.START   0, px+sbx/2, py+sbx/2, rotcl
GR.BITMAP.DRAW    cl,  clship, px, py
GR.ROTATE.END
                         ! ------------------------------
              
px               = cx 
py               = cy 
oShip            = 1.4
gravi            = 0.20  % 0.08
ro               = 1.2 * gravi 

dtdes            = 38
!-------------------
DO

 tx1            =  0
 tx2            =  0
 GR.TOUCH          t1, tx1,ty1
 GR.TOUCH2         t2, tx2,ty2

 IF                t1 & tx1>cx | t2 & tx2>cx THEN tr=1 ELSE tr=0
 IF                t1 & tx1<cx | t2 & tx2<cx THEN tl=1 ELSE tl=0

! IF tr    THEN rr=ro : GR.MODIFY cr,"alpha",255 ELSE rr =0 : GR.MODIFY cr,"alpha",25
! IF tl    THEN lr=ro : GR.MODIFY cl,"alpha",255 ELSE lr =0 : GR.MODIFY cl,"alpha",25

 omega          += (rr - lr ) / oship   % reverse commands...
 phi            += omega

 vy             += gravi - ( rr + lr ) * COS(TORADIANS(phi))
 vx             +=         ( rr + lr ) * SIN(TORADIANS(phi))
 px             += vx
 py             += vy
 
 GR.MODIFY         ship , "x" , px ,  "y" , py 
 GR.MODIFY      rotShip , "x" , px+sbx/2 ,  "y" , py+sbx/2  ,  "angle" , phi
 GR.MODIFY           cr , "x" , px ,  "y" , py
 GR.MODIFY           cl , "x" , px ,  "y" , py
 GR.MODIFY        rotcr , "x" , px+sbx/2 ,  "y" , py+sbx/2  ,  "angle" , phi
 GR.MODIFY        rotcl , "x" , px+sbx/2 ,  "y" , py+sbx/2  ,  "angle" , phi

 IF tr          THEN rr=ro : GR.SHOW cl ELSE rr =0 : GR.HIDE cl   % reverse commands
 IF tl          THEN lr=ro : GR.SHOW cr ELSE lr =0 : GR.HIDE cr

 GR.RENDER

! PRINT             toc
! toc             = CLOCK()-tic
! PAUSE             MAX(dtdes-toc,1)
! tic             = CLOCK()

UNTIL              quit
!-------------------
