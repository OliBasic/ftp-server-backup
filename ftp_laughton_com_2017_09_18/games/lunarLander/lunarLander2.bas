gr              = 22
GR.OPEN           255, gr,gr,gr,0,1
GR.COLOR          255,255,255,255,1
GR.SCREEN         rw,rh
sca             = 0.7
GR.SCALE          sca , sca

cx              = rw/2 
cy              = rh/2


GR.BITMAP.LOAD    bship, "lander1.png"
GR.BITMAP.SIZE    bship,sbx,sby
GR.BITMAP.CROP    bship,bship,0,0,sbx,sby-5


GR.COLOR          255, 255,255,55,1
dr              = 76
dy              = 15

LIST.CREATE       n,fire
LIST.ADD            fire,-4,-4,4,-4,8,11,-8,11

GR.ROTATE.START   0,0,0, rotAll
GR.BITMAP.DRAW    ship,bship, cx-sbx/2,cy-sby/2
GR.POLY           cl, fire, cx-dr , cy+dy
GR.POLY           cr, fire, cx+dr , cy+dy
GR.ROTATE.END 

!GR.GROUP.GETDL    gr1

GR.RENDER

SOUNDPOOL.OPEN    10
SOUNDPOOL.LOAD    s1, "audiocheck.net_pinknoise.mp3"

PAUSE             1000

SOUNDPOOL.PLAY    ss1,  s1 , 0.99 , 0.99 , 0, -1 , 1
SOUNDPOOL.PAUSE   ss1 

px               = cx 
py               = cy 
oShip            = 1.4
gravi            = 0.08
ro               = 1.6 * gravi 


dtdes            = 33
!-------------------
DO

 tx1            =  0
 tx2            =  0
 GR.TOUCH          t1, tx1,ty1
 GR.TOUCH2         t2, tx2,ty2
 tr             =  t1 & tx1>cx | t2 & tx2>cx 
 tl             =  t1 & tx1<cx | t2 & tx2<cx 

 IF tr             THEN rr=ro : GR.MODIFY cr,"alpha",255 ELSE rr =0 : GR.MODIFY cr,"alpha",25
 IF tl             THEN lr=ro : GR.MODIFY cl,"alpha",255 ELSE lr =0 : GR.MODIFY cl,"alpha",25
 IF tl | tr        THEN SOUNDPOOL.RESUME ss1 ELSE SOUNDPOOL.PAUSE ss1

 omega          +=         (-rr + lr ) / oship
 phi            +=         omega

 vy             += gravi - ( rr + lr ) * COS(TORADIANS(phi))
 vx             +=         ( rr + lr ) * SIN(TORADIANS(phi))
 px             += vx
 py             += vy


 GR.MODIFY         ship , "x" , px-sbx/2 ,  "y" , py-sby/2 
 GR.MODIFY           cl , "x" , px-dr    ,  "y" , py+dy 
 GR.MODIFY           cr , "x" , px+dr    ,  "y" , py+dy

 GR.MODIFY       rotAll , "x" , px       ,  "y" , py      ,  "angle" , phi 


 GR.RENDER

 toc             = CLOCK()-tic
 PAUSE             MAX(dtdes-toc,1)
 tic             = CLOCK()

UNTIL              0
!-------------------
