!!
This program demonstrats some 
of the possibilities
when using the BASIC! html commands.
!!

! html must be opened before doing 
! any html command
html.open
html.clear.cache

! Load the file, htmlDemo1.html

html.load.url "http://medienreservierung.12hp.de/guess-my-number02.html"

!The user now sees the html
  
  anum = round(rnd()*100)
  print anum
  attempt = 1

! We can now monitor the user
! actions

xnextUserAction:

! loop until data$ is not ""

do
html.get.datalink data$
until data$ <> ""

! The first four characters of data$
! identify the type of data returned.
! Extract those three characters to
! use in a switch statement

type$ = left$(data$, 4)

! Trim the first four characters from
! data$

data$ = mid$(data$,5)

! Act on the data type
! Shown are all the current data types

sw.begin type$

! Back Key hit.
! if we can go back then do it
 sw.case "BAK:"
  print "BACK key: " + data$
  if data$ = "1" then html.go.back else end
  sw.break
  
 ! A hyperlink was clicked on
 sw.case "LNK:"
   print "Hyperlink selected: "+ data$
   lnk$ = "javascript:document.getElementById('Bok').style.visibility="
  if data$ ="about:exit" then   
    html.load.url lnk$ + "'hidden';"
    print " user ended the game ..."
    html.close
    end
   endif
   sw.break
   
 ! An error occured
 sw.case "ERR:"
   print "Error: " + data$
   sw.break
   
  ! User data returned: modifying the html via javascript
  sw.case "DAT:"
    print "User data: " + data$
 
    if (anum > val(data$)) then
! zahl zu klein
    print "too low"

    html.load.url "javascript:document.getElementById('Bup').style.visibility='visible';"
    pause 200
    html.load.url "javascript:document.getElementById('Bok').style.visibility='hidden';"
    pause 200
    html.load.url "javascript:document.getElementById('Bdown').style.visibility='hidden';"
    pause 200

    msg$ = "javascript:document.getElementsByName('Text2')[0].value = ' No : " + format$("##",attempt)+ "   Too Low !!!';"
    html.load.url msg$


    pause 1000
    attempt = attempt + 1
    sw.break

    elseif (anum < val(data$)) then
! zahl zu gross
    print "too high"
    
    html.load.url "javascript:document.getElementById('Bup').style.visibility='hidden';"
    pause 200
    html.load.url "javascript:document.getElementById('Bok').style.visibility='hidden';"
    pause 200
    html.load.url "javascript:document.getElementById('Bdown').style.visibility='visible';"
    pause 200

    msg$ = "javascript:document.getElementsByName('Text2')[0].value = ' No : " + format$("##",attempt)+ "   Too High !!!';"
    html.load.url msg$

    pause 1000
    attempt = attempt + 1
    sw.break 

    endif

! zahl gefunden
    print "you got it"
    
    html.load.url "javascript:document.getElementById('Bup').style.visibility='hidden';"
    pause 200
    html.load.url "javascript:document.getElementById('Bok').style.visibility='visible';"
    pause 200
    html.load.url "javascript:document.getElementById('Bdown').style.visibility='hidden';"
    pause 200

    msg$ = "javascript:document.getElementsByName('Text2')[0].value = ' No : " + format$("##",attempt)+ "   You Got It !!!';"
    html.load.url msg$
      
     pause 1000
     sw.break
	
! answer return
 
sw.default
 print "Unexpected data type:", type$ + data$
 END
 
 sw.end
 
 goto xnextUserAction
 
