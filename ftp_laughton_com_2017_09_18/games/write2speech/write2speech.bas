REM Start of BASIC! Program
!this is my very first program in !basic, i wrote it to see how !basic work.
!the program only speak what you write in a text box but has nice graphic.
!You can also copy/paste text from other apps leaving write2speech in background
!maybe this program isn't useful but it show use of graphics elements, 
!interrupt on back and menu key, popup and so on, so take it 'as is'.
!there are only 3 buttons: WRITE, REPEAT and EXIT, REPEAT will be activated after you write something.
!The back key in your device act as EXIT, the menu key will change language (ITA or ENG)
!if you press the home key the program still run in background ( a popup message appear), the
!only way to exit is... EXIT button or back key.
!Note: only portrait mode is implemented.
!you can change the ITA dictionary to your own, remember to use the TTS engine for your language.
!tested on 480x800 and 240x320 smartphones.


!variable init.
ripeti=0	%repeat only if there is something to speak
lang=1		%select dictionary: 1=ITA, 2=ENG
BkgFlag=0	%background mode flag, used for background popup message

dim lang$[2,10]
!Italian dictionary setup, can be changed to your own language
lang$[1,1]= "Scrivi"
lang$[1,2]= "Ripeti"
lang$[1,3]= "Esci"
lang$[1,4]= "Ciao, per iniziare tocca la matita"
lang$[1,5]= "Grazie per avere giocato con me"
lang$[1,6]= "inserisci la frase da pronunciare"
lang$[1,7]= "Mi dispiace, si è verificato un errore nell'applicazione. ciao"
lang$[1,8]= "Write2Speech è in esecuzione in background"
lang$[1,9]= "Notifica"

!english dictionary setup
lang$[2,1]= "Write"
lang$[2,2]= "Repeat"
lang$[2,3]= "Exit"
lang$[2,4]= "Hello, touch the pencil to start"
lang$[2,5]= "Thank you for playing with me"
lang$[2,6]= "insert the phrase to speak"
lang$[2,7]= "i'm sorry, there was an application error. bye"
lang$[2,8]= "Write2Speech is in background"
lang$[2,9]= "Notify"

!open graphic screen   480 x 800  (portrait)
di_height = 800 % y
di_width = 480 % x

gr.open 255, 0, 0, 0, 1
gr.orientation 1

!read screen dimension and scale graphics
gr.screen actual_w, actual_h
scale_width = actual_w /di_width 
scale_height = actual_h /di_height 
gr.scale scale_width, scale_height

!Text mode...
gr.color 255,0,0,0,0
!gr.set.stroke 2
gr.text.align 1
gr.text.size 40
gr.text.typeface 1
gr.text.bold 1

!clear screen, load button, icon and splash screen...
gr.cls
gr.bitmap.load splash,"Tts_Splash_Screen.png"
gr.bitmap.load BtnNormal,"BtnNormal.png"
gr.bitmap.load BtnGrayed,"BtnGrayed.png"
gr.bitmap.load BtnTouched,"BtnTouched.png"
gr.bitmap.load IcnExit,"esci.png"
gr.bitmap.load IcnWrite,"scrivi.png"
gr.bitmap.load IcnRpt,"ripetere.png"

!Draw splash screen
gr.bitmap.draw bkground, splash, 0, 0

!draw write button
btnnum=1
graph=BtnNormal
Icon=IcnWrite
txt$=lang$[lang,1]
gosub Btnrender

!draw repeat button
btnnum=2
graph=BtnGrayed
Icon=IcnRpt
txt$=lang$[lang,2]
gosub Btnrender

!draw exit button
btnnum=3
graph=BtnNormal
Icon=IcnExit
txt$=lang$[lang,3]
gosub Btnrender

!TTS on
tts.init
tts.speak lang$[lang,4]

!main program loop
do

!check if running in background, if so popup a message
if background () then 
	if BkgFlag=0 then
		BkgFlag=1
		!notify require too much cpu/battery in background, now use popup message 4 sec.
		!notify "Write2Speech" , lang$[lang,8] , "",1
		popup lang$[lang,8] ,0,0,1
		!tts.speak lang$[lang,8]
	endif
else
	BkgFlag=0
endif
	
!if running in background save battery drain doing nothing...
!Thx to Mougino  :)
while background ()	
	pause 1000
repeat

scr_key=0

  !Exit button check
  gr.bounded.touch scr_key, 100*scale_width ,685*scale_height ,380*scale_width ,745*scale_height
  IF scr_key =1 THEN 
	btnnum=3
	graph=Btntouched
	Icon=IcnExit
	txt$=lang$[lang,3]
	gosub Btnrender
  
	tts.speak lang$[lang,5]
	pause 1000
	exit
  endif 
  
  !repeat button check
  gr.bounded.touch scr_key, 100*scale_width ,605*scale_height ,380*scale_width ,665*scale_height
  IF (scr_key & ripeti) =1 THEN 
  
	!touched repeat button
	btnnum=2
	graph=BtnTouched
	Icon=IcnRpt
	txt$=lang$[lang,2]
	gosub Btnrender
	
	pause 200
	
	!Grayed repeat button
	btnnum=2
	graph=BtnGrayed
	Icon=IcnRpt
	txt$=lang$[lang,2]
	gosub Btnrender
	
	tts.speak lettura$
	
	!Untouched repeat button
	btnnum=2
	graph=BtnNormal
	Icon=IcnRpt
	txt$=lang$[lang,2]
	gosub Btnrender
	
  endif 
  
  !write button check
  gr.bounded.touch scr_key, 100*scale_width ,525*scale_height ,380*scale_width ,585*scale_height
  IF scr_key =1 THEN 
  
    !touched
	btnnum=1
	graph=BtnTouched
	Icon=IcnWrite
	txt$=lang$[lang,1]
	gosub Btnrender

	pause 200
  
	tts.speak lang$[lang,6]
	text.input lettura$
	
	!grayed
	btnnum=1
	graph=BtnGrayed
	Icon=IcnWrite
	txt$=lang$[lang,1]
	gosub Btnrender
	
	tts.speak lettura$
	
	!normal write and repeat button
	btnnum=1
	graph=BtnNormal
	Icon=IcnWrite
	txt$=lang$[lang,1]
	gosub Btnrender
	
	btnnum=2
	graph=BtnNormal
	Icon=IcnRpt
	txt$=lang$[lang,2]
	gosub Btnrender
	
	!enable repeat button 
	ripeti=1
		
  endif 
  !do nothing

until 0

BtnRender:
!button render subroutine with graph, icon and text
sw.begin btnnum
	sw.case 1
		gr.bitmap.draw Btn1, graph, 100,525
		gr.bitmap.draw Icn1, icon,110,535
		gr.text.draw Txt1, 170,525+50, txt$
	sw.break
	
	sw.case 2
		gr.bitmap.draw Btn2, graph, 100,605
		gr.bitmap.draw Icn2, icon,110,615
		gr.text.draw Txt2, 170,605+50, txt$
	sw.break
	
	sw.case 3
		gr.bitmap.draw Btn3, graph, 100,685
		gr.bitmap.draw Icn3, icon, 110,695
		gr.text.draw Txt3, 170,685+50, txt$
	sw.break
	
sw.end
gr.render
return

onError:
tts.speak lang$[lang,7]
exit


onBackKey:
!perform the exit action as touching exit button when press back key
btnnum=3
graph=Btntouched
Icon=IcnExit
txt$=lang$[lang,3]
gosub Btnrender
  
tts.speak lang$[lang,5]
pause 1000
exit
!notify "Write2Speech" , "sono in attesa" , "vedo",1
!back.resume

onMenuKey:
!swap button text language and some phrases too everytime menu key is pressed
if lang=1 then
	lang=2
	popup "English language" ,0,0,0
else
	lang=1
	popup "lingua italiana" ,0,0,0
endif

gr.modify Txt1, "text",lang$[lang,1]
gr.modify Txt2, "text",lang$[lang,2]
gr.modify Txt3, "text",lang$[lang,3]
gr.render

MenuKey.Resume

!onbackground:
!show a notification if background...
!if background() then 
	!notify "Write2Speech" , lang$[lang,8] , "",1
!endif

!background.resume


