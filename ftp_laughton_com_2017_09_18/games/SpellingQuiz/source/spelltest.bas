! spelling quiz
! C Terpin
! button field by brochi
!

INCLUDE "cclib.bas"
INCLUDE "ccedit.bas"


FN.DEF          butCreate (x,y,rad, color, txt$,type) 

 GR.TEXT.SIZE    rad*1.3
 GR.SET.STROKE   4
 GR.TEXT.ALIGN   2
 GR.TEXT.BOLD    1

 GR.BITMAP.CREATE   bmp,2*rad+1,2*rad+1
 GR.BITMAP.DRAW     nn, bmp, x,y


 GR.BITMAP.DRAWINTO.START bmp


 IF type     = 1 
  GR.COLOR        255, 40  , 00  , 140 , 1 
  GR.RECT         nn , 0, 0, 2*rad, 2*rad
  GR.COLOR        255, 120 , 120 , 255 , 0
  GR.RECT         nn , 1 ,1, 2*rad,2*rad
  GR.COLOR        255, 00  , 00  , 45  , 0
  GR.RECT         nn , 3, 3, 2*rad+1, 2*rad+1

 ELSEIF type     = 2

  GR.COLOR        255, 40  , 00  , 140 , 1 
  GR.CIRCLE       nn , rad,rad,rad
  GR.COLOR        255, 120 , 120 , 255 , 0
  GR.CIRCLE       nn ,  rad,rad,rad-2
  GR.COLOR        255, 00  , 00  , 45  , 0
  GR.CIRCLE       nn , rad+1,rad+1,rad-2

 ELSEIF type     = 3
  GR.COLOR        255, 40  , 00  , 140 , 1 
  GR.POLY         nn , 1, rad, rad
  GR.COLOR        255, 120 , 120 , 255 , 0
  GR.POLY         nn , 1, rad, rad
  GR.COLOR        255, 00  , 00  , 45  , 0
  GR.POLY         nn , 1, rad+1, rad+1

 ENDIF

 GR.COLOR        255, 255 , 120 , 120 , 1
 GR.TEXT.DRAW   nn, rad,rad*1.5,txt$

 GR.COLOR        255, 90  , 20  , 10  , 1
 GR.TEXT.DRAW   nn, rad+2,rad*1.5+2,txt$

 GR.BITMAP.DRAWINTO.END

 FN.RTN         nn
FN.END



FN.DEF                roundCornerRect (b, h, r)

 dphi               = PI()/2 / 8
 LIST.CREATE          N, S1

 FOR phi            = 0 TO PI()/2 STEP dphi
  LIST.ADD            s1, -b/2+r -COS(phi)*r, -h/2+r -SIN(phi)*r
 NEXT
 FOR phi            = 0 TO PI()/2 STEP dphi
  LIST.ADD            s1,  b/2-r +SIN(phi)*r, -h/2+r -COS(phi)*r
 NEXT
 FOR phi            = 0 TO PI()/2 STEP dphi
  LIST.ADD            s1,  b/2-r +COS(phi)*r,  h/2-r +SIN(phi)*r
 NEXT
 FOR phi            = 0 TO PI()/2 STEP dphi
  LIST.ADD            s1, -b/2+r -SIN(phi)*r,  h/2-r +COS(phi)*r
 NEXT

 FN.RTN s1
FN.END



!------------------------




FN.DEF clearscreen()
 GR.CLS
FN.END

FN.DEF tint()
 FN.RTN 100+FLOOR(RND()*126+1)
FN.END
! main parameters --------------
dinit()



ARRAY.LOAD      nCtrl[], 29, 120, 180 , 240
ARRAY.LOAD      nCols[], 5,  6,   4  , 6
ARRAY.LOAD      rad[],   30, 30,  27  , 23

butType       = 2

variant       = 1

scale         = 1.1

nctrl         = nCtrl [variant]
nCols         = nCols [variant]


TTS.INIT
q$="please spell the word "

FILE.EXISTS e,"userspell.txt"

IF !e
 GRABFILE d$,"spell.txt"
 writeln("userspell.txt",d$)
ENDIF


IF askyn("edit word list?")

 IF askyn("restore to factory default?")
  GRABFILE d$,"spell.txt"
  writeln("userspell.txt",d$)
 ENDIF
 GRABFILE d$,"userspell.txt"
 d$=htmledit$("edit word list",d$)
 writeln("userspell.txt",TRIM$(d$))
ENDIF 
GRABFILE d$,"userspell.txt"

SPLIT wl$[],d$,"\n"

dmode("gr")
GR.SCALE        scale, scale


ARRAY.LENGTH n,wl$[]
DIM check[n]
q=10
FOR wds=1 TO q
 rad           = rad   [variant]  / scale

 GOSUB          init 
 userent=0

 DO
  j=FLOOR(RND()*n+1)
 UNTIL (check[j]=0) & (LEN(wl$[j])>2)
 check[j]=1

 w$=TRIM$(LOWER$(wl$[j]))
 tries=0
 correct=0
 DO
  TTS.SPEAK q$+w$
  tries++
  ! INPUT "spelling?",a$,"",canc
  ! IF canc 
  !  IF askyn("quit?") THEN EXIT
  ! dmode("gr")
  ! ENDIF
  DO
   PAUSE 33
  UNTIL  userent
  userent=0

  a$=LOWER$(txt$)
  txt$=""
  IF (a$=w$) 
   correct=1
   POPUP "correct!"
   TTS.SPEAK "that's correct!"
  ELSE
   IF tries<2
    POPUP "no.. please try again"
    TTS.SPEAK "no, please try again"
   ENDIF
  ENDIF
 UNTIL correct | tries>1

 IF correct 
  score++
 ELSE
  c$= "sorry, the correct spelling is: "
  dmode("gr")
  clearscreen()
  GR.SCREEN w,h
  !  PRINT c$
  TTS.SPEAK c$
  s$=""

  GR.COLOR 255,tint(),tint(),tint()
  GR.TEXT.SIZE 45
  GR.TEXT.ALIGN 2
  FOR k=1 TO LEN(w$)
   s$=MID$(w$,k,1)
   !  PRINT s$
   GR.TEXT.DRAW t,k*35,h/2,s$
   GR.RENDER
   TTS.SPEAK s$
   PAUSE 400
  NEXT k
  ! PRINT w$
  TTS.SPEAK w$
  PAUSE 3000
  clearscreen()
 ENDIF
NEXT wds

DIALOG.MESSAGE "score",INT$(score)+"/"+INT$(q)+" correct",c,"ok"
EXIT






!------------------------------------



!press action (all buts) --------

butPressAction:

RETURN
!------------------------



!release action (all buts) ------

butReleaseAction:
IF curBut=27
 txt$=MID$(txt$,1,LEN(txt$)-1)
ENDIF
IF curBut=28
 userent=1
ENDIF
IF curBut<27


 txt$         += CHR$(64+ curBut )
 IF              !MOD(LEN(txt$),20) THEN  txt$ =""+CHR$(64+curBut)
ENDIF
GR.MODIFY       txt1, "text", txt$
tts.speak chr$(64+curBut)

!VIBRATE         vib2[],-1


RETURN
!------------------------


!===========================================
!Private part ==============================

!------------------------
ONGRTOUCH:
tic= CLOCK()
GOSUB           butPressAction

curBut        = 0
DO
 tic= CLOCK()
 GR.TOUCH       tou,tx,ty
 GR.MODIFY      dum ,"x", tx /scale , "y", ty /scale
 FOR i        = 1 TO nctrl
  IF            GR_COLLISION(ctrl[i],dum) THEN GOSUB colliHandle
 NEXT
UNTIL           !tou

IF               curBut
 GR.MODIFY       ctrl[curBut], "alpha", 255

 GOSUB           butReleaseAction
 GR.RENDER
ENDIF

GR.ONGRTOUCH.RESUME
!------------------------

!------------------------
colliHandle:
PRINT            CLOCK()-tic
IF               i <> curBut THEN
 !VIBRATE         vib1[],-1
 IF              curBut THEN GR.MODIFY ctrl[curBut], "alpha", 255
 GR.MODIFY       ctrl[i]       , "alpha", 150
 curBut        = i
 tic= CLOCK()
 GR.RENDER
 PRINT            CLOCK()-tic
ENDIF
RETURN
!------------------------





!-------------------------------
init: 
GR.CLS


GR.COLOR        0 ,255,0,0,1
GR.CIRCLE       dum, 0,0,0
UNDIM vib1[]
UNDIM vib2[]
ARRAY.LOAD      vib1[],0,20
ARRAY.LOAD      vib2[],0,10


! colors ----------------------
! now below
GR.PAINT.GET    colorIdx


! create button grid ----------

rcr           = roundCornerRect (rad*2, rad*2, 10)
UNDIM ctrl[]
DIM             ctrl[nctrl]
kx            = 2*rad*1.30
ky            = 2*rad*1.15
FOR i         = 0 TO nctrl-1
 x            = 25+MOD(i,nCols) *kx  + MOD(INT(i/nCols),2) *kx/2
 y            =150+ INT(i/nCols) *ky 
 IF i=27
  k$="del"
 ELSE if i=28
  k$="ent"
 ELSE
  k$=CHR$(64+i)
 ENDIF
 ctrl[i+1]    = butCreate (x, y ,rad, colorIdx,k$, butType )
NEXT

! text bar for feedback -------
GR.TEXT.ALIGN   1
GR.TEXT.SIZE    50
GR.COLOR        255, 120,120,255,1
GR.TEXT.DRAW    txt1, 50, 60 ,"--"
GR.RENDER

RETURN
! ------------------------
