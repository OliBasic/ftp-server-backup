
BUNDLE.CREATE global 
BUNDLE.PUT global, "mode", "console"
BUNDLE.PUT global, "back", "0"
BUNDLE.PUT global, "bkg","0"


!---------------------------------------------------
! function aliases/abbreviations
!

FN.DEF rp$(s$,r$,v$)
 FN.RTN REPLACE$(s$,r$,v$)
FN.END

FN.DEF rp(s$,r$,v$)
 s$=REPLACE$(s$,r$,v$)
FN.END

FN.DEF al(a[])
 ARRAY.LENGTH z,a[]
 FN.RTN z
FN.END

FN.DEF als(a$[])
 ARRAY.LENGTH z,a$[]
 FN.RTN z
FN.END

FN.DEF ls(l)
 LIST.SIZE l,z
 FN.RTN z
FN.END

FN.DEF arl(a[])
 ARRAY.LENGTH n,a[]
 FN.RTN n
FN.END

!
!read next record from open file handle h
! return 1 if EOF 

FN.DEF readrecord(h,a$,b$,c$)
 TEXT.READLN h,r$
 TEXT.EOF h,e
 IF e THEN FN.RTN 1
 SPLIT s$[],r$,"\t"
 ARRAY.LENGTH z,s$[]
 IF z=3
  LET a$=s$[1]:LET b$=s$[2]:LET c$=s$[3]
 ENDIF
 FN.RTN 0
FN.END

! add value v to value for bundle key k$
FN.DEF tallybundle(b,k$,newv) 
 LET v=0:BUNDLE.CONTAIN b,k$,e
 IF e THEN BUNDLE.GET b,k$,v
 BUNDLE.PUT b,k$,v+newv
FN.END 

! slightly more tolerant val function
FN.DEF val2(s$)
 LET s$=TRIM$(s$):LET l=LEN(s$)
 DO
  i++
  LET bad=!IS_IN(MID$(s$,i,1),"-0123456789.")
 UNTIL bad|i>=l
 IF !bad THEN FN.RTN VAL(s$)  
 FN.RTN 0
FN.END



FN.DEF rset(s$) 
 ! PRE: s$ is setting name
 ! POST: RETURNs value of setting s$ or 0 if not found 
 LET F$="settings.ini"
 FILE.EXISTS e,f$ 
 IF !e THEN FN.RTN 0 
 GRABFILE b$,f$
 LET i=IS_IN(s$,b$)
 IF !i THEN FN.RTN 0
 LET t$=MID$(b$,i+LEN(s$),20)
 LET v$=TRIM$(WORD$(t$,1,CHR$(10)))
 IF v$="" THEN FN.RTN 0
 FN.RTN VAL2(v$)
FN.END


FN.DEF readln$(f$) 
 FILE.EXISTS e,f$ 
 IF e
  TEXT.OPEN r,h,f$
  TEXT.READLN h,a$
  TEXT.CLOSE h
 ELSE
  LET A$=""
 ENDIF
 FN.RTN a$
FN.END


FN.DEF isold(f$) 
 FILE.EXISTS e,f$ 
 FN.RTN e
FN.END

FN.DEF appendln(f$,m$) 
 TEXT.OPEN a,h,f$
 TEXT.WRITELN h,m$
 TEXT.CLOSE h
FN.END

FN.DEF writeln(f$,msg$) 
 TEXT.OPEN w,h,f$
 TEXT.WRITELN h,msg$
 TEXT.CLOSE h 
FN.END

! replace first occcurence of f$ by r$ in s$ 
FN.DEF REPLACE1$(s$,f$,r$)
 LET i=IS_IN(f$,s$)
 FN.RTN LEFT$(S$,I-1)+r$+RIGHT$(S$,LEN(S$)-LEN(F$)-I+1)
FN.END

FN.DEF wset(s$,v) 
 ! PRE: s$ is setting name,v is value to set 
 ! POST: setting named s$ is set to v
 LET F$="settings.ini"
 LET ns$=s$+"\t"+INT$(v)
 FILE.EXISTS e,f$ 
 IF e
  TEXT.OPEN r,h,f$
  LET o$=ns$+CHR$(10)
  DO
   TEXT.READLN h,a$
   IF a$="EOF" THEN D_U.BREAK
   IF !found & WORD$(a$,1,"\t")=s$
    LET found=1
   ELSE
    o$+=a$+CHR$(10)
   ENDIF
  UNTIL a$="EOF"
 ENDIF
 TEXT.CLOSE h
 CALL writeln(f$,TRIM$(o$))
FN.END


FN.DEF readsetting(s$)
 FN.RTN rset(s$)
FN.END

FN.DEF writesetting(s$,v)
 CALL wset(s$,v)
FN.END

!
!
! init display mode manager
!
FN.DEF dinit()
 LET s$="<html><body style=\"background-color:black\"></body></html>"
 HTML.CLOSE
 HTML.OPEN 0
 HTML.LOAD.STRING s$
 BUNDLE.PUT 1,"mode","html"
FN.END 

!
! change display mode to console,html or gr(aphics)
!
FN.DEF dmode(m$)
 BUNDLE.GET 1,"mode",oldm$
 IF oldm$=m$ THEN FN.RTN 0
 BUNDLE.PUT 1,"mode",m$
 IF oldm$="html" THEN HTML.CLOSE
 IF oldm$="gr" THEN GR.CLOSE
 IF m$="html" THEN HTML.OPEN 0
 IF m$="gr" THEN GR.OPEN 255,0,0,0,0,1
FN.END

! returns true if users answers yes to prompt m$
FN.DEF cask(m$)
 CALL dmode("console")
 DIALOG.MESSAGE,m$,c,"yes","no"
 IF c=1 THEN FN.RTN 1
 FN.RTN 0
FN.END

!
! return TRUE if in background
FN.DEF backg()
 BUNDLE.GET 1,"bkg",s$
 FN.RTN s$="1"
FN.END

!
! save numerical bundle
FN.DEF nbsv(db$,b) 
 LET D$="\t"
 TEXT.OPEN w,f,db$
 BUNDLE.KEYS b,l
 LIST.SIZE l,z
 IF z<1 
  TEXT.WRITELN f,""
  TEXT.CLOSE f
  FN.RTN 0
 ENDIF
 LIST.TOARRAY l,ll$[] 
 ARRAY.SORT ll$[] 
 FOR i=1 TO z
  LET K$=ll$[i] 
  BUNDLE.GET b,k$,v
  IF k$<>""
   TEXT.WRITELN f,k$+d$+INT$(v) 
  ENDIF
 NEXT
 TEXT.CLOSE f
FN.END

! load string bundle from file 
FN.DEF nbld(db$,b) 
 BUNDLE.CLEAR b
 LET D$="\t"
 FILE.EXISTS e,db$
 IF !e THEN FN.RTN 0
 TEXT.OPEN R,h,db$
 DO
  TEXT.READLN h,a$
  LET s1$=WORD$(a$,1,d$)
  LET s2$=WORD$(a$,2,d$)
  IF s1$<>""&S2$<>"" THEN BUNDLE.PUT b,s1$,VAL(s2$)
 UNTIL a$="EOF"
 TEXT.CLOSE h
FN.END


FN.DEF grab$(f$)
 FILE.EXISTS e,f$ 
 IF e
  GRABFILE s$,f$
  FN.RTN s$
 ELSE
  FN.RTN ""
 ENDIF
FN.END

! html format for calories
FN.DEF frmcal$(n) 
 LET i$=INT$(n)
 LET s$="&nbsp;&nbsp;&nbsp;&nbsp;"
 FN.RTN MID$(s$,1,6*(4-LEN(i$)))+i$
FN.END 


FN.DEF frm$(n)
 FN.RTN FORMAT$("####%.#",n)
FN.END

FN.DEF startswith(f$,i$)
 ! return 0 if doesnt match,len(i$) if it matches
 LET l=LEN(f$)
 IF LEN(i$)<l THEN
  FN.RTN 0
 ENDIF
 IF LEFT$(i$,l)=f$ THEN
  FN.RTN l
 ELSE
  FN.RTN 0
 ENDIF
FN.END


FN.DEF getdate$()
 TIME Y$,M$,D$,H$,Mi$,S$,WkD,DST
 FN.RTN m$+"-"+d$+"-"+y$
FN.END

FN.DEF Asklist2$(p,msg$,r)
 ! PRE: string list p
 !       msg$ is prompt to display
 ! POST: r=index of item in p chosen
 !     Returns  string of item
 !
 DO
  LET I$=""
  DIALOG.SELECT r,p,msg$
  IF r>0
   LIST.GET p,r,i$
  ENDIF
 UNTIL r>0
 FN.RTN TRIM$(i$)
FN.END



FN.DEF editor(f$,p$) 
 ue$=grab$(f$)
 ue$=TRIM$(ue$)+CHR$(10)
 TEXT.INPUT e$,ue$,p$
 CALL writeln(f$,TRIM$(e$))
FN.END

FN.DEF INPUTNUMBER(s$,d)
 PAUSE 100
 KB.HIDE
 PAUSE 1000
 ?
 KB.TOGGLE
 IF d>=0
  INPUT s$,v,d,c
 ELSE
  INPUT s$,v,,c
 ENDIF
 IF c THEN EXIT 
 FN.RTN v
FN.END 



FN.DEF isadigit(s$) 
 FN.RTN IS_IN(s$,"-0123456789.")
FN.END

FN.DEF hasdigits(s$) 
 LET I=1:LET Found=0:LET l=LEN(s$)
 DO
  LET found=isadigit(MID$(s$,I,1))
  I++
 UNTIL i>l|found
 FN.RTN found
FN.END


! extract number from s$
FN.DEF readnumber(s$) 
 LET N$=""
 z=LEN(s$)
 FOR i=1 TO z 
  LET C$=MID$(s$,I,1)
  IF isadigit(c$) THEN N$+=c$
 NEXT
 IF LEN(n$)>0 THEN FN.RTN VAL(n$) 
 FN.RTN 0
FN.END

! extract number from s$
FN.DEF readnumber2(s$) 
 s$=LOWER$(s$)
 IF hasdigits(s$)
  LET N$=""
  z=LEN(s$) 
  FOR i=1 TO z
   LET C$=MID$(s$,I,1)
   IF isadigit(c$) THEN N$+=c$
  NEXT 
  IF LEN(n$)>0 THEN FN.RTN VAL(n$) 
 ELSE
  v=0
  IF IS_IN(" one ",s$) THEN v=1
  IF IS_IN(" two ",s$) THEN v=2
  IF IS_IN(" three ",s$) THEN v=3
  IF IS_IN(" four ",s$) THEN v=4
  IF IS_IN(" five ",s$) THEN v=5
  IF IS_IN(" six ",s$) THEN v=6
  IF IS_IN(" seven ",s$) THEN v=7
  IF IS_IN(" eight ",s$) THEN v=8
  IF IS_IN(" nine ",s$) THEN v=9
  IF IS_IN(" ten ",s$) THEN v=10
  IF IS_IN(" eleven ",s$) THEN v=11
  IF IS_IN(" twelve ",s$) THEN v=12
  IF IS_IN(" thirteen ",s$) THEN v=13
  IF IS_IN(" fourteen ",s$) THEN v=14
  IF IS_IN(" fifteen ",s$) THEN v=15
  IF IS_IN(" sixteen ",s$) THEN v=16
  IF IS_IN(" seventeen ",s$) THEN v=17
  IF IS_IN(" eighteen ",s$) THEN v=18
  IF IS_IN(" nineteen ",s$) THEN v=19
  IF IS_IN(" twenty ",s$) THEN v=20
  FN.RTN v
 ENDIF
 FN.RTN 0
FN.END



! val that returns 0 if not valid
! returns s$ with all numbers removed
FN.DEF stripdigits$(s$) 
 LET N$=""
 z=LEN(s$)
 FOR i=1 TO z
  LET C$=MID$(s$,I,1)
  IF !isadigit(c$) THEN N$+=c$
 NEXT 
 IF LEN(n$)>0 THEN FN.RTN n$
 FN.RTN "" 
FN.END


! load string bundle from file 
FN.DEF bld(db$,b) 
 BUNDLE.CLEAR b

 FILE.EXISTS e,db$
 IF !e THEN FN.RTN 0
 TEXT.OPEN R,h,db$
 DO
  TEXT.READLN h,a$
  LET s1$=WORD$(a$,1,"\t")
  LET s2$=WORD$(a$,2,"\t")
  IF s1$<>""&S2$<>"" THEN BUNDLE.PUT b,s1$,s2$
 UNTIL a$="EOF"
 TEXT.CLOSE h
FN.END

! write string bundle to file
FN.DEF bsv(db$,b)
 TEXT.OPEN w,f,db$
 BUNDLE.KEYS b,l
 LIST.SIZE l,z
 IF z<1 
  TEXT.WRITELN f,""
  TEXT.CLOSE f
  FN.RTN 0
 ENDIF
 LIST.TOARRAY l,ll$[] 
 ARRAY.SORT ll$[] 
 FOR i=1 TO z
  LET K$=ll$[i] 
  IF k$<>""
   BUNDLE.GET b,k$,v$
   TEXT.WRITELN f,k$+"\t"+v$
  ENDIF
 NEXT
 TEXT.CLOSE f
FN.END


! get numeric value from bundle b with key f$ 
FN.DEF getval(b,f$) 
 LET v=0
 BUNDLE.CONTAIN b,f$,e
 IF e THEN BUNDLE.GET b,f$,v
 FN.RTN v
FN.END


! read 3 field tab delimited table fn$ into string lists l1,l2,l3
FN.DEF readlists(f$,l1,l2,l3)
 FILE.EXISTS e,f$
 IF !e THEN FN.RTN 0 
 TEXT.OPEN R,h,f$
 DO
  TEXT.READLN h,a$
  eof=(a$="EOF")
  IF eof THEN D_U.BREAK
  UNDIM r$[]
  SPLIT r$[],a$,"\t"
  ARRAY.LENGTH z,r$[]	
  IF z=3
   LIST.ADD l1,r$[1]
   LIST.ADD l2,r$[2]
   LIST.ADD l3,r$[3]
  ENDIF
 UNTIL eof 
 TEXT.CLOSE h
FN.END

! read 3 field tab delimited table fn$ into string lists l1 and number list l2
FN.DEF readlist2(f$,l1,l2)
 FILE.EXISTS e,f$
 IF !e THEN FN.RTN 0
 TEXT.OPEN R,h,f$
 LET r1$="":LET r2$=""
 DO
  TEXT.READLN h,a$
  eof=(a$="EOF")
  IF !eof
   LET r1$=WORD$(a$,1,"\t")
   LET r2$=WORD$(a$,2,"\t")
   IF r1$<>""&r2$<>"" 
    LIST.ADD l1,r1$
    LIST.ADD l2,VAL(r2$)
   ENDIF
  ENDIF
 UNTIL eof 
 TEXT.CLOSE h
FN.END

! write contents of ordered string lists l1, l2,l3 to file
FN.DEF writelists(f$,l1,l2,l3)
 TEXT.OPEN W,h,f$
 LIST.SIZE l1,n1
 LIST.SIZE l2,n2
 LIST.SIZE l3,n3
 FOR i=1 TO n1
  LIST.GET l1,i,s1$
  LIST.GET l2,i,s2$
  LIST.GET l3,i,s3$
  TEXT.WRITELN h,s1$+"\t"+s2$+d$+s3$
 NEXT
 TEXT.CLOSE h
FN.END

! write contents of ordered n list l1 to file
FN.DEF writenlist(f$,l)
 TEXT.OPEN W,h,f$
 LIST.SIZE l,z
 FOR i=1 TO z
  LIST.GET l,i,s1
  TEXT.WRITELN h,INT$(s1)
 NEXT
 TEXT.CLOSE h
FN.END


! read single string list
FN.DEF readlist(f$,l)
 FILE.EXISTS e,f$ 
 IF e
  TEXT.OPEN R,h,f$
  DO
   TEXT.READLN h,a$
   IF a$<>"EOF" THEN LIST.ADD l,a$
  UNTIL a$="EOF"
  TEXT.CLOSE h
 ENDIF
FN.END


! read numeric string list from file
FN.DEF readnlist(f$,l)
 FILE.EXISTS e,f$ 
 IF e
  TEXT.OPEN R,h,f$
  DO
   TEXT.READLN h,a$
   IF a$<>"EOF" THEN LIST.ADD l,VAL(a$)
  UNTIL a$="EOF"
  TEXT.CLOSE h
 ENDIF
FN.END

! add value v to queue
FN.DEF qadd(v)
 LIST.CREATE n,w
 CALL readnlist("weekcal.txt",w)
 LIST.ADD w,v
 LIST.SIZE w,z
 IF z>7 THEN LIST.REMOVE w,1
 CALL writenlist("weekcal.txt",w)
FN.END

! calculate average of calories in weeks queue
FN.DEF qavg(z)
 LIST.CREATE n,w
 CALL readnlist("weekcal.txt",w)
 LIST.SIZE w,z
 FOR i=1 TO z
  LIST.GET w,i,v:s+=v
 NEXT
 IF z>0
  FN.RTN FLOOR(s/z+0.5)
 ELSE 
  FN.RTN 0
 ENDIF
FN.END

!
! wait for click on html screen 
FN.DEF waitclick$()
 DO
  PAUSE 200
  HTML.GET.DATALINK data$
  IF backg() THEN EXIT
 UNTIL data$ <> ""

 IF IS_IN("BAK:",data$)=1
  EXIT
 ELSEIF IS_IN("DAT:",data$)=1
  data$=MID$(data$,5) 
 ELSEIF IS_IN("LNK:file:///",data$)=1&IS_IN("?",data$) %' Submit link
  i=IS_IN("?",data$)
  data$="SUBMIT&"+MID$(data$,i+1)+"&"
 ENDIF
 FN.RTN data$
FN.END



! do string search for food f$ on tab delimited food data contained in db$
FN.DEF guess(f$)
 LET d$="fooddb.txt"
 LET f$=TRIM$(f$)
 LET r$=CHR$(10)
 FILE.EXISTS e,d$ 
 IF !e THEN FN.RTN -99999
 GRABFILE db$,d$
 LET g=IS_IN(r$+f$+"\t",r$+db$) 
 IF !g THEN FN.RTN -99999
 LET i$=WORD$(MID$(db$,g,100),1,r$)
 LET c$=WORD$(i$,2,"\t") 
 FN.RTN readnumber(c$) 
FN.END

FN.DEF asklist$(l,msg$,c)
 r$="<br>"
 h$="<!DOCTYPE html><html lang=~en~><head>"
 h$+="<meta charset=~utf-8~ /> <title>Home</title>"
 h$+="<meta content='width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0' name='viewport' />"
 h$+="<meta name=~viewport~ content=~width=device-width; initial-scale=1.0;maximum-scale=1.0~/></head>"
 h$+="<h1 style=~color:#999999~>"+msg$+"</h1>"
 h$+="<body style=~background-color:#000000;text-align:center;~>"
 h$+="<div>"
 LIST.SIZE l,z
 FOR i=1 TO z
  LIST.GET l,i,s$
  s$=REPLACE$(s$,"'","&apos;")
  but$="<button type=~button~  style=~background-color:#333333;color:#FFFFFF;text-align:center;font-size:20px;width:300px;height:50px;~ onclick=~doDataLink('"+STR$(i)+"')~;>"+s$+"</button><br>"
  h$+=but$
 NEXT
 h$+="</div>"
 h$+="<script type=~text/javascript~>"
 h$+="function doDataLink(data)"
 h$+="{Android.dataLink(data);}</script>"
 h$+="</body>"
 h$+="</html>"
 h$=REPLACE$(h$,"~","\"")
 dmode("html")
 HTML.LOAD.STRING h$
 DO
  r$=waitclick$()
  c=readnumber(r$)
 UNTIL c>0
 LIST.GET l,c,s$
 FN.RTN s$
FN.END


FN.DEF askyn(p$)
 LIST.CREATE s,m
 LIST.ADD m,"yes","no"
 c=0
 asklist$(m,p$,&c)
 LIST.CLEAR m
 IF c=1 THEN FN.RTN 1
 FN.RTN 0
FN.END

! lookup calories/other info on web 
FN.DEF findcals(p$)
 CALL dmode("html")
 LET P$=REPLACE$(p$,"find","") 
 LET P$=REPLACE$(p$,"search","") 
 m$="use web?"
 LIST.CREATE s,l
 LIST.ADD l,"USDA Foodapedia","google","CDC BMI calculator","bing "+p$,"duckduckgo "+p$,"Basal energy expenditure","YouTube playlist","cancel"
 LET ch=0
 asklist$(l,m$,&ch)
 IF ch>0 & ch<8
  POPUP "press BACK key to return to app"
 ENDIF
 IF ch=1
  BROWSE "https://m.supertracker.usda.gov/foodapedia.aspx"
 ELSE if ch=2
  BROWSE "http:/www.google.com"
 ELSE if ch=3
  BROWSE "http://www.cdc.gov/healthyweight/assessing/bmi/adult_bmi/english_bmi_calculator/bmi_calculator.html"
 ELSE if ch=4
  BROWSE "http://www.bing.com/?q="+p$+" calories"
 ELSE if ch=5 
  BROWSE "https://duckduckgo.com/?q="+p$+"+calories&ia=nutrition"
 ELSE if ch=6
  BROWSE "http://www-users.med.cornell.edu/~spon/picu/calc/beecalc.htm"
 ELSE if ch=7
  BROWSE "https://www.youtube.com/playlist?list=PLi6Y_PQh4vOmk9JJinwzYC-bWct_CT8sK"
 ENDIF
 DO 
  PAUSE 100
 UNTIL !BACKGROUND()
 PAUSE 1000
FN.END

! clear back key flag
FN.DEF bkclr()
 BUNDLE.PUT 1,"back","0"
FN.END


FN.DEF bkhit()
 BUNDLE.GET 1,"back",b$
 LET b=(b$="1")
 IF b
  POPUP "back key hit"
  BUNDLE.PUT 1,"back","0"
 ENDIF
 FN.RTN b
FN.END

! return 0 if doesnt match,len(si$) if it matches
FN.DEF endswith(sf$,si$)
 l=LEN(sf$)
 IF LEN(si$)<l THEN FN.RTN 0
 IF RIGHT$(si$,l)=sf$ 
  FN.RTN l
 ELSE
  FN.RTN 0
 ENDIF
FN.END



FN.DEF simplifyPath$(rep$)
 IF rep$="../../../../../" 
  FN.RTN "../../../../"
 ENDIF

 ew=endswith("/../",rep$)
 IF ew=0 THEN FN.RTN rep$

 IF rep$="/../" THEN FN.RTN rep$

 i=LEN(rep$)-4
 WHILE ((MID$(rep$,i,1)<>"/")&(i>1))
  i--
 REPEAT
 IF i=1 THEN i=0
 a$=MID$(rep$,i+1,LEN(rep$)-i-4)
 IF a$=".."
  FN.RTN rep$
 ELSE
  FN.RTN LEFT$(rep$,i)
 ENDIF
FN.END

FN.DEF chooseFileDir$(rep$,dironly)
 ! rep$ is the starting directory
 ! dironly=1=> displays only directories
 !	each directory is displayed twice: once for navigation and once for selection
 ! dironly=0=> displays files + directories
 !	only files can be selected,directories are displayed for navigation only
 ! returns the full path of chosen file/directory or "<cancel>" if user used the back button
 fin=0
 DO
  FILE.DIR rep$,a$[]
  ARRAY.LENGTH n,a$[]
  DIM b$[2*n+2]
  b$[1]="Current Directory: "+rep$
  b$[2]="..(d)"
  j=2
  FOR i=1 TO n
   ajoute=1
   IF ajoute 
    j++
    b$[j]=a$[i]
   ENDIF
  NEXT
  UNDIM a$[]

  LIST.CREATE s,clist

  LIST.ADD clist, b$[1]
  FOR i=2 TO j
   IF endswith(".csv",b$[i] )| endswith(".bas",b$[i] |  endswith(".txt",b$[i] )| endswith("(d)",b$[i] ) 
    LIST.ADD clist,b$[i]
   ENDIF
  NEXT

  LIST.TOARRAY clist,c$[]

  UNDIM b$[]
  SELECT choix,c$[],rep$
  IF choix>1 
   d$=c$[choix]
   sw=startswith("  -> select ",d$)
   ew=endswith("(d)",d$)
   IF sw 
    e$=c$[choix-1]
    e$=LEFT$(e$,LEN(e$)-3)
    e$=rep$+e$+"/"
    e$=simplifyPath$(e$)
    fin=1
   ELSEIF ew 
    e$=d$
    e$=LEFT$(e$,LEN(e$)-3)
    rep$=simplifyPath$(rep$+e$+"/")
   ELSE
    e$=rep$+d$
    fin=1
   ENDIF
  ELSEIF choix=0
   e$="<cancel>"
   fin=1
  ENDIF
  UNDIM c$[]
 UNTIL fin
 FN.RTN e$
FN.END

FN.DEF ChooseFile$(path$) 
 ! From f25_dir.bas
 ! 	Path$=""
 LOOP:
 ARRAY.DELETE d1$[]
 FILE.DIR Path$,d1$[]
 ARRAY.LENGTH z,d1$[]
 LIST.CREATE s,d2
 ARRAY.DELETE d2$[]
 LIST.ADD d2,".."

 FOR i=1 TO z
  IF endswith(".txt",d1$[i])|endswith(".bas",d1$[i])|endswith(".csv",d1$[i])|IS_IN("(d)",d1$[i])
   LIST.ADD d2,d1$[i] 
  ENDIF
 NEXT
 LIST.TOARRAY d2,d2$[]
 s=0
 asklist$(d2,path$+"<b>Select Datafile...",&s)

 !	SELECT s,d2$[] ,path$+" Select Datafile..."
 IF s=0 THEN FN.RTN ""
 IF s>1 
  n=IS_IN("(d)",d2$[s])
  IF n=0
   FN.RTN path$+d2$[s]
  ENDIF
  dname$=LEFT$(d2$[s],n-1)
  Path$+=dname$+"/"
  GOTO LOOP
 ENDIF
 IF Path$="" 
  Path$="../"
  GOTO LOOP
 ENDIF
 ARRAY.DELETE p$[]
 SPLIT p$[],Path$,"/"
 ARRAY.LENGTH z,p$[]
 IF p$[z]=".." 
  Path$+="../"
  GOTO LOOP
 ENDIF
 IF z=1 
  Path$=""
  GOTO LOOP
 ENDIF
 Path$=""
 FOR i=1 TO z-1
  Path$+=p$[i]+"/"
 NEXT
 GOTO LOOP
 FN.RTN path$
FN.END
