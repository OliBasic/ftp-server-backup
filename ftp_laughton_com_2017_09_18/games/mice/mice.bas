! **************************************
! antiraton
! **************************************


GR.OPEN 255,255,255,255,0,0  %modo grafico colores orientacion landscape
PAUSE 1000     % ponerlo siempre despues de gr.open

AUDIO.LOAD disp_ptr,"disparo.mp3"
AUDIO.LOAD explo_ptr,"explosion.mp3"
FN.DEF numero$(numero)
 numero$=FORMAT$("####",numero)
 FN.RTN numero$
FN.END

inicio:

tiempo=120

njuego$="1" %  numero del juego, 1 o 2, 1 el cañon no se para de mover
dificultad$="1"  %nivel de dificultad

a=RANDOMIZE(CLOCK())
xm=1280
ym=800
r1=80 %limite superior del raton
r2=400 % limite inferior del raton

raton$="decha" % sentido de movimiento del raton
canon$="decha"  % id del cañon



GR.CLS
GR.RENDER

GR.SET.STROKE 5   % ancho de linea de los objetos
GR.SCREEN w,h   % devuelve el ancho  y alto de la pantalla
sx=w/xm
sy=h/ym
esc=sx
IF sy<sx THEN esc=sy
GR.SCALE esc,esc  % escala todos los comandos graficos

GOSUB pantalla_inicial
IF dificultad$="1" THEN
 ancho=80 % ancho del raton
 alto=10  % alto del raton
 pasoraton=3
 pasocanon=4
 pausaraton=40
ENDIF
IF dificultad$="2" THEN
 ancho=50 % ancho del raton
 alto=10  % alto del raton
 pasoraton=5
 pasocanon=5
 pausaraton=20
ENDIF
IF dificultad$="3" THEN
 ancho=30 % ancho del raton
 alto=10  % alto del raton
 pasoraton=7
 pasocanon=6
 pausaraton=1
ENDIF


GR.COLOR 255,255,255,255,1
GR.CLS
xr=0
yr=r1+RND()*(r2-r1)
GR.RECT raton_ptr,xr,yr,xr+ancho,yr+alto

x5=xm/2
y5=ym
r=150
alfa=60+90*RND()
alfami=30
alfama=150
x6=x5+r*COS(alfa*3.14/180)
y6=y5-r*SIN(alfa*3.14/180)

GR.LINE canon_ptr,x5,y5,x6,y6


x8=10
y8=ym-10

GOSUB mandos



sx=30
sy=70
GR.TEXT.SIZE 70
GR.COLOR 255,255,0,0,1
sc=1
GR.TEXT.DRAW sc_ptr,sx,sy,"Score: "+numero$(sc)
GR.TEXT.DRAW game_ptr, 540,sy,"Game: "+njuego$
tx=950
ty=sy

GR.TEXT.DRAW ti_ptr,tx,ty,"Time:"+numero$(tiempo)


x9=xm-200
y9=ym-10
al=90
an=180
x91=x9
y91=y8-al
x93=x91+an
y93=y8

GR.COLOR 255,255,255,0,1
GR.RECT nr, x91,y91,x93,y93
GR.COLOR 255,0,0,0,1
GR.TEXT.SIZE al-8
GR.TEXT.DRAW pt, x9+25, y9-13,"Fire"
fin=0
segundos_inicial=CLOCK()/1000
GR.RENDER
tocado_izda=0
tocado_decha=0
DO

 GOSUB mover_raton
 GOSUB dibujar_raton
 PAUSE pausaraton
 IF njuego$="1" THEN  GOSUB mover_canon
 IF njuego$="2" & tocado_decha<>0 THEN
  canon$="decha"
  GOSUB mover_canon
  tocado_decha=0
  tocado_izda=0
 ENDIF
 IF njuego$="2" & tocado_izda<>0 THEN
  canon$="izda"
  GOSUB mover_canon
  tocado_izda=0
  tocado_decha=0
 ENDIF
 segundos_transcurridos=CLOCK()/1000-segundos_inicial
 IF (tiempo-segundos_transcurridos)>=1 THEN
  GR.MODIFY ti_ptr,"text","Time:"+numero$(tiempo - segundos_transcurridos)
 ELSE
  GR.MODIFY ti_ptr,"text","Time:"+"    0"
  fin=1
  D_U.BREAK
 ENDIF
 GR.RENDER

UNTIL 0
IF fin=1 THEN goto terminar
!----------------------------------------------------------------------
ONGRTOUCH:
GR.BOUNDED.TOUCH touched,x91*esc,y91*esc,x93*esc,y93*esc  %fire
IF touched THEN gosub fuego
!-- Mira si se ha pulsado left o right

IF njuego$="2"THEN
 GR.BOUNDED.TOUCH tocado_izda, x81*esc,y81*esc,x83*esc,y83*esc
 GR.BOUNDED.TOUCH tocado_decha,x3*esc,y3*esc,x31*esc,y31*esc
ENDIF



GR.ONGRTOUCH.RESUME
onbackkey:
end
onerror:
end
!-----------------------------------------------------------------------FUEGO
fuego:
IF fin=1 THEN return
xxf= ((-r1+y5)/TAN(alfa*3.14/180))+x5
yyf=r1
AUDIO.STOP
AUDIO.PLAY disp_ptr
PAUSE 100
colision=0
GOSUB hay_colision
IF colision=1 THEN
 AUDIO.STOP
 AUDIO.PLAY explo_ptr
 sc=sc+1
 GR.MODIFY sc_ptr,"text","Score: "+numero$(sc)
 xxf=xcol
 yyf=yr+alto

ENDIF
GR.MODIFY canon_ptr,"x2",xxf
GR.MODIFY canon_ptr,"y2",yyf
GR.RENDER
PAUSE 600
GR.MODIFY canon_ptr,"x2",x5+r*COS(alfa*3.14/180)
GR.MODIFY canon_ptr,"y2",y5-r*SIN(alfa*3.14/180)
RETURN
!------------------------------------------------ dibujar raton
dibujar_raton:
GR.MODIFY raton_ptr ,"left",xr
GR.MODIFY raton_ptr ,"top",yr
GR.MODIFY raton_ptr ,"right",xr+ancho
GR.MODIFY raton_ptr ,"bottom",yr+alto
RETURN
!------------------------------------------------ mover raton
mover_raton:
IF raton$="decha" THEN
 IF (xr+pasoraton)<=xm THEN
  xr=xr+pasoraton
 ELSE
  raton$="izda"
  xr=xr-pasoraton
  yr=r1+RND()*(r2-r1)
 ENDIF
ENDIF
IF raton$="izda" THEN
 IF (xr-pasoraton)>=0 THEN
  xr=xr-pasoraton
 ELSE
  raton$="decha"
  yr=r1+RND()*(r2-r1)
 ENDIF
ENDIF
RETURN
!-------------------------------------------------- mover canon
mover_canon:
IF canon$="decha" THEN
 IF (alfa-pasocanon)>=alfami THEN
  alfa=alfa-pasocanon
  x6=x5+r*COS(alfa*3.14/180)
  y6=y5-r*SIN(alfa*3.14/180)
 ELSE
  canon$="izda"
  alfa=alfa+pasocanon
  x6=x5+r*COS(alfa*3.14/180)
  y6=y5-r*SIN(alfa*3.14/180)
 ENDIF
else 
 IF (alfa+pasocanon)<=alfama THEN
  alfa=alfa+pasocanon
  x6=x5+r*COS(alfa*3.14/180)
  y6=y5-r*SIN(alfa*3.14/180)
 ELSE
  canon$="decha"
  alfa=alfa-pasocanon
  x6=x5+r*COS(alfa*3.14/180)
  y6=y5-r*SIN(alfa*3.14/180)
 ENDIF
ENDIF


GR.MODIFY canon_ptr ,"x2",x6
GR.MODIFY canon_ptr ,"y2",y6

gr.render  

RETURN
!---------------------------------------------------- hay colision
hay_colision:
xcol=(-(yr+alto)+y5)/(TAN(alfa*3.14/180))+x5
IF xcol>xr & xcol<(xr+ancho) THEN colision=1
RETURN
!------------------------------------------------------------------------ PANTALLA INICIAL
pantalla_inicial:
GR.TEXT.SIZE 90
GR.COLOR 255,0,0,0,1
GR.TEXT.DRAW tt1,0.2*xm,0.12*ym,"Cannon vs. Mice"

GR.TEXT.DRAW tt2,0.1*xm,0.4*ym,"Game type:"
xxx=700
yyy=200
lado=130
GR.COLOR 255,255,0,0,1  %rojo
GR.RECT tt11,xxx,yyy,xxx+lado,yyy+lado
GR.COLOR 255,251,242,101,1  %amarillo
GR.RECT tt12, xxx+250,yyy,xxx+250+lado,yyy+lado
GR.COLOR 255,0,0,0,1
GR.TEXT.DRAW tty3,0.1*xm,0.6*ym,"Difficulty:"
x1x=640
y1y=360
lado2=110
GR.COLOR 255,255,0,0,1
GR.RECT rect7,x1x,y1y,x1x+lado,y1y+lado

GR.COLOR 255,251,242,101,1
GR.RECT rect9, x1x+200,y1y,x1x+200+lado,y1y+lado
GR.RECT rect10,x1x+400,y1y,x1x+400+lado,y1y+lado
GOSUB numeros


xxw=950
yyw=600
ww=200
xx=99
GR.COLOR 255,0,121,12,1
GR.RECT tt_ini,xxw,yyw,xxw+ww,yyw+xx
GR.COLOR 255,0,0,0,1
GR.TEXT.DRAW tex8,	xxw+5,yyw+85,"Start"

GR.TEXT.SIZE 30
GR.TEXT.DRAW mangri,30,0.85*ym,"by mangriton@gmail.com"
GR.RENDER

DO
 DO
  GR.TOUCH touched ,x,y
 UNTIL touched
 DO
  GR.TOUCH touched,x,y
 UNTIL !touched
 % x,y son las coordenadas al tocar pantalla
 x=x/esc
 y=y/esc

 ! mirar tecla del tipo de juego
 IF x>xxx & x<xxx+lado & y>yyy & y<yyy+lado THEN  % game type 1
  !poner 1 en rojo
  GR.COLOR 255,255,0,0,1 %rojo
  GR.RECT tt11,xxx,yyy,xxx+lado,yyy+lado
  GR.COLOR 255,251,242,101,1   % dificultad 2, amarillo
  GR.RECT tt12, xxx+250,yyy,xxx+250+lado,yyy+lado
  GOSUB numeros
  njuego$="1"
 ENDIF
 IF x>xxx+250 & x<xxx+250+lado & y>yyy & y<yyy+lado THEN  % game type 2
  !poner 1 en amarillo
  GR.COLOR 255,251,242,101,1 %amarillo
  GR.RECT tt11,xxx,yyy,xxx+lado,yyy+lado
  GR.COLOR 255,255,0,0,1   % dificultad 2, rojo
  GR.RECT tt12, xxx+250,yyy,xxx+250+lado,yyy+lado
  GOSUB numeros
  njuego$="2"
 ENDIF
 IF x>x1x & x<x1x+lado & y>y1y & y<y1y+lado THEN  % dificultad 1
  !poner 1 en amarillo
  GR.COLOR 255,251,242,101,1
  GR.RECT rect9, x1x+200,y1y,x1x+200+lado,y1y+lado
  GR.RECT rect10,x1x+400,y1y,x1x+400+lado,y1y+lado
  GR.COLOR 255,255,0,0,1
  GR.RECT rect7,x1x,y1y,x1x+lado,y1y+lado
  GOSUB numeros
  dificultad$="1"
 ENDIF

 IF x>x1x+200 & x<x1x+200+lado & y>y1y & y<y1y+lado THEN  % dificultad 2
  !poner 1 en amarillo
  GR.COLOR 255,251,242,101,1
  GR.RECT rect7,x1x,y1y,x1x+lado,y1y+lado
  GR.RECT rect10,x1x+400,y1y,x1x+400+lado,y1y+lado
  GR.COLOR 255,255,0,0,1
  GR.RECT rect9, x1x+200,y1y,x1x+200+lado,y1y+lado
  GOSUB numeros
  dificultad$="2"
 ENDIF

 IF x>x1x+400 & x<x1x+400+lado & y>y1y & y<y1y+lado THEN  % dificultad 3
  !poner 1 en amarillo
  GR.COLOR 255,251,242,101,1
  GR.RECT rect9, x1x+200,y1y,x1x+200+lado,y1y+lado
  GR.RECT rect7,x1x,y1y,x1x+lado,y1y+lado
  GR.COLOR 255,255,0,0,1
  GR.RECT rect10,x1x+400,y1y,x1x+400+lado,y1y+lado
  GOSUB numeros
  dificultad$="3"
 ENDIF

 IF x>xxw & x<xxw+ww & y>yyw & y<yyw+xx % start
  ! salir del bucle, se ha pulsado start
  D_U.BREAK
 ENDIF
 GR.RENDER
UNTIL 0
RETURN
!---------------------------------------------------------------------numeros
numeros:
GR.COLOR 255,0,0,0,1
GR.TEXT.SIZE 90
GR.TEXT.DRAW tx8,	xxx+40,yyy+lado-30,"1"
GR.TEXT.DRAW tx9,xxx+40+250,yyy+lado-30,"2"
GR.TEXT.DRAW tex8,	x1x+35,y1y+lado-20,"1"
GR.TEXT.DRAW tex9,x1x+35+200,y1y+lado-20,"2"
GR.TEXT.DRAW tex10,x1x+35+400,y1y+lado-20,"3"
RETURN
!-------------------------------------------------------------------terminar
terminar:
GR.RECT termin_ptr,0.2*xm,0.4*ym,0.8*xm,0.6*ym
GR.TEXT.SIZE 0.14*ym
GR.COLOR 255,0,255,0,1
GR.TEXT.DRAW termin,0.2*xm+90,0.6*ym-40,"Game Over"
GR.RENDER
DO
 GR.TOUCH touched ,x,y
UNTIL touched
DO
 GR.TOUCH touched,x,y
UNTIL !touched
GOTO inicio

!-----------------------------------------------------------------------------mandos
mandos:
IF njuego$="2" THEN
 al=90
 an=185

 x81=x8
 y81=y8-al
 x83=x8+an
 y83=y8
 GR.COLOR 255,255,255,0,1
 GR.RECT nr, x81,y81,x83,y83
 GR.COLOR 255,0,0,0,1
 GR.TEXT.SIZE al-15
 GR.TEXT.DRAW pt, x8+10, y8-13,"Left"


 x3=x8+an+10
 y3=y81
 x31=x8+an+10+an
 y31=y8
 GR.COLOR 255,255,255,0,1
 GR.RECT nr2, x3,y3,x31,y31
 GR.COLOR 255,0,0,0,1
 GR.TEXT.SIZE al-15
 GR.TEXT.DRAW pt2, x8+13+an, y8-13,"Right"
ENDIF
RETURN
