!'---------------------------------------------------------------------------------------------------
!'
!'                               BWING, BY MOUGINO (2013)
!'                              GAME POWERED BY RFO-BASIC!
!'
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' The usual functions
!'---------------------------------------------------------------------------------------------------
Fn.def Stg$(c)
  Fn.rtn Left$(Str$(c),Is_In(".",Str$(c))-1)
Fn.end

Fn.def Hex2$(c)
  h$=Hex$(c)
  if len(h$)=1 then Fn.rtn "0"+h$ else Fn.rtn h$
Fn.end

Fn.def SetRGB(alpha,RGB$,fill)
  gr.color alpha,Hex(Mid$(RGB$,1,2)),Hex(Mid$(RGB$,3,2)),Hex(Mid$(RGB$,5,2)),fill
Fn.end

Fn.def IntToStr$(c)
  b2=Mod(c,256)
  b1=(c-b2)/256
  Fn.rtn Chr$(b1)+Chr$(b2)
Fn.end

Fn.def GetIntFrom(e$)
  b1=Ascii(e$)
  e$=Right$(e$,Len(e$)-1)
  b2=Ascii(e$)
  e$=Right$(e$,Len(e$)-1)
  Fn.rtn 256*b1+b2
Fn.end

Fn.def GetLang$()
  system.open
  system.write "getprop"
  pause 100
  do
    system.read.line l$
    system.read.ready ready
    fd=Is_In("language", l$)
  until !ready | fd
  system.close
  if fd then Fn.rtn Mid$(Word$(l$,2,":"),3,2)
Fn.end
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Check game folder and preferences
!'---------------------------------------------------------------------------------------------------
File.Exists o, "prefs.txt"
if o then
  Text.open r, c, "prefs.txt"
  Text.readln c, levelsDone$
  Text.close c
End If
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Startup
!'---------------------------------------------------------------------------------------------------
UNDEF=2^16-1
tilew=128
tileh=128
!' Graphic initialization
gr.open 255,255,255,255,1
gr.orientation -1
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Load external resources
!'---------------------------------------------------------------------------------------------------
!' 1) Unpack levels file
Byte.open r, c, "Bwing.lvl"
Byte.read.byte c, NbOfLevels %' number of levels
  dim mapw[NbOfLevels]
  dim maph[NbOfLevels]
  dim scenery[NbOfLevels]
  dim mapdata$[NbOfLevels]
Byte.read.byte c, NbOfScenery %' number of sceneries
  dim wallpaperBmp[NbOfScenery]
  dim groundTile[NbOfScenery]
  dim holeTile[NbOfScenery]
  dim boxTile[NbOfScenery]
  dim spikesTile[NbOfScenery]
  dim waterbgnTile[NbOfScenery]
  dim waterfgnTile[NbOfScenery]
  dim frontTile[NbOfScenery]
  dim iceTile[NbOfScenery]
for i=1 to NbOfLevels
  Byte.read.byte c, mapw[i]     %' current map width
  Byte.read.byte c, maph[i]     %' current map height
  Byte.read.byte c, scenery[i]  %' current map scenery
  Byte.read.buffer c, Ceil(mapw[i]/2)*maph[i], mapdata$[i]     %' current map data
next
Byte.close c
!' 2) Load graphics
for i=1 to NbOfScenery
  gr.bitmap.load wallpaperBmp[i], "wallpaper"+Stg$(i)+".jpg"
  gr.bitmap.load groundTile[i], "ground"+Stg$(i)+".png"
  gr.bitmap.load spikesTile[i], "spikes"+Stg$(i)+".png"
  gr.bitmap.load holeTile[i], "hole"+Stg$(i)+".png"
  gr.bitmap.load boxTile[i], "box"+Stg$(i)+".png"
  gr.bitmap.load waterbgnTile[i], "waterbgn"+Stg$(i)+".png"
  gr.bitmap.load waterfgnTile[i], "waterfgn"+Stg$(i)+".png"
  gr.bitmap.load frontTile[i], "front"+Stg$(i)+".png"
  gr.bitmap.load iceTile[i], "ice"+Stg$(i)+".png"
next
dim thermoTile[2] %' thermoTile[cold,hot]
gr.bitmap.load thermoTile[1], "thermocold.png"
gr.bitmap.load thermoTile[2], "thermohot.png"
dim heroTile[2,6,7] %' heroTile[page,x,y]
gr.bitmap.load tmpBmp, "chars1.png"
gr.bitmap.size tmpBmp, w, h
for y=0 to 6 %' load original hero tiles in page 1
  for x=0 to 5
    gr.bitmap.crop heroTile[1,x+1,y+1], tmpBmp, x*tilew, y*tileh, tilew, tileh
  next x
next y
gr.bitmap.delete tmpBmp
for y=0 to 6 %' mirror the hero tiles in page 2
  for x=0 to 5
    gr.bitmap.scale heroTile[2,x+1,y+1], heroTile[1,x+1,y+1], -tilew, tileh
  next x
next y
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Create internal resources
!'---------------------------------------------------------------------------------------------------
dim arrow[4] %' up right down left arrows
dim arrowBmp[4]
dim arrowResizedBmp[4]
dim arrowOffset[4]
read.data 1,3,1,0
for i=1 to 4
  read.next arrowOffset[i]
  list.create n, arrow[i] %' "n" for list of *n*umbers
  gr.bitmap.create arrowBmp[i], tilew, tileh
next
list.add arrow[1], -tilew/2,-tileh/2, tilew/2,-tileh/2, 0,-tileh
list.add arrow[2], tilew/2,-tileh/2, tilew/2,tileh/2, tilew,0
list.add arrow[3], tilew/2,tileh/2, -tilew/2,tileh/2, 0,tileh
list.add arrow[4], -tilew/2,tileh/2, -tilew/2,-tileh/2, -tilew,0
gr.bitmap.drawinto.start arrowBmp[1] %' up arrow
  Call SetRGB(175,"777777",1)
  gr.poly nul, arrow[1], tilew/2,tileh-3
  Call SetRGB(175,"EEEEEE",1)
  gr.poly nul, arrow[1], tilew/2,tileh
  list.clear arrow[1]
gr.bitmap.drawinto.end
gr.bitmap.drawinto.start arrowBmp[2] %' right arrow
  Call SetRGB(175,"777777",1)
  gr.poly nul, arrow[2], -tilew/2+3,tileh/2
  Call SetRGB(175,"EEEEEE",1)
  gr.poly nul, arrow[2], -tilew/2,tileh/2
  list.clear arrow[2]
gr.bitmap.drawinto.end
gr.bitmap.drawinto.start arrowBmp[3] %' down arrow
  Call SetRGB(175,"777777",1)
  gr.poly nul, arrow[3], tilew/2,-tileh/2+3
  Call SetRGB(175,"EEEEEE",1)
  gr.poly nul, arrow[3], tilew/2,-tileh/2
  list.clear arrow[3]
gr.bitmap.drawinto.end
gr.bitmap.drawinto.start arrowBmp[4] %' left arrow
  Call SetRGB(175,"777777",1)
  gr.poly nul, arrow[4], tilew-3,tileh/2
  Call SetRGB(175,"EEEEEE",1)
  gr.poly nul, arrow[4], tilew,tileh/2
  list.clear arrow[4]
gr.bitmap.drawinto.end
Call SetRGB(255,"FFFFFF",1)
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Define the animations
!'---------------------------------------------------------------------------------------------------
dim herOffset[10] %' 10 characters
dim heroIdleBmp$[10]
dim heroGoRightBmp$[10]
dim heroGoRightMov$[10]
dim heroGoLeftBmp$[10]
dim heroActRightBmp$[10]
dim heroActLeftBmp$[10]
!' character #1: Lilian
herOffset[1]=15
read.data 1,1,1, 1,2,1, 1,3,1, 1,4,1, 1,5,1, 1,4,1, 1,3,1, 1,2,1, 1,1,1, 2,2,1, 2,3,1, 2,4,1, 2,5,1, 2,4,1, 2,3,1, 2,2,1
for i=1 to 16
  read.next page,x,y
  heroIdleBmp$[1] += IntToStr$(heroTile[page,x,y])
next
read.data 1,6,1, 1,1,2, 1,2,2, 1,3,2, 1,4,2, 1,5,2, 1,6,2, 1,6,2, 1,5,2, 1,4,2, 1,3,2, 1,3,2, 1,2,2, 1,1,2, 1,6,1
for i=1 to 15
  read.next page,x,y
  heroGoRightBmp$[1] += IntToStr$(heroTile[page,x,y])
  heroGoLeftBmp$[1] += IntToStr$(heroTile[3-page,x,y])
next
read.data 0,0, 0,0, 0,0, 0,0, 16,-8, 16,-8, 16,-6, 16,-4, 16,4, 16,6, 16,8, 16,8, 0,0, 0,0, 0,0
for i=1 to 15
  read.next x,y
  heroGoRightMov$[1] += Chr$(Mod(256+x,256)) + Chr$(Mod(256+y,256))
next
read.data 1,1,3, 1,2,3, 1,3,3, 1,4,3, 1,5,3, 1,4,3, 1,3,3, 1,2,3, 1,1,3
for i=1 to 9
  read.next page,x,y
  heroActRightBmp$[1] += IntToStr$(heroTile[page,x,y])
  heroActLeftBmp$[1] += IntToStr$(heroTile[3-page,x,y])
next
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Level rendering: display the map layer by layer
!'---------------------------------------------------------------------------------------------------
!' Set the level
lvl=1

!' Initialize view
zoom=1
x0=0
y0=0
hero=1
frame=1

!' 1st layer: wallpaper
gr.bitmap.draw wallpaperPtr, wallpaperBmp[scenery[lvl]], 0, 0
gr.bitmap.size wallpaperBmp[scenery[lvl]], wpw, wph

!' 2nd layer: platforms
!'    - background platforms
gr.bitmap.create platformBmp, tilew*(mapw[lvl]+2), tileh*(maph[lvl]+2)
gr.bitmap.drawinto.start platformBmp
  for y=1 to maph[lvl]
    for x=1 to mapw[lvl]
      Gosub GetMapValue %' m <- mapdata$ @(x,y)
      if m=1 then gr.bitmap.draw nul, groundTile[scenery[lvl]], x*tilew, y*tileh
      if m=2 then gr.bitmap.draw nul, spikesTile[scenery[lvl]], x*tilew, y*tileh
      if m=9 then Gosub DisplayWater %' water needs to be displayed in reverse order
      if m=10 then gr.bitmap.draw nul, holeTile[scenery[lvl]], x*tilew, y*tileh
      if m=3 then %' hero is there
        hx=x
        hy=y
        herox=hx*tilew
        heroy=hy*tileh + herOffset[hero]
      end if
    next x
  next y
gr.bitmap.drawinto.end
gr.bitmap.draw platformPtr, platformBmp, x0, y0
!'    - ice sheet
gr.bitmap.create icesheetBmp, tilew*(mapw[lvl]+2), tileh*(maph[lvl]+2)
gr.bitmap.drawinto.start icesheetBmp
  for y=1 to maph[lvl]
    for x=1 to mapw[lvl]
      Gosub GetMapValue %' m <- mapdata$ @(x,y)
      if m=9 then gr.bitmap.draw nul, iceTile[scenery[lvl]], x*tilew, y*tileh
    next x
  next y
gr.bitmap.drawinto.end
gr.bitmap.draw icesheetPtr, icesheetBmp, x0, y0
gr.hide icesheetPtr
!'    - boxes and thermometers
gr.bitmap.create boxesBmp, tilew*(mapw[lvl]+2), tileh*(maph[lvl]+2)
gr.bitmap.drawinto.start boxesBmp
  for y=maph[lvl] to 1 step -1
    for x=1 to mapw[lvl]
      Gosub GetMapValue %' m <- mapdata$ @(x,y)
      if m=4 then gr.bitmap.draw nul, boxTile[scenery[lvl]], x*tilew, y*tileh
      if m=12 then gr.bitmap.draw nul, thermoTile[1], x*tilew, y*tileh
    next x
  next y
gr.bitmap.drawinto.end
gr.bitmap.draw boxesPtr, boxesBmp, x0, y0

!' 3rd layer: characters
e$=Mid$(heroIdleBmp$[hero], 2*frame-1, 2)
tmpBmp=GetIntFrom(e$)
gr.bitmap.draw heroPtr, tmpBmp, herox+x0, heroy+y0

!' 4th layer: foreground water curtain & elements of decorum
gr.bitmap.create fgndBmp, tilew*(mapw[lvl]+2), tileh*(maph[lvl]+2)
gr.bitmap.drawinto.start fgndBmp
  for y=1 to maph[lvl]
    for x=1 to mapw[lvl]
      Gosub GetMapValue %' m <- mapdata$ @(x,y)
      if m=9 then gr.bitmap.draw nul, waterfgnTile[scenery[lvl]], x*tilew, y*tileh
      if m=5 then gr.bitmap.draw nul, frontTile[scenery[lvl]], x*tilew, y*tileh
    next x
  next y
gr.bitmap.drawinto.end
gr.bitmap.draw fgndPtr, fgndBmp, x0, y0

!' 5th layer: mask, translation arrows, double-tap circles...
Call SetRGB(175,"000000",1)
gr.rect maskPtr,0,0,0,0
gr.modify maskPtr,"alpha",128
gr.hide maskPtr
dim arrowPtr[4]
for i=1 to 4
  gr.bitmap.draw arrowPtr[i], arrowBmp[i], 0, 0
  gr.hide arrowPtr[i]
next
gr.set.stroke 10
Call SetRGB(175,"777777",1)
gr.circle circlebPtr,0,0,0
gr.hide circlebPtr
Call SetRGB(175,"EEEEEE",0)
gr.circle circlewPtr,0,0,0
gr.hide circlewPtr
gr.set.stroke 0
Call SetRGB(255,"FFFFFF",1)
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Center the hero on screen before level starts
!'---------------------------------------------------------------------------------------------------
gr.screen real_w, real_h
di_w=real_w/zoom
di_h=di_w*real_h/real_w
scale_w=real_w/di_w
scale_h=real_h/di_h
tx=0.5*real_w
ty=0.5*real_h
x0=tx/scale_w-herox-tilew/2
y0=ty/scale_h-heroy-tileh/2
gr.modify platformPtr, "x", x0
gr.modify platformPtr, "y", y0
gr.modify icesheetPtr, "x", x0
gr.modify icesheetPtr, "y", y0
gr.modify boxesPtr, "x", x0
gr.modify boxesPtr, "y", y0
gr.modify heroPtr, "x", herox+x0
gr.modify heroPtr, "y", heroy+y0
gr.modify fgndPtr, "x", x0
gr.modify fgndPtr, "y", y0
real_w=0 %' to force screen orientation change
!'---------------------------------------------------------------------------------------------------

!'---------------------------------------------------------------------------------------------------
!' Main loop: animate (idle) character
!'---------------------------------------------------------------------------------------------------
do

  !' Detect screen orientation change
  gr.screen w, h
  if w<>real_w then %' screen was tilted
    real_w=w
    real_h=h
    di_w=real_w/zoom
    di_h=di_w*real_h/real_w
    scale_w=real_w/di_w
    scale_h=real_h/di_h
    if real_w>real_h then %' landscape orientation: resize wallpaper so that its width = 2x screen width
      wprw=2*real_w
      wprh=2*real_w*wph/wpw
      wprx0=0
      wpry0=(real_h-real_w*wph/wpw)/scale_w
    else                  %' portrait orientation: resize wallpaper so that its height = 2x screen height
      wprw=2*real_h*wpw/wph
      wprh=2*real_h
      wprx0=(real_w-real_h*wpw/wph)/scale_h
      wpry0=0
    end if
    if wallpaperResizedBmp then gr.bitmap.delete wallpaperResizedBmp
    gr.bitmap.scale wallpaperResizedBmp, wallpaperBmp[scenery[lvl]], wprw, wprh %' keep wallpaper's aspect ratio
    gr.modify wallpaperPtr, "bitmap", wallpaperResizedBmp
    gr.modify wallpaperPtr, "x", wprx0
    gr.modify wallpaperPtr, "y", wpry0
  end if

  !' Idle animation:
  aniTmr=Clock()
  frame++
  if 2*frame > Len(heroIdleBmp$[hero]) then frame=1
  e$=Mid$(heroIdleBmp$[hero], 2*frame-1, 2)
  tmpBmp=GetIntFrom(e$)
  gr.modify heroPtr, "bitmap", tmpBmp
  if falling then %' animate fall
    heroy+=16
    if m2=9 then heroy-=8 %' fall slower in water
    gr.modify heroPtr, "y", heroy+y0
  end if
  gr.render
  if !falling & mov$="" then Gosub DetectTouch
  aniTmr=60-Clock()+aniTmr
  if aniTmr>0 then Pause aniTmr
  if falling then %' detect end of fall
    if (m2=2 & falling=2) | (m2=9 & falling=11) then %' spikes, water
      falling=0
      dying=1
    elseif (m2=10 & falling=4) then %' hole
      falling=0
    elseif m2<>9 & falling=8 then %' full tile down
      falling=0
    else
      falling++
    end if
  end if

  !' Player died
  if dying then
    gr.modify maskPtr, "right", di_w
    gr.modify maskPtr, "bottom", di_h
    gr.show maskPtr
    for i=0 to 255 step 8
      aniTmr=Clock()
      gr.modify maskPtr,"alpha",i
      gr.render
      aniTmr=20-Clock()+aniTmr
      if aniTmr>0 then Pause aniTmr
    next
    END "GAME OVER"
  end if

  !' Player movement:
  if mov$<>"" then
    if hero=1 & (frame=1 | frame=9) then
      frame=0
      if mov$="right" then
        Gosub WhatsOnTheRight
        do
          aniTmr=Clock()
          frame++
          e$=Mid$(heroGoRightBmp$[hero], 2*frame-1, 2)
          tmpBmp=GetIntFrom(e$)
          gr.modify heroPtr, "bitmap", tmpBmp
          if !obstacle then      %' no obstacle = hero is free to go
            x=Ascii(Mid$(heroGoRightMov$[hero], 2*frame-1, 1))
            if x>128 then x-=256
            herox+=x
            if herox+x0>di_w-2*tilew then %' hero close to the right edge of the screen
              x0-=x
              gr.modify platformPtr, "x", x0
              gr.modify icesheetPtr, "x", x0
              gr.modify boxesPtr, "x", x0
              gr.modify fgndPtr, "x", x0
            end if
            gr.modify heroPtr, "x", herox+x0
          end if
          y=Ascii(Mid$(heroGoRightMov$[hero], 2*frame, 1))
          if y>128 then y-=256
          heroy+=y
          gr.modify heroPtr, "y", heroy+y0
          gr.render
          aniTmr=60-Clock()+aniTmr
          if aniTmr>0 then Pause aniTmr
          if frame=12 then %' hero has finished his movement
            if !obstacle then hx++
            gr.touch touched,ta,tb
            if !falling & touched & ta/scale_w>herox+x0+tilew then
              Gosub WhatsOnTheRight %' check if he can continue moving
              frame=3
            end if
          end if
        until frame=Len(heroGoRightBmp$[hero])/2
      elseif mov$="left" then
        Gosub WhatsOnTheLeft
        do
          aniTmr=Clock()
          frame++
          e$=Mid$(heroGoLeftBmp$[hero], 2*frame-1, 2)
          tmpBmp=GetIntFrom(e$)
          gr.modify heroPtr, "bitmap", tmpBmp
          if !obstacle then      %' no obstacle = hero is free to go
            x=Ascii(Mid$(heroGoRightMov$[hero], 2*frame-1, 1))
            if x>128 then x-=256
            herox-=x
            if herox+x0<tilew then %' hero close to the left edge of the screen
              x0+=x
              gr.modify platformPtr, "x", x0
              gr.modify icesheetPtr, "x", x0
              gr.modify boxesPtr, "x", x0
              gr.modify fgndPtr, "x", x0
            end if
            gr.modify heroPtr, "x", herox+x0
          end if
          y=Ascii(Mid$(heroGoRightMov$[hero], 2*frame, 1))
          if y>128 then y-=256
          heroy+=y
          gr.modify heroPtr, "y", heroy+y0
          gr.render
          aniTmr=60-Clock()+aniTmr
          if aniTmr>0 then Pause aniTmr
          if frame=12 then %' hero has finished his movement
            if !obstacle then hx--
            gr.touch touched,ta,tb
            if !falling & touched & ta/scale_w<herox+x0 then
              Gosub WhatsOnTheLeft %' check if he can continue moving
              frame=3
            end if
          end if
        until frame=Len(heroGoRightBmp$[hero])/2
      end if
      mov$=""
      frame=0
    end if %' end of character #1 movement
  end if

until 0
!'---------------------------------------------------------------------------------------------------

DetectTouch:
!'---------------------------------------------------------------------------------------------------
!' Detect player action on the screen
!'---------------------------------------------------------------------------------------------------
mov$=""
if !zooming & !dragging then
  gr.touch touched,ta,tb
  gr.touch2 touched2,ta2,tb2
  pause 20
end if
if touched & touched2 then %' 2 fingers on the screen
  gr.touch touched,tx,ty
  gr.touch2 touched2,tx2,ty2
  if touched & touched2 then %' pinch to zoom / unzoom
    zooming=1
    r=Sqr((tx2-tx)^2+(ty2-ty)^2) - Sqr((ta2-ta)^2+(tb2-tb)^2)
    if Abs(r) > 10 then %' zoom level has changed
      zoom*=Sqr((tx2-tx)^2+(ty2-ty)^2) / Sqr((ta2-ta)^2+(tb2-tb)^2)
      if zoom>4 then zoom=4
      if zoom<0.5 then zoom=0.5
      x0 -= 0.5*(ta+ta2)/scale_w
      y0 -= 0.5*(tb+tb2)/scale_h
      di_w=real_w/zoom
      di_h=di_w*real_h/real_w
      scale_w=real_w/di_w
      scale_h=real_h/di_h
      gr.scale scale_w, scale_h
      x0 += 0.5*(tx+tx2)/scale_w
      y0 += 0.5*(ty+ty2)/scale_h
      Gosub CheckX0Y0Limits
      Gosub DoTheDrag
      ta=tx
      tb=ty
      ta2=tx2
      tb2=ty2
      Return
    end if
  else %' 1 quick two-finger-tap followed by a release
    Gosub FinishedZooming
    Return
  end if
elseif touched then %' only 1 finger on the screen
  if dbltap=2 & Clock()-dbltapTmr<1000 ~
    & Abs(dtx-tx)<10 & Abs(dty-ty)<10 then   %' double-tap on the screen: center view on character
    x=x0
    y=y0
    tx=dtx/scale_w-herox-tilew/2-x0
    ty=dty/scale_h-heroy-tileh/2-y0
    gr.modify circlebPtr, "x", dtx/scale_w
    gr.modify circlebPtr, "y", dty/scale_h
    gr.show circlebPtr
    gr.modify circlewPtr, "x", dtx/scale_w
    gr.modify circlewPtr, "y", dty/scale_h
    gr.show circlewPtr
    for i=2 to 20 step 2
      circleTmr=Clock()
      if i<=10 then
        gr.modify circlebPtr, "radius", 0.5*tilew*log(i)/log(10)
        gr.modify circlewPtr, "radius", 0.5*tilew*log(i)/log(10)
      else
        gr.modify circlebPtr, "radius", 0.5*tilew*log(22-i)/log(10)
        gr.modify circlewPtr, "radius", 0.5*tilew*log(22-i)/log(10)
      end if
      x0=x+tx*log(i)/log(20)
      y0=y+ty*log(i)/log(20)
      Gosub DoTheDrag
      circleTmr=40-Clock()+circleTmr
      if circleTmr>0 then pause circleTmr
    next
    gr.hide circlebPtr
    gr.hide circlewPtr
    dtx=UNDEF
    dty=UNDEF
    Return
  else
    dbltap=1
    dtx=ta
    dty=tb
  end if
  gr.touch touched,tx,ty
  if touched then
    if dragging then %' currently dragging the view
      x0+=(tx-ta)/scale_w
      y0+=(ty-tb)/scale_h
      Gosub CheckX0Y0Limits
      Gosub DoTheDrag
      ta=tx
      tb=ty
      Return
    elseif tx-ta>20 then %' swipe right
      if herox+x0>-tilew & herox+x0<di_w-tilew ~
        & heroy+y0>-tileh & heroy+y0<di_h then %' hero is visible: move it to the right
        mov$="right"
      else
        Gosub ActivateDragging %' hero out of screen: drag view
      end if
      Return
    elseif tx-ta<-20 then %' swipe left
      if herox+x0>0 & herox+x0<di_w ~
        & heroy+y0>-tileh & heroy+y0<di_h then %' hero is visible: move it to the left
        mov$="left"
      else
        Gosub ActivateDragging %' hero out of screen: drag view
      end if
      Return
    elseif Abs(tx-ta)<10 & Abs(ty-tb)<10 then %' finger stays on the same spot
      longPress++
      if longPress=4 then
        longPress=0
        if tx/scale_w>herox+x0 & tx/scale_w<herox+x0+tilew ~
          & ty/scale_h>heroy+y0 & ty/scale_h<heroy+y0+tileh then %' long-press on character: do action
          gr.modify maskPtr, "right", di_w
          gr.modify maskPtr, "bottom", di_h
          gr.show maskPtr
          Gosub ShowActionArrows
          do
            gr.touch touched,tx,ty
          until !touched
          gr.hide maskPtr
          for i=1 to 4
            gr.hide arrowPtr[i]
          next
          gr.render
        else %' long-press on screen: drag view
          Gosub ActivateDragging
        end if
      end if
      Return
    end if
  else %' 1 quick tap followed by a release
    dbltap=0
    dragging=0
    for i=1 to 4
      gr.hide arrowPtr[i]
    next
  end if
else %' no touch on the screen
  if dbltap=1 then
    dbltapTmr=Clock()
    dbltap=2
  end if
  if dragging=1 then
    dbltap=0
    dragging=0
    for i=1 to 4
      gr.hide arrowPtr[i]
    next
  end if
  Gosub FinishedZooming
end if
!'---------------------------------------------------------------------------------------------------
Return

ActivateDragging:
dragging=1
for i=1 to 4
  gr.modify arrowPtr[i], "x", (tx+tilew*(0.75*arrowOffset[i]-1.5))/scale_w
  gr.modify arrowPtr[i], "y", (ty+tileh*(0.75*arrowOffset[Mod(2+i,4)+1]-1.5))/scale_h
  gr.show arrowPtr[i]
next
Return

FinishedZooming:
if zooming=1 then
  zooming=0
  if ozoom<>zoom then
    ozoom=zoom
    for i=1 to 4
      if arrowResizedBmp[i] then gr.bitmap.delete arrowResizedBmp[i]
      gr.bitmap.scale arrowResizedBmp[i], arrowBmp[i], 1.5*tilew/zoom, 1.5*tileh/zoom
      gr.modify arrowPtr[i], "bitmap", arrowResizedBmp[i]
    next
  end if
end if
Return

DoTheDrag:
gr.modify platformPtr, "x", x0
gr.modify platformPtr, "y", y0
gr.modify icesheetPtr, "x", x0
gr.modify icesheetPtr, "y", y0
gr.modify boxesPtr, "x", x0
gr.modify boxesPtr, "y", y0
gr.modify heroPtr, "x", herox+x0
gr.modify heroPtr, "y", heroy+y0
gr.modify fgndPtr, "x", x0
gr.modify fgndPtr, "y", y0
for a=1 to 4
  gr.modify arrowPtr[a], "x", (tx+tilew*(0.75*arrowOffset[a]-1.5))/scale_w
  gr.modify arrowPtr[a], "y", (ty+tileh*(0.75*arrowOffset[Mod(2+a,4)+1]-1.5))/scale_h
next
gr.render
Return

ShowActionArrows:
for a=1 to 4
  gr.modify arrowPtr[a], "x", (tx+tilew*(0.75*arrowOffset[a]-1.5))/scale_w
  gr.modify arrowPtr[a], "y", (ty+tileh*(0.75*arrowOffset[Mod(2+a,4)+1]-1.5))/scale_h
next
Gosub WhatsOnTheRight
if hero=1 & (m1=4 | m1=12) then
  gr.show arrowPtr[2] %' right arrow
end if
Gosub WhatsOnTheLeft
if hero=1 & (m1=4 | m1=12) then
  gr.show arrowPtr[4] %' left arrow
end if
gr.render
Return

CheckX0Y0Limits:
!' if x0<-tilew*(mapw[lvl]+2) then x0=-tilew*(mapw[lvl]+2)
!' if x0>2*tilew*(mapw[lvl]+2) then x0=2*tilew*(mapw[lvl]+2)
!' if y0<-tileh*(maph[lvl]+2) then y0=-tileh*(maph[lvl]+2)
!' if y0>2*tileh*(maph[lvl]+2) then y0=2*tileh*(maph[lvl]+2)
Return

GetMapValue:
if x<0 | y<0 | x>mapw[lvl] | y>maph[lvl] then
  m=0 %' out of map = no tile (= tile "nothing")
  Return
end if
nulw=Ceil(mapw[lvl]/2)
nul$=Mid$(mapdata$[lvl], nulw*(y-1) + Floor((x-1)/2) + 1, 1)
nula=Ascii(nul$)
if Mod(x,2)=1 then
  m=(nula-Mod(nula,16))/16
else
  m=Mod(nula,16)
end if
Return

DisplayWater:
w=x %' backup current position
i=w %' then go look up to what tile the water continues
for x=w+1 to mapw[lvl]
  Gosub GetMapValue %' m <- mapdata$ @(x,y)
  if m<>9 then %' no more water
    i=x-1 %' last water tile was @(i,y)
    f_n.break
  end if
next
if m=9 then i=mapw[lvl] %' special case where water continues to the very right of map
for x=i to w step -1 
  gr.bitmap.draw nul, waterbgnTile[scenery[lvl]], x*tilew, y*tileh
next
x=i %' restore position at last water tile
Return

WhatsOnTheRight:
x=hx+1
Goto WhatsThere
WhatsOnTheLeft:
x=hx-1
WhatsThere:
y=hy %' look what tile there is where the hero wants to go
Gosub GetMapValue %' m <- mapdata$ @(x,y)
m1=m
if mov$="" then Return
y=hy+1 %' look what ground there is under where the hero wants to go
Gosub GetMapValue %' m <- mapdata$ @(x,y)
m2=m
x=hx %' look what ground there is currently under the hero
Gosub GetMapValue %' m <- mapdata$ @(x,y)
obstacle=0
falling=0
if hero=1 then
  if (m=10 & m2<>10) | m1=1 | m1=2 | m1=4 | m1=9 | m1=10 | m2=0 then
    obstacle=1
    Return
  end if
  if m2=2 | m2=3 | m2>4 then falling=1
end if
Return

OnError:
e$=geterror$()
gr.close
if e$<>"" then print e$
END ""

OnBackKey:
END "Bye bye!"
