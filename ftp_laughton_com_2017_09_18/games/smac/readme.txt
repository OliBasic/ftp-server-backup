SMAC is short for Show My Analog Clock.
It's not much of game.
You just have to go through some action to see the clock.
A ball can be controlled by tilting your device.
Six coloured squares have to be removed. The square ready
to be removed will turn red.
When the ball hits the red square, the square will disappear.
After removing all six squares you'll see the clock.....

For me the fun was drawing the clock !

You may have to fiddle with the scaling if your aspect ratio is not 3:4 !
