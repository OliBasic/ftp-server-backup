! SMAC
! Aat Don @2013
SENSORS.LIST List$[]
ARRAY.LENGTH Size, List$[]
SF=0
FOR i = 1 TO Size
	IF IS_IN("Type = 1",List$[i]) THEN SF=1
NEXT i
! Orientation sensor
IF SF=1 THEN
	SENSORS.OPEN 1
ELSE
	PRINT "Acceleration sensor NOT found"
	END
ENDIF
GR.OPEN 255,246,151,69,0,1
GR.FRONT 0
PRINT "***SMAC***"
PRINT "At start you will see"
PRINT "six coloured squares"
PRINT "and a white ball."
PRINT "Move the ball against"
PRINT "the coloured square"
PRINT "which has become red."
PRINT "The square will disappear."
PRINT "When all squares are gone"
PRINT "the background will be revealed...."
AUDIO.LOAD song, "sound.mp3"
GR.SCREEN w,h
sx=w/720
sy=h/960
GR.SCALE sx,sy
cX=360
cY=480
pi=3.1415
! Circle radius
OuterRing=300
Ring1Rail=280
Ring2Rail=245
Ring1Numbers=225
Ring2Numbers=165
InnerRing=150
Center=35
ARRAY.LOAD Hours$[],"III","IV","V","VI","VII","VIII","IX","X","XI","XII","I","II"
ARRAY.LOAD mh[],0,20,10,0,25,8,40,10,380,15,380,25,40,30,25,32,10,40
ARRAY.LOAD hh[],0,25,15,5,32,15,116,20,131,10,141,20,157,20,152,10,158,0,166,0,189,20,199,20,204,18,210,25,204,32,199,30,189,30,166,50,158,50,152,40,157,30,141,30,131,40,116,30,32,35,15,45
DIM MinHand[60]
DIM HourHand[60]
DIM gx[8]
DIM Cover[6]
DIM Order[6]
FOR i=1 TO 6
	Order[i]=i
NEXT i
ARRAY.SHUFFLE Order[]
gr.bitmap.load fuss_ptr, "fussb.png"
! Draw clock face
GR.COLOR 255,90,90,90,1
GR.RECT g,cX-(OuterRing-7),cY-(OuterRing-7),cX+(OuterRing-7),cY+(OuterRing-7)
GR.SET.STROKE 5
GR.COLOR 255,221,210,180,0
GR.RECT g,cX-(OuterRing-7),cY-(OuterRing-7),cX+(OuterRing-7),cY+(OuterRing-7)
GR.CIRCLE c,cX,cY,OuterRing+30
GR.SET.STROKE 40
GR.COLOR 255,60,60,160,0
GR.RECT g,cX-(OuterRing+20),cY-(OuterRing+20),cX+(OuterRing+20),cY+(OuterRing+20)
GR.SET.STROKE 10
GR.COLOR 255,60,60,60,0
GR.RECT g,cX-(OuterRing),cY-(OuterRing),cX+(OuterRing),cY+(OuterRing)
GR.CIRCLE c,cX,cY,OuterRing+20
GR.SET.STROKE 20
GR.COLOR 255,221,210,180,0
GR.RECT g,cX-(OuterRing+10),cY-(OuterRing+10),cX+(OuterRing+10),cY+(OuterRing+10)
GR.CIRCLE c,cX,cY,OuterRing+10
GR.SET.STROKE 5
l=OuterRing+65
FOR angle=45 TO 315 STEP 90
	x=l*COS(angle*pi/180)+cX
	y=l*SIN(angle*pi/180)+cY
	GR.COLOR 255,221,210,180,0
	GR.CIRCLE g,x,y,33
	GR.COLOR 255,221,210,180,1
	GR.CIRCLE g,x,y,15
	IF angle=45 | angle=225 THEN
		x=l*COS((angle+10)*pi/180)+cX
		y=l*SIN((angle+4)*pi/180)+cY
		GR.CIRCLE g,x,y,12
		x=l*COS((angle-4)*pi/180)+cX
		y=l*SIN((angle-10)*pi/180)+cY
		GR.CIRCLE g,x,y,12
	ELSE
		x=l*COS((angle+4)*pi/180)+cX
		y=l*SIN((angle+10)*pi/180)+cY
		GR.CIRCLE g,x,y,12
		x=l*COS((angle-10)*pi/180)+cX
		y=l*SIN((angle-4)*pi/180)+cY
		GR.CIRCLE g,x,y,12
	ENDIF
NEXT angle
GR.TEXT.TYPEFACE 4
GR.TEXT.SIZE 30
GR.COLOR 255,64,64,64,1
GR.RECT g,20,cY+OuterRing+40,700,cY+OuterRing+85
GR.COLOR 255,221,210,180,0
GR.TEXT.DRAW g,30,cY+OuterRing+75,":: :::: r.f.o.-.B.A.S.I.C.!. .i.s. .a.w.e.s.o.m.e.:::: ::"
GR.TEXT.TYPEFACE 1
GR.SET.STROKE 10
GR.COLOR 255,225,225,225,1
GR.CIRCLE c,cX,cY,OuterRing
GR.COLOR 255,60,60,60,0
GR.CIRCLE c,cX,cY,OuterRing
GR.SET.STROKE 5
GR.CIRCLE c,cX,cY,Ring1Rail
GR.CIRCLE c,cX,cY,Ring2Rail
GR.SET.STROKE 7
GR.CIRCLE c,cX,cY,Ring1Numbers
GR.SET.STROKE 4
GR.CIRCLE c,cX,cY,Ring2Numbers
GR.SET.STROKE 7
GR.CIRCLE c,cX,cY,InnerRing
! Rails
l1=Ring1Rail+8
l2=Ring2Rail-8
GR.COLOR 255,60,60,60,1
GR.SET.STROKE 5
FOR angle=0 TO 354 STEP 6
	x1=l1*COS(angle*pi/180)+cX
	y1=l1*SIN(angle*pi/180)+cY
	x2=l2*COS(angle*pi/180)+cX
	y2=l2*SIN(angle*pi/180)+cY
	GR.LINE g,x1,y1,x2,y2
NEXT angle
! Thin lines
l1=Ring1Numbers
l2=Ring2Numbers
GR.SET.STROKE 1
GR.COLOR 255,63,63,63,1
FOR angle=0 TO 352 STEP 7.5
	x1=l1*COS(angle*pi/180)+cX
	y1=l1*SIN(angle*pi/180)+cY
	x2=l2*COS(angle*pi/180)+cX
	y2=l2*SIN(angle*pi/180)+cY
	GR.LINE g,x1,y1,x2,y2
NEXT angle
! Decoration
GR.SET.STROKE 5
GR.COLOR 255,60,60,60,1
l1=Ring1Rail+10
l2=Ring2Rail+17.5
l3=Ring2Rail-10
l4=Ring2Numbers-7
FOR angle=0 TO 330 STEP 30
	x1=l1*COS(angle*pi/180)+cX
	y1=l1*SIN(angle*pi/180)+cY
	x2=l2*COS(angle*pi/180)+cX
	y2=l2*SIN(angle*pi/180)+cY
	x3=l3*COS(angle*pi/180)+cX
	y3=l3*SIN(angle*pi/180)+cY
	x4=l4*COS(angle*pi/180)+cX
	y4=l4*SIN(angle*pi/180)+cY
	GR.Circle g,x1,y1,7
	GR.Circle g,x2,y2,7
	GR.Circle g,x3,y3,7
	GR.Circle g,x4,y4,7
	x1=l2*COS((angle-2.5)*pi/180)+cX
	y1=l2*SIN((angle-2.5)*pi/180)+cY
	x2=l2*COS((angle+2.5)*pi/180)+cX
	y2=l2*SIN((angle+2.5)*pi/180)+cY
	GR.LINE g,x1,y1,x2,y2
	GR.Circle g,x1,y1,6
	GR.Circle g,x2,y2,6
NEXT angle
! Numbers
GR.TEXT.BOLD 1
GR.TEXT.SIZE 60
l=Ring2Numbers+8
FOR angle=0 TO 330 STEP 30
	! use radians for position
	x=l*COS(angle*pi/180)+cX
	y=l*SIN(angle*pi/180)+cY
	! use degrees for rotation
	GR.TEXT.WIDTH Q,Hours$[angle/30+1]
	GR.ROTATE.START angle+90,x,y
	GR.TEXT.DRAW g,x-Q/2,y,Hours$[angle/30+1]
	GR.ROTATE.END
NEXT angle
! Pattern
GR.COLOR 255,153,101,21,0
GR.SET.STROKE 2
l=InnerRing
! use radians for position
x1=l*COS(0*pi/180)+cX
y1=l*SIN(0*pi/180)+cY-25
x2=l*COS(180*pi/180)+cX
y2=l*SIN(180*pi/180)+cY+25
FOR angle=0 TO 165 STEP 15
	! use degrees for rotation
	GR.ROTATE.START angle,cX,cY
	GR.OVAL g,x1,y1,x2,y2
	GR.ROTATE.END
NEXT angle
l=Center+10
FOR angle=0 TO 330 STEP 30
	! use radians for position
	x=l*COS(angle*pi/180)+cX
	y=l*SIN(angle*pi/180)+cY
	GR.COLOR 255,225,225,225,1
	GR.CIRCLE g,x,y,10
	GR.COLOR 255,153,101,21,0
	GR.CIRCLE g,x,y,10
NEXT angle
! Center
GR.SET.STROKE 4
GR.COLOR 255,0,0,0,1
GR.CIRCLE c,cX,cY,Center
! Draw minute hand 60x
GR.COLOR 255,0,0,0,1
GR.BITMAP.CREATE minuut,OuterRing+80,40
GR.BITMAP.DRAWINTO.START minuut
LIST.CREATE n, List1
LIST.ADD.ARRAY List1, mh[]
GR.SET.STROKE 1
GR.POLY pt,List1,0,0
GR.BITMAP.DRAWINTO.END
FOR angle=0 TO 354 STEP 6
	GR.ROTATE.START angle,cX,cY
	IF angle<=270 THEN
		GR.BITMAP.DRAW MinHand[angle/6+15],minuut,cX-80,cY-20
		GR.HIDE MinHand[angle/6+15]
	ELSE
		GR.BITMAP.DRAW MinHand[angle/6-45],minuut,cX-80,cY-20
		GR.HIDE MinHand[angle/6-45]
	ENDIF
	GR.ROTATE.END
NEXT angle
! Draw hour hand 60x
GR.COLOR 255,0,0,0,1
GR.BITMAP.CREATE uur,InnerRing+60,50
GR.BITMAP.DRAWINTO.START uur
LIST.CREATE n, List2
LIST.ADD.ARRAY List2, hh[]
GR.SET.STROKE 1
GR.POLY pt,List2,0,0
! axis
GR.COLOR 255,60,60,60,1
GR.CIRCLE c,60,25,Center-25
GR.BITMAP.DRAWINTO.END
FOR angle=0 TO 354 STEP 6
	GR.ROTATE.START angle,cX,cY
	IF angle<=270 THEN
		GR.BITMAP.DRAW HourHand[angle/6+15],uur,cX-60,cY-25
		GR.HIDE HourHand[angle/6+15]
	ELSE
		GR.BITMAP.DRAW HourHand[angle/6-45],uur,cX-60,cY-25
		GR.HIDE HourHand[angle/6-45]
	ENDIF
	GR.ROTATE.END
NEXT angle
PAUSE 3000
! BackGround
GR.COLOR 255,0,0,0,1
GR.RECT bg,0,125,720,875
GOSUB GetTime
CLS
GR.FRONT 1
GOSUB SetTime
! DRAW RECTANGLES
GR.COLOR 255,200,200,200,1
GR.RECT Cover[1],155,225,205,275
GR.COLOR 255,0,255,0,1
GR.RECT Cover[2],515,225,565,275
GR.COLOR 255,0,0,255,1
GR.RECT Cover[3],155,475,205,525
GR.COLOR 255,255,255,0,1
GR.RECT Cover[4],515,475,565,525
GR.COLOR 255,255,0,255,1
GR.RECT Cover[5],155,725,205,775
GR.COLOR 255,0,255,255,1
GR.RECT Cover[6],515,725,565,775
ballx=cX
bally=cY
accAmplify=30
f=0.8
gr.bitmap.draw ball, fuss_ptr, ballx , bally
GR.COLOR 255,255,0,0,1
GR.PAINT.GET Paint1
PAUSE 1000
FOR i=1 TO 6
	PAUSE 500
	GR.MODIFY Cover[Order[i]],"paint",Paint1
	PAUSE 500
	GR.RENDER
	Hit=0
	DO
		SENSORS.READ 1,accx,accy,accz
		ballxri=(ballxri+accx*accAmplify)*f
		ballyri=(ballyri-accy*accAmplify)*f
		gr.modify ball,"x",ballx
		gr.modify ball,"y",bally
		GR.RENDER
		bally=bally-ballyri
		ballx=ballx-ballxri
		if ballx<=0 | ballx>=648 then
			if ballx<0 then ballx=0
			if ballx>648 then ballx=648
			ballxri=-ballxri
		endif
		if bally<=0 | bally>=888 then
			if bally<0 then bally=0
			if bally>888 then bally=888
			ballyri=-ballyri
		endif

!!		
		! left
		IF accx<0 & XPos<700 THEN
			XPos=Xpos+20
		ENDIF
		! right
		IF accx>0 & XPOS>20 THEN
			XPos=Xpos-20
		ENDIF
		! up
		IF accy<0 & YPos>20 THEN
			YPos=Ypos-20
		ENDIF
		! down
		IF accy>0 & YPOS<940 THEN
			YPos=Ypos+20
		ENDIF
		GR.MODIFY ball,"x",XPos
		GR.MODIFY ball,"y",YPos
!!
		Hit=GR_COLLISION (Cover[Order[i]],ball)
		GR.RENDER
	UNTIL Hit<>0
	TONE 700,200
	GR.HIDE Cover[Order[i]]
	GR.MODIFY bg,"alpha",255-i*5
NEXT i
SENSORS.CLOSE
TIMER.SET 1000
GR.HIDE ball
AUDIO.PLAY song
FOR i=225 TO 0 STEP -25
	GR.MODIFY bg,"alpha",i
	PAUSE 250
NEXT i
GR.COLOR 255,0,0,0,0
GR.SET.STROKE 2
GR.TEXT.DRAW g,100,50,"Tap screen to exit"
GR.RENDER
DO
	GR.TOUCH touched,x,y
UNTIL touched
GR.CLOSE
EXIT

GetTime:
	TIME Year$,Month$,Day$,Hour$,Minute$,Second$
	StHr=ROUND(MOD(VAL(Hour$),12))
	IF StHr=0 THEN StHr=12
	StMin=ROUND(VAL(Minute$))
	! Shift hour hand every 12 min
	StHr=ROUND(StHr*5+FLOOR(StMin/12))
	IF StHr>60 THEN StHr=StHr-60
	IF StMin=0 THEN StMin=60
RETURN

SetTime:
GR.HIDE HourHand[StHr]
GR.HIDE MinHand[StMin]
GOSUB GetTime
GR.SHOW HourHand[StHr]
GR.SHOW MinHand[StMin]
GR.RENDER
RETURN

ONTIMER:
	GOSUB SetTime
TIMER.RESUME
