! Drawing of BigBen
! Aat Don @2013
GR.OPEN 255,246,151,69,0,1
AUDIO.LOAD song, "../../SMAC/data/sound.mp3"
GR.SCREEN w,h
ScaleX=720
ScaleY=h/w*ScaleX
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
cX=360
cY=480
pi=3.1415
WAKELOCK 3
! Circle radius
OuterRing=300
Ring1Rail=280
Ring2Rail=245
Ring1Numbers=225
Ring2Numbers=165
InnerRing=150
Center=35
ARRAY.LOAD Hours$[],"III","IV","V","VI","VII","VIII","IX","X","XI","XII","I","II"
ARRAY.LOAD mh[],0,20,10,0,25,8,40,10,380,15,380,25,40,30,25,32,10,40
ARRAY.LOAD hh[],0,25,15,5,32,15,116,20,131,10,141,20,157,20,152,10,158,0,166,0,189,20,199,20,204,18,210,25,204,32,199,30,189,30,166,50,158,50,152,40,157,30,141,30,131,40,116,30,32,35,15,45
DIM MinHand[60]
DIM HourHand[60]
DIM gx[8]
DIM Cover[6]
DIM Order[6]
FOR i=1 TO 6
	Order[i]=i
NEXT i
ARRAY.SHUFFLE Order[]
! Draw clock face
GR.COLOR 255,90,90,90,1
GR.RECT g,cX-(OuterRing-7),cY-(OuterRing-7),cX+(OuterRing-7),cY+(OuterRing-7)
GR.RENDER
GR.SET.STROKE 5
GR.COLOR 255,221,210,180,0
GR.RECT g,cX-(OuterRing-7),cY-(OuterRing-7),cX+(OuterRing-7),cY+(OuterRing-7)
GR.RENDER
GR.CIRCLE c,cX,cY,OuterRing+30
GR.SET.STROKE 40
GR.COLOR 255,60,60,160,0
GR.RECT g,cX-(OuterRing+20),cY-(OuterRing+20),cX+(OuterRing+20),cY+(OuterRing+20)
GR.RENDER
GR.SET.STROKE 10
GR.COLOR 255,60,60,60,0
GR.RECT g,cX-(OuterRing),cY-(OuterRing),cX+(OuterRing),cY+(OuterRing)
GR.RENDER
GR.CIRCLE c,cX,cY,OuterRing+20
GR.RENDER
GR.SET.STROKE 20
GR.COLOR 255,221,210,180,0
GR.RECT g,cX-(OuterRing+10),cY-(OuterRing+10),cX+(OuterRing+10),cY+(OuterRing+10)
GR.RENDER
GR.CIRCLE c,cX,cY,OuterRing+10
GR.RENDER
GR.SET.STROKE 5
l=OuterRing+65
FOR angle=45 TO 315 STEP 90
	x=l*COS(angle*pi/180)+cX
	y=l*SIN(angle*pi/180)+cY
	GR.COLOR 255,221,210,180,0
	GR.CIRCLE g,x,y,33
	GR.COLOR 255,221,210,180,1
	GR.CIRCLE g,x,y,15
  GR.RENDER
	IF angle=45 | angle=225 THEN
		x=l*COS((angle+10)*pi/180)+cX
		y=l*SIN((angle+4)*pi/180)+cY
		GR.CIRCLE g,x,y,12
  GR.RENDER
		x=l*COS((angle-4)*pi/180)+cX
		y=l*SIN((angle-10)*pi/180)+cY
		GR.CIRCLE g,x,y,12
  GR.RENDER
	ELSE
		x=l*COS((angle+4)*pi/180)+cX
		y=l*SIN((angle+10)*pi/180)+cY
		GR.CIRCLE g,x,y,12
  GR.RENDER
		x=l*COS((angle-10)*pi/180)+cX
		y=l*SIN((angle-4)*pi/180)+cY
		GR.CIRCLE g,x,y,12
  GR.RENDER
	ENDIF
NEXT angle
GR.TEXT.TYPEFACE 4
GR.TEXT.SIZE 30
GR.COLOR 255,64,64,64,1
GR.RECT g,20,cY+OuterRing+40,700,cY+OuterRing+85
GR.RENDER
GR.COLOR 255,221,210,180,0
GR.TEXT.DRAW g,30,cY+OuterRing+75,":: :::: r.f.o.-.B.A.S.I.C.!. .i.s. .a.w.e.s.o.m.e.:::: ::"
GR.RENDER
GR.TEXT.TYPEFACE 1
GR.SET.STROKE 10
GR.COLOR 255,225,225,225,1
GR.CIRCLE c,cX,cY,OuterRing
GR.RENDER
GR.COLOR 255,60,60,60,0
GR.CIRCLE c,cX,cY,OuterRing
GR.RENDER
GR.SET.STROKE 5
GR.CIRCLE c,cX,cY,Ring1Rail
GR.RENDER
GR.CIRCLE c,cX,cY,Ring2Rail
GR.RENDER
GR.SET.STROKE 7
GR.CIRCLE c,cX,cY,Ring1Numbers
GR.RENDER
GR.SET.STROKE 4
GR.CIRCLE c,cX,cY,Ring2Numbers
GR.RENDER
GR.SET.STROKE 7
GR.CIRCLE c,cX,cY,InnerRing
GR.RENDER
! Rails
l1=Ring1Rail+8
l2=Ring2Rail-8
GR.COLOR 255,60,60,60,1
GR.SET.STROKE 5
FOR angle=0 TO 354 STEP 6
	x1=l1*COS(angle*pi/180)+cX
	y1=l1*SIN(angle*pi/180)+cY
	x2=l2*COS(angle*pi/180)+cX
	y2=l2*SIN(angle*pi/180)+cY
	GR.LINE g,x1,y1,x2,y2
  GR.RENDER
NEXT angle
! Thin lines
l1=Ring1Numbers
l2=Ring2Numbers
GR.SET.STROKE 1
GR.COLOR 255,63,63,63,1
FOR angle=0 TO 352 STEP 7.5
	x1=l1*COS(angle*pi/180)+cX
	y1=l1*SIN(angle*pi/180)+cY
	x2=l2*COS(angle*pi/180)+cX
	y2=l2*SIN(angle*pi/180)+cY
	GR.LINE g,x1,y1,x2,y2
  GR.RENDER
NEXT angle
! Decoration
GR.SET.STROKE 5
GR.COLOR 255,60,60,60,1
l1=Ring1Rail+10
l2=Ring2Rail+17.5
l3=Ring2Rail-10
l4=Ring2Numbers-7
FOR angle=0 TO 330 STEP 30
	x1=l1*COS(angle*pi/180)+cX
	y1=l1*SIN(angle*pi/180)+cY
	x2=l2*COS(angle*pi/180)+cX
	y2=l2*SIN(angle*pi/180)+cY
	x3=l3*COS(angle*pi/180)+cX
	y3=l3*SIN(angle*pi/180)+cY
	x4=l4*COS(angle*pi/180)+cX
	y4=l4*SIN(angle*pi/180)+cY
	GR.Circle g,x1,y1,7
  GR.RENDER
	GR.Circle g,x2,y2,7
  GR.RENDER
	GR.Circle g,x3,y3,7
  GR.RENDER
	GR.Circle g,x4,y4,7
  GR.RENDER
	x1=l2*COS((angle-2.5)*pi/180)+cX
	y1=l2*SIN((angle-2.5)*pi/180)+cY
	x2=l2*COS((angle+2.5)*pi/180)+cX
	y2=l2*SIN((angle+2.5)*pi/180)+cY
	GR.LINE g,x1,y1,x2,y2
  GR.RENDER
	GR.Circle g,x1,y1,6
  GR.RENDER
	GR.Circle g,x2,y2,6
  GR.RENDER
NEXT angle
! Numbers
GR.TEXT.BOLD 1
GR.TEXT.SIZE 60
l=Ring2Numbers+8
FOR angle=0 TO 330 STEP 30
	! use radians for position
	x=l*COS(angle*pi/180)+cX
	y=l*SIN(angle*pi/180)+cY
	! use degrees for rotation
	GR.TEXT.WIDTH Q,Hours$[angle/30+1]
	GR.ROTATE.START angle+90,x,y
	GR.TEXT.DRAW g,x-Q/2,y,Hours$[angle/30+1]
	GR.ROTATE.END
  GR.RENDER
NEXT angle
! Pattern
GR.COLOR 255,153,101,21,0
GR.SET.STROKE 2
l=InnerRing
! use radians for position
x1=l*COS(0*pi/180)+cX
y1=l*SIN(0*pi/180)+cY-25
x2=l*COS(180*pi/180)+cX
y2=l*SIN(180*pi/180)+cY+25
FOR angle=0 TO 165 STEP 15
	! use degrees for rotation
	GR.ROTATE.START angle,cX,cY
	GR.OVAL g,x1,y1,x2,y2
	GR.ROTATE.END
  GR.RENDER
NEXT angle
l=Center+10
FOR angle=0 TO 330 STEP 30
	! use radians for position
	x=l*COS(angle*pi/180)+cX
	y=l*SIN(angle*pi/180)+cY
	GR.COLOR 255,225,225,225,1
	GR.CIRCLE g,x,y,10
  GR.RENDER
	GR.COLOR 255,153,101,21,0
	GR.CIRCLE g,x,y,10
  GR.RENDER
NEXT angle
! Center
GR.SET.STROKE 4
GR.COLOR 255,0,0,0,1
GR.CIRCLE c,cX,cY,Center
GR.RENDER
! Draw minute hand 60x
GR.COLOR 255,0,0,0,1
GR.BITMAP.CREATE minuut,OuterRing+80,40
GR.BITMAP.DRAWINTO.START minuut
LIST.CREATE n, List1
LIST.ADD.ARRAY List1, mh[]
GR.SET.STROKE 1
GR.POLY pt,List1,0,0
GR.BITMAP.DRAWINTO.END
FOR angle=0 TO 354 STEP 6
	GR.ROTATE.START angle,cX,cY
	IF angle<=270 THEN
		GR.BITMAP.DRAW MinHand[angle/6+15],minuut,cX-80,cY-20
    GR.RENDER
		GR.HIDE MinHand[angle/6+15]
	ELSE
		GR.BITMAP.DRAW MinHand[angle/6-45],minuut,cX-80,cY-20
    GR.RENDER
		GR.HIDE MinHand[angle/6-45]
	ENDIF
	GR.ROTATE.END
NEXT angle
! Draw hour hand 60x
GR.COLOR 255,0,0,0,1
GR.BITMAP.CREATE uur,InnerRing+60,50
GR.BITMAP.DRAWINTO.START uur
LIST.CREATE n, List2
LIST.ADD.ARRAY List2, hh[]
GR.SET.STROKE 1
GR.POLY pt,List2,0,0
! axis
GR.COLOR 255,60,60,60,1
GR.CIRCLE c,60,25,Center-25
GR.BITMAP.DRAWINTO.END
FOR angle=0 TO 354 STEP 6
	GR.ROTATE.START angle,cX,cY
	IF angle<=270 THEN
		GR.BITMAP.DRAW HourHand[angle/6+15],uur,cX-60,cY-25
    GR.RENDER
		GR.HIDE HourHand[angle/6+15]
	ELSE
		GR.BITMAP.DRAW HourHand[angle/6-45],uur,cX-60,cY-25
    GR.RENDER
		GR.HIDE HourHand[angle/6-45]
	ENDIF
	GR.ROTATE.END
NEXT angle
GOSUB GetTime
TIMER.SET 1000
GR.COLOR 255,0,0,0,0
GR.SET.STROKE 2
GR.TEXT.DRAW g,100,50,"Tap screen to exit"
GR.RENDER
PAUSE 3000
AUDIO.PLAY song
DO
	GR.TOUCH touched,x,y
UNTIL touched
GR.CLOSE
WAKELOCK 5
END

GetTime:
	TIME Year$,Month$,Day$,Hour$,Minute$,Second$
	StHr=ROUND(MOD(VAL(Hour$),12))
	IF StHr=0 THEN StHr=12
	StMin=ROUND(VAL(Minute$))
	! Shift hour hand every 12 min
	StHr=ROUND(StHr*5+FLOOR(StMin/12))
	IF StHr>60 THEN StHr=StHr-60
	IF StMin=0 THEN StMin=60
RETURN

ONTIMER:
! Set time
GR.HIDE HourHand[StHr]
GR.HIDE MinHand[StMin]
GOSUB GetTime
GR.SHOW HourHand[StHr]
GR.SHOW MinHand[StMin]
GR.RENDER
TIMER.RESUME
