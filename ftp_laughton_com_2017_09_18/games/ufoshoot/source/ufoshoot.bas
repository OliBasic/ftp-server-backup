! ufo game 
FN.DEF buttons$()
 GR.SCREEN width, height
 whalf = FLOOR(width/2)
 hhalf = FLOOR(height/2)
 DIM pad[2]
 ARRAY.LOAD b$[],"play again","quit"
 sq=height/10
 canshoot=1

 FOR i=1 TO 2
  GR.TEXT.DRAW pad[i],whalf,hhalf+hhalf*i*0.3, b$[i]
 NEXT i
 GR.RENDER
 found=0
 DO
  FOR i=1 TO 2
   GR.GET.POSITION pad[i],sx,sy
   GR.BOUNDED.TOUCH touched, sx-sq, sy-sq, sx+sq, sy+sq
   IF touched
    LET found=1
    F_N.BREAK
   ENDIF
  NEXT I
  PAUSE 50
 UNTIL found
 FN.RTN b$[i]
FN.END


ARRAY.LOAD Response$[], "1 - slow","2","3","4","5","6","7- fast"
Nbox=4

DIM bx[nbox],by[nbox]   % box coords
DIM vx[nbox],vy[nbox]   % box velocities
DIM bh[nbox],bw[nbox]   % box dimensions
DIM alien[nbox]           % box objects
Games=0
DIM tbmp[nbox]


Playagain:

shots=0
score=0
Cantouch=0
canshoot=1
maxshots=6
Games++

Msg$="select game speed"

DIALOG.SELECT rnum, response$[], msg$

CLS
GR.OPEN 255, 0, 0, 0
PAUSE 500
GR.ORIENTATION 1
GR.SET.ANTIALIAS 0
GR.SCREEN awidth, aheight
Width=awidth
Height=aheight
S= (3*rnum)*(width/480)



Samp=1

whalf = FLOOR(width/2)
hhalf = FLOOR(height/2)
wqtr = FLOOR(width/4)
hqtr = FLOOR(height/4)
hthird= FLOOR(height/3)
wthird=FLOOR(width/3)

sq=width/16
msq=width/32




vx[1]=s
vy[1]=s
bx[1]=sq
by[1]=sq*2
bw[1]=sq
bh[1]=sq

vx[2]=2*s
vy[2]=-s/2
bx[2]=0
by[2]=sq*4
bw[2]=sq
bh[2]=sq

vx[3]=s/3
vy[3]=-s/3
bx[3]=0
by[3]=sq*3
bw[3]=sq
bh[3]=sq

vx[4]=-s/2
vy[4]=-s/4
bx[4]=0
by[4]=sq*5
bw[4]=sq*2
bh[4]=sq

asize =FLOOR(w/10)
FOR i=1 TO nbox
 GR.BITMAP.LOAD tbmp[i], "ufo.png"
 GR.BITMAP.DRAW alien[i],tbmp[i],bx[i],by[i]
NEXT i
FOR i= 1 TO nbox
 GR.SHOW alien[i]
NEXT i

GR.SET.STROKE 0


GR.TEXT.TYPEFACE 2
GR.COLOR 255,200,200,00,1
GR.TEXT.SIZE 28
GR.TEXT.BOLD 1
GR.TEXT.DRAW info,30,35,INT$(maxshots)


mx=width/8
my=7*height/8


! red missile
GR.COLOR 255,255,0,0,1
GR.CIRCLE missile , mx, my, msq
GR.HIDE missile

GR.COLOR 0,200,0,0


GR.RENDER

tstart=CLOCK()

Cantouch=1
tic = CLOCK()
!------------------------------------------
DO

 ! process aliens---------------
 FOR i=1 TO nbox

  GR.GET.POSITION alien[i],fx,fy
  IF fx >width THEN GR.MODIFY alien[i],"x",1
  IF fy<1 THEN GR.MODIFY alien[i],"y",height
  IF fy > height THEN GR.MODIFY alien[i],"y",1
  IF fx<1 THEN GR.MODIFY alien[i],"x",width

  GR.MOVE alien[i], vx[I], vy[I]

  IF GR_COLLISION(missile,alien[i])
   GR.HIDE alien[i]
   score++
  ENDIF 
 NEXT i

 ! move missile

 GR.GET.POSITION missile,mx,my

 IF mx >width | my < 0
  canshoot=1
  GR.HIDE missile
  mx = sq
  my = 7*height/8
  GR.MODIFY missile,"x",mx
  GR.MODIFY missile,"y",my
 ENDIF 

 IF canshoot=0 THEN  GR.MOVE missile,mvx,mvy

 GR.RENDER

 !process touch ---------------------
 GR.TOUCH touched,x,y

 IF touched & canshoot
  shots++
  GR.MODIFY info,"text",INT$(maxshots-shots)
  canshoot =0
  GR.SHOW missile
  IF x < width/3
   mvx=s/3
   mvy=-s
  ENDIF
  IF x >width/3  & x < 2*width /3
   mvx = s/2
   mvy = -s
  ENDIF
  IF x >width/2
   mvx = s
   mvy = -s
  ENDIF 
 ENDIF 

 GR.RENDER

 !timing ---------------
 tocfilt += (toc-tocfilt)*0.025
 LET toc = CLOCK()-tic
 PAUSE max (33 - toc,1)
 LET tic = CLOCK()

UNTIL (shots>=maxshots | score>=nbox) & canshoot
!-------------------------------------


GR.SET.ANTIALIAS 1
GR.COLOR 255, 0, 255,0,1  % green text
GR.TEXT.SIZE 30
GR.TEXT.ALIGN 2
GR.TEXT.DRAW txtend,whalf,hhalf*0.2,"game over"
GR.TEXT.ALIGN 2
GR.TEXT.DRAW txtend,whalf,hhalf*0.4,"speed "+ INT$(rnum)

IF score=4
 T$= "earth is safe!"
ELSE
 t$="we are toast!"
ENDIF

GR.TEXT.DRAW txt,whalf,hhalf*0.6, t$



DO
 GR.TOUCH touched,x,y
 PAUSE 100
UNTIL !touched

b$=buttons$()

IF b$="play again" THEN
 GR.CLOSE
 PAUSE 300
 Tend=0
 drag=0
 GOTO playagain
ENDIF

EXIT
