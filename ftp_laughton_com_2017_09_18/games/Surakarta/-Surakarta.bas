! Surakarta  @Cassiope34  09/2014

Fn.def clr( cl$ )
  gr.color val(word$(cl$,1)), val(word$(cl$,2)), val(word$(cl$,3)), val(word$(cl$,4)), val(word$(cl$,5))
Fn.end

Fn.def LastPtr( ptr )  % put the graphic pointer 'ptr' (so the bitmap) UP to all other.
  gr.getDL ndl[],1
  array.length sz, ndl[]
  if !ptr | sz=1 | ndl[sz] =ptr
    array.delete ndl[]
    Fn.rtn 0
  endif
  array.search ndl[],ptr,n
  if n
    for nn=n to sz-1
      ndl[nn] =ndl[nn+1]
    next
    ndl[sz] =ptr
    gr.newDL ndl[]
  endif
  array.delete ndl[]
Fn.end

Fn.def movepix( ptr, arx, ary )    % move the bitmap 'ptr' to arx,ary smoothly...
 do
   gr.get.position ptr, px, py
   ix =floor((px-arx)/10)
   iy =floor((py-ary)/10)
   if abs(ix) <3 & abs(iy) <3 then D_u.break
   gr.modify ptr, "x", px-ix
   gr.modify ptr, "y", py-iy
   gr.render
 Until 0
 gr.modify ptr, "x", arx
 gr.modify ptr, "y", ary
 gr.render
Fn.end

File.Root tmp$
IstandAlone =Is_In("rfo-basic",tmp$)    % to know if it's an APK for the way to exit.

gr.open 255,116,212,129,0,-1
gr.screen w,h
if w>h    % landscape
  ori =0
  scx =1280
  scy =800
  rp  =floor(scy/23)  %34      % rayon d'un pion.
  ec  =2.4*rp          % écart entre les pions
  plx =(scx-scy)/2+rp  % position du bitmap du plateau de jeu
  ply =(scy-9*ec)/2
else      %  portrait
  ori =1
  scx =800
  scy =1280
  rp  =floor(scx/23)  %34      % rayon d'un pion.
  ec  =2.4*rp          % écart entre les pions
  plx =(scx-9*ec)/2    % position du bitmap du plateau de jeu
  ply =(scy-scx)/2+rp
endif
sx  =w/scx
sy  =h/scy
gr.scale sx,sy

DIM c[6,6,4], cl$[4], main[2]
! param. pour c[6,6,4]
jo =1  % joueur 1 ou 2 a qui appartient cette case
pt =2  % pointeur du pion
cr =3  % n° de la case reliée si c'est le cas
ns =4  % nouveau sens de trajectoire

ARRAY.LOAD diX[],0,1,0,-1,-1,1,-1,1  % 1 North  2 East  3 South  4 West  rest=diagonals
ARRAY.LOAD diY[],-1,0,1,0,-1,-1,1,1
cl$[1] ="255 0 230 216 0"    % bleu
cl$[2] ="255 255 210 120 0"  % orange
cl$[3] ="255 200 200 200 0"  % gris
cl$[4] ="255 116 212 129 1"  % fond
fileSav$ ="Surakarta.sav"

! les paramètres pour le parcours des cercles par les pions.
array.load pdd[], 21,31,12,13, 41,51,62,63, 46,56,64,65, 14,15,26,36
array.load se[], -1,-1,1,1, 1,1,-1,-1, -1,-1,1,1, -1,-1,1,1
array.load ad[],  0,0,90,90, 180,180,90,90, 180,180,270,270, 270,270,0,0
array.load prx[], plx+2*ec, plx+7*ec, plx+7*ec, plx+2*ec
array.load pry[], ply+2*ec, ply+2*ec, ply+7*ec, ply+7*ec

! fabrication des bitmaps
xh =7.8*ec
yh =7.8*ec
gr.bitmap.create bhelp, xh, yh     % bitmap de l'aide
gr.bitmap.drawinto.start bhelp
gr.color 255,116,212,129,1
gr.rect nul, 0, 0, xh, yh
inl =45
gr.text.size 50
gr.set.stroke 4
gr.color 255, 255, 169, 4, 2
gr.text.draw nul, 1.5*ec, 42, "S U R A K A R T A"
gr.text.size 32
gr.set.stroke 1
gr.color 255, 238, 255, 0, 1   % jaune
dim h$[11]
h$[1] ="This game is of Indonesian origin."
h$[2] ="The rule is very simple !"
h$[3] ="In order to make a capture, a pawn must"
h$[4] ="loop around one or more of the circular"
h$[5] ="paths before moving onto a space occupied"
h$[6] ="by an enemy pawn."
h$[7] ="Otherwise, a pawn can move in all direction"
h$[8] ="but only one cell."
h$[9] ="Of course the player who capture the most"
h$[10] ="opponent's pawns win !"
h$[11] ="Always whites begin."
for lg=1 to 11
  gr.text.draw nul, 14, 60+lg*inl, h$[lg]
next
gr.text.draw nul, xh-2*ec, yh-inl, "have fun..."
array.delete h$[]
gr.bitmap.drawinto.end

gr.bitmap.create pion2, 2*rp, 2*rp     % pion 2 noir
gr.bitmap.drawinto.start pion2
gr.set.stroke 2
clr =40
for r=rp-2 to 1 step -1
  gr.color 255,clr,clr,clr,0
  gr.circle nul, rp, rp, r
  clr+=6
next
gr.bitmap.drawinto.end

gr.bitmap.create pion1, 2*rp, 2*rp     % pion 1 blanc (gris clair)
gr.bitmap.drawinto.start pion1
gr.set.stroke 2
clr =180
for r=rp-2 to 1 step -1
  gr.color 255,clr,clr,clr,0
  gr.circle nul, rp, rp, r
  clr+=2
next
gr.bitmap.drawinto.end

gr.bitmap.create plateau, min(scx,scy)-rp, min(scx,scy)-rp      % bitmap du plateau
gr.bitmap.drawinto.start plateau
gr.set.stroke 3
call clr( cl$[1] )
gr.circle nul, 2*ec, 2*ec, 2*ec
gr.circle nul, 2*ec, 7*ec, 2*ec
gr.circle nul, 7*ec, 2*ec, 2*ec
gr.circle nul, 7*ec, 7*ec, 2*ec
call clr( cl$[2] )
gr.circle nul, 2*ec, 2*ec, ec
gr.circle nul, 2*ec, 7*ec, ec
gr.circle nul, 7*ec, 2*ec, ec
gr.circle nul, 7*ec, 7*ec, ec
call clr( cl$[4] )
gr.rect nul, 2*ec, 2*ec, 7*ec, 7*ec
for px=0 to 5                 %  grille
  c =3
  if px=1 | px=4 then c=2
  if px=2 | px=3 then c=1
  call clr( cl$[c] )
  gr.line nul, 2*ec+px*ec, 2*ec, 2*ec+px*ec, 7*ec
next
for py=0 to 5
  c =3
  if py=1 | py=4 then c=2
  if py=2 | py=3 then c=1
  call clr( cl$[c] )
  gr.line nul, 2*ec, 2*ec+py*ec, 7*ec, 2*ec+py*ec
next
gr.color 255,200,200,200,1     % petits points d'intersections gris.
for py=0 to 5
  for px=0 to 5
    gr.circle nul, 2*ec+px*ec, 2*ec+py*ec, 6
  next
next
gr.bitmap.drawinto.end


!   initialisation plateau & pions. c[].
new:
gr.cls
gr.bitmap.draw plt,plateau, plx, ply    % le plateau
gr.bitmap.draw helpb,bhelp, plx+1.5*rp, ply+1.5*rp   % help
gr.hide helpb
UnDim c[],ttp[]
Dim c[6,6,4], ttp[2]
cc$ ="12 2 13 2 63 4 62 4 21 3 51 3 31 3 41 3 36 1 46 1 26 1 56 1 15 2 14 2 64 4 65 4"
c=0
for py=1 to 6
  for px=1 to 6
    if ((py=1 | py=6) & px>1 & px<6) | ((px=1 | px=6) & py>1 & py<6)   % les bords
      c++
      c[px,py,cr] =val(word$(cc$,c))   % case reliée
      c++
      c[px,py,ns] =val(word$(cc$,c))   % nouvelle direction
    endif
  next
next

gr.set.stroke 2      % les mains
gr.text.size 85
gr.color 255,255,255,255,1
gr.text.draw main[1], 100-90*ori, scy-190, chr$(9758)     % à qui c'est le tour.
gr.color 255,0,0,0,1
gr.text.draw main[2], 100-90*ori, 270, chr$(9758)
gr.hide main[1]
gr.hide main[2]

j    =1    %  blanc commence par défaut.
dep  =0
gosub loadgame     % dernière partie éventuelle à recharger... (fe=1)

if !fe    % nouvelle partie  (pas de partie rechargée)
 for py=0 to 5        % place les pions
  for px=0 to 5
    if py=0 | py=1
      gr.bitmap.draw c[px+1,py+1,pt], pion2, plx+2*ec+px*ec-rp, ply+2*ec+py*ec-rp
      c[px+1,py+1,jo] =2   % noir
    elseif py=4 | py=5
      gr.bitmap.draw c[px+1,py+1,pt], pion1, plx+2*ec+px*ec-rp, ply+2*ec+py*ec-rp
      c[px+1,py+1,jo] =1   % blanc
    endif
  next
 next
 fe  =0
 dep =1
endif

gr.show main[j]
gr.hide main[3-j]
gr.render

gr.set.stroke 3
gr.color 255,255,0,0,0
gr.circle crg, rp+5, rp+5, rp+5   % curseur cercle rouge
gr.hide crg
gr.color 255,0,0,255,0
gr.circle cbl, rp+5, rp+5, rp+5   % curseur cercle bleu
gr.hide cbl

gr.color 255,255,255,0,1    % texte jaune
gr.text.size 35
gr.text.align 2
gr.color 255,0,230,216,1    % texte bleu
gr.text.draw newt, scx-ec, ec-20, "N E W"
gr.text.draw helpt, ec+10, ec-20, "H E L P"

gr.color 255,255,255,0,1    % texte jaune
gr.text.draw mess, scx/2, 40, ""   % message

new  =0
quit =0
slx  =0
sly  =0
FIN  =0

do             %   boucle principale
  do
    gr.touch touched, x, y
    if !background()
      gosub oridetect
      gr.render
    endif
  until touched | quit
  if quit then D_u.continue     %  exit
  if help
    gr.hide helpb
    help =0
  endif
  do
    gr.touch touched, tx, ty
  until !touched
  tx/=sx
  ty/=sy
  cx =floor((tx-plx-4*rp)/ec)+1    % coord. case
  cy =floor((ty-ply-4*rp)/ec)+1
  if cx>0 & cx<7 & cy>0 & cy<7     % dans la grille
    if cx=slx & cy=sly        % même case sélectionnée !
      D_u.continue

    elseif c[cx,cy,jo]=j      % sélection d'un pion de j: slx,sly
      slx =cx
      sly =cy
      gr.modify crg, "x", plx+(cx+1)*ec    % cercle rouge
      gr.modify crg, "y", ply+(cy+1)*ec
      gr.show crg
      gr.hide cbl

    elseif slx<>0 & c[cx,cy,jo]=0        % case vide pour déplacement.
      if abs(slx-cx)<2 & abs(sly-cy)<2   % vérifie distance d'une case.
        gr.modify c[slx,sly,pt], "x", plx+(cx+1)*ec-rp    % déplacement
        gr.modify c[slx,sly,pt], "y", ply+(cy+1)*ec-rp
        swap c[cx,cy,jo],c[slx,sly,jo]
        swap c[cx,cy,pt],c[slx,sly,pt]
        gr.modify cbl, "x", plx+(cx+1)*ec   % cercle bleu
        gr.modify cbl, "y", ply+(cy+1)*ec
        gr.show cbl
        dep =1
        slx =0
        sly =0
        j   =3-j   % chg de joueur
        gr.show main[j]
        gr.hide main[3-j]

      endif

    elseif slx<>0 & c[cx,cy,jo]=(3-j)      % sélection d'un pion adverse pour prise.
      gosub ctrlok      % faisable: ok=1, ok=0 si pas possible.
      if ok

        gosub move      % effectue la prise.

        gr.hide c[cx,cy,pt]
        gr.modify c[slx,sly,pt], "x", plx+(cx+1)*ec-rp   % déplacement maj
        gr.modify c[slx,sly,pt], "y", ply+(cy+1)*ec-rp
        swap c[cx,cy,jo], c[slx,sly,jo]
        swap c[cx,cy,pt], c[slx,sly,pt]
        c[slx,sly,jo] =0
        c[slx,sly,pt] =0
        gosub counting
        if ttp[1]=0 | ttp[2]=0 then FIN =1

        gr.modify cbl, "x", plx+(cx+1)*ec   % cercle bleu
        gr.modify cbl, "y", ply+(cy+1)*ec
        gr.show cbl

        slx =0
        sly =0
        j   =3-j   % chg de joueur
        gr.show main[j]
        gr.hide main[3-j]
      endif

    endif

  else         % touche hors grille
    slx =0
    sly =0
    gr.hide crg
    gr.hide cbl

   ! gr.modify mess, "text", int$(cx)+" , "+int$(cy)   % infos...
    if (ori=0 & cy=-1) | (ori=1 & cy=-4)
      if (ori=0 & cx>9) | (ori=1 & cx>6)    % New
        new =1
        quit =1

      elseif (ori=0 & cx<-2) | (ori=1 & cx<1)   % help
        help =1
        call LastPtr( helpb )
        gr.show helpb

      endif
    endif
  endif

until quit       %  fin de boucle principale

if new then goto New

if dep & !FIN then gosub savegame
if IstandAlone
  END "Bye...!"
else
  EXIT
endif      %  ----------------------  exit  ----------------------

onbackkey:
if help
  gr.hide helpb
  help =0
  Back.resume
endif
lastime =clock()
if lastime-thistime <1000
  quit =1
else
  quit =0
  thistime =lastime
  popup " Presser encore pour quitter ",0,0,0
endif
Back.resume

oridetect:
gr.screen w,h      % 0=landscape, 1=portrait
change =0
if ori=0 & w<h     % go portrait
  ori =1
  scx =800
  scy =1280
  rp  =floor(scx/23)  %34      % rayon d'un pion.
  ec  =2.4*rp          % écart entre les pions
  plx =(scx-9*ec)/2    % position du bitmap du plateau de jeu
  ply =(scy-scx)/2+rp
  pmy1 =scy-190
  change =1

elseif ori=1 & w>h    % go landscape
  ori =0
  scx =1280
  scy =800
  rp  =floor(scy/23)  %34      % rayon d'un pion.
  ec  =2.4*rp          % écart entre les pions
  plx =(scx-scy)/2+rp  % position du bitmap du plateau de jeu
  ply =(scy-9*ec)/2
  pmy1 =scy-190
  change =1

endif
if change
  gr.modify plt,"x",plx
  gr.modify plt,"y",ply
  gr.modify helpb,"x",plx+1.5*rp
  gr.modify helpb,"y",ply+1.5*rp
  gr.modify newt,"x",scx-ec
  gr.modify newt,"y",ec-20
  gr.modify main[1],"x",100-(90*ori)
  gr.modify main[1],"y",pmy1
  gr.modify main[2],"x",100-(90*ori)
  for py=0 to 5        % replace les pions
    for px=0 to 5
      if c[px+1,py+1,pt]
        gr.modify c[px+1,py+1,pt], "x", plx+2*ec+px*ec-rp
        gr.modify c[px+1,py+1,pt], "y", ply+2*ec+py*ec-rp
      endif
    next
  next
  slx =0
  sly =0
  gr.hide crg
  gr.hide cbl
  UnDim prx[], pry[]
  array.load prx[], plx+2*ec, plx+7*ec, plx+7*ec, plx+2*ec
  array.load pry[], ply+2*ec, ply+2*ec, ply+7*ec, ply+7*ec
  sx  =w/scx
  sy  =h/scy
  gr.scale sx,sy
endif
return

counting:   % compte les pions pour détecter la fin de partie...
ttp[1] =0
ttp[2] =0
for yc=1 to 6
  for xc=1 to 6
    if c[xc,yc,jo] then ttp[ c[xc,yc,jo] ]++
  next
next
return

ctrlok:   % est-ce que slx,sly -> cx,cy est possible pour j = prise de cx,cy.
cs =slx*10+sly
if cs=11 | cs=16 | cs=61 | cs=66 then return    % sauf pour les 4 cases de coin.
! dans les 4 directions dp depuis slx,sly
!  ds=direction secondaire
! si hors limite grille voir si case reliée
!   si oui: continuer à cette case dans nouvelle direction ds
!   si non: continue ds
! jusqu'à rencontrer:
!    sa couleur :
!       on change de direction dp depuis slx,sly
!    couleur adverse à cx,cy :
!      -- variante : voir si case suivante est libre... --
!       si une boucle parcourue : ok =1 : on sort
UnDim vo[]
Dim vo[24]   % pour enregistrer le parcours.
ok   =0
bcle =0
chdo =0
dp   =1   % 2,3,4  directions depuis slx,sly
ds   =dp
nx   =slx
ny   =sly
nb   =0
c[slx,sly,jo] =0   % retire temporairement le pion de 'j' qui bouge...
do
  if nx+diX[ds]=0 | nx+diX[ds]=7 | ny+diY[ds]=0 | ny+diY[ds]=7    % ext. plateau
    if c[nx,ny,cr]
      ds =c[nx,ny,ns]            % nouvelle direction.
      vx =floor(c[nx,ny,cr]/10)  % extrait coord. case reliée.
      ny =c[nx,ny,cr]-vx*10
      nx =vx
      bcle =1     % on emprunte une boucle...
    else
      chdo =1     % un coin
    endif

  else
    nx +=diX[ds]   % on continue dans le sens ds (tout droit!)
    ny +=diY[ds]

  endif
  nb++     % nbre de cases testées
  vo[nb] =nx*10+ny   % les mémorise
  if c[nx,ny,jo]=j            % rencontre ma couleur
    chdo =1

  elseif c[nx,ny,jo]=(3-j)    % rencontre couleur adverse:
    if nx=cx & ny=cy & bcle then ok =1 else chdo =1

  endif
  if chdo    % on essaye depuis case de départ dans la direction suivante de dp.
    dp++
    ds   =dp
    nx   =slx
    ny   =sly
    bcle =0
    nb   =0
  endif
  chdo =0

until ok=1 | dp=5 | nb=24
c[slx,sly,jo] =j   % restore le pion
RETURN

move:    % déplace slx,sly en suivant vo[]
  call LastPtr( c[slx,sly,pt] )
  vx =slx
  vy =sly
  mx =vx
  my =vy
  n=0
  do
      if n
        vx =floor(vo[n]/10)   % extrait coord.
        vy =vo[n]-vx*10
      endif
      if n+1<=nb
        vsx =floor(vo[n+1]/10)   % extrait coord. suivante
        vsy =vo[n+1]-vsx*10
        if vx<>vsx & vy<>vsy     % vx,vy est le départ d'une boucle:
          !   parcourir la ligne droite précédente... d'oú il est jusqu'à vx,vy.
          if mx<>vx | my<>vy
            call movepix( c[slx,sly,pt], plx+(vx+1)*ec-rp, ply+(vy+1)*ec-rp )
          endif
          gosub gorot     % 270° depuis vx,vy jusqu'à vsx,vsy
          mx =vsx
          my =vsy
        endif
      endif
      n++
  until n>nb
  vfx =floor(vo[nb]/10)    % extrait coord. de la dernière case du parcours.
  vfy =vo[nb]-vfx*10
  if mx<>vfx | my<>vfy     %  parcourir la dernière ligne droite jusqu'à vfx,vfy.
    call movepix( c[slx,sly,pt], plx+(vfx+1)*ec-rp, ply+(vfy+1)*ec-rp )
  endif
return

gorot:   % trouve la boucle de vx,vy et l'effectue. (doit arriver à vsx,vsy)
cs =vx*10+vy
array.search pdd[], cs, t   % trouve les paramètres de rotation.
co  =floor((t-0.1)/4)+1     % coin oú ça se passe  1 à 4 (centre de la rotation)
prx =prx[co]   % coord. du coin
pry =pry[co]
se  =se[t]     % sens de rotation
ad  =ad[t]     % angle de départ     IMPORTANT : 0° est horizontal à droite...
rayon =2*ec    % grand cercle
if cs=21 | cs=12 | cs=51 | cs=62 | cs=65 | cs=56 | cs=26 | cs=15 then rayon =ec
pas =10
a   =pas
do
  aa  =ad+a*se
  aa  =aa-360*(aa>360)+360*(aa<0)
  vpx =prx+cos(toradians(aa))*rayon
  vpy =pry+sin(toradians(aa))*rayon
  gr.modify c[slx,sly,pt], "x", vpx-rp
  gr.modify c[slx,sly,pt], "y", vpy-rp
  gr.render
  a+=pas
until a>=270
return

savegame:
cls
if !FIN
  print int$(j)
  sa$=""
  for y=1 to 6
    for x=1 to 6
      sa$ =sa$ +int$(c[x,y,jo])+" "
    next
  next
  print sa$
  console.save fileSav$
  cls
endif
return

loadgame:
File.exists fe, fileSav$
if fe
  GRABFILE ldgame$, fileSav$
  split ln$[], ldgame$, "\n"
  j =val(ln$[1])
  n =0
  for py=0 to 5        % place les pions
    for px=0 to 5
      n++
      p =val(word$(ln$[2],n))
      if p=1
        gr.bitmap.draw c[px+1,py+1,pt], pion1, plx+2*ec+px*ec-rp, ply+2*ec+py*ec-rp
        c[px+1,py+1,jo] =1   % blanc
      elseif p=2
        gr.bitmap.draw c[px+1,py+1,pt], pion2, plx+2*ec+px*ec-rp, ply+2*ec+py*ec-rp
        c[px+1,py+1,jo] =2   % noir
      endif
    next
  next
  array.delete ln$[]
  File.delete fe, fileSav$    % delete last game file
endif
return
