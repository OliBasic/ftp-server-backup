My first RFOBasic game for Android.
Classic tile-puzzle game written in RFOBasic 1.77, then adapted to 1.86.
Built in 7 pictures and 4 midi music. Easy to add up to 20 own mp3/ogg/mid music.
Built in minimalistic picture editor to add (replace built in) any picture, even
from camera.
Built in surprise animation (easter-egg).

Everything in source is in hungarian language.
Feel free to ask me about program at fhindicki@gmail.com or G+

