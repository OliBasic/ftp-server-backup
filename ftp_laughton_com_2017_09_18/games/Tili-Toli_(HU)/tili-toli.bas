! Tili-Toli (480*800, v177.02,v186)

fn.def himan(qh,pic,num$,usr$,idx[],egb[],wav)
popup "Kiraktad !\nGratulálok !!",0,-(qh*250),0
list.get pic,idx[1],tb$
tb$=left$(tb$,-4)
num$=right$(num$,3)
db=idx[5]
if idx[2]=3
 lv$="lo"
else
 lv$="hi"
endif
sql.query cur,db,tb$,"_id,User_"+lv$+","+lv$,"",lv$+" DESC"
sql.next xdn,cur,id$,tus$,ths$
if tus$="Ismeretlen" | num$<ths$
 sql.update db,tb$,lv$,num$:"_id = '"+id$+"'"
 if usr$="" | usr$<>tus$
  input "Új rekord ! Írd be a neved.",use$,usr$,cnc
  if use$="Vanelope" then call egg(egb[],wav)
  gr.render
  if cnc & usr$=""
   usr$=tus$+"_"+id$
  else
   usr$=use$
  endif
  sql.update db,tb$,"User_"+lv$,usr$:"_id = '"+id$+"'"
 endif
endif
fn.rtn 0
fn.end

fn.def egg(egb[],wav)
gr.bitmap.draw etb,egb[1],160,262
gr.render
soundpool.play snd,wav,0.8,0.8,0,0,1
for i=2 to 27
 gr.modify etb,"bitmap",egb[i]
 pause 25
 gr.render
next i
pause 500
gr.hide etb
gr.render
fn.end


fn.def hiscor(as,pic,idx[])
list.get pic,idx[1],jpg$
jpg$=left$(jpg$,-4)
if idx[2]=3
 sql.query cur,idx[5],jpg$,"User_lo,lo","","lo ASC"
 inf$=" 3x4 "
else
 sql.query cur,idx[5],jpg$,"User_hi,hi","","hi ASC"
 inf$=" 4x6 "
endif
gr.color 200,0,0,0,1
gr.text.size 18
gr.bitmap.create hst,300,497
gr.bitmap.drawinto.start hst
gr.rect dum,0,0,300,497
gr.color 200,0,0,255,1
gr.rect dum,0,0,300,45
gr.color 255,255,255,255,0
gr.rect dum,1,1,299,496
gr.text.draw dum,15,30,jpg$+inf$+" ranglista"
for i=1 to 10
 sql.next xdn,cur,tus$,ths$
 gr.text.draw dum,10,(i*45)+25,tus$
 gr.text.draw dum,260,(i*45)+25,ths$
 gr.line dum,0,(i*45)+45,300,(i*45)+45
next i
gr.bitmap.drawinto.end
gr.bitmap.draw hsel,hst,90,150
gr.render
gr.bitmap.delete hst
gr.hide hsel
fn.end

fn.def index(as,pic,idx[])
gr.bitmap.draw logo,idx[4],0,0
gr.render
gr.cls
gr.color 255,200,200,55,0
gr.bitmap.create opt,480,800
gr.bitmap.drawinto.start opt
for i=1 to as
 list.get pic,i,fn$
 gr.bitmap.load tmp_bm,fn$
 gr.bitmap.size tmp_bm,xbm,ybm
 rat=xbm/ybm
 gr.bitmap.scale rsbm,tmp_bm,rat*100,100
 gr.bitmap.draw dum,rsbm,2,(i-1)*102
 gr.bitmap.delete tmp_bm
 gr.bitmap.delete rsbm
 fn$=left$(fn$,-4)
 sql.query cur,idx[5],fn$,"lo","","lo ASC"
 sql.next xdn,cur,t34$
 sql.query cur,idx[5],fn$,"hi","","hi ASC"
 sql.next xdn,cur,t46$
 gr.color 255,10,250,10,0
 gr.text.size 18
 gr.text.draw dum,330,((i-1)*102)+80,"("+t34$+")"
 gr.text.draw dum,420,((i-1)*102)+80,"("+t46$+")"
 gr.color 255,200,200,55,0
 gr.text.size 24
 gr.text.draw dum,80,((i-1)*102)+60,fn$
 gr.text.draw dum,330,((i-1)*102)+40,"3 x 4"
 gr.text.draw dum,420,((i-1)*102)+40,"4 x 6"
 gr.line dum,0,i*102,480,i*102
next i
gr.line dum,320,0,320,as*102
gr.line dum,405,0,405,as*102
gr.bitmap.drawinto.end
gr.bitmap.draw msel,opt,0,0
gr.render
gr.bitmap.delete opt
fn.rtn time()
fn.end

fn.def pres(w,h)
wr=468/w : hr=660/h
if w>h
 h=660 : w=w*hr
elseif w<h
 w=468 : h=h*wr
else
 h=660 : w=468
endif
while w<468|h<660
 w=w*1.1 : h=h*1.1
repeat
fn.end

vol=100 : vol2=vol
mode=0 : usr$=""
map$=""
list.create n,mask
list.add mask,0,0,5,0,5,660,475,660,475,0~
480,0,480,800,0,800
dim fdr$[1]
rgx$="[^a-z|A-Z|_]+"
list.create s,fdr
list.create s,his
list.add his,"../../" : old=1
array.load idx[],1,3,4,0,8
dbm$="../databases/"
dbn$="tmp_hiscores.db"
file.exists cnc,dbm$+dbn$
if !cnc
 file.rename dbm$+right$(dbn$,11),dbm$+dbn$
endif
sql.open idx[5],dbn$
array.load men$[],"  ---","Új kép","Trófeák","Kirak"
list.create s,pic
list.create n,men
list.add men,0,0,112,0,112,50,0,50 
dw=480
dh=800
gr.open 255,0,0,0,0,1
gr.screen scw,sch
device tel
bundle.get tel,"OS",os$
!!
Az itt megadott Android verziók kissebb felbontást
jeleznek a valódinál az állapot sor fix mivolta miatt.
Itt lehet korrigálni a valódi értékre. Infó hiányában
csak a 4.0.3 van megadva. (gr.scale tesztelésre is jó!)
!!
if os$="4.0.3"  % | os$="4.0.2"  ...
 gr.statusbar sbs
 sch=sch + sbs
endif
qw=scw/dw : qh=sch/dh
gr.scale qw,qh
gr.bitmap.load tmm,map$+"editor.png"
gr.bitmap.load idx[4],map$+"logo.png"
gr.bitmap.draw logo,idx[4],0,0
gr.render
dim zum[20]
dim egb[27]
soundpool.open 2
soundpool.load wav,map$+"easter.egg"
snd=0
file.dir map$,pic$[]
array.length as,pic$[]
for i=1 to as
 muz=ends_with(".mp3",pic$[i])
 muz|=ends_with(".ogg",pic$[i])
 muz|=ends_with(".mid",pic$[i])
 if muz & snd<20
  snd=snd+1
  audio.load zum[snd],map$+pic$[i]
 endif
next i
as=7
sql.query cur,idx[5],"puzz","name","","name ASC"
for i=1 to as
 sql.next xdn,cur,dbn$ 
 list.add pic,dbn$
next i
array.copy zum[1,snd],pls[]
timer.set 1000
tout=time()
gr.bitmap.load ebm,map$+"e_egg.png"
for i=1 to 27
 gr.bitmap.crop tgb,ebm,(i-1)*96,0,96,144
 gr.bitmap.scale egb[i],tgb,160,240
next i
gr.bitmap.delete ebm

do
dim sol[32]
tout=time()
tout=index(as,pic,idx[])
do
 if mode=4
   do
    undim fdr$[]
    list.clear fdr
    list.add fdr,"..."
    list.get his,old,pop$
    file.dir pop$,fdr$[]
    array.length al,fdr$[]
    for i=1 to al
     lwr$=lower$(fdr$[i])
     pix=ends_with(".jpg",lwr$)
     pix|=ends_with(".png",lwr$)
     pix|=ends_with(".gif",lwr$)
     pix|=ends_with("(d)",lwr$)
     if pix then list.add fdr,fdr$[i]
    next i
    dialog.select id,fdr,"Válassz képet !"
    if id
     list.get fdr,id,fn$
     if ends_with("(d)",fn$)
      pop$=pop$+left$(fn$,-3)+"/"
      list.add his,pop$
      old=old+1 : id=1
     elseif id=1 & old>1
      list.remove his,old
      old=old-1
      list.get his,old,pop$
     endif
    endif
   until id>1
   mode=7
 endif
 if mode=5|mode=7
   gr.cls
   gr.bitmap.load tpp,pop$+fn$
   gr.bitmap.size tpp,w,h
   call pres(&w,&h)
   gr.bitmap.scale tpz,tpp,w,h
   gr.bitmap.draw tpo,tpz,0,0
   gr.color 200,0,0,0,1
   gr.poly pmk,mask,0,0
   gr.color 255,255,255,255,0
   gr.bitmap.draw mnu,tmm,0,610
   gr.render
   x=0 : y=0
   w2=w : h2=h
   mode=6
 endif
until mode=1
gr.cls
gr.color 255,200,200,55,0
gr.text.size 24
osz=idx[2]
sor=idx[3]
pcs=osz*sor
dim bmp[pcs],uns[pcs]
bx=468/osz
bw=bx+2
bws=((qw*468)/osz)+2
by=660/sor
bh=by+2
bhs=((qh*660)/sor)+2
list.get pic,idx[1],ttn$
gr.bitmap.load tol,map$+ttn$
for i=0 to (sor-1)
 for j=0 to (osz-1)
  h=(i*osz)+j+1
  gr.bitmap.crop bmp[h],tol,j*bx,i*by,bx,by
  gr.bitmap.draw sol[h],bmp[h],(j*bw)+2,i*bh
 next j
next i
gr.bitmap.delete tol
for i=0 to 3
 gr.poly sol[i+pcs+1],men,2+(i*120),710
 gr.text.draw sol[i+pcs+5],22+(i*120),742,men$[i+1]
next i
array.copy bmp[],uns[]
array.shuffle uns[]
array.search uns[],bmp[pcs],se
swap uns[pcs],uns[se]
 for h=1 to pcs
  gr.modify sol[h],"bitmap",uns[h]
 next h
 se=pcs
 gr.hide sol[se]
gr.render
score=0
do
until !mode
undim sol[],bmp[],uns[]
gr.cls
gr.render
until 0

ongrtouch:
if mode=1|mode=2
 gr.bounded.touch t2,2,qh*710,scw,qh*764
 gr.bounded.touch t3,2,0,qw*474,qh*666
 gr.touch t4,x,y
 if t2
  sw.begin floor((x-2)/(qw*122))
  sw.case 0
   if mode=1 & score=45
    call egg(egb[],wav)
   endif
   sw.break
  sw.case 1
   mode=0
   sw.break
  sw.case 2
   tmode=mode
   mode=9
   call hiscor(as,pic,idx[])
   sw.break
  sw.case 3
   if mode=1
    for i=1 to pcs
     gr.modify sol[i],"bitmap",bmp[i]
    next i
    gr.show sol[se]
    gr.render
    do
     gr.touch t4,x,y
    until !t4
    for i=1 to pcs
     gr.modify sol[i],"bitmap",uns[i]
    next i
    gr.hide sol[se]
    gr.render
   endif
   sw.break
  sw.end
 elseif t3 & mode=1
  x=x-2
  de=(floor(y/bhs)*osz)+ceil(x/bws)
  if abs(de-se)=1|abs(de-se)=osz
   gr.modify sol[se],"bitmap",uns[de]
   gr.modify sol[de],"bitmap",uns[se]
   swap uns[de],uns[se]
   gr.hide sol[de]
   gr.show sol[se]
   se=de
   score=min(score+1,999)
   num$=format$("%%%",score)
   gr.modify sol[pcs+5],"text",num$
   gr.render
   ok=1
   for i=1 to pcs
    if bmp[i]=uns[i] then f_n.continue
    ok=0
    f_n.break
   next i
   if ok
    gr.show sol[se]
    mode=2
    gr.render
    call himan(qh,pic,num$,&usr$,idx[],egb[],wav)
   endif
  endif
 endif
elseif mode=0
 gr.bounded.touch t0,0,0,scw,as*(qh*102)
 if t0
  gr.touch t0,x,y
  idx[1]=ceil(y/(qh*102))
  if x>(qw*320)
   x=x>=(qw*405)
   idx[2]=3+x
   idx[3]=4+(2*x)
   mode=1
  elseif x<(qw*80) % picture editor
   list.get pic,idx[1],spn$
   msg$="A "+spn$+" képhez tartozó\n"
   msg$ +="ranglista törölve lesz !!"
   pop$=map$ : fn$=spn$
   mode=5
  else        % what to do on name?
  endif
 endif
elseif mode=9
 x=scw/5.333
 y=sch/5.333
 w=scw/1.23
 h=sch/1.23
 gr.bounded.touch t9,x,y,w,h
 if t9
  do
   gr.touch t9,x,y
  until !t9
  gr.render
  mode=tmode
 endif
elseif mode=6        % kisérleti editor
 gr.bounded.touch tc,0,0,scw,qh*660
 gr.bounded.touch tm,0,qh*665,scw,sch
 if tc
  gr.touch t,a,b
  do
   gr.touch t,n,m
   nsc=n*(dw/scw)
   if m>(qh*600)
    mag=((240+(nsc-dw))/280)+1.1
    wx=w*mag : hx=h*mag
    if wx>=468 & hx>=660
     w2=wx : h2=hx
     gr.bitmap.delete tpz
     gr.bitmap.scale tpz,tpp,w2,h2
     gr.modify tpo,"bitmap",tpz
    endif
   endif
   gr.modify tpo,"x",max(min(x+(n-a),6),-(w2-474))
   gr.modify tpo,"y",max(min(y+(m-b),0),-(h2-660))
   gr.render
  until !t
  gr.get.position tpo,x,y
 elseif tm
  gr.touch tm,n,m
  n=floor(n/(qw*120))
  if n=0
   gr.bitmap.delete tpp
   gr.bitmap.delete tpz
   mode=4
  elseif n=1
   gr.bitmap.delete tpp
   gr.bitmap.delete tpz
   gr.orientation 0
   gr.camera.manualshoot tpp
   gr.bitmap.size tpp,w,h
   call pres(&w,&h)
   gr.bitmap.scale tpz,tpp,w,h
   gr.modify tpo,"bitmap",tpz
   gr.orientation 1
   gr.render
   fn$="Kamera_"+chr$(rnd()*26+65)+chr$(rnd()*26+65)+".jpg"
  elseif n=2
   dialog.message "Figyelem !",msg$,bch,"Mégsem","Rendben"
   if bch=2
    gr.bitmap.delete tpp
    gr.bitmap.crop tpp,tpz,abs((x*qw)-6),abs(y*qh),468,660
    gr.modify tpo,"bitmap",tpp
    gr.modify tpo,"x",6
    gr.hide mnu
    gr.render
    gr.bitmap.delete tpz
    msg$=word$(fn$,1,rgx$)
    do
     popup "A név nem kezdődhet számmal.\n \"a-z és _\" lehet benne !!",0,-(qh*255),0
     input "Adj nevet a képnek !",fn$,msg$,cnc
     fn$=word$(fn$,1,rgx$)
     cnc=cnc|(ascii(fn$)=256)
    until !cnc
    gr.render
    gr.bitmap.save tpp,map$+fn$+".jpg",65
    gr.bitmap.delete tpp
    u1$="User_lo" : u2$="User_hi"
    if usr$=""
     u3$="Ismeretlen"
    else
     u3$=usr$
    endif
    sql.update idx[5],"puzz","name",fn$+".jpg":"name = '"+spn$+"'" 
    sql.drop_table idx[5],left$(spn$,-4)
    sql.new_table idx[5],fn$,u1$,"lo",u2$,"hi"
    for j=1 to 10
     sql.insert idx[5],fn$,u1$,u3$,"lo","999",u2$,u3$,"hi","999"
    next j
    list.replace pic,idx[1],fn$+".jpg"
    undim fdr$[]
    list.toarray pic,fdr$[]
    array.sort fdr$[]
    list.clear pic
    list.add.array pic,fdr$[]
    gr.cls
    tout=index(as,pic,idx[])
    mode=0
   endif
  else
   tout=index(as,pic,idx[])
   mode=0
  endif
 endif
endif
gr.ongrtouch.resume
onkeypress:
inkey$ k$
if k$="key 24"|k$="up"
  vol2=min(vol+10,100)
elseif k$="key 25"|k$="down"
  vol2=max(vol-10,0)
endif
if vol2<>vol
 vol=vol2
 audio.volume vol/100,vol/100
endif
key.resume
ontimer:
 audio.isdone mup
 bg=background()
 if mup & !bg
  audio.stop
  array.shuffle pls[]
  audio.play pls[1]
 elseif bg
  audio.stop
 elseif mode=0 & (time()-tout)>30000
  idx[1]=(rnd()*7)+1
  mode=1
 endif
timer.resume
onbackkey:
if !mode
 idx[1]=(rnd()*7)+1
 mode=1
elseif mode=9
 mode=tmode
 gr.render
elseif mode=6
 gr.cls
 gr.bitmap.delete tpp
 gr.bitmap.delete tpz
 tout=index(as,pic,idx[])
 mode=0
else
sql.close idx[5]
exit
endif
back.resume
onerror:
end geterror$()
