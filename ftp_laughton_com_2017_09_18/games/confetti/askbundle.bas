REM Start of BASIC! Program
rem options menu using bundles of lists

! display mode
bundle.create globals
Bundle.put globals,"mode","html"
Html.open

! use this if you prefer console mode
FN.DEF oldasklist$(l,msg$,c)
 DIALOG.SELECT c,l,msg$
 IF c THEN LIST.GET l,c,c$
 FN.RTN c$
FN.END

FN.DEF isadigit(s$) 
 FN.RTN IS_IN(s$,"-0123456789.")
FN.END

FN.DEF readnumber(s$) 
 LET N$=""
 FOR i=1 TO LEN(s$) 
  LET C$=MID$(s$,I,1)
  IF isadigit(c$) THEN N$+=c$
 NEXT i
 IF LEN(n$)>0 THEN FN.RTN VAL(n$) 
 FN.RTN 0
FN.END

! display mode
! html,gr(aphics) or console
FN.DEF dmode(m$)
 BUNDLE.GET 1,"mode",oldm$
 IF oldm$=m$ THEN FN.RTN 0
 BUNDLE.PUT 1,"mode",m$
 IF oldm$="html" THEN HTML.CLOSE
 IF oldm$="gr" THEN GR.CLOSE
 IF m$="html" THEN HTML.OPEN 0
 IF m$="gr" THEN GR.OPEN 255,0,0,0,0,1
 PAUSE 300
FN.END

FN.DEF waitclick$()
 DO
  PAUSE 100
  HTML.GET.DATALINK data$
  IF BACKGROUND() 
   cs=CLOCK()
   DO
    PAUSE 100
   UNTIL (CLOCK()-cs)>5000|!BACKGROUND()
   IF BACKGROUND() THEN EXIT
  ENDIF
 UNTIL data$ <> ""
 ! popup data$,0,0,0
 IF IS_IN("BAK:",data$)=1
  EXIT
 ELSEIF IS_IN("DAT:",data$)=1
  data$=MID$(data$,5) %' User link
 ELSEIF IS_IN("LNK:file:///",data$)=1&IS_IN("?",data$) %' Submit link
  i=IS_IN("?",data$)
  data$="SUBMIT&"+MID$(data$,i+1)+"&"
 ENDIF
 FN.RTN data$
FN.END


! show list l with prompt msg$
! c is assigned choice number
! returns l[c]
FN.DEF asklist$(l,msg$,c)
 r$="<br>"
 h$="<!DOCTYPE html><html lang=~en~>"
 h$+="<h1 style=~color:#fff;font-size:18px;~>"+msg$ +"</h1>"
 h$+="<body style=~background-color:#000; text-align:center;font-size:10;~>"
 LIST.SIZE l,z
 h$+="<div>"
 FOR i=1 TO z
  LIST.GET l,i,s$
  s$=REPLACE$(s$,"'","&apos;")
  but$="<button type=~button~  style=~background-color:#030;color:#0F0;text-align:center;font-size:20px;width:300px;height:50px;moz-border-radius: 15px; -webkit-border-radius: 15px;~ onclick=~doDataLink('"+STR$(i)+"')~;>"+s$+"</button><br>"
  h$+=but$
 NEXT i
 h$+="</div>"
 h$+="<script type=~text/javascript~>"
 h$+="function doDataLink(data) "
 h$+="{Android.dataLink(data);}</script>"
 h$+="</body>"
 h$+="</html>"
 h$=REPLACE$(h$,"~","\"")
 dmode("html")
 HTML.LOAD.STRING h$
 DO
  r$=waitclick$()
  c=readnumber(r$)
 UNTIL c>0
 !popup r$
 LIST.GET l,c,s$
 FN.RTN s$
FN.END

fn.def askbundle(msg$,b,choices)
bundle.keys b,keys
list.size keys,z
list.create s,l
do
List.clear l
for i=1 to z
list.get keys,i,k$
list.get choices,i,cc
bundle.get b,k$,cl
list.get cl,cc,s$
list.add l,k$+":"+s$
next
list.add l,"ok"
list.add l,"cancel"
c=0
o$=asklist$(l,msg$,&c)
if c>=1 & c<=z
list.get keys,c,k$
bundle.get b,k$,ll
list.size ll,zz
list.get choices,c,j
j++
if j>zz then j=1
list.replace choices,c,j
endif
until o$="ok" | o$="cancel"
If o$="cancel" then fn.rtn -1
fn.end

fn.def getbchoice$(b,c,o$)
bundle.keys b,keys
bundle.get b,o$,l
list.search keys,o$,i
list.get c,i,v
list.get l,v,s$
fn.rtn s$
fn.end

fn.def testaskb()
bundle.create b
list.create s,l0
list.add l0,"1","5","10","50","100","200","300","400","500"
bundle.put b,"size",l0
list.create s, l1
list.add l1,"circle","square"
bundle.put b,"shape",l1
list.create s, l2
list.add l2,"zigzag","normal"
bundle.put b,"motion",l2
list.create s, l3
list.add l3,"opaque","glass"
bundle.put b,"color",l3
list.create n,choices
! set defaults/responses
list.add choices,1,1,1,1

askbundle("select options",b,choices)

print getbchoice$(b,choices,"color")
print getbchoice$(b,choices,"motion")
print getbchoice$(b,choices,"size")
print getbchoice$(b,choices,"shape")


fn.end

!Testaskb()
