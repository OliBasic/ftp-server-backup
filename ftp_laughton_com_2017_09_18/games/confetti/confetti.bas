! confetti demo
!
!
INCLUDE "askbundle.bas"

fn.def lin(x)
let xx=mod(x,6)
if xx>3 then let xx=6-xx
fn.rtn xx
fn.end

Dmode("html")
bundle.create b
list.create s,l0
list.add l0,"200","300","400","500","1","5","10","50","100"
bundle.put b,"pixel",l0
list.create s, l1
list.add l1,"square","circle"
bundle.put b,"shape",l1
list.create s, l2
list.add l2,"normal","zigzag"
bundle.put b,"motion",l2
list.create s, l3
list.add l3,"glass","opaque"
bundle.put b,"color",l3
list.create s, l4
list.add l4,"fixed","variable"
bundle.put b,"size",l4


list.create n,choices

list.add choices,1,1,1,1,1

X=askbundle("select options",b,choices)

If x=-1 then exit

nm=(getbchoice$(b,choices,"motion")="normal")
grd=(getbchoice$(b,choices,"color")="glass")

S$=getbchoice$(b,choices,"pixel")
If is_number(s$)
Pz=val(s$)
Else
Pz=200
Endif

Crc=(getbchoice$(b,choices,"shape")="circle")
Mix=(getbchoice$(b,choices,"size")="variable")

Dmode("gr")

!GR.OPEN 255,0,0,0,0
GR.ORIENTATION 1
gr.set.antialias 1
GR.SCREEN w,h
w+=5:h+=5:e=-5
POPUP "touch or drag screen to create moving dots"
LIST.CREATE n,l :LIST.CREATE n,lvx :LIST.CREATE n,lvy
GR.SET.STROKE pz
GR.SET.ANTIALIAS 0
br=60:hh=99:p5=0.5:a=255:mv=10
GR.RECT screen,0,0,w,h

DO
 GR.TOUCH t,x,y
 IF t
  IF grd 
   xc+=0.1:GR.COLOR 50,hh*(1+lin(xc))+br,hh*(1+lin(xc*1.2))+br,hh*(1+lin(xc*1.3))+br
  ELSE
  GR.COLOR a,RND()*hh+br, RND()*hh+br,RND()*hh+br
 ENDIF

let sz=pz
  IF mix THEN let sz*=rnd()
if crc
gr.circle p,x,y,sz
else
GR.SET.STROKE sz:GR.POINT p,x,y 
endif
  LIST.ADD l,p:LIST.ADD lvx, SGN(RND()-0.5)*(RND()*mv+1):LIST.ADD lvy, SGN(RND()-0.5)*(RND()*mv+1) 
 ENDIF

 LIST.SIZE l,z:max = z
 FOR i=1 TO z

  ii = MIN(max,i):LIST.GET l,ii,q

  IF GR_COLLISION(q,screen) 
   LIST.GET lvx,ii,dx:LIST.GET lvy,ii,dy:GR.MOVE q,dx,dy
  ELSE
   GR.HIDE q:LIST.REMOVE l,ii   
   IF nm
    LIST.REMOVE lvx,ii:LIST.REMOVE lvy,ii
   ENDIF
   max--
  ENDIF
 NEXT

 GR.RENDER

UNTIL 0
