REM --- Basic! program for android
REM --- title: Softlanding game
REM --- version:
ver$="ver. 1.012"
REM --- original by SHARP PC-1401
REM --- (c) 2013, refo
REM --- history of versions:
REM --- ver. 1.007 - funkcni
REM --- ver. 1.009 - pridano vstup hodnot
REM --- ver. 1.012 - uprava pro rfo-basic forum
REM
!!
This is retro game from year around 1985, 
I tried to keep the spirit of the game - 
it look so simple, as on the original 
computer on an one-line display.

In the code I preserved the structure of original 
algorithm with GOTO statements and labels.

Control game by pressing key 0-9 and watching height,
speed and fuel. You will have success landing with 
speed lower then 5 m/s, in other case you die on planet
or fly out and be lost in the space. 
You can change initial values and try different simulations.

It work at Portrait Orientation and was tested on mobile 
phone SonyEricsson Xperia with resolution around 800x600.

Put file softlanding_bg.jpg to directory ../rfo-basic/data
and file Softlanding.bas to ../rfo-basic/source.
!!
!Introduction
!------------
pp=1300
print "*** Softlanding game "+ver$+" ***"
PAUSE pp
print "Dedicated to memory of pocket computer Sharp PC-1401"
PAUSE pp
print "from year 1985, programmable in BASIC."
PAUSE pp
print "The rocket is in free fall,"
PAUSE pp
print "land the rocket on planet as softly possible"
PAUSE pp
print "by controlling the engines while watching"
PAUSE pp
print "how much fuel is burned."
PAUSE pp
print "Enjoy!"
PAUSE pp
CLS

! Set the device independent sizes
di_height = 800
di_ width = 480

! get the actual width and height
Gr.open 255, 100, 100, 100, 0, 1
Gr.orientation 1
gr.screen actual_w, actual_h

! calculate the scale factors
scale_width = actual_w /di_width 
scale_height = actual_h /di_height 

! Set the scale
gr.scale scale_width, scale_height
Gr.bitmap.create softlanding_bg, 800, 480
gr.bitmap.load softlanding_bg, "softlanding_bg.jpg" %pozadi
gr.bitmap.draw softlanding_bg, softlanding_bg, 0, 0
gr.render

gr.color 255,0,0,0,0
gr.text.size 40
gr.text.align 1
ss$=""
gr.text.draw text,50,150,ss$
gr.render

noveHodnoty=0

L10:
!------
S=-50
A=0
D$=""
C=A
ss$="NEW VALUES (Y/N)?"
gr.modify text,"text",ss$
gr.render
DO
 GOSUB VolbaTlacitka
 Z$=Tlacitko$
UNTIL Z$="Y" | Z$="N"
if Z$="Y"
 noveHodnoty=1
 gr.front 0
 INPUT "WAIT (ms)", WW, 500
 INPUT "FUEL (kg)", FF, 200
 INPUT "HEIGHT (m)", HH, 500
 gr.front 1 
endif

!Initial values
!--------------
if noveHodnoty=0
	W=500 %WAIT (ms)
	F=200 %FUEL
	H=500 %HEIGHT
else
	W=WW %WAIT (ms)
	F=FF %FUEL
	H=HH %HEIGHT
endif	

ss$=" ***START***"
gr.modify text,"text",ss$
gr.render
PAUSE 2000


L70:
!------
if F=0 & S<0
	ss$=Format$("####", H)+Format$("####", S)+" FALLING"
	gr.modify text,"text",ss$
	gr.render
else
	ss$=Format$("####%", H)+Format$("###%", S)+Format$("###%", F)+Format$("##%", C)  
	gr.modify text,"text",ss$
	gr.render
endif
PAUSE W

GOSUB VolbaTlacitka
D$=Tlacitko$
if D$="" | D$="Y" | D$="N"
	C=A
else
	C=val(D$)
	A=C	
endif

if C>F
	C=F
endif

!Equations
!Gravity is set to be 5m/(unit time)^2
!------------------------------------
F=F-C
X=C-5
H=H+S+X/2
S=S+X

if H>5 THEN GOTO L70
if H>0 & ABS(S)<5
	ss$="SUCCES !!"
	gr.modify text,"text",ss$
	gr.render
	PAUSE 2000
	!ss$="FUEL F="+str$(F)
	ss$="FUEL F="+Format$("####", F)
	gr.modify text,"text",ss$
	gr.render
else
	ss$="GOOD BY !!"
	gr.modify text,"text",ss$
	gr.render
endif	

PAUSE 2000
ss$="REPLAY (Y/N) ?"
gr.modify text,"text",ss$
gr.render
DO
 GOSUB VolbaTlacitka
 Z$=Tlacitko$
UNTIL Z$="Y" | Z$="N"
if Z$="Y" THEN GOTO L10

gr.close
!For standalone running of program deactivate END and activate EXIT:
END
!EXIT
	
VolbaTlacitka:
!------------
!Detection of pressing key on virtual keyboard
!Virtual Keyboard 0-9, Y,N
konecTlac=0
Tlacitko$=""
ii=0
DO
ii=ii+1
gr.bounded.touch touched, 202*scale_width, 698*scale_height, 281*scale_width, 745*scale_height %--- volba Y
if touched
	Tlacitko$="Y"
	GOSUB Vibrace1	
	konecTlac=1
	PAUSE 100
endif
gr.bounded.touch touched, 328*scale_width, 698*scale_height, 407*scale_width, 745*scale_height %--- volba N
if touched
	Tlacitko$="N"
	GOSUB Vibrace1	
	konecTlac=1
	PAUSE 100
endif
gr.bounded.touch touched, 73*scale_width, 698*scale_height, 152*scale_width, 745*scale_height %--- volba 0
if touched
	Tlacitko$="0"
	GOSUB Vibrace1	
	konecTlac=1
	PAUSE 100
endif
gr.bounded.touch touched, 73*scale_width, 617*scale_height, 152*scale_width, 662*scale_height %--- volba 1
if touched
	Tlacitko$="1"
	GOSUB Vibrace1	
	konecTlac=1
	PAUSE 100
endif
gr.bounded.touch touched, 202*scale_width, 617*scale_height, 282*scale_width, 662*scale_height %--- volba 2
if touched
 	Tlacitko$="2"
	GOSUB Vibrace1	
	konecTlac=1
	PAUSE 100
endif 
gr.bounded.touch touched, 328*scale_width, 617*scale_height, 407*scale_width, 662*scale_height %--- volba 3
if touched
 	Tlacitko$="3"
	GOSUB Vibrace1	
	konecTlac=1
	PAUSE 100
endif 
gr.bounded.touch touched, 73*scale_width, 541*scale_height, 152*scale_width, 586*scale_height %--- volba 4
if touched
 	Tlacitko$="4"
	GOSUB Vibrace1	
	konecTlac=1
	PAUSE 100
endif 
gr.bounded.touch touched, 202*scale_width, 541*scale_height, 282*scale_width, 586*scale_height %--- volba 5
if touched
 	Tlacitko$="5"
	GOSUB Vibrace1	
	konecTlac=1
	PAUSE 100
endif 
gr.bounded.touch touched, 328*scale_width, 540*scale_height, 408*scale_width, 586*scale_height %--- volba 6
if touched
 	Tlacitko$="6"
	GOSUB Vibrace1	
	konecTlac=1
	PAUSE 100
endif 
gr.bounded.touch touched, 73*scale_width, 462*scale_height, 153*scale_width, 507*scale_height %--- volba 7
if touched
 	Tlacitko$="7"
	GOSUB Vibrace1	
	konecTlac=1
	PAUSE 100
endif 
gr.bounded.touch touched, 202*scale_width, 462*scale_height, 282*scale_width, 507*scale_height %--- volba 8
if touched
 	Tlacitko$="8"
	GOSUB Vibrace1	
	konecTlac=1
	PAUSE 100
endif 
gr.bounded.touch touched, 328*scale_width, 461*scale_height, 407*scale_width, 507*scale_height %--- volba 9
if touched
	Tlacitko$="9"
 	GOSUB Vibrace1	
 	konecTlac=1
 	PAUSE 100
endif  
if ii>50 then konecTlac=1
UNTIL konecTlac=1
RETURN

Vibrace1:
!--------
 !One time vibrate after pressing key
 !1x zavibruje pri zmacknuti tlacitka
 !v array ve vzor pro vibraci: pausa, vibrace, pausa ... v ms
 ARRAY.LOAD Pattern[], 1, 30 
 VIBRATE Pattern[], -1
 UnDim Pattern[]
RETURN

