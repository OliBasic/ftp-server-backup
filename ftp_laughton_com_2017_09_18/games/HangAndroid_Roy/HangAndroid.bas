Rem Hang Android
Rem For Android
Rem With RFO Basic!
Rem January 2017
Rem Version 1.00
Rem By Roy Shepherd

di_height = 1150
di_width = 670

gr.open 255, 244, 17, 114
gr.orientation 1 % portrait      
pause 1000
WakeLock 3
 
gr.screen screenWidth, screenHeight
scale_x = screenWidth / di_width
scale_y = screenHeight / di_height
gr.scale scale_x, scale_y

gosub Functions
gosub Initialise
gosub LoadWords

gosub SetUp

do
    gosub KeyPressed 
    gosub OptionsMenu
    Call Render()

until 0

onBackKey:
    yn = ShowDialogBox(scale_x, scale_y, collision, dialogPtr[], "Exit Hang Android")
    if yn=1 then gosub SaveData : pause 100 : exit
back.resume 

!------------------------------------------------
! Do once at first run of game
!------------------------------------------------
Initialise:
    FALSE = 0 : TRUE = 1
    solved = 0 : hung = 0
    sound = True : buzz = TRUE
    randomWordLen = TRUE
    usersWord = FALSE
    alphabet$ ="qwertyuiopasdfghjklzxcvbnm"
    dash$ = "------------"
    path$ = "HangAndroid/data/"
    gosub LoadData
    
    ! Setup colours for keyboard
    gr.color 150, 255, 255, 255 : gr.paint.get white
    gr.color 150, 0, 0, 0       : gr.paint.get black
    gr.color 255, 255, 255, 255
    
    ! set typeface. Load and draw keyboard
    gr.text.typeface 2
    gr.bitmap.load k, path$ + "k7.png"
    if k = -1 then call ImageLoadError("Keyboard")
    gr.bitmap.draw keyBoard, k, 0, di_height - 250
    
    ! Load Android man
    gr.bitmap.load man, path$ + "androidMan.png"
    if man = -1 then call ImageLoadError("androidMan")
    gr.bitmap.scale man, man, 100, 150
    
    ! Heading 
    gr.color 255, 100, 100, 100
    gr.rect null , 0, 0, di_width, 50 
    gr.color 255, 255, 0, 0
    gr.line null , 0, 50, di_width, 50
    gr.color 255, 255, 255, 255 : gr.text.size 30
    gr.text.align 2 : gr.text.draw null, di_width / 2, 30, "Hang Android"
     
    ! Load & draw options list button
    gr.bitmap.load op, path$ + "optionList.png"
    if op = -1 then call ImageLoadError("optionList")
    gr.bitmap.draw butOption, op, di_width - 50, 0
   
   ! Draw Text
    gr.text.align 1 : gr.text.draw txtSolved, 10,100, "Solved: " + int$(solved)
    gr.text.align 3 : gr.text.draw txtHung, di_width - 10,100, "Hung: " + int$(hung)
    gr.text.draw txtGuessesLeft, di_width - 10,150, "Guesses Left: " + int$(guessesLeft)
    gr.text.align 1 : gr.text.draw txtwordLen, 10, 150, "Word Length: " + int$(wordLength)
    gr.text.align 2 
    gr.text.draw txtStartGuessing, di_width / 2, di_height / 2, "Start Guessing"
    gr.text.size 80
    gr.text.draw txtGuessWord, di_width / 2, di_height - 270, ""
    
    !gr.text.draw txtThisWord, di_width / 2, di_height - 370, "" % used for testing
    
    ! Setup for collision for keyboard and options menu 
    gr.color 0,0,0,0
    gr.point collision, -1, -1
    gr.color 255, 255, 200, 200, 1
    
    ! Load Sounds
    soundpool.open 4
    soundpool.load soundRight,path$ + "right.wav"
    if ! soundRight then call SoundLoadError("right")

    soundpool.load soundWrong, path$ + "Wrong.wav"
    if ! soundWrong then call SoundLoadError("wrong")
    
    soundpool.load soundWinner, path$ + "winner.wav"
    if ! soundWinner then call SoundLoadError("winner")
    
    soundpool.load soundHung, path$ + "hung.wav"
    if ! soundHung then call SoundLoadError("hung")
    
    ! buzz
    array.load buzzGame[], 1, 100
    
return

!------------------------------------------------
! Do at start of each new game
!------------------------------------------------
SetUp:
    if ! usersWord then
        List.size appWords, numOfWords
        if randomWordLen then 
            do
                thisWord = floor(numOfWords * rnd()) + 1
                List.get appWords, thisWord, wordToGuess$
            until len(wordToGuess$) < 11 & len(wordToGuess$) > 3
        else 
            do 
                thisWord = floor(numOfWords * rnd()) + 1
                List.get appWords, thisWord, wordToGuess$
            until len(wordToGuess$) = wordLength
        endif
    endif
    gr.show txtStartGuessing
    gr.hide allHangSteps
    hangingStep = 0
    wordLength = len(wordToGuess$)
    guessesLeft = 8
    myGuess$ = left$(dash$, wordLength)
    gr.modify txtGuessWord, "text", myGuess$
    gr.modify txtGuessesLeft, "text", "guesses Left: " + int$(guessesLeft)
    gr.modify txtwordLen, "text", "Word Length: " + int$(wordLength)
    ! gr.modify txtThisWord, "text", wordToGuess$ % used for testing
    Call Render()
    
    gosub DrawKeyboardButtons
    gosub DrawDialogBox
    gosub DrawAboutBox
    gosub DrawHangAndroid
return

!------------------------------------------------
! Draw Keyboard collision  Buttons:
!------------------------------------------------
DrawKeyboardButtons:
    dim keyBoardButtons[26] : button = 1 
    dim usedLetter[26] : y = di_height - 215 
    
    gr.color 0, 255, 255, 255
    for x = 35 to di_width step di_width / 10
        gr.circle keyBoardButtons[button], x, y, 25
        gr.circle usedLetter[button], x, y, 25
        button ++
    next
    y += 65
    for x = 35 to di_width - 65 step (di_width / 10)
        gr.circle keyBoardButtons[button], x, y, 25
        gr.circle usedLetter[button], x, y, 25
        button ++
    next
    
    y += 70
    for x = 100 to di_width - 130 step (di_width / 10)
        gr.circle keyBoardButtons[button], x, y, 25
        gr.circle usedLetter[button], x, y, 25
        button ++
    next
    gr.color 255, 255, 255, 255
return

!------------------------------------------------
! Draw Dialog Box
!------------------------------------------------
DrawDialogBox:
    dWidth = di_width / 2 : dHeight = di_height / 2
    y = 600 : s = 180
    dim dialogPtr[10]
    gr.color 255, 255, 255, 255
    gr.rect dialogPtr[1], dWidth - s, y, dWidth + s, y + 170
    gr.color 255,0,0,0 : gr.text.align 2 : gr.text.size 30
    
    gr.text.draw dialogPtr[2], dWidth, y + 50, ""
    gr.text.draw dialogPtr[3], dWidth, y + 100, "Guess Another Word"
    gr.text.draw dialogPtr[4], dWidth - (s / 2), y + 150, "Yes"
    gr.text.draw dialogPtr[5], dWidth + (s / 2), y + 150, "No"
        
    gr.color 255,187,187,187
   
    gr.line dialogPtr[6], dWidth - s, y + 115, dWidth + s, y + 115
    gr.line dialogPtr[7], dWidth, y + 115, dWidth, y + 170
    
    gr.color 0,0,0,0
    gr.rect dialogPtr[8], dWidth - s, y + 115, dWidth, y + 170
    gr.rect dialogPtr[9], dWidth, y + 115, dWidth + s, y + 170
    
    gr.color 255, 0, 0, 0
    for m = 1 to 9
        gr.hide dialogPtr[m]
    next
  
return

!------------------------------------------------
! As any key on the on screen keyboard been taper  
! If the letter appears in the word that is being guessed 
! then add the letters to myGuess$ 
!------------------------------------------------
KeyPressed:
    gr.touch t, tx, ty 
    if ! t then return 
    tx /= scale_x : ty /= scale_y
    if ty < di_height - 250 then return
    gr.modify collision, "x", tx, "y", ty 
    guessedRight = FALSE 
    gr.hide txtStartGuessing
    for x = 1 to 26 
        if gr_collision(collision, keyBoardButtons[x]) then 
            if buzz then vibrate buzzGame[], -1
            for letter = 1 to len(wordToGuess$)
                if mid$(alphabet$, x, 1) = mid$(wordToGuess$, letter, 1) then
                    call PlaySound(sound, soundRight)
                    myGuess$ = left$(myGuess$, letter - 1) + mid$(wordToGuess$, letter, 1) + mid$(myGuess$, letter + 1)
                    gr.modify txtGuessWord, "text", myGuess$
                    guessedRight = TRUE
                    gr.show usedLetter[x]
                    gr.modify usedLetter[x], "paint", white
                    gr.hide keyBoardButtons[x]
                endif
            next
            if ! guessedRight then 
            call PlaySound(sound, soundWrong)
                guessesLeft -- 
                gr.show usedLetter[x]
                gr.modify usedLetter[x], "paint", black
                gr.modify txtGuessesLeft, "text", "guesses Left: " + int$(guessesLeft)
                gr.hide keyBoardButtons[x]
                hangingStep ++ 
                gosub HangAndroid
            endif
        endif
        if guessedRight then f_n.break
    next
    if myGuess$ = wordToGuess$ then gosub WordGuessed
    if myGuess$ <> wordToGuess$ & guessesLeft = 0 then gosub AndroidHung
    do : gr.touch t, null, null : until ! t
return

!------------------------------------------------
! You guessed the word
!------------------------------------------------
WordGuessed:
    call PlaySound(sound, soundWinner)
    solved ++
    gr.modify txtSolved, "text", "Solved: " + int$(solved)
    Call Render()
    yn = ShowDialogBox(scale_x, scale_y, collision, dialogPtr[], "Success, Well Done!")
    if yn = 2 then gosub SaveData : pause 100 : exit
    gosub NewWord
return

!------------------------------------------------
! You didn't guess the word
!------------------------------------------------
AndroidHung:
    call PlaySound(sound, soundHung)
    hung ++
    myGuess$ = wordToGuess$
    gr.modify txtHung, "text", "Hung: " + int$(hung)
    gr.modify txtGuessWord, "text", myGuess$
    Call Render()
    yn = ShowDialogBox(scale_x, scale_y, collision, dialogPtr[], "Android is hung")
    if yn = 2 then gosub SaveData : pause 100 : exit
    gosub NewWord
return

!------------------------------------------------
! You guessed the word
!------------------------------------------------
NewWord:
    for x = 1 to 26
        gr.hide usedLetter[x]
        gr.show keyBoardButtons[x]
    next
    gosub SetUp
return

!------------------------------------------------
! Draw About box
!------------------------------------------------
DrawAboutBox:
    aboutText$="Hang Android-January 2017-Version 1.00-Author: Roy Shepherd-OK"

    dWidth = di_width / 2 : dHeight = di_height / 2
    
    split aboutMenu$[], aboutText$ , "-"
    array.length aboutItems, aboutMenu$[]

    wordLen = len(aboutMenu$[1])
    for m = 1 to aboutItems
        if m > 1 then
            if len(aboutMenu$[m]) > wordLen then wordLen = len(aboutMenu$[m])
        endif
    next

    menuWidth = wordLen * 20 : menuDrop = aboutItems * 40
    dim aboutPtr[ aboutItems + 2]
    gr.color 255, 255, 255, 255
    gr.rect aboutPtr[1],dWidth - (menuWidth / 2), dHeight - (menuDrop / 2), dWidth + (menuWidth / 2), dHeight + (menuDrop / 2)
    gr.color 255,0,0,0 : gr.text.align 2 : gr.text.size 30
    
    textY = dHeight - (menuDrop / 2) + 30
    for m = 1 to aboutItems
        gr.text.draw aboutPtr[m + 1], dWidth, textY, aboutMenu$[m]
        textY += 40
    next
   
    gr.color 255,187,187,187
   
    gr.line aboutPtr[aboutItems + 2], dWidth - (menuWidth / 2), textY - 70, dWidth + (menuWidth / 2), textY - 70
    
    gr.color 255, 0, 0, 0
    for m = 1 to aboutItems + 2
        gr.hide aboutPtr[m]
    next
  
return

!------------------------------------------------
! Show About Box
!------------------------------------------------
ShowAboutBox:
    for m = 1 to aboutItems + 2
        gr.show aboutPtr[m]
    next
    Call Render() 
    
    do : gr.touch t, tx, ty : until ! t
    do : gr.touch t, tx, ty : until t
    do : gr.touch t, tx, ty : until ! t
    
    for m = 1 to aboutItems + 2
        gr.hide aboutPtr[m]
    next
    Call Render()
return

!----------------------------------------------------
! Options Menu
!----------------------------------------------------
OptionsMenu:
    gr.touch t, tx, ty 
    if ! t then return 
    tx /= scale_x : ty /= scale_y
    if ty > 50 then return
    gr.modify collision, "x", tx, "y", ty 
    if ! gr_collision(collision, butOption) then return
    if buzz then vibrate buzzGame[], -1
    
    if sound then sound$="Sound On" else sound$ = "Sound Off"
    if buzz then buzz$="Buzz On" else buzz$ = "Buzz Off"
	
    array.load menu$[],~
            sound$ ~
            buzz$,~
            "Word Length",~
            "Enter own Word"~
            "About",~
            "Help",~
            "Exit"
    dialog.select menuSelected, menu$[]

    sw.begin menuSelected
  
    sw.case 1
			if sound then sound = FALSE else sound = TRUE
		Sw.break

		sw.case 2
			if buzz then buzz = FALSE else buzz = TRUE
		Sw.break
    
    sw.case 3
        gosub NewWordLength
    sw.break
    
    sw.case 4
        gosub UserEnterWord
    sw.break
    
		sw.case 5
        gosub ShowAboutBox
		sw.break
		
		sw.case 6
        call Help()
		sw.break
    
		sw.case 7
        yn = ShowDialogBox(scale_x, scale_y, collision, dialogPtr[], "Exit Hang Android")
        if yn=1 then gosub SaveData : pause 100 : exit
		sw.break
    
	  sw.end
return

!----------------------------------------------------
! Select a new word length, to take effect at the start of a new word
!----------------------------------------------------
NewWordLength:
    randomWordLen = False
    array.load newLength$[],~
            "Three", ~
            "Four", ~
            "Five",~
            "Six",~
            "Seven", ~
            "Eight",~
            "Nine",~
            "Ten",~
            "Random Length"
    dialog.select newLength, newLength$[], "Select New Word Length"
    if newLength = 0 then return
    
    if newLength = 9 then randomWordLen = TRUE : gosub NewWord : return
    
    wordLength = newLength + 2
    gosub NewWord
    
return

!----------------------------------------------------
! Load the words
!----------------------------------------------------
LoadWords:
    text.open r, fileNumber, path$ + "hangWords1.txt"
    if fileNumber = -1 then dialog.message"Hang Words Not Found",,ok,"ok" : end
    List.create s, appWords
    do
        text.readln fileNumber, line$
        if line$ <> "EOF" then 
            l = len(line$)
            if l > 2 & l < 11 then
                List.add appWords, lower$(line$)
            endif
        endif
    until line$ = "EOF"
    text.close fileNumber
    
return

!------------------------------------------------
! Save sound, Buzz and word length
!------------------------------------------------
SaveData:
    file.exists pathPresent,path$ + "GameData.txt"
    if ! pathPresent then file.mkdir path$

    text.open w,hs,Path$ + "GameData.txt"
    text.writeln hs,int$(sound)
    text.writeln hs,int$(buzz)
    if randomWordLen then 
        text.writeln hs,int$(11)
    else
        text.writeln hs,int$(wordLength)
    endif
    text.close hs
return

!------------------------------------------------
! Load sound, Buzz and word length
!------------------------------------------------
LoadData:
    file.exists pathPresent,path$  +"GameData.txt"
    if pathPresent then
        text.open r,hs,path$ + "GameData.txt"
        text.readln hs,sound$
        text.readln hs,buzz$
        text.readln hs,wordLength$
        text.close hs
        sound = val(sound$)
        buzz = val(buzz$)
        wordLength = val(wordLength$)
        if wordLength = 11 then 
            randomWordLen = TRUE
        else 
            randomWordLen = FALSE
        endif
    endif
return

!------------------------------------------------
! Draw the eight steps to hang Android
!------------------------------------------------
DrawHangAndroid:
    dim hangStep[7]
    w = di_width : h = di_height
    gr.color 255, 100, 100, 100
    gr.rect hangStep[1], w / 2, h / 2, w, (h / 2) - 20
    gr.rect hangStep[2], w - 50, h / 2, w - 30, (h / 2)- 300
    gr.rect hangStep[3], w - 30, (h / 2)- 300, w - 200, (h / 2)- 320 
    gr.set.stroke 4
    gr.line hangStep[4], w - 130, (h / 2)- 300, w - 30, (h / 2) - 200
    gr.rect hangStep[5], w - 200, h / 2, w - 100, (h / 2) - 100
    gr.bitmap.draw hangStep[6], man, w - 200, (h / 2) - 235
    gr.set.stroke 2
    gr.line hangStep[7], w - 147, (h / 2)- 300, w - 147, (h / 2) - 220
    ! Step 8 will hide hangStep[5]. Android man is hung
    gr.group allHangSteps, hangStep[1], hangStep[2], hangStep[3], hangStep[4], hangStep[5], hangStep[6], hangStep[7] 
    gr.hide allHangSteps
return

!------------------------------------------------
! Eight steps to hang Android
!------------------------------------------------
HangAndroid:
    if hangingStep = 8 then gr.hide hangStep[5] : return
    gr.show hangStep[hangingStep]
return

!------------------------------------------------
! Used enters there own word to be guessed
!------------------------------------------------
UserEnterWord:
    badWord = FALSE
    input "Enter your word (3 to 10) letters", ownWord$, "", cancel 
    ownWord$ = lower$(ownWord$)
    ownWord$ = TRIM$(ownWord$) 
    for x = 1 to len(ownWord$)
        asc = ascii(mid$(ownWord$, x, 1))
        if asc < 97 | asc > 122 then badWord = TRUE
    next
    if len(ownWord$) > 2 & len(ownWord$) < 11 & ! badWord then
        wordToGuess$ = ownWord$
        usersWord = TRUE
    else
        dialog.message "(" + ownWord$ +")", "Word as to be (3 to 10) letters",OK,"OK"
        usersWord = FALSE
    endif
    gosub NewWord
    usersWord = FALSE
return

!------------------------------------------------
! All functions below
!------------------------------------------------
Functions:

!------------------------------------------------
! Show Dialog Box
!------------------------------------------------
fn.def ShowDialogBox(scale_x, scale_y, collision, dialogPtr[], gameResult$)
    flag = 0
    for m = 1 to 9
        gr.show dialogPtr[m]
    next
    gr.modify dialogPtr[2], "text", gameResult$
    if gameResult$ = "Exit Hang Android" then gr.modify dialogPtr[3], "text", "" 
    Call Render() 
    
    do : gr.touch t, tx, ty : until ! t
    do
        do : gr.touch t, tx, ty : until t
        do : gr.touch t, tx, ty : until ! t
        tx /= scale_x : ty /= scale_y
        
        gr.modify collision, "x", tx, "y", ty
        if gr_collision(collision, dialogPtr[8]) then flag = 1 % Yes
        if gr_collision(collision, dialogPtr[9]) then flag = 2 % No
    until flag

    for m = 1 to 9
        gr.hide dialogPtr[m]
    next
    Call Render()
    fn.rtn flag
fn.end
!----------------------------------------------------
! Display game Help
!----------------------------------------------------
    fn.def Help()
        h$ = "Playing Hang Android:\n\n"
        
        h$ = h$ + "Your device choose a secret word"
        h$ = h$ + " and draws a blank line for each letter in the word \n\n"

        h$ = h$ + "You start guessing letters, if you guess correctly your letters fill in the blanks." 
        h$ = h$ + " When you guess wrong, part of the 'Hang Android' is drawn.\n\n"
           
        h$ = h$ + "If Android hangs before you complete the word, all the blanks are filled in with"
        h$ = h$ + " the word you where trying to guess.\n\n"
        
        h$ = h$ + "From the options menu (top right) you can set the word length from 3 to 10 letters."
        h$ = h$ + "The default is Random Length.\n\n"
        
        h$ = h$ + "Also from the options menu you can select 'Enter own Word' and enter a word from"
        h$ = h$ + " 3 to 10 letters, then someone else can try and guess it. If they are not getting"
        h$ = h$ + " the right word, you could give them clues.\n\n"
        
        array.load info$[],h$
        dialog.select null, info$[]
        array.delete info$[]
        
    fn.end
    
!------------------------------------------------
! Render if not in background
!------------------------------------------------
    fn.def Render()
        if ! background() then gr.render
    fn.end

!------------------------------------------------
! if sound is on then play sound (ptr)
!------------------------------------------------
  fn.def PlaySound(sound, ptr)
      if sound then soundpool.play s,ptr,0.99,0.99,1,0,1
  fn.end
  
!------------------------------------------------
! load Sound files not found
!------------------------------------------------
    fn.def SoundLoadError(e$)
        dialog.message "Sound file " + e$ + " not found", "Game will end",OK,"OK" 
        end
    fn.end
    
!------------------------------------------------
! Load image file not found
!------------------------------------------------
    fn.def ImageLoadError(e$)
        dialog.message "Image file " + e$ + " not found", "Game will end",OK,"OK" 
        end
    fn.end
    
return

