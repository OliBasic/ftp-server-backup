! Slotmachine
! Aat Don @2013
GR.OPEN 255, 255, 255, 255
GR.ORIENTATION 0
GR.SCREEN w,h

LeftMargin = 80
TopMargin=51
DIM COINS[4]
GR.BITMAP.LOAD fruit0, "Machine.png"
GR.BITMAP.DRAW p, fruit0,0,0
GR.BITMAP.LOAD coin, "Coin.png"
GR.BITMAP.DRAW COINS[1], coin,23,197
GR.BITMAP.DRAW COINS[2], coin,23,172
GR.BITMAP.DRAW COINS[3], coin,23,147
GR.BITMAP.DRAW COINS[4], coin,23,122
GR.BITMAP.LOAD win, "Win.png"
GR.BITMAP.DRAW popwin, win,63,137
GR.HIDE popwin
GR.BITMAP.LOAD fruit1, "Reel01.png"
GR.BITMAP.LOAD fruit2, "Reel02.png"
GR.BITMAP.LOAD fruit3, "Reel03.png"

GR.COLOR 150, 255, 0, 0, 1
! Hold buttons
GR.RECT Hold1,91,113,91+38,113+16
GR.RECT Hold2,142,113,142+38,113+16
GR.RECT Hold3,192,113,192+38,113+16
GR.COLOR 255, 255, 255, 255, 0
GR.HIDE HOLD1
GR.HIDE HOLD2
GR.HIDE HOLD3
! Hold buttons hide
GR.COLOR 255, 0, 128, 0, 1
GR.RECT NoHold1,91,113,91+38,113+16
GR.RECT NoHold2,142,113,142+38,113+16
GR.RECT NoHold3,192,113,192+38,113+16
GR.COLOR 255, 255, 255, 255, 0
GR.HIDE NoHOLD1
GR.HIDE NoHOLD2
GR.HIDE NoHOLD3
DIM HoldVal[3]
HoldVal[1]=0
HoldVal[2]=0
HoldVal[3]=0
DIM LUT$[12]
LUT$[1]="7B"
LUT$[2]="49"
LUT$[3]="97"
LUT$[4]="26"
LUT$[5]="18"
LUT$[6]="83"
LUT$[7]="A1"
LUT$[8]="C5"
LUT$[9]="B2"
LUT$[10]="6A"
LUT$[11]="3C"
LUT$[12]="54"
DIM Symbols$[12,3]
Symbols$[1,1]="red"
Symbols$[2,1]="green"
Symbols$[3,1]="yellow"
Symbols$[4,1]="red"
Symbols$[5,1]="green"
Symbols$[6,1]="yellow"
Symbols$[7,1]="yellow"
Symbols$[8,1]="white"
Symbols$[9,1]="red"
Symbols$[10,1]="red"
Symbols$[11,1]="yellow"
Symbols$[12,1]="green"
Symbols$[1,2]="green"
Symbols$[2,2]="red"
Symbols$[3,2]="yellow"
Symbols$[4,2]="green"
Symbols$[5,2]="green"
Symbols$[6,2]="red"
Symbols$[7,2]="red"
Symbols$[8,2]="yellow"
Symbols$[9,2]="yellow"
Symbols$[10,2]="yellow"
Symbols$[11,2]="red"
Symbols$[12,2]="white"
Symbols$[1,3]="yellow"
Symbols$[2,3]="red"
Symbols$[3,3]="yellow"
Symbols$[4,3]="green"
Symbols$[5,3]="white"
Symbols$[6,3]="red"
Symbols$[7,3]="yellow"
Symbols$[8,3]="green"
Symbols$[9,3]="green"
Symbols$[10,3]="red"
Symbols$[11,3]="red"
Symbols$[12,3]="yellow"

Dist=37
PrevCount1=0
PrevCount2=0
PrevCount3=0
CoinsLeft=4
Games=12
WinGames=0
GR.TEXT.SIZE 30
GR.TEXT.BOLD 1
GR.TEXT.DRAW Score,265,200,REPLACE$(FORMAT$("%%", Games)," ","")
GR.TEXT.DRAW WinScore,75,200,REPLACE$(FORMAT$("##", WinGames)," ","")
GR.HIDE WinScore

! 1 piece of fruit is 40 x 37 Pixels
! Total reel is 40 x 444 pixels
! Viewport window (clip) is 51 pixels high

! Clip region for fruit
GR.CLIP Region,LeftMargin,TopMargin,LeftMargin+160,TopMargin+51
GR.BITMAP.DRAW reel1, fruit1,LeftMargin+10, -363
GR.BITMAP.DRAW reel2, fruit2,LeftMargin+60, -363
GR.BITMAP.DRAW reel3, fruit3,LeftMargin+110, -363
GR.RENDER

GOTO Start
DropCoin:
TONE 1200,200
FOR i=1 to 5
	GR.HIDE COINS[1]
	GR.RENDER
	PAUSE 100
	GR.SHOW COINS[1]
	GR.RENDER
	PAUSE 100
NEXT i
GR.HIDE COINS[CoinsLeft]
GR.RENDER
RETURN
DrawScore:
	GR.SHOW popwin
	GR.MODIFY WinScore,"text",REPLACE$(FORMAT$("##", XtraGame)," ","")
	GR.SHOW WinScore
	GR.RENDER
	FOR j=300 TO 700 STEP 100
		TONE j,200
	NEXT j
	FOR j=700 TO 300 STEP -100
		TONE j,200
	NEXT j
	GR.MODIFY Score,"text",REPLACE$(FORMAT$("%%",Games+WinGames)," ","")
	GR.RENDER
	PAUSE 500
	GR.HIDE WinScore
	GR.HIDE popwin
	GR.RENDER
	HoldVal[1]=2
	HoldVal[2]=2
	HoldVal[3]=2
RETURN

Start:
DO
	GOSUB DropCoin
	CoinsLeft=CoinsLeft-1
	! Three times per coin
	FOR i=1 TO 3
		Games=Games-1
WinLoop:		
		If WinGames>0 THEN
			WinGames=WinGames-1
		ENDIF
		GR.HIDE HOLD1
		GR.HIDE HOLD2
		GR.HIDE HOLD3
		Count1=0
		Count2=0
		Count3=0
		! Number of positions to move fast
		NumPos1=(FLOOR(RND()*60)+12)*Dist
		NumPos2=(FLOOR(RND()*60)+12)*Dist
		NumPos3=(FLOOR(RND()*60)+12)*Dist
		! Number of positions to move slowly
		SlowPos1=(FLOOR(RND()*5)+3)*Dist
		SlowPos2=(FLOOR(RND()*5)+3)*Dist
		SlowPos3=(FLOOR(RND()*5)+3)*Dist
		IF HoldVal[1]>0 THEN HoldVal[1]=HoldVal[1]-1
		IF HoldVal[2]>0 THEN HoldVal[2]=HoldVal[2]-1
		IF HoldVal[3]>0 THEN HoldVal[3]=HoldVal[3]-1
		IF HoldVal[1]>0 THEN
			GR.SHOW NoHOLD1
		ELSE
			GR.HIDE NoHOLD1
		ENDIF
		IF HoldVal[2]>0 THEN
			GR.SHOW NoHOLD2
		ELSE
			GR.HIDE NoHOLD2
		ENDIF
		IF HoldVal[3]>0 THEN
			GR.SHOW NoHOLD3
		ELSE
			GR.HIDE NoHOLD3
		ENDIF
		GR.RENDER
		! Wait for SPIN press
		Spin=0
		DO
			DO
				GR.TOUCH touched,x,y
			UNTIL touched
			DO
				GR.TOUCH touched,x,y
			UNTIL !touched
			IF y>=113 & y <=129 THEN
				IF x>=91 & x<= 129 & HoldVal[1]=0 THEN
					GR.SHOW HOLD1
					NumPos1=0
					SlowPos1=0
					GR.RENDER
					Holdval[1]=2
					TONE 500, 200
				ENDIF
				IF x>=142 & x<= 180 & HoldVal[2]=0 THEN
					GR.SHOW HOLD2
					NumPos2=0
					SlowPos2=0
					GR.RENDER
					Holdval[2]=2
					TONE 500, 200
				ENDIF
				IF x>=192 & x<= 230 & HoldVal[3]=0 THEN
					GR.SHOW HOLD3
					NumPos3=0
					SlowPos3=0
					GR.RENDER
					Holdval[3]=2
					TONE 500, 200
				ENDIF
			ENDIF
			IF x>=269 & y>=61 & x<=306 & y<=98 THEN
				Spin=1
			ENDIF
		UNTIL Spin
		
		GR.MODIFY Score,"text",REPLACE$(FORMAT$("%%", Games+WinGames)," ","")
		TONE 300, 200
		! the rounding below has to be done to keep the numbers precise.....
		DO
			IF Count1<NumPos1 THEN
				Count1=ROUND(Count1+37)
			ELSE
				IF Count1<NumPos1+SlowPos1 THEN
					Count1=ROUND((Count1+3.7)*10)/10
				ENDIF
			ENDIF
			GR.MODIFY reel1,"y", -363 + MOD(PrevCount1+Count1,444)
			IF Count2<NumPos2 THEN
				Count2=ROUND(Count2+37)
			ELSE
				IF Count2<NumPos2+SlowPos2 THEN
					Count2=ROUND((Count2+3.7)*10)/10
				ENDIF
			ENDIF
			GR.MODIFY reel2,"y", -363 + MOD(PrevCount2+Count2,444)
			IF Count3<NumPos3 THEN
				Count3=ROUND(Count3+37)
			ELSE
				IF Count3<NumPos3+SlowPos3 THEN
					Count3=ROUND((Count3+3.7)*10)/10
				ENDIF
			ENDIF
			GR.MODIFY reel3,"y", -363 + MOD(PrevCount3+Count3,444)
			GR.RENDER
		UNTIL Count1=NumPos1+SlowPos1 & Count2=NumPos2+SlowPos2 & Count3=NumPos3+SlowPos3
		Symbol1=ROUND(MOD(PrevCount1+Count1,444)/37)+1
		Symbol2=ROUND(MOD(PrevCount2+Count2,444)/37)+1
		Symbol3=ROUND(MOD(PrevCount3+Count3,444)/37)+1
		IF HEX(LEFT$(LUT$[Symbol1],1))=Symbol2 & HEX(RIGHT$(LUT$[Symbol1],1))=Symbol3 THEN
			WinGames=WinGames+12
			XtraGame=12
			GOSUB DrawScore
		ELSE
			IF Symbols$[Symbol1,1]="green" & Symbols$[Symbol2,2]="green" & Symbols$[Symbol3,3]="green" THEN
				WinGames=WinGames+4
				XtraGame=4
				GOSUB DrawScore
			ENDIF
			IF Symbols$[Symbol1,1]="red" & Symbols$[Symbol2,2]="red" & Symbols$[Symbol3,3]="red" THEN
				WinGames=WinGames+3
				XtraGame=3
				GOSUB DrawScore
			ENDIF
			IF Symbols$[Symbol1,1]="yellow" & Symbols$[Symbol2,2]="yellow" & Symbols$[Symbol3,3]="yellow" THEN
				WinGames=WinGames+3
				XtraGame=3
				GOSUB DrawScore
			ENDIF	
		ENDIF
		PrevCount1=ROUND(MOD(PrevCount1+Count1,444))
		PrevCount2=ROUND(MOD(PrevCount2+Count2,444))
		PrevCount3=ROUND(MOD(PrevCount3+Count3,444))
		If WinGames>0 THEN
			GOTO WinLoop
		ENDIF
	NEXT i
UNTIL Games=0
PAUSE 1000
TONE 440,200
! End of Game
GR.COLOR 255,0,0,0,1
GR.RECT p,LeftMargin,TopMargin,LeftMargin+164,TopMargin+52
GR.SET.STROKE 2
GR.COLOR 255,255,255,255,0
GR.TEXT.DRAW Over,LeftMargin,TopMargin,"Game OVER"
FOR i=1 to 40
	GR.MODIFY Over,"y",TopMargin+i
	GR.RENDER
	PAUSE 100
NEXT i
PAUSE 5000
GR.CLOSE
EXIT
