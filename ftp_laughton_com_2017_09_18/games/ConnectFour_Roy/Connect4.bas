Rem Connect four
Rem With RFO Basic!
Rem December 2014/January 2015
Rem Version 1.00
Rem By Roy
Rem
debug.on
console.title"Connect four"
Rem Open Graphics
di_width = 1280 % set to my Device
di_height = 800

gr.open 255,100,100,100 % Black
gr.orientation 0
pause 1000
gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y
gr.text.typeface 1
gr.text.size 30 
wakelock 3
gr.render

Rem Globel Vars
array.load posDrop[],103,220,335,450,565,682 % the vertical hols
array.load moveOrder[],1,2,3,4 % to make game less predictable array is shuffled each game
array.load buzzConnect[], 1, 100
dim connectBG[3] % (Images) 1: Board Background 2: Board Top 3: Menu Screen
dim connect4[3] % (Pointer) 1: Board Background 2: Board Top 3: Menu Screen
dim counterOne[22] % (Images) Red counters
dim counterTwo[22] % (Images) Yellow Counters
dim counterInPlay1[27] % pointer to counterOne
dim counterInPlay2[27] % pointer to counterTwo
dim counter[4]
dim moveCounter[2] %(Pointer) 1: Red counter 2: Yellow Counter
dim lightUpRect[7]
dim board[7,6]
dim players[4]
dim wins[4] % 1: Player one 2: Player two and when playing Android 3: Player 4: Android
dim draws[1] % holds the draws when playing Android
dim info[4] % 1: player's wins 2: android's wins 3: Draws 4: will hole the average
dim score[4] % pointer to wins[]  Player one 2: Player two and when playing Android 3: Player 4: Android
dim yellowCircle[1] % shows which player's turn it is
dim winningCircle[4] % for the winning line

EMPTY=0
twoPlayerGame=1
sound=1
vibration=1
menuSelected=0
menuFirstTime=1
dialogMessFirstTime=1

Rem load sounds
Soundpool.open 4
Soundpool.load playerWin,"win.wav"
Soundpool.load playerLose,"lose.wav"
Soundpool.load dropCounnter,"drop2.wav"
Soundpool.load click,"buttonClick.wav"

Rem Start of Functions

fn.def SaveBestScore(info[])
	BestScorePath$="../../ConnectFour/data/"
	file.exists BestPresent,BestScorePath$+"BestScore.txt"
	if BestPresent then
		text.open w,hs,BestScorePath$+"BestScore.txt"
		for x=1 to 3
			text.writeln hs,int$(info[x])
		next
		text.close hs
	else
		file.mkdir BestScorePath$
		text.open w,hs,BestScorePath$+"BestScore.txt"
		for x=1 to 3
			text.writeln hs,int$(info[x])
		next
		text.close hs
	endif
fn.end

fn.def LoadBestScore(info[])
	dim connectInfo$[3]
	BestScorePath$="../../ConnectFour/data/"
	file.exists BestPresent,BestScorePath$+"BestScore.txt"
	if BestPresent then
		text.open r,hs,BestScorePath$+"BestScore.txt"
			for x=1 to 3
				text.readln hs,connectInfo$[x]
				info[x]=val(connectInfo$[x])
			next
		text.close hs
	else
		for x=1 to 3
			info[x]=0
		next
	endif
	array.delete connectInfo$[]
fn.end

fn.def DialogQuestion(m$,b1$,b2$,scale_x,scale_y)
	button=0
	dim messRect[7],messText[3]
	gr.color 255,255,255,255,1 
	gr.rect messRect[1],300,1,980,150 
	gr.color 255,0,0,0,1
	gr.rect messRect[2],305,5,975,145 
	gr.color 255,40,40,40,1
	gr.rect messRect[3],305,5,975,70 
	gr.color 255,255,255,255,1
	gr.rect messRect[4],310,80,636,140 % Button 1
	gr.rect messRect[5],645,80,970,140 % Button 2
	gr.color 255,255,255,255,1 
	gr.text.size 40
	gr.text.align 2 
	gr.text.draw messText[1],640,50,m$
	gr.color 155,255,255,0,1 % yellow
	gr.rect messRect[6],310,80,636,140 % show when button is touched
	gr.rect messRect[7],645,80,970,140 % show when button is touched
	gr.color 255,0,0,0,1
	gr.text.draw messText[2],470,128,b1$
	gr.hide messRect[6]
	gr.text.draw messText[3],805,128,b2$
	gr.hide messRect[7]
	gr.text.size 30
	gr.text.align 1
	gr.render
	
	do
		do
			Gr.touch touched, tx,ty
		until touched 	
		do
			Gr.touch touched, tx,ty
			tx/=scale_x 
			ty/=scale_x
			
			if tx>=310 & ty>=80 & tx<=636 & ty<=140 then gr.show messRect[6] else gr.hide messRect[6]
			if tx>=640 & ty>=80 & tx<=970 & ty<=140 then gr.show messRect[7] else gr.hide messRect[7]
			gr.render
		until !touched 

		if tx>=310 & ty>=80 & tx<=636 & ty<=140 then button=1
		if tx>=640 & ty>=80 & tx<=970 & ty<=140 then button=2
	until button>0
	
	for x=1 to 7 
		gr.hide messRect[x] 
		if x<4 then gr.hide messText[x] 
	next
	array.delete messText[]
	array.delete messRect[] 

	gr.render
	fn.rtn button
fn.end

Rem Functions for Android's move
fn.def AndroidLookLeftToRight(num,pass,board[],slotFound)
	for y=6 to 1 step - 1 
		count=0
		EMPTY=0
		for x=1 to 7
			if board[x,y]=pass then 
				count++
				if count=num then 
					if x+1<8 & y<6 then	if board[x+1,y] = EMPTY & board[x+1,y+1]<>EMPTY then slotFound=(x+1)
					if !slotFound & x+1<8 & y=6 then if board[x+1,y] = EMPTY then slotFound=(x+1)
					
					if !slotFound & x-count>0 & y=6 then if board[x-count,y] = EMPTY then slotFound=(x-count)
					if !slotFound & x-count>0 & y<6 then if board[x-count,y] = EMPTY & board[x-count,y+1] <>EMPTY then slotFound=(x-count)
					
					!if slotFound then tone 400,400
					if slotFound then f_n.break
				endif
			else 
				count=0
			endif
		next x
		if slotFound then f_n.break
	next y	
	fn.rtn slotFound
fn.end

fn.def AndroidLookBottomToTop(num,pass,board[],slotFound)
	EMPTY=0
	for x=1 to 7 
		count=0
		for y=6 to 2 step - 1 
			if board[x,y]=pass then 
				count++
				if count=num then if board[x,y-1] = EMPTY then slotFound=x
				if slotFound then f_n.break
			else
				count=0 
			endif
		next y
		if slotFound then f_n.break 
	next x 
	fn.rtn slotFound
fn.end

fn.def AndroidLookDiagonallyBottomLeftToTopRight(num,pass,board[],slotFound)
	EMPTY=0
	for y=6 to 4 step - 1 
		for x=1 to 7-num
			if num=3 & board[x,y]=pass & board[x+1,y-1]=pass & board[x+2,y-2]=pass then 
				
				if board[x+3,y-3]=EMPTY & board[x+3,y-2]<>EMPTY then slotFound=x+3 
				
				if !slotFound & x>1 & y<5 then if board[x-1,y+1]=EMPTY & board[x-1,y+2]<>EMPTY then slotFound=x-1
					
				if !slotFound & x>1 & y=5 then if board[x-1,y+1]=EMPTY then slotFound=x-1
				!if slotFound then tone 400,400
				if slotFound then f_n.break 
			endif
		
			
				if num=2 & board[x,y]=pass & board[x+1,y-1]=pass then 
					if board[x+2,y-2]=EMPTY & board[x+2,y-1]<>EMPTY then slotFound=x+2
					
					if !slotFound & x>1 &y<5 then if board[x-1,y+1]=EMPTY & board[x-1,y+2]<>EMPTY then slotFound=x-1
						
					if !slotFound & x>1 &y=5 then if board[x-1,y+1]=EMPTY then slotFound=x-1
					!if slotFound then tone 400,400
					if slotFound then f_n.break 
				endif	
			
			!if slotFound then tone 400,400
			if slotFound then f_n.break 
		next x 
		if slotFound then f_n.break 
	next y
	fn.rtn slotFound
fn.end

fn.def AndroidLookDiagonallyBottomRightToTopLeft(num,pass,board[],slotFound)
	EMPTY=0
	if num=3 then min=4 else min=3
	for y=6 to 4 step - 1 % 4 was a 3 but gor error
		for x=7 to min step - 1
			if num=3 & board[x,y]=pass & board[x-1,y-1]=pass & board[x-2,y-2]=pass then 
				
				if board[x-3,y-3]=EMPTY & board[x-3,y-2]<>EMPTY then slotFound=x-3 
				
				if !slotFound & x<7 & y<5 then if board[x+1,y+1]=EMPTY & board[x+1,y+2]<>EMPTY then slotFound=x+1
					
				if !slotFound & x<7 & y=5 then if board[x+1,y+1]=EMPTY then slotFound=x+1
				
			endif
			!if slotFound then tone 400,400
			if slotFound then f_n.break 
			if num=2 & board[x,y]=pass & board[x-1,y-1]=pass then 
				if board[x-2,y-2]=EMPTY & board[x-2,y-1]<>EMPTY then slotFound=x-2
				
				if !slotFound & x<7 &y<5 then if board[x+1,y+1]=EMPTY & board[x+1,y+2]<>EMPTY then slotFound=x+1
					
				if !slotFound & x<7 &y=5 then if board[x+1,y+1]=EMPTY then slotFound=x+1
				
			endif	
			!if slotFound then tone 400,400
			if slotFound then f_n.break 
		next x 
		if slotFound then f_n.break 
	next y
	fn.rtn slotFound
fn.end

Rem End of Functions

Rem Main
call LoadBestScore(info[])
gosub LoadImages
gosub SetUpScreen
gosub SetUpBoard

do
	gosub MainMenu
	array.shuffle moveOrder[1,4] % if playing the device moveOrder will make it less predictable
	gosub PlayerGame % if menu 2 as been selectered then player 1 plays Android
until menuSelected = 7

end

LoadImages:
	gr.bitmap.load allCounters,"counters.png"
	gr.bitmap.load connectBG[1],"Connect4BG2.png" 
	gr.bitmap.load connectBG[2],"TopBox3.png" 
	gr.bitmap.load connectBG[3],"MenuBG.png"
	
	for x=1 to 4 
		gr.bitmap.crop counter[x],allCounters,(x-1)*120,0,120,120 
	next
	gr.bitmap.delete allCounters
	
	gr.bitmap.scale counter[3],counter[3],80,80
	gr.bitmap.scale counter[4],counter[4],80,80
	gr.bitmap.scale connectBG[2],connectBG[2],900,700
	
	for x=1 to 22
		counterOne[x]=counter[1]
		counterTwo[x]=counter[2]
	next
return

SetUpScreen:
	gr.bitmap.draw connect4[1],connectBG[1],0,0 % Board Background
	gr.bitmap.draw moveCounter[1],counter[1],1,-120 % Red Counter 
	gr.bitmap.draw moveCounter[2],counter[2],1,-120 % Green Counter
	gosub DoRectangales
	gr.color 255,255,255,255,1
	gr.text.size 40
	gr.set.stroke 1
	gr.text.bold 2
	gr.bitmap.draw pc1,counter[3],1195,545
	gr.bitmap.draw pc2,counter[4],1195,645
	gr.text.draw players[1],1000,600,""
	gr.text.draw players[2],1000,700,""
	gr.color 255,0,0,0,0
	gr.text.draw players[3],1000,600,""
	gr.text.draw players[4],1000,700,""
	gr.color 255,255,255,0,0
	gr.text.bold 0
	gr.set.stroke 5
	gr.circle yellowCircle[1],1235,585,38 % shows which player's turn it is
	
	gr.color 255,0,0,0,1
	gr.text.size 25
	gr.text.align 2
		
	gr.text.draw score[1],1235,595,"" % player one
	gr.text.draw score[2],1235,695,"" % player two
	gr.text.draw score[3],1235,595,"" % player one when playing android so that can save wins
	gr.text.draw score[4],1235,695,"" % Android
	
	for x=1 to 4 
		gr.hide score[x] % do not know whether a two player or playing aginst Android, so hide them
	next
	gr.text.align 1
	
	for x=1 to 22 
		gr.bitmap.draw counterInPlay1[x],counterOne[x],1100,100
		gr.bitmap.draw counterInPlay2[x],counterTwo[x],1100,100
		gr.hide counterInPlay1[x] 
		gr.hide counterInPlay2[x]
	next
	gr.bitmap.draw connect4[2],connectBG[2],100,100 % Board Top
	gr.bitmap.draw connect4[3],connectBG[3],0,0 % Menu
	gr.set.stroke 10
	gr.color 255,255,255,255,0
	for x=1 to 4 
		gr.circle winningCircle[x],1235,-100,50 
		gr.hide winningCircle[x]
	next
return

DoRectangales:
	Rem light up when touched
	gr.color 120,100,100,100,1
	for x=110 to 880 step 127
		gr.rect lightUpRect[int(x/110)],x,110,x+120,800
		gr.hide lightUpRect[int(x/110)]
	next
	gr.color 255,255,0,0,1
return

SetUpBoard:
	for y=1 to 6 
		for x=1 to 7
			board[x,y]=EMPTY 
		next 
	next 
return

MainMenu:
	menuSelected=0
	inMenu=1
	gr.show connect4[3]
	gr.render
	if menuFirstTime then
		array.load menuList$[], "Two Player","Single Player","Sound On","Vibration On","About","Rules","Leave Game","Sound Off","Vibration Off"
		p$=chr$(10)+chr$(10) % Do a paragraph brake
		dim menuRect[8] % menu menu buttons
		dim menuText[8] % text on menu
		dim menuTap[7] % show when menu is taped
		dim menuScores[4] % Show wins, losses, draws and win% when plaing Android
		gr.text.size 40
		gr.text.bold 2 
		gr.text.align 2
		text=1
		x=200
		for y=120 to 570 step 75 
			gr.color 255,255,255,255,1
			gr.rect menuRect[text],x,y,1080,y+50 
			gr.color 155,255,255,0,1 % yellow
			gr.rect menuTap[text],x,y,1080,y+50
			gr.hide menuTap[text]
			gr.color 255,0,0,0,1
			gr.text.draw menuText[text],640,y+40,menuList$[text]
			text++
		next

		gr.text.bold 0 
		rem Game Statistics
		gr.text.draw menuText[8],620,690,"Wins             Losses             Draws              Win%"
		gr.text.align 1
		gr.color 255,255,255,255,1
		
		if info[1]>0 | info[2]>0 then
			numOfGames=(info[1]+info[2])
			info[4]=(info[1]*100)/numOfGames % Thank you Gilles for help working out the persent 
		endif
		gr.rect menuRect[8],x,700,1080,750
		gr.color 255,0,0,0,1
		y=740
		gr.text.draw menuScores[1],210,y,int$(info[1])
		gr.text.draw menuScores[2],460,y,int$(info[2])
		gr.text.draw menuScores[3],730,y,int$(info[3])
		gr.text.draw menuScores[4],960,y,int$(info[4])
		gr.render
		menuFirstTime=0
	else
		if info[1]>0 | info[2]>0 then
			numOfGames=(info[1]+info[2])
			info[4]=(info[1]*100)/numOfGames
		endif
		
		for y=1 to 8
			gr.show menuRect[y]
			gr.show menuText[y] 
			if y<5 then 
				gr.show menuScores[y]
				gr.modify menuScores[y],"text",int$(info[y]) 
			endif
		next
		
		gr.render
	endif
	do
		do 
			gr.touch touched,tx,ty 
		until touched
		do 
			gr.touch touched,tx,ty 
			tx/=scale_x 
			ty/=scale_y
			tap=int(ty/80)
			if tap>0 & tap<8 & tx>200 & tx<1080 then 
				if oldTap & oldTap<8 & oldTap<>tap then gr.hide menuTap[oldTap]
				gr.show menuTap[tap]
				oldTap=tap
				gr.render
			endif
		until !touched
		
		for y=1 to 7 
			gr.hide menuTap[y]
		next
		gr.render
		
		if tx>200 &tx<1080 then menuSelected=tap else menuSelected=0
		
		Sw.begin menuSelected
			Sw.case 1
				twoPlayerGame=1 
				if sound then Soundpool.play s,click,0.99,0.99,1,0,1
				Sw.break
			Sw.case 2
				twoPlayerGame=0 
				if sound then Soundpool.play s,click,0.99,0.99,1,0,1
				Sw.break
			Sw.case 3
				if sound then 
					sound=0 
					gr.modify menuText[3],"text",menuList$[8] 
				else 
					sound=1 
					gr.modify menuText[3],"text",menuList$[3] 
				endif
				gr.render
				if sound then Soundpool.play s,click,0.99,0.99,1,0,1
				Sw.break
			Sw.case 4
				if vibration then 
					vibration=0 
					gr.modify menuText[4],"text",menuList$[9] 
				else 
					vibration=1 
					gr.modify menuText[4],"text",menuList$[4] 
				endif
				gr.render
				if sound then Soundpool.play s,click,0.99,0.99,1,0,1
				Sw.break
			sw.case 5 
				if sound then Soundpool.play s,click,0.99,0.99,1,0,1
				Dialog.Message " About:",~
				" Connect Four: for Android" + p$ + ~
				" With RFO Basic" + p$ + ~
				" January 2015" + p$ +~
				" Version 1.00" + p$ + ~ 
				" Author: Roy Shepherd", ~
				ok, "OK"
				if sound then Soundpool.play s,click,0.99,0.99,1,0,1
				sw.break
			sw.case 6 
				if sound then Soundpool.play s,click,0.99,0.99,1,0,1
				dialog.Message " Help:",~
				" This game is based on the legendary 1970’s board game Connect Four." + p$ + ~
				" The two counters are Red and Green. Player one plays Red and player two or your device playes Green." + p$ + ~
				" To start with player one goes first. He/she drop a " + ~ 
				" counter into one of the slots in the board, by " + ~
				" tapping on a column. Then player two or your device" + ~
				" drops a counters into the board." + p$ + ~
				" This game is similar to the classic tic-tac-toe game," + ~
				" the primary difference being that in this game " + ~
				" players must connect four of their counters in a straight" + ~
				" line, either horizontally, vertically or diagonally. " + p$ + ~
				" A record is kept of your wins aginst the device.", ~
				ok, "OK"
				if sound then Soundpool.play s,click,0.99,0.99,1,0,1
				sw.break
			sw.case 7 
				if sound then Soundpool.play s,click,0.99,0.99,1,0,1
				yn=DialogQuestion("Exit Game?","Yes","No",scale_x,scale_y)
				if sound then Soundpool.play s,click,0.99,0.99,1,0,1
				if yn=1 then
					pause 100 % so that button sound can be heard
					call SaveBestScore(info[])
					soundpool.release
					exit 
				endif
				sw.break
		Sw.end
	until menuSelected=1 | menuSelected=2
	
		for y=1 to 8 
			if y<8 then gr.hide menuTap[y]
			gr.hide menuRect[y]
			gr.hide menuText[y] 
			if y<5 then gr.hide menuScores[y]
		next
	gr.hide connect4[3]
	gr.render
	inMenu=0
return

UpDateScore:
	if twoPlayerGame then 
		gr.modify players[1],"text","Player One"
		gr.modify players[2],"text","Player Two"
		gr.modify players[3],"text","Player One"
		gr.modify players[4],"text","Player Two"
		gr.show score[1]
		gr.show score[2]
		gr.hide score[3]
		gr.hide score[4]
		gr.modify score[1],"text",int$(wins[1])
		gr.modify score[2],"text",int$(wins[2])
	else 
		gr.modify players[1],"text","Player One"
		gr.modify players[2],"text","Android"
		gr.modify players[3],"text","Player One"
		gr.modify players[4],"text","Android"
		gr.show score[3]
		gr.show score[4]
		gr.hide score[1]
		gr.hide score[2]
		gr.modify score[3],"text",int$(wins[3])
		gr.modify score[4],"text",int$(wins[4])
	endif
	gr.render
return

PlayerGame:
	turn=0
	aWinner = 0
	deadHeat = 0
	backKeyTapped=0
	cip=1 % counters in play
	gosub UpDateScore
	
		do
			yn=0
			player=0
			drop=0
			mx=1
			boardOK=0
			turn++
			if turn=3 then 
				turn = 1
				cip++ 
			endif
			if turn =1 then gr.modify yellowCircle[1],"y",585 % show whose turn it is
			if turn =2 then gr.modify yellowCircle[1],"y",685
			gr.render
			do	
				gr.touch touched,tx,ty 
				if touched then 
					tx/=scale_x 
					ty/=scale_y
					for x=110 to 889 step 127
						if tx>x & tx<x+115 & ty>110 & ty<800 then 	
							drop=x
							if int(x/110)<>mx then gr.hide lightUpRect[int(mx)]
							mx=int(x/110)
							gr.show lightUpRect[int(x/110)]
							gr.modify moveCounter[turn],"x",x-3,"y",1
							gr.render
						endif
					next
				endif
				gr.touch touched,dtx,dty
				if drop then if board[drop/110,1]=EMPTY then boardOK=1
				
			until !touched & drop & boardOK & tx>110 & tx<1000 & ty>110
				
			gr.hide lightUpRect[int(drop/110)]
			gosub DropCounter
			gosub LookForWin
			if !aWinner then gosub LookForDraw
			if !twoPlayerGame & !aWinner & !deadHeat then gosub AndroidMove
			if aWinner | deadHeat then gosub AnnounceResults
			if player & yn <>2 then gosub AndroidMove
			
		until yn=2 % back to Menu
return

AnnounceResults:
	if deadHeat then 
		if !twoPlayerGame then draws[1]++
		if sound then Soundpool.play s,playerLose,0.99,0.99,1,0,1
		yn = DialogQuestion("A Draw!:", "New Game","Menu",scale_x,scale_y)
		if sound then Soundpool.play s,click,0.99,0.99,1,0,1
	endif
		
	if aWinner then 
		if twoPlayerGame then
			wins[turn]++
			w$="Player "+int$(turn)+" Wins" 
		endif 
		
		if !twoPlayerGame then wins[turn+2]++
		gosub UpDateScore
		if !twoPlayerGame & !androidWins then 
			if sound then Soundpool.play s,playerWin,0.99,0.99,1,0,1
			w$="You Win"
			player=1 
			wins[3]++
			info[1]++
		endif
		if !twoPlayerGame & androidWins then
			if sound then Soundpool.play s,playerLose,0.99,0.99,1,0,1
			w$="Android Wins"
			wins[4]++
			info[2]++
		endif
		yn = DialogQuestion(w$, "New Game","Menu",scale_x,scale_y)
		if sound then Soundpool.play s,click,0.99,0.99,1,0,1
	endif
	gosub ResetForNewGame
return

DropCounter:
	dropTo=6
	Rem find out how far to drop
	for y=1 to 5 
		if board[int(drop/110),y+1]<>EMPTY then 
			dropTo=y
			f_n.break 
		endif 
	next
	board[int(drop/110),y]=turn
	y=1
	speed=50
	do 
		if y+speed<= posDrop[dropTo] then y=y+speed else y=posDrop[dropTo]
		gr.modify moveCounter[turn],"y",y
		gr.render 
	until y>=posDrop[dropTo]
	gosub Bounce
	if turn =1 then
		gr.modify counterInPlay1[cip],"x",drop-3,"y",y 
		gr.show counterInPlay1[cip]
	else 
		gr.modify counterInPlay2[cip],"x",drop-3,"y",y 
		gr.show counterInPlay2[cip]
	endif
	gr.render
return

Bounce:
	jumpStart = posDrop[dropTo]
	jumpHight=jumpStart/5 
	if sound then Soundpool.play s,dropCounnter,0.99,0.99,1,0,1
	if vibration then vibrate buzzConnect[], -1
	for bounceHight=1 to 2
		for y=jumpStart to jumpStart-(jumpHight/bounceHight) step - 30/bounceHight
			gr.modify moveCounter[turn],"y",y
			gr.render
		next
		if sound & jumpStart<>105 then Soundpool.play s,dropCounnter,0.99,0.99,1,0,1
		do 
			if y+speed<= posDrop[dropTo] then y=y+speed else y=posDrop[dropTo]
			gr.modify moveCounter[turn],"y",y
			gr.render 
		until y>=posDrop[dropTo]
	next bounceHight
return

LookForWin:
	startX=0 
	startY=0 
	count=0
	Rem Look left to right
	for y=1 to 6 
		for x=1 to 4 
			if board[x,y]=turn then 
				count=1
				startX=(x*127)+40
				startY=(y*115)+52
				for look=x+1 to x+3 
					if board[look,y]=turn then count++
				next
				if count=4 then
					aWinner=1
					xx=(look*115)+30 
					m=1
					for x=startX to xx step 127
						gr.modify winningCircle[m],"x",x,"y",startY 
						gr.show winningCircle[m]
						m++
					next
					gr.render
					return
				endif
			endif
		next 
	next
	Rem Look top to botton
	for x=1 to 7 
		for y=1 to 3 
			if board[x,y]=turn then 
				count=1
				startX=(x*127)+40
				startY=(y*115)+50
				for look=y+1 to y+3 
					if board[x,look]=turn then count++
				next
				if count=4 then
					aWinner=1
					yy=(look*115)-40 
					m=1
					for y=startY to yy step 115
						gr.modify winningCircle[m],"x",startX,"y",y
						gr.show winningCircle[m]
						m++
					next
					gr.render
					return
				endif
			endif
		next 
	next
	Rem Look Digonally bottom left to right
	for y=6 to 4 step - 1 
		for x=1 to 4
			if board[x,y]=turn then 
				count=1
				startX=(x*127)+40
				startY=(y*115)+50
				yy=y-1 
				for xx=x+1 to x+3 
					if board[xx,yy]=turn then count++ 
					yy-- 
				next
				if count=4 then
					aWinner=1
					for m=1 to 4
						gr.modify winningCircle[m],"x",startX,"y",startY
						gr.show winningCircle[m]
						startX+=127
						startY-=115
					next
					gr.render
					return
				endif
			endif
		next 
	next
	Rem Look Digonally bottom right to left
	for y=6 to 4 step - 1 
		for x=7 to 4 step - 1
			if board[x,y]=turn then 
				count=1
				startX=(x*127)+40
				startY=(y*115)+50
				yy=y-1 
				for xx=x-1 to x-3 step - 1
					if board[xx,yy]=turn then count++ 
					yy-- 
				next
				if count=4 then
					aWinner=1
					for m=1 to 4
						gr.modify winningCircle[m],"x",startX,"y",startY
						gr.show winningCircle[m]
						startX-=127
						startY-=115
					next
					gr.render
					return
				endif
			endif
		next 
	next
return 

LookForDraw:
	Rem If top line is full and no winer then a draw
	flag = 0
	for x=1 to 7 
		if board[x,1]=EMPTY then flag = 1 
	next
	if flag=0 then deadHeat=1
return

ResetForNewGame:
	for x=1 to 22
		gr.modify counterInPlay1[x],"x",1100,"y",100 
		gr.modify counterInPlay2[x],"x",1100,"y",100 
		gr.hide counterInPlay1[x] 
		gr.hide counterInPlay2[x]
	next
	gr.modify moveCounter[1],"x",1,"y",-120
	gr.modify moveCounter[2],"x",1,"y",-120
	for x=1 to 4 
		gr.hide winningCircle[x] 
	next
	aWinner = 0
	deadHeat = 0
	cip=1 % counters in play
	gosub SetUpBoard
return

AndroidMove:
	slotFound=0
	turn++
	androidWins=0
	if turn=3 then 
		turn = 1
		cip++
	endif
	if turn =1 then gr.modify yellowCircle[1],"y",585
	if turn =2 then gr.modify yellowCircle[1],"y",685
	Rem Using the moveOrder will make the game call the function in a diferent order each game so it will be less predictable
	for num=3 to 2 step-1
		for pass = 2 to 1 step -1 % look to add to androids counters. If not, look to block players 
			for order=1 to 4
				if !slotFound & moveOrder[order]=1 then slotFound=AndroidLookLeftToRight(num,pass,board[],slotFound)
			
				if !slotFound & moveOrder[order]=2 then slotFound=AndroidLookBottomToTop(num,pass,board[],slotFound)
				
				if !slotFound & moveOrder[order]=3 then slotFound=AndroidLookDiagonallyBottomLeftToTopRight(num,pass,board[],slotFound)
				
				if !slotFound & moveOrder[order]=4 then slotFound=AndroidLookDiagonallyBottomRightToTopLeft(num,pass,board[],slotFound)
			next order
		next pass
	next num
	
	Rem if all else fails then do random move
	if !slotFound then	
		gr.render
		do 
			slotFound=int(rnd() * 7)+1 
		until board[slotFound,1]=EMPTY
	endif
	
	drop=int((slotFound*127)-12-slotFound)
	gr.modify moveCounter[turn],"x",drop-3,"y",1
	gr.render 
	pause 250
	
	gosub DropCounter
	gosub LookForWin
	if !aWinner then gosub LookForDraw
	if aWinner | deadHeat then 
		 if aWinner then androidWins=1
		 gosub AnnounceResults 
	endif
return

onBackKey:

	yn = DialogQuestion("Exit Game?","Yes","No",scale_x,scale_y) 
	if sound then Soundpool.play s,click,0.99,0.99,1,0,1

	if yn=2 then
		yn=0
		back.resume
	else 
		call SaveBestScore(info[])
		pause 100
		soundpool.release
		exit
	endif 

end
