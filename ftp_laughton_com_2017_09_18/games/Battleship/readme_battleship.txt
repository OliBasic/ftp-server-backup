Battleship v1.0 wi-fi
by [Antonis]
Dic.2011-Gen.2012

This is a sockets program!
You will need 2 devices on the same Wi-Fi network (or Local Area Network),
and 2 copies of the same program.

How to play:
Position your 3 ships (horizzontally o vertically) by touching them
and then touching where you want the mid point of the ship.
Each ship is 3 squares width or height.
After, select "Play first" or "Play second"
Second player will have to copy the ip address of the first player.
Start shooting!
Leggend:
A big green circle is your missed shot.
A big green rectangle is your shot on target.
A small minus is opponent's shot on target.
A small plus is opponent's missed shot.
