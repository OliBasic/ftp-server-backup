REM battleship v1.0 wi-fi
rem by [Antonis]
rem Dic.2011-Gen.2012

Dim matrix1[8,8], matrix2[8,8],shippointer[3]
Dim shooted1[8,8], shooted2[8,8]
! matrix1 = ship positions p1
! matrix2 = temporary matrix to check with 1

FOR i=1 to 8
  FOR j=1 to 8
     matrix1[i,j]=0
	 matrix2[i,j]=0
  NEXT j
NEXT i
positioned1=0
iamplayer1=1 % = server

GR.open 255, 0, 0, 0
GR.color 255, 255, 255, 255, 0
GR.text.size 37
GR.orientation 1
GR.screen w, h
IF w>h THEN % exchange values
    z = w
    w = h
    h = z
ENDIF
IF w = 752 THEN w = 800
scalex = w / 800
scaley = h / 1280

GR.scale scalex,scaley
gr.text.draw mylogo,220,835, "Battleship v1.0 by [Antonis]"
GR.text.size 67
! draw board
GR.set.stroke 5
GR.rect r1, 100,100, 799, 799
FOR i=1 to 7
  GR.color 255, 255, 255, 255, 0
  GR.line l1, 100, 100+87*i, 799, 100+87*i
  GR.line l2, 100+87*i,100,100+87*i,799
  GR.color 255, 255, 0, 255, 0
  GR.text.draw t1, 30, 100+87*i-10, chr$(48+i)
  GR.text.draw t2,87*i+30, 80,chr$(64+i)
NEXT i
GR.text.draw t1, 30, 100+87*i-10, chr$(48+8)
GR.text.draw t1, 87*i+30, 80,chr$(64+8)
GR.bitmap.load b1, "frigate1.png"
GR.bitmap.load b2, "frigate2.png"
GR.bitmap.draw bp1,b1, 400,1078
GR.bitmap.draw bp2,b2, 700,900
GR.render

!position ships
GR.color 255, 195, 195, 0, 0
GR.text.draw t2, 30, 900,"Touch ships"
GR.text.draw t3, 30, 1000,"then touch screen"
GR.render
ship=0
DO
   GOSUB touchaship
   GOSUB touchmatrix
UNTIL ship=3
pause 1000
GR.hide t2
GR.hide t3
GR.color 255, 195, 0, 0, 1
GR.text.draw t2, 30, 900,"[ play first ]"
GR.text.draw t3, 30, 1000,"[ play second ]"
GR.hide bp1
GR.hide bp2
GR.render
  GOSUB whoplaysfirst
GR.hide t2
GR.hide t3
GR.bitmap.load b3,"cannon.png"
GR.bitmap.draw bp3,b3,30,900
GR.render

! establish connection
!  
port=1080

IF iamplayer1=1 THEN % p1 here (server side)
   GR.text.draw t4,30,1200,"Wait for connection..."
   SOCKET.myip ip$
   gr.text.draw txt,30,1100,"my ip="+ip$
   gr.render
   SOCKET.server.create port  
   SOCKET.server.connect
   ! Connected to a Client

   ! Wait for Client to send password
   ! or time out after 10 seconds
   maxclock = clock() + 10000
   flag=0
   DO
   SOCKET.server.read.ready flag
     IF clock() > maxclock THEN
	   GR.hide t4
	   GR.text.draw t4,30,1200,"timeout, closing..."
	   GR.render
	   pause 5000
     SOCKET.server.close
	   gr.front 0
     print "timeout, client didn't send password"
     end
    ENDIF
   UNTIL flag

 ! Password received. Read it.
   SOCKET.server.read.line password$
    pass$=right$(password$,10)
   
  IF pass$="1234567890" THEN
    SOCKET.server.write.line "ok"
    gr.hide t4
    GR.text.draw t4,30,1200,"Password correct,ready..."
    pause 1000
    gr.render
    POPUP "touch cannon and then touch target",0,0,0
   else
    gr.hide t4
    GR.text.draw t4,30,1200,"Password not ok, quitting..."
	  GR.render
    SOCKET.server.close 
    pause 5000
   	 gr.front 0
     print "client didn't send *correct* password"
     end
   ENDIF
ENDIF  % p1 here 


IF iamplayer1=0 THEN % p2 here (client side)
  gr.front 0
  Input "give the server ip", sip$,"192.168.10.2"
  gr.front 1
  SOCKET.client.connect sip$, port
  ! When the connection is established,
  ! send the server the password
  SOCKET.client.write.line "1234567890"

  ! and then wait for Server to respond 
  ! or time out after 10 seconds
  flag2=0
  maxclock = clock() + 10000
  DO
  SOCKET.client.read.ready flag2
  IF clock() > maxclock then
     GR.hide t4
	   GR.text.draw t4,30,1200,"timeout,no ok from server"
	   GR.render
     SOCKET.client.close
	   pause 3000
     gr.front 0
     print "server has not responded 'ok', or i didnt listen.."
     end
    ENDIF
  UNTIL flag2
 ! Server has sent message.
 ! Read it.
 SOCKET.client.read.line line$

 line$=right$(line$,2)
 IF line$="ok" THEN
   gr.hide t4
   GR.text.draw t4,30,1200,"Ok, ready to play..."
   gr.render
    POPUP "please, wait 120 sec for player 1",0,0,0
   else
     gr.hide t4
     GR.text.draw t4,30,1200,"response was "+line$
   	GR.render
  	SOCKET.client.close
    pause 5000
    gr.front 0
	end	
  ENDIF
ENDIF  % p2 here 

! play the game till ends
turnp1=1
shotsontarget1=0
shotsontarget2=0
gr.color 255,0,255,0,0
playthegame:
DO
  if turnp1=1 & iamplayer1=1 then  % ******* CASE 1 ********
      GOSUB giveashot % now i have the coordinates of last NEW shot
     ! transmit coordinates to client and wait response
	  SOCKET.server.write.line str$(x1)
	  SOCKET.server.write.line str$(y1)
     ! wait 10 sec for client response yes=on target
    flag=0
     maxclock = clock() + 10000
     do
     socket.server.read.ready flag
      if clock() > maxclock then
        print "Read time out"
      end
    endif
    until flag
	! client response will be yes or no
	socket.server.read.line line$

	if line$=right$("yes",3) then
	  shotsontarget1=shotsontarget1+1
	  POPUP "Your Shot found target",0,0,0
	  for ii=0 to 500 step 100
	  tone 500+ii,100
	  next ii
    ! draw a rectangle instead of a circle
    gr.hide cir1
	  gr.rect re6,100+87*(x1-1)+7,100+87*(y1-1)+7,100+87*(x1-1)+80,100+87*(y1-1)+80
      gr.render
     elseif line$=right$("no",2) then 
        POPUP "Missed Shot",0,0,0
    else
	  print "unexpected error"
    print line$
	  gr.front 0
	  end
	endif 
! invert players turn
turnp1=0
if shotsontarget1=9 then goto gameover
endif	  
if turnp1=0 & iamplayer1=1 then  % ******* CASE 2 ********
    ! wait 2 minutes for player2 to play
     POPUP "please, wait 120 sec for player 2",0,0,0
    flag=0
   	maxclock = clock() + 120000
     do
     socket.server.read.ready flag
      if clock() > maxclock then
        gr.front 0
        print "Read time out"
        print "player 1 wins"
      end
     endif
     until flag
	! wait for coordinates 
	SOCKET.server.read.line line1$
  !  gosub waitsever
	SOCKET.server.read.line line2$
	xx=val(line1$)
	yy=val(line2$)
	! check if on target and respond with a yes or no
    if matrix1[yy,xx]=1 then	
	    shotsontarget2=shotsontarget2+1
		  POPUP "Opponent Shot found target",0,0,0
		  tone 500,800
		  SOCKET.server.write.line "yes"
		  ! draw a plus on my board
		  gr.text.draw tobj1, 100+87*(xx-1)+7,100+87*(yy-1)+77,"+"
		  gr.render
	   else
	      ! draw a minus on my screen
         gr.text.draw tobj1,100+87*(xx-1)+7,100+87*(yy-1)+77,"-"
		 SOCKET.server.write.line "no"
     endif
! invert players turn
turnp1=1	 
if shotsontarget2=9 then goto gameover
endif	 
if turnp1=0 & iamplayer1=0 then  % ******* CASE 3 ********
     GOSUB giveashot % now i have the coordinates of last NEW shot
     ! transmit coordinates to SERVER and wait response
	  SOCKET.client.write.line str$(x1)
	  SOCKET.client.write.line str$(y1)
     ! wait 10 sec for 	SERVER response yes=on target
     flag=0
     maxclock = clock() + 10000
     do
     socket.client.read.ready flag
      if clock() > maxclock then
        print "Read time out"
        gr.front 0
        end
    endif
    until flag

	! server response will be yes or no
	socket.client.read.line line$
	if line$=right$("yes",3) then
	  shotsontarget1=shotsontarget1+1
	  for ii=0 to 500 step 100
	  tone 500+ii,100
	  next ii
    ! draw a rectangle instead of a circle
    gr.hide cir1
	  gr.rect re6,100+87*(x1-1)+7,100+87*(y1-1)+7,100+87*(x1-1)+80,100+87*(y1-1)+80
      gr.render
	  elseif line$=right$("no",2) then
        POPUP "Your Shot missed",0,0,0
    else
	    print "unexpected error"
      print line$
	    gr.front 0
	    end
	endif 
! invert players turn
turnp1=1	
if shotsontarget1=9 then goto gameover
endif	  
if turnp1=1 & iamplayer1=0 then  % ******* CASE 4 ********
    ! wait 2 minutes for player1 to play
    POPUP "please, wait 120 sec for player 1",0,0,0
  flag=0
	maxclock = clock() + 120000
     do
     socket.client.read.ready flag
      if clock() > maxclock then
        gr.front 0
        print "Read time out"
        print " player 2 wins"
      end
     endif
     until flag
	! wait for coordinates 
	SOCKET.client.read.line line1$
	SOCKET.client.read.line line2$
	xx=val(line1$)
	yy=val(line2$)
	! check if on target and respond with a yes or no
    if matrix1[yy,xx]=1 then	
	    shotsontarget2=shotsontarget2+1
		  POPUP "Opponent Shot found target",0,0,0
		  tone 500,800
		  SOCKET.client.write.line "yes"
		  ! draw a plus on my board
		  gr.text.draw tobj1, 100+87*(xx-1)+7,100+87*(yy-1)+77,"+"
		  gr.render
	   else
	      ! draw a minus on my screen
         gr.text.draw tobj1,100+87*(xx-1)+7,100+87*(yy-1)+77,"-"
         gr.render
		     SOCKET.client.write.line "no"
     endif
! invert players turn
turnp1=0	 
endif	 
gameover:
UNTIL shotsontarget1=9 | shotsontarget2=9


! first name winner
   tts.init
if iamplayer1=1 & shotsontarget1=9 then 
      POPUP "Player 1 wins",0,0,0
	  tts.speak "player one wins battleship"
  endif
if iamplayer1=1 & shotsontarget2=9 then 
      POPUP "Player 2 wins",0,0,0
	  tts.speak "player two wins battleship"
  endif
if iamplayer1=0 & shotsontarget1=9 then 
      POPUP "Player 2 wins",0,0,0
	  tts.speak "player two wins battleship"
  endif
if iamplayer1=0 & shotsontarget2=9 then 
      POPUP "Player 1 wins",0,0,0
	  tts.speak "player one wins battleship"
  endif
	
tts.speak "game over" 
popup " closing connection, pls wait!" ,0,0,0  
pause 5000

! then close connections
if iamplayer1=1 then socket.server.close else socket.client.close
gr.front 0
! and end
print "  >>Game finished<<"
print "[Thanks for playing]"
end


!****************************
! position 3 ships on screen
touchaship:
choosed =0
DO
   touched = -1
   GR.touch touched, x, y
   pause 100
UNTIL touched>0
IF x >400*scalex & x<650*scalex & y>1078*scaley & y<1156*scaley  THEN
    choosed = 1
    GR.rect re1, 400-10, 1078-10, 650+10, 1156+10
 	GR.render
	return
  ENDIF
 IF x>700*scalex  & x<778*scalex & y>900*scaley & y<1150*scaley  THEN
    choosed = 2
    GR.rect re2, 700-10, 900-10, 778+10, 1150+10
 	GR.render
	return
  ENDIF 
GOTO  touchaship

!*****************************
! touch valid point on screen
! considering middle point to center ship 
touchmatrix:
DO
   GR.touch touched, x, y
UNTIL touched>0
DO
   GR.touch touched, x, y
UNTIL !touched

IF choosed=1 THEN
  IF (x<187*scalex & x>100*scalex & y>100*scaley & y<800*scaley)  |   (x<800*scalex & x>712*scalex & y>100*scaley & y<800*scaley) THEN 
          POPUP "Doesn't fit there, retouch matrix",0,0,0
        GOTO touchmatrix
  ENDIF
  IF x <187*scalex | x>(800-87)*scalex | y<100*scaley | y>800*scaley  THEN GOTO touchmatrix

   FOR j=1 to 8
	   FOR i=1 to 8 % 2 to 7
	    IF x>(100+87*(i-1))*scalex  & x<(100+87*i)*scalex  & y>(100+87*(j-1))*scaley  & y<(100+87*j)*scaley THEN
          x1=i
          y1=j
		 ENDIF 
     NEXT j
	 NEXT i
	 matrix2[y1,x1]=1
     matrix2[y1,x1-1]=1
     matrix2[y1,x1+1]=1
     intersect=0
     GOSUB checkintersection
     IF intersect=1 THEN         
	       POPUP "illegal position, retry",0,0,0
		     GR.hide re1	
         GR.render	  
         return
     ENDIF
	   matrix1[y1,x1]=1
     matrix1[y1,x1-1]=1
     matrix1[y1,x1+1]=1     
     ship=ship+1	 
	   GR.bitmap.draw shippointer[ship],b1,100+87*x1-160, 100+87*y1-90
     GR.hide re1	 
 	   GR.render
	return

ENDIF

IF choosed=2 THEN
  IF (x<800*scalex & x>100*scalex & y>100*scaley & y<187*scaley) | (x<800*scalex & x>100*scalex & y>713*scaley & y<800*scaley) THEN 
          POPUP "doesn't fit there, retry",0,0,0
        GOTO touchmatrix
  ENDIF
  IF x <100*scalex | x>799*scalex | y<187*scaley | y>713*scaley  THEN GOTO touchmatrix
    FOR j=1 to 8
	  FOR i=1 to 8
	    IF x>(100+87*(i-1))*scalex  & x<(100+87*i)*scalex  & y>(100+87*(j-1))*scaley  & y<(100+87*j)*scaley THEN
          x1=i
          y1=j
		 ENDIF 
      NEXT j
	 NEXT i
    matrix2[y1,x1]=1
    matrix2[y1+1,x1]=1
    matrix2[y1-1,x1]=1

	 intersect=0
     GOSUB checkintersection
     IF intersect=1 THEN 
	       POPUP "illegal position, retry",0,0,0
		     GR.hide re2		
         GR.render    
         return
     ENDIF
  	 matrix1[y1,x1]=1
     matrix1[y1-1,x1]=1
     matrix1[y1+1,x1]=1 
     ship=ship+1	 
  	 GR.bitmap.draw shippointer[ship],b2,100+87*x1-90, 100+87*y1-160
     GR.hide re2
 	   GR.render
	 return
  ENDIF
GOTO  touchmatrix

!***************************
! check IF my ships intersect
checkintersection:
IF ship=0 THEN intersect=0
IF ship>0 THEN 
FOR i=1 to 8
 FOR j=1 to 8
   IF matrix1[i,j]=1 & matrix1[i,j]=matrix2[i,j] THEN intersect=1
 NEXT j
NEXT i
ENDIF
IF choosed=1 THEN 
 matrix2[y1,x1]=0
 matrix2[y1,x1-1]=0
 matrix2[y1,x1+1]=0
ENDIF
IF choosed=2 THEN 
       matrix2[y1,x1]=0
       matrix2[y1+1,x1]=0
       matrix2[y1-1,x1]=0
ENDIF
return  

!*********************************
! determine server and client
whoplaysfirst:
DO
   touched = -1
   GR.touch touched, x, y
   pause 100
UNTIL touched>0
IF x<550*scalex & y<930*scaley & y>840*scaley  THEN
    iamplayer1=1
	POPUP "you are player 1",0,0,0
 	GR.render
	pause 1000
	return
  ENDIF
IF x<550*scalex & y<1030*scaley & y>940*scaley  THEN
    iamplayer1=0   
	POPUP "you are player 2",0,0,0	
 	GR.render
	pause 1000
	return
  ENDIF
GOTO  whoplaysfirst

! *****************************************
! shoot. at 30,900 is the cannnon 200x100

giveashot:
   gr.color 255,0,55,255,1
  gr.hide t4
  GR.text.draw t4, 30, 1200,"[ first touch cannon ]"
	GR.render
DO
   touched = -1
   GR.touch touched, x, y
   pause 20
UNTIL touched>0
IF x>30*scalex & x<230*scalex & y<1100*scaley & y>900*scaley  THEN
    !first draw rectangle on cannon
  gr.color 255,255,255,255,0
	GR.rect re5, 30-20, 900-20, 230+20, 1000+20
  gr.render  
  gr.color 255,0,55,255,1
  gr.hide t4
  GR.text.draw t4, 30, 1200,"[ select target on screen ]"
	GR.render
	GOTO retouchme
ENDIF
GOTO giveashot 
retouchme:
	DO
     touched = -1
     GR.touch touched, x, y
     pause 20
    UNTIL touched>0
	IF x<100*scalex | x>799*scalex | y<100*scaley | y>799*scaley  THEN GOTO retouchme
	
	!find coordinates of shot
  gr.hide t4
	FOR j=1 to 8
	  FOR i=1 to 8
	    IF x>(100+87*(i-1))*scalex  & x<(100+87*i)*scalex  & y>(100+87*(j-1))*scaley  & y<(100+87*j)*scaley THEN
          x1=i
          y1=j
		 ENDIF 
      NEXT i
	 NEXT j
	! now see if this is a fresh shot
        if turn1=1 then % plays player 1
		   if shooted1[x1,y1]=0 then 
		     freshshot=1
			   shooted1[x1,y1]=1   
			   ! now render the shot with a green circle for current player
         gr.color 255,0,255,0,0
	       gr.circle cir1,100+87*x1-44,100+87*y1-44, 35
         gr.render
		   else 
		    freshshot=0
		  	 goto retouchme
	 	    endif
		endif   
	  
    if turn1=0 then % plays player 2
		   if shooted2[x1,y1]=0 then 
		     freshshot=1
			   shooted2[x1,y1]=1   
			   !now render the shot with a green circle for current player
         gr.color 255,0,255,0,0  
	       gr.circle cir1,100+87*x1-44,100+87*y1-44, 35
         gr.render
		   else 
		    freshshot=0
			   goto retouchme
	 	   endif
	endif   
	GR.hide re5
	GR.render
	
RETURN	% with coordinates of shot, and a circle on currents player screen
 
waitclient:
     ! wait 60 sec for server
     flag=0
     maxclock = clock() + 60000
     do
     socket.client.read.ready flag
      if clock() > maxclock then
	   gr.front 0
       print "Read time out"
      end
    endif
    until flag
return 
 
 waitserver:
     ! wait 60 sec for response
     flag=0
     maxclock = clock() + 60000
     do
     socket.server.read.ready flag
      if clock() > maxclock then
	  gr.front 0
      print "Read time out"
      end
    endif
    until flag
return 
 
