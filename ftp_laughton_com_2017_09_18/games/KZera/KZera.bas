!debug.on

fn.def atan2(nx , ny ,pi[])
	If nx <> 0 Then
	   tanout = atan(ny / nx)
	   If ny < 0 & nx > 0 Then tanout = tanout + pi[2]
	   If ny > 0 & nx < 0 Then tanout = tanout + pi[1]
	   If ny < 0 & nx < 0 Then tanout = tanout + pi[1]
	   If ny = 0 & nx < 0 Then tanout = pi[1]
	Else
	   tanout = pi[2] + pi[3]
	End If
	fn.rtn tanout
fn.end

Dim polys[12] 
Dim polyT[12]
Dim TXT[12]
Dim pi[4]
pi[1]=atan(1)*4
pi[2]= pi[1] *2
pi[3]= pi[1]/2
pi[4]= 0-pi[1]
speed =0
RCOS =0
RSIN =0
rotval =0
SRad =0
Erad =0
Prad =0
mFlag =0
pyth =0
ago = 150 ^ 2



gr.open 255, 0, 0, 0
gr.Orientation 0
gr.Screen mx, my
gr.scale (mx /800), (my/480)

!-------------------
!font scaling 
!--------------------

gr.text.size 10
gr.text.width t,"5"
i=(my / 28)*10/t
gr.text.size i
!---------------------


mxt = mx *2/ 3
myt = my / 2
gr.Color 255, 0, 0, 0, 0
 
array.load RC[],255,0,64,255,255,0,255,0,138,162,255,192
array.load GC[],0,255,64,255,0,255,138,255,0,255,0,162
array.load BC[],0,0,255,0,255,255,0,138,255,0,162,255

gr.color 255,rc[1],gc[1],bc[1],1
gr.text.draw txt[1],mx/20,my*2/14,"I Don't Think So. "
gr.color 255,rc[2],gc[2],bc[2],1
gr.text.draw txt[2],mx/20,my*3/14,"I Will be So. "
gr.color 255,rc[3],gc[3],bc[3],1
gr.text.draw txt[3],mx/20,my*4/14,"It is Unclear. "
gr.color 255,rc[4],gc[4],bc[4],1
gr.text.draw txt[4],mx/20,my*5/14,"Yes. "
gr.color 255,rc[5],gc[5],bc[5],1
gr.text.draw txt[5],mx/20,my*6/14,"No. "
gr.color 255,rc[6],gc[6],bc[6],1
gr.text.draw txt[6],mx/20,my*7/14,"Maybe. "
gr.color 255,rc[7],gc[7],bc[7],1
gr.text.draw txt[7],mx/20,my*8/14,"The Future is Hazy. " 
gr.color 255,rc[8],gc[8],bc[8],1
gr.text.draw txt[8],mx/20,my*9/14,"I am Positive. "
gr.color 255,rc[9],gc[9],bc[9],1
gr.text.draw txt[9],mx/20,my*10/14,"Not Going to Happen." 
gr.color 255,rc[10],gc[10],bc[10],1
gr.text.draw txt[10],mx/20,my*11/14,"Don't Count On It."
gr.color 255,rc[11],gc[11],bc[11],1
gr.text.draw txt[11],mx/20,my*12/14,"You Can Count On It."
gr.color 255,rc[12],gc[12],bc[12],1
gr.text.draw txt[12],mx/20,my*13/14,"No Way of Knowing."

For i = 1 To 12
	gr.modify txt[i],"alpha",96
next

For i = 1 To 12
    List.create n,Polys[i]
	 List.create n,PolyT[i]
    list.add Polys[i], 0
    list.add Polys[i], 0
    list.add Polys[i],Cos(i * pi[2] / 12) * 150
    list.add Polys[i],Sin(i * pi[2] / 12) * 150
    !list.add Polys[i],Cos((i + 0.25) * pi[2] / 12) * 150
    !list.add Polys[i],Sin((i + 0.25) * pi[2] / 12) * 150
    list.add Polys[i],Cos((i + 0.5) * pi[2] / 12) * 150
    list.add Polys[i],Sin((i + 0.5) * pi[2] / 12) * 150
    !list.add Polys[i],Cos((i + 0.75) * pi[2] / 12) * 150
    !list.add Polys[i],Sin((i + 0.75) * pi[2] / 12) * 150
    list.add Polys[i],Cos((i + 1) * pi[2] / 12) * 150
    list.add Polys[i],Sin((i + 1) * pi[2] / 12) * 150
    list.add.list PolyT[i],Polys[i]
    gr.color 255,rc[i],gc[i],bc[i],1
	 gr.poly obj_ptr,polyT[i],mxt,myt 
Next

list.create n,point
list.add point,0,-140,-20,-170,20,-170
gr.color 255,255,255,255,1
gr.poly obj_ptr,point,mxt,myt 

!debug.echo.on
do 
	!spinner------------------
	gr.touch touched,x,y
	if touched  then 
		pyth = (X - mxt)* (X - mxt) + (Y - myt)*(Y - myt)
		If mFlag=1 Then	
			If pyth < ago Then
				Prad = SRad
				Srad = atan2(X -mxt, Y - myt,pi[])
				rotval = rotval + (Srad - PRad)
			Else
				Prad = SRad
				Srad = atan2(X -mxt, Y - myt,pi[])
				speed = SRad - Prad
				print speed,"out of range"
				If speed > pi[1] Then speed = pi[4]
				If speed < pi[4] Then speed = pi[1] 
				mFlag = 0
			End If
		Else
			If pyth < ago Then
				SRad = atan2(X - mxt, Y - myt,pi[])
				PRad= SRad
				mFlag = 1
				For i = 1 To 12
					gr.modify txt[i],"alpha",96
				next
			End If
		End If
	else
		If mFlag=1 Then
			speed = SRad - Prad
			print speed,"off the screen"
			If speed > pi[1] Then speed = pi[4]
			If speed < pi[4] Then speed = pi[1] 
		endif
		mFlag = 0
	endif
	!end spinner-----------------

	If mFlag =0 Then
		rotval = rotval + speed
		speed = speed * 0.95
		If Abs(speed) < 0.001 Then 
		      speed = 0
		      i=floor(rotval*12/pi[2])
		      i=mod(19-i,12)+1
		      gr.modify txt[i],"alpha",255
		endif
	End If
	
	If rotval > pi[2] Then 
		rotval = rotval - pi[2]
	endif
	
	If rotval < 0 Then 
		rotval = rotval + pi[2]
	endif
	
	RCOS = Cos(rotval)
	RSIN = Sin(rotval)
	
	For i = 1 To 12
		For j = 1 To 7 step2
			 list.get polys[i],j,tmpx
			 list.get polys[i],j+1,tmpy
		    list.replace polyT[i],j, RCOS * tmpx - RSIN * tmpy 
		    list.replace polyT[i],j+1, RCOS * tmpy + RSIN * tmpx 
		Next

	Next
	gr.render
	pause 5
until 0


!OnError:
gr.Close
End