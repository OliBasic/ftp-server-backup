! CodeWar
! Aat @2016
FN.DEF DrawPiece(Piece,Column,Row)
	GR.BITMAP.DRAW g,Piece,8+(Column-1)*71,8+(Row-1)*71
	FN.RTN g
FN.END
FN.DEF MovePiece(Piece,PieceCover,BackSide,Column,Row)
	GR.MODIFY Piece,"x",8+(Column-1)*71
	GR.MODIFY Piece,"y",8+(Row-1)*71
	GR.MODIFY PieceCover,"x",8+(Column-1)*71
	GR.MODIFY PieceCover,"y",8+(Row-1)*71
	GR.MODIFY BackSide,"x",8+(Column-1)*71
	GR.MODIFY BackSide,"y",8+(Row-1)*71
FN.END
FN.DEF MyPieceValue(Board[],s,x)
	V=MOD(Board[s-1+x,1],100)
	FN.RTN V
FN.END
FN.DEF MyPieceColor(Board[],s,x)
	C=(MOD(Board[s-1+x,1],500)-MOD(Board[s-1+x,1],100))/100+1
	FN.RTN C
FN.END
FN.DEF YourPieceValue(Board[],s,x)
	V=MOD(Board[s-1+x,10],100)
	FN.RTN V
FN.END
FN.DEF YourPieceColor(Board[],s,x)
	C=(MOD(Board[s-1+x,10],500)-MOD(Board[s-1+x,10],100))/100+1
	FN.RTN C
FN.END
GR.OPEN 255,120,120,120,0,0
GR.SCREEN w,h
ScaleX=720
ScaleY=w/h*ScaleX
sx=h/ScaleX
sy=w/ScaleY
GR.SCALE sx,sy
WAKELOCK 3
RePlay:
GOSUB Init
GOSUB CreatePieces
GOSUB DrawBoard
GOSUB MakePositions
GOSUB Start
GOSUB ShowRes
GOSUB PlayGame
DO
	DIALOG.MESSAGE "PLAY AGAIN?","",Answer,"YES","NO"
UNTIL Answer>0
IF Answer=1 THEN GOTO RePlay
WAKELOCK 5
EXIT

ONBACKKEY:
	DO
		DIALOG.MESSAGE "I guessed "+INT$(YourCodeBroken)+"/"+INT$(YourNumOfPieces)+", You guessed "+INT$(MyCodeBroken)+"/"+INT$(MyNumOfPieces),"Do you want to QUIT now?",Answer,"YES","NO"
	UNTIL Answer>0
	IF Answer=1 THEN
		WAKELOCK 5
		EXIT
	ENDIF
BACK.RESUME

GetTouch:
	DO
		GR.TOUCH Touched,x,y
	UNTIL Touched
	DO
		GR.TOUCH Touched,x,y
	UNTIL !Touched
	x/=sx:y/=sy
	x=INT((x-8)/71)+1:y=INT((y-8)/71)+1
RETURN

PlayGame:
	DO
		Turn=Turn+1
		IF Turn/2=INT(Turn/2) THEN
			! My Turn
			MyNumOfTurns=MyNumOfTurns+1
			GR.MODIFY MySkore,"text",INT$(YourCodeBroken)+"/"+INT$(MyNumOfTurns)
			GR.MODIFY MyText,"text","I will pick a piece"
			GR.SHOW MyText:GR.RENDER
			PAUSE 1000
			DO
				! Choose row
				y=FLOOR(RND()*4+4)
				! Choose column
				IF y=4 | y=7 THEN
					x=FLOOR(RND()*10+1)
				ELSE
					x=FLOOR(RND()*2+5)
				ENDIF
			UNTIL Board[x,y]>0
			x1=x:y1=y
			IF Board[x1,y1]<100 THEN
				GR.HIDE CoverMaskRed[MOD(Board[x1,y1],100)]
				MovePiece(DrawRedPiece[MOD(Board[x1,y1],100)],CoverMaskRed[MOD(Board[x1,y1],100)],CoverRed[MOD(Board[x1,y1],100)],MyStartPlace+MyNumOfPieces,1)
			ELSE
				GR.HIDE CoverMaskBlue[MOD(Board[x1,y1],100)]
				MovePiece(DrawBluePiece[MOD(Board[x1,y1],100)],CoverMaskBlue[MOD(Board[x1,y1],100)],CoverBlue[MOD(Board[x1,y1],100)],MyStartPlace+MyNumOfPieces,1)
			ENDIF
			Board[MyStartPlace+MyNumOfPieces,1]=Board[x1,y1]:Board[x1,y1]=0
			! Cycle through known pieces
			Dup=0
			FOR i=1 TO MyNumOfPieces
				IF MyPieceValue(Board[],MyStartPlace,i)=MyPieceValue(Board[],MyStartPlace,MyNumOfPieces+1) THEN Dup=1
			NEXT i
			FOR i=1 TO YourNumOfPieces
				IF Board[YourStartPlace+i-1,10]>500 & YourPieceValue(Board[],YourStartPlace,i)=MyPieceValue(Board[],MyStartPlace,MyNumOfPieces+1) THEN Dup=1
			NEXT i
			! Remove double values
			IF Dup=1 THEN
				FOR i=1 TO YourNumOfpieces
					LIST.SEARCH CrackLst[i],MyPieceValue(Board[],MyStartPlace,MyNumOfPieces+1),Index
					LIST.SIZE CrackLst[i],L
					IF L>1 & Index>0 THEN LIST.REMOVE CrackLst[i],Index
				NEXT i
			ENDIF
			! Some more numbers may be skipped
			FOR i=1 TO YourNumOfPieces
				IF MyPieceColor(Board[],MyStartPlace,MyNumOfPieces+1)=YourPieceColor(Board[],YourStartPlace,i) THEN
					LIST.SEARCH CrackLst[i],MyPieceValue(Board[],MyStartPlace,MyNumOfPieces+1),Index
					LIST.SIZE CrackLst[i],L
					IF L>1 & Index>0 THEN LIST.REMOVE CrackLst[i],Index
				ENDIF
			NEXT i
			GOSUB ShowRes
			PAUSE 1000
			DO
				! Choose a piece to guess
				MinNum=10
				FOR i=1 TO YourNumOfpieces
					IF Board[YourStartPlace+i-1,10]<500 THEN
						! Is there a good chance to make a correct guess
						LIST.SIZE CrackLst[i],L
						IF L<MinNum THEN
							MinNum=L
							GuessPiece=i
						ENDIF
					ENDIF
				NEXT i
				IF MinNum>3 THEN
					! Not a good chance, so random
					DO
						GuessPiece=FLOOR(RND()*YourNumOfPieces+1)
					UNTIL Board[GuessPiece+YourStartPlace-1,10]<500
				ENDIF
				GR.MODIFY DrawArrow,"x",8+(GuessPiece+YourStartPlace-2)*71
				GR.SHOW DrawArrow
				LIST.SIZE CrackLst[GuessPiece],L
				! Pick a value around the middle of the list
				R=FLOOR(2*RND()+1):GuessIndex=FLOOR(0.5*L+0.5*R)
				LIST.GET CrackLst[GuessPiece],GuessIndex,GuessValue
				GR.MODIFY MyText,"text","I will guess the value of this piece"
				GR.RENDER
				PAUSE 1000
				DO
					Cheat=0
					DO
						DIALOG.MESSAGE "I guess this piece is a "+AllPieces$[GuessValue]+"?","Is this correct?",Answer,"YES","NO"
					UNTIL Answer>0
					IF (Answer=1 & YourPieceValue(Board[],YourStartPlace,GuessPiece)<>GuessValue) | (Answer=2 & YourPieceValue(Board[],YourStartPlace,GuessPiece)=GuessValue) THEN Cheat=1
				UNTIL !Cheat
				IF Answer=2 THEN
					! Show my new piece
					IF Board[MyStartPlace+MyNumOfPieces,1]<100 THEN
						GR.HIDE CoverRed[MOD(Board[MyStartPlace+MyNumOfPieces,1],100)]
					ELSE
						GR.HIDE CoverBlue[MOD(Board[MyStartPlace+MyNumOfPieces,1],100)]
					ENDIF
					Board[MyStartPlace+MyNumOfPieces,1]=Board[MyStartPlace+MyNumOfPieces,1]+500
					GR.HIDE DrawArrow:GR.RENDER
					LIST.SEARCH CrackLst[GuessPiece],GuessValue,Index
					LIST.SIZE CrackLst[GuessPiece],L
					IF L>1 & Index>0 THEN LIST.REMOVE CrackLst[GuessPiece],Index
					MyCodeBroken=MyCodeBroken+1
					GR.MODIFY YourSkore,"text",INT$(MyCodeBroken)+"/"+INT$(YourNumOfTurns)
					GOSUB ShowRes
				ELSE
					! Show guessed piece
					IF Board[GuessPiece+YourStartPlace-1,10]<100 THEN
						GR.HIDE CoverMaskRed[MOD(Board[GuessPiece+YourStartPlace-1,10],100)]
					ELSE
						GR.HIDE CoverMaskBlue[MOD(Board[GuessPiece+YourStartPlace-1,10],100)]
					ENDIF
					Board[GuessPiece+YourStartPlace-1,10]=Board[GuessPiece+YourStartPlace-1,10]+500
					GR.HIDE DrawArrow:GR.RENDER
					LIST.CLEAR CrackLst[GuessPiece]
					LIST.ADD CrackLst[GuessPiece],GuessValue
					YourCodeBroken=YourCodeBroken+1
					GR.MODIFY MySkore,"text",INT$(YourCodeBroken)+"/"+INT$(MyNumOfTurns)
					GOSUB UpDateKnowledge
					GOSUB ShowRes
					IF MyNumOfPieces=9 & YourNumOfPieces= 10 THEN
						Answer=0
					ELSE
						IF YourNumOfPieces=YourCodeBroken THEN
							GR.MODIFY MyText,"text","YES! I WIN!"
							GR.RENDER
							Winner$="me"
							Answer=0
						ELSE
							! Decide to guess next or stop
							MinNum=10
							FOR i=1 TO YourNumOfpieces
								! Is there a good chance to make a correct guess
								LIST.SIZE CrackLst[i],L
								IF L<MinNum THEN
									MinNum=L
								ENDIF
							NEXT i
							IF MinNum<4 THEN
								ContVal=1
							ELSE
								ContVal=FLOOR(RND()*4+1)
							ENDIF
							IF ContVal=1 THEN
								! Guess next in 25% of cases
								GR.MODIFY MyText,"text","I will make another guess"
								GR.RENDER
								PAUSE 2000
							ELSE
								! Stop guessing
								GR.MODIFY MyText,"text","I will stop guessing and extend my code"
								GR.RENDER
								PAUSE 2000
								Answer=2
							ENDIF
						ENDIF
						GOSUB ShowRes
					ENDIF
				ENDIF
			UNTIL Answer<>1
			MyNumOfPieces=MyNumOfPieces+1
			IF Winner$="me" THEN
				NoWait=1
			ELSE
				NoWait=0
			ENDIF
			GOSUB ArrMyCode
			GOSUB ShowRes
			PAUSE 1000
			GR.HIDE MyText:GR.RENDER
		ELSE
			! Your Turn
			YourNumOfTurns=YourNumOfTurns+1
			GR.MODIFY YourSkore,"text",INT$(MyCodeBroken)+"/"+INT$(YourNumOfTurns)
			Result=0
			GR.MODIFY YourText,"text","Pick a piece"
			GR.SHOW YourText:GR.RENDER
			DO
				DO
					GOSUB GetTouch
				UNTIL x<11 & y>3 & y<8
			UNTIL Board[x,y]>0 & Board[x,y]<500
			x1=x:y1=y
			IF Board[x1,y1]<100 THEN
				MovePiece(DrawRedPiece[MOD(Board[x1,y1],100)],CoverMaskRed[MOD(Board[x1,y1],100)],CoverRed[MOD(Board[x1,y1],100)],YourStartPlace+YourNumOfPieces,10)
				GR.HIDE CoverRed[MOD(Board[x1,y1],100)]
			ELSE
				MovePiece(DrawBluePiece[MOD(Board[x1,y1],100)],CoverMaskBlue[MOD(Board[x1,y1],100)],CoverBlue[MOD(Board[x1,y1],100)],YourStartPlace+YourNumOfPieces,10)
				GR.HIDE CoverBlue[MOD(Board[x1,y1],100)]
			ENDIF
			Board[YourStartPlace+YourNumOfPieces,10]=Board[x1,y1]:Board[x1,y1]=0
			DO
				GR.MODIFY YourText,"text","Tap one of my code pieces and guess its value"
				GR.RENDER
				DO
					DO
						GOSUB GetTouch
					UNTIL x>=MyStartPlace & x<MyStartPlace+MyNumOfPieces & y=1
				UNTIL Board[x,y]<500
				x2=x:y2=y
				DO
					DIALOG.SELECT PickVal,AllPieces$[],"Which value do you guess for this piece"
				Until PickVal>0
				IF PickVal=MOD(Board[x2,y2],100) THEN
					GR.MODIFY YourText,"text","Well done! You guessed the right value!"
					! Show my piece
					IF Board[x2,y2]<100 THEN
						GR.HIDE CoverRed[MOD(Board[x2,y2],100)]
					ELSE
						GR.HIDE CoverBlue[MOD(Board[x2,y2],100)]
					ENDIF
					Board[x2,y2]=Board[x2,y2]+500
					GR.RENDER
					MyCodeBroken=MyCodeBroken+1
					GR.MODIFY YourSkore,"text",INT$(MyCodeBroken)+"/"+INT$(YourNumOfTurns)
					GOSUB ShowRes
					IF YourNumOfPieces=10 & MyNumOfPieces=10 THEN
						Result=0
					ELSE
						IF MyNumOfPieces=MyCodeBroken THEN
							GR.MODIFY YourText,"text","Well done! You WIN!"
							GR.RENDER
							YourNumOfPieces=YourNumOfPieces+1
							Winner$="you"
							Result=0
						ELSE
							DO
								DIALOG.MESSAGE "What do you want to do?","Continue or Stop guessing",Result,"CONTINUE","STOP"
							UNTIL Result>0
							IF Result=2 THEN
								YourNumOfPieces=YourNumOfPieces+1
								NoWait=0:GOSUB ArrYourCode
							ENDIF
						ENDIF
					ENDIF
				ELSE
					GR.MODIFY YourText,"text","Sorry, WRONG value"
					GR.RENDER
					PAUSE 2000
					Result=0
					YourCodeBroken=YourCodeBroken+1
					GR.MODIFY MySkore,"text",INT$(YourCodeBroken)+"/"+INT$(MyNumOfTurns)
					GOSUB ShowRes
					! Show( and rank) your new piece
					GR.RENDER
					Board[YourStartPlace+YourNumOfPieces,10]=Board[YourStartPlace+YourNumOfPieces,10]+500
					IF YourPieceColor(Board[],YourStartPlace,YourNumOfPieces+1)=1 THEN
						GR.HIDE CoverMaskRed[YourPieceValue(Board[],YourStartPlace,YourNumOfPieces+1)]
					ELSE
						GR.HIDE CoverMaskBlue[YourPieceValue(Board[],YourStartPlace,YourNumOfPieces+1)]
					ENDIF
					! Cycle through known pieces
					Dup=0
					FOR i=1 TO MyNumOfPieces
						IF MyPieceValue(Board[],MyStartPlace,i)=YourPieceValue(Board[],YourStartPlace,YourNumOfPieces+1) THEN Dup=1
					NEXT i
					FOR i=1 TO YourNumOfPieces
						IF Board[YourStartPlace+i-1,10]>500 & YourPieceValue(Board[],YourStartPlace,i)=YourPieceValue(Board[],YourStartPlace,YourNumOfPieces+1) THEN Dup=1
					NEXT i
					! Remove double values
					IF Dup=1 THEN
						FOR i=1 TO YourNumOfpieces
							LIST.SEARCH CrackLst[i],YourPieceValue(Board[],YourStartPlace,YourNumOfPieces+1),Index
							LIST.SIZE CrackLst[i],L
							IF L>1 & Index>0 THEN LIST.REMOVE CrackLst[i],Index
						NEXT i
					ENDIF
					YourNumOfPieces=YourNumOfPieces+1
					NoWait=0:GOSUB ArrYourCode
					LIST.CLEAR CrackLst[FndI]
					LIST.ADD CrackLst[FndI],YourPieceValue(Board[],YourStartPlace,FndI)
					GOSUB AdjustAll
					GOSUB UpDateKnowledge
					PAUSE 2000
				ENDIF
				GR.RENDER
				GOSUB ShowRes
				PAUSE 1000
			UNTIL Result<>1
			GR.HIDE YourText:GR.RENDER
		ENDIF
		GOSUB ShowRes

	UNTIL (MyNumOfPieces=10 & YourNumOfPieces=10) | Winner$<>""
	! Check winner
	GR.SHOW MaskBoard
	GR.TEXT.SIZE 50
	GR.TEXT.ALIGN 2
	GR.COLOR 255,255,255,255,1
	IF Winner$<>"" THEN
		IF Winner$="you" THEN
			GR.TEXT.DRAW g,360,360,"YOU WIN !!!!"
			W$=INT$(MyCodeBroken)+"/"+INT$(YourNumOfTurns)
		ELSE
			GR.TEXT.DRAW g,360,360,"I WIN !!!!"
			W$=INT$(YourCodeBroken)+"/"+INT$(MyNumOfTurns)
		ENDIF
	ELSE
		IF YourCodeBroken=MyCodeBroken THEN
			GR.TEXT.DRAW g,360,360,"It's a DRAW !!!!"
			W$=INT$(MyCodeBroken)+"/"+INT$(YourNumOfTurns)
			Winner$="you"
		ELSE
			IF YourCodeBroken>MyCodeBroken THEN
				GR.TEXT.DRAW g,360,360,"I WIN !!!!"
				W$=INT$(YourCodeBroken)+"/"+INT$(MyNumOfTurns)
				Winner$="me"
			ELSE
				GR.TEXT.DRAW g,360,360,"You WIN !!!!"
				W$=INT$(MyCodeBroken)+"/"+INT$(YourNumOfTurns)
				Winner$="you"
			ENDIF
		ENDIF
	ENDIF
	GR.TEXT.SIZE 25
	GR.TEXT.DRAW g,360,510,"I guessed "+INT$(YourCodeBroken)+" of your pieces in "+INT$(MyNumOfTurns)+" turns"
	GR.TEXT.DRAW g,360,560,"You guessed "+INT$(MyCodeBroken)+" of my pieces in "+INT$(YourNumOfTurns)+" turns"
	GR.RENDER
	IF Winner$="me" | YourCodeBroken>=MyCodeBroken
		GR.MODIFY MyText,"text","I will show my pieces"
		GR.SHOW MyText
		GR.RENDER
		PAUSE 2000
		FOR i=1 TO MyNumOfPieces
			IF Board[MyStartPlace+i-1,1]<500 & Board[MyStartPlace+i-1,1]>0 THEN
				IF Board[MyStartPlace+i-1,1]<100 THEN
					GR.HIDE CoverRed[MOD(Board[MyStartPlace+i-1,1],100)]
					GR.SHOW CoverMaskRed[MOD(Board[MyStartPlace+i-1,1],100)]
				ELSE
					GR.HIDE CoverBlue[MOD(Board[MyStartPlace+i-1,1],100)]
					GR.SHOW CoverMaskBlue[MOD(Board[MyStartPlace+i-1,1],100)]
				ENDIF
			GR.RENDER
			PAUSE 500
			ENDIF
		NEXT i
	ENDIF
	GR.HIDE MyText:GR.RENDER
	! Check Hi Score
	InsPnt1=0:InsPnt2=0
	FOR i=1 TO 5
		IF (VAL(WORD$(W$,2,"/"))<=VAL(WORD$(Score$[i],2,"/"))) THEN
			InsPnt1=i
			F_N.BREAK
		ENDIF
	NEXT i
	IF InsPnt1>0 THEN
		FOR i=InsPnt1 TO 5
			IF (VAL(WORD$(W$,2,"/"))=VAL(WORD$(Score$[i],2,"/"))) & (VAL(WORD$(W$,1,"/"))<VAL(WORD$(Score$[i],1,"/"))) THEN
				InsPnt2=InsPnt2+1
			ENDIF
		NEXT i
	ENDIF
	InsPnt=InsPnt1+InsPnt2
	IF InsPnt>0 THEN
		! New Hi Score
		IF Winner$="you" THEN
			DO
				INPUT "You have a new High Score! Please, enter your name",NN$,"",IsCancelled
			UNTIL !IsCancelled
		ELSE
			NN$="Me"
			DIALOG.MESSAGE "I have a new High Score!","tap screen to continue",Answer
		ENDIF
		FOR i=4 TO InsPnt STEP-1
			Name$[i+1]=Name$[i]
			Score$[i+1]=Score$[i]
		NEXT i
		Name$[InsPnt]=NN$
		Score$[InsPnt]=W$
		FOR i=1 TO 5
			GR.MODIFY HiName[i],"text",INT$(i)+"."+Name$[i]
			GR.MODIFY HiScore[i],"text",Score$[i]
		NEXT i
		GR.RENDER
	ENDIF
	TEXT.OPEN W,ScoreTable,DataPath$+"HiScore.CW"
		FOR i=1 TO 5
			TEXT.WRITELN ScoreTable,Name$[i]
			TEXT.WRITELN ScoreTable,Score$[i]
		NEXT i
	TEXT.CLOSE ScoreTable
RETURN

InsertLst:
	! Shift right lists from insertion point
	FOR i=YourNumOfPieces TO FndI+1 STEP -1
		LIST.CLEAR CrackLst[i]
		LIST.SIZE CrackLst[i-1],L
		FOR j=1 TO L
			LIST.GET CrackLst[i-1],j,K
			LIST.ADD CrackLst[i],K
		NEXT j
	NEXT i
	! Insert new values in list
	LIST.CLEAR CrackLst[FndI]
	MaxVal=12-INT((YourNumOfPieces-FndI)/2)
	MinVal=INT((FndI-1)/2)+1
	FOR i=MinVal TO Maxval
		LIST.ADD CrackLst[FndI],i
	NEXT i
	GOSUB AdjustAll
RETURN

UpDateKnowledge:
	! Learn from known pieces
	FOR i=2 TO YourNumOfPieces
	! Remove impossible high values
		DO
			LIST.SIZE CrackLst[i-1],L
			LIST.GET CrackLst[i-1],L,V1
			LIST.SIZE CrackLst[i],L2
			LIST.GET CrackLst[i],L2,V2
			IF V1>V2 & L>1 THEN LIST.REMOVE CrackLst[i-1],L
		UNTIL V1<=V2 | L=1
	! Remove impossible low values
		DO
			LIST.GET CrackLst[i-1],1,V1
			LIST.SIZE CrackLst[i],L
			LIST.GET CrackLst[i],1,V2
			IF V1>V2 & L>1 THEN LIST.REMOVE CrackLst[i],1
		UNTIL V1<=V2 | L=1
	NEXT i
RETURN

AdjustAll:
	! Adjust lists to the left
	FOR i=YourNumOfPieces-1 TO 1 STEP-1
		! Find highest value in right list
		LIST.SIZE CrackLst[i+1],L
		LIST.GET CrackLst[i+1],L,HiVal
		IF MOD(BOARD[YourStartPlace+i,10],500)>100 & MOD(BOARD[YourStartPlace+i-1,10],500)<100 THEN HiVal=HiVal+1
		Above=0
		DO
			LIST.SEARCH CrackLst[i],HiVal+Above,Index
			LIST.SIZE CrackLst[i],L
			IF L>1 & Index>0 THEN LIST.REMOVE CrackLst[i],Index
			Above=Above+1
		UNTIL HiVal+Above>12 | L=1
	NEXT i
	! Adjust lists to the right
	FOR i=2 TO YourNumOfPieces
		! Find lowest value in left list
		LIST.SIZE CrackLst[i-1],L
		LIST.GET CrackLst[i-1],1,LoVal
		IF MOD(BOARD[YourStartPlace+i-2,10],500)<100 & MOD(BOARD[YourStartPlace+i-1,10],500)>100 THEN LoVal=LoVal-1
		Below=0
		DO
			LIST.SEARCH CrackLst[i],LoVal+Below,Index
			LIST.SIZE CrackLst[i],L
			IF L>1 & Index>0 THEN LIST.REMOVE CrackLst[i],Index
			Below=Below-1
		UNTIL LoVal+Below<1 | L=1
	NEXT i
RETURN

ShowRes:
	! Show how close from finding your code
	TK=0
	FOR i=1 TO YourNumOfPieces
		LIST.SIZE CrackLst[i],L
		TK=TK+L
	NEXT i
	TK=TK-YourNumOfPieces
	GR.MODIFY IKnow,"bottom",100+10*(44-TK)
	GR.MODIFY Percent,"text",INT$(100-TK/0.44)+" %"
	GR.RENDER
RETURN

Init:
	GR.CLS
	GR.SET.ANTIALIAS 0
	DIM RedPiece[12],BluePiece[12],CoverRed[12],CoverBlue[12],CoverMaskRed[12],CoverMaskBlue[12]
	DIM DrawRedPiece[12],DrawBluePiece[12]
	DIM DistributeX[24],DistributeY[24],PickPos[24]
	DIM CopyCode[10],CopyColor[10]
	DIM Board[10,10],CrackLst[10]
	DIM HiName[5],HiScore[5]
	DataPath$="../../CodeWar/data/"
	GR.BITMAP.LOAD Lady,DataPath$+"Lady.png"
	GR.BITMAP.LOAD Help,DataPath$+"Help.png"
	ARRAY.LOAD CrackCode[],1,2,3,4,5,6,7,8,9,10
	ARRAY.LOAD AllPieces$[],"1. Flag","2. Bomb","3. Spy","4. Scout","5. Miner","6. Sergeant","7. Lieutenant","8. Captain","9. Major","10. Colonel","11. General","12. Marshal"
	ARRAY.LOAD Name$[],"Nobody","Nobody","Nobody","Nobody","Nobody"
	ARRAY.LOAD Score$[],"0/10","0/10","0/10","0/10","0/10"
	FILE.EXISTS IsThere,DataPath$+"HiScore.CW"
	IF !IsThere THEN
		TEXT.OPEN W,ScoreTable,DataPath$+"HiScore.CW"
			FOR i=1 TO 5
				TEXT.WRITELN ScoreTable,Name$[i]
				TEXT.WRITELN ScoreTable,Score$[i]
			NEXT i
		TEXT.CLOSE ScoreTable
	ENDIF
	TEXT.OPEN R,ScoreTable,DataPath$+"HiScore.CW"
		FOR i=1 TO 5
			TEXT.READLN ScoreTable,Name$[i]
			TEXT.READLN ScoreTable,Score$[i]
		NEXT i
	TEXT.CLOSE ScoreTable
	FONT.LOAD Gothic,DataPath$+"Gothic.ttf"
	MyNumOfPieces=4:YourNumOfPieces=4
	MyStartPlace=4:YourStartPlace=4
	MyCodeBroken=0:YourCodeBroken=0
	Winner$=""
	FOR i=1 TO 10
		LIST.CREATE N,CrackLst[i]
		DistributeX[i]=i:DistributeY[i]=4
		DistributeX[i+14]=i:DistributeY[i+14]=7
	NEXT i
	DistributeX[11]=5:DistributeY[11]=5
	DistributeX[12]=6:DistributeY[12]=5
	DistributeX[13]=5:DistributeY[13]=6
	DistributeX[14]=6:DistributeY[14]=6
	FOR i=1 TO 10
		LIST.CLEAR CrackLst[i]
	NEXT i
	FOR i=1 TO 11
			LIST.ADD CrackLst[1],i
			LIST.ADD CrackLst[2],i
			LIST.ADD CrackLst[3],i+1
			LIST.ADD CrackLst[4],i+1
	NEXT i
	FOR i=1 TO 24
		PickPos[i]=i
	NEXT i
	MyNumOfTurns=0
	YourNumOfTurns=0
	GR.BITMAP.DRAW Intro,Help,0,0
	GR.COLOR 255,255,255,0,1
	GR.TEXT.SIZE 12
	GR.TEXT.ALIGN 1
	GR.TEXT.SETFONT Gothic
	GR.TEXT.DRAW g,955,28,"DONE"
	GR.COLOR 255,255,255,0,0
	GR.RECT g,945,5,1018,35
	GR.TEXT.SETFONT "Sans Serif"
	GR.RENDER
	NwY=0
	Kap=0
	DO
		DO
			GR.TOUCH Touched,x,y1
		UNTIL Touched
		y1/=sy
		DO
			GR.TOUCH Touched,x,y2
		UNTIL !Touched
		x/=sx
		y2/=sy
		IF x>945 & x<1018 & y2>5 & y2<1018 THEN
			Kap=1
		ELSE
			IF y1-20<Y2 & NwY<=-100 THEN
				NwY=NwY+200
				GR.MODIFY Intro,"y",NwY
				GR.RENDER
			ENDIF
			IF y1+20>Y2 & NwY>=-700 THEN
				NwY=NwY-200
				GR.MODIFY Intro,"y",NwY
				GR.RENDER
			ENDIF
		ENDIF
	UNTIL Kap=1
	GR.CLS
	POPUP "OK, let's get started",-350,-200
RETURN

Start:
	GR.SHOW MaskBoard
	GR.SHOW LakeLady
	GR.SHOW BLink
	Lot=FLOOR(RND()*11)
	Toss$="Red"
	DO
		DIALOG.MESSAGE "Who will start? The 'Lady of the lake' will tell !!!","If the lakes turn RED YOU wiil start, if they turn BLUE I will start",Q,"OK"
	UNTIL Q=1
	FOR i=1 TO 10+Lot
		IF i/2=INT(i/2) THEN
			GR.MODIFY LakeL,"paint",RedPaint
			GR.MODIFY LakeR,"paint",RedPaint
			GR.MODIFY Blink,"paint",RedPaint
			Start$="Red"
		ELSE
			GR.MODIFY LakeL,"paint",BluePaint
			GR.MODIFY LakeR,"paint",BluePaint
			GR.MODIFY Blink,"paint",BluePaint
			Start$="Blue"
		ENDIF
		GR.RENDER
		PAUSE 500
	NEXT i
	POPUP "THE LADY HAS SPOKEN...",-350,120,1
	PAUSE 3000
	GR.MODIFY LakeL,"paint",LakePaint
	GR.MODIFY LakeR,"paint",LakePaint
	GR.HIDE MaskBoard
	GR.HIDE LakeLady
	GR.HIDE BLink
	GR.SHOW Kader1
	GR.SHOW Kader
	GR.SHOW KnowTxt
	GR.SHOW Percent
	GR.SHOW Skore
	GR.RENDER
	GR.COLOR 255,255,255,0,1
	GR.TEXT.SIZE 30
	GR.TEXT.ALIGN 2
	IF Toss$=Start$ THEN
		Turn=0
		GR.TEXT.DRAW MyText,360,195,""
		GR.TEXT.DRAW YourText,360,550,"You will start the game"
		GR.RENDER:PAUSE 2000
		GOSUB YourPick
		GOSUB MyPick
	ELSE
		Turn=1
		GR.TEXT.DRAW MyText,360,195,"I will start the game"
		GR.TEXT.DRAW YourText,360,550,""
		GR.RENDER:PAUSE 2000
		GOSUB MyPick
		GOSUB YourPick
	ENDIF
	GR.TEXT.ALIGN 1
	GR.RENDER
	! If I have a double(s) this value can be skipped
	FOR i=1 TO MyNumOfPieces-1
		FOR j=i+1 TO MyNumOfPieces
			IF MyPieceValue(Board[],MyStartPlace,i)=MyPieceValue(Board[],MyStartPlace,j) THEN
				! Remove double value
				FOR k=1 TO YourNumOfpieces
					LIST.SEARCH CrackLst[k],MyPieceValue(Board[],MyStartPlace,i),Index
					LIST.SIZE CrackLst[k],L
					IF L>1 & Index>0 THEN LIST.REMOVE CrackLst[k],Index
				NEXT k
			ENDIF
		NEXT j
	NEXT i
	! Some numbers can be skipped
	FOR i=1 TO YourNumOfPieces
		FOR j=1 TO MyNumOfPieces
			IF MyPieceColor(Board[],MyStartPlace,j)=YourPieceColor(Board[],YourStartPlace,i) THEN
				LIST.SEARCH CrackLst[i],MyPieceValue(Board[],MyStartPlace,j),Index
				LIST.SIZE CrackLst[i],L
				IF L>1 & Index>0 THEN LIST.REMOVE CrackLst[i],Index
			ENDIF
		NEXT j
	NEXT i
RETURN

YourPick:
	! Player picks code
	GR.MODIFY YourText,"text","Pick your 4 pieces...."
	GR.RENDER
	FOR i=1 TO YourNumOfPieces
		DO
			DO
				GOSUB GetTouch
			UNTIL x<11 & y>3 & y<8
		UNTIL Board[x,y]>0
		IF Board[x,y]<100 THEN
			MovePiece(DrawRedPiece[MOD(Board[x,y],100)],CoverMaskRed[MOD(Board[x,y],100)],CoverRed[MOD(Board[x,y],100)],i+YourStartPlace-1,10)
			GR.HIDE CoverRed[MOD(Board[x,y],100)]
		ELSE
			MovePiece(DrawBluePiece[MOD(Board[x,y],100)],CoverMaskBlue[MOD(Board[x,y],100)],CoverBlue[MOD(Board[x,y],100)],i+YourStartPlace-1,10)
			GR.HIDE CoverBlue[MOD(Board[x,y],100)]
		ENDIF
		Board[i+YourStartPlace-1,10]=Board[x,y]:Board[x,y]=0
		GR.RENDER
	NEXT i
	NoWait=1:GOSUB ArrYourCode
	GOSUB AdjustAll
	PAUSE 1000
	GR.HIDE YourText:GR.RENDER
RETURN

ArrYourCode:
	GR.MODIFY YourText,"text","Arranging your code..."
	GR.RENDER
	! Shift if needed
	IF YourNumOfPieces/2<>INT(YourNumOfPieces/2) THEN
		YourStartPlace=YourStartPlace-1
		FOR i=1 TO YourNumOfPieces
			Board[i+YourStartplace-1,10]=Board[i+YourStartplace,10]
		NEXT i
		Board[YourStartplace+YourNumOfPieces,10]=0
	ENDIF
	NwVal=YourPieceValue(Board[],YourStartPlace,YourNumOfPieces):NwColor=YourPieceColor(Board[],YourStartPlace,YourNumOfPieces)
	FOR i=1 TO YourNumOfPieces
		! Find lowest value
		Lowest=13
		FOR j=1 TO YourNumOfPieces
			IF YourPieceValue(Board[],YourStartPlace,j)<Lowest THEN
				Lowest=YourPieceValue(Board[],YourStartPlace,j)
				FndI=j
			ENDIF
		NEXT j
		! Same values present, then Red first
		FndN=0
		IF FndI<YourNumOfPieces & YourPieceColor(Board[],YourStartPlace,FndI)=2 THEN
			FOR j=FndI+1 TO YourNumOfPieces
				IF YourPieceValue(Board[],YourStartPlace,j)=Lowest THEN FndN=j
			NEXT j
		ENDIF
		IF FndN>FndI THEN FndI=FndN
		! Copy values to row 8
		Board[i+YourStartplace-1,8]=Board[FndI+YourStartplace-1,10]
		Board[FndI+YourStartplace-1,10]=113
	NEXT i
	FOR i=1 TO YourNumOfPieces
		!Set new values on correct place
		Board[i+YourStartPlace-1,10]=Board[i+YourStartPlace-1,8]:Board[i+YourStartPlace-1,8]=0
	NEXT i
	FOR i=1 TO YourNumOfPieces
		! Find insert place
		IF YourPieceValue(Board[],YourStartPlace,i)=NwVal & YourPieceColor(Board[],YourStartPlace,i)=NwColor THEN FndI=i
	NEXT i
	! Show piece to insert
	PtI=YourPieceValue(Board[],YourStartPlace,FndI)
	IF YourPieceColor(Board[],YourStartPlace,FndI)=1 THEN
		MovePiece(DrawRedPiece[PtI],CoverMaskRed[PtI],CoverRed[PtI],YourStartPlace+FndI-1,9)
	ELSE
		MovePiece(DrawBluePiece[PtI],CoverMaskBlue[PtI],CoverBlue[PtI],YourStartPlace+FndI-1,9)
	ENDIF
	IF !NoWait THEN
		GR.RENDER
		PAUSE 1000
	ELSE
		PAUSE 300
	ENDIF
	FOR i=1 TO FndI-1
		PtI=YourPieceValue(Board[],YourStartPlace,i)
		CtI=YourPieceColor(Board[],YourStartPlace,i)
		IF CtI=1 THEN
			MovePiece(DrawRedPiece[PtI],CoverMaskRed[PtI],CoverRed[PtI],i+YourStartPlace-1,10)
		ELSE
			MovePiece(DrawBluePiece[PtI],CoverMaskBlue[PtI],CoverBlue[PtI],i+YourStartPlace-1,10)
		ENDIF
	NEXT i
	IF !NoWait THEN
		GR.RENDER
		PAUSE 1000
	ELSE
		PAUSE 300
	ENDIF
	FOR i=FndI+1 TO YourNumOfPieces
		PtI=YourPieceValue(Board[],YourStartPlace,i)
		CtI=YourPieceColor(Board[],YourStartPlace,i)
		IF CtI=1 THEN
			MovePiece(DrawRedPiece[PtI],CoverMaskRed[PtI],CoverRed[PtI],i+YourStartPlace-1,10)
		ELSE
			MovePiece(DrawBluePiece[PtI],CoverMaskBlue[PtI],CoverBlue[PtI],i+YourStartPlace-1,10)
		ENDIF
	NEXT i
	IF !NoWait THEN
		GR.RENDER
		PAUSE 1000
		GOSUB InsertLst
	ELSE
		PAUSE 300
	ENDIF
	! Insert it
	PtI=YourPieceValue(Board[],YourStartPlace,FndI)
	IF YourPieceColor(Board[],YourStartPlace,FndI)=1 THEN
		MovePiece(DrawRedPiece[PtI],CoverMaskRed[PtI],CoverRed[PtI],YourStartPlace+FndI-1,10)
	ELSE
		MovePiece(DrawBluePiece[PtI],CoverMaskBlue[PtI],CoverBlue[PtI],YourStartPlace+FndI-1,10)
	ENDIF
	GR.RENDER
RETURN

MyPick:
	! Pick code for Android
	GR.MODIFY MyText,"text","Picking my 4 pieces...."
	GR.RENDER:PAUSE 500
	FOR i=1 TO MyNumOfPieces
		DO
			! Choose row
			y=FLOOR(RND()*4+4)
			! Choose column
			IF y=4 | y=7 THEN
				x=FLOOR(RND()*10+1)
			ELSE
				x=FLOOR(RND()*2+5)
			ENDIF
		UNTIL Board[x,y]>0
		IF Board[x,y]<100 THEN
			MovePiece(DrawRedPiece[MOD(Board[x,y],100)],CoverMaskRed[MOD(Board[x,y],100)],CoverRed[MOD(Board[x,y],100)],i+MyStartPlace-1,1)
			GR.HIDE CoverMaskRed[MOD(Board[x,y],100)]
		ELSE
			MovePiece(DrawBluePiece[MOD(Board[x,y],100)],CoverMaskBlue[MOD(Board[x,y],100)],CoverBlue[MOD(Board[x,y],100)],i+MyStartPlace-1,1)
			GR.HIDE CoverMaskBlue[MOD(Board[x,y],100)]
		ENDIF
		Board[i+MyStartPlace-1,1]=Board[x,y]:Board[x,y]=0
		GR.RENDER
		PAUSE 1000
	NEXT i
	NoWait=1:GOSUB ArrMyCode
	PAUSE 2000
	GR.HIDE MyText:GR.RENDER
RETURN

ArrMyCode:
	GR.MODIFY MyText,"text","Arranging my code..."
	GR.RENDER
	! Shift if needed
	IF MyNumOfPieces/2<>INT(MyNumOfPieces/2) THEN
		MyStartPlace=MyStartPlace-1
		FOR i=1 TO MyNumOfPieces
			Board[i+MyStartplace-1,1]=Board[i+MyStartplace,1]
		NEXT i
		Board[MyStartplace+MyNumOfPieces,1]=0
	ENDIF
	NwVal=MyPieceValue(Board[],MyStartPlace,MyNumOfPieces):NwColor=MyPieceColor(Board[],MyStartPlace,MyNumOfPieces)
	FOR i=1 TO MyNumOfPieces
		! Find lowest value
		Lowest=13
		FOR j=1 TO MyNumOfPieces
			IF MyPieceValue(Board[],MyStartPlace,j)<Lowest THEN
				Lowest=MyPieceValue(Board[],MyStartPlace,j)
				FndI=j
			ENDIF
		NEXT j
		! Same values present, then Red first
		FndN=0
		IF FndI<MyNumOfPieces & MyPieceColor(Board[],MyStartPlace,FndI)=2 THEN
			FOR j=FndI+1 TO MyNumOfPieces
				IF MyPieceValue(Board[],MyStartPlace,j)=Lowest THEN FndN=j
			NEXT j
		ENDIF
		IF FndN>FndI THEN FndI=FndN
		! Copy values to row 3
		Board[i+MyStartplace-1,3]=Board[FndI+MyStartplace-1,1]
		Board[FndI+MyStartplace-1,1]=113
	NEXT i
	FOR i=1 TO MyNumOfPieces
		!Set new values on correct place
		Board[i+MyStartPlace-1,1]=Board[i+MyStartPlace-1,3]:Board[i+MyStartPlace-1,3]=0
	NEXT i
	FOR i=1 TO MyNumOfPieces
		! Find insert place
		IF MyPieceValue(Board[],MyStartPlace,i)=NwVal & MyPieceColor(Board[],MyStartPlace,i)=NwColor THEN FndI=i
	NEXT i
	! Show piece to insert
	PtI=MyPieceValue(Board[],MyStartPlace,FndI)
	IF MyPieceColor(Board[],MyStartPlace,FndI)=1 THEN
		MovePiece(DrawRedPiece[PtI],CoverMaskRed[PtI],CoverRed[PtI],MyStartPlace+FndI-1,2)
	ELSE
		MovePiece(DrawBluePiece[PtI],CoverMaskBlue[PtI],CoverBlue[PtI],MyStartPlace+FndI-1,2)
	ENDIF
	IF !NoWait THEN
		GR.RENDER
		PAUSE 1000
	ELSE
		PAUSE 300
	ENDIF
	FOR i=1 TO FndI-1
		PtI=MyPieceValue(Board[],MyStartPlace,i)
		CtI=MyPieceColor(Board[],MyStartPlace,i)
		IF CtI=1 THEN
			MovePiece(DrawRedPiece[PtI],CoverMaskRed[PtI],CoverRed[PtI],i+MyStartPlace-1,1)
		ELSE
			MovePiece(DrawBluePiece[PtI],CoverMaskBlue[PtI],CoverBlue[PtI],i+MyStartPlace-1,1)
		ENDIF
	NEXT i
	IF !NoWait THEN
		GR.RENDER
		PAUSE 1000
	ELSE
		PAUSE 300
	ENDIF
	FOR i=FndI+1 TO MyNumOfPieces
		PtI=MyPieceValue(Board[],MyStartPlace,i)
		CtI=MyPieceColor(Board[],MyStartPlace,i)
		IF CtI=1 THEN
			MovePiece(DrawRedPiece[PtI],CoverMaskRed[PtI],CoverRed[PtI],i+MyStartPlace-1,1)
		ELSE
			MovePiece(DrawBluePiece[PtI],CoverMaskBlue[PtI],CoverBlue[PtI],i+MyStartPlace-1,1)
		ENDIF
	NEXT i
	IF !NoWait THEN
		GR.RENDER
		PAUSE 1000
	ELSE
		PAUSE 300
	ENDIF
	! Insert it
	PtI=MyPieceValue(Board[],MyStartPlace,FndI)
	IF MyPieceColor(Board[],MyStartPlace,FndI)=1 THEN
		MovePiece(DrawRedPiece[PtI],CoverMaskRed[PtI],CoverRed[PtI],MyStartPlace+FndI-1,1)
	ELSE
		MovePiece(DrawBluePiece[PtI],CoverMaskBlue[PtI],CoverBlue[PtI],MyStartPlace+FndI-1,1)
	ENDIF
	GR.RENDER
RETURN

MakePositions:
	ARRAY.SHUFFLE PickPos[]
	! Setup pieces for start
	FOR i=1 TO 12
		DrawRedPiece[i]=DrawPiece(RedPiece[i],DistributeX[PickPos[i]],DistributeY[PickPos[i]])
		CoverMaskRed[i]=DrawPiece(Mask,DistributeX[PickPos[i]],DistributeY[PickPos[i]])
		CoverRed[i]=DrawPiece(RedBack,DistributeX[PickPos[i]],DistributeY[PickPos[i]])
		DrawBluePiece[i]=DrawPiece(BluePiece[i],DistributeX[PickPos[i+12]],DistributeY[PickPos[i+12]])
		CoverMaskBlue[i]=DrawPiece(Mask,DistributeX[PickPos[i+12]],DistributeY[PickPos[i+12]])
		CoverBlue[i]=DrawPiece(BlueBack,DistributeX[PickPos[i+12]],DistributeY[PickPos[i+12]])
		Board[DistributeX[PickPos[i]],DistributeY[PickPos[i]]]=i
		Board[DistributeX[PickPos[i+12]],DistributeY[PickPos[i+12]]]=i+100
	NEXT i
	GR.SET.ANTIALIAS 1
	GR.BITMAP.DRAW MaskBoard,BoardMask,0,0
	GR.HIDE MaskBoard
	GR.BITMAP.DRAW LakeLady,Lady,289,260
	GR.HIDE LakeLady
	GR.CIRCLE Blink,360,317,3
	GR.HIDE BLink
	GR.RENDER
RETURN

DrawBoard:
	GR.SET.STROKE 3
	GR.COLOR 255,160,160,160,1
	GR.ROTATE.START 25,720,360
		FOR i=1 TO 22
			GR.TEXT.DRAW g,550,-200+i*40,"Code War Code War Code War Code War"
		NEXT i
	GR.ROTATE.END
	GR.COLOR 255,0,120,0,1
	GR.RECT g,0,0,719,719
	GR.COLOR 255,200,0,200,1
	GR.RECT g,0,0,719,4
	GR.RECT g,0,0,4,719
	GR.RECT g,715,0,719,719
	GR.RECT g,0,715,719,719
	GR.COLOR 255,0,0,0,1
	FOR i=5 TO 715 STEP 71
		GR.LINE g,i,5,i,715
		GR.LINE g,5,i,715,i
	NEXT i
	GR.COLOR 255,128,128,255,1
	GR.CIRCLE LakeL,5+3*71,359,70
	GR.CIRCLE LakeR,5+7*71,359,70
	GR.PAINT.GET LakePaint
	GR.COLOR 255,255,0,0,1
	GR.PAINT.GET RedPaint
	GR.COLOR 255,0,0,255,1
	GR.PAINT.GET BluePaint
	GR.COLOR 255,0,0,0,0
	GR.CIRCLE g,5+3*71,359,70
	GR.CIRCLE g,5+7*71,359,70
	GR.COLOR 100,255,255,255,0
	GR.TEXT.DRAW g,55+2*71,329,"~"
	GR.TEXT.DRAW g,25+2*71,359,"~~~"
	GR.TEXT.DRAW g,25+2*71,389,"~~~"
	GR.TEXT.DRAW g,55+2*71,419,"~"
	GR.TEXT.DRAW g,55+6*71,329,"~"
	GR.TEXT.DRAW g,25+6*71,359,"~~~"
	GR.TEXT.DRAW g,25+6*71,389,"~~~"
	GR.TEXT.DRAW g,55+6*71,419,"~"
	GR.COLOR 255,120,120,120,1
	GR.RECT g,815,20,1005,80
	GR.RECT g,820,15,1000,85
	GR.COLOR 255,255,255,0,0
	GR.RECT g,815,20,1005,80
	GR.RECT g,820,15,1000,85
	GR.TEXT.SETFONT Gothic
	GR.TEXT.SIZE 25
	GR.COLOR 255,50,255,50,1
	GR.TEXT.DRAW g,825,65,"Code War"
	GR.COLOR 255,255,255,0,0
	GR.TEXT.DRAW g,825,65,"Code War"
	GR.COLOR 150,255,255,255,1
	GR.RECT g,800,100,1020,540
	GR.COLOR 255,0,0,0,0
	GR.RECT g,800,100,1020,540
	GR.TEXT.UNDERLINE 1
	GR.TEXT.DRAW g,825,150,"Hi Scores"
	GR.TEXT.UNDERLINE 0
	GR.COLOR 100,255,255,255,1
	GR.RECT g0,740,550,1020,710
	GR.COLOR 255,0,0,200,0
	GR.RECT g1,740,550,1020,710
	GR.TEXT.DRAW g2,810,588,"Scores"
	GR.COLOR 255,0,0,0,1
	GR.TEXT.SIZE 16
	FOR i=1 TO 5
		GR.TEXT.ALIGN 2
		GR.TEXT.DRAW HiScore[i],930,160+i*70,Score$[i]
		GR.TEXT.ALIGN 1
		GR.TEXT.DRAW HiName[i],810,120+i*70,INT$(i)+"."+Name$[i]
	NEXT i
	GR.TEXT.SETFONT "Sans Serif"
	GR.TEXT.SIZE 20
	GR.COLOR 255,0,0,200,1
	GR.TEXT.DRAW g3,810,615,"(Pieces/Turn)"
	GR.TEXT.SIZE 40
	GR.TEXT.DRAW g4,780,658,"Me"
	GR.TEXT.DRAW g5,905,658,"You"
	GR.TEXT.DRAW MySkore,780,698,"0/0"
	GR.TEXT.DRAW YourSkore,910,698,"0/0"
	GR.LINE l1,740,625,1020,625
	GR.LINE l2,875,625,875,710
	GR.GROUP Skore,l1,l2,g0,g1,g2,g3,g4,g5,MySkore,YourSkore
	GR.HIDE Skore
	GR.BITMAP.DRAW DrawArrow,Arrow,8,8+8*71
	GR.HIDE DrawArrow
	GR.COLOR 255,120,120,120,1
	GR.RECT Kader1,731,96,795,544
	GR.HIDE Kader1
	GR.COLOR 255,255,255,0,0
	GR.RECT Kader,731,96,764,544
	GR.HIDE Kader
	GR.COLOR 255,0,255,255,1
	GR.RECT IKnow,735,100,760,100
	GR.COLOR 255,0,0,0,1
	GR.TEXT.SIZE 20
	GR.ROTATE.START 270,785,450
		GR.TEXT.DRAW KnowTxt,785,450,"my knowledge about your pieces"
	GR.ROTATE.END
	GR.HIDE KnowTxt
	GR.TEXT.SIZE 35
	GR.ROTATE.START 270,761,330
		GR.TEXT.DRAW Percent,761,330,"0 %"
	GR.ROTATE.END
	GR.HIDE Percent
RETURN

CreatePieces:
	! Create Board mask
	GR.BITMAP.CREATE BoardMask,720,720
	GR.BITMAP.DRAWINTO.START BoardMask
		GR.COLOR 255,0,0,0,0
		GR.RECT g,0,0,719,719
		GR.COLOR 255,0,0,0,0
		GR.SET.STROKE 1
		GR.CIRCLE g,5+3*71,359,70
		GR.CIRCLE g,5+7*71,359,70
		GR.COLOR 100,255,255,255,1
		GR.BITMAP.FILL BoardMask,5,5
	GR.BITMAP.DRAWINTO.END
	! Create arrow
	GR.BITMAP.CREATE Arrow,66,66
	GR.BITMAP.DRAWINTO.START Arrow
		GR.COLOR 255,0,0,0,1
		GR.LINE g,5,33,60,33
		GR.LINE g,5,33,32,66
		GR.LINE g,60,33,32,66
		GR.BITMAP.FILL Arrow,33,40
	GR.BITMAP.DRAWINTO.END
	! Create mask piece
	GR.BITMAP.CREATE Mask,66,66
	GR.BITMAP.DRAWINTO.START Mask
		GR.COLOR 255,255,255,255,0
		GR.CIRCLE g,33,33,27
		GR.COLOR 200,0,0,0,1
		GR.BITMAP.FILL Mask,1,1
	GR.BITMAP.DRAWINTO.END
	GR.TEXT.SIZE 50
	GR.TEXT.BOLD 1
	FOR i=0 TO 1
		READ.FROM 1
		GR.SET.STROKE 0
		FOR j=12 TO 1 STEP -1
			IF i=0 THEN
				GR.BITMAP.CREATE RedPiece[j],66,66
				GR.BITMAP.DRAWINTO.START RedPiece[j]
				GR.COLOR 255,255,255,255,1
				GR.RECT g,0,0,65,65
				GR.COLOR 255,255,0,0,1
				GR.RECT g,0,0,65,2
				GR.RECT g,0,0,9,65
				GR.RECT g,57,0,65,65
				GR.COLOR 255,255,0,0,0
			ELSE
				GR.BITMAP.CREATE BluePiece[j],66,66
				GR.BITMAP.DRAWINTO.START BluePiece[j]
				GR.COLOR 255,255,255,255,1
				GR.RECT g,0,0,65,65
				GR.COLOR 255,0,0,255,1
				GR.RECT g,0,0,65,2
				GR.RECT g,0,0,9,65
				GR.RECT g,57,0,65,65
				GR.COLOR 255,0,0,255,0
			ENDIF
Cont:
			READ.NEXT L
NxtPnt:
				READ.NEXT Nr
				IF Nr>0 THEN
					READ.NEXT C
					GR.LINE g,C+8,L+4,C+Nr+7,L+4
					GOTO NxtPnt
				ENDIF
				IF L<59 THEN
					GOTO Cont
				ENDIF
				GR.TEXT.ALIGN 2
				GR.COLOR 192,0,0,0,1
				GR.TEXT.DRAW g,30,50,INT$(j)
			GR.BITMAP.DRAWINTO.END
		NEXT j
		IF i=0 THEN
			GR.BITMAP.CREATE RedBack,66,66
			GR.BITMAP.DRAWINTO.START RedBack
				GR.COLOR 255,255,255,0,1
				GR.RECT g,0,0,65,65
				GR.COLOR 255,255,0,0,1
				GR.RECT g,5,5,60,60
				GR.COLOR 255,230,0,0,1
		ELSE
			GR.BITMAP.CREATE BlueBack,66,66
			GR.BITMAP.DRAWINTO.START BlueBack
				GR.COLOR 255,255,255,0,1
				GR.RECT g,0,0,65,65
				GR.COLOR 255,0,0,255,1
				GR.RECT g,5,5,60,60
				GR.COLOR 255,0,0,230,1
		ENDIF
		GR.TEXT.ALIGN 1
		GR.TEXT.DRAW g,15,50,"S"
		GR.BITMAP.DRAWINTO.END
	NEXT i
RETURN

READ.DATA 0,0,1,5,24,0,2,3,22,3,29,2,37,0,3,1,21,7,32,0,4,1,20,2,36,0,5,2,19,4,32,2,42,0
READ.DATA 6,1,19,10,33,0,7,2,5,2,13,2,18,2,37,2,40,2,44,0,8,3,4,2,13,1,18,1,33,3,38,3,42,0
READ.DATA 9,3,5,3,12,1,18,9,33,2,43,0,10,2,7,2,11,1,18,3,41,0,11,5,7,1,18,5,37,0,12,3,8,1,18,9,35,0
READ.DATA 13,5,7,1,18,2,20,1,24,1,27,3,40,0,14,3,6,3,10,1,18,2,20,1,24,1,27,1,31,1,34,3,37,0
READ.DATA 15,4,4,3,11,1,18,1,21,1,24,1,27,1,31,1,33,1,35,2,37,0,16,3,4,3,12,2,17,1,21,1,24,2,27,1,31,1,33,6,35,0
READ.DATA 17,2,5,2,12,3,16,1,21,2,24,2,27,11,31,0,18,1,16,1,18,1,21,2,24,2,28,9,32,0,19,2,15,1,19,2,21,2,24,2,28,4,32,3,37,0
READ.DATA 20,1,15,1,19,2,21,3,24,3,28,1,32,4,34,0,21,1,14,3,18,6,22,5,29,1,35,0,22,1,14,4,17,12,22,1,35,0
READ.DATA 23,2,9,1,13,16,17,1,35,0,24,5,7,1,13,17,16,1,36,0,25,6,7,18,16,1,36,0,26,3,8,1,12,19,15,1,36,0
READ.DATA 27,4,8,19,15,1,36,0,28,4,8,20,14,2,36,0,29,3,8,8,13,11,23,1,37,0,30,6,8,3,16,12,23,1,37,0
READ.DATA 31,4,8,1,15,1,18,1,21,13,23,1,37,0,32,4,8,1,15,10,23,3,35,0,33,4,8,3,15,9,23,1,37,0
READ.DATA 34,4,8,2,14,10,22,0,35,3,9,3,14,8,19,1,31,0,36,3,9,5,13,3,20,3,29,0,37,3,9,4,14,1,20,7,25,0
READ.DATA 38,3,9,4,15,1,20,9,23,0,39,3,9,5,15,10,22,0,40,3,9,4,16,9,22,0,41,3,9,4,16,10,21,0
READ.DATA 42,5,9,3,16,12,21,0,43,5,9,3,16,23,20,0,44,3,9,3,13,26,17,0,45,2,8,1,13,4,16,23,21,0
READ.DATA 46,2,7,1,13,5,16,22,22,0,47,3,6,1,13,5,15,21,24,0,48,1,6,2,8,2,13,4,16,20,25,0
READ.DATA 49,6,5,3,13,2,17,18,27,0,50,5,5,5,13,16,28,0,51,2,4,2,7,7,12,16,29,0,52,1,4,11,10,14,30,0
READ.DATA 53,3,4,15,8,13,31,0,54,3,6,14,10,8,36,0,55,16,9,1,38,3,41,0,56,3,10,12,15,0,57,5,17,5,23,0
READ.DATA 58,1,18,0,59,0,0,0,1,1,19,1,21,0,2,8,15,0,3,2,15,1,22,0,4,2,14,1,22,0,5,2,14,1,22,0
READ.DATA 6,2,14,1,22,0,7,2,14,1,22,0,8,2,14,6,17,1,34,1,41,0,9,8,15,3,33,3,40,0,10,8,15,2,34,3,40,0
READ.DATA 11,8,15,0,12,7,16,0,13,8,16,0,14,10,16,1,34,1,41,0,15,10,17,3,33,3,40,0,16,12,16,2,34,3,40,0
READ.DATA 17,7,15,4,24,0,18,2,15,1,18,4,20,3,25,0,19,1,15,8,17,3,26,0,20,15,14,0,21,12,14,2,27,0
READ.DATA 22,15,14,0,23,1,14,4,16,6,21,2,28,0,24,1,14,4,16,6,21,2,28,0,25,1,14,11,16,2,28,0
READ.DATA 26,17,14,0,27,6,14,9,22,0,28,18,14,0,29,19,14,0,30,6,14,11,23,0,31,7,13,12,22,0,32,7,13,1,22,1,24,3,26,0
READ.DATA 33,7,13,3,27,0,34,7,13,7,23,0,35,9,12,5,25,0,36,3,11,3,15,3,19,1,24,2,28,0,37,15,15,0
READ.DATA 38,9,15,2,27,0,39,1,15,7,17,3,26,0,40,2,15,7,22,0,41,7,15,5,24,0,42,8,15,5,24,0
READ.DATA 43,9,15,4,25,0,44,10,15,3,26,0,45,14,11,3,26,0,46,18,11,0,47,20,10,0,48,10,10,9,22,0
READ.DATA 49,10,10,7,25,0,50,13,9,7,26,0,51,17,9,5,28,0,52,23,9,0,53,22,9,0,54,20,10,0,55,19,10,0
READ.DATA 56,18,9,0,57,10,9,5,21,0,58,3,14,0,59,0,0,0,1,0,2,8,27,0,3,1,26,2,35,0,4,2,24,1,37,0
READ.DATA 5,1,24,1,38,0,6,1,23,1,26,1,39,0,7,1,23,1,26,1,40,0,8,1,22,2,25,1,40,0,9,1,22,1,25,1,29,1,41,0
READ.DATA 10,1,22,1,25,1,28,1,41,0,11,1,22,1,25,1,28,1,41,0,12,1,22,1,25,1,28,1,32,2,41,0
READ.DATA 13,2,21,1,25,1,28,1,35,1,42,0,14,5,18,2,25,1,28,1,32,1,42,0,15,8,16,2,25,1,28,1,32,1,35,1,42,0
READ.DATA 16,10,14,2,25,1,28,1,32,1,35,1,42,0,17,14,11,1,26,1,29,1,32,1,35,1,39,2,41,0,18,16,9,1,26,1,29,1,32,1,35,1,37,1,39,1,41,0
READ.DATA 19,20,8,1,29,1,32,1,35,1,37,1,39,1,41,0,20,20,8,1,29,1,32,1,35,2,37,1,41,0,21,2,8,17,11,1,29,1,32,1,35,2,37,1,40,0
READ.DATA 22,3,7,14,14,1,29,1,32,2,35,1,38,1,40,0,23,4,7,13,15,1,29,1,32,3,34,1,38,1,40,0
READ.DATA 24,3,8,6,16,1,27,1,29,2,31,1,34,2,36,1,39,0,25,4,8,5,16,5,25,2,31,6,34,0,26,5,8,4,17,1,22,15,24,0
READ.DATA 27,2,8,3,11,3,17,1,22,14,24,0,28,5,11,2,17,16,21,0,29,7,12,16,20,0,30,24,13,0,31,23,14,0
READ.DATA 32,21,16,0,33,20,17,0,34,20,17,0,35,12,16,9,29,0,36,9,16,2,26,9,29,0,37,9,16,12,26,0
READ.DATA 38,22,16,0,39,22,16,0,40,3,16,13,20,3,34,0,41,4,16,4,22,4,28,3,34,0,42,3,17,1,28,4,33,0
READ.DATA 43,3,17,1,28,3,33,0,44,4,17,6,24,3,33,0,45,3,18,6,24,3,32,0,46,3,11,7,18,4,30,0
READ.DATA 47,2,12,14,20,0,48,2,20,3,23,3,28,1,33,0,49,3,20,1,25,1,28,2,32,0,50,4,20,4,25,3,31,0
READ.DATA 51,1,20,12,22,0,52,1,10,1,15,3,20,4,25,2,32,0,53,3,9,3,14,5,19,4,25,5,30,0,54,2,9,2,15,6,19,9,26,0
READ.DATA 55,6,19,9,26,0,56,9,9,6,19,8,26,0,57,8,9,11,21,0,58,6,24,0,59,0,0,0,1,0,2,8,28,0
READ.DATA 3,5,27,4,33,0,4,9,20,3,36,0,5,13,16,3,37,0,6,15,14,2,38,0,7,16,13,3,37,0,8,17,11,2,37,0
READ.DATA 9,17,11,1,37,0,10,18,10,2,36,0,11,13,9,2,26,2,36,0,12,10,9,8,20,3,36,0,13,11,9,1,22,1,25,1,27,4,35,0
READ.DATA 14,11,9,1,22,3,25,5,34,0,15,12,8,2,22,3,25,6,33,0,16,11,9,1,23,3,25,7,33,0,17,11,9,1,23,3,25,8,32,0
READ.DATA 18,11,9,1,23,3,25,9,31,0,19,9,10,6,22,9,31,0,20,7,11,6,19,2,26,10,30,0,21,2,19,7,27,5,35,0
READ.DATA 22,2,18,2,23,7,26,5,34,0,23,3,17,2,22,13,26,0,24,4,16,2,22,3,26,8,31,0,25,4,15,2,21,4,25,8,31,0
READ.DATA 26,6,14,2,21,4,25,7,31,0,27,6,13,2,20,4,25,8,30,0,28,3,12,2,19,1,26,1,28,8,30,0
READ.DATA 29,3,11,2,20,1,25,1,28,7,30,0,30,12,10,10,27,0,31,29,8,0,32,29,7,0,33,29,7,0,34,4,7,8,13,13,22,0
READ.DATA 35,17,6,11,24,0,36,3,6,6,10,3,21,9,25,0,37,4,9,5,20,8,26,0,38,1,9,2,16,14,20,0
READ.DATA 39,4,8,2,13,14,20,0,40,4,9,17,18,0,41,2,8,18,18,0,42,3,8,1,16,3,19,2,23,10,26,0
READ.DATA 43,9,8,9,21,0,44,2,9,4,12,9,20,0,45,1,9,2,13,12,17,0,46,1,9,6,16,2,27,0,47,1,9,4,13,4,25,0
READ.DATA 48,2,8,1,12,9,19,0,49,4,9,12,16,1,35,0,50,2,10,14,14,3,34,0,51,2,10,14,14,2,34,0
READ.DATA 52,2,10,14,14,0,53,2,10,10,14,0,54,2,10,6,14,0,55,2,10,4,13,10,30,0,56,4,11,0,57,2,11,0
READ.DATA 58,0,59,0,0,0,1,2,10,0,2,4,9,0,3,6,8,0,4,6,8,9,20,0,5,6,8,9,15,2,28,0
READ.DATA 6,7,9,1,29,0,7,1,11,1,29,0,8,1,11,12,19,0,9,2,11,17,14,0,10,19,12,0,11,19,12,0
READ.DATA 12,20,12,0,13,20,12,0,14,20,12,0,15,20,12,0,16,20,12,0,17,1,12,19,14,0,18,1,12,19,14,0
READ.DATA 19,2,11,19,14,0,20,21,12,0,21,21,13,0,22,21,13,0,23,21,13,0,24,21,13,0,25,21,13,0,26,21,13,0
READ.DATA 27,21,13,0,28,1,13,15,19,0,29,28,7,0,30,27,8,0,31,22,13,0,32,8,12,13,22,0,33,5,13,14,21,0
READ.DATA 34,4,13,14,21,0,35,5,13,14,20,0,36,1,12,14,20,0,37,1,11,13,20,0,38,1,10,5,19,6,26,0
READ.DATA 39,4,10,4,19,5,27,0,40,2,11,2,19,2,26,2,29,0,41,4,11,3,18,1,24,1,26,2,29,0,42,4,12,2,18,1,30,0
READ.DATA 43,1,12,3,17,7,21,2,30,0,44,1,13,15,17,3,37,0,45,1,13,12,16,3,29,2,37,0,46,1,12,8,16,5,27,1,37,0
READ.DATA 47,7,13,7,24,0,48,3,15,3,21,5,26,0,49,6,15,8,23,0,50,1,15,1,17,4,20,3,28,1,35,1,41,0
READ.DATA 51,6,15,5,26,3,34,3,40,0,52,4,15,8,23,2,35,2,40,0,53,3,15,3,20,6,25,0,54,5,15,5,22,0
READ.DATA 55,3,15,5,19,0,56,6,15,0,57,3,15,0,58,0,59,0,0,0,1,0,2,4,20,0,3,8,18,0,4,8,17,0
READ.DATA 5,6,18,0,6,3,20,0,7,2,21,0,8,2,21,0,9,2,21,0,10,3,21,0,11,5,20,0,12,5,20,0
READ.DATA 13,6,20,0,14,13,19,1,33,0,15,6,16,4,23,2,33,0,16,3,13,4,18,8,23,2,33,0,17,2,12,17,17,0
READ.DATA 18,2,11,8,15,10,24,0,19,12,11,10,24,0,20,11,10,3,22,6,26,0,21,1,10,8,13,3,22,5,26,1,32,0
READ.DATA 22,1,10,1,13,6,15,8,22,0,23,2,12,13,16,2,31,0,24,4,12,9,18,3,30,0,25,5,12,1,22,4,29,0
READ.DATA 26,3,11,5,15,7,26,0,27,2,12,17,15,0,28,2,12,16,16,0,29,2,12,17,16,0,30,2,12,18,15,0
READ.DATA 31,2,12,18,15,0,32,2,12,3,15,9,19,3,29,0,33,2,12,2,16,1,20,1,24,2,30,0,34,3,12,2,16,1,24,2,30,0
READ.DATA 35,3,12,3,16,4,22,2,29,0,36,3,12,2,17,2,23,1,29,0,37,3,12,1,18,4,22,3,28,0,38,2,13,3,18,1,24,4,27,0
READ.DATA 39,3,13,4,18,1,24,4,26,0,40,3,13,13,18,0,41,3,13,13,18,0,42,3,13,13,18,0,43,2,14,13,18,0
READ.DATA 44,2,14,16,18,0,45,24,14,6,40,0,46,5,4,35,11,0,47,16,4,2,29,14,32,0,48,13,4,2,18,2,29,12,34,0
READ.DATA 49,11,4,11,35,0,50,10,3,1,14,12,35,0,51,12,3,11,36,0,52,11,3,8,36,0,53,10,4,8,36,0
READ.DATA 54,8,6,8,36,0,55,9,6,8,36,0,56,3,12,2,36,0,57,0,58,0,59,0,0,0,1,0,2,0,3,0
READ.DATA 4,3,33,0,5,4,13,5,31,0,6,7,12,7,30,0,7,9,12,10,28,0,8,23,12,1,37,0,9,21,12,1,37,0
READ.DATA 10,8,11,10,20,1,37,0,11,8,10,2,37,0,12,8,10,4,35,0,13,8,9,8,31,0,14,7,9,19,20,0
READ.DATA 15,7,8,21,19,0,16,6,8,22,18,0,17,5,9,23,17,0,18,4,10,24,16,0,19,26,14,0,20,26,14,0
READ.DATA 21,27,14,0,22,27,14,0,23,27,14,0,24,27,14,0,25,12,13,13,28,0,26,7,13,14,27,0,27,3,14,14,27,0
READ.DATA 28,2,14,15,26,0,29,6,13,16,25,0,30,6,14,16,25,0,31,1,14,2,16,2,19,15,25,0,32,9,13,7,25,5,34,0
READ.DATA 33,1,12,1,14,1,16,6,24,1,31,3,33,2,37,0,34,1,11,1,17,7,24,6,32,0,35,1,11,1,13,1,16,2,18,2,23,3,26,8,30,0
READ.DATA 36,6,11,1,18,5,23,6,31,0,37,1,11,1,13,2,18,6,22,8,29,0,38,2,9,1,19,10,22,4,33,0
READ.DATA 39,2,9,1,12,2,14,1,19,1,24,3,28,1,35,0,40,3,8,1,12,2,14,1,19,1,22,6,25,1,35,0
READ.DATA 41,3,8,4,12,2,18,15,22,0,42,7,9,2,18,15,22,0,43,6,10,19,18,0,44,25,12,0,45,1,12,12,17,7,30,0
READ.DATA 46,1,12,10,15,11,26,0,47,9,12,1,26,9,28,0,48,4,15,14,23,0,49,2,15,1,21,11,26,0,50,1,15,2,18,13,24,0
READ.DATA 51,1,15,16,21,0,52,1,15,12,19,0,53,1,15,11,17,0,54,10,15,0,55,8,15,0,56,6,15,0,57,4,15,0
READ.DATA 58,1,15,0,59,0,0,2,30,0,1,12,25,0,2,6,23,9,30,0,3,5,21,8,33,0,4,6,19,8,34,0
READ.DATA 5,7,18,8,35,0,6,8,17,10,34,0,7,10,16,11,34,0,8,13,15,14,32,0,9,33,14,0,10,33,14,0
READ.DATA 11,35,13,0,12,36,12,0,13,36,12,0,14,37,11,0,15,38,11,0,16,39,10,0,17,40,9,0,18,41,8,0
READ.DATA 19,7,8,32,16,0,20,41,7,0,21,43,5,0,22,25,3,19,29,0,23,32,2,12,35,0,24,34,0,12,35,0
READ.DATA 25,40,0,6,41,0,26,36,11,0,27,5,11,28,18,0,28,4,10,26,19,0,29,2,9,1,12,25,20,0,30,2,8,25,20,0
READ.DATA 31,2,8,2,13,24,21,0,32,6,9,23,22,0,33,5,9,24,22,0,34,4,9,2,15,1,18,18,22,1,44,0
READ.DATA 35,5,9,4,15,18,21,0,36,8,9,2,18,4,21,13,26,0,37,6,10,5,19,2,27,2,30,5,33,0,38,5,11,4,17,1,23,1,26,11,28,0
READ.DATA 39,2,10,2,18,1,22,8,25,0,40,2,11,3,18,1,22,5,24,3,35,0,41,2,11,8,18,9,30,0,42,1,11,6,18,11,28,0
READ.DATA 43,2,10,6,17,14,25,0,44,2,10,4,17,15,24,0,45,8,11,17,22,0,46,6,11,19,20,0,47,2,14,21,18,0
READ.DATA 48,1,14,22,17,0,49,1,14,23,16,0,50,2,13,23,16,0,51,2,13,14,17,0,52,14,14,0,53,1,14,9,16,0
READ.DATA 54,1,14,7,16,0,55,1,14,4,17,0,56,1,14,2,17,0,57,1,14,1,17,0,58,1,14,0,59,0,0,0
READ.DATA 1,2,21,0,2,4,20,0,3,5,19,0,4,5,19,0,5,8,18,1,28,0,6,10,17,4,28,0,7,11,16,6,28,0
READ.DATA 8,11,16,7,28,0,9,12,15,9,28,0,10,12,15,10,28,0,11,23,15,0,12,12,15,11,28,0,13,12,15,11,28,0
READ.DATA 14,13,14,11,28,0,15,13,14,11,28,0,16,13,14,11,28,0,17,2,14,7,17,14,25,0,18,5,15,18,21,0
READ.DATA 19,14,14,9,29,0,20,24,14,0,21,16,14,7,31,0,22,24,14,0,23,24,14,0,24,24,14,0,25,4,15,17,21,0
READ.DATA 26,2,16,13,25,0,27,3,17,12,26,0,28,4,17,12,25,0,29,6,16,13,25,0,30,4,17,1,22,14,24,0
READ.DATA 31,1,17,16,22,0,32,1,17,11,22,1,35,1,37,0,33,14,18,1,35,2,37,0,34,14,18,4,35,0,35,1,19,10,21,4,35,0
READ.DATA 36,1,19,6,21,2,29,4,35,0,37,1,19,5,22,1,30,3,35,0,38,1,19,4,22,1,30,3,35,0,39,3,18,4,22,2,29,4,34,0
READ.DATA 40,25,8,3,34,0,41,6,8,27,15,0,42,2,8,20,21,0,43,9,7,1,19,2,22,10,31,0,44,8,6,2,22,6,34,0
READ.DATA 45,8,7,2,19,2,22,7,34,0,46,6,8,1,19,2,22,8,34,0,47,9,7,1,22,8,34,0,48,9,7,1,22,7,34,0
READ.DATA 49,16,7,6,34,0,50,18,6,8,33,0,51,13,13,8,33,0,52,16,11,9,32,0,53,15,11,2,27,6,32,0
READ.DATA 54,11,15,7,29,0,55,9,17,5,29,0,56,17,17,0,57,10,20,0,58,3,23,0,59,0,0,7,21,0,1,18,16,0
READ.DATA 2,25,13,0,3,29,11,0,4,29,11,0,5,29,11,0,6,29,11,0,7,27,12,0,8,27,12,0,9,26,12,0
READ.DATA 10,25,13,0,11,25,13,0,12,24,13,0,13,23,14,0,14,23,14,0,15,23,14,0,16,21,15,0,17,4,15,16,20,0
READ.DATA 18,21,15,0,19,22,14,0,20,24,12,0,21,24,12,0,22,26,11,0,23,27,11,0,24,28,11,0,25,15,12,11,28,0
READ.DATA 26,24,13,0,27,12,15,9,28,0,28,10,15,3,28,5,32,0,29,3,15,5,19,4,27,4,32,0,30,1,15,1,17,3,27,5,31,0
READ.DATA 31,3,15,1,20,1,23,3,28,5,32,0,32,2,14,4,17,1,22,10,28,0,33,3,13,1,19,1,23,11,27,0
READ.DATA 34,6,12,4,20,11,27,0,35,10,12,2,24,11,27,0,36,4,12,2,18,2,21,1,24,13,26,0,37,5,11,1,19,1,24,12,27,0
READ.DATA 38,5,11,1,24,14,26,0,39,6,11,1,19,1,23,14,26,0,40,30,10,0,41,32,9,0,42,34,9,0,43,33,12,0
READ.DATA 44,35,10,0,45,35,8,0,46,35,6,0,47,35,4,0,48,34,3,0,49,34,2,0,50,31,3,0,51,27,5,0
READ.DATA 52,24,7,0,53,20,9,0,54,17,11,0,55,13,13,0,56,10,15,0,57,7,17,0,58,4,18,0,59,0,0,0
READ.DATA 1,1,33,0,2,2,33,0,3,4,31,0,4,7,28,0,5,6,28,0,6,4,28,0,7,3,28,0,8,3,28,0
READ.DATA 9,6,24,0,10,5,23,0,11,2,23,0,12,10,19,0,13,13,18,0,14,3,18,8,23,0,15,11,19,0,16,9,20,0
READ.DATA 17,7,21,0,18,10,19,0,19,16,16,0,20,20,14,0,21,22,13,0,22,26,11,0,23,4,10,21,17,0,24,4,9,21,18,0
READ.DATA 25,4,8,22,18,0,26,3,8,24,17,0,27,4,7,24,17,0,28,4,6,25,17,0,29,4,6,26,16,0,30,5,5,28,15,0
READ.DATA 31,38,5,0,32,39,5,0,33,40,4,0,34,40,4,0,35,40,4,0,36,40,4,0,37,41,4,0,38,41,4,0
READ.DATA 39,41,4,0,40,40,4,0,41,40,4,0,42,40,4,0,43,40,4,0,44,40,4,0,45,38,5,0,46,38,5,0
READ.DATA 47,36,6,0,48,36,6,0,49,34,7,0,50,33,7,0,51,32,8,0,52,30,9,0,53,28,10,0,54,25,11,0
READ.DATA 55,22,13,0,56,18,15,0,57,14,17,0,58,5,22,0,59,0,0,0,1,1,7,0,2,1,7,0,3,2,7,0
READ.DATA 4,2,7,0,5,3,7,0,6,4,7,0,7,3,7,1,11,3,35,0,8,6,7,8,22,7,31,0,9,10,7,21,18,0
READ.DATA 10,28,7,2,37,0,11,4,7,21,13,4,36,0,12,4,8,20,14,5,35,0,13,6,7,19,15,5,35,0,14,7,7,17,16,6,34,0
READ.DATA 15,9,7,23,18,0,16,11,7,21,20,0,17,14,7,19,22,0,18,33,8,0,19,33,8,0,20,17,9,12,29,0
READ.DATA 21,14,11,12,30,0,22,13,12,14,28,0,23,15,12,15,28,0,24,15,12,4,29,9,34,0,25,15,12,6,29,7,36,0
READ.DATA 26,15,12,6,29,7,37,0,27,10,10,16,21,6,38,0,28,9,10,18,20,4,40,0,29,8,10,19,20,2,42,0
READ.DATA 30,2,11,3,14,21,19,2,43,0,31,3,14,27,18,0,32,2,14,27,18,0,33,30,14,0,34,13,14,0,35,1,15,2,21,0
READ.DATA 36,1,15,0,37,2,15,0,38,2,15,0,39,1,16,0,40,1,16,0,41,2,16,0,42,2,16,0,43,2,16,0
READ.DATA 44,1,17,0,45,1,17,0,46,2,17,0,47,2,17,0,48,2,17,0,49,1,18,0,50,2,18,0,51,2,18,0
READ.DATA 52,2,18,0,53,1,19,0,54,1,19,0,55,2,19,0,56,2,19,0,57,1,20,0,58,0,59,0