Rem Guess my Number
Rem With RFO Basic!
Rem Using Nicolas's gw Library
Rem January 2015
Rem Version 1.00
Rem By Roy

include gw.bas
gw_force_mode(1)

gosub Initialise
gosub SetUp

gw_load_theme ("classic")
page=gw_new_page()
gw_inject_html(page,"<h2>")
	gw_add_titlebar(page,"Guess My Number")
gw_inject_html(page,"</h2>")
Rem Get the range the user wants to guess
gw_inject_html(page,"<h3><font color='Yellow'>")
	numSelect = gw_add_text(page,"Please Select the Number Range you want to Guess.")

	gw_open_group(page)
	ctl_parent=gw_add_radio(page,0,"1 to 100")
	ctl_child1=gw_add_radio(page,ctl_parent,"1 to 200")
	ctl_child2=gw_add_radio(page,ctl_parent,"1 to 300")
	ctl_child3=gw_add_radio(page,ctl_parent,"1 to 400")
	gw_use_theme_custo_once("inline")
	gw_add_submit(page,"Enter")
	gw_close_group(page)

Rem User uses the sliders to guess the number
gw_start_center(page)
	txtNumRange = gw_add_text(page, "Use the Sliders to Guess the Number.")
gw_inject_html(page,"<font color='Green'>")

ctl_slider1 = gw_add_slider(page, "Steps of Ten:", 0, maxNum, 10, maxNum/2)
ctl_slider2 = gw_add_slider(page, "Step of One:", 0, 9, 1, 5)

gw_use_theme_custo_once("inline")
gw_add_submit(page, "Try Guess")

txtGuessesLeft=gw_add_text(page,"You have Ten Guesses Remaining")
gw_stop_center(page)

gw_inject_html(page,"<h2></font>")
txtResults = gw_add_textbox(page, "")
gw_inject_html(page,"</h2>")

gosub DoBottomButtons
gw_render(page)
gw_unload_theme()

do

	do
		r$ = gw_wait_action$()
		if sound then Soundpool.play s,click,0.99,0.99,1,0,1
		if is_in("Enter", r$)>1 &  numToGuess<>0 then 
			if sound then Soundpool.play s,wrong,0.99,0.99,1,0,1
			gw_show_dialog_message(rangeSelected)
		endif
		if r$="ExitGuess" then exit % close game
		if r$="NewGame" then gosub NewGame % Reset and start a new game
		if r$="SOUNDBUT" then gosub soundOnOff
		if r$<>"NO" & r$<>"SOUNDBUT" & r$<>"NewGame" then
			if numToGuess=0 then gosub GetNumberToGuess 
			if is_in("Try", r$)>1 & numToGuess <>0 then gosub GuessNumber
			if is_in("Try", r$)>1 & numToGuess = 0 then 
				if sound then Soundpool.play s,wrong,0.99,0.99,1,0,1
				gw_show_dialog_message(noRange)
			endif
		endif
	until userGuess=numToGuess | guesses = 10
	gosub GameOver

until 0

end

Initialise:
	array.load guessNum$[],"Ten","Nine","Eight","Seven","Six","Five","Four","Three","Two","One"
	about$="<div align=center><font color='turquoise'>Guess My Number<p/>With RFO Basic!<p/>Using Nicolas's GW Library<p/>January 2015<p/>Version 1.00<p/>Author: Roy Shepherd<p/></div>"
	
	help$="<font color='Red'>"
	help$=help$+"Guess My Number.<br><br>"
	
	help$=help$+"<font color='Yellow'>"
	
	help$=help$+"To play this game first select the number range you want to guess, be tapping one of<br/>"
	help$=help$+"the radio buttons, followed by Enter. Your device will then pick a number, at random,<br/>"
	help$=help$+"in the range you just selected.<br/><hr/>"
	
	help$=help$+"<font color='Green'>"

	help$=help$+"To guess the number, use the slider bars. The top slider gos in steps of ten whist the<br/>"
	help$=help$+"bottom slider gos in steps of one. When you tap Try Guess the value of the two sliders<br/>"
	help$=help$+"are added together, which enables you to enter any number in the range you selected.<br/><hr/></p>"
	
	help$=help$+"<font color='Blue'>"

	help$=help$+"You have ten guesses and after each guess you device will tell you if your guess is to<br/>"
	help$=help$+"high or to low or you guessed right. If you haven't guessed the number after ten<br/>"
	help$=help$+"guesses, then your device will tell you the number it had picked.<br><hr/>"
	
	help$=help$+"<font color='turquoise'>"
	
	help$=help$+"Win or loss, you will have the choice to play again or exit.<br/><hr/>"
	
	help$=help$+"</font>"
	
	guess$=""
	guessGame=1
	sound=1
	Rem load sounds
	Soundpool.open 4
	Soundpool.load win,"win.wav"
	Soundpool.load lose,"lose.wav"
	Soundpool.load wrong,"wrong.wav"
	Soundpool.load click,"buttonClick.wav"
return

SetUp:
	numToGuess=0 
	guesses=0
	userGuess=-1
	maxNum=100
	gameInPlay=0
return

GetNumberToGuess:

	if gw_radio_selected(ctl_parent) then
		popup "Easy",0,0,0
		numToGuess=int(100 * rnd() + 1)
		gw_modify(txtNumRange,"text","Use the Sliders to Guess the Number Between 1 & 100") % +int$(numToGuess))
		maxNum=100
		gw_modify(ctl_slider1, "max", int$(maxNum))
		gw_modify(ctl_slider1, "val", int$(maxNum/2))
		gameInPlay=1
	endif
		
	if gw_radio_selected(ctl_child1) then 
		popup "Not to Difficult",0,0,0
		numToGuess=int(200 * rnd() + 1) 
		gw_modify(txtNumRange,"text","Use the Sliders to Guess the Number Between 1 & 200") % +int$(numToGuess))
		maxNum=200
		gw_modify(ctl_slider1, "max", int$(maxNum))
		gw_modify(ctl_slider1, "val", int$(maxNum/2))
		gameInPlay=1
	endif
	
	if gw_radio_selected(ctl_child2) then
		popup "Difficlt",0,0,0
		numToGuess=int(300 * rnd() + 1) 
		gw_modify(txtNumRange,"text","Use the Sliders to Guess the Number Between 1 & 300") % +int$(numToGuess))
		maxNum=300
		gw_modify(ctl_slider1, "max", int$(maxNum))
		gw_modify(ctl_slider1, "val", int$(maxNum/2))
		gameInPlay=1
	endif
	
	if gw_radio_selected(ctl_child3) then
		popup "Very difficlt - good Luck",0,0,0
		numToGuess=int(400 * rnd() + 1) 
		gw_modify(txtNumRange,"text","Use the Sliders to Guess the Number Between 1 & 400") % +int$(numToGuess))
		maxNum=400
		gw_modify(ctl_slider1, "max", int$(maxNum))
		gw_modify(ctl_slider1, "val", int$(maxNum/2))
		gameInPlay=1
	endif
	if !gameInPlay then 
		if sound then Soundpool.play s,wrong,0.99,0.99,1,0,1
		gw_show_dialog_message(noRange)
	endif
	if gameInPlay then gw_modify(numSelect,"text","Number Range Selected.")
return

! Use the sliders to guess the number
GuessNumber:
	if guesses <10 then guesses++
	print guesses
	userGuess=GW_GET_VALUE(ctl_slider1)+GW_GET_VALUE(ctl_slider2)
	temp$="Guess "+guessNum$[11-guesses]+" "+int$(userGuess)
	if userGuess<numToGuess then temp$+=" Is to Low"
	if userGuess>numToGuess then temp$+=" Is to High"
	if userGuess=numToGuess then temp$+=" Is the Right Number - Well Done!"
	
	if guesses=10 & userGuess<>numToGuess then temp$+=" - Number is "+int$(numToGuess)
		
	if guesses<10 then g$= "You have " + guessNum$[guesses+1] + " Guesses Remaining" 
	
	if guesses = 10 then g$=" You have no Guess Remaining"
	
	gw_modify(txtGuessesLeft,"text",g$)
	temp$+=chr$(10)
	guess$=temp$+guess$
	gw_modify(txtResults,"text","<p style='color:Yellow'>"+ guess$)
	
	if userGuess=numToGuess then 
		temp$="________ Game "+int$(guessGame)+" Win________\n"
		guess$=temp$+guess$
		gw_modify(txtResults,"text","<p style='color:Yellow'>" + guess$)
		guessGame++
		if sound then Soundpool.play s,win,0.99,0.99,1,0,1
	endif
	if guesses=10 & userGuess<>numToGuess then 
		temp$="________ Game "+int$(guessGame)+" Lose________\n"
		guess$=temp$+guess$
		gw_modify(txtResults,"text","<p style='color:Yellow'>" + guess$)
		guessGame++
		if sound then Soundpool.play s,lose,0.99,0.99,1,0,1
	endif
return

DoBottomButtons:
	gw_start_center(page)
	Rem Add a row of button at the botton of the screen
	Rem Exit Guess My Number dialogbox and button
	array.load exitGuess$[], "Yes>ExitGuess", "No>NO"
	leaveGuess = gw_add_dialog_message(page, "<font color='yellow'>Exit", "<font color='red'>Leave Guess My number", exitGuess$[])
	array.delete exitGuess$[]
	gw_use_theme_custo_once("inline icon=plus")
	gw_add_button(page, "Exit", gw_show_dialog_message$(leaveGuess))

	Rem Sound on/off button
	gw_use_theme_custo_once("inline icon=info")
	souBut = gw_add_button(page,"Sound On","SOUNDBUT")
	
	Rem About DialogBox and button
	array.load guessAbout$[], "OK>NO" % I've set r$=GW_WAIT_ACTION$() to ignore "NO"
	guessAbout = gw_add_dialog_message(page, "<font color='yellow'>About", about$, guessAbout$[])
	array.delete guessAbout$[]
	gw_use_theme_custo_once("inline icon=info")
	gw_add_button(page, "About", gw_show_dialog_message$(guessAbout))
	
	Rem Help DialogBox and button
	array.load guessHelp$[], "OK>NO"
	guessHelp = gw_add_dialog_message(page, "<font color='yellow'>Help", help$, guessHelp$[])
	array.delete guessHelp$[]
	gw_use_theme_custo_once("inline icon=info")
	gw_add_button(page, "Help", gw_show_dialog_message$(guessHelp))
	gw_stop_center(page)
	
	Rem DialogBox but not using buttons
	Rem Game over show results
	array.load ban$[], "Yes>NewGame","No>ExitGuess"
	guessRight = gw_add_dialog_message(page, "<font color='yellow'>Results", "<font color='white'><div align=center>Well Done! You Guessed My Number.<br/>Another Game.</div>", ban$[])
	array.delete ban$[]
	
	array.load ban$[], "Yes>NewGame","No>ExitGuess"
	guessWrong = gw_add_dialog_message(page, "<font color='yellow'>Results", "<font color='white'><div align=center>You Didn't Guess My Number This Time.<br/>Another Game.</div>" , ban$[])
	array.delete ban$[]

	Rem Number Range not selected
	array.load ban$[], "OK>NO"
	rangeSelected = gw_add_dialog_message(page, "<font color='yellow'>Range Selected", "<font color='red'>Number Range Selected" , ban$[])
	array.delete ban$[]
	
	Rem Number Range selected
	array.load ban$[], "OK>NO"
	noRange = gw_add_dialog_message(page, "<font color='Yellow'>No Range", "<font color='red'>Please Select the Number Range you want to Guess." , ban$[])
	array.delete ban$[]
	gw_inject_html(page,"</font>")
return

GameOver:
	if userGuess=numToGuess  then 
		gw_show_dialog_message(guessRight)
	else
		gw_show_dialog_message(guessWrong)
	endif
return

NewGame:
	gosub SetUp
	gw_modify(ctl_parent, "selected", "0")
	gw_modify(numSelect,"text","Please Select the Number Range you want to Guess.")
return

SoundOnOff:
	if sound = 1 then 
		sound =0 
		gw_modify(souBut,"text","Sound Off")
	else 
		sound =1 
		gw_modify(souBut,"text","Sound On")
	endif
return

