Rem Hopper
Rem For Android
Rem With RFO Basic!
Rem August 2016
Rem Version 1.02
Rem By Roy Shepherd

Rem Hard pad addition
Rem by Nicolas Mougin

di_height = 672 % set to my Device
di_width = 1152

gr.open 255,128,128,255
gr.orientation 0 % landscape 
pause 1000
WakeLock 3
 
gr.screen screenWidth, screenHeight
scale_x = screenWidth / di_width
scale_y = screenHeight / di_height
gr.scale scale_x, scale_y

!-------------------------------------------------
!Let Basic see the functions before they are called
!-------------------------------------------------
gosub Functions

gosub Initialise
gosub SetUp
gosub PlayHopper

end

onBackKey:
    dialog.message "Exit Hopper",,yn,"Yes","No"
    if yn = 1 then gosub SaveData : exit
back.resume 

!------------------------------------------------
! Do once at first run of game
!------------------------------------------------
Initialise:
    !Set some variables 
    FALSE = 0 : TRUE = 1
    path$ = "Hopper/data/"
    sound = TRUE : buzz = TRUE : music = TRUE
    highScore = 0 : soundFlag = 0 : allowedTime = 300 % seconds 
    frogStep = 57 : level = 0
    sb = di_height - 45  : bb = di_height - 8
    
    gosub LoadData 
    gosub LoadSounds
    gosub DrawScenery
    gosub LoadImages
    gosub DrawTopButtons
    gosub DrawControler
    gosub DrawTimer
    !Using gr_collision for when a button is tapped
    gr.color 0,0,0,0
    gr.point collision, - 1, - 1 % Control the frog
    gr.point butCollision, - 1, - 1 % Top buttons
    
    gr.render
return

!------------------------------------------------
! Do at start of each new game
!------------------------------------------------
SetUp:
    score = 0 : frogLane = 0 : level = 0
    carSpeed = 3 : logSpeed = 8
    centreLane = FALSE : frogsHome = 0
    noTimeLeft = FALSE : gameOver = FALSE
    frogsDead = 0 : allowedDead = 5
    virtualPad = FALSE : hardPad = FALSE 
    tick = time()
    for x = 1 to 5 
        gr.hide frogHomeSafe[x]
        frogCount[x] = FALSE
        if x < 5 then gr.show frogLives[x]
    next
    gr.modify frogRun, "x", frogX, "y", frogY
    gr.modify hopTime, "left", (di_width / 2) - 150, "top", sb+10, "right", ((di_width / 2) + 150), "bottom", bb
    gr.modify txtScore, "text", "Score: " + int$(score)
    gr.render
return


!------------------------------------------------
! Load Sound files and setup buzz
!------------------------------------------------
LoadSounds:
    soundpool.open 7
    soundpool.load soundClick,path$ + "buttonClick.wav"
    if ! soundClick then call SoundLoadError("soundClick")
    soundpool.load soundNewHigh, path$ + "newHigh.wav"
    if ! soundNewHigh then call SoundLoadError("soundNewHigh")
    soundpool.load soundNotHigh, path$ + "notHigh.wav"
    if ! soundNotHigh then call SoundLoadError("soundNotHigh")
    soundpool.load soundFrog, path$ + "frog1.mp3"
    if ! soundFrog then call SoundLoadError("soundFrog")
    soundpool.load soundRunOver, path$ + "runOver.wav"
    if ! soundRunOver then call SoundLoadError("soundRunOver")
    soundpool.load soundAllHome, path$ + "allHome.wav"
    if ! soundAllHome then call SoundLoadError("allHome") 
    soundpool.load soundFrogHome, path$ + "frogHome.wav"
    if ! soundFrogHome then call SoundLoadError("frogHome")
    
    audio.load babyJane, Path$ + "babyJane.mp3" 
    if ! babyJane then call SoundLoadError("babyJane")
    
    array.load buzzGame[], 1, 100
    
    gosub StartMusic
return


!------------------------------------------------
! Load Images files. Cars, logs, turtles, frogs
! Draw them and add list pointers for the groups
!------------------------------------------------
LoadImages:
    ! Load the cars
    dim car1Lane1[4],car2Lane1[4] % Cars go left on lane one bottom lane
    dim car1Lane2[4],car2Lane2[4] % Cars go right on lane two
    dim car1Lane3[4],car2Lane3[4] % Cars go left on lane three
    dim car1Lane4[4],car2Lane4[4] % Cars go right on lane four top lane
    
    dim frogLives[4]
   
    for x = 1 to 4
        gr.bitmap.load BleftCar, path$ + "carLeft0" + int$(x) + ".png"
        if BleftCar  = - 1 then call ImageLoadError("BleftCar")
        gr.bitmap.scale car1Lane1[x], BleftCar, 90, 50
        gr.bitmap.scale car2Lane1[x], BleftCar, 90, 50
        
        gr.bitmap.scale car1Lane3[x], BleftCar, 90, 50
        gr.bitmap.scale car2Lane3[x], BleftCar, 90, 50
    
        
        gr.bitmap.load rightCar, path$ + "carRight0" + int$(x) + ".png"
        if rightCar  = - 1 then call ImageLoadError("carRight")
        gr.bitmap.scale car1Lane2[x], rightCar, 90, 50
        gr.bitmap.scale car2Lane2[x], rightCar, 90, 50
        
        gr.bitmap.scale car1Lane4[x], rightCar, 90, 50
        gr.bitmap.scale car2Lane4[x], rightCar, 90, 50
        
    next
    
    !array.shuffle Bottom1LeftCars[] : array.shuffle Bottom2LeftCars[]
    !array.shuffle Middle1Cars[] : array.shuffle Middle2Cars[]
    
    !array.load goLeftPos[],50, 350,700,1050, 1400, 1750, 2100, 2450
    array.load goLeftPos[],50, 550, 1050, 1550, 2050, 2550, 3050, 3550
    array.load goRightPos[], 1100, 750, 400, 50, - 300, - 650, - 1000, - 1350
    
    list.create n, lane1Car1 : list.create n, lane1Car2 : list.create n, lane2Car1 : list.create n, lane2Car2
    list.create n, lane3Car1 : list.create n, lane3Car2 : list.create n, lane4Car1 : list.create n, lane4Car2
    lane1Y = 562 : lane2Y = 502 : lane3Y = 444 : lane4Y = 390
    for x = 1 to 4
        gr.bitmap.draw car1Lane1[x], car1Lane1[x], goLeftPos[x], lane1Y
        list.add lane1Car1, car1Lane1[x]
        gr.bitmap.draw car2Lane1[x], car2Lane1[x], goLeftPos[x + 4], lane1Y
        list.add lane1Car2, car2Lane1[x]
        
        gr.bitmap.draw car1Lane2[x], car1Lane2[x], goRightPos[x], lane2Y
        list.add lane2Car1, car1Lane2[x]
        gr.bitmap.draw car2Lane2[x], car2Lane2[x], goRightPos[x + 4], lane2Y
        list.add lane2Car2, car2Lane2[x]
        
        gr.bitmap.draw car1Lane3[x], car1Lane3[x], goLeftPos[x], lane3Y
        list.add lane3Car1, car1Lane3[x]
        gr.bitmap.draw car2Lane3[x], car2Lane3[x], goLeftPos[x + 4], lane3Y
        list.add lane3Car2, car2Lane3[x]
        
        gr.bitmap.draw car1Lane4[x], car1Lane4[x], goRightPos[x], lane4Y
        list.add lane4Car1, car1Lane4[x]
        gr.bitmap.draw car2Lane4[x], car2Lane4[x], goRightPos[x + 4], lane4Y
        list.add lane4Car2, car2Lane4[x]
        
    next
    
    gosub InitialiseCars
    
    dim logs1[2], logs2[2], frogHomeSafe[5], frogCount[5]
    dim turtles1[2], turtles2[2]
    for x = 1 to 2
        gr.bitmap.load logs, path$ + "log.png"
        if logs  = -1 then call ImageLoadError("log")
        gr.bitmap.scale logs1[x], logs, 250,50
        gr.bitmap.scale logs2[x], logs, 250,50
        
        gr.bitmap.load turtle , path$ + "turtle.png"
        if turtle  = -1 then call ImageLoadError("turtle")
        gr.bitmap.scale turtles1[x], turtle, 250, 45
        gr.bitmap.scale turtles2[x], turtle, 250, 45
    next
    
    log1Pos = 110 : log2Pos = 220: turtle1Pos = 165 : turtle2Pos = 280
    gr.bitmap.draw logs1[1], logs1[1], 1000, log1Pos 
    gr.bitmap.draw logs1[2], logs1[2], 100, log1Pos 
    gr.bitmap.draw logs2[1], logs2[1], 300, log2Pos
    gr.bitmap.draw logs2[2], logs2[2], 1300, log2Pos
    
    gr.bitmap.draw turtles1[1], turtles1[1], 1000, turtle1Pos
    gr.bitmap.draw turtles1[2], turtles1[2], 100, turtle1Pos
    gr.bitmap.draw turtles2[1], turtles2[1], 300, turtle2Pos
    gr.bitmap.draw turtles2[2], turtles2[2], 1300, turtle2Pos
    
    frogX = di_width / 2 : frogY = 614
    gr.bitmap.load frogRun, path$ + "frogRun.png"
    if frogRun  = -1 then call ImageLoadError("frogRun")
    gr.bitmap.scale frogRun, frogRun, 60,50
    gr.bitmap.draw frogRun, frogRun, frogX, frogY
    
    !Load the four frogs that show how many lives are left 
    for x = 1 to 4
        gr.bitmap.load lives, path$ + "frogRun.png"
        if lives  = -1 then call ImageLoadError("frogRun")
        gr.bitmap.scale frogLives[x], lives, 30, 25
        gr.bitmap.draw frogLives[x], frogLives[x], (x - 1) * 30, di_height - 60
    next
    
    gr.bitmap.load deadFrog, path$ + "squashedFrog.png"
    if deadFrog  = -1 then call ImageLoadError("squashedFrog")
    gr.bitmap.scale deadFrog, deadFrog, 80, 70
    gr.bitmap.draw deadFrog, deadFrog, - 100, - 100
    
    for x = 1 to 5
        gr.bitmap.load frogHome, path$ + "frogHome.png"
        if frogHome  = -1 then call ImageLoadError("frogHome")
        gr.bitmap.scale frogHome, frogHome, 100,50
        gr.bitmap.draw frogHomeSafe[x], frogHome, 70 + (x - 1) * 230, 60
    next
    
    !Test for collision with frog got home boxes
    dim homeBox[5]
    gr.color 0,255,255,255
    for x = 1 to 5
        gr.rect homeBox[x], 80 + (x - 1) * 230, 70, (70 + (x - 1) * 230) + 90, 100
    next
    
    gr.bitmap.load arrows, path$ + "conArrows.png"
    if arrows  = -1 then call ImageLoadError("conArrows")
    gr.bitmap.scale arrows, arrows, 250, 250
    
return

!------------------------------------------------
! Load and draw, scale the game background scenery
!------------------------------------------------
DrawScenery:
    !Load and scale the background scenery
    gr.bitmap.load bg, path$ + "hopperBG2.png"
    if ! bg then call ImageLoadError("hopperBG2")
    gr.bitmap.scale bg, bg, di_width, di_height - 45
    gr.bitmap.draw bg, bg, 0, 45
    
    ! Draw score, time and high score at Bottom of screen
    gr.text.size 30 : gr.color 255,255,255,255
    gr.text.draw txtScore, 50, bb, "Score: " + int$(score)
    gr.text.align 2 : gr.text.draw txtTime, di_width / 2, bb, "Time"
    gr.text.draw txtInfo, di_width / 2, 370, "Congratulations level " + int$(level + 1) + " Completed" 
    gr.text.align 3 : gr.text.draw txtHigh, di_width - 50, bb, "High Score: " + int$(highScore)
    gr.text.align 1 : gr.hide txtInfo
    gr.render
return

!------------------------------------------------
! Draw the games 300 seconded timer
!------------------------------------------------
DrawTimer:
    gr.color 150,0,0,255,1
    gr.rect hopTime, (di_width / 2) - 150, sb+10, (di_width / 2) + 150, bb
return

!------------------------------------------------
! Decrease the time the games as left
!------------------------------------------------
DecreaseTime:
    now = (time() - tick) / 1000 
    countDown = int(now) 
    gr.modify hopTime, "left", (di_width / 2) - 150, "top", sb+10, "right", ((di_width / 2) + 150) - countDown, "bottom", bb
    if countDown > = allowedTime then noTimeLeft = TRUE
return

!------------------------------------------------
! Draw the controler so the user can moves the frog
!------------------------------------------------
DrawControler:
    gr.set.stroke 3 : cir = 40
    gr.color 100,255,255,255,0
   !gr.circle null, di_width - 250, di_height - 200, 80
    gr.bitmap.draw arrows, arrows, di_width - 250, di_height - 250
    gr.circle butTop, 1028,472, cir : gr.circle butBottom, 1028,620, cir
    gr.circle butLeft, 955,548, cir : gr.circle butRight, 1097,548, cir
    gr.render
return

!------------------------------------------------
! Play Hopper
!------------------------------------------------
PlayHopper:
    do
        do
            gosub TopButton
            gosub MoveCars
            gosub MoveLogsTurtles

            gosub MoveFrog
            gosub FrogCrossRoad
            gosub FrogCrossRiver 
            gosub FrogScoring
            gosub DecreaseTime
            gr.render
        until noTimeLeft | frogsDead = 5
        gosub NewGame
    until gameOver
return

!------------------------------------------------
! Initialise cars by seting up groups for top, middle and bottom cars
!------------------------------------------------
InitialiseCars:
    ! Setup group for lane one bottom, cars going left
    list.get lane1Car1, 4, pos1 
    list.get lane1Car2, 4, pos2
    gr.group.list runLane1Cars1, lane1Car1
    gr.group.list runLane1Cars2, lane1Car2
    
    ! Setup group for lane two, cars going right
    list.get lane2Car1, 4, pos3 
    list.get lane2Car2, 4, pos4
    gr.group.list runLane2Cars1, lane2Car1
    gr.group.list runLane2Cars2, lane2Car2
    
    !Setup group for lane three, cars going left
    list.get lane3Car1, 4, pos5
    list.get lane3Car2, 4, pos6
    gr.group.list runLane3Cars1, lane3Car1
    gr.group.list runLane3Cars2, lane3Car2
    
    ! Setup group for lane four top, cars going right
    list.get lane4Car1, 4, pos7
    list.get lane4Car2, 4, pos8
    gr.group.list runLane4Cars1, lane4Car1
    gr.group.list runLane4Cars2, lane4Car2
    
return

!------------------------------------------------
! Move the logs and turtles when they go of screen they appear at the other side
!------------------------------------------------
MoveLogsTurtles:
    ! Top logs
    gr.move logs1[1], logSpeed - 1
    gr.move logs1[2], logSpeed - 1
    gr.get.position logs1[1], Xpos1, y
    gr.get.position logs1[2], Xpos2, y
    
    if Xpos1 > di_width + 100 then gr.move logs1[1], - 1700
    if Xpos2 > di_width + 100 then gr.move logs1[2], - 1700
    
    ! Bottom logs
    gr.move logs2[1], logSpeed
    gr.move logs2[2], logSpeed
    gr.get.position logs2[1], Xpos1, y
    gr.get.position logs2[2], Xpos2, y
    
    if Xpos1 > di_width + 100 then gr.move logs2[1], - 1700
    if Xpos2 > di_width + 100 then gr.move logs2[2], - 1700
    
    !Top turtle
    gr.move turtles1[1], - logSpeed +1
    gr.move turtles1[2], - logSpeed + 1
    gr.get.position turtles1[1], Xpos1, y
    gr.get.position turtles1[2], Xpos2, y
    
    if Xpos1 < - 250 then gr.move turtles1[1], di_width + 1000
    if Xpos2 < - 250 then gr.move turtles1[2], di_width + 1000
    
    !Top bottom
    gr.move turtles2[1], - logSpeed
    gr.move turtles2[2], - logSpeed
    gr.get.position turtles2[1], Xpos1, y
    gr.get.position turtles2[2], Xpos2, y
    
    if Xpos1 < - 250 then gr.move turtles2[1], di_width + 1000
    if Xpos2 < - 250 then gr.move turtles2[2], di_width + 1000
return

!------------------------------------------------
! Move the cars and when they go of screen they appear at the other side
!------------------------------------------------
MoveCars:
    
    ! Lane one Bottom cars going left
    gr.move runLane1Cars1, - carSpeed - 2
    gr.move runLane1Cars2, - carSpeed - 2
 
    gr.get.position pos1, Xpos1, y
    gr.get.position pos2, Xpos2, y
    
    ! was 1500
    if Xpos1 < - 100 then gr.move runLane1Cars1, di_width + 3000
    if Xpos2 < - 100 then gr.move runLane1Cars2, di_width + 3000
    
    ! Lane two cars going right
    gr.move runLane2Cars1, + carSpeed + 2
    gr.move runLane2Cars2, + carSpeed + 2
    gr.get.position pos3, Xpos3, y
    gr.get.position pos4, Xpos4, y
   
    if Xpos3 > di_width + 100 then gr.move runLane2Cars1, - 2700
    if Xpos4 > di_width + 100 then gr.move runLane2Cars2, - 2700
    
    ! Lane three cars going left
    gr.move runLane3Cars1, - carSpeed
    gr.move runLane3Cars2, - carSpeed
    gr.get.position pos5, Xpos5, y
    gr.get.position pos6, Xpos6, y
    !was 150
    if Xpos5 < - 100 then gr.move runLane3Cars1, di_width + 3000
    if Xpos6 < - 100 then gr.move runLane3Cars2, di_width + 3000
    
    ! Lane four cars going right
    gr.move runLane4Cars1, + carSpeed
    gr.move runLane4Cars2, + carSpeed
    gr.get.position pos7, Xpos7, y
    gr.get.position pos8, Xpos8, y
    
    if Xpos7 > di_width + 100 then gr.move runLane4Cars1, - 2700
    if Xpos8 > di_width + 100 then gr.move runLane4Cars2, - 2700
   
return

!------------------------------------------------
! If the user taps on the control panel to move the frog
!------------------------------------------------
MoveFrog:
    frogSafe = FALSE
    inkey$ ik$

    if ik$ = "@" then       % no hard key has been typed
        gr.touch t, tx, ty  % -> try to get action from virtual pad
        if oldT & t then return else oldT = 0

        if t then
            tx /= scale_x : ty /= scale_y
            gr.modify collision, "x", tx, "y", ty 
            gr.get.position frogRun, fx, fy

            if gr_collision(collision,butTop) then
                ik$ = "up"
            elseif fy < frogY & gr_collision(collision,butBottom) then
                ik$ = "down"
            elseif fx + 120 < di_width & gr_collision(collision,butRight) then
                ik$ = "right"
            elseif fx > 70 & gr_collision(collision,butLeft) then
                ik$ = "left"
            endif
            virtualPad = TRUE  % Needed for starting new game
            oldT = 1
        endif
    endif
    
    
    if oldKey & ik$ <> "@" then return else oldKey = FALSE
    gr.get.position frogRun, fx, fy
   
    if ik$ = "up" then
        gr.move frogRun, 0, - frogStep 
        frogLane ++ : frogSafe = TRUE : oldKey = TRUE
        hardPad = TRUE % Needed for starting new game
    elseif ik$ = "down" & frogLane > 1 then
        gr.move frogRun, 0, + frogStep : oldKey = TRUE
        frogLane -- : frogSafe = TRUE
    elseif ik$ = "right" & fx + 120 < di_width then
        gr.move frogRun, + frogStep : oldKey = TRUE
        frogSafe = TRUE
    elseif ik$ = "left" & fx > 70 then
        gr.move frogRun, - frogStep : oldKey = TRUE
        frogSafe = TRUE
    else % no action, neither from hard pad nor virtual keypad
        return
    endif

    if frogSafe then
        if buzz then vibrate buzzGame[], -1
        call PlaySound(sound,soundFrog)
    endif
return

!------------------------------------------------
! Test for collision on the road
!------------------------------------------------
FrogCrossRoad:
    frogDead = FALSE
    if frogLane = 1 then
        for x = 1 to 4
            if gr_collision(frogRun, car1Lane1[x]) then frogDead = TRUE
            if gr_collision(frogRun, car2Lane1[x]) then frogDead = TRUE
        next
    endif
    
    if frogLane = 2 then
        for x = 1 to 4
            if gr_collision(frogRun, car1Lane2[x]) then frogDead = TRUE
            if gr_collision(frogRun, car2Lane2[x]) then frogDead = TRUE
        next
    endif
    
    if frogLane = 3 then
        for x = 1 to 4
            if gr_collision(frogRun, car1Lane3[x]) then frogDead = TRUE
            if gr_collision(frogRun, car2Lane3[x]) then frogDead = TRUE
        next
    endif
    
    if frogLane = 4 then
        for x = 1 to 4
            if gr_collision(frogRun, car1Lane4[x]) then frogDead = TRUE
            if gr_collision(frogRun, car2Lane4[x]) then frogDead = TRUE
        next
    endif
    if frogDead then gosub DeadFrogCount
return

!------------------------------------------------
! Frog cross the river using the logs and turtles
!------------------------------------------------
FrogCrossRiver:
    frogSafe = FALSE
    if frogLane = 6 then
        for x = 1 to 2
            if gr_collision(frogRun, turtles2[x]) then 
                 gr.move frogRun, - logSpeed
                 frogSafe = TRUE
            endif
        next
        if ! frogSafe then gosub DeadFrogCount
    endif
    
    if frogLane = 7 then
        for x = 1 to 2
            if gr_collision(frogRun, logs2[x]) then
                 gr.move frogRun, logSpeed
                 frogSafe = TRUE
            endif
        next
        if ! frogSafe then gosub DeadFrogCount
    endif
    
    if frogLane = 8 then
        for x = 1 to 2
            if gr_collision(frogRun, turtles1[x]) then
                 gr.move frogRun, - logSpeed + 1
                 frogSafe = TRUE
            endif
        next
        if ! frogSafe then gosub DeadFrogCount
    endif
    
    if frogLane = 9 then
        for x = 1 to 2
            if gr_collision(frogRun, logs1[x]) then
                 gr.move frogRun, logSpeed - 1
                 frogSafe = TRUE
            endif
        next
        if ! frogSafe then gosub DeadFrogCount
    endif
    
return

!------------------------------------------------
! Add to the dead frogs. Revive Frog if dead frogs is less then allowed dead
!------------------------------------------------
DeadFrogCount:
    call PlaySound(sound, soundRunOver)
    frogsDead ++ 
    if frogsDead < allowedDead then gosub ReviveFrog : gr.hide frogLives[5 - frogsDead]
return

!------------------------------------------------
! Scoring. Lane five and ten gain you points
!------------------------------------------------
FrogScoring:
     if frogLane = 10 then
        bonus = FALSE
        for x = 1 to 5
            if ! frogCount[x] & gr_collision(frogRun, homeBox[x]) then 
                gr.show frogHomeSafe[x]
                frogCount[x] = TRUE
                score += 100
                bonus = TRUE 
                frogsHome += 1
                if frogsHome < 5 then call PlaySound(sound, soundFrogHome)
            endif
        next
        if ! bonus then score += 50
        if frogsHome = 5 then gosub ResetFrogs
        gr.modify txtScore, "text", "Score: " + int$(score)
        gr.modify frogRun, "x", frogX, "y", frogY
        centreLane = FALSE : frogLane = 0
        gr.render
    endif
    
    if frogLane = 5 & ! centreLane then 
        score += 50 : centreLane = TRUE
        gr.modify txtScore, "text", "Score: " + int$(score)
    endif
return

!------------------------------------------------
! Display the dead frog and reset the frog
!------------------------------------------------
ReviveFrog:
    gr.get.position frogRun, fx, fy
    gr.modify deadFrog, "x", fx, "y", fy
    gr.modify frogRun, "x", frogX, "y", frogY
    centreLane = FALSE 
    for x = 1 to 15
        gosub MoveCars
        gosub MoveLogsTurtles
        gr.render
    next
    gr.modify deadFrog, "x", - 100, "y", - 100
    frogLane = 0
return

!------------------------------------------------
! Hide the top frog. Give bones. increase level, logSpeed and carSpeed
!------------------------------------------------
ResetFrogs:
    call PlaySound(sound, soundAllHome)
    gr.show txtInfo
    gr.modify txtInfo, "text", "Congratulations level " + int$(level + 1) + " Completed" 
    gr.modify frogRun, "x", frogX, "y", frogY
    for x = 1 to 100
        gosub MoveCars
        gosub MoveLogsTurtles
        gr.render
    next
    gr.hide txtInfo
    
    for x = 1 to 5
        frogCount[x] = FALSE
        gr.hide frogHomeSafe[x]
    next
    score += 1000
    centreLane = FALSE : frogsHome = 0
    level += 1 : logSpeed += level
    carSpeed += level
   
return

!------------------------------------------------
! Draw the top, play and exit buttons and draw text on them
!------------------------------------------------
DrawTopButtons:
    !Top Buttons
    t = 5 : b = 40
    gr.color 255,79,79,255
    gr.rect butMusic, 20, t, 170, b
    gr.rect butSound, 180, t, 330, b
    gr.rect butBuzz, 340, t, 490, b
    gr.rect butReset, 500, t, 650, b
    gr.rect butHelp, 660, t, 810, b
    gr.rect butExit, di_width - 170, t, di_width - 20, b
    
    ! Play button to start and new game.
    gr.color 155,79,79,255
    gr.rect butPlay, (di_width / 2) - 160, 390, (di_width / 2) + 160, 440
    
    !Text on bottom buttons
    gr.color 255,255,255,255
    gr.text.align 2 : gr.text.size 27
    if music then m$ = "Music: On" else m$ = "Music: Off"
    gr.text.draw txtMusic, 95, b - 10, m$
    if sound then s$ = "Sound: On" else s$ = "Sound: Off"
    gr.text.draw txtSound, 255,b - 10, s$
    if buzz then b$ ="Buzz: On" else b$ = "Buzz: Off"
    gr.text.draw txtBuzz, 415,b - 10,b$
    gr.text.draw txtReset, 575,b - 10,"Reset High"
    gr.text.draw txtHelp, 730, b - 10, "Help"
    gr.text.draw txtExit, di_width - 95, b - 10, "Exit"
    
    ! Play text on button to start and new game.
    gr.text.size 40
    gr.text.draw txtPlay, (di_width / 2), 425, "Play"
    gr.hide butPlay : gr.hide txtPlay
return

!------------------------------------------------
! Start a new game or end game
!------------------------------------------------
NewGame:
    if noTimeLeft then mess$ = "Time Up." else mess$ = "All Frogs Dead."
    
    if score > highScore then 
        call PlaySound(sound, soundNewHigh)
        highScore = score 
        mess$ += " New High Score!"
        gr.modify txtHigh, "text", "High Score: " + int$(highScore)
    else
       call PlaySound(sound, soundNotHigh)
    endif
    gr.modify txtInfo, "text", mess$
    gr.show butPlay : gr.show txtPlay : gr.show txtInfo
    
    if virtualPad then
        gr.modify txtPlay, "text", "Play"
    else
        gr.modify txtPlay, "text", "Go/Space to Play"
    endif
    
    gr.modify frogRun, "x", frogX, "y", frogY
    
    restartFlag = FALSE
    do
        gosub TopButton
        gosub MoveCars
        gosub MoveLogsTurtles
        if virtualPad then
            gr.touch tt, rtx, rty 
            rtx /= scale_x : rty /= scale_y
            gr.modify collision, "x", rtx, "y", rty
        else
            inkey$ ik$
            !if ik$ = " " then ik$ = "go"
            if ik$ <> "@" then
                if ik$ <> "up" & ik$ <> "down" & ik$ <> "left" & ik$ <> "right" then ik$ = "go"
            endif
        endif
        gr.render
        if virtualPad & gr_collision(collision, butPlay) then restartFlag = TRUE
        if hardPad & ik$ = "go" then restartFlag = TRUE
    until restartFlag
    if buzz then vibrate buzzGame[], -1
    call PlaySound(sound,soundClick)
    gr.hide butPlay : gr.hide txtPlay : gr.hide txtInfo
    gosub SetUp
return

!------------------------------------------------
! Test to see if one of the top bottons has been tapped
!------------------------------------------------
TopButton:
    gr.touch butTouch, btx, bty 
    if oldB & butTouch then return else oldB = FALSE
    if ! butTouch then return else oldB = TRUE
    btx /= scale_x : bty /= scale_y
    if bty > 40 then return
    gr.modify butCollision, "x", btx, "y", bty
    if gr_collision(butCollision,butMusic) then gosub ModMusic
    if gr_collision(butCollision,butSound) then gosub ModSound
    if gr_collision(butCollision,butBuzz) then gosub ModBuzz
    if gr_collision(butCollision,butReset) then gosub ResetHigh
    if gr_collision(butCollision,butHelp) then gosub ShowHelp
    if gr_collision(butCollision,butExit) then gosub LeaveHopper
    btx = 0 : bty = 0 : butTouch = 0
    butTap = TRUE
return

!------------------------------------------------
! Turn music on and off
!------------------------------------------------
ModMusic:
  if buzz then vibrate buzzGame[], -1
  call PlaySound(sound,soundClick)
  if music then
    music = FALSE
    audio.stop 
    gr.modify txtMusic, "text", "Music: Off"
  else
    music = TRUE
    audio.play babyJane
    audio.loop 
    gr.modify txtMusic, "text", "Music: On"
  endif
  gr.render
return

!------------------------------------------------
! Turn sound on and off
!------------------------------------------------
ModSound:
  if buzz then vibrate buzzGame[], -1
  call PlaySound(sound,soundClick)
  if sound then
    sound = FALSE
    gr.modify txtSound, "text", "Sound: Off"
  else
    sound = TRUE
    gr.modify txtSound, "text", "Sound: On"
  endif
  gr.render 
return

!------------------------------------------------
! Turn buzz on and off
!------------------------------------------------
ModBuzz:
  if buzz then vibrate buzzGame[], -1
  call PlaySound(sound,soundClick)
  if buzz then
    buzz = FALSE
    gr.modify txtBuzz, "text", "Buzz: Off"
  else
    buzz = TRUE
    gr.modify txtBuzz, "text", "Buzz: On"
  endif
  gr.render
return

!------------------------------------------------
! Reset the high score to zero 
!------------------------------------------------
ResetHigh:
  if buzz then vibrate buzzGame[], -1
  call PlaySound(sound,soundClick)
  do : dialog.message "Reset high Scores to Zero?",,yn,"Yes","No" : until yn > 0
  if buzz then vibrate buzzGame[], -1
  call PlaySound(sound,soundClick)
  if yn = 1 then 
    highScore = 0
    gr.modify txtHigh, "text", "High Score: " + int$(highScore)
  endif
  gr.render
return

!-------------------------------------------------
! Start background music at start of game, if music = TRUE
!-------------------------------------------------
StartMusic:
if music then
    audio.play babyJane 
    audio.loop 
  endif 
return

!------------------------------------------------
! Display the game's help file
!------------------------------------------------
ShowHelp:
    path$="Hopper/data/"
    if buzz then vibrate buzzGame[], -1
    call PlaySound(sound,soundClick)
    byte.open r, helpId, path$ + "HopperInfo.txt"
    if helpId <> -1 then
        file.size lof, path$ + "HopperInfo.txt"
        byte.read.buffer helpId, lof, help$
        byte.close helpId
        do : dialog.message " Hopper Help",help$,OK,"OK" : until OK > 0
        if buzz then vibrate buzzGame[], -1
        call PlaySound(sound,soundClick)
    else
         do : dialog.message "Hopper Help","Can't find help file",OK,"OK" : until OK > 0
    endif
return

!------------------------------------------------
! Exit the game or return to it
!------------------------------------------------
LeaveHopper:
    if buzz then vibrate buzzGame[], -1
    call PlaySound(sound,soundClick)
    do : dialog.message "Exit Hopper",,yn,"Yes", "No" : until yn > 0
    if buzz then vibrate buzzGame[], -1
    call PlaySound(sound,soundClick)
    if yn = 1 then gosub SaveData : pause 500 : exit
return

!------------------------------------------------
! Save the High Score and the game settings 
!------------------------------------------------
SaveData:
    path$="Hopper/data/"
    file.exists pathPresent,path$+"GameData.txt"
    if ! pathPresent then file.mkdir path$

    text.open w,hs,Path$+"GameData.txt"
    
    text.writeln hs,int$(highScore)
    text.writeln hs,int$(music)
    text.writeln hs,int$(sound)
    text.writeln hs,int$(buzz)
    text.close hs
return

!------------------------------------------------
! Load the High Score and the game settings 
!------------------------------------------------
LoadData:
    path$="Hopper/data/"
    file.exists pathPresent,path$+"GameData.txt"
    if pathPresent then
    text.open r,hs,path$+"GameData.txt"
    
    text.readln hs,highScore$
    text.readln hs,music$
    text.readln hs,sound$
    text.readln hs,buzz$
    text.close hs
    highScore = val(highScore$)
    music = val(music$)
    sound = val(sound$)
    buzz = val(buzz$)
    endif
return

!------------------------------------------------
! Functions in this gosub
!------------------------------------------------
Functions:

!------------------------------------------------
! if sound is on. Play sound (ptr)
!------------------------------------------------
  fn.def PlaySound(sound, ptr)
      if sound then Soundpool.play s,ptr,0.99,0.99,1,0,1
  fn.end
  
!------------------------------------------------
! load Sound files not found
!------------------------------------------------
    fn.def SoundLoadError(e$)
        dialog.message "Sound file " + e$ + " not found", "Game will end",OK,"OK" 
        end
    fn.end
    
!------------------------------------------------
! Load image file not found
!------------------------------------------------
    fn.def ImageLoadError(e$)
        dialog.message "Image file " + e$ + " not found", "Game will end",OK,"OK" 
        end
    fn.end
    
return
 


