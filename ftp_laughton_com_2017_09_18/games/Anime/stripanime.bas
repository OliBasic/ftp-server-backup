!debug.on

 
gr.open 255, 0,0,0
gr.Orientation 0
gr.Screen mx, my
gr.Color 255, 0, 0, 0, 0
myt=my/3
mxt=myt*4500/200
ryt=my/4
rxt=ryt*800/200


gr.bitmap.load tmp_ptr, "gallop.png"
gr.bitmap.scale gallup_ptr,tmp_ptr,mxt,myt
gr.bitmap.delete tmp_ptr

gr.bitmap.load tmp_ptr, "jump.png"
gr.bitmap.scale jump_ptr,tmp_ptr,mxt,myt*3/2 
gr.bitmap.delete tmp_ptr

gr.bitmap.load tmp_ptr, "panorama.png"
gr.bitmap.scale bg_ptr,tmp_ptr,mx*2,my
gr.bitmap.delete tmp_ptr

gr.bitmap.load tmp_ptr, "roadblock.png"
gr.bitmap.scale rb_ptr,tmp_ptr,rxt,ryt
gr.bitmap.delete tmp_ptr

gr.bitmap.size gallup_ptr,gx,gy
gr.bitmap.size jump_ptr,jx,jy 
gr.bitmap.size rb_ptr,rbx,rby

gx=gx/15 
bgx=gx/15
gtop=my*0.7-gy
jtop=my*0.7-jy
rbx=rbx/4
rbtop=my*0.7 - rby
mxpos=mx*0.3

framenum=0
bgnum=0
rbnum=0
rbstyle=4
collide=0

!!road blocks styles
noblock=4
bison=0
fence=1
cactus=2
tumbleweed=3
!!

!Jump stuff
jumpflag=0
jumphi=jy/6
jumpnum=0
array.load jarr[],0,1,2,3,4,5,6,6,7,7,8,9,10,11,12,13,14

gr.text.size my/5
gr.text.align 2
 
 

!debug.echo.on 		
gr.bitmap.crop tmp2_ptr,rb_ptr,rbstyle*rbx,0,rbx,rby 


Do
	gr.cls
	gr.bitmap.draw obj_ptr1,bg_ptr,0-bgnum,0 

	if rbstyle<4 then
		gr.bitmap.draw obj_ptr3,tmp2_ptr,mx-rbnum,rbtop
	endif 

	if jumpflag=1 then
		offY=jumphi - (abs(7-jarr[jumpnum])/7 )^2 * jumphi
		gr.bitmap.crop tmp_ptr,jump_ptr, jarr[jumpnum] *gx,0,gx,jy
		gr.bitmap.draw obj_ptr2, tmp_ptr, mxpos, jtop - offY
   	jumpnum=jumpnum+1
	else
		gr.bitmap.crop tmp_ptr,gallup_ptr,framenum*gx,0,gx,gy
		gr.bitmap.draw obj_ptr2, tmp_ptr, mxpos, gtop
	endif

 	
	!collide |
	if abs((mx-rbnum)-(mxpos+gy/2))<bgx & rbstyle < 4 then
		if jumpnum< 8 | jumpnum>10 then
           collide=1
		endif
	endif 

    if collide=1 then 
         gr.color 255,255,0,0,1 
        gr.text.draw ouch, mx/2, my*2/5, "Ouch!" 
     endif
	gr.render
	
 	gr.bitmap.delete tmp_ptr 
  
	pause 10

	
	!counters
	framenum=mod(framenum + 1, 15)
	bgnum=mod(bgnum+bgx,mx)
	rbnum=mod(rbnum+bgx,mx+rbx)

	
	!check for jumps
	if jumpnum=18 & jumpflag=1 then jumpflag=0
	gr.touch touched, tx,ty
	
	if touched=1 & jumpflag=0 then 
 
	   jumpflag=1
	   framenum=0
     jumpnum=1
	endif 
	
	!check for roadblocks
	if rbnum<bgx then
      collide=0
		 rbstyle=floor(rnd(0)*5)
      gr.bitmap.delete tmp2_ptr 
   	 	gr.bitmap.crop tmp2_ptr,rb_ptr,rbstyle*rbx,0,rbx,rby 
	endif
	
until 0
 
 
OnError:
gr.Close
End

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
