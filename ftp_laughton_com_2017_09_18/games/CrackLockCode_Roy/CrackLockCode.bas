Rem Crack Lock Code
Rem For Android
Rem With RFO Basic!
Rem March 2017
Rem Version 1.00
Rem Author: Roy Shepherd

PORTRAIT = 1 : LANDSCAPE = 0
orientation = PORTRAIT

if orientation = LANDSCAPE then 
    di_height = 672
    di_width = 1150
elseif orientation = PORTRAIT then
    di_height = 1150
    di_width = 672
endif
        
gr.open 255, 117, 117, 255
gr.color 255, 255, 255, 255
gr.text.size 30

gr.orientation orientation
pause 500
WakeLock 3
gr.screen screenWidth, screenHeight
scale_x = screenWidth / di_width
scale_y = screenHeight / di_height
gr.scale scale_x, scale_y

globals.all : locals.off
gosub Functions            
gosub Initialise
gosub SetUp
call Render()
timer.set 250
do 
    gosub KeyPressed
until 0

!------------------------------------------------
! BackKey interrupt. Double tap back key to end
!------------------------------------------------
onBackKey:
    backKeyHit = BackKey(backKeyHit)
back.resume 

!------------------------------------------------
! onTimer interrupt. I'm using it to reduce the score
!------------------------------------------------
onTimer:
    call ReduceScore()
timer.resume

!------------------------------------------------
! Do once at first run of app
!------------------------------------------------
Initialise:
    !Set Global variables 
    TRUE = 1 : FALSE = 0
    sound = TRUE : buzz = TRUE
    backKeyHit = 1 : best = 0 : score = 1000
    backTick = 0 % for double tap back key to end
    userInput = 0 : totalGuesses = 0
    dim userNum[3], androidNum[3]
    maxSize = max(di_width, di_height)
    w = di_width : h = di_height
    redB = 86 : greenB = 119 : blueB = 252
   
    dataPath$ = "CrackLockCode/data/"
    file$ = "LockData.txt"
    fn$ = dataPath$ + "/" + file$
    gosub LoadData
    
    ! Set button colours
    gr.color 255, 230, 230, 230 : gr.paint.get gray 
    gr.color 255, 100, 100, 100 : gr.paint.get darkGray
    ! Lock unlocked circle colours
    gr.color 255, 255, 0, 0     : gr.paint.get red
    gr.color 255, 0, 255, 0     : gr.paint.get green
    
    ! Set background colour
    gr.color 255, redB, greenB, blueB, 1
    gr.rect background, 0, 0, maxSize, maxSize
    gr.get.value background, "paint", pBg
    
    ! Heading 
    gr.color 255, 100, 100, 100
    gr.rect null , 0, 0, di_width, 50 
    gr.color 255, 255, 255, 255
    gr.text.align 2 : gr.text.draw null, w / 2, 30, "Numeric Lock"

    butMenu = DrawOptionsButton()
   
    ! Load and draw the lock
    gr.bitmap.load lock1, dataPath$ + "lock.png"
    call ImageLoadError(lock1, "lock.png")
    gr.bitmap.scale scale, lock1, w - 100, w - 100
    gr.bitmap.draw lock, scale, 50, 100
    gr.bitmap.delete lock1
    
    ! Draw & hide dark window
    gr.color 180, 0, 0, 0, 1
    gr.rect darkWindow, 0, 0, maxSize, maxSize
    gr.color 255 : gr.hide darkWindow
    
    ! Load and draw sound on/off buzz on/off
    gr.bitmap.load s, dataPath$ + "soundOn.png"
    call ImageLoadError(s, "soundOn.png")
    gr.bitmap.scale scale, s, 75, 75
    gr.bitmap.draw soundOn, scale, w - 100, (h / 2)
    
    gr.bitmap.load s, dataPath$ + "soundOff.png"
    call ImageLoadError(s, "soundOff.png")
    gr.bitmap.scale scale, s, 75, 75
    gr.bitmap.draw soundOff, scale, w - 100, (h / 2)
    if sound then gr.hide soundOff
    ! Buzz
    gr.bitmap.load b, dataPath$ + "buzzOn.png"
    call ImageLoadError(b, "buzzOn.png")
    gr.bitmap.scale scale, b, 75, 75
    gr.bitmap.draw buzzOn, scale, 40, (h / 2)
    
    gr.bitmap.load b, dataPath$ + "buzzOff.png"
    call ImageLoadError(b, "buzzOff.png")
    gr.bitmap.scale scale, b, 75, 75
    gr.bitmap.draw buzzOff, scale, 40, (h / 2)
    if ! buzz then gr.hide buzzOn
    
    ! Draw on/off sound and buzz buttons
    gr.color 0
    gr.rect butSoundOnOff, w - 120, (h / 2), w, (h / 2) + 100
    gr.rect butBuzzOnOff, 10, (h / 2), 120, (h / 2) + 100
    gr.color 255 
    
    ! Draw and display the guess boxes
    dim txt1[9], txt2[9], guess[10, 3], userTry[3]
    openShut = DrawGuessBoxes() 
    gosub HideGuessText
    gosub DrawLockButtons
   
   ! Draw Score and Best
    gr.color 255, 255, 255, 255,1
    gr.text.draw myScore, 20, 100, "Score: " + int$(score)
    gr.text.align 3
    gr.text.draw myBest, w - 20, 100, "Best: " + int$(best)
    
    ! Draw and hide Options Menu, About and Help boxes
    gr.text.size 40 : gr.text.align 1
    gosub DrawOptionsMenu
    gosub DrawAbout
    gosub DrawHelp
    gosub DrawYesNoBox
    gosub DrawPopUp
    
    ! Load the colour picker to change background colour
    gr.bitmap.load c, dataPath$ + "colourPicker.png"
    call ImageLoadError(c, "colourPicker.png")
    gr.bitmap.draw colourPick, c, 190, 400
    gr.hide colourPick
    
    ! Setup for collision for buttons tapper
    gr.color 0
    gr.point collision, -1, -1
    gr.color 255
    
    ! Load button sound
    soundpool.open 3
    soundpool.load buttonClick, dataPath$ + "click.wav"
    if ! buttonClick then call SoundLoadError("click.wav")
    soundpool.load  dogsOut, dataPath$ + "dogsOut.mp3"
    if ! dogsOut then call SoundLoadError("dogsOut.mp3")
    soundpool.load  beep, dataPath$ + "beep.wav"
    if ! beep then call SoundLoadError("beep.wav")
    soundpool.load loser, dataPath$ + "loser.mp3"
    if ! loser then call SoundLoadError("loser.mp3")
    
    ! for testing
    gr.color 255, 255, 255, 255
    gr.text.draw test, 50, 200, ""

return

!------------------------------------------------
! Do at start of each new game
!------------------------------------------------
SetUp:
    userInput = 0 : totalGuesses = 0
    score = 1000
    ! Hide hints
    gr.hide allTxt1 : gr.hide allTxt2
    ! Hide all guesses
    for x = 1 to 10
        for y = 1 to 3 
            gr.hide guess[x,y]
        next 
    next 
    call GetAndroidNum(androidNum[])
    !gr.modify test, "text", int$(androidNum[1]) + " " + int$(androidNum[2]) + " " + int$(androidNum[3])
    gr.modify openShut, "paint", red
    gr.modify myScore, "text", "Score: " + int$(score)
    gameOver = FALSE : pausing = TRUE
    call Render()
return

!------------------------------------------------
! Hide the int text users input and all the guesses
!------------------------------------------------
HideGuessText:
    ! Hide top and bottom lines of text
    list.create n, txt1ListPointer
    list.add.array txt1ListPointer, txt1[]
    gr.group.list allTxt1, txt1ListPointer
    
    list.create n, txt2ListPointer
    list.add.array txt2ListPointer, txt2[]
    gr.group.list allTxt2, txt2ListPointer
     
    gr.hide allTxt1 : gr.hide allTxt2
    
return

!------------------------------------------------
! Draw the buttons to select the lock numbers
!------------------------------------------------
DrawLockButtons:
    
    dim butLock[12]
    gr.color 0, 255, 255, 255, 0
    ! buttons 1 to 4
    gr.rect butLock[1], 150, 190, 235, 285
    gr.rect butLock[2], 250, 172, 325, 285
    gr.rect butLock[3], 348, 172, 416, 285
    gr.rect butLock[4], 440, 179, 510, 285
    
    ! Buttons 5 to 8
    gr.rect butLock[5], 133, 295, 223, 398
    gr.rect butLock[6], 250, 295, 322, 398
    gr.rect butLock[7], 350, 295, 420, 398
    gr.rect butLock[8], 440, 295, 540, 398
    
    ! Buttons Cancel, 9, 0, and Try
    gr.rect butLock[9], 250, 410, 322, 515
    gr.rect butLock[10], 350, 410, 421, 515 % zero
    gr.rect butLock[11], 126, 410, 227, 532 % Cancel
    gr.rect butLock[12], 440, 410, 542, 532 % Try
    gr.color 255, 255, 255, 255, 0
return

!------------------------------------------------
! Draw the options menu
!------------------------------------------------
DrawOptionsMenu:
    gr.color 255, 255, 255, 255, 1
    gr.rect optRec, w - 300, 50, w, 405, 10, 10 
    gr.color 0
    gr.rect butBgColour, (w - 275) , 60, (w - 25), 135, 10, 10 

    gr.rect butZeroBest, (w - 275) , 145, (w - 25), 220, 10, 10 
    
    gr.rect butAbout, (w - 275) , 230, (w - 25), 305, 10, 10
    
    gr.rect butHelp, (w - 275) , 315, (w - 25), 390, 10, 10
    
    gr.color 255, 0, 0, 0
    gr.text.draw txtBgColour, w - 265, 110, "Background"
    gr.text.draw txtZeroBest, w - 256, 195, "Reset Best"
    gr.text.draw txtAbout, w - 210, 280, "About"
    gr.text.draw txtHelp, w - 190, 365, "Help"
    
    gr.group allOptionsMenu, optRec, butBgColour, butZeroBest, butAbout, butHelp, txtBgColour, txtZeroBest, txtAbout, txtHelp
    gr.hide allOptionsMenu
return

!------------------------------------------------
! Draw the About box
!------------------------------------------------
DrawAbout:
    dim aboutBox[5]
    gr.color 255, 255, 255, 255
    gr.rect aboutBox[1], (w / 2) - 200, (h / 2) - 150, (w / 2) + 200, (h / 2) + 100, 15, 15
    gr.color 255, 0, 0, 0 : gr.text.align 2
    gr.text.draw aboutBox[2], w / 2, (h / 2) - 100, "Crack Lock Code"
    gr.text.draw aboutBox[3], w / 2, (h / 2) - 40,  "March 2016"
    gr.text.draw aboutBox[4], w / 2, (h / 2) + 20,  "Version: 1.00"
    gr.text.draw aboutBox[5], w / 2, (h / 2) + 80,  "Author: Roy Shepherd"
    gr.group about, aboutBox[1], aboutBox[2], aboutBox[3], aboutBox[4], aboutBox[5] 
    gr.hide about : array.delete aboutBox[] : gr.text.align 1
return

!------------------------------------------------
! Draw the popup box
!------------------------------------------------
DrawPopUp:
    gr.color 220, 255, 255, 255
    gr.text.size 30
    gr.rect myPopUp, (w / 2) - 200, (h / 2) - 50, (w / 2) + 200, (h / 2) + 50, 50, 50
    gr.color 255, 0, 0, 0 : gr.text.align 2
    gr.text.draw txtPopUp, w / 2, (h / 2) + 10, "No Repeating Digits"
    gr.text.size 40 : gr.hide myPopUp : gr.hide txtPopUp
return

!------------------------------------------------
! Draw the Yes No box
!------------------------------------------------
DrawYesNoBox:
    gr.text.size 40
    dim yesNoBox[9]
    gr.color 255, 255, 255, 255
    gr.rect yesNoBox[1], (w / 2) - 200, (h / 2) - 150, (w / 2) + 200, (h / 2) + 100, 15, 15
    gr.color 255, 0, 0, 0 : gr.text.align 2
    gr.text.draw yesNoBox[2], w / 2, (h / 2) - 100, "Line one"
    gr.text.draw yesNoBox[3], w / 2, (h / 2) - 40,  "Line two"
    
    gr.color 255, 230, 230, 230
    gr.rect yesNoBox[4], (w / 2) - 200, (h / 2) + 20, (w / 2), (h / 2) + 100, 15, 15 
    gr.rect yesNoBox[5], (w / 2), (h / 2) + 20, (w / 2) + 200, (h / 2) + 100, 15, 15
    
    gr.color 255, 100, 100, 100, 0
    gr.rect yesNoBox[6], (w / 2) - 200, (h / 2) + 20, (w / 2), (h / 2) + 100, 15, 15 
    gr.rect yesNoBox[7], (w / 2), (h / 2) + 20, (w / 2) + 200, (h / 2) + 100, 15, 15
    
    gr.color 255, 0, 0, 0, 1
    gr.text.draw yesNoBox[8], (w / 2)- 100, (h / 2) + 70,  "Yes"
    gr.text.draw yesNoBox[9], (w / 2) + 100, (h / 2) + 70,  "No"
    
    list.create n, yesNoListPtr
    list.add.array yesNoListPtr, yesNoBox[]
    gr.group.list allYesNo, yesNoListPtr
    gr.hide allYesNo : gr.text.align 1
return

!------------------------------------------------
! Draw Help
!------------------------------------------------
DrawHelp:
    dim helpBox[16], help$[16]
    help$[1] = "Crack the Lock's Code:"
    help$[2] = "This numeric lock has a three digit code, chosen at"
    help$[3] = "random by your device. There are no repeated digits."

    help$[4] = "You have to workout the code by tapping in three digits" 
    help$[5] = "and then tap Try. You will get clues to the code in the"
    help$[6] = "boxes bellow the numeric lock. Use these clues to workout"
    help$[7] = "the lock's code. You have ten trys to obtain the lock's" 
    help$[8] = "code. If you tap in a digit and realize you have made an"
    help$[9] = "error, tap Cancel to start over."
    
    help$[10] = "The score starts at 1000 and begins to decrease after a"
    help$[11] = "button is tapped, so the faster you solve to lock's code"
    help$[12] = "the higher your score."

    help$[13] = "If the score falls to zero before you solve the code,"
    help$[14] = "you can still carry on and try and solve the code."
    help$[15] = "For options tap the options button (top right)"
    
    gr.color 255, 240, 240, 240 : gr.text.size 22
    gr.rect helpBox[1], (w / 2) - 300, (h / 2) - 450, (w / 2) + 300, (h / 2) + 430, 15, 15
    gr.text.align 1 : gr.color 255, 0, 0, 0 : y = (h / 2) - 400
    for x = 1 to 15
        gr.text.draw helpBox[x + 1], (w / 2) - 290, y, help$[x]
        y += 50
        if x = 1  then gr.color 255, 0, 0, 255
        if x = 3  then y += 25 : gr.color 255, 0, 128, 0
        if x = 9  then y += 25 : gr.color 255, 255, 17, 17
        if x = 12 then y += 25 : gr.color 255, 255, 0, 255
    next
    list.create n, elementListPointer
    list.add.array elementListPointer, helpBox[]
    gr.group.list allHelp, elementListPointer
    array.delete help$[] : array.delete helpBox[] 
    gr.hide allHelp
return

!------------------------------------------------
! If a key is pressed then acted accordingly 
!------------------------------------------------
KeyPressed:
    gr.touch t, tx2, ty2
    if ! t then return
    tx2 /= scale_x : ty2 /= scale_y
    gr.modify collision, "x", tx2, "y", ty2
    gosub WaitKeyUp
    
    if gr_collision(collision, butMenu) then gosub OptionsMenu       : return
    if gr_collision(collision, butSoundOnOff)  then gosub SoundOnOff : return
    if gr_collision(collision, butBuzzOnOff)   then gosub BuzzOnOff  : return
    pausing = FALSE
    for x = 1 to 12
        if gr_collision(collision, butLock[x]) then 
            call UserEnter(userInput, x) : gosub WaitKeyUp : f_n.break
        endif
    next    
return

!------------------------------------------------
! Display the options menu
!------------------------------------------------
OptionsMenu:
    pausing = TRUE
    gr.show darkWindow
    gr.show allOptionsMenu
    call PlaySound(sound, buttonClick) : call Buzzer(buzz)
    call Render()
    gosub WaitKeyUp
    do : gr.touch t, tx1, ty1 : until t
    tx1 / = scale_x : ty1 /= scale_y
    gr.modify collision, "x", tx1, "y", ty1
    gosub WaitKeyUp
     
    if gr_collision(collision, butBgColour) then call NewBackground()
    if gr_collision(collision, butZeroBest) then call ResetBest()
    if gr_collision(collision, butAbout)    then call ShowAbout(about)
    if gr_collision(collision, butHelp)     then call ShowHelp(allHelp)
    
    call PlaySound(sound, buttonClick) : call Buzzer(buzz)
    gr.hide allOptionsMenu
    gr.hide darkWindow
    gosub WaitKeyUp
    call Render()
    pausing = FALSE
return

!------------------------------------------------
! Wait for user to lift off key
!------------------------------------------------
WaitKeyUp:
    do
        gr.touch t, null, null
        !call ReduceScore()
    until ! t
return

!------------------------------------------------
! Turn sound on off
!------------------------------------------------
SoundOnOff:
    call PlaySound(sound, buttonClick) : call Buzzer(buzz)
    gosub WaitKeyUp
    if sound then
        sound = FALSE
        gr.show soundOff
    else
        sound = TRUE
        gr.hide soundOff
    endif
    call Render()
return

!------------------------------------------------
! Turn buzz on off
!------------------------------------------------
BuzzOnOff:
    call PlaySound(sound, buttonClick) : call Buzzer(buzz)
    gosub WaitKeyUp
    if buzz then
        buzz = FALSE
        gr.show buzzOff : gr.hide buzzOn
    else
        buzz = TRUE
        gr.hide buzzOff : gr.show buzzOn
    endif
    call Render()
return

!------------------------------------------------
! Save sound, Buzz and background colours
!------------------------------------------------
SaveData:
    file.exists ok, fn$
    if ! ok then file.mkdir dataPath$

    text.open w,hs,fn$
    text.writeln hs,int$(redB)
    text.writeln hs,int$(greenB)
    text.writeln hs,int$(blueB)
    text.writeln hs,int$(sound)
    text.writeln hs,int$(buzz)
    text.writeln hs,int$(best)
    text.close hs
return

!------------------------------------------------
! Load sound, Buzz and background colours
!------------------------------------------------
LoadData:
    file.exists ok, fn$
    if ok then
        text.open r,hs, fn$
        text.readln hs,redB$
        text.readln hs,greenB$
        text.readln hs,blueB$
        text.readln hs,sound$
        text.readln hs,buzz$
        text.readln hs,best$
        text.close hs
        
        redB = val(redB$)
        greenB = val(greenB$)
        blueB = val(blueB$)
        sound = val(sound$)
        buzz = val(buzz$)
        best = val(best$)
    endif
return

Functions:

!------------------------------------------------
! Timer interrupt to count down the score
!------------------------------------------------
fn.def ReduceScore()
    if gameOver | pausing then fn.rtn 0
    score --
    if score < 0 then score = 0
    gr.modify myScore, "text", "Score: " + int$(score)
    call Render()
fn.end


!------------------------------------------------
! User enters a number or cancel or try
!------------------------------------------------
fn.def UserEnter(userInput, x)
    call PlaySound(sound, buttonClick) : call Buzzer(buzz)
    num = x
    if num < 11 & userInput < 3 then
        if num = 10 then num = 0
        
       if NumNotUsed(userInput, userNum[], num) then 
            userInput ++ 
            gr.modify userTry[userInput], "text", int$(num) 
            userNum[userInput] = num
        endif
    endif
    
    if num = 11 then call ClearUserInput()
    
    if num = 12 & userInput = 3 then 
        if totalGuesses < 9 then call TryUserGuess() else call LastGuess()
        call ClearUserInput()
    endif
    call Render()
fn.end 

!------------------------------------------------
! No repeating numbers
!------------------------------------------------
fn.def NumNotUsed(userInput, userNum[], num)
    if userInput = 0 then fn.rtn 1
    if userInput = 1 & userNum[1] <> num then fn.rtn 1
    if userInput = 2 & userNum[1] <> num & userNum[2] <> num then fn.rtn 1
    
    gr.show myPopUp : gr.show txtPopUp
    call PlaySound(sound, beep) : now = time()
    call Render()
    do 
    until time() - now >= 2000
    gr.hide myPopUp : gr.hide txtPopUp
    fn.rtn 0
fn.end 

!------------------------------------------------
! Clear User input
!------------------------------------------------
fn.def ClearUserInput()
    userInput = 0 
    for reset = 1 to 3 
        gr.modify userTry[reset], "text", ""
        userNum[reset] = FALSE
    next
fn.end 

!------------------------------------------------
! See if the users guess is correct
! If not enter number in the hint box
!------------------------------------------------
    fn.def TryUserGuess()
        win = 0
        totalGuesses ++
        for w = 1 to 3
            if androidNum[w] = userNum[w] then win ++
        next
       
        if win = 3 then 
            gr.modify openShut, "paint", green
            call PlaySound(sound, dogsOut)
            gameOver = TRUE
            if score > best then best = score : gr.modify myBest, "text", "Best: " + int$(best)

            yesNo = ShowYesNo("Winner!", "Another Game")
            if yesNo = 1 then gosub SetUp else gosub SaveData : exit
        else
            for y = 1 to 3
                gr.modify guess[totalGuesses,y], "text", int$(userNum[y])
                gr.show guess[totalGuesses,y]
            next
            call GiveHint(androidNum[], userNum[])
        endif
    fn.end

!------------------------------------------------
! The tenth and last guess to see if users guess is correct
!------------------------------------------------
    fn.def LastGuess()
        win = 0
        totalGuesses ++
        for w = 1 to 3
            if androidNum[w] = userNum[w] then win ++
        next
       
        if win = 3 then 
            gr.modify openShut, "paint", green
            call PlaySound(sound, dogsOut)
            gameOver = TRUE
            if score > best then best = score : gr.modify myBest, "text", "Best: " + int$(best)
            yesNo = ShowYesNo("Winner!", "Another Game")
            if yesNo = 1 then gosub SetUp else gosub SaveData : exit
        else 
            num$ = int$(androidNum[1]) + " " + int$(androidNum[2]) + " " + int$(androidNum[3])
            gameOver = TRUE : score = 0 
            call PlaySound(sound, loser)
            gr.modify myScore, "text", "Score: " + int$(score)
            yesNo = ShowYesNo("Loser! Number is " + num$, "Another Game")
            if yesNo = 1 then gosub SetUp else gosub SaveData : exit
        endif 
    fn.end 
 
!------------------------------------------------
! Display hint to help user guess right number
!------------------------------------------------ 
    fn.def GiveHint(androidNum[], userNum[])
        array.load n$[], "One", "Two", "Three"
        right = 0 : wrong = 0 : hintLine1$ = "" : hintLine2$ = ""
        
        for x = 1 to 3
            if androidNum[x] = userNum[x] then right ++
        next
        if right = 1 then hintLine1$ = n$[right] + " number correct"
        if right = 2 then hintLine1$ = n$[right] + " numbers correct"
        
        for y = 1 to 3 
            for x = 1 to 3
                if x <> y & userNum[y] = androidNum[x] then wrong ++
            next
        next 
        
        if wrong = 1 then hintLine2$ = n$[wrong] + " in wrong place"
        if wrong > 1 then hintLine2$ = n$[wrong] + " in wrong place"
        
        if right = 0 & wrong = 0 then hintLine1$ = "Nothing correct"
        
        gr.modify txt1[totalGuesses], "text", hintLine1$
        gr.show txt1[totalGuesses]
        
        if wrong > 0 then 
            gr.modify txt2[totalGuesses], "text", hintLine2$
            gr.show txt2[totalGuesses]
        endif
        array.delete n$[]
    fn.end
    
!------------------------------------------------
! Device picks a number for user to guess
!------------------------------------------------
    fn.def GetAndroidNum(androidNum[])
        androidNum[1] = floor( 10 * rnd())
        do 
            androidNum[2] = floor( 10 * rnd())
        until androidNum[1] <> androidNum[2]
        
        do
            androidNum[3] = floor( 10 * rnd())
        until androidNum[3] <> androidNum[1] & androidNum[3] <> androidNum[2]        
    fn.end
    
!------------------------------------------------
! Draw a button with text
!------------------------------------------------
    fn.def DrawButton(bx, by, xw, yh, ptrText, butText$)
        gr.rect ptrButton, bx, by, xw, yh, 15, 15
        gr.modify ptrButton, "paint", darkGray
        ! White boarder around buttons
        gr.set.stroke 2 : gr.color 255, 255, 255, 255, 0
        gr.rect null, bx, by, xw, yh, 15, 15
        gr.set.stroke 1 : gr.color 255, 255, 255, 255, 1
        
        ! Text on Button
        diff = HYPOT(bx - xw, 0 - 0)
        gr.text.width txtLen, butText$
        tx = bx + (diff / 2) - (txtLen / 2)
        gr.text.draw ptrText, tx, by + 50, butText$
       
       fn.rtn ptrButton
    fn.end
    
!------------------------------------------------
! Draw the guess boxes
!------------------------------------------------
fn.def DrawGuessBoxes()
    gr.text.align 1 %: gr.text.bold 1
    txt = 1
    for y = (h / 2) + 100 to h - 100 step 160
        for x = 20 to 500 step 220
            ! Draw big box
            gr.color 255, 102, 102, 102, 1
            gr.rect null, x, y, x + 200, y + 140, 15, 15
            gr.color 255, 255, 255, 255, 0
            gr.rect null, x, y, x + 200, y + 140, 15, 15
            
            ! Draw small boxes
            box = 1
            for sx = x + 20 to x + 180 step 60 
                gr.color 255, 255, 255, 255, 1 : gr.text.size 40
                gr.rect null, sx, y + 20, sx + 40, y + 60, 5, 5
                gr.color 255, 0, 0, 0
                gr.text.draw guess[txt, box],sx + 8, y + 55, "0"
                box ++
            next
            gr.color 255, 255, 255, 255 : gr.text.size 19
            gr.text.draw txt1[txt], x + 10, y + 90, ""
            gr.text.draw txt2[txt], x + 10, y + 120, ""
            txt ++
       next
    next
    
    ! Draw top guess boxes 
    gr.text.size 40 : try = 1
     for x = 255 to 380 step 60
        gr.color 255, 255, 255, 255, 1
        gr.rect null, x, 60, x + 40, 100, 5, 5
        gr.color 255, 0, 0, 0
        gr.text.draw userTry[try], x + 8, 94, ""
        try ++
     next
     
     ! Draw circle for locked or open
     gr.color 255, 255, 0, 0, 1
     gr.circle openShut, (w / 2) - 2, 166, 10
    fn.rtn openShut
fn.end 

!------------------------------------------------
! Select a new background colour
!------------------------------------------------
    fn.def NewBackground()
        gr.show colourPick 
        call Render()
        call PlaySound(sound, buttonClick) : call Buzzer(buzz)
        gosub WaitKeyUp
        do 
            gr.touch t, tx, ty
            if t then
                x = tx : y = ty
                tx /= scale_x : ty /= scale_y
                gr.modify collision, "x", tx, "y", ty 
                if gr_collision(collision, colourPick) then
                    gr.get.pixel x, y, alpha, redB, greenB, blueB
                    gr.color 255, redB, greenB, blueB, , pBg
                endif
            endif
        until t
        gr.hide colourPick
    fn.end

!------------------------------------------------
! Display about box
!------------------------------------------------
    fn.def ShowAbout(about)
        call PlaySound(sound, buttonClick) : call Buzzer(buzz)
        gosub WaitKeyUp
        gr.show about
        call Render()
        do : gr.touch t, null, null : until t
        gr.hide about
    fn.end

!------------------------------------------------
! Display help for the timer
!------------------------------------------------ 
    fn.def ShowHelp(allHelp)
        call PlaySound(sound, buttonClick) : call Buzzer(buzz)
        gosub WaitKeyUp
        gr.show allHelp
        call Render()
        do : gr.touch t, null, null : until t
        gr.hide allHelp
    fn.end

!------------------------------------------------
! Display the yes no dialog box and wait for user input
!------------------------------------------------ 
    fn.def ShowYesNo(txt1$, txt2$)
        butPressed = FALSE
        gr.show darkWindow
        gr.show allYesNo
        gr.modify yesNoBox[2], "text", txt1$
        gr.modify yesNoBox[3], "text", txt2$
        gosub WaitKeyUp
        call Render()
        do 
            gr.touch t, tx, ty
            if t then
                tx /= scale_x : ty /= scale_y
                gr.modify collision, "x", tx, "y", ty
                if gr_collision(collision, yesNoBox[4]) then
                    gr.modify yesNoBox[4], "paint", darkGray : call Render()
                    gosub WaitKeyUp
                    gr.modify yesNoBox[4], "paint", gray : call Render()
                    butPressed = 1 % Yes
                endif
                
                if gr_collision(collision, yesNoBox[5]) then
                    gr.modify yesNoBox[5], "paint", darkGray : call Render()
                    gosub WaitKeyUp
                    gr.modify yesNoBox[5], "paint", gray : call Render()
                    butPressed = 2 % No
                endif
            endif
        until butPressed
        gr.hide darkWindow : gr.hide allYesNo
        
        fn.rtn butPressed
    fn.end 
    
!------------------------------------------------
! Reset the best score to zero
!------------------------------------------------ 
    fn.def ResetBest()
        yesNo = ShowYesNo("Reset Best to Zero", "")
        if yesNo = 1 then 
            best = 0
            gr.modify myBest, "text", "Best: " + int$(best)
        endif
    fn.end 
    
!------------------------------------------------
! Draw the options button and return it's pointer
!------------------------------------------------ 
    fn.def DrawOptionsButton()
        gr.color 255, 255, 255, 255, 0
        gr.rect menu, w - 50, 0, w, 50
        gr.color 255, 255, 255, 255, 1
        gr.set.stroke 4
        for y = 5 to 45 step 10
            gr.line null, w - 50, y, w, y 
        next
        gr.set.stroke 1
        fn.rtn menu
    fn.end
    
!------------------------------------------------
! Replace a character in a string
!------------------------------------------------    
    fn.def ReplaceChr$(string$, place, character$)
        string$ = left$(string$, place - 1) + character$ + mid$(string$, place + 1)
        fn.rtn string$
    fn.end

!------------------------------------------------
! Render if not in background
!------------------------------------------------
    fn.def Render()
        if ! background() then gr.render
    fn.end
    
!------------------------------------------------
! if sound then Play sound (ptr)
!------------------------------------------------
    fn.def PlaySound(sound, ptr)
        if sound then soundpool.play s,ptr,0.99,0.99,1,0,1
    fn.end
    
!------------------------------------------------
! if buzz then  vibrate
!------------------------------------------------
    fn.def Buzzer(buzz)
        if buzz then 
            array.load buzzGame[], 1, 100
            vibrate buzzGame[], -1
            array.delete buzzGame[]
        endif
    fn.end
  
!------------------------------------------------
! Load Sound files not found
!------------------------------------------------
    fn.def SoundLoadError(ptr, e$)
        if ptr = 0 then
            dialog.message "Sound file " + e$ + " not found", "App will end",OK,"OK" 
            end
        endif
    fn.end
    
!------------------------------------------------
! Load image file not found
!------------------------------------------------
    fn.def ImageLoadError(ptr, e$)
        if ptr =- 1 then
            dialog.message "Image file " + e$ + " not found", "App will end",OK,"OK" 
            end
        endif
    fn.end
    
!------------------------------------------------
! Double tap Back Key to end
!------------------------------------------------
    fn.def BackKey(backKeyHit)
        inTime = 3 % Three seconds 
        if backKeyHit = 3 then backKeyHit = 0
        
        if backKeyHit = 1 then 
            outTime = time() - backTick 
            outTime /= 1000 
            if outTime > inTime then backKeyHit = 0
        endif
        
        backKeyHit ++
        
        if backKeyHit = 1 then 
            backTick = time() 
            popup "Press again to Exit",, screenHeight / 2
        endif
       
        if backKeyHit = 2 then 
            backTock = time() - backTick
            backTock /= 1000
            if backTock <= inTime then gosub SaveData : exit
        endif
        fn.rtn backKeyHit
    fn.end
    
return