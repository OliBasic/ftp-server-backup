Rem Hungry Worm
Rem With RFO Basic!
Rem November 2015
Rem Version 2.00
Rem By Roy Shepherd

di_height = 672 % set to my Device
di_width = 1152

gr.open
gr.orientation 0 % Landscape
pause 1000
WakeLock 3 
gr.text.size 40

gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y

bundle.create global % 'global' bundle pointer = 1

gosub Functions % Let basic see the function before they are called
!-------------------------------------------------
! Global variables
!-------------------------------------------------
setg("Divice_W", di_width)
setg("Divice_H", di_height)
setg("Scale_X", scale_x) 
setg("Scale_Y", scale_y)

!-------------------------------------------------
! Start the game
!-------------------------------------------------
backFlag = 1
call Info()
backFlag = 0
call Initialise()
call RunWorm()

end
!-------------------------------------------------
! Back key for menu
!-------------------------------------------------
onBackKey: 
	if backFlag then exit else call Menu()
back.resume
end

!-------------------------------------------------
! Functions
!-------------------------------------------------
Functions: % Let basic see the function before they are called

!-------------------------------------------------
! Do this stuff once
!-------------------------------------------------
fn.def Initialise()
	di_width = getg("Divice_W") : di_height = getg("Divice_H")
	dataPath$="../../HungryWorm/data/"
	! global variables
	setg("Sound", 1) : setg("Buzz", 1)
	setg("Best", 0)  : setg("Score", 0)
	setg("BombNum", 0) : setg("FruitEaten", 0)
	setg("WormSpeed", 15)
	! set worm directions global variables
	setg("Worm_Left", 1) : setg("Worm_Right", 0)
	setg("Worm_Up", 0)   : setg("Worm_Down", 0)
	! Load and draw the background
	gr.bitmap.load bg,dataPath$ + "background.png"
	if bg < 0 then dialog.message "Back Ground Image, Not Found",,ok,"OK" : end
	gr.bitmap.scale bg, bg, di_width, di_height % scale background
	gr.bitmap.draw bg,bg, 0, 0 
	gr.bitmap.delete bg
	! Draw some text and set their pointers to global, so they can be modified when needed 
	gr.text.draw scoreNum, 10,40,"Score:0" 
	gr.text.align 3 : gr.text.draw bestNum, di_width - 10, 40, "Best:0" : gr.text.align 2
	gr.text.draw tapToStart, di_width / 2, di_height / 2, "Tap to Start" : gr.text.align 1
	setg("ScoreModNum", scoreNum)
	setg("BestModNum", bestNum)
	setg("TapToStart", tapToStart)
	
	! Load the fruits and store their pointers into a list; set the list pointer to global
	list.create N, fruit : setg("Fruit", fruit)	
	for x = 1 to 5
		gr.bitmap.load f,dataPath$ + "fruit" + int$(x) + ".png"
		gr.bitmap.draw f, f, 0, 0
		list.add fruit, f
	next
	
	! Load 20 bombs and store their pointers into a list; set the list pointer to global
	list.create N, bombs : setg("Bombs", bombs)	
	for x = 1 to 20
		gr.bitmap.load b,dataPath$ + "bomb.png"
		gr.bitmap.draw b, b, -150, -150
		list.add bombs, b
	next
	
	! Load explosion and store their pointers into a list; set the list pointer to global
	list.create N, explode : setg("Explode", explode)	
	for x = 1 to 6
		gr.bitmap.load e,dataPath$ + "blow" + int$(x) + ".png"
		gr.bitmap.scale e, e, 50, 50
		gr.bitmap.draw e, e, -150, -150
		list.add explode, e
	next
	
	call LoadBest() : call BestScore()
	call ReadHelp()
	call LoadSounds()
	gr.render
fn.end

!------------------------------------------------
! Load sounds and give each sound a global name
!------------------------------------------------
fn.def LoadSounds()
	dataPath$="../../HungryWorm/sounds/"
	Soundpool.open 6
	Soundpool.load  oops, dataPath$ + "oops.wav"          : setg("S_Oops", oops)
	Soundpool.load  bitten, dataPath$ + "bitten.wav"      : setg("S_Bitten", bitten)
	Soundpool.load  newBest, dataPath$ + "newBest.wav"    : setg("S_NewBest", newBest)
	Soundpool.load  eat, dataPath$ + "eat.wav"            : setg("S_Eat", eat)
	Soundpool.load  goodBye, dataPath$ + "goodbye.wav"    : setg("S_GoodBye", goodBye)
	Soundpool.load  explode, dataPath$ + "explode.wav"    : setg("S_Explode", explode)
	! Load background sound
	audio.load loopSound, dataPath$ + "wormLoop.mp3"      : setg("S_Loop", loopSound)
	if loopSound = 0 then e$ = GETERROR$() : ?e$ : end
fn.end

!------------------------------------------------
! Set this stuff at start of each new game
!------------------------------------------------
fn.def SetUpNewGame()
	setg("Score", 0) % Reset score
	setg("FruitEaten", 0) : setg("BombNum", 0)  % Reset fruit eaten and bombs
	setg("Worm_Left", 1) : setg("Worm_Right", 0) : setg("Worm_Up", 0) : setg("Worm_Down", 0) % Reset start off position
	setg("TouchDown", 0) : setg("TouchUp", 0)
	Call Score(0)
	! Reset fruit
	fruit = getg("Fruit")
	list.size fruit, fruit_Len
	for x = 1 to fruit_Len
		list.get fruit, x, fruitList
		gr.modify fruitList, "x", - 50, "y", - 50
	next
	! Reset bombs
	bombs = getg("Bombs")
	list.size bombs, bomb_Len
	for x = 1 to bomb_Len
		list.get bombs, x, bombList
		gr.modify bombList, "x", - 50, "y", - 50
	next
	! Reset explode
	explode = getg("Explode")
	list.size explode, explode_Len 
	for x = 1 to explode_Len 
		list.get explode, x, explodeList 
		gr.modify explodeList, "x", - 50, "y", - 50
	next
fn.end

!------------------------------------------------
! The worm runs the user changes direction
!------------------------------------------------
fn.def RunWorm()
	MAX_LEN = 100 : setg("Max_Len", MAX_LEN)
	dim worm[MAX_LEN] % worm
	dim wx[MAX_LEN]   % worm x position 
	dim wy[MAX_LEN]   % worm y position
	dim owx[MAX_LEN]  % old x worm
	dim owy[MAX_LEN]  % old y worm
	wormX = 500 : wormY = 250 % worm start position 
	s = 12 : worm_Len = 10
	setg("WormLen", worm_Len)
	gameIsDone = 0
	! Buid the worm. For the head of the worm, alpha is 255; for the rest of the worm alpha is 200
	! slightly transparent and the colour of the body changes as it gets longer
	for x = 1 to MAX_LEN
		if x = 1 then gr.color 255,255,x * 5, x * 10,1 else gr.color 200,255,x * 5, x * 10,1
		wx[x] = wormX + (x * 15) : wy[x] = wormY
		if x < = worm_Len
			gr.circle worm[x], wx[x], wy[x], s % Draw the worm and show screen
		else
			gr.circle worm[x], -25, -25, s % Draw the worm and hide it off screen
		endif
		if s = 12 then s = 8 else s = 12
	next
	call LoopSound()
	setg("TouchDown", 0) : setg("DownX", 0) : setg("DownY", 0)
	setg("TouchUp", 0)   : setg("UpX", 0)   : setg("UpY", 0)
	do
		call ShowFruit()
		call WaitForStartTap()
		do 
			speed = getg("WormSpeed")
			array.copy wx[], owx[] : array.copy wy[], owy[]
			! Get touch dowm
			if ! getg("TouchDown") then 
				gr.touch touchDown, tdx, tdy
				if touchDown then setg("TouchDown", touchDown) : setg("DownX", tdx) : setg("DownY", tdy)
			endif
			! if touch down then get touch up
			if getg("TouchDown") then 
				gr.touch touchUp, tux, tuy
				if touchUp then setg("TouchUp", touchUp) : setg("UpX", tux) : setg("UpY", tuy)
			endif
			! if touch down and touch up, carry on moving worm until
			! user has finished swiping screen then get direction
			if getg("TouchDown") & getg("TouchUp") 
				gr.touch touchOn, nx, ny
				if ! touchOn then call GetDirection()
			endif
			
			for x = 1 to worm_Len
				if getg("Worm_Left") then 
					wx[x] -= speed
				elseif getg("Worm_Right") 
					wx[x] += speed
				elseif getg("Worm_Up") 
					wy[x] -= speed
				elseif getg("Worm_Down") 
					wy[x] += speed
				endif
				
				array.copy owx[], wx[2] : array.copy owy[], wy[2]
				gr.modify worm[x], "x", wx[x], "y", wy[x] 
				
			next
			! if fruit is eaten. Increase worm length and score
			if EatFruit(worm[]) then 
				if worm_Len < MAX_LEN then worm_Len ++ : setg("WormLen", worm_Len)
			endif
			gr.render
			! Worm runs into a bomb; game over
			if HitBomb(worm[]) then gameIsDone = 1
			
			! Worm runs off the field; game over
			if OffField(worm[]) then gameIsDone = 2
			
			! Worm Bits own body; game over
			if WormBitsSelf(worm[]) then gameIsDone = 3
			
		until gameIsDone
		anotherGame = GameOverMessage(gameIsDone)
		if anotherGame = 1 then 
			call SetUpNewGame() : worm_Len = 10 : setg("WormLen", worm_Len)
			gameIsDone = 0 : wormX = 500 : wormY = 250 % worm start position 
			for x = 1 to MAX_LEN % Reset the worm and the worm's wx and wy positions
				wx[x] = wormX + (x * 15) : wy[x] = wormY
				if x < = worm_Len then
					gr.modify worm[x], "x", wx[x], "y", wy[x] 
				else 
					gr.modify worm[x], "x", - 50, "y", - 50 
				endif
			next
		endif
	until anotherGame = 2 
	call GoodBye()
fn.end

!------------------------------------------------
! User has swiped the screen so work out direction to turn worm
!------------------------------------------------
fn.def GetDirection()
	call Buzz()
	di_width = getg("Divice_W") : di_height = getg("Divice_H")
	
	touchDownX = int(getg("DownX")) : touchDownY = int(getg("DownY"))
	upX = int(getg("UpX"))          : upY = int(getg("UpY"))
	
	touchDownX /= getg("Scale_X") : touchDownY /= getg("Scale_Y")
	upX /= getg("Scale_X")        : upY /= getg("Scale_Y")
	
	x = Difference(touchDownX, upX)
	y = Difference(touchDowny, upY)
	
	if x > y then
		if upX < touchDownX then 
			setg("Worm_Left", 1) : setg("Worm_Right", 0) : setg("Worm_Up", 0) : setg("Worm_Down", 0)
		elseif upX > touchDownX
			setg("Worm_Left", 0) : setg("Worm_Right", 1) : setg("Worm_Up", 0) : setg("Worm_Down", 0)
		endif
	endif
	
	if y > x then
			if upY < touchDownY then
				setg("Worm_Left", 0) : setg("Worm_Right", 0) : setg("Worm_Up", 1) : setg("Worm_Down", 0)
			elseif upY > touchDownY
				setg("Worm_Left", 0) : setg("Worm_Right", 0) : setg("Worm_Up", 0) : setg("Worm_Down", 1)
		endif
	endif
	! Reset these global variables to zero ready for next screen swipe
	setg("TouchDown", 0) : setg("TouchUp", 0) 
fn.end

!------------------------------------------------
! Get the difference between two numbers
!------------------------------------------------
fn.def Difference(num1, num2) 
	if num1 < num2 then swap num1, num2 
	fn.rtn (num1 - num2)
fn.end

!------------------------------------------------
! Give reason why game is over and ask user if a new game is wanted 
!------------------------------------------------
fn.def GameOverMessage(gameIsDone)
	score = getg("Score")
	best = getg("Best")
	s$="" 
	if score > best then 
		s$="A New Best Score\n"
		call BestScore()
	endif 
	if gameIsDone = 1 then m$ = "Blown up with Bomb\n" +s$ + "Another Game"
	if gameIsDone = 2 then m$ = "Gone off Field\n" +s$ + "Another Game"
	if gameIsDone = 3 then m$ = "Bitten into Own Body\n" +s$ + "Another Game"
	do : dialog.message"Game Over",m$,yn,"Yes","No" : until yn > 0
	fn.rtn yn
fn.end

!------------------------------------------------
! Test for a collision with fruit; if so eat fruit
!------------------------------------------------
fn.def EatFruit(worm[])
	fruit = getg("Fruit") : flag = 0
	fruitEaten = getg("FruitEaten")
	List.size fruit, fruit_Len
	for x = 1 to fruit_Len
		list.get fruit, x, fruitList
		if gr_collision(worm[1], fruitList) then % dialog.message"Fruit eaten " + int$(fruitList),,ok,"OK" : f_n.break
			gr.hide fruitList
			fruitEaten++ : setg("FruitEaten", fruitEaten)
			call Score(10): flag = 1
			call PlaySound(getg("S_Eat"))
		endif
		if fruitEaten = fruit_Len & flag then 
			setg("FruitEaten", 0) : call Score(100) : call ShowFruit() : call ShowBomb()
		endif
		if flag then f_n.break
	next
	fn.rtn flag
fn.end

!------------------------------------------------
! Test for a collision with bomb; if so explode bomb
!------------------------------------------------
fn.def HitBomb(worm[])
	bombs = getg("Bombs") : flag = 0 
	List.size bombs, bomb_Len
	for x = 1 to bomb_Len 
		list.get bombs, x, thisBomb
		if gr_collision(worm[1], thisBomb) then 
			call PlaySound(getg("S_Explode"))
			explode = getg("Explode") 
			gr.get.position thisBomb, x, y
			List.size explode, explode_Len
			for e = 1 to explode_Len
				list.get explode, e, blow
				gr.modify blow, "x", x, "y", y
				gr.render : pause 10
			next
			flag = 1 : f_n.break
		endif
	next
	fn.rtn flag
fn.end

!------------------------------------------------
! Test for worm off the screen
!------------------------------------------------
fn.def OffField(worm[])
	di_width = getg("Divice_W") : di_height = getg("Divice_H") : flag = 0
	gr.get.position worm[1], x, y
	if x < 0 | x > di_width | y < 0 | y > di_height then flag = 1 : call PlaySound(getg("S_Oops"))
	fn.rtn flag
fn.end

!------------------------------------------------
! Test for worm biting its self
!------------------------------------------------
fn.def WormBitsSelf(worm[])
	gr.get.position worm[1], x1, y1 : flag = 0
	for x = 2 to getg("WormLen")
		gr.get.position worm[x], x2, y2
		if x1 = x2 & y1 = y2 then flag = 1 : call PlaySound(getg("S_Bitten")) : f_n.break
	next
	fn.rtn flag
fn.end

!------------------------------------------------
! Display the fruits.
! Ensure that they are not over lapping each other.
! Ensure that they are not over lapping a bomb.
!------------------------------------------------
fn.def ShowFruit()
	di_width = getg("Divice_W") : di_height = getg("Divice_H")
	fruit = getg("Fruit")
	bombs = getg("Bombs")
	List.size bombs, bomb_Len
	List.size fruit, fruit_Len
	do : flag = 0
		for x = 1 to fruit_Len
			list.get fruit, x, fruitList
			gr.show fruitList
			gr.modify fruitList, "x", floor(rnd() * (di_width - 100) + 50), "y", floor(rnd() * (di_height - 100) + 50)
		next
		! test to see if one fruit is over another fruit
		for y = 1 to fruit_Len
			list.get fruit, y, fruitTest
			for x = 1 to fruit_Len
				list.get fruit, x, fruitList
				if y <> x then if gr_collision(fruitTest, fruitList) then flag = 1 : f_n.break
			next
			if flag then f_n.break
		next
		if ! flag then
			! test to see in fruit is over a bomb
			for y = 1 to bomb_Len
				list.get bombs, y, bombTest
				for x = 1 to fruit_Len
					list.get fruit, x, fruitList
					if gr_collision(bombTest, fruitList) then flag = 1 : f_n.break
				next
				if flag then f_n.break
			next
		endif
	until ! flag
fn.end

!------------------------------------------------
! If less then 20 bombs on screen display a bombs
! Ensure that they are not over lapping each other.
! Ensure that they are not over lapping the fruits
!------------------------------------------------
fn.def ShowBomb()
	di_width = getg("Divice_W") : di_height = getg("Divice_H")
	fruit = getg("Fruit")
	List.size fruit, fruit_Len
	bombs = getg("Bombs")
	bombNum = getg("BombNum")
	bombNum++ : setg("BombNum", bombNum)
	List.size bombs, bomb_Len
	if bombNum < = bomb_Len then
		list.get bombs, bombNum, bombList
		do : flag = 0
			gr.modify bombList, "x", floor(rnd() * (di_width - 100) + 50), "y", floor(rnd() * (di_height - 100) + 50)
			! test to see if the bomb is over a fruit
			for y = 1 to fruit_Len 
				list.get fruit, y, fruitTest
				if gr_collision(bombList, fruitTest) then flag = 1 : f_n.break
			next
			! test to see if the bomb is over another bomb
			if ! flag then 
				for y = 1 to bombNum
					list.get bombs, y, bombTest
					if bombList <> bombTest then if gr_collision(bombList, bombTest) then flag = 1 : f_n.break
					if flag then f_n.break
				next
			endif
		until ! flag
	endif
fn.end

!------------------------------------------------
! if sound is on. Play sound (ptr)
!------------------------------------------------
fn.def PlaySound(ptr)
	if getg("Sound") then Soundpool.play s,ptr,0.99,0.99,1,0,1
fn.end

!-------------------------------------------------
! Record and display the best score
!------------------------------------------------- 
fn.def BestScore()
	flag = 0
	score = getg("Score")
	best = getg("Best")
	if score > best then best = score : flag = 1 : call PlaySound(getg("S_NewBest"))
	if flag	then setg("Best", best)
	gr.modify getg("BestModNum"), "text", "Best: "+int$(getg("Best"))
	gr.render
	fn.rtn flag
fn.end

!-------------------------------------------------
! Record and display the score
!-------------------------------------------------
fn.def Score(t)
	s = getg("Score")
	s += getg("WormSpeed")
	setg("Score", s) 
	gr.modify getg("ScoreModNum"), "text", "Score: "+int$(getg("Score"))
fn.end

!-------------------------------------------------
! if buzz is on then vibrate when user taps screen
!-------------------------------------------------
fn.def Buzz()
	if getg("Buzz") then 
		array.load buzzGame[], 1, 100
		vibrate buzzGame[], -1
		array.delete buzzGame[]
	endif
fn.end

!-------------------------------------------------
! Loads best score, sound and vibration
!-------------------------------------------------
fn.def LoadBest()
	dataPath$="../../HungryWorm/data/"
	file.exists BestPresent,dataPath$+"WormBest.txt"
	if BestPresent then
		text.open r,hs,dataPath$+"WormBest.txt"
			text.readln hs,best$
			best = val(best$)
			
			text.readln hs,best$
			sound = val(best$)
			
			text.readln hs,best$
			vibration = val(best$)
			
			text.readln hs,speed$
			speed = val(speed$)
			
			setg("Best", best)
			setg("Sound", sound)
			setg("Buzz", vibration)
			setg("WormSpeed", speed)
		text.close hs
	endif
fn.end

!-------------------------------------------------
! Saves best score, sound and vibration
!-------------------------------------------------
fn.def SaveBest()
	dataPath$="../../HungryWorm/data/"
	best = getg("Best")
	sound = getg("Sound")
	vibration = getg("Buzz")
	speed = getg("WormSpeed")
	file.exists BestPresent,dataPath$+"WormBest.txt"
	if ! BestPresent then file.mkdir dataPath$
	text.open w,hs,dataPath$+"WormBest.txt"
		text.writeln hs,int$(best)
		text.writeln hs,int$(sound)
		text.writeln hs,int$(vibration)
		text.writeln hs,int$(speed)
	text.close hs
fn.end

!-------------------------------------------------
! Wait for user to tap screen
!-------------------------------------------------
fn.def WaitForStartTap()
	gr.show getg("TapToStart")
	gr.modify getg("TapToStart") ,"text", "Tap to Start" : gr.render
	do : gr.touch t, nx, ny : until t
	do : gr.touch t, nx, ny : until ! t
	gr.hide getg("TapToStart")
	gr.render : pause 1000
fn.end

!-------------------------------------------------
! Saves best score, sound, vibration and worm speed then leave the game
!-------------------------------------------------
fn.def GoodBye()
	audio.stop
	call SaveBest()
	call PlaySound(getg("S_GoodBye"))
	gr.color 50, 0, 0, 0, 1
	gr.rect fade, 0, 0, getg("Divice_W"), getg("Divice_H")
	for a = 60 to 250 step 10 
		gr.modify fade, "alpha", a
		pause 50
		gr.render
	next
	gr.color 0,255,255,255,1 
	gr.text.size 200 
	gr.text.draw bye, 100, 400, "Good Bye" 
	for a = 1 to 255 step 5
		gr.modify bye, "alpha", a
		pause 50
		gr.render
	next
	pause 1000
	soundpool.release
	exit
fn.end

!-------------------------------------------------
! Read the help from read.data and make it global. 
!-------------------------------------------------
fn.def ReadHelp()
	read.from 1
	while h$ <> "STOP"
		read.next h$ 
		if h$ <> "STOP" then help$ = help$ + h$
	repeat
	setg$("Help", help$)
fn.end

!-------------------------------------------------
! If sound is on then play the background sound in a loop
!-------------------------------------------------
fn.def LoopSound()
	if getg("Sound") then 
		audio.play getg("S_Loop") 
		audio.loop
	else 
		audio.stop
	endif
fn.end

!-------------------------------------------------
! Back key tapped. Call the menu
!-------------------------------------------------
fn.def Menu()
	sound = getg("Sound")
	vibration = getg("Buzz")
	if sound then sound$="Sound On" else sound$="Sound Off"
	if vibration then vibration$="Vibration On" else vibration$="Vibration Off"
	
	array.load menu$[],	sound$,~
						vibration$,~
						"Reset Best Score",~
						"Worm Speed",~
						"About",~
						"Help",~
						"Back to Game",~
						"Exit"
	dialog.select menuSelected, menu$[], "Main"
	
	sw.begin menuSelected
		sw.case 1
			if sound then sound = 0 else sound = 1	
			setg("Sound", sound) : call LoopSound()
		sw.break
			
		sw.case 2
			if vibration then vibration = 0 else vibration = 1
			setg("Buzz", vibration)
		Sw.break
		
		sw.case 3
			do : dialog.Message"Reset Best Score to zero?",,yn,"Yes","No" : until yn > 0
			if yn = 1 then 
				setg("Best", 0)
				call SaveBest()
				call BestScore()
			endif
		sw.break
		
		sw.case 4
			s = getg("WormSpeed")
			if s = 10 then 
				s$="Slow" 
			elseif speed = 15 
				s$="Steady" 
			else 
				s$ = "Fast"
			endif
			
			do : dialog.Message"Worm Speed is: " + s$,,speed,"Slow","Steady","Fast" : until speed > 0
			
			if speed = 1 then
				setg("WormSpeed", 10) 
			elseif speed = 2
				setg("WormSpeed", 15) 
			else 
				setg("WormSpeed", 20) 
			endif
		sw.break
		
		sw.case 5
			Dialog.Message " About:\n\n",~
			"   Hungry Worm for Android\n\n" + ~
			"   With: RFO BASIC!\n\n" + ~
			"   November 2015\n\n" + ~
			"   Version: 1.00\n\n" + ~ 
			"   Author: Roy Shepherd\n\n", ~
			ok, "OK"
		sw.break
		
		sw.case 6
			help$ = getg$("Help") 
			dialog.Message " Hungry Worm Help:",help$,ok,"OK"
		sw.break
		
		sw.case 7
			! Back to game
		sw.break
			
		sw.case 8
			do : dialog.Message"Exit Game?",,yn,"Yes","No" : until yn > 0
			if yn=1 then call GoodBye()
		sw.break
	sw.end
fn.end

!-------------------------------------------------
! When game first starts, show the user how to control the worm
!-------------------------------------------------
fn.def Info()
	di_width = getg("Divice_W") : di_height = getg("Divice_H")
	gr.color 255, 0, 0, 0 ,1
	gr.text.size 30
	gr.set.stroke 1 : gr.text.align 2
	gr.text.draw null, di_width / 2, 100, " To Control the Worm"
	gr.text.draw null, di_width / 2, 200, " If you want the worm to turn right; slide your finger from left to right."
	gr.text.draw null, di_width / 2, 250, " If you want the worm to turn left; slide your finger from right to left."
	gr.text.draw null, di_width / 2, 300, " If you want the worm to turn up; slide your finger from bottom to top."
	gr.text.draw null, di_width / 2, 350, " If you want the worm to turn down; slide your finger from top to bottom."
	gr.text.draw null, di_width / 2, 400, " The worm turns when you take your finger off the screen,"
	gr.text.draw null, di_width / 2, 450, " so you can keep your finger on the screen until the worm reached the point"
	gr.text.draw null, di_width / 2, 500, " where you want it to turn."
	gr.text.draw null, di_width / 2, 550, " Try not to slide your finger diagonally as this can confuse the worm."
	gr.text.draw null, di_width / 2, 650, " Tap to Start"
	gr.text.align 1 : gr.text.size 20
	gr.render
	do : gr.touch t, nx, ny : pause 1 : until t
	do : gr.touch t, nx, ny : pause 1 : until ! t
	gr.text.size 40 : gr.cls : gr.color 255,255,255,255,1
fn.end

!----------------------------------------------------
! These functions give simulate Global variables
! Thanks to Mougino for the functions
!----------------------------------------------------
FN.DEF SETG(var$, n) % Set global-numeric-variable
  BUNDLE.PUT 1, var$, STR$(n)
FN.END

FN.DEF SETG$(var$, s$) % Set string-numeric-variable
  BUNDLE.PUT 1, var$, s$
FN.END

FN.DEF GETG(var$) % Get global-numeric-variable
  BUNDLE.GET 1, var$, s$
  FN.RTN VAL(s$)
FN.END

FN.DEF GETG$(var$) % Get string-numeric-variable
  BUNDLE.GET 1, var$, s$
  FN.RTN s$
FN.END

return

!------------------------------------------------
! Help data for the Game.
!------------------------------------------------
read.data ""
read.data " Playing Hungry Worm:\n\n"
read.data " The player controls the worm in a field. As the worm moves forward,"
read.data " it leaves a trail behind, resembling a moving worm."
read.data "\n\n"
read.data " The player loses when the worm runs off the screen , runs into his own body,"
read.data " or runs into a bomb."
read.data "\n\n"
read.data " The player attempts to eat fruits by running into them with the head of the worm."
read.data " Each fruit eaten makes the worm longer, so maneuvering is progressively more difficult." 
read.data "\n\n"
read.data " Points:\n"
read.data " To start off there are five fruits in the field. The player gets points depending on the worm's speed."
read.data " Worm running Slow gets the player 10 points, running Steady 15 points and running Fast 20 points."
read.data " Do not miss the banana, its small and hard to see in the long grass."
read.data "\n\n"
read.data " Once all five fruits are eaten the player gets a hundred bonus points and another five fruits"  
read.data " Each time the player eats five fruits a bomb appears; these bombs must be avoided."  
read.data "\n\n"
read.data " Controls:\n"
read.data " If you want the worm to turn right; slide your finger from left to right."
read.data " If you want the worm to turn left; slide your finger from right to left."
read.data " If you want the worm to turn up; slide your finger from bottom to top."
read.data " If you want the worm to turn down; slide your finger from top to bottom."
read.data " The worm turns when you take your finger off the screen,"
read.data " so you can keep your finger on the screen until the worm reached the point where you want it to turn."
read.data " Try not to slide your finger diagonally as this can confuse the worm."
read.data "\n\n"
read.data "Menu:\n"
read.data "     Tap the Back Key to pause and access the Menu.\n"
read.data "     From the menu, you can:\n"
read.data "     Turn Sound On and Off.\n"
read.data "     Turn Vibration On and Off.\n"
read.data "     Reset the Best Score to Zero.\n"
read.data "     Set the Worm's Speed, Slow, Steady and Fast.\n"
read.data "     Read About and Help.\n"
read.data "     Go back to the game.\n"
read.data "     Exit the game.\n"
read.data "\n"
read.data "	------------------------------------------------------------"
read.data "STOP"
return
