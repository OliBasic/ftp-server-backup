Dim Tile[54,12]
Dim TileColor[54]
Dim Zpoly[54,2] 
Dim List[54]
Dim pt[108]
Dim HitX[4]
Dim HitY[4]
Dim Tracks[9,12]
Dim Faces[9,8]

!debug.on
!debug.echo.on
Cls
Print ""
Print "RUBIX ICE-CUBE 3D by TechnoRobbo"
Print "by TechnoRobbo"
Print ""
Print "Initializing 3D Graphics......"
Pause 1000

gr.open 255, 0, 0, 0
gr.Orientation 0
gr.Screen mx, my
gr.Color 255, 0, 0, 0, 0

!z axis offset
zOff = 20

!focal length
zp = 900 

mxt=mx/2
myt=my/2
mytxt=my/10
mxtxt=mx/4
PI = Atan(1) * 4
RotLR = -0.25
RotUD = -0.25
RotHPI = RotLR * PI
RotVPI = RotUD * PI
COSRPI = Cos(RotHPI)
SINRPI = Sin(RotHPI)
COSRPP = Cos(RotVPI)
SINRPP = Sin(RotVPI)
LockUp=0
RenderCall=0

tempx =0 
tempy=0
tempz=0
tmpx =0
tmpy =0
tmpz=0
touchflag =0
touchx=0
touchy=0
LowX=0
LowY=0
HiX=0
HiY=0
FromTile=0
ToTile=0
Xtrack=0

!-----------------------
init:


!load tiles
For i = 1 To 6
    For j = 1 To 9
        v = (i * 9-9)+ j
        gosub loadcube
        For k = 1 to 12
            Tile[v, k] = tmparray[k]
        Next
        TileColor[v]=i
        array.delete tmparray[]
        list.create n,List[v]
    Next
Next

!load tracks
for v=1 to 9
	gosub LoadTracks
   For k = 1 to 12
       Tracks[v, k] = tmparray[k]
   Next
   array.delete tmparray[]
next

!load faces
for v=1 to 9
	gosub LoadFaces
   For k = 1 to 8
       Faces[v, k] = tmparray[k]
   Next
   array.delete tmparray[]
next


gosub CreateMenu

gosub ShowMenu


popup "Rendering",0,0,0
RenderCall=1

!---------------------------
GameLoop:

do
	!---------------------------
	!3D Render---------------
	if RenderCall then
		For k = 1 To 54
		   Zpoly[k, 1] = 0
		   gosub getThreeD 			 
	 	Next
		
		
		gosub ShellSort
	
		gr.Cls
		For i = 54 To  1 step -1
			tmpval = Zpoly[i, 2]
			gosub SetColor
			gr.poly pt[i], List[tmpval]
			gr.modify pt[i],"alpha",220
			gr.color 255,0,0, tmpval ,0
			gr.poly pt[i+54], List[tmpval]

		Next
		gr.text.align 1
		gr.text.size mytxt
		
		if LockUp=0 then
	  		gr.color 255,0,0,255,1
	  		gr.bitmap.draw Lock,bmpLockOff,0,0
	  	else
	  		gr.bitmap.draw Lock,bmpLockOn,0,0
	  		gr.color 255,255,0,0,1
	  	endif
	  	gr.bitmap.draw MenuIcon,bmpIconMenu,10,my-52
		
		
		gr.render
		RenderCall=0
	endif
	!3D render complete
	!---------------
	!---------------		
	!Check for screen touch
	
   gr.bounded.touch touched,0,0,mxtxt,mytxt*2
	gr.bounded.touch touched2,0,my-100,50,my
	gr.touch touched3,x,y
	if touched then
		do
			gr.touch touched,x,y
		until touched =0
		
		LockUp=abs(Lockup-1)
		if LockUp=0 then
	  		gr.modify Lock,"bitmap",bmpLockOff
	  	else
	  		gr.modify Lock,"bitmap",bmpLockOn
	  	endif

	  	gr.render
	  	
	elseif touched2 then
		
		gosub ShowMenu
		
	elseif touched3=1 & LockUp=0 then
		
		gosub getTouch
		
	elseif touched3=1 & LockUp=1 then
		
		gosub Rotate
	endif
	
until 0


OnError:
gr.Close
End

!----------------------------------------------
!----------------------------------------------
!----------------------------------------------
!Subroutines 
!----------------------------------------------
!----------------------------------------------
Rotate:
	!--------------------------------
	!rotate tiles

  gr.touch touched,x,y
	if touched then
		
	   touchx = X
	   touchy = Y	
	
		do
		 gr.touch touched,x,y
		until touched =0
		
		FromTile=0
		ToTile=0
		i=1
		Do
					
			v=Zpoly[i, 2]
			List.ToArray List[v],LArr[]		
			HitX[1]=Larr[1]
			HitY[1]=Larr[2]
			HitX[2]=Larr[3]
			HitY[2]=Larr[4]
			HitX[3]=Larr[5]
			HitY[3]=Larr[6]
			HitX[4]=Larr[7]
			HitY[4]=Larr[8]	
			array.delete Larr[]
			
			array.min LowX,HitX[]	
			array.max HiX,HitX[]	
			array.min LowY,HitY[]	
			array.max HiY,HitY[]
						
			if FromTile=0 then
				if touchx < LowX | touchx > HiX | touchy < LowY | touchy > HiY then
					FromTile=0 
				else	
					FromTile=v 
				endif
			endif
			if ToTile=0 then
				if x < LowX | x > HiX | y < LowY | y > HiY then
					ToTile=0 
				else	
					ToTile=v 
				endif
			endif
			i=i+1
			
		Until (i=55) | (FromTile>0 & ToTile>0)
		
		!valid find?
		if FromTile=0 | ToTile=0 then goto leave1
		if FromTile=ToTile then goto leave1
		
		!check tracks
		i=1
		xtrack=0
		do
			first=0 
			second=0
			for v = 1 to 12
				if Tracks[i,v]=FromTile then first=v
				if Tracks[i,v]=ToTile then second=v
				if first>0 & second>0 then xtrack=i
			next		
			i=i+1
		Until (i=10) | xtrack>0
		
		if xtrack=0 then goto leave1
		
		popup "Spinning",0,0,0
		!spin cube			
		array.copy TileColor[],tmparray[]
		
		for i=1 to 12
			x=tracks[xtrack,i]
			y=tracks[xtrack,mod(i+2,12)+1]

			if first>second & abs(first-second)<6 then			
				TileColor[x]=tmparray[y]
			elseif first<second & abs(first-second)<6 then
				TileColor[y]=tmparray[x]
			elseif first<second then			
				TileColor[x]=tmparray[y]
			else
				TileColor[y]=tmparray[x]
			endif
		next
		
		for i=1 to 8
			x=Faces[xtrack,i]
			y=Faces[xtrack,mod(i+1,8)+1]
			if x=0 then
				!do nothing
			elseif first>second & abs(first-second)<6 then			
				TileColor[x]=tmparray[y]
			elseif first<second & abs(first-second)<6 then
				TileColor[y]=tmparray[x]
			elseif first<second then			
				TileColor[x]=tmparray[y]
			else
				TileColor[y]=tmparray[x]
			endif
		next


		array.delete tmparray[]
		
		RenderCall=1
	endif
	
	leave1:
return
!---------------------------------
getTouch:
	!-----------------------------
	!touch interaction
	
	gr.touch touched,x,y
	if touched then
		
	   touchx = X
	   touchy = Y	
	
		do
		 gr.touch touched,x,y
		until touched =0
		
		popup "Rendering",0,0,0
			    
	   RotLR = RotLR + (X - touchx) * 0.5 / mx
	   touchx = X
	
	   RotUD = RotUD - (Y - touchy) * 0.5 / my
	   touchy = Y
	
	   If RotLR > 1 Then RotLR = RotLR - 2
	   If RotLR < -1 Then RotLR = RotLR + 2
	   If RotUD > 0.25 Then RotUD = 0.25
	   If RotUD < -0.25 Then RotUD = -0.25
	   
	   RotHPI = RotLR * PI
	   RotVPI = RotUD * PI
		COSRPI = Cos(RotHPI)
		SINRPI = Sin(RotHPI)
		COSRPP = Cos(RotVPI)
		SINRPP = Sin(RotVPI)
		RenderCall=1
	endif
	
return

!--------------------
SetColor:
		

	sw.begin TileColor[tmpval]
	
		sw.case 1
				gr.color 255,255,0,0,1
			sw.break
		sw.case 2
				gr.color 255,0,255,0,1		
			sw.break
		sw.case 3
				gr.color 255,0,0,255,1		
			sw.break
		sw.case 4
				gr.color 255,255,255,0,1		
			sw.break
		sw.case 5
				gr.color 255,255,0,255,1		
			sw.break
		sw.case 6
				gr.color 255,0,255,255,1		
			sw.break															
	sw.end
	
return

!---------------------------------

getThreeD:
	ix=1
	list.clear List[k]
	for i = 1 to 12 step 3
	    tmpx = COSRPI * Tile[k, i] - SINRPI * Tile[k, 2+i]
	    tmpz = COSRPI * Tile[k, 2+i] + SINRPI * Tile[k, i]
	    tmpy = COSRPP * Tile[k, 1+i] - SINRPP * tmpz
	    tmpz = COSRPP * tmpz + SINRPP * Tile[k,1+i] + zOff
	    Zpoly[k, 1] = Zpoly[k, 1] + tmpz * 0.25
	    list.add List[k], mxt + tmpx * zp / tmpz
	    list.add List[k], myt - tmpy * zp / tmpz
   next
return

!----------------------------------

ShellSort:

	For i = 1 To 54
	    Zpoly[i, 2] = i
	Next
	gap = 40
	Do
	    gap = floor(gap / 3)
	    For i = (gap + 1) To 54
	        CurPos = i
	        tempval = Zpoly[i, 1]
	        tempval2 = Zpoly[i, 2]
	        
	        skip=0
	        
	        do 
	        		if (Zpoly[CurPos - gap, 1] > tempval) then
		            Zpoly[CurPos, 1] = Zpoly[CurPos - gap, 1]
		            Zpoly[CurPos, 2] = Zpoly[CurPos - gap, 2]
		            CurPos = CurPos - gap
		         else
		         	skip=1
	        		endif
		        If (CurPos - gap) < 1 Then skip=1
		        
		     until skip=1

	        Zpoly[CurPos, 1] = tempval
	        Zpoly[CurPos, 2] = tempval2
	    Next
	Until gap = 1

return
!-------------------------------
!DATA LOADER
!------------------------------
loadcube:
    sw.begin v
        sw.case 1
                array.load tmparray[],-3, -3, -3, -1, -3, -3, -1, -1, -3, -3, -1, -3
        			 sw.break
        sw.case 2
                array.load tmparray[],-1, -3, -3, 1, -3, -3, 1, -1, -3, -1, -1, -3
        			 sw.break
        sw.case 3
                array.load tmparray[],1, -3, -3, 3, -3, -3, 3, -1, -3, 1, -1, -3
        			 sw.break
        sw.case 4
                array.load tmparray[],-3, -1, -3, -1, -1, -3, -1, 1, -3, -3, 1, -3
        			 sw.break
        sw.case 5
                array.load tmparray[],-1, -1, -3, 1, -1, -3, 1, 1, -3, -1, 1, -3
                sw.break
        sw.case 6
                array.load tmparray[],1, -1, -3, 3, -1, -3, 3, 1, -3, 1, 1, -3
                sw.break
        sw.case 7
                array.load tmparray[],-3, 1, -3, -1, 1, -3, -1, 3, -3, -3, 3, -3
                sw.break
        sw.case 8
                array.load tmparray[],-1, 1, -3, 1, 1, -3, 1, 3, -3, -1, 3, -3
                sw.break
        sw.case 9
                array.load tmparray[],1, 1, -3, 3, 1, -3, 3, 3, -3, 1, 3, -3
        			 sw.break
        sw.case 10
                array.load tmparray[],-3, -3, 3, -1, -3, 3, -1, -3, 1, -3, -3, 1

        			 sw.break
        sw.case 11
                array.load tmparray[],-1, -3, 3, 1, -3, 3, 1, -3, 1, -1, -3, 1
        			 sw.break
        sw.case 12
                array.load tmparray[],1, -3, 3, 3, -3, 3, 3, -3, 1, 1, -3, 1
        			 sw.break
        sw.case 13
                array.load tmparray[],-3, -3, 1, -1, -3, 1, -1, -3, -1, -3, -3, -1
        			 sw.break
        sw.case 14
                array.load tmparray[],-1, -3, 1, 1, -3, 1, 1, -3, -1, -1, -3, -1
        			 sw.break
        sw.case 15
                array.load tmparray[],1, -3, 1, 3, -3, 1, 3, -3, -1, 1, -3, -1
        			 sw.break
        sw.case 16
                array.load tmparray[],-3, -3, -1, -1, -3, -1, -1, -3, -3, -3, -3, -3
        			 sw.break
        sw.case 17
                array.load tmparray[],-1, -3, -1, 1, -3, -1, 1, -3, -3, -1, -3, -3
        			 sw.break
        sw.case 18
                array.load tmparray[],1, -3, -1, 3, -3, -1, 3, -3, -3, 1, -3, -3
        			 sw.break
        sw.case 19
                array.load tmparray[],-3, 3, 3, -1, 3, 3, -1, 1, 3, -3, 1, 3
        			 sw.break
        sw.case 20
                array.load tmparray[],-1, 3, 3, 1, 3, 3, 1, 1, 3, -1, 1, 3
        			 sw.break
        sw.case 21
                array.load tmparray[],1, 3, 3, 3, 3, 3, 3, 1, 3, 1, 1, 3
        			 sw.break
        sw.case 22
                array.load tmparray[],-3, 1, 3, -1, 1, 3, -1, -1, 3, -3, -1, 3
        			 sw.break
        sw.case 23
                array.load tmparray[],-1, 1, 3, 1, 1, 3, 1, -1, 3, -1, -1, 3
        			 sw.break
        sw.case 24
                array.load tmparray[],1, 1, 3, 3, 1, 3, 3, -1, 3, 1, -1, 3
        			 sw.break
        sw.case 25
                array.load tmparray[],-3, -1, 3, -1, -1, 3, -1, -3, 3, -3, -3, 3
        			 sw.break
        sw.case 26
                array.load tmparray[],-1, -1, 3, 1, -1, 3, 1, -3, 3, -1, -3, 3
        			 sw.break
        sw.case 27
                array.load tmparray[],1, -1, 3, 3, -1, 3, 3, -3, 3, 1, -3, 3
        			 sw.break
        sw.case 28
                array.load tmparray[],-3, 3, -3, -1, 3, -3, -1, 3, -1, -3, 3, -1
        			 sw.break
        sw.case 29
                array.load tmparray[],-1, 3, -3, 1, 3, -3, 1, 3, -1, -1, 3, -1
        			 sw.break
        sw.case 30
                array.load tmparray[],1, 3, -3, 3, 3, -3, 3, 3, -1, 1, 3, -1
        			 sw.break
        sw.case 31
                array.load tmparray[],-3, 3, -1, -1, 3, -1, -1, 3, 1, -3, 3, 1
        			 sw.break
        sw.case 32
                array.load tmparray[],-1, 3, -1, 1, 3, -1, 1, 3, 1, -1, 3, 1
        			 sw.break
        sw.case 33
                array.load tmparray[],1, 3, -1, 3, 3, -1, 3, 3, 1, 1, 3, 1
        			 sw.break
        sw.case 34
                array.load tmparray[],-3, 3, 1, -1, 3, 1, -1, 3, 3, -3, 3, 3
        			 sw.break
        sw.case 35
                array.load tmparray[],-1, 3, 1, 1, 3, 1, 1, 3, 3, -1, 3, 3
        			 sw.break
        sw.case 36
                array.load tmparray[],1, 3, 1, 3, 3, 1, 3, 3, 3, 1, 3, 3
        			 sw.break
        sw.case 37
                array.load tmparray[],3, -3, -3, 3, -3, -1, 3, -1, -1, 3, -1, -3 
        			 sw.break
        sw.case 38
                array.load tmparray[],3, -3, -1, 3, -3, 1, 3, -1, 1, 3, -1, -1
        			 sw.break
        sw.case 39
                array.load tmparray[],3, -3, 1, 3, -3, 3, 3, -1, 3, 3, -1, 1
        			 sw.break
        sw.case 40
                array.load tmparray[],3, -1, -3, 3, -1, -1, 3, 1, -1, 3, 1, -3
        			 sw.break
        sw.case 41
                array.load tmparray[],3, -1, -1, 3, -1, 1, 3, 1, 1, 3, 1, -1
        			 sw.break
        sw.case 42
                array.load tmparray[],3, -1, 1, 3, -1, 3, 3, 1, 3, 3, 1, 1
        			 sw.break
        sw.case 43
                array.load tmparray[],3, 1, -3, 3, 1, -1, 3, 3, -1, 3, 3, -3
        			 sw.break
        sw.case 44
                array.load tmparray[],3, 1, -1, 3, 1, 1, 3, 3, 1, 3, 3, -1
        			 sw.break
        sw.case 45
                array.load tmparray[],3, 1, 1, 3, 1, 3, 3, 3, 3, 3, 3, 1
        			 sw.break
        sw.case 46
                array.load tmparray[],-3, -3, 3, -3, -3, 1, -3, -1, 1, -3, -1, 3
        			 sw.break
        sw.case 47
                array.load tmparray[],-3, -3, 1, -3, -3, -1, -3, -1, -1, -3, -1, 1
        			 sw.break
        sw.case 48
                array.load tmparray[],-3, -3, -1, -3, -3, -3, -3, -1, -3, -3, -1, -1
        			 sw.break
        sw.case 49
                array.load tmparray[],-3, -1, 3, -3, -1, 1, -3, 1, 1, -3, 1, 3
        			 sw.break
        sw.case 50
                array.load tmparray[],-3, -1, 1, -3, -1, -1, -3, 1, -1, -3, 1, 1
        			 sw.break
        sw.case 51
                array.load tmparray[],-3, -1, -1, -3, -1, -3, -3, 1, -3, -3, 1, -1
        			 sw.break
        sw.case 52
                array.load tmparray[],-3, 1, 3, -3, 1, 1, -3, 3, 1, -3, 3, 3
        			 sw.break
        sw.case 53
                array.load tmparray[],-3, 1, 1, -3, 1, -1, -3, 3, -1, -3, 3, 1
        			 sw.break
        sw.case 54
                array.load tmparray[],-3, 1, -1, -3, 1, -3, -3, 3, -3, -3, 3, -1
            	 sw.break
    sw.end
return
 
!-------------------------------
!-------------------------------
!Load Logical Tracks
!--------------------------
 
LoadTracks:
sw.begin v
	sw.case 1
		array.load tmparray[], 7 , 4 , 1 , 16 , 13 , 10 , 25 , 22 , 19 , 34 , 31 , 28 
      sw.break
	sw.case 2
		array.load tmparray[], 8 , 5 , 2 , 17 , 14 , 11 , 26 , 23 , 20 , 35 , 32 , 29 
      sw.break
	sw.case 3
		array.load tmparray[], 9 , 6 , 3 , 18 , 15 , 12 , 27 , 24 , 21 , 36 , 33 , 30 
      sw.break
	sw.case 4
		array.load tmparray[], 43 , 40 , 37 , 18 , 17 , 16 , 48 , 51 , 54 , 28 , 29 , 30 
      sw.break
	sw.case 5
		array.load tmparray[], 44 , 41 , 38 , 15 , 14 , 13 , 47 , 50 , 53 , 31 , 32 , 33 
      sw.break
	sw.case 6
		array.load tmparray[], 45 , 42 , 39 , 12 , 11 , 10 , 46 , 49 , 52 , 34 , 35 , 36 
      sw.break
	sw.case 7
		array.load tmparray[], 7 , 8 , 9 , 43 , 44 , 45 , 21 , 20 , 19 , 52 , 53 , 54 
      sw.break
	sw.case 8
		array.load tmparray[], 4 , 5 , 6 , 40 , 41 , 42 , 24 , 23 , 22 , 49 , 50 , 51 
      sw.break
	sw.case 9
		array.load tmparray[], 1 , 2 , 3 , 37 , 38 , 39 , 27 , 26 , 25 , 46 , 47 , 48 
sw.end
Return
!---------------------------
LoadFaces:
sw.begin v
	sw.case 1
		array.load tmparray[], 54 , 51 , 48 , 47 , 46 , 49 , 52 , 53 
      sw.break
	sw.case 2		
		array.load tmparray[], 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 
      sw.break
	sw.case 3		
		array.load tmparray[], 43 , 40 , 37 , 38 , 39 , 42 , 45 , 44 
      sw.break
	sw.case 4		
		array.load tmparray[], 9 , 6 , 3 , 2 , 1 , 4 , 7 , 8 
      sw.break
	sw.case 5		
		array.load tmparray[], 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 
      sw.break
	sw.case 6		
		array.load tmparray[], 21 , 24 , 27 , 26 , 25 , 22 , 19 , 20 
      sw.break
	sw.case 7		
		array.load tmparray[], 28 , 29 , 30 , 33 , 36 , 35 , 34 , 31 
      sw.break
	sw.case 8		
		array.load tmparray[], 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 
      sw.break
   sw.case 9		
		array.load tmparray[], 16 , 17 , 18 , 15 , 12 , 11 , 10 , 13 
      sw.break
sw.end
Return
!--------------------
CreateMenu:


	gr.cls
	pmx=(mx-mx/4)/2
	pmy=(my-my*2/3)/2
	pmx2=pmx+mx/4
	pmy2=pmy+my*2/3
	pmb=3
	gr.color 255,128,128,255,1
	gr.rect g1,pmx,pmy,pmx2,pmy2
	gr.color 255,255,255,255,1
	gr.rect g1,pmx,pmy,pmx2-pmb,pmy2-pmb
	gr.color 255,0,0,0,1
	gr.rect g2,pmx+pmb,pmy+pmb,pmx2,pmy2
	gr.modify g1,"alpha",64
	gr.modify g2,"alpha",128
	gr.text.align 2
	tx=(pmy2-pmy)/9
	gr.text.size tx
	gr.color 255,255,255,255,1
	gr.text.draw g1,(pmx+pmx2)/2,pmy+tx*2, "SCRAMBLE"
	gr.text.draw g2,(pmx+pmx2)/2,pmy+tx*4, "SAVE"
	gr.text.draw g3,(pmx+pmx2)/2,pmy+tx*6,"LOAD"
	gr.text.draw g4,(pmx+pmx2)/2,pmy+tx*8, "Exit Menu"
	gr.modify g1,"alpha",128
	gr.modify g2,"alpha",128
	gr.modify g3,"alpha",128
	gr.modify g4,"alpha",128
	gr.color 255,0,0,0,1
	gr.text.draw g1,(pmx+pmx2)/2,pmy+tx*2+2, "SCRAMBLE"
	gr.text.draw g2,(pmx+pmx2)/2,pmy+tx*4+2, "SAVE"
	gr.text.draw g3,(pmx+pmx2)/2,pmy+tx*6+2,"LOAD"
	gr.text.draw g4,(pmx+pmx2)/2,pmy+tx*8+2, "Exit Menu"
	gr.modify g1,"alpha",64
	gr.modify g2,"alpha",64
	gr.modify g3,"alpha",64
	gr.modify g4,"alpha",64
	
	gr.render
	
	gr.screen.to_bitmap bmptmp
	gr.bitmap.crop bmpMenu, bmptmp,pmx,pmy, pmx2-pmx+1, pmy2-pmy+1
	gr.bitmap.delete bmptmp
	!-----------------
	!Lock On
	gr.cls
	gr.color 255,0,0,255,0
	gr.set.stroke 5	
	gr.Circle Circle6,80,12,17/2
	gr.Circle Circle7,80,32,17/2
	gr.Circle Circle8,93,25,12/2
	gr.color 255,0,0,255,1
	gr.set.stroke 0
	gr.Rect Rect5,24,39,30,48 
	gr.Rect Rect4,19,31,28,43 
	gr.Rect Rect1,8,18,83,27 
	gr.Rect Rect2,16,39,22,48 
	gr.Rect Rect3,16,23,31,33 

	gr.render
	
	gr.screen.to_bitmap bmptmp
	gr.bitmap.crop bmpLockOff, bmptmp,0,0, 104,50
	gr.bitmap.delete bmptmp
	!-----------------
	!Lock Off
	gr.cls
	
	gr.color 255,255,0,0,0
	gr.set.stroke 5	
	gr.Circle Circle6,80,12,17/2
	gr.Circle Circle7,80,32,17/2
	gr.Circle Circle8,93,25,12/2
	gr.color 255,255,0,0,1
	gr.set.stroke 0
	gr.Rect Rect5,24,39,30,48 
	gr.Rect Rect4,19,31,28,43 
	gr.Rect Rect1,8,18,83,27 
	gr.Rect Rect2,16,39,22,48 
	gr.Rect Rect3,16,23,31,33 
 
	gr.render
	
	gr.screen.to_bitmap bmptmp
	gr.bitmap.crop bmpLockOn, bmptmp,0,0, 104,50
	gr.bitmap.delete bmptmp
	
	!-----------------	
	!Menu Icon
	gr.cls
	gr.color 255,0,0,255,0
	gr.set.stroke 2
	gr.Rect Rect5,10,my-50,40,my
	gr.Rect Rect4,10,my-40,40,my   
	gr.Rect Rect1,10,my-30,40,my  
	gr.Rect Rect2,10,my-20,40,my  
	gr.Rect Rect3,10,my-10,40,my 

	gr.render
	
	gr.screen.to_bitmap bmptmp
	gr.bitmap.crop bmpIconMenu, bmptmp,0,my-50, 50,50
	gr.bitmap.delete bmptmp
	gr.cls
	gr.render
	
return
	
	
!----------------------
ShowMenu:

	gr.bitmap.size bmpMenu,x,y
	gr.bitmap.draw Menu,bmpMenu,(mx-x)/2,(my-y)/2
	gr.render
	
	RenderCall=0
	do	
		tx=(pmy2-pmy)/9
		gr.bounded.touch touched1,pmx,pmy+tx*1,pmx2,pmy+tx*3-1
		gr.bounded.touch touched2,pmx,pmy+tx*3,pmx2,pmy+tx*5-1
		gr.bounded.touch touched3,pmx,pmy+tx*5,pmx2,pmy+tx*7-1
		gr.bounded.touch touched4,pmx,pmy+tx*7,pmx2,pmy2
		
		if touched1 then
			!new gamew	
			popup "Scrambling Cube, Please be Patient.",0,0,1
			for j=1 to 200
				xtrack=floor(rnd()*9)+1
				if xtrack>9 then xtrack=9
				array.copy TileColor[],tmparray[]
			
				for i=1 to 12
					x=tracks[xtrack,i]
					y=tracks[xtrack,mod(i+2,12)+1]
					TileColor[x]=tmparray[y]
				next
				
				for i=1 to 8
					x=Faces[xtrack,i]
					y=Faces[xtrack,mod(i+1,8)+1]
					if x=0 then
						!do nothing
					else
						TileColor[x]=tmparray[y]
					endif
				next
				array.delete tmparray[]
			next
			RenderCall=1
		Elseif touched2 then
		
			!Save Game
			popup "Saving Game",0,0,0
			text.open w,fn,"RUBIXSAVE.TXT"
			if fn=-1 then
				popup "Error: Could Not Save Game!!!",0,0,0	
			else
					for i=1 to 54
						tmp$=format$("##",TileColor[i])
						text.writeln fn,tmp$
						
					next	
					text.close fn
			endif
						
			
			RenderCall=1
		Elseif touched3 then
		
			!load Game
			Pause 10
			popup "Loading Game",0,0,0
			
			
			file.Exists fn,"RUBIXSAVE.TXT"
			
			if fn=0 then
				popup "No Saved Game!!!",0,0,0	
			else
					text.open r,fn,"RUBIXSAVE.TXT"
					popup "Loading Game!!!",0,0,0
					for i=1 to 54
						text.readln fn,tmp$
						TileColor[i]=val(tmp$)
					next	
					text.close fn
			endif
			
			
			RenderCall=1
		Elseif touched4 then
			popup "Exiting Menu",0,0,0
			RenderCall=1			
	   endif
   Until RenderCall
return
 
