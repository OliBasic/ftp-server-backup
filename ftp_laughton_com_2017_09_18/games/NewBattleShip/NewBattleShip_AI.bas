! By cassiope34  10/04/2013  can run only with v1.74 or next.
! additions by mougino:
! a variable name finishing with "Bmp" means a bitmap pointer, taking place in memory
! a variable name finishing with "Ptr" means an object in the display list, for which we can
!       change attributes (bitmap, x, y, etc.)
! the variable "nul" is used for objects in the display list that we do *not* need to re-use
!       e.g. fill the screen with many tiles of sea once and for all

! constants
OWNER = 1  % owner
BOAT = 2   % boat number (each owner has 10 boats)
LIFE = 3   % number of life remaining
SENS = 4   % direction
dcell = 70 % size of a cell in pixels (it is a square) in bitmap source
db=64 % size of a sprite (boat, canon fire, explosion)
ox=0  % left border
oy=28 % top border

! global var
son = 1  % sound on/off
autoManual = 0   % computer play on/off    % 0=Auto 1=Manual
dim cell[12,8,4] % the grid of the sea. cell[x,y,ITEM]
dim canonBmp[4]  % when battle between 2 boats (each side)
dim exploBmp[7]  % frames of explosion
dim boatBmp[2,4,4] % bitmap used  :  boatBmp[player,orientation,live]
dim playerTurn[2]  % the small circle indicating the player's turn
dim fleet[2]     % the number of boats for each player
dim fleetPtr[2]  % the text object showing the number of boats for each player (in the small circles)
dim boatPtr[2,10]
array.load dix[],1,0,-1,0  % to walk around a cell : => 1=right, 2=down, 3=left, 4=up
array.load diy[],0,1,0,-1
array.load lives[],2,2,2,2,2,3,3,3,4,4  % boats distribution by player

Fn.def oriBt(ori)
    if ori=1 | ori=3 then Fn.rtn 1 % horizontal
    Fn.rtn 2         % vertical
Fn.end

Fn.def max(a,b)
    if a>b then Fn.rtn a
    Fn.rtn b
Fn.end

Fn.def Stg$(c)
  Fn.rtn Left$(Str$(c),Is_In(".",Str$(c))-1)
Fn.end

Fn.def SetRGB(alpha,RGB$,fill)
  gr.color alpha,Hex(Mid$(RGB$,1,2)),Hex(Mid$(RGB$,3,2)),Hex(Mid$(RGB$,5,2)),fill
Fn.end

! LOAD AND SCALE GRAPHICS !

gr.open 255,0,0,128,1  %blue
gr.orientation 0
gr.screen real_w, real_h
di_w=12*dcell + 2*ox + 130
di_h=8*dcell + oy
scale_w=real_w/di_w
scale_h=real_h/di_h
gr.scale scale_w, scale_h

gr.bitmap.load tmpBmp,"boats.png"

! create bitmaps boats
for player=1 to 2
  for live=1 to 4
    ori=1  % orientation : 1=right
    gr.bitmap.crop boatBmp[player,ori,live],tmpBmp,(live-1)*db,(player-1)*db,db,db
    for ori = 2 to 4  % orientation : 2=down 3=left 4=up
      gr.bitmap.create boatBmp[player,ori,live], db, db
      gr.bitmap.drawinto.start boatBmp[player,ori,live]
        gr.rotate.start 90*(ori-1), floor(db/2), floor(db/2)
          gr.bitmap.draw nul, boatBmp[player,1,live],0,0
        gr.rotate.end
      gr.bitmap.drawinto.end
    next
  next
next

! create bitmaps canons
ori=4  % orientation : 4=up
gr.bitmap.crop canonBmp[ori],tmpBmp,0,2*db,db,db
for ori = 1 to 3  % orientation : 1=right 2=down 3=left
  gr.bitmap.create canonBmp[ori], db, db
  gr.bitmap.drawinto.start canonBmp[ori]
    gr.rotate.start 90*ori, floor(db/2), floor(db/2)
      gr.bitmap.draw nul, canonBmp[4],0,0
    gr.rotate.end
  gr.bitmap.drawinto.end
next

! create bitmaps frames of explosion
for i = 1 to 7
  gr.bitmap.crop exploBmp[i],tmpBmp,Mod(i,4)*db,(floor(i/4)+2)*db,db,db
next

gr.bitmap.crop seaBmp,tmpBmp,0,257,dcell+1,dcell+1
gr.bitmap.crop sonBmp,tmpBmp,71,257,45,45

gr.bitmap.crop newBmp,tmpBmp,136,257,120,35
gr.bitmap.crop setBmp,tmpBmp,136,292,120,35

! now the original bitmap is not useful anymore -> we free the memory it uses!
gr.bitmap.delete tmpBmp

Soundpool.open 2
Soundpool.load boomPtr, "Boum.mp3"
gr.text.bold 1

CurPlayer = 1  % orange start by default.
ComputerPlayer = 2

! load Saved Game if it exist : SavedGame=1
GOSUB LoadGame

new:
if SavedGame=0
  undim cell[]  % resets the grid of the sea
  dim cell[12,8,4]
  if tx>0 & tx<ox+12*dcell+5 then   % we must change the player who started the last game...!
      CurPlayer = 3-CurPlayer
 ! else
 !    CurPlayer = 1  % the current player
  endif
 ! ComputerPlayer = 3-CurPlayer
  ComputerStart = !autoManual   % 0=Auto 1=Manual
  fleet[1]=10
  fleet[2]=10
endif

! DRAW THE SCREEN
gr.cls

! the sea
Call SetRGB(255,"000080",1) % dark blue
for y=1 to 8
    for x=1 to 12
        gr.bitmap.draw nul,seaBmp,ox+(x-1)*dcell,oy+(y-1)*dcell
    next
next
gr.rect horPtr,ox,0,ox+12*dcell,0
gr.hide horPtr
gr.rect verPtr,0,oy,0,oy+8*dcell
gr.hide verPtr
gr.bitmap.draw canonPtr,canonBmp[1],0,0
gr.hide canonPtr
gr.bitmap.draw exploPtr,exploBmp[1],0,0
gr.hide exploPtr

! install boats
if SavedGame=0 then
 for player=1 to 2
  for bt=1 to 10          % 10 boats by player
    do    % to not have 2 boats in a same cell
        x=floor(rnd()*4)+1+(player-1)*8
        y=floor(rnd()*8)+1
    until cell[x,y,OWNER]=0
    cell[x,y,BOAT] = bt
    cell[x,y,OWNER]= player
    cell[x,y,LIFE] = lives[bt]
    cell[x,y,SENS] = (player-1)*2+1   % 1=right 3=left
    gr.bitmap.draw boatPtr[player,bt], boatBmp[player,(player-1)*2+1,lives[bt]], ox+(x-1)*dcell+3, oy+(y-1)*dcell+3
  next
 next
else
   for y=1 to 8
      for x=1 to 12
         pl = cell[x,y,OWNER]
         if pl then
            gr.bitmap.draw boatPtr[pl, cell[x,y,BOAT]], boatBmp[pl, cell[x,y,SENS], cell[x,y,LIFE]], ox+(x-1)*dcell+3, oy+(y-1)*dcell+3
         endif
      next
   next
endif

! the buttons on the right
Call SetRGB(255,"444444",1) % grey
gr.text.size 20
gr.text.align 1

gr.bitmap.draw nul,newBmp,ox+12*dcell+5,oy+35
gr.text.draw nul,ox+12*dcell+52,oy+60,"New"

gr.bitmap.draw nul,setBmp,ox+12*dcell+5,oy+7*dcell
gr.text.draw setPtr,ox+12*dcell+43,oy+7*dcell+25, word$("1 player;2 players",autoManual+1,";")

gr.text.align 2
gr.text.size 40

Call SetRGB(255,"000080",1) % dark blue
gr.bitmap.draw nul,sonBmp,ox+12*dcell+43,oy+120
gr.text.draw sonPtr,ox+12*dcell+65,oy+158,"X"
  if !son then gr.show sonPtr else gr.hide sonPtr

Call SetRGB(255,"FF9900",1) % orange (player #1)
gr.circle playerTurn[1],12*dcell+2*ox+130/2,oy+265,40
Call SetRGB(255,"000080",1) % dark blue
gr.circle nul,12*dcell+2*ox+130/2,oy+265,35
Call SetRGB(255,"FF9900",1) % orange (player #1)
gr.circle nul,12*dcell+2*ox+130/2,oy+265,30
Call SetRGB(255,"000080",1) % dark blue
gr.text.draw fleetPtr[1],12*dcell+2*ox+130/2,oy+280,Stg$(fleet[1])

Call SetRGB(255,"008000",1) % dark green (player #2)
gr.circle playerTurn[2],12*dcell+2*ox+130/2,oy+395,40
Call SetRGB(255,"000080",1) % dark blue
gr.circle nul,12*dcell+2*ox+130/2,oy+395,35
Call SetRGB(255,"008000",1) % dark green (player #2)
gr.circle nul,12*dcell+2*ox+130/2,oy+395,30
gr.hide playerTurn[2]
Call SetRGB(255,"000080",1) % dark blue
gr.text.draw fleetPtr[2],12*dcell+2*ox+130/2,oy+410,Stg$(fleet[2])
gr.text.align 1
gr.text.size 40
!gr.color 150, 0, 90, 190, 1
call SetRGB(150,"005ABE",1)
gr.text.draw aiStrNo, 5*dcell, 8*dcell , "thinking...."
gr.hide aiStrNo
Call SetRGB(255,"000080",1)
gr.render

if SavedGame=1 | ComputerStart=1 then  % there is a file "NewBattleShip.ini"  ?
   gr.hide playerTurn[3-CurPlayer]
   gr.show playerTurn[CurPlayer]
   gr.render
   if autoManual=0 then
      if CurPlayer = ComputerPlayer then   % computer play first.
         popup "  Computer play. ",0,0,0
         GOSUB AI
      endif
   endif
   SavedGame=0
   ComputerStart=0
endif

! GAME ROUTINE !
do
    do
       gr.touch touched,tx,ty
       pause 10 % does not stress the CPU ;o)
       if xSel then
        horverAlpha += 3*horverPulse
        if horverAlpha=0 | horverAlpha=84 then horverPulse *= -1
        gr.modify horPtr,"alpha",horverAlpha
        gr.modify verPtr,"alpha",horverAlpha
        gr.render
       end if
    until touched
    do
       gr.touch touched,tx,ty
    until !touched
    tx/=scale_w
    ty/=scale_h
    
    ! Test tap on "New" button
    if tx>ox+12*dcell+5 & ty>oy+40 & tx<ox+12*dcell+125 & ty<oy+70 then goto new

    ! Test tap on "Auto/Manual" button
    if tx>ox+12*dcell+5 & ty>oy+7*dcell & tx<ox+12*dcell+125 & ty<oy+7*dcell+35 then
      autoManual=1-autoManual % 0=Auto 1=Manual
      gr.modify setPtr,"text",word$("1 player;2 players",autoManual+1,";")
    end if
    
    ! Test tap on "sound" button
    if tx>ox+12*dcell+43 & ty>oy+120 & tx<ox+12*dcell+88 & ty<oy+165 then
      son=1-son
      if !son then gr.show sonPtr else gr.hide sonPtr
      gr.render
    end if

    tx=floor((tx-ox)/dcell)+1
    ty=floor((ty-oy)/dcell)+1
    if tx<13 & ty<9 then 
      if cell[tx,ty,OWNER]=CurPlayer then % user tapped on a boat belonging to current player
        xSel=tx
        ySel=ty
        horverAlpha=0
        horverPulse=+1
        gr.modify horPtr,"top",oy+(ty-1)*dcell
        gr.modify horPtr,"bottom",oy+(ty)*dcell
        gr.modify verPtr,"left",ox+(tx-1)*dcell
        gr.modify verPtr,"right",ox+(tx)*dcell
        gr.show horPtr
        gr.show verPtr
      else if xSel>0 & (tx=xSel | ty=ySel) & !(tx=xSel & ty=ySel) then % user tapped target cell
        dx=tx % target coordinates
        dy=ty
        tx=xSel % origin coordinates
        ty=ySel
        gosub hidecurs
        ! Now, move boat from (tx,ty) to (dx,dy). If there are obstacles, boat may not
        ! go all the way. In any case the final cell of the boat will be returned in (dx,dy)
        gosub moveboat
        if autoManual then ok = 0
        if ok & fleet[curPlayer] then GOSUB AI
      else
        gosub hidecurs
      endif
    else
      gosub hidecurs
    endif
until 0
! save the current game ???
gr.close
end

onbackkey:
  ! save the current game
  if fleet[1]>0 & fleet[2]>0 then GOSUB SaveGame else File.Delete fnb,"NewBattleShip.ini"
  gr.cls
  gr.render
  popup "goodbye",0,0,0
  pause 500
  gr.close
end

hidecurs:
  xSel=0
  gr.modify horPtr,"alpha",84
  gr.modify verPtr,"alpha",84
  gr.hide horPtr
  gr.hide verPtr
  gr.render
return

rotateboat:
! Rotate boat at (tx,ty) to the direction "tori" (=target orientation)
! tori=1=right, tori=2=down, tori=3=left, tori=4=up
ori  = cell[tx,ty,SENS]
live = cell[tx,ty,LIFE]
angle = Mod(360+90*(ori-1),360)
if Mod(tori-ori+4,4)=3 then ro=-1 else ro=+1
do
  angle=Mod(360+angle+ro*10,360) % rotation clockwise or counter-clockwise (whichever the fastest)
  if rotaBmp then gr.bitmap.delete rotaBmp % important to avoid memory leak!
  gr.bitmap.create rotaBmp,db,db
  gr.bitmap.drawinto.start rotaBmp
    gr.rotate.start angle, floor(db/2), floor(db/2)
      gr.bitmap.draw nul, boatBmp[cell[tx,ty,OWNER],1,live], 0, 0
    gr.rotate.end
  gr.bitmap.drawinto.end
  gr.modify boatPtr[cell[tx,ty,OWNER],cell[tx,ty,BOAT]], "bitmap", rotaBmp
  gr.render
  pause 2
until angle=90*(tori-1)
cell[tx,ty,SENS]=tori
gr.modify boatPtr[cell[tx,ty,OWNER],cell[tx,ty,BOAT]], "bitmap", boatBmp[cell[tx,ty,OWNER],tori,live]
return

moveboat:
! Move boat from (tx,ty) to (dx,dy)
! It is assumed that at least tx=dx (vertical move) or ty=dy (horizontal move)
! First, check if boat is in good direction, if not rotate it
ok = 0
ori = cell[tx,ty,SENS]
distX = tx - dx
distY = ty - dy
if abs(distX) > 0 & distY = 0 then
   if distX > 0 then tori = 3 else tori = 1    % left or right
elseif distX = 0 & abs(distY) > 0
   if distY > 0 then tori = 4 else tori = 2    % up or down
endif
if tori<>ori then   % orientation of the boat changes
   ! rotate boat at (tx,ty) to the direction "tori" (=target orientation)
   gosub rotateboat
endif
! Then move boat
mx=tx+dix[tori]
my=ty+diy[tori]
x=ox+(tx-1)*dcell+3
y=oy+(ty-1)*dcell+3
while (mx>0 & mx<13) & (my>0 & my<9) & !(mx=dx+dix[tori] & my=dy+diy[tori])
  if cell[mx,my,BOAT]=0 then
    for i=0 to dcell-10 step 10
      x+=10*dix[tori]
      y+=10*diy[tori]
      gr.modify boatPtr[cell[tx,ty,OWNER],cell[tx,ty,BOAT]], "x", x
      gr.modify boatPtr[cell[tx,ty,OWNER],cell[tx,ty,BOAT]], "y", y
      gr.render
      pause 2
    next
    mx+=dix[tori]
    my+=diy[tori]
  else
    w_r.break
  end if
repeat
gosub hidecurs
dx=mx-dix[tori] % dx=final destination X-coordinate
dy=my-diy[tori] % dy=final destination Y-coordinate
if tx<>dx | ty<>dy then
  cell[dx,dy,BOAT]= cell[tx,ty,BOAT]
  cell[dx,dy,OWNER]= cell[tx,ty,OWNER]
  cell[dx,dy,LIFE]= cell[tx,ty,LIFE]
  cell[dx,dy,SENS]= cell[tx,ty,SENS]
  cell[tx,ty,BOAT]= 0
  cell[tx,ty,OWNER]= 0
  cell[tx,ty,LIFE]= 0
  cell[tx,ty,SENS]= 0
  
  GOSUB battle
  gosub changeplayer
  ok = 1
  
end if
return

battle:
! manage the battle around dx,dy
! Start battle between boat #1 at (dx,dy) and boat #2 at (mx,my)
! The boat #1 is firing first unless it is head to side of boat #2
oori = cell[dx,dy,SENS]  % origin direction of boat #1
shot$=""
bt$  =""
bt1$ =""
bt2$ =""
bt3$ =""
bt4$ =""
for look=0 to 3
    !ori = Mod(oori+look-1,4)+1  % look in front, then in every other direction, clockwise
    ori = val(word$("1 2 3 4 1 2 3",oori+look))
    mx = dx+dix[ori]
    my = dy+diy[ori]
    if mx>0 & mx<13 & my>0 & my<9 then
        if cell[mx,my,OWNER]=3-CurPlayer & cell[dx,dy,LIFE]>0 then % it's an ennemy's boat!
            mori = cell[mx,my,SENS]  % origin orientation of bat #2
            if look=0 then
              if oriBt(mori) <> oriBt(oori) then   % perpendicular so shot first
                swap dx,mx
                swap dy,my
                gosub firecanon
                swap mx,dx
                swap my,dy
              endif
            else
                ! must shot the weakest enemy first so first enemy so perpandicular first and lowest life next.
                bt$ = str$(ori)+" "  % store direction where a boat must be shot.
                if oriBt(mori) <> oriBt(oori) then
                   if len(bt1$) then bt2$=bt$ else bt1$=bt$
                else
                   if len(bt3$) then bt4$=bt$ else bt3$=bt$
                endif
            endif
        endif
    endif
next
if len(bt3$)>0 & len(bt4$)>0 then   % sort 2 boats by the lowest LIFE.
  if cell[dx+dix[val(bt3$)],dy+diy[val(bt3$)],LIFE]> cell[dx+dix[val(bt4$)],dy+diy[val(bt4$)],LIFE] then swap bt3$,bt4$
endif
shot$ = bt1$ + bt2$ + bt3$ + bt4$
if len(shot$)>0 then
    for shot = 1 to len(shot$)/4
        dr = val(word$(shot$,shot))
        if cell[dx,dy,LIFE] then
            mx = dx+dix[dr]
            my = dy+diy[dr]
            gosub firecanon
            if cell[mx,my,LIFE]>0 & oriBt(cell[mx,my,SENS]) = oriBt(oori) then
               swap dx,mx
               swap dy,my
               gosub firecanon
               swap mx,dx
               swap my,dy
            endif
        endif
    next
endif
return

firecanon:
! Fire canons of boat #1 which is at (dx,dy) towards boat #2 which is at (mx,my)
if mx=dx+1 then cori=1 % cori=cannon orientation
if my=dy+1 then cori=2
if mx=dx-1 then cori=3
if my=dy-1 then cori=4
gr.modify canonPtr,"bitmap",canonBmp[cori]
gr.modify canonPtr,"x",ox+(dx-1)*dcell
gr.modify canonPtr,"y",oy+(dy-1)*dcell
! Boat #1 fires
if son then Soundpool.play nul,boomPtr,0.99,0.99,1,0,1
gr.show canonPtr
gr.render
pause 100
gr.hide canonPtr
gr.render
! Boat #2 is hit
cell[mx,my,LIFE]--
if cell[mx,my,LIFE]>0 then % Boat #2 loses a life but is not sunk yet
  pause 100
  gr.modify boatPtr[cell[mx,my,OWNER],cell[mx,my,BOAT]],"bitmap",~
      boatBmp[cell[mx,my,OWNER],cell[mx,my,SENS],cell[mx,my,LIFE]]
  for i = 1 to 1
    gr.hide boatPtr[cell[mx,my,OWNER],cell[mx,my,BOAT]]
    gr.render
    pause 50
    gr.show boatPtr[cell[mx,my,OWNER],cell[mx,my,BOAT]]
    gr.render
    pause 50
  next
else % Boat #2 says hello to the fishes!
  gr.modify exploPtr,"x",ox+(mx-1)*dcell
  gr.modify exploPtr,"y",oy+(my-1)*dcell
  gr.show exploPtr
  for i = 1 to 1
    gr.modify exploPtr,"bitmap",exploBmp[i]
    gr.render
    pause 50
  next
  gr.hide boatPtr[cell[mx,my,OWNER],cell[mx,my,BOAT]]
  for i = 4 to 7
    gr.modify exploPtr,"bitmap",exploBmp[i]
    gr.render
    pause 60
  next
  gr.hide exploPtr
  fleet[cell[mx,my,OWNER]]--
  gr.modify fleetPtr[cell[mx,my,OWNER]],"text",Stg$(fleet[cell[mx,my,OWNER]])
  gr.render
  cell[mx,my,BOAT]= 0
  cell[mx,my,OWNER]= 0
  cell[mx,my,SENS]= 0
end if
pause 200
return

changeplayer:
  gr.hide playerTurn[CurPlayer]
  CurPlayer = 3-CurPlayer   % other player's turn
  gr.show playerTurn[CurPlayer]
  gr.render
return

!  ------------------------------- AI --------------------------

AI:
! simu    ' simu=0 play,  simu=1 : just show hint.
ComputerPlayer = CurPlayer
flagShow_AI_string = 1
gr.show aiStrNo
gr.render
Timer.set 500

undim b[]
undim rb[]
undim deb[]
dim b[18]
dim rb[18]
dim deb[18]
n=0
for yb = 1 to 8      % all the sea...
    for xb = 1 to 12
        if cell[xb,yb,OWNER] = CurPlayer then  % pour chaque bateau de CurPlayer
            n++        % compte les bateaux
            b[n] = yb*100+xb    % ses coordonn�es
            lives = cell[xb,yb,LIFE]
            se = cell[xb,yb,SENS]
            param$ = str$(xb)+" "+str$(yb)+" "+str$(se)+" "+str$(lives)
            gosub danger
            rb[n] = result  % niveau de danger pour ce bateau
            param$ = str$(xb)+" "+str$(yb)
            gosub evalttdpl
            r$ = result$    % meilleur d�placement possible
            if r$ = ""      % bateau bloqu�
               n = max(1,n-1)
            else
               deb[n] = val(left$(r$,3)) % destination du bateau
               rb[n] = rb[n] + val(mid$(r$,4))  % valeur de cette destination
            endif
        endif
    next
next
! choisir le plus gros rb[] ou au hasard si plusieurs rb[] sont �quivalents...
rc = -100
while n > 0  % tous les bateaux restants pour CurPlayer
    if rb[n] > rc
        rc = rb[n]
        c$ = str$(100+n)+" "
    elseif rb[n] = rc
        c$ = c$ + str$(100+n)+" "
    endif
    n--
repeat
c$ = replace$(c$,".0","")
np = len(c$)/4   % nbre de possibilit�s �gales
np = floor(rnd()*np)+1   % choix au hasard
pl = val(right$(word$(c$,np),2))
! jouer le coup.
! if deb[pl] = 0 then return   'partie termin�e (le joueur ne peut plus jouer...)
ty = floor(b[pl]/100)
tx = b[pl] - ty*100
dy = floor(deb[pl]/100)
dx = deb[pl] - dy*100
Timer.Clear
gr.hide aiStrNo
gr.render

!if simu then
!    call hint tx, ty, dx, dy  % don't play but only show the hint.
!else
    gosub moveBoat  % play tx,ty to dx,dy and change player
!endif
ok = 0
RETURN

onTimer:
  flagShow_AI_string = !flagShow_AI_string
  if flagShow_AI_string then
    gr.show aiStrNo
    gr.show playerTurn[CurPlayer]
  else
    gr.hide aiStrNo      
    gr.hide playerTurn[CurPlayer]
  endif
  gr.render
Timer.Resume
!RETURN

danger:
result = 0
xx5 = val(word$(param$,1))
yy5 = val(word$(param$,2))
se5 = val(word$(param$,3))
vi = val(word$(param$,4))
!   peut-il �tre atteind par un bateau de aj ?
sp1 = val(word$("2 3 4 1",se5)) % se+1-4*(se=4)      perpendiculaire � se5
sp2 = val(word$("4 1 2 3",se5)) % sp1+2-4*(sp1>2)      2eme perpend. � se5
aj  = 3-CurPlayer % Opposite player
dg5=0
! 8 bateaux adverses pourraient venir autour de xx,yy
! scrute les 4 positions autour de xx,yy
for s0 = 1 to 4
    x0 = xx5 + dix[s0]
    y0 = yy5 + diy[s0]
    if y0>0 & y0<9 & x0>0 & x0<13 then
        if cell[x0,y0,OWNER] = 0 then
            sr = val(word$("2 3 4 1",s0)) % s+1-4*(s=4) ' sens + 1
            param$ = str$(x0)+" "+str$(y0)+" "+str$(sr)+" "+str$(aj)
            gosub atteind
            va = result
            if va > 0 then
                if vi = 1
                    dg5 = 11
                    F_n.break
                elseif sr = sp1 | sr = sp2
                    dg5 = 10
                endif
            endif
            sr = val(word$("3 4 1 2",sr)) % sr+2-4*(sr>2)  ' sens oppos� au 1er
            param$ = str$(x0)+" "+str$(y0)+" "+str$(sr)+" "+str$(aj)
            gosub atteind
            va = result
            if va > 0 then
                if vi = 1
                    dg5 = 11
                    F_n.break
                elseif sr = sp1 | sr = sp2
                    dg5 = 10
                endif
            endif
        endif
    endif
next
result = dg5
if fleet[CurPlayer] > fleet[aj]+1 then result = max(0,dg5-1)
RETURN

atteind:
! identifie si un bateau de jo4 peut atteindre xx4,yy4 par 'se4'
result = 0
xx4 = val(word$(param$,1))
yy4 = val(word$(param$,2))
se4 = val(word$(param$,3))
jo4 = val(word$(param$,4))
x4 = xx4 + dix[se4]
y4 = yy4 + diy[se4]
while y4>0 & y4<9 & x4>0 & x4<13
    bj4 = cell[x4,y4,OWNER]
    if bj4 = jo4
       result = cell[x4,y4,LIFE]    % nbre de vie(s) du bateau arrivant
       W_r.break
    elseif bj4 > 0
       W_r.break
    endif
    x4 = x4 + dix[se4]
    y4 = y4 + diy[se4]
repeat
RETURN

evalttdpl:
result$ = ""
! effacer ce bateau temporairement...
! �valuer chaque positions possibles
! �valuer le combat potentiel
! �valuer s'il peut �tre atteind
! ou atteindre un adversaire.
xx = val(word$(param$,1))
yy = val(word$(param$,2))
undim db[]
undim vd[]
dim db[18]
dim vd[18]
! retire temporairement le bateau
mm1 = cell[xx,yy,OWNER]
mm2 = cell[xx,yy,BOAT]
mm3 = cell[xx,yy,LIFE]
mm4 = cell[xx,yy,SENS]
lives = mm3
cell[xx,yy,OWNER]= 0
cell[xx,yy,BOAT] = 0
cell[xx,yy,LIFE] = 0
cell[xx,yy,SENS] = 0
dg = 0
d = 0
for s = 1 to 4
    x = xx + dix[s]
    y = yy + diy[s]
    while y>0 & y<9 & x>0 & x<13
        if cell[x,y,OWNER] = 0 then   % position libre d'acc�s
            d++
            db[d] = y*100+x
            param$ = str$(x)+" "+str$(y)+" "+str$(s)+" "+str$(lives)+" "+str$(CurPlayer)
            GOSUB combat
            vd[d] = result 
            if vd[d] >= 0 & vd[d] <= 16
               ! Cette position peut-elle �tre atteinte ?
                param$ = str$(x)+" "+str$(y)+" "+str$(s)+" "+str$(lives)
                GOSUB danger
                dg = result
                vd[d] = vd[d] - dg
                ! ou atteindre un bateau adverse sans danger
                if dg = 0 then   
                    param$ = str$(x)+" "+str$(y)+" "+str$(s)
                    GOSUB attaque
                    vd[d] = vd[d] + result
                endif
            elseif vd[d] >= 0
                vd[d] = vd[d] + lives  % bateau le + gros
            endif
       else
            W_r.break
       endif
       x = x + dix[s]
       y = y + diy[s]
    repeat
next
! restore le bateau
cell[xx,yy,OWNER]= mm1
cell[xx,yy,BOAT] = mm2
cell[xx,yy,LIFE] = mm3
cell[xx,yy,SENS] = mm4
!   choisir le plus gros rd[] ou au hasard si plusieurs rb[] sont �quivalents...
if d = 0 then RETURN  % bateau bloqu�...
r1$ = ""
nd = 0
v = -200
while d > 0
    if vd[d] > v
        v = vd[d]
        r1$ = str$(100+d)
        nd=1
    elseif vd[d] = v
        r1$ = r1$ + " " + str$(100+d)
        nd++
    endif
    d--
repeat
r1$= replace$(r1$,".0","")
v = floor(rnd()*nd)+1
v = val(right$(word$(r1$,v),2))  % v = n� du d�placement choisi.
result$ = str$(db[v]) + str$(vd[v])  % doit faire 6 caract�res....
result$ = replace$(result$,".0","")
RETURN

attaque:
! depuis xx,yy le bateau peut-il atteindre un bateau adverse perpendiculairement � 'se'.
xx1 = val(word$(param$,1))
yy1 = val(word$(param$,2))
se1 = val(word$(param$,3))
undim s[]
dim s[2]
r1 = 0
so   = val(word$("3 4 1 2",se1))  % se+2-4*(se>2) ' sens oppos� � se
s[1] = val(word$("2 3 4 1",se1))  % se+1-4*(se=4) ' perpendiculaire � se
s[2] = val(word$("4 1 2 3",se1))  % s(1)+2-4*(s(1)>2) ' 2eme perpend. � se
aj1  = CurPlayer+1-2*(CurPlayer=2)  % joueur adverse
for s1 = 1 to 2
    x1 = xx1 + dix[s[s1]]
    y1 = yy1 + diy[s[s1]]
    while x1>0 & x1<13 & y1>0 & y1<9
        bj1 = cell[x1,y1,OWNER]
        IF bj1 = 0  % position libre
            ! voir s'il y a de chaque c�t� un bateau adv. orient� se ou so
            vx = x1 + dix[se1]
            vy = y1 + diy[se1]
            if vx>0 & vx<13 & vy>0 & vy<9 then
                if cell[vx,vy,OWNER] = aj1 & (cell[vx,vy,SENS] = se1 | cell[vx,vy,SENS] = so | cell[vx,vy,LIFE] = 1) then
                    r1 = 13
                    if cell[vx,vy,LIFE] = 1 then r1 = 14
                    W_r.break
                endif
            endif
            vx = x1 + dix[so]
            vy = y1 + diy[so]
            if vx>0 & vx<13 & vy>0 & vy<9 then
                if cell[vx,vy,OWNER] = aj1 & (cell[vx,vy,SENS] = se1 | cell[vx,vy,SENS] = so | cell[vx,vy,LIFE] = 1) then
                    r1 = 13
                    if cell[vx,vy,LIFE] = 1 then r1 = 14
                    W_r.break
                endif
            endif
        ELSEIF bj1 = aj1
            r1 = 10-cell[x1,y1,LIFE]
            W_r.break
        ELSE
            W_r.break
        ENDIF
        x1 = x1 + dix[s[s1]]
        y1 = y1 + diy[s[s1]]
    repeat
next
result = r1
RETURN

combat:
! �valuation d'un combat autour d'un bateau de jo,
! arrivant en xx,yy, ds le sens sa, ayant vi vie(s)
undim s[]
dim s[2]
r=0
xx2 = val(word$(param$,1))
yy2 = val(word$(param$,2))
sa  = val(word$(param$,3))
vi2  = val(word$(param$,4))
jo2  = val(word$(param$,5))
aj2  = jo2+1-2*(jo2=2)   % joueur adverse
sp1 = val(word$("2 3 4 1",sa)) % sa+1-4*(sa=4)   % 1er sens perpendiculaire � 'sa'
sp2 = val(word$("4 1 2 3",sa)) % sp1+2-4*(sp1>2) % 2eme sens perpend. � 'sa'
vb  = vi2
vb1 = 0
vb2 = 0
! d'abord en butée dix[sa],diy[sa]
if xx2+dix[sa]>0 & xx2+dix[sa]<13 & yy2+diy[sa]>0 & yy2+diy[sa]<9 then
     bj = cell[xx2+dix[sa],yy2+diy[sa],OWNER]
     sb = cell[xx2+dix[sa],yy2+diy[sa],SENS]
     IF bj = aj2 & (sb = sp1 | sb = sp2)
          vb--
          if vb = 0 then
              r = -70
              result = r
              RETURN   %  perte s�che...
          else
              r = -25  % mon bateau perd une vie
              if vb = 1 then r = r - 10   % mon bateau n'a plus qu'1 vie
          endif
      ELSEIF bj = aj2 & sb <> sp1 & sb <> sp2
          ! bute contre un bateau adverse //
          if cell[xx2+dix[sa],yy2+diy[sa],LIFE] <= vi2 then
              su = 14 + vi2   % peut �tre bon si pas de combat...
          else
              su = 14
          endif
      ENDIF
endif
! �valuer le plus petit bateau adverse sur les flancs
if xx2+dix[sp1]>0 & xx2+dix[sp1]<13 & yy2+diy[sp1]>0 & yy2+diy[sp1]<9 then
    vb1 = cell[xx2+dix[sp1],yy2+diy[sp1],LIFE]
endif
if xx2+dix[sp2]>0 & xx2+dix[sp2]<13 & yy2+diy[sp2]>0 & yy2+diy[sp2]<9 then
    vb2 = cell[xx2+dix[sp2],yy2+diy[sp2],LIFE]
endif
IF vb1 + vb2 = 0
    goto out
ELSEIF vb1 > 0 & vb2 = 0
    s[1] = sp1
ELSEIF vb2 > 0 & vb1 = 0
    s[1] = sp2
ELSEIF vb1 <= vb2
    s[1] = sp1
    s[2] = sp2
ELSEIF vb2 < vb1
    s[1] = sp2
    s[2] = sp1
ENDIF
!  �valuation des 2 cot�s
for s2 = 1 to 2
    if s[s2]>0 then
        x2 = xx2 + dix[s[s2]]
        y2 = yy2 + diy[s[s2]]
        if x2>0 & x2<13 & y2>0 & y2<9 then
            if cell[x2,y2,OWNER] = aj2 then
                vba = cell[x2,y2,LIFE] - 1
                sb = cell[x2,y2,SENS]
                if vba = 0 then
                    r = r + 82   % d�truit bateau adverse
                else
                    r = r + 40   % bateau adv. perd une vie
                    if fleet[CurPlayer] > fleet[aj2]+1 then r = r + 5  % offensif
                    if vba = 1 then r = r + 10  % bateau adv. n'a plus qu'1 vie
                    if sb = sa | sb = val(word$("3 4 1 2",sa)) then   %  then  % sa+2-4*(sa>2) oriBt(sb) = oriBt(sa)' bateau //
                        vb--
                        if vb = 0 then
                            r = r - 65
                            F_n.break   % perd mon bateau
                        else
                            r = r - 25       % mon bateau perd une vie
                            if vb >= vba then r = r + 5
                            if vb = 1 then r = r - 10  % mon bateau n'a plus qu'1 vie
                        endif
                    endif
                endif
            endif
        endif
    endif
next
out:
if r = 0 then r = su
result = r
RETURN

SaveGame:
    Text.open w, BackupGame, "NewBattleShip.ini"
    Text.writeln BackupGame, str$(autoManual)+" "+str$(son)+" "+str$(CurPlayer)+" "+str$(ComputerPlayer)
    for cy = 1 to 8
        L$ = ""
        for cx = 1 to 12
             L$ = L$ +str$(cell[cx,cy,OWNER])+" "+str$(cell[cx,cy,BOAT])~
                 +" "+str$(cell[cx,cy,LIFE]) +" "+str$(cell[cx,cy,SENS])+" "
        next
        Text.writeln BackupGame, L$
    next
    Text.close BackupGame
RETURN

LoadGame:
File.Exists LdGame, "NewBattleShip.ini"
if LdGame
   Text.open r, fnb, "NewBattleShip.ini"
   DIM Ln$[10]
   i=0
   DO
      Text.readln fnb, line$
      i++      
      Ln$[i] = line$
   UNTIL line$ = "EOF"
   Text.close fnb
   autoManual = val(word$(Ln$[1],1))
   son = val(word$(Ln$[1],2))
   CurPlayer = val(word$(Ln$[1],3))
   ComputerPlayer = val(word$(Ln$[1],4))
   for cy=1 to 8
      c=1
      for cx=1 to 12
         cell[cx,cy,OWNER] = val(word$(Ln$[cy+1],c))
         cell[cx,cy,BOAT] = val(word$(Ln$[cy+1],c+1))
         cell[cx,cy,LIFE] = val(word$(Ln$[cy+1],c+2))
         cell[cx,cy,SENS] = val(word$(Ln$[cy+1],c+3))
         if cell[cx,cy,OWNER] then fleet[cell[cx,cy,OWNER]]++
         c+=4                 
      next
   next
   popup " Last game. ",0,0,0
   SavedGame = 1
   array.delete Ln$[]
endif
RETURN


