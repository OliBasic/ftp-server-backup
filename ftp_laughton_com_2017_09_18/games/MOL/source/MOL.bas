! Aat @2016 Monsters Of the Maze
FN.DEF GetNum(a,b)
	! random number ranging from a to b
	c=FLOOR(((b-a)+1)*RND()+a)
	FN.RTN c
FN.END
GR.OPEN 255,0,0,0,0,1
GR.SCREEN w,h:ScaleX=600:ScaleY=h/w*ScaleX:sx=w/ScaleX:sy=h/ScaleY:GR.SCALE sx,sy:WAKELOCK 3
DataPath$="../../MOL/data/"
DIM Grimace[9],Food[9],Top[9],Bottom[9],Mon[3],GrimShuff[9],FoodShuff[9],F$[9]
FOR i=1 TO 9:GrimShuff[i]=i:FoodShuff[i]=i:NEXT i
ARRAY.SHUFFLE GrimShuff[]:ARRAY.SHUFFLE FoodShuff[]
ARRAY.LOAD NDir[],3,4,1,2
ARRAY.LOAD Start$[],"Sound is ON","Start at Level 1 (of 9)","START"
ARRAY.LOAD Menu$[],"Solve this maze","Stop playing","Cancel"
ARRAY.LOAD ChLevel$[],"1","2","3","4","5","6","7","8","9"
FOR i=1 TO 9
	F$[i]="Level "+INT$(i)+" not finished"
NEXT i
LIST.CREATE N,MazePath:LIST.CREATE N,PCoords
FOR i=1 TO 9
	GR.BITMAP.LOAD Grimace[i],DataPath$+"Grimace"+INT$(GrimShuff[i])+".png"
	GR.BITMAP.LOAD Food[i],DataPath$+"Food"+INT$(FoodShuff[i])+".png"
NEXT i
FONT.LOAD Toxia,DataPath$+"Toxia.ttf"
AUDIO.LOAD Beep,DataPath$+"BeepBeep.mp3"
AUDIO.LOAD Woosh,DataPath$+"Whoosh.mp3"
AUDIO.LOAD Tamba,DataPath$+"Tamba.mp3"
GR.BITMAP.CREATE ScoreBMP,500,300
! Menu button
GR.SET.STROKE 10:GR.TEXT.SIZE 60:GR.TEXT.ALIGN 2
GR.BITMAP.CREATE Button,200,80
GR.BITMAP.DRAWINTO.START Button
	GR.COLOR 255,0,255,0,1:GR.OVAL g,5,5,195,75
	GR.COLOR 255,255,255,255,0:GR.OVAL g,5,5,195,75
	GR.COLOR 255,0,0,0,0:GR.SET.STROKE 3
	GR.TEXT.DRAW g,100,60,"MENU"
GR.BITMAP.DRAWINTO.END
! Next button
GR.BITMAP.CREATE NxtButton,200,80
GR.BITMAP.DRAWINTO.START NxtButton
	GR.COLOR 255,0,255,0,1:GR.OVAL g,5,5,195,75
	GR.COLOR 255,255,255,255,0:GR.OVAL g,5,5,195,75
	GR.COLOR 255,0,0,0,0:GR.SET.STROKE 3
	GR.TEXT.DRAW g,100,60,"NEXT"
GR.BITMAP.DRAWINTO.END
GR.TEXT.SETFONT Toxia:GR.TEXT.SIZE 40:GR.COLOR 255,255,255,255,1
GR.TEXT.DRAW g,300,40,"MONSTERS OF the LABYRINTH"
FOR i=1 TO 3
	GR.BITMAP.DRAW g,Food[i],(i-1)*200+50,650
	GR.BITMAP.DRAW Mon[i],Grimace[i],(i-1)*200+50,45
NEXT i
GR.TEXT.SIZE 30:GR.COLOR 255,255,255,255,1
GR.TEXT.DRAW g,300,230,"The monsters cannot wait to devour their food"
GR.TEXT.DRAW g,300,270,"But a labyrinth separates each monster from"
GR.TEXT.DRAW g,300,310,"his favourite snack"
GR.TEXT.DRAW g,300,350,"It is your task to show the best way through"
GR.TEXT.DRAW g,300,390,"the labyrinth 9 times in a row"
GR.TEXT.DRAW g,300,430,"Keep on tapping on one of the circles until"
GR.TEXT.DRAW g,300,470,"you reach the snack to show which steps to take"
GR.TEXT.DRAW g,300,510,"When you are done I will show you the best route"
GR.TEXT.DRAW g,300,550,"and you can advance to the next level"
GR.TEXT.DRAW g,300,620,"Tap screen to start"
GR.RENDER
GridSize=4:Counter=1:SOUND=1:GR.TEXT.SIZE 25:GR.TEXT.TYPEFACE 1:GR.TEXT.ALIGN 1
TIMER.SET 3000:GOSUB GetTouch:TIMER.CLEAR
DO
	Start$[2]="Start at Level  "+INT$(GridSize-3)+" (of 9)"
	DIALOG.SELECT Cont,Start$[],"To the MONSTERS...."
	IF Cont=1 THEN
		IF Sound=1 THEN
			Sound=0:Start$[1]="Sound is OFF"
		ELSE
			Sound=1:Start$[1]="Sound is ON"
		ENDIF
	ENDIF
	IF Cont=2 THEN
		DO
			DIALOG.SELECT A,ChLevel$[],"Start at Level"
		UNTIL A>0
		GridSize=A+3
	ENDIF
UNTIL Cont=3
DO
	GR.CLS
	IF SOUND THEN AUDIO.STOP:AUDIO.PLAY Beep
	GOSUB CreateMaze
	GOSUB DrawMaze
	GOSUB SolveMaze
	GR.BITMAP.SCALE Top[GridSize-3],Grimace[GridSize-3],CellSize,CellSize
	GR.BITMAP.SCALE Bottom[GridSize-3],Food[GridSize-3],CellSize,CellSize
	GR.BITMAP.DRAW Target,Bottom[GridSize-3],XOffSet+(X_End-1)*CellSize,YOffSet+GridSize*CellSize
	GR.BITMAP.DRAW Icon,Top[GridSize-3],XOffSet+(X_Start-1)*CellSize,YOffSet-CellSize
	GR.RENDER
	GOSUB UserDraw
	GOSUB DrawPath
	GOSUB CompareScore
	DO
		GOSUB GetTouch
	UNTIL tx>TS & tx<TS+200 & ty>710 & ty<790
	GridSize=GridSize+1
UNTIL GridSize=13
GridSize=GridSize-1
GOSUB Finish
EXIT

ONTIMER:
	FOR i=45 TO 650 STEP 25
		GR.MODIFY Mon[Counter],"y",i:GR.RENDER
	NEXT i
	Counter=Counter+1:IF Counter=4 THEN Counter=1
TIMER.RESUME

GetTouch:
	DO
		GR.TOUCH Touched,tx,ty
	UNTIL Touched
	DO
		GR.TOUCH Touched,tx,ty
	UNTIL !Touched
	tx/=sx:ty/=sy
RETURN

CreateMaze:
	IF Sound THEN AUDIO.STOP:AUDIO.PLAY Tamba:AUDIO.LOOP
	CellSize=500/GridSize:XOffSet=50:YOffSet=200
	DIM W[GridSize,GridSize]
	! Progress bar
	GR.COLOR 255,128,128,128,1:GR.RECT g,XOffSet-10,YOffSet-60,XOffSet+510,YOffSet-20
	GR.COLOR 255,255,0,0,0:GR.RECT g,XOffSet-10,YOffSet-60,XOffSet+510,YOffSet-20
	GR.COLOR 255,255,255,0,1:GR.RECT ProgBar,XOffSet,YOffSet-53,XOffSet,YOffSet-30
	GR.COLOR 255,0,0,255,1:GR.TEXT.DRAW Prep,XOffSet+100,YOffSet-35,"Preparing next maze....0%"
	GR.COLOR 255,255,255,0,1
	! Depth-first search algorithm
	FOR i=1 TO GridSize
		FOR j=1 TO GridSize
			! Set all walls
			W[j,i]=15
		NEXT j
	NEXT i
	Visited=0
	Row=GetNum(2,GridSize-1):Col=GetNum(2,GridSize-1)
	!Pick any direction. North=1 East=2 South=3 West=4
	CDir=GetNum(1,4)
	LIST.ADD MazePath,Row,Col
	! Set cell as visited
	W[Row,Col]=W[Row,Col]+16
	GR.CIRCLE g,XOffSet+Col*CellSize-CellSize/2,YOffSet+Row*CellSize-CellSize/2,5
	DO
		DE=0
		IF CDir=1 THEN
			IF  Row>1 THEN
				IF BAND(W[Row-1,Col],16)=0 THEN
					! Remove wall current
					W[Row,Col]=W[Row,Col]-1:Row=Row-1
					! Set cell as visited
					W[Row,Col]=W[Row,Col]+16:Visited=Visited+1
					! Save path for back track
					LIST.ADD MazePath,Row,Col
					! Remove wall next
					W[Row,Col]=W[Row,Col]-4
				ENDIF
			ENDIF
		ENDIF
		IF CDir=2 THEN
			IF  Col<GridSize THEN
				IF BAND(W[Row,Col+1],16)=0 THEN
					! Remove wall current
					W[Row,Col]=W[Row,Col]-2:Col=Col+1
					! Set cell as visited
					W[Row,Col]=W[Row,Col]+16:Visited=Visited+1
					! Save path for back track
					LIST.ADD MazePath,Row,Col
					! Remove wall next
					W[Row,Col]=W[Row,Col]-8
				ENDIF
			ENDIF
		ENDIF
		IF CDir=3 THEN
			IF  Row<GridSize THEN
				IF BAND(W[Row+1,Col],16)=0 THEN
					! Remove wall current
					W[Row,Col]=W[Row,Col]-4:Row=Row+1
					! Set cell as visited
					W[Row,Col]=W[Row,Col]+16:Visited=Visited+1
					! Save path for back track
					LIST.ADD MazePath,Row,Col
					! Remove wall next
					W[Row,Col]=W[Row,Col]-1
				ENDIF
			ENDIF
		ENDIF
		IF CDir=4 THEN
			IF Col>1 THEN
				IF BAND(W[Row,Col-1],16)=0 THEN
					! Remove wall current
					W[Row,Col]=W[Row,Col]-8:Col=Col-1
					! Set cell as visited
					W[Row,Col]=W[Row,Col]+16:Visited=Visited+1
					! Save path for back track
					LIST.ADD MazePath,Row,Col
					! Remove wall next
					W[Row,Col]=W[Row,Col]-2
				ENDIF
			ENDIF
		ENDIF
		GR.CIRCLE g,XOffSet+Col*CellSize-CellSize/2,YOffSet+Row*CellSize-CellSize/2,5
		GOSUB DeadEnd
		! At a dead end
		IF DE=1 THEN
			DE=0
			! Go back
			LIST.SIZE MazePath,GoBack
			IF GoBack>1 THEN
				LIST.GET MazePath,GoBack-1,Row:LIST.GET MazePath,GoBack,Col
				LIST.REMOVE MazePath,GoBack:LIST.REMOVE MazePath,GoBack-1
			ENDIF
		ELSE
			! Pick 1 of 3 random directions
			Q=NDir[CDir]
			DO
				CDir=GetNum(1,4)
			UNTIL CDir<>Q
		ENDIF
		GR.MODIFY ProgBar,"right",XOffSet+(Visited+1)*(500/(GridSize*GridSize))
		GR.MODIFY Prep,"text","Preparing next maze...."+INT$((Visited+1)/(GridSize*GridSize)*100)+"%"
		GR.RENDER
	UNTIL Visited=GridSize*GridSize-1
	LIST.CLEAR MazePath
RETURN

DeadEnd:
	! Check for dead end
	IF Row>1 & Row<GridSize & Col>1 & Col<GridSize THEN
		IF BAND(W[Row-1,Col],16)=16 & BAND(W[Row,Col+1],16)=16 & BAND(W[Row+1,Col],16)=16 & BAND(W[Row,Col-1],16)=16 THEN DE=1
	ELSE
		IF Row=1 & Col<GridSize THEN
			IF Col=1 THEN
				!Top left
				IF BAND(W[Row,Col+1],16)=16 & BAND(W[Row+1,Col],16)=16 THEN DE=1
			ELSE
				!Top
				IF BAND(W[Row,Col+1],16)=16 & BAND(W[Row+1,Col],16)=16 & BAND(W[Row,Col-1],16)=16 THEN DE=1
			ENDIF
		ENDIF
		IF Col=GridSize & Row<GridSize THEN
			IF  Row=1 THEN
				!Top Right
				IF BAND(W[Row+1,Col],16)=16 & BAND(W[Row,Col-1],16)=16 THEN DE=1
			ELSE
				!Right
				IF BAND(W[Row-1,Col],16)=16 & BAND(W[Row+1,Col],16)=16 & BAND(W[Row,Col-1],16)=16 THEN DE=1
			ENDIF
		ENDIF
		IF Row=GridSize & Col>1 THEN
			IF Col=GridSize THEN
				!Bottom Right
				IF BAND(W[Row-1,Col],16)=16 & BAND(W[Row,Col-1],16)=16 THEN DE=1
			ELSE
				!Bottom
				IF BAND(W[Row,Col+1],16)=16 & BAND(W[Row-1,Col],16)=16 & BAND(W[Row,Col-1],16)=16 THEN DE=1
			ENDIF
		ENDIF
		IF Col=1 & Row>1 THEN
			IF Row=GridSize THEN
				!Bottom left
				IF BAND(W[Row-1,Col],16)=16 & BAND(W[Row,Col+1],16)=16 THEN DE=1
			ELSE
				!Left
				IF BAND(W[Row-1,Col],16)=16 & BAND(W[Row,Col+1],16)=16 & BAND(W[Row+1,Col],16)=16 THEN DE=1
			ENDIF
		ENDIF
	ENDIF
RETURN

DrawMaze:
	! Maze Entry and Exit
	X_Start=GetNum(1,GridSize):X_End=GetNum(1,GridSize)
	W[1,X_Start]=W[1,X_Start]-1:W[GridSize,X_End]=W[GridSize,X_End]-4
	IF X_Start>=GridSize/2 THEN
		TS=50
	ELSE
		TS=450
	ENDIF
	GR.CLS
	GR.ROTATE.START 270,XOffSet-5,600:GR.TEXT.SIZE 60:GR.COLOR 255,0,255,0,1:GR.TEXT.DRAW g,XOffSet-5,600,"Level "+INT$(GridSize-3):GR.ROTATE.END
	GR.TEXT.SETFONT Toxia:GR.TEXT.SIZE 40:GR.TEXT.DRAW g,XOffSet+TS,YOffSet-120,"M"
	GR.TEXT.DRAW g,XOffSet+TS,YOffSet-70,"O":GR.TEXT.DRAW g,XOffSet+TS,YOffSet-20,"L":GR.TEXT.TYPEFACE 1
	GR.ROTATE.START 270,XOffSet-5,600:GR.TEXT.SIZE 60:GR.COLOR 255,255,255,0,0:GR.TEXT.DRAW g,XOffSet-5,600,"Level "+INT$(GridSize-3):GR.ROTATE.END
	GR.TEXT.SETFONT Toxia:GR.TEXT.SIZE 40:GR.TEXT.DRAW g,XOffSet+TS,YOffSet-120,"M"
	GR.TEXT.DRAW g,XOffSet+TS,YOffSet-70,"O":GR.TEXT.DRAW g,XOffSet+TS,YOffSet-20,"L":GR.TEXT.TYPEFACE 1
	GR.COLOR 255,92,255,92,1:GR.TEXT.SIZE 14:GR.TEXT.ALIGN 2
	GR.TEXT.DRAW g,XOffSet+TS+20,YOffSet-110,"monsters":GR.TEXT.DRAW g,XOffSet+TS+15,YOffSet-60,"of the":GR.TEXT.DRAW g,XOffSet+TS+10,YOffSet-10,"labyrinth"
	GR.COLOR 255,255,255,0,1:GR.TEXT.SIZE 25:GR.TEXT.ALIGN 1
	FOR i=1 TO GridSize
		FOR j=1 TO GridSize
			! Draw north wall
			IF BAND(W[i,j],1)=1 THEN GR.LINE g,XOffSet+(j-1)*CellSize,YOffSet+(i-1)*CellSize,XOffSet+j*CellSize,YOffSet+(i-1)*CellSize
			! Draw east wall
			IF BAND(W[i,j],2)=2 THEN GR.LINE g,XOffSet+j*CellSize,YOffSet+(i-1)*CellSize,XOffSet+j*CellSize,YOffSet+i*CellSize
			! Draw south wall
			IF BAND(W[i,j],4)=4 THEN GR.LINE g,XOffSet+(j-1)*CellSize,YOffSet+i*CellSize,XOffSet+j*CellSize,YOffSet+i*CellSize
			! Draw west wall
			IF BAND(W[i,j],8)=8 THEN GR.LINE g,XOffSet+(j-1)*CellSize,YOffSet+(i-1)*CellSize,XOffSet+(j-1)*CellSize,YOffSet+i*CellSize
		NEXT j
	NEXT i
RETURN

SolveMaze:
	! Right hand rule
	LIST.CLEAR PCoords
	GoDir=3
	StX=XOffSet+CellSize/2+(X_Start-1)*CellSize:StY=YOffSet+CellSize/2
	LIST.ADD PCoords,INT(StX),INT(StY)
	GR.COLOR 255,255,255,0,1
	DO
		IF GoDir=3 THEN
		LuX=ROUND((StX-XOffSet+CellSize/2)/CellSize):LuY=ROUND((StY-YOffSet+CellSize/2)/CellSize)
			IF BAND(W[LuY,LuX],8)=0 THEN
				StX=StX-CellSize:GoDir=4
			ELSE
				IF BAND(W[LuY,LuX],4)=0 THEN
					StY=StY+CellSize
				ELSE
					IF BAND(W[LuY,LuX],2)=0 THEN
						StX=StX+CellSize:GoDir=2
					ELSE
						StY=StY-CellSize:GoDir=1
					ENDIF
				ENDIF
			ENDIF
			LIST.ADD PCoords,INT(StX),INT(StY)
		ENDIF
		IF GoDir=4 THEN
		LuX=ROUND((StX-XOffSet+CellSize/2)/CellSize):LuY=ROUND((StY-YOffSet+CellSize/2)/CellSize)
			IF BAND(W[LuY,LuX],1)=0 THEN
				StY=StY-CellSize:GoDir=1
			ELSE
				IF BAND(W[LuY,LuX],8)=0 THEN
					StX=StX-CellSize
				ELSE
					IF BAND(W[LuY,LuX],4)=0 THEN
						StY=StY+CellSize:GoDir=3
					ELSE
						StX=StX+CellSize:GoDir=2
					ENDIF
				ENDIF
			ENDIF
			LIST.ADD PCoords,INT(StX),INT(StY)
		ENDIF
		IF GoDir=1 THEN
		LuX=ROUND((StX-XOffSet+CellSize/2)/CellSize):LuY=ROUND((StY-YOffSet+CellSize/2)/CellSize)
			IF BAND(W[LuY,LuX],2)=0 THEN
				StX=StX+CellSize:GoDir=2
			ELSE
				IF BAND(W[LuY,LuX],1)=0 THEN
					StY=StY-CellSize
				ELSE
					IF BAND(W[LuY,LuX],8)=0 THEN
						StX=StX-CellSize:GoDir=4
					ELSE
						StY=StY+CellSize:GoDir=3
					ENDIF
				ENDIF
			ENDIF
			LIST.ADD PCoords,INT(StX),INT(StY)
		ENDIF
		IF GoDir=2 THEN
		LuX=ROUND((StX-XOffSet+CellSize/2)/CellSize):LuY=ROUND((StY-YOffSet+CellSize/2)/CellSize)
			IF BAND(W[LuY,LuX],4)=0 THEN
				StY=StY+CellSize:GoDir=3
			ELSE
				IF BAND(W[LuY,LuX],2)=0 THEN
					StX=StX+CellSize
				ELSE
					IF BAND(W[LuY,LuX],1)=0 THEN
						StY=StY-CellSize:GoDir=1
					ELSE
						StX=StX-CellSize:GoDir=4
					ENDIF
				ENDIF
			ENDIF
			LIST.ADD PCoords,INT(StX),INT(StY)
		ENDIF
	UNTIL StY<YOffSet | StY>YOffSet+GridSize*CellSize
	IF StY<YOffSet THEN RETURN
	LIST.SIZE PCoords,SolveNum
	FOR j=1 TO 2
		SL=1:TS=1
		DO
			LIST.GET PCoords,SL,StX:LIST.GET PCoords,SL+1,StY
			DO
				LIST.SEARCH PCoords,StX,Found,SL+2
				IF Found & (Found/2<>INT(Found/2)) THEN
					LIST.GET PCoords,Found,FStX
					LIST.GET PCoords,Found+1,FStY
					IF (StX=FStX) & (StY=FStY) THEN
						FOR i=TS+2 TO Found+1
							LIST.REPLACE PCoords,i,0
						NEXT i
						TS=Found
					ENDIF
				ENDIF
				SL=SL+2
			UNTIL ((StX=FStX) & (StY=FStY)) | SL>=SolveNum-1
			TS=TS+2:SL=TS
		UNTIL TS>=SolveNum-1
	NEXT j
	AUDIO.STOP
RETURN

DrawPath:
	GR.SET.STROKE 3:GR.COLOR 255,255,255,255,1:GR.TEXT.DRAW G2,300,40,"Steps needed "+INT$(0):GR.COLOR 255,255,0,0,1
	OldStX=XOffSet+CellSize/2+(X_Start-1)*CellSize:OldStY=YOffSet
	IF X_End>=GridSize/1.99 THEN
		TS=50
	ELSE
		TS=350
	ENDIF
	Shortest=0
	FOR i=1 TO SolveNum STEP 2
		LIST.GET PCoords,i,StX:LIST.GET PCoords,i+1,StY
		IF StX<>0 THEN
			IF SOUND THEN AUDIO.STOP:AUDIO.PLAY Beep
			Shortest=Shortest+1
			GR.MODIFY G2,"text","Steps needed "+INT$(Shortest)
			IF i<SolveNum-1 THEN
				GR.LINE g,OldStX,OldStY,StX,StY
			ELSE
				GR.LINE g,OldStX,OldStY,StX,StY-CellSize/2
			ENDIF
			GR.MODIFY Icon,"x",StX-CellSize/2,"y",StY-CellSize/2:GR.RENDER:OldStX=StX:OldStY=StY
			PAUSE 100
		ENDIF
	NEXT i
	GR.HIDE Target:GR.BITMAP.DRAW ShowNxt,NxtButton,TS,710:GR.RENDER
RETURN

UserDraw:
	CurX=XOffSet+CellSize/2+(X_Start-1)*CellSize:CurY=YOffSet-CellSize/2
	IF X_End>=GridSize/1.99 THEN
		TS=50
	ELSE
		TS=350
	ENDIF
	GR.SET.STROKE 3:GR.COLOR 255,255,0,255,0:GR.CIRCLE Pg1,-CellSize,0,(CellSize/2)*0.8
	GR.CIRCLE Pg2,-CellSize,0,(CellSize/2)*0.8
	GR.CIRCLE Pg3,-CellSize,0,(CellSize/2)*0.8
	GR.CIRCLE Pg4,-CellSize,0,(CellSize/2)*0.8
	UserSteps=1:GiveUp=0
	GR.COLOR 255,255,255,0,1:GR.TEXT.DRAW SU,50,40,"Steps taken "+INT$(0)
	GR.MODIFY Pg4,"x",CurX,"y",CurY+CellSize
	GR.BITMAP.DRAW gBut,Button,TS,710:GR.RENDER
NoAction:
	Choice=0
	DO
		GOSUB GetTouch
	UNTIL (tx<CurX+CellSize/2 & tx>CurX-CellSize/2 & ty<CurY+CellSize*1.5 & ty>CurY+CellSize/2) | (tx>TS & tx<TS+200 & ty>710 & ty<790)
	IF ty>710 THEN
		IF SOUND THEN AUDIO.STOP:AUDIO.PLAY Beep
		DO
			DIALOG.SELECT Choice,Menu$[],"MENU"
		UNTIL Choice>0
		IF Choice=1 THEN GR.SET.STROKE 3:GiveUp=1:GR.HIDE Pg4:GR.HIDE gBut:RETURN:! Solve maze
		IF Choice=2 THEN GridSize=GridSize-1:GOSUB Finish:EXIT
		IF Choice=3 THEN GOTO NoAction
	ENDIF
	!GO South
	GR.COLOR 92,0,0,255,1:GR.CIRCLE CuP,CurX,CurY+CellSize,(CellSize/2)*0.5
	IF SOUND THEN AUDIO.STOP:AUDIO.PLAY Woosh
	GR.COLOR 255,255,255,255,1:GR.SET.STROKE 10:GR.LINE g,CurX,CurY+CellSize/2,CurX,CurY+CellSize
	GR.MODIFY SU,"text","Steps taken "+INT$(UserSteps)
	CurY=CurY+CellSize
	DO
		! Show current position
		GR.MODIFY CuP,"x",CurX,"y",CurY
		! Show possible moves
		LuX=ROUND((CurX-XOffSet+CellSize/2)/CellSize):LuY=ROUND((CurY-YOffSet+CellSize/2)/CellSize)
		! Left possible
		IF BAND(W[LuY,LuX],8)=0 THEN
			GR.MODIFY Pg1,"x",CurX-CellSize,"y",CurY:GR.SHOW Pg1
		ELSE
			GR.HIDE Pg1
		ENDIF
		! Right possible
		IF BAND(W[LuY,LuX],2)=0 THEN
			GR.MODIFY Pg2,"x",CurX+CellSize,"y",CurY:GR.SHOW Pg2
		ELSE
			GR.HIDE Pg2
		ENDIF
		! Up possible
		IF LuY>1 THEN
			IF BAND(W[LuY,LuX],1)=0 THEN
				GR.MODIFY Pg3,"x",CurX,"y",CurY-CellSize:GR.SHOW Pg3
			ELSE
				GR.HIDE Pg3
			ENDIF
		ELSE
			GR.HIDE Pg3
		ENDIF
		! Down possible
		IF LuY<GridSize+1 THEN
			IF BAND(W[LuY,LuX],4)=0 THEN
				GR.MODIFY Pg4,"x",CurX,"y",CurY+CellSize:GR.SHOW Pg4
			ELSE
				GR.HIDE Pg4
			ENDIF
		ELSE
			GR.HIDE Pg4
		ENDIF
		GR.RENDER
Nothing:
		Choice=0
		GOSUB GetTouch
		IF tx>TS & tx<TS+200 & ty>710 & ty<790 THEN
			IF SOUND THEN AUDIO.STOP:AUDIO.PLAY Beep
			DO
				DIALOG.SELECT Choice,Menu$[],"MENU"
			UNTIL Choice>0
			IF Choice=1 THEN GR.SET.STROKE 3:GiveUp=1:GR.HIDE gBut:D_U.BREAK:! Solve maze
			IF Choice=2 THEN GridSize=GridSize-1:GOSUB Finish:EXIT
			IF Choice=3 THEN GOTO Nothing
		ENDIF
		IF tx>XOffSet & tx<ScaleX-XOffSet & ty>YOffSet & ty<YOffSet+CellSize*(GridSize+1) THEN
			IF tx<CurX-CellSize/2 & tx>CurX-CellSize*1.5 & ty>CurY-CellSize/2 & ty<CurY+CellSize/2 THEN
				IF BAND(W[LuY,LuX],8)=0 THEN
					!GO Left
					GR.GET.PIXEL (CurX-CellSize/2)*sx,CurY*sy,Alpha,MRed,MGreen,MBlue
					IF MRed>0 THEN
						GR.COLOR 255,0,255,255,1
					ELSE
						IF MGreen=0 THEN GR.COLOR 255,255,255,255,1
					ENDIF
					IF SOUND THEN AUDIO.STOP:AUDIO.PLAY Woosh
					GR.LINE g,CurX,CurY,CurX-CellSize,CurY
					CurX=CurX-CellSize
					UserSteps=UserSteps+1
				ENDIF
			ENDIF
			IF tx>CurX+CellSize/2 & tx<CurX+CellSize*1.5 & ty>CurY-CellSize/2 & ty<CurY+CellSize/2 THEN
				IF BAND(W[LuY,LuX],2)=0 THEN
					!GO Right
					GR.GET.PIXEL (CurX+CellSize/2)*sx,CurY*sy,Alpha,MRed,MGreen,MBlue
					IF MRed>0 THEN
						GR.COLOR 255,0,255,255,1
					ELSE
						IF MGreen=0 THEN GR.COLOR 255,255,255,255,1
					ENDIF
					IF SOUND THEN AUDIO.STOP:AUDIO.PLAY Woosh
					GR.LINE g,CurX,CurY,CurX+CellSize,CurY
					CurX=CurX+CellSize
					UserSteps=UserSteps+1
				ENDIF
			ENDIF
			IF tx<CurX+CellSize/2 & tx>CurX-CellSize/2 & ty>CurY-CellSize*1.5 & ty<CurY-CellSize/2 THEN
				IF BAND(W[LuY,LuX],1)=0 THEN
					!GO North
					GR.GET.PIXEL CurX*sx,(CurY-CellSize/2)*sy,Alpha,MRed,MGreen,MBlue
					IF MRed>0 THEN
						GR.COLOR 255,0,255,255,1
					ELSE
						IF MGreen=0 THEN GR.COLOR 255,255,255,255,1
					ENDIF
					IF SOUND THEN AUDIO.STOP:AUDIO.PLAY Woosh
					GR.LINE g,CurX,CurY,CurX,CurY-CellSize
					CurY=CurY-CellSize
					UserSteps=UserSteps+1
				ENDIF
			ENDIF
			IF tx<CurX+CellSize/2 & tx>CurX-CellSize/2 & ty<CurY+CellSize*1.5 & ty>CurY+CellSize/2 THEN
				IF LuY<GridSize+1 THEN
					!GO South
					GR.GET.PIXEL CurX*sx,(CurY+CellSize/2)*sy,Alpha,MRed,MGreen,MBlue
					IF MRed>0 THEN
						GR.COLOR 255,0,255,255,1
					ELSE
						IF MGreen=0 THEN GR.COLOR 255,255,255,255,1
					ENDIF
					IF SOUND THEN AUDIO.STOP:AUDIO.PLAY Woosh
					IF CurY<(GridSize-1)*CellSize+YOffSet THEN
						GR.LINE g,CurX,CurY,CurX,CurY+CellSize
					ELSE
						GR.LINE g,CurX,CurY,CurX,CurY+CellSize/2
					ENDIF
					CurY=CurY+CellSize
					UserSteps=UserSteps+1
				ENDIF
			ENDIF
			GR.MODIFY SU,"text","Steps taken "+INT$(UserSteps)
			GR.RENDER
		ENDIF
	UNTIL CurY>GridSize*CellSize+YOffSet
Skip:
	GR.HIDE CuP:GR.HIDE Pg1:GR.HIDE Pg2:GR.HIDE Pg3:GR.HIDE Pg4:GR.HIDE gBut:GR.RENDER
RETURN

CompareScore:
	!Compare Shortest,UserSteps
	GR.BITMAP.DRAWINTO.START ScoreBMP
		GR.COLOR 255,0,0,0,1:GR.RECT g,0,0,500,300
		GR.COLOR 255,255,255,0,0:GR.SET.STROKE 10:GR.RECT g,20,20,480,280
		GR.SET.STROKE 3:GR.RECT g,0,0,500,300
		GR.BITMAP.DRAW g,Grimace[GridSize-3],130,30
		GR.COLOR 255,255,255,255,1:GR.TEXT.SETFONT Toxia:GR.TEXT.ALIGN 2:GR.TEXT.SIZE 50
		IF GiveUp THEN
			GR.TEXT.DRAW g,250,210,"You gave up"
			F$[GridSize-3]="Level "+INT$(GridSize-3)+" You gave up ("+INT$(Shortest)+" steps for me)"
		ELSE
			GR.TEXT.DRAW g,250,210,"You needed "+INT$(UserSteps)+" steps"
			F$[GridSize-3]="Level "+INT$(GridSize-3)+" Steps "+INT$(UserSteps)+"/"+INT$(Shortest)+" = "+LEFT$(STR$(UserSteps-Shortest),-2)+" steps more than me"
		ENDIF
		GR.TEXT.DRAW g,250,260,"I showed you "+INT$(Shortest)+" steps"
		GR.TEXT.SIZE 25:GR.TEXT.TYPEFACE 1:GR.TEXT.ALIGN 1
	GR.BITMAP.DRAWINTO.END
	GR.HIDE ShowNxt
	GR.BITMAP.DRAW Sg,ScoreBMP,XOffSet,300:GR.RENDER
	PAUSE 5000
	GR.SHOW ShowNxt:GR.HIDE Sg:GR.RENDER
RETURN

Finish:
	! End game
	GR.CLS
	GR.TEXT.SETFONT Toxia:GR.TEXT.SIZE 40:GR.COLOR 255,255,255,255,1
	GR.TEXT.DRAW g,50,50,"Well Done"
	GR.TEXT.DRAW g,50,100,"You have played "+INT$(GridSize-3)+" levels"
	GR.TEXT.DRAW g,50,150,"With the following results"
	GR.TEXT.DRAW g,100,690,"The Monsters say"
	GR.TEXT.DRAW g,100,740,"SEE YOU NEXT TIME"
	GR.TEXT.DRAW g,100,790,"tap screen to exit"
	GR.TEXT.TYPEFACE 1:GR.TEXT.SIZE 25:GR.COLOR 255,255,255,0,1
	FOR i=1 TO 9
		GR.TEXT.DRAW g,20,170+i*50,F$[i]
	NEXT i
	GR.RENDER
	GOSUB GetTouch
	WAKELOCK 5
RETURN
