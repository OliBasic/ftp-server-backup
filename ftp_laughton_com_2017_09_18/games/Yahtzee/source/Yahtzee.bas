! Yatzee
! Aat Don 2014
FN.DEF PlayNote(Note,Duration,SndOn)
	IF SndOn=1 THEN	TONE 880*2^(Note/12),Duration,0
FN.END
Rules$="<html><head><title>Yahtzee Rules</title><style>#title{font-size:150%;font-weight:bold;text-decoration: underline;color:orange;}"
Rules$=Rules$+"#category{font-weight:bold;text-decoration: underline;color:green;}#progstart{font-weight:bold;color:blue;text-align:center;}"
Rules$=Rules$+"#Button{color:black;background-color:rgb(128,255,128);font-weight:bold;font-size:150%;margin-left:45%;}table{table-layout:fixed;font-weight:bold;color:blue;}td{width:10%;text-align:center;}</style>"
Rules$=Rules$+"<script>function doDataLink(data){Android.dataLink(data);}</script></head>"
Rules$=Rules$+"<body bgcolor='white'><font color='0000ff'><a href='top'></a>Since many variants of</font> YAHTZEE <font color='0000ff'>exist,"
Rules$=Rules$+"the rules for this app are extensively given below.<br>Rules are mixed with directions you'll need to play this App.</font><br>"
Rules$=Rules$+"<hr>BLACK TEXT literally from <a href='http://www.yahtzee.org.uk/rules.html' target='_blank'>www.yahtzee.org.uk</a>, <font color='0000ff'>BLUE TEXT</font> are directions for this App !<hr>"
Rules$=Rules$+"<div id='progstart'><h1>App start (schematic)</h1><table><tr><td>1.</td><td></td><td>Enter number of Players (1-4)</td><td>or show this page</td></tr><tr><td></td></tr>"
Rules$=Rules$+"<tr><td>2.</td><td>Enter names</td><td></td><td>Choose names from DataBase</td></tr><tr><td></td></td><tr><td>3.</td><td>Start</td><td>Shuffle names</td><td>Options</td></tr><tr><td></td></tr>"
Rules$=Rules$+"<tr><td>4.</td><td></td><td></td><td>View Scores</td><td>Sound On/Off</td></tr></table></div><br>"
Rules$=Rules$+"<font color='0000ff'>Note 1: duplicate names will Not be added to the database ! So choose unique names....<br>"
Rules$=Rules$+"Note 2: Choosing 'ANDROID' as player from the database OR entering the name 'Android' as a player<br>will add a virtual opponent to the game !!!</font><br>"
Rules$=Rules$+"<font color='ff0000'>So starting a game with only one player whose name is 'Android' will give you a DEMO of the game....</font><br><hr><br>"
Rules$=Rules$+"<input id='Button' type='button' value='DONE' onClick='doDataLink("+CHR$(34)+"OK"+CHR$(34)+")'/><br><br>"
Rules$=Rules$+"<nav><a href='#object'>Object of the Game</a> | <a href='#game'>Game Start</a> | <a href='#score'>Scoring</a> | "
Rules$=Rules$+"<a href='#upper'>Upper Section Scoring</a> | <a href='#lower'>Lower Section Scoring</a> | <a href='#dump'>Scratch scores</a></nav><h1>Yahtzee Rules</h1><br>"
Rules$=Rules$+"<a id='object'><div id='title'>Object of the game</div></a>The object of Yahtzee is to obtain the highest score from throwing 5 dice.<br>"
Rules$=Rules$+"The game consists of 13 rounds. In each round, you roll the dice and then score the roll in one of 13 categories.<br>"
Rules$=Rules$+"You must score once in each category. The score is determined by a different rule for each category.<br>The game ends once all 13 categories have been scored."
Rules$=Rules$+"<a id='game'><div id='title'>Game Start</div></a>To start with, roll all the dice. After rolling you can either score the current roll (see below), or re-roll any or all of the dice.<br>"
Rules$=Rules$+"You may only roll the dice a total of 3 times. After rolling 3 times you must choose a category to score.<br>"
Rules$=Rules$+"You may score the dice at any point in the round, i.e. it doesn't have to be after the 3rd roll.<br>"
Rules$=Rules$+"<font color='0000ff'><hr>When it's your turn tap the column below your name (highlighted green) to do the first casting.<br>5 Dice are rolled and then shown sorted, next "
Rules$=Rules$+"to the score sheet.<br>Tap on the dice you want to keep (tap again to unselect). Then tap the column below your name, to perform the next casting.<br>"
Rules$=Rules$+"The name of the Player currently playing is highlighted in red.<br>If a player is satisfied with a result after one or two castings, tap (select) all dice to enter the score.<hr></font>"
Rules$=Rules$+"<a id='score'><div id='title'>Scoring</div></a>To score your combination of 5 dice, you click one of the 13 boxes, or write it on the "
Rules$=Rules$+"scorecard (scoresheet).<br>There are two sections to the score table - the Upper Section and the Lower Section.<br>"
Rules$=Rules$+"Once a box has been scored, it cannot be scored again for the rest of the game (except the Yahtzee category), so choose wisely.<br>"
Rules$=Rules$+"<font color='0000ff'><hr>You record a score by tapping (yellow highlight) on a category and then tap your score column.<br>If <mark>possible</mark> "
Rules$=Rules$+"the app will highlight a suggested category. If you agree you only have to tap your score column.<br>Mind you, these suggestions are not perfect, hence not always the best way to score !<hr></font>"
Rules$=Rules$+"<a id='upper'><div id='title'>Upper Section Scoring</div></a>If you score in the upper section of the table, your score is the total of the specified dice face.<br>"
Rules$=Rules$+"So if you roll: 5 - 2 - 5 - 6 - 5<br>and score in the Fives category, your total for the category would be 15, because there are three fives, which are added together.<br>"
Rules$=Rules$+"If the One, Three or Four Categories were selected for scoring with this roll, you would score a zero.<br>If placed in the Two or Six category, you would score 2 and 6 respectively.<br>"
Rules$=Rules$+"Bonus<br> If the total of Upper scores is 63 or more, add a bonus of 35. Note that 63 is the total of three each of 1s, 2s, 3s, 4s, 5s and 6s.<a id='lower'><div id='title'>Lower Section Scoring</div>"
Rules$=Rules$+"In the lower scores, you score either a set amount, or zero if you don't satisfy the category requirements."
Rules$=Rules$+"<div id='category'>3 and 4 of a kind</div> For 3 of a kind you must have at least 3 of the same dice faces. You score the total of all the dice.<br>For 4 of a kind you would need 4 dice faces the same."
Rules$=Rules$+"<div id='category'>Small and Large Straight</div> A Straight is a sequence of consecutive dice faces, where a smallstraight is 4 consecutive faces, and a large straight 5 consecutive faces.<br>"
Rules$=Rules$+"Small straights score 30 and a large 40 points.<br>So if you rolled: 2 - 3 - 2 - 5 - 4<br>you could score 30 in small straight or 0 in large straight."
Rules$=Rules$+"<div id='category'>Full House</div> A Full House is where you have 3 of a kind and 2 of a kind. Full houses score 25 points.<br>i.e.: 3 - 3 - 2 - 3 - 2<br>would score 25 in the Full House category."
Rules$=Rules$+"<div id='category'>First Yahtzee</div> A Yahtzee is 5 of a kind and scores 50 points, although you may elect NOT to "
Rules$=Rules$+"score it as a yahtzee,<br>instead choosing to take it as a top row score and safegaurd your bonus."
Rules$=Rules$+"<div id='category'>Additional Yahtzees.</div> If you roll a second Yahtzee in a game, and you scored your first "
Rules$=Rules$+"yahtzee in the Yahtzee box,<br>you would score a further bonus 100 points in the yahtzee box.<br>You must also put this roll into another category, as follows;<br>"
Rules$=Rules$+"-If the corresponding Upper section category is not filled then you must score there.<br>i.e. if you rolled 4 - 4 - 4 - 4 - 4<br>and the Fours Category is not filled, you must put the score in "
Rules$=Rules$+"the Fours category.<br>-If the corresponding Upper section category is filled you may then put the score anywhere on "
Rules$=Rules$+"the Upper Section (scoring zero).<br>In 3 of a Kind, 4 of a Kind, and Chance categories you would score the total of the dice faces.<br>For the Small Straight, Large Straight, and Full House "
Rules$=Rules$+"categories, you would score 30, 40 and 25 points respectively.<br><font color='0000ff'><hr>This rule will be automatically applied by the app.<hr></font>"
Rules$=Rules$+"<div id='category'>Chance</div> You can roll anything and be able to put it in the Chance category. You score the total of the dice faces.<a id='dump'><div id='title'>Scratch or Dump scores.</div></a>"
Rules$=Rules$+"You can score any roll in any category at any time, even if the resulting score is zero.<br>Eg, you can take 2-3-3-4-6 in the 5's category. It will score 0.<br>This could be used near the end of a "
Rules$=Rules$+"game to lose a poor roll against a difficult-to-get category that you've failed to fill<br>(eg, long "
Rules$=Rules$+"straight or yahtzee).<font color='0000ff'><hr>The highest score you can possibly achieve in this variant of </font>YAHTZEE<font color='0000ff'> is 1575 points.<hr></font><a href='#top'>Go to top</a>"
Rules$=Rules$+"<input id='Button' type='button' value='DONE' onClick='doDataLink("+CHR$(34)+"OK"+CHR$(34)+")'/></body></html>"
GR.OPEN 255,0,0,0,0,0
PAUSE 1000
GR.SCREEN w,h
ScaleX=900
ScaleY=h/w*ScaleX
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
WAKELOCK 3
Zoom$=STR$(0.8)
SndOn=1
DBPath$="../../Yahtzee/databases/"
DataPath$="../../Yahtzee/data/"
DIM DiceFace$[6,2],Cast[5],CastSave[5],DiceKeep[5],DicePos[16],PosCoords[16,2],Dice[6],ViewDice[6],smallDice[6],Score[4,21],Joker[4],ScorePrnt[4,21],Rank[4],Names$[4],Intro[6],roll[5]
ARRAY.LOAD Players$[],"One Player (solo)","Two Players (a pair)","Three Players (a triplet)","Four Players (max.)","Hey, show Rules/Directions first !"
LIST.CREATE N,DrawDice
LIST.ADD DrawDice,5,1,46,1,47,2,48,2,49,3,49,4,50,5,50,46,49,47,49,48,48,49,47,49,46,50,5,50,4,49,3,49,2,48,2,47,1,46,1,5,2,4,2,3,3,2,4,2
FONT.LOAD Fnt1,DataPath$+"King.ttf"
FOR i=1 TO 6
	READ.NEXT DiceFace$[i,1],DiceFace$[i,2]
NEXT i
! Landing positions for dice
k=0
FOR i=-400 TO 50 STEP 150
	FOR j=100 TO 550 STEP 150
		k=k+1
		DicePos[k]=k
		PosCoords[k,1]=i
		PosCoords[k,2]=j
	NEXT j
NEXT i
! Sounds
FOR i=1 TO 5
	AUDIO.LOAD roll[i],DataPath$+"dice"+INT$(i)+".mp3"
NEXT i
AUDIO.LOAD joy,DataPath$+"joy.mp3"
AUDIO.STOP
! Load/create Database
DBName$=DBPath$+"YahtzeeBase.db"
TableName$="Yahtzee"
C1$="Names"
C2$="Score"
C3$="PlayDate"
FILE.EXISTS DBPresent,DBName$
SQL.OPEN DB_Ptr,DBName$
IF !DBPresent THEN
	SQL.NEW_TABLE DB_Ptr,TableName$,C1$,C2$,C3$
	SQL.INSERT DB_Ptr,TableName$,C1$,"ANDROID",C2$,"0"
ENDIF
! Hi score
GR.SET.STROKE 21
GR.BITMAP.CREATE Hi,700,300
GR.BITMAP.DRAWINTO.START Hi
	! Rainbow
	GR.COLOR 255,255,0,0,0
	GR.CIRCLE g,350,270,250
	GR.COLOR 255,255,127,0,0
	GR.CIRCLE g,350,270,230
	GR.COLOR 255,255,255,0,0
	GR.CIRCLE g,350,270,210
	GR.COLOR 255,0,255,0,0
	GR.CIRCLE g,350,270,190
	GR.COLOR 255,0,128,255,0
	GR.CIRCLE g,350,270,170
	GR.COLOR 255,0,0,128,0
	GR.CIRCLE g,350,270,150
	GR.COLOR 255,128,0,128,0
	GR.CIRCLE g,350,270,130
	GR.COLOR 255,0,0,0,1
	GR.RECT g,0,220,700,300
	! clouds
	GR.COLOR 255,255,255,255,1
	FOR i=100 TO 600 STEP 50
		GR.CIRCLE g,i,220,50-(ABS(i-350)/15)+RND()*20
	NEXT i
	GR.SET.STROKE 1
	GR.TEXT.SIZE 50
	GR.COLOR 255,0,0,0,1
	GR.TEXT.DRAW g,120,240,"NEW HIGH SCORE !!!"
GR.BITMAP.DRAWINTO.END
! Create Dice
GR.SET.STROKE 0
FOR i=1 TO 6
	GR.BITMAP.CREATE ViewDice[i],51,51
	GR.BITMAP.DRAWINTO.START ViewDice[i]
		GR.SET.STROKE 1
		GR.COLOR 255,204,204,204,1
		GR.POLY g,DrawDice,0,0
		GR.COLOR 255,0,0,0,0
		GR.POLY g,DrawDice,0,0
		FOR j=0 TO 20
			GR.COLOR 255,204-j*2,204-j*2,204-j*2,1
			GR.SET.STROKE 20-j
			GR.LINE g,10-j/4,40+j/4,40+j/4,10-j/4
		NEXT j
		GR.SET.STROKE 0
		IF i=1 | i=3 | i=5 THEN
			GR.COLOR 255,204,0,0,1
			GR.CIRCLE g,25,25,6
			GR.COLOR 255,255,0,0,1
			GR.CIRCLE g,25,25,5
		ENDIF
		IF i=2 | i=4 | i=5 | i=6 THEN
			GR.COLOR 255,204,0,0,1
			GR.CIRCLE g,12,39,6
			GR.CIRCLE g,39,12,6
			GR.COLOR 255,255,0,0,1
			GR.CIRCLE g,12,39,5
			GR.CIRCLE g,39,12,5
		ENDIF 
		IF i=3 | i=4 | i=5 | i=6 THEN
			GR.COLOR 255,204,0,0,1
			GR.CIRCLE g,12,12,6
			GR.CIRCLE g,39,39,6
			GR.COLOR 255,255,0,0,1
			GR.CIRCLE g,12,12,5
			GR.CIRCLE g,39,39,5
		ENDIF 
		IF i=6 THEN
			GR.COLOR 255,204,0,0,1
			GR.CIRCLE g,12,25,6
			GR.CIRCLE g,39,25,6
			GR.COLOR 255,255,0,0,1
			GR.CIRCLE g,12,25,5
			GR.CIRCLE g,39,25,5
		ENDIF 
	GR.BITMAP.DRAWINTO.END
	GR.BITMAP.SCALE smallDice[i],ViewDice[i],22,22
NEXT i
! Create Joker
GR.SET.STROKE 0
GR.SET.ANTIALIAS 0
GR.BITMAP.CREATE Nar,18,23
GR.BITMAP.DRAWINTO.START Nar
	GR.COLOR 255,255,0,0,1
	DO
		READ.NEXT x,y
		GR.POINT g,x,y
	UNTIL x=12 & y=8
	GR.COLOR 255,0,0,0,1
	DO
		READ.NEXT x,y
		GR.POINT g,x,y
	UNTIL x=9 & y=16
	GR.COLOR 255,255,0,255,1
	DO
		READ.NEXT x,y
		GR.POINT g,x,y
	UNTIL x=9 & y=22
	GR.COLOR 255,200,20,20,1
	GR.LINE g,8,12,10,12
	GR.POINT g,9,13
	GR.COLOR 255,0,0,255,1
	GR.POINT g,8,9
	GR.POINT g,10,9
GR.BITMAP.DRAWINTO.END
GR.SET.ANTIALIAS 1
! Create selection bar
GR.BITMAP.CREATE Bar,170,23
GR.COLOR 128,255,255,0,1
GR.BITMAP.DRAWINTO.START Bar
	GR.RECT g,0,0,170,23
	GR.COLOR 228,255,0,0,1
	GR.RECT g,0,0,5,23
	GR.RECT g,165,0,170,23
GR.BITMAP.DRAWINTO.END
! Create player bar
GR.BITMAP.CREATE PlayBar,100,506
GR.BITMAP.DRAWINTO.START PlayBar
	GR.COLOR 92,255,0,0,1
	GR.RECT g,0,0,100,23
	GR.COLOR 92,0,255,0,1
	GR.RECT g,0,24,100,506
GR.BITMAP.DRAWINTO.END
! Get number of players/names
StartGame:
GOSUB GetNames
GR.CLS
GR.TEXT.SIZE 30
GR.COLOR 255,255,255,0,1
i=3
GOSUB GetHiScores
GR.COLOR 255,0,0,0,1
GR.RECT cover,0,185,ScaleX,ScaleY
GR.HIDE cover
GR.RENDER
FOR i=1 TO 6
	GR.BITMAP.DRAW Intro[i],ViewDice[INT(1+RND()*6)],400+RND()*200,30+RND()*90
NEXT i
Y$="Yahtzee"
GR.SET.STROKE 3
GR.TEXT.SETFONT Fnt1
GR.TEXT.SIZE 100
GR.COLOR 255,255,0,255,1
GR.TEXT.DRAW g,50,100,Y$
GR.COLOR 255,0,255,255,0
GR.TEXT.DRAW g,50,100,Y$
GR.RENDER
GR.SHOW cover
GR.COLOR 255,0,255,255,0
GR.TEXT.DRAW gt,320,350,"Let's GO !!!"
GR.HIDE gt
GR.TEXT.TYPEFACE
GR.TEXT.SIZE 50
GR.COLOR 255,255,255,0,1
GR.SET.STROKE 15
GR.LINE g,20,130,20,520
GR.LINE g,0,500,740,500
GR.LINE g,720,50,720,520
GR.SET.STROKE 3
GR.COLOR 255,0,0,0,1
GR.LINE g,20,130,20,520
GR.LINE g,0,500,740,500
GR.LINE g,720,50,720,520
GR.RENDER
GR.COLOR 255,255,255,0,0
Rules:
DIALOG.SELECT NumOfPlayers,Players$[],"Please, enter number of players...."
IF NumOfPlayers=5 THEN
	GOSUB HELP
	GOTO Rules
ENDIF
IF NumOfPlayers=0 THEN GOTO Rules
GR.TEXT.DRAW g,150,170,"Number of Players = "+INT$(NumOfPlayers)
GR.RENDER
GR.COLOR 255,230,230,230,1
GR.TEXT.SETFONT "monospace","BI"
GR.TEXT.SIZE 35
IF NumOfNames>=NumOfPlayers THEN
	DIALOG.MESSAGE "Number of names in DBase is "+INT$(NumOfNames),"Select names from DBase ?",Begin,"YES","NO, Enter new names"
	IF Begin<2 THEN
		FOR i=1 TO NumOfPlayers
			DO
				DIALOG.SELECT sel,AllNames,"Please, select a name...."
			UNTIL sel>0
			LIST.GET AllNames,sel,Names$[i]
			LIST.REMOVE AllNames,sel
			GR.TEXT.DRAW g,30,230+i*50,"#"+INT$(i)+": "+Names$[i]
			GR.RENDER
		NEXT i
	ENDIF
ELSE
	Begin=2
ENDIF
IF Begin=2 THEN
	FOR i=1 TO NumOfPlayers
		INPUT "Enter name of Player "+INT$(i)+" (max.8 chars)",Names$[i],"Player "+INT$(i),Cancel
		IF LEN(Names$[i])>8 THEN Names$[i]=LEFT$(Names$[i],8)
		IF Names$[i]="" THEN Names$[i]="Player "+INT$(i)
		Names$[i]=UPPER$(LEFT$(Names$[i],1))+LOWER$(RIGHT$(Names$[i],LEN(Names$[i])-1))
		IF Names$[i]="Android" THEN Names$[i]="ANDROID"
		GR.TEXT.DRAW g,30,230+i*50,"#"+INT$(i)+": "+Names$[i]
		GR.RENDER
	NEXT i
ENDIF
GR.COLOR 255,230,100,100,0
GR.RECT g,330,450,350,480
GR.COLOR 255,100,230,100,1
GR.TEXT.DRAW ChckMrk,330,478,"X"
GR.TEXT.DRAW g,360,480,"Sound"
IF SndOn=0 THEN GR.HIDE ChckMrk
GR.RENDER
Shuffled:
DIALOG.MESSAGE ,"Yahtzee, Ready to Roll ?",Start,"START","SHUFFLE names","Options"
IF Start=2 THEN
	IF NumOfPlayers=1 THEN
			POPUP "Well, uhhh, how do you want me to shuffle ONE name ?????",0,0,1
			PlayNote(7,500,SndOn)
			PAUSE 3000
			GOTO Shuffled
	ENDIF
	ARRAY.SHUFFLE Names$[1,NumOfPlayers]
	! remove names
	GR.COLOR 255,0,0,0,1
	GR.RECT g,30,230,300,440
	GR.COLOR 255,230,230,230,1
	! show shuffled
	FOR i=1 TO NumOfPlayers
		GR.TEXT.DRAW g,30,230+i*50,"#"+INT$(i)+": "+Names$[i]
	NEXT i
	GR.RENDER
	GOTO Shuffled
ENDIF
IF Start=3 THEN
	IF SndOn=0 THEN
		B$="on"
	ELSE
		B$="off"
	ENDIF
	DIALOG.MESSAGE ,"Yahtzee, Choose option",Option,"View scores","Sound "+B$
	IF Option=1 THEN
		LIST.CREATE S,ScoreTable
		SQL.QUERY Cursor,DB_Ptr,TableName$,"_id,"+C1$+","+C2$+","+C3$
		xdone = 0
		DO
			SQL.NEXT xdone,Cursor,Index$,V1$,V2$,V3$
			IF !xdone THEN
				IF VAL(V2$)=0 THEN
					L$=" has not scored yet..."
				ELSE
					L$=" - "+V2$+" points on "+V3$
				ENDIF
				LIST.ADD ScoreTable,V1$+L$
			ENDIF
		UNTIL xdone
		LIST.SIZE ScoreTable,n
		IF n THEN
			DIALOG.SELECT n,ScoreTable,"Best Scores, tap a score to exit..."
		ELSE
			POPUP "NO Scores PRESENT yet !!!",0,0,0
		ENDIF
		LIST.CLEAR ScoreTable
	ENDIF
	IF Option=2 THEN
		IF SndOn=0 THEN
			SndOn=1
			GR.MODIFY ChckMrk,"text","X"
		ELSE
			SndOn=0
			GR.MODIFY ChckMrk,"text",""
		ENDIF
		GR.RENDER
	ENDIF
	GOTO Shuffled
ENDIF
IF Begin=2 THEN
	! Save new names to DataBase
	FOR i=1 TO NumOfPlayers
		SQL.QUERY Cursor,DB_Ptr,TableName$,C1$,"Names='"+Names$[i]+"'"
		SQL.NEXT xdone,Cursor,N$
		IF N$<>Names$[i] THEN
			SQL.INSERT DB_Ptr,TableName$,C1$,Names$[i],C2$,"0"
		ELSE
			POPUP "Duplicate Name(s) NOT added to DataBase",0,0,0
		ENDIF
	NEXT i
ENDIF
GR.SHOW gt
GR.RENDER
S$="445775420024422"
FOR j=1 TO LEN(S$)
	FOR i=1 TO 6
		GR.GET.POSITION Intro[i],x,y
		GR.MODIFY Intro[i],"x",x+RND()*35-10,"y",y+20+RND()*10
	NEXT i
	GR.RENDER
	PlayNote(VAL(MID$(S$,j,1)),200,SndOn)
	IF SndOn=0 THEN PAUSE 200
NEXT j
PAUSE 1000
! Start of game
GR.CLS
GR.TEXT.TYPEFACE
! Draw dice for viewing
GR.COLOR 255,64,64,255,1
GR.RECT g,0,0,40,250
GR.COLOR 255,64,64,64,1
GR.RECT g,0,250,80,525
FOR i=1 TO 5
	GR.BITMAP.DRAW Dice[i],ViewDice[i],10,197+i*55
	GR.HIDE Dice[i]
NEXT i
GR.COLOR 128,255,255,255,1
FOR i=1 To 5
	GR.RECT DiceKeep[i],5,200+i*55,70,245+i*55
	GR.HIDE DiceKeep[i]
NEXT i
! Create sheet
GR.SET.STROKE 1.5
GR.COLOR 255,230,230,230,1
GR.RECT g,70,5,260+NumOfPlayers*100,525
GR.COLOR 255,0,0,0,1
FOR i=1 TO 22
	GR.COLOR 255,0,0,0,1
	READ.NEXT T$,S
	GR.TEXT.BOLD S
	GR.TEXT.SIZE 18
	GR.TEXT.DRAW g,85,(i-1)*23+30,T$
	IF i>1 & i<8 THEN GR.BITMAP.DRAW g,smallDice[i-1],185,(i-2)*23+35
	GR.TEXT.SIZE 15
	GR.TEXT.BOLD 0
	GR.COLOR 255,0,0,255,1
	IF i>1 & i<8 THEN GR.TEXT.DRAw g,215,(i-1)*23+28,"(x #)"
	IF i=9 THEN GR.TEXT.DRAw g,222,(i-1)*23+28,"(35)"
	IF i=12 | i=13 | i=18 THEN GR.TEXT.DRAw g,221,(i-1)*23+28,"(all)"
	IF i=14 THEN GR.TEXT.DRAw g,222,(i-1)*23+28,"(25)"
	IF i=15 THEN GR.TEXT.DRAw g,222,(i-1)*23+28,"(30)"
	IF i=16 THEN GR.TEXT.DRAw g,222,(i-1)*23+28,"(40)"
	IF i=17 THEN GR.TEXT.DRAw g,222,(i-1)*23+28,"(50)"
	IF i=19 THEN GR.TEXT.DRAw g,215,(i-1)*23+28,"(100)"
	GR.COLOR 255,0,0,0,0
	GR.LINE g,80,(i-1)*23+11,250+NumOfPlayers*100,(i-1)*23+12
NEXT i
GR.COLOR 255,0,0,0,1
GR.TEXT.BOLD 0
FOR i=250 TO 150+NumOfPlayers*100 STEP 100
	GR.LINE g,i,10,i,243
	GR.LINE g,i,266,i,520
NEXT i
GR.COLOR 255,0,255,0,0
GR.RECT g,43,110,68,130
GR.RECT g,43,230,68,250
GR.SET.STROKE 5
GR.COLOR 255,0,0,0,0
GR.RECT g,80,10,250+NumOfPlayers*100,520
GR.TEXT.SETFONT Fnt1
GR.TEXT.SIZE 45
GR.COLOR 255,255,255,0,1
FOR i=1 TO LEN(Y$)
	GR.TEXT.DRAW g,10,i*35,MID$(Y$,i,1)
NEXT i
GR.RECT g,270+NumOfPlayers*100,100,320+NumOfPlayers*100,280
GR.COLOR 255,0,0,255,0
GR.RECT g,270+NumOfPlayers*100,100,320+NumOfPlayers*100,280
GR.COLOR 255,0,0,255,1
H$="HELP"
FOR i=1 TO LEN(H$)
	GR.TEXT.DRAW g,283+NumOfPlayers*100,110+i*35,MID$(H$,i,1)
NEXT i
GR.TEXT.TYPEFACE
GR.TEXT.ALIGN 2
GR.TEXT.SIZE 20
GR.TEXT.BOLD 1
GR.COLOR 155,150,255,150,1
R$="Round  Turn"
FOR i=1 TO LEN(R$)
	GR.TEXT.DRAW g,55,i*20,MID$(R$,i,1)
NEXT i
GR.COLOR 255,0,255,0,1
GR.TEXT.DRAW Rnds,55,128,"1"
GR.TEXT.DRAW Trns,55,248,"1"
GR.TEXT.BOLD 0
GR.TEXT.SETFONT "monospace","BI"
GR.TEXT.SIZE 20
GR.COLOR 255,0,0,255,1
FOR i=1 TO NumOfPlayers
	GR.COLOR 255,0,0,0,1
	GR.TEXT.DRAW g,200+i*100,30,Names$[i]
	GR.COLOR 255,0,0,255,1
	GR.TEXT.DRAW ScorePrnt[i,21],200+i*100,21*23+30,"1"
NEXT i
GR.BITMAP.DRAW Place,Bar,80,11+23
GR.HIDE Place
GR.BITMAP.DRAW BarPlay,PlayBar,250,11
GR.RENDER
GR.COLOR 255,255,255,255,1
GameRun=1
PN$="YOUR COLUMN TO THROW"
GR.ROTATE.START 270,60,400
	GR.TEXT.DRAW PN1,60,400,PN$
GR.ROTATE.END
PN$=UPPER$(Names$[1])
GR.ROTATE.START 270,30,390
	GR.TEXT.DRAW PN2,30,390,PN$+", TAP"
GR.ROTATE.END
GR.COLOR 255,0,0,255,1

! TEST
TEXT.OPEN W,Game,DataPath$+"Yahtzee.txt"

FOR Lap=1 TO 13

! TEST
TEXT.WRITELN Game,
TEXT.WRITELN Game, "Lap "+INT$(Lap)

	GR.MODIFY Rnds,"text",INT$(Lap)
	FOR Player=1 TO NumOfPlayers
		DiceLeft=5
		Turn=0
		GR.MODIFY BarPlay,"x",150+Player*100
		GR.MODIFY Trns,"text",INT$(Turn+1)
		FOR i=1 TO 5
			CastSave[i]=0
			GR.HIDE Dice[i]
		NEXT i
		IF Names$[Player]<>"ANDROID" THEN 
			GR.MODIFY PN2,"text",UPPER$(Names$[Player])+", TAP"
			GR.SHOW PN1
			GR.SHOW PN2
		ELSE
			GR.MODIFY PN2,"text","ANDROID will throw !"
			GR.HIDE PN1
			GR.SHOW PN2
		ENDIF
		GR.RENDER
		IF Names$[Player]<>"ANDROID" THEN
			DO
				GOSUB GetTouch
			UNTIL (x>150+Player*100 & x<250+Player*100)
		ELSE
			PAUSE 2000
		ENDIF
		GR.HIDE PN1
		GR.HIDE PN2
		GR.RENDER
		PlayNote(0,200,SndOn)
		DO
			Turn=Turn+1
			GR.MODIFY Trns,"text",INT$(Turn)
			GR.RENDER
			PAUSE 1000
			POPUP Names$[Player]+" throws "+INT$(DiceLeft)+" dice. Casting "+INT$(Turn),0,0,0
			GOSUB ThrowDice
			PlayNote(0,200,SndOn)
			ARRAY.SORT Cast[1,DiceLeft]

! TEST
TEXT.WRITELN Game, Names$[Player]+" "+INT$(Turn)+" Turn  ";

			FOR i=1 TO DiceLeft
				GR.MODIFY Dice[i],"bitmap",ViewDice[Cast[i]]
				GR.SHOW Dice[i]

! TEST
TEXT.WRITELN Game, INT$(Cast[i])+" ";
				
			NEXT i
			GR.RENDER
			IF Turn<3 THEN
				IF Names$[Player]<>"ANDROID" THEN
					! Select dice to keep
					POPUP "Select dice to keep, then tap your score column",0,0,0
					DO
						GOSUB GetTouch
						IF x>0 & x<70 THEN
							FOR t=250 TO 470 STEP 55
								IF y>t & y<t+55 THEN
									IF CastSave[(t-195)/55]=0 THEN
										GR.SHOW DiceKeep[(t-195)/55]
										CastSave[(t-195)/55]=Cast[(t-195)/55]
										DiceLeft=DiceLeft-1
										PlayNote(4,200,SndOn)
									ELSE
										GR.HIDE DiceKeep[(t-195)/55]
										CastSave[(t-195)/55]=0
										DiceLeft=DiceLeft+1
										PlayNote(6,200,SndOn)
									ENDIF
								ENDIF
							NEXT t
							GR.RENDER
						ENDIF
					UNTIL y>0 & y<ScaleY & x>150+Player*100 & x<250+Player*100
				ELSE
					! Decide on dice to keep (Android)
					! Add kept dice to current casting
					IF DiceLeft<5 THEN
						FOR i=DiceLeft+1 TO 5
							Cast[i]=CastSave[i]
						NEXT i
						DiceLeft=5
					ENDIF

! TEST
TEXT.WRITELN Game, " becomes ";
FOR i=1 TO 5
TEXT.WRITELN Game, INT$(Cast[i])+" ";
NEXT i

					DIM MyDice[6]
					Keep=0
					FOR i=1 TO 5
						FOR j=1 TO 6
							IF Cast[i]=j THEN MyDice[j]=MyDice[j]+1
						NEXT j
					NEXT i
					T$=INT$(MyDice[1])+INT$(MyDice[2])+INT$(MyDice[3])+INT$(MyDice[4])+INT$(MyDice[5])+INT$(MyDice[6])
					FOR i=1 TO 5
						CastSave[i]=0
						GR.HIDE DiceKeep[i]
					NEXT i
					GR.RENDER
					POPUP "ANDROID: Considering options....",0,0,0
					PAUSE 2000
					! Upper section
					! Two or more equal and still needed
					FOR i=1 TO 6
						IF Lap>8 THEN
							IF MyDice[i]>0 & ScorePrnt[Player,i]=0 THEN Keep=i
						ENDIF
						IF MyDice[i]>1 & ScorePrnt[Player,i]=0 THEN Keep=i
						! Yahtzee
						IF MyDice[i]>2 & ScorePrnt[Player,16]=0 THEN Keep=i
					NEXT i
					! Three or more equal and still needed
					FOR i=1 TO 6
						IF MyDice[i]>2 & ScorePrnt[Player,i]=0 THEN Keep=i
					NEXT i
					! Lower section
					IF Keep=0 THEN
						! 3/4 of a kind
						FOR i=4 TO 6
							IF MyDice[i]>1 & ScorePrnt[Player,11]=0 THEN Keep=i
							IF MyDice[i]>1 & ScorePrnt[Player,12]=0 THEN Keep=i
						NEXT i
						! straights
						IF (ScorePrnt[Player,14]=0 | ScorePrnt[Player,15]=0) & Keep=0 THEN
							Sel$=""
							ARRAY.COPY Cast[],Temp[]
							ARRAY.SORT Temp[]
							Sel$=Sel$+INT$(Temp[3])
							IF Temp[3]-Temp[2]=1 THEN
								Sel$=INT$(Temp[2])+Sel$
								IF Temp[2]-Temp[1]=1 THEN
									Sel$=INT$(Temp[1])+Sel$
								ENDIF
							ENDIF
							IF Temp[4]-Temp[3]=1 THEN
								Sel$=Sel$+INT$(Temp[4])
								IF Temp[5]-Temp[4]=1 THEN
									Sel$=Sel$+INT$(Temp[5])
								ENDIF
							ENDIF
							ARRAY.DELETE Temp[]
							Comp$=""
							FOR i=1 TO 5
								Comp$=Comp$+INT$(Cast[i])
							NEXT i
							FOR i=1 TO LEN(Sel$)
								IF IS_IN(MID$(Sel$,i,1),Comp$) THEN
									CastSave[IS_IN(MID$(Sel$,i,1),Comp$)]=Cast[IS_IN(MID$(Sel$,i,1),Comp$)]
									GR.SHOW DiceKeep[IS_IN(MID$(Sel$,i,1),Comp$)]
									DiceLeft=DiceLeft-1
								ENDIF
							NEXT i
						ENDIF
					ENDIF
					IF Keep=0 THEN
						! Full House
						FH=0
						FOR i=1 TO 6
							IF  ScorePrnt[Player,i]>0 THEN FH=FH+1
						NEXT i
						IF FH>3 THEN
							IF IS_IN("2",T$) & IS_IN("3",T$) THEN
								FOR i=1 TO 5
									CastSave[i]=Cast[i]
									GR.SHOW DiceKeep[i]
								NEXT i
								DiceLeft=0
								Keep=0
							ENDIF
						ENDIF
						IF Lap<10 THEN
							C$=INT$(Cast[1])+INT$(Cast[2])+INT$(Cast[3])+INT$(Cast[4])+INT$(Cast[5])
							! Small straight
							IF (IS_IN("1",C$) & IS_IN("2",C$) & IS_IN("3",C$) & IS_IN("4",C$)) & ScorePrnt[Player,14]=0 THEN
								FOR i=1 TO 5
									CastSave[i]=0
								NEXT i
								CastSave[IS_IN("1",C$)]=Cast[IS_IN("1",C$)]
								CastSave[IS_IN("2",C$)]=Cast[IS_IN("2",C$)]
								CastSave[IS_IN("3",C$)]=Cast[IS_IN("3",C$)]
								CastSave[IS_IN("4",C$)]=Cast[IS_IN("4",C$)]
								GR.SHOW DiceKeep[IS_IN("1",C$)]
								GR.SHOW DiceKeep[IS_IN("2",C$)]
								GR.SHOW DiceKeep[IS_IN("3",C$)]
								GR.SHOW DiceKeep[IS_IN("4",C$)]
								DiceLeft=1
								Keep=0
							ENDIF
							IF (IS_IN("2",C$) & IS_IN("3",C$) & IS_IN("4",C$) & IS_IN("5",C$)) & ScorePrnt[Player,14]=0 THEN
								FOR i=1 TO 5
									CastSave[i]=0
								NEXT i
								CastSave[IS_IN("2",C$)]=Cast[IS_IN("2",C$)]
								CastSave[IS_IN("3",C$)]=Cast[IS_IN("3",C$)]
								CastSave[IS_IN("4",C$)]=Cast[IS_IN("4",C$)]
								CastSave[IS_IN("5",C$)]=Cast[IS_IN("5",C$)]
								GR.SHOW DiceKeep[IS_IN("2",C$)]
								GR.SHOW DiceKeep[IS_IN("3",C$)]
								GR.SHOW DiceKeep[IS_IN("4",C$)]
								GR.SHOW DiceKeep[IS_IN("5",C$)]
								DiceLeft=1
								Keep=0
							ENDIF
							IF (IS_IN("3",C$) & IS_IN("4",C$) & IS_IN("5",C$) & IS_IN("6",C$)) & ScorePrnt[Player,14]=0 THEN
								FOR i=1 TO 5
									CastSave[i]=0
								NEXT i
								CastSave[IS_IN("3",C$)]=Cast[IS_IN("3",C$)]
								CastSave[IS_IN("4",C$)]=Cast[IS_IN("4",C$)]
								CastSave[IS_IN("5",C$)]=Cast[IS_IN("5",C$)]
								CastSave[IS_IN("6",C$)]=Cast[IS_IN("6",C$)]
								GR.SHOW DiceKeep[IS_IN("3",C$)]
								GR.SHOW DiceKeep[IS_IN("4",C$)]
								GR.SHOW DiceKeep[IS_IN("5",C$)]
								GR.SHOW DiceKeep[IS_IN("6",C$)]
								DiceLeft=1
								Keep=0
							ENDIF
							! Large straight
							IF (Cast[5]-1=Cast[4] & Cast[4]-1=Cast[3] & Cast[3]-1=Cast[2]  & Cast[2]-1=Cast[1]) & ScorePrnt[Player,15]=0 THEN
								FOR i=1 TO 5
									CastSave[i]=Cast[i]
									GR.SHOW DiceKeep[i]
								NEXT i
								DiceLeft=0
								Keep=0
							ENDIF
						ENDIF
					ENDIF
					IF Keep>0 THEN
						FOR i=1 TO 5
							IF Cast[i]=Keep THEN
								CastSave[i]=Cast[i]
								GR.SHOW DiceKeep[i]
								DiceLeft=DiceLeft-1
							ELSE
								CastSave[i]=0
							ENDIF
						NEXT i
					ENDIF
					IF Joker[Player]=1 & (ScorePrnt[Player,13]=0 | ScorePrnt[Player,14]=0 | ScorePrnt[Player,15]=0) THEN
						FOR i=1 TO 5
							CastSave[i]=Cast[i]
							GR.SHOW DiceKeep[i]
						NEXT i
						DiceLeft=0
					ENDIF
					GR.RENDER
					ARRAY.DELETE MyDice[]
					PAUSE 3000
				ENDIF
				PlayNote(6,200,SndOn)
				! Save chosen dice
				ARRAY.SORT CastSave[]
				FOR i=1 TO 5
					GR.HIDE DiceKeep[i]
					IF CastSave[i]>0 THEN
						GR.MODIFY Dice[i],"bitmap",ViewDice[CastSave[i]]
						GR.SHOW DiceKeep[i]
					ELSE
						GR.HIDE Dice[i]
					ENDIF
				NEXT i
				GR.RENDER
			ELSE
				FOR i=1 TO 5
					IF CastSave[i]=0 THEN CastSave[i]=Cast[i]
				NEXT i
			ENDIF

! TEST
TEXT.WRITELN Game, " Selection ";
FOR i=1 TO 5
TEXT.WRITELN Game, INT$(CastSave[i])+" ";
NEXT i
TEXT.WRITELN Game,

			! All dice selected ?
			AllSel=0
			FOR i=1 TO 5
				IF CastSave[i]>0 THEN AllSel=AllSel+1
			NEXT i
		UNTIL AllSel=5
		FOR i=1 TO 5
			GR.HIDE DiceKeep[i]
		NEXT i
		ARRAY.SORT CastSave[]
		Suggest=10
		! Analyse casting
		! Is it Yahtzee ?
		YahtzeeBonus=0
		IF CastSave[1]=CastSave[2] & CastSave[1]=CastSave[3] & CastSave[1]=CastSave[4] & CastSave[1]=CastSave[5] THEN
			IF ScorePrnt[Player,16]=0 THEN
				Suggest=16
				GOTO SetPlace
			ELSE
				IF Score[Player,16]=50 THEN YahtzeeBonus=100
				! Obligation to put score in upper section if possible !
				ObligedScore=0
				FOR i=1 TO 6
					IF CastSave[1]=i & ScorePrnt[Player,i]=0 & Score[Player,16]=50 THEN
						ObligedScore=1
						Suggest=i
					ENDIF
				NEXT i
				IF Joker[Player]=0 THEN
					GR.TEXT.DRAW g,170+Player*100,260,"<"
					GR.BITMAP.DRAW Joker,Nar,190+Player*100,242
					GR.TEXT.DRAW g,230+Player*100,260,">"
					Joker[Player]=1
				ENDIF
				GOTO SetPlace
			ENDIF
		ENDIF
		! Is it Ones - Sixes ?
		FOR j=1 TO 6
			IF ScorePrnt[Player,j]=0 THEN
				Cnt=0
				FOR i=1 TO 5
					IF CastSave[i]=j THEN Cnt=Cnt+1
				NEXT i
				IF Cnt>2 THEN Suggest=j
			ENDIF
		NEXT j
		IF Suggest<7 THEN GOTO SetPlace
		! Is it LARGE Straight ?
		IF ScorePrnt[Player,15]=0 THEN
			IF (CastSave[5]-1=CastSave[4] & CastSave[4]-1=CastSave[3] & CastSave[3]-1=CastSave[2]  & CastSave[2]-1=CastSave[1]) THEN
				Suggest=15
				GOTO SetPlace
			ENDIF
			! Use joker
			IF  Joker[Player]=1 & CastSave[1]=CastSave[2] & CastSave[1]=CastSave[3] & CastSave[1]=CastSave[4] & CastSave[1]=CastSave[5] THEN
				Suggest=15
				GOTO SetPlace
			ENDIF
		ENDIF
		! Is it small Straight ?
		IF ScorePrnt[Player,14]=0 THEN
			C$=INT$(CastSave[1])+INT$(CastSave[2])+INT$(CastSave[3])+INT$(CastSave[4])+INT$(CastSave[5])
			IF (IS_IN("1",C$) & IS_IN("2",C$) & IS_IN("3",C$) & IS_IN("4",C$)) | (IS_IN("2",C$) & IS_IN("3",C$) & IS_IN("4",C$) & IS_IN("5",C$)) | (IS_IN("3",C$) & IS_IN("4",C$) & IS_IN("5",C$) & IS_IN("6",C$)) THEN
				Suggest=14
				GOTO SetPlace
			ENDIF
			! Use joker
			IF  Joker[Player]=1 & CastSave[1]=CastSave[2] & CastSave[1]=CastSave[3] & CastSave[1]=CastSave[4] & CastSave[1]=CastSave[5] THEN
				Suggest=14
				GOTO SetPlace
			ENDIF
		ENDIF
		! Is it Full House ?
		IF ScorePrnt[Player,13]=0 THEN
			IF (CastSave[1]=CastSave[2] & CastSave[3]=CastSave[4] & CastSave[3]=CastSave[5]) | (CastSave[1]=CastSave[2] & CastSave[1]=CastSave[3] & CastSave[4]=CastSave[5]) THEN
				Suggest=13
				GOTO SetPlace
			ENDIF
		ENDIF
		! Is it 4 of kind ?
		IF ScorePrnt[Player,12]=0 THEN
			IF (CastSave[5]=CastSave[4] & CastSave[4]=CastSave[3]  & CastSave[3]=CastSave[2]) | (CastSave[1]=CastSave[2] & CastSave[2]=CastSave[3]  & CastSave[3]=CastSave[4]) THEN
				Suggest=12
				GOTO SetPlace
			ENDIF
		ENDIF
		! Is it 3 of kind ?
		IF ScorePrnt[Player,11]=0 THEN
			IF (CastSave[5]=CastSave[4] & CastSave[4]=CastSave[3]) | (CastSave[1]=CastSave[2] & CastSave[2]=CastSave[3]) | (CastSave[2]=CastSave[3] & CastSave[3]=CastSave[4]) THEN
				Suggest=11
				GOTO SetPlace
			ENDIF
		ENDIF
		! Chance still free ?
		IF ScorePrnt[Player,17]=0 THEN
			Suggest=17
			GOTO SetPlace
		ENDIF
		! No obvious choice, so look for alternative
		! Upper Section
		FOR i=6 TO 2 STEP -1
			IF Score[Player,i]>3*i THEN
				FOR j=i-1 TO 1 STEP -1
					IF ScorePrnt[Player,j]=0 THEN
						ts=0
						FOR k=1 TO 5
							IF CastSave[k]=j THEN ts=ts+j
						NEXT i
						IF (Score[Player,i]-3*i)-(3*j-ts)>=0 THEN Suggest=j
					ENDIF
				NEXT j
			ENDIF
		NEXT i
		IF Suggest<6 THEN GOTO SetPlace
		! Lower section
		IF ScorePrnt[Player,14]=0 THEN Suggest=14
		IF ScorePrnt[Player,15]=0 THEN Suggest=15
		IF ScorePrnt[Player,11]=0 THEN Suggest=11
		IF ScorePrnt[Player,16]=0 THEN Suggest=16
		IF ScorePrnt[Player,12]=0 THEN Suggest=12
		IF ScorePrnt[Player,13]=0 THEN Suggest=13
		SetPlace:
		! No suggetion found
		IF Suggest=10 & Names$[Player]="ANDROID" THEN
			Damage=18
			FOR i=1 TO 6
				IF ScorePrnt[Player,i]=0 & (3*i-VAL(MID$(T$,i,1))*i)<Damage THEN
					Damage=3*i-VAL(MID$(T$,i,1))*i
				ENDIF
			NEXT i
			FOR i=1 TO 6
				IF ScorePrnt[Player,i]=0 & (3*i-VAL(MID$(T$,i,1))*i)=Damage THEN
					Suggest=i
				ENDIF
			NEXT i
		ENDIF
		IF Suggest=10 & Names$[Player]="ANDROID" THEN	
			FOR i=1 TO 6
				IF ScorePrnt[Player,i]=0 THEN
					Suggest=i
					F_N.BREAK
				ENDIF
			NEXT i
		ENDIF
		FOR i=6 TO 1 STEP -1
			IF Joker[Player]=1 & ObligedScore=0 & ScorePrnt[Player,i]=0 & Names$[Player]="ANDROID" THEN Suggest=i
		NEXT i
		IF Joker[Player]=1 & ObligedScore=0 & ScorePrnt[Player,17]=0 & Names$[Player]="ANDROID" THEN Suggest=17
		FOR i=11 TO 15
			IF Joker[Player]=1 & ObligedScore=0 & ScorePrnt[Player,i]=0 & Names$[Player]="ANDROID" THEN Suggest=i
		NEXT i
		! Suggest placement
		GR.MODIFY Place,"y",Suggest*23+11
		GR.SHOW Place
		GR.RENDER
		! Get placement
		Placement=Suggest
		IF Lap<13 THEN
			IF ObligedScore=0 THEN
				IF Names$[Player]<>"ANDROID" THEN
					POPUP "Tap desired category and tap your score column to acknowledge",0,0,0
					DO
						GOSUB GetTouch
						IF x>80 & x<250 THEN
							IF ((INT((y-11)/23)>0 & INT((y-11)/23)<7) | (INT((y-11)/23)>10 & INT((y-11)/23)<18)) & ScorePrnt[Player,INT((y-11)/23)]=0 THEN
								Placement=INT((y-11)/23)
								GR.MODIFY Place,"y",Placement*23+11
								GR.RENDER
							ENDIF
						ENDIF
					UNTIL y>0 & y<ScaleY & x>150+Player*100 & x<250+Player*100 & Placement<>10
				ELSE
					PAUSE 2000
				ENDIF
			ELSE
				ObligedScore=0
			ENDIF
		ELSE
			!Last round so only one empty category left
			FOR i=1 TO 6
				IF ScorePrnt[Player,i]=0 THEN Placement=i
			NEXT i
			FOR i=11 TO 17
				IF ScorePrnt[Player,i]=0 THEN Placement=i
			NEXT i
		ENDIF
		! Place casting
		PlayNote(5,200,SndOn)
		Score=0
		! 1-6
		IF Placement<7 THEN
			FOR i=1 TO 5
				IF CastSave[i]=Placement THEN Score=Score+Placement
			NEXT i
		ELSE
			SW.BEGIN Placement
			SW.CASE 11
				IF (CastSave[5]=CastSave[4] & CastSave[4]=CastSave[3]) | (CastSave[1]=CastSave[2] & CastSave[2]=CastSave[3]) | (CastSave[2]=CastSave[3] & CastSave[3]=CastSave[4]) THEN
					Score=CastSave[5]+CastSave[4]+CastSave[3]+CastSave[2]+CastSave[1]
				ENDIF
				SW.BREAK
			SW.CASE 12
				IF CastSave[5]=CastSave[4] & CastSave[4]=CastSave[3] & CastSave[3]=CastSave[2] THEN
					Score=CastSave[5]+CastSave[4]+CastSave[3]+CastSave[2]+CastSave[1]
				ELSE
					IF CastSave[1]=CastSave[2] & CastSave[2]=CastSave[3] & CastSave[3]=CastSave[4] THEN
						Score=CastSave[1]+CastSave[2]+CastSave[3]+CastSave[4]+CastSave[5]
					ENDIF
				ENDIF
				SW.BREAK
			SW.CASE 13
				IF (CastSave[1]=CastSave[2] & CastSave[3]=CastSave[4] & CastSave[3]=CastSave[5]) | (CastSave[1]=CastSave[2] & CastSave[1]=CastSave[3] & CastSave[4]=CastSave[5]) THEN Score=25
			SW.BREAK
			SW.CASE 14
				C$=INT$(CastSave[1])+INT$(CastSave[2])+INT$(CastSave[3])+INT$(CastSave[4])+INT$(CastSave[5])
				IF Joker[Player]=1 | (IS_IN("1",C$) & IS_IN("2",C$) & IS_IN("3",C$) & IS_IN("4",C$)) | (IS_IN("2",C$) & IS_IN("3",C$) & IS_IN("4",C$) & IS_IN("5",C$)) | (IS_IN("3",C$) & IS_IN("4",C$) & IS_IN("5",C$) & IS_IN("6",C$)) THEN
					Score=30
				ENDIF
			SW.BREAK
			SW.CASE 15
				IF Joker[Player]=1 | (CastSave[5]-1=CastSave[4] & CastSave[4]-1=CastSave[3] & CastSave[3]-1=CastSave[2]  & CastSave[2]-1=CastSave[1]) THEN Score=40
				SW.BREAK
			SW.CASE 16
				IF CastSave[1]=CastSave[2] & CastSave[1]=CastSave[3] & CastSave[1]=CastSave[4] & CastSave[1]=CastSave[5] THEN Score=50
				SW.BREAK
			SW.CASE 17
				FOR i=1 TO 5
					Score=Score+CastSave[i]
				NEXT i
				SW.BREAK
			SW.END
		ENDIF
		GOSUB UpdateScore

! TEST
TEXT.WRITELN Game, " Placement "+INT$(Score)+" points on ";
FOR i=1 TO 6
IF Placement=i THEN TEXT.WRITELN Game, INT$(i)
NEXT i
IF Placement=11 THEN TEXT.WRITELN Game, "3 of a kind"
IF Placement=12 THEN TEXT.WRITELN Game, "4 of a kind"
IF Placement=13 THEN TEXT.WRITELN Game, "Full House"
IF Placement=14 THEN TEXT.WRITELN Game, "small Straight"
IF Placement=15 THEN TEXT.WRITELN Game, "Large Straight"
IF Placement=16 THEN TEXT.WRITELN Game, "Yahtzee"
IF Placement=17 THEN TEXT.WRITELN Game, "Chance"

		IF Names$[Player]="ANDROID" THEN PAUSE 3000
		GR.HIDE Place
		PAUSE 2000
		FOR i=1 TO 5
			GR.HIDE Dice[i]
		NEXT i
		GR.RENDER
	NEXT Player
NEXT Lap

! TEST
TEXT.WRITELN Game,
TEXT.WRITELN Game, "Score Table"
FOR j=1 TO NumOfplayers
TEXT.WRITELN Game, Names$[j]
FOR i=1 TO 20
TEXT.WRITELN Game, INT$(i)+" "+INT$(Score[j,i])
NEXT i
TEXT.WRITELN Game, "Ranking "+INT$(Score[j,21])
NEXT j
TEXT.CLOSE Game

GR.HIDE BarPlay
GR.TEXT.SIZE 40
GR.TEXT.BOLD 1
GR.TEXT.ALIGN 1
GR.COLOR 128,255,0,0,1
HS=0
FOR i=1 TO NumOfPlayers
	IF Score[i,20]>maxScore THEN
		GR.ROTATE.START 90,155+i*100,50
			GR.TEXT.DRAW g,155+i*100,50,"new High Score !!!"
		GR.ROTATE.END
		HS=1
	ENDIF
NEXT i
GR.TEXT.SIZE 20
GR.RENDER
IF HS=1 THEN
	AUDIO.STOP
	AUDIO.PLAY joy
	AUDIO.VOLUME SndOn,SndOn
	PAUSE 3000
	GR.BITMAP.DRAW RainBow,Hi,0,500
	FOR i=500 TO 0 STEP -5
		GR.MODIFY RainBow,"y",i
		GR.RENDER
		PAUSE 50
	NEXT i
	FOR i=0 TO 150 STEP 5
		GR.MODIFY RainBow,"y",i
		GR.RENDER
		PAUSE 100
	NEXT i
	DO
		AUDIO.ISDONE isdone
		PAUSE 1000
	UNTIL isdone
	GR.HIDE RainBow
	GR.RENDER
ELSE
	S$="22402454024542027"
	FOR i=1 TO LEN(S$)
		PlayNote(VAL(MID$(S$,i,1)),100,SndOn)
	NEXT i
ENDIF
x$="Tap Screen to end"
GR.COLOR 255,255,0,0,1
FOR i=1 TO LEN(x$)
	GR.TEXT.DRAW g,30,255+i*15,MID$(x$,i,1)
NEXT i
GR.RENDER
GameRun=0
GOSUB GetTouch
! Show and Save scores to DBase
GR.CLS
GR.COLOR 255,255,255,255,1
GR.TEXT.DRAW g,50,50,"Scores this round"
GR.TEXT.DRAW g,50,100,"Name"
GR.TEXT.DRAW g,150,100,"Score"
GR.TEXT.DRAW g,250,100,"Rank"
T$=USING$("","%tF",time())+" at "+USING$("","%tT",time())
FOR i=1 TO NumOfPlayers
	GR.TEXT.DRAW g,50,100+i*30,Names$[i]
	GR.TEXT.DRAW g,160,100+i*30,INT$(Score[i,20])
	GR.TEXT.DRAW g,270,100+i*30,INT$(Score[i,21])
	SQL.QUERY Cursor,DB_Ptr,TableName$,C2$,"Names='"+Names$[i]+"'"
	SQL.NEXT xdone,Cursor,N$
	IF VAL(N$)<Score[i,20] THEN
		SQL.UPDATE DB_Ptr,TableName$,C2$,INT$(Score[i,20]),C3$,T$:"Names='"+Names$[i]+"'"
	ENDIF
NEXT i
! Show best score so far
GOSUB GetHiScores
PlayNote(2,300,SndOn)
GR.RENDER
PAUSE 2000
PlayNote(0,300,SndOn)
DIALOG.MESSAGE "Yahtzee","Play again ?",Q,"Yes","No"
GR.TEXT.SIZE 50
GR.COLOR 255,255,255,0,1
IF Q=2 THEN
	GR.TEXT.DRAW g,50,450,"See you next time !"
	GR.RENDER
	PAUSE 3000
ENDIF
IF Q=1 THEN
	GR.TEXT.DRAW g,50,450,"YES, we'll play again !"
	GR.RENDER
	READ.FROM 291
	FOR i=1 TO 4
		FOR j=1 TO 21
			Score[i,j]=0
			ScorePrnt[i,j]=0
		NEXT j
		Rank[i]=0
		Joker[i]=0
	NEXT i
	PAUSE 3000
	GOTO StartGame
ENDIF
SQL.CLOSE DB_Ptr
GR.CLOSE
WAKELOCK 5
EXIT
ONBACKKEY:
	PlayNote(2,200,SndOn)
	DIALOG.MESSAGE "Yahtzee","Do you want to quit ?",Q,"Yes","No"
	IF Q=1 THEN
		SQL.CLOSE DB_Ptr
		GR.CLOSE
		WAKELOCK 5
		EXIT
	ENDIF
BACk.RESUME
GetTouch:
	DO
		GR.TOUCH Touched,x,y
	UNTIL Touched
	DO
		GR.TOUCH Touched,x,y
	UNTIL !Touched
	x/=sx
	y/=sy
RETURN
ONGRTOUCH:
	IF GameRun=1 THEN
		GR.TOUCH Touched,x,y
		x/=sx
		y/=sy
		IF x>270+NumOfPlayers*100 & x<320+NumOfPlayers*100 & y>100 & y<280 THEN
			GOSUB HELP
		ENDIF
	ENDIF
GR.ONGRTOUCH.RESUME
HELP:
	HTML.OPEN
	HTML.LOAD.STRING Rules$
	DO
		HTML.GET.DATALINK data$
	UNTIL LEFT$(data$,3)="BAK" | data$="DAT:OK"
	PlayNote(0,200,SndOn)
	HTML.CLOSE
	GR.RENDER
RETURN
GetNames:
	! Returns List with names in DBase
	LIST.CREATE S,AllNames
	LIST.CREATE S,HiScore
	Columns$="_id,"+C1$+","+C2$
	SQL.QUERY Cursor,DB_Ptr,TableName$,Columns$
	xdone = 0
	DO
		SQL.NEXT xdone,Cursor,Index$,V1$,V2$
		IF !xdone THEN
			LIST.ADD AllNames,V1$
			LIST.ADD HiScore,V2$
		ENDIF
	UNTIL xdone
	LIST.SIZE AllNames,NumOfNames
RETURN
GetHiScores:
	LIST.CLEAR AllNames
	LIST.CLEAR HiScore
	GOSUB GetNames
	maxScore=0
	count=0
	count$=""
	FOR j=1 TO NumOfNames
		LIST.GET HiScore,j,S$
		IF VAL(S$)>maxScore THEN
			maxScore=VAL(S$)
		ENDIF
	NEXT j
	FOR j=1 TO NumOfNames
		LIST.GET HiScore,j,S$
		IF VAL(S$)=maxScore & maxScore>0 THEN
			count=count+1
			LIST.GET AllNames,j,Hi$
			GR.TEXT.DRAW g,50,100+(i+2)*30+(count-1)*30,Hi$+" - Score "+S$+count$
			IF count>0 THEN count$=" EX AEQUO !!!"
		ENDIF
		IF count=1 THEN GR.TEXT.DRAW g,50,100+(i+1)*30,"Hi Score until now"
	NEXT j
RETURN
ThrowDice:
	GOSUB ReadString
	HTML.OPEN
	HTML.LOAD.STRING HTML$
	PAUSE 500
	AUDIO.STOP
	IF DiceLeft=1 THEN
		PAUSE 2300
		AUDIO.PLAY roll[1]
		AUDIO.VOLUME SndOn,SndOn
		PAUSE 2000
	ENDIF
	IF DiceLeft=2 THEN
		PAUSE 2000
		AUDIO.PLAY roll[2]
		AUDIO.VOLUME SndOn,SndOn
		PAUSE 2300
	ENDIF
	IF DiceLeft=3 THEN
		PAUSE 1500
		AUDIO.PLAY roll[3]
		AUDIO.VOLUME SndOn,SndOn
		PAUSE 2800
	ENDIF
	IF DiceLeft=4 THEN
		AUDIO.PLAY roll[4]
		AUDIO.VOLUME SndOn,SndOn
		PAUSE 4300
	ENDIF
	IF DiceLeft=5 THEN
		AUDIO.PLAY roll[5]
		AUDIO.VOLUME SndOn,SndOn
		PAUSE 4300
	ENDIF
	AUDIO.STOP
	PAUSE 5000
	HTML.CLOSE
RETURN
UpdateScore:
! Update score (in: Player,Placement,Score,YahtzeeBonus . out: Updated Score,Totals,Bonuses,Ranking)
Score[Player,Placement]=Score
Score[Player,7]=Score[Player,1]+Score[Player,2]+Score[Player,3]+Score[Player,4]+Score[Player,5]+Score[Player,6]
IF Score[Player,7]>62 THEN Score[Player,8]=35
Score[Player,9]=Score[Player,7]+Score[Player,8]
Score[Player,18]=Score[Player,18]+YahtzeeBonus
Score[Player,19]=Score[Player,11]+Score[Player,12]+Score[Player,13]+Score[Player,14]+Score[Player,15]+Score[Player,16]+Score[Player,17]+Score[Player,18]
Score[Player,20]=Score[Player,19]+Score[Player,9]
IF ScorePrnt[Player,Placement]=0 THEN
	GR.TEXT.DRAW ScorePrnt[Player,Placement],200+Player*100,Placement*23+30,INT$(Score[Player,Placement])
ELSE
	GR.MODIFY ScorePrnt[Player,Placement],"text",INT$(Score[Player,Placement])
ENDIF
IF ScorePrnt[Player,7]=0 THEN
	GR.TEXT.DRAW ScorePrnt[Player,7],200+Player*100,7*23+30,INT$(Score[Player,7])
ELSE
	GR.MODIFY ScorePrnt[Player,7],"text",INT$(Score[Player,7])
ENDIF
IF ScorePrnt[Player,8]=0 THEN
	GR.TEXT.DRAW ScorePrnt[Player,8],200+Player*100,8*23+30,INT$(Score[Player,8])
ELSE
	GR.MODIFY ScorePrnt[Player,8],"text",INT$(Score[Player,8])
ENDIF
IF ScorePrnt[Player,9]=0 THEN
	GR.TEXT.DRAW ScorePrnt[Player,9],200+Player*100,9*23+30,INT$(Score[Player,9])
ELSE
	GR.MODIFY ScorePrnt[Player,9],"text",INT$(Score[Player,9])
ENDIF
IF ScorePrnt[Player,18]=0 THEN
	GR.TEXT.DRAW ScorePrnt[Player,18],200+Player*100,18*23+30,INT$(Score[Player,18])
ELSE
	GR.MODIFY ScorePrnt[Player,18],"text",INT$(Score[Player,18])
ENDIF
IF ScorePrnt[Player,19]=0 THEN
	GR.TEXT.DRAW ScorePrnt[Player,19],200+Player*100,19*23+30,INT$(Score[Player,19])
ELSE
	GR.MODIFY ScorePrnt[Player,19],"text",INT$(Score[Player,19])
ENDIF
IF ScorePrnt[Player,20]=0 THEN
	GR.TEXT.DRAW ScorePrnt[Player,20],200+Player*100,20*23+30,INT$(Score[Player,20])
ELSE
	GR.MODIFY ScorePrnt[Player,20],"text",INT$(Score[Player,20])
ENDIF
! Ranking
FOR i=1 TO NumOfPlayers
	Rank[i]=Score[i,20]
NEXT i
ARRAY.SORT Rank[]
ARRAY.REVERSE Rank[]
FOR i=1 TO NumOfPlayers
	ARRAY.SEARCH Rank[],Score[i,20],Score[i,21]
	GR.MODIFY ScorePrnt[i,21],"text",INT$(Score[i,21])
NEXT i
GR.RENDER
RETURN
ReadString:
! pick positions
ARRAY.SHUFFLE DicePos[]
! dice values
FOR i=1 TO 5
	Cast[i]=FLOOR(6*RND()+1)
NEXT i
HTML$="<html><head><meta name='viewport' content='width=device-width, user-scalable=no' /><style>body{zoom:"+Zoom$+";background-color:rgb(230,230,230);background-image: linear-gradient(90deg,rgba(120,180,160,.5) 50%,transparent 50%),"
HTML$=HTML$+"linear-gradient(rgba(120,180,160,.5) 50%,transparent 50%);border-style:solid;border-color:rgb(120,180,160);border-width:30px;margin:0px;background-size:50px 50px;}"
HTML$=HTML$+".container{-webkit-perspective: 2000px;perspective-origin: 50% 50%;}.cube, .cube .face,.cube .face-back,.cube .face-cent{height: 98px;width: 98px;}"
! dice speed / duration
t1=3*RND()+3
t2=300*RND()+300
cnt=INT(t1*1000/t2)
HTML$=HTML$+".Dice1{-webkit-transform-style: preserve-3d;-webkit-transform: translateX("+INT$(PosCoords[DicePos[1],1])+"px) translateY("+INT$(PosCoords[DicePos[1],2])+"px) rotateZ("+INT$(360*RND()+1)+"deg);-webkit-animation-name:move1;-webkit-animation-duration:"+INT$(t1)+"s;"
HTML$=HTML$+"-webkit-animation-iteration-count:1;-webkit-animation-timing-function:ease-out;}.Dice1 .cube{-webkit-transform-style: preserve-3d;-webkit-transform: rotateX("+DiceFace$[Cast[1],1]+"deg) rotateY("+DiceFace$[Cast[1],2]+"deg);margin:0px auto;"
HTML$=HTML$+"-webkit-animation-name:rotate;-webkit-animation-duration:"+INT$(t2)+"ms;-webkit-animation-iteration-count:"+INT$(cnt)+";-webkit-animation-timing-function:linear;}"
! dice speed / duration
t1=3*RND()+3
t2=300*RND()+300
cnt=INT(t1*1000/t2)
HTML$=HTML$+".Dice2{-webkit-transform-style: preserve-3d;-webkit-transform: translateX("+INT$(PosCoords[DicePos[2],1])+"px) translateY("+INT$(PosCoords[DicePos[2],2]-100)+"px) rotateZ("+INT$(360*RND()+1)+"deg);-webkit-animation-name:move2;-webkit-animation-duration:"+INT$(t1)+"s;"
HTML$=HTML$+"-webkit-animation-iteration-count:1;-webkit-animation-timing-function:ease-out;}.Dice2 .cube{-webkit-transform-style: preserve-3d;-webkit-transform: rotateX("+DiceFace$[Cast[2],1]+"deg) rotateY("+DiceFace$[Cast[2],2]+"deg);margin:0px auto;"
HTML$=HTML$+"-webkit-animation-name:rotate;-webkit-animation-duration:"+INT$(t2)+"ms;-webkit-animation-iteration-count:"+INT$(cnt)+";-webkit-animation-timing-function:linear;}"
! dice speed / duration
t1=3*RND()+3
t2=300*RND()+300
cnt=INT(t1*1000/t2)
HTML$=HTML$+".Dice3{-webkit-transform-style: preserve-3d;-webkit-transform: translateX("+INT$(PosCoords[DicePos[3],1])+"px) translateY("+INT$(PosCoords[DicePos[3],2]-200)+"px) rotateZ("+INT$(360*RND()+1)+"deg);-webkit-animation-name:move3;-webkit-animation-duration:"+INT$(t1)+"s;"
HTML$=HTML$+"-webkit-animation-iteration-count:1;-webkit-animation-timing-function:ease-out;}.Dice3 .cube{-webkit-transform-style: preserve-3d;-webkit-transform: rotateX("+DiceFace$[Cast[3],1]+"deg) rotateY("+DiceFace$[Cast[3],2]+"deg);margin:0px auto;"
HTML$=HTML$+"-webkit-animation-name:rotate;-webkit-animation-duration:"+INT$(t2)+"ms;-webkit-animation-iteration-count:"+INT$(cnt)+";-webkit-animation-timing-function:linear;}"
! dice speed / duration
t1=3*RND()+3
t2=300*RND()+300
cnt=INT(t1*1000/t2)
HTML$=HTML$+".Dice4{-webkit-transform-style: preserve-3d;-webkit-transform: translateX("+INT$(PosCoords[DicePos[4],1])+"px) translateY("+INT$(PosCoords[DicePos[4],2]-300)+"px) rotateZ("+INT$(360*RND()+1)+"deg);-webkit-animation-name:move4;-webkit-animation-duration:"+INT$(t1)+"s;"
HTML$=HTML$+"-webkit-animation-iteration-count:1;-webkit-animation-timing-function:ease-out;}.Dice4 .cube{-webkit-transform-style: preserve-3d;-webkit-transform: rotateX("+DiceFace$[Cast[4],1]+"deg) rotateY("+DiceFace$[Cast[4],2]+"deg);margin:0px auto;"
HTML$=HTML$+"-webkit-animation-name:rotate;-webkit-animation-duration:"+INT$(t2)+"ms;-webkit-animation-iteration-count:"+INT$(cnt)+";-webkit-animation-timing-function:linear;}"
! dice speed / duration
t1=3*RND()+3
t2=300*RND()+300
cnt=INT(t1*1000/t2)
HTML$=HTML$+".Dice5{-webkit-transform-style: preserve-3d;-webkit-transform: translateX("+INT$(PosCoords[DicePos[5],1])+"px) translateY("+INT$(PosCoords[DicePos[5],2]-400)+"px) rotateZ("+INT$(360*RND()+1)+"deg);-webkit-animation-name:move5;-webkit-animation-duration:"+INT$(t1)+"s;"
HTML$=HTML$+"-webkit-animation-iteration-count:1;-webkit-animation-timing-function:ease-out;}.Dice5 .cube{-webkit-transform-style: preserve-3d;-webkit-transform: rotateX("+DiceFace$[Cast[5],1]+"deg) rotateY("+DiceFace$[Cast[5],2]+"deg);margin:0px auto;"
HTML$=HTML$+"-webkit-animation-name:rotate;-webkit-animation-duration:"+INT$(t2)+"ms;-webkit-animation-iteration-count:"+INT$(cnt)+";-webkit-animation-timing-function:linear;}"
HTML$=HTML$+".cube .face{background:-webkit-linear-gradient(left top,#ccc,#999,#ccc);position:absolute;border: 1px solid #bbb;border-radius:40px;}"
HTML$=HTML$+".cube .face-back{background:#bbb;position: absolute;border: 1px solid #bbb;border-radius: 35px;}.cube .face-cent{background:#bbb;position: absolute;}"
HTML$=HTML$+".cube .face:nth-child(1){-webkit-transform: translateZ(50px);}.cube .face:nth-child(2){-webkit-transform: rotateY(180deg) translateZ(50px);}"
HTML$=HTML$+".cube .face:nth-child(3){-webkit-transform: rotateY(-90deg) translateZ(50px);}.cube .face:nth-child(4){-webkit-transform: rotateY(90deg) translateZ(50px);}"
HTML$=HTML$+".cube .face:nth-child(5){-webkit-transform: rotateX(90deg) translateZ(50px);}.cube .face:nth-child(6){-webkit-transform: rotateX(-90deg) translateZ(50px);}"
HTML$=HTML$+".cube .face-back:nth-child(7){-webkit-transform: translateZ(49px);}.cube .face-back:nth-child(8){-webkit-transform: rotateY(180deg) translateZ(49px);}"
HTML$=HTML$+".cube .face-back:nth-child(9){-webkit-transform: rotateY(-90deg) translateZ(49px);}.cube .face-back:nth-child(10){-webkit-transform: rotateY(90deg) translateZ(49px);}"
HTML$=HTML$+".cube .face-back:nth-child(11){-webkit-transform: rotateX(90deg) translateZ(49px);}.cube .face-back:nth-child(12){-webkit-transform: rotateX(-90deg) translateZ(49px);}"
HTML$=HTML$+".cube .face-cent:nth-child(13){-webkit-transform: rotateX(180deg);}.cube .face-cent:nth-child(14){-webkit-transform: rotateY(90deg);}.cube .face-cent:nth-child(15){-webkit-transform: rotateX(90deg);}"
HTML$=HTML$+"@-webkit-keyframes move1{from{-webkit-transform: translateX(3000px);}to{-webkit-transform: translateX("+INT$(PosCoords[DicePos[1],1])+"px) translateY("+INT$(PosCoords[DicePos[1],2])+"px);}}@-webkit-keyframes move2{from{-webkit-transform: translateX(3000px);}"
HTML$=HTML$+"to{-webkit-transform: translateX("+INT$(PosCoords[DicePos[2],1])+"px) translateY("+INT$(PosCoords[DicePos[2],2]-100)+"px);}}@-webkit-keyframes move3{from{-webkit-transform: translateX(3000px);}to{-webkit-transform: translateX("+INT$(PosCoords[DicePos[3],1])+"px) translateY("+INT$(PosCoords[DicePos[3],2]-200)+"px);}}"
HTML$=HTML$+"@-webkit-keyframes move4{from{-webkit-transform: translateX(3000px);}to{-webkit-transform: translateX("+INT$(PosCoords[DicePos[4],1])+"px) translateY("+INT$(PosCoords[DicePos[4],2]-300)+"px);}}@-webkit-keyframes move5{from{-webkit-transform: translateX(3000px);}"
HTML$=HTML$+"to{-webkit-transform: translateX("+INT$(PosCoords[DicePos[5],1])+"px) translateY("+INT$(PosCoords[DicePos[5],2]-400)+"px);}}@-webkit-keyframes rotate{from{-webkit-transform: rotateX(0deg) rotateY(0deg);}to{-webkit-transform: rotateX(180deg) rotateY(360deg);}}"
HTML$=HTML$+"</style></head><body><div class='container'>"

HTML$=HTML$+"<div class='Dice1'><div class='cube'>"
HTML$=HTML$+"<div class='face'><svg height='100' width='100'><defs><radialGradient id='eyes' cx='50%' cy='50%' r='30%' fx='30%' fy='30%'><stop offset='0%' style='stop-color:rgb(192,0,0);stop-opacity:0.4' />"
HTML$=HTML$+"<stop offset='100%' style='stop-color:rgb(255,0,0);stop-opacity:0.8' /></radialGradient></defs><circle cx='22' cy='22' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/>"
HTML$=HTML$+"<circle cx='22' cy='50' r='10' fill='url(#eyes)'/><circle cx='78' cy='50' r='10' fill='url(#eyes)'/><circle cx='22' cy='78' r='10' fill='url(#eyes)'/>"
HTML$=HTML$+"<circle cx='78' cy='78' r='10' fill='url(#eyes)'/></svg> </div><div class='face'><svg height='100' width='100'><circle cx='50' cy='50' r='10' fill='url(#eyes)'/></svg> </div><div class='face'>"
HTML$=HTML$+"<svg height='100' width='100'><circle cx='22' cy='78' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/></svg> </div><div class='face'><svg height='100' width='100'>"
HTML$=HTML$+"<circle cx='22' cy='22' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/><circle cx='50' cy='50' r='10' fill='url(#eyes)'/>"
HTML$=HTML$+"<circle cx='22' cy='78' r='10' fill='url(#eyes)'/><circle cx='78' cy='78' r='10' fill='url(#eyes)'/></svg> </div><div class='face'><svg height='100' width='100'>"
HTML$=HTML$+"<circle cx='22' cy='78' r='10' fill='url(#eyes)'/><circle cx='50' cy='50' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/></svg> </div><div class='face'>"
HTML$=HTML$+"<svg height='100' width='100'><circle cx='22' cy='22' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/><circle cx='22' cy='78' r='10' fill='url(#eyes)'/>"
HTML$=HTML$+"<circle cx='78' cy='78' r='10' fill='url(#eyes)'/></svg> </div><div class='face-back'></div><div class='face-back'></div><div class='face-back'></div><div class='face-back'></div>"
HTML$=HTML$+"<div class='face-back'></div><div class='face-back'></div><div class='face-cent'></div><div class='face-cent'></div><div class='face-cent'></div></div></div>"
IF DiceLeft>1 THEN
	HTML$=HTML$+"<div class='Dice2'><div class='cube'>"
	HTML$=HTML$+"<div class='face'><svg height='100' width='100'><defs><radialGradient id='eyes' cx='50%' cy='50%' r='30%' fx='30%' fy='30%'><stop offset='0%' style='stop-color:rgb(192,0,0);stop-opacity:0.4' />"
	HTML$=HTML$+"<stop offset='100%' style='stop-color:rgb(255,0,0);stop-opacity:0.8' /></radialGradient></defs><circle cx='22' cy='22' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/>"
	HTML$=HTML$+"<circle cx='22' cy='50' r='10' fill='url(#eyes)'/><circle cx='78' cy='50' r='10' fill='url(#eyes)'/><circle cx='22' cy='78' r='10' fill='url(#eyes)'/>"
	HTML$=HTML$+"<circle cx='78' cy='78' r='10' fill='url(#eyes)'/></svg> </div><div class='face'><svg height='100' width='100'><circle cx='50' cy='50' r='10' fill='url(#eyes)'/></svg> </div><div class='face'>"
	HTML$=HTML$+"<svg height='100' width='100'><circle cx='22' cy='78' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/></svg> </div><div class='face'><svg height='100' width='100'>"
	HTML$=HTML$+"<circle cx='22' cy='22' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/><circle cx='50' cy='50' r='10' fill='url(#eyes)'/>"
	HTML$=HTML$+"<circle cx='22' cy='78' r='10' fill='url(#eyes)'/><circle cx='78' cy='78' r='10' fill='url(#eyes)'/></svg> </div><div class='face'><svg height='100' width='100'>"
	HTML$=HTML$+"<circle cx='22' cy='78' r='10' fill='url(#eyes)'/><circle cx='50' cy='50' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/></svg> </div><div class='face'>"
	HTML$=HTML$+"<svg height='100' width='100'><circle cx='22' cy='22' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/><circle cx='22' cy='78' r='10' fill='url(#eyes)'/>"
	HTML$=HTML$+"<circle cx='78' cy='78' r='10' fill='url(#eyes)'/></svg> </div><div class='face-back'></div><div class='face-back'></div><div class='face-back'></div><div class='face-back'></div>"
	HTML$=HTML$+"<div class='face-back'></div><div class='face-back'></div><div class='face-cent'></div><div class='face-cent'></div><div class='face-cent'></div></div></div>"
ENDIF
IF DiceLeft>2 THEN
	HTML$=HTML$+"<div class='Dice3'><div class='cube'>"
	HTML$=HTML$+"<div class='face'><svg height='100' width='100'><defs><radialGradient id='eyes' cx='50%' cy='50%' r='30%' fx='30%' fy='30%'><stop offset='0%' style='stop-color:rgb(192,0,0);stop-opacity:0.4' />"
	HTML$=HTML$+"<stop offset='100%' style='stop-color:rgb(255,0,0);stop-opacity:0.8' /></radialGradient></defs><circle cx='22' cy='22' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/>"
	HTML$=HTML$+"<circle cx='22' cy='50' r='10' fill='url(#eyes)'/><circle cx='78' cy='50' r='10' fill='url(#eyes)'/><circle cx='22' cy='78' r='10' fill='url(#eyes)'/>"
	HTML$=HTML$+"<circle cx='78' cy='78' r='10' fill='url(#eyes)'/></svg> </div><div class='face'><svg height='100' width='100'><circle cx='50' cy='50' r='10' fill='url(#eyes)'/></svg> </div><div class='face'>"
	HTML$=HTML$+"<svg height='100' width='100'><circle cx='22' cy='78' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/></svg> </div><div class='face'><svg height='100' width='100'>"
	HTML$=HTML$+"<circle cx='22' cy='22' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/><circle cx='50' cy='50' r='10' fill='url(#eyes)'/>"
	HTML$=HTML$+"<circle cx='22' cy='78' r='10' fill='url(#eyes)'/><circle cx='78' cy='78' r='10' fill='url(#eyes)'/></svg> </div><div class='face'><svg height='100' width='100'>"
	HTML$=HTML$+"<circle cx='22' cy='78' r='10' fill='url(#eyes)'/><circle cx='50' cy='50' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/></svg> </div><div class='face'>"
	HTML$=HTML$+"<svg height='100' width='100'><circle cx='22' cy='22' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/><circle cx='22' cy='78' r='10' fill='url(#eyes)'/>"
	HTML$=HTML$+"<circle cx='78' cy='78' r='10' fill='url(#eyes)'/></svg> </div><div class='face-back'></div><div class='face-back'></div><div class='face-back'></div><div class='face-back'></div>"
	HTML$=HTML$+"<div class='face-back'></div><div class='face-back'></div><div class='face-cent'></div><div class='face-cent'></div><div class='face-cent'></div></div></div>"
ENDIF
IF DiceLeft>3 THEN
	HTML$=HTML$+"<div class='Dice4'><div class='cube'>"
	HTML$=HTML$+"<div class='face'><svg height='100' width='100'><defs><radialGradient id='eyes' cx='50%' cy='50%' r='30%' fx='30%' fy='30%'><stop offset='0%' style='stop-color:rgb(192,0,0);stop-opacity:0.4' />"
	HTML$=HTML$+"<stop offset='100%' style='stop-color:rgb(255,0,0);stop-opacity:0.8' /></radialGradient></defs><circle cx='22' cy='22' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/>"
	HTML$=HTML$+"<circle cx='22' cy='50' r='10' fill='url(#eyes)'/><circle cx='78' cy='50' r='10' fill='url(#eyes)'/><circle cx='22' cy='78' r='10' fill='url(#eyes)'/>"
	HTML$=HTML$+"<circle cx='78' cy='78' r='10' fill='url(#eyes)'/></svg> </div><div class='face'><svg height='100' width='100'><circle cx='50' cy='50' r='10' fill='url(#eyes)'/></svg> </div><div class='face'>"
	HTML$=HTML$+"<svg height='100' width='100'><circle cx='22' cy='78' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/></svg> </div><div class='face'><svg height='100' width='100'>"
	HTML$=HTML$+"<circle cx='22' cy='22' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/><circle cx='50' cy='50' r='10' fill='url(#eyes)'/>"
	HTML$=HTML$+"<circle cx='22' cy='78' r='10' fill='url(#eyes)'/><circle cx='78' cy='78' r='10' fill='url(#eyes)'/></svg> </div><div class='face'><svg height='100' width='100'>"
	HTML$=HTML$+"<circle cx='22' cy='78' r='10' fill='url(#eyes)'/><circle cx='50' cy='50' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/></svg> </div><div class='face'>"
	HTML$=HTML$+"<svg height='100' width='100'><circle cx='22' cy='22' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/><circle cx='22' cy='78' r='10' fill='url(#eyes)'/>"
	HTML$=HTML$+"<circle cx='78' cy='78' r='10' fill='url(#eyes)'/></svg> </div><div class='face-back'></div><div class='face-back'></div><div class='face-back'></div><div class='face-back'></div>"
	HTML$=HTML$+"<div class='face-back'></div><div class='face-back'></div><div class='face-cent'></div><div class='face-cent'></div><div class='face-cent'></div></div></div>"
ENDIF
IF DiceLeft>4 THEN
	HTML$=HTML$+"<div class='Dice5'><div class='cube'>"
	HTML$=HTML$+"<div class='face'><svg height='100' width='100'><defs><radialGradient id='eyes' cx='50%' cy='50%' r='30%' fx='30%' fy='30%'><stop offset='0%' style='stop-color:rgb(192,0,0);stop-opacity:0.4' />"
	HTML$=HTML$+"<stop offset='100%' style='stop-color:rgb(255,0,0);stop-opacity:0.8' /></radialGradient></defs><circle cx='22' cy='22' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/>"
	HTML$=HTML$+"<circle cx='22' cy='50' r='10' fill='url(#eyes)'/><circle cx='78' cy='50' r='10' fill='url(#eyes)'/><circle cx='22' cy='78' r='10' fill='url(#eyes)'/>"
	HTML$=HTML$+"<circle cx='78' cy='78' r='10' fill='url(#eyes)'/></svg> </div><div class='face'><svg height='100' width='100'><circle cx='50' cy='50' r='10' fill='url(#eyes)'/></svg> </div><div class='face'>"
	HTML$=HTML$+"<svg height='100' width='100'><circle cx='22' cy='78' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/></svg> </div><div class='face'><svg height='100' width='100'>"
	HTML$=HTML$+"<circle cx='22' cy='22' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/><circle cx='50' cy='50' r='10' fill='url(#eyes)'/>"
	HTML$=HTML$+"<circle cx='22' cy='78' r='10' fill='url(#eyes)'/><circle cx='78' cy='78' r='10' fill='url(#eyes)'/></svg> </div><div class='face'><svg height='100' width='100'>"
	HTML$=HTML$+"<circle cx='22' cy='78' r='10' fill='url(#eyes)'/><circle cx='50' cy='50' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/></svg> </div><div class='face'>"
	HTML$=HTML$+"<svg height='100' width='100'><circle cx='22' cy='22' r='10' fill='url(#eyes)'/><circle cx='78' cy='22' r='10' fill='url(#eyes)'/><circle cx='22' cy='78' r='10' fill='url(#eyes)'/>"
	HTML$=HTML$+"<circle cx='78' cy='78' r='10' fill='url(#eyes)'/></svg> </div><div class='face-back'></div><div class='face-back'></div><div class='face-back'></div><div class='face-back'></div>"
	HTML$=HTML$+"<div class='face-back'></div><div class='face-back'></div><div class='face-cent'></div><div class='face-cent'></div><div class='face-cent'></div></div></div>"
ENDIF
HTML$=HTML$+"</div></body></html>"
RETURN
! rotation angles for dice faces
READ.DATA "0","180","0","90","270","0","90","0","0","270","0","0"
! Joker
READ.DATA 9,1,10,1,3,2,8,2,9,2,11,2,15,2,2,3,3,3,4,3,8,3,9,3,14,3,15,3,16,3,2,4,4,4,5,4,7,4,8,4,9,4,10,4,13,4,14,4,16,4
READ.DATA 5,5,6,5,7,5,8,5,9,5,10,5,11,5,12,5,13,5,14,5,5,6,6,6,7,6,9,6,11,6,12,6,13,6,5,7,6,7,12,7,13,7,6,8,12,8
READ.DATA 6,9,12,9,5,10,13,10,5,11,13,11,6,12,12,12,6,13,12,13,6,14,12,14,7,15,8,15,10,15,11,15,9,16
READ.DATA 5,13,13,13,3,14,4,14,5,14,13,14,14,14,15,14,2,15,3,15,4,15,5,15,6,15,12,15,13,15,14,15,15,15,16,15
READ.DATA 1,16,2,16,4,16,5,16,6,16,7,16,8,16,10,16,11,16,12,16,13,16,14,16,16,16,17,16
READ.DATA 1,17,5,17,6,17,7,17,8,17,9,17,10,17,11,17,12,17,13,17,17,17,4,18,5,18,7,18,8,18,9,18,10,18,11,18,13,18,14,18
READ.DATA 4,19,5,19,8,19,9,19,10,19,13,19,14,19,3,20,4,20,5,20,8,20,9,20,10,20,13,20,14,20,15,20
READ.DATA 3,21,4,21,9,21,14,21,15,21,9,22
! Category names
READ.DATA "UPPER Section",1,"Ones  ",0,"Twos  ",0,"Threes",0,"Fours ",0,"Fives ",0,"Sixes ",0,"Score",1,"BONUS",1,"TOTAL upper",1,"LOWER Section",1
READ.DATA "3 of a kind",0,"4 of a kind",0,"Full House",0,"small Straight",0,"LARGE Straight",0,"YAHTZEE",1,"Chance",0,"Yahtzee BONUS",1,"TOTAL lower",1,"GRAND TOTAL",1,"Rank",1
