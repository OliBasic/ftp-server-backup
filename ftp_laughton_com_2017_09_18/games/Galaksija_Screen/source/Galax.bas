REM Galaksija screen (v1.86, 800x600)
dim gs[512,2], cs[256]    % screen coord`s, charset
list.create s,raw         % dir list
gr.open 0,55,55,55,0,0  % grey background, landscape
for i=0 to 15             % set 16 row...
 for j=1 to 32            % 32 column...
  gs[(i*32)+j,1]=(j-1)*16 % screen char
  gs[(i*32)+j,2]=i*26     % pixel coordinates
 next j
next i
gr.bitmap.load bm,"charset2x64.bmp"  % load charset
for i=1 to 64             % get 2x64 chars into indexed place
 j=band(i-1,63)*16
 gr.bitmap.crop cs[i+32],bm,j,0,16,26
 gr.bitmap.crop cs[i+128],bm,j,26,16,26
 cs[i+192]=cs[i+128]
 if i<33 then cs[i]=cs[33] else cs[i+64]=cs[i+32]
next i
text.open r,wn,"files.txt" 
do 
text.readln wn,raw$ 
list.add raw,raw$
until raw$ = "EOF" 
text.close wn
ss=1                      % first pic in list
as=37
wakelock 2
do
list.get raw,ss,fn$       % get indexed filename
gr.color 255,0,255,0,1    % text color green
gr.text.size 22
tm=clock()                % beginning time
byte.open r,s,fn$    % open raw pic
gr.bitmap.create tmp,512,416   % new empty galaxy screen
gr.bitmap.drawinto.start tmp   % all drawing on galaxy screen
for i=1 to 512            % we have 16*32 screen chars
 byte.read.byte s,ch      % read them one by one
 gr.bitmap.draw f,cs[ch+1],gs[i,1],gs[i,2] % "print" to galaxy screen
next i
gr.bitmap.drawinto.end
gr.bitmap.draw h,tmp,15,15  % draw finished screen to basic screen
let k=(clock()-tm)/1000         % how much time is elapsed
gr.text.draw fx,570,30,fn$  % picture name
gr.text.draw tx,570,80,str$(k)+" sec"     % elapsed draw time
gr.text.draw rt,650,300,format$("#",10-k) % time until next pic
gr.color 255,255,50,50,1
gr.text.draw px,570,180,"Touch for Previous"  % message to user
gr.text.draw nx,590,400,"Touch for Next"      % message to user
gr.render                 % show all drawings
byte.close s              % close raw pic file
do                        % auto show delay loop
 pause 100
 gr.touch gt,w1,w2        % touch speeds up slide
 k=(clock()-tm)/1000      % calculate elapsed time
 gr.modify rt,"text",format$("%",10-k)  % show remaining time
 gr.render                % draw all
until gt|k>9              % loop until time out (10s) or touch
gr.cls                    % clear all drawings and settings
gr.bitmap.delete tmp      % discard galaxy screen
gr.render                 % show empty background
if (gt&(w2<222)) then let ss=ss-1 else let ss=ss+1  % next/prev file
if ss>as then let ss=1        % if out of list, start again
if !ss then let ss=as
until 0

onerror:
wakelock 5
end
