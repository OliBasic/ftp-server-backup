! Othello  11/2014 by @Cassiope34

Fn.def map( v, in_min, in_max, out_min, out_max )
  Fn.rtn int((v-in_min)*(out_max-out_min)/(in_max-in_min)+out_min)
Fn.end

Fn.def cjj( diX[], diY[], c[], j )   % nbre de coups possibles pour j.
! cj$ =""    % chaine des coups jouables
aj =3-j
for y=1 to 8
  for x=1 to 8
    if c[x,y]=0       % from an empty cell
      ok =0
      for s =1 to 8   % in all directions
        if x+diX[s]>0 & x+diX[s]<9 & y+diY[s]>0 & y+diY[s]<9   % into the grid
          if c[ x+diX[s], y+diY[s] ]=aj
          dx  =x+diX[s]
          dy  =y+diY[s]
          cpt =1
          do
            nok =1
            if dx+diX[s]>0 & dx+diX[s]<9 & dy+diY[s]>0 & dy+diY[s]<9
              dx +=diX[s]
              dy +=diY[s]
              if c[dx,dy]=aj
                cpt++
                nok =0
              elseif c[dx,dy]=j & cpt
                cjt++
                ok =1
              endif
            endif
          until nok
          endif
        endif
        if ok then f_n.break
      next
    endif
  next
next
Fn.rtn cjt
Fn.end

FN.DEF roundCornerRect (b, h, r)
 half_pi = 3.14159 / 2
 dphi    = half_pi / 8
 LIST.CREATE N, S1
 mx      = -b/2+r
 my      = -h/2+r
 FOR phi= 0 TO half_pi STEP dphi
  LIST.ADD s1, mx-COS(phi)*r, my-sin (phi)*r
 NEXT
 mx      = b/2-r
 my      = -h/2+r
 FOR phi= 0 TO half_pi STEP dphi
  LIST.ADD s1, mx+SIN(phi)*r, my- cos (phi)*r
 NEXT
 mx      = b/2-r
 my      = h/2-r
 FOR phi= 0 TO half_pi STEP dphi
  LIST.ADD s1, mx+COS(phi)*r, my+sin (phi)*r
 NEXT
 mx      = -b/2+r
 my      =  h/2-r
 FOR phi= 0 TO half_pi STEP dphi
  LIST.ADD s1, mx-SIN(phi)*r, my+cos (phi)*r
 NEXT
 FN.RTN s1
FN.END

File.Root tmp$
IstandAlone =Is_In("rfo-basic",tmp$)    % to know if it's an APK for the way to exit.

gr.open 255, 0, 128, 0, 0, 1
gr.screen scrx,scry
svx =1600
svy =2560
sx  =scrx/svx
sy  =scry/svy
gr.scale sx,sy

fileSav$ ="othello.sav"
ARRAY.LOAD diX[],0,1,0,-1,-1,1,-1,1  % 1 North  2 East  3 South  4 West  rest=diagonals
ARRAY.LOAD diY[],-1,0,1,0,-1,-1,1,1
Dim cell[8,8], ce[8,8], cptr[8,8], incj[8,8], tbval[8,8], sol[30]
Dim cx1[2], cx2[2], bt[2], cl[2], cpt[2], cj[2], plt[2], undo$[8]

! différentes grilles de valorisation des cases.
!rf$ ="91433419 10222201 42433424 32300323 32300323 42433424 10222201 91433419"
!rf$ ="12-503030303-512 -5-8-2-2-2-2-8-5 03-205050505-203 03-205000005-203 "~
!    +"03-205000005-203 03-205000005-203 -5-8-2-2-2-2-8-5 12-503030303-512"
rf$ ="3001201010200130 0101030303030101 2003050505050320 1003050000050310 "~
    +"1003050000050310 2003050505050320 0101030303030101 3001201010200130"
for y=1 to 8
  for x=1 to 8
   ! tbval[x,y] =val(mid$(word$(rf$,y),x,1))  % seulement pour la 1ere grille.
    tbval[x,y] =val(mid$(word$(rf$,y),x*2-1,2))   % table de valeurs...
  next
next

pos =30                 % grille à 30 pixels du bord.
sc =int((svx-2*pos)/8)  % dimension d'une case

sp =sc-30               % dimension d'un pion
og =(sc-sp)/2
gr.set.stroke 2/sx

gr.bitmap.create bt[1], sp, sp    % white pawn
gr.bitmap.drawinto.start bt[1]
gr.color 255,255,255,212,1
gr.circle nul, sp/2, sp/2, sp/2
gr.bitmap.drawinto.end

gr.bitmap.create bt[2], sp, sp    % black pawn
gr.bitmap.drawinto.start bt[2]
gr.color 255,60,60,60,1
gr.circle nul, sp/2, sp/2, sp/2
gr.bitmap.drawinto.end

gr.bitmap.create grd, svx, svy   % all background
gr.bitmap.drawinto.start grd
gr.color 255,0,0,0,2         % black grid
for y=0 to 8
  gr.line nul, pos, pos+sc*y, pos+8*sc, pos+sc*y
next
for x=0 to 8
  gr.line nul, pos+sc*x, pos, pos+sc*x, pos+8*sc
next

GR.SET.STROKE 14/sx
gr.color 255, 170, 72, 0, 0    % brown
gr.rect nul, 10, 10, svx-10, 8*sc+54
gr.color 255, 170, 72, 0, 1    % brown
gr.rect nul, 10, 8*sc+54, svx-10, svy-10

poly = roundCornerRect (400, 150, 30)    % button "Players"
GR.COLOR 255 ,128 ,128 ,128 ,1
GR.POLY nul , poly , 300 , svy-170
GR.COLOR 255 ,255 ,255 ,255 ,0
GR.POLY nul , poly , 300 , svy-170
GR.COLOR 255 ,100 ,100 ,100 ,0
GR.POLY nul , poly , 300+2 , svy-170+2

poly = roundCornerRect (300, 150, 30)    % button "New"
GR.COLOR 255 ,128 ,128 ,128 ,1
GR.POLY nul , poly , svx/2+20 , svy-170
GR.COLOR 255 ,255 ,255 ,255 ,0
GR.POLY nul , poly , svx/2+20 , svy-170
GR.COLOR 255 ,100 ,100 ,100 ,0
GR.POLY nul , poly , svx/2+22 , svy-170+2

GR.COLOR 255 ,128 ,128 ,128 ,1          % button "Exit"
GR.POLY nul , poly , 1300 , svy-170
GR.COLOR 255 ,255 ,255 ,255 ,0
GR.POLY nul , poly , 1300 , svy-170
GR.COLOR 255 ,100 ,100 ,100 ,0
GR.POLY nul , poly , 1300+2 , svy-170+2
gr.bitmap.draw nul, bt[2], pos+sc*2+70, pos+10*sc
gr.bitmap.draw nul, bt[1], pos+sc*5+70, pos+10*sc
gr.text.size 70
gr.text.align 2
gr.color 255,0,255,255,1
gr.text.draw play, 300, pos+12.4*sc, "    Player(s)"
gr.text.draw nul, svx/2+20, pos+12.4*sc, "NEW"
gr.text.draw nul, 1300, pos+12.4*sc, "EXIT"
gr.bitmap.drawinto.end

GR.SET.STROKE 2/sx
File.exists fe, fileSav$

NEW:       % -------------- New game ---------------
gr.cls
for b=1 to 8
  undo$[b] =""
  for a=1 to 8
    cell[a,b] =0
  next
next
undo =0

gr.bitmap.draw nul,grd,0,0    % draw all the background

for y=1 to 8     %  init graphics pointers in 64 (8x8) cells (pawns, etc...)
  for x=1 to 8
    gr.color 255,0,0,0,1
    gr.bitmap.draw cptr[x,y], bt[1], og+pos+(x-1)*sc, og+pos+(y-1)*sc
    gr.hide cptr[x,y]
    gr.color 100,40,40,40,1     % les petits ronds
    gr.paint.get cl[2]
    gr.color 80,255,255,212,1
    gr.paint.get cl[1]
    gr.circle incj[x,y], pos+(x-1)*sc+sc/2, pos+(y-1)*sc+sc/2, sc/10
    gr.hide incj[x,y]
  next
next

if !fe
  j      =2    % black start.
  g$ ="0000000000000000000000000001200000021000000000000000000000000000"
else
  gosub loadgame
  fe =0
endif
gosub installPos

gr.text.size 90           % scores
gr.text.align 1
gr.color 255,0,255,255,1
gr.text.draw plt[2], pos+sc*2-60, pos+10*sc+sc/2+8, int$(cpt[1])
gr.text.draw plt[1], pos+sc*5-60, pos+10*sc+sc/2+8, int$(cpt[2])

cx1[1] =pos+4*sc
cx1[2] =pos+1*sc+20
cx2[1] =pos+7*sc
cx2[2] =pos+4*sc
gr.color 255,255,210,0,0
gr.rect curs, cx1[j], 10*sc, cx2[j], pos+11*sc    % current player rect.

gr.text.size 70
gr.text.align 2
gr.color 255,0,255,255,1
gr.text.draw mess, svx/2, pos+9*sc, ""   % just for debug
nPlayers =1
gr.text.align 1
gr.text.draw play, 126, pos+12.4*sc, int$(nPlayers)
gr.render

quit =0
new  =0
fin  =0
m$   =""

DO
  gr.modify plt[1], "text", int$(cpt[1])    % nb / color
  gr.modify plt[2], "text", int$(cpt[2])

  gr.modify curs, "left", cx1[j], "right", cx2[j]    % show who play.
  !m$ = int$(px)+","+int$(py)
  gr.modify mess, "text", m$+" "+word$("Black White",3-j)+" play..."
  
  gosub casesjouables   % pour j courant.
  
  sh =1         % montre (1) ou efface (0) les cases jouables cj$ par j.
  gosub affcj   % affiche les cases jouables de j

  cj[j]   =len(cj$)/3
  cj[3-j] =cjj( diX[],diY[], cell[], 3-j)

  if (cj[1]=0 & cj[2]=0) | cpt[1]+cpt[2]=64    % End game detection
    if cpt[1]>cpt[2]
      m$ ="White win !"
    elseif cpt[2]>cpt[1]
      m$ ="Black win !"
    else
      m$ ="Deuce !!"
    endif
    gr.modify mess, "text", m$
    fin =1
  endif  
  if cj[j]=0 & !fin   % pas de coup jouable pour j : passe.
    j =3-j
    gr.modify curs, "left", cx1[j], "right", cx2[j]    % show who play.
    gr.modify mess, "text", m$+" "+word$("Black White",3-j)+" play..."
    gr.render
    if nPlayers=1 then gosub AI    % computer play
    D_U.continue
  endif
  
  do
    gr.touch touched, tx, ty
    if !background() then gr.render
  until touched | quit
  if quit then d_u.break
  
  sh =0         % montre (1) ou efface (0) les cases jouables cj$ par j.
  gosub affcj   % efface les cases jouables de j
  
  do
    gr.touch touched, tx, ty
  until !touched
  tx/=sx
  ty/=sy
  
  px =int((tx-pos)/sc)+1
  py =int((ty-pos)/sc)+1
  
  if py<9     % into the grid
    if is_in(int$(px*10+py),cj$) & !fin    % j joue px,py
      gosub maj
      j =3-j
      gr.modify curs, "left", cx1[j], "right", cx2[j]    % show who play.
      gr.render
      
      if nPlayers=1 & !fin then gosub AI    % computer play
    endif
  
  elseif py=13
    if px<4        %  1 (AI) or 2 players
      nPlayers =3-nPlayers
      gr.modify play, "text", int$(nPlayers)
      if nPlayers=1 then gosub AI    % computer play
      
    elseif px=4 | px=5   % new game
      dialog.message "NEW GAME ?",, go, "YES", "Continue"
      if go=1
        new  =1
        quit =1
      endif
      
    elseif px=7 | px=8    % exit
      quit =1
      
   ! elseif undo           %  UNDO
   !   gosub doUndo
   !   if nPlayers=1 & undo     % undo a second time if AI play.
   !     if val(right$(undo$[undo],1))=3-j
   !       gosub doUndo
   !     endif
   !   endif
    endif
  
  endif
  
UNTIL quit

if new then goto new
gosub savegame
if IstandAlone
  END "Bye...!"
else
  EXIT
endif      %  ---------------  exit  ---------------

onbackkey:    % do nothing.
Back.resume

casesjouables:    % make cj$  : cases jouables par j
cj$ =""    % chaine des coups jouables
aj =3-j
for y=1 to 8
  for x=1 to 8
    if cell[x,y]=0    % from an empty cell
      ok =0
      for s=1 to 8    % in all directions
        if x+diX[s]>0 & x+diX[s]<9 & y+diY[s]>0 & y+diY[s]<9   % into the grid
          if cell[ x+diX[s], y+diY[s] ]=aj
            dx  =x+diX[s]
            dy  =y+diY[s]
            cpt =1
            do
              nok =1
              if dx+diX[s]>0 & dx+diX[s]<9 & dy+diY[s]>0 & dy+diY[s]<9
                dx +=diX[s]
                dy +=diY[s]
                if cell[dx,dy]=aj
                  cpt++
                  nok =0
                elseif cell[dx,dy]=j & cpt
                  cj$ =cj$+int$(x*10+y)+" "
                  ok =1
                endif
              endif
            until nok
          endif
        endif
        if ok then f_n.break
      next
    endif
  next
next
return

affcj:      % affiche ou efface (sh) les possibilités de j (cj$)
if cj$<>""
  for y=1 to 8
    for x=1 to 8
      gr.hide incj[x,y]     % efface par défaut tous les petits ronds.
      if is_in(int$(x*10+y),cj$)
        if sh
          gr.modify incj[x,y], "paint", cl[j]
          gr.show incj[x,y]
        endif
      endif
    next
  next
  gr.render
endif
return

cjval:     % nbre de cases adverses prises par coup dans cj$
if cj$<>""
  cv$ =""
  nc =len(cj$)/3
  for c=1 to nc
    px =val( left$(word$(cj$,c),1))
    py =val(right$(word$(cj$,c),1))
    gosub cv
    cv$ =cv$+int$(stt)+" "
  next
endif
return

cv:     % nbre de cases adverses prises par le coup px,py
stt =0
aj =3-j
for s=1 to 8  % dans les 8 directions
  dx =px
  dy =py
  cpt =0
  nok =0
  do
    if dx+diX[s]>0 & dx+diX[s]<9 & dy+diY[s]>0 & dy+diY[s]<9    % into the grid
      dx +=diX[s]
      dy +=diY[s]
      if cell[dx,dy] =aj
        cpt++
      elseif cell[dx,dy]=0
        nok =1
        cpt =0
      elseif cell[dx,dy]=j
        nok =1
      endif
    else
      nok =1
      cpt =0
    endif
  until nok
  stt+=cpt
next
return

maj:     % met à jour cell[] et l'affichage à partir de px,py joué.
gosub RecUndo
aj =3-j
c$ =int$(px*10+py)+" "
gosub majaff
for s=1 to 8    % dans les 8 directions
  dx =px
  dy =py
  c$ =""
  nok =0
  do
    if dx+diX[s]>0 & dx+diX[s]<9 & dy+diY[s]>0 & dy+diY[s]<9
      dx +=diX[s]
      dy +=diY[s]
      if cell[dx,dy] =aj
        c$ =c$ +int$(dx*10+dy)+" "
      elseif cell[dx,dy]=0
        nok =1
        c$ =""
      elseif cell[dx,dy]=j
        nok =1
      endif
    else
      nok =1
      c$ =""
    endif
  until nok
  if c$<>"" then gosub majaff
next
gosub cpte
return

majaff:
for c=1 to len(c$)/3
  x =val( left$(word$(c$,c),1))
  y =val(right$(word$(c$,c),1))
  cell[x,y] =j
  gr.modify cptr[x,y], "bitmap", bt[j]
  gr.show cptr[x,y]
  gr.render
next
return

cpte:      % pawns count
cpt[1] =0
cpt[2] =0
aj     =3-j
for y=1 to 8
  for x=1 to 8
    if cell[x,y]=j
      cpt[j]++
    elseif cell[x,y]=aj
      cpt[aj]++
    endif
  next
next
return

RecUndo:
undo++
if undo>8    % FIFO
  for u=1 to 7
    undo$[u] =undo$[u+1]
  next
  undo =8
endif
gosub savegrid
undo$[undo] =g$+int$(j)
return

doUndo:
if undo
  g$ =undo$[undo]
  gosub installPos
  gosub cpte
  undo--
  if undo then j =val(right$(undo$[undo],1))
  fin =0
endif
return

AI:       % computer play
gr.modify mess, "text", "computer thinking..."
gr.render
gosub casesjouables    % make cj$
if cj$<>""  
  for so=1 to 30   % reinit sol[]
    sol[so] =0
  next
  gosub cjval     % make cv$ (nb de cases prises par chaque coups de cj$)
  nc =len(cj$)/3
  ns =0
  vr =-10
  for c=1 to nc
    x =val( left$(word$(cj$,c),1))
    y =val(right$(word$(cj$,c),1))
    
    ! valorisation
    vcp =tbval[x,y]
    gosub coins      % si coins appartient à j: vcp = valeur différente...
    gosub bords    % comment ne pas donner les bords ?
    gosub liberte    % modif de vcp avec nbre de liberté APRES avoir joué x,y.

    if vcp>vr      % coup >
      ns =1
      sol[1]  =c   % le coup choisi ds cj$
      vr =vcp
    elseif vcp=vr  % coup =
      ns++
      sol[ns] =c
    endif
  next
  px =0
  py =0
  if ns
    aj =3-j
    for c=1 to ns
      px =val( left$(word$(cj$,sol[c]),1))
      py =val(right$(word$(cj$,sol[c]),1))
      npp =val(word$(cv$,sol[c]))
      if npp=cpt[aj] then f_n.break   % prise de TOUS les pions adverse: gagne.
    next
    if !px      % hasard parmis les solutions équivalentes
      c  =int(rnd()*ns)+1
      px =val( left$(word$(cj$,sol[c]),1))
      py =val(right$(word$(cj$,sol[c]),1))
    endif
    
    ! joue le coup px,py.
    gr.modify cptr[px,py], "bitmap", bt[j]   % clignottement du coup à jouer.
    for cli =1 to 2
      gr.hide cptr[px,py]
      pause 100
      gr.render
      gr.show cptr[px,py]
      pause 200
      gr.render
    next
    gosub maj   % joue le coup choisi
  endif
endif
j =3-j     % passe la main.
return

coins:     % change valeur du coup si coin appartient à j.
aj =3-j
v$ =int$(x*10+y)
nv =tbval[8,1]-2
aa =1
if     is_in(v$,"21 22 12") & cell[1,1]=j
elseif is_in(v$,"71 72 82") & cell[8,1]=j
elseif is_in(v$,"17 27 28") & cell[1,8]=j
elseif is_in(v$,"77 78 87") & cell[8,8]=j
elseif v$="21" & cell[1,1]=aj & cell[3,1]=aj & cell[4,1]=aj & cell[5,1]=aj & cell[6,1]=aj & cell[7,1]=aj
elseif v$="71" & cell[8,1]=aj & cell[3,1]=aj & cell[4,1]=aj & cell[5,1]=aj & cell[6,1]=aj & cell[2,1]=aj
elseif v$="12" & cell[1,1]=aj & cell[1,3]=aj & cell[1,4]=aj & cell[1,5]=aj & cell[1,6]=aj & cell[1,7]=aj
elseif v$="17" & cell[1,8]=aj & cell[1,3]=aj & cell[1,4]=aj & cell[1,5]=aj & cell[1,6]=aj & cell[1,2]=aj
elseif v$="82" & cell[8,1]=aj & cell[8,3]=aj & cell[8,4]=aj & cell[8,5]=aj & cell[8,6]=aj & cell[8,7]=aj
elseif v$="87" & cell[8,8]=aj & cell[8,3]=aj & cell[8,4]=aj & cell[8,5]=aj & cell[8,6]=aj & cell[8,2]=aj
elseif v$="28" & cell[1,8]=aj & cell[3,8]=aj & cell[4,8]=aj & cell[5,8]=aj & cell[6,8]=aj & cell[7,8]=aj
elseif v$="78" & cell[8,8]=aj & cell[3,8]=aj & cell[4,8]=aj & cell[5,8]=aj & cell[6,8]=aj & cell[2,8]=aj
elseif v$="21" & cell[1,1] & cell[3,1] & cell[4,1] & cell[5,1] & cell[6,1] & cell[7,1] & cell[8,1]
elseif v$="71" & cell[8,1] & cell[3,1] & cell[4,1] & cell[5,1] & cell[6,1] & cell[2,1] & cell[1,1]
elseif v$="12" & cell[1,1] & cell[1,3] & cell[1,4] & cell[1,5] & cell[1,6] & cell[1,7] & cell[1,8]
elseif v$="17" & cell[1,8] & cell[1,3] & cell[1,4] & cell[1,5] & cell[1,6] & cell[1,2] & cell[1,1]
elseif v$="82" & cell[8,1] & cell[8,3] & cell[8,4] & cell[8,5] & cell[8,6] & cell[8,7] & cell[8,8]
elseif v$="87" & cell[8,8] & cell[8,3] & cell[8,4] & cell[8,5] & cell[8,6] & cell[8,2] & cell[8,1]
elseif v$="28" & cell[1,8] & cell[3,8] & cell[4,8] & cell[5,8] & cell[6,8] & cell[7,8] & cell[8,8]
elseif v$="78" & cell[8,8] & cell[3,8] & cell[4,8] & cell[5,8] & cell[6,8] & cell[2,8] & cell[1,8]
else
  aa =0
endif
if aa then vcp =nv
return

bords:
aj =3-j
v$ =int$(x*10+y)
nv =tbval[x,y]-8
aa =1
if is_in(v$,"41 51 14 15 84 85 48 58")
  if     v$="41" & (cell[3,1]=aj | (cell[6,1]=aj & cell[5,1]))
  elseif v$="51" & (cell[6,1]=aj | (cell[3,1]=aj & cell[4,1]))
  elseif v$="14" & (cell[1,3]=aj | (cell[1,6]=aj & cell[1,5]))
  elseif v$="15" & (cell[1,6]=aj | (cell[1,3]=aj & cell[1,4]))
  elseif v$="84" & (cell[8,3]=aj | (cell[8,6]=aj & cell[8,5]))
  elseif v$="85" & (cell[8,6]=aj | (cell[8,3]=aj & cell[8,4]))
  elseif v$="48" & (cell[3,8]=aj | (cell[6,8]=aj & cell[5,8]))
  elseif v$="58" & (cell[6,8]=aj | (cell[3,8]=aj & cell[4,8]))
  else
    aa =0
  endif
  if aa then vcp=nv
endif
return

liberte:   % modifie vcp avec le nbre de coups restant possibles après avoir joué x,y.
! ce[8,8]   % grille temporaire.
for b=1 to 8
  for a=1 to 8
    ce[a,b] =cell[a,b]  % init ce[] with cell[]
  next
next
ce[x,y] =j
aj =3-j
for s=1 to 8  % dans les 8 directions: maj
  dx =x
  dy =y
  c$ =""
  nok =0
  do
    if dx+diX[s]>0 & dx+diX[s]<9 & dy+diY[s]>0 & dy+diY[s]<9   % into the grid
      dx +=diX[s]
      dy +=diY[s]
      if ce[dx,dy] =aj
        c$ =c$ +int$(dx*10+dy)+" "
      elseif ce[dx,dy]=0
        nok =1
        c$ =""
      elseif ce[dx,dy]=j
        nok =1
      endif
    else
      nok =1
      c$ =""
    endif
  until nok
  if c$<>""
    for cc=1 to len(c$)/3
      xx =val( left$(word$(c$,cc),1))
      yy =val(right$(word$(c$,cc),1))
      ce[xx,yy] =j
    next
  endif
next
nlib =cjj(diX[], diY[], ce[], j)    % nbre de coups possibles maintenant.
vcp  =vcp + map( nlib, 0,22, 0,5)   % fourchette proportionnelle.
return

savegrid:    % save all the grid in g$.
g$ =""
for y=1 to 8
  for x=1 to 8
    g$=g$+int$(cell[x,y])
  next
next
return

installPos:     % install g$ (all cells) in the grid.
cpt[1] =0
cpt[2] =0
c =0
for y=1 to 8
  for x=1 to 8
    c++
    vl =val(mid$(g$,c,1))
    if cell[x,y]<>vl
      cell[x,y] =vl
      if vl
        gr.modify cptr[x,y], "bitmap", bt[vl]
        gr.show cptr[x,y]
        cpt[vl]++
      else
        gr.hide cptr[x,y]
      endif
    endif
  next
next
gr.render
return

savegame:
gosub savegrid
cls
print j
print g$
console.save fileSav$
cls
RETURN

loadgame:
GRABFILE ldgame$, fileSav$
split ln$[], ldgame$, "\n"
j  =val(ln$[1])
g$ =ln$[2]
array.delete ln$[]
!file.delete fe, fileSav$
return

