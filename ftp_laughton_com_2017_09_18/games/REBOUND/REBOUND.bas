!INCLUDE /functions/functions.bas
gosub userfunctions

flagUseBackgrBmp = 0
flagPortaitMode  = 0
flagUseSounds    = 1
percAimSupport   = 0.6

GR.OPEN 255, 0, 0, 30, 0, 0
PAUSE 100
GR.ORIENTATION 0               %Portrait-mode
GR.SCREEN actual_w, actual_h
refW = 1280
refH = 780

scale_width  = actual_w /refW
scale_height = actual_h /refH
GR.SCALE scale_width, scale_height

!screen center / taskbar
cx       = refW/2
cy       = refH/2
taskbarx = refw*0.9


radBall = 30
diaBall = radBall * 2


bask1X   = radBall * 0.9
bask2X   = bask1X +  1.87* diaBall
bask1Y   = refH*0.33
bask2Y   = bask1Y
bask1Rad = 6
bask2Rad = bask1Rad
centBask = bask1X + (bask2X - bask1X)/2


lirand =  -2*diaball
rerand =  refW*0.9  - diaBall
obrand = -refH
unrand =  refH      - diaBall

sAreaL     = refW  * 0.2
sAreaT     = refH  * 0.3
sAreaR     = refW  * 0.72
sAreaB     = refH  * 0.72
sAreaRangeW= sAreaR - sAreaL
sAreaRangeH= sAreaB - sAreaT

movingFriction    = 0.998
curMovingFriction = movingFriction
bounceFriction    = 0.85
basketBouFric     = 0.8
netFriction       = 0.6
accAmplify        = 1.25
shotAmplify       = 0.35
vBallMax          = diaBall*1.4
vBallMin          = 0.5

!physical-based calculations -------
diaBallReal  = 0.24                    % ball dia in meter gives the scale
dt           = 35                      % des. timeintervall
pix2meter    = diaBallReal / diaBall   % factor pix to meters
pix2mPerSec  = pix2meter   / (dt/1000) % factor pixPerTimeinterv.to mPerSec
pix2mPerSec2 = pix2mPerSec / (dt/1000) % one more...see above

!physical acc's (...when amplify=1, 'tuning' possible) ------
ballAccx    = 0
ballAccy    = 9.81 / pix2mPerSec2 * accAmplify

durationGame        = 90
rechargePerfectShot = 5


ballx   = centbask
bally   = unrand-2*diaball
ballxri = 0
ballyri = 0
criticalCond = 0
IF criticalCond=1
 ballx   = bask2x - radball
 bally   = bask2y - 5 *diaball - bask2rad-5
 ballxri = 0
 ballyri = 0
ENDIF
IF criticalCond=2
 ballx   = bask2x
 bally   = bask2y-3*diaball
 ballxri = -40
 ballyri = 30
ENDIF



ARRAY.LOAD vib[],0,25
vibBorderVal   = 4
vibAmplify=0.6

ARRAY.LOAD vib2[],0,40


GR.BITMAP.LOAD     fuss_ptr, "bball.png"
GR.BITMAP.SCALE    fuss_ptr2, fuss_ptr, diaBall , diaBall

IF flagUseBackgrBmp
 GR.BITMAP.LOAD     backgrBmp, "brickwall5.jpg"
 GR.BITMAP.SCALE    backGrBmpScaled,    backgrBmp, refW, refH
ENDIF

IF flagUseSounds THEN
 AUDIO.LOAD hitSound,"applause-light-02.wav"
 AUDIO.LOAD whistle1,"RefereeWhistle02.mp3"
ENDIF

lenTimeArray = 30
DIM calcTimeArray [lenTimeArray]
DIM tocTimeArray  [lenTimeArray]


anzAimSupport = 15
DIM curParab[ anzAimSupport ]

infoStr2$ = "fps: "



DIM grObjs [50 ]
DIM cntr  [20, 50]
DIM cntr$ [20, 50]


restart:

slomoFac          = 0
percAimSupport    = 0.6
flagStartposFree  =  1
flagshot          =  1
GOSUB redraw


!mainLoop ------------------------------------
tt=clock ()
tic=clock ()

DO

 frameCtr+=1

 IF flagDowncount THEN
  ttimer     =  clock ()
  dttimer   +=  ttimer - ttimerOld
  IF dttimer>=  1000 THEN GOSUB myontimer
  IF flagGameEnd THEN GOSUB gameend
  ttimerOld  =  ttimer
 ENDIF



 !check for pick-up-the-ball / touch controls ----------------
 IF !bSlomo
  GR.TOUCH touch, xx, yy
  IF touch
   xx = xx / scale_width
   IF xx> taskbarx THEN GOSUB controlsHandles
   IF xx< taskbarx THEN GOSUB touchballHandles
  ENDIF
 ENDIF


 !update ball Accel Velocity Position ----------------
 ballAccx  = 0
 IF ballx  < rimPlainX-diaball THEN ballAccx = 0.2

 ballxri   = (ballxri +  ballAccx ) * curMovingFriction
 ballyri   = (ballyri +  ballAccy ) * curMovingFriction
 vBall     = sqr ( ballxri^2+ ballyri^2)

 ballx     = ballx + ballxri
 bally     = bally + ballyri


 !update graphic ---------------------------
 GR.MODIFY  p, "x", ballx
 GR.MODIFY  p, "y", bally



 !check collisions ---------------------------

 IF gr_collision (p, hitCircle ) THEN ticHitCircle =CLOCK()
 hitChk1old = hitChk1
 hitChk1    = gr_collision (p, hitChckRect)
 IF hitChk1 THEN GOSUB hitcheckHandles


 IF slomoFac THEN GOSUB slomoHandles



 collChk1 = gr_collision (p, bask1)
 collChk2 = gr_collision (p, bask2)

 IF collChk1 THEN coll1    = calcBounceFixCircle ( ~
 bask1Rad , radBall , bask1X , bask1Y , ~
 ballx    , bally   , ballxri, ballyri, fnOut[])

 IF collChk2 THEN  coll2 = calcBounceFixCircle ( ~
 bask2Rad , radBall , bask2X , bask2Y , ~
 ballx    , bally   , ballxri, ballyri, fnOut[])


 IF coll1| coll2  THEN GOSUB collBasketHandles


 collRimplain = gr_collision (p, Rimplain )
 IF collRimplain THEN GOSUB  collRimplainHandles

 IF ballx  <= lirand | ballx >= rerand THEN GOSUB wallHandles1
 IF bally  <= obrand | bally >= unrand THEN GOSUB wallHandles2





 !everything should be fine now .....--------
 GR.RENDER



 !(re)settings -------------------------------

 IF bstar
  bstar =0
  GR.SET.STROKE 2
  GR.COLOR 255, 150 , 150, 150 , 0
  GR.POLY  grobjs[1], s2, centbask, bask2y
  GR.NEWDL grobjs[ ]
 ELSE
  IF alphastar> 30 THEN
   alphastar-= 8
   GR.MODIFY grobjs[1],"alpha", alphastar
  ENDIF
 ENDIF

 IF !hitChk1 & hitChk1old THEN GOSUB resetHitChkHandles



 !time measurement (only for info) ----------

 IF flagshot
  calcTime                   = CLOCK()-tic
  ptrcalcTime                += 1
  calcTimeArray[ptrcalcTime] = calcTime
  tocTimeArray [ptrcalcTime] = toc
  IF ptrcalcTime             = lenTimeArray THEN
   ptrcalcTime               = 0
   ARRAY.AVERAGE               calcTimeAvg, calcTimeArray[]
   ARRAY.AVERAGE               tocAvg     , tocTimeArray[]
   !GOSUB updateInfoStr
   GR.MODIFY infoStr2 , "text", "fps: " + STR$(round (1000/calcTimeAvg))
  ENDIF
 ENDIF





 !create constant time step ------------------
 DO
  toc = CLOCK() - tic
 UNTIL toc >= dt
 tic = CLOCK()



UNTIL 0
!--------------------------------------------




!--------------------------------------------
redraw:

GR.CLS


ri = 140
s2 = star ( ri, 3*ri, 7, 0.1, 0.1, 36)
GR.SET.STROKE 3
GR.COLOR 0, 150 , 150, 150 , 0
GR.POLY nn, s2, centbask ,bask2y




GR.TEXT.SIZE 30
GR.COLOR 100, 0,0,0, 1
IF flagUseBackgrBmp THEN GR.BITMAP.DRAW backgr, backGrBmpScaled, 0, 0
gr.modify backgr, "alpha", 30

GR.SET.STROKE 3
GR.COLOR 220, 250,250,250, 1
GR.LINE infoArea, refW * 0.9, refH * 0.0 , refW * 0.9 , refH * 1.0


GR.SET.STROKE 3
GR.TEXT.SIZE 28
GR.TEXT.ALIGN 2
GR.COLOR 220, 250,250,250, 1
GR.TEXT.DRAW nn         , refW * 0.95, refh*0.05  , "rBound"
GR.TEXT.DRAW nn         , refW * 0.95, refh*0.15  , "shot / hit"
GR.COLOR 250, 2,200,200, 1
GR.TEXT.DRAW rbounds    , refW * 0.95, refh*0.09  , "0"
GR.TEXT.DRAW shots      , refW * 0.95, refh*0.19  , "0"

GR.TEXT.SIZE 35
GR.TEXT.ALIGN 1
GR.COLOR 220, 2,200,200, 1
GR.TEXT.DRAW minsSecs   , refW * 0.03, refh*0.05  , " 00:00   0"
gr.hide minsSecs
gr.paint.get cyanLetters
GR.COLOR 250, 255,0,0, 1
gr.paint.get redLetters
GR.COLOR 250, 20,250,20, 1
gr.paint.get greenLetters



anzControls = 5

cntr$[1,1] = "Reset CTRs"
cntr$[2,1] = "SLOMO off"
cntr$[3,1] = "AIM-Lev 1" 
cntr$[4,1] = "POS free"
cntr$[5,1] = "New Game"


for no=1 to anzControls

cntr[no,1]  = taskbarx + 10
cntr[no,2]  = refh *(0.92 - (no-1)*0.10)
cntr[no,3]  = refw       - 10
cntr[no,4]  = cntr[no,2] + 55
cntr[no,5]  = cntr[no,1] * scale_width
cntr[no,6]  = cntr[no,2] * scale_height
cntr[no,7]  = cntr[no,3] * scale_width
cntr[no,8]  = cntr[no,4] * scale_height
cntr[no,20] = 1                           % enabled/disabled
cntr[no,30] = 3                                       
cntr[no,31] = 2
cntr[no,32] = 20
cntr[no,33] = 230
cntr[no,34] = 170
cntr[no,34] = 100
cntr[no,40] = (cntr[no,1]+cntr[no,3])/2
cntr[no,41] = (cntr[no,2]+cntr[no,4])/2+cntr[no,32]/2
GR.SET.STROKE cntr[no,30]
Gr.text.align cntr[no,31]
Gr.text.size  cntr[no,32]
GR.COLOR      cntr[no,33] , 255,255,255,1
gr.text.draw  cntr[no,49] , cntr[no,40], cntr[no,41], cntr$[no,1]
GR.COLOR      cntr[no,33] , 255,255,255,0
gr.rect       cntr[no,50] , cntr[no,1] , cntr[no,2] , cntr[no,3], cntr[no,4]

next




GR.TEXT.SIZE 25
GR.TEXT.ALIGN 1
GR.COLOR 175, 180 , 180 , 180 , 1
GR.TEXT.DRAW infoStr2, 650, 50 , curInfoStr2$


GR.SET.STROKE 2* bask1Rad
GR.COLOR 255, 75 , 75 , 75 , 1
GR.LINE nn, -2, bask1y , bask1x , bask1y

GR.SET.STROKE 1
GR.COLOR 225, 180,180,180, 1
rimPlainX         = 5
rimplaneTop       = bask1y -2.5*diaBall
rimplaneTopBorder = rimplaneTop - radball
GR.RECT rimPlain , -1 , rimplaneTop , rimPlainX , bask1y+0.5*diaBall


GR.SET.STROKE 5
GR.COLOR 255, 150,150,150, 1
knot1x = bask1X + radBall/1.7
knot1y = bask1y + radball
knot2x = bask2X - radBall/1.7
knot2y = bask2y + radball
knot3x = bask1X + radBall*0.9
knot3y = bask1y + radBall*3
knot4x = bask2X - radBall*0.9
knot4y = bask2y + radBall*3
GR.LINE  netL1  ,  bask1X  , bask1Y ,  knot1x , knot1y
GR.LINE  netL2  ,  knot1x  , knot1y ,  knot3x , knot3y
GR.LINE  netR1  ,  bask2X  , bask2Y ,  knot2x , knot2y
GR.LINE  netR2  ,  knot2x  , knot2y ,  knot4x , knot4y
GR.COLOR 105, 150,150,150, 1
GR.LINE  netLR1 ,  knot1x  , knot1y ,  knot2x , knot2y
GR.LINE  netLR2 ,  knot3x  , knot3y ,  knot4x , knot4y


GR.COLOR 255, 100,00,00, 1
GR.RECT   baskringRect, ~
bask1X , bask1Y-bask1Rad, ~
bask2X , bask2Y+bask2Rad


GR.COLOR 255, 150,00,00, 1
GR.CIRCLE bask1, bask1X , bask1Y  , bask1Rad
GR.CIRCLE bask2, bask2X , bask2Y  , bask2Rad


GR.SET.STROKE 1
GR.COLOR 15, 150,00,00, 0
GR.RECT slomoRange , ~
bask1X-diaBall    , bask1Y - 5*diaBall, ~
bask2X+radBall*1.5, bask1Y 

GR.COLOR 15 , 255,0,0, 0
GR.circle hitCircle, centbask, bask1y-5, 5

GR.COLOR 15 , 255,0,0, 0
GR.RECT   hitChckRect, ~
0      , bask1Y + 0.99*diaBall + bask1Rad , ~
bask2X , bask1Y + 0.99*diaBall + bask1Rad + 40

GR.SET.STROKE 5
GR.COLOR 255, 0,250,0, 1
GR.LINE line1, 0, 0, 0, 0
GR.HIDE line1


GR.SET.STROKE 2
GR.COLOR 255, 0,250,0,1
FOR i = 1 TO anzAimSupport
 GR.LINE  curParab[i], 0 , 0 , 0 , 0
 GR.HIDE curParab[ i ]
NEXT

GR.SET.STROKE 6
GR.COLOR 255, 250,0,0,0
GR.CIRCLE forbiddenSign , 0, 0, radball*1.15
GR.HIDE forbiddenSign

GR.SET.STROKE 3
GR.COLOR 255, 0,255,0, 0
GR.CIRCLE allowedSign , 0, 0,radball*1.05
GR.HIDE allowedSign



GR.SET.STROKE 3
GR.COLOR 0, 0,255,0, 0
GR.RECT startAreaRect,  sAreaL , sAreaT , sAreaR , sAreaB
GR.SET.STROKE 1


GR.COLOR 255, 255,0,0, 1
GR.BITMAP.DRAW  p, fuss_ptr2, ballx , bally

anzGrObjs = p

undim grObjs [  ]
DIM   grObjs [ anzGrObjs ]
FOR i=1 TO anzGrObjs
 grObjs [ i ] = i
NEXT

RETURN
!--------------------------------------------


!--------------------------------------------
resetHitChkHandles:

curMovingFriction = movingFriction
GR.MODIFY netL1, "x2", knot1x
GR.MODIFY netL2, "x1", knot1x
GR.MODIFY netR1, "x2", knot2x
GR.MODIFY netR2, "x1", knot2x
GR.MODIFY netLR1,"x1", knot1x
GR.MODIFY netLR1,"x2", knot2x

RETURN
!--------------------------------------------


!--------------------------------------------
wallHandles1:

ballxri  = -ballxri * bounceFriction
IF ballx < lirand
 ballx = lirand
 IF bally <bask1y THEN bally= bask1y+2*diaball
ENDIF
IF ballx > rerand THEN ballx = rerand
GR.MODIFY  p, "x", ballx
flagWallHit = 1
vib[2] = vball * vibAmplify
IF vball > vibBorderVal THEN VIBRATE vib[],-1

RETURN
!--------------------------------------------


!--------------------------------------------
wallHandles2:

IF !flagBallGrounded
 ballyri  = -ballyri * bounceFriction
 IF abs (ballyri) < vBallMin & bally> unrand-1.2*diaball THEN ballyri=0
 IF ballyri=0  THEN flagBallGrounded=1
 IF bally < obrand THEN bally = obrand
 IF bally > unrand THEN bally = unrand
 GR.MODIFY  p, "y", bally
 flagGroundHit = 1
 vib[2] = vball * vibAmplify
 IF abs (ballyri) > vibBorderVal THEN VIBRATE vib[],-1
ELSE
 bally = unrand
 ballyri=0
ENDIF

RETURN
!--------------------------------------------


!--------------------------------------------
collRimplainHandles:
IF bally > rimplaneTopBorder
 ballx = rimPlainX +1
 ballxri  = -ballxri * bounceFriction
 GR.MODIFY  p, "x", ballx
 flagWallHit = 1
 vib[2] = vball * vibAmplify
 IF vball > vibBorderVal THEN VIBRATE vib[],-1
ENDIF

RETURN
!--------------------------------------------


!--------------------------------------------
collBasketHandles:

flagBounceBasket = 1
coll1 = 0
coll2 = 0

bFrictionAct = basketBouFric      % 1 - basketBouFric * vBall / vBallMax
! if vball < 10 then bFrictionAct = 1.01

ballx   = fnOut[1]
bally   = fnOut[2]
ballxri = fnOut[3] * bFrictionAct
ballyri = fnOut[4] * bFrictionAct
GR.MODIFY  p, "x", ballx
GR.MODIFY  p, "y", bally
vib[2] = vball * vibAmplify
IF ABS(ballxri) | ABS(ballyri) > vibBorderVal THEN VIBRATE vib[],-1

RETURN
!--------------------------------------------


!--------------------------------------------
consecutiveCount:

IF !(hitCtr-hitCtrOld-1)
 consecHitCtr += 1
 IF consecHitCtr> consecHitCtrMAX THEN consecHitCtrMAX = consecHitCtr
ELSE
 IF !flagRebound THEN consecHitCtr  = 0
ENDIF
hitCtrOld= hitCtr

RETURN
!--------------------------------------------


!--------------------------------------------
slomoHandles:

bSlomoOld = bSlomo
bSlomo    = gr_collision (p,slomoRange)
IF bSlomo & !bSlomoOld
 ballxri= ballxri/ slomoFac
 ballyri= ballyri/ slomoFac
 ballAccy= ballAccy / slomoFac^2
ENDIF
IF !bSlomo & bSlomoOld
 ballxri= ballxri* slomoFac*0.5
 ballyri= ballyri* slomoFac*0.5
 ballAccy= ballAccy* slomoFac^2
ENDIF
RETURN
!--------------------------------------------


!--------------------------------------------
hitcheckHandles:

ticHitChkOld = ticHitChk
ticHitChk    = clock ()
IF ( CLOCK() - ticHitCircle ) < 500 & ( clock ()- ticHitChkOld )>1500 THEN

 IF flagUseSounds
  AUDIO.STOP
  AUDIO.PLAY hitSound
 ENDIF

 flagHit =  1
 hitCtr  += 1
 GOSUB consecutiveCount
 ptrCtr += consecHitCtr
 IF consecHitCtr = 0 THEN ptrCtr += 1

 GOSUB updateStartpos

 GOSUB updateInfoStr

 IF !slomoFac
  curMovingFriction = netFriction
  deltaCentPos      = centBask - (ballx+radBall)
  ballxri           = ( deltaCentPos ) * 0.6
  shift             = 0.3* deltaCentPos
  GR.MODIFY netL1, "x2", knot1x - shift
  GR.MODIFY netL2, "x1", knot1x - shift
  GR.MODIFY netR1, "x2", knot2x - shift
  GR.MODIFY netR2, "x1", knot2x - shift
  GR.MODIFY netLR1,"x1", knot1x - shift
  GR.MODIFY netLR1,"x2", knot2x - shift
 ENDIF

 IF !flagBounceBasket & !flagWallHit
  flagPerfectShot = 1
  curTrchg += rechargePerfectShot
  GR.MODIFY minsSecs,"paint", greenLetters
  rr=35
  GR.COLOR 200, 150*RND()+80 , 150*RND()+80 , 150*RND()+80 , 1
  s2 = star ( rr, rr*3, 7 , 0.3, 0.2, 10 )
  GR.POLY nn, s2, centbask, bask2y
  alphastar=255
  bstar=1
 ENDIF

ENDIF

RETURN
!--------------------------------------------


!--------------------------------------------
touchballHandles:

p1x = (ballx -   radBall)* scale_width
p1y = (bally -   radBall)* scale_height
p2x = (ballx + 3*radBall )* scale_width
p2y = (bally + 3*radBall )* scale_height
GR.BOUNDED.TOUCH touchBall, p1x , p1y , p2x , p2y

IF touchBall THEN

 IF flagshot THEN GOSUB resetShot

 IF !flagStartArea
  GOSUB slidingAround
 ELSE
  GOSUB aim
 ENDIF

ENDIF


RETURN
!--------------------------------------------


!--------------------------------------------
controlsHandles:

FOR  no = 1 TO anzControls
 GR.BOUNDED.TOUCH cntr[no,48], cntr[no,5], cntr[no,6], cntr[no,7], cntr[no,8]
NEXT
IF cntr[1,48] THEN GOSUB but1
IF cntr[2,48] THEN GOSUB but2
IF cntr[3,48] THEN GOSUB but3
IF cntr[4,48] THEN GOSUB but4
IF cntr[5,48] THEN GOSUB but5

RETURN
!--------------------------------------------


!--------------------------------------------
but1:
no=1
nn=hitReleaseControl ( cntr[], no, 1)
if !flagdowncount
 reboundCtr        = 0
 shotCtr           = 0
 hitCtr            = 0
 GOSUB updateInfoStr
 endif

nn=hitReleaseControl ( cntr[], no, 0)
PAUSE 200

RETURN
!--------------------------------------------

!--------------------------------------------
but2:
no=2
nn=hitReleaseControl ( cntr[], no, 1)

slomoFac+=4
IF slomoFac> 8.1 THEN slomoFac=0
cntr$[no,1] = "SLOMO" + FORMAT$("##",slomoFac)
IF slomoFac=0 THEN cntr$[no,1] = "SLOMO off"
GR.MODIFY cntr[ no ,49],"text" , cntr$[no,1]

nn=hitReleaseControl ( cntr[], no, 0)
PAUSE 200

RETURN
!--------------------------------------------

!--------------------------------------------
but3:
no=3
nn=hitReleaseControl ( cntr[], no, 1)

aimsupLevel+=1
IF aimsupLevel> 2 THEN aimsupLevel=0
percAimSupport = 0.6 + aimsupLevel*0.15
cntr$[no,1] = "AIM-Lev" + FORMAT$("%", aimsupLevel+1 )

GR.MODIFY cntr[ no ,49],"text" , cntr$[no,1]

nn=hitReleaseControl ( cntr[], no, 0)
PAUSE 200

RETURN

!--------------------------------------------

!--------------------------------------------
but4:
no=4
nn=hitReleaseControl ( cntr[], no, 1)

IF !flagDowncount

 flagStartposFree = !flagStartposFree
 cntr$[no,1] = "Pos FREE"
 IF !flagStartposFree THEN
  cntr$[no,1] = "Pos GIVEN"
 ENDIF
 GOSUB updateStartpos
 GR.MODIFY cntr[ no ,49],"text" , cntr$[no,1]

ELSE

 flagGamepaused = ! flagGamepaused

 IF !flagGamepaused
  cntr$[no,1]       = "PAUSE"
  IF flagresetPauseVal
   ballxri = ballxriTmp
   ballyri = ballyriTmp
  ENDIF
  if flagshot then curMovingFriction = movingFriction
 ELSE
  flagresetPauseVal = 1
  ballxriTmp        = ballxri
  ballyriTmp        = ballyri
  curMovingFriction = 0
  cntr$[no,1]="PAUSED...."
 ENDIF

 GR.MODIFY cntr[ no ,49],"text" , cntr$[no,1]

ENDIF

nn=hitReleaseControl ( cntr[], no, 0)
PAUSE 200

RETURN
!--------------------------------------------

!--------------------------------------------
but5:
no=5

nn=hitReleaseControl ( cntr[], no, 1)

! TIMER.CLEAR
IF flagDowncount

 flagGameEnd       = 1
 flagBreaked       = 1

ELSE

 flagshot          = 1
 flagStartArea     = 0
 curMovingFriction = movingFriction
 flagStartposFree  = 0
 flagBallGrounded  = 0
 flagGamepaused    = 0
 flagresetPauseVal = 0

 reboundCtr        = 0
 shotCtr           = 0
 hitCtr            = 0
 ptrCtr            = 0
 consecHitCtr      = 0
 consecHitCtrMAX   = 0
 curTrchg          = 0

 ballx         = cx-radball
 bally         = cy-radball
 ballxri       = 0
 ballyri       = -50

 secs          = 0
 flagDowncount = 1
 cntDwnSt      = durationGame

 GR.MODIFY minsSecs,"paint", cyanLetters

 GR.MODIFY startAreaRect ,  "alpha" ,  255
 GR.MODIFY startAreaRect ,  "left"  ,  cx-radball/2
 GR.MODIFY startAreaRect ,  "top"   ,  cy-radball/2
 GR.MODIFY startAreaRect ,  "right" ,  cx+radball/2
 GR.MODIFY startAreaRect ,  "bottom",  cy+radball/2

 GR.MODIFY p             , "x", ballx
 GR.MODIFY p             , "y", bally
 GR.MODIFY forbiddenSign , "x", cx
 GR.MODIFY forbiddenSign , "y", cy
 GR.MODIFY cntr[ no ,49],"text" , cntr$[no,1]
 GR.MODIFY cntr[ no ,49],"alpha", cntr[no,33]
 GR.MODIFY cntr[ no ,50],"alpha", cntr[no,33]

 GR.MODIFY cntr[ 4 ,49],"text" ,  "PAUSE"
 GR.MODIFY cntr[ 5 ,49],"text" ,   "STOP"

 GR.HIDE allowedSign
 GR.HIDE line1
 FOR i = 1 TO anzAimSupport
  GR.HIDE curParab[ i ]
 NEXT
 GR.SHOW forbiddenSign

 GOSUB updateTimerStr
 GR.SHOW minsSecs

 GOSUB updateInfoStr

 GR.RENDER

 PAUSE 750

 IF flagUsesounds
  AUDIO.STOP
  AUDIO.PLAY whistle1
 ENDIF

 GR.HIDE forbiddenSign
 GR.RENDER

 !timer.set 1000
 secs       = 0
 curTimer   = 0
 ttimer     = clock ()
 ttimerOld  = ttimer

ENDIF

nn=hitReleaseControl ( cntr[], no, 0)

RETURN
!--------------------------------------------



!--------------------------------------------
gameEnd:

GR.TOUCH touIn, nn, mm

GR.RENDER

!TIMER.CLEAR

IF !flagbreaked

 IF flagUsesounds
  AUDIO.STOP
  AUDIO.PLAY whistle1
 ENDIF

 PAUSE 1000

 GR.HIDE p
 GR.HIDE startAreaRect
 GR.HIDE allowedSign
 GR.HIDE forbiddenSign
 GR.HIDE line1
 FOR i = 1 TO anzAimSupport
  GR.HIDE curParab[ i ]
 NEXT

 GR.TEXT.ALIGN 1
 GR.COLOR 235, 255, 255 , 255, 1
 offx = 220
 px= refw*0.35
 py= refh*0.45
 GR.TEXT.SIZE 45
 GR.TEXT.DRAW tt, px      , py -80     , "Game over !"
 GR.TEXT.SIZE 35
 GR.TEXT.DRAW tt, px      , py +50 * 0 , "shots"
 GR.TEXT.DRAW tt, px+offx , py +50 * 0 ,  format$ ("#%", shotctr)
 GR.TEXT.DRAW tt, px      , py +50 * 1 , "hits"
 GR.TEXT.DRAW tt, px+offx , py +50 * 1 ,  format$ ("#%", hitctr)
 GR.TEXT.DRAW tt, px      , py +50 * 2 , "long. series  "
 GR.TEXT.DRAW tt, px+offx , py +50 * 2 ,  format$ ("#%", consecHitCtrmax) + "×"
 GR.TEXT.DRAW tt, px      , py +50 * 3 , "rebounds  "
 GR.TEXT.DRAW tt, px+offx , py +50 * 3 ,  format$ ("#%", reboundCtr )
 GR.TEXT.SIZE 45
 GR.TEXT.DRAW tt, px      , py +60 * 4 , "Points "
 GR.TEXT.DRAW tt, px+offx , py +60 * 4 ,  format$ ("##%", ptrCtr )

 GR.RENDER

 IF touIn
  DO
   GR.TOUCH tou, nn, mm
  UNTIL !tou
 ENDIF

 DO
  GR.TOUCH tou, nn, mm
 UNTIL tou

 DO
  GR.TOUCH tou, nn, mm
 UNTIL !tou

ENDIF

ballx = centbask
bally = unrand - 2*diaball
ballxri=5
ballyri=0
flagGameEnd       = 0
flagDowncount     = 0
flagshot          = 1
flagStartArea     = 0
flagPerfectShot   = 0
flagStartposFree  = 0
flagBallGrounded  = 0
flagresetPauseVal = 0
flagGamepaused    = 0
flagBreaked       = 0
shotctr           = 0
hitctr            = 0
reboundctr        = 0
curRechPerfectShot= 0
curMovingFriction = movingFriction

GR.MODIFY cntr[ 4 ,49],"text" , "POS free"
GR.MODIFY cntr[ 5 ,49],"text" , "New Game"


GOSUB redraw


RETURN
!--------------------------------------------




!--------------------------------------------
myONTIMER:

IF ! flagGamepaused
 IF !curTrchg
  curTimer += dttimer
 ELSE
  curTrchg    -= 1
  IF curTrchg = 0 THEN GR.MODIFY minsSecs,"paint", cyanLetters
 ENDIF
ENDIF

dttimer   =  0

secs  = round ( curTimer/1000 )

GOSUB updateTimerStr

RETURN
! TIMER.RESUME
!--------------------------------------------


!--------------------------------------------
updateTimerStr:

IF !flagDowncount
 secs$ =  FORMAT$("%%", mod   (secs, 60 ))
 mins$ =  FORMAT$("%%", floor (secs/ 60 ))
ELSE
 secs$ =  FORMAT$("%%", mod   ( cntDwnSt -secs  , 60 ))
 mins$ =  FORMAT$("%%", floor (( cntDwnSt -secs) / 60 ))
 IF cntDwnSt-secs <= 0 THEN   flagGameend=1
 IF cntDwnSt-secs <= 10 & !curTrchg THEN   GR.MODIFY minsSecs,"paint", redLetters
ENDIF

tStr$   = REPLACE$( mins$ +":"+ secs$ , " " , "" )
ptrStr$ = REPLACE$( FORMAT$("##%",    ptrCtr   ) , " " , "" )
tmp3$   = REPLACE$( FORMAT$("##%", consecHitCtr) , " " , "" ) +"×"
IF consecHitCtr = 0 THEN tmp3$ = "--"
GR.MODIFY minsSecs , "text", tStr$ + "   " + ptrStr$ + "    " + tmp3$



RETURN
!--------------------------------------------




!--------------------------------------------
updateStartpos:

IF !flagStartposFree
 newStartL = sAreaL + rnd () * sAreaRangeW
 newStartT = sAreaT + rnd () * sAreaRangeH
 GR.MODIFY startAreaRect ,  "alpha" ,220
 GR.MODIFY startAreaRect ,  "left"  ,  newStartL
 GR.MODIFY startAreaRect ,  "top"   ,  newStartT
 GR.MODIFY startAreaRect ,  "right" ,  newStartL+radBall
 GR.MODIFY startAreaRect ,  "bottom",  newStartT+radBall
ELSE
 GR.MODIFY startAreaRect ,  "alpha" ,  0
 GR.MODIFY startAreaRect ,  "left"  ,  sAreaL
 GR.MODIFY startAreaRect ,  "top"   ,  sAreaT
 GR.MODIFY startAreaRect ,  "right" ,  sAreaR
 GR.MODIFY startAreaRect ,  "bottom",  sAreaB
ENDIF

RETURN
!--------------------------------------------


!--------------------------------------------
updateInfoStr:

tmp0$ = REPLACE$( FORMAT$("##%", reboundCtr ) ," ","")
tmp1$ = REPLACE$( FORMAT$("##%", shotCtr) ," ","")
tmp2$ = REPLACE$( FORMAT$("##%", hitCtr) ," ","")

GR.MODIFY rbounds , "text", tmp0$
GR.MODIFY shots   , "text", tmp1$ + " / "+ tmp2$

RETURN
!--------------------------------------------


!--------------------------------------------
resetShot:

ballxri           = 0
ballyri           = 0
curMovingFriction = 0

IF (flagWallHit|flagBounceBasket) & !flagGroundHit & !flaghit THEN
 reboundCtr+=1
 flagRebound=1
ENDIF

IF !flaghit THEN GOSUB consecutiveCount

GOSUB updateInfoStr
flagRebound            = 0
flagStartArea          = 0
flagBounceBasket       = 0
flagGroundHit          = 0
flagShot               = 0
flagWallHit            = 0
flaghit                = 0
flagBallGrounded       = 0
alphastar              = 0

flagGameEnd            = 0

GR.MODIFY infoStr2, "text", infoStr2$ + "-"

! hide possible rest of star animation
GR.MODIFY grobjs[1],"alpha", 0


RETURN
!--------------------------------------------

!--------------------------------------------
slidingAround:
DO

 IF flagDowncount THEN
  ttimer     =  clock ()
  dttimer   +=  ttimer - ttimerOld
  IF dttimer>=  1000 THEN GOSUB myontimer
  ttimerOld  =  ttimer
 ENDIF

 GR.TOUCH touch2 , sx, sy
 sx = sx / scale_width
 sy = sy / scale_height
 GR.MODIFY  p, "x", sx - radBall
 GR.MODIFY  p, "y", sy - radBall
 flagStartArea = gr_collision ( p,startAreaRect)
 IF !flagStartArea THEN
  GR.MODIFY  forbiddenSign , "x", sx
  GR.MODIFY  forbiddenSign , "y", sy
  GR.SHOW forbiddenSign
  GR.HIDE allowedSign
 ELSE
  GR.HIDE forbiddenSign
  GR.MODIFY  allowedSign , "x", sx
  GR.MODIFY  allowedSign , "y", sy
  GR.SHOW allowedSign
 ENDIF
 GR.RENDER
UNTIL !touch2 | flagGameEnd

ballx = sx - radBall
bally = sy - radBall

GR.MODIFY line1 , "x1", sx
GR.MODIFY line1 , "y1", sy
GR.MODIFY line1 , "x2", sx
GR.MODIFY line1 , "y2", sy

RETURN
!--------------------------------------------


!--------------------------------------------
aim:

GR.SHOW line1
FOR i = 1 TO anzAimSupport
 GR.SHOW curParab[ i ]
NEXT
GR.HIDE allowedSign

DO

 IF flagDowncount THEN
  ttimer     =  clock ()
  dttimer   +=  ttimer - ttimerOld
  IF dttimer>=  1000 THEN GOSUB myontimer
  ttimerOld  =  ttimer
 ENDIF

 GR.TOUCH touch2 , x ,y
 x = x / scale_width
 y = y / scale_height

 radakt = sqr ( (sx-x)^2  + (sy-y)^2 )

 IF radakt < vBallMax / shotAmplify

  vx = (sx-x) * shotAmplify
  vy = (sy-y) * shotAmplify

  GR.MODIFY line1 , "x2", x
  GR.MODIFY line1 , "y2", y

  nn = ballParabel (x, y,vx, vy, centBask, sx,sy, ballAccy, ~
  curParab[], anzAimSupport, percAimSupport, taskbarx )

  GR.RENDER

 ENDIF

UNTIL !touch2  | flagGameEnd

GR.SHOW allowedSign

GR.HIDE line1
FOR i = 1 TO anzAimSupport
 GR.HIDE curParab[ i ]
NEXT


IF radakt<vBallMax/shotAmplify*1.5 & radakt>radball

 ! after final release .....
 ballx   = sx - radBall
 bally   = sy - radBall
 ballxri = vx
 ballyri = vy
 curMovingFriction = movingFriction

 shotCtr      += 1
 flagShot      = 1
 flagStartArea = 0
 flagNoTimeCnt = 1
 GOSUB updateInfoStr

 GR.HIDE allowedSign

 ptrcalcTime   = 0
 tic           = CLOCK()

ENDIF


RETURN
!--------------------------------------------



!--------------------------------------------
userfunctions:
dim fnOut [10]

!--------------------------------------------
FN.DEF waitForTouch()
 DO
  GR.TOUCH touTmp, xx, yy
 UNTIL touTmp
 tic=clock ()
 DO
  GR.TOUCH touTmp, xx, yy
 UNTIL !touTmp
 fn.rtn clock ()-tic
FN.END
!--------------------------------------------


!--------------------------------------------
FN.DEF hitReleaseControl ( cntr[], no, edge)
 IF cntr[no ,20]
  IF edge
   tic=CLOCK()
   GR.MODIFY cntr[no ,49],"alpha", cntr[no,34]
   GR.MODIFY cntr[no ,50],"alpha", cntr[no,34]
   GR.RENDER
   DO
    GR.TOUCH touTmp, xx, yy
   UNTIL! touTmp
   FN.RTN CLOCK()-tic
  ELSE
   GR.MODIFY cntr[ no ,49],"alpha", cntr[no,33]
   GR.MODIFY cntr[ no ,50],"alpha", cntr[no,33]
   GR.RENDER
  ENDIF
 ENDIF
FN.END
!--------------------------------------------


!--------------------------------------------
FN.DEF ballParabel (x, y,vx, vy, centBask,sx,sy, g,grObj[],anzAimSupport,percAimSupport, taskbarx )
 IF vx=0 THEN vx=0.001
 beta  = ATAN(vy/vx)
 v     = sqr (vx^2+vy^2)
 k1    = - g / (2 * v^2 * COS(beta)^2 )
 stepW = (sx - centBask) * percAimSupport / anzAimSupport
 IF vx> 0 THEN stepW= stepW*-1
 xold=sx
 yold=sy
 ctr=1
 DO
  ctr  += 1
  i    =  i+ stepW
  cc   = tan ( beta ) * i + k1 * i^2 * 1.07
  xx = sx - i
  yy = sy - cc
  IF xx > taskbarx THEN xx= taskbarx
  GR.MODIFY  grObj [ ctr ] , "x1", xold
  GR.MODIFY  grObj [ ctr ] , "y1", yold
  GR.MODIFY  grObj [ ctr ] , "x2", xx
  GR.MODIFY  grObj [ ctr ] , "y2", yy
  xold = xx
  yold = yy
 UNTIL ctr= anzAimSupport
FN.END
!--------------------------------------------


!--------------------------------------------
FN.DEF calcBounceFixCircle( rad1, rad2 ,x1, y1, x2, y2, vx, vy, fn[])
 nomDist    = rad1+rad2
 x2         = x2+ rad2 -x1
 y2         = y2+ rad2 -y1
 curDist    = SQR(x2^2+y2^2)
 IF curDist/nomDist >= 0.99 THEN FN.RTN 0
 IF vx=0 THEN vx=0.01
 m          = vy/vx
 n          = y2 - m*x2
 fac        = 1
 IF vx      <=0 THEN fac=-1
 xs         = - ( m*n + fac*SQR( nomDist ^2 * (m^2+1) - n^2 ) ) / (m^2 + 1)
 ys         = m*xs + n

 nx         = -ys / nomDist
 ny         =  xs / nomDist
 n_dot_v    = nx * vx + ny * vy

 fn[1]      = x1 + xs -rad2
 fn[2]      = y1 + ys -rad2
 fn[3]      = -(vx - 2 * n_dot_v * nx)
 fn[4]      = -(vy - 2 * n_dot_v * ny)

 FN.RTN 1
FN.END
!--------------------------------------------


!--------------------------------------------
FN.DEF star (innerRadius, outerRadius, anzSpikes,rndRad,rndPhi, dphi)
 anzCor      = anzSpikes *2
 phiStep     = 360/anzCor
 DIM coo [ ( anzCor )*2]

 FOR i = 1 TO anzCor
  phi    = i*phiStep
  radbas = innerRadius
  IF MOD(i,2) THEN
   radbas = outerRadius
   IF rndRad THEN radbas = radbas *  (1 + (RND()-0.5)*2*rndRad)
   IF rndPhi THEN phi    = phi + phiStep* (RND()-0.5)*2*rndPhi
  ENDIF
  coo [i*2-1] = radbas* cos (TORADIANS(phi+dphi))
  coo [i*2]   = radbas* SIN (TORADIANS(phi+dphi))
  !PRINT ctr, i,radbas, tmp   %, coo [i*2-1], coo [i*2]
 NEXT
 LIST.CREATE N, S1
 LIST.ADD.ARRAY s1, coo []

 FN.RTN s1

FN.END
!--------------------------------------------


!--------------------------------------------

!--------------------------------------------


!--------------------------------------------

!--------------------------------------------



!--------------------------------------------
Return
!--------------------------------------------

