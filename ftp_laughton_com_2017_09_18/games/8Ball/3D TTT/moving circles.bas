REM Start of BASIC! Program
DIM CRL[64] % array of circles
DIM xPosBall[64]
DIM yPosBall[64]
DIM delta[64]     % change for each ball



! black background, no status, portrait
GR.OPEN 255,0,0,0,0,1
! alpha 128 solid, yellow, filled style
GR.COLOR 208,255,250,0,1

! calculate initial positions of circles
FOR I=1 TO 64
 xPosBall[I]=RND()*800
 yPosBall[I]=RND()*800

 ! and calculate change amount in same loop
 delta[I]=RND()*10-5

NEXT i

! draw 64 circles. number 1 bigger
GR.CIRCLE CRL[1], xPosBall[1],yPosBall[1], 40

FOR I= 2 TO 64
 GR.CIRCLE crl[I], xPosBall[I],yPosBall[I],10
NEXT I
! show initial position
GR.RENDER

! movement to right. step affects speed
! moving completely across screen

FOR j=1 TO 1400. STEP 1.0

 ! do all 64 circles
 FOR I=1 TO 64

  ! recalculate each ball's position
  xPosBall[I]=xPosBall[I]+delta[I]
!  yPosBall[I]=yPosBall[I]+delta[I]


  ! modify the x parameter of each circle
  GR.MODIFY CRL[I], "x", xPosBall[I]
!  GR.MODIFY CRL[I], "y", yPosBall[I]+ delta[I]

  ! no gr.render yet          % render here is too slow
 NEXT i

 ! render after all 64 modified
 GR.RENDER

NEXT j


pause 10000
GR.CLS
GR.CLOSE
END
