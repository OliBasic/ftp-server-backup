REM Open    graphics
GR.OPEN     255, 0, 0, 0, 0, 1
refw      = 800
refh      = 1280
GR.SCREEN   curw, curh
scaw      = curw/refw
scah      = curh/refh
GR.SCALE    scaw, scah


REM Initial start up variables:
size = 4
mode = 5
camX = 100
camY = 100
camZ = 200 % Position of the camera.
moveX = curw/2
moveY = curh/2
scale = 2 % Image fixed to 'paper' so you can move around and 'scale' in/out.
ANGLEX = 1
ANGLEY = 0
ANGLEZ = 0 % Angle of rotation about the following pivots:
PIVX= 5
PIVY= 0
PIVZ= 0 % Pivots x, y and z.

DIM             xx[8], yy[8]


REM Object coordinates (these 8 points form a cube):
I        = curw/2 % общий размер

sil:

!!
ARRAY.LOAD      x[],  I, I,  I,  I, -I, -I , -I, -I
ARRAY.LOAD      y[],  I, I, -I, -I,  I,  I , -I, -I
ARRAY.LOAD      z[], -I, I, -I,  I, -I,  I , -I,  I
!!


ARRAY.LOAD      x[],  I, I,  I,  I, -I, -I , -I, -I
ARRAY.LOAD      y[],  I, I, -I, -I,  I,  I , -I, -I
ARRAY.LOAD      z[], -I, I, -I,  I, -I,  I, -I, I 






rad           = i/5 %размер шаров
GR.TEXT.SIZE    26
GR.TEXT.ALIGN   2
GR.COLOR        255, 125, 125, 255, 1
FOR i         = 1 TO 8
 GR.CIRCLE      dummy, -5, -5, rad*0.9
 GR.TEXT.DRAW   dummy, -5, -5, INT$(i)
NEXT

!!
GR.COLOR        255, 255, 50, 50, 1
GR.CIRCLE       cen, movex+camx, movey+camy, rad/2
!!

LIST.CREATE     n,poly0
LIST.CREATE     n,poly1
LIST.CREATE     n,poly2
LIST.ADD        poly0, 0, 0, 0, 0, 0, 0
LIST.ADD        poly1, 0, 0, 0, 0, 0, 0
LIST.ADD        poly2, 0, 0, 0, 0, 0, 0

GR.COLOR        255, 55, 50, 250, 0
GR.SET.STROKE   1
GR.POLY         nn, poly0, 0, 0

GR.COLOR        255, 255, 155, 155, 0
GR.SET.STROKE   3
GR.POLY         nn, poly1, 0, 0
GR.POLY         nn, poly2, 0, 0

GR.COLOR        255, 155, 150, 150, 1
GR.TEXT.ALIGN   1
GR.TEXT.DRAW    info, 10, 30,""


GOSUB ABC

angularDelta   = 0.1
kx             = 1.2 * angularDelta
ky             = 0.4 * angularDelta
kz             = 1.0 * angularDelta




DO

 camX+ = 1
 camY+ = 1


 LET ANGLEX   +=  kx
 LET ANGLEY   +=  ky
 LET ANGLEZ   +=  kz

 FOR N=1 TO 8
  REM Rotation code
  LET XD = x[N]-PIVX
  LET YD = y[N]-PIVY
  LET ZD = z[N]-PIVZ
  LET ZX = XD*COS(ANGLEZ)      - YD*SIN(ANGLEZ)      - XD
  LET ZY = XD*SIN(ANGLEZ)      + YD*COS(ANGLEZ)      - YD
  LET YX = (XD+ZX)*COS(ANGLEY) - ZD*SIN(ANGLEY)      - (XD+ZX)
  LET YZ = (XD+ZX)*SIN(ANGLEY) + ZD*COS(ANGLEY)      - ZD
  LET XY = (YD+ZY)*COS(ANGLEX) - (ZD+YZ)*SIN(ANGLEX) - (YD+ZY)
  LET XZ = (YD+ZY)*SIN(ANGLEX) + (ZD+YZ)*COS(ANGLEX) - (ZD+YZ)
  LET xx[n] = ( x[N] + YX+ZX + camX )  +movex
  LET yy[n] = ( y[N] + ZY+XY + camY )  +movey

  GOSUB vol

 NEXT N

 LIST.CLEAR poly0
 LIST.CLEAR poly1
 LIST.CLEAR poly2

 LIST.ADD poly0, ~
 xx[1],yy[1], xx[8],yy[8], xx[4],yy[4], xx[5],yy[5], xx[6],yy[6],~
 xx[3],yy[3], xx[7],yy[7], xx[2],yy[2] 

 LIST.ADD poly1, ~
 xx[1],yy[1], xx[2],yy[2], xx[6],yy[6], xx[5],yy[5], xx[1],yy[1],~
 xx[3],yy[3], xx[4],yy[4], xx[2],yy[2] 

 LIST.ADD poly2, ~
 xx[7],yy[7], xx[3],yy[3], xx[4],yy[4], xx[8],yy[8], xx[7],yy[7],~
 xx[5],yy[5], xx[6],yy[6], xx[8],yy[8] 


 GR.RENDER

 IF !MOD(ctr++, 20) THEN GOSUB speedometer
 !gr.cls

 !goto sil

UNTIL_END

END





!--------------
speedometer:
fps         = ROUND(20000/(CLOCK()-tt),1)
tt          = CLOCK()
GR.MODIFY     info,"text", "fps: "+STR$(fps)+" [1/s]"
RETURN
!--------------




ABC:

flagCircles   = 1

GR.GROUP circles,  1, 3, 5, 7, 9, 11, 13, 15

IF flagCircles   THEN  
 GR.SHOW  circles 
ELSE 
 GR.HIDE circles
ENDIF

flagNumbers   = 1

GR.GROUP numbers,  2, 4, 6, 8, 10,12, 14, 16

IF flagNumbers   THEN  
 GR.SHOW  numbers 
ELSE 
 GR.HIDE numbers
ENDIF

RETURN

!--------------




vol:

IF flagCircles THEN 
 GR.MODIFY   N*2-1, "x", xx[n], "y", yy[n]
ENDIF

IF flagNumbers THEN 
 GR.MODIFY   N*2  , "x", xx[n], "y", yy[n]-rad
ENDIF

RETURN

!--------------
