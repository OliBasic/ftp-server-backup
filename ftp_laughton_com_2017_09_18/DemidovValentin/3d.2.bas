

 rem Chain
    rem by David A. Williams with modifications by Richard T. Russell
    rem Modified for RFO Basic by Roy Shepherd
    
    di_height = 1152 % set to my Device
    di_width = 672

    gr.open 255, 0, 0, 0
    gr.orientation 1 % Portrait 

    pause 1000

    WakeLock 3 

    gr.text.size 40
    
    gr.screen screenWidth, screenHeight
    scale_x = screenWidth / di_width
    scale_y = screenHeight / di_height
    gr.scale scale_x, scale_y
    
      t5=2
      t=0 

      inc = 0.1
      !inc = 0.09

      balls = 10

      r = 50 : g = 150 : b = 250

      do

        r ++ : g ++ : b ++

       gr.cls

        gr.color 150, r, g, b, 1



         if t5< balls then  t5+=0.1

         if t5> balls then  t5=2


        for I = 1 to balls

         for ia = 0 to 50 step 10

           X = screenWidth +ia+ 180 * sin(toradians(t/3+I )) * cos(toradians(t+6*I ))

           Y = screenHeight +ia+ 180 * cos(toradians(t/4+2*I )) * sin(toradians(t+6*I ))

        
           t+=inc

          gr.circle null, X , Y , t5


         next ia

        next i

        gr.render

      until 0
     
     onBackKey:
     
     end

! Меняя значения inc и Y можно эксперементировать.
