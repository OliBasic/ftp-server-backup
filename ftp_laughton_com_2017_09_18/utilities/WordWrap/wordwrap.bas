!! -----------------------------------
    Wordwrap Function 
	January 2011
	Rev. Joanthan C. Watt
	
	This function divides a string 
	into a list of strings bounded by
	the specified pixel witdth.
	
	Parameters:
	  t$ - Original String to display
	  w - display width in pixels
	Return
	  ww - list of strings bound by 
		the pixel length specified (w)
!! -----------------------------------

  Fn.def  wordwrap ( t$, w )
	debug.on
	debug.print "wordwrap"
	debug.print "t$",t$
	debug.print "w",w
	
	List.create  S, ww			% create the list to return.
 
	s = is_in( " ", t$ ) 		% find the first space
	if s=0 then fn.rtn = 0		% can't be processed

	l$ = left$( t$, s )			% get string up to this point
	gr.text.width l, l$			% get the lenght in pixels
		
	while s < len(t$)			% process the string
		s1 = is_in( " ", t$, s+1 )
		if s1 = 0 then s1 = len(t$) 
		
		lln = len(l$)			% save the current length
		l$ = l$ + mid$( t$, s+1, s1-s )
		gr.text.width l, l$		
		debug.print "s,s1,l,l$", s,s1,l,l$ 
		if l > w				% is it too long?
			list.add ww, left$(l$, lln )
			l$ = mid$( t$, s+1, s1-s )	
		endif
		s = s1					% look at the next section
	repeat
	
	if len (l$)>0 then			% don't forget what's left 
         list.add ww, l$

	debug.dump.list ww
	
	fn.rtn ww					% return the list
  fn.end
  
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
