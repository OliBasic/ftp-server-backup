This program enhances the programming of basic! by including features such as shorthand and line formatting.  I hope that the features here find their way into basic!.

To use this program:
1. The filename must be provided in the variable 'bas_program_filename$'
2. Step 1 must be done before including the pipeline.
3. 'INCLUDE enhance/pipeline.bas'
4. If you want the outputted program isolated then before step 3 define the variable 'bas_program_save$' with the basic! path and name.

Complete list of features:
1.Shorthand
 A.++
  I.  ++variable
   a. variable = variable + 1
  II. variable++
   a. variable = variable + 1
 B.--
  I.  --variable
   a. variable = variable - 1
  II. variabl--
   a. variable = variable - 1
 C.+=
  I. variable+=10
   a. variable = variable + 10
 D.-=
  I. variable-=10
   a. variable = variable - 10
 E.*=
  I. variable*=10
   a. variable = variable * 10
 F./=
  I. variable/=10
   a. variable = variable / 10

2. Line formatting
 A. ' _'
  I. (2.A) or the ' _' signal's a custom line format that includes the next line as a part of the signing line. Note: you must have a space separating the underscore, as 'varible_' is a valid name and before would add the next line, now it won't.

3. User Defined Function(UDF) fix
 A. There's a bug in basic! that does not allow nested UDF calls, this will format the calls to UDFs into working code.

4. Auto UDF Call
 A. If you forgot to place 'Call' before your UDFs, this will do it for you.

5. Globalization
 A. global variable
  I. Creates a numeric or string variable viewable and alterable everywhere including UDFs.



Example program:
[code]

REM Start of BASIC! Program

bas_program_filename$ = "test.bas" %required: the programs file path and name.

bas_program_save$="tmp.bas" %optional: default is tmp.bas, i would not put in the same name as the main program as it will overwrite the original code.  default is "tmp.bas"

bas_program_stop=0  %optional: stops the final draft from running.

bas_program_view_final =0 %optional: dumps the final output.


INCLUDE enhance/pipeline.bas %this line is skipped or excluded from being written as it would cause an infinite loop of processing the main file.

INCLUDE testinclude.bas %included files are processed as well into one file.


global variableN =66
global variable$ ="what ever%"

FN.DEF test1()
 variable$ += "$"
FN.END

FN.DEF test2(a)
 ++variablen
 variablen++
 FN.RTN a
FN.END

FN.DEF test3(a _
            ,b _
            ,c)
 FN.RTN a+b+c
FN.END

FOR i=1TOtest3(1,2,3)
 test1()
 x=test2(3)
 variable$=variable$ _
          +"#" _
          +STR$(x)
NEXT

IF 1 THEN test3(10,test2(6),32)

PRINT test2(test3(10 ,test2(6),16))
PRINT variableN
PRINT variable$
te()
PRINT upper$(v$)

[/code]

