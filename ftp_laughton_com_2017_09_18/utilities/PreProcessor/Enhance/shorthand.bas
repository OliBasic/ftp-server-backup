REM ---------shorthand--------
fn.def shorthand$(line$)

 sh$=MID$(line$,LEN(line$)-1,2)
 IF sh$="++" | LEFT$(line$,2)="++"THEN
  IF sh$ ="++"
   line$=LEFT$(line$,LEN(line$)-2)
   line$=line$+"="+line$+"+1"
  ELSE
   line$=RIGHT$(line$,LEN(line$)-2)
   line$=line$+"="+line$+"+1"
  ENDIF
 ENDIF
 IF sh$="--" | LEFT$(line$,2)="--"THEN
  IF sh$ ="--"
   line$=LEFT$(line$,LEN(line$)-2)
   line$=line$+"="+line$+"-1"
  ELSE
   line$=RIGHT$(line$,LEN(line$)-2)
   line$=line$+"="+line$+"-1"
  ENDIF
 ENDIF
 s = searchstring(line$,"+=")
 IF s
  va$= LEFT$(line$,s-1)
  line$=va$+"="+va$+"+"+RIGHT$(line$,LEN(line$)-s-1)
 ENDIF
 s = searchstring(line$,"-=")
 IF s
  va$= LEFT$(line$,s-1)
  line$=va$+"="+va$+"-"+RIGHT$(line$,LEN(line$)-s-1)
 ENDIF
 s = searchstring(line$,"*=")
 IF s
  va$= LEFT$(line$,s-1)
  line$=va$+"="+va$+"*"+RIGHT$(line$,LEN(line$)-s-1)
 ENDIF
 s = searchstring(line$,"/=")
 IF s
  va$= LEFT$(line$,s-1)
  line$=va$+"="+va$+"/"+RIGHT$(line$,LEN(line$)-s-1)
 ENDIF
 !BUNDLE.PUT flb,"line$",line$
 !PRINT line$
 fn.rtn line$
fn.end
!NEXT


!PRINT"Done!"

