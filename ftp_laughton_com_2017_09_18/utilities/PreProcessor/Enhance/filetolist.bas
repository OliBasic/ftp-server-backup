REM Start of BASIC! Program
! Load file to list.

LIST.CREATE n,flbl %File Line Bundle List%
LIST.CREATE n,fnbl %Function Bundle List%
LIST.CREATE n,stbl %String Bundle List%

FN.DEF debugflbl(flbl)
 LIST.SIZE flbl,flbs
 PRINT flbs
 FOR flbi = 1 TO flbs
  LIST.GET flbl,flbi,flb
  BUNDLE.GET flb,"line$",line$
  PRINT flbi;"  :  "; line$
 NEXT
FN.END

FN.DEF SaveToFile(flbl,filename$)
 PRINT "Writing to: ";filename$
 TEXT.OPEN w,stf,"../source/"+filename$
 LIST.SIZE flbl,flbs
 FOR flbi = 1 TO flbs
  LIST.GET flbl,flbi,flb
  BUNDLE.GET flb,"line$",l$
  TEXT.WRITELN stf,l$
 NEXT
 TEXT.CLOSE stf
FN.END

FN.DEF quoteskip$(l$)
 ls = LEN(l$)
 FOR i = 1 TO ls
  d$ = charat$(l$,i)
  IF d$ = CHR$(34) THEN rp =1-rp
  IF rp = 1
   IF d$ =" " THEN d$ = "®"
   IF d$ ="%" THEN d$ = "¢"
   t$ = t$+d$
  ENDIF
  IF rp = 0
   IF d$ = " " THEN d$ = ""
   t$ = t$ + LOWER$(d$)
  ENDIF
 NEXT
 FN.RTN t$
FN.END

FN.DEF RemoveLineComment$(l$)
 lp=srchinstleft(l$,"%",1)
 IF lp THEN l$ = LEFT$(l$,lp-1)
 IF LEFT$(l$,3)="rem"|LEFT$(l$,1)="!"THEN l$ = ""
 FN.RTN l$
FN.END

FN.DEF addline(flbl,l$,from$)
 IF l$<>"eof"&l$<>""
  l$=shorthand$(l$)
  REM --------add line---------
  BUNDLE.CREATE lb
  BUNDLE.PUT lb,"line$",l$
  !PRINT from$;" : ";l$
  PRINT "Final: ";l$
  LIST.ADD flbl,lb
 ENDIF
FN.END

TEXT.OPEN r,f,"../source/"+bas_program_filename$
STACK.create n,files
STACK.push files,f
STACK.isempty files,empty
WHILE !empty
 STACK.pop files,f
 IF f <> -1
  PRINT "Applying pre-run changes...."
  block =0 %comment block trigger%
  lblock=0 %comment block ending trigger%
  DO
   REM ---------line formating---------
   tl$=""
   con=1
   DO
    DO
     TEXT.READLN f,line$
     ln=ln+1
    UNTIL line$ <>""
    l$ = line$
    
    IF MID$(l$,LEN(l$)-1,2)=" _"
     tl$=tl$+LEFT$(l$,LEN(l$)-1)
     con =0
     pnl = 1
    ENDIF
    IF pnl =1 & MID$(l$,LEN(l$)-1,2)<>" _"
     tl$ = tl$ + l$
     pnl = 0
     con = 1
     l$ = tl$
    ENDIF
    l$=quoteskip$(l$)
    l$=REPLACE$(l$," ","")
    l$=REPLACE$(l$,"®"," ")
    l$= RemoveLineComment$(l$)
    l$= REPLACE$(l$,"¢","%")
   UNTIL con =1
   PRINT "Processing line: ";ln;" ";l$
   IF l$<>"eof"
    REM ---------ignore comment block-------
    IF LEFT$(l$,2)="!!" THEN block=1
    IF MID$(l$,LEN(l$)-1,2)="!!" | l$ ="!!"
     block=0
     lblock=1
    ENDIF
    IF block =0 & lblock =0
     sk = 0
     sk=globalize(pog,gif,ghf,flbl,l$,globallist)
     IF LEFT$(l$,6) ="global" THEN pog =1
     REM ------capture and dissect udf's-------
     IF LEFT$(l$,6)="fn.def"
      gif = 1
      ghf = 0
      fs=0
      rp=searchstring(l$,"(")
      ft=searchstring(l$,"$")
      BUNDLE.CREATE fnb
      LIST.ADD fnbl,fnb
      IF ft>0 & ft<rp
       fnt = ft
       fs=1
      ELSE
       fnt = rp
      ENDIF
      fn$ = MID$(l$,7,rp-7)
      par$= MID$(l$,rp+1,LEN(l$)-rp-1)
      BUNDLE.PUT fnb,"fn$",fn$
      BUNDLE.PUT fnb,"fntype",fs

      IF par$="" THEN
       pc = 0
      ELSE
       pc = stringoccurs(l$,",")+1
      ENDIF
      BUNDLE.PUT fnb,"pcount",pc
      IF pc
       BUNDLE.PUT fnb,"param$",par$+",globals"
       l$ = LEFT$(l$,LEN(l$)-1)+",globals)"
      ELSE
       BUNDLE.PUT fnb,"param$","globals"
       l$ = LEFT$(l$,LEN(l$)-1)+"globals)"
      ENDIF
      !PRINT "Dissect UDF: ";fn$;"  ";par$;"  ";pc
      CALL addline(flbl,l$,"ftl")
      CALL get_globals(globallist,flbl)
      sk =1
     ENDIF
     IF udfix(flbl,ln,l$,globallist,fnbl,ufl)
      sk =1
     ENDIF
     IF LEFT$(l$,6)="fn.rtn"|l$="next"
      CALL put_globals(globallist,flbl)
     ENDIF
     IF LEFT$(l$,6)="fn.end"
      gif =0
      CALL put_globals(globallist,flbl)
     ENDIF
     IF LEFT$(l$,7) = "include" THEN
      sk=1
      IF l$ <> "includeenhance/pipeline.bas"
       STACK.push files,f
       TEXT.OPEN r,f,"../source/"+MID$(l$,8,LEN(l$)-7)
      ENDIF
     ENDIF
     IF !sk THEN CALLaddline(flbl,l$,"ftl")

    ENDIF
   ENDIF

  UNTIL line$ ="EOF"
  TEXT.CLOSE f
  STACK.isempty files,empty
 REPEAT
 PRINT "Done..."
ELSE
 PRINT "File failed to load."
 END
ENDIF






