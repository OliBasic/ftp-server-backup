slDEBUG=0

REM-------------STRING FUNCTIONS---------------

FN.DEF charAt$(l$,n)
 IF l$ <> ""
  n=MOD(n,LEN(l$)+1)
  IF n=0 THEN n =1
  FN.RTN MID$(l$,n,1)
 ENDIF
FN.END

FN.DEF searchRANGE(l$,f$,start,stop)
 rtn =0
 IF l$<>""&f$<>""
  ls = LEN(l$)
  lf = LEN(f$)
  FOR i = start TO stop-lf
   IF MID$(l$,i,lf)=f$ THEN rtn = 1
  NEXT
 ENDIF
 FN.RTN rtn
FN.END

FN.DEF stringOccurs(l$,f$)
 sl=LEN(l$)
 sfl=LEN(f$)
 FOR i =1TOsl-sfl
  IF MID$(l$,i,sfl)=f$ THEN c=c+1
 NEXT
 FN.RTN c
FN.END

FN.DEF searchSTRING(s$ , sf$)
 ls = LEN(sf$)
 tp=0
 IF ls
  l = LEN(s$)-ls
  IF l>0
   FOR p = 1 TO l
    IF MID$(s$,p,ls) = sf$ THEN
     tp = p
     F_N.BREAK
    ENDIF
   NEXT
  ENDIF
 ENDIF
 FN.RTN tp
FN.END

FN.DEF srchleft(s$,sf$,start)
 ti =0
 ls=LEN(s$)
 lsf=LEN(sf$)
 IF start < ls-lsf THEN
  FOR i = start TO Ls-Lsf
   IF MID$(s$,i,Lsf)=sf$ THEN ti =i
  NEXT
 ENDIF
 FN.RTNti
FN.END

FN.DEF srchInstLeft(s$,sf$,inst)
 ls=LEN(s$)
 lsf=LEN(sf$)
 rtn=0
 FOR i = 1 TO Ls-Lsf
  IF MID$(s$,i,Lsf)=sf$
   tms= tms+1
   IF tms=inst THEN
    RTN = i
    F_N.BREAK
   ENDIF
  ENDIF
 NEXT
 FN.RTNrtn
FN.END

FN.DEF srchright(s$,start,sf$)
 ls=LEN(s$)
 lsf=LEN(sf$)
 FOR i = start TO Ls-Lsf
  IF MID$(s$,Ls-(i-Lsf),Lsf)=sf$ THEN FN.RTN i
 NEXT
 FN.RTN0
FN.END

FN.DEF srchInstRight(s$,sf$,inst)
 ls=LEN(s$)
 lsf=LEN(sf$)
 FOR i = 1 TO Ls-Lsf
  IF MID$(s$,Ls-(i-Lsf),Lsf)=sf$
   tms=tms+1
   IF tms=inst THEN FN.RTN i
  ENDIF
 NEXT
 FN.RTN0
FN.END

FN.DEF truncate$(l$,l,r)
 re$= MID$(l$,l+1,(LEN(l$)-l)-r)
 FN.RTN re$
FN.END

FN.DEF truncate2$(l$,l1,l2)
 re$=MID$(l$,l1+1,l2-(l1+1))
 FN.RTN re$
FN.END

FN.DEF parseL2(l$,a$,b$,c$,d$,e$,f$,list)
 sa =searchstring(l$,a$)
 sb =searchstring(l$,b$)
 sc =searchstring(l$,c$)
 sd =searchstring(l$,d$)
 se =searchstring(l$,e$)
 sf =searchstring(l$,f$)
 PRINT sa,sb,sc,sd,se,sf
 IF a$="@" THEN at=1
 IF b$="@" THEN bt=1
 IF c$="@" THEN ct=1
 IF d$="@" THEN dt=1
 IF e$="@" THEN et=1
 IF f$="@" THEN ft=1
 PRINT at,bt,ct,dt,et,ft
 IF (sa<sb|bt=1)&(LEFT$(l$,LEN(a$))=a$& at=1)&a$<>""
  IF (sb<sc|ct=1)
   IF (sc<sd|dt=1)
    IF (sd<se|et=1)
     IF (se<sf|ft=1)
      IF at
       t$=LEFT$(l$,sb-1)
       LIST.ADD list,t$
       PRINT "at",t$
      ENDIF
      IF bt
       t$=truncate2$(l$,(sa+LEN(a$))-1,sc)
       LIST.ADD list,t$
       PRINT "bt",t$
      ENDIF
      IF ct
       t$=truncate2$(l$,(sb+LEN(b$)),sd)
       LIST.ADD list,t$
       PRINT "ct",t$
      ENDIF
      IF dt
       t$ = truncate2$(l$,(sc+LEN(c$)-1),se)
       PRINT "dt",t$
      ENDIF
      IF et
       t$ = truncate2$(l$,(sd+LEN(d$)-1),sf)
       PRINT "et",t$
      ENDIF
      IF ft
       t$ =RIGHT$(l$,LEN(l$)-(LEN(e$)+se)+1)
       PRINT "ft",t$
      ENDIF
     ELSEIF et
     ENDIF
    ELSEIF dt
    ENDIF
   ELSEIF ct
    t$ = RIGHT$(l$,LEN(l$)-LEN(b$)-1)
    PRINT t$
   ENDIF
  ELSEIF bt
   t$= RIGHT$(l$,LEN(l$)-LEN(a$))
   IF t$ <> "" THEN PRINT t$
  ENDIF
 ELSEIF at
  t$ = l$
  PRINT t$
 ENDIF

FN.END

FN.DEF parseL(l$,a$,b$,c$,d$,e$,f$,list)
 sa =srchleft(l$,1,a$)
 sb =srchleft(l$,sa+1,b$)
 sc =srchleft(l$,sb+1,c$)
 sd =srchleft(l$,sc+1,d$)
 se =srchleft(l$,sd+1,e$)
 sf =srchleft(l$,se+1,f$)
 PRINT sa,sb,sc,sd,se,sf
 IF a$="@" THEN at=1
 IF b$="@" THEN bt=1
 IF c$="@" THEN ct=1
 IF d$="@" THEN dt=1
 IF e$="@" THEN et=1
 IF f$="@" THEN ft=1
 PRINT at,bt,ct,dt,et,ft
 IF a$ ="" THEN na =1
 IF b$ ="" THEN nb =1
 IF c$ ="" THEN nc =1
 IF d$ ="" THEN nd =1
 IF e$ ="" THEN ne =1
 IF f$ ="" THEN nf =1
 PRINT na,nb,nc,nd,ne,nf
 IF sa=0 & at =0 & na =0 THEN FN.RTN0
 IF sb=0 & bt =0 & nb =0 THEN FN.RTN0
 IF sc=0 & ct =0 & nc =0 THEN FN.RTN0
 IF sd=0 & dt =0 & nd =0 THEN FN.RTN0
 IF se=0 & et =0 & ne =0 THEN FN.RTN0
 IF sf=0 & ft =0 & nf =0 THEN FN.RTN0
 IF at THEN PRINT LEFT$(l$,sb-1)
 IF bt THEN PRINT MID$(l$,sa+LEN(a$),sc-1-LEN(a$))
 IF ct THEN PRINT MID$(l$,sb+LEN(b$),sd-1-LEN(b$))
 IF dt THEN PRINT MID$(l$,sc+LEN(c$),se-1-LEN(c$))
 IF et THEN PRINT e$
 IF ft THEN PRINT f$

FN.END

IF SLDEBUG
 LIST.CREATE s,ls
 l$="foruth=1tolikely"
 !l$="waaat"
 CALLparsel(l$,"for","@","=","@","to","@",ls)
 !CALLparsel(l$,"w","aaa","@","","","",ls)
ENDIF







