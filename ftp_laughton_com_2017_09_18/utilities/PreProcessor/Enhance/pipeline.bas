REM Start of BASIC! Program
REM pipeline include

!bas_program_filename$="test.bas"

INCLUDE enhance/string_lib.bas

INCLUDE enhance/shorthand.bas

INCLUDE enhance/UDF_fix.bas

INCLUDE enhance/globalize.bas

INCLUDE enhance/filetolist.bas

IF bas_program_save$<>""
 CALL savetofile(flbl,bas_program_save$)
 IF !bas_program_stop THEN RUN bas_program_save$
ELSE
 CALL savetofile(flbl,"tmp.bas")
 IF !bas_program_stop THEN RUN "tmp.bas"
ENDIF

IF bas_program_view_final THEN CALL debugflbl(flbl)


END














