REM ----------GLOBAL SCRIPT-----------

gs_poses_globals = 0
gs_in_function = 0
null = 0
null$ = CHR$(34)+CHR$(34)
LIST.CREATE s,globallist

REM ------------GLOBAL DUMP/RETURN-----------
FN.DEF get_globals(gl,flbl)
 LIST.SIZE gl, ls
 FOR i =1 TO ls
  LIST.GET gl,i,vn$
  t$ ="bundle.get globals,"+CHR$(34)+vn$+CHR$(34)+","+vn$
  CALL addline(flbl,t$,"getg")
 NEXT
FN.END

FN.DEF put_globals(gl,flbl)
 LIST.SIZE gl, ls
 FOR i =1 TO ls
  LIST.GET gl,i,vn$
  t$="bundle.put globals,"+CHR$(34)+vn$+CHR$(34)+","+vn$
  CALL addline(flbl,t$,"putg")
 NEXT
FN.END

FN.DEF globalize(gs_poses_globals,gs_in_function,gs_has_functions,flbl,line$,globallist)
 REM ---------------GLOBAL------------
 IF LEFT$(line$,6)="global"
  IF gs_in_function=0
   IF gs_has_functions
    PRINT "Error : Globals must be declared before the first function."
    END
   ENDIF
   IF gs_poses_globals = 0
    CALL addline(flbl,"bundle.create globals","gm1")
    !CALL debugflbl(flbl)
    gs_poses_globals =1
   ENDIF
   sfe = searchstring(line$,"=")
   IF sfe
    varName$=MID$(line$,7,sfe-7)
    value$=RIGHT$(line$,LEN(line$)-sfe)
    t$="bundle.put globals,"+CHR$(34)+varName$+CHR$(34)+","+value$
    CALL addline(flbl,t$,"gm2")
   ELSE
    varName$ = RIGHT$(line$,LEN(line$)-6)
    IF right$ (line$,1)="$"
     CALL addline(flbl,"bundle.put globals,"+ CHR$(34)+ varName$ +CHR$(34) +"," + null$,"gm3")
    ELSE
     CALL addline(flbl,"bundle.put globals,"+ CHR$(34)+ varName$ +CHR$(34) +",0","gm4")
    ENDIF
   ENDIF
   CALL addline(flbl,"bundle.get globals,"+ CHR$(34)+ varName$ +CHR$(34) +"," + varName$,"gm5")
   LIST.ADD globalList,varName$
   alt=1
  ELSE
   PRINT "Error : Globals cannot be declared inside of UDFs."
   END
  ENDIF
  alt=1
 ENDIF
 FN.RTN alt
FN.END
