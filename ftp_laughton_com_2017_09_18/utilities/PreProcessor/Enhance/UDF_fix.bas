!! Function fixer -
For nesting function calls.
!!

LIST.CREATE n,ufl %scope bundle list%

FN.DEF fnsearch(s$,fnbl,ufl)
 rtn =0
 !PRINT s$
 LIST.SIZE fnbl,fs
 FOR fi =1TOfs
  LIST.GET fnbl,fi,fnb
  BUNDLE.GET fnb,"fn$",fn$
  IF s$ = fn$
   rtn = 1
   LIST.SIZE ufl,ufs
   sk=0
   FOR ufi = 1 TO ufs
    LIST.GET ufl,ufi,tfnb
    IF fnb = tfnb THEN sk =1
   NEXT
   IF !sk THEN LIST.ADD ufl,fnb
  ENDIF
 NEXT
 FN.RTN rtn
FN.END

FN.DEF LineForm(l$,ufl,gl,flbl)
 tti$=""
 IF LEFT$(l$,2)="if"
  idt=searchstring(l$,"then")
  tti$=LEFT$(l$,idt+3)
  l$=MID$(l$,(idt+4),LEN(l$)-idt+3)
 ENDIF
 IF LEFT$(l$,3)="for"
  idt=searchstring(l$,"to")
  tti$=LEFT$(l$,idt+1)
  l$=MID$(l$,(idt+2),LEN(l$)-idt+1)
  skc =1
 ENDIF
 IF searchstring(l$,"=")=0&(LEFT$(l$,4)<>"call"&LEFT$(l$,5)<>"print")&!skc
  actl=1
 ENDIF
 REM -put fix for calling functions from if/then-
 CALL put_globals(gl,flbl)
 LIST.SIZE ufl,ufs
 lineindex=0
 scope=0
 FOR ufi = 1 TO ufs
  LIST.GET ufl,ufi,fnb
  BUNDLE.GET fnb,"fn$",fn$
  BUNDLE.GET fnb,"pcount",pc
  !PRINT "UDF Found: ";fn$
  fni =1
  sil = srchinstleft(l$,fn$+"(",fni)
  WHILE sil>0
   lineindex=sil+LEN(fn$)
   ts = scope
   FOR i = lineindex TO LEN(l$)
    d$ = charat$(l$,i)
    IF d$ ="(" THEN scope = scope +1
    IF d$ =")" THEN scope = scope -1
    !PRINT scope;"  ";l$;" ";ts ;"  ";d$;"  ";i
    IF !scope & d$ = ")" THEN
     IF pc
      l$=LEFT$(l$,i-1)+",globals~"+RIGHT$(l$,LEN(l$)-i+1)
     ELSE
      l$=LEFT$(l$,i-1)+"globals~"+RIGHT$(l$,LEN(l$)-i+1)
     ENDIF
     F_N.BREAK
    ENDIF
   NEXT
   fni=fni+1
   sil = srchinstleft(l$,fn$+"(",fni)
  REPEAT
 NEXT
 !PRINT l$
 IF actl THEN l$ = "call"+l$
 l$=tti$+REPLACE$(l$,"~",")")
 !PRINT "Fixed: ";l$
 CALL addline(flbl,l$,"fnf1")
 CALL get_globals(gl,flbl)
 FN.RTN 1
FN.END

FN.DEF udfix(flbl,li,l$,gl,fnbl,ufl)
 IF l$ <>""
  !PRINT "Processing: ";li ;" : ";l$
  lconfn =0
  tscope = 0
  continue = 0
  lineindex = 0
  tli=0
  fb=0
  IF LEFT$(l$,5)="print" THEN lineindex =5
  IF LEFT$(l$,4)="call" THEN lineindex =4
  IF LEFT$(l$,2)="if" THEN
   lineindex=searchstring(l$,"then")
   IF lineindex THEN lineindex = lineindex +3
  ELSEIF LEFT$(l$,3)="for"
   lineindex =searchstring(l$,"to")
   IF lineindex THEN lineindex = lineindex +1
  ENDIF
  IF LEFT$(l$,6)<>"fn.def"
   lineindex = lineindex +1
   tli = lineindex
   DO
    tlit = 0
    d$ = charat$(l$,tli)
    IF d$= "="|d$="+"|d$="-"|d$="*"|d$="/"|d$="("|d$=">"|d$="<"|d$=")"|d$=","
     fb = fnsearch(MID$(l$,lineindex,tli-lineindex),fnbl,ufl)
     tlit =1
    ENDIF
    tli = tli+1
    IF tlit THEN lineindex = tli
    IF tli > LEN(l$) THEN continue =1
   UNTIL continue
  ENDIF
  LIST.SIZE ufl,ufs
  IF ufs THEN
   CALL LineForm(l$,ufl,gl,flbl)
   sk =1
  ENDIF
  LIST.CLEAR ufl
 ENDIF
 FN.RTNsk
FN.END










