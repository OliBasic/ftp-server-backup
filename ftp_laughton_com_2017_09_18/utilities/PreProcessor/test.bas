REM Start of BASIC! Program

bas_program_filename$ = "test.bas" %required: the programs file path and name.

bas_program_save$="tmp.bas" %optional: default is tmp.bas, i would not put in the same name as the main program as it will overwrite the original code.  default is "tmp.bas"

bas_program_stop=0  %optional: stops the final draft from running.

bas_program_view_final =0 %optional: dumps the final output.


INCLUDE enhance/pipeline.bas %this line is skipped or excluded from being written as it would cause an infinite loop of processing the main file.

INCLUDE testinclude.bas %included files are processed as well into one file.


global variableN =66
global variable$ ="what ever%"

FN.DEF test1()
 variable$ += "$"
FN.END

FN.DEF test2(a)
 ++variablen
 variablen++
 FN.RTN a
FN.END

FN.DEF test3(a _
            ,b _
            ,c)
 FN.RTN a+b+c
FN.END

FOR i=1TOtest3(1,2,3)
 test1()
 x=test2(3)
 variable$=variable$ _
          +"#" _
          +STR$(x)
NEXT

IF 1 THEN test3(10,test2(6),32)

PRINT test2(test3(10 ,test2(6),16))
PRINT variableN
PRINT variable$
te()
PRINT upper$(v$)


