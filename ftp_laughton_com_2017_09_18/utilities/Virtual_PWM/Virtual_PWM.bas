Rem       PWM made simple (by Alberto)
REM              March 2016
REM **********************************
!
!---------- Setting the graphic screen -------

Let Fix_W = 1280
Let Fix_H = 720

GR.OPEN 255, 15, 15, 15,0
GR.ORIENTATION 0

Pause 200

gr.screen DW, DH

  Let SW = DW/Fix_W
  Let SH = DH/Fix_H

gr.scale SW, SH

!----- Preparing graphics objects ------------

Dim Rt[64,4]
gr.set.stroke 3
gr.color 255,0,128,0
For I = 1 to 64
Gr.line Rt[I,1],0,0,100,0
Gr.line Rt[I,2],0,0,100,0
Gr.line Rt[I,3],0,0,100,0
Gr.line Rt[I,4],0,0,100,0
gr.hide Rt[I,1]
gr.hide Rt[I,2]
gr.hide Rt[I,3]
gr.hide Rt[I,4]
next
Gr.line Ln1,0,0,10,0
gr.hide Ln1

! ---- Drawing Duty and Frequency slides ----

gr.color 255,128,128,128
gr.rect DS,720,600,1160,650
gr.rect FS,120,600,560,650
gr.color 255,0,0,0
gr.line Es1,740,625,1140,625
gr.line Es2,140,625,540,625
gr.color 255,60,60,60,1
gr.circle DS1, 935,625,20
gr.circle FS1, 340,625,20

!------- Drawing the led circuit -------------

gr.color 255,64,0,0,0
gr.circle Led0, 980,90,25
gr.color 128,255,0,0,1
gr.circle Led, 980,90,25

gr.color 255,128,128,128,0
Gr.line Ld1, 830,90,950,90
Gr.Rect Ld2,730,75,830,105
Gr.line Ld3, 580,90,730,90
Gr.line Ld4, 1010,90,1160,90
Gr.line Ld5, 1160,90,1160,150
Gr.line Ld6, 580,150,1160,150
gr.circle Ld7,576,150,4
gr.circle Ld8,576,90,4
gr.color 128,255,255,255,1
gr.rect Led9,990,80,997,87

!-------- Writing screen text ------------------

gr.color 255,128,128,128,0
gr.rect sc1,120,210,1160,560
gr.line Or,225,270,225,490
gr.line As,225,490,1080,490
gr.line zz,215,460,225,460
gr.line ss,215,270,225,270

gr.color 255,128,128,128,1
gr.text.size 27
gr.text.draw tx2,500,130,""
gr.text.draw tx9, 725,182,""
gr.text.draw tx12, 742,65,"R=760"
gr.text.draw tx13, 1020,80,"Led"
gr.text.draw tx3,550,530,"Time Domain "
gr.text.draw tx14,185,470,"0"
gr.text.draw tx15,165,280,"12"
gr.text.draw tx5,875,690,"Duty Cycle"
gr.text.draw tx6, 1110,690,"max"
gr.text.draw tx7, 720,690,"min"
gr.text.draw tx8, 235,690,"PWM Frequency"
gr.text.draw tx10, 510,690,"max"
gr.text.draw tx11, 120,690,"min"

GR.ROTATE.START -90,210,165
gr.text.draw tx4,-25,150,"Volts"
gr.rotate.end

gr.text.size 35
gr.text.draw tx1,125,100,""
gr.text.draw tx13,125,160,""

!---------Setting some variables ---------------

Let Lo = 260
Let Hi = 460
Let Duty = 50
Let os = 280
BNb = 100
gosub Frequenza
gr.render
goto loop
!----------------- Ini program ----------------

Frequenza:

Let Frq = int(820/BNb)
If Frq > 64 then Let Freq = 64

For I = 1 to 64
gr.hide Rt[I,1]
gr.hide Rt[I,2]
gr.hide Rt[I,3]
gr.hide Rt[I,4]
next

Let Flag_F = 0

Disegna:

Let Duty1 = Duty * BNb/100 

For I = 1 to Frq

if (duty < 100) & (duty > 0) then
gr.hide Ln1
gr.show Rt[I,1]
gr.show Rt[I,2]
gr.show Rt[I,3]
gr.show Rt[I,4]

gr.modify Rt[I,1], "x1" ,((I-1)*BNb) + os, "y1", Lo, "x2", (((I-1)*BNb)+Duty1) + os, "y2", Lo
gr.modify Rt[I,2], "x1" ,((I-1)*BNb) + Duty1 + os , "y1", Hi, "x2", ((I-1)*BNb) + Duty1 + os + (BNb-Duty1), "y2", Hi
gr.modify Rt[I,3], "x1" ,((I-1)*BNb) + os, "y1", Lo, "x2",((I-1)*BNb) + os, "y2", Hi
gr.modify Rt[I,4], "x1" ,((I-1)*BNb) + Duty1 + os, "y1", Lo, "x2",((I-1)*BNb) + Duty1 + os, "y2", Hi

else

gr.hide Rt[I,1]
gr.hide Rt[I,2]
gr.hide Rt[I,3]
gr.hide Rt[I,4]
endif

next

if Duty = 0 Then
gr.modify Ln1, "x1" , os , "y1", Hi, "x2", 1050 , "y2", Hi
gr.show Ln1
endif

if Duty >= 100 Then
gr.modify Ln1, "x1" , os, "y1", Lo, "x2", 1050 , "y2", Lo
gr.show Ln1
endif

gr.modify tx1,"text", "Duty Cycle = " + trim$(format$("##%",Duty)) + "%"
gr.modify tx2,"text", "V in = " + trim$(format$("##%.##",Duty*12/100)) 
gr.modify tx9,"text", "I = " + trim$(format$("#%.###",(Duty*12/100)/760)) 
gr.modify tx13,"text", "Frequency = " + trim$(format$("##%",Frq))+ " KHz"

Let flag_D = 0

return
!------------------ Main loop ----------------

loop:
pause 1
goto loop
end

!------------------- ISR ----------------------

OnGrTouch:

do
pause 1
gr.touch touched,x,y
until touched

Let x = x / SW: Let y = y / SH

do
pause 1
gr.touch touched,xx,yy


Let xx =xx / SW: Let yy = yy / SH

if (yy > 550) & (yy < 700) then

! ------ Duty cycle slide service ---------------

if (xx >700) & (xx < 1180) then
if xx > 1140 then Let xx = 1140
if xx < 740 then Let xx = 740
Let Duty = 100-int((1140-xx)/4)
gr.modify DS1,"x",xx
Let flag_D = 1
If Duty < 0 Then Let Duty = 0
If Duty > 100 Then Let Duty = 100
gr.modify Led, "alpha", int(Duty * 2.55)
gr.modify Led9, "alpha", int(Duty * 2.55)
 gosub Disegna
endif


!------- Frequency slide service -------------

if (xx >120) & (xx < 560) then 
if xx < 140 then Let xx = 140
if xx > 540 then Let xx = 540
BNb = 400-int(xx/1.3)
if BNb < 25 then Let BNb = 25
if BNb > 400 then Let BNb = 400
gr.modify FS1,"x",xx

  Let flag_F = 1
  Let flag_D = 1
gosub Frequenza
endif
!----------------------------------------------
endif
gr.render
until !touched

gr.OnGrTouch.Resume

End
