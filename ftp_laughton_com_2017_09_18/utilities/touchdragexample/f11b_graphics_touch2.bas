! This program is designed to demonstrate
! the gr.touch and gr.touch2 commands
! with or without a status bar.
!
! Run the program and follow the instructions.
! Try touching/dragging with two fingers.
!
! Tested on a 7" tablet running Android 4.2.2 with BASIC! 1.76
! Comments to Chris Lusby Taylor clusbytaylor@gmail. com

!Debug.on
PRINT "Touch the screen here when ready. >>>>..<<<< to enter graphic mode."
PRINT "Then touch the screen to see x,y coordinates. (0,0) is top left."
print "Note how first and second (double) touches are kept separate."
print "You can drag into the top status bar (y<38) and bottom notification bar but"
print " not draw in them, nor start a drag in them."
print "The stated screen height does not include the notification bar."
print "It does include the status bar if shown so you can't draw into the top"
print "few lines (38 on my 7\" tablet, your device may differ.)"
print "Touches leave coloured trails."
print "The difference between screen height in landscape and width in portrait"
print "is the height of the notification bar. Dragging into the bar gives y values"
print "greater than the stated screen height."
PRINT "When done, Press the BACK key."
array.load sel$[], "Hide the status bar","Show the Status bar"
select seln, sel$[], "Do you want to see the status bar at the top of the screen?"
SB=0
if seln=2 then SB=1
! Wait for the screen to be tapped
Tapped = 0
DO
UNTIL Tapped
GOTO Start

onConsoleTouch:
Tapped = 1
ConsoleTouch.Resume

Start:
! open the Graphics Screen with a
! black background

GR.OPEN 255, 0,0,0,sb,-1. %Not transparent, Black, status bar, sense orientation
pause 1000

gr.text.size 30
gr.text.align 2 %centre align

w=0
! Run an infinite loop testing for a touch
! and looking for key presses

DO
 DO
  gr.screen w2,h2
  if w2<>w then
   gr.CLS % clearscreen
   wh_OBJ=0
   xy_OBJ=0
   xy2_OBJ=0
   if w then scrnht=abs(w2-h) %Calculate bottom bar height.
   w=w2
   h=h2
   swh$="screen width" + format$("###%", w) + ",  height" + format$("###%", h)
   if scrnht then swh$ += ",  plus" + format$("##%", scrnht) + " bar"
   gr.color 255,255,255,0,0
   gr.text.draw wh_OBJ, w/2, h2/2, swh$
   gr.color 255, 255, 0, 0, 0
   d=38*sb %margin needed if status line at top
   gr.oval oval_OBJ, 0, d, w, h
   gr.color 255,255,255,255,0
   gr.render
  endif
  GR.TOUCH flag, x , y
  GR.TOUCH2 flag2, x2, y2 %Detect a simultaneous touch, if any
 UNTIL flag | flag2 %Wait until touch detected, then...
  IF flag2
    s2$= "Touch2   x " + format$("####%", x2) + "   y " + format$("####%", y2)
    if xy2_OBJ then gr.hide xy2_obj
    gr.color 255,0,255,255,0
    gr.text.draw xy2_obj, w/2, 3*h/4, s2$
    if (abs(x20-x2)<7 & abs(y20-y2)<7)
      gr.line line_OBJ, x20, y20, x2, y2
    else
      gr.point pt2_OBJ, x2, y2
    endif
    gr.color 255, 255, 255, 255, 0
    x20=x2
    y20=y2
  endif
  if flag
    s$= "Touch   x " + format$("####%", x) + "   y " + format$("####%", y)
    if xy_OBJ then gr.hide xy_obj
    gr.text.draw xy_obj, w/2, h/4, s$
    if (abs(x0-x)<7 & abs(y0-y)<7)
      gr.line line_OBJ, x0, y0, x, y
    else
      gr.point pt_OBJ, x,y
    endif
    x0=x
    y0=y
  endif
  GR.RENDER


UNTIL 0

! When you exit the graphic screen
! with the back key, you will get
! run time error at gr.touch. This
! This because the graphics mode
! is turned off when the graphics
! screen is  exited. Hit the Back
! key one more time to get back
! to the Editor.

! The Onerror will catch the error
! and not print the graphics not
! opened error message
ONERROR:
END
