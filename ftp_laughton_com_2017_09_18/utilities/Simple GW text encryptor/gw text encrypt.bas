
include "GW.bas"
list.create s, files

p1=gw_new_page()

array.load pwdccl$[], "OK>PWD", "Cancel>CCL"
gw_use_theme_custo_once("inline")
dlg1=gw_add_dialog_input(p1, "PROTECTED", "Type the password:", "*>", pwdccl$[])

array.load newccl$[], "OK>"+gw_show_dialog_input$(dlg1), "Cancel"
gw_use_theme_custo_once("inline")
dlg2=gw_add_dialog_input(p1, "NEW FILE", "Type a file name:", "new.txt", newccl$[])

gw_add_title(p1, "Simple GW text encryptor")
gw_add_button(p1, "CREATE NEW TEXT FILE", gw_show_dialog_input$(dlg2))
gw_add_text(p1, "or open an existing one:")
gosub listTxt
gw_add_listview(p1, txt$[])
gw_add_text(p1, "<i>Hit Back at any time to exit</i>")

gw_render(p1)

start:
gw_modify(dlg1, "input", "*>")
do
 r$=gw_wait_action$()
 if r$="BACK" then exit
 if is_in("OPN:", r$) then txt$=mid$(r$, 5): gw_show_dialog_input(dlg1)
 if r$="PWD" then pwd$=gw_get_value$(dlg1)
 if r$="CCL" then gw_modify(dlg1, "input", "*>")
until len(pwd$)

if txt$="" % new (empty) text
 txt$=gw_get_value$(dlg2)
 buf$=""
else % existing encrypted text
 grabfile enc$, txt$
 decrypt pwd$, enc$, buf$
 if is_in("decryption error", lower$(geterror$())) then pwd$="": txt$=""
end if
error:
if pwd$="" then popup "bad password, or\nnot an encrypted file": goto start


p2=gw_new_page()

gw_use_theme_custo_once("notext icon=edit")
bar$+=gw_add_bar_lbutton$(">SAVE")
bar$+=gw_add_bar_title$("Simple GW text encryptor")
gw_use_theme_custo_once("notext icon=power")
bar$+=gw_add_bar_rbutton$(">BACK")
gw_add_titlebar(p2, bar$)

gw_inject_html(p2, "<style>textarea.ui-input-text {height:inherit !important}</style>") % css for big inputbox
gw_use_theme_custo_once("rows=16")
buf=gw_add_inputbox(p2, "<b>Edited file:</b> \""+txt$+"\"", "")
gw_add_text(p2, "<i>Hit Back to exit</i>")

gw_render(p2)
js("$('#"+gw_id$(buf)+"').val(\""+replace$(replace$(buf$, "\n", "\\n"), "\"", "\\\"")+"\")")

do
 r$=gw_wait_action$()
 if r$="SAVE"
  buf$=gw_get_value$(buf)
  encrypt pwd$, buf$, enc$
  byte.open w, fid, txt$
  byte.write.buffer fid, enc$
  byte.close fid
  popup "file saved"
 end if
until r$="BACK"

exit


listTxt:
dir "", all$[], "/"
list.clear files
array.length nfiles, all$[]
for i=1 to nfiles
 if lower$(right$(all$[i], 4)) = ".txt" then list.add files, all$[i] + ">OPN:" + all$[i]
next
array.delete all$[]
list.size files, nfiles
if nfiles then list.toarray files, txt$[] else array.load txt$[], "[no .txt file found]"
return


onerror:
pwd$="": txt$=""
goto error

