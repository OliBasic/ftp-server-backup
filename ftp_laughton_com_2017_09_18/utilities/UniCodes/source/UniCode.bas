! Unicode characters
! Aat Don @2014
GR.OPEN 255,0,0,0,0,1
PAUSE 1000
GR.SCREEN w,h
ScaleX=600
ScaleY=h/w*ScaleX
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
WAKELOCK 3
CLS
LIST.CREATE S,Categories
LIST.ADD Categories,"Numbers","Letters","Borders","Lines","Shapes","Stars","Telephones","Chess","Cards","Music","Dice","Scissors","Arrows","Various"
LIST.SIZE Categories,NumOfCats
DIM CatLength[NumOfCats]
DIM CatNumber[NumOfCats]
LIST.CREATE N,CatNumber[1]
LIST.ADD CatNumber[1],9312,9312,9313,9313,9314,9314,9315,9315,9316,9316,9317,9317,9318,9318,9319,9319,9320,9320,9321,9321,9322,9322,9323,9323,9324,9324,9325,9325,9326,9326,9327,9327,9328,9328,9329,9329,9330,9330,9331,9331,9332,9333,9334,9335,9336,9337,9338,9339,9340,9341,9342,9343,9344,9345,9346,9347,9348,9349,9350,9351,9352,9353,9354,9355,9356,9357,9358,9359,9360,9361,9362,9363,9364,9365,9366,9367,9368,9369,9370,9371
LIST.SIZE CatNumber[1],CatLength[1]
LIST.CREATE N,CatNumber[2]
LIST.ADD CatNumber[2],9372,9373,9374,9375,9376,9377,9378,9379,9380,9381,9382,9383,9384,9385,9386,9387,9388,9389,9390,9391,9392,9393,9394,9395,9396,9397,9424,9425,9426,9427,9428,9429,9430,9431,9432,9433,9434,9435,9436,9437,9438,9439,9440,9441,9442,9443,9444,9445,9446,9447,9448,9449
LIST.SIZE CatNumber[2],CatLength[2]
LIST.CREATE N,CatNumber[3]
LIST.ADD CatNumber[3],9472,9473,9474,9475,9476,9477,9478,9479,9480,9481,9482,9483,9484,9485,9486,9487,9488,9489,9490,9491,9492,9493,9494,9495,9496,9497,9498,9499,9500,9501,9502,9503,9504,9505,9506,9507,9508,9509,9510,9511,9512,9513,9514,9515,9516,9517,9518,9519,9520,9521,9522,9523,9524,9525,9526,9527,9528,9529,9530,9531,9532,9533,9534,9535,9536,9537,9538,9539,9540,9541,9542,9543,9544,9545,9546,9547,9552,9553,9554,9555,9556,9557,9558,9559,9560,9561,9562,9563,9564,9565,9566,9567,9568,9569,9570,9571,9572,9573,9574,9575,9576,9577,9578,9579,9580,9581,9582,9583,9584,9585,9586,9587,9588
LIST.SIZE CatNumber[3],CatLength[3]
LIST.CREATE N,CatNumber[4]
LIST.ADD CatNumber[4],9601,9602,9603,9604,9605,9606,9607,9608,9609,9610,9611,9612,9613,9614,9615
LIST.SIZE CatNumber[4],CatLength[4]
LIST.CREATE N,CatNumber[5]
LIST.ADD CatNumber[5],9650,9651,9654,9655,9660,9661,9664,9665,9670,9671,9672,9674,9675,9676,9678,9679,9680,9681,9698,9699,9700,9701
LIST.SIZE CatNumber[5],CatLength[5]
LIST.CREATE N,CatNumber[6]
LIST.ADD CatNumber[6],10025,10026,10027,10028,10029,10030,10031,10032,10033,10034,10035,10036,10037,10038,10039,10040,10041,10042,10043,10044,10045,10046,10047,10048,10049,10050,10051,10052,10053,10054,10055,10056,10057,10058,10059
LIST.SIZE CatNumber[6],CatLength[6]
LIST.CREATE N,CatNumber[7]
LIST.ADD CatNumber[7],9742,9743
LIST.SIZE CatNumber[7],CatLength[7]
LIST.CREATE N,CatNumber[8]
LIST.ADD CatNumber[8],9812,9813,9814,9815,9816,9817,9818,9819,9820,9821,9822,9823
LIST.SIZE CatNumber[8],CatLength[8]
LIST.CREATE N,CatNumber[9]
LIST.ADD CatNumber[9],9824,9825,9826,9827,9828,9829,9830,9831
LIST.SIZE CatNumber[9],CatLength[9]
LIST.CREATE N,CatNumber[10]
LIST.ADD CatNumber[10],9833,9834,9836,9837,9839
LIST.SIZE CatNumber[10],CatLength[10]
LIST.CREATE N,CatNumber[11]
LIST.ADD CatNumber[11],9856,9857,9858,9859,9860,9861
LIST.SIZE CatNumber[11],CatLength[11]
LIST.CREATE N,CatNumber[12]
LIST.ADD CatNumber[12],9985,9986,9987,9988
LIST.SIZE CatNumber[12],CatLength[12]
LIST.CREATE N,CatNumber[13]
LIST.ADD CatNumber[13],10136,10137,10138,10139,10140,10141,10142,10143,10144,10145,10146,10147,10148,10149,10150,10151,10152,10153,10154,10155,10156,10157,10158,10159,10161,10162,10163,10164,10165,10166,10167,10168,10169,10170,10171,10172,10173,10174,9756,9758
LIST.SIZE CatNumber[13],CatLength[13]
LIST.CREATE N,CatNumber[14]
LIST.ADD CatNumber[14],9618,9619,9620,9621,9632,9633,9634,9635,9636,9637,9638,9639,9640,9641,9642,9996,9997,9998,9999,10000,10001,10002,10003,10004,10005,10006,10007,10008,10009,10010,10011,10012,10013,10014,10015,10016,10017,10018,10019,10020,10021,10022,10023
LIST.SIZE CatNumber[14],CatLength[14]
CONSOLE.TITLE "UNICODE Characters"
FOR i=1 TO 5
  PRINT
NEXT i
L$=CHR$(9556)
FOR i=1 TO 29
  L$=L$+CHR$(9552)
NEXT i
L$=L$+CHR$(9559)
PRINT L$
PRINT CHR$(9553)+" Spice up your text screen ! "+CHR$(9553)
L$=CHR$(9568)
FOR i=1 TO 29
  L$=L$+CHR$(9552)
NEXT i
L$=L$+CHR$(9571)
PRINT L$
PRINT CHR$(9553)+" Use Unicode characters to   "+CHR$(9553)
PRINT CHR$(9553)+" add 'graphics' to your text "+CHR$(9553)
PRINT CHR$(9553)+" e.g. show a playing card... "+CHR$(9553)
L$=CHR$(9553)+" "+CHR$(9581)
FOR i=1 TO 10
  L$=L$+CHR$(9472)
NEXT i
L$=L$+CHR$(9582)
L$=L$+"                "+CHR$(9553)
PRINT L$
PRINT CHR$(9553)+" "+CHR$(9474)+CHR$(9825)+"       "+CHR$(9825)+CHR$(9474)+"                "+CHR$(9553)
PRINT CHR$(9553)+" "+CHR$(9474)+"          "+CHR$(9474)+"                "+CHR$(9553)
PRINT CHR$(9553)+" "+CHR$(9474)+"          "+CHR$(9474)+"                "+CHR$(9553)
PRINT CHR$(9553)+" "+CHR$(9474)+CHR$(9825)+"       "+CHR$(9825)+CHR$(9474)+"                "+CHR$(9553)
L$=CHR$(9553)+" "+CHR$(9584)
FOR i=1 TO 10
  L$=L$+CHR$(9472)
NEXT i
L$=L$+CHR$(9583)
L$=L$+"                "+CHR$(9553)
PRINT L$
PRINT CHR$(9553)+" or show some dice.....      "+CHR$(9553)
L$=CHR$(9553)+"  "
FOR i=9856 TO 9861
  L$=L$+CHR$(i)+"  "
NEXT i
L$=L$+"     "+CHR$(9553)
PRINT L$
PRINT CHR$(9553)+" or use them in graphics mode"+CHR$(9553)
L$=CHR$(9553)+" "
FOR i=10025 TO 10042
  L$=L$+CHR$(i)
NEXT i
L$=L$+"  "+CHR$(9553)
PRINT L$
PRINT CHR$(9553)+"                             "+CHR$(9553)
PRINT CHR$(9553)+"     Well, have fun !!!!     "+CHR$(9553)
L$=CHR$(9562)
FOR i=1 TO 29
  L$=L$+CHR$(9552)
NEXT i
L$=L$+CHR$(9565)
PRINT L$
GR.COLOR 255,255,0,255,1
GR.TEXT.SIZE 800
GR.TEXT.DRAW g,50,700,CHR$(9996)
GR.RENDER
PAUSE 1000
GR.FRONT 0
PAUSE 5000
FOR i=1 TO 26
  PRINT
  PAUSE 500
NEXT i
GR.FRONT 1
NwCat:
GR.CLS
GR.TEXT.SIZE 30
GR.TEXT.ALIGN 1
GR.COLOR 255,255,0,0,1
GR.RECT g,490,10,590,100
GR.COLOR 255,255,255,0,0
GR.TEXT.DRAW g,510,70,"EXIT"
GR.COLOR 255,255,255,0,0
GR.TEXT.SIZE 100
FOR i=1 TO 5
  GR.TEXT.DRAW g,5,150+i*100,CHR$(10046+i)
  GR.TEXT.DRAW g,500,150+i*100,CHR$(10046+i)
NEXT i
GR.TEXT.SIZE 30
GR.TEXT.ALIGN 2
GR.TEXT.DRAW g,300,70,"Graphic Characters"
GR.TEXT.DRAW g,300,100,"Please, select a category"
FOR i=1 TO NumOfCats
  GR.COLOR 255,255,0,0,1
  GR.RECT g,150,115+i*40,450,150+i*40
  GR.COLOR 255,0,0,255,0
  GR.RECT g,150,115+i*40,450,150+i*40
  LIST.GET Categories,i,CatName$
  GR.COLOR 255,255,255,0,0
  GR.TEXT.DRAW g,300,140+i*40,CatName$
NEXT i
GR.RENDER
GOSUB GetTap
IF CatSel<1 THEN GOTO Finito
GR.CLS
GR.TEXT.SIZE 30
GR.TEXT.ALIGN 1
GR.COLOR 255,255,0,0,1
GR.RECT g,10,10,150,100
GR.COLOR 255,255,255,0,0
GR.TEXT.DRAW g,40,70,"BACK"
GR.TEXT.ALIGN 2
GR.TEXT.DRAW g,300,70,"Graphic Characters"
LIST.GET Categories,CatSel,CatName$
GR.TEXT.DRAW g,300,100,CatName$
GR.TEXT.ALIGN 1
GR.COLOR 255,0,255,0,0
j=0
k=0
FOR i=1 TO CatLength[CatSel]
  LIST.GET CatNumber[CatSel],i,Char
  GR.TEXT.SIZE 25
  GR.TEXT.DRAW g,10+k*60,160+j*60,CHR$(Char)
  GR.TEXT.SIZE 15
  GR.TEXT.DRAW g,10+k*60,180+j*60,REPLACE$(FORMAT$("#####",Char)," ","")
  k=k+1
  IF i/10=FLOOR(i/10) THEN
    j=j+1
    k=0
  ENDIF
NEXT i
GR.RENDER
GOSUB GetBack
GOTO NwCat
Finito:
WAKELOCK 5
GR.CLOSE
EXIT
GetBack:
  DO
    GOSUB GetTouch
  UNTIL x>=10 & x<=150 & y>=10 & y<=100
RETURN
GetTap:
  DO
    GOSUB GetTouch
  UNTIL (x>=150 & x<=450 & y>=155 & y<=710) | (x>=490 & x<=590 & y>=10 & y<=100)
  CatSel=FLOOR((y-155)/40)+1
RETURN
GetTouch:
  DO
    GR.TOUCH touched,x,y
  UNTIL touched
  DO
    GR.TOUCH touched,x,y
  UNTIL !touched
  x/=sx
  y/=sy
RETURN
