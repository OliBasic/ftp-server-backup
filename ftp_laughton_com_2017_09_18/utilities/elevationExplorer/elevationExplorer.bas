GOSUB userfun

flagOri          = 1
refW             = 800
refH             = 1232
IF !flagOri        THEN SWAP refW, refH
GR.OPEN            255, 0 , 0 , 0 ,1, flagOri
GR.SCREEN          curW, curH
sca              = curW / refW
!PAUSE             500
GR.COLOR           255, 255, 0, 0, 1

fname$          = "tmpMap.jpg"
la              = 43
lo              = 5
latCent         = la+0.5
lonCent         = lo+0.5
type            = 3
zoom            = 14
sizex           = 640
sizey           = 640
centx           = sizex /2
centy           = sizey /2
offsptr         = 50
LIST.CREATE       n, scaleData
GOSUB             scaling


GR.COLOR          255, 255, 70, 000, 1
GR.TEXT.SIZE      28*sca
GR.TEXT.BOLD      1
GR.TEXT.ALIGN     1
GR.TEXT.TYPEFACE  2
GR.TEXT.DRAW      txtLat, 20,100, "Latitude  : "
GR.TEXT.DRAW      txtLon, 20,140, "Longutude : "
GR.TEXT.DRAW      txtGoogApi, 20,220, "Height Google : "
GR.TEXT.DRAW      txtHSrtm3, 20,260, "Height SRTM3 : "
GR.TEXT.SIZE      22*sca
GR.TEXT.DRAW      txtNeededFile, 20,300, "SRTM3file : "


posx            = (curw-sizex)/2
posy            = refh*0.4
GR.BITMAP.CREATE  canvas, sizex, sizey
GR.BITMAP.DRAW    pic, canvas, posx , posy
GOSUB             reloadmap

GR.set. stroke    3
GR.COLOR          255, 000, 000, 255 , 0
ptrRad          = 6
GR.CIRCLE         ptrPos, posx+lon2pix( lonCent ), posy+lat2pix( latCent ) , ptrRad


ARRAY.LOAD        menue0$[] , "zoom","moveMap", "END"
ARRAY.LOAD        menue01$[], "zoom 4", "zoom 5"," zoom 6 ", " zoom 7 ", "zoom 8"," zoom 9 ", " zoom 10 ", "zoom 11"," zoom 12 ", " zoom 13", "zoom 14"," zoom 15 ", " zoom 16"
posSetx         = refw*0.05
posSety         = refh*0.28
radSet          = 20
gearPoly        = gear ( radSet*0.8 , radSet , radSet/2 , 8)
GR.COLOR          255, 255, 70, 000 , 1
GR.POLY           settings, gearPoly, posSetx , posSety


!main -------------------------
DO

 touOld        = tou
 GR.TOUCH        tou, toux, touy
 IF              tou  &!touold THEN gosub touched
 IF              tou  & touold THEN gosub holded
 IF              !tou & touold THEN gosub released

 GR.RENDER

 PAUSE           MAX(50 -(CLOCK()-tic), 1)
 tic           = clock ()
UNTIL_END
!-------------------------------


END


!--------------------
touched:
IF                toux-posSetx<radSet & touy-posSety<radSet THEN gosub menue
GR.MODIFY         txtHSrtm3 , "text", "Height SRTM3 : --"
GR.MODIFY         txtGoogApi, "text", "Height Google: -- "
RETURN
!--------------------
holded:
GR.MODIFY         ptrPos ,"x",toux-offsptr ,"y",touy- offsptr
lat             = ROUND( pix2lat (touy-posy-offsPtr), 5)
lon             = ROUND( pix2lon (toux-posx-offsptr), 5)
GR.MODIFY         txtLat,"text", "Latitude  : "+ FORMAT$("#%.#####",lat)+"º"
GR.MODIFY         txtLon,"text", "Longutude : "+ FORMAT$("#%.#####",lon)+"º"
RETURN
!--------------------
released:
heiSrtm$         = STR$( getElevationSrtm3 ( lat , lon ))
srtmFile$        = getHgt$ ( lat,lon, 0)
IF                heiSrtm$ ="-9999.0" THEN heiSrtm$      = "no data, please download"
GR.MODIFY         txtHSrtm3 , "text",   "Height SRTM3 : "+ heiSrtm$
GR.MODIFY         txtNeededFile, "text","SRTM3-file : "+ srtmFile$
GR.MODIFY         txtGoogApi, "text",   "Height Google: "+ "...waiting for Google"
GR.RENDER
heiGoog$         = STR$( getElevationGoogleApi (lat,lon))
GR.MODIFY          txtGoogApi, "text",   "Height Google: "+ heiGoog$
RETURN
!--------------------


!--------------------
menue:
menue0 = 0
menue01 = 0
menue02 = 0
DIALOG.SELECT      menue0 ,menue0 $[]
IF                 menue0 =1 THEN dialog.select menue01,menue01 $[]
IF                 menue0 =2 THEN popup "under construction...", 0, 0, 0
IF                 menue0 =3 THEN _end=1
IF                 menue01 THEN
 zoom             = menue01+3
 GOSUB              scaling
 GOSUB              reloadMap
toux             = posx+centx+ offsptr
touy             = posy+centy+ offsptr
gosub              holded
 gosub              released

ENDIF
RETURN
!--------------------

!--------------------
scaling:
scaleLon        = 256 * 2^zoom / 360
scaleLat        = scaleLon / COS(TORADIANS(latCent))
LIST.CLEAR        scaleData
LIST.ADD          scaleData , centx, centy, scaleLon, scaleLat, lonCent, latCent
RETURN
!--------------------
reloadMap:
popup             "loading map...", 0, 0, 0
getStaticMap$ (latCent, lonCent, type, zoom, sizex, sizey ,fname$)
GR.BITMAP.DRAWINTO.START canvas
GR.COLOR          255, 255, 255, 000, 0
GR.BITMAP.LOAD    map, fname$
GR.BITMAP.DRAW    nn, map, 0, 0
GR.BITMAP.DELETE  map
GR.RECT           nn , lon2pix(lo), lat2pix(la+1) , lon2pix(lo+1), lat2pix(la)
GR.COLOR          255, 255, 255, 000, 1
GR.CIRCLE         nn, lon2pix(lo), lat2pix(la) ,6
GR.CIRCLE         nn, lon2pix(lo), lat2pix(la+1) ,6
GR.CIRCLE         nn, lon2pix(lo+1), lat2pix(la) ,6
GR.CIRCLE         nn, lon2pix(lo+1), lat2pix(la+1) ,6
GR.BITMAP.DRAWINTO.END
RETURN
!--------------------



!--------------------
userfun :

FN.DEF lat2pix (lat)
 LIST.TOARRAY     1, t[]
 FN.RTN           t[2]-(lat-t[6])*t[3]/COS(TORADIANS((lat+t[6])/2))
FN.END

FN.DEF lon2pix (lon)
 LIST.TOARRAY     1, t[]
 FN.RTN           t[1]+(lon-t[5])*t[3]
FN.END

FN.DEF pix2lat (pix)
 LIST.TOARRAY     1, t[]
 lat1           = t[2]/t[4]+t[6]-pix/t[4]
 tmp            = (t[3]/COS(TORADIANS((lat1+t[6])/2)))
FN.RTN           t[2]/tmp+t[6]-pix/tmp
FN.END

FN.DEF pix2lon (pix)
 LIST.TOARRAY     1, t[]
 FN.RTN           -t[1]/t[3]+t[5]+pix/t[3]
FN.END



FN.DEF           getElevationGoogleApi (lat,lon)
 !---------------
 !link: https://developers.google.com/maps/documentation/elevation/?hl=de
 !---------------
 url$          = "http://maps.googleapis.com/maps/api/elevation/json?locations=@@@&sensor=true_or_false"
 latLon$       = STR$(lat)+"," +STR$(lon)
 fname$        = "elev.txt"
 BYTE.OPEN       r, xx, REPLACE$(url$,"@@@", latLon$)
 BYTE.COPY       xx, fname$
 TEXT.OPEN       r, fid, fname$
 TEXT.READLN     fid, li$
 DO
  ctr++
  !PRINT         ctr,li$
  IF             is_in ("elevation" , li $) THEN d_u.break
  TEXT.READLN    fid, li$
 UNTIL           li$="EOF"
 FILE.DELETE     nn,fname$
 val           = -9999
 IF              is_in ("elevation" , li $) THEN val= ROUND(VAL(WORD$( li$, 2,"[:|,]")),1)
 FN.RTN          val
FN.END



FN.DEF getHgt$ ( lat,lon, flagDownload)
 url$           = "http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/"

 IF lon>=-34 & lon<180 & lat<62
  dStr$          = "Eurasia/"
  IF lon>=-34 & lon< 60 & lat<35   THEN dStr$="Africa/"
  IF lon>=110           & lat<-10  THEN dStr$="Australia/"
 ENDIF

 IF lon>=-170 & lon<-40 & lat<61 & lat>15  THEN dStr$ ="North_America/"
 IF lon>=-95  & lon<-30 & lat<15 & lat>-60 THEN dStr$ ="South_America/"

 IF lon>=0 THEN EW$="E" ELSE EW$="W"
 IF lat>=0 THEN NS$="N" ELSE NS$="S"
 hgt$   =   REPLACE$(NS$ + FORMAT$("%%", abs ( FLOOR(lat)))~
 + EW$ + FORMAT$("%%%", abs ( FLOOR(lon)))," ","") + ".hgt.zip"

 !PRINT url$ + dStr$ + hgt $

 IF flagDownload
  PRINT "Downloading....
  BYTE.OPEN r,xx, url$ + dStr$ + hgt $
  BYTE.COPY xx, hgt$
  PRINT "...ready"
 ENDIF

 FN.RTN ".. SRTM3/"+ dStr$ + hgt $

FN.END



FN.DEF getElevationSrtm3 ( curLat , curLon )
 ! Variables -------------------------------------------------

 ! filename of needed srtm3-file(*.hgt) ------------------
 hgt$	=	REPLACE$("N" + FORMAT$("%%",FLOOR(curLat))~
 + "E" + FORMAT$("%%%",FLOOR(curLon))," ","") + ".hgt"

 ! http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/Eurasia/


 FILE.EXISTS fileExistsHgt, hgt$
 IF !fileExistsHgt THEN FN.RTN -9999

 gridWidth	=  1/1200			% each .hgt file  contains 1200 values per 1 deg
 refLat 	= FLOOR(curLat)+1
 refLon 		= FLOOR(curLon)

 posLat 		    = FLOOR((refLat - curLat)  / 	gridWidth )
 posLon 		    = FLOOR((curLon - refLon)  / gridWidth )
 ptrUpperLeft = FLOOR((posLat * 1201 + posLon) * 2 + 1)

 ! print curLat , curLon  , refLat, 	refLon , 	posLat , posLon

 BYTE.OPEN r, no1,  hgt$

 BYTE.POSITION.SET no1, ptrUpperLeft
 GOSUB read
 hnw = msb * 256 + lsb
 GOSUB read
 hne = msb * 256 + lsb

 BYTE.POSITION.SET no1, ptrUpperLeft+2402
 GOSUB read
 hsw = msb * 256 + lsb
 GOSUB read
 hse = msb * 256 + lsb

 BYTE.CLOSE no1

 ! linear interpolation --------------------------------------------
 hn = MOD(curLon, gridWidth ) / gridWidth * (hne - hnw) + hnw
 hs = MOD(curLon, gridWidth ) / gridWidth * (hse - hsw) + hsw
 hi = MOD(curLat, gridWidth ) / gridWidth * (hn - hs) + hs

 FN.RTN ROUND ( hi,1)

 read:
 BYTE.READ.BYTE no1,msb
 BYTE.READ.BYTE no1,lsb
 RETURN

FN.END





FN.DEF getStaticMap$ (lat, lon, type, zoom, sizex, sizey ,fname$)

 type$   = "roadmap"
 IF TYPE = 2 THEN type$ = "satellite"
 IF TYPE = 3 THEN type$ = "terrain"
 IF TYPE = 4 THEN type$ = "hybrid"

 url$ =         "http://maps.google.com/maps/api/staticmap?"
 url$ =  url$ + " center=" + FORMAT$("##%.######", lat) + "," + FORMAT$("##%.######", lon)
 url$ =  url$ + "&zoom="   + FORMAT$("##", zoom)
 url$ =  url$ + "&size="   + FORMAT$("####", sizex )    + "x" + FORMAT$("####", sizey )
 url$ =  url$ + "&maptype="+ type$

 url$ =  url$ + pa$
 url$ =  url$ + "&sensor=false"
 url$ =  replace$ (url$," ","")

 BYTE.OPEN r,xx,url$
 BYTE.COPY xx, fname$

 FN.RTN url$
FN.END


FN.DEF             gear(r1,r2,r3,ntooth)
 LIST.CREATE        n, li
 r=r1
 FOR i=0 TO 360 STEP 360/(ntooth*4)
  ctr+= 1
  IF ctr = 3 THEN
   ctr=1
   IF r=r1 THEN r=r2 ELSE r= r1
  END if
  LIST.ADD li,  r*COS(TORADIANS(i)), r*SIN(TORADIANS(i))
 NEXT
 FOR i=360 TO 0 STEP -12
  LIST.ADD li,  r3*COS(TORADIANS(i)), r3*SIN(TORADIANS(i))
 NEXT
 LIST.ADD li,  r*COS(TORADIANS(0)), r*SIN(TORADIANS(0))
 FN.RTN             li
FN.END



RETURN
!--------------------
