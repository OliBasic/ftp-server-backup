REM Start of BASIC! Program
!setup the program
!--------------
DIM button[10,10,5],Button$[80],keysprintable$[2],buttongridsize[7,5]
dim keyboardobj[200,5],gon[80],Screen$[10]

!Screen Setup block
!(INCLUDE includetdgrv4.2.bas)
!--------------
vmx=80	%Maximum number of Columns
vmy=28	%Maximum number of Rows
TSZ=26	%text size
ORI=0		%Screen Orientation view
!		0=Lanscape, 1=Portrait
GOSUB screensetup
!setup onscreen keyboard
GOSUB LoadButtonData
GOSUB drawbuttons
!initial keyboard settings
keyboard=1
keyshift=1
Entry$=""
Cursor$="<L"
Message=2

gosub showkeyboard

!main Program
!--------------
!this part makes up the keyboard input.
ProgramStop=0
Do

!do input...yea.
GOSUB gettouch
gosub whatkeytouched


until programstop=1

GR.CLOSE

print "you typed:"
for x=1 to 10
 print screen$[x]
next

END
!--------------


ScreenSetup:
!--------------
GR.OPEN 255,0,255,0
PAUSE 1000
GR.CLS
GR.ORIENTATION ORI
GR.COLOR 255,0,0,0,0
GR.SCREEN mgx,mgy
GR.TEXT.SIZE tsz
GR.TEXT.TYPEFACE 2
for x=1 to 10
  gr.text.draw gon[x],10,x*TSZ,chr$(160)
next x
screen$[1]="Please Type Something."
Screen$[2]="<L"
gr.modify gon[1],"text",screen$[1]
gr.modify gon[2],"text",screen$[2]
RETURN

DisplayMessage:
M$=Entry$+Cursor$
GR.Modify GON[Message],"text",M$
gr.render
return

gettouch:
!Start of the touch loop.
!Returns TX1,TY1,TX2,TY2 - coordinates of touch and release.
!loop until the screen is touched
touch = 0
DO
 GR.TOUCH touch,tx1,ty1
UNTIL touch = 1
!loop until screen is not touched
DO
 GR.TOUCH touch,tx2,ty2
UNTIL touch = 0
RETURN

WhatKeyTouched:
!takes the coordinates and figures out which key was touched.
xpos=ceil(tx1/bxp)
Ypos=ceil(ty2/byp)
keytouch=button[xpos,ypos,keyboard]

if keytouch=0
  key$="none"
else
  key$=button$[keytouch]
  Tone 525.25,140
endif
type$=mid$(keytype$,keytouch,1)

if type$="1"
 !typeable character
 if keyshift=3
  E$=mid$(keysprintable$[2],keytouch,1)
 else
  E$=mid$(keysprintable$[keyshift],keytouch,1)
 endif
 Entry$=Entry$+E$
 if keyshift=2
  keyshift=1
 endif
 gosub setcursor
 gosub DisplayMessage
endif

!control keys
if keytouch=1
 !menu
endif

if keytouch=2
 !exit
 programstop=1
endif

if keytouch=3
 !switch
 gosub hidekeyboard
 keyboard=keyboard+1
 if keyboard>numkeyboard
  keyboard=1
 endif
 gosub showkeyboard
endif

if keytouch=5
 !enter
 !end one input, scroll everything down, start another.
 gosub scrollscreen
endif

if keytouch=16
 !tab
 e=len(entry$)
 t=frac(e/8)
 t=8-ceil(t*8)
 for x = 1 to t
  Entry$=Entry$+" "
 next
 gosub DisplayMessage
endif

if keytouch=26
 !shift
 keyshift=keyshift+1
 if keyshift>3
  keyshift=1
 endif
 gosub setcursor
 gosub DisplayMessage
endif

if keytouch=35
 !back space
 e=len(entry$)
 if e=0
  if message>1
   entry$=""
   c$=cursor$
   cursor$=""
   gosub displaymessage
   entry$=screen$[message-1]
   message=message-1
   gosub displaymessage
   cursor$=c$
  endif
 endif
 IF e>1
  Entry$=left$(entry$,e-1)
 endif
 if e=1
  entry$=""
 endif
 gosub DisplayMessage
endif

!add more control keys as needed.
return

setcursor:
if keyshift=1
  cursor$="<L"
endif
if keyshift=2
  cursor$="<U"
endif
if keyshift=3
  cursor$="<C"
endif
return

ScrollScreen:
screen$[message]=entry$
message=message+1
if message>10
 for x=2 to 10
  screen$[x-1]=screen$[x]
 next
 screen$[10]=cursor$
 message=10
else
 screen$[message]=cursor$
endif
for x=1 to message
 GR.Modify GON[x],"text",screen$[x]
 gr.render
next
entry$=""
Return

Drawbuttons:
!draws the button lines/names on screen.
!set keyboard color -- transparent but visible
!about 1/3 opacity / alpha
GR.COLOR 85,0,0,255,1

bts=CEIL(byp/1.7)
GR.TEXT.SIZE bts
for z=1 to numkeyboard
 numcol=buttongridsize[1,z]
 numrow=buttongridsize[2,z]
 bxp=buttongridsize[3,z]
 byp=buttongridsize[4,z]
 bnmaxx=buttongridsize[5,z]
 bnmaxy=buttongridsize[6,z]
 nobj=0
 FOR y=1 TO numrow
  FOR x = 1 TO numcol
   CB=button[x,y,z]
   !calculate coordinates for lines.
   curl=x-1
   curl=curl*bxp
   curl=curl+1
   curr=x*bxp
   curt=y-1
   curt=curt*byp
   curt=curt+1
   curb=y*byp
   !get button values of adjacent squares
   IF x+1>numcol
    Rb=cb
   ELSE
    rb=button[x+1,y,z]
   ENDIF
   IF y+1>numrow
    bb=cb
   ELSE
    bb=button[x,y+1,z]
   ENDIF
   !compare button values, and draw lines
   IF cb<>rb
    nobj=nobj+1
    GR.LINE a,curr,curt,curr,curb
    keyboardobj[nobj,z]=a
   ENDIF
   IF cb<>bb
    nobj=nobj+1
    GR.LINE a,curl,curb,curr,curb
    keyboardobj[nobj,z]=a
   ENDIF

   !check if button to left is same as current button
   if x-1>0
    lb=button[x-1,y,z]
   else
    lb=0
   endif
   if cb=lb
    cb=0
   endif
   !check if button above is same as current button
   if y-1>0
    tb=button[x,y-1,z]
   else
    tb=0
   endif
   if cb=tb
    cb=0
   endif

   !draw button name
   IF CB>0
    !make sure name fits square.
    loop1=0
    loop2=bnmaxy
    do
     gr.text.size loop2
     gr.text.width a,button$[cb]
     if a<=bnmaxx
      loop1=1
     else
      loop2=loop2-1
     endif
    until loop1=1
    axoff=floor((bxp-a)/2)+curl
    ayoff=curb-floor((byp-loop2)/2)
    nobj=nobj+1
    GR.TEXT.DRAW a,axoff,ayoff,Button$[cb]
    keyboardobj[nobj,z]=a
    gr.text.size bts
   ENDIF
  NEXT x
 NEXT y
 GR.RENDER
 buttongridsize[7,z]=nobj
 keyboard=z
 gosub hidekeyboard
next z
RETURN

hidekeyboard:
n=buttongridsize[7,keyboard]
for x=1 to n
 gr.hide keyboardobj[x,keyboard]
next
gr.render
return

showkeyboard:
n=buttongridsize[7,keyboard]
for x=1 to n
 gr.show keyboardobj[x,keyboard]
next
gr.render
return

LoadButtonData:
!--------------
!Read the button data
READ.NEXT NumKeyboard
For z=1 to numKeyboard
 read.next NumCol,NumRow
 FOR y = 1 TO NumRow
  FOR X = 1 TO NumCol
   READ.NEXT Button[x,y,Z]
  NEXT
 NEXT
 BXP=MGX / NumCol
 BXp=CEIL(BXP)
 BYP=MGY / NumRow
 BYP=CEIL(bYP)
 !ButtonNameMax pixels
 bnmaxx=bxp-4
 bnmaxy=byp-4
 buttongridsize[1,z]=numcol
 buttongridsize[2,z]=numrow
 buttongridsize[3,z]=bxp
 buttongridsize[4,z]=byp
 buttongridsize[5,z]=bnmaxx
 buttongridsize[6,z]=bnmaxy
next
READ.NEXT NumBut
FOR x = 1 TO NumBut
 READ.NEXT Button$[x]
NEXT x
read.next p1$,p2$
KeyType$=p1$+p2$
read.next p1$,p2$
KeysPrintable$[2]=p1$+p2$
read.next p1$,p2$
KeysPrintable$[1]=p1$+p2$
!need to put in the \ and the "
!"
button$[37]=chr$(34)
for x=1 to 2
 r1$=left$(keysprintable$[x],36)
 r2$=right$(keysprintable$[x],33)
 r3$=r1$+chr$(34)+r2$
 keysprintable$[x]=r3$
next
!\
button$[62]=chr$(92)
for x=1 to 2
 r1$=left$(keysprintable$[x],61)
 r2$=right$(keysprintable$[x],8)
 r3$=r1$+chr$(92)+r2$
 keysprintable$[x]=r3$
next
return

!KEYBOARD button data
!number of keyboards
read.data 3
!keyboard#1
!number of columns, number of rows
READ.DATA 10,6
!button grid data
Read.data 01,00,00,00,00,00,00,00,00,02
read.data 00,00,00,00,00,00,00,00,00,00
read.data 06,07,08,09,10,11,12,13,14,15
read.data 16,17,18,19,20,21,22,23,24,25
read.data 26,27,28,29,30,31,32,33,34,35
read.data 03,36,37,38,04,04,04,39,05,05
!keyboard #2
READ.DATA 10,6
Read.data 01,00,00,00,00,00,00,00,00,02
read.data 00,00,00,00,00,00,00,00,00,00
read.data 40,41,42,43,44,45,46,47,48,49
read.data 50,51,52,53,54,55,56,57,58,59
read.data 60,61,62,63,64,65,66,67,68,35
read.data 03,69,70,38,04,04,04,39,05,05
!keyboard #3
READ.DATA 10,6
Read.data 01,00,00,00,00,00,00,00,00,02
read.data 00,00,00,00,00,00,00,00,00,00
read.data 00,00,00,00,00,00,00,00,00,00
read.data 00,00,00,00,00,00,00,00,00,00
read.data 00,00,00,00,00,00,00,00,00,00
read.data 03,00,00,00,04,04,04,00,05,05
!number of uneak button names
READ.DATA 70
!button names
READ.DATA "MENU","EXIT","SWITCH","SPACE","ENTER"
Read.data "Q","W","E","R","T","Y","U","I","O","P"
Read.data "TAB","A","S","D","F","G","H","J","K","L"
Read.data "SHIFT","!","Z","X","C","V","B","N","M","BACK"
Read.data "Under Score","QUOTE","COMMA","."
read.data "1","2","3","4","5","6","7","8","9","0"
read.data "@","#","$","%","&","*","-","+","(",")"
read.data "|","~"," "
read.data "`"
read.data ":"
read.data ";"
read.data "/"
read.data "?"
read.data "="
read.data "<"
read.data ">"
!the following three strings are in two halfs.
!just read both halves then add togather.
! (string$=1st$ + 2nd$)
!KeyType$
read.data "00010111111111101111111110111111110"
read.data "11111111111111111111111111111111111"
!printed keys -- upper case string, then lower case string.
read.data "     QWERTYUIOP ASDFGHJKL !ZXCVBNM _ ,."
read.data "1234567890@#$%&*-+()|~ `:;/?=<>"
read.data "     qwertyuiop asdfghjkl !zxcvbnm _ ,."
read.data "1234567890@#$%&*-+()|~ `:;/?=<>"
