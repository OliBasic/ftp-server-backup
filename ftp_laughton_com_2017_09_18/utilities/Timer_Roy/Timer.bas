Rem Simple Timer
Rem For Android
Rem With RFO Basic!
Rem Febuary 2017
Rem Version 1.00
Rem By Roy Shepherd

PORTRAIT = 1 : LANDSCAPE = 0
orientation = PORTRAIT

if orientation = LANDSCAPE then
    di_height = 672
    di_width = 1150
elseif orientation = PORTRAIT then
    di_height = 1150
    di_width = 672
endif
now = time()
gr.open 255, 117, 117, 255
gr.color 255, 255, 255, 255
gr.text.size 30
gr.orientation orientation
pause 500
WakeLock 3
 
gr.screen screenWidth, screenHeight
scale_x = screenWidth / di_width
scale_y = screenHeight / di_height
gr.scale scale_x, scale_y

gosub Functions
gosub Initialise

call Render()
do : pause 1 : until 0

!------------------------------------------------
! BackKey interrupt. Double tap back key to end
!------------------------------------------------
onBackKey:
    backKeyHit = BackKey(backKeyHit)
back.resume 

!------------------------------------------------
! Touch interrupt
!------------------------------------------------
OnGrTouch:
    call TouchButton()
gr.onGrTouch.resume

!------------------------------------------------
! Timer interrupt
!------------------------------------------------
onTimer:
   call CountDown()
timer.resume

!------------------------------------------------
! Do once at first run of app
!------------------------------------------------
Initialise:
    !Set Global variables 
    dim digitsOrder$[10]
    TRUE = 1 : FALSE = 0
    sound = TRUE : buzz = TRUE
    backTick = 0 % for double tap back key
    w = di_width : h = di_height
    _runningTimer = FALSE
    seconed = 0 : minute = 0 : hour = 0
    stringTime$ = "00:00:00"
    setDigit = 9 : allSeconeds = 0
    _timerSet = FALSE
    alarmSounding = 0 
   
    ! Heading 
    gr.color 255, 100, 100, 100
    gr.rect null , 0, 0, di_width, 50 
    gr.color 255, 255, 255, 255
    gr.text.align 2 : gr.text.draw null, w / 2, 30, "Timer"
    
    ! Timer box
    gr.set.stroke 4 : gr.color 255, 255, 255, 255, 0
    gr.rect null, 50, 100, w - 50, 250
    gr.set.stroke 1 : gr.color 255, 255, 255, 255, 1
    gr.text.size 140 : gr.text.align 2
    gr.text.draw txtTimer, w / 2, 225, stringTime$
    gr.text.size 30
    gr.text.align 1 : gr.text.draw txtStartPause, 100, 300, "Start"
    gr.text.align 3 : gr.text.draw txtReset, w - 100, 300, "Reset/Stop"
    
    gr.color 0 : gr.rect butStartPause, 75, 260, 250, 330
    gr.rect butReset, 400, 260, 600, 330
    
    ! Setup some colours 
    gr.color 255, 255, 255, 255 : gr.paint.get white
    gr.color 255, 0, 0, 0       : gr.paint.get black
    gr.color 255, 255, 0, 0     : gr.paint.get red
    gr.color 255, 0, 255, 0     : gr.paint.get green
    gr.color 255, 0, 0, 255     : gr.paint.get blue
    gr.color 255, 255, 255, 0   : gr.paint.get yellow
    gr.color 255, 200, 200, 200 : gr.paint.get gray 
    
    ! Draw buttons to set timer 
    dim keyButtons[10] : b = 1
    
    gr.text.size 60 : gr.set.stroke 2
    for y = 350 to 750 step 200
        for x = 40 to 540 step 200
            !gr.color 255, 200, 200, 200, 1
            gr.rect keyButtons[b], x, y, x + 200, y + 200
            gr.modify keyButtons[b], "paint", gray
            gr.color 255, 50, 50, 50, 1
            gr.text.draw null, x + 115, y + 120, int$(b)
            b ++
        next 
    next
    gr.text.align 2
    gr.rect keyButtons[b], 40, y, w - 31, y + 150
    gr.modify keyButtons[b], "paint", gray
    gr.color 255, 50, 50, 50, 1
    gr.text.draw null, w / 2, y + 100, "0"
    
    ! Setup for collision for buttons tapper
    gr.color 0
    gr.point collision, -1, -1
    gr.color 255
    
    ! Set timer interrupt to be called each seconed
    timer.set 1000
return
            
!------------------------------------------------
! All functions below
!------------------------------------------------
Functions:

!------------------------------------------------
! Render if not in background
!------------------------------------------------
    fn.def Render()
        if ! background() then gr.render
    fn.end
    
!------------------------------------------------
! Using interrupt. Has a button been tapped 
!------------------------------------------------
    fn.def TouchButton()
        if time() - now < 3000 then fn.rtn 0
        if dropDownShowing then fr.rtn 0
        gr.touch t, tx, ty
        tx /= scale_x : ty /= scale_y
        gr.modify collision, "x", tx, "y", ty
        if gr_collision(collision, butStartPause) then call ToggleStartPause() : fn.rtn 0
        if gr_collision(collision, butReset) then call ZeroTime() : fn.rtn 0
        call KeyPad()
    fn.end

!------------------------------------------------
! Has a button (0 to 9) been tapped
!------------------------------------------------
    fn.def KeyPad()
        if _runningTimer | setDigit = 1 then fn.rtn 0
        if _timerSet then fn.rtn 0
        for key = 1 to 10
            if gr_collision(collision, keyButtons[key]) then
                gr.modify keyButtons[key], "paint", green
                call Render()
                do : gr.touch t, null, null : until ! t
                gr.modify keyButtons[key], "paint", gray
                call SetTimer(key)
                call Render()
            endif
        next
    fn.end

!------------------------------------------------
! Add digits to timer
!------------------------------------------------
    fn.def SetTimer(key)
        if setDigit > 1 then 
            setDigit --
            if key = 10 then key$ = "0" else key$ = int$(key)
            
            if setDigit = 3 then setDigit --
            if setDigit = 6 then setDigit --
           
            stringTime$ = ReplaceChr$(stringTime$, setDigit, key$)
            gr.modify txtTimer, "text", stringTime$
            digitsOrder$[setDigit] = key$
            
            ! Swap seconds
            if setDigit = 7 then 
                stringTime$ = ReplaceChr$(stringTime$, 8, digitsOrder$[7])
                stringTime$ = ReplaceChr$(stringTime$, 7, digitsOrder$[8])
                gr.modify txtTimer, "text", stringTime$
            endif
            ! Swap minute
            if setDigit = 4 then 
                stringTime$ = ReplaceChr$(stringTime$, 5, digitsOrder$[4])
                stringTime$ = ReplaceChr$(stringTime$, 4, digitsOrder$[5])
                gr.modify txtTimer, "text", stringTime$
            endif
            ! Swap hours
            if setDigit = 1 then 
                stringTime$ = ReplaceChr$(stringTime$, 2, digitsOrder$[1])
                stringTime$ = ReplaceChr$(stringTime$, 1, digitsOrder$[2])
                gr.modify txtTimer, "text", stringTime$
            endif
            call SetHourMinuteSeconed(stringTime$)
        endif
    fn.end
    
!------------------------------------------------
! Set the Hour Minute and Seconed
!------------------------------------------------
    fn.def SetHourMinuteSeconed(stringTime$)
        hour = val(left$(stringTime$, 2))
        minute = val(mid$(stringTime$, 4, 2))
        seconed = val(right$(stringTime$, 2))
        allSeconeds = (hour * 60) * 60
        allSeconeds += (minute * 60) + seconed
    fn.end
    
!------------------------------------------------
! Show the remaining time in hours, minute and seconds
!------------------------------------------------
    fn.def RemainingTime(allSeconeds)
        hrs = 0 : mins = 0 : secs = 0
        secs = allSeconeds

        while secs >= 60
            secs = secs - 60
            mins = mins + 1
        repeat

        while mins >= 60
            mins = mins - 60 
            hrs = hrs + 1
        repeat
        
        hrs$ = int$(hrs) : mins$ = int$(mins) : secs$ = int$(secs)
        if hrs < 10 then hrs$ = "0" + int$(hrs)
        if mins < 10 then mins$ = "0" + int$(mins)
        if secs < 10 then secs$ = "0" + int$(secs)

        stringTime$ = hrs$ + ":" + mins$ + ":" + secs$
        gr.modify txtTimer, "text", stringTime$
       
        call Render()
        if allSeconeds > 0 then allSeconeds --
    fn.end

!------------------------------------------------
! Time up. Alarm sounds and device buzzes
!------------------------------------------------ 
    fn.def SoundAlarm()
        Tone 2000, 500 , 0
        call Buzzer(buzz)
    fn.end
    
!------------------------------------------------
! Update timer every seconed
!------------------------------------------------    
    fn.def CountDown()
        if ! _runningTimer then fn.rtn 0
        call RemainingTime(allSeconeds)
        if allSeconeds = 0 then call SoundAlarm() 
    fn.end
!------------------------------------------------
! Replace a character in a string
!------------------------------------------------    
    fn.def ReplaceChr$(string$, place, character$)
        string$ = left$(string$, place - 1) + character$ + mid$(string$, place + 1)
    fn.rtn string$
fn.end

!------------------------------------------------
! Toggle start and pause
!------------------------------------------------
    fn.def ToggleStartPause()
        _timerSet = TRUE
        if _runningTimer then 
            _runningTimer = FALSE
            gr.show menu
            gr.modify txtStartPause, "text", "Start"
        else
            _runningTimer = TRUE
            gr.modify txtStartPause, "text", "Pause"
            gr.hide menu
        endif
        do : gr.touch t, null, null : until ! t
        call Render()
        
    fn.end
    
!------------------------------------------------
! Reset the timer to zero 
!------------------------------------------------
    fn.def ZeroTime()
        _runningTimer = FALSE
        _timerSet = FALSE
        seconed = 0 : minute = 0 : hour = 0
        setDigit = 9 : allSeconeds = 0
        alarmSounding = 0
        stringTime$ = "00:00:00
        for r = 1 to 8 : digitsOrder$[r] = "" : next
        gr.show menu
        gr.modify txtStartPause, "text", "Start"
        gr.modify txtTimer, "text", stringTime$
        call Render()
    fn.end
    
!------------------------------------------------
! if sound then Play sound (ptr)
!------------------------------------------------
    fn.def PlaySound(sound, ptr)
        if sound then soundpool.play s,ptr,0.99,0.99,1,0,1
    fn.end
    
!------------------------------------------------
! if buzz then  vibrate
!------------------------------------------------
    fn.def Buzzer(buzz)
        if buzz then 
            array.load buzzGame[], 1, 100
            vibrate buzzGame[], -1
            array.delete buzzGame[]
        endif
    fn.end
  
!------------------------------------------------
! Load Sound files not found
!------------------------------------------------
    fn.def SoundLoadError(ptr, e$)
        if ptr = 0 then
            dialog.message "Sound file " + e$ + " not found", "Game will end",OK,"OK" 
            end
        endif
    fn.end
    
!------------------------------------------------
! Load image file not found
!------------------------------------------------
    fn.def ImageLoadError(ptr, e$)
        if ptr =- 1 then
            dialog.message "Image file " + e$ + " not found", "Game will end",OK,"OK" 
            end
        endif
    fn.end

!------------------------------------------------
! Double tap Back Key to end
!------------------------------------------------
    fn.def BackKey(backKeyHit)
        inTime = 3 % Three seconds 
        if backKeyHit = 3 then backKeyHit = 0
        if backKeyHit = 1 then 
            outTime = time() - backTick 
            outTime /= 1000 
            if outTime > inTime then backKeyHit = 0
        endif
        backKeyHit ++
        if backKeyHit = 1 then 
            backTick = time() 
            popup "Press again to Exit",, screenHeight / 2
        endif
        
        if backKeyHit = 2 then 
            backTock = time() - backTick
            backTock /= 1000
            if backTock <= inTime then exit
        endif
        fn.rtn backKeyHit
    fn.end

return

