! testing keyb.show v0.1
! http://rfobasic.freeforums.org/kb-show-working-in-graphics-t960.html

GR.OPEN 255,0,0,0,0,1
GR.COLOR 255,255,0,0,1
GR.TEXT.SIZE 100
PAUSE 1000
GR.TEXT.DRAW tx,100,100,""
KB.SHOW
GR.RENDER

DO
 inkey$ s$
 IF s$<>"@" THEN
  GR.MODIFY tx,"text",s$
  GR.RENDER
  PAUSE 500
  ENDIF
UNTIL 0