! testing keyb.show v0.21
! http://rfobasic.freeforums.org/kb-show-working-in-graphics-t960.html

GR.OPEN 255,0,0,0,0,1
GR.COLOR 255,255,0,0,1
GR.TEXT.SIZE 70
PAUSE 1000
GR.TEXT.DRAW tx,100,100,""
KB.SHOW
GR.RENDER

DO
 inkey$ s1$
 inkey$ s2$
 IF s2$="key 59" THEN
  IF ASCII(s1$)>=97 & ASCII(s1$)<=122 THEN mykey$=CHR$(ASCII(s1$)-32)
  IF s1$="0" THEN mykey$=")"
  IF s1$="1" THEN mykey$="!"
  IF s1$="2" THEN mykey$="@"
  IF s1$="3" THEN mykey$="#"
  IF s1$="4" THEN mykey$="$"
  IF s1$="5" THEN mykey$="%"
  IF s1$="6" THEN mykey$="^"
  IF s1$="7" THEN mykey$="&"
  IF s1$="8" THEN mykey$="*"
  IF s1$="9" THEN mykey$="("
  IF s1$="key 70" THEN mykey$="+"
  IF s1$="key 74" THEN mykey$=":"
  IF s1$="key 55" THEN mykey$="<"
  IF s1$="key 56" THEN mykey$=">"
  IF s1$="key 76" THEN mykey$="?"
  IF s1$="key 69" THEN mykey$="_"
  IF s1$="key 71" THEN mykey$="{"
  IF s1$="key 70" THEN mykey$="+"
  IF s1$="key 72" THEN mykey$="}"
  IF s1$="key 73" THEN mykey$="|"
  IF s1$="key 68" THEN mykey$="~"
  IF s1$="key 75" THEN mykey$=CHR$(34)
 ELSE
  mykey$=s1$
  IF s1$="key 74" THEN mykey$=";"
  IF s1$="key 75" THEN mykey$="'"
  IF s1$="key 56" THEN mykey$="."
  IF s1$="key 69" THEN mykey$="-"
  IF s1$="key 55" THEN mykey$=","
  IF s1$="key 76" THEN mykey$="/"
  IF s1$="key 70" THEN mykey$="="
  IF s1$="key 71" THEN mykey$="["
  IF s1$="key 73" THEN mykey$=CHR$(92)
  IF s1$="key 72" THEN mykey$="]"
  IF s1$="key 68" THEN mykey$="`"
  IF s1$="key 67" THEN mykey$="Del"
  IF s1$="key 66" THEN mykey$="Enter"
  IF s1$="key 24" THEN mykey$="Volume +"
  IF s1$="key 25" THEN mykey$="Volume -"
  IF s1$="key 61" THEN mykey$="Tab"
  IF s1$="key 82" THEN mykey$="Menu"
  IF s1$="key 84" THEN mykey$="Search"
 ENDIF
 IF mykey$<>"@" | s1$="2"  THEN
  GR.MODIFY tx,"text",mykey$
  GR.RENDER
 ENDIF
UNTIL 0
