! testing keyb.show v0.31
! http://rfobasic.freeforums.org/kb-show-working-in-graphics-t960.html
! type exit to quit 

kFlag=0 % Keyboard Flag
! GR.OPEN 255,0,0,0,0,0
GR.OPEN 255,0,0,0,0,1
GR.TEXT.ALIGN 1
GR.TEXT.SIZE 40
PAUSE 1000
GR.SCREEN w,h


GR.GET.TEXTBOUNDS "Ag",x,y,cx,cy
curtop=100+y-10
curBot=100+cy+10
GR.GET.TEXTBOUNDS "A g",x,y,dx,cy
curspace=dx-cx
curHome=150
curpos=0
curmode=0
curalpha=255
curcnt=0
GR.COLOR 255,255,255,255,2
GR.RECT cobj,curhome,curtop,curhome+1,curbot

GR.COLOR 255,255,0,255,2
GR.RECT lobj, 130,200,230,250
GR.RECT lobj, 250,200,350,250
GR.RECT lobj, 370,200,470,250
GR.COLOR 255,0,255,0,2
GR.TEXT.SIZE 30
GR.TEXT.DRAW ltxt,140,240,"Left"
GR.TEXT.DRAW rtxt,260,240,"Right"
GR.TEXT.DRAW itxt,380,240,"Insert"

GR.COLOR 255,255,0,0,1
GR.TEXT.SIZE 40
GR.TEXT.DRAW tx,curhome,100,""

KB.SHOW
GR.RENDER


kFlag=1 % Entering Keyboard routine
DO
start:
bg=background() % check if program is running in background mode
if bg=1 then kb.hide else kb.show  % hide if in background mode
 inkey$ s1$
 inkey$ s2$
 IF s2$="key 59" THEN
  IF ASCII(s1$)>=97 & ASCII(s1$)<=122 THEN mykey$=CHR$(ASCII(s1$)-32)
  IF s1$="0" THEN mykey$=")"
  IF s1$="1" THEN mykey$="!"
  IF s1$="2" THEN mykey$="@"
  IF s1$="3" THEN mykey$="#"
  IF s1$="4" THEN mykey$="$"
  IF s1$="5" THEN mykey$="%"
  IF s1$="6" THEN mykey$="^"
  IF s1$="7" THEN mykey$="&"
  IF s1$="8" THEN mykey$="*"
  IF s1$="9" THEN mykey$="("
  IF s1$="key 70" THEN mykey$="+"
  IF s1$="key 74" THEN mykey$=":"
  IF s1$="key 55" THEN mykey$="<"
  IF s1$="key 56" THEN mykey$=">"
  IF s1$="key 76" THEN mykey$="?"
  IF s1$="key 69" THEN mykey$="_"
  IF s1$="key 71" THEN mykey$="{"
  IF s1$="key 70" THEN mykey$="+"
  IF s1$="key 72" THEN mykey$="}"
  IF s1$="key 73" THEN mykey$="|"
  IF s1$="key 68" THEN mykey$="~"
  IF s1$="key 75" THEN mykey$=CHR$(34)
 ELSE
  mykey$=s1$
  IF s1$="key 74" THEN mykey$=";"
  IF s1$="key 75" THEN mykey$="'"
  IF s1$="key 56" THEN mykey$="."
  IF s1$="key 69" THEN mykey$="-"
  IF s1$="key 55" THEN mykey$=","
  IF s1$="key 76" THEN mykey$="/"
  IF s1$="key 70" THEN mykey$="="
  IF s1$="key 71" THEN mykey$="["
  IF s1$="key 73" THEN mykey$=CHR$(92)
  IF s1$="key 72" THEN mykey$="]"
  IF s1$="key 68" THEN mykey$="`"
  IF s1$="key 67" THEN mykey$="Del"
  IF s1$="key 66" THEN mykey$="Enter"
  IF s1$="key 24" THEN mykey$="Volume +"
  IF s1$="key 25" THEN mykey$="Volume -"
  IF s1$="key 61" THEN mykey$="Tab"
  IF s1$="key 82" THEN mykey$="Menu"
  IF s1$="key 84" THEN mykey$="Search"
 ENDIF
 IF (mykey$<>"@" | s1$="2") & s1$<>"key 59" THEN
  IF mykey$="left" | mykey$="right" THEN
   GOSUB adjustcursor
  ELSE
   IF mykey$="Del" THEN
    IF tx$<>"" & curpos<>0 THEN
     IF LEN(tx$)=1 THEN
      tx$=""
      curpos=0
     ELSE
      IF curpos<LEN(tx$) THEN
       tx$=LEFT$(tx$,curpos-1)+MID$(tx$,curpos+1)
      ELSE
       tx$=LEFT$(tx$,LEN(tx$)-1)
      ENDIF
      curpos=curpos-1
     ENDIF
    ENDIF
   ELSEIF mykey$="Enter" THEN
    tx$=""
    curpos=0
   ELSEIF LEN(mykey$)=1 THEN
    IF curmode=0 THEN
     tx$=LEFT$(tx$,curpos)+mykey$+MID$(tx$,curpos+1)
    ELSE
     tx$=LEFT$(tx$,curpos)+mykey$+MID$(tx$,curpos+2)
    ENDIF
    IF curpos<LEN(tx$) THEN
     curpos=curpos+1
    ENDIF
   END IF
  ENDIF
  GR.MODIFY tx,"text",tx$
  if tx$="exit" then end % end here
  curcnt=0
  curalpha=255
  GOSUB showcursor
 ELSE
  GR.TOUCH touched,x,y
  IF touched<>0 THEN
   GR.BOUNDED.TOUCH i,130,200,230,250
   GR.BOUNDED.TOUCH j,250,200,350,250
   GR.BOUNDED.TOUCH k,370,200,470,250
   DO
    GR.TOUCH touched,x,y
   UNTIL !touched
   i$=""
   IF i<>0 THEN i$="left"
   IF j<>0 THEN i$="right"
   IF k<>0 THEN i$="insert"
   IF i$<>"" THEN
    mykey$=i$
    GOSUB adjustcursor
   ENDIF
  ELSE
   curcnt=curcnt+1
   IF curcnt=100 THEN
    curcnt=0
    IF curalpha=0 THEN curalpha=255 ELSE curalpha=0
    GOSUB ShowCursor
   ENDIF
  ENDIF
 ENDIF
UNTIL 0

END

adjustcursor:
curold=curpos
IF mykey$="right" THEN
 IF curpos<LEN(tx$) THEN curpos=curpos+1
ELSEIF mykey$="left" then
 IF curpos>0 THEN curpos=curpos-1
ELSEIF mykey$="insert" then
 IF curmode=0 THEN
  curmode=1
  i$="Over"
 ELSE
  curmode=0
  i$="Insert"
 ENDIF
 GR.MODIFY itxt,"text",i$
 curold=-1
ENDIF
IF curold<>curpos THEN
 GOSUB ShowCursor
ENDIF
RETURN

ShowCursor:
GR.MODIFY cobj,"alpha",curalpha
IF curpos=0 THEN
 IF curmode=0 THEN
  GR.MODIFY cobj,"left",curhome
  GR.MODIFY cobj,"right",curhome+1
 ELSE
  IF tx$="" THEN
   dx=0
  ELSE
   IF LEFT$(tx$,1)=" " THEN
    dx=curspace
   ELSE
    GR.GET.TEXTBOUNDS LEFT$(tx$,1),x,y,dx,dy
   ENDIF
  ENDIF
  GR.MODIFY cobj,"left",curhome
  GR.MODIFY cobj,"right",curhome+dx+1
 ENDIF
ELSEIF curpos=LEN(tx$) then
 GR.GET.TEXTBOUNDS tx$,x,y,cx,cy
 IF RIGHT$(tx$,1)=" " THEN cx=cx+curspace
 GR.MODIFY cobj,"left",curhome+cx
 GR.MODIFY cobj,"right",curhome+cx+1
ELSE
 GR.GET.TEXTBOUNDS LEFT$(tx$,curpos),x,y,cx,cy
 IF MID$(tx$,curpos,1)=" " THEN cx=cx+curspace
 IF curmode=0 THEN
  GR.MODIFY cobj,"left",curhome+cx
  GR.MODIFY cobj,"right",curhome+cx+1
 ELSE
  IF MID$(tx$,curpos+1,1)=" " THEN
   dx=curspace
  ELSE
   GR.GET.TEXTBOUNDS MID$(tx$,curpos+1,1),x,y,dx,dy
  ENDIF
  GR.MODIFY cobj,"left",curhome+cx
  GR.MODIFY cobj,"right",curhome+cx+dx+1
 ENDIF
ENDIF
If !background() then GR.RENDER % Donnot stop running in background mode
RETURN

! back key pressed
onbackkey:
if kFlag=1 then goto start else back.resume
