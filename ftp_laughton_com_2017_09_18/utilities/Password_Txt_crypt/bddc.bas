! little encryption/decryption text with password
! cassiope34 Nov 2013

FN.DEF gsrc(rd)
  rz=randomize(rd)
  for f=1 to 5
    sr =rnd()
  next
  Fn.Rtn sr
FN.END

path$ ="BDDC/"
fileName$ ="bddc.cry"
gr.open 255,0,14,37,1,-1

gr.bitmap.load clavs,path$+"Clavier_M.png"

gr.screen r_w,r_h
if r_w>r_h 
  screenMode=0   % landscape
  d_w =1280
  d_h =800
  clx =550
  cly =340
  cdx =clx
  cdy =150
else
  screenMode=1
  d_w =800
  d_h =1280
  clx =50
  cly =800
  cdx =clx
  cdy =660
endif
gr.orientation screenMode   % fix the screen mode
scw =r_w/d_w
sch =r_h/d_h
gr.scale scw,sch

gr.bitmap.draw clav,clavs,clx,cly
gr.color 255,0,255,255,1
gr.rect code,cdx,cdy,cdx+550,cdy+80
gr.set.stroke 4
gr.text.size 32
gr.text.draw nul,cdx,cdy-35,"Password :"
gr.text.draw mess,cdx,cdy+130,""
gr.color 255,255,255,255,0
gr.rect rt1,1,1,1,1
gr.rect rt2,1,1,1,1
gr.hide rt1
gr.hide rt2
gr.color 255,0,0,0,1
gr.text.size 92
gr.text.draw cd,cdx+10,cdy+75,""
gr.render

File.Exists fe,path$+fileName$
if fe
  grabfile sources$, path$+fileName$
  src$ =left$(sources$,18)
  source$ =mid$(sources$,19)
else
  mess$ ="Créez un code secret.  (4 caract. mini)"
endif
gosub codeinput
if ok
  !print texte$
  TEXT.INPUT text$,texte$,"Tout sera crypté en quittant."
  gosub savefile
endif
OnError:
end "A bientôt"

codeinput:
cree:
gr.modify mess,"text",mess$
gr.render
res =0
res$ =""
cd$ =""
DO
  do
    gr.touch touched,tx,ty
  until touched
  tx/=scw
  ty/=sch
  if tx>clx & ty>cly
    tchx =floor((tx-clx)/192)+1
    tchy =floor((ty-cly)/108)+1
    res  =4*(tchy-1)+tchx
    res$ =mid$("123-456.789e*0_o",res,1)
    if res$ ="e"
      if len(cd$) then cd$=left$(cd$,len(cd$)-1)
    else
      if len(cd$)<10 & res$<>"o" then cd$+=res$
    endif
    gr.modify cd,"text",left$("• • • • • • • • • •",len(cd$)*2)
    rt=rt1
    offset=187
    if tchx>3
      rt=rt2
      offset=140
    endif
    gr.modify rt,"left",  3+clx+(tchx-1)*192
    gr.modify rt,"top",   5+cly+(tchy-1)*111
    gr.modify rt,"right", 3+clx+(tchx-1)*192+offset
    gr.modify rt,"bottom",5+cly+(tchy-1)*111+91
    gr.show rt
    gr.render
  endif
  do
    gr.touch touched,tx,ty
  until !touched
  gr.hide rt1
  gr.hide rt2
  gr.render
UNTIL tchx=4 & tchy=4
if len(cd$)>3
  if mess$<>"" & try$=""   % create
    try$ =cd$
    mess$ ="confirmez svp"
    gr.modify cd,"text",""
    goto cree
  elseif try$<>""    % confirm
    if cd$ = try$
      ascB$=""
      for c=1 to len(cd$)
        ascB$+=replace$(str$(ucode(mid$(cd$,c,1))),".0","")
      next
      rdz =val(ascB$)
      ok=1
    endif
  else
    gosub ctrl
    if wrong & wrong<3 & !ok
      gr.modify cd,"text",""
      mess$=""
      try$=""
      goto cree
    endif
  endif
endif
RETURN

ctrl:
  ascB$=""
  for c=1 to len(cd$)
    ascB$+=replace$(str$(ucode(mid$(cd$,c,1))),".0","")
  next
  rdz =val(ascB$)
  r =gsrc(rdz)
  if left$(str$(r)+"0000000000",18)=src$
    gr.close
    print "Décryptage..."
    Decrypt left$(str$(r)+"0000000000",18),source$,texte$
    cls
    ok =1
  else
    wrong++
    cls
    print "code erroné."
  endif
RETURN

savefile:
if rdz>0 & text$<>source$
  if text$=""
    File.Delete nul,path$+fileName$
    RETURN
  endif
  if fe then File.Rename path$+filename$,path$+"bddc.bak"
  cls
  print "Cryptage..."
  r=gsrc(rdz)
  enc$=left$(str$(r)+"0000000000",18)
  Encrypt enc$,text$,encpt$
  cls
  print enc$+encpt$
  console.save path$+fileName$
  cls
endif
RETURN
