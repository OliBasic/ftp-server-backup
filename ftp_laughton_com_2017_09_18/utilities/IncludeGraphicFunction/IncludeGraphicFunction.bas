fn_def_test.bas

REM Start of BASIC! Program

include /functions/hello.bas
! include some graphic funtionality

g = gr_init(1)

gr.text.size 30

a = hello (40,40, "Hello World")
b = hello (90,90, "Hello Pello")
c = hello (140,140, "Hello Kello")

! Set some text, get the
! graphic object list
! handle as result

do
gr.hide a
gr.render
pause 1000
gr.show a
gr.hide b
gr.render
pause 1000
gr.show b
gr.hide c
gr.render
pause 1000
gr.show c
gr.render
until 0

gr.close
end

