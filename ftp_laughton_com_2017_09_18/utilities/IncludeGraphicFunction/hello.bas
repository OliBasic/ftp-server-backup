! INCLUDED PRG:
! hello.bas

REM Start of INCLUDED FUNCTION set
fn.def gr_init(a)
gr.open 255,255,255,255
gr.color 255,0,0,0,1
gr.orientation 1
fn.rtn 1
fn.end

fn.def Hello(x,y,text$)
gr.text.draw drw1,x,y,text$
gr.render
fn.rtn drw1
fn.end
