!'Example values input
!'Input Voltage:     9v  
!'Output Voltage:  5.1v  
!'Max curret load: 15mW 

Menu:
cls
Print "Zener Resistor Calc"
Print "1 Zener"
Print "2 End"
Print " "
Print "     Pause 2 sec, wait"
pause 2000

INPUT "Choose a Number: ", c
If c = 1 then goto zcalc 
If c = 2 then goto goexit
If c > 2 then goto Menu

zcalc:
cls
Print "ZENER RESISTOR CALC"
Input "Voltage Zener Input: ",V
Input "Standard Voltage Zener. Out: ",Z
Input "Max current load in mAmp: ",A
!'
var1=1000*(V-Z)/A
Print "   Resistor value: "; round(var1)
!'W in mWatts. Use a value above the result.
var2=Z*A
Print "   mWatts Zener: "; round(var2)
!'W in mWatts. Use a value above the result.
var3=(V-Z)*A
Print  "   mWatts Resist: "; round(var3) 
Print "Pause 3 sec, wait"
Pause 3000

cls
Goto Menu

goexit:
cls
end