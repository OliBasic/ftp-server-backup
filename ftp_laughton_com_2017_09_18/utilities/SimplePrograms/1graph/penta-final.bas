REM Start of BASIC! Program
REM pentagram-final.bas
REM LucasDuarte and me :-) 05/24/2013

d = 2 %density

!'''=========================================
GR.OPEN 255,0,0,0,0,1
GR.ORIENTATION 1
PAUSE 500 %1000
GR.SCREEN w,h
sx=w/800
sy=h/1280
GR.SCALE sx,sy

GR.CLS
fill=1 %1 is fill
GR.COLOR 255,255,255,255,fill %white
GR.TEXT.SIZE 35
GR.TEXT.DRAW t1,250,1100," Magic Pentagram "

!'''=========================================
GR.COLOR 255,0,255,0,fill %green


FOR t = 0 TO 0.5 STEP 0.1
z=t
GOSUB @formula
x1=x
y1=y
z=t+(d*0.05)
GOSUB @formula
x2=x
y2=y

GR.LINE line,(-x1*280)+400,(-y1*280)+600,(-x2*280)+400,(-y2*280)+600

PAUSE 1000
NEXT t


FOR t = 0 TO 360 STEP 1
z=t+0.0141  %(d*0.05)/7.1
GOSUB @formula
GR.CIRCLE cir,(x*290)+400,(y*290)+600,2
NEXT t


TONE 500,300
DO
UNTIL 0
PAUSE 1000
GR.CLOSE
END


!'''==========================================
@formula:
x=SIN(666*z)
y=COS(666*z)

GR.RENDER
RETURN