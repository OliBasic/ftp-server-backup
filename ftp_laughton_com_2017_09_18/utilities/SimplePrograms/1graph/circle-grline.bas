! circle con line
! 2013/05/27


GR.OPEN 255,0,0,0,0,1
GR.ORIENTATION 1
PAUSE 500 %1000
GR.SCREEN w,h
ScaleX=800
ScaleY=1280
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
GR.CLS

GR.COLOR 255,255,255,255,0


!'''Oval
!xR=(200-1)/2
!yR=(230-1)/2
!FOR x=0 TO 200
! y1=yR+SQR(xR^2-(x-xR)^2)*(230/200)
! y2=yR-SQR(xR^2-(x-xR)^2)*(230/200)
! GR.LINE gl,x,y1,x,y2
!NEXT x


!'''circle
FOR t=0 TO 6.28 STEP 0.1

 !'''
 z=t
 x1=SIN(z)
 y1=-COS(z)
 !'''
 z=t+0.1
 x2=SIN(z)
 y2=-COS(z)


 GR.LINE gl,(x1*280)+400, (y1*280)+600, (x2*280)+400, (y2*280)+600
 
 GR.LINE gl,(x1*180)+400, (y1*180)+600, (x2*180)+400, (y2*180)+600
 
 GR.LINE gl,(x1*80)+400, (y1*80)+600, (x2*80)+400, (y2*80)+600
 

 GR.RENDER
NEXT t


TONE 400,310
DO
UNTIL 0
GR.CLOSE
END



