REM rfo Basic!

GR.OPEN 255,0,0,0,0,1
GR.ORIENTATION 1 %1
GR.SCREEN w,h
sx=w/800
sy=h/1280
GR.SCALE sx,sy

!'----------
pi= ATAN(1)*4
scale=1
item = 0
min = (9 * 180) / 500
max = 36 * 180

!'---------
FOR t = min TO max STEP 5 %0.01

 !'---------
 angle = t * (pi / 180)

 !'---------
 xi = -angle * COS((666*pi*angle)/8) * COS(angle)
 yi = -angle * SIN((666*pi*angle)/8) * SIN(angle)


GR.COLOR 255,RND()*255,RND()*255,RND()*255,1

GR.LINE lin, xi*400, -yi*600, xi+1*400, -yi+1*600

 GR.RENDER

NEXT t


Fin:
TONE 400,300
DO
UNTIL 0
GR.CLOSE
END
