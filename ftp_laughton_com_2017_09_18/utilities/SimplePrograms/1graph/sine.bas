rem Sine, today 
rem  original file biorythmsv099.bas by Antonis

dim intlx[701],intly[701]
pi=Atan(1)*4

Start:
cls

!''' Sine wave structure....
j=0
for i=0 to 28 step 0.04
    j=j+1
!'''(number 28)Separate cycles of the sine wave. (axe x) test it with 15     
    intlx[j]=i*28

!'''Formula to show
!'''Position(400) and amplitud(200) of y
    intly[j]=400-sin((i/28)*8*pi)*200
next i

!'''Graphic mode
gr.open 255,0,0,0,255
gr.orientation 1

!''' Draw in 800x1280 and scale
GR.screen w, h

IF w>h THEN % exchange values
    z = w
    w = h
    h = z
ENDIF

IF w = 752 THEN w = 800
scalex = w / 800
scaley = h / 1280
GR.scale scalex, scaley

gr.set.stroke 0
!'''3 Line white
gr.color 255,255,255,255,1
gr.line lobj, 0,400,800,400
gr.line lobj, 0,600,800,600
gr.line lobj, 0,200,800,200


!''' Green line
gr.set.stroke 12 %width line (test with 4)
gr.color 255,0,255,0,1

for i=1 to 701
   if i>1 then
      gr.line lobj,intlx[i-1],intly[i-1], intlx[i],intly[i]
   endif
   next i 

gr.render

!'''Wait, touch screen to exit or return key.
DO
  touched = -1
  GR.touch touched, x, y
  UNTIL touched>0
gr.cls
gr.close

end