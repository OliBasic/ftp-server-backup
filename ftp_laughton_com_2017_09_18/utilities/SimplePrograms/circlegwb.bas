REM translate from GWBasic to rfo Basic!
GR.OPEN 255,0,0,0,0,1
GR.ORIENTATION 1
GR.SCREEN w,h
sx=w/800
sy=h/1280
GR.SCALE sx,sy


fill=0 % 1 is fill
GR.COLOR 255,255,255,255,fill

FOR x1 = 50 TO 600 STEP 4
 y1 = 600 + SIN(x1 * 0.05) * 40
 GR.CIRCLE cir, x1 , y1 ,50
 GR.RENDER
NEXT x1

!GR.RENDER
Fin:
TONE 300,500
DO
UNTIL 0
GR.CLOSE
END
