REM Pentagrama Estrellado de Pitagoras

!Background color Black
a = 0
r = 0
g = 0
b = 0
gr.open a,r,g,b

! Draw 
!Set the draw color to white
a = 255
r = 255
g = 255
b = 255
fill = 255
gr.color a,r,g,b,fill

!'Line 
!'150,35,200,190
x1 = 240
y1 = 10
x2 = 350
y2 = 300
gr.line n, x1, y1, x2, y2

!Line 
!'200,190,65,100
x1 = 350
y1 = 300
x2 = 70
y2 = 120
gr.line n, x1, y1, x2, y2

!Line 
!'65,100,235,100
x1 = 70
y1 = 120
x2 = 400
y2 = 120
gr.line n, x1, y1, x2, y2

!Line 
!'235,100,100,190
x1 = 400
y1 = 120
x2 = 100
y2 = 300
gr.line n, x1, y1, x2, y2

!Line 
!'100,190,150,35
x1 = 240
y1 = 10
x2 = 100
y2 = 300
gr.line n, x1, y1, x2, y2

! Now render everything
gr.render

end