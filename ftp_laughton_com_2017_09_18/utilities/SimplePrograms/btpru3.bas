REM btkey.bas
REM  2012

!'''
bt.open
pause 500 %'pause to stabilize
gosub bt_connection

GR.open 255,0,0,0
GR.orientation 1
GR.render
GR.screen w, h
GR.render
IF w>h THEN % exchange values
    z = w
    w = h
    h = z
ENDIF
IF w = 752 THEN w = 800
scalex = w / 800
scaley = h / 1280

!'''Vibrate when touch a key
a=100
array.load Pattern[], 1~
a, a


!'**** Text of Buttons ****
GR.text.size 99*scalex
GR.text.bold 1
GR.color 255,255,255,255,1
GR.text.draw t1,80*scalex,145*scaley, "A"
GR.text.draw t2,80*scalex,305*scaley, "B"
GR.text.draw t3,80*scalex,465*scaley, "C"
GR.text.draw t4,630*scalex,145*scaley, "D"
GR.text.draw t5,630*scalex,305*scaley, "E"
GR.text.draw t6,630*scalex,465*scaley, "F"
!'''
GR.text.draw t7,325*scalex,650*scaley, "Up"
GR.text.draw t8,325*scalex,1060*scaley, "Dn"
GR.text.draw t9,360*scalex,850*scaley, "C"
GR.text.draw t10,580*scalex,850*scaley, "R"
GR.text.draw t11,140*scalex,850*scaley, "L"

GR.text.size 50*scalex
GR.text.draw t12,630*scalex,1195*scaley, "Exit"

GR.text.size 99*scalex
GR.text.bold 0
GR.color 255,255,255,255,0

!''' Draw Buttons.
!'***A & D
GR.rect rob1,30*scalex,40*scaley,200*scalex,180*scaley
GR.rect rob2,570*scalex,40*scaley,750*scalex,180*scaley

!'***B & E
GR.rect rob3,30*scalex,200*scaley,200*scalex,340*scaley
GR.rect rob4,570*scalex,200*scaley,750*scalex,340*scaley

!'***C & F
GR.rect rob5,30*scalex,360*scaley,200*scalex,500*scaley
GR.rect rob6,570*scalex,360*scaley,750*scalex,500*scaley


!'***up, down, center, rigth, left
GR.rect rob7,300*scalex,540*scaley,480*scalex,700*scaley   %up
GR.rect rob8,300*scalex,940*scaley,480*scalex,1100*scaley  %dwn
GR.rect rob9,300*scalex,740*scaley,480*scalex,900*scaley   %center
GR.rect rob10,520*scalex,740*scaley,700*scalex,900*scaley  %rigth
GR.rect rob11,80*scalex,740*scaley,260*scalex,900*scaley   %left

GR.rect rob12,600*scalex,1140*scaley,750*scalex, 1220*scaley %exit


GR.color 255,255,255,255,1
GR.render
!'*** Draw all components Finished *** 


!'******MAIN******
!'****************

!'****control=exchange data till STOP****
!'****first to talk is listener****

control:
 
!'****now send caracter**** 
!'****wait for user input****
 keyed$=""

touchme:
 DO % check 
   touched = -1
   GR.touch touched, x, y
   PAUSE 50
 UNTIL touched>0


!'***A  
  IF  x<200*scalex & y<180*scaley & y>40*scaley then 
  !'*** A 
    keyed$="a"
    !TONE 500,100
    Vibrate Pattern[], -1
	gosub W_Loop
   !POPUP "put a ",0,-160,0
    GR.render
 ENDIF 

!'***B
  IF  x<200*scalex & y<340*scaley & y>200*scaley then 
  !'*** B 
    keyed$="b"
    !TONE 500,100
    Vibrate Pattern[], -1
	gosub W_Loop
   !POPUP "put b ",0,-160,0
    GR.render
 ENDIF 

!'***C
  IF  x<200*scalex & y<500*scaley & y>360*scaley then 
  !'*** C 
    keyed$="c"
    !TONE 500,100
    Vibrate Pattern[], -1
	gosub W_Loop
   !POPUP "put c ",0,-160,0
    GR.render
 ENDIF 

!'***D
  IF  x<750*scalex & y<180*scaley & x>570*scalex & y>40*scaley then 
  !'*** D 
    keyed$="d"
    !TONE 500,100
    Vibrate Pattern[], -1
	gosub W_Loop
   !POPUP "put d ",0,-160,0
    GR.render
 ENDIF 

!'***E
  IF  x<750*scalex & y<340*scaley & x>570*scalex & y>200*scaley then 
  !'*** E 
    keyed$="e"
    !TONE 500,100
    Vibrate Pattern[], -1
	gosub W_Loop
   !POPUP "put e ",0,-160,0
    GR.render
 ENDIF 

!'***F
  IF  x<750*scalex & y<500*scaley & x>570*scalex & y>360*scaley then 
  !'*** F 
    keyed$="f"
    !TONE 500,100
    Vibrate Pattern[], -1
	gosub W_Loop
   !POPUP "put f ",0,-160,0
    GR.render
 ENDIF 

!'***Up
  IF  x<480*scalex & y<700*scaley & y>540*scaley then 
  !'*** Up 
    keyed$="U"
    !TONE 500,100
    Vibrate Pattern[], -1
	gosub W_Loop
   !POPUP "put Up ",0,-160,0
    GR.render
 ENDIF 

!'***Down
  IF  x<480*scalex & y<1100*scaley & y>940*scaley then 
  !'*** Dn 
    keyed$="D"
    !TONE 500,100
    Vibrate Pattern[], -1
	gosub W_Loop
   !POPUP "put Dn ",0,-160,0
    GR.render
 ENDIF 

!'***Center
  IF  x<480*scalex & y<900*scaley & x>300*scalex & y>740*scaley then 
  !'*** Center 
    keyed$="C"
    !TONE 500,100
    Vibrate Pattern[], -1
	gosub W_Loop
   !POPUP "put Center ",0,-160,0
    GR.render
 ENDIF 

!'***Right
  IF  x<700*scalex & y<900*scaley & x>520*scalex & y>740*scaley then 
  !'*** Right 
    keyed$="R"
    !TONE 500,100
    Vibrate Pattern[], -1
	gosub W_Loop
   !POPUP "put Right ",0,-160,0
    GR.render
 ENDIF 

!'***Left
  IF x<260*scalex & y<900*scaley & y>740*scaley then 
  !'*** Left 
    keyed$="L"
    !TONE 500,100
    Vibrate Pattern[], -1
	gosub W_Loop
   !POPUP "put Left ",0,-160,0
    GR.render
 ENDIF 

!'***Exit
  IF x<750*scalex & y<1220*scaley & y>1140*scaley then 
  !'*** exit 
    Vibrate Pattern[], -1
	gosub toexit 
   POPUP "EXIT ",0,-160,0
    GR.render
 ENDIF 


 !'*** loop FOR NEXT key *** 
 GOTO touchme 
ENDIF  % compose and send keyed
!'******* end of main program *********


!'**** subroutines ****
!'*********************

bt_connection:
 bt.connect

!''' Read status until
!''' a connection is made
!'''Remember activate and make
!'''visible the other device 
Do
 bt.status s
 if s = 1 
  print "Listening
  elseif s =2
   print "Connecting"
   elseif s = 3
    print "Connected"
 endif
 pause 1000

until s =3

!''' When a connection is made
!''' get the name of the connected
!''' device
!'bt.device.name device$
return


!''' *** Write Loop ****
W_Loop:

!''' Read status to insure
!''' that we remain connected.
!''' If disconnected, program
!''' reverts to listen mode.
!''' In that case, ask user
!''' what to do.
 bt.status s
 if s<> 3
  print "Connection lost"
  goto bt_connection
 endif
 
!''' Send a caracter
  bt.write keyed$

return
  

toexit:
bt.close
end
