REM f_BT8_i.bas
REM 2013, 2014
REM 780x1280


REM El include debe estar en source/xprotolab
INCLUDE /xprotolab/fn_xyplot.bas

flagOri     = 1

!'***---------------------------------------
BT.OPEN
PAUSE 500 %'pause to stabilize
GOSUB bt_connection

pi=ATAN(1)*4
_2pi = 2*pi
aa   = 256 %3000
DIM   x[ aa ]
DIM  y1[ aa ]
DIM y2$[ aa ]

!'***---------------------------------------
!'***openScreen

refW       = 780
refH       = 1280
IF !flagOri  THEN SWAP refW, refH
centW      = refW/2
centH      = refH/2
GR.OPEN    255,0,0,60,0,flagOri
PAUSE      1000
GR.SCREEN  curW,  curH
scaW       = curW / refW
scaH       = curH / refH
GR.SCALE   scaW,  scaH
desLoopTime= 50


!'***Vibrate when touch a key vib100ms
ARRAY.LOAD Pattern[], 0, 100


!'****Draw All Components****
GR.TEXT.SIZE 30
GR.COLOR 255,0,0,255,1
GR.TEXT.DRAW txp, 55, 850,"Xprotolab Simple Program rfo-Basic!"

!'**** Text of Buttons ****
GR.TEXT.SIZE 55
GR.TEXT.BOLD 1
GR.COLOR 255,255,255,255,1

!'***
GR.TEXT.DRAW t1,  65, 950, "CH1"
GR.TEXT.DRAW t2, 210, 950, "CH2"
GR.TEXT.DRAW t3, 360, 950, "CHD"

GR.TEXT.SIZE 40
GR.TEXT.DRAW t4, 630,1095, "Exit"

GR.TEXT.SIZE 89
GR.TEXT.BOLD 0
GR.COLOR 255,255,255,255,0

!'*** Draw Buttons.
!'***buttons left, center, rigth
GR.RECT rb1, 50,890,180,970   %left
GR.RECT rb2,200,890,330,970   %center
GR.RECT rb3,350,890,480,970   %rigth
!'***button exit
GR.RECT rb4,580,1109,750,1050 %exit


GR.COLOR 255,255,255,255,1
GR.RENDER

POPUP " Stabilization System ",0, 70,0

!'*** Draw all components Finished ***


!'*****************MAIN******************
!'***************************************

!'****control=exchange data till STOP****
!'****first to talk is listener****

control:
!'****now send caracter****
!'****wait for user input****
keyed$=""

touchme:
DO  %check
 touched = -1
 GR.TOUCH touched, xk, yk
 PAUSE 50
UNTIL touched>0

!'***Channel_1 boton izquierdo
IF xk<130*scaW & yk<980*scaH & xk>80*scaW & yk>900*scaH THEN
 !'*** Channel_1
 keyed$ = "r"
 !TONE 500,100
 VIBRATE Pattern[], -1
 PAUSE 300
 GOSUB  W_Loop
 GR.RENDER
ENDIF

!'***Channel_2 boton central
IF xk<350*scaW & yk<980*scaH & xk>250*scaW & yk>900*scaH THEN
 !'*** Channel_2
 keyed$ = "s"
 !TONE 500,100
 VIBRATE Pattern[], -1
 PAUSE 300
 !GOSUB W_Loop
 POPUP "put CH2, not yet ",0,-10,0
 GR.RENDER
ENDIF

!'***Channel_D boton derecho
IF xk<450*scaW & yk<980*scaH & xk>320*scaW & yk>900*scaH THEN
 !'*** Channel_D
 keyed$ = "t"
 !TONE 500,100
 VIBRATE Pattern[], -1
 PAUSE 300
 !GOSUB W_Loop
 POPUP "put CHD, not yet ",0,-10,0
 GR.RENDER
ENDIF


!'***Exit boton salir
IF xk<790*scaW & yk<1150*scaH & xk>400 & yk>1000*scaH THEN
 !'*** exit
 VIBRATE Pattern[], -1
 PAUSE 300
 GOSUB toexit
 GR.RENDER
ENDIF


!'*** loop FOR NEXT key ***
GOTO touchme
ENDIF  %Compose and send keyed
!'******* end of main program *********


!'************ Subroutines *****************
!'******************************************
bt_connection:
BT.CONNECT

!''' Read status until
!''' a connection is made
!'''Remember activate and make
!'''visible the other device
DO
 BT.STATUS s
 IF s = 1
  PRINT "Listening
 ELSEIF s =2
  PRINT "Connecting"
 ELSEIF s = 3
  PRINT "Connected"
 ENDIF
 PAUSE 1000

UNTIL s = 3
RETURN


!'*** Write Loop ****
W_Loop:

!'***Read status to insure that we remain connected.
!'***If disconnected, program reverts to listen mode.
!'*** In that case, ask user what to do.
BT.STATUS s
IF s <> 3
 PRINT "Connection lost"
 GOTO  bt_connection
ENDIF

!'*** Send a caracter
BT.WRITE keyed$

!'*** A time to finish data received
PAUSE 1000 
POPUP "Begin Capture ",0,70,0

!'***Open in \data folder
TEXT.OPEN W, F1, "BT8-i.txt"

!'*** Read messages until the message queue is
!'*** empty
DO
 BT.READ.READY rr
 IF rr
  BT.READ.BYTES btInp$

  !'******** Print and graph the result *******
  !'*** determine length of stream ...
  cntVal  = LEN( btInp$ )

  FOR i = 1 TO cntVal

   !'*** Pick up the characters of the stream--
   val8$ = MID$( btInp$, i, 1 )

   !'*** STRING TO INT.
   !'*Convert the characters to numbers (bytes)
   !'*Convert to signed data and scale
   !'*if necessary
   !'*and invert wave form (-1)
   !'*(/100) integer values to decimal
   
   y1[i] = ((ASCII( val8$ )-128) * -1)/100
      
   !'***Save result in \data folder  
   !'***F1 es "BT8-i.txt"
   TEXT.WRITELN F1, y1[i]
  NEXT i
 ENDIF
UNTIL rr = 0
TEXT.CLOSE F1 %Close file F1


!'***x[] 
!FOR k = -_2pi TO _2pi STEP 0.1 
FOR k = -6 TO 6 STEP 0.1 
 ctr      += 1
 x[ctr]   = k
NEXT k

!'***y[] 
TEXT.OPEN r, f, "BT8-i.txt" %path$
FOR i = 1 TO 256
 TEXT.READLN f, y2$[i]
 y1[i] = VAL(y2$[i])
NEXT i
TEXT.CLOSE f


Jump1:
!'-----------
GOSUB ploting

RETURN



!'***------------------------------------------
!'*** To exit program
toexit:

!'***Save result in \data folder
!CONSOLE.SAVE device$ + "-8-i.txt"

BT.CLOSE
GR.CLOSE
END

!'***------------------------------------------
!'***
ONERROR:
BT.CLOSE
GR.CLOSE
END


!'***------------------------------------------
!'***test it ----------------------------------
ploting:

!'*** create a diagram bundle (...object) -----
BUNDLE.CREATE     diag1

BUNDLE.PUT        diag1, "npoints"     ,  ctr
BUNDLE.PUT        diag1, "xs"          , -_2pi
BUNDLE.PUT        diag1, "xe"          ,  _2pi
BUNDLE.PUT        diag1, "ys"          ,  -1.1
BUNDLE.PUT        diag1, "ye"          ,  1.1
BUNDLE.PUT        diag1, "posX1"       ,  0.05
BUNDLE.PUT        diag1, "posY1"       ,  0.05 %0.502
BUNDLE.PUT        diag1, "posX2"       ,  1.4  %0.95
BUNDLE.PUT        diag1, "posY2"       ,  0.5  %0.95
BUNDLE.PUT        diag1, "cntDivX"     ,  12
BUNDLE.PUT        diag1, "cntDivY"     ,  9
BUNDLE.PUT        diag1, "border"      ,  0.09
BUNDLE.PUT        diag1, "borderCol"   ,  " 230 200 200 200 "
BUNDLE.PUT        diag1, "backGrCol"   ,  " 255 58  58  58  "
BUNDLE.PUT        diag1, "gridCol"     ,  " 255 50  100 50  "
!'*** Color line
BUNDLE.PUT        diag1, "lineCol"     ,  " 255 20 200 200 "


BUNDLE.PUT        diag1, "numbersSize" ,  16
BUNDLE.PUT        diag1, "nDigitXaxis" ,  2
BUNDLE.PUT        diag1, "nDigitYaxis" ,  4
BUNDLE.PUT        diag1, "updaterate"  ,  20


CALL plot(diag1, x[], y1[])

RETURN
!'------------------------------------------
