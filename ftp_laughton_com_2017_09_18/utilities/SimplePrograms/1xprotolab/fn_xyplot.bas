REM fn_xyplot.bas 
REM Brochi xyplot function.
REM 2014

!'***------------------------------------------
FN.DEF plot(diag, xval[], yval[])

 BUNDLE.GET  diag, "npoints"     ,  npoints
 BUNDLE.GET  diag, "xs"          ,  xs
 BUNDLE.GET  diag, "xe"          ,  xe
 BUNDLE.GET  diag, "ys"          ,  ys
 BUNDLE.GET  diag, "ye"          ,  ye
 BUNDLE.GET  diag, "posX1"       ,  posX1
 BUNDLE.GET  diag, "posY1"       ,  posY1
 BUNDLE.GET  diag, "posX2"       ,  posX2
 BUNDLE.GET  diag, "posY2"       ,  posY2
 BUNDLE.GET  diag, "cntDivX"     ,  cntDivX
 BUNDLE.GET  diag, "cntDivY"     ,  cntDivY
 BUNDLE.GET  diag, "border"      ,  border
 BUNDLE.GET  diag, "borderCol"   ,  borderCol$
 BUNDLE.GET  diag, "backGrCol"   ,  backGrCol$
 BUNDLE.GET  diag, "gridCol"     ,  gridCol$
 BUNDLE.GET  diag, "lineCol"     ,  lineCol$
 BUNDLE.GET  diag, "numbersSize" ,  numbersSize
 BUNDLE.GET  diag, "nDigitXaxis" ,  nDigitXaxis
 BUNDLE.GET  diag, "nDigitYaxis" ,  nDigitYaxis
 BUNDLE.GET  diag, "updaterate"  ,  updaterate


 GR.SCREEN      curW, curH
 offsX          =  posX1 * curW
 offsY          =  posY1 * curH
 widX           =  ( posX2 - posX1 ) * curW
 widY           =  ( posY2 - posY1 ) * curH
 borderX        =  border * widX
 borderY        =  border * widY
 pixX           =  widX - 2* borderX
 pixY           =  widY - 2* borderY

 fmt $          =  "################"
 pScax          =  pixX/(xe-xs)
 pScay          =  pixY/(ye-ys)
 border         =  (border *(pixX+pixy)/2)/2

 DIM            xp[ npoints ]
 DIM            yp[ npoints ]

 !PRINT curW, offsX, widX, offsX+ widX,

 curCol$        = borderCol$
 curFill        = 1
 GOSUB          setColor
 GR.RECT        nn,  offsX, offsY, offsX+ widx, offsY+ widy

 curCol$        = backGrCol$
 curFill        = 1
 GOSUB setColor
 GR.RECT         nn, offsX+borderx,offsY+bordery,offsX+pixX+borderx, offsY+pixY+bordery

 curCol$        = gridCol$
 GOSUB          setColor

 GR.SET.STROKE  1
 GR.TEXT.SIZE   numbersSize


 GR.TEXT.ALIGN   2
 FOR i           = 0 TO cntDivX
  tmp            = offsX+ borderx + i*pixX/cntDivX
  fmtstr$        = "#############%."+right $(fmt$, nDigitXaxis)
  tmp$           = replace $(format$ ( fmtstr$, xs+i*(xe-xs)/cntDivX)," " ,"")
  GR.LINE        nn, tmp, offsY+ bordery, tmp, offsY+ pixY+ bordery
  GR.TEXT.DRAW   nn, tmp, offsY+ pixY+ bordery+ numbersSize *1.2 , tmp$
 NEXT

 GR.TEXT.ALIGN   3
 FOR i           = 0 TO cntDivY
  tmp            = offsY+ bordery + i*pixY/cntDivY
  fmtstr$        = "#############%."+right $(fmt$, nDigitXaxis)
  tmp$           = replace $(format$ ( fmtstr$, ye - i* (ye-ys) /cntDivY )," " ,"")
  GR.LINE        nn, offsX+ borderx, tmp, offsX+ pixX+ borderx, tmp
  GR.TEXT.DRAW   nn, offsX+ borderx - 5, tmp, tmp$
 NEXT


 curCol$        = lineCol$
 GOSUB          setColor
 GR.SET.STROKE  1
 xo             =  offsX+ pixX+ borderx
 xu             =  offsX+       borderx
 yo             =  offsY+ pixY+ bordery
 yu             =  offsY+       bordery


 FOR i          =  1 TO npoints

  xp[i]         =  offsX+ borderx+        (xval[i] - xs )* pScax
  yp[i]         =  offsY+ bordery+ pixY - (yval[i] - ys )* pScay

  flagHideOld   = flagHide
  flagHide      = xp[i]>xo | xp[i]<xu | yp[i]>yo | yp[i]<yu

  IF            i>1 THEN  gr.line nn1,xp[i-1],yp[i-1],xp[i], yp[i]
  GR. circle    nn2, xp[i], yp[i], 2
  IF            flagHide | flagHideOld THEN gr.hide nn1
  IF            flagHide THEN gr.hide nn2
  IF            !MOD(i, updaterate ) THEN gr.render

 NEXT


 GR.RENDER


 FN.RTN 1

 setColor:
 GR.COLOR  ~
 VAL(WORD$(curcol$,1)), VAL(WORD$(curCol$,2)), ~
 VAL(WORD$(curCol$,3)), VAL(WORD$(curCol$,4)), curFill
 RETURN

FN.END
!'***------------------------------------------


