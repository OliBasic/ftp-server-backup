REM Capacitor Calc Touch
REM Capacitor-Touch.bas

!'''========================================
!'''mr. tony_gr function bigNumbers
FN.DEF bigNumbers$(number) 
 a$=FORMAT$("#####################.####################",number)
 a$=REPLACE$(a$," ","") % trim leading spaces

 !'''trim ending 0s of decimal numbers
 a=IS_IN(".", a$)
 IF a>0 THEN
  l=LEN(a$)
  DO
   d$=MID$(a$,l,1)
   IF d$="0" THEN a$=LEFT$(a$,l-1)
   l=l-1
  UNTIL MID$(a$,l,1)<>"0"

  IF ABS(number)<1 THEN
   a$=REPLACE$(a$,".","0.")
  ENDIF

  FN.RTN a$
 FN.END


 ARRAY.LOAD retorna$[], "1 - retorna"~
 "5 - Exit"

 ARRAY.LOAD menu$[], "1 - Nomenclature", "2 - MicroFaradio", "3 - NanoFaradio","4 - PicoFaradio"~
 "5 - Exit"

!'''======================================== 
Menu:
 knum = 1000

 DO
  msg$ ="Touch an Option"
  SELECT menu, menu$[], msg$

  IF (menu = 0) | (menu = 5)
   GOTO prgend
  ELSE
   IF menu = 1
    GOSUB nomenc
   ELSE
    IF menu = 2
     GOSUB microf
    ELSE
     IF menu = 3
      GOSUB nanof
     ELSE
      GOSUB picof
     ENDIF
    ENDIF
   ENDIF
  ENDIF
  prgend:
 UNTIL menu = 0 | menu = 5
 END

 REM ================
 !'nomenc subrutin
 nomenc:
 CLS
 var1$=" "

 PRINT "The value of a capacitor from the 3 digit code printed on its side."
 INPUT "Capacitor Number Code: ", key1

 key2$ = LEFT$(STR$(key1),3)
 num1$ = LEFT$(key2$,2)
 num2$ = RIGHT$(key2$,1)
 num3 = VAL(RIGHT$(key2$,1))
 var1$ = num1$

 FOR i = 1 TO num3
  var1$ = var1$ + "0"
 NEXT

 !capval$ = str$(num1)
 capval = VAL(var1$)
 Pico  = capval
 !'Pico  Value
 Nano  = Pico/knum
 !'Nano  Value
 Micro = Nano/knum
 !'Micro Value
 Mili  = Micro/knum
 !'Mili  Value

 GOSUB @Printing

 GOSUB @OnBackK

 CLS
 RETURN

 REM =======================
 !'microf subrutin
 microf:
 CLS
 PRINT "MicroFaradio"
 INPUT "Capacitor uF Value: ", key1

 Micro=key1

 Mili  = Micro/knum
 Micro = Micro
 Nano  = Micro*knum
 Pico  = Micro*knum^2

 GOSUB @Printing

 GOSUB @OnBackK

 CLS
 RETURN

 REM =======================
 !'nanof subrutin
 nanof:
 CLS
 PRINT "NanoFaradio"
 INPUT "Capacitor nF Value: ",key1

 Nano=key1

 Mili  = Nano/knum^2
 !'Mili  Value
 Micro = Nano/knum
 !'Micro Value
 Nano  = Nano
 !'Nano  Value
 Pico  = Nano*knum
 !'Pico  Value

 GOSUB @Printing

 GOSUB @OnBackK

 CLS
 RETURN

 REM =======================
 !'picof subrutin
 picof:
 CLS
 PRINT "PicoFaradio"
 INPUT "Capacitor pF Value: ",key1
 PRINT ""
 Pico=key1
 Mili  = Pico/knum^3
 !'Mili  Value
 Micro = Pico/knum^2
 !'Micro Value
 Nano  = Pico/knum
 !'Nano  Value
 Pico  = Pico
 !'Pico  Value

 GOSUB @Printing

 GOSUB @OnBackK

 CLS
 RETURN

 REM =======================
 !'@Printing subrutin
 @Printing:

 PRINT "Cap in  miliF: "; bigNumbers$(Mili)
 PRINT "Cap in microF: "; bigNumbers$(Micro)
 PRINT "Cap in  nanoF: "; bigNumbers$(Nano)
 PRINT "Cap in  picoF: "; bigNumbers$(Pico)
 PRINT ""
 PRINT ""
 PRINT "          Touch BackKey to Return"

 RETURN


 !'''------------------------------------------
 @OnBackK:
 wait:
 IF wait$ <> "no" THEN GOTO wait
 wait$ = ""
 RETURN

 ONBACKKEY:
 wait$="no"
 PAUSE 99   %Update 21.11.2012
 BACK.RESUME


OnError:
GOTO Menu



