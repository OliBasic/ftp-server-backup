REM KeyboardNumeric v0.1 / rfo-basic! v1.50
REM (C) Antonis Gen.24. 2012


DIM tobj[26],tobj2[10],tobj3[10],robj[50]

GR.open 255,0,0,0
GR.orientation 1
GR.render
GR.screen w, h
GR.render
IF w>h THEN % exchange values
    z = w
    w = h
    h = z
ENDIF
IF w = 752 THEN w = 800
scalex = w / 800
scaley = h / 1280

!'''Vibrate when touch a number
a=100
array.load Pattern[], 1~
a, a

!****keyboard chars****
str1$="1234567890"
str2$=",.():;-'!?"


!****draw keyboard rectangle****
GR.text.size 40*scalex
GR.color 255,0,0,255,1
GR.rect rectObj, 0, 790*scaley, 799*scalex, 1230*scaley
GR.color 255,255,255,255,0

GR.text.draw txp, 5*scalex, 1225*scaley,"Keyb-num rfo-Basic! by [Antonis]"
GR.text.size 60*scalex


!****numbers****
FOR j=1 to len(str1$)
strg$=MID$(str1$,j,1)

GR.text.draw tobj2[j],(10+(j-1)*81)*scalex,948*scaley,strg$
l=l+1
GR.rect robj[l],(3+(j-1)*81*scalex),890*scaley,(68+(j-1)*81)*scalex,958*scaley
NEXT j

!****special chars ",.():;-'!?"****
FOR j=1 to len(str2$)
strg$=MID$(str2$,j,1)

GR.text.draw tobj3[j],(10+(j-1)*81)*scalex,1020*scaley,strg$
l=l+1
GR.rect robj[l],(3+(j-1)*81)*scalex,970*scaley,(68+(j-1)*81)*scalex,1030*scaley

NEXT j

!****capslock, space, delete, RETURN****
GR.text.bold 1
GR.color 255,255,255,255,1
GR.text.draw t2,250*scalex,1100*scaley,"   [________] "
GR.text.draw t3,140*scalex,1100*scaley,"Del"
GR.text.draw t4,600*scalex ,1100*scaley,"  Go "
GR.text.bold 0
GR.color 255,255,255,255,0

GR.rect rob2,240*scalex,1040*scaley,580*scalex,1120*scaley
GR.rect rob3,135*scalex,1040*scaley,230*scalex,1120*scaley
GR.rect rob4,590*scalex,1040*scaley,755*scalex,1120*scaley
GR.color 255,255,255,255,1
GR.render
!---PRINTing characters finished--- 


!******MAIN******
!****************

!****chat=exchange data till STOP****
!****first to talk is listener****
firsttime=1
msgreceived=0
GR.color 255,25,255,0,1

chat:

!****now compose and send message**** 
!****wait for user input****
IF  msgreceived=1 | firsttime=1 THEN % here
  firsttime=0
writed$=""
touchme:
DO % check FOR max lenght
   touched = -1
   GR.touch touched, x, y
   PAUSE 50
UNTIL touched>0
IF  len(writed$)=45 THEN 
    POPUP "maximum length, press Go",0,0,2
  touchsend:
  DO
   touched = -1
   GR.touch touched, x, y
  UNTIL touched>0
  IF  x>590*scalex & y>1040*scaley & x<755*scalex &y<1120*scaley THEN 
      !??? GOSUB sendtext 
      msgreceived=0
     GOTO chat
    ELSE 
      GOTO touchsend
  ENDIF  
ENDIF 


!****scan keyboard for numbers****
FOR j=1 to 10
IF  x>(3+(j-1)*81)*scalex & x<((j-1)*81+68)*scalex & y>890*scaley & y<958*scaley THEN 
  !TONE 500,100
  Vibrate Pattern[], -1
  writed$=writed$+MID$(str1$,j,1)  
  GOSUB printmystring
  GR.render
ENDIF 
NEXT j

!***scan keyboard for letters
ydisp=0
xdisp=0
j=0
k=1

!****scan keyboard for space****
IF  x>240*scalex & y>1040*scaley & x<580*scalex &y<1120*scaley then
  !*** SPACE   
  writed$=writed$+" "
  !TONE 500,100   
  Vibrate Pattern[], -1
  GOSUB printmystring
  GR.render
ENDIF 

!****scan keyboard for DEL****
IF  x>135*scalex & y>1040*scaley & x<230*scalex &y<1120*scaley then
  !*** DEL 
  IF  len(writed$)>=1 then
    writed$=LEFT$(writed$,len(writed$)-1)
    TONE 500,100  
    GOSUB printmystring
    GR.render
  ENDIF 
ENDIF 


!****scan keyboard FOR Go****
IF  x>590*scalex & y>1040*scaley & x<755*scalex &y<1120*scaley then
     IF LEN(writed$)>0 THEN 
      !'''Put value writed$ on screen
      TONE 300,800
      GR.hide txtObjjj
      GR.text.draw txtObjjj, 50*scalex, 100*scaley, "Test: " + writed$
      GR.render
      writed$="" 
      msgreceived=0
      incoming=0
      !'''Clean value in blue rectangle
      gosub printmystring
     ELSE 
      Popup "Can't Go empty Value",0,0,2
     ENDIF 
ENDIF 


!****scan keyboard for special chars****
FOR j=1 to 10
IF  x>(3+(j-1)*81)*scalex & x<((j-1)*81+68)*scalex & y>970*scaley & y<1030*scaley THEN 
  !TONE 500,100
  Vibrate Pattern[], -1
  writed$=writed$+MID$(str2$,j,1)   
  GOSUB printmystring
  GR.render
ENDIF 
NEXT i
!*** loop FOR NEXT char *** 
GOTO touchme 
ENDIF  % compose and send message
!*******end of main program *********


!****subroutines****
!*******************

!****prints on screen my text****
printmystring:
GR.color 255,25,255,0,1
if incoming=0 then
  GR.hide tobj
  GR.hide tobjj
  GR.hide tobjjj
endif
l=len(writed$)

!***split writed$ into 3 pieces
GR.text.width ww,writed$
IF  ww<=800*scalex THEN 
                 wrtd$=writed$
                 wr$=""
                 w$=""
           ENDIF 
IF  ww>800*scalex & ww<=1600*scalex then
FOR i=2 to l
   GR.text.width w1,LEFT$(writed$,i-1)
   GR.text.width w2,LEFT$(writed$,i)
   IF  w1<=800*scalex & w2>800*scalex THEN ll=i
  NEXT i
  wrtd$=LEFT$(writed$,ll-1)
  wr$=MID$(writed$,ll,l-ll+1)
  w$=""
ENDIF 
IF  ww>1600*scalex then
FOR i=2 to l
   GR.text.width w1,LEFT$(writed$,i-1)
   GR.text.width w2,LEFT$(writed$,i)
   IF  w1<=800*scalex & w2>800*scalex THEN ll=i
   IF  w1<=1600*scalex & w2>1600*scalex THEN lll=i
  NEXT i
  wrtd$=LEFT$(writed$,ll-1)
  wr$=MID$(writed$,ll,lll-ll)
  w$=MID$(writed$,lll,l-lll+1) 
ENDIF  

IF  incoming=1 THEN 
        p1=200
        p2=260
        p3=320
        GR.hide tin
        GR.hide tinn
        GR.hide tinnn
        GR.text.draw tin,0,p1*scaley, wrtd$     
        IF  wr$<>"" THEN GR.text.draw tinn,0,p2*scaley, wr$    
        IF  w$<>""  THEN GR.text.draw tinnn,0,p3*scaley, w$
        GR.render
    ELSE 
        p1=850
        p2=850
        p3=850
        GR.text.draw tobj,0,p1*scaley, wrtd$     
        IF  wr$<>"" THEN GR.text.draw tobjj,0,p2*scaley, wr$    
        IF  w$<>""  THEN GR.text.draw tobjjj,0,p3*scaley, w$
        GR.render
ENDIF 

IF  incoming=0 THEN %I'm composing a message, count chars
  counter=45-l
  k=Ends_with( ".0", str$(counter))
  counter$=LEFT$(str$(counter),k-1)
  GR.hide countobj
  GR.text.draw countobj, 210*scalex, 790*scaley, counter$
  GR.render
ENDIF 
RETURN
