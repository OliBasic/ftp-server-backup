REM Start of BASIC! Program
REM chopter.bas


GR.OPEN 255, 58, 58, 58, 0, 0
PAUSE 1000

!'***Vibrate when touch a key vib20ms
ARRAY.LOAD vib20[], 0, 20

!'***Folder into /Data
path$ ="games/"


level=0
GOSUB LevelOne

@Start:
GR.SCREEN width, height
speed = 1  %velocidad chopter
xc = 250   %Posicion chopter
yc = 80    %en la pantalla
xb = 300   %chpath1 posicion (-1120 final)
yb = 0

!'***
GR.BITMAP.LOAD  png4,  path$+"chopter4.png"
GR.BITMAP.SCALE png5,  png4, 42, 37 %'chop size
GR.BITMAP.DRAW  chop1, png5, xc, yc

!'***Buttons
GR.COLOR   255, 255, 255, 255, 0 %BtnColor
GR.RECT BRight, 460, 360, 520, 420
GR.RECT  BLeft, 340, 360, 400, 420
GR.RECT    BUp, 400, 300, 460, 360
GR.RECT  BDown, 400, 420, 460, 480
GR.RECT  BFire, 405, 365, 455, 415

GR.RENDER
GOTO Loop


!'=============================================
Chpathmove:
xb = xb - speed
GR.MODIFY chpath, "x", xb
GR.MODIFY  chop1, "y", yc
GR.RENDER
RETURN


!'=============================================
Chopmove:
!'***Buttons action
GR.TOUCH touched, xt, yt
!GR.GET.POSITION chop1, chop1_x, chop1_y

!'***Movebuttons
!'***BRight
IF touched &  xt>460 & yt>360 & xt<520 & yt<420 THEN
 VIBRATE vib20[], -1
 key$ = "right"
ENDIF

!'***BLeft
IF touched &  xt>340 & yt>360 & xt<400 & yt<420 THEN
 VIBRATE vib20[], -1
 key$ = "left"
ENDIF

!'***BDown
IF touched &  xt>400 & yt>300 & xt<460 & yt<360 THEN
 VIBRATE vib20[], -1
 key$ = "down"
ENDIF

!'***BUp
IF touched &  xt>400 & yt>420 & xt<460 & yt<480 THEN
 VIBRATE vib20[], -1
 key$ = "up"
ENDIF

!'***BFire
IF touched &  xt>405 & yt>365 & xt<455 & yt<420 THEN
 VIBRATE vib20[], -1
 key$ = "go"
ENDIF

IF key$ = "go" %'*Program exit.
 finite = 1
ENDIF

IF key$ = "left"  THEN speed = speed -0.5
IF key$ = "right" THEN speed = speed +0.5
IF key$ = "up"    THEN yc= yc+5
IF key$ = "down"  THEN yc= yc-5
RETURN


!'=============================================
Checkcollision:
GR.GET.PIXEL  xc-2, yc-2, aa, rr, gg, bb
IF gg = 128 THEN goto Crash

GR.GET.PIXEL xc+24, yc-2, aa, rr, gg, bb
IF gg = 128 THEN goto Crash
RETURN


!'=============================================
Crash:
speed = 0
GR.COLOR 255, 255, 0, 0, 1
GR.TEXT.SIZE 30
GR.TEXT.DRAW txt1, xc-30, yc, "CRASH!!!"
GR.RENDER
PAUSE 2000
GOTO @EXIT


!'=============================================
Loop:
DO
 GOSUB Checkcollision
 GOSUB Chpathmove
 GOSUB Chopmove

 key$ = "" %'***Reset joystick value

 IF xb <= -1200 & level=1 THEN
  GOSUB LevelTwo
 ELSEIF xb <= -1200 & level=2 THEN
  GOSUB LevelThree
 ELSEIF xb <= -1200 & level=3 THEN
  GOTO @YouWon
 ENDIF

 GR.RENDER
UNTIL finite = 1
GOTO @Exit


!'=============================================
!'***Labels Level
LevelOne:
level=1
GR.HIDE chpath
GR.HIDE chop1
GR.HIDE text1
GR.TEXT.SIZE 40 %
GR.COLOR 255,0,255,0,1
GR.TEXT.DRAW text1,80,35,"Level One"
!'***
GR.BITMAP.LOAD  png1,  path$+"chpath1.png"
GR.BITMAP.DRAW  chpath, png1, xb, yb
GOTO @Start
RETURN

LevelTwo:
level=2
GR.HIDE chpath
GR.HIDE chop1
GR.HIDE text1
GR.TEXT.SIZE 40 %
GR.COLOR 255,0,255,0,1
GR.TEXT.DRAW text1,80,35,"Level Two"
!'***
GR.BITMAP.LOAD  png2,  path$+"chpath2.png"
GR.BITMAP.DRAW  chpath, png2, xb, yb
GOTO @Start
RETURN

LevelThree:
level=3
GR.HIDE chpath
GR.HIDE chop1
GR.HIDE text1
GR.TEXT.SIZE 40 %
GR.COLOR 255,0,255,0,1
GR.TEXT.DRAW text1,80,35,"Level Three"
!'***
GR.BITMAP.LOAD  png3,  path$+"chpath3.png"
GR.BITMAP.DRAW  chpath, png3, xb, yb
GOTO @Start
RETURN


!'=============================================
!'***Winner
@YouWon:
GR.COLOR 255,90,90,90,1
GR.RECT winner,390,130,750,250
GR.TEXT.SIZE 60
GR.COLOR 255,255,0,0,1
GR.TEXT.DRAW text2,425,205,"You Won"
GR.RENDER
PAUSE 2000


!'=============================================
!ONERROR:
@EXIT:
GR.COLOR 255,90,90,90,1
GR.RECT ender,390,130,750,250
GR.TEXT.SIZE 60
GR.COLOR 255,255,0,0,1
GR.TEXT.DRAW text3,425,205,"Game Over"
!GR.TEXT.DRAW text3,425,205,"You are a Best"
GR.RENDER
PAUSE 3000

GR.CLOSE
!PRINT "Program Finished..."
END
