REM Translate from smallbasic.
!'==================

pi = ATAN(1)*4
t0 = 0
tmax = 12 * pi
r = 0.25
n = 0.25
st = 1/10  % 1/128

!'==================
GR.OPEN 255,0,0,0,0,1
GR.ORIENTATION 1
PAUSE 1000
GR.SCREEN w,h
sx=w/800
sy=h/1280
GR.SCALE sx,sy


DO
 GR.CLS
 fill=1 % 1 is fill
 !GR.COLOR 255,0,0,255,0 %blue
 !‘White Color
 GR.COLOR 255,255,255,255,fill

 GR.TEXT.SIZE 35
 GR.TEXT.DRAW t1,110,600,"Press Back key to stop"
 GR.TEXT.DRAW t2,110,650,"LISSAGEOUS FIGURES"
 GR.TEXT.DRAW t3,110,700,"Plot of SIN(x) vs SIN(nx)"
 GR.TEXT.DRAW t4,110,750,"Frequency ratio is " + STR$(r)

 !'=================
 GR.LINE ln,100,100,500,100   %Linea Horiz. Superior
 GR.LINE ln,100,100,100,500   %Linea Verti. Izquierda
 GR.LINE ln,500,100,500,500   %Linea Verti. Derecha
 GR.LINE ln,100,500,500,500   %Linea Horiz. Inferrior

 !'=================
 !'Green Color
 GR.COLOR 255,0,255,0,fill
 t = t0
 GOSUB formula
 FOR t = t0 TO tmax STEP st
  GOSUB formula
  GR.CIRCLE cir,(x*900*sy/5)+300,(y*900*sy/5)+300,2
 NEXT
 TONE 300,500
 r = r + n

UNTIL 0
GR.CLOSE
END


!'=================
formula:
 x=SIN(t)
 y=SIN(t*r)
 GR.RENDER
 RETURN

