knum = 1000

Print "Calculate Capacitor Values"
PRINT ""
PRINT "1 Nomenclatura"
PRINT "2 MicroFaradio"
PRINT "3 NanoFaradio"
PRINT "4 PicoFaradio"
PRINT "5 Salir"
Print " "
Print "Pause 3sec, wait"
pause 3000

INPUT "Choose Number: ", c
If c = 1 then Goto Nomenclatura
If c = 2 then Goto MicroFaradio
If c = 3 then Goto NanoFaradio
If c = 4 then Goto PicoFaradio
If c = 5 then Goto Fin
If c > 5 then Goto Menu

Rem ================
Nomenclatura:
cls
var1$=" "

Print "The value of a capacitor from the 3 digit code printed on its side." 
Input "Capacitor Number Code: ", key1$

num1$ = Left$(key1$,2)
num2$ = Right$(key1$,1)
num3 = Val(Right$(key1$,1))
var1$ = num1$

FOR i = 1 TO num3 
     var1$ = var1$ + "0"
NEXT

capval = val(var1$)
Pico  = capval
!'Pico  Value
Nano  = Pico/knum
!'Nano  Value
Micro = Nano/knum
!'Micro Value
Mili  = Micro/knum
!'Mili  Value

Print "Cap in  miliF: ";Mili
Print "Cap in microF: ";Micro
Print "Cap in  nanoF: ";Nano
Print "Cap in  picoF: ";Pico

Print "Pause 4seg, wait"
pause 4000
cls
Goto Menu

Rem =======================
MicroFaradio:

CLS
Print "MicroFaradio"
Input "Capacitor uF Value: ", key1

Micro=key1

Mili  = Micro/knum
Micro = Micro
Nano  = Micro*knum
Pico  = Micro*knum^2


Print "Cap in  miliF: ";Mili
Print "Cap in microF: ";Micro
Print "Cap in  nanoF: ";Nano
Print "Cap in  picoF: ";Pico

Print "Pause 3seg, wait"
pause 3000
cls
Goto Menu

Rem======================
NanoFaradio:
cls
Print "NanoFaradio"
Input "Capacitor nF Value: ",key1

Nano=key1

Mili  = Nano/knum^2
!'Mili  Value
Micro = Nano/knum
!'Micro Value
Nano  = Nano
!'Nano  Value
Pico  = Nano*knum
!'Pico  Value

Print "Cap in  miliF: ";Mili
Print "Cap in microF: ";Micro
Print "Cap in  nanoF: ";Nano
Print "Cap in  picoF: ";Pico

Print "Pause 3seg, wait"
pause 3000
cls
Goto Menu

Rem===================
PicoFaradio:
cls
Print "PicoFaradio"
Input "Capacitor pF Value: ",key1
Print ""
Pico=key1
Mili  = Pico/knum^3
!'Mili  Value
Micro = Pico/knum^2
!'Micro Value
Nano  = Pico/knum
!'Nano  Value
Pico  = Pico
!'Pico  Value

Print "Cap in  miliF: ";Mili
Print "Cap in microF: ";Micro
Print "Cap in  nanoF: ";Nano
Print "Cap in  picoF: ";Pico

Print "Pause 3seg, wait"
pause 3000
cls
Goto Menu

Fin:
cls
end