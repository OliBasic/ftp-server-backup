!'''
PRINT  "Data Format (grd.mm.ss)"
PRINT  " 8?.17'.11''  use  8.1711"

INPUT  "    First Latitud:", D1
INPUT  "First Longitud:", D2
INPUT  "   Second Latitud:", D3
INPUT  "Second Longitud:", D4


!'''Converted to radians
a = toradians(D1)
b = toradians(D2)
c = toradians(D3)
d = toradians(D4)   

dist = acos(cos(a) * cos(b) * cos(c) * cos(d) + cos(a) * sin(b) * cos(c) * sin(d) + sin(a) *sin(c)) * 3963.1

!'''Or You can use this. It's more small
!dist = 3963.1 * acos(sin(a) * sin(c) + cos(a) * cos(c) * cos(b-d))

!'''
PRINT "DISTANCE Mil:"; dist
PRINT "DISTANCE Km:" ;dist/1.609344

end
