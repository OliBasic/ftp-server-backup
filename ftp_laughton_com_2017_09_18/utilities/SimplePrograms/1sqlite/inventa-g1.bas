REM inventa-g1.bas
REM  15/03/2013

!'''******************************************
!'''Text mode
!'''******************************************

!'''Vibrate when touch exit button
a=100
ARRAY.LOAD Pattern[], 1~
a, a

!'''Menu main
ARRAY.LOAD menu$[], "Initialize File (  *Obligatory* )", "Create a new Entry", "Display Inventory for one Part", "Update to Stock", "Remove from Database", "Display all Item Inventory"~
"Exit"

DO
 msg$ ="Select an Option"
 SELECT menu, menu$[], msg$

 IF (menu = 0) | (menu = 7)
  GOTO prgEnd
 ELSE
  IF menu = 1
   GOSUB GS1
  ELSE
   IF menu = 2
    GOSUB GS2
   ELSE
    IF menu = 3
     GOSUB GS3
    ELSE
     IF menu = 4
      GOSUB GS4
     ELSE
      IF menu = 5
       GOSUB GS5
      ELSE
       GOSUB GS6
      ENDIF
     ENDIF
    ENDIF
   ENDIF
  ENDIF
 ENDIF
 prgEnd:
UNTIL menu = 0 | menu = 7


SQL.CLOSE DB_Ptr

!GR.CLOSE

END %'''Menu main


!'''******************************************
!'''Sub Rutines
!'''******************************************

!'''------------------------------------------
GS1: %'Open Database and Create Table
INPUT "Database to Open", dbname$, "inventory.db"

SQL.OPEN DB_Ptr, dbname$

!'''Create a Table in this new DataBase
tbname$ = "invent01"
c1$ = "Code"
c2$ = "Description"
c3$ = "Quanty"
c4$ = "Price"

POPUP " .DB Initialized and Open. Table Created ",0,-50,2
RETURN

!'''------------------------------------------
GS2: %'New entry to inventory
INPUT "Put new Code", cd$
INPUT "Put new Description", dn$
INPUT "Put new Quantity", qt$
INPUT "Put new Price", pr$

SQL.INSERT DB_Ptr, tbname$, c1$,cd$,  c2$,dn$, c3$,qt$, c4$,pr$

!POPUP " Worked Well ",0,-160,0
RETURN

!'''------------------------------------------
GS3: %'Inventory view an Item

Columns$ = c1$ + "," + c2$ + "," + c3$ + "," + c4$
INPUT "Now put Code View", cd$
Where$ = "Code = '" + cd$ +"'"
SQL.QUERY Cursor, DB_Ptr, tbname$,  Columns$, Where$

!'''Now go display the result
title$ = "Inventory an Item "
GOSUB GSgraph
RETURN

!'''------------------------------------------
GS4: %'Update Qantity and Price Stock.

INPUT "Now put Code to Update", cd$
Where$ = "Code = '" + cd$ +"'"

INPUT "Put Quantity", qt$
SQL.UPDATE DB_Ptr, tbname$, c3$, qt$: Where$

INPUT "Put Price", pr$
SQL.UPDATE DB_Ptr, tbname$, c4$, pr$: Where$
RETURN

!'''------------------------------------------
GS5: %'Remove from the database.

INPUT "Now put Code Remove", cd$

Where$ = "Code = '" + cd$ +"'"
SQL.DELETE DB_ptr, tbname$, Where$
RETURN

!'''------------------------------------------
GS6: %'View all Items Inventory

Columns$ = c1$ + "," + c2$ + "," + c3$ + "," + c4$

SQL.QUERY Cursor, DB_Ptr, tbname$, Columns$

title$ = "Inventario Look"
GOSUB GSgraph
RETURN


!'''******************************************
!'''Graphic mode
!'''******************************************
GSgraph:
GR.OPEN 255,0,0,0,0
GR.ORIENTATION 1
GR.RENDER
GR.SCREEN w, h
GR.RENDER
IF w>h THEN % exchange values
 z = w
 w = h
 h = z
ENDIF
IF w = 752 THEN w = 800
scalex = w / 800
scaley = h / 1280

!'****Draw All Components****
GR.TEXT.SIZE 40*scalex
!GR.COLOR 255,0,0,255,1 %azul
!GR.RECT rectObj, 0*scalex, 0*scaley, 799*scalex, 1279*scaley
GR.COLOR 255,255,255,255,0
GR.TEXT.DRAW txp, 5*scalex, 1225*scaley,"Inventory rfo-Basic!"

!'**** Text of title$ ****
GR.TEXT.SIZE 80*scalex
GR.COLOR 255,255,255,255,1

GR.TEXT.DRAW ta, 20*scalex,145*scaley, "    " + title$

GR.TEXT.SIZE 30*scalex
GR.TEXT.DRAW tb, 20*scalex,185*scaley, "Code | Description                                | Quanty | Price       |"
!PRINT "-------------------------------------------"

xdone = 0
item = 0
DO
 SQL.NEXT xdone,cursor,v1$,v2$,v3$,v4$
 item ++

 IF !xdone THEN
  GR.TEXT.DRAW t1,20*scalex,(185+40*item)*scaley,  v1$

  GR.TEXT.DRAW t2,135*scalex,(185+40*item)*scaley,  v2$

  GR.TEXT.DRAW t3,500*scalex,(185+40*item)*scaley,  v3$

  GR.TEXT.DRAW t4,630*scalex,(185+40*item)*scaley,  v4$

 ENDIF
UNTIL xdone


!''' Text button Return
!GR.TEXT.SIZE 40*scalex
GR.TEXT.BOLD 1
GR.TEXT.DRAW t12,630*scalex,1210*scaley, "Return"
GR.TEXT.BOLD 0


!''' Draw Button Return
GR.COLOR 255,255,255,255,0
GR.RECT rob12,600*scalex,1180*scaley,750*scalex, 1220*scaley %exit


GR.COLOR 255,255,255,255,1
GR.RENDER
!'*** Draw all components Finished ***


!'''------------------------------------------
!'''Control exit button
!'****wait for user press****
touchme:
DO % check
 touched = -1
 GR.TOUCH touched, x, y
 PAUSE 50
UNTIL touched>0


!'***Exit
IF x<750*scalex & y<1220*scaley & x>600*scalex & y>1140*scaley THEN
 !''' exit
 VIBRATE Pattern[], -1
 PAUSE 200
 GOTO toexit
 GR.RENDER
ENDIF


!'''--------
goto touchme
RETURN
!'******* end of main program *********


!'''------------------------------------------
toexit:
!SQL.CLOSE DB_Ptr
GR.CLOSE
RETURN

