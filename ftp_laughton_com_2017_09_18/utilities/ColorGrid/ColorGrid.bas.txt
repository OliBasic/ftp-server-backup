ColorGrid.bas
by
SirDonzer (donzerme@yahoo.com)

This is a handy utility to compare colors.  It is programmed with 20 common colors, but many more can be added - just edit the color string at the top of the program.  

  By the way, it uses my new Text Display v10.  It handles multiple text "screens" at the same time.  Each "screen" can be a different text resolution, and color!  I will upload it as well.