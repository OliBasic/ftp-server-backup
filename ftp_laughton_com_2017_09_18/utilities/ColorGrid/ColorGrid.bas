!Program Name: ColorGrid.bas
!By: Donald Hosford
!Purpose: A usefull tool to show how different colors look overlapping each other.
!----------------------------
!DisplayNC -- number of colors in GCOL/OpenGR. If the number of colors are changed, this must be changed also.
DisplayNC=20
!----------------------------
!Colors used: (Edit these strings. The groups of numbers must be separated by spaces.  Any number of colors may be used.)

 C1$="000000 ff0000 00ff00 0000ff ffff00 00ffff ff00ff 999999 ffffff 000099 "
 C2$="009900 009999 990000 990099 999900 993333 CCCCCC FFCC99 FF9900 FFCC00 "
 
 ColorString$=c1$+c2$

!!
Twenty different common colors.
web safe color level numbers: (hex) 00 33 66 99 CC FF

(hex)    Color    number
000000 - Black    - 1
000099 - navy     -10
0000ff - blue     - 4
009900 - green    -11
009999 - teal     -12
00ff00 - lime     - 3
00ffff - cyan     - 6
990000 - maroon   -13
990099 - purple   -14
999900 - olive    -15
999999 - grey     - 8
993333 - Brown    -16
CCCCCC - silver   -17
FFCC99 - Tan      -18
ff0000 - red      - 2
ff00ff - magenta  - 7
ff9900 - orange   -19
ffCC00 - Gold     -20
ffff00 - yellow   - 5
ffffff - white    - 9
!!

!load functions
FN.DEF gcol(Alpha,color,fill,ColorString$)
 cp=color*7-6
 c$=MID$(colorstring$,cp,7)
 GR.COLOR Alpha, Hex(Mid$(c$,1,2)), Hex(Mid$(C$,3,2)), Hex(Mid$(C$,5,2)), Fill
FN.END
 !Written by:
 !  Donald Hosford (SirDonzer@Yahoo.com)
 !
 !The credits:
 !  Roy -- His Excellent Idea to simplify the colors! Why didn't I think of it!
 !  Mougino -- Nice idea to reduce the color codes to a short function.
 !  Me -- I added 11 more "web safe" colors.  Added all the colors in a string.
 !
 !Input:
 !  Alpha -- how solid the colors.  0 - transparent, 255 - solid.
 !  Color -- the desired color (0 to 19).
 !  Fill -- 0 - outline, 1 - filled it.
 !
 !Source:
 !  This is from a couple of ideas by Roy and Mougino on the rfo.basic.forum.com.
 !  Hex codes for twenty common colors, spaces are just separators.
 !
 !The Colors: (his first)
 !  Black, Red, Lime, Blue, Yellow, Cyan, Magenta, Grey, White
 !  Navy, Green, Teal, Maroon, Purple, Olive, Brown, Silver
 !  Tan, Orange, Gold

FN.DEF OpenGR(Alpha,color,Status,Orientation,ColorString$)
 cp=color*7-6
 c$=MID$(colorstring$,cp,7)
 GR.Open Alpha, Hex(Mid$(c$,1,2)), Hex(Mid$(C$,3,2)), Hex(Mid$(C$,5,2)), Status, Orientation
 pause 1000
FN.END

FN.DEF NumberClip$(Number,Number$)
!take the number, clip off the decimals.
Number$=str$(number)
L=len(number$)
P=IS_IN(".",number$)
Number$=Left$(number$,P-1)
If Number<10 then number$=" "+number$
FN.RTN Number$
FN.END

!----------------------------
!----------------------------
!Program 
!----------------------------
!setup graphics
! Set these to suit your project.
!Current screen
CurScreen=0

!change this to the number of screens you need.
DisplayMaxScreens=3

! screen array
Dim DisplaySc[DisplayMaxScreens,11]

!Load screen data
!Set these once
DisplayORI=1
DISPlayBoarder=6
!Fancy gr.render toggle, leave at 1.
DisplayRender=1

!Setup each screen here.
!NumType is the number of that size you want.

!Screen Type 1
NumType=1
DisplayMX=4
DisplayMY=1
DisplayBackground=20
DisplayStatus=0
gosub Loadscreen

!Screen Type 2
NumType=1
DisplayMX=(DisplayNC*2)+4
DisplayMY=DisplayNC+3
DisplayBackground=3
DisplayStatus=0
gosub Loadscreen

!Screen Type 3
NumType=1
DisplayMX=30
DisplayMY=15
DisplayBackground=20
DisplayStatus=0
gosub Loadscreen
!Store number of screens
DisplayMaxScreens=CurScreen

!----------------------------
gosub displaysetup
!Gosub KeyboardSetup
!----------------------------
!MAIN Program
!----------------------------

!hide unneeded screens.
gr.hide DisplaySc[1,11]
gr.render

!draw screen 3 -- loading screen
curscreen=3
gosub boarders
!Fill Splash screen.
TCX=10:TCY=2:cf=1:cb=3:txt$="Color Grid!"
gosub write
TCX=14:TCY=3:cf=1:cb=20:txt$="by"
gosub write
TCX=11:TCY=4:cf=1:cb=3:txt$="SirDonzer"
gosub write
tcx=7:tcy=5:cf=1:cb=3:txt$="Donzerme@Yahoo.com"
gosub write
TCX=3:TCY=6:cf=1:cb=20:txt$="Just a handy way to see how"
gosub write
TCX=2:TCY=7:cf=1:cb=20:txt$="a bunch of colors compare."
gosub write
TCX=2:TCY=8:cf=1:cb=20:txt$="Color names are listed at"
gosub write
tcx=2:tcy=9:cf=1:cb=20:txt$="the top of the program."
gosub write
tcx=2:tcy=10:cf=1:cb=20:txt$="Numbers across the top are"
gosub write
tcx=2:tcy=11:cf=1:cb=20:txt$="foreground, down the side"
gosub write
tcx=2:tcy=12:cf=1:cb=20:txt$="are background."
gosub write
TCX=9:TCY=14:cf=1:cb=20:txt$="Loading..."
gosub write
!t=DisplaySc[CurScreen,1]-12:t2=ceil(t/2):TCX=t2:TCY=DisplaySc[CurScreen,2]:CF=1:CB=3
!TXT$=chr$(9571)+"Touch"+chr$(9568)
!gosub gettouch
gr.render

!draw screen 2 -- color grid
CurScreen=2
gosub boarders
!program Title
t=DisplaySc[CurScreen,1]-10
t2=ceil(t/2)
TCX=t2
TCY=1
cf=1
cb=3
TXT$=chr$(9571)+"Color Grid"+chr$(9568)
GOSUB write
gosub colorgrid
gr.hide DisplaySc[3,11]
gr.render
!touch to end
t=DisplaySc[CurScreen,1]-12:t2=ceil(t/2):TCX=t2:TCY=DisplaySc[CurScreen,2]:CF=1:CB=3
TXT$=chr$(9571)+"Touch to End"+chr$(9568)
gosub write
gosub gettouch
Gr.hide DisplaySc[2,11]
gr.render

!draw screen 1 -- Exit screen
CurScreen=1
gr.show DisplaySc[1,11]
Gr.Text.Size DisplaySc[CurScreen,7]
gr.render
tcx=1
tcy=1
cf=1
cb=3
txt$="BYE!"
gosub write
pause 1000

END
!Any program data must appear AFTER this include statement
!No includes.  Everything is loaded.
!----------------------------
!Subroutines from here on.
!----------------------------
boarders:
Gr.Text.Size DisplaySc[CurScreen,7]
!draw a fancy boarder around screen.
y=DisplaySc[CurScreen,1]-2
b$=""
for x=1 to y
B$=B$+chr$(9552)
next
!set boarder colors
cf=1
cb=20
!Put up boarders
TCX=1:TCY=1:TXT$=chr$(9556)+b$+chr$(9559)+chr$(9553)
GOSUB write
For y = 2 to DisplaySc[CurScreen,2]-1
 TCX=DisplaySc[CurScreen,1]: TCY=Y: TXT$=chr$(9553)+Chr$(9553)
 goSUB write
next
TCX=1
TCY=DisplaySc[CurScreen,2]
TXT$=chr$(9562)+b$+chr$(9565)
GOSUB Write 
return
!----------------------------
colorgrid:

!add lastval, curval, percent fraction, 
w1=displayNC*DisplayNC
PerFrac=100/w1
CurVal=0
LastVal=0
icurval$=""
curscreen=2
Gr.Text.Size DisplaySc[CurScreen,7]
for x=1 to DisplayNC
 tcx=x*2+2
 tcy=2
 cf=1
 cb=3
 numberclip$(x,&txt$)
 gosub write
next
for y=1 to DisplayNC
 tcx=2
 tcy=y+2
 cf=1
 cb=3
 numberclip$(y,&txt$)
 gosub write
 for x=1 to DisplayNC
  tcx=x*2+2
  tcy=y+2
  cf=x
  cb=y
  txt$="Cg"
  !txt$=chr$(9562)+chr$(9559)
  gosub write
  CurVal=CurVal+PerFrac
  ICurVal=int(CurVal)
  if ICurVal>LastVal
   numberclip$(icurval,&icurval$)
   !switch to screen 1, print icurval(TCX=12:TCY=9), switch back to screen 2
   !screen switches -- do without hiding other screen.  screen 1 stays up until screen 2 is done.
   curscreen=3
   Gr.Text.Size DisplaySc[CurScreen,7]
   tcx=19:TCY=14:cf=1:cb=20:txt$=icurval$+"%":gosub write
   gr.render
   curscreen=2
   Gr.Text.Size DisplaySc[CurScreen,7]
   LastVal=ICurVal
  endif
 next
next
gr.render
return
!----------------------------
formgrid:
return
!----------------------------
gettouch:
 !Start of the touch loop.
 !Detects single touches only.
 !Returns TX1,TY1,TX2,TY2 - coordinates of touch and release.
 !loop until the screen is touched
 touch = 0
 DO
  GR.TOUCH touch,tx1,ty1
 UNTIL touch = 1
 !loop until screen is not touched
 DO
  GR.TOUCH touch,tx2,ty2
 UNTIL touch = 0
 RETURN
!----------------------------
! IncDisplay10.bas
!----------------------------
!Purpose: Do an include file with just the new text display in it.
!DisplayORI=0
!DISPlayBoarder=10
!DisplayRender=1

!DisplaySc[] format:
!Screen Type 2
!NumType=1
! DisplayMX=DisplaySc[X,1]
! DisplayMY=DisplaySc[X,2]
! DisplayBackground=DisplaySc[X,3]
! DisplayStatus=DisplaySc[X,4]
! DisplaySc[X,5]=Rowhigh
! DisplaySc[X,6]=ColumnWide
! DisplaySc[X,7}=DisplayTSZ
! DisplaySc[X,8]=DisplayOffY
! DisplaySc[X,9]=DisplayOffX
! DisplaySc[X,10]=ScreenPTR
! DisplaySc[X,11]=GON

!DisplayTW   - Text Width (Pixels)

LoadScreen:
!loads screen data into DisplaySc[].
for x = 1 to NumType
 CurScreen=CurScreen+1
 DisplaySc[CurScreen,1]=DisplayMX
 DisplaySc[CurScreen,2]=DisplayMY
 DisplaySc[CurScreen,3]=DisplayBackground
 DisplaySc[CurScreen,4]=DisplayStatus
next
Return

DisplaySetup:
!setup graphics
!New setup subroutine auto figures settings.
!Set MX, MY, and ORI.  Gosub DisplaySetup
!DisplayMY   - Number of Displayed Rows.
!DisplayMX   - Number of Displayed Columns.
!DisplayORI  - Screen orientation. Portrait = 1, Landscape = 0
!DisplayBackground - Set ColorBackground to color number from GCOL. 
!DisplayBoarder - Set to color number from GCOL.
!DisplayStatus - Show/Hide status bar. 0 = Hide, 1 = Show.
!DisplayRender - Set 0 to NOT gr.render, 1 to gr.render every time.
!-----
!Setup figures TSZ, TW, OFFX and OFFY.
!DisplayTSZ  - Text Size (Pixels)
!DisplayTW   - Text Width (Pixels)
!DisplayOFFX - Offset X (Pixels)
!DisplayOFFY - Offset Y (Pixels)
!-----
call OpenGR(255,DisplayBoarder,DisplaySc[CurScreen,4],DisplayORI,ColorString$)
GR.SCREEN DisplayMGX,DisplayMGY
If DisplaySc[CurScreen,4]=1
 gr.statusbar DisplayStatusHight
else
 DisplayStatusHight=0
endif

!Start multi-screen loop
for x = 1 to DisplayMaxScreens
curscreen=x
!load screen variables from array here.
! DisplaySc[X,1]=DisplaySc[X,1]
! DisplayMY=DisplaySc[X,2]
! DisplayBackground=DisplaySc[X,3]
! DisplayStatus=DisplaySc[X,4]

!subtract hight of the status bar if present.
DisplayMGY=DisplayMGY-DisplayStatusHight
DisplaySc[CurScreen,5]=floor(DisplayMGY/DisplaySc[CurScreen,2])
DisplayMGY=DisplayMGY+DisplayStatusHight
DisplaySc[CurScreen,6]=Floor(DisplayMGX/DisplaySc[CurScreen,1])
!figure DisplayTSZ
DisplaySc[CurScreen,7]=DisplaySc[CurScreen,5]
gr.text.typeface 2
Do
 A14=0
 gr.text.size DisplaySc[CurScreen,7]
 GR.TEXT.WIDTH DisplayTW, "A"
 gr.text.height A08,A09,A10
 A09=floor(A09)
 A11=ceil(A10)
 A12=floor(A09)
 TextTR=A11-A12
 IF TextTR>DisplaySc[CurScreen,5]
  A14=A14+1
 Endif
if DisplayTW>DisplaySc[CurScreen,6]
 A14=A14+1
endif
If A14>0
 DisplaySc[CurScreen,7]=DisplaySc[CurScreen,7]-1
Endif
until A14=0
!fix the size of RowHigh and ColumnWide to the size of the text.
if DisplaySc[CurScreen,5]>TextTR then DisplaySc[CurScreen,5]=TextTR
if DisplaySc[CurScreen,6]>DisplayTW then DisplaySc[CurScreen,6]=DisplayTW
!calculate offsets.
A04=DisplayMGY-Displaystatushight
A05=DisplaySc[CurScreen,5]*DisplaySc[CurScreen,2]
A06=A04-A05
A07=floor(A06/2)
DisplaySc[CurScreen,8]=A07+Displaystatushight
DisplaySc[CurScreen,9]=floor((DisplayMGX-(DisplaySc[CurScreen,6] * DisplaySc[CurScreen,1]))/2)
!The RowHigh and ColumnWide are the size of the background graphic boxes. 
!TR is the total hight of Text in pixels.
!----
!set background color, create a screen sized bitmap, and show it.
gcol(255,Displayboarder,1,ColorString$)
!calculate bmp size here.
A48=DisplaySc[CurScreen,5] * DisplaySc[CurScreen,2]
A49=DisplaySc[CurScreen,6] * DisplaySc[CurScreen,1]
GR.BITMAP.CREATE DisplaySc[CurScreen,10], A49,A48
!GR.BITMAP.CREATE DisplaySc[CurScreen,10],DisplayMGX,DisplayMGY
!use offsets to center bmp.
!must set color for bitmap.
!Gr.bitmap.drawinto.start points the graphic tools at the bitmap "screenptr" 
!must start and end before/after each "Printing/drawing".
GR.BITMAP.DRAWINTO.START DisplaySc[CurScreen,10]
!use ordinary graphic commands to draw stuff on bitmap.
!GR.TEXT.DRAW UOP,1,17,"Loading..."
GR.BITMAP.DRAWINTO.END
Gosub Clear
!this next command adds the bitmap to the display list.
!only need to do this once.
GR.BITMAP.DRAW DisplaySc[CurScreen,11],DisplaySc[CurScreen,10],1,1
!Centers the bmpimage.
gr.modify DisplaySc[CurScreen,10],"x",DisplaySc[CurScreen,9]
gr.modify DisplaySc[CurScreen,10],"y",DisplaySc[CurScreen,8]
!then gr.render to make it appear.
GR.RENDER
!!
!Store variables into array here.
 DisplaySc[X,5]=Rowhigh
 DisplaySc[X,6]=ColumnWide
 DisplaySc[X,7]=DisplaySc[CurScreen,7]
 DisplaySc[X,8]=DisplayOffY
 DisplaySc[X,9]=DisplayOffX
 DisplaySc[X,10]=ScreenPTR
 DisplaySc[X,11]=GON
 !!
!end multi-screen loop.
next

!----
!get color pointers
! array to hold paint color pointers
DIM PC[20]
A15=0
A16=20
FOR A01 = 1 TO 20
 A15=A15+15
 CALL gcol(255,A01,1,ColorString$)
 a03$=CHR$(64+A01)
 GR.TEXT.DRAW a02,A15,A16,A03$
 GR.PAINT.GET pc[A01]
 GR.HIDE a02
NEXT
GR.RENDER
Return
!----------------------------
Write:
!set TCX,TCY,TXT$,CF,CB,DisplayRender
!Start write operation
GR.BITMAP.DRAWINTO.START DisplaySc[CurScreen,10]
A20=tcx
A21=tcy
A22=LEN(TXT$)
If A22>DisplaySc[CurScreen,1]
  A23=DisplaySc[CurScreen,1]
 else
  if (A22+A20-1)>DisplaySc[CurScreen,1]
   A23=(A22+A20-1)-DisplaySc[CurScreen,1]
  else
   A23=A22
  endif
endif
A24=1
A25=0
!calculate TCX,TCY in pixels
!text char position in pixels
!both corners of the box, in pixels.

!Do write operation
Do
 !start Loop
 A26$=MID$(TXT$,A24,A23)
 A27=len(A26$)

 !Draw Box
! A28=(((A20-1))*DisplaySc[CurScreen,6])+DisplaySc[CurScreen,9]
! A29=((A21-1)*DisplaySc[CurScreen,5])+DisplaySc[CurScreen,8]

 A28=(((A20-1))*DisplaySc[CurScreen,6])
 A29=((A21-1)*DisplaySc[CurScreen,5])

 A30=A28+(DisplaySc[CurScreen,6] * A27)
 A31=A29+DisplaySc[CurScreen,5]
 CALL gcol(255,CB,1,ColorString$)
 GR.RECT A32,A28,A29,A30,A31
 
 !Draw Character
! A33=((A20-1)*DisplaySc[CurScreen,6])+DisplaySc[CurScreen,9]
! A34=((A21-1)*DisplaySc[CurScreen,5])+DisplaySc[CurScreen,7]+DisplaySc[CurScreen,8]

 A33=((A20-1)*DisplaySc[CurScreen,6])
 A34=((A21-1)*DisplaySc[CurScreen,5])+DisplaySc[CurScreen,7]
 

 CALL gcol(255,CF,1,ColorString$)
 GR.TEXT.DRAW A32,A33,A34,A26$
 
!update cursor position 
 A20=A20+A27
 IF A20>DisplaySc[CurScreen,1]
  A20=1
  A21=A21+1
  if A21>DisplaySc[CurScreen,2]
   A21=1
  endif
 ENDIF

 !End Loop
 If A23<DisplaySc[CurScreen,1] then A23=DisplaySc[CurScreen,1]
 IF A24<A22
  A25=1
 else
  A25=0
 endif
 A24=A24+A27
until A25=0

!stop write operation
GR.BITMAP.DRAWINTO.END
IF DisplayRender=1
 GR.RENDER
endif
TCX=A20
TCY=A21
RETURN

!----------------------------
Clear:
!Clears the screen.(Paints entire screen the background color.)
!Uses ClearABox to do the work.
TCX=1
TCY=1
Width=DisplaySc[CurScreen,1]
Hight=DisplaySc[CurScreen,2]
!----------------------------
ClearABox:
!Clears a box on the screen (in characters).
!(Paints a box in the background color.)
! Specs: (In characters)
!  TCX,TCY - coords of the upper left corner of the box
!  Width - width of box.
!  Hight - Hight of box.
A40=TCY
A41=TCY+Hight-1
A42=TCX
A43=TCX+Width-1
boxcolor=colorbackground
!Draw Box
GR.BITMAP.DRAWINTO.START DisplaySc[CurScreen,10]
!A44=(((A42-1))*DisplaySc[CurScreen,6])+DisplaySc[CurScreen,9]
!A45=((A40-1)*DisplaySc[CurScreen,5])+DisplaySc[CurScreen,8]
!A46=((A43)*DisplaySc[CurScreen,6])+DisplaySc[CurScreen,9]
!A47=((A41)*DisplaySc[CurScreen,5])+DisplaySc[CurScreen,8]

A44=(((A42-1))*DisplaySc[CurScreen,6])
A45=((A40-1)*DisplaySc[CurScreen,5])
A46=((A43)*DisplaySc[CurScreen,6])
A47=((A41)*DisplaySc[CurScreen,5])

CALL gcol(255,DisplaySc[CurScreen,3],1,ColorString$)
GR.RECT A48,A44,A45,A46,A47
GR.BITMAP.DRAWINTO.END
Return
!----------------------------
