Rem Floating Digits 
Rem Converted from Mintoris basic to RFO Basic
Rem For Android
Rem November 2016
Rem Version 1.00
Rem By Roy Shepherd

di_height = 1152 % set to my Device
di_width = 672

gr.open 255,0,0,0
gr.orientation 1 % Portrait  
pause 1000
WakeLock 3
 
gr.screen screenWidth, screenHeight
scale_x = screenWidth / di_width
scale_y = screenHeight / di_height
gr.scale scale_x, scale_y

gr.text.size 50
gr.set.stroke 4

maxSprites = 10

dim bitmap[maxSprites]
dim x[maxSprites]
dim y[maxSprites]
dim vx[maxSprites]
dim vy[maxSprites]

maxX = di_width - 100
maxY = di_height - 100

For i = 1 to maxSprites
    gosub MakeBitmap
    x[i] = floor(maxX * rnd())
    y[i] = floor(maxY * rnd())
    vx[i] = floor(20 * rnd()-10)
    vy[i] = floor(20 * rnd()-10)
Next

Do

  For i = 1 to maxSprites

    x[i] = x[i] + vx[i]
    y[i] = y[i] + vy[i]

    if x[i] < 0 then
      x[i] = 0
     vx[i] = vx[i] * -1
    endif

    if x[i] > maxX then
      x[i] = maxX
      vx[i] = vx[i] * -1
    endif

    if y[i] < 0 then
      y[i] = 0
      vy[i] = vy[i] * -1
    endif

    if y[i] > maxY then
      y[i] = maxY
      vy[i] = vy[i] * -1
    endif

    gr.modify bitmap[i], "x", x[i], "y", y[i]
    
  Next

gr.render

until 0

onBackKey:
  exit
back.resume 

MakeBitmap:
    digit = i - 1
    gr.bitmap.create bitmap, 100, 100
    gr.bitmap.drawinto.start bitmap

    gr.color 255,0, 255, 0, 1
    gr.line null, 0,0,99,99
    gr.line null, 0,99,99,0
    
    gr.color 255,255, 0, 0, 0
    gr.circle null, 50, 50, 48
    
    gr.color 255, 255, 255, 255, 1
    d$ = int$(digit)
    gr.text.draw null, 35, 65, d$
    
    gr.bitmap.drawinto.end
    gr.bitmap.draw bitmap[i], bitmap, - 100, - 100 
return
  