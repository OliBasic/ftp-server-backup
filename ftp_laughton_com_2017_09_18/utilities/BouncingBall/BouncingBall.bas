rem bouncing ball color rotation with
rem touchscreen - 15.jan.2011 LX
rem call of rnd() fixed by mougino 12.jan.2014

screenwidth=800
screenhight=480

gr.open 0,0,0,0
x=100
y=100
xx=15
yy=15
x=400
y=240
cr=100
ccr=3
cg=100
ccg=4
cb=100
ccb=2
gr.color 255,0,0,170,0
do
gr.touch touched,xt,yt
if touched<>0
x=xt
y=yt
cr=rnd()*230+10
cg=rnd()*230+10
cb=rnd()*230+10
endif
x=x+xx
y=y+yy
if x>screenwidth-40 | x<40 then xx=xx*-1
if y>screenhight-40 | y<40 then yy=yy*-1
c=c+1
if n>300
n=0
l=1
endif
cr=cr+ccr
if cr<5 | cr>250 then ccr=ccr*-1
cg=cg+ccg
if cg<5 | cg>250 then ccg=ccg*-1
cb=cb+ccb
if cb<5 | cb>250 then ccb=ccb*-1
gr.color 130,cr,cg,cb,1
gr.circle n,x,y,30
if n>150
gr.hide n-150
gr.show n
endif
gr.render
until 0
end