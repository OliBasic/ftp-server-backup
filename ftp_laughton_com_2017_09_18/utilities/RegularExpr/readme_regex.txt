
WARNING - see notes marked 
*** about output data files. 
When the programs are run, 
these outputs will change 
and/or overwrite existing 
files with the same names.

-------------------------

Files in this directory:

-------------------------

regex_examples.bas (10 KB)

24 examples of using regular 
expresssions in the Basic! 
SPLIT command.

*** Creates and overwrites 
file regex_examples.txt in 
rfo-basic/data

-----------------------

regex_notes.pdf (31 KB)

Notes and reference tables 
about using regular 
expressions in the Basic! 
SPLIT command and WORD$() 
function. It cross-references 
with regex_examples.

---------------------

regex_fns.bas (9 KB)

Various utility functions 
all using the Basic! WORD$() 
builtin function.

See the block comment at 
the start of the file for 
more information.

The file also includes code 
to try out the functions.

*** Creates and updates 
file regex_fns.txt in 
rfo-basic/data

------------------

whereis.bas (11 KB)

A longer example of using 
regular expressions.

It searches for files with 
names matching a regular 
expression.
Both the path to search and 
the regex are user inputs. 
It has been updated to use 
new features in Basic! 
version 1.84

*** Creates and updates 
file whereis.txt in 
rfo-basic/data

-----------------

Keith B
2014-11-14

