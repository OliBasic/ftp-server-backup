 !
 ! Demo of using regular expressions in SPLIT
 ! Numbers xref with regular expressions document
 !

 use_exit = 1 % comment out to finish with END instead of EXIT

 !
 ! show a string as it would be in source code
 !
 FN.DEF source$(s$) 
  t$ = REPLACE$(s$, "\\", "\\\\") 
  t$ = REPLACE$(t$, "\"", "\\\"") 
  t$ = REPLACE$(t$, CHR$(10) , "\\n") 
  t$ = REPLACE$(t$, CHR$(160) , "\" + CHR$(160) + \"") 
  t$ = REPLACE$(t$, CHR$(9) , "\\t") 
  FN.RTN t$
 FN.END 
 

 ! Replace tab characters by right arrows and
 ! replace non-breaking spaces by shaded blocks
 !
 FN.DEF showtabs$(s$) 
  t$ = REPLACE$(s$, CHR$(9) , CHR$(8594) ) 
  FN.RTN REPLACE$(t$, CHR$(160) , CHR$(9618) ) 
 FN.END 
 
 GOSUB LoadData
 GOSUB Intro
 
 !
 ! Main While Loop over the available tests
 ! (not using for-next because of option to choose test)
 !
restart:
 t = 1
 changed = 1
 WHILE t <= numtests
  IF changed THEN 
   CLS 
   Array.delete field$[]
   SPLIT field$[], record$[t], delimiter$[t]
   Array.length numfields, field$[]
   
   L$ = USING$("", "Example %1.0f of %1.0f", t, numtests) 
   Console.title L$
   
   L$ = "Source:\n SPLIT field$[], ~\n \"" + source$(record$[t]) + ~
    "\", ~\n \"" + source$(delimiter$[t]) + "\""
   PRINT L$
   
   L$ = "Input record:\n«" + showtabs$(record$[t]) + "»"
   PRINT L$
   
   L$ = "Delimiter:\n«" + showtabs$(delimiter$[t]) + "»"
   PRINT L$
   
   PRINT "Words:"; 
   
   FOR i = 1 TO numfields
    L$ = "\n" + FORMAT$("##%", i) + " «" + showtabs$(field$[i]) + "»"
    PRINT L$; 
   NEXT i
   PRINT 
   
   IF t < numtests THEN 
    PRINT "\nNext Example (Back)\n"
    PRINT "\nOther Options (Menu)\n"
   ELSE 
    PRINT "\nStart Again (Back)\n"
    PRINT "\nOther Options (Menu)\n"
   ENDIF 
  ENDIF 
  
  menupress = 0
  waitback = 1
  WHILE waitback
   PAUSE 100
   IF menupress THEN W_R.BREAK 
  REPEAT 
  
  Changed = 1
  IF menupress THEN 
   Dialog.Select choice, op$[], "What next?"
   
   IF choice = 0 THEN         % back key pressed - leave t unchanged
    Changed = 0
   ELSEIF choice = 1 THEN     % The Exit option
    t = numtests + 1
   ELSE                       % list of tests with offset of 2 to numbers
    IF choice = t + 1 THEN Changed = 0
    t = choice - 1
   ENDIF 
   menupress = 0
  ELSE 
   t = t + 1
   IF t > numtests THEN t = 1
  ENDIF 
  
  
 REPEAT 
 
 CLS 
 Console.title "Regular Expressions"
 Dialog.message "Finished", "Do you want to save the results (for all " + INT$(numtests) + " of the examples) to  a text file?", i, "Save and Exit", "Exit\n(No Save)", "Cancel"
 IF i = 3 THEN GOTO restart
 IF i = 1 THEN 
  GOSUB WriteFile
  Dialog.message , "Results written to " + fname$, i, "Exit", "View"
  IF i = 2 THEN 
   GRABFILE L$, fname$, 1
   Html.open 1, -1
   Html.load.string "<plaintext>" + L$
   POPUP "Press 'Back' to exit", 0, 0, 0
   DO 
    DO 
     Html.get.datalink hdata$
    UNTIL hdata$ <> ""
    IF STARTS_WITH("BAK:", hdata$) THEN D_U.BREAK 
   UNTIL 0
   Html.close 
  ENDIF 
 ENDIF 
 
 IF use_exit THEN EXIT 
 END
 
 
OnBackKey:
 waitback = 0
 Back.resume 
 
OnMenuKey:
 menupress = 1
 MenuKey.Resume 
 
OnConsoleTouch:
 Console.line.touched conline
 Console.line.text conline, context$
 IF IS_IN("(Back)", context$) THEN 
  waitback = 0
 ELSEIF IS_IN("(Menu)", context$) THEN 
  menupress = 1
 ENDIF 
 ConsoleTouch.Resume 
 
 
 
 !
 ! Display the initial screen
 !
Intro:
 Console.title "Regular Expressions"
 PRINT 
 PRINT " Some Examples of Regular\n"; 
 PRINT " Expressions in SPLIT"
 L$ = " Tabs are shown by " + CHR$(8594) + " and \n non-breaking spaces by " + CHR$(9618) 
 PRINT L$
 PRINT " Strings are enclosed in\n"; 
 PRINT " « » to make leading and\n"; 
 PRINT " trailing blanks evident."
 PRINT 
 PRINT " Touch here or press (Back) to start..."
 waitback = 1
 WHILE waitback
  PAUSE 100
 REPEAT 
 CLS 
 RETURN 
 
 
 !
 ! Write results of all the tests to a text file
 !
WriteFile:
  
 fname$ = "regex_examples.txt"
 CLS 
 PRINT "\n Writing results to\n  "; fname$; "\n"
 Text.open W, outfile, fname$
 TIME yr$, mo$, dy$, hr$, mi$
 Text.writeln outfile, yr$; "/"; mo$; "/"; dy$; " at "; hr$; ":"; mi$
 Text.writeln outfile, "------------------------"
 Text.writeln outfile, "Some Examples of Regular"
 Text.writeln outfile, "Expressions in SPLIT"
 Text.writeln outfile, "Tabs are shown by " + CHR$(8594) + " and"
 Text.writeln outfile, "non-breaking spaces by " + CHR$(9618) 
 Text.writeln outfile, "Strings are enclosed in"
 Text.writeln outfile, "« » to make leading and"
 Text.writeln outfile, "trailing blanks evident."
 Text.writeln outfile, "------------------------"
 Text.writeln outfile, 
 
 FOR t2 = 1 TO numtests
  Array.delete field$[]
  SPLIT field$[], record$[t2], delimiter$[t2]
  Array.length numfields, field$[]
  
  L$ = "Example " + REPLACE$(FORMAT$("####%", t2) , " ", "") + ~
    " of " + REPLACE$(FORMAT$("####%", numtests) , " ", "") 
  Text.writeln outfile, L$
  
  L$ = "Source:\n SPLIT field$[], ~\n \"" + source$(record$[t2]) + ~
    "\", ~\n \"" + source$(delimiter$[t2]) + "\""
  Text.writeln outfile, L$
  
  L$ = "Input record:\n«" + showtabs$(record$[t2]) + "»"
  Text.writeln outfile, L$
  
  L$ = "Delimiter:\n«" + showtabs$(delimiter$[t2]) + "»"
  Text.writeln outfile, L$
  
  Text.writeln outfile, "Words:"; 
  
  FOR i = 1 TO numfields
   L$ = "\n" + FORMAT$("##%", i) + " «" + showtabs$(field$[i]) + "»"
   Text.writeln outfile, L$; 
  NEXT i
  
  Text.writeln outfile, "\n\n--------------------------------------------------------------------------------"
  
 NEXT t2
 
 Text.close outfile
 
 CLS 
 RETURN 
 
 !
 ! Initialise the test data and select choices
 !
 
LoadData:
 Array.load record$[], "first.second.third" ~               %  1
    "first\\second\\third" ~                                %  2
    "first\tsecond\tthird" ~                                %  3
    "first;second,third" ~                                  %  4
    "first 10 second 200 third 300" ~                       %  5
    "first.second,third" ~                                  %  6
    "Greek from " + CHR$(945,946,947) + " via " + CHR$(955,956,957) + " to " + CHR$(967,968,969) + "." ~ % 7
    "a,b" ~                                                 %  8
    "first,    second, third,    " ~                        %  9
    "first,\tsecond, third,  " ~                            % 10
    "first,\tsecond," + CHR$(160) + " third, " ~            % 11
    "first,\tsecond," + CHR$(160) + " third, " ~            % 12
    "first; second., third" ~                               % 13
    "first:ignore:second:junk:third" ~                      % 14
    "first:ignore:second:junk:third" ~                      % 15
    "abc <first> xyz <second> p q r <third>  " ~            % 16
    "abc <first> xyz <second> p q r <third>  " ~            % 17
    "abc <first> xyz <second> p q r <third>  " ~            % 18
    "first End.  second END.  third end." ~                 % 19
    "First.  Second?  Third! " ~                            % 20
    "First.  \"Second?\"  Third! " ~                        % 21
    "First.  \"Second?\"\nThird! " ~                        % 22
    "First.  \"Second?\"\nThird! " ~                        % 23
    "<h1>The main heading</h1><p>Some plain text and <i>some in italic.</i></p>"  % 24
 
 Array.load delimiter$[], "\\." ~                           %  1
    "\\\\" ~                                                %  2
    "\\t" ~                                                 %  3
    "[,;]" ~                                                %  4
    "[^0-9]+" ~                                             %  5
    "[,.]" ~                                                %  6
    "\\P{inGreek}+" ~                                       %  7
    ",?" ~                                                  %  8
    ", *" ~                                                 %  9
    ",[ \\t]*" ~                                            % 10
    ",[ \\t\\xA0]*" ~                                       % 11
    ",[\\p{javaSpaceChar}\\p{javaWhitespace}]*" ~           % 12
    "(;|\\.,) *" ~                                          % 13
    ":.*:" ~                                                % 14
    ":.*?:" ~                                               % 15
    "^.*?<|>.*?<|>.*?$" ~                                   % 16
    "(^|>).*?($|<)" ~                                       % 17
    "(^|>)[^<]*($|<)" ~                                     % 18
    "(?i-)end\\. *" ~                                       % 19
    "(?<=[.?!]) +" ~                                        % 20
    "(?<=[.?!][\"\\p{Pf}]?) +" ~                            % 21
    "(?<=[.?!][\"\\p{Pf}]?)[\\n\\t \\xA0]+" ~               % 22
    "(?x-)  ( ?<=[.?!] [\"\\p{Pf}]? ) [\\n\\t\\ \\xA0]+" ~  % 23
    "(?<=>)|(?<=.)(?=<)"                                    % 24
 
 Array.length numtests, record$[]
 
 DIM op$[numtests + 1]
 op$[1] = " Exit"
 FOR i = 1 TO numtests
  op$[i + 1] = USING$("", " Example %02.0f", i) 
 NEXT i
 
 RETURN 
 
