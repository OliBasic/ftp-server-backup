!!
  -----------------------------------------------------------------------
  
  Functions using word$() and regular expressions
  ===============================================
  
  Some functions that use various "tricks" with word$() and regular
  expression patterns.
  
  Note that you will get quicker, but more obscure, code by putting
  the word$() calls in-line rather than using these function calls.
  
  Most of these are single-line functions and would be prime candidates
  for pre-processor macros if your development environment supports them.
  
  File includes code to get input and make test calls of the functions.
  
  
  1. Test whether a string matches a regular expression.
  ---------------------------------------------------
  
  Function: matchpat(x$,p$)
  
  Returns true (1) if x$ matches p$ and false (0) if it doesn't match.
  
  
  2. Functions that test whether strings contain valid numbers.
  ----------------------------------------------------------
  
  Function: is_hex(x$)
  Function: is_oct(x$)
  Function: is_bin(x$)
  Function: is_val(x$)
  
  These return 1 (true) if the string would be valid input to the
  corresponding builtin function, hex(), oct(), bin(), and val().
  
  
  Function: test_val$(x$)
  
  A more complex check on whether strings equate to valid floating point
  numbers. This catches some underflow and overflow cases that don't
  cause errors when used in val() but may cause other problems.
  
  Returns "ill-formed" if val(x$) will result in an error because
  it is not in the correct format or if x$ has an exponent greater
  than +399 or less than -399. These are the cases where is_val()
  returns false (0).
  
  Returns "OK" if val(x$) will be a valid finite number and would not
  be rounded to zero as a result of underflow.
  
  Returns "overflow" if val(x$) will evaluate to +/- infinity and the
  exponent part of x$ is less than 399. For example x$="-1.23E330".
  
  Returns "underflow" if val(x$) will evaluate to zero but x$ has
  non-zero digits and its exponent is greater than -399.
  For example x$="1.23e-350".
  
  
  
  3. Functions to trim unwanted characters from start/end of strings
  ---------------------------------------------------------------
  
  These remove white space or specified characters from the
  start and/or end of strings. They use word$() calls with the anchors
  ^ and $ in the regular expressions to tie them to the start or end
  of the string.
  
  Remove white space from the left or right, or both ends of x$:
  
  Function: ltrim$(x$)
  Function: rtrim$(x$)
  Function: trim$(x$)
  
  
  Remove characters in c$ from the left or right or both ends of x$:
  
  Function: ltrimc$(x$,c$)
  Function: rtrimc$(x$,c$)
  Function: trimc$(x$,c$)

  4. Functions to test whether strings are valid dates.
  -----------------------------------------------------
  
  is_dateG(x$)
  is_dateJ(x$)
  
  These return true if d$ is a date in one of these formats:
  
  yyyymmdd yyyy-mm-dd yyyy/mm/dd yyyy.mm.dd yyyy:mm:dd
  
  ... and is a valid date in the Julian or Gregorian calendars.
  
  All digits must be present, including leading zeros if necessary.
  
  
  ----------------------------------------------------------------------
!!
 
 
 !---------------------------------------------------------------------
 ! Functions start here
 !---------------------------------------------------------------------
 
 FN.DEF matchpat(x$, p$) 
  FN.RTN WORD$("a" + x$, 1, "^a(?:" + p$ + ")") = ""
 FN.END 
 
 FN.DEF is_hex(x$) 
  FN.RTN WORD$("a" + x$, 1, "^a(?:[+-]?0*[0-9A-Fa-f]{1,16})") = ""
 FN.END 
 
 FN.DEF is_oct(x$) 
  FN.RTN WORD$("a" + x$, 1, "^a(?:[+-]?0*1?[0-7]{1,21})") = ""
 FN.END 
 
 FN.DEF is_bin(x$) 
  FN.RTN WORD$("a" + x$, 1, "^a(?:[+-]?0*[01]{1,64})") = "" 
 FN.END 
 
 FN.DEF is_val(x$) 
  FN.RTN "" = WORD$("a" + x$, 1, "^a(?:[\\c@- ]*[+-]?(?:[0-9]+\\.?[0-9]*|\\.[0-9]+)(?:[eE][+-]?0*[123]?[0-9]{1,2})?[\\c@- ]*)")
 FN.END       
              
 FN.DEF test_val$(x$) 
  IF "" <> WORD$("a" + x$, 1, "^a(?:[\\c@- ]*[+-]?(?:[0-9]+\\.?[0-9]*|\\.[0-9]+)(?:[eE][+-]?0*[123]?[0-9]{1,2})?[\\c@- ]*)") THEN FN.RTN "ill-formed"
  x = VAL(x$) 
  IF ABS(x) = val("Infinity") THEN FN.RTN "overflow"
  IF x <> 0 THEN FN.RTN "OK"
  SPLIT x$[], x$, "[eE]"
  IF VAL(x$[1]) <> 0 THEN FN.RTN "underflow"
  FN.RTN "OK"
 FN.END 
 
 FN.DEF ltrim$(x$) 
  FN.RTN WORD$(x$, 1, "^\\s*") 
 FN.END 
 
 FN.DEF rtrim$(x$) 
  FN.RTN WORD$(x$, 1, "\\s*$") 
 FN.END 
 
 FN.DEF trim$(x$) 
  FN.RTN WORD$(x$, 1, "^\\s*|\\s*$") 
 FN.END 
 
 FN.DEF ltrimc$(x$, c$) 
  FN.RTN WORD$(x$, 1, "^[\\Q" + c$ + "\\E]*") 
 FN.END 
 
 FN.DEF rtrimc$(x$, c$) 
  FN.RTN WORD$(x$, 1, "[\\Q" + c$ + "\\E]*$") 
 FN.END 
 
 FN.DEF trimc$(x$, c$) 
  FN.RTN WORD$(x$, 1, "^[\\Q" + c$ + "\\E]*|[\\Q" + c$ + "\\E]*$") 
 FN.END 
 
 FN.DEF is_dateG(x$)
  FN.RTN "" = WORD$("a" + x$, 1, ~
   "^a(?!0000)(?:[0-9]{4}([-.:/ ]?)(?:" + ~
   "(?:0[13578]|1[02])\\1(?:0[1-9]|[12][0-9]|30|31)" + ~
   "|(?:0[469]|11)\\1(?:0[1-9]|[12][0-9]|30)" + ~
   "|02\\1(?:0[1-9]|1[0-9]|2[0-8])" + ~
   ")|(?:[0-9]{2}(?:[2468][048]|0[48]|[13579][26])" + ~
   "|(?:[02468][048]|[13579][26])00)" + ~
   "([-.:/ ]?)02\\229)")
 FN.END
 
 FN.DEF is_dateJ(x$)
  FN.RTN "" = WORD$("a" + x$, 1, ~
   "^a(?!0000)(?:[0-9]{4}([-.:/ ]?)(?:" + ~
   "(?:0[13578]|1[02])\\1(?:0[1-9]|[12][0-9]|30|31)" + ~
   "|(?:0[469]|11)\\1(?:0[1-9]|[12][0-9]|30)" + ~
   "|02\\1(?:0[1-9]|1[0-9]|2[0-8])" + ~
   ")|[0-9]{2}(?:[02468][048]|[13579][26])([-.:/ ]?)02\\229)")
 FN.END
 
 !----------------------------------------------------------------------
 ! Test code starts here
 !----------------------------------------------------------------------
 
 !useexit = 1  % uncomment to finish with exit rather than end
 
 Array.load m$[], "is_hex(x$)", "is_oct(x$)", "is_bin(x$)" ~
    "is_val(x$)", "test_val$(x$)" ~
    "ltrim$(x$)", "rtrim$(x$)", "trim$(x$)" ~
    "ltrimc$(x$,c$)", "rtrimc$(x$,c$)", "trimc$(x$,c$)" ~
    "matchpat(x$,p$)", "is_dateG(x$)", "is_dateJ(x$)"
 
 pu$ = "'Back' key to cancel"

 DO 
  SELECT t, m$[], "Test Which Function?", pu$
  IF t = 0 THEN D_U.BREAK 
  t$ = "Test " + m$[t]
  Console.title t$
  CLS 
  PRINT "Enter a test string or\npress 'Back' to cancel."
  backflag = 0
  DO 
   TGET x$, "x$: ", t$
   IF backflag THEN D_U.BREAK 
   IF t > 12 THEN
   
   ELSEIF t > 11 THEN 
    TGET c$, "p$: ", t$
   ELSEIF t > 8 THEN 
    TGET c$, "c$: ", t$
   ENDIF 
   
   IF backflag THEN D_U.BREAK 
   GOSUB t, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14
   l$ = REPLACE$(m$[t], "x$", "\"" + x$ + "\"") 
   IF t > 12 THEN
   
   ELSEIF t > 11 THEN 
    l$ = REPLACE$(l$, ",p$", ",\"" + replace$(c$,"\\","\\\\") + "\"") 
   ELSEIF t > 8 THEN 
    l$ = REPLACE$(l$, ",c$", ",\"" + c$ + "\"") 
   ENDIF 
   l$ = l$ + " = " + r$ + "\n"
   report$ = report$ + l$
   CLS 
   PRINT l$
   PRINT "Another test string or\npress 'Back' to finish."
  UNTIL 0
  IF report$ <> "" THEN pu$ = "'Back' key to finish and view report"
 UNTIL 0
 
 IF report$ = "" THEN GOTO finish
 
 CLS 
 Console.title "Test Results"
 PRINT report$
 PRINT "Touch here to save report and finish (or 'Menu' key)"
 PRINT "Touch here to finish without saving (or 'Back' key)"
 menuflag = 0
 backflag = 0
 DO 
  IF menuflag THEN 
   Text.open A, f, "regex_fns.txt"
   TIME y$, m$, d$, h$, mi$
   Text.writeln f, 
   Text.writeln f, y$; "-"; m$; "-"; d$; " at "; h$; ":"; mi$
   Text.writeln f, 
   Text.writeln f, report$
   Text.writeln f, "----------------------------------------"
   POPUP "report added to\nregex_fns.txt", 0, 0, 0
   Text.close f
   PAUSE 1800
   backflag = 1
  ENDIF 
  PAUSE 100
 UNTIL backflag
 
finish:
 POPUP "Finished", 0, 0, 0
 PAUSE 1800
 
 IF useexit THEN EXIT 
 CLS 
 END "Press 'Back' key"
 
 
t1:
 r = is_hex(x$) 
 r$ = INT$(r) 
 RETURN 
t2:
 r = is_oct(x$) 
 r$ = INT$(r) 
 RETURN 
t3:
 r = is_bin(x$) 
 r$ = INT$(r) 
 RETURN 
t4:
 r = is_val(x$) 
 r$ = INT$(r) 
 RETURN 
t5:
 r$ = test_val$(x$) 
 RETURN 
t6:
 r$ = "\"" + ltrim$(x$) + "\""
 RETURN 
t7:
 r$ = "\"" + rtrim$(x$) + "\""
 RETURN 
t8:
 r$ = "\"" + trim$(x$) + "\""
 RETURN 
t9:
 r$ = "\"" + ltrimc$(x$, c$) + "\""
 RETURN 
t10:
 r$ = "\"" + rtrimc$(x$, c$) + "\""
 RETURN 
t11:
 r$ = "\"" + trimc$(x$, c$) + "\""
 RETURN 
t12:
 r = matchpat(x$, c$) 
 r$ = INT$(r) 
 RETURN 
t13:
 r = is_dateG(x$)
 r$ = INT$(r)
 RETURN
t14:
 r = is_dateJ(x$)
 r$ = INT$(r)
 RETURN
 
OnBackKey:
 backflag = 1
 Back.resume 
 
OnMenuKey:
 menuflag = 1
 MenuKey.Resume 
 
OnConsoleTouch:
 Console.line.touched cl
 Console.line.text cl, cl$
 IF IS_IN("(or 'Menu' key)", cl$) THEN 
  menuflag = 1
 ELSEIF IS_IN("(or 'Back' key)", cl$) THEN 
  backflag = 1
 ENDIF 
 ConsoleTouch.Resume 
 
