 !
 ! Search for a file with a name matching a pattern
 !

 useexit = 1   % comment out to finish with END instead of EXIT
 
 !
 ! Select the directory to search
 !
 FN.DEF getpath$(title$) 
  File.root rootpath$
  SPLIT rootpath$[], rootpath$, "/"
  Array.length np, rootpath$[]
  FOR i = 2 TO np
   toroot$ = toroot$ + "../"
  NEXT i
  
  path$ = ""
  FOR i = 2 TO np - 1
   path$ = path$ + rootpath$[i] + "/"
  NEXT i
  
  fin = 0
  
  DO 
   ! Get the listing from the path directory
   Array.delete d1$[], d2$[]
   File.Dir toroot$ + path$, d1$[], "/"
   Array.length length, d1$[]
   
   ! go through file list to find where the directories end
   
   i = 0
   WHILE i < length
    IF ENDS_WITH("/", d1$[i + 1]) = 0 THEN W_R.BREAK 
    i = i + 1
   REPEAT 
   
   ! Copy the file list adding select and parent entries unless at root
   IF path$ = "" THEN 
    Array.copy d1$[1, i], d2$[-1]
   ELSE 
    Array.copy d1$[1, i], d2$[-2]
    d2$[2] = ".."
   ENDIF 
   d2$[1] = "Choose this path:\n /" + path$
   
   ! Present the list and get the user's choice
   Dialog.Select s, d2$[], title$
   
   IF s = 0 THEN           % back key was pressed in select
    fin = -1
    
   ELSEIF s = 1 THEN       % directory was chosen
    fin = 1
    
   ELSE 
    IF ENDS_WITH("/", d2$[s]) THEN   % add chosen directory to path
     path$ = path$ + d2$[s]
     
     ! cases where s = 1 i.e. ".." entry WAS selected
    ELSEIF path$ = "" THEN      % Already at root so shouldn't happen
     POPUP "Internal Error 1", 0, 0, 1
     TONE 900, 500
    ELSE                        % split the path by the "/" chars
     Array.delete p$[]
     SPLIT p$[], path$, "/"
     Array.length length, p$[]
     
     path$ = ""
     FOR i = 1 TO length - 1
      path$ = path$ + p$[i] + "/"
     NEXT i
    ENDIF 
   ENDIF 
  UNTIL fin
  
  IF fin < 0 THEN FN.RTN ""
  FN.RTN "/" + path$
 FN.END 
 
 !
 ! Get the pattern to match file names with
 !
 FN.DEF getmask$(p0$) 
  Array.delete s$[]
  Array.load s$[], "(", ")", "[", "{", "|", "^", "$", "+", "."
  Array.length n, s$[]
  
  INPUT "File or pattern\n(wildcards: ? *)", p0$, p0$, can
  IF can THEN 
   p$ = ""
  ELSE 
   IF LEFT$(p0$, 1) = ":" THEN 
    p$ = MID$(p0$, 2) 
   ELSE 
    p$ = p0$
    FOR i = 1 TO n
     p$ = REPLACE$(p$, s$[i], "\\" + s$[i]) 
    NEXT i
    p$ = REPLACE$(p$, "*", ".*") 
    p$ = REPLACE$(p$, "?", ".") 
   ENDIF 
  ENDIF 
  FN.RTN p$
 FN.END 
 
 
 FN.DEF gettoroot$() 
  File.root rootpath$
  SPLIT rootpath$[], rootpath$, "/"
  Array.length np, rootpath$[]
  FOR i = 2 TO np
   toroot$ = toroot$ + "../"
  NEXT i
  FN.RTN toroot$
 FN.END 
 
 
 !
 ! Get all files in directory path$ that match mask$
 ! Add them to the list filelist
 !
 FN.DEF findfiles(mask$, path$, filelist) 
  Array.delete x$[]
  SPLIT x$[], path$, "/"
  Array.length i, x$[]
  PRINT LEFT$(":::::::::::::", i) ; " /"; x$[i]
  
  m$ = "^((?i-)" + mask$ + ")$"
  
  ! Get the listing from the path directory
  Array.delete d$[]
  File.Dir gettoroot$() + path$, d$[], "/"
  Array.length length, d$[]
  
  n = 0
  
  ! Two passes, so files are done before subdirectories
  ! and the order is kept in both cases
  
  FOR i = 1 TO length
   IF !ENDS_WITH("/", d$[i]) THEN 
    IF d$[i] <> ".." THEN 
     IF d$[i] <> "" THEN 
      IF WORD$(d$[i], 1, m$) = "" THEN 
       List.add filelist, path$ + d$[i]
       n = n + 1
      ENDIF 
     ENDIF 
    ENDIF 
   ENDIF 
  NEXT i
  FOR i = 1 TO length
   IF ENDS_WITH("/", d$[i]) THEN 
    n = n + findfiles(mask$, path$ + d$[i], filelist) 
   ENDIF 
  NEXT i
  
  Array.delete d$[], x$[]
  
  FN.RTN n
  
 FN.END 
 
 !--------------------------!
 ! Main program starts here !
 !--------------------------!
 
 Console.title "File Finder"
 
 Array.load askfirst$[], "Show Info", "Search for a File", "Cancel"
 Array.load askagain$[], "Search Again", "Finished"
 
 nm = -1
 do
  read.next buf$, t$
  nm = nm+1
 until buf$="" 
 
 Read.from 1
 DIM ftyp$[nm], mm$[nm]
 FOR i = 1 TO nm
  Read.next buf$, mm$[i]
  ftyp$[i] = buf$ + "\n\t" + mm$[i]
 NEXT i
 
 List.create S, filelist
 
 DO 
  Dialog.Select i, askfirst$[]
  IF i = 1 THEN GOSUB showinfo
 UNTIL i <> 1
 
 IF i <> 2 THEN GOTO finished
 
 DO 
  p$ = getpath$("Set search path\n('Back' cancels)") 
  IF p$ = "" THEN 
   PRINT "No path selected"
   waitback = 20
   WHILE waitback > 0
    PAUSE 100
    waitback = waitback - 1
   REPEAT 
  ELSE 
   PRINT "Searching:\n"; p$; "\nand subdirectories..."
   
errorback2:
   Dialog.Select i, ftyp$[], "File mask to edit"
   IF i = 0 THEN 
    mm$ = ""
   ELSE 
    mm$ = mm$[i]
   ENDIF 
   
   m$ = getmask$(mm$) 
   
   errorwhere = 2
   t$ = WORD$("ABC", 1, m$)   % test that pattern is valid RegEx
   errorwhere = 0
   
   IF m$ = "" THEN 
    PRINT "Cancelled"
    waitback = 20
    WHILE waitback > 0
     PAUSE 100
     waitback = waitback - 1
    REPEAT 
   ELSE 
    Console.title "Find " + mm$
    
    toroot$ = gettoroot$() 
    
    PRINT "Searching..."
    matches = findfiles(m$, p$, filelist) 
    
    IF matches = -1 THEN 
     PRINT "Invalid Search Pattern "; m$
    ELSE 
     List.size filelist, numfiles
     
     IF numfiles <> matches THEN 
      PRINT "WARNING - Inconsistent results: "; 
      PRINT INT$(numfiles) ; " and "; INT$(matches) 
      waitback = 40
      WHILE waitback > 0
       PAUSE 100
       waitback = waitback - 1
      REPEAT 
     ENDIF 
     
     IF numfiles = 0 THEN 
      PRINT "No files matching\n "; mm$; "\nwere found in\n "; p$
     ELSE 
      IF numfiles = 1 THEN 
       List.get filelist, 1, f$
       PRINT "One matching file:\n "; f$
      ELSE 
       PRINT INT$(numfiles) ; " matching files were found"
      ENDIF 
      waitback = 20
      WHILE waitback > 0
       PAUSE 100
       waitback = waitback - 1
      REPEAT 
      
      Array.delete f$[]
      List.ToArray filelist, f$[]
      lp = LEN(p$) + 1
      FOR i = 1 TO numfiles
       f$[i] = MID$(f$[i], lp) 
      NEXT i
      
      Dialog.Select f, f$[], "Copy file name to clipboard ('Back' cancels)"
      IF f = 0 THEN 
       PRINT "No file was selected"
      ELSE 
       List.get filelist, f, f$
       PRINT "Selected file:\n "; f$
       IF clip$ = "" THEN 
        clip$ = f$
       ELSE 
        clip$ = clip$ + "\n" + f$
       ENDIF 
       Clipboard.put clip$
       PRINT "Filename put in clipboard"
      ENDIF 
     ENDIF 
     
     Text.open A, outfile, "whereis.txt"
     Text.writeln outfile, "Whereis.bas - run on "; 
     TIME yr$, mo$, dy$, hr$, mi$
     Text.writeln outfile, yr$; "/"; mo$; "/"; dy$; " at "; hr$; ":"; mi$
     Text.writeln outfile, "Find:    "; mm$
     Text.writeln outfile, "= regex: "; m$
     Text.writeln outfile, "In path: "; p$
     Text.writeln outfile, INT$(numfiles) ; " matching files found"
     Text.writeln outfile, 
     FOR i = 1 TO numfiles
      List.get filelist, i, f$
      IF i = f THEN 
       Text.writeln outfile, "> "; f$
      ELSE 
       Text.writeln outfile, "  "; f$
      ENDIF 
     NEXT i
     Text.writeln outfile, 
     IF f THEN 
      Text.writeln outfile, " > indicates selected file"
      Text.writeln outfile, 
     ENDIF 
     Text.writeln outfile, "--------------------------------------------------------------------------------"
     
     Text.close outfile
     
     PRINT "Results saved in rfo-basic/data/whereis.txt"
    ENDIF 
    
    List.clear filelist
    waitback = 40
    WHILE waitback > 0
     PAUSE 100
     waitback = waitback - 1
    REPEAT 
   ENDIF 
  ENDIF 
  Dialog.Select again, askagain$[], "Another?"
 UNTIL again <> 1
 
finished:
 IF useexit THEN EXIT 
 END "Press back to finish"
 
 
 
showinfo:
 Html.open 1
 POPUP "Loading Help", 0, 0, 0
 h$ = ""
 Read.from 3 + nm + nm
 DO 
  Read.next t$
  h$ = h$ + t$
 UNTIL t$ = ""
 Html.load.string h$
 DO 
  Html.get.datalink t$
 UNTIL STARTS_WITH("BAK:", t$) | STARTS_WITH("LNK:", t$) 
 Html.close 
 h$ = ""
 t$ = ""
 RETURN 
 
 
! If Back is pressed, stop wait loops by zeroing the counter
OnBackKey:
 waitback = 0
 PAUSE 100
 Back.resume 
 
 
OnError:
 emsg$ = GetError$() 
 inerror = inerror + 1
 IF inerror > 1 THEN END "Error Recursion"
 IF errorwhere = 2 THEN GOTO errortype2
 
errorstop:
 PRINT "Stopped due to ERROR:"
 PRINT emsg$
 END 
 
! Handle errors in regular expression syntax 
errortype2:
 errorwhere = 0
 IF IS_IN("is invalid argument", emsg$) = 0 THEN GOTO errorstop
 PRINT "Invalid regular expression " + m$
 waitback = 30
 WHILE waitback > 0
  PAUSE 100
  waitback = waitback - 1
 REPEAT 
 inerror = 0
 GOTO errorback2
 
 !
 ! Names and values of pre-set masks
 !
 Read.data "Any file", "?*.*"
 Read.data "HTML files", ":.+\\.html?"
 Read.data "PDF files", "?*.pdf"
 Read.data "Plain text", ":.+\\.(txt|asc|csv)"
 Read.data "Basic Source", "?*.bas"
 Read.data "Images", ":.+\\.(png|gif|jpg)"
 Read.data "PNG bitmaps", "?*.png"
 Read.data "JPEG Graphics", "?*.jpg"
 Read.data "Media Player Files", "?*.mp?"

 ! You can add more names and masks here. They must be in matched
 ! pairs and be inserted above the two empty strings that mark the
 ! end of the list.

 Read.data "", ""


 !
 ! Text for "show information"
 !
 Read.data "<html><body><p><br></p>"
 Read.data "<h3>Whereis.bas<br> a program to find files</h3>"
 Read.data "<p>Searches for files whose names match a specified "
 Read.data "pattern. Searches in a specified location.</p>"
 Read.data "<p><i>First</i> select a path to search. The search will include any subdirectories.</p>"
 Read.data "<p><i>Then</i> select one of the pre-set patterns.</p>"
 Read.data "<p><i>Finally</i>, edit the pattern if necessary.</p>"
 Read.data "<p>There are 2 types of patterns: "
 Read.data "<ul><li>Ordinary patterns use traditional <b>*</b>&nbsp;and&nbsp;<b>?</b> wildcards "
 Read.data "in file names where <b>?</b> matches any 1 character and "
 Read.data "<b>*</b> matches any zero or more characters. (The pattern "
 Read.data "can also be a file name without any wildcards.)"
 Read.data "<li>Regular expression patterns start with a colon (which "
 Read.data "is there to show that it is a regex, and is discarded.) "
 Read.data "They can include any regular expression operators."
 Read.data "</ul></p>"
 Read.data "<p>If any matching files are found you are shown a list "
 Read.data "and can select a file to have its name put on the "
 Read.data "Android clipboard (so it can be pasted where needed.) "
 Read.data "Press the 'Back' key if you don't want to do that.</p>"
 Read.data "<p>Results are written to a text file <code>whereis.text</code> in "
 Read.data "the <code>rfo-basic/data</code> directory. This holds cumulative "
 Read.data "results, so the latest results will be at the end of the "
 Read.data "file. You may want to delete or edit it from time "
 Read.data "to time to stop it getting too large.</p>"
 Read.data "<p>The program will loop to do further searches until "
 Read.data "you select the Finish option.<br><br><br></p>"
 Read.data "<hr><p><center><a HREF=\"exit.html\">Return to Main Menu</a></center><br><br></p>"
 Read.data "</body></html>"
 Read.data ""
 
 
 
