Rem Battery Status 
Rem For Android
Rem With RFO Basic!
Rem November 2016
Rem Version 1.00
Rem By Roy Shepherd

Rem Thanks to Mog, Aat and evolbug for there help with this app.

di_height = 1152 % set to my Device
di_width = 672

gr.open 255,0,0,255
gr.orientation 1 % Portrait  
pause 1000
WakeLock 3
 
gr.screen screenWidth, screenHeight
scale_x = screenWidth / di_width
scale_y = screenHeight / di_height
gr.scale scale_x, scale_y

gosub Initialise
do
    gosub UpDateBattery
    pause 10000 % Wait a bit and then update battery info again.
until 0

onBackKey:
  exit
back.resume 
end

!------------------------------------------------
! Draw the battery and text
!------------------------------------------------
Initialise:
    array.load command$[], "capacity", "health", "status"
    dim system$[3]
    w = di_width : h = di_height
    gr.set.stroke 3 : gr.text.size 45 : gr.text.align 2
    
    ! Set colours
    gr.color 255, 255, 0, 0, 1 : gr.paint.get colourRed
    gr.color 255, 0, 255, 0, 1 : gr.paint.get colourGreen
    gr.color 255, 255, 255, 0, 1 : gr.paint.get colourYellow
    
    ! Fill battery
    gr.color 255, 0, 255, 0, 1    
    gr.rect batteryPersent, (w / 2) - 250, (h / 2) + 400, (w / 2) + 250, ((h / 2) - 400)
    
    ! Battery case
    gr.color 255, 255, 255, 255, 0
    gr.rect null, (w / 2) - 250, (h / 2) - 400, (w / 2) + 250, (h / 2) + 400 

    ! Battery top
    gr.rect null, (w / 2) - 50, (h / 2) - 400, (w / 2) + 50, (h / 2) - 420
    
    ! Battery infC
    gr.color ,,,,1
    gr.text.draw txtHealth, (w / 2), (h / 2) - 100, "Health"
    gr.text.draw txtPersent, (w / 2), (h / 2), "capacity" + "%"
    gr.text.draw txtCharge, (w / 2), (h / 2) + 100, "Charging"
    
    ! Title 
    gr.text.draw null, (w / 2), 50, "Battery Status"
return

!------------------------------------------------
! Update the battery info
!------------------------------------------------
UpDateBattery:
    gosub GetBatteryInfo
    
    if is_number(capacity$) then 
        capacity = val(capacity$)
    else
        dialog.message "Can't find Battery Status",, ok, "OK"
        exit
    endif

    ! Set colour for the battery
    gr.modify batteryPersent, "paint", colourGreen
    if capacity > 10 & capacity < 26 then gr.modify batteryPersent, "paint", colourYellow
    if capacity < 11 then gr.modify batteryPersent, "paint", colourRed
    
    ! Do the battery and display the battery info
    gr.modify batteryPersent, "left", (w / 2) - 250, "top", ((h / 2) + 400) - (capacity * 8), "right", (w / 2) + 250, "bottom", (h / 2) + 400
    gr.modify txtHealth, "text", "Health: " + health$
    gr.modify txtPersent, "text", capacity$ + "%"
    gr.modify txtCharge, "text", charging$
    gr.render
return

!------------------------------------------------
! Get battery info capacity, health and status
!------------------------------------------------
GetBatteryInfo:
    path$ = "cat /sys/class/power_supply/battery/"
    path2$ = "cat /sys/class/power_supply/bms/"
    
   system.open
    pause 200
    for i = 1 to 3
        system.write path$ + command$[i]
        system.write path2$ + command$[i]
        pause 100
        do
            system.read.line system$[i]
            system.read.ready ready
        until !ready
       ! print command$[i], system$[i]
    next i
    system.close
    capacity$ = system$[1]
    health$ = system$[2]
    if health$ = "" then health$ = "Not Found"
    charging$ = system$[3]
return

