!!	

the code below is an coversion from vb-syntax to 
RFO Basic!-syntax; the original code could be found here:

http://www.freevbcode.com/ShowCode.asp?ID=2090

The author is Jonathan Adamit

	
	
																
original disclaimer:		

												
																
																
    %                           Acclaimer and Disclaimer!														
     %*********************************************************************************************											
     %*          This code is written by Jonathan Adamit (c) 20th in December 2000.               *										
     %*  I Am in no way responsible for anything that occures as a result of implementing         *										
     %*                  this code or by using it in any way.                                     *											
     %*  This code is given to you for free to do whatever you want to do with it.                *											
     %*  My only request is that if you use it - you give me the credit for it.                   *											
     %*  I will also be very greatful if you E-mail me any improvements or changes you made to it.*											
     %*              Any comments or changes are to be sent to: adamit@bgumail.bgu.ac.il          *											
     %*********************************************************************************************										
 

!!	
																	
fn.def Parse(Exp$)		
	
	 bDebug=0		
	 										
	Exp$ = RSpaces$(Exp$) 
  if bDebug then print " RSpaces ", exp$	
  													       																
	Exp$ = RPlusMinus$(Exp$)  
	 if bDebug then print " RPlusMinus ", exp$   																
	               																
	If is_in(  "[",Exp$) > 0 Then Exp$ = CSTA$(Exp$) 
	  if bDebug then print " CSTA ", exp$ 
	             														             																
	If is_in( "(",Exp$) > 0 Then Exp$ = Parentheses$(Exp$)  
	 if bDebug then print " Parentheses ", exp$  
	   																
	If is_in(  "^",Exp$) > 0 Then Exp$ = Power$(Exp$)  
	 if bDebug then print " Power ", exp$  
	         																
	If is_in( "*",Exp$) > 0 Then Exp$ = Mul$(Exp$)		
	 if bDebug then print " Mul ", exp$ 
	 														
	If is_in( "/",Exp$) > 0 Then Exp$ = Div$(Exp$)	
	 if bDebug then print " Div ", exp$ 															
 																
	If is_in( "+",Exp$) > 0 Then Exp$ = Add$(Exp$)		
	 if bDebug then print " Add ", exp$ 
	 													
	If is_in( "-",Exp$,2) > 0 Then Exp$ = Subt$(Exp$) 
	 if bDebug then print " Subt ", exp$ 	
	 														
	fn.rtn  val(Exp$)																
fn.end																	
																	
																	
																	
																	
																	
fn.def Mul$(Exp$)																	
	Exp$ = RPlusMinus$(Exp$)  																
	Place = is_in("*",Exp$)																
	If Place > 0 Then																
	    Y = 0																
	    X = Place																
	    Do    																
	        Y = Y + 1																
	        X = X - 1																
	        If X = 0 Then D_u.break    																
	        If (Mid$(Exp$, X, 1) <> "+") & (Mid$(Exp$, X, 1) <> "-" | Y = 1) & (Mid$(Exp$, X, 1) <> "/") Then																
	            Before$ = Mid$(Exp$, X, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "+") | (Mid$(Exp$, X, 1) = "-" & Y <> 1) | (Mid$(Exp$, X, 1) = "/")   																
	    BeforeStr$ = Mid$(Exp$, 1, X)																
	    Y = 0																
	    X = Place																
	    Do																
	        Y = Y + 1																
	        X = X + 1																
	        If (Mid$(Exp$, X, 1) <> "+") & (Mid$(Exp$, X, 1) <> "-" | Y = 1) & (Mid$(Exp$, X, 1) <> "*") & (Mid$(Exp$, X, 1) <> "/") Then																
	            After$ = Mid$(Exp$, Place + 1, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "+") | (Mid$(Exp$, X, 1) = "-" & Y <> 1) | (Mid$(Exp$, X, 1) = "*") | (Mid$(Exp$, X, 1) = "/") | (X > Len(Exp$))																
	    NewExp$ = BeforeStr$ + str$( val(Before$) * val(After$) ) + Mid$(Exp$, X)																
	    If is_in(  "*", NewExp$ ) > 0 Then																
	        NewExp$ = Mul$(NewExp$)   																
	    End If																
	fn.rtn NewExp$																
	End If																
	fn.rtn Exp$																
fn.end																	
																	
fn.def Add$(Exp$)
!print exp $																
	Exp$ = RPlusMinus$(Exp$)																
	Place = is_in( "+",Exp$)																
	If Place > 0 Then																
	    Y = 0																
	    X = Place																
	    Do      %loop backwards on the string to find out the number Before$ the sign																
	        Y = Y + 1																
	        X = X - 1																
	        If X = 0 Then D_u.break      %incase we got to the start of the string																
	        If (Mid$(Exp$, X, 1) <> "-" | X = 1) & (Mid$(Exp$, X, 1) <> "*") & (Mid$(Exp$, X, 1) <> "/") Then																
	            Before$ = Mid$(Exp$, X, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "*") | (Mid$(Exp$, X, 1) = "-" & X <> 1) | (Mid$(Exp$, X, 1) = "/")																
	    BeforeStr$ = Mid$(Exp$, 1, X)	
	    if BeforeStr$<>""
	     if right $(BeforeStr$, 1 )="-"
	       BeforeStr$ = Mid$(Exp$, 1, X-1)+"+"
	       Before$= "-"+Before$
	     endif
	    endif
	   ! print BeforeStr$, before $	, x, exp $														
	    Y = 0																
	    X = Place																
	    Do																
	        Y = Y + 1																
	        X = X + 1																
	        If (Mid$(Exp$, X, 1) <> "+") & (Mid$(Exp$, X, 1) <> "-") & (Mid$(Exp$, X, 1) <> "*") & (Mid$(Exp$, X, 1) <> "/") Then																
	            After$ = Mid$(Exp$, Place + 1, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "+") | (Mid$(Exp$, X, 1) = "-") | (Mid$(Exp$, X, 1) = "*") | (Mid$(Exp$, X, 1) = "/") | (X > Len(Exp$))																
	    NewExp$ = BeforeStr$ + str$( val(Before$) + val(After$) ) + Mid$(Exp$, X)																
	    If is_in(  "+",NewExp$) > 0 Then																
	        NewExp$ = Add$(NewExp$)      %Recourse incase there are +     %s Left$																
	    End If																
	fn.rtn NewExp$																
	End If																
	fn.rtn Exp$																
fn.end																	
																	
fn.def Subt$(Exp$)																	
	Exp$ = RPlusMinus$(Exp$)	
	!print "--------"	
	!print exp $														
	Place = is_in( "-",Exp$)																
	If Place = 1 Then Place =is_in( "-",Exp$,2)																
	If Place > 1 Then																
	    Y = 0																
	    X = Place																
	    Do      %loop backwards on the string to find out the number Before$ the sign																
	        Y = Y + 1																
	        X = X - 1																
	        If X = 0 Then D_u.break      %incase we got to the start of the string																
	        If (Mid$(Exp$, X, 1) <> "+") & (Mid$(Exp$, X, 1) <> "*") & (Mid$(Exp$, X, 1) <> "/") Then																
	            Before$ = Mid$(Exp$, X, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "*") | (Mid$(Exp$, X, 1) = "+") | (Mid$(Exp$, X, 1) = "/")																
	    BeforeStr$ = Mid$(Exp$, 1, X)																
	    Y = 0																
	    X = Place																
	    Do																
	        Y = Y + 1																
	        X = X + 1																
	        If (Mid$(Exp$, X, 1) <> "+") & (Mid$(Exp$, X, 1) <> "-") & (Mid$(Exp$, X, 1) <> "*") & (Mid$(Exp$, X, 1) <> "/") Then																
	            After$ = Mid$(Exp$, Place + 1, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "+") | (Mid$(Exp$, X, 1) = "-") | (Mid$(Exp$, X, 1) = "*") | (Mid$(Exp$, X, 1) = "/") | (X > Len(Exp$))																
	    NewExp$ = BeforeStr$ + str$( val(Before$) - val(After$) ) + Mid$(Exp$, X)																
	    If is_in(  "-",NewExp$) > 0 Then																
	        NewExp$ = Subt$(NewExp$)      %Recourse incase there are -     %s Left$																
	    End If																
	    fn.rtn NewExp$																
	End If																
	   fn.rtn Exp$																
fn.end																	
																	
fn.def Parentheses$(Exp$)																	
	Exp$ = RPlusMinus$(Exp$)														
	Place = is_in( ")",Exp$)																
	If Place > 0 Then																
	    Y = 0																
	    X = Place
	    Do      %loop to find the inside of the parentheses																
	        Y = Y + 1																
	        X = X - 1																
	        If X = 0 Then D_u.break      %incase we got to the start of the string																
	        If Mid$(Exp$, X, 1) <> "(" Then																
	            MiddleStr$ = Mid$(Exp$, X, Y)																
	        End If																
	    Until Mid$(Exp$, X, 1) = "("																
	    BeforeStr$ = ""      %if it     %s the beginning of the string - there is nothing Before$.																
	    If X > 0 Then BeforeStr$ = Mid$(Exp$, 1, X - 1)															
	    NewExp$ = BeforeStr$ + str$(Parse(MiddleStr$) )+ Mid$(Exp$, Place + 1)																
	    If is_in(")",Exp$) > 0 Then																
	        NewExp$ = Parentheses$(NewExp$)      %Recourse incase there are more Parentheses$																
	    End If																
	   fn.rtn NewExp$																
	End If																
	   fn.rtn Exp$																
fn.end																	
																	
fn.def Div$(Exp$)																	
	!On Error Resume Next																
	Exp$ = RPlusMinus$(Exp$)																
	Place = is_in( "/",Exp$)																
	If Place > 0 Then																
	    Y = 0																
	    X = Place																
	    Do      %loop backwards on the string to find out the number Before$ the sign																
	        Y = Y + 1																
	        X = X - 1																
	        If X = 0 Then D_u.break      %incase we got to the start of the string																
	        If (Mid$(Exp$, X, 1) <> "+") & (Mid$(Exp$, X, 1) <> "-" | Y = 1) & (Mid$(Exp$, X, 1) <> "*") Then																
	            Before$ = Mid$(Exp$, X, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "+") | (Mid$(Exp$, X, 1) = "-" & Y <> 1) | (Mid$(Exp$, X, 1) = "*")																
	    BeforeStr$ = Mid$(Exp$, 1, X)																
	    Y = 0																
	    X = Place																
	    Do																
	        Y = Y + 1																
	        X = X + 1																
	        If (Mid$(Exp$, X, 1) <> "+") & (Mid$(Exp$, X, 1) <> "-" | Y = 1) & (Mid$(Exp$, X, 1) <> "*") & (Mid$(Exp$, X, 1) <> "/") Then																
	            After$ = Mid$(Exp$, Place + 1, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "+") | (Mid$(Exp$, X, 1) = "-" & Y <> 1) | (Mid$(Exp$, X, 1) = "*") | (Mid$(Exp$, X, 1) = "/") | (X > Len(Exp$))																
	    NewExp$ = BeforeStr$ + str$( val(Before$) / val(After$) ) + Mid$(Exp$, X)	
	    
!!															
	    If Err.Number = 11 Then																
	        Div$ = "Impossible"																
	        Exit Function																
	    End If	
!!
	    															
	    If is_in( "/",NewExp$) > 0 Then																
	        NewExp$ = Div$(NewExp$)      %Recourse incase there are /     %s Left$																
	    End If																
	fn.rtn NewExp$																
	End If																
	fn.rtn Exp$																
fn.end																	
																	
fn.def Power$(Exp$)																	
	Exp$ = RPlusMinus$(Exp$)																
	Place = is_in( "^",Exp$)																
	If Place > 0 Then																
	    Y = 0																
	    X = Place																
	    Do      %loop backwards on the string to find out the number Before$ the sign																
	        Y = Y + 1																
	        X = X - 1																
	        If X = 0 Then D_u.break      %incase we got to the start of the string																
	        If (Mid$(Exp$, X, 1) <> "+") & (Mid$(Exp$, X, 1) <> "-" | X = 1) & (Mid$(Exp$, X, 1) <> "*") & (Mid$(Exp$, X, 1) <> "/") Then																
	            Before$ = Mid$(Exp$, X, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "+") | (Mid$(Exp$, X, 1) = "-" & X <> 1) | (Mid$(Exp$, X, 1) = "*") | (Mid$(Exp$, X, 1) = "/")																
	    BeforeStr$ = Mid$(Exp$, 1, X)																
	    Y = 0																
	    X = Place																
	    Do																
	        Y = Y + 1																
	        X = X + 1																
	        If (Mid$(Exp$, X, 1) <> "+") & (Mid$(Exp$, X, 1) <> "-") & (Mid$(Exp$, X, 1) <> "*") & (Mid$(Exp$, X, 1) <> "/") Then																
	            After$ = Mid$(Exp$, Place + 1, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "+") | (Mid$(Exp$, X, 1) = "-") | (Mid$(Exp$, X, 1) = "*") | (Mid$(Exp$, X, 1) = "/") | (X > Len(Exp$))																
	    NewExp$ = BeforeStr$ + str$( val(Before$) ^ val(After$) ) + Mid$(Exp$, X)																
	    If is_in(  "^",NewExp$) > 0 Then																
	        NewExp$ = Power$(NewExp$)      %Recourse incase there are ^     %s Left$																
	    End If																
	fn.rtn NewExp$																
	End If																
	fn.rtn Exp$																
fn.end																	
																	
fn.def RPlusMinus$(Exp$)																	
	Place = is_in( "+-",Exp$)	
	out $=""															
	If Place > 0 Then																
	    NewExp$ = Left$(Exp$, Place - 1) + "-" + Right$(Exp$, Len(Exp$) - Place - 1)																
	    If is_in(  "+-",NewExp$) > 0 Then																
	        NewExp$ = RPlusMinus$(NewExp$)																
	    End If																
	    out $=  NewExp$																
	End If																
	Place = is_in(  "--",NewExp$)																
	If Place > 0 Then																
	    NewExp$ = Left$(NewExp$, Place - 1) + "+" + Right$(NewExp$, Len(NewExp$) - Place - 1)																
	    If is_in(  "--",NewExp$) > 0 Then																
	        NewExp$ = RPlusMinus$(NewExp$)																
	    End If																
	   out $=  NewExp$																
	End If	
		
	if out $<>"" then fn.rtn  out$			
 	fn.rtn exp$											
fn.end																	
															
																	
fn.def CSTA$(Exp$)																	
	Exp$ = RPlusMinus$(Exp$)																
	Place = is_in( "]",Exp$)																
	If Place > 0 Then																
	    Y = 0																
	    X = Place																
	    Do      %loop to find the inside of the brackets																
	        Y = Y + 1																
	        X = X - 1																
	        If X = 0 Then D_u.break      %incase we got to the start of the string																
	        If Mid$(Exp$, X, 1) <> "[" Then																
	            MiddleStr$ = Mid$(Exp$, X, Y)																
	        End If																
	    Until Mid$(Exp$, X, 1) = "["																
	    BeforeStr$ = ""   
	    If X > 0 Then BeforeStr$ = Mid$(Exp$, 1, X - 4)																
	    If X > 0 Then XType$ = Mid$(Exp$, X - 3, 3)																
	    sw.begin lower$(XType$	)															
	    sw.Case "cos"																
	        NewExp$ = BeforeStr$ + str$(Cos(parse(MiddleStr$))) + Mid$(Exp$, Place + 1)		
	        sw.break													
	    sw.Case "sin"																
	        NewExp$ = BeforeStr$ + str$( Sin(parse(MiddleStr$))) + Mid$(Exp$, Place + 1)		
	        sw.break													
	    sw.Case "tan"																
	        NewExp$ = BeforeStr$ + str$( Tan(parse(MiddleStr$))) + Mid$(Exp$, Place + 1)	
	        sw.break													
	    sw.Case "atn"																
	        NewExp$ = BeforeStr$ + str$( Atan(parse(MiddleStr$))) + Mid$(Exp$, Place + 1)	
	        sw.break													
	    sw.end																
	    If is_in( "]",Exp$) > 0 Then																
	        NewExp$ = CSTA$(NewExp$)															
	    End If																
	    fn.rtn NewExp$																
	End If																
	fn.rtn Exp$																
fn.end																	
																	
fn.def RSpaces$(Exp$)																	
	If Exp$ = "" Then Fn.rtn ""																
	X = 0																
	Do																
	X = X + 1																
	If Mid$(Exp$, X, 1) <> " " Then NewExp$ = NewExp$ + Mid$(Exp$, X, 1)																
	Until X = Len(Exp$)																
	fn.rtn NewExp$																
fn.end																	
																	

