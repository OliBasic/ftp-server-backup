
FN.DEF evalVec(eStr$,	x[], y[])

 ARRAY.LENGTH xLen, x []
 ARRAY.LENGTH yLen, y []

 IF yLen< xLen THEN FN.RTN 0

 FOR i=1 TO xLen
  tmp$ = REPLACE$(eStr$,"x", STR$(x[i]))
  y[i] = Parse(tmp$)
 NEXT

 FN.RTN 1

FN.END


FN.DEF evalVal(eStr$,	xVal)

  tmp$ = REPLACE$(eStr$,"x", str $(xVal))
  y    = Parse(tmp$)

  FN.RTN y

FN.END
