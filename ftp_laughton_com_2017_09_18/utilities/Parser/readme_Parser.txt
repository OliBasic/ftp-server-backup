related thread in forum:

http://rfobasic.freeforums.org/post6474.html#p6474




brief description:

1)

fn_parser.bas

...contains the main routine 

  nexp = parse(parseStr$) 
   
 it returns the caculation result of the passed 
 string as numerical expression.
 
 The passed string has to fullfill the common
 restrictions for formulas.
 It has not to contain any letters !
 
 Trig-functions have to be used with this kind of 
 brackets: []
 
 No validation of the string is performed inside the function
 
 All other functions in this module are not intended for 
 being used from 'outside' 



   
  
2)   

fn_eval.bas

...contains the routines

   nexp = evalVec (parseStr$, x[], y[])
   nexp = evalVal (parseStr$, x)

these functions are only examples for possible 
'wrapping' the parse-function from above

parseStr$ is different from above in that way, that it 
has to contain no numbers but the letter 'x' for representing
numbers.



3)

testParser.bas

example for testing












