INCLUDE /parser/fn_parser.bas
INCLUDE /parser/fn_eval.bas

! testing complex (more or less..) string

 x1=       -7+3*4^2.34*2*Cos(5*sin(2*(3-(2/(7+1))))^2)+33.764
 x2=Parse("-7+3*4^2.34*2*Cos[5*sin[2*(3-(2/(7+1)))]^2]+33.764")
print x1, x2


! testing eval function

!prepare input
ARRAY.LOAD xVec[], 0,1,2,3,4,5,6,7,8,9,10
ARRAY.length xVecLen, xVec[]
DIM yVec[200]

!eval....
bEval = evalVec("x^2+3*x+4", xVec[], yVec[])

!output
PRINT "bEval:"; bEval
IF bEval
 FOR i=1 TO xVecLen
  PRINT xVec[i], yVec[i], evalVal("x^2+3*x+4", xVec[i]) ,xVec[i]^2+3*xVec[i]+4
 NEXT
ENDIF

