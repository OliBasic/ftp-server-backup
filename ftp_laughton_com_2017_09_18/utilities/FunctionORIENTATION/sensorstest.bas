Rem Start Of Basic! Program

!!
This function returns actual orientation of the device
Sensor must be opened first
Then with Sensor,read 3 geht orientation
Don't forget to close the sensor, needs battery!
!! 

!*************************************************************
!** Functionname:	Orientation
!** Parameter:		None
!** Return:		Orientation as number
!*************************************************************
!**			fn.rtn(Orient)
!**			1 = top side up
!*			2 = bottom side up
!**			3 = right side up
!**			4 = left side up
!*************************************************************

fn.def Orientation()
	Sensors.Read 3, azimuth,pitch,roll
   	if (pitch < -45) & (pitch > -135) then Orient = 1
   	if (pitch > 45) & (pitch < 135) then Orient = 2
   	if roll > 45 then Orient = 3
   	If roll < -45 then Orient = 4
	FN.Rtn (Orient)
Fn.End

!*************************************************************
!** Start of the Programm
!*************************************************************

Sensors.Open 3
Turns=0
ActualOrient = Orientation
print ActualOrientation
pause 2000

!*************************************************************
!** Main loop
!*************************************************************

Do
    TempOrientation = orientation()
    If TempOrientation <> ActualOrientation then 
        Print "Orientation changed from: ",ActualOrientation," to ",TempOrientation
        ActualOrientation = TempOrientation
        Print "Turn The Pad!"
        pause 3000
        turns = turns + 1
    EndIf
Until Turns = 10

!*************************************************************
!** End of the Programm
!*************************************************************

Sensors.Close 3
End

!*************************************************************
