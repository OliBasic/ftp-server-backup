This apk. was created for my daughter as I have heard her complaining about online color pickers. 

It is a really simple app...

Features.
* Ability to select a color using
    Slides
    Color Wheel
    Text

* Saves the last selected color values
    on to the clipboard in this 
    format ("Hex" Alpha,Red,Green,Blue)
    delete un-desired.

* Remembers the last color selected and 
    last palette used

I've tried to support all screen ratios.

Thanx
    