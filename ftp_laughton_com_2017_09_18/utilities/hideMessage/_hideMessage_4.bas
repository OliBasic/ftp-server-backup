GOSUB          userfun
GR.OPEN        255,0,0,0,0,-1
GR.FRONT       0


txt2hide$    = "The quick brown fox jumps over the lazy dog"
carryerPic$  = "cartman.png"
picOut$      = "cman.png"
seed         =  - 7359014233

CALL           ENCODE$( seed, txt2hide$, carryerPic$, picOut$)

msg$         = DECODE$( seed  ,  picOut$ )
PRINT          msg$

PRINT
PRINT

msg$         = DECODE$( seed+1  ,  picOut$ )
PRINT          msg$


END


!---------------------
userfun: 

FN.DEF            DECODE$( seed,  carryerPic$ )
 PRINT              "--------------------------"
 PRINT              "decoding... "
 PRINT

 GR.BITMAP.LOAD   carryer, carryerPic$
 GR.BITMAP.SIZE   carryer , carx, cary

 PRINT "carryer picture        : "; carryerPic$
 PRINT "carryerpic size        : " ; INT$( carx ) ; " x " ; INT$( cary ) ; " ("; int $(carx*cary)" pixel)"
 PRINT "reading header (12 bit) .....

 DIM              p [4], px [4], py [4]
 CALL             randomPix(4, carx, cary, seed, p [], px [],py [])

 FOR i=1 TO 4
  GR.GET.BMPIXEL  carryer, px [i] , py [i] , a, r, g, b
  header$      += INT$(BAND(r,1))+ INT$(BAND(g,1))+ INT$(BAND(b,1))
 NEXT
 lenMessage     = BIN( header $)
 npix           = CEIL( lenMessage *8 / 3)+4

 IF npix  > (carx*cary)/4 THEN FN.RTN "wrong key (message to long)"

 DIM              pp [ npix ], ppx [ npix ], ppy [ npix ]
 CALL             randomPix( npix , carx, cary, seed, pp [], ppx [],ppy [])
 PRINT
 PRINT "message length (sscii char) : " ; INT$(lenMessage)
 PRINT "number of used pixels       : " ; INT$( npix)
 PRINT 
 PRINT "processing (decoding)..." 
 PRINT
 FOR i          = 5 TO npix
  GR.GET.BMPIXEL  carryer, ppx [i] , ppy [i] , a, r, g, b
  txt$         += INT$(BAND(r,1))+ INT$(BAND(g,1))+ INT$(BAND(b,1))
  IF              !mod (i,500) THEN PRINT i;" / "; npix
 NEXT

 PRINT            "bitstream:"
 PRINT            header$ + txt$
 FOR i          = 0 TO lenMessage-1
  msg$         += CHR$(BIN(MID$(txt$,i*8+1,8))) 
 NEXT
 PRINT
 PRINT            "message:"
 PRINT            msg$
 PRINT
 PRINT "....decoding ready"
 PRINT              "--------------------------"
 PRINT
 FN.RTN           msg$
FN.END




FN.DEF              encode$ ( seed, txt2hide$, carryerPic$, picOut$)


 PRINT              "--------------------------"
 PRINT              "encoding... "
 PRINT

 PRINT "text to encode: "
 PRINT  txt2hide$
 PRINT 
 PRINT "carryer picture        : "; carryerPic$
 PRINT "output picture         : "; picOut$

 lenTxt           = len ( txt2hide$ )

 IF lenTxt  > 4096 THEN FN.RTN "message to long ("+int$(lenTxt)+" bytes, max. 4096 bytes)"

 FOR i            = 1 TO lenTxt
  b1$             = BIN$(ASCII(MID$( txt2hide$ , i,1)))
  binTxt$        += LEFT$("00000000",8-LEN(b1$))+b1$
 NEXT

 bHeader$         = BIN$( lenTxt )
 bHeader$         = LEFT$("000000000000000",12-LEN(bHeader$)) + bHeader$
 binTxt$          = bHeader$ + binTxt$ 
 binTxt$         += LEFT$("00",3- MOD( LEN(binTxt$) ,3))
 npix             = CEIL( lenTxt *8 / 3) + 4

 GR.BITMAP.LOAD     carryer, carryerPic$
 GR.BITMAP.SIZE     carryer , carx, cary
 GR.BITMAP.CREATE   out, carx, cary

 IF npix  > (carx*cary)/4 THEN FN.RTN "encoded aborted(message to long in relation to picture-size)"

 PRINT "carryerpic size        : " ; INT$( carx ) ; " x " ; INT$( cary ) ; " ("; int $(carx*cary)" pixel)"
 PRINT
 PRINT "length ascii           : " ; INT$( lenTxt)
 PRINT "length bits            : " ; INT$( LEN(binTxt$))
 PRINT "number of used pixels  : " ; INT$( npix)
 PRINT "bitstream: " 
 PRINT binTxt$
 PRINT
 PRINT               "randomize pixel...."
 DIM                pix [ npix ], pixx [ npix ], pixy [ npix ]
 CALL               randomPix(npix, carx, cary, seed, pix [], pixx [],pixy [])

 PRINT
 PRINT               "processing (encoding)..."


 GR.BITMAP.DRAWINTO.START out
 GR.SET.ANTIALIAS   0
 GR.SET.STROKE      0
 GR.COLOR           255,0,0,0,1
 GR.RECT            nn,0,0, carx, cary
 GR.BITMAP.DRAW     nn, carryer,0,0


 FOR i            = 1 TO npix

  bitr            = VAL(MID$( binTxt$, i*3-2,1))
  bitg            = VAL(MID$( binTxt$, i*3-1,1))
  bitb            = VAL(MID$( binTxt$, i*3  ,1))

  GR.GET.BMPIXEL    out, pixx [i] , pixy [i] , a, r, g, b

  IF bitr           THEN rr = BOR(r,1) ELSE rr = BAND(r, 254)
  IF bitg           THEN gg = BOR(g,1) ELSE gg = BAND(g, 254)
  IF bitb           THEN bb = BOR(b,1) ELSE bb = BAND(b, 254)

  GR.COLOR          a , rr, gg, bb, 1
  GR.POINT          nn, pixx[i], pixy[i]
  IF                !mod (i,100) THEN PRINT INT$(i);" / "; INT$( npix)

 NEXT

 PRINT
 PRINT              "saving output picture..."

 GR.BITMAP.DRAWINTO.END
 GR.BITMAP.SAVE     out, picOut$
 GR.BITMAP.DELETE   out

 PRINT 
 PRINT              "...encoding ready"
 PRINT              "--------------------------"
 PRINT
 FN.RTN             "1"
FN.END




FN.DEF               randomPix(npix, carx, cary, seed, p [], px [],py [])

 dummy             = randomize (seed)
 picSize           = carx* cary

 FOR i             = 1 TO npix

  DO
   rnd             = int (RND()* (picSize-1) )+1
   ARRAY.SEARCH       p [], rnd, found
  UNTIL              !found

  p  [++ctr]       = rnd
  px [ctr]         = MOD( rnd , carx) 
  py [ctr]         = INT( rnd / carx) 

  !print ctr, p  [ctr], px [ctr], py [ctr]

  IF                 !mod (i,100) THEN PRINT INT$(i);" / "; INT$( npix)

 NEXT 

FN.END


RETURN
!---------------------
