A demo for creating gradient filled shapes.
Contains a function that can be copied and used elsewhere.
I hope the function is straightforward and self-explanatory.
Can be easily extended with more effects and/or shapes.

Enjoy, Aat.