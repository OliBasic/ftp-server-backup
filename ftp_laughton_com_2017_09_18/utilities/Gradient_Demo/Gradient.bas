! Gradient demo
! AatDon @2013

FN.DEF Gradient(PNTR,Shape$,gWidth,gHeight,sR,sG,sB,eR,eG,eB)
! PNTR => pointer to created bitmap
! Shape$ => "R"ectangle, "O"val, "T"riangle, "RT"riangle, "A"rrow
! gWidth, gHeight => bounding box
! sR, sG, sB => start RGB values
! eR, eG, eB => end RGB values
	pi=3.1415
	GR.SET.ANTIALIAS 0
	GR.SET.STROKE 0
	RStep=(eR-sR)/(gWidth-1)
	GStep=(eG-sG)/(gWidth-1)
	BStep=(eB-sB)/(gWidth-1)
	GR.BITMAP.CREATE PNTR,gWidth,gHeight
	GR.BITMAP.DRAWINTO.START PNTR
	SW.BEGIN Shape$
		SW.CASE "R"
			! Rectangle
			FOR x=0 TO gWidth-1
				GR.COLOR 255,sR,sG,sB,0
				GR.LINE gl,x,0,x,gHeight-1
				sR=sR+RStep
				sG=sG+GStep
				sB=sB+BStep
			NEXT x
		SW.BREAK
		SW.CASE "O"
			! Oval
			xR=(gWidth-1)/2
			yR=(gHeight-1)/2
			FOR x=0 TO gWidth
				y1=yR+SQR(xR^2-(x-xR)^2)*(gHeight/gWidth)
				y2=yR-SQR(xR^2-(x-xR)^2)*(gHeight/gWidth)
				GR.COLOR 255,sR,sG,sB,0
				GR.LINE gl,x,y1,x,y2
				sR=sR+RStep
				sG=sG+GStep
				sB=sB+BStep
			NEXT x
		SW.BREAK
		SW.CASE "T"
			! Triangle
			r=((gWidth-1)/2)/(gHeight-1)
			FOR y=0 TO gHeight-1
				x1=(gWidth-1)/2-(y*r)
				x2=(gWidth-1)/2+(y*r)
				GR.COLOR 255,sR,sG,sB,0
				GR.LINE gl,x1,y,x2,y
				sR=sR+RStep
				sG=sG+GStep
				sB=sB+BStep			
			NEXT y
		SW.BREAK
		SW.CASE "RT"
			! Rectangular Triangle
			r=(gWidth-1)/(gHeight-1)
			x2=gWidth-1
			FOR y=0 TO gHeight-1
				x1=(gWidth-1)-(y*r)
				GR.COLOR 255,sR,sG,sB,0
				GR.LINE gl,x1,y,x2,y
				sR=sR+RStep
				sG=sG+GStep
				sB=sB+BStep			
			NEXT y
		SW.BREAK
		SW.CASE "A"
			! Arrow
			Y=(gHeight/3)
			FOR x=0 TO gWidth/2-1
				y1=gHeight-Y
				y2=gHeight-2*Y
				GR.COLOR 255,sR,sG,sB,0
				GR.LINE gl,x,y1,x,y2
				sR=sR+RStep
				sG=sG+GStep
				sB=sB+BStep			
			NEXT x
			dY=(gHeight/2)/(gWidth/2)
			FOR x=gWidth/2 TO gWidth-1
				ix=gWidth-x
				y1=gHeight/2+dY*ix
				y2=gHeight/2-dY*ix
				GR.COLOR 255,sR,sG,sB,0
				GR.LINE gl,x,y1,x,y2
				sR=sR+RStep
				sG=sG+GStep
				sB=sB+BStep			
			NEXT x
		SW.BREAK
	SW.END
	GR.BITMAP.DRAWINTO.END
	GR.SET.ANTIALIAS 1
	FN.RTN PNTR
FN.END

GR.OPEN 255,0,0,0,0,1
GR.SCREEN w,h
! Please, change the values of ScaleX and ScaleY if you
! encounter problems with resolution and/or aspect ratio
ScaleX=240
ScaleY=320
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
GR.CLS
T$="GRADIENT DEMO"
GOSUB Intro

F1=Gradient(Rect1,"R",100,100,255,255,255,0,0,255)
F2=Gradient(Ov1,"O",100,130,255,0,0,0,255,0)
F3=Gradient(Tr1,"T",100,100,255,255,0,255,0,255)
F4=Gradient(Tr2,"RT",100,100,0,255,255,0,0,0)
F5=Gradient(St1,"A",150,50,0,0,255,255,0,0)

GR.BITMAP.DRAW BM1,F1,10,10
GR.BITMAP.DRAW BM2,F2,130,10
GR.BITMAP.DRAW BM3,F3,10,130
GR.BITMAP.DRAW BM4,F4,130,130
GR.RENDER
DIM BM5[37]
FOR angle=0 TO 360 STEP 10
	! Fade
	GR.COLOR ROUND(SIN(angle/29)*100+155),0,0,0,0
	! Rotate
	GR.ROTATE.START angle,120,155
	GR.BITMAP.DRAW BM5[angle/10+1],F5,90,130
	GR.ROTATE.END
	GR.HIDE BM5[angle/10+1]
	GR.RENDER
NEXT angle
! End button
F6=Gradient(Rect1,"R",40,60,0,0,0,255,0,0)
GR.ROTATE.START 90,180,245
	GR.BITMAP.DRAW BM6,F6,160,215
GR.ROTATE.END
GR.COLOR 255,255,255,255,0
GR.TEXT.SIZE 20
GR.TEXT.DRAW g,157,252,"STOP"
GR.RECT g,150,225,210,265
GR.RENDER
Kap=0
DO
	FOR i=1 TO 37
		GR.SHOW BM5[i]
		GR.RENDER
		PAUSE 100
		GR.HIDE BM5[i]
		IF KAP=1 THEN F_n.BREAK
	NEXT i
UNTIL Kap=1
GR.CLOSE
END

ONGRTOUCH:
DO
	GR.TOUCH touched,x,y
UNTIL !touched
IF x<150*sx | y<225*sy | x>210*sx | y>265*sy THEN
	GR.ONGRTOUCH.RESUME
ELSE
	Kap=1
	GR.ONGRTOUCH.RESUME
ENDIF

Intro:
GR.TEXT.TYPEFACE 2
GR.SET.STROKE 1
DIM gl[LEN(T$)]
DIM tg[LEN(T$)]
Array.LOAD Notes[],523,587,659,698,784,880,988,1047,0,988,880,784,698
FOR j=1 TO LEN(T$)
	IF MID$(T$,j,1)<>" " THEN
		GR.COLOR 255,255,255,255,0
		GR.TEXT.SIZE 40
		FOR i=1 TO 5
			GR.TEXT.SKEW COS(6)
			GR.TEXT.DRAW g,100,i*20,MID$(T$,j,1)
			GR.RENDER
			PAUSE 50
			GR.HIDE g
		NEXT i
		FOR i=6 TO 14.1 STEP 0.9
			GR.TEXT.WIDTH x1,MID$(T$,j,1)
			GR.TEXT.SKEW COS(i)
			GR.TEXT.DRAW g,100,100,MID$(T$,j,1)
			GR.RENDER
			PAUSE 100
			GR.HIDE g
		NEXT i
		TONE Notes[j],200
		GR.TEXT.SIZE 25
		GR.GET.TEXTBOUNDS MID$(T$,j,1),left,top,right,bottom
		GR.COLOR 255,255,0,0,1
		GR.LINE gl[j],100+x1/2,100,5+(j-1)*17+(right-left)/2,200+top
		GR.TEXT.SKEW 0
		GR.TEXT.DRAW g,(j-1)*17+10,200,MID$(T$,j,1)
		GR.COLOR 255,255,255,255,0
		GR.TEXT.DRAW tg[j],(j-1)*17+10,200,MID$(T$,j,1)
		GR.RENDER
	ENDIF
NEXT j
FOR i=1 TO 7
	FOR j=1 TO LEN(T$)
		IF MID$(T$,j,1)<>" " THEN
			GR.MODIFY gl[j],"y1",100+i*10
		ENDIF
	NEXT j
	PAUSE 100
	GR.RENDER
NEXT i
PAUSE 500
GR.CLS
GR.SET.STROKE 1
GR.TEXT.SIZE 25
FOR i=1 TO LEN(T$)
	IF MID$(T$,i,1)<>" " THEN
		GR.COLOR 255,255,0,0,1
		GR.TEXT.DRAW tg[i],(i-1)*17+10,300,MID$(T$,i,1)
		GR.COLOR 255,255,255,255,0
		GR.TEXT.DRAW tg[i],(i-1)*17+10,300,MID$(T$,i,1)
		GR.RENDER
	ENDIF
NEXT i
FOR i=0 TO 35
	GR.COLOR 128+i,40+i*3,40+i*6,40+i*4,1
	GR.LINE g,0,280+i,240,280+i
	GR.RENDER
	PAUSE 20
NEXT i
GR.SET.STROKE 5
GR.COLOR 160,255,255,255,0
GR.RECT g,0,0,240,320
GR.TEXT.TYPEFACE 1
RETURN
