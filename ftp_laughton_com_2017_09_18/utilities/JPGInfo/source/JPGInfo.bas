! Read info from JPG files
! Aat Don 2014
FN.DEF Calc2Value(Align$,ByteIn01,ByteIn02)
	IF Align$="II" THEN
		! little endian
		V=ByteIn01+ByteIn02*256
	ELSE
		! big endian
		V=ByteIn02+ByteIn01*256
	ENDIF
FN.RTN V
FN.END
FN.DEF Calc4Value(Align$,ByteIn01,ByteIn02,ByteIn03,ByteIn04)
	IF Align$="II" THEN
		! little endian
		V=ByteIn01+ByteIn02*256+ByteIn03*65536+ByteIn04*16777216
	ELSE
		! big endian
		V=ByteIn04+ByteIn03*256+ByteIn02*65536+ByteIn01*16777216
	ENDIF
FN.RTN V
FN.END
FN.DEF Orientation()
	SENSORS.READ 3,Azimuth,Pitch,Roll
   	if (Pitch<-45) & (Pitch>-135) then Orient=1
   	if (Pitch>45) & (Pitch<135) then Orient=2
   	if Roll>45 then Orient=3
   	If Roll<-45 then Orient=4
FN.RTN (Orient)
FN.END

ARRAY.LOAD NoB[],1,1,2,4,8,1,1,2,4,8,4,8
SENSORS.OPEN 3
CONSOLE.TITLE "Picture INFO"
GR.OPEN 255,0,0,0,0,-1
PAUSE 1000
GR.SCREEN w,h
ScaleX=720
ScaleY=h/w*ScaleX
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
WAKELOCK 3
Path$="../../"

FILE.EXISTS F,"../../JPGInfo"
IF F>0 THEN FILE.DELETE F,"../../JPGInfo/data"
IF F>0 THEN FILE.DELETE F,"../../JPGInfo"

NwPic:
Kap=0
ActualOrient=0
ShowGraph=1
ChangeGr=1
Brand$=""
Model$=""
Description$=""
Orientation$=""
XRes$=""
YRes$=""
RU$=""
Software$=""
Mod_Date$=""
Mod_Time$=""
ExpTime$=""
Diaphragm$=""
ExpProg$=""
ISO$=""
ExVs$=""
Org_Date$=""
Org_Time$=""
ShutSpd$=""
ActDiaphragm$=""
Bias$=""
MaxDiaphragm$=""
SubDist$=""
MeterMode$=""
LightSource$=""
Flash$=""
FLength$=""
ColorSpace$=""
ImWidth$=""
ImHeight$=""
SensMethod$=""
ExpMode$=""
WhiteMode$=""
DigiZoom$=""
FLength35$=""
Scene$=""
GPSVersion$=""
GPSLatRef$=""
GPSLat$=""
GPSLongRef$=""
GPSLong$=""
GPSAltRef$=""
GPSAlt$=""
ExifOffset=0
GPSOffset=0
GPSInfo$="None"
CamInfo$=""
CamSettings$=""
ShowMap=0
GR.FRONT 0
GOSUB GetFile
GR.FRONT 1
GR.BITMAP.LOAD Pic,FName$
BYTE.OPEN r,JPGFile,FName$

! Read ID
BYTE.READ.BYTE JPGFile,ID01
BYTE.READ.BYTE JPGFile,ID02
ID$=UPPER$(HEX$(ID01))+UPPER$(HEX$(ID02))
IF ID$<>"FFD8" THEN
	END "Not a valid JPG file"
ENDIF
! Read APP marker
BYTE.READ.BYTE JPGFile,Marker
BYTE.READ.BYTE JPGFile,Marker
Marker$=UPPER$(HEX$(Marker))
IF Marker$="E0" THEN
	! JFIF marker found - Read APP0 Size
	BYTE.READ.BYTE JPGFile,DataSize01
	BYTE.READ.BYTE JPGFile,DataSize02
	! Data size is big endian
	DataSize=256*DataSize01+DataSize02-2
	BYTE.POSITION.GET JPGFile,FPos
	BYTE.POSITION.SET JPGFile,FPos+DataSize
	! Read APP1 marker
	BYTE.READ.BYTE JPGFile,Marker
	BYTE.READ.BYTE JPGFile,Marker
ENDIF
Marker$=UPPER$(HEX$(Marker))
IF Marker$<>"E1" THEN
	CamInfo$="No Camera Info found"
	CamSettings$="No Settings found"
	GOTO SkipExif
ENDIF
! Read APP1 Size
BYTE.READ.BYTE JPGFile,DataSize01
BYTE.READ.BYTE JPGFile,DataSize02
! Data size is big endian
DataSize=256*DataSize01+DataSize02-2
! Read Exif Header
BYTE.READ.BUFFER JPGFile,6,DataBuffer$
IF LEFT$(DataBuffer$,4)<>"Exif" THEN
	PRINT "NO valid Exif header found"
	BYTE.CLOSE JPGFile
	END
ENDIF
BYTE.POSITION.GET JPGFile,StartPos
! Read TIFF Header
! BYTE Alignment: "II"='Intel', little endian
!                 "MM"='Motorola', big endian  
BYTE.READ.BUFFER JPGFile,2,Align$
! Read '002A' or '2A00'
GOSUB Read2Bytes
! Read Offset to first IFD
GOSUB Read4Bytes
Offset=Calc4Value(Align$,ByteIn01,ByteIn02,ByteIn03,ByteIn04)-8
! Go to IFD (useually no offset)
BYTE.POSITION.GET JPGFile,FPos
BYTE.POSITION.SET JPGFile,FPos+Offset
! Read number of directory entries
GOSUB Read2Bytes
NumOfEntries=Calc2Value(Align$,ByteIn01,ByteIn02)
DO
	GOSUB ReadTag
	SW.BEGIN Tag$
		SW.CASE "010E"
			! Tag &H010E = Image Description
			Description$=Data$
			SW.BREAK
		SW.CASE "010F"
			! Tag &H010F = Manufacturer
			Brand$=Data$
			SW.BREAK
		SW.CASE "0110"
			! Tag &H0110 = Model
			Model$=Data$
			SW.BREAK
		SW.CASE "0112"
			! Tag &H0112 = Orientation
			Orientation$="Row0,Column0 = "
			Data=Calc2Value(Align$,ByteIn01,ByteIn02)
			IF Data=1 THEN Orientation$=Orientation$+"top,left"
			IF Data=2 THEN Orientation$=Orientation$+"top,right"
			IF Data=3 THEN Orientation$=Orientation$+"bottom,right"
			IF Data=4 THEN Orientation$=Orientation$+"bottom,left"
			IF Data=5 THEN Orientation$=Orientation$+"left,top"
			IF Data=6 THEN Orientation$=Orientation$+"right,top"
			IF Data=7 THEN Orientation$=Orientation$+"right,bottom"
			IF Data=8 THEN Orientation$=Orientation$+"left,bottom"
			IF Data=9 THEN Orientation$="Undefined"
			SW.BREAK
		SW.CASE "011A"
			! Tag &H011A = X-resolution
			GOSUB GetRational
			XRes$=STR$(Numerator/Denominator)
			XRes$=LEFT$(XRes$,LEN(XRes$)-2)
			SW.BREAK
		SW.CASE "011B"
			! Tag &H011B = Y-resolution
			GOSUB GetRational
			YRes$=STR$(Numerator/Denominator)
			YRes$=LEFT$(YRes$,LEN(YRes$)-2)
			SW.BREAK
		SW.CASE "0128"
			! Tag &H0128 = Resolution unit 1=none 2=inch 3=cm
			Data=Calc2Value(Align$,ByteIn01,ByteIn02)
			IF Data=2 THEN RU$="inch"
			IF Data=3 THEN RU$="cm"
			SW.BREAK
		SW.CASE "0131"
			! Tag &H0131 = Firmware version (camera)
			Software$=Data$
			SW.BREAK
		SW.CASE "0132"
			! Tag &H0132 = Date last modified "YYYY:MM:DD HH:MM:SS"+&H00 (20 bytes)
			Mod_Date$=LEFT$(Data$,10)
			Mod_Time$=MID$(Data$,12,8)
			SW.BREAK
		SW.CASE "8769"
			! Tag &H8769 contains offset to Exif SubIFD
			ExifOffset=DataOffset
			SW.BREAK
		SW.CASE "8825"
			! Tag &H8825 contains offset to GPS IFD
			GPSOffset=DataOffset
			SW.BREAK
	SW.END
	NumOfEntries=NumOfEntries-1
UNTIL NumOfEntries=0
IF ExifOffset=0 THEN
	CamSettings$="No Settings found"
	GOTO SkipExif
ENDIF
BYTE.POSITION.SET JPGFile,StartPos+ExifOffset
GOSUB Read2Bytes
NumOfEntries=Calc2Value(Align$,ByteIn01,ByteIn02)
DO
	! Read Exif entry (TAG)
	GOSUB ReadTag
	SW.BEGIN Tag$
		SW.CASE "829A"
			! Tag &H829A = Exposure time
			GOSUB GetRational
			ExpTime$=STR$(ROUND(Denominator/Numerator))
			ExpTime$="1/"+LEFT$(ExpTime$,LEN(ExpTime$)-2)+" s"
			SW.BREAK
		SW.CASE "829D"
			! Tag &H829D = F-stop (lens aperture [f/number])
			GOSUB GetRational
			Diaphragm$="f/"+STR$(ROUND(Numerator/Denominator,2))
			SW.BREAK
		SW.CASE "8822"
			! Tag &H8822 = Exposure program
			Data=Calc2Value(Align$,ByteIn01,ByteIn02)
			ExpProg$="Unknown"
			IF Data=1 THEN ExpProg$="Manual"
			IF Data=2 THEN ExpProg$="Normal"
			IF Data=3 THEN ExpProg$="Aperture priority"
			IF Data=4 THEN ExpProg$="Shutter priority"
			IF Data=5 THEN ExpProg$="Creative"
			IF Data=6 THEN ExpProg$="Action"
			IF Data=7 THEN ExpProg$="Portrait"
			IF Data=8 THEN ExpProg$="Landscape"
			SW.BREAK
		SW.CASE "8827"
			! Tag &H8827 = ISO speed rating
			Data=Calc2Value(Align$,ByteIn01,ByteIn02)
			ISO$=STR$(Data)
			ISO$=LEFT$(ISO$,LEN(ISO$)-2)
			SW.BREAK
		SW.CASE "9000"
			! Tag &H9000 = Exif version - 4 ASCII bytes
			Major$=STR$(VAL(CHR$(ByteIn01)+CHR$(ByteIn02)))
			ExVs$=LEFT$(Major$,LEN(Major$)-2)+"."+CHR$(ByteIn03)+CHR$(ByteIn04)
			SW.BREAK
		SW.CASE "9003"
			! Tag &H9003 = Date taken
			Org_Date$=LEFT$(Data$,10)
			Org_Time$=MID$(Data$,12,8)
			SW.BREAK
		SW.CASE "9201"
			! Tag &H9201 = Actual Shutter speed
			GOSUB GetRational
			ShutSpd$=STR$(ROUND(POW(2,Numerator/Denominator)))
			ShutSpd$="1/"+LEFT$(ShutSpd$,LEN(ShutSpd$)-2)+" s"
			SW.BREAK
		SW.CASE "9202"
			! Tag &H9202 = Actual F-stop (lens aperture [f/number])
			GOSUB GetRational
			ActDiaphragm$="f/"+STR$(ROUND(POW(SQR(2),Numerator/Denominator),2))
			SW.BREAK
		SW.CASE "9204"
			! Tag &H9204 Exposure bias value
			GOSUB GetRational
			Bias$=STR$(ROUND(Numerator/Denominator))+ " EV"
			SW.BREAK
		SW.CASE "9205"
			! Tag &H9205 = MAX F-stop (lens aperture [f/number])
			GOSUB GetRational
			MaxDiaphragm$="f/"+STR$(ROUND(POW(SQR(2),Numerator/Denominator),2))
			SW.BREAK
		SW.CASE "9206"
			! Tag &H9206 Subject distance
			GOSUB GetRational
			SubDist$=STR$(ROUND(Numerator/Denominator))+" m"
			SW.BREAK
		SW.CASE "9207"
			! Tag &H9207 = Metering mode
			Data=Calc2Value(Align$,ByteIn01,ByteIn02)
			IF Data=1 THEN MeterMode$="Average"
			IF Data=2 THEN MeterMode$="Center Weighted Average"
			IF Data=3 THEN MeterMode$="Spot"
			IF Data=4 THEN MeterMode$="Multi Spot"
			IF Data=5 THEN MeterMode$="Multi Segment"
			SW.BREAK
		SW.CASE "9208"
			! Tag &H9208 = Light source (=white balance setting)
			Data=Calc2Value(Align$,ByteIn01,ByteIn02)
			IF Data=1 THEN LightSource$="Daylight"
			IF Data=2 THEN LightSource$="Fluorescent"
			IF Data=3 THEN LightSource$="Tungsten"
			IF Data=10 THEN LightSource$="Flash"
			IF Data=17 THEN LightSource$="Standard Light A"
			IF Data=18 THEN LightSource$="Standard Light B"
			IF Data=19 THEN LightSource$="Standard Light C"
			IF Data=20 THEN LightSource$="D55"
			IF Data=21 THEN LightSource$="D65"
			IF Data=22 THEN LightSource$="D75"
			SW.BREAK
		SW.CASE "9209"
			! Tag &H9209 = Flash
			Data=Calc2Value(Align$,ByteIn01,ByteIn02)
			IF Data=0 THEN Flash$="Flash did not fire"
			IF Data=1 THEN Flash$="Flash fired"
			IF Data=5 THEN Flash$="Strobe return light not detected"
			IF Data=7 THEN Flash$="Strobe return light detected"
			IF Data=9 THEN Flash$="Flash fired, compulsory flash mode"
			IF Data=13 THEN Flash$="Flash fired, compulsory flash mode, return light not detected"
			IF Data=15 THEN Flash$="Flash fired, compulsory flash mode, return light detected"
			IF Data=16 THEN Flash$="Flash did not fire, compulsory flash mode"
			IF Data=24 THEN Flash$="Flash did not fire, auto mode"
			IF Data=25 THEN Flash$="Flash fired, auto mode"
			IF Data=29 THEN Flash$="Flash fired, auto mode, return light not detected"
			IF Data=31 THEN Flash$="Flash fired, auto mode, return light detected"
			IF Data=32 THEN Flash$="No flash function"
			IF Data=65 THEN Flash$="Flash fired, red-eye reduction mode"
			IF Data=69 THEN Flash$="Flash fired, red-eye reduction mode, return light not detected"
			IF Data=71 THEN Flash$="Flash fired, red-eye reduction mode, return light detected"
			IF Data=73 THEN Flash$="Flash fired, compulsory flash mode, red-eye reduction mode"
			IF Data=77 THEN Flash$="Flash fired, compulsory flash mode, red-eye reduction mode, return light not detected"
			IF Data=79 THEN Flash$="Flash fired, compulsory flash mode, red-eye reduction mode, return light detected"
			IF Data=89 THEN Flash$="Flash fired, auto mode, red-eye reduction mode"
			IF Data=93 THEN Flash$="Flash fired, auto mode, return light not detected, red-eye reduction mode"
			IF Data=95 THEN Flash$="Flash fired, auto mode, return light detected, red-eye reduction mode"
			SW.BREAK
		SW.CASE "920A"
			! Tag &H920A = Focal Length used (mm)
			GOSUB GetRational
			FLength$=STR$(Numerator/Denominator)+" mm"
			SW.BREAK
		SW.CASE "A001"
			! Tag &HA001 = ColorSpace
			Data=Calc2Value(Align$,ByteIn01,ByteIn02)
			ColorSpace$="Unknown"
			IF Data=1 THEN ColorSpace$="sRGB"
			SW.BREAK
		SW.CASE "A002"
			! Tag &HA002 = Exif Image Width
			ImWidth$=STR$(DataOffset)
			ImWidth$=LEFT$(ImWidth$,LEN(ImWidth$)-2)
			SW.BREAK
		SW.CASE "A003"
			! Tag &HA003 = Exif Image Height
			ImHeight$=STR$(DataOffset)
			ImHeight$=LEFT$(ImHeight$,LEN(ImHeight$)-2)
			SW.BREAK
		SW.CASE "A217"
			! Tag &HA217 = Sensing Method
			Data=Calc2Value(Align$,ByteIn01,ByteIn02)
			IF Data=1 THEN SensMethod$="Not Defined"
			IF Data=2 THEN SensMethod$="One-Chip Color Area Sensor"
			IF Data=3 THEN SensMethod$="Two-Chip Color Area Sensor"
			IF Data=4 THEN SensMethod$="Three-Chip Color Area Sensor"
			IF Data=5 THEN SensMethod$="Color Sequential Area Sensor"
			IF Data=7 THEN SensMethod$="Trilinear Sensor"
			IF Data=8 THEN SensMethod$="Color Sequential Linear Sensor"
			SW.BREAK
		SW.CASE "A402"
			! Tag &HA402 = Exposure Mode
			Data=Calc2Value(Align$,ByteIn01,ByteIn02)
			IF Data=0 THEN ExpMode$="Auto"
			IF Data=1 THEN ExpMode$="Manual"
			IF Data=2 THEN ExpMode$="Auto Bracket"
			SW.BREAK
		SW.CASE "A403"
			! Tag &HA403 = White Balance Mode used
			Data=Calc2Value(Align$,ByteIn01,ByteIn02)
			IF Data=0 THEN WhiteMode$="Auto"
			IF Data=1 THEN WhiteMode$="Manual"
			SW.BREAK
		SW.CASE "A404"
			! Tag &HA404 = Digital Zoom Ratio
			GOSUB GetRational
			IF Numerator=0 THEN
				DigiZoom$="Not used"
			ELSE
				DigiZoom$=STR$(Numerator/Denominator)+" X"
			ENDIF
			SW.BREAK
		SW.CASE "A405"
			! Tag &HA405 = Focal Length 35mm Equivalent
			Data=Calc2Value(Align$,ByteIn01,ByteIn02)
			FLength35$=STR$(Data)
			FLength35$=LEFT$(FLength35$,LEN(FLength35$)-2)+" mm"
			IF Data=0 THEN FLength35$="Unknown"
			SW.BREAK
		SW.CASE "A406"
			! Tag &HA406 = Scene Capture Type
			Data=Calc2Value(Align$,ByteIn01,ByteIn02)
			IF Data=0 THEN Scene$="Standard"
			IF Data=1 THEN Scene$="Landscape"
			IF Data=2 THEN Scene$="Portrait"
			IF Data=3 THEN Scene$="Night Scene"
			SW.BREAK
	SW.END
	NumOfEntries=NumOfEntries-1
UNTIL NumOfEntries=0
! Go to GPS Tags if present
IF GPSOffset>0 THEN
	GPSInfo$="OK"
	BYTE.POSITION.SET JPGFile,StartPos+GPSOffset
	GOSUB Read2Bytes
	NumOfEntries=Calc2Value(Align$,ByteIn01,ByteIn02)
	DO
		! Read GPS entry (TAG)
		GOSUB ReadTag
		SW.BEGIN Tag$
			SW.CASE "0000"
				! Tag &H0000 = GPS Version
				GPSVersion$=LEFT$(STR$(ByteIn01),LEN(STR$(ByteIn01))-2)
				GPSVersion$=GPSVersion$+"."+LEFT$(STR$(ByteIn02),LEN(STR$(ByteIn02))-2)
				GPSVersion$=GPSVersion$+"."+LEFT$(STR$(ByteIn03),LEN(STR$(ByteIn03))-2)
				GPSVersion$=GPSVersion$+"."+LEFT$(STR$(ByteIn04),LEN(STR$(ByteIn04))-2)
				SW.BREAK
			SW.CASE "0001"
				! Tag &H0001 = GPS Latitude Ref
				GPSLatRef$=CHR$(ByteIn01)
				SW.BREAK
			SW.CASE "0002"
				! Tag &H0002 = GPS Latitude
				GOSUB GetRational
				GPSLat=Numerator/Denominator
				GOSUB GetRational
				Numerator=Calc4Value(Align$,ASCII(MID$(Data$,9,1)),ASCII(MID$(Data$,10,1)),ASCII(MID$(Data$,11,1)),ASCII(MID$(Data$,12,1)))				
				Denominator=Calc4Value(Align$,ASCII(MID$(Data$,13,1)),ASCII(MID$(Data$,14,1)),ASCII(MID$(Data$,15,1)),ASCII(MID$(Data$,16,1)))
				GPSLat=GPSLat+(Numerator/Denominator)/60
				Numerator=Calc4Value(Align$,ASCII(MID$(Data$,17,1)),ASCII(MID$(Data$,18,1)),ASCII(MID$(Data$,19,1)),ASCII(MID$(Data$,20,1)))
				Denominator=Calc4Value(Align$,ASCII(MID$(Data$,21,1)),ASCII(MID$(Data$,22,1)),ASCII(MID$(Data$,23,1)),ASCII(MID$(Data$,24,1)))
				GPSLat=GPSLat+(Numerator/Denominator)/3600
				GPSLat$=STR$(ROUND(GPSLat,5))
				SW.BREAK
			SW.CASE "0003"
				! Tag &H0003 = GPS Longitude Ref
				GPSLongRef$=CHR$(ByteIn01)
				SW.BREAK
			SW.CASE "0004"
				! Tag &H0004 = GPS Longitude
				GOSUB GetRational
				GPSLong=Numerator/Denominator
				Numerator=Calc4Value(Align$,ASCII(MID$(Data$,9,1)),ASCII(MID$(Data$,10,1)),ASCII(MID$(Data$,11,1)),ASCII(MID$(Data$,12,1)))
				Denominator=Calc4Value(Align$,ASCII(MID$(Data$,13,1)),ASCII(MID$(Data$,14,1)),ASCII(MID$(Data$,15,1)),ASCII(MID$(Data$,16,1)))
				GPSLong=GPSLong+(Numerator/Denominator)/60
				Numerator=Calc4Value(Align$,ASCII(MID$(Data$,17,1)),ASCII(MID$(Data$,18,1)),ASCII(MID$(Data$,19,1)),ASCII(MID$(Data$,20,1)))
				Denominator=Calc4Value(Align$,ASCII(MID$(Data$,21,1)),ASCII(MID$(Data$,22,1)),ASCII(MID$(Data$,23,1)),ASCII(MID$(Data$,24,1)))
				GPSLong=GPSLong+(Numerator/Denominator)/3600
				GPSLong$=STR$(ROUND(GPSLong,5))
				SW.BREAK
			SW.CASE "0005"
				! Tag &H0005 = GPS Altitude Ref
				IF ByteIn01=0 THEN
					GPSAltRef$="Above sea level"
				ELSE
					GPSAltRef$="Below sea level"
				ENDIF
				SW.BREAK
			SW.CASE "0006"
				! Tag &H0006 = GPS Altitude
				GOSUB GetRational
				GPSAlt=Numerator/Denominator
				GPSAlt$=STR$(ROUND(GPSAlt,2))+" m "
				SW.BREAK
		SW.END
		NumOfEntries=NumOfEntries-1
	UNTIL NumOfEntries=0
ELSE
	GPSInfo$="None"
ENDIF
SkipExif:
BYTE.CLOSE JPGFile
CLS
PRINT "+-------------+"
PRINT "| Camera INFO |"
PRINT "+-------------+"
IF CamInfo$<>"" THEN PRINT CamInfo$
IF Brand$<>"" THEN PRINT "Camera brand : "+Brand$
IF Model$<>"" THEN PRINT "Camera model : "+Model$
IF Software$<>"" THEN PRINT "Software : "+Software$
IF ExVs$<>"" THEN PRINT "Exif version : "+ExVs$
IF MaxDiaphragm$<>"" THEN PRINT "Max Diaphragm  : "+MaxDiaphragm$
PRINT "+-----------------+"
PRINT "| Camera settings |"
PRINT "+-----------------+"
IF CamSettings$<>"" THEN PRINT CamSettings$
IF ExpTime$<>"" THEN PRINT "Exposure time : "+ExpTime$
IF Diaphragm$<>"" THEN PRINT "Diaphragm set : "+Diaphragm$
IF ExpProg$<>"" THEN PRINT "Exposure program : "+ExpProg$
IF ISO$<>"" THEN PRINT "CCD Sensitivity : ISO "+ISO$
IF LightSource$<>"" THEN PRINT "White Balance Setting : "+LightSource$
IF WhiteMode$<>"" THEN PRINT "White Balance Mode : "+WhiteMode$
IF FLength$<>"" THEN PRINT "Focal Length Used : "+FLength$
IF FLength35$<>"" THEN PRINT "Focal Length (35mm equivalent) : "+FLength35$
IF SensMethod$<>"" THEN PRINT "Camera Sensor Type : "+SensMethod$
IF ExpMode$<>"" THEN PRINT "Exposure Mode : "+ExpMode$
IF DigiZoom$<>"" THEN PRINT "Digital Zoom : "+DigiZoom$
IF Scene$<>"" THEN PRINT "Type of Scene : "+Scene$
IF MeterMode$<>"" THEN PRINT "Metering mode : "+MeterMode$
IF ColorSpace$<>"" THEN PRINT "ColorSpace : "+ColorSpace$
IF Flash$<>"" THEN PRINT "Flash : "+Flash$
PRINT "+------------+"
PRINT "| Image INFO |"
PRINT "+------------+"
PRINT "File : "+FName$
IF Description$<>"" THEN PRINT "Image description : "+Description$
IF Orientation$<>"" THEN PRINT "Image orientation : "+Orientation$
IF XRes$<>"" THEN PRINT "X-resolution : "+XRes$+" dots per "+RU$
IF YRes$<>"" THEN PRINT "Y-resolution : "+YRes$+" dots per "+RU$
IF ImWidth$<>"" THEN PRINT "Image Width : "+ImWidth$
IF ImHeight$<>"" THEN PRINT "Image Height : "+ImHeight$
IF ShutSpd$<>"" THEN PRINT "Shutter speed used: "+ShutSpd$
IF ActDiaphragm$<>"" THEN PRINT "Diaphragm used : "+ActDiaphragm$
IF Bias$<>"" THEN PRINT "Exposure bias : "+Bias$
IF SubDist$<>"" THEN PRINT "Distance to focus point : "+SubDist$
IF Org_Date$<>"" THEN PRINT "Image taken : Date "+Org_Date$+" Time "+Org_Time$
IF Mod_Date$<>"" THEN PRINT "Image last modified : Date "+Mod_Date$+" Time "+Mod_Time$
PRINT "+----------+"
PRINT "| GPS INFO |"
PRINT "+----------+"
IF GPSInfo$="OK" THEN
	IF GPSVersion$<>"" THEN PRINT "GPS Version : "+GPSVersion$
	IF GPSLat$<>"" THEN PRINT "GPS latitude : "+GPSLatRef$+" "+GPSLat$
	IF GPSLong$<>"" THEN PRINT "GPS longitude : "+GPSLongRef$+" "+GPSLong$
	IF GPSAlt$<>"" THEN PRINT "GPS Altitude : "+GPSAlt$+GPSAltRef$
PRINT "==========================================="
	FOR i=1 TO 6
		PRINT CHR$(9758);
	NEXT i
	PRINT "SHOW THIS LOCATION ON MAP";
	FOR i=1 TO 5
		PRINT CHR$(9756);
	NEXT i
	PRINT CHR$(9756)
ELSE
	PRINT "No GPS Info found"
ENDIF
PRINT "==========================================="
PRINT "********Tap Screen to View Picture*********"
PRINT "*********Scroll to view more INFO**********"
PRINT "==========================================="

TIMER.SET 500
DO
	IF ShowGraph=1 & ChangeGr=1 THEN
		GR.FRONT 1
		ChangeGr=0
	ENDIF
	IF ShowGraph=0 & ChangeGr=1 THEN
		GR.FRONT 0
		ChangeGr=0
	ENDIF
UNTIL Kap=1
TIMER.CLEAR
GR.CLS
GR.FRONT 1
GR.RENDER
! Quit or New file ?
GR.SET.STROKE 3
GR.COLOR 255,0,255,0,0
GR.RECT g,10,60,200,100
GR.COLOR 255,255,0,0,0
GR.RECT g,210,60,400,100
FOR i=11 TO 199
	GR.COLOR 255,0,i+30,0,1
	GR.LINE g,i,61,i,99
	GR.COLOR 255,i+30,0,0,1
	GR.LINE g,i+200,61,i+200,99
NEXT i
GR.COLOR 255,255,255,0,1
GR.TEXT.DRAW g,30,90,"Get new Pic"
GR.TEXT.DRAW g,265,90,"QUIT"
GR.RENDER
WaitChoice:
DO
	GR.TOUCH Touched,x,y
	x/=sx
	y/=sy
UNTIL Touched
IF y>60 & y<100 THEN
	IF x>10 & x<200 THEN
		CLS
		GR.CLS
		GR.RENDER
		GOTO NwPic
	ENDIF
	IF x>210 & x<400 THEN
		GOTO NoMore
	ENDIF
ENDIF
GOTO WaitChoice
NoMore:
SENSORS.CLOSE
WAKELOCK 5
GR.CLOSE
EXIT

ONTIMER:
	OrientTemp=Orientation()
	IF OrientTemp<>ActualOrient & ShowGraph=1 THEN
		ActualOrient=OrientTemp
		GR.SCREEN w,h
		ScaleX=720
		ScaleY=h/w*ScaleX
		sx=w/ScaleX
		sy=h/ScaleY
		GR.SCALE sx,sy
		GR.BITMAP.SIZE Pic,ImW,ImH
		GR.CLS
		GR.BITMAP.SCALE ScPic,Pic,ImW/ImH*ScaleY*0.6,ScaleY*0.6
		GR.BITMAP.DRAW g,ScPic,30,50
		IF ShowMap=1 THEN
			GR.BITMAP.SCALE bmp2,bmp1,ImW/ImH*ScaleY*0.6,ScaleY*0.6
			GR.BITMAP.DRAW bmpPtr,bmp2,30,50
		ENDIF
		GR.TEXT.SIZE 25
		GR.COLOR 255,255,255,0,1
		GR.TEXT.DRAW g,30,25,F$
		GR.TEXT.DRAW g,30,ScaleY*0.6+75,"Tap Screen for INFO"
		GR.TEXT.DRAW g,30,ScaleY*0.6+95,"Use BackKey to quit"
		GR.RENDER
	ENDIF
TIMER.RESUME

ONGRTOUCH:
	ShowGraph=0
	ChangeGr=1
GR.ONGRTOUCH.RESUME

ONCONSOLETOUCH:
	CONSOLE.LINE.TOUCHED LineNr
	CONSOLE.LINE.TEXT LineNr,Line$
	ShowMap=0
	IF IS_IN("ON MAP",Line$)>0 THEN
		SOCKET.MyIP Connected$
		IF Connected$<>"" THEN
			IF ImW<ImH THEN
				MW$="400"
				MH$=STR$(ROUND(ImH/ImW*400))
				MH$=LEFT$(MH$,LEN(MH$)-2)
				MH$=REPLACE$(MH$," ","")
			ELSE
				MW$=STR$(ROUND(ImW/ImH*400))
				MW$=LEFT$(MW$,LEN(MW$)-2)
				MW$=REPLACE$(MW$," ","")
				MH$="400"
			ENDIF
			url$="http://maps.googleapis.com/maps/api/staticmap?center="
			url$=url$+GPSLat$+","+GPSLong$
			url$=url$+"&zoom=13&size="
			url$=url$+MW$+"x"+MH$
			url$=url$+"&markers=color:red%7Clabel:P%7C"
			url$=url$+GPSLat$+","+GPSLong$
			url$=url$+"&sensor=false"
			BYTE.OPEN r,xx,url$
			BYTE.COPY xx,"../../Map.png"
			BYTE.CLOSE xx
			GR.FRONT 1
			GR.BITMAP.LOAD bmp1,"../../Map.png"
			GR.RENDER
			FILE.DELETE xx,"../../Map.png"
			ShowMap=1
		ELSE
			POPUP "NO INTERNET CONNECTION TO SHOW MAP!",0,0,1
		ENDIF
	ENDIF
	ShowGraph=1
	ActualOrient=0
	ChangeGr=1
CONSOLETOUCH.RESUME

ONBACKKEY:
	TONE 1000,100,0
	Kap=1
BACK.RESUME

ReadTag:
	! Read TAG
	GOSUB Read2Bytes
	Tag=Calc2Value(Align$,ByteIn01,ByteIn02)
	Tag$=UPPER$(RIGHT$("000"+HEX$(Tag),4))
	! Read data format
	GOSUB Read2Bytes
	DFormat=Calc2Value(Align$,ByteIn01,ByteIn02)
	BytesPerComponent=NoB[DFormat]
	! Read number of components
	GOSUB Read4Bytes
	NumComp=Calc4Value(Align$,ByteIn01,ByteIn02,ByteIn03,ByteIn04)
	GOSUB Read4Bytes
	DataOffset=Calc4Value(Align$,ByteIn01,ByteIn02,ByteIn03,ByteIn04)
	IF NumComp*BytesPerComponent>4 THEN
		! Next 4 bytes are offset to data
		BYTE.POSITION.GET JPGFile,FPos
		BYTE.POSITION.SET JPGFile,StartPos+DataOffset
		! Get data from offset
		BYTE.READ.BUFFER JPGFile,NumComp*BytesPerComponent,Data$
		BYTE.POSITION.SET JPGFile,FPos
	ENDIF
RETURN

Read2Bytes:
	BYTE.READ.BYTE JPGFile,ByteIn01
	BYTE.READ.BYTE JPGFile,ByteIn02
RETURN

Read4Bytes:
	BYTE.READ.BYTE JPGFile,ByteIn01
	BYTE.READ.BYTE JPGFile,ByteIn02
	BYTE.READ.BYTE JPGFile,ByteIn03
	BYTE.READ.BYTE JPGFile,ByteIn04
RETURN

GetRational:
	Numerator=Calc4Value(Align$,ASCII(MID$(Data$,1,1)),ASCII(MID$(Data$,2,1)),ASCII(MID$(Data$,3,1)),ASCII(MID$(Data$,4,1)))
	Denominator=Calc4Value(Align$,ASCII(MID$(Data$,5,1)),ASCII(MID$(Data$,6,1)),ASCII(MID$(Data$,7,1)),ASCII(MID$(Data$,8,1)))
RETURN

GetFile:
	ARRAY.DELETE d1$[]
	FILE.DIR Path$,d1$[]
	ARRAY.LENGTH Length,d1$[]
	! How many JPG's
	NumJPG=0
	FOR i=1 TO Length
		IF LOWER$(RIGHT$(d1$[i],4))=".jpg" | IS_IN("(d)",d1$[i])>0 THEN NumJPG=NumJPG+1
	NEXT i
	ARRAY.DELETE d2$[]
	DIM d2$[NumJPG+1]
	d2$[1] = ".."
	q=0
	FOR i=1 TO Length
		IF LOWER$(RIGHT$(d1$[i],4))=".jpg" | IS_IN("(d)",d1$[i])>0 THEN
			q=q+1
			d2$[q+1]=d1$[i]
		ENDIF
	NEXT i
	SELECT s,d2$[],"Select a picture","Tap .. to move up one directory"
	IF s>1 THEN
		n=IS_IN("(d)",d2$[s])
		IF n=0 THEN GOTO FileChosen
		DName$=LEFT$(d2$[s],n-1)
		Path$=Path$+DName$+"/"
		GOTO GetFile
	ENDIF
	IF Path$="" THEN
		Path$ = "../"
		GOTO GetFile
	ENDIF
	ARRAY.DELETE p$[]
	SPLIT p$[],Path$,"/"
	ARRAY.LENGTH Length,p$[]
	IF p$[Length]=".."
		Path$=Path$+"../"
		GOTO GetFile
	ENDIF
	IF Length=1
		Path$=""
		GOTO GetFile
	ENDIF
	Path$=""
	FOR i=1 TO Length - 1
		Path$=Path$+p$[i]+"/"
	NEXT i
	GOTO GetFile
FileChosen:
	F$=d2$[s]
	FName$=Path$+F$
RETURN