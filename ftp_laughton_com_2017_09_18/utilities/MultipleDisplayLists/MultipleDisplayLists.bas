% demo showing the use of two display lists!

delay=250

gr.open 255,0,0,0
gr.color 255,255,255,255,1
gr.text.size 50
numberofcircles = 10
dim circles[numberofcircles]

for n = 1 to numberofcircles-1
gosub getrandom
gr.circle circles[n], x, y, 50
next n
gr.color 255,255,0,255,1
gr.text.draw circles[n],60,200,"Or use to make flashy text!"

numberofsquares = 10
dim squares[numberofsquares]

for n = 1 to numberofsquares-1
gosub getrandom
gr.rect squares[n], x,y,x+50,y+50
next n

gr.color 255,255,255,0,1
gr.text.draw squares[n],60,200,"Or use to make flashy text!"

loop:
gr.newdl circles[]
gr.render
pause delay
gr.newdl squares[]
gr.render
pause delay

goto loop

getrandom:
x=rnd()*800
y=rnd()*480
r=rnd()*255
g=rnd()*255
b=rnd()*255
gr.color 255,r,g,b,1
return