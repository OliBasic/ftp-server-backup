REM Start of BASIC! Program
REM Module: ticker
REM functions list:
REM
REM ticker_create (y,text$)
REM x Coordinate will bet set automatically
REM y Coordinate on screen
REM text$ Text to run
REM
REM ticker_update (pace)
REM Actually the speed 
REM at which the ticker
REM text moves from right
REM to left
REM
REM filename to download / INCLUDE:
REM fn_ticker.bas

!dim xpos[1] % have to defined after fn.def ?

fn.def ticker_create (y,text$,xpos[],size[])
gr.screen width, height
gr.text.size 25
gr.text.skew -0.25
xpos[1] = width + 5
gr.text.width size[1], text$ % detects text width
gr.text.draw tic1,xpos[1],y,text$
gr.render
fn.rtn tic1
fn.end

fn.def ticker_update (thandle, pace,xpos[],size[])
xpos[1] = xpos[1] - 3  % Set the x step

if xpos[1] < (0 - size[1])  % has text disappeared ?
gr.screen width, height
xpos[1] = width + 5
endif
gr.modify thandle,"x",xpos[1] % Change text x value
gr.render
pause pace
fn.rtn 1
fn.end

! --- --- --- Main prg --- --- --- 

gr.open 255,255,255,255
gr.color 255,244,44,0,1
gr.orientation 1
gr.screen xw,yh
dim xpos[1] % try to define a GLOBAL x variable
dim size[1] % try to define a GLOBAL text width var

tic = ticker_create(yh-50,"Hallo. This is the first ticker text running on BASIC! I hope you can enjoy it....",xpos[],size[])

do
rb = ticker_update(tic,1,xpos[],size[])


until 0

gr.close
end










 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
