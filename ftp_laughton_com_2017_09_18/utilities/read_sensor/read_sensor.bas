%***************************************************
%*  Orientation  *     V1.00     *    2015/02/23   *
%***************************************************
%* 							UP
%* 					7		8		9
%*           LEFT	4		5		6	RIGHT
%* 					1		2		3
%* 						   DOWN
%***************************************************

GR.OPEN 255, 128, 128, 128							% Open Screen Window / Gray Background
GR.ORIENTATION -1									% Display Landscape (0) or Portrait (1)
SENSORS.OPEN 3										% Activate Gyroscope Sensors
WAKELOCK 3											% Bright Screen enable
PAUSE 1000											% Delay require for Orientation update

GR.TEXT.TYPEFACE 2									% Select Font MonoSpace
GR.TEXT.SIZE 64          							% will give 80 x 26 chrs
GR.TEXT.ALIGN 1										% Align Text Left
GR.TEXT.BOLD 1										% Bold Enable
GR.TEXT.UNDERLINE 0									% Underline Disable
GR.TEXT.STRIKE 0									% StrikeOut Disable

FN.DEF Gyroscope()

	SENSORS.READ 3, Axe_Z,Axe_Y,Axe_X
	
	If ROUND(ABS(Axe_X))<ROUND(ABS(Axe_Y))			% If Android in Vertical (Portrait)
		%Axe_X Validation
		IF (Axe_X>10) then 
		   Position = 4
		ELSEIF (Axe_X>=-5) & (Axe_X<=10) then 
		   Position = 5
		ELSEIF (Axe_X<-6) then 
		   Position = 6
		ENDIF

		%Axe_Y Validation
		IF (Axe_Y<-65) then 
		   Position -= 3
		ELSEIF (Axe_Y>-45) Then  
		   Position += 3
		ENDIF
	Else											% If Android in Horizontal (Landscape)
		%Axe_Y Validation
		IF (Axe_Y>6) then 
		   Position = 4
		ELSEIF (Axe_Y>=-5) & (Axe_Y<=6) then 
		   Position = 5
		ELSEIF (Axe_Y<-5) then 
		   Position = 6
		ENDIF

		%Axe_X Validation
		IF (Axe_X<38) then 
		   Position += 3
		ELSEIF (Axe_X>55) Then  
		   Position -= 3
		ENDIF
	Endif
	
	GR.CLS
	GR.COLOR 255,255,255,255,1
	GR.TEXT.DRAW Msg, 100, 175, "Axe_X: "+STR$(INT(Axe_X))
	GR.TEXT.DRAW Msg, 100, 250, "Axe_Y: "+STR$(INT(Axe_Y))
	GR.COLOR 255,128,128,255,1
	GR.TEXT.DRAW Msg, 100, 500, "Action: "+STR$(Position)
	GR.COLOR 255,255,255,000,1
	GR.TEXT.DRAW Msg, 100, 800, "Touch the screen to Quit"
	GR.RENDER

	FN.Rtn(Position)
FN.END

!*************************************************************
!** Main loop
!*************************************************************

Init=0
ARRAY.LOAD Pattern[], 1,200
do
    Init=1
    Action=Gyroscope()
	If Action <> 5 Then TONE Action*100,200
	If Action  > 6 Then VIBRATE Pattern[], -1		
    gr.touch touched, x, y
until touched

SENSORS.CLOSE
END
