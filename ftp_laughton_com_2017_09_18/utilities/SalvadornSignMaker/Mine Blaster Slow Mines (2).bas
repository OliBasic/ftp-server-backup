fn.def removeobject (objectnumber)
    gr.group.getDL allobjects
        list.search allobjects, objectnumber, target
        list.remove allobjects, target
    Gr.group.newDL allobjects
fn.end

gr.open ,0,0,0


gr.orientation 0
gamewidth = 1920
gameheight = 1080
gr.screen devicewidth, deviceheight
scale_width = devicewidth / gamewidth
scale_height = deviceheight / gameheight
rem gr.scale scale_width, scale_height

gr.color ,255,255,255,1
gr.set.stroke 1


gr.rect leftbutton, 10, 900, 160, 1050
gr.rect rightbutton, 1750, 900, 1910, 1050
gr.rect thrustbutton, 10, 700, 160, 850
gr.rect firebutton, 1750, 700, 1910, 850

gr.color  ,0,0,0,1
gr.text.size 100
gr.text.typeface 4,2
gr.text.draw l, 50, 1000, "L"
gr.text.draw th, 50, 800, "T"
gr.text.draw r, 1790, 1000, "R"
gr.text.draw f, 1790, 800, "F"
gr.render


gr.color ,255,255,255,0
gr.set.stroke 5
array.load triangle[], 1000, 540, 1020, 600, 1000, 580, 980, 600
list.create n, shipshape
list.add.array shipshape, triangle[]
angle = 0


gr.render

shipx = 0
shipy = 0
speedx = 0
speedy = 0

soundpool.open 10
rem soundpool.load rocket, "Thrust.wav" 
soundpool.load laser, "Laser.wav"
rem "sdcard/rfo-basic/data/Thrust.wav"
audio.load rocket, "Rocket.mp3"
thrustflag = 1
list.create n, bulletx
list.create n, bullety
list.create n, bulletxspeed
list.create n, bulletyspeed
list.create n, bulletobject
list.create n, mineobject
list.create n, minespeedx
list.create n, minespeedy
lastfire = time()


for thismine = 1 to 8
    newminex = 1000
    newminey = 570
    do
        newminex = int(rnd()*1590) + 160
    until (newminex < 800) | (newminex > 1200)
    do 
        newminey = int(rnd()*1080) 
    until (newminey < 470) | (newminey > 670)
    gr.circle newmine, newminex, newminey, 12
    list.add mineobject, newmine
    list.add minespeedx, 0
    list.add minespeedy, 0
next
    
while 1 = 1
    now = time()
    audio.isdone thrustflag
    gr.bounded.touch lefttouch, 10, 900, 160,1050
    gr.bounded.touch2 lefttouch2, 10, 900, 160, 1050
    if lefttouch | lefttouch2 then angle = angle - 5
    gr.bounded.touch righttouch, 1750, 900, 1910, 1050 
    gr.bounded.touch2 righttouch2, 1750, 900, 1910, 1050
    if righttouch | righttouch2 then angle = angle + 5 
    gr.bounded.touch firetouch, 1750, 700, 1910, 850
    gr.bounded.touch2 firetouch2, 1750, 700, 1910, 850
    if firetouch | firetouch2 then 
        if (now > (lastfire + 1000)) then 
            soundpool.play laserfire,laser, 0.9, 0.9, 1, 1, 1 
            lastfire = now
            newbulletx = 1000 + shipx + cos(toradians(angle-90)) * 45
            list.add bulletx, newbulletx
            newbullety = 570 + shipy + sin(toradians(angle-90)) * 45
            list.add bullety, newbullety
            newbulletxspeed = speedx + cos(toradians(angle-90)) * 10
            list.add bulletxspeed, newbulletxspeed
            newbulletyspeed = speedy + sin(toradians(angle-90)) * 10
            list.add bulletyspeed, newbulletyspeed
            gr.circle newbullet, newbulletx, newbullety, 2
            list.add bulletobject, newbullet
        end if
    end if    
    list.size bulletobject, shots
    takeout = 0
    for count = 1 to shots
        bullet = count - takeout
        list.get bulletobject, bullet, thisbullet
        list.get bulletx, bullet, thisbulletx
        list.get bullety, bullet, thisbullety
        list.get bulletxspeed, bullet, thisbulletxspeed
        list.get bulletyspeed, bullet, thisbulletyspeed
        thisbulletx = thisbulletx + thisbulletxspeed
        thisbullety = thisbullety + thisbulletyspeed
        bulletout = 1
        if ((thisbulletx > 160) & (thisbulletx < 1750)) then
            if ((thisbullety > 0) & (thisbullety < 1080)) then
                gr.move thisbullet, thisbulletxspeed, thisbulletyspeed    
                bulletout = 0
            end if
        end if
        if bulletout = 1 then
            gr.hide thisbullet
            list.get bulletobject, bullet, target
            removeobject(target)
            list.remove bulletobject, bullet
            list.remove bulletx, bullet
            list.remove bullety, bullet
            list.remove bulletxspeed, bullet
            list.remove bulletyspeed, bullet
            takeout = takeout + 1
        end if
    next
    gr.bounded.touch thrusttouch, 10, 700, 160, 850
    gr.bounded.touch2 thrusttouch2, 10, 700, 160, 850
    if thrusttouch | thrusttouch2 then 
        speedx = speedx + cos(toradians(angle-90))/2
        speedy = speedy + sin(toradians(angle-90))/2
        if thrustflag = 1 then audio.play rocket
    else 
        audio.stop
    endif
    shipx = shipx + speedx
    if shipx > 750 then shipx = 160 - 1000
    if shipx < 160 - 1000 then shipx = 750
    shipy = shipy + speedy
    if shipy + 570  > 1080 then shipy = -570
    if shipy + 570 < 0 then shipy = 1080 - 570
    gr.rotate.start angle, (1000 + shipx), (570 + shipy)
    gr.poly ship, shipshape, shipx, shipy
    if thrustflag = 0 then 
        gr.set.stroke 15
        gr.line flame, (1000 + shipx), (580 + shipy), (1000 + shipx), (600 + shipy)
        gr.set.stroke 5
    end if
    gr.rotate.end    
    
    for thismine = 1 to 8
        list.get mineobject, thismine, movemine
        list.get minespeedx, thismine, xspeed
        list.get minespeedy, thismine, yspeed
        gr.get.position movemine, minex, miney
        realshipx = shipx + 1000
        realshipy = shipy + 570
        minexdistance = realshipx - minex
        mineydistance = realshipy - miney
        minedistance = hypot(minexdistance, mineydistance)
        if minedistance > 0 then
            xaccel = minexdistance / minedistance / 1
            yaccel = mineydistance / minedistance / 1
        end if
        newdistance = hypot((minexdistance+xspeed+xaccel),(mineydistance+yspeed+yaccel))
      if abs(newdistance - minedistance) > 5 then 
            xaccel = 0
            yaccel = 0
        end if
rem        if (newdistance > minedistance) then
 rem           xaccel = xaccel * 2
   rem         yaccel = yaccel * 2

    rem    end if
        xspeed = xspeed + xaccel
        yspeed = yspeed + yaccel
        minex = minex + xspeed
        miney = miney + yspeed
        if minex < 160 then minex = 1750
        if minex > 1750 then minex = 160
        if miney < 0 then miney = 1080
        if miney > 1080 then miney = 0
        rem if (minex < 160 & xspeed < 0) | (minex > 1750 & xspeed > 0) then xspeed = 0
        rem if (miney < 0 & yspeed < 0) | (miney > 1080 & yspeed > 0) then yspeed = yspeed = 0
        gr.modify movemine, "x", minex, "y", miney
        list.replace minespeedx, thismine, xspeed
        list.replace minespeedy, thismine, yspeed
    next
    
    gr.render
    gr.hide ship 
    gr.hide flame
    removeobject(ship)
    removeobject(flame)
repeat




