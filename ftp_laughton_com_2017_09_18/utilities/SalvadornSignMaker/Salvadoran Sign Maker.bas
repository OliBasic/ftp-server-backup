INCLUDE "GW.bas"


Fn.Def GetLength (Files, Sizes, ThisLine$, AllChars$)
  Let Place = 1
  For Count = 1 to len (ThisLine$)
    Let ThisChar$ = MID$(ThisLine$, Count, 1)
    Let ThisChar = is_in (ThisChar$, AllChars$)
    If (ThisChar < 1) then ThisChar = 11 %Replace unsupported characters with spaces
    If (ThisChar$ = "\"") & (Count > 1) then 
      Let Previous$ = Mid$ (ThisLine$, (Count - 1), 1)
      if Previous$ <> " " then
        Let ThisChar = ThisChar + 1% Change to closed quotes if the previous space is not blank
      Endif
    Endif
    list.get Sizes, ThisChar, Size
    Let Place = Place + Size
  Next
  Fn.Rtn Place
  
 Fn.End
 
  
Fn.def MakeBitmap (LineLength, Sizes, Options[])
  
  list.size LineLength, #lines
  Let MaxLine = 100
  For Row = 1 to #lines
    list.get LineLength, Row, ThisLineLength
    if ThisLineLength > MaxLine then Maxline = ThisLineLength
  Next
  Let SignWidth = Int (MaxLine + Maxline / (Options[1] - 1)) %Add margins
  Let SignHeight = Int (SignWidth / Options[1] * Options[2])
  Let NeededHeight = Int (#lines * 625 * 1.10) % Make sure the characters will fit with line spacing and margins
  If NeededHeight > SignHeight then
    SignWidth = SignWidth * NeededHeight / SignHeight
    SignHeight = NeededHeight
  Endif    
  gr.bitmap.create Sign, SignWidth, SignHeight
  list.add Sizes, SignWidth % Used for calculating ratio
  Let NewWidth = SignWidth
  Let NewHeight = SignHeight
  %Let divisor = 1
  While Sign < 1 % Reduce the size of the bitmap to fit memory
    Let NewWidth = Int (NewWidth * 0.95)
    Let NewHeight = Int (NewHeight * 0.95)
    gr.bitmap.create Sign, NewWidth, NewHeight
  Repeat
  Fn.Rtn Sign
Fn.End

Fn.def Proportion (Sign, CharSize) % Get the ratio to scale the stamps.
  list.size CharSize, LastSize
  list.get Charsize, LastSize, OriginalSignWidth % The original sign width was stored in the last position of CharSize.
  gr.bitmap.size Sign, x, y
  Let Ratio = x / OriginalSignWidth
  fn.rtn Ratio
Fn.end
  

Fn.Def DrawLine (Sign, CharFile, CharSize, ThisLine$, LineNumber, LineLength, AllChars$, Options[])
  Let Finished = 1
  Let Ratio = Proportion (Sign, CharSize)
  list.size LineLength, Rows
  list.get LineLength, LineNumber, ThisLineLength
  Let ThisLineLength = Int (ThisLineLength * Ratio)
  gr.bitmap.size Sign, SignWidth, SignHeight
  Let BetweenLines = (SignHeight - (Rows * 625 * Ratio)) / (Rows + 1)
  If Options [3] = 2 then Let StartX = Int (SignWidth / 2) - Int (ThisLineLength / 2) %Centered
  If Options [3] = 1 then Let StartX = Int(SignWidth /Options [1] / 2)  %Left Justified
  If Options [3] = 3 then Let StartX = SignWidth - ThisLineLength - Int(SignWidth /Options [1] / 2)% Right Justified
  
  
  If Rows = 1 then
    Let StartY = Int (SignHeight / 2) - Int (300 * Ratio)
  Else
    Let StartY = Betweenlines * LineNumber + ((LineNumber - 1) * 625 * Ratio)
  Endif
  Let Place = StartX
  For Count = 1 to len (ThisLine$)
    Let ThisChar$ = MID$(ThisLine$, Count, 1)
    Let ThisChar = is_in (ThisChar$, AllChars$)
    If (ThisChar < 1) then ThisChar = 11 %Replace unsupported characters with spaces
    If (ThisChar$ = "\"") & (Count > 1) then 
      Let Previous$ = Mid$ (ThisLine$, (Count - 1), 1)
      if Previous$ <> " " then
        Let ThisChar = ThisChar + 1 % Change to closed quotes if the previous space is not blank
      Endif
    Endif
  list.get CharFile, ThisChar, Filename$
  If Ratio = 1 then gr.bitmap.load Stamp, FileName$ %Save memory by only loading graphics as needed
  If Ratio <> 1 then % Reduce Stamp size to fit Sign if necessary
    gr.bitmap.load Stamp1, Filename$
    if Stamp1 < 0 then
      Let Finished = 0
      F_N.break
    Endif
    gr.bitmap.size Stamp1, x, y
      Let x = Int (x * ratio)
      Let y = Int (y * ratio)
    gr.bitmap.scale Stamp, Stamp1, x, y
      gr.bitmap.delete Stamp1
  EndIF
  If Stamp < 0 | Stamp1 < 0 then 
    Let Finished = 0
    F_N.break
  EndIf
  gr.bitmap.draw X, Stamp, Place, StartY
  gr.bitmap.delete Stamp %Release memory
  list.get CharSize, ThisChar, Size
  Let Place = Int(Place + Size * ratio + 1)
Next

Fn.rtn Finished

Fn.End

Fn.Def Extension (FileName$)
  Correct = 1
  Extension$ = Upper$ (Right$ (FileName$, 3))
  If Extension$ <> "JPG" & Extension$ <> "PNG" Then Correct = 0
  Fn.Rtn Correct
Fn.End

Fn.Def ShrinkSign (Sign)
  Do
    gr.bitmap.size sign, x, y
    gr.bitmap.delete Sign
    let x = x * 0.9
    let y = y * 0.9
    gr.bitmap.create Sign, x, y
  Until Sign > 1
Fn.Rtn Sign
Fn.End

list.create s, CharFile
list.create n, CharSize
list.create s, Lines

list.add CharFile, "1.jpg"
list.add CharSize, 363
list.add CharFile, "2.jpg"
list.add CharSize, 480
list.add CharFile, "3.jpg"
list.add CharSize, 477
list.add CharFile, "4.jpg"
list.add CharSize, 419
list.add CharFile, "5.jpg"
list.add CharSize, 505
list.add CharFile, "6.jpg"
list.add CharSize, 403
list.add CharFile, "7.jpg"
list.add CharSize, 458
list.add CharFile, "8.jpg"
list.add CharSize, 463
list.add CharFile, "9.jpg"
list.add CharSize, 422
list.add CharFile, "0.jpg"
list.add CharSize, 516
list.add CharFile, "Space.jpg"
list.add CharSize, 300
list.add CharFile, "A.jpg"
list.add CharSize, 551
list.add CharFile, "B.jpg"
list.add CharSize, 505
list.add CharFile, "C.jpg"
list.add CharSize, 471
list.add CharFile, "D.jpg"
list.add CharSize, 431
list.add CharFile, "E.jpg"
list.add CharSize, 524
list.add CharFile, "F.jpg"
list.add CharSize, 503
list.add CharFile, "G.jpg"
list.add CharSize, 456
list.add CharFile, "H.jpg"
list.add CharSize, 567
list.add CharFile, "I.jpg"
list.add CharSize, 268
list.add CharFile, "J.jpg"
list.add CharSize, 466
list.add CharFile, "K.jpg"
list.add CharSize, 464
list.add CharFile, "L.jpg"
list.add CharSize, 459
list.add CharFile, "M.jpg"
list.add CharSize, 600
list.add CharFile, "N.jpg"
list.add CharSize, 619
list.add CharFile, "O.jpg"
list.add CharSize, 487
list.add CharFile, "P.jpg"
list.add CharSize, 478
list.add CharFile, "Q.jpg"
list.add CharSize, 530
list.add CharFile, "R.jpg"
list.add CharSize, 532
list.add CharFile, "S.jpg"
list.add CharSize, 458
list.add CharFile, "T.jpg"
list.add CharSize, 563
list.add CharFile, "U.jpg"
list.add CharSize, 560
list.add CharFile, "V.jpg"
list.add CharSize, 631
list.add CharFile, "W.jpg"
list.add CharSize, 692
list.add CharFile, "X.jpg"
list.add CharSize, 587
list.add CharFile, "Y.jpg"
list.add CharSize, 519
list.add CharFile, "Z.jpg"
list.add CharSize, 513
list.add CharFile, "Parentheses Begin.jpg"
list.add CharSize, 309
list.add CharFile, "Parentheses End.jpg"
list.add CharSize, 325
list.add CharFile, "period.jpg"
list.add CharSize, 155
list.add CharFile, "Comma.jpg"
list.add CharSize, 220
list.add CharFile, "Exclamation Point Begin.jpg"
list.add CharSize, 236
list.add CharFile, "Exclamation Point End.jpg"
list.add CharSize, 248
list.add CharFile, "Question Mark Begin.jpg"
list.add CharSize, 336
list.add CharFile, "Question Mark End.jpg"
list.add CharSize, 314
list.add CharFile, "Colon.jpg"
list.add CharSize, 173
list.add CharFile, "Apostrophe.jpg"
list.add CharSize, 209
list.add CharFile, "Hashtag.jpg"
list.add CharSize, 428
list.add CharFile, "And.jpg"
list.add CharSize, 626
list.add CharFile, "Asterisk.jpg"
list.add CharSize, 317
list.add CharFile, "At.jpg"
list.add CharSize, 649
list.add CharFile, "Bar.jpg"
list.add CharSize, 186	
list.add CharFile, "Cents.jpg"
list.add CharSize, 305		
list.add CharFile, "Open Bracket.jpg"
list.add CharSize, 258
list.add CharFile, "Close Bracket.jpg"
list.add CharSize, 258		
list.add CharFile, "Open Squiggle.jpg"
list.add CharSize, 308		
list.add CharFile, "Close Squiggle.jpg"
list.add CharSize, 311	
list.add CharFile, "Dash.jpg"
list.add CharSize, 219
list.add CharFile, "Double Greater Than.jpg"
list.add CharSize, 473
list.add CharFile, "Double Less Than.jpg"
list.add CharSize, 473	
list.add CharFile, "Equals.jpg"
list.add CharSize, 338		
list.add CharFile, "Euro.jpg"	
list.add CharSize, 619
list.add CharFile, "Percent.jpg"	
list.add CharSize, 537	
list.add CharFile, "Plus.jpg"	
list.add CharSize, 438	
list.add CharFile, "Slash.jpg"
list.add CharSize, 334		
list.add CharFile, "Underscore.jpg"
list.add CharSize, 339		
list.add CharFile, "Yen.jpg"
list.add CharSize, 588		
list.add CharFile, "Backwards Apostrophe.jpg"
list.add CharSize, 206		
list.add CharFile, "CopyRight.jpg"
list.add CharSize, 521	
list.add CharFile, "Degree.jpg"
list.add CharSize, 160
list.add CharFile, "Dollar.jpg"
list.add CharSize, 430
list.add CharFile, "Greater Than.jpg"
list.add CharSize, 387
list.add CharFile, "Less Than.jpg"
list.add CharSize, 387
list.add CharFile, "Mid Dot.jpg"
list.add CharSize, 160	
list.add CharFile, "Pound.jpg"
list.add CharSize, 560
list.add CharFile, "Reserved.jpg"
list.add CharSize, 540
list.add CharFile, "TM.jpg"
list.add CharSize, 394 
list.add CharFile, "Semicolon.jpg"
list.add CharSize, 220
list.add CharFile, "Caret.jpg"
list.add CharSize, 436
list.add CharFile, "Tilde.jpg"
list.add CharSize, 412
list.add CharFile, "Quotes Begin.jpg"
list.add CharSize, 443
list.add CharFile, "Quotes End.jpg"
list.add CharSize, 436
list.add CharFile, "Quotes End.jpg"
list.add CharSize, 436

Let AllChars$ = "1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZ().,¡!¿?:'#&*@|¢[]{}-»«=€%+/_¥`©°$><·£®™;^~\""

Array.Load Options [], 17, 11, 2

MainPage = GW_NEW_PAGE()
GW_USE_THEME_CUSTO_ONCE ("fit-screen")
GW_ADD_IMAGE (MainPage, "Salvadoran Sign Maker Title.png")
GW_ADD_BUTTON (MainPage, "Enter Text", "Enter Text")
GW_ADD_BUTTON (MainPage, "Select Options", "Select Options")
GW_ADD_BUTTON (MainPage, "Instructions", "Instructions")
GW_ADD_BUTTON (MainPage, "About", "About")
GW_ADD_BUTTON (MainPage, "Exit", "Exit")

Manual = GW_NEW_PAGE()
GW_ADD_TITLEBAR (Manual, "Instructions")
Text.open r, Instructext, "Instructions.txt"
DO
  Text.ReadLN Instructext, Line$
  If Line$ <> "" & Line$ <> "EOF" then GW_ADD_TEXT (Manual, Line$)
Until Line$ = "EOF"
Text.close Instructext
GW_ADD_BUTTON (Manual, "Continue", "Continue")

Info = GW_NEW_PAGE()
GW_USE_THEME_CUSTO_ONCE ("fit-screen")
GW_ADD_IMAGE (Info, "About Header Graphics.png")
Text.open r, AboutText, "About.txt"
DO
  Text.ReadLN AboutText, Line$
  If Line$ <> "" & Line$ <> "EOF" then GW_ADD_TEXT (Info, Line$)
Until Line$ = "EOF"
Text.close AboutText
GW_ADD_BUTTON (Info, "Continue", "Continue")

MainLoop:

GW_RENDER (MainPage)
r$ = GW_WAIT_ACTION$()
If r$ = "Select Options" then
  GW_CLOSE_PAGE (MainPage)
  Goto Choices
EndIf
If r$ = "Enter Text" then 
  GW_CLOSE_PAGE (MainPage)
  goto EntryPage
EndIf
If r$= "Instructions" then
  GW_CLOSE_PAGE (MainPage)
  Goto Instructions
EndIf
If r$ = "About" then
  GW_CLOSE_PAGE (MainPage)
  Goto About
Endif
If r$ = "Exit" then Goto leave

Goto MainLoop

Choices:
Decimal = Options [1] - Int(Options[1])
Width$ = Int$(Options [1]) + "." + Int$(Decimal * 100)
Decimal = Options [2] - Int(Options[2])
Height$ = Int$(Options [2]) + "." + Int$(Decimal * 100)
Options = GW_NEW_PAGE()
GW_ADD_TITLEBAR (Options, "Dimensions & Justification")
WidthEntry = GW_ADD_INPUTNUMBER (Options, "Width", Width$) %Load previous choices
HeightEntry =GW_ADD_INPUTNUMBER (Options, "Height", Height$)
Array.Load JustChoices$ [], "Left", "Center", "Right"
JustChoices$ [Options[3]] = ">" + JustChoices$ [Options[3]]
JustBox = GW_ADD_SELECTBOX (Options, "Justification: ", JustChoices$[])
GW_ADD_BUTTON (Options, "Default Options", "Default Options")
GW_ADD_BUTTON (Options, "Enter Text", "Enter Text")
GW_ADD_BUTTON (Options, "Main", "Main")
GW_RENDER (Options)

Let r$ = GW_WAIT_ACTION$ ()

If r$ = "Default Options" then 
  Array.Load Options [], 17, 11, 2
  Goto Choices
EndIf

Let Options[1] = ABS (GW_GET_VALUE (WidthEntry))
Let Options[2] = ABS (GW_GET_VALUE (HeightEntry))
Let Options[3] = GW_GET_VALUE (JustBox)
If Options [1] <= 0 | Options [2] <= 0 then %Prevent division by zero error.
  popup "Dimensions must be greater than zero!"
  Goto Choices
EndIf
GW_CLOSE_PAGE (Choices)

If r$ = "Enter Text" then Goto EntryPage

if r$ = "Main" then Goto MainLoop

Goto MainLoop

EntryPage:
Entry = GW_NEW_PAGE()
GW_ADD_TITLEBAR (Entry, "Enter up to 5 lines:")
list.size Lines, KeepLines

If KeepLines > 0 then %The text from the last sign will be loaded
  list.get Lines, 1, Temp$
Else
  Temp$ = ""
EndIf
line1 = GW_ADD_INPUTLINE (Entry,"",Temp$)
If KeepLines > 1 then
  list.get Lines, 2, Temp$
Else
  Temp$ = ""
EndIf
line2 = GW_ADD_INPUTLINE (Entry,"",Temp$)
If KeepLines > 2 then
  list.get Lines, 3, Temp$
Else
  Temp$ = ""
EndIf
line3 = GW_ADD_INPUTLINE (Entry,"",Temp$)
If KeepLines > 3 then
  list.get Lines, 4, Temp$
Else
  Temp$ = ""
EndIf
line4 = GW_ADD_INPUTLINE (Entry,"",Temp$)
If KeepLines > 4 then
  list.get Lines, 5, Temp$
Else
  Temp$ = ""
EndIf
line5 = GW_ADD_INPUTLINE (Entry,"",Temp$)
GW_ADD_BUTTON (Entry, "Make Your Sign", "Submit")
GW_ADD_BUTTON (Entry, "Clear Text", "Clear Text")
GW_USE_THEME_CUSTO_ONCE ("color = b")
GW_ADD_BUTTON (Entry, "Main", "Main")
GW_RENDER(Entry)
r$=GW_WAIT_ACTION$()

list.clear Lines

If r$ = "Clear Text" then goto EntryPage

list.clear Lines
Line1$ = GW_GET_VALUE$ (line1)
if Line1$ = "" then Let Line1$ = " " %prevent errors associated with a 0 X 0 bitmap
Let Line1$ = Upper$ (Line1$)
list.add Lines, Line1$

Line2$ = GW_GET_VALUE$ (line2)
Let Line2$ = Upper$ (Line2$)
If Line2$ <> "" then list.add Lines, Line2$

Line3$ = GW_GET_VALUE$ (line3)
Let Line3$ = Upper$ (Line3$)
If Line3$ <> "" then list.add Lines, Line3$

Line4$ = GW_GET_VALUE$ (line4)
Let Line4$ = Upper$ (Line4$)
If Line4$ <> "" then list.add Lines, Line4$

Line5$ = GW_GET_VALUE$ (line5)
Let Line5$ = Upper$ (Line5$)
If Line5$ <> "" then list.add Lines, Line5$

if r$ = "Main" then GOTO MainLoop
if r$ = "Submit" then 
   GW_CLOSE_PAGE (Entry)
   Popup "Creating Your Sign--Please wait."
   Goto MakeSign 
Endif
 
Goto MainLoop
 
MakeSign:


list.size Lines, #Lines
For Row = 1 to #Lines
  list.get Lines, Row, ThisLine$
Next


gr.open , 0, 0, 0
gr.orientation 1


Complete = 0


%determine width of bitmap
list.create n, LineLength
list.size Lines, #Lines

For Row = 1 to #Lines
  list.get Lines, Row, ThisLine$
  Let ThisLineLength = GetLength (CharFile, CharSize, ThisLine$, AllChars$) 
  list.add LineLength, ThisLineLength
Next

Let Sign = MakeBitmap (LineLength, CharSize, Options[])

While Complete = 0

gr.bitmap.size Sign, SignWidth, SignHeight

Gr.bitmap.drawinto.start Sign
gr.color , 255, 255, 255
gr.rect dummy, 0, 0, SignWidth, SignHeight  %Prevent Autocropping


For Row = 1 to #Lines
  list.size CharFile, Size
  list.get Lines, Row, Line$
  Finished = DrawLine (Sign, CharFile, CharSize, Line$, Row, LineLength, AllChars$, Options[])
  If Finished = 0 then F_N.break
Next

If Finished = 1 then Complete = 1

gr.bitmap.drawinto.end

If Finished = 0 then Sign = ShrinkSign (Sign)


Repeat

gr.render



gr.bitmap.size Sign, SignWidth, SignHeight
gr.screen ScWidth, ScHeight
If ScWidth < ScHeight then
  Let ScMin = ScWidth
Else
  Let ScMin = ScHeight
Endif



Let NewWidth = Int (SignWidth * ScWidth / SignWidth)
Let NewHeight = Int (SignHeight * ScMin / SignWidth)
If NewHeight > ScHeight then
  Let NewWidth = Int (NewWidth * ScHeight / NewHeight)
  Let NewHeight = Int (NewHeight * ScHeight / NewHeight)
EndIf

gr.bitmap.scale ShowSign, Sign, NewWidth, NewHeight

If ShowSign > 0 then
  gr.bitmap.draw x, ShowSign, 1, 1
  gr.render
  Do
    gr.touch Test, x, y
  Until Test
  Dialog.message "Save this sign?", "", decision, "Yes", "No"
Else
  Dialog.message "There is not enough memory to display preview.", "Save this sign anyway?", decision, "Yes", "No"
Endif

If decision = 1 then
  Input "Filename?", FileName$, "My Salvadoran Sign.jpg", cancel
  If Extension (FileName$) = False then FileName$ = FileName$ + ".jpg"
  File.exists taken, FileName$
  Message$ = "Replace " + FileName$ + "?"
  While taken <> 0
    Dialog.message Message$, "", decision, "Yes", "No", "Cancel"
    If decision = 1 | decision = 3 then
      taken = 0
    Else 
      Input "Filename?", FileName$, "My Salvadoran Sign.jpg"
      If Extension (FileName$) = 0 then FileName$ = FileName$ + ".jpg"
      File.exists taken, FileName$
    EndIf
  Repeat
  If decision = 1 then gr.bitmap.save Sign, FileName$
EndIf  

If ShowSign > 0 then gr.bitmap.delete ShowSign
gr.bitmap.delete Sign
gr.close


list.clear LineLength
list.size CharSize, last
list.get Charsize, last, OriginalSignWidth

list.remove Charsize, last % This is the original width of the sign that has been finished.

Goto MainLoop

Instructions:

GW_RENDER (Manual)
r$=GW_WAIT_ACTION$()
GW_CLOSE_PAGE (Manual)
Goto MainLoop

About: 

GW_RENDER (Info)
r$=GW_WAIT_ACTION$()
GW_CLOSE_PAGE (Info)

GW_CLOSE_PAGE (Info)
Goto MainLoop

leave:
End

