! This example shows the different effects
! available for drawing graphical text.
!
! Open Graphics Screen with a White background.

gr.open 255, 255, 255, 255

! Draw a Black text alignment line

gr.color 255,0,0,0,1
gr.line n, 200,0, 200,480

! Set the text color to Red with fill = false

gr.color 255,255, 0, 0, 0

! Set the text size to 30 pixels

gr.text.size 30

! Set the text align to Left = 1

gr.text.align 1

! Create text object at x = 200, y = 120

gr.text.draw P, 2, 40, "Left Aligned"

! Set the text align to Center = 2

gr.text.align 2

! Create text object a x = 200, y = 240

gr.text.draw P, 100, 120, "Center Aligned"

! Set the text align to Right = 3

gr.text.align 3

! Create text object at x = 200, y = 360

gr.text.draw P, 200, 240, "Right Aligned"

! Demonstrate Text Sizes and Styles
! All examples will be aligned Left = 1
! and with Blue (0,0,255) text with fill = false

gr.text.align 1
gr.color 255, 0, 0, 255, 0

! Tiny Text, 10 pixels

gr.text.size 10
x = 220
y = 30

! Create text object at x , y

gr.text.draw P, x, y, "Tiny text"

! Big Text, 50 pixels

gr.text.size  50
y = y +10 + 50

! Create text object at x , y

gr.text.draw blink_ptr, x, y, "Bigger text"

! Using a normal text size,
! Show some Bold, unfilled text

gr.text.size 30
y =  y +25 + 30
gr.text.bold 1

! Create text object at x , y

gr.text.draw BTP, x, y, "Bold, unfilled text"

! Show Bold, filled text. Change the color
! fill parameter to true

gr.color 255, 0, 0, 255, 1

! Create text object at x , y

y = y + 10 + 30

gr.text.draw P, x, y, "Bold, filled text"

! Show Underlinded text

gr.text.bold 0
gr.text.underline 1

! Create text object at x , y

y = y + 10 + 30
gr.text.draw P, x, y, "Underlined text"

! Show strike through text

gr.text.underline 0
gr.text.strike 1

! Create text object at x , y

y = y + 10 + 30
gr.text.draw P, x, y, "Strike through text"

! Show skewed text

gr.text.strike 0
gr.text.skew -0.25

! Create text object at x , y

y = y + 10 + 30
gr.text.draw P, x, y, "Skewed text"

! Now render everything

gr.render

! After a one second pause,
! start to blink the big text

pause 1000

do
  gr.hide blink_ptr
    gr.render
     pause 500
  gr.show blink_ptr
    gr.render
     pause 300
until 0

! The program does not end until the
! BACK key is pressed.