! When you run this program, the Graphics
! Screen will be displayed. To exit back
! to the BASIC! text output screen, hit
! the Back key.
!
! Open graphics screen with
! background color of black
  
a = 0
r = 0
g = 0
b = 0
gr.open a,r,g,b

! Draw divider lines

!Set the draw color to white
a = 255
r = 255
g = 255
b = 255
fill = 255
gr.color a,r,g,b,fill

!'Linea horiz.
x1 = 0
y1 = 140
!'tenia 800
x2 = 460
y2 = 140

gr.line n, x1, y1, x2, y2

!'Linea Vert
x1 = 460
y1 = 0
x2 = 460
!'tenia 480 
y2 = 330

gr.line n, x1, y1, x2, y2

! Draw unfilled red circle

r = 255
g = 0
b = 0
gr.color a,r,g,b,fill

cx = 60
cy = 60
!'tenia 100
radius = 50

gr.circle nc, cx, cy, radius

! Label the circle

gr.color 255,0,0,0,1
!'tenia 30
gr.text.size 25
gr.text.align 2
gr.text.draw pt, cx, cy + 10, "Circle"

! Draw a filled red oval

fill = 1
gr.color a,r,g,b,fill

left = 300
let top = 20
right = 450
bottom = 120

gr.oval no, left, top, right, bottom

! Label the oval

gr.color 255,255,255,0,1
gr.text.draw pt, 370, 80, "Oval"


! Draw a blue filled rectangle

r = 0
b = 255
g = 0
gr.color a,r,g,b,fill

left = 40
let top = 160
right = 360
bottom = 240

gr.rect nr, left, top, right, bottom

! Label the rectangle

gr.color 255,255,255,0,1
gr.text.draw pt, 200, 205, "Rectangle"


! Draw a filled yellow Arc as a Pie Chart

r = 255
g = 255
b = 0
gr.color a,r,g,b,fill

left = 140
let top = 5
right = 280
bottom = 130
sa = 0
ea = 240
cf = 1

gr.arc na, left, top, right, bottom, sa,ea,cf

! Label the Pie Chart

gr.color 255,255,0,0,1
gr.text.draw pt, 210, 100, "Filled Arc"

! Now render everything
gr.render


! Hide and show the objects
! Pause 1 second before starting
! Pause 950 milliseconds with each change

pause 1000

t = 950

   gr.hide nc
     gr.render
     pause t
   gr.show nc
     gr.render
      pause t
   gr.hide no
     gr.render
     pause t
   gr.show no
     gr.render
      pause t
   gr.hide nr
     gr.render
     pause t
   gr.show nr
     gr.render
      pause t
   gr.hide na
     gr.render
     pause t
   gr.show na
     gr.render
      pause t
end