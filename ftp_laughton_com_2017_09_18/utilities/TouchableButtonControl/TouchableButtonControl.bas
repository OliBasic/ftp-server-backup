REM Start of BASIC! Program

! Load the images for the button 
print "Loading. Please wait..."
print" 0% ..."

BYTE.OPEN R, rf, "http://medienreservierung.12hp.de/bas/button_up.png"
Byte.copy rf, "button_up.png"

print " 50% ..."

BYTE.OPEN R, rf2, "http://medienreservierung.12hp.de/bas/button_down.png"
BYTE.copy rf2, "button_down.png"
print "100% ..."


! Open graphics and show Button 

gr.open 255,200,200,200

gr.orientation 1

gr.color 255,255,0,0,1

gr.text.size 20

gr.text.bold 1

x = 20
y = 100

gr.bitmap.load btp1, "button_up.png"
gr.bitmap.load btp3, "button_down.png"

gr.bitmap.scale btp2, btp1, 160, 100
gr.bitmap.scale btp4, btp3, 160,100

gr.bitmap.draw drw1, btp2, x,y

gr.circle crc1, 235,315,10

gr.text.draw txt1, 60,160, "Press me..."
gr.text.draw cnt1, 0,0,""

gr.render
counter = 0
onn = 0

! Because gr.bounded.touch delivers two
! times a touch events (why..?), we use 
! variable onn: only every second event
! will be used

do
gr.bounded.touch tch, 20,100,180,200
if onn = 0
if tch = 1
counter = counter +1
gr.modify drw1,"bitmap",btp4
gr.color 255,255,255,255,1
gr.hide txt1
gr.text.draw txt1, 62,162, "Pressed..."
gr.hide cnt1
gr.text.draw cnt1, 20,290, format$("###",counter)+" times pressed..."
gr.render 
pause 250
gr.modify drw1, "bitmap", btp2
gr.color 255,255,0,0,1
gr.hide txt1
gr.text.draw txt1,60,160,"Press me..."
gr.render 
endif
onn = 1
else
onn = 0
endif
until 0

gr.close

end