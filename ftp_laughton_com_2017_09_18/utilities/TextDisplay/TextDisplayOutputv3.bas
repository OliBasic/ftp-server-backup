!TextScreenDemo.bas
!!
About:
Text Screen Demo (version 3)
by Donald Hosford (donzerme@yahoo.com)

This is a demo of an addressable text display on the
 text output console.

  The amount of text on the screen is deturmined by
 the size of your device's screen, and the text size
 setting in RFO Basic!'s preferences.  

How to use:
 1) Set TXT$ to your text.
   Can be any text.  Disruptive characters in TXT$ get
   replaced automaticly.  Doesn't catch all of them...
 2) Set the coordinates.
   Using TCX/TCY:
    Set SD$ to "" (null).  Then set TCX/TCY to the
    desired coordinates.
   Using SD$ -- format:
   chars 1 and 2 -- X axis (columns)
   chars 3 and 4 -- Y axis (rows)
   chars 5+ -- flags:
     c -- center TXT$ on row.  Must be shorter than MX.
 3) Gosub WriteToScreen.

What it can do:
--> Display text in an addressable fashion.
--> Can center text on a row.

problems:
--> It doesn't know how big your screen is.  You have
 to fiddle with the MX/MY values yourself.  On my
 device landscape is: Small text is 71 x 19.  Medium
 text is 49 x 13. Large text is 35 x 10.  Portrait uses
 different numbers.
--> It doesn't have any way to lock the screen
 orientation.  One way it looks great, the other it
 looks messy.
--> There no colors, no underlining, no italics, or form
 characters.
-->  Spaces and periods cause word wrapping, which
 disrupts the screen.  The program coverts spaces to
 160, and periods to 183.
!!

GOSUB ScreenSetup

!Main  program

SD$="0101c"
TXT$="Text Screen Demo"
GOSUB WriteToScreen

SD$="0302"
TXT$="See how the text can be placed anywhere."
GOSUB WriteToScreen
TXT$="  This is the 'semicolon'"
GOSUB WriteToScreen

SD$="0103"
TXT$="function.  Works like the print command.  Numbers can be handled, "
GOSUB writetoscreen

SD$="2804"
TXT$=" but must be strings."
gosub writetoscreen

!This section shows how to use the text cursor
! variables.
SD$="0505"
TXT$="Multiplication Table"
GOSUB writetoscreen
SD$=""
FOR A = 1 TO 6
 FOR B = 1 TO 6
  c=b* a
  TXT$=FORMAT$("##",c)
  TCX = A * 4-3
  TCY = B * 2 + 5
  GOSUB WriteToScreen
 NEXT B
NEXT A

!This shows that flags can be set alone.
TCX=15
TCY=MY
SD$="c"
TXT$="Touch screen to Continue"
GOSUB WriteToScreen

GOSUB ScreenPause

CLS


END

ScreenSetup:
!------------------
!Set Screen Sizes.
MX=71			%Maximum Screen X-Axis
MY=19			%Maximum Screen Y-Axis
MSP=MX*MY		%Maximum Screen Position

!Build Blank Screen.  I like using 160, or 183.
Screen$=""
FOR X=1 TO MSP
 Screen$=Screen$+CHR$(160)
NEXT X
Background$=Screen$

!Setup Text Cursor.
TCX=1		%Cursor X Position
TCY=1		%Cursor Y Position
TCP=1		%Cursor Position in Screen$
RETURN

ClearScreen:
!------------------
CLS
Screen$=Background$
RETURN

WriteToScreen:
!------------------
!Get Screen coordinates and flag Data
GOSUB readflags

L=LEN(TXT$)

!Screen center flag.
IF TCN=1
 C1=MX-L
 IF c1>1
  C2=C1/2
  CX=FLOOR(C2)
 ELSE
  CX=1
 ENDIF
ENDIF

!Convert spaces, and periods.
T$=""
FOR X = 1 TO L
 A$=MID$(TXT$,X,1)
 IF A$=CHR$(32)
  A$=CHR$(160)
 ENDIF
 IF A$=CHR$(44)
  A$=CHR$(183)
 ENDIF
 IF A$=CHR$(46)
  A$=CHR$(183)
 ENDIF
 T$=T$+A$
NEXT X
TXT$=T$

!Write Text to Screen$.
SP=(CY-1)*MX+CX
L=LEN(TXT$)
IF SP>1
 N=SP-1
 S1$=LEFT$(Screen$,(SP-1))
ELSE
 S1$=""
ENDIF
S2$=TXT$
IF SP<MSP
 N=MSP-(SP+L-1)
 S3$=RIGHT$(Screen$,N)
ELSE
 S3$=""
ENDIF
Screen$=S1$+S2$+S3$

!Move Text Cursor to end of Text.
SP=SP+L
CX=CX+L
IF CX>MX
 CX=CX-MX
 CY=CY+1
ENDIF
TCX=CX
TCY=CY
!clear variables and flags.
TXT$=""
SD$=""
TCN=0

!Display Screen
CLS
PRINT Screen$
RETURN

ReadFlags:
!------------------
!Read SD$ for coordinates, and flags.
S$=LEFT$(SD$,4)
IF LEN(S$)=4
 CX=VAL(LEFT$(S$,2))
 CY=VAL(RIGHT$(S$,2))
ELSE
 CX=TCX
 CY=TCY
ENDIF
IF LEN(SD$)>4
 S$=RIGHT$(SD$,LEN(SD$)-4)
 FOR S=1 TO LEN(S$)
  T$=MID$(S$,S,1)
  IF T$="c"
   TCN=1
  ELSE
   TCN=0
  ENDIF
  !add other letter codes here.
 NEXT S
ENDIF
!check for flag only SD$
IF LEN(SD$)<4
 FOR S=1 TO LEN(S$)
  T$=MID$(SD$,S,1)
  IF T$="c"
   TCN=1
  ELSE
   TCN=0
  ENDIF
  !add other letter codes here.
 NEXT S
ENDIF
RETURN


SCREENPAUSE:
!------------------
!text output console pause
Touch=0
DO
UNTIL Touch=1
RETURN

ONCONSOLETOUCH:
!------------------
Touch=1
CONSOLETOUCH.RESUME
