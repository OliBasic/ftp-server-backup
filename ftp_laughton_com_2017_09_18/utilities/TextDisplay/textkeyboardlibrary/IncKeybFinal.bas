!Typing Keyboard data
!This is to be a "typing" keyboard.
!----------------------------
!KEYBOARD button data
!number of keyboards
read.data 3
!LargestGrid X,Y -- number of columns, number of rows
Read.Data 13,7

!#1 -- Unshifted
Read.Data 13,7
read.data   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
read.data   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
Read.data 103, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 56, 35
read.data  16, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 68, 62
read.data   3,  3, 81, 82, 83, 84, 85, 86, 87, 88, 89, 65,  5
read.data  26, 26, 90, 91, 92, 93, 94, 95, 96, 38, 39,106, 66
read.data 111,  1,  1, 97, 98,  4,  4,  4,  4, 63,107,108,109
!Button size: Single Character
Read.data 1,1
!Button Size: Multiple characters
Read.data 1,1

!#2 -- Shifted
Read.Data 13,7
read.data   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
read.data   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
read.data  61, 27, 50, 51, 52, 53,102, 54, 55, 58, 59, 36, 35
read.data  16,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 57, 60
read.data   3,  3, 17, 18, 19, 20, 21, 22, 23, 24, 25, 64,  5
read.data  26, 26, 28, 29, 30, 31, 32, 33, 34, 69, 70,106, 67
read.data 111,  1,  1,100,101,  4,  4,  4,  4, 37,107,108,109
!Button size: Single Character
Read.data 1,1
!Button Size: Multiple characters
Read.data 1,1

!#3 -- show
Read.Data 13,7
read.data   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
read.data   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
read.data   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
read.data   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
read.data   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
read.data   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
read.data 110,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
!Button size: Single Character
Read.data 1,1
!Button Size: Multiple characters
Read.data 1,1

!number of unique button names
READ.DATA 111
!button name, printedkey,keytype
read.data "MENU 0","EXIT 0","CapLock 0","SPACE 1","ENTER 0","QQ1","WW1","EE1","RR1","TT1"
read.data "YY1","UU1","II1","OO1","PP1","TAB 0","AA1","SS1","DD1","FF1"
read.data "GG1","HH1","JJ1","KK1","LL1","Shift 0","!!1","ZZ1","XX1","CC1"
read.data "VV1","BB1","NN1","MM1","BACK 0","UndSco_1","QUOTE 1","COMMA 1","..1","111"
read.data "221","331","441","551","661","771","881","991","001","@@1"

read.data "##1","$$1","%%1","&&1","**1","--1","Plus 1","((1","))1","||1"
read.data "~~1","Backslash 1","Apostr 1","::1",";;1","//1","??1","==1","<<1",">>1"
read.data "qq1","ww1","ee1","rr1","tt1","yy1","uu1","ii1","oo1","pp1"
read.data "aa1","ss1","dd1","ff1","gg1","hh1","jj1","kk1","ll1","zz1"
read.data "xx1","cc1","vv1","bb1","nn1","mm1","[[1","]]1","\\1","{{1"

READ.DATA "}}1","^^1","``1","Message 0","Answer 0","Uar 0","Lar 0","Dar 0","Rar 0","Show 0"
read.data "Hide 0"


!keys to be fixed -- these can't appear in data statements...
!number of keys
read.data 10
!keys -- format: position, ascii value.
read.data 36,95
read.data 37,34
read.data 38,44
read.data 57,43
read.data 62,92
read.data 63,39
read.data 106,9651
read.data 107,9665
read.data 108,9661
read.data 109,9655



!end.
