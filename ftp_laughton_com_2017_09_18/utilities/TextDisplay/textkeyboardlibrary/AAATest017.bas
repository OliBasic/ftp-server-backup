!AAATEST017.BAS
!---------------------------------------------
!porpose: Demo program to utilize the new
!  DonsBasicLibrary.bas include file.
!---------------------------------------------
!FINDFunctions
!---------------------------------------------
!includes


FN.DEF L_Inpt$(MSG$)
!Old style input.  Requires enter to end it.
!uses: variable$=L_Input(Message$)

!set base bundle key name
B1=1
BUNDLE.GET B1, "curscreen",curscreen
kn$="scrn"+STR$(curScreen)

!get screen colors
BUNDLE.GET B1,"foreground",CF
BUNDLE.GET B1,"background",CB

!Get screen data from bundle.
Kn2$=kn$+"DisplayMX"
B2=1
BUNDLE.GET B2,kn2$,DisplayMX
Kn2$=kn$+"DisplayMY"
BUNDLE.GET B2,kn2$,DisplayMY
Kn2$=kn$+"Displaybackground"
BUNDLE.GET B2,kn2$,Displaybackground
Kn2$=kn$+"RowHigh"
BUNDLE.GET B2,kn2$,Rowhigh
Kn2$=kn$+"ColumnWide"
BUNDLE.GET B2,kn2$,Columnwide
Kn2$=kn$+"DisplayTSZ"
BUNDLE.GET B2,kn2$,DisplayTSZ
Kn2$=kn$+"DisplayOFFY"
BUNDLE.GET B2,kn2$,DisplayOffY
Kn2$=kn$+"DisplayOFFX"
BUNDLE.GET B2,kn2$,DisplayOffX
Kn2$=kn$+"ScreenPtr"
BUNDLE.GET B2,kn2$,ScreenPTR
Kn2$=kn$+"GON"
BUNDLE.GET B2,kn2$,GON
Kn2$=kn$+"TCX"
BUNDLE.GET B2,kn2$,TCX
Kn2$=kn$+"TCY"
BUNDLE.GET B2,kn2$,TCY

!retrieve cursor info
kn2$=kn$+"cursorgon"
Bundle.get b2,kn2$,cursorGON
kn2$=kn$+"screenptr2"
bundle.get b2,kn2$,screenptr2



!figure coordinates of input box
ULCX=TCX
ULCY=TCY

!show keyboard
L_showkeyboard(1)

!process input message, if any...
L=len(msg$)
if l>0
 A$=MSG$
 L_write(A$)
 Kn2$=kn$+"TCX"
 BUNDLE.GET B2,kn2$,TCX
 Kn2$=kn$+"TCY"
 BUNDLE.GET B2,kn2$,TCY
else
 A$=""
endif

!Show cursor, put at the end of MSG$
gr.show cursorGON
CX=((TCX-1)*Columnwide)
CY=((TCY-1)*Rowhigh)
GR.MODIFY ScreenPTR2,"x",CX
GR.MODIFY ScreenPTR2,"y",CY

!start input loop
Exit=0
do

Debug.show.program

touched=0
 DO
  GR.TOUCH touched, x, y

  debug.show.program

 UNTIL touched
 ! Touch detected, now wait for
 ! finger lifted
 DO
  GR.TOUCH touched, x, y
 UNTIL !touched

! gosub gettouch
!get touch, did user touch a key?
! touch = 0
! DO
!  GR.TOUCH touch,t1x,t1y
! UNTIL touch = 1
!loop until screen is not touched
!DO
! GR.TOUCH touch,t2x,t2y
!UNTIL touch = 0


 L_WhatKeyTouched(X,Y)

 !keytouch,Key$,Type$,KeyPr$
 B1=1
 BUNDLE.GET B1, "keytouch",keytouch
 BUNDLE.GET B1, "Key$",Key$
 BUNDLE.GET B1, "Type$",Type$
 BUNDLE.GET B1, "KeyPr$",KeyPr$

print key$

 !get type/key values
 type=val(type$)
 if key$="none"
  Type=-1
  !go back for another key.
 endif

 if type=1
  A$=A$+KeyPr$
  L_Locate(tcx,tcy)
  L_write(KeyPr$)
 endif

 !process control keys
 if type=0
  if key=5
   !enter
   exit=1
  endif
  if key=35
   !backspace
   L=len(a$)
   A$=left$(A$,l-1)
   !move cursor, update a$
   TCX=TCX-1
   L_Locate(TCX,TCY)
   L_write(" ")
   TCX=TCX-1
  endif
  if key=110
   !show key
   L_showkeyboard(prevkeyboard)
  endif
  if key=111
   !hide key
   B1=1
   BUNDLE.GET B1, "CurKeyboard", Keyboard
   prevkeyboard=keyboard
   L_showkeyboard(3)
  endif
 endif

 !end input loop?
until exit=1

!Store input in MSG$
MSG$=A$


print msg$

!hide cursor/keyboard
gr.hide cursorGON
L_showkeyboard(1)

!clear input box back to displaybackground

functend:
!pass msg$ back and end function
FN.RTN MSG$
FN.END

!desired keyboard include here
INCLUDE DonsBasicLibrary.bas
INCLUDE IncKeybFinal.bas

!---------------------------------------------
!--------------------------------------------
!FINDMain
!---------------------------------------------
!setup display/keyboard
L_DisplaySetup(1,0,1)
L_addscreen(80,25,10)
L_keyboardsetup(200,9)
!---------------------------------------------
!main program:

Debug.on

!switch to screen 1
L_changescreen(1)

! set foreground/background colors
L_setcolors(5,10)

!write stuff on screen 1
L_locate(1,1)
L_write("Screen Keyboard Demo")
L_locate(2,3)
L_write("Hello World!")
L_locate(2,4)
L_write("Hi There!")
q$="This IS concatination."
L_write(q$)
Q$="  And so is this."+chr$(13)
L_write(Q$)
Q$="Please Type Something: "
l_write(Q$)

MSG$="Hi"
L_Inpt$(MSG$)

print msg$


!show all keyboards
!B3=1
!BUNDLE.GET B3,"keyboards",NumKeyboard
!if NumKeyboard>0
! L_showkeyboard(1)
! if NumKeyboard>1
!  for x=2 to NumKeyboard
!   gosub gettouch
!   L_hidekeyboard(x-1)
!   L_showkeyboard(x)
!  next x
! endif
! GOSUB gettouch
!endif

Debug.off








END
