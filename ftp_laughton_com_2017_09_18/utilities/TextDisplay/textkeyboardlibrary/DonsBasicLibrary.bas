!--------------------------------------------------------------
!DonsBasicLibrary.bas
!----------------------------
!Last Change: 12-19-2016 @ 10:32am
!Purpose: This file pulls all of my functions togather into a
! single library file.
!All functions here will begin with L_ to differenciate them from
! other functions and basic commands.
!All data is stored in bundle number 1.
!
!Contents:
!
!  Library Setup:
!    L_DisplaySetup(DisplayBoarder,DisplayORI,DisplayStatus)
!      (Run first.  Sets the bundle data, and the graphics screen.)
!
!  Display:
!    L_addscreen(NumberOfThisType,DisplayMX,DisplayMY,Displaybackground)
!      (Adds a text display screen.)
!    L_ChangeScreen(NewScreen)
!      (Switches the text display being used.)
!    L_Write (TXT$)
!      ("Prints" to the current text display.)
!    L_Clear()
!      (Clears the current text display to it's background color.)
!    L_ClearABox(TCX,TCY,Width,Hight,ColorToUse)
!      (Clears a portion of the current text display to a specific color.)
!    L_Setcolors(foreground,background)
!      (Sets the current foreground and background colors.)
!    L_locate(tcx,tcy)
!      (Sets the location of the current text display's text cursor.)
!
!  Keyboard:
!    L_KeyboardSetup(KeyboardAlpha,KeyboardColor)
!      (Loads the keyboard data, and draws the keyboards.)
!    L_WhatKeyTouched(TouchXcoord,TouchYcoord)
!      (Deturmines if a key has been touched, and stores it in bundle.)
!    L_Drawbuttons()
!      (Draws the keyboards.)
!    L_hidekeyboard(kbrd)
!      (Hides the keyboard.)
!    L_showkeyboard(kbrd)
!      (Shows the keyboard.)
!    L_CurrentKeyboard(keyboard)
!      (Sets the current keyboard.)
!    L_LoadButtonData()
!      (Loads the keyboard data.)
!
!  Keyboard gosubs:
!    Gettouch:
!      (Gets the touch coordinates.)
!    GetLastKey:
!      (Retreives the last key touched from the bundle.)
!
!  Graphics:
!    L_gcol(Alpha,color,fill)
!      (Internal.  Sets the graphics color.)
!    L_OpenGR(Alpha,color,Status,Orientation)
!      (Internal.  Opens the graphics screen.)
!
!  Miscellanious Functions:
!    A$=L_NumberClip$(Number,Number$)
!      (Returns the number with the decimal removed. Example 1.0 becomes 1.)
!
!  Test program:
!    Used to find problems in the library code.
!    1) Rem out the line: goto Skipthis.  At the top of the test program section.
!    2) Run the library as a program.
!
!--------------------------------------------------------------
!FindDisplay
!----------------------------
! IncDisplay11.bas
!----------------------------
!Last Change:09-19-2016
!Purpose: The new flexible multi-screen text display!  Now with
!  Bundles and Functionized.

!DisplayORI=0
!DISPlayBoarder=10
!DisplayRender=1

!DisplaySc[] format:
!Screen Type 2
!NumType=1
! DisplayMX=Display number of columns wide (char)
! DisplayMY=Display number of rows high (char)
! DisplayBackground=Background color 1 - 20
! DisplayStatus=Show status bar 0 = no, 1 = Yes.
! Rowhigh=Hight of character in pixels
! ColumnWide=Width of character in pixels
! DisplayTSZ=Text size number
! DisplayOffY=Offset Y coord of display
! DisplayOffX=Offset X coord of display
! ScreenPTR=BMP image pointer
! GON=Graphical Object Number of BMP image
! DisplayTW   - Text Width (Pixels)
!----------------------------
FN.DEF L_DisplaySetup(DisplayBoarder,DisplayORI,DisplayStatus)
 !setup graphics
 !New setup subroutine auto figures settings.
 !Set MX, MY, and ORI.  Gosub DisplaySetup
 !DisplayMY   - Number of Displayed Rows.
 !DisplayMX   - Number of Displayed Columns.
 !DisplayORI  - Screen orientation. Portrait = 1, Landscape = 0
 !DisplayBackground - Set ColorBackground to color number from GCOL. 
 !DisplayBoarder - Set to color number from GCOL.
 !DisplayStatus - Show/Hide status bar. 0 = Hide, 1 = Show.
 !-----
 !Setup figures TSZ, TW, OFFX and OFFY.
 !DisplayTSZ  - Text Size (Pixels)
 !DisplayTW   - Text Width (Pixels)
 !DisplayOFFX - Offset X (Pixels)
 !DisplayOFFY - Offset Y (Pixels)
 !-----
 B1=1
 BUNDLE.CREATE B1
 a$="DonsBasicLibrary.bas  All data in this one bundle!"
 BUNDLE.PUT B1, "purpose",a$
 BUNDLE.PUT B1, "NumBundles", 1
 BUNDLE.PUT B1, "screens", -1
 BUNDLE.PUT B1, "keyboards",0
 BUNDLE.PUT B1, "curscreen", 0
 BUNDLE.PUT B1, "foreground",9
 BUNDLE.PUT B1, "background",1

 !Colors for L_Gcol, and L_OpenGR
 C1$="000000 ff0000 00ff00 0000ff ffff00 00ffff ff00ff 999999 ffffff 000099 "
 C2$="009900 009999 990000 990099 999900 993333 CCCCCC FFCC99 FF9900 FFCC00 "
 ColorString$=c1$+c2$
 B1=1
 BUNDLE.PUT B1, "colorstring",Colorstring$

 CALL L_OpenGR(255,DisplayBoarder,DisplayStatus,DisplayORI)
 GR.SCREEN DisplayMGX,DisplayMGY
 IF DisplayStatus=1
  GR.STATUSBAR DisplayStatusHight
 ELSE
  DisplayStatusHight=0
 ENDIF
 BUNDLE.PUT B1, "DisplayMGX", DisplayMGX
 BUNDLE.PUT B1, "DisplayMGY", DisplayMGY
 BUNDLE.PUT B1, "DisplayStatus", DisplayStatusHight
 !add screen 0 -- internal info screen
 L_addscreen(30,15,10)
 L_ChangeScreen(0)

 !load screen 0 with startup info
 L_locate(1,1)
 L_write("     Don's Basic Library")
 L_locate(9,2)
 L_write("by Sirdonzer")
 L_locate(2,3)
 L_write("Email: Donzerme@Yahoo.com")
 L_locate(1,4)
 P$="  --------------------------"+chr$(13)
 L_write(P$)
 P$="  --------------------------"+chr$(13)
 L_write(P$)
 P$="Screen  0 "+chr$(13)
 L_write(P$)
FN.END

!----------------------------
FN.DEF L_addscreen(DisplayMX,DisplayMY,Displaybackground)
 !Adds a text display screen.
 !get bundle 1 info
 B1=1
 BUNDLE.GET B1, "NumBundles",NumBundles
 BUNDLE.GET B1, "screens",NumScreens
 BUNDLE.GET B1, "keyboards",NumKeyboards
 BUNDLE.GET B1, "DisplayMGX", DisplayMGX
 BUNDLE.GET B1, "DisplayMGY", DisplayMGY
 BUNDLE.GET B1, "DisplayStatus", DisplayStatusHight
 BUNDLE.GET B1, "curscreen", curscreen

 !Start textsize calculation
 ! curscreen=a51
 !subtract hight of the status bar if present.
 DisplayMGY=DisplayMGY-DisplayStatusHight
 Rowhigh=FLOOR(DisplayMGY/DisplayMY)
 DisplayMGY=DisplayMGY+DisplayStatusHight
 Columnwide=FLOOR(DisplayMGX/DisplayMX)
 !figure DisplayTSZ
 DisplayTSZ=Rowhigh
 GR.TEXT.TYPEFACE 2
 DO
  A14=0
  GR.TEXT.SIZE DisplayTSZ
  GR.TEXT.WIDTH DisplayTW, "A"
  GR.TEXT.HEIGHT A08,A09,A10
  A09=FLOOR(A09)
  A11=CEIL(A10)
  A12=FLOOR(A09)
  TextTR=A11-A12
  IF TextTR>Rowhigh
   A14=A14+1
  ENDIF
  IF DisplayTW>Columnwide
   A14=A14+1
  ENDIF
  IF A14>0
   DisplayTSZ=DisplayTSZ-1
  ENDIF
 UNTIL A14=0
 !fix the size of RowHigh and ColumnWide to the size of the text.
 IF Rowhigh>TextTR THEN Rowhigh=TextTR
 IF Columnwide>DisplayTW THEN Columnwide=DisplayTW
 !calculate offsets.
 A04=DisplayMGY-Displaystatushight
 A05=Rowhigh*DisplayMY
 A06=A04-A05
 A07=FLOOR(A06/2)
 DisplayOffY=A07+Displaystatushight
 DisplayOffX=FLOOR((DisplayMGX-(Columnwide * DisplayMX))/2)
 !The RowHigh and ColumnWide are the size of the background graphic boxes. 
 !TR is the total hight of Text in pixels.
 !----
 !set background color, create a screen sized bitmap, and show it.
 L_gcol(255,Displayboarder,1)
 !calculate bmp size here.
 A48=Rowhigh * DisplayMY
 A49=Columnwide * DisplayMX
 GR.BITMAP.CREATE ScreenPTR, A49,A48
 !GR.BITMAP.CREATE ScreenPTR,DisplayMGX,DisplayMGY
 !use offsets to center bmp.
 !must set color for bitmap.
 !Gr.bitmap.drawinto.start points the graphic tools at the bitmap "screenptr" 
 !must start and end before/after each "Printing/drawing".
 GR.BITMAP.DRAWINTO.START ScreenPTR
 !use ordinary graphic commands to draw stuff on bitmap.

 !clear screen
 Tcx=1
 TCY=1
 Width=DisplayMX
 Hight=DisplayMY
 A40=TCY
 A41=TCY+Hight-1
 A42=TCX
 A43=TCX+Width-1
 A44=(((A42-1))*Columnwide)
 A45=((A40-1)*Rowhigh)
 A46=((A43)*Columnwide)
 A47=((A41)*Rowhigh)
 CALL L_gcol(255,Displaybackground,1)
 GR.RECT A48,A44,A45,A46,A47
 GR.BITMAP.DRAWINTO.END
 !this next command adds the bitmap to the display list.
 !only need to do this once.
 GR.BITMAP.DRAW GON,ScreenPTR,1,1
 !Centers the bmpimage.
 GR.MODIFY ScreenPTR,"x",DisplayOffX
 GR.MODIFY ScreenPTR,"y",DisplayOffY
 !then gr.render to make it appear.
 GR.RENDER
 GR.HIDE GON

 !draw cursor
 L_gcol(255,Displayboarder,1)
 !calculate bmp size here.
 A48=Rowhigh
 A49=Columnwide
 GR.BITMAP.CREATE ScreenPTR2, A49,A48
 GR.BITMAP.DRAWINTO.START ScreenPTR2
 !Draw Box
 A28=1
 A29=1
 A30=Columnwide
 A31=Rowhigh
 CALL L_gcol(255,9,1)
 GR.RECT A32,A28,A29,A30,A31
 GR.BITMAP.DRAWINTO.END
 GR.BITMAP.DRAW CursorGON,ScreenPTR2,1,1
 GR.MODIFY ScreenPTR2,"x",DisplayOffX
 GR.MODIFY ScreenPTR2,"y",DisplayOffY
 gr.render
 gr.hide cursorGON

 !Store variables here.
 NumScreens=NumScreens+1
 !Put screen data into bundle #1.
 kn$="scrn"+STR$(NumScreens)
 B2=1
 Kn2$=kn$+"DisplayMX"
 BUNDLE.PUT B2,kn2$,DisplayMX
 Kn2$=kn$+"DisplayMY"
 BUNDLE.PUT B2,kn2$,DisplayMY
 Kn2$=kn$+"Displaybackground"
 BUNDLE.PUT B2,kn2$,Displaybackground
 Kn2$=kn$+"RowHigh"
 BUNDLE.PUT B2,kn2$,Rowhigh
 Kn2$=kn$+"ColumnWide"
 BUNDLE.PUT B2,kn2$,Columnwide
 Kn2$=kn$+"DisplayTSZ"
 BUNDLE.PUT B2,kn2$,DisplayTSZ
 Kn2$=kn$+"DisplayOFFY"
 BUNDLE.PUT B2,kn2$,DisplayOffY
 Kn2$=kn$+"DisplayOFFX"
 BUNDLE.PUT B2,kn2$,DisplayOffX
 Kn2$=kn$+"ScreenPtr"
 BUNDLE.PUT B2,kn2$,ScreenPTR
 Kn2$=kn$+"GON"
 BUNDLE.PUT B2,kn2$,GON
 !set the Text Cursor to the upper left corner
 Kn2$=kn$+"TCX"
 BUNDLE.PUT B2,kn2$,1
 Kn2$=kn$+"TCY"
 BUNDLE.PUT B2,kn2$,1
 kn2$=kn$+"cursorgon"
 Bundle.put b2,kn2$,cursorGON
 kn2$=kn$+"screenptr2"
 bundle.put b2,kn2$,screenptr2
 curscreen=numscreens
 BUNDLE.PUT B1, "curscreen", curscreen
 BUNDLE.PUT B1, "screens",NumScreens
 L_ChangeScreen(0)
 a$="Screen  "+str$(curscreen)+chr$(13)
 L_write(a$)
FN.END

!----------------------------
FN.DEF L_ChangeScreen(NewScreen)
 !Set NewScreen to the desired screen number, call ChangeScreen.
 kn$="scrn"
 B1=1
 BUNDLE.GET B1, "curscreen",curscreen

 ScreenNum$=kn$+STR$(curScreen)+"GON"
 B2=1
 BUNDLE.GET B2, screenNum$,GON
 GR.HIDE GON
 ScreenNum$=kn$+STR$(NewScreen)+"GON"
 BUNDLE.GET B2, screenNum$,GON

 Screennum$=kn$+STR$(NewScreen)+"DisplayTSZ"
 BUNDLE.GET B2,Screennum$,DisplayTSZ
 GR.TEXT.SIZE DisplayTSZ
 !put any other change screen stuff here.
 GR.SHOW GON
 GR.RENDER
 B1=1
 BUNDLE.PUT B1, "curscreen",NewScreen
FN.END

!----------------------------
FN.DEF L_Write (TXT$)
 !set TCX,TCY,TXT$

 !get screen info
 B1=1
 BUNDLE.GET B1, "curscreen",curscreen
 kn$="scrn"+STR$(curScreen)
 BUNDLE.GET B1,"foreground",CF
 BUNDLE.GET B1,"background",CB

 !Get screen data from bundle.
 Kn2$=kn$+"DisplayMX"
 B2=1
 BUNDLE.GET B2,kn2$,DisplayMX
 Kn2$=kn$+"DisplayMY"
 BUNDLE.GET B2,kn2$,DisplayMY
 Kn2$=kn$+"Displaybackground"
 BUNDLE.GET B2,kn2$,Displaybackground
 Kn2$=kn$+"RowHigh"
 BUNDLE.GET B2,kn2$,Rowhigh
 Kn2$=kn$+"ColumnWide"
 BUNDLE.GET B2,kn2$,Columnwide
 Kn2$=kn$+"DisplayTSZ"
 BUNDLE.GET B2,kn2$,DisplayTSZ
 Kn2$=kn$+"DisplayOFFY"
 BUNDLE.GET B2,kn2$,DisplayOffY
 Kn2$=kn$+"DisplayOFFX"
 BUNDLE.GET B2,kn2$,DisplayOffX
 Kn2$=kn$+"ScreenPtr"
 BUNDLE.GET B2,kn2$,ScreenPTR
 Kn2$=kn$+"GON"
 BUNDLE.GET B2,kn2$,GON
 Kn2$=kn$+"TCX"
 BUNDLE.GET B2,kn2$,TCX
 Kn2$=kn$+"TCY"
 BUNDLE.GET B2,kn2$,TCY
 
 GR.TEXT.SIZE DisplayTSZ

 !prepair for writing
 A20=TCX
 A21=TCY
 !Lenth of TXT$
 A22=len(TXT$)
 !size of the grab in characters
 A23=1
 !TXT$ position pointer
 A24=1
 
 !Start write operation
 GR.BITMAP.DRAWINTO.START ScreenPTR
 
 !calculate TCX,TCY in pixels
 !text char position in pixels
 !both corners of the box, in pixels.

 !Do write operation
 For A24=1 to A22
  !Grab a part of TXT$
  A26$=MID$(TXT$,A24,A23)

  !check for special characters.
  c=ucode(a26$)
  !note: These are not ascii/ansi/html codes.
  ! After searching, I desided to keep it simple,
  !and setup my own codes.
  if c>31
   !draw everything else.
   !Draw Box
   A28=((A20-1)*Columnwide)
   A29=((A21-1)*Rowhigh)
   A30=A28+(Columnwide * A23)
   A31=A29+Rowhigh
   CALL L_gcol(255,CB,1)
   GR.RECT A32,A28,A29,A30,A31
   !Draw Character
   A33=((A20-1)*Columnwide)
   A34=((A21-1)*Rowhigh)+DisplayTSZ
   CALL L_gcol(255,CF,1)
   GR.TEXT.DRAW A32,A33,A34,A26$
   !update cursor position
   A20=A20+1
   if A20>DisplayMX
    A20=1
    A21=A21+1
    if A21>DisplayMY
     A21=1
    endif
   Endif
  else
   if c=1
    !home the cursor (move to upper left corner. Thats 1,1.)
    A20=1
    A21=1
   endif
   if c=2 
    !cursor up
    A21=A21-1
    if A21<1 then A21=DisplayMY
   endif
   if c=3
    !cursor down
    A21=A21+1
    IF A21>DisplayMY then A21=1
   endif
   if c=4
    !cursor left
    A20=A20-1
    if A20<1 then A20=DisplayMX
   endif
   if c=5
    !cursor right
    A20=A20+1
    if A20>DisplayMX then A20=1
   endif
   if c=8
    !backspace -- distructive backspace
    A20=A20-1
    If A20<1 then A20=DisplayMX
    !Draw Box
    A28=((A20-1)*Columnwide)
    A29=((A21-1)*Rowhigh)
    A30=A28+(Columnwide * A23)
    A31=A29+Rowhigh
    CALL L_gcol(255,CB,1)
    GR.RECT A32,A28,A29,A30,A31
   endif
   if c=13
    !enter
    A20=1
    A21=A21+1
    if A21>DisplayMY then A21=1
   endif
  endif
 !End Loop
 next

 !stop write operation
 GR.BITMAP.DRAWINTO.END
 GR.RENDER

 !Store cursor position
 TCX=A20
 TCY=A21
 kn$="scrn"+STR$(curScreen)
 kn2$=kn$+"TCX"
 B2=1
 BUNDLE.PUT B2,kn2$,TCX
 kn2$=kn$+"TCY"
 BUNDLE.PUT B2,kn2$,TCY

FN.END

!----------------------------
FN.DEF L_Clear()
 !Clears the screen.(Paints entire screen the background color.)
 !Uses ClearABox to do the work.

 !get screen data


 !Get screen data from bundle Numbun.
 B1=1
 BUNDLE.GET B1, "curscreen",curscreen
 kn$="scrn"+STR$(curScreen)
 Kn2$=kn$+"DisplayMX"
 B2=1
 BUNDLE.GET B2,kn2$,DisplayMX
 Kn2$=kn$+"DisplayMY"
 BUNDLE.GET B2,kn2$,DisplayMY
 Kn2$=kn$+"Displaybackground"
 BUNDLE.GET B2,kn2$,Displaybackground
 Kn2$=kn$+"RowHigh"
 BUNDLE.GET B2,kn2$,Rowhigh
 Kn2$=kn$+"ColumnWide"
 BUNDLE.GET B2,kn2$,Columnwide
 Kn2$=kn$+"DisplayTSZ"
 BUNDLE.GET B2,kn2$,DisplayTSZ
 Kn2$=kn$+"DisplayOFFY"
 BUNDLE.GET B2,kn2$,DisplayOffY
 Kn2$=kn$+"DisplayOFFX"
 BUNDLE.GET B2,kn2$,DisplayOffX
 Kn2$=kn$+"ScreenPtr"
 BUNDLE.GET B2,kn2$,ScreenPTR
 Kn2$=kn$+"GON"
 BUNDLE.GET B2,kn2$,GON
 Kn2$=kn$+"TCX"
 BUNDLE.GET B2,kn2$,TCX
 Kn2$=kn$+"TCY"
 BUNDLE.GET B2,kn2$,TCY

 !set coordinates to whole screen
 TCX=1
 TCY=1
 Width=DisplayMX
 Hight=DisplayMY
 CALL L_ClearABox(TCX,TCY,Width,Hight,Displaybackground)

 !Move cursor back to upper left corner
 Kn2$=kn$+"TCX"
 B2=2
 BUNDLE.PUT B2,kn2$,TCX
 Kn2$=kn$+"TCY"
 BUNDLE.PUT B2,kn2$,TCY

FN.END

!----------------------------
FN.DEF L_ClearABox(TCX,TCY,Width,Hight,colortouse)
 !Clears a box on the screen (in characters).
 !(Paints a box in the background color.)
 ! Specs: (In characters)
 !  TCX,TCY - coords of the upper left corner of the box
 !  Width - width of box.
 !  Hight - Hight of box.

 !Get screen data from bundle Numbun.
 B1=1
 BUNDLE.GET B1, "curscreen",curscreen
 kn$="scrn"+STR$(curScreen)
 Kn2$=kn$+"DisplayMX"
 B2=1
 BUNDLE.GET B2,kn2$,DisplayMX
 Kn2$=kn$+"DisplayMY"
 BUNDLE.GET B2,kn2$,DisplayMY
 Kn2$=kn$+"Displaybackground"
 BUNDLE.GET B2,kn2$,Displaybackground
 Kn2$=kn$+"RowHigh"
 BUNDLE.GET B2,kn2$,Rowhigh
 Kn2$=kn$+"ColumnWide"
 BUNDLE.GET B2,kn2$,Columnwide
 Kn2$=kn$+"DisplayTSZ"
 BUNDLE.GET B2,kn2$,DisplayTSZ
 Kn2$=kn$+"DisplayOFFY"
 BUNDLE.GET B2,kn2$,DisplayOffY
 Kn2$=kn$+"DisplayOFFX"
 BUNDLE.GET B2,kn2$,DisplayOffX
 Kn2$=kn$+"ScreenPtr"
 BUNDLE.GET B2,kn2$,ScreenPTR
 Kn2$=kn$+"GON"
 BUNDLE.GET B2,kn2$,GON
 ! Kn2$=kn$+"TCX"
 ! BUNDLE.GET B2,kn2$,TCX
 ! Kn2$=kn$+"TCY"
 ! BUNDLE.GET B2,kn2$,TCY

 A40=TCY
 A41=TCY+Hight-1
 A42=TCX
 A43=TCX+Width-1
 !Draw Box
 GR.BITMAP.DRAWINTO.START ScreenPTR
 A44=(((A42-1))*Columnwide)
 A45=((A40-1)*Rowhigh)
 A46=((A43)*Columnwide)
 A47=((A41)*Rowhigh)

 CALL L_gcol(255,colortouse,1)
 GR.RECT A48,A44,A45,A46,A47
 GR.BITMAP.DRAWINTO.END
FN.END
!----------------------------
FN.DEF L_Setcolors(foreground,background)

 B1=1
 BUNDLE.PUT B1,"foreground",foreground
 BUNDLE.PUT B1,"background",background

FN.END
!----------------------------
FN.DEF L_locate(tcx,tcy)

 !get screen info
 B1=1
 BUNDLE.GET B1, "curscreen",curscreen
 kn$="scrn"+STR$(curScreen)
 Kn2$=kn$+"TCX"
 Numbun=1
 BUNDLE.PUT Numbun,kn2$,TCX
 Kn2$=kn$+"TCY"
 BUNDLE.PUT Numbun,kn2$,TCY

FN.END
!----------------------------
!

!--------------------------------------------------------------
!FindKeyboard
!----------------------------
!IncKeyboard4.bas
!----------------------------
!Last Change:11-11-2016
!Purpose: An onscreen keyboard.  This has just the
!  keyboard load/draw subroutines.  Need to include
!  whatever keyboard data you need.
!
!Requires:
!  IncDisplay11.bas
!
!Use:
!  Put "IncKeyboard4.bas" after the Include IncDisplay11.bas line.
!  Put "setupkeyboard(keyboard alpha value, keyboard color number)
!   after Displaysetup() line.
!  Then include the keyboard data you want, after the
!   IncKeyboard4.bas line.
!    Example: INCLUDE IncDataKeyboardtyping.bas
!----------------------------
!Keyboard Start Block
!KeyboardAlpha=64
!KeyboardColor=9
!----------------------------

GOTO skip

gettouch:
 !Start of the touch loop.
 !Detects single touches only.
 !Returns T1X,T1Y,T2X,T2Y - coordinates of touch and release.
 !loop until the screen is touched
 touch = 0
 DO
  GR.TOUCH touch,t1x,t1y
 UNTIL touch = 1
 !loop until screen is not touched
 DO
  GR.TOUCH touch,t2x,t2y
 UNTIL touch = 0
RETURN
!----------------------------
GetLastKey:
 !get the last key touched.
 !keytouch,Key$,Type$,KeyPr$
 B1=1
 BUNDLE.GET B1, "keytouch",keytouch
 BUNDLE.GET B1, "Key$",Key$
 BUNDLE.GET B1, "Type$",Type$
 BUNDLE.GET B1, "KeyPr$",KeyPr$
! if key$="none" then no key pressed.
Return
!----------------------------
skip:
!----------------------------
FN.DEF L_KeyboardSetup(KeyboardAlpha,KeyboardColor)
 L_ChangeScreen(0)
 P$="Keyboard data Loading"+chr$(13)
 L_write(P$)
 L_CurrentKeyboard(1)
 !setup onscreen keyboard
 P$=">Colors"+chr$(13)
 L_write(P$)
 B3=1
 BUNDLE.PUT B3, "KeyboardAlpha",KeyboardAlpha
 BUNDLE.PUT B3, "KeyboardColor",KeyboardColor
 P$=">Reading Data"
 L_write(P$)
 L_LoadButtonData()
 P$=">Drawing Keyboard: "
 L_write(P$)
 L_drawbuttons()
 L_CurrentKeyboard(1)
 L_clear()
FN.END
!----------------------------
FN.DEF L_WhatKeyTouched(TX1,TY1)
 !takes the coordinates and figures out which key was touched.
 !returns: KeyTouch, Key$, KeyPr$ and Type$
 !get bundle 1 info
 B1=1
 BUNDLE.GET B1, "NumBundles",NumBundles
 BUNDLE.GET B1, "screens",NumScreens
 BUNDLE.GET B1, "keyboards",NumKeyboards
 BUNDLE.GET B1, "DisplayMGX", DisplayMGX
 BUNDLE.GET B1, "DisplayMGY", DisplayMGY
 BUNDLE.GET B1, "DisplayStatus", DisplayStatusHight
 BUNDLE.GET B1, "CurKeyboard", Keyboard

 !get all keyboard data from bundle #3
 Numbun=1

 !base keyboard name
 kN$="kbrd"+STR$(keyboard)

 !get the keyboard info
 B3=1
 BUNDLE.GET B3, "KeysPrintable",KeysPrintable$
 BUNDLE.GET B3, "KeyType",KeyType$
 keynum$=kn$+"BXP"
 BUNDLE.GET B3, keyNum$,BXP
 keynum$=kn$+"BYP"
 BUNDLE.GET B3, keyNum$,BYP
 keynum$=kn$+"BOFFX"
 BUNDLE.GET B3, keyNum$,BOFFX
 keynum$=kn$+"BOFFY"
 BUNDLE.GET B3, keyNum$,BOFFY
 keynum$=kn$+"NumCol"
 BUNDLE.GET B3, keyNum$,NumCol
 keynum$=kn$+"NumRow"
 BUNDLE.GET B3, keyNum$,NumRow

 TX1=TX1-BOFFX
 TY1=TY1-BOFFY
 xpos=CEIL(tx1/bxp)
 Ypos=CEIL(ty2/byp)
 IF xpos<1 THEN xpos=1
 IF xpos>NumCol THEN xpos=NumCol
 IF ypos<1 THEN ypos=1
 IF ypos>NumRow THEN ypos=NumRow
 keynum$=kn$+STR$(Xpos)+STR$(Ypos)
 BUNDLE.GET B3, keyNum$,keytouch
 IF keytouch=0
  key$="none"
 ELSE
  keyBNum$=kn$+"bttn"+STR$(keytouch)
  BUNDLE.GET B3, keyBNum$,key$
  TONE 525.25,140
 ENDIF
 type$=MID$(keytype$,keytouch,1)
 IF type$="1"
  !typeable character
  KeyPr$=MID$(keysprintable$,keytouch,1)
 ELSE
  KeyPr$=" "
 ENDIF

 !store keytouch,Key$,Type$,KeyPr$
 B1=1
 BUNDLE.PUT B1, "keytouch",keytouch
 BUNDLE.PUT B1, "Key$",Key$
 BUNDLE.PUT B1, "Type$",Type$
 BUNDLE.PUT B1, "KeyPr$",KeyPr$

FN.END
!----------------------------
FN.DEF L_Drawbuttons()
 !draw keyboard here.
 !draws the button lines/names on screen.
 !get bundle 1 info
 B1=1
 BUNDLE.GET B1, "NumBundles",NumBundles
 BUNDLE.GET B1, "screens",NumScreens
 BUNDLE.GET B1, "keyboards",NumKeyboards
 BUNDLE.GET B1, "DisplayMGX", DisplayMGX
 BUNDLE.GET B1, "DisplayMGY", DisplayMGY
 BUNDLE.GET B1, "DisplayStatus", DisplayStatusHight
 BUNDLE.GET B1, "KeyboardAlpha",KeyboardAlpha
 BUNDLE.GET B1, "KeyboardColor",KeyboardColor

 !get keyboard bundle number
 numbun=1

 kn$="kbrd"

 keynum$="NumOfButtons"
 B3=1
 BUNDLE.GET B3, keyNum$,NumBut
 DIM ButtonFlag[NumBut]

 FOR z=1 TO numkeyboards
  P$=" "+str$(z)
  L_write(P$)
  kn2$=kn$+STR$(z)
  !Set initial text size
  keynum$=kn2$+"BYP"
  BUNDLE.GET B3, keyNum$,BYP
  bts=CEIL(byp/1.7)
  GR.TEXT.SIZE bts
  GR.SET.STROKE 4
  GR.TEXT.BOLD 1

  !create BMP image, and paint it transparent.
  GR.SCREEN w, h
  GR.BITMAP.CREATE BPtr, w, h
  GR.BITMAP.DRAWINTO.START BPtr
  GR.COLOR 0,0,0,0
  GR.RECT nobj, 1, 1, w, h

  !retreive keyboard data
  keynum$=kn2$+"NumCol"
  BUNDLE.GET B3, keyNum$,NumCol
  keynum$=kn2$+"NumRow"
  BUNDLE.GET B3, keyNum$,NumRow
  keynum$=kn2$+"BXP"
  BUNDLE.GET B3, keyNum$,BXP
  keynum$=kn2$+"BYP"
  BUNDLE.GET B3, keyNum$,BYP
  keynum$=kn2$+"BOFFX"
  BUNDLE.GET B3, keyNum$,BOFFX
  keynum$=kn2$+"BOFFY"
  BUNDLE.GET B3, keyNum$,BOFFY

  ! numcol=buttongridsize[1,z]
  ! numrow=buttongridsize[2,z]
  ! bxp=buttongridsize[3,z]
  ! byp=buttongridsize[4,z]
  ! BOFFX=Buttongridsize[5,z]
  ! BOFFY=ButtonGridSize[6,z]
  nobj=0

  !set keyboard color -- transparent but visible
  !about 1/3 opacity / alpha
  CALL L_gcol(KeyboardAlpha,KeyboardColor,1)

  ARRAY.FILL buttonflag[], 0
  !search for shortest/longest button names.
  S1C=0
  nmc=0
  pmc=0
  smc=0
  FOR y=1 TO numrow
   FOR x=1 TO numcol
    !get button lengths
    keynum$=kn2$+"bttn"+STR$(X)+STR$(Y)
    BUNDLE.GET B3, keyNum$,B
    IF b>0
     L=b
     keyBNum$="bttn"+STR$(L)
     BUNDLE.GET B3, keyBNum$,B$
     L1=LEN(b$)
     IF L1>nmc
      pmc=L
      nmc=L1
     ENDIF
    ENDIF
   NEXT
  NEXT

  !calculate text sizes

  !make sure name fits square.
  !figure single-character name size
  a$="@"
  keynum$=kn2$+"1CX"
  BUNDLE.GET B3, keyNum$,B7
  keynum$=kn2$+"1CY"
  BUNDLE.GET B3, keyNum$,B8
  keynum$=kn2$+"MCX"
  BUNDLE.GET B3, keyNum$,B9
  keynum$=kn2$+"MCY"
  BUNDLE.GET B3, keyNum$,B10
  bw=bxp * B7
  bh=byp * B8*0.7
  loop1=0
  loop2=bh
  DO
   GR.TEXT.SIZE loop2
   GR.TEXT.WIDTH a,a$
   IF a<=bw
    loop1=1
   ELSE
    loop2=loop2-1
   ENDIF
  UNTIL loop1=1
  s1c=loop2

  !figure multi-character name size

  !  keyBNum$=kn2$
  !  BUNDLE.GET Numbun, keyBNum$,B$
  bw=bxp * B9
  bh=byp * B10*0.7
  loop1=0
  loop2=bh
  DO
   keyBNum$="bttn"+STR$(pmc)
   BUNDLE.GET B3, keyBNum$,B$
   GR.TEXT.SIZE loop2
   GR.TEXT.WIDTH a,b$
   IF a<=bw
    loop1=1
   ELSE
    loop2=loop2-1
   ENDIF
  UNTIL loop1=1
  smc=loop2

  !draw button lines
  ARRAY.FILL buttonflag[],0
  FOR y = 1 TO numrow
   FOR x = 1 TO numcol
    !get current button
    keynum$=kn2$+"bttn"+STR$(X)+STR$(Y)
    BUNDLE.GET B3, keyNum$,CB
    !calculate coordinates for lines.
    curl=x-1
    curl=curl*bxp
    curl=curl+1+BOFFX
    curr=x*bxp+BOFFX
    curt=y-1
    curt=curt*byp
    curt=curt+1+BOFFY
    curb=y*byp+BOFFY
    !get button values of adjacent squares
    IF x+1>numcol
     Rb=0
    ELSE
     keynum$=kn2$+"bttn"+STR$(X+1)+STR$(Y)
     BUNDLE.GET B3, keyNum$,rB
     !rb=button[x+1,y,z]
    ENDIF
    IF y+1>numrow
     bb=0
    ELSE
     keynum$=kn2$+"bttn"+STR$(X)+STR$(Y+1)
     BUNDLE.GET B3, keyNum$,bB
     !bb=button[x,y+1,z]
    ENDIF
    !compare button values, and draw lines.
    !Draw Right Line
    IF cb<>rb
     nobj=nobj+1
     GR.LINE a,curr,curt,curr,curb
     !keyboardobj[nobj,z]=a
    ENDIF
    !Draw bottom Line
    IF cb<>bb
     nobj=nobj+1
     GR.LINE a,curl,curb,curr,curb
     !keyboardobj[nobj,z]=a
    ENDIF
    !top row draw top line.
    IF CB>0 & Y=1
     nobj=nobj+1
     GR.LINE a,curl,curt,curr,curt
     !keyboardobj[nobj,z]=a
    ENDIF
    !left most column draw left line.
    IF cb>0 & x=1
     nobj=nobj+1
     GR.LINE a,curl,curt,curl,curb
     !keyboardobj[nobj,z]=a
    ENDIF

    !Check size of the button in squares.
    !Count from the upper left square of the button.
    IF cb>0
     IF ButtonFlag[cb]=0
      ButtonFlag[cb]=1

      !Counting to right.
      U=x
      V=0
      W=x
      Flagdo=0
      DO
       u=u+1
       IF u>numcol
        Flagdo=1
       ELSE
        keynum$=kn2$+"bttn"+STR$(u)+STR$(Y)
        BUNDLE.GET B3, keyNum$,V
        !V=button[u,y,z]
        IF v=cb
         w=u
        ELSE
         Flagdo=1
        ENDIF
       ENDIF
      UNTIL flagdo=1
      BCL1=x-1
      BCL=w-BCL1

      !Counting downwards.
      U=y6
      V=0
      W=y
      flagdo=0
      DO
       u=u+1
       IF u>numrow
        Flagdo=1
       ELSE
        keynum$=kn2$+"bttn"+STR$(X)+STR$(u)
        BUNDLE.GET B3, keyNum$,V
        !V=button[x,u,z]
        IF v=cb
         w=u
        ELSE
         Flagdo=1
        ENDIF
       ENDIF
      UNTIL flagdo=1
      bcd1=y-1
      BCD=w-BCD1

      !calculate button size in pixels
      BNPX=BCL * Bxp
      BNPY=BCD * Byp

      !Calculate position of button name
      keyBNum$="bttn"+STR$(cb)
      BUNDLE.GET B3, keyBNum$,B$
      L=LEN(b$)
      IF L=1
       loop2=s1c
      ELSE
       loop2=smc
      ENDIF
      GR.TEXT.SIZE loop2
      GR.TEXT.WIDTH a,b$
      a1=BNPX-a
      IF a1<1
       a2=0
      ELSE
       a2=a1 / 2
      ENDIF
      Axoff=curl+a2
      !axoff=FLOOR((bxp-a)/2)+curl
      a1=BNPY-loop2
      IF a1<1
       a2=0
      ELSE
       a2=a1 / 2
      ENDIF
      ayoff=curt+(BNPY * 0.5)+(loop2 * 0.5)

      !Draw the button name.
      nobj=nobj+1
      GR.TEXT.DRAW a,axoff,ayoff,B$
      !keyboardobj[nobj,z]=a
      GR.TEXT.SIZE bts
     ENDIF
    ENDIF
   NEXT x
  NEXT y
  GR.RENDER
  !buttongridsize[11,z]=nobj
  keyboard=z

  GR.TEXT.BOLD 0
  GR.SET.STROKE 0
  GR.BITMAP.DRAWINTO.END
  GR.BITMAP.DRAW GON, BPtr, 1 , 1
  GR.RENDER

  !Store keyboard data here
  !store screen GON here
  keyBNum$=kn2$+"GON"
  B3=1
  BUNDLE.PUT B3, keyBNum$,GON
  GR.HIDE GON
  GR.RENDER
 NEXT z
 P$=chr$(13)
 L_write(P$)

 !all done
FN.END

!----------------------------
FN.DEF L_hidekeyboard(kbrd)
 !get bundle 1 info
 B1=1
 BUNDLE.GET B1, "NumBundles",NumBundles
 BUNDLE.GET B1, "screens",NumScreens
 BUNDLE.GET B1, "keyboards",NumKeyboards
 BUNDLE.GET B1, "DisplayMGX", DisplayMGX
 BUNDLE.GET B1, "DisplayMGY", DisplayMGY
 BUNDLE.GET B1, "DisplayStatus", DisplayStatusHight
 BUNDLE.GET B1, "CurKeyboard", Keyboard

 numbun=1
 !Get keyboard GON number
 keyBNum$="kbrd"+STR$(kbrd)+"GON"
 B3=1
 BUNDLE.GET B3, keyBNum$,GON

 GR.HIDE GON

 GR.RENDER
FN.END
!----------------------------
FN.DEF L_showkeyboard(kbrd)
 !get bundle 1 info
 B1=1
 BUNDLE.GET B1, "NumBundles",NumBundles
 BUNDLE.GET B1, "screens",NumScreens
 BUNDLE.GET B1, "keyboards",NumKeyboards
 BUNDLE.GET B1, "DisplayMGX", DisplayMGX
 BUNDLE.GET B1, "DisplayMGY", DisplayMGY
 BUNDLE.GET B1, "DisplayStatus", DisplayStatusHight
 BUNDLE.GET B1, "CurKeyboard", Keyboard

 !get keyboard bundle number
 numbun=1
 !Get keyboard GON number
 keyBNum$="kbrd"+STR$(kbrd)+"GON"
 B3=1
 BUNDLE.GET B3, keyBNum$,GON

 GR.SHOW GON

 GR.RENDER
FN.END
!----------------------------
FN.DEF L_CurrentKeyboard(keyboard)
 B1=1
 BUNDLE.PUT B1, "CurKeyboard", Keyboard
FN.END
!----------------------------
FN.DEF L_LoadButtonData()
 !--------------
 !get bundle 1 info
 B1=1
 BUNDLE.GET B1, "NumBundles",NumBundles
 BUNDLE.GET B1, "screens",NumScreens
 BUNDLE.GET B1, "keyboards",NumKeyboards
 BUNDLE.GET B1, "DisplayMGX", DisplayMGX
 BUNDLE.GET B1, "DisplayMGY", DisplayMGY
 BUNDLE.GET B1, "DisplayStatus", DisplayStatusHight

 !put all keyboard data into this bundle
 Numbun = 1

 !Read the button data
 !read number of keyboards, dim array.
 READ.NEXT NumKeyboard
 B3=1
 BUNDLE.PUT B3,"keyboards",NumKeyboard

 READ.NEXT LargestGridX,LargestGridY
 BUNDLE.PUT B3,"LargestGridX",LargestGridX
 BUNDLE.PUT B3,"LargestGridY",LargestGridY

 !not needed, keep for info.
 !DIM buttongridsize[11,NumKeyboard],Button[LargestGridX,LargestGridY,NumKeyboard]

 n$="("+str$(numKeyboard)+" keyboards)"+chr$(13)
 L_write(n$)
 !read button grids
 FOR z=1 TO numKeyboard

  !base keyboard name for each keyboard
  kn$="kbrd"+STR$(z)

  READ.NEXT NumCol,NumRow
  keynum$=kn$+"NumCol"
  BUNDLE.PUT B3, keyNum$,NumCol
  keynum$=kn$+"NumRow"
  BUNDLE.PUT B3, keyNum$,NumRow

  keyBNum$=kn$+"bttn"
  FOR y = 1 TO NumRow
   !base Button name
   FOR X = 1 TO NumCol
    READ.NEXT B
    keynum$=keyBnum$+STR$(X)+STR$(Y)
    BUNDLE.PUT B3, keyNum$,B
   NEXT
  NEXT
  !calculate grid size per square.
  BXP=DisplayMGX / NumCol
  BXP=FLOOR(BXP)
  BYP=DisplayMGY / NumRow
  BYP=FLOOR(BYP)
  !This next section makes the buttons square.
  IF BXP>BYP
   BXP=BYP
  ELSE
   BYP=BXP
  ENDIF
  !calculate Keyboard Offsets
  BOFFX=DisplayMGX-(NumCol*BXP)
  IF BOFFX=0
   BOFFX=0
  ELSE
   BOFFX=FLOOR(BOFFX/2)
  ENDIF
  BOFFY=DisplayMGY-(NumRow*BYP)
  IF BOFFY=0
   BOFFY=0
  ELSE
   BOFFY=FLOOR(BOFFY/2)
  ENDIF
  !Store the keyboard info
  keynum$=kn$+"BXP"
  B3=1
  BUNDLE.PUT B3, keyNum$,BXP
  keynum$=kn$+"BYP"
  BUNDLE.PUT B3, keyNum$,BYP
  keynum$=kn$+"BOFFX"
  BUNDLE.PUT B3, keyNum$,BOFFX
  keynum$=kn$+"BOFFY"
  BUNDLE.PUT B3, keyNum$,BOFFY

  !read and store the last stuff.
  READ.NEXT B
  keynum$=kn$+"1CX"
  BUNDLE.PUT B3, keyNum$,B
  READ.NEXT B
  keynum$=kn$+"1CY"
  BUNDLE.PUT B3, keyNum$,B
  READ.NEXT B
  keynum$=kn$+"MCX"
  BUNDLE.PUT B3, keyNum$,B
  READ.NEXT B
  keynum$=kn$+"MCY"
  BUNDLE.PUT B3, keyNum$,B
 NEXT

 !Build keytype$, and KeyPrintable$
 KeyType$=""
 KeysPrintable$=""
 READ.NEXT NumBut
 keynum$="NumOfButtons"
 BUNDLE.PUT B3, keyNum$,NumBut

 !needed, keep for info
 DIM button$[NumBut], ButtonFlag[NumBut]

 !base Button name
 kN$="bttn"

 FOR x = 1 TO NumBut
  keyBNum$=kn$+STR$(x)

  READ.NEXT a$
  a=LEN(a$)-2
  B$=LEFT$(a$,a)
  BUNDLE.PUT B3, keyBNum$,B$
  a1$=RIGHT$(a$,2)
  KeysPrintable$=KeysPrintable$+LEFT$(a1$,1)
  KeyType$=keytype$+RIGHT$(a1$,1)
 NEXT

 !read Key fixes.
 !These keys can't appear in the data statements.
 !Need to put them in manually.
 k=LEN(keysprintable$)
 !read number of fixes
 READ.NEXT r1
 IF r1>0
  FOR x=1 TO r1
   READ.NEXT r2,r3
   r4=r2-1
   r5=k-r2
   b$=CHR$(r3)
   r1$=LEFT$(keysprintable$,r4)
   r2$=RIGHT$(keysprintable$,r5)
   r3$=r1$+b$+r2$
   keysprintable$=r3$
   keyBNum$=kn$+STR$(r2)
   BUNDLE.PUT B3, keyBNum$,B$
  NEXT
 ENDIF

 !Store KeysPrintable$, and KeyType$
 BUNDLE.PUT B1, "KeysPrintable",KeysPrintable$
 BUNDLE.PUT B1, "KeyType",KeyType$

FN.END


!keyboard data blank -- LEAVE REMARKED!
!This is here to show the keyboard data file format.
!read.data "","","","","","","","","",""
!----------------------------
!Example Keyboard data format -- change as needed.
!There are Data Include files of various keyboards.
!KEYBOARD button data
!number of keyboards
!read.data 1
!LargestGrid X,Y
!Read.Data 10,6
!keyboard#1
!number of columns, number of rows
!READ.DATA 10,6
!button grid - layout the button pattern here.
! numbers refer to the button list below.
! 00 - no button there.
! repeat button number in adjacent positions for bigger buttons.
!Read.data 01,00,00,00,00,00,00,00,00,02
!read.data 00,00,00,00,00,00,00,00,00,00
!read.data 71,72,73,74,75,76,77,78,79,80
!read.data 16,81,82,83,84,85,86,87,88,89
!read.data 26,27,90,91,92,93,94,95,96,35
!read.data 03,36,37,38,04,04,04,39,05,05
!Button size: Single Character
!Read.data 1,1
!Button Size: Multiple characters
!Read.data 1,1
!keyboard #2, etc.
!number of unique button names
!READ.DATA 98
!format: button name, printed character, keytype (0 = control key, 1 = printable key)
!buttons are listed in groups of 10 per line. (Just to make it easier to count.)
!read.data "MENU 0","EXIT 0","SWITCH 0","SPACE 1","ENTER 0","QQ1","WW1","EE1","RR1","TT1"
!read.data "YY1","UU1","II1","OO1","PP1","TAB 0","AA1","SS1","DD1","FF1"
!read.data "GG1","HH1","JJ1","KK1","LL1","SHIFT 0","!!1","ZZ1","XX1","CC1"
!read.data "VV1","BB1","NN1","MM1","BACK 0","Underscore_1","QUOTE 1","COMMA 1","..1","111"
!read.data "221","331","441","551","661","771","881","991","001","@@1"
!read.data "##1","$$1","%%1","&&1","**1","--1","++1","((1","))1","||1"
!read.data "~~1","  1","''1","::1",";;1","//1","??1","==1","<<1",">>1"
!read.data "qq1","ww1","ee1","rr1","tt1","yy1","uu1","ii1","oo1","pp1"
!read.data "aa1","ss1","dd1","ff1","gg1","hh1","jj1","kk1","ll1","zz1"
!read.data "xx1","cc1","vv1","bb1","nn1","mm1","[[1","]]1"
!this next data section for key info that is not include-able in data statments.
!keys to be fixed
!number of keys
!read.data 1
!keys -- format: button number, ascii value.
!read.data 37,34
!--------------------------------------------------------------
!FindGraphics
!----------------------------
!Various Graphics setup and color functions.
!----------------------------
!The Color Names and their Codes:
!  Black, Red, Lime, Blue, Yellow, Cyan, Magenta, Grey, White
!  Navy, Green, Teal, Maroon, Purple, Olive, Brown, Silver
!  Tan, Orange, Gold
!
!Twenty different common colors.
!web safe color level numbers:
! (hex) 00  33  66  99  CC  FF
! (dec) 00  33  66  99 204 255
!
!000000 - Black
!000099 - Navy
!0000ff - Blue
!009900 - Green
!009999 - Teal
!00ff00 - Lime
!00ffff - Cyan
!990000 - Maroon
!990090 - Purple
!999900 - Olive
!999999 - Grey
!993333 - Brown
!CCCCCC - Silver
!FFCC99 - Tan
!ff0000 - Red
!ff00ff - Magenta
!ff9900 - Orange
!ffCC00 - Gold
!ffff00 - Yellow
!ffffff - White
!----------------------------
FN.DEF L_gcol(Alpha,color,fill)
!color number can be 1-20,
!Or it can be 9 or 10 digits long.
!The leading 1 only holds the number at 10 digits.
!(example: 1255204099 = Tan see above)

 C$=str$(color)
 c=len(c$)
 if c>8
  !get color codes from color number itself
  cc$=right$(c$,9)
  c1$=mid$(cc$,1,3)
  c2$=mid$(cc$,4,3)
  c3$=mid$(cc$,7,3)
  c1=val(c1$)
  c2=val(c2$)
  c3=val(c3$)
 else
  !get color codes from colorstring$
  B1=1
  BUNDLE.GET B1, "colorstring",Colorstring$
  cp=color*7-6
  c$=MID$(colorstring$,cp,7)
  c1$=mid$(c$,1,2)
  c2$=MID$(C$,3,2)
  c3$=MID$(C$,5,2)
  c1=hex(c1$)
  c2=hex(c2$)
  c3=hex(c3$)
 endif
 GR.COLOR Alpha, c1, c2, c3, Fill
FN.END
!Written by:
!  Donald Hosford (Donzerme@Yahoo.com)
!
!The credits:
!  Roy -- His Excellent Idea to simplify the colors! Why didn't I think of it!
!  Mougino -- Nice idea to reduce the color codes to a short function.
!  Me -- I added 11 more "web safe" colors.  Added all the colors in a string.
!
!Input:
!  Alpha -- how solid the colors.  0 - transparent, 255 - solid.
!  Color -- the desired color (0 to 19).
!  Fill -- 0 - outline, 1 - filled it.
!
!Source:
!  This is from a couple of ideas by Roy and Mougino on the rfo.basic.forum.com.
!  Hex codes for twenty common colors, spaces are just separators.
!----------------------------
FN.DEF L_OpenGR(Alpha,color,Status,Orientation)
!OpenGR function replaces GR.open,Pause1000, and Gr.orientation.
 B1=1
 BUNDLE.GET B1, "colorstring",Colorstring$
 cp=color*7-6
 c$=MID$(colorstring$,cp,7)
 GR.OPEN Alpha, HEX(MID$(c$,1,2)), HEX(MID$(C$,3,2)), HEX(MID$(C$,5,2)), Status, Orientation
 PAUSE 1000
FN.END
!Written by:
!  Donald Hosford (SirDonzer@Yahoo.com)
!
!The credits:
!  This is based on My version of Roy's GCOL function.
!Input:
!  Alpha -- how solid the colors.  0 - transparent, 255 - solid.
!  Color -- the desired color (0 to 19).
!  Status -- Show/hide the status bar.  0 - outline, 1 - filled it.
!  Orientation -- What screen orientation.  0 - Lanscape, 1 - Portrait.
!
!Source:
!  This is based on my version of Roy's GCOL function.
!  See the GCOL notes.
!
!The Colors: (his first)
!  Black, Red, Lime, Blue, Yellow, Cyan, Magenta, Grey, White
!  Navy, Green, Teal, Maroon, Purple, Olive, Brown, Silver
!  Tan, Orange, Gold
!--------------------------------------------------------------
!FindMiscellanious
!----------------------------
!Various miscellanious functions.
!
!----------------------------
FN.DEF L_NumberClip$(Number,Number$)
 !take the number, clip off the decimals.
 Number$=STR$(number)
 L=LEN(number$)
 P=IS_IN(".",number$)
 Number$=LEFT$(number$,P-1)
 IF Number<10 THEN number$=" "+number$
 FN.RTN Number$
FN.END
!--------------------------------------------------------------
!FindTest Program
!----------------------------
!Main test program
!Use this to solve problems with the library code.
!----------------------------
!rem out this next line to run test program.
goto Skiptest

INCLUDE IncDataKeyboardFull.bas
!---------------------------------------------
!setup display/keyboard
L_DisplaySetup(1,0,1)
L_addscreen(80,25,10)
L_keyboardsetup(200,9)
!---------------------------------------------
!test program:

!switch to screen 1
L_changescreen(1)

! set foreground/background colors
L_setcolors(5,10)

!write stuff on screen 1
L_locate(1,1)
L_write("Screen Keyboard Demo")
L_locate(2,3)
L_write("Hello World!")
L_locate(2,4)
L_write("Hi There!")
q$="This IS concatination."
L_write(q$)
Q$="  And so is this."
L_write(Q$)
Q$="Really!"
l_write(Q$)

!show all keyboards
b3=1
BUNDLE.GET B3,"keyboards",NumKeyboard
if NumKeyboard>0
 L_showkeyboard(1)
 if NumKeyboard>1
  for x=2 to NumKeyboard
   gosub gettouch
   L_hidekeyboard(x-1)
   L_showkeyboard(x)
  next x
 endif
 GOSUB gettouch
endif

END

Skiptest:

