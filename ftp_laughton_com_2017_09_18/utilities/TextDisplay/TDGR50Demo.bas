REM Testprog12.bas
!Setup block
!INCLUDE includetdgrv4.2.bas
!--------------
vmx=80
vmy=28
TSZ=26   %text size
ORI=0
GOSUB screensetup

!--------------
!Main Program
!screen movement tests

Gosub SetupTestScreen

programstop = 0
DO
 GOSUB RealTimetouch
UNTIL programstop = 1

END

!--------------
SetupTestScreen:

t3=ceil(vmx/10)
T$="+---------"
t2$="!         "
FOR t=1 TO t3
 T1$=T1$+T$
 t3$=t3$+t2$
NEXT t
T1$=Left$(T1$,VMX-1)+"+"
T3$=Left$(T3$,VMX-1)+"!"
t3=Ceil(vmy/5)
FOR T=1 TO t3
 FOR t2=1 TO 5
  T4=T-1
  T4=T4*5
  T4=T4+T2
  IF T4 <= VMY 
   tcx=1
   TCY=t-1
   TCY=TCY*5
   TCY=TCY+t2
   IF t2=1 THEN txt$=t1$ ELSE txt$=t3$
   GOSUB writetoscreen
  Endif
 NEXT
NEXT
TCX=1
TCY=VMY
TXT$=T1$
Gosub writetoscreen

sd$="001002c"
txt$="Testprog12.bas
GOSUB writetoscreen

sd$="003007"
txt$="Hardware Screen Size:"
GOSUB writetoscreen

SD$="003008"
TXT$=STR$(MX)+" Columns by "+STR$(MY)+" Rows"
GOSUB writetoscreen

SD$="003012"
TXT$="Text Size: "+str$(TSZ)
Gosub writetoscreen

sd$="003009"
txt$="Virtual Screen Size:"
GOSUB writetoscreen

sd$="003010"
txt$=STR$(vmx)+" "+STR$(vmy)
GOSUB writetoscreen

!This shows that flags can be set alone.
TCX=5
TCY=04
SD$="c"
TXT$="Use screen swipes to move it around."
GOSUB WriteToScreen

return

!--------------------------
RealTimetouch:
!This subroutine does both zooming, and screen moves.
!ie: Touch screen with two fingers, move them togather or apart, lift off screen.
!Subroutine updates screen.
!zooming in/out works fine!  Execelent!
!scrolling the screen jumps around yet.
!calculate screen size for zoom distance
py=CEIL(MGX / 40)
!loop until the screen is touched
touch = 0
touch2 = 0
AccX = 0
AccY = 0
AccZ = 0
T1X1=0
t1y1=0
t2x1=0
t2y1=0
DO
 GR.TOUCH touch,t1x1,t1y1
 GR.TOUCH2 touch2, t2x1, t2y1
UNTIL touch = 1
NumberTouched=touch + touch2
IF numbertouched=1
 AccX=0
 AccY=0
ENDIF
IF numbertouched =2
 !calculate first touch distance.
 A1=T1X1
 A2=T1Y1
 A3=T2X1
 A4=T2Y1
 GOSUB Distance
 Distance1=distance
ENDIF
!AccZ=0
!loop until screen is not touched
DO
 GR.TOUCH touch,t1x2,t1y2
 GR.TOUCH2 touch2,t2x2, t2y2
 numbertouched=touch+touch2
 IF numbertouched=1
  GOSUB ProcessSwipes
 ENDIF
 IF numbertouched=2
  GOSUB ProcessZooms
 ENDIF
 !redraw screen
 IF numbertouched=2 THEN GOSUB DrawScreen
 GOSUB checkhardwarescreenposition
 tx1=tx2
 ty1=ty2
UNTIL numbertouched = 0
AccX = 0
AccY = 0
AccZ = 0
RETURN

ProcessSwipes:
!Process Swipes.
tx2=t1x2
ty2=t1y2
TXA=tx2-tx1
TYA=TY2-TY1
AccX=AccX+FLOOR(txa)
AccY=AccY+FLOOR(tya)
!check to see if enough accumulated to move 1 character.
!Loop this next section until no more characters to move.
DO
 lpx=accx/tw
 lpy=accy/tsz
 IF lpx < -1
  hsx=hsx+2
  accx=accx+tw
 ENDIF
 IF lpx > 1
  hsx=hsx-2
  accx=accx-tw
 ENDIF
 IF lpy < -1
  hsy=hsy+1
  accy=accy+tsz
 ENDIF
 IF lpy > 1
  hsy=hsy-1
  accy=accy-tsz
 ENDIF
 loopa=0
 IF lpx < -1 THEN loopa=1
 IF lpx > 1 THEN loopa=1
 IF lpy < -1 THEN loopa=1
 IF lpy > 1 THEN loopa=1
UNTIL loopa=0
GOSUB checkhardwarescreenposition
RETURN

ProcessZooms:
!Process Zooms.
!calculate second touch distance
A1=t1x2
A2=t1Y2
A3=t2X2
A4=t2y2
GOSUB distance
Distance2=distance
td=distance2 - distance1
AccZ=AccZ+td
Distance1 = distance2
px =  accz / py
IF px > 1
 tsz = tsz +2
 !  Accz = Accz - py
 AccZ = 0
ENDIF
IF px < -1
 Tsz = Tsz -2
 !  Accz = Accz + py
 Accz = 0
ENDIF
IF TSZ < 5 THEN TSZ=5
IF TSZ > (MGX/2) THEN TSZ=CEIL(MGX/2)
TSZ=CEIL(TSZ)
!update screen info
SD$="003008"
TXT$=STR$(MX)+" Columns by "+STR$(MY)+" Rows"
GOSUB writetoscreen
SD$="003012"
TXT$="Text Size: "+str$(TSZ)
Gosub writetoscreen
RETURN

Distance:
!Calculates Distance between two points.
!point 1 X = A1
!point 1 Y = A2
!point 2 X = A3
!point 2 Y = A4
!output = Distance
distance=0
D1=a3-a1
D2=a4-a2
S1=D1*D1
S2=D2*D2
T=S1+S2
r= SQR(T)
Distance=CEIL(R)
RETURN

!--------------
!screen Movement Notes
!!
1) pick out screen moves.
  Two types:
    A) D-pad keys/keyboard presses.
      Use the d-pad keys to move the screen.  Say 1/2 screen at a time.
      D-pad keys are returned as "up", "down", "left", "right", and "go".
    B) Touch screen swipes.
      1) Use real-time screen updates.  (I don't think RFO basic is fast enough.)
      2) Use screen "swipes".  Touch screen, drag finger, lift off screen.
      In both cases, find number of rows/columns moved, and update hardware screen
      position.
2) trap screen coordinates, and fix problems.
3) redisplay screen.
4) loop for next keypress/screen touch.
VMX/VMY = Virtual screen size.
MX/MY = hardware screen size.
HSX/HSY = location of Upper left corner of hardware screen on the virtual screen.
Box2 = graphical object number of Hardware Screen Box.

!half hardware screen sizes
!deturmines how far screen moves each key press.
HMX = FLOOR(MX/2)
HMY = FLOOR(MY/2)
!!
!------------------------------------------------
!------------------------------------------------
!INCLUDE includetdgrv50.bas
!!
A00:IncludeTDGRv50.bas
  Include Text Display Graphic (Version 5.0)
By: Donald Hosford (donzerme@yahoo.com)
About: This is an include file for an addressable
 text display on the graphic screen.

Instructions:
 Setup: In your program, include the following code at the start of your program:
( Change these to suit yourself.)

INCLUDE IncludeTDGRv50.bas
! Set Screen Orientation: 0=lanscape, 1=portrait
  ORI = 0
! Set Text Size: Any size you want.
  TSZ = 26
!Virtual Screen Size
!  VMX - is width of screen in characters.
!  VMY - is hight of screen in rows of characters.
  VMX = 80
  VMY = 28
gosub ScreenSetup

 Use:
 1) Set TXT$ to your text.
   Can be any text.
 2) Set the coordinates.
   Using TCX/TCY:
    Set SD$ to "" (null).  Then set TCX/TCY to the
    desired coordinates.
   Using SD$ -- format:
   chars 1 - 3 -- X axis (columns)
   chars 4 - 6 -- Y axis (rows)
   chars 7+ -- flags:
     c -- center TXT$ on row.  TXT$ must be shorter than VMX.
 3) Gosub WriteToScreen.

 Clearing the screen:
 Gosub ClearScreen

Subroutines:
--------------------
ScreenSetup:
 Sets up the screen, must be run first.
Screenpause:
 Pauses the screen.  Only works on the graphic screen.
ClearScreen:
 Clears the screen.
WriteToScreen:
 Writes Txt$ to the screen, then displays it.

!Main  program
!--------------------
!Shows various printing techniques.
!This line shows the centering function. The 'c'.
SD$="0101c"
TXT$="Text Screen Demo"
GOSUB WriteToScreen

!!

!CLS
END

ScreenSetup:
!--------------------
!A01: screen setup
!Must be done at the start of the program.
![a]Setup arrays
DIM SRF[160], GON[160]	%setup ScreenRowFlag[], GraphicalObjectNumber[]!Set text size.
!Position of Hardware screen on virtual screen -- Start in upper left corner.
HSX = 1
HSY = 1
!start graphics screen
GR.OPEN 255, 0, 255, 0		%open graphic screen with a green background
PAUSE 1000
!Maximum screen position. Also number of characters on "screen".
MSP=VMX * VMY
!Build Blank Screen - Do it fast.
Screen$=""
A01a$="                    "
A01B$=""
FOR A01b=1 TO 5
 A01B$=A01B$+A01A$
NEXT A01b
A01a=MSP/100
A01A=CEIL(A01a)
FOR A01X=1 TO A01a
 Screen$=Screen$+A01B$
NEXT A01X
Screen$=LEFT$(Screen$,MSP)
Background$=Screen$
!Setup Text Cursor.  
TCX=1		%Cursor X Position
TCY=1		%Cursor Y Position
TCP=1		%Cursor Position in Screen$


DrawScreen:
!A02: Draw Screen.
!Re-run whenever TSZ or orientation changes. 
!setup hardware screen
GR.CLS
GR.ORIENTATION ORI 	%Set orientation
GR.COLOR 255, 0, 0, 0, 0 	%black foreground color
GR.SCREEN MGX,MGY		%get screen size
GR.TEXT.SIZE TSZ		%set text size
GR.TEXT.TYPEFACE 2		%set mono spaced font
A02T$="A"
GR.TEXT.WIDTH tw,A02T$		%Gets the width of single character.
MX=FLOOR(mgx/tw)		%figure MX
MY=FLOOR(mgy/tsz) 		%figure MY
!check for virtual screen smaller than hardware screen.
IF VMX < MX THEN MX=VMX
IF VMY < MY THEN MY=VMY
!set offsets
OFFX=MGX-(MX*TW)
IF OFFX>0
 OFFX=FLOOR(OFFX/2)
ELSE
 OFFX=0
ENDIF
OFFY=MGY-(MY*TSZ)
IF OFFY>0
 OFFY=FLOOR(OFFY/2)
ELSE
 OFFY=0
ENDIF
!Draw text objects, get the object numbers
A02LPZ=TSZ+OFFY
FOR A02LPX = 1 TO MY
 GR.TEXT.DRAW GON[A02LPX],1+OFFX,A02LPZ,CHR$(32)
 A02LPZ=A02LPZ+TSZ
NEXT A02LPX
!draw location boxes.

gr.color 128,0,0,0,0

boxright=VMX*2
boxbottom=VMY*2
Gr.rect Box1, 2, 2, boxright, boxbottom

gr.color 128,128,0,0,0

boxright=(HSX+MX)*2-1
boxbottom=(HSY+MY)*2-1
Gr.rect Box2, HSX, HSY, boxright, boxbottom
gr.render

gr.color 255,0,0,0,0

RETURN

ClearScreen:
!------------------
!A03:ClearScreen
FOR A03LPX=1 TO MY
 GR.MODIFY GON[A03LPX],"text",CHR$(32)
NEXT A03LPX
FOR A03lpx=1 TO vmy
 srf[A03lpx]=0
NEXT A03lpx
Screen$=Background$
GR.RENDER
RETURN

WriteToScreen:
!------------------
!A04: Write To Screen
!Get Screen coordinates and flag Data
!ReadFlags:
!Read SD$ for coordinates, and flags.
if len(sd$)<6
 SD$="000000"+sd$
endif
CX=0
CY=0
readSd$=LEFT$(SD$,6)
cx=val(left$(SD$,3))
CY=val(mid$(SD$,4,3))
if CX=0
 cx=tcx
endif
if CY=0
 cy=tcy
endif
for readsd = 1 to len(sd$)
 readsd$=mid$(sd$,readsd,1)
 if readsd$="c"
  TCN=1
 else
  TCN=0
 endif
! add other letter codes here.
next

txtLen=LEN(TXT$)
rightend=CY-1

!Screen center flag.
IF TCN=1
 Charleft=VMX-txtLen
 IF charleft>1
  Charleft2=Charleft/2
  CX=FLOOR(Charleft2)
 ELSE
  CX=1
 ENDIF
ENDIF

!Write Text to Screen$.
SP=(CY-1)*VMX+CX
IF SP>1
 Num=SP-1
 Screen1$=LEFT$(Screen$,(SP-1))
ELSE
 Screen1$=""
ENDIF
Screen2$=TXT$
IF SP<MSP
 Num=MSP-(SP+txtLen-1)
 Screen3$=RIGHT$(Screen$,Num)
ELSE
 Screen3$=""
ENDIF
Screen$=Screen1$+Screen2$+Screen3$

!Move Text Cursor to end of Text.
SP=SP+txtLen
CX=CX+txtLen
DO
 CX=CX-VMX
 CY=CY+1
UNTIL cx<=VMX
TCX=CX
TCY=CY
rightend2=CY

!clear variables and flags.
TXT$=""
SD$=""
TCN=0
IF rightend<1
 rightend=1
ENDIF
IF Rightend=Rightend2
 SRF[rightend]=1
ELSE
 FOR endloop=rightend TO rightend2+1
  srf[endloop]=1
 NEXT
ENDIF

!DisplayRow
!Draw Only affected screen rows.
A04TT$=""
FOR A04LPY=HSY TO HSY+MY-1
if SRF[A04lpy]=1 
 A04LPX=A04LPY * VMX
 A04LPZ=A04LPX+HSX
 A04TT$=MID$(Screen$,A04LPZ,MX)
 GR.MODIFY GON[A04LPY],"text",A04TT$
endif
NEXT
GR.RENDER
RETURN

DisplayScreen:
!A05: Display screen - all of it.
!Draw whole screen.
A05TT$=""
A05LPX=1
FOR A05LPY=1 TO MY
 A05p=A05LPY+HSY-2
 A05p=A05p * VMX
 A05LPZ=A05p+HSX
 A05TT$=MID$(Screen$,A05LPZ,MX)
 GR.MODIFY GON[A05LPX],"text",A05TT$
 A05LPX=A05LPX+1
NEXT
GR.RENDER
RETURN

DisplayOutputConsole":
!A06: Display Screen$ on output console.
PRINT"------"
!my 7" tablet landscape is 90 characters wide.
A06o=90-mx
FOR A06y=1 TO MY
 A06p=A06y+HSY-2
 A06p=A06p * VMX
 A06p=A06p+HSX
 FOR A06x=1 TO MX
  A06a$=MID$(screen$,A06p,1)
  IF A06a$=" " THEN print "."; ELSE print A06a$;
  A06P=A06P+1
 NEXT A06x
 FOR A06z=1 TO A06o
  PRINT"-";
 NEXT A06z
NEXT A06y
PRINT
PRINT "------"
return

Screenpause:
!--------------------------
!A07: Touch Screen Pause
!pauses until touch screen is touched.  Works only on graphics screen.  Or press a key.
key=0
WHILE key=0
 GR.TOUCH key,x,y
 INKEY$ k$
 IF LEN(k$) >1
  key=1
 ENDIF
REPEAT
RETURN

CheckHardWareScreenPosition:
!--------------------------
!A08: Check Hardware Screen Position, and fix errors.
!Lower right corner hardware screen.
HSM1=MX+HSX-1
HSM2=MY+HSY-1
!Keep hardware screen on virtual screen.
IF HSX<1
 HSX=1
ENDIF
IF HSM1>VMX
 HSX=VMX-MX+1
ENDIF
IF HSY<1
 HSY=1
ENDIF
IF HSM2>VMY
 HSY=VMY-MY+1
ENDIF
!redisplay screen
GOSUB displayscreen
!move hardware screen box
HSBx1=hsx*2
HSBx2=(hsx+MX) * 2 - 2
HSBy1=hsy*2
HSBy2=(hsy+MY) * 2 - 2
GR.MODIFY Box2, "left", HSBx1
GR.MODIFY box2, "right", HSBx2
GR.MODIFY Box2, "top", HSBy1
GR.MODIFY box2, "bottom", HSBy2
GR.RENDER
RETURN
