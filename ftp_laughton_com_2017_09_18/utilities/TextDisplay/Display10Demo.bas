!----------------------------
!----------------------------
!Program Name: Display10Demo.bas
!Purpose: An all included version.  Suitable to start a program with.
!----------------------------
!Notes:
! This StartBlock contains all functions/subroutines for my Display10 text display.
! Just insert your code where it says: Main program.
!----------------------------
!Functions:
!
!So far:
!  GCOL(Alpha,Color,Fill)
!  OpenGR(Alpha,Color,Status,Orientation)

!  Note: GCOL and OpenGR use SEPARATE ColorString$.
!----------------------------
FN.DEF gcol(Alpha,color,fill)
 C1$="000000 ff0000 00ff00 0000ff ffff00 00ffff ff00ff 999999 ffffff 000099 "
 C2$="009900 009999 990000 990099 999900 993333 CCCCCC FFCC99 FF9900 FFCC00 "
 ColorString$=c1$+c2$
 cp=color*7-6
 c$=MID$(colorstring$,cp,7)
 GR.COLOR Alpha, Hex(Mid$(c$,1,2)), Hex(Mid$(C$,3,2)), Hex(Mid$(C$,5,2)), Fill
FN.END
 !Written by:
 !  Donald Hosford (SirDonzer@Yahoo.com)
 !
 !The credits:
 !  Roy -- His Excellent Idea to simplify the colors! Why didn't I think of it!
 !  Mougino -- Nice idea to reduce the color codes to a short function.
 !  Me -- I added 11 more "web safe" colors.  Added all the colors in a string.
 !
 !Input:
 !  Alpha -- how solid the colors.  0 - transparent, 255 - solid.
 !  Color -- the desired color (0 to 19).
 !  Fill -- 0 - outline, 1 - filled it.
 !
 !Source:
 !  This is from a couple of ideas by Roy and Mougino on the rfo.basic.forum.com.
 !  Hex codes for twenty common colors, spaces are just separators.
 !
 !The Colors: (his first)
 !  Black, Red, Lime, Blue, Yellow, Cyan, Magenta, Grey, White
 !  Navy, Green, Teal, Maroon, Purple, Olive, Brown, Silver
 !  Tan, Orange, Gold
!!
Twenty different common colors.
web safe color level numbers: (hex) 00 33 66 99 CC FF

000000 - Black
000099 - navy
0000ff - blue
009900 - green
009999 - teal
00ff00 - lime
00ffff - cyan
990000 - maroon
990090 - purple
999900 - olive
999999 - grey
993333 - Brown
CCCCCC - silver
FFCC99 - Tan
ff0000 - red
ff00ff - magenta
ff9900 - orange
ffCC00 - Gold
ffff00 - yellow
ffffff - white
!!
!----------------------------
!OpenGR function replaces GR.open,Pause1000, and Gr.orientation.
FN.DEF OpenGR(Alpha,color,Status,Orientation)
 C1$="000000 ff0000 00ff00 0000ff ffff00 00ffff ff00ff 999999 ffffff 000099 "
 C2$="009900 009999 990000 990099 999900 993333 CCCCCC FFCC99 FF9900 FFCC00 "
 ColorString$=c1$+c2$
 cp=color*7-6
 c$=MID$(colorstring$,cp,7)
 GR.Open Alpha, Hex(Mid$(c$,1,2)), Hex(Mid$(C$,3,2)), Hex(Mid$(C$,5,2)), Status, Orientation
 pause 1000
FN.END
 !Written by:
 !  Donald Hosford (SirDonzer@Yahoo.com)
 !
 !The credits:
 !  This is based on My version of Roy's GCOL function.
 !Input:
 !  Alpha -- how solid the colors.  0 - transparent, 255 - solid.
 !  Color -- the desired color (0 to 19).
 !  Status -- Show/hide the status bar.  0 - outline, 1 - filled it.
 !  Orientation -- What screen orientation.  0 - Lanscape, 1 - Portrait.
 !
 !Source:
 !  This is based on my version of Roy's GCOL function.
 !  See the GCOL notes.
 !
 !The Colors: (his first)
 !  Black, Red, Lime, Blue, Yellow, Cyan, Magenta, Grey, White
 !  Navy, Green, Teal, Maroon, Purple, Olive, Brown, Silver
 !  Tan, Orange, Gold
!----------------------------

!----------------------------
!Program Setup
!----------------------------
!setup graphics
! Set these to suit your project.
!Current screen
curdisplay=0

!change this to the number of screens you need.
DisplayMaxScreens=1

! screen array
Dim DisplaySc[DisplayMaxScreens,16]

!Load screen data
!Set these once
DisplayORI=1
DISPlayBoarder=10
!Fancy gr.render toggle, leave at 1.
DisplayRender=1

!Setup each screen here.
!NumType is the number of that type if you want.

!Screen Type 1
NumType=1
DisplayMX=15
DisplayMY=9
DisplayBackground=20
DisplayStatus=0
gosub Loadscreen

!Screen Type 2
!NumType=1
!DisplayMX=80
!DisplayMY=28
!DisplayBackground=3
!DisplayStatus=0
!gosub LoadScreen

!Store number of screens
DisplayMaxScreens=curdisplay
!----------------------------
!Call setup routines here.
Gosub DisplaySetup

!switch to screen 1.
newscreen=1
gosub ChangeDisplay
!----------------------------
!MAIN Program
!----------------------------
!Place your program code here. 

txt$="CoolTextDemo"
!tcx=1:tcy=1:cf=1:cb=3:gosub write
!tcx=12:tcy=1:cf=1:cb=3:gosub write
!tcx=14:tcy=2:cf=1:cb=3:gosub write
!tcx=14:tcy=3:cf=1:cb=3:gosub write
!tcx=15:tcy=4:cf=1:cb=3:gosub write

tcx=1:tcy=1:cf=1:cb=3
x1=DisplaySc[curdisplay,1] * DisplaySc[curdisplay,2]
x2=len(TXT$)
x3=ceil(x1/x2)
for x=1 to x3
 gosub write
next

gosub gettouch


End
!----------------------------
!Any program data must appear AFTER this include statement.
!----------------------------
! IncDisplay10.bas
!----------------------------
!Purpose: Do an include file with just the new text display in it.
!DisplayORI=0
!DISPlayBoarder=10
!DisplayRender=1

!DisplaySc[] format:
!Screen Type 2
!NumType=1
! DisplayMX=DisplaySc[X,1]
! DisplayMY=DisplaySc[X,2]
! DisplayBackground=DisplaySc[X,3]
! DisplayStatus=DisplaySc[X,4]
! DisplaySc[X,5]=Rowhigh
! DisplaySc[X,6]=ColumnWide
! DisplaySc[X,7}=DisplayTSZ
! DisplaySc[X,8]=DisplayOffY
! DisplaySc[X,9]=DisplayOffX
! DisplaySc[X,10]=ScreenPTR
! DisplaySc[X,11]=GON
! DisplaySc[x,12]=Last TCX
! DisplaySc[x,13]=Last TCY


!DisplayTW   - Text Width (Pixels)

LoadScreen:
!loads screen data into DisplaySc[].
for x = 1 to NumType
 curdisplay=curdisplay+1
 DisplaySc[curdisplay,1]=DisplayMX
 DisplaySc[curdisplay,2]=DisplayMY
 DisplaySc[curdisplay,3]=DisplayBackground
 DisplaySc[curdisplay,4]=DisplayStatus
next
Return

DisplaySetup:
!setup graphics
!New setup subroutine auto figures settings.
!Set MX, MY, and ORI.  Gosub DisplaySetup
!DisplayMY   - Number of Displayed Rows.
!DisplayMX   - Number of Displayed Columns.
!DisplayORI  - Screen orientation. Portrait = 1, Landscape = 0
!DisplayBackground - Set ColorBackground to color number from GCOL. 
!DisplayBoarder - Set to color number from GCOL.
!DisplayStatus - Show/Hide status bar. 0 = Hide, 1 = Show.
!DisplayRender - Set 0 to NOT gr.render, 1 to gr.render every time.
!-----
!Setup figures TSZ, TW, OFFX and OFFY.
!DisplayTSZ  - Text Size (Pixels)
!DisplayTW   - Text Width (Pixels)
!DisplayOFFX - Offset X (Pixels)
!DisplayOFFY - Offset Y (Pixels)
!-----
call OpenGR(255,DisplayBoarder,DisplaySc[curdisplay,4],DisplayORI)
GR.SCREEN DisplayMGX,DisplayMGY
If DisplaySc[curdisplay,4]=1
 gr.statusbar DisplayStatusHight
else
 DisplayStatusHight=0
endif

!Start multi-screen loop
for x = 1 to DisplayMaxScreens
curdisplay=x
!load screen variables from array here.
! DisplaySc[X,1]=DisplaySc[X,1]
! DisplayMY=DisplaySc[X,2]
! DisplayBackground=DisplaySc[X,3]
! DisplayStatus=DisplaySc[X,4]

!subtract hight of the status bar if present.
DisplayMGY=DisplayMGY-DisplayStatusHight
DisplaySc[curdisplay,5]=floor(DisplayMGY/DisplaySc[curdisplay,2])
DisplayMGY=DisplayMGY+DisplayStatusHight
DisplaySc[curdisplay,6]=Floor(DisplayMGX/DisplaySc[curdisplay,1])
!figure DisplayTSZ
DisplaySc[curdisplay,7]=DisplaySc[curdisplay,5]
gr.text.typeface 2
Do
 A14=0
 gr.text.size DisplaySc[curdisplay,7]
 GR.TEXT.WIDTH DisplayTW, "A"
 gr.text.height A08,A09,A10
 A09=floor(A09)
 A11=ceil(A10)
 A12=floor(A09)
 TextTR=A11-A12
 IF TextTR>DisplaySc[curdisplay,5]
   A14=A14+1
 Endif
 if DisplayTW>DisplaySc[curdisplay,6]
   A14=A14+1
 endif
 If A14>0
   DisplaySc[curdisplay,7]=DisplaySc[curdisplay,7]-1
 Endif
 until A14=0
 !fix the size of RowHigh and ColumnWide to the size of the text.
 if DisplaySc[curdisplay,5]>TextTR then DisplaySc[curdisplay,5]=TextTR
 if DisplaySc[curdisplay,6]>DisplayTW then DisplaySc[curdisplay,6]=DisplayTW
 !calculate offsets.
 A04=DisplayMGY-Displaystatushight
 A05=DisplaySc[curdisplay,5]*DisplaySc[curdisplay,2]
 A06=A04-A05
 A07=floor(A06/2)
 DisplaySc[curdisplay,8]=A07+Displaystatushight
 DisplaySc[curdisplay,9]=floor((DisplayMGX-(DisplaySc[curdisplay,6] * DisplaySc[curdisplay,1]))/2)
 !The RowHigh and ColumnWide are the size of the background graphic boxes. 
 !TR is the total hight of Text in pixels.
 !----
 !set background color, create a screen sized bitmap, and show it.
 gcol(255,Displayboarder,1)
 !calculate bmp size here.
 A48=DisplaySc[curdisplay,5] * DisplaySc[curdisplay,2]
 A49=DisplaySc[curdisplay,6] * DisplaySc[curdisplay,1]
 GR.BITMAP.CREATE DisplaySc[curdisplay,10], A49,A48
 !GR.BITMAP.CREATE DisplaySc[curdisplay,10],DisplayMGX,DisplayMGY
 !use offsets to center bmp.
 !must set color for bitmap.
 !Gr.bitmap.drawinto.start points the graphic tools at the bitmap "screenptr" 
 !must start and end before/after each "Printing/drawing".
 GR.BITMAP.DRAWINTO.START DisplaySc[curdisplay,10]
 !use ordinary graphic commands to draw stuff on bitmap.
 !GR.TEXT.DRAW UOP,1,17,"Loading..."
 GR.BITMAP.DRAWINTO.END
 Gosub Clear
 !this next command adds the bitmap to the display list.
 !only need to do this once.
 GR.BITMAP.DRAW DisplaySc[curdisplay,11],DisplaySc[curdisplay,10],1,1
 !Centers the bmpimage.
 gr.modify DisplaySc[curdisplay,10],"x",DisplaySc[curdisplay,9]
 gr.modify DisplaySc[curdisplay,10],"y",DisplaySc[curdisplay,8]
 !then gr.render to make it appear.
 GR.RENDER
 !end multi-screen loop.
next

!----
!get color pointers
! array to hold paint color pointers
DIM PC[20]
A15=0
A16=20
FOR A01 = 1 TO 20
 A15=A15+15
 CALL gcol(255,A01,1)
 a03$=CHR$(64+A01)
 GR.TEXT.DRAW a02,A15,A16,A03$
 GR.PAINT.GET pc[A01]
 GR.HIDE a02
NEXT
GR.RENDER
Return
!----------------------------
ChangeDisplay:
!Set NewScreen the screen number, call ChangeScreen.
 !put any other change screen stuff here.
 
 curdisplay=NewScreen
 Gr.Text.Size DisplaySc[curdisplay,7]
 TCX=DisplaySc[curdisplay,12]
 TCY=DisplaySc[curdisplay,13]

return
!----------------------------
Write:
!set TCX,TCY,TXT$,CF,CB,DisplayRender
!Start write operation
GR.BITMAP.DRAWINTO.START DisplaySc[curdisplay,10]

!correct tcx/tcy if off display
if tcx<1 then tcx=1
if tcx>displaysc[curdisplay,1] then tcx=displaysc[curdisplay,1]
if tcy<1 then tcy=1
if tcy>displaysc[curdisplay,2] then tcy=displaysc[curdisplay,2]

!prepare for writing
A20=tcx
A21=tcy
!length of txt$
A22=LEN(TXT$)
!size of the grab in charactors
a23=1	
!Txt$ position pointer
A24=1

!Do write operation
for a24 = 1 to a22
 !get part of txt$
 a23=1
 A26$=MID$(TXT$,A24,A23)

 !Draw Box
 A28=(((A20-1))*DisplaySc[curdisplay,6])
 A29=((A21-1)*DisplaySc[curdisplay,5])
 A30=A28+(DisplaySc[curdisplay,6] * A23)
 A31=A29+DisplaySc[curdisplay,5]
 CALL gcol(255,CB,1)
 GR.RECT A32,A28,A29,A30,A31
 
 !Draw Character
 A33=((A20-1)*DisplaySc[curdisplay,6])
 A34=((A21-1)*DisplaySc[curdisplay,5])+DisplaySc[curdisplay,7]
 CALL gcol(255,CF,1)
 GR.TEXT.DRAW A32,A33,A34,A26$
 
 !update cursor position 
 A20=A20+1
 IF A20>DisplaySc[curdisplay,1]
  A20=1
  A21=A21+1
  if A21>DisplaySc[curdisplay,2]
   A21=1
  endif
 ENDIF

 !End Loop
next

!stop write operation
GR.BITMAP.DRAWINTO.END
IF DisplayRender=1
 GR.RENDER
endif

!store cursor position
TCX=A20
TCY=A21
DisplaySc[curdisplay,12]=A20
DisplaySc[curdisplay,13]=A21
RETURN
!----------------------------
Write2:
!Warning! This subroutine does work...sortof.
!It is faster, but doesn't wrap text properly.
!----------
!set TCX,TCY,TXT$,CF,CB,DisplayRender
!Start write operation
GR.BITMAP.DRAWINTO.START DisplaySc[curdisplay,10]

!correct tcx/tcy if off display
if tcx<1 then tcx=1
if tcx>displaysc[curdisplay,1] then tcx=displaysc[curdisplay,1]
if tcy<1 then tcy=1
if tcy>displaysc[curdisplay,2] then tcy=displaysc[curdisplay,2]

!prepare for writing
A20=tcx
A21=tcy
!length of txt$
A22=LEN(TXT$)
!Txt$ position pointer
A24=1
!loop flag
A25=0

!Do write operation
Do
 !get part of txt$
   a23=A22-a24+1
 if a23>(displaysc[curdisplay,1]-a20)
   a23=displaysc[curdisplay,1]-a20
 endif
 A26$=MID$(TXT$,A24,A23)

 !Draw Box
 A28=(((A20-1))*DisplaySc[curdisplay,6])
 A29=((A21-1)*DisplaySc[curdisplay,5])
 A30=A28+(DisplaySc[curdisplay,6] * A23)
 A31=A29+DisplaySc[curdisplay,5]
 CALL gcol(255,CB,1)
 GR.RECT A32,A28,A29,A30,A31
 
 !Draw Character
 A33=((A20-1)*DisplaySc[curdisplay,6])
 A34=((A21-1)*DisplaySc[curdisplay,5])+DisplaySc[curdisplay,7]
 CALL gcol(255,CF,1)
 GR.TEXT.DRAW A32,A33,A34,A26$

!update cursor position 
 A20=A20+A23-1
 IF A20>DisplaySc[curdisplay,1]
  A20=1
  A21=A21+1
  if A21>DisplaySc[curdisplay,2]
   A21=1
  endif
 ENDIF

 !End Loop
 a24=a24+a23
 IF A24<A22
  A25=1
 else
  A25=0
 endif
until A25=0

!stop write operation
GR.BITMAP.DRAWINTO.END
IF DisplayRender=1
 GR.RENDER
endif

!store cursor position
TCX=A20
TCY=A21
DisplaySc[curdisplay,12]=A20
DisplaySc[curdisplay,13]=A21
RETURN

!----------------------------
Clear:
!Clears the screen.(Paints entire screen the background color.)
!Uses ClearABox to do the work.
TCX=1
TCY=1
Width=DisplaySc[curdisplay,1]
Hight=DisplaySc[curdisplay,2]
!----------------------------
ClearABox:
!Clears a box on the screen (in characters).
!(Paints a box in the background color.)
! Specs: (In characters)
!  TCX,TCY - coords of the upper left corner of the box
!  Width - width of box.
!  Hight - Hight of box.
A40=TCY
A41=TCY+Hight-1
A42=TCX
A43=TCX+Width-1
!Draw Box
GR.BITMAP.DRAWINTO.START DisplaySc[curdisplay,10]
!A44=(((A42-1))*DisplaySc[curdisplay,6])+DisplaySc[curdisplay,9]
!A45=((A40-1)*DisplaySc[curdisplay,5])+DisplaySc[curdisplay,8]
!A46=((A43)*DisplaySc[curdisplay,6])+DisplaySc[curdisplay,9]
!A47=((A41)*DisplaySc[curdisplay,5])+DisplaySc[curdisplay,8]

A44=(((A42-1))*DisplaySc[curdisplay,6])
A45=((A40-1)*DisplaySc[curdisplay,5])
A46=((A43)*DisplaySc[curdisplay,6])
A47=((A41)*DisplaySc[curdisplay,5])

CALL gcol(255,DisplaySc[curdisplay,3],1)
GR.RECT A48,A44,A45,A46,A47
GR.BITMAP.DRAWINTO.END
Return
!----------------------------
gettouch:
 !Start of the touch loop.
 !Detects single touches only.
 !Returns TX1,TY1,TX2,TY2 - coordinates of touch and release.
 !loop until the screen is touched
 touch = 0
 DO
  GR.TOUCH touch,tx1,ty1
 UNTIL touch = 1
 !loop until screen is not touched
 DO
  GR.TOUCH touch,tx2,ty2
 UNTIL touch = 0
 RETURN

!----------------------------
!Subroutines from here on.
!----------------------------