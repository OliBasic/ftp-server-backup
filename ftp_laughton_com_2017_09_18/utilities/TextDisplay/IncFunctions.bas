!IncludeFunctions.bas
!This is not a complete file.  It is intended to load functions.
!Just put :  "Include Functions.bas" at the start of your program.
!
!So far:
!  GCOL(Alpha,Color,Fill)
!  NumberClip$(number,number$)
!  OpenGR(Alpha,Color,Status,Orientation)
!----------------------------
FN.DEF gcol(Alpha,color,fill)
 C1$="000000 ff0000 00ff00 0000ff ffff00 00ffff ff00ff 999999 ffffff 000099 "
 C2$="009900 009999 990000 990099 999900 993333 CCCCCC FFCC99 FF9900 FFCC00 "
 ColorString$=c1$+c2$
 cp=color*7-6
 c$=MID$(colorstring$,cp,7)
 GR.COLOR Alpha, Hex(Mid$(c$,1,2)), Hex(Mid$(C$,3,2)), Hex(Mid$(C$,5,2)), Fill
FN.END
 !Written by:
 !  Donald Hosford (SirDonzer@Yahoo.com)
 !
 !The credits:
 !  Roy -- His Excellent Idea to simplify the colors! Why didn't I think of it!
 !  Mougino -- Nice idea to reduce the color codes to a short function.
 !  Me -- I added 11 more "web safe" colors.  Added all the colors in a string.
 !
 !Input:
 !  Alpha -- how solid the colors.  0 - transparent, 255 - solid.
 !  Color -- the desired color (0 to 19).
 !  Fill -- 0 - outline, 1 - filled it.
 !
 !Source:
 !  This is from a couple of ideas by Roy and Mougino on the rfo.basic.forum.com.
 !  Hex codes for twenty common colors, spaces are just separators.
 !
 !The Colors: (his first)
 !  Black, Red, Lime, Blue, Yellow, Cyan, Magenta, Grey, White
 !  Navy, Green, Teal, Maroon, Purple, Olive, Brown, Silver
 !  Tan, Orange, Gold
!!
Twenty different common colors.
web safe color level numbers: (hex) 00 33 66 99 CC FF

000000 - Black
000099 - navy
0000ff - blue
009900 - green
009999 - teal
00ff00 - lime
00ffff - cyan
990000 - maroon
990090 - purple
999900 - olive
999999 - grey
993333 - Brown
CCCCCC - silver
FFCC99 - Tan
ff0000 - red
ff00ff - magenta
ff9900 - orange
ffCC00 - Gold
ffff00 - yellow
ffffff - white
!!
!----------------------------
FN.DEF NumberClip$(Number,Number$)
!take the number, clip off the decimals.
Number$=str$(number)
L=len(number$)
P=IS_IN(".",number$)
Number$=Left$(number$,P-1)
If Number<10 then number$=" "+number$
FN.RTN Number$
FN.END
!----------------------------
!OpenGR function replaces GR.open,Pause1000, and Gr.orientation.
FN.DEF OpenGR(Alpha,color,Status,Orientation)
 C1$="000000 ff0000 00ff00 0000ff ffff00 00ffff ff00ff 999999 ffffff 000099 "
 C2$="009900 009999 990000 990099 999900 993333 CCCCCC FFCC99 FF9900 FFCC00 "
 ColorString$=c1$+c2$
 cp=color*7-6
 c$=MID$(colorstring$,cp,7)
 GR.Open Alpha, Hex(Mid$(c$,1,2)), Hex(Mid$(C$,3,2)), Hex(Mid$(C$,5,2)), Status, Orientation
 pause 1000
FN.END
 !Written by:
 !  Donald Hosford (SirDonzer@Yahoo.com)
 !
 !The credits:
 !  This is based on My version of Roy's GCOL function.
 !Input:
 !  Alpha -- how solid the colors.  0 - transparent, 255 - solid.
 !  Color -- the desired color (0 to 19).
 !  Status -- Show/hide the status bar.  0 - outline, 1 - filled it.
 !  Orientation -- What screen orientation.  0 - Lanscape, 1 - Portrait.
 !
 !Source:
 !  This is based on my version of Roy's GCOL function.
 !  See the GCOL notes.
 !
 !The Colors: (his first)
 !  Black, Red, Lime, Blue, Yellow, Cyan, Magenta, Grey, White
 !  Navy, Green, Teal, Maroon, Purple, Olive, Brown, Silver
 !  Tan, Orange, Gold
!----------------------------
