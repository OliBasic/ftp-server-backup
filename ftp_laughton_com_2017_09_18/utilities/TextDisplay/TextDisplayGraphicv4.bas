!TextDisplayGraphic.bas
DIM SRF[60], GON[60]	%setup ScreenRowFlag[], GraphicalObjectNumber[]
!!
About:
Text Display Graphic demo (version 4)
by Donald Hosford (donzerme@yahoo.com)

This is a demo of an addressable text display on the
 graphic screen.

  The amount of text on the screen is determined by
 the size of your device's screen, and the text size
 set in the setup subroutine.

How to use:
 1) Set TXT$ to your text.
   Can be any text.  Disruptive characters in TXT$ get
   replaced automaticly.  Doesn't catch all of them...
 2) Set the coordinates.
   Using TCX/TCY:
    Set SD$ to "" (null).  Then set TCX/TCY to the
    desired coordinates.
   Using SD$ -- format:
   chars 1 and 2 -- X axis (columns)
   chars 3 and 4 -- Y axis (rows)
   chars 5+ -- flags:
     c -- center TXT$ on row.  Must be shorter than MX.
 3) Gosub WriteToScreen.

What it can do:
--> Display text in an addressable fashion.
--> Can center text on a row.
--> Automatic calculation of rows and columns.
     Just set TSZ, and Screen Orientation in the setup subroutine.
--> Screen orientation can be locked.
--> Colors can be set.
--> Text and graphics can be mixed.
	 
problems:
--> There is no underlining, no italics, or form characters.
!!

GOSUB ScreenSetup

!Main  program
!--------------------
!Shows various printing techniques.
!This line shows the centering function. The 'c'.
SD$="0101c"
TXT$="Text Screen Demo"
GOSUB WriteToScreen

!Normal printing function.
SD$="0303"
TXT$="See how the text can be placed anywhere."
GOSUB WriteToScreen
SD$=""
TXT$="  This is the 'semicolon' function.  Works like the print command.  Lets text be 'printed' after whatever was 'printed' last.  This is always assumed.  Also lets you display large blocks of text."
GOSUB writetoscreen
SD$="0307"
TXT$="New paragraphs are made by setting new coordinates, and 'printing' another block of text.  Numbers can also be handled, but must be made into strings first."
GOSUB writetoscreen

!This section shows how to use the text cursor variables.
SD$="0310"
TXT$="Multiplication Table"
GOSUB writetoscreen
SD$=""
FOR A = 1 TO 4
 FOR B = 1 TO 4
  c=b * a
  T$=FORMAT$("###",c)
  Txt$=RIGHT$(t$,LEN(t$)-2)
  TCX = A * 4-2
  TCY = B * 2 + 10
  GOSUB WriteToScreen
 NEXT B
NEXT A

!This shows that flags can be set alone.
TCX=15
TCY=MY
SD$="c"
TXT$="Touch screen to Continue"
GOSUB WriteToScreen

GOSUB ScreenPause

!CLS
END

ScreenSetup:
!!
--------------------
Must be done at the start of the program.
I happen to like textsize 26
Landscape is 50 x 18.
Portrait is 30 x 30.
!!
!Set text size.
TSZ=17			%Text Size.  Set this to desired Text Size.

!start graphics screen
GR.OPEN 255,0,255,0		%open graphic screen with a green background
PAUSE 1000
GR.CLS
GR.ORIENTATION 0 	%landscape orientation
GR.COLOR 255,0,0,0,0	%black foreground color
GR.SCREEN MGX,MGY		%get screen size
GR.TEXT.SIZE TSZ		%set text size
GR.TEXT.TYPEFACE 2		%set mono spaced font
T$="A"
GR.TEXT.WIDTH tw,T$		%Gets the width of single character.
MX=FLOOR(mgx/tw)		%figure MX
MY=FLOOR(mgy/tsz) 		%figure MY
MSP=MX * MY				%Maximum screen position. Also number of characters on "screen".
!set offsets
OFFX=MGX-(MX*TW)
IF OFFX>0
 OFFX=FLOOR(OFFX/2)
ELSE
 OFFX=0
ENDIF
OFFY=MGY-(MY*TSZ)
IF OFFY>0
 OFFY=FLOOR(OFFY/2)
ELSE
 OFFY=0
ENDIF

!Draw text objects, get the object numbers
LPZ=TSZ+OFFY
FOR LPX = 1 TO MY
 GR.TEXT.DRAW GON[LPX],1+OFFX,LPZ,CHR$(32)
 LPZ=LPZ+TSZ
NEXT LPX

!Build Blank Screen.
Screen$=""
FOR X=1 TO MSP
 Screen$=Screen$+CHR$(32)
NEXT X
Background$=Screen$

!Setup Text Cursor.
TCX=1		%Cursor X Position
TCY=1		%Cursor Y Position
TCP=1		%Cursor Position in Screen$
RETURN


ClearScreen:
!------------------
FOR LPX=1 TO MY
 SRF[LPX]=0
 GR.MODIFY GON[LPX],"text",CHR$(32)
NEXT LPX
Screen$=Background$
RETURN

WriteToScreen:
!------------------
!Get Screen coordinates and flag Data
GOSUB readflags

L=LEN(TXT$)
R1=CY-1
!Screen center flag.
IF TCN=1
 C1=MX-L
 IF c1>1
  C2=C1/2
  CX=FLOOR(C2)
 ELSE
  CX=1
 ENDIF
ENDIF

!Write Text to Screen$.
SP=(CY-1)*MX+CX
L=LEN(TXT$)
IF SP>1
 N=SP-1
 S1$=LEFT$(Screen$,(SP-1))
ELSE
 S1$=""
ENDIF
S2$=TXT$
IF SP<MSP
 N=MSP-(SP+L-1)
 S3$=RIGHT$(Screen$,N)
ELSE
 S3$=""
ENDIF
Screen$=S1$+S2$+S3$

!Move Text Cursor to end of Text.
SP=SP+L
CX=CX+L
do
 CX=CX-MX
 CY=CY+1
until cx<=mx
TCX=CX
TCY=CY
R2=CY

!clear variables and flags.
TXT$=""
SD$=""
TCN=0
if R1<1
 R1=1
ENDIF
if R1=R2
 SRF[R1]=1
else
 FOR x=r1 TO r2+1
  srf[x]=1
 NEXT x
endif


!Display Screen
!Draw Only affected screen rows.
LPZ=1
TT$=""
FOR LPY=1 TO MY
 IF SRF[LPY]=1
  TT$=MID$(Screen$,LPZ,MX)
  GR.MODIFY GON[LPY],"text",TT$
  SRF[LPY]=0
 ENDIF
 LPZ=LPZ+MX
NEXT Y
GR.RENDER
RETURN

ReadFlags:
!------------------
!Read SD$ for coordinates, and flags.
S$=LEFT$(SD$,4)
IF LEN(S$)=4
 CX=VAL(LEFT$(S$,2))
 CY=VAL(RIGHT$(S$,2))
ELSE
 CX=TCX
 CY=TCY
ENDIF
IF LEN(SD$)>4
 S$=RIGHT$(SD$,LEN(SD$)-4)
 FOR S=1 TO LEN(S$)
  T$=MID$(S$,S,1)
  IF T$="c"
   TCN=1
  ELSE
   TCN=0
  ENDIF
  !add other letter codes here.
 NEXT S
ENDIF
!check for flag only SD$
IF LEN(SD$)<4
 FOR S=1 TO LEN(S$)
  T$=MID$(SD$,S,1)
  IF T$="c"
   TCN=1
  ELSE
   TCN=0
  ENDIF
  !add other letter codes here.
 NEXT S
ENDIF
RETURN

Screenpause:
!--------------------------
!pauses until touch screen is touched.  Works only on graphics screen.
touch=0
DO
 GR.TOUCH touch,TX,TY
UNTIL Touch
RETURN
