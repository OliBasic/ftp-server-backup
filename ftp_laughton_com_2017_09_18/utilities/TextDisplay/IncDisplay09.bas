!Include IncDisplay09.bas
!----------------------------
!Purpose: Do an include file with just the new text display in it.
!!
!Start block Just copy everything between the double lines -- but not the double lines...
!----------------------------
!----------------------------
!Program Name: ????
!Purpose: ????
!----------------------------
!load functions
Include IncFunctions.bas
!----------------------------
!Program Setup
!----------------------------
!setup graphics
! An 80 x 28 Text Display just fills an 800 x 480 screen.
! Set these to suit your project.
MX=80
MY=28
ORI=0
ColorBackground=1
Status=0
DisplayRender=1
!----------------------------
!Call setup routines here.
Gosub DisplaySetup
!----------------------------
!MAIN Program
!----------------------------


!(Your Program Goes Here)



End
!----------------------------
!Any program data must appear AFTER this include statement.
Include IncDisplay09.bas
!----------------------------
!Subroutines from here on.
!----------------------------

!----------------------------
!----------------------------
!!
DisplaySetup:
!setup graphics
!New setup subroutine auto figures settings.
!Set MX, MY, and ORI.  Gosub DisplaySetup
!MY   - Number of Displayed Rows.
!MX   - Number of Displayed Columns.
!ORI  - Screen orientation. Portrait = 1, Landscape = 0
!ColorBackground - Set ColorBackground to color number from GCOL. 
!Status - Show/Hide status bar. 0 = Hide, 1 = Show.
!DisplayRender - Set 0 to NOT gr.render, 1 to gr.render every time.
!-----
!Setup figures TSZ, TW, OFFX and OFFY.
!TSZ  - Text Size (Pixels)
!TW   - Text Width (Pixels)
!OFFX - Offset X (Pixels)
!OFFY - Offset Y (Pixels)
!-----
call OpenGR(255,ColorBackground,Status,ORI)
GR.SCREEN MGX,MGY
If Status=1
 gr.statusbar StatusHight
else
 StatusHight=0
endif 
!subtract hight of the status bar if present.
MGY=MGY-StatusHight
RowHigh=floor(MGY/MY)
MGY=MGY+StatusHight
ColumnWide=Floor(MGX/MX)
!figure TSZ
TSZ=RowHigh
gr.text.typeface 2
 !    HT=is the TSZ number passed back.
 !    UP=position of top of text box(pixels)  (In relation to draw line.  THis is negative.) 
 !    DN=Position of bottom of text box (in relation to draw line.  This is positive.) 
Do
 FLAG=0
 gr.text.size TSZ
 GR.TEXT.WIDTH TW, "A"
 gr.text.height HT,UP,DN
 up=floor(up)
 dn=ceil(dn)
 TB=ceil(DN)
 TT=floor(UP)
 TR=TB-TT
 IF TR>RowHigh
  Flag=Flag+1
 Endif
if TW>ColumnWide
 Flag=Flag+1
endif
If Flag>0
 TSZ=TSZ-1
Endif
until flag=0
!fix the size of RowHigh and ColumnWide to the size of the text.
if Rowhigh>TR then Rowhigh=TR
if Columnwide>TW then Columnwide=TW
!calculate offsets.
!OFFY=floor((MGY-(RowHigh * MY))/2)+StatusHight
o0=mgy-statushight
o1=rowhigh*my
o2=o0-o1
o3=floor(o2/2)
offy=o3+statushight
OFFX=floor((MGX-(ColumnWide * MX))/2)
!The RowHigh and ColumnWide are the size of the background graphic boxes. 
!TR is the total hight of Text in pixels.
!----
!set background color, create a screen sized bitmap, and show it.
gcol(255,colorbackground,1)
GR.BITMAP.CREATE ScreenPTR,MGX,MGY
!must set color for bitmap.
!Gr.bitmap.drawinto.start points the graphic tools at the bitmap "screenptr" 
!must start and end before/after each "Printing".
GR.BITMAP.DRAWINTO.START screenptr
!use ordinary graphic commands to draw stuff on bitmap.
GR.TEXT.DRAW UOP,1,17,"Loading..."
GR.BITMAP.DRAWINTO.END
!this next command adds the bitmap to the display list.
!only need to do this once.
GR.BITMAP.DRAW gon,screenptr,1,1
!then gr.render to make it appear.
GR.RENDER
!----
!get color pointers
! array to hold paint color pointers
DIM PC[20]
xpixel=0
ypixel=20
FOR x = 1 TO 20
 xpixel=xpixel+15
 CALL gcol(255,x,1)
 t$=CHR$(64+x)
 GR.TEXT.DRAW a,xpixel,ypixel,t$
 GR.PAINT.GET pc[x]
 GR.HIDE a
NEXT
GR.RENDER
Return
!----------------------------
Write:
!set TCX,TCY,TXT$,CF,CB,DisplayRender
!Setup write operation
GR.BITMAP.DRAWINTO.START screenptr
tcx1=tcx
tcy1=tcy
L=LEN(TXT$)
If L>MX
  G=MX
 else
  if (L+TCX1-1)>MX
   G=(L+TCX1-1)-MX
  else
   G=L
  endif
endif
P=1
Flag=0
!calculate TCX,TCY in pixels
!text char position in pixels
!both corners of the box, in pixels.

!Do write operation
Do
 !start Loop
 T$=MID$(TXT$,P,G)
 L2=len(T$)

 !Draw Box
 PX1=(((TCX1-1))*Columnwide)+OFFX
 PY1=((TCY1-1)*Rowhigh)+OFFY
 PX2=PX1+(Columnwide * L2)
 PY2=PY1+Rowhigh
 CALL gcol(255,CB,1)
 GR.RECT A,PX1,PY1,PX2,PY2
 
 !Draw Character
 PX=((TCX1-1)*Columnwide)+OFFX
 PY=((TCY1-1)*Rowhigh)+TSZ+OFFY
 CALL gcol(255,CF,1)
 GR.TEXT.DRAW A,PX,PY,T$
 
!update cursor position 
 TCX1=TCX1+L2
 IF tcx1>mx
  tcx1=1
  tcy1=tcy1+1
  if tcy1>MY
   tcy1=1
  endif
 ENDIF

 !End Loop
 If G<MX then G=MX
 IF P<L
  Flag=1
 else
  Flag=0
 endif
 P=P+L2
until Flag=0

!stop write operation
GR.BITMAP.DRAWINTO.END
IF DisplayRender=1
 GR.RENDER
endif
TCX=TCX1
TCY=TCY1
RETURN

!----------------------------
Clear:
!Clears the screen.(Paints entire screen the background color.)
!Uses ClearABox to do the work.
TCX=1
TCY=1
Width=MX
Hight=MY
!----------------------------
ClearABox:
!Clears a box on the screen.
!(Paints a box in the background color.)
! Specs: (In characters)
!  TCX,TCY - coords of the upper left corner of the box
!  Width - width of box.
!  Hight - Hight of box.
Y1=TCY
Y2=TCY+Hight-1
X1=TCX
X2=TCX+Width-1
boxcolor=colorbackground
!Draw Box
GR.BITMAP.DRAWINTO.START screenptr
PX1=(((X1-1))*Columnwide)+OFFX
PY1=((Y1-1)*Rowhigh)+OFFY
PX2=((X2)*Columnwide)+offx
PY2=((Y2)*Rowhigh)+offy
CALL gcol(255,ColorBackground,1)
GR.RECT A,PX1,PY1,PX2,PY2
GR.BITMAP.DRAWINTO.END
Return
!----------------------------
gettouch:
!Start of the touch loop.
!Returns TX1,TY1,TX2,TY2 - coordinates of touch and release.
!loop until the screen is touched
touch = 0
DO
 GR.TOUCH touch,tx1,ty1
UNTIL touch = 1
!loop until screen is not touched
DO
 GR.TOUCH touch,tx2,ty2
UNTIL touch = 0
RETURN
!----------------------------
