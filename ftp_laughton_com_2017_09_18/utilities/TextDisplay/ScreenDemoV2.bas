!Display Console Screen Demo
DIM SRF[60], GON[60]	%setup ScreenRowFlag[], and GraphicalObjectNumber[]
 
GOSUB Setupscreen

!main program
!--------------------------
!Screen 1: Test text to print
GOSUB PTSClearScreen 
TXT$="(ScreenDemo.bas -- Screen 1)"
SD$="01021"
GOSUB PTS
TXT$="Text on the graphic screen."
SD$="01031"
GOSUB PTS
TXT$="Some of these coordinates are set with SD$."
SD$="0304"
GOSUB PTS
TXT$="  On this line, SD$ is set to: 0304.  That reads: third column, fourth row."
SD$=""
GOSUB PTS
TXT$="  The text on this screen is textsize"+format$("##",TSZ)+" -- that's"+format$("##",MX)+" columns by"+format$("##",MY)+" rows.  Maximum size is automatically deturmined.  Just set TSZ in the setup subroutine.  Maximum size is 100 columns by 100 rows."
SD$=""
GOSUB PTS
TXT$="  Every line 'printed' assumes a ';' is being used.  Same as the print command.  Just set SD$ to "+Chr$(34)+chr$(34)+" (null)."
SD$=""
GOSUB PTS
TXT$="  To clear the screen, gosub PTSClearScreen.  The other graphic functions are available, so text and graphics can be mixed.  Any colors may be used."
GOSUB PTS
TXT$="  The Print to Screen$ can handle numbers, so long as they are converted to strings first."
GOSUB PTS
TXT$="Touch screen to continue."
SD$="01011"
GOSUB PTS
!GOSUB drawscreen
GOSUB screenwait

!--------------------------
!Screen 2: More test text to print
GOSUB PTSClearScreen
TXT$="(ScreenDemo.bas -- Screen 2)"
SD$="01021"
GOSUB PTS
TXT$="This was printed by adjusting the text cursor."
SD$="03031"
GOSUB PTS
TXT$="  Simply adjust TCX, TCY, and TCN.  Then gosub PTS."
SD$=""
GOSUB PTS
for x = 1 to 4
 for y = 1 to 4
  z=x*y
  txt$=format$("##",z)
  tcx=x*4
  TCY=y*2+7
  gosub pts
 next y
next x

TXT$="Touch screen to continue."
SD$="01011"
GOSUB PTS
GOSUB screenwait

!--------------------------
END

Screenwait:
!--------------------------
!pauses until touch screen is touched.  Works only on graphics screen.
touch=0
DO
 GR.TOUCH touch,TX,TY
UNTIL Touch
RETURN

SetupScreen:
!!
--------------------------
Must be done at the start of the program.
I happen to like textsize 26
Landscape is 50 x 18.
Portrait is 30 x 30.
!!
!setup screen variables. A list of all Variables used:
!These effect the screen orientation, and the text size.
TSZ=17   %sets text size for Gr.text.size
!-------------------------------
!These send the stuff to be printed to the Print To String subroutine.
TXT$=""					%Text to be "printed".
SD$=""					%Screen coordinates and flag data.
!The TextCursor X, Y, Centering variables.
TCX=0					%Text Cursor X Coord.
TCY=0					%Text Cursor Y Coord.
TCN=0					%Text Centering flag.
!-------------------------------
!These are internal variables.  Please don't change these.
MX=50					%Maximum X value. Maximum columns of "screen".
MY=18					%Maximum Y value. Maximum rows of "screen".
MSP=900
SP=0					%Screen Position. Position in screen$.
SX=0					%Screen column. "X"
SY=0					%Screen Row. "Y"
MGX=0					%Maximum Graphics Screen X size
MGY=0					%Maximum Graphics Screen Y size
SCREEN$=""				%"Screen" string.  Stores the printed screen.
LPL=0					%Length of TXT$
LPX=0					%Loop X
LPY=0					%Loop Y
LPZ=0					%Loop Z
!U1$, U2$, U3$			%miscellaneous strings.
!-------------------------------
!Build screen$.
!On the output console, the blank character is 160.
!(Same as 32, but displays properly.)
BGCHAR$=CHR$(32)		%Background character.
FOR LPX = 1 TO MSP
 Screen$=SCREEN$+BGCHAR$
NEXT LPX
BlankScreen$=Screen$	%Empty screen.
!start graphics screen
GR.OPEN 255,0,255,0		%open graphic screen with a green background
pause 1000
gr.cls
GR.ORIENTATION 0			%landscape orientation
GR.COLOR 255,0,0,0,0	%black foreground color
GR.SCREEN MGX,MGY		%get screen size
GR.TEXT.SIZE TSZ		%set text size
GR.TEXT.TYPEFACE 2		%set mono spaced font
 
T$="A"
GR.TEXT.WIDTH tw,T$
MX=floor(mgx/tw)		%figure MX
MY=floor(mgy/tsz) 		%figure MY
MSP=MX * MY				%Maximum screen position. Also number of characters on "screen".
!Draw text objects, get the object numbers
LPZ=TSZ
for LPX = 1 to MY
 GR.TEXT.DRAW GON[LPX],1,LPZ,chr$(32)
 LPZ=LPZ+TSZ
next LPX
GR.render
RETURN

PTSClearScreen:
!--------------------------
!Resets all variables to blank screen.
Screen$=""
FOR LPX=1 TO MY
 SRF[LPX]=0
 GR.MODIFY GON[LPX],"text",chr$(160)
NEXT LPX
Screen$=BlankScreen$ 
RETURN

PTS:
!!
Print to screen$
--------------------------
Print To Screen$ Notes
Set TXT$ to the object to be printed.
Set SD$ to input coordinates:
The first four characters are the coordinates.
Any extra characters after that are flags.
 Characters 1 and 2 are the Column. (X axis) Also TCX.
 Characters 3 and 4 are the Row. (Y axis)  Also TCY.
 Character 5 is the text centering flag.  Also TCN.
Upper left corner is 1,1. (X axis, Y axis)
Center Text: Set to 1 for centering.  set to 0 to turn off.
Example: 0418 = sets the text cursor to 4,18.
Example: 01051 = sets the text cursor to 1,5 and turns on text centering.
TCX, TCY, TCN may be set instead. Just set SD$ = "".
(ie: TCX=3 TCY=10 TCN=1 is the same as: SD$="03101")
The "text cursor" will always be positioned at the end of the last TXT$ printed.
This simulates the ";" character in print commands.
!!
!Get Screen coordinates and flag Data
IF LEN(sd$)>0
 SX=VAL(LEFT$(SD$,2))
 SY=VAL(MID$(SD$,3,2))
ENDIF
IF LEN(SD$)=0
 !Use current text cursor coordinates
 SX=TCX
 SY=TCY
ENDIF
IF LEN(SD$)=4
 TCN=0
ENDIF
IF LEN(SD$)>4
 !then read flags
 TCN=VAL(MID$(SD$,5,1))		%text centering flag
ENDIF
!Text Centering function
LPL=LEN(TXT$)
IF TCN=1
 SX=FLOOR(MX-LPL)/2+1
ENDIF
!check screen position.
!clips coordinates to
!the screen edges.
IF SY<1
 SY=1
ELSE
 IF SY>MY
  SY=MY
 ELSE
  IF SX<1
   SX=1
  ELSE
   IF SX>MX
    SX=MX
   ENDIF
  ENDIF
 ENDIF
ENDIF
SP=((SY-1)*MX)+SX
!"print" to screen string
!Inserts text into string
U1=SP+LPL-1
IF U1>MSP
 TXT$=LEFT$(TXT$,MSP-SP)
 LPL=LEN(TXT$)
ENDIF
IF SP>1
 U1$=LEFT$(Screen$,SP-1)
ELSE
 U1$=""
ENDIF
U2$=TXT$
U2=SP+LPL-1
U3=MSP-U2
IF U2<MSP
 U3$=RIGHT$(Screen$,U3)
ELSE
 U3$=""
ENDIF
Screen$=U1$+U2$+U3$
TCN=0
!Move the text cursor to the end of the printed text.
SRF[SY]=1
SX=SX+LPL
LPX=0
DO
 IF SX>MX
  SY=SY+1
  SX=SX-MX
  SRF[SY]=1
 ELSE
  LPX=1
 ENDIF
UNTIL LPX=1
tcx=sx
tcy=sy


drawscreen:
!draw just the effected row
LPX=1
LPY=1
DO
 IF srf[LPY]=1
  TT$=MID$(Screen$,LPX,MX)
  GR.MODIFY GON[LPY],"text",TT$
  srf[LPY]=0
 ENDIF
 !next x
 LPY=LPY+1
 LPX=LPX+MX
UNTIL LPY>my
GR.RENDER
txt$=""
RETURN
