!Program Name: TDColorTest1.bas
!Purpose: Just a simple demo to show off my new text display with colors. It is based on Roy's
!  PrintTAB.bas on the RFO Basic Forum.  With my own ideas thrown in.  I love his program!
!  But I need a more controlled version.  I would like to use this with my onscreen keyboard.
!----------------------------
!load functions
Include IncFunctions.bas
!----------------------------
!Program Setup
!----------------------------
!setup graphics
! An 80 x 28 Text Display just fills an 800 x 480 screen.
! Set these to suit your project.
MY=23
MX=80
ORI=0
ColorBackground=4
Status=1
displaYrender=1
Gosub DisplaySetup
!----------------------------
!MAIN Program
!----------------------------
!Put up boarders
TCX=1
TCY=1
CF=9
CB=4
TXT$="+------------------------------------------------------------------------------+!"
!GOSUB write

For y = 2 to 27

 TCX=80
 TCY=Y
 CF=9
 CB=4
 TXT$="!!"
! GOSUB write

next

TCX=1
TCY=28
CF=9
CB=4
TXT$="+------------------------------------------------------------------------------+"
!GOSUB Write

TCX=65
TCY=1
CF=9
CB=4
TXT$="+Color Test!+"
GOSUB write

for x=1 to 20
 tcx=x*3+0
 tcy=1
 cf=9
 cb=4
 numberclip$(x,&txt$)
 gosub write
next
for y=1 to 20
 tcx=1
 tcy=y+1
 cf=9
 cb=4
 numberclip$(y,&txt$)
 gosub write
 for x=1 to 20
  tcx=x*3+0
  tcy=y+1
  cf=x
  cb=y
  txt$=chr$(9553)+chr$(9553)
  gosub write
 next
next
gr.render

tcx=65
tcy=3
cf=9
cb=4
txt$="touch screen"
gosub write

!touch screen to end
GOSUB gettouch

tcx=64
tcy=3
cf=9
cb=4
txt$="Screen clear"
gosub write

gosub Clear

tcx=64
tcy=3
cf=9
cb=1
txt$="touch to end"
gosub write

gosub gettouch

END
!Any program data must appear AFTER this include statement.
Include IncDisplay09.bas
!----------------------------
!Subroutines from here on.
!----------------------------
