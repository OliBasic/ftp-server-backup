array.load vib[],0,50
countup=1
finished=0
muted=0
hrs=0
minst=0
minsu=0
secst=0
secsu=0
oldhrs=0
oldminst=0
oldminsu=0
oldsecst=0
oldsecsu=0
adjusthrs=0
tim$=format$("%%:%%:%%",(hrs*10000)+(minst*1000)+(minsu*100)+(secst*10)+secsu)
textsize=150
gr.open 255,0,0,0
gr.orientation 0
gr.color 255,255,255,0,1
gr.screen w,h
gr.text.size textsize
gr.text.draw textnum,-1,-5,tim$
gr.text.width twidth,tim$
gr.screen w,h
textx=(w-twidth)/2
texty=(h-(textsize/2))/2
gr.text.draw textnum,textx,texty,tim$
gr.text.size 40
gr.text.draw u,280,418,"Up"
gr.text.draw d,490,418,"Down"
gr.text.draw m,700,418,"Mute"
gr.color 255,220,220,220,1
gr.rect startbtn,220,295,395,355
gr.rect stopbtn,430,295,605,355
gr.rect resetbtn,635,295,810,355

gr.color 255,0,0,255,1
gr.text.draw st,260,340,"Start"
gr.text.draw st,470,340,"Stop"
gr.text.draw st,665,340,"Reset"
gr.color 255,220,220,220,1
gr.circle c,247,400,20
gr.circle c,457,400,20
gr.rect mute,635,380,675,420
gr.color 255,0,0,255,1
gr.circle ctup,247,400,13
gr.circle ctdn,457,400,13
gr.hide ctdn
gr.set.stroke 5
gr.line muteticka,640,400,650,413
gr.line mutetickb,650,413,670,385
gr.hide muteticka
gr.hide mutetickb

gr.color 255,160,160,160,1
gr.circle ctupgry,247,400,13
gr.circle ctdngry,457,400,13
gr.hide ctupgry
gr.hide ctdngry

gr.color 255,240,240,240,1
gr.line starttop,220,297,395,297
gr.line startlft,220,297,220,355
gr.line stoptop,430,297,605,297
gr.line stoplft, 430,297,430,355
gr.line resettop,635,297,810,297
gr.line resetlft,635,297,635,355

gr.color 255,128,128,128,1
gr.line startbot,220,355,395,355
gr.line startrt,395,297,395,355
gr.line stopbot,430,355,605,355
gr.line stoprt,605,297,605,355
gr.line resetbot,635,355,810,355
gr.line resetrt,810,297,810,355

gr.set.stroke 1
if background()=0 then gr.render
WakeLock 1

loop:
if finished=1 then
 for b=0 to 1
  for a=1 to 4
   gr.bounded.touch stoppress,435,300,610,350
   if stoppress=1 then
    finished=0
    vibrate vib[],-1
    if countup=1 then
     gr.hide ctupgry
    else
     gr.hide ctdngry
    endif
   endif
   gr.color 255,255,b*255,0,1
   gr.text.size 150
   gr.paint.get tn
   gr.modify textnum,"paint",tn
   if background()=0 then gr.render
   tone 2000*(1-b)*(1-muted),105
   tone 0,105
  next a
  vibrate vib[],-1
  tone 0,105
 next b
 goto loop
endif

TIME Year$,Month$,Day$,Hour$,Minute$,Second$
secs$=Second$
while secs$=Second$
 TIME Year$,Month$,Day$,Hour$,Minute$,Second$

 gr.bounded.touch startpress,225,300,400,350
 if startpress=1 & started= 0 then
  vibrate vib[],-1
  gr.text.size 150
  gr.color 255,255,255,0,1
  gr.paint.get tn
  gr.modify textnum,"paint",tn
  gr.set.stroke 10
  gr.color 255,128,128,128,0
  gr.paint.get stln
  gr.modify starttop,"paint",stln
  gr.modify startlft,"paint",stln
  if background()=0 then gr.render
  tot=hrs+minst+minsu+secst+secsu
  if tot=0 & countup=0 then started=0
  if tot=0 & countup=1 then started=1
  if tot>0 then started=1
  if started=1 then
   if countup=1 then
    gr.show ctupgry
   else
    gr.show ctdngry
   endif
   gr.set.stroke 5
   gr.color 255,160,160,160,0
   gr.paint.get mt
   gr.modify muteticka,"paint",mt
   gr.modify mutetickb,"paint",mt
  endif
  do
   gr.touch s,x,y
  until s=0
  gr.set.stroke 5
  gr.color 255,240,240,240,0
  gr.paint.get stln
  gr.modify starttop,"paint",stln
  gr.modify startlft,"paint",stln
 endif

 gr.bounded.touch stoppress,435,300,610,350
 if stoppress=1 then
  vibrate vib[],-1
  gr.set.stroke 10
  gr.color 255,128,128,128,0
  gr.paint.get spln
  gr.modify stoptop,"paint",spln
  gr.modify stoplft,"paint",spln
  started=0
  if countup=1 then
   gr.hide ctupgry
  else
   gr.hide ctdngry
  endif
  gr.set.stroke 5
  gr.color 255,0,0,255,0
  gr.paint.get mt
  gr.modify muteticka,"paint",mt
  gr.modify mutetickb,"paint",mt
  do
   gr.touch s,x,y
  until s=0
  gr.set.stroke 5
  gr.color 255,240,240,240,0
  gr.paint.get spln
  gr.modify stoptop,"paint",spln
  gr.modify stoplft,"paint",spln
 endif
 if started=1 then goto timingend

 gr.bounded.touch resetpress,650,300,825,350
 if resetpress=1 & started=0 then
  vibrate vib[],-1
  started=0
  hrs=oldhrs
  minsu=oldminsu
  minst=oldminst
  secsu=oldsecsu
  secst=oldsecst
  gosub prnt
  gr.set.stroke 10
  gr.color 255,128,128,128,0
  gr.paint.get rsln
  gr.modify resettop,"paint",rsln
  gr.modify resetlft,"paint",rsln
  rstime=clock()+1000
  do
   gr.touch r,x,y
     if clock()=rstime then
     vibrate vib[],-1
      hrs=0
      minst=0
      minsu=0
      secst=0
      secsu=0
      oldhrs=0
      oldminst=0
      oldminsu=0
      oldsecst=0
      oldsecsu=0
     gosub prnt
    endif
  until r=0
  gr.set.stroke 5
  gr.color 255,240,240,240,0
  gr.paint.get rsln
  gr.modify resettop,"paint",rsln
  gr.modify resetlft,"paint",rsln
 endif

 gr.bounded.touch selup,225,380,350,420
 if selup=1 then 
  vibrate vib[],-1
  gr.show ctup
  gr.hide ctdn
  countup=1
  do
   gr.touch s,x,y
  until s=0
 endif

 gr.bounded.touch seldown,430,380,615,420
 if seldown=1 then 
  vibrate vib[],-1
  countup=0
  gr.show ctdn
  gr.hide ctup
  do
   gr.touch s,x,y
  until s=0
 endif

 gr.bounded.touch mutetouch,635,380,675,420
 if mutetouch=1 then 
  vibrate vib[],-1
  muted=bxor(muted,1)
  if muted=1 then
   gr.show muteticka
   gr.show mutetickb
  else
   gr.hide muteticka
   gr.hide mutetickb
  endif
  do
   gr.touch m,x,y
  until m=0
 endif

 gr.bounded.touch hrsarea,220,110,395,250
 if hrsarea=1 then
  vibrate vib[],-1
  t1=0
  do
   gr.touch ht,x,y
   if t1=0 then t1=y
   t2=y
  until ht=0
  if t1>t2 then
   hrs=hrs+1
   if hrs>23 then hrs=0
  endif
  if t2>t1 then
   hrs=hrs-1
   if hrs<0 then hrs=23
  endif
  oldhrs=hrs
  gosub prnt
 endif

 gr.bounded.touch minstensarea,430,110,515,250
 if minstensarea=1 then
  vibrate vib[],-1
  t1=0
  do
   gr.touch mt,x,y
   if t1=0 then t1=y
   t2=y
  until mt=0
  if t1>t2 then
   minst=minst+1
   if minst>5 then minst=0
  endif
  if t2>t1 then
   minst=minst-1
   if minst<0 then minst=5
  endif
  oldminst=minst
  gosub prnt
 endif

 gr.bounded.touch minsunitarea,520,110,595,250
 if minsunitarea=1 then
  vibrate vib[],-1
  t1=0
  do
   gr.touch mt,x,y
   if t1=0 then t1=y
   t2=y
  until mt=0
  if t1>t2 then
   minsu=minsu+1
   if minsu>9 then minsu=0 
  endif
  if t2>t1 then
   minsu=minsu-1
   if minsu<0 then minsu=9
  endif
  oldminsu=minsu
  gosub prnt
 endif

gr.bounded.touch secstensarea,640,110,720,250
 if secstensarea=1 then
  vibrate vib[],-1
  t1=0
  do
   gr.touch st,x,y
   if t1=0 then t1=y
   t2=y
  until st=0
  if t1>t2 then
   secst=secst+1
   if secst>5 then secst=0 
  endif
  if t2>t1 then
   secst=secst-1
   if secst<0 then secst=5
  endif
  oldsecst=secst
  gosub prnt
 endif

gr.bounded.touch secsunitarea,725,110,805,250
 if secsunitarea=1 then
  vibrate vib[],-1
  t1=0
  do
   gr.touch st,x,y
   if t1=0 then t1=y
   t2=y
  until st=0
  if t1>t2 then
   secsu=secsu+1
   if secsu>9 then secsu=0 
  endif
  if t2>t1 then
   secsu=secsu-1
   if secsu<0 then secsu=9
  endif
  oldsecsu=secsu
  gosub prnt
 endif

timingend:
 if background()=0 then gr.render
repeat %end of seconds test

if started=0 then goto loop
if countup=1 then
 secsu=secsu+1
 if secsu>9 then
  secsu=0
  secst=secst+1
  if secst>5 then
   secst=0
   minsu=minsu+1
   if minsu>9 then
    minsu=0
    minst=minst+1
    if minst>5 then
     minst=0
     hrs=hrs+1
     if hrs>23 then hrs=0
    endif
   endif
  endif
 endif
else
 secsu=secsu-1
 if secsu<0 then
  secsu=9
  secst=secst-1
  if secst<0 then
   secst=5
   minsu=minsu-1
   if minsu<0 then
    minsu=9
    minst=minst-1
    if minst<0 then
     minst=5
     hrs=hrs-1
    endif
   endif
  endif
 endif
endif
tottime=hrs+minst+minsu+secst+secsu
if tottime=0 then
 started=0
 finished=1
 goto loop
endif
gosub prnt
goto loop

prnt:
 tim$=format$("%%:%%:%%",(hrs*10000)+(minst*1000)+(minsu*100)+(secst*10)+secsu
 gr.modify textnum,"text",tim$
 if background()=0 then gr.render
 return

onerror:
 end

