FN.DEF                wavCreate$ ( freqS1,freqE1, freqS2,freqE2 , numChan, duration, sampRate, wavOut$ )

 if numChan<1| numChan>2 then numChan=2
 IF wavOut$         = ""
  IF numChan        = 1
   wavOut$          = STR$(freqS1) +"_" + STR$(freqE1)+ "Hz_" 
   wavOut$          = wavOut$ +STR$(duration*1000)+"ms_"+STR$(sampRate)+"Hz_mono.wav"
  ENDIF
  IF numChan        = 2
   wavOut$          =          STR$(freqS1) +"_" + STR$(freqE1)+ "Hz_"
   wavOut$          = wavOut$ +STR$(freqS2) +"_" + STR$(freqE2)+ "Hz_"
   wavOut$          = wavOut$ +STR$(duration*1000)+"ms_"+STR$(sampRate)+"Hz_stereo.wav"
  ENDIF
  wavOut$           = replace $( wavOut$ ,".0","")
 ENDIF

 _2pi              = 2*3.14159
 bitsPerSamp       = 8
 amplitudeRel      = 1
 amplitudeCent     = 2^(bitsPerSamp-1)
 amplitude         = (amplitudeCent -1) * amplitudeRel

 nSamp             = duration * sampRate
 nbytes            = nSamp * numChan * bitsPerSamp/8
 dt                = 1/sampRate
 freqRate1         = (freqE1-freqS1)/duration
 freqRate2         = (freqE2-freqS2)/duration
 !PRINT               amplitude, amplitudeCent, nSamp, nbytes

 BYTE.OPEN           w, fid, wavOut$

 BYTE.WRITE.BUFFER   fid, "RIFF"
 x                 = 36 + nbytes
 BYTE.WRITE.BUFFER   fid, CHR$(x)+CHR$(x/2^8)+CHR$(x/2^16)+CHR$(x/2^24)
 BYTE.WRITE.BUFFER   fid, "WAVEfmt "
 BYTE.WRITE.BUFFER   fid, CHR$(16) +CHR$(0) +CHR$(0) +CHR$(0)
 BYTE.WRITE.BUFFER   fid, CHR$(1) +CHR$(0)
 BYTE.WRITE.BUFFER   fid, CHR$(numChan) +CHR$(0)
 x                 = sampRate
 BYTE.WRITE.BUFFER   fid, CHR$(x)+CHR$(x/2^8)+CHR$(x/2^16)+CHR$(x/2^24)
 x                 = sampRate * numChan * bitsPerSamp/8
 BYTE.WRITE.BUFFER   fid, CHR$(x)+CHR$(x/2^8)+CHR$(x/2^16)+CHR$(x/2^24)
 BYTE.WRITE.BUFFER   fid, CHR$(numChan * bitsPerSamp/8 ) +CHR$(0)
 BYTE.WRITE.BUFFER   fid, CHR$(bitsPerSamp) +CHR$(0)
 BYTE.WRITE.BUFFER   fid, "data"
 x                 = nbytes
 BYTE.WRITE.BUFFER   fid, CHR$(x)+CHR$(x/2^8)+CHR$(x/2^16)+CHR$(x/2^24)

 IF                  bitsPerSamp = 8 & numChan =1
  FOR t            = 0 TO duration STEP dt
   BYTE.WRITE.BYTE   fid , amplitude * SIN((freqS1+freqRate*t) *_2pi *t) + amplitudeCent
  NEXT
 ENDIF
 IF                  bitsPerSamp = 8 & numChan = 2
  FOR t            = 0 TO duration STEP dt
   BYTE.WRITE.BYTE   fid , amplitude * SIN((freqS1+ freqRate1*t) *_2pi *t) + amplitudeCent
   BYTE.WRITE.BYTE   fid , amplitude * SIN((freqS2+ freqRate2*t) *_2pi *t) + amplitudeCent
  NEXT
 ENDIF


 BYTE.CLOSE          fid

 FN.RTN              wavOut$

FN.END







! test it ------------------------------------------------



duration          = 0.3
sampRate          = 11000
frq               = 440

PRINT               "1st example, const. 440Hz, please wait ..."
tic               = clock ()
wavFile$          = wavCreate$ ( frq , frq , frq , frq ,2, duration, sampRate, "" )
PRINT               "time to create wav: " ; clock ()-tic ; " [msec]"
AUDIO.STOP
AUDIO.LOAD          fid, wavFile$
AUDIO.PLAY          fid

pause               1000
PRINT               "...reference: the tone-command, listen..."
pause               1000
tone                frq, duration*1000, 0


pause               1000
print
PRINT               "2nd example, 440 to 880 Hz, please wait ..."
tic               = clock ()
wavFile$          = wavCreate$ ( 1*frq , 2*frq , 1*frq , 2*frq ,2, duration, sampRate, "" )
PRINT               "time to create wav: " ; clock ()-tic ; " [msec]"
AUDIO.STOP
AUDIO.LOAD          fid, wavFile$
AUDIO.PLAY          fid
PAUSE               duration*1000


pause               1000
print
PRINT               "3rd example, 880 to 440 Hz, please wait ..."
tic               = clock ()
wavFile$          = wavCreate$ ( 2*frq , 1*frq , 2*frq , 1*frq ,2, duration, sampRate, "" )
PRINT               "time to create wav: " ; clock ()-tic ; " [msec]"
AUDIO.STOP
AUDIO.LOAD          fid, wavFile$
AUDIO.PLAY          fid
PAUSE               duration*1000



pause               1000
print
PRINT          "4th example: ";
print          "making 12 halftones, starting from c'' (523.251 Hz) "
print          "follow this link:";
print          " http://www.sengpielaudio.com/calculator-notenames.htm"
print


firstNote   = 52

for i       = firstNote  to  firstNote + 12 

  frq       = round (  440 * 2^((i-49)/12)  *10)/10

  n$        = "toneNo_" + str$(i) + ".wav"
  wavFile$  = wavCreate$ (frq ,frq,frq,frq ,1, duration, sampRate, n$)
 
  print       i, frq

  AUDIO.STOP
  AUDIO.LOAD          fid, wavFile$
  AUDIO.PLAY          fid 
 
next

pause               2000

end




