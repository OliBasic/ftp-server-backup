! Aat @2017
BGColor$="0,0,0"
GR.OPEN 255,VAL(WORD$(BGColor$,1,",")),VAL(WORD$(BGColor$,2,",")),VAL(WORD$(BGColor$,3,",")),0,0
GR.SCREEN w,h
XRes=1280:YRes=800
sx=XRes:sy=YRes
sx=w/sx:sy=h/sy
GR.SCALE sx,sy
NumOfTabs=4:TW=200:TabOff=10:Kap=0:Br=0
DIM STab[NumOfTabs],CTab[NumOfTabs],TabTitles$[NumOfTabs],TabColor$[NumOfTabs],TabOrder[NumOfTabs]
DIM MS[25],Tiles[25],TVis[25],GuessNum[25],Subs[12],MSUsed[25]
DataPath$="../../Tabs/data/"
Hlp$="To help you a litle bit....."+CHR$(13)+CHR$(10)
Hlp$=Hlp$+"First of all: you can be lucky! or not...."+CHR$(13)+CHR$(10)+CHR$(13)+CHR$(10)
Hlp$=Hlp$+"The sum of visible numbers on each row, column and diagonal"+CHR$(13)+CHR$(10)
Hlp$=Hlp$+"are given on the grey rectangles."+CHR$(13)+CHR$(10)
Hlp$=Hlp$+"On the right side you will see the numbers still hidden behind blue tiles."+CHR$(13)+CHR$(10)+CHR$(13)+CHR$(10)
Hlp$=Hlp$+"Numbers used are always a series of 25 consecutive numbers and <100"+CHR$(13)+CHR$(10)+CHR$(13)+CHR$(10)
Hlp$=Hlp$+"MIND YOU..."+CHR$(13)+CHR$(10)
Hlp$=Hlp$+"Once you have started the game, you are stuck on this tab!"+CHR$(13)+CHR$(10)
Hlp$=Hlp$+"Finish or Quit the game to leave this tab."+CHR$(13)+CHR$(10)+CHR$(13)+CHR$(10)
Hlp$=Hlp$+"Have fun !"
Pr$="DEMO of TABS in RFO BASIC!"
TabTitles$[1]="General":TabColor$[1]="192,255,192"
TabTitles$[2]="Connection":TabColor$[2]="192,255,255"
TabTitles$[3]="Game":TabColor$[3]="255,192,192"
TabTitles$[4]="Play":TabColor$[4]="192,192,192"
GOSUB CreateTabs

DO
	DO
		IF !Br THEN GOSUB GetTouch
	UNTIL x>Taboff & x<XRes & y<2*YRes/20
	TabNr=CEIL((x-Taboff)/TW):Br=0
	IF TabNr<=NumOfTabs & y>YRes/20 THEN GOSUB TabNr,Tab01,Tab02,Tab03,Tab04
	IF !Br THEN
		IF x>XRes-50 THEN
			DO
				DIALOG.MESSAGE "QUIT ???",,q,"YES","NO"
			UNTIL q
			IF q=1 THEN Kap=1
		ENDIF
	ENDIF
UNTIL Kap

EXIT

ONBACKKEY:
	DO
		DIALOG.MESSAGE ,"Tap on X in top right corner to exit!",q,"OK"
	UNTIL q
BACK.RESUME

GetTouch:
	DO
		GR.TOUCH Touched,x,y
	UNTIL Touched
	DO
		GR.TOUCH Touched,x,y
	UNTIL !Touched
	x/=sx:y/=sy
RETURN

CreateTabs:
	GR.COLOR 255,255,255,0,1
	GR.TEXT.SIZE 45
	GR.TEXT.BOLD 1
	GR.TEXT.DRAW g,100,35,Pr$
	FOR i=1 TO NumOfTabs
		GR.BITMAP.CREATE CTab[i],XRes,YRes-YRes/20
		GR.BITMAP.DRAWINTO.START CTab[i]
			GR.COLOR 255,VAL(WORD$(TabColor$[i],1,",")),VAL(WORD$(TabColor$[i],2,",")),VAL(WORD$(TabColor$[i],3,",")),1
			GR.RECT g,0,YRes/20,XRes,YRes-YRes/20
			Toff=TabOff+(i-1)*TW
			GR.SET.ANTIALIAS 0
			GR.COLOR 255,VAL(WORD$(TabColor$[i],1,",")),VAL(WORD$(TabColor$[i],2,",")),VAL(WORD$(TabColor$[i],3,",")),0
			GR.ARC g,Toff-XRes/100,0,Toff+XRes/100,YRes/20,0,90,0
			GR.ARC g,Toff+XRes/100,0,Toff+XRes/100+YRes/20,YRes/20,180,90,0
			GR.LINE g,Toff+XRes/100+YRes/40,0,Toff+TW-(XRes/100+YRes/40),0
			GR.ARC g,Toff+TW-XRes/100,0,Toff+TW+XRes/100,YRes/20,90,90,0
			GR.ARC g,Toff+TW-(XRes/100+YRes/20),0,Toff+TW-XRes/100,YRes/20,0,-90,0
			GR.BITMAP.FILL CTab[i],Toff+TW/2,YRes/40
			GR.COLOR 255,VAL(WORD$(BGColor$,1,",")),VAL(WORD$(BGColor$,2,",")),VAL(WORD$(BGColor$,3,",")),0
			GR.ARC g,0,YRes/20,20,YRes/20+20,180,90,0
			GR.ARC g,XRes-20,YRes/20,XRes,YRes/20+20,-90,90,0
			GR.ARC g,0,YRes-40-YRes/20,40,YRes-YRes/20,90,90,0
			GR.ARC g,XRes-40,YRes-40-YRes/20,XRes,YRes-YRes/20,0,90,0
			GR.COLOR 255,VAL(WORD$(BGColor$,1,",")),VAL(WORD$(BGColor$,2,",")),VAL(WORD$(BGColor$,3,",")),1
			GR.BITMAP.FILL CTab[i],1,YRes/20+1
			GR.BITMAP.FILL CTab[i],XRes-1,YRes/20+1
			GR.BITMAP.FILL CTab[i],1,YRes-1-YRes/20
			GR.BITMAP.FILL CTab[i],XRes-1,YRes-1-YRes/20
			GR.SET.ANTIALIAS 1
			GR.COLOR 255,VAL(WORD$(BGColor$,1,",")),VAL(WORD$(BGColor$,2,",")),VAL(WORD$(BGColor$,3,",")),1
			GR.TEXT.BOLD 1
			GR.TEXT.SIZE YRes/30
			GR.TEXT.ALIGN 2
			GR.TEXT.DRAW g,Toff+TW/2,YRes/25,TabTitles$[i]
			GR.TEXT.BOLD 0
			GR.SET.STROKE 1
			GR.LINE g,TabOff,YRes/20,TabOff+TW*(i-1),YRes/20
			GR.LINE g,TabOff+TW*i,YRes/20,TabOff+TW*(NumOfTabs),YRes/20
			GR.TEXT.ALIGN 1
			GR.SET.STROKE 0
			GOSUB i,SetUp01,SetUp02,SetUp03,SetUp04,SetUp05,SetUp06
		GR.BITMAP.DRAWINTO.END
		GR.BITMAP.DRAW STab[i],CTab[i],0,YRes/20
		GR.GETDL Order[],1:ARRAY.LENGTH TabOrder[i],Order[]:ARRAY.DELETE Order[]
	NEXT i
	GR.SET.STROKE 3
	GR.TEXT.ALIGN 2
	GR.TEXT.SIZE 50
	GR.COLOR 255,255,255,255,1
	GR.RECT g,1230,2,1278,78
	GR.COLOR 255,255,0,0,0
	GR.RECT g,1230,2,1278,78
	GR.COLOR 255,255,0,0,1
	GR.TEXT.DRAW g,1255,53,"X"
	GR.TEXT.ALIGN 1
	GR.SET.STROKE 0
	GOSUB Tab01
RETURN

OnTop:
	GR.GETDL Order[],1
	ARRAY.SEARCH Order[],TabOrder[T],L1
	SWAP Order[L1],Order[TabOrder[NumOfTabs]]
	GR.NEWDL Order[]
	GR.RENDER:ARRAY.DELETE Order[]
RETURN

SetUp01:
	GR.TEXT.SIZE 40:GR.TEXT.BOLD 1
	GR.COLOR 255,255,128,128,1
	GR.TEXT.UNDERLINE 1
	GR.TEXT.DRAW g,30,100,"Device info"
	GR.TEXT.UNDERLINE 0
	GR.TEXT.DRAW g,30,170,"Brand"
	GR.TEXT.DRAW g,30,240,"Model"
	GR.TEXT.DRAW g,30,310,"Device"
	GR.TEXT.DRAW g,30,380,"Device ID"
	GR.TEXT.DRAW g,30,450,"Android version"
	GR.TEXT.DRAW g,30,520,"Language"
	DEVICE GInf
	BUNDLE.GET GInf,"Brand",B$
	BUNDLE.GET GInf,"Model",M$
	BUNDLE.GET GInf,"Device",D$
	BUNDLE.GET GInf,"DeviceID",DI$
	BUNDLE.GET GInf,"OS",O$
	BUNDLE.GET GInf,"Language",L$
	GR.TEXT.DRAW g,330,170,B$
	GR.TEXT.DRAW g,330,240,M$
	GR.TEXT.DRAW g,330,310,D$
	GR.TEXT.SIZE 30
	GR.TEXT.DRAW g,330,380,DI$
	GR.TEXT.SIZE 40
	GR.TEXT.DRAW g,330,450,O$
	GR.TEXT.DRAW g,330,520,L$
	GR.TEXT.UNDERLINE 1
	GR.TEXT.DRAW g,620,100,"Sensors"
	GR.TEXT.UNDERLINE 0
	SENSORS.LIST Sens$[]
	ARRAY.LENGTH L,Sens$[]
	FOR j=1 TO L
		GR.TEXT.DRAW g,620,100+j*50,Sens$[j]
	NEXT j
RETURN
Tab01:
	T=1:GOSUB OnTop
RETURN

SetUp02:
	GR.TEXT.SIZE 40:GR.TEXT.BOLD 1
	GR.COLOR 255,0,0,255,1
	GR.TEXT.UNDERLINE 1
	GR.TEXT.DRAW g,30,100,"Bluetooth"
	GR.TEXT.UNDERLINE 0
	GR.TEXT.DRAW g,30,170,"Status"
	GR.TEXT.DRAW g,30,240,"Device name"
	GR.TEXT.DRAW g,30,310,"MAC address"
	BT.STATUS C$,D$,M$
	D$=""+D$:M$=""+M$
	IF IS_NUMBER(C$) THEN
		IF VAL(C$)=-1 THEN C$="Not Enabled"
		IF VAL(C$)=0 THEN C$="Idle"
		IF VAL(C$)=1 THEN C$="Listening"
		IF VAL(C$)=2 THEN C$="Connecting"
		IF VAL(C$)=3 THEN C$="Connected"
	ENDIF
	IF D$="null" THEN D$="n/a"
	IF M$="null" THEN M$="n/a"
	GR.TEXT.DRAW g,330,170,C$
	GR.TEXT.DRAW g,330,240,D$
	GR.TEXT.SIZE 30
	GR.TEXT.DRAW g,330,310,M$
	GR.TEXT.SIZE 40
	GR.COLOR 255,128,128,0,1
	GR.TEXT.UNDERLINE 1
	GR.TEXT.DRAW g,620,100,"WIFI"
	GR.TEXT.UNDERLINE 0
	GR.TEXT.DRAW g,620,170,"SSID"
	GR.TEXT.DRAW g,620,240,"BSSID"
	GR.TEXT.DRAW g,620,310,"MAC address"
	GR.TEXT.DRAW g,620,380,"IP address"
	GR.TEXT.DRAW g,620,450,"Speed (Mbps)"
	WIFI.INFO S$,B$,M$,IP$,S
	GR.TEXT.DRAW g,920,170,S$
	B$=""+B$:GR.TEXT.DRAW g,920,240,B$
	GR.TEXT.SIZE 30
	GR.TEXT.DRAW g,920,310,M$
	GR.TEXT.SIZE 40
	GR.TEXT.DRAW g,920,380,IP$
	GR.TEXT.DRAW g,920,450,INT$(S)
	GR.COLOR 255,128,0,128,1
	GR.TEXT.UNDERLINE 1
	GR.TEXT.DRAW g,30,450,"Phone"
	GR.TEXT.UNDERLINE 0
	GR.TEXT.DRAW g,30,520,"Phone type"
	GR.TEXT.DRAW g,30,590,"Network type"
	GR.TEXT.DRAW g,30,660,"Operator"
	PHONE.INFO Inf
	BUNDLE.GET Inf,"PhoneType",P$
	BUNDLE.GET Inf,"NetworkType",N$
	BUNDLE.CONTAIN Inf,"Operator",T
	IF T THEN
		BUNDLE.GET Inf,"Operator",O$
	ELSE
		O$="n/a"
	ENDIF
	GR.TEXT.DRAW g,330,520,P$
	GR.TEXT.DRAW g,330,590,N$
	GR.TEXT.DRAW g,330,660,O$
RETURN
Tab02:
	T=2:GOSUB OnTop
RETURN

SetUp03:
	GR.TEXT.SIZE 40:GR.TEXT.BOLD 1
	GR.COLOR 255,0,128,0,1
	GR.TEXT.ALIGN 2:GR.PAINT.GET CorCol:GR.TEXT.ALIGN 1
	GR.COLOR 255,0,0,0,1
	GR.TEXT.UNDERLINE 1
	GR.TEXT.DRAW g,30,100,"Play Magic Square"
	GR.TEXT.UNDERLINE 0
	GR.LINE g,550,80,550,800
	GR.TEXT.DRAW g,30,150,"A Magic Square is an NxN"
	GR.TEXT.DRAW g,30,195,"matrix in which every row,"
	GR.TEXT.DRAW g,30,240,"column, and diagonal add"
	GR.TEXT.DRAW g,30,285,"up to the same number."
	GR.TEXT.DRAW g,30,350,"A 5x5 Magic Square is"
	GR.TEXT.DRAW g,30,395,"hidden behind blue tiles."
	GR.TEXT.DRAW g,30,460,"By tapping on a tile you"
	GR.TEXT.DRAW g,30,505,"get a chance to guess the"
	GR.TEXT.DRAW g,30,550,"number behind this tile."
	GR.TEXT.DRAW g,30,595,"You should at least score"
	GR.TEXT.DRAW g,30,640,"11 correct answers!"
	GR.TEXT.DRAW g,30,685,"As the game progresses"
	GR.TEXT.DRAW g,30,730,"your chances increase...."
	GR.SET.STROKE 3
	GR.COLOR 255,0,0,0,0
	FOR j=1 TO 6
		GR.LINE g,610+j*50,150,610+j*50,400
		GR.LINE g,660,100+j*50,910,100+j*50
	NEXT j
	GR.COLOR 255,128,128,128,1
	FOR j=1 TO 5
		GR.RECT g,920,100+j*50,980,146+j*50
		GR.RECT g,610+j*50,410,656+j*50,470
		GR.LINE g,910,125+j*50,920,125+j*50
		GR.LINE g,635+j*50,400,635+j*50,410
	NEXT j
	GR.RECT g,920,410,980,470
	GR.RECT g,590,410,650,470
	GR.LINE g,910,400,922,412
	GR.LINE g,659,400,647,412
	GR.COLOR 255,0,0,0,0
	GR.RECT g,1130,100,1230,750
RETURN
Tab03:
	T=3:GOSUB OnTop
	GR.TEXT.SIZE 30
	GR.COLOR 255,0,0,0,0
	GR.RECT f1,898,698,1082,742
	GR.COLOR 255,0,0,255,1
	GR.RECT f2,900,700,1080,740
	GR.COLOR 255,255,255,0,1
	GR.TEXT.DRAW f3,910,730,"Quit playing"
	GR.GROUP But1,f1,f2,f3
	GR.HIDE But1
	GR.COLOR 255,0,0,0,0
	GR.RECT g1,568,698,762,742
	GR.RECT h1,568,638,762,682
	GR.COLOR 255,0,0,255,1
	GR.RECT g2,570,700,760,740
	GR.RECT h2,570,640,760,680
	GR.COLOR 255,255,255,0,1
	GR.TEXT.DRAW g3,580,730,"Explain more"
	GR.TEXT.DRAW h3,580,670," Start Play"
	GR.GROUP But2,g1,g2,g3,h1,h2,h3
	GR.RENDER:Sp=0:Br=0
	DO
		GOSUB GetTouch
			IF x>570 & x<760 & y>700 & y<740 THEN
				DIALOG.MESSAGE ,Hlp$,q,"OK"
			ENDIF
			IF (y>YRes/20 & y<2*YRes/20 & (CEIL((x-Taboff)/TW)<3 | CEIL((x-Taboff)/TW)=4)) | (x>XRes-50 & y<2*YRes/20) THEN
				GR.GETDL Order[],1:ARRAY.LENGTH L,Order[]:GR.NEWDL Order[1,L-11]:GR.RENDER:ARRAY.DELETE Order[]
				POPUP "Tap Game Tab to Play again !",,-300
				Br=1:D_U.BREAK
			ENDIF
	UNTIL x>570 & x<760 & y>640 & y<680
	IF Br=1 THEN RETURN
	GR.HIDE But2:GR.SHOW But1:GR.SET.STROKE 0:GR.RENDER

	! Create numbers
	GR.TEXT.SIZE 40:GR.TEXT.ALIGN 2
	GR.COLOR 255,0,0,255,1
	aM=FLOOR(74*RND()+1):rM=FLOOR(4*RND())
	READ.FROM 1+rM*25
	FOR k=1 TO 5
		FOR j=1 TO 5
			READ.NEXT MS[j+(k-1)*5]
			MS[j+(k-1)*5]=MS[j+(k-1)*5]+aM
			GR.TEXT.DRAW GuessNum[j+(k-1)*5],635+j*50,180+k*50,INT$(MS[j+(k-1)*5])
		NEXT j
	NEXT k
	GR.TEXT.ALIGN 1
	GR.TEXT.SIZE 30
	ARRAY.SUM Total,MS[1,5]
	GR.TEXT.DRAW g,650,170,"Sum (row, column, diagonal)= "+INT$(Total)
	ARRAY.MIN SMin,MS[]:ARRAY.MAX SMax,MS[]
	GR.TEXT.DRAW g,650,120,"Numbers in this square "+INT$(SMin)+" - "+INT$(SMax)
	ARRAY.FILL TVis[],1
	! Totals
	GR.TEXT.ALIGN 2
	FOR j=1 TO 5
		GR.TEXT.DRAW Subs[j],950,173+j*50,"0"
		GR.ROTATE.START 90,620+j*50,480
			GR.TEXT.DRAW Subs[j+5],620+j*50,480,"0"
		GR.ROTATE.END
	NEXT j
	GR.ROTATE.START 45,940,485
		GR.TEXT.DRAW Subs[11],940,485,"0"
	GR.ROTATE.END
	GR.ROTATE.START 315,625,487
		GR.TEXT.DRAW Subs[12],625,487,"0"
	GR.ROTATE.END
	GR.TEXT.ALIGN 1
	! Show used numbers
	GR.ROTATE.START 90,1150,350
		GR.TEXT.DRAW g,1150,350,"Unused Numbers"
	GR.ROTATE.END
	FOR i=1 TO 25
		GR.TEXT.DRAW MSUsed[i],1180,150+i*25,INT$(SMin+i-1)
	NEXT i

	! Create tiles for game
	FOR i=1 TO 5
		FOR j=1 TO 5
			GR.RECT Tiles[j+(i-1)*5],612+j*50,142+i*50,658+j*50,188+i*50
		NEXT j
	NEXT i
	TilesLeft=25:Sp=0:WellGuessed=0
	GR.TEXT.DRAW Score,650,560,"Score "+INT$(WellGuessed)
	GR.COLOR 255,255,255,0,1
	GR.CIRCLE Ind,0,0,10:GR.HIDE Ind:GR.RENDER
	DO
		DO
			GOSUB GetTouch
			IF (y>YRes/20 & y<2*YRes/20 & (CEIL((x-Taboff)/TW)<3 | CEIL((x-Taboff)/TW)=4)) | (x>XRes-50 & y<2*YRes/20) THEN
				DIALOG.MESSAGE "PLEASE Quit this game first!",,q,"OK"
			ENDIF
			xt=FLOOR((x-(610))/50)
			yt=FLOOR((y-(140))/50)
			IF x>900 & x<1080 & y>700 & y<740 THEN Sp=1
		UNTIL (xt>0 & xt<6 & yt>0 & yt<6) | Sp=1
		IF Sp THEN D_U.BREAK
		IF TVis[xt+(yt-1)*5]=1 THEN
			GR.MODIFY Ind,"x",635+xt*50,"y",165+yt*50:GR.SHOW Ind:GR.RENDER
			DO
				INPUT "Guess the number behind this block",GB,,IsCanceled
			UNTIL !IsCanceled
			IF GB=MS[xt+(yt-1)*5] THEN
				R$="CORRECT"
				WellGuessed=WellGuessed+1
				GR.MODIFY Score,"text","Score "+INT$(WellGuessed)
				GR.MODIFY GuessNum[xt+(yt-1)*5],"paint",CorCol
			ELSE
				R$="WRONG"
			ENDIF
			DO
				DIALOG.MESSAGE "The answer is "+R$+" !","You chose "+INT$(GB)+". The number is "+INT$(MS[xt+(yt-1)*5])+".",Q,"OK"
			UNTIL Q
			GR.HIDE Tiles[xt+(yt-1)*5]:GR.HIDE Ind:GR.HIDE MSUsed[MS[xt+(yt-1)*5]-SMin+1]
			TVis[xt+(yt-1)*5]=0
			TilesLeft=TilesLeft-1
			FOR j=1 TO 5
				HS=0
				FOR i=1 TO 5
					IF TVis[i+(j-1)*5]=0 THEN HS=HS+MS[i+(j-1)*5]
				NEXT i
				GR.MODIFY Subs[j],"text",INT$(HS)
			NEXT j
			FOR j=1 TO 5
				HS=0
				FOR i=1 TO 5
					IF TVis[j+(i-1)*5]=0 THEN HS=HS+MS[j+(i-1)*5]
				NEXT i
				GR.MODIFY Subs[j+5],"text",INT$(HS)
			NEXT j
			HS=0
			FOR i=1 TO 25 STEP 6
				IF TVis[i]=0 THEN HS=HS+MS[i]
			NEXT i
			GR.MODIFY Subs[11],"text",INT$(HS)
			HS=0
			FOR i=5 TO 21 STEP 4
				IF TVis[i]=0 THEN HS=HS+MS[i]
			NEXT i
			GR.MODIFY Subs[12],"text",INT$(HS)
			GR.RENDER
		ENDIF
	UNTIL TilesLeft=0
	IF WellGuessed<11 THEN R$="You should try harder !"
	IF WellGuessed=11 THEN R$="You performed OK !"
	IF WellGuessed>11 THEN R$="You have done VERY well !"
	DO
		DIALOG.MESSAGE "You have scored "+INT$(WellGuessed)+" correct guesses...",R$+CHR$(10)+CHR$(13)+"Tap Game tab to play again",Q,"OK"
	UNTIL Q
	! Remove numbers and tiles and the rest
	GR.GETDL Order[],1:ARRAY.LENGTH L,Order[]:GR.NEWDL Order[1,L-119]:GR.RENDER:ARRAY.DELETE Order[]
	POPUP "Tap Game Tab to Play!"
RETURN

SetUp04:
	GR.COLOR 255,255,255,255,1
	AUDIO.LOAD Ultra,DataPath$+"Vienna.mp3"
	AUDIO.LENGTH TotTm,Ultra
	GR.BITMAP.LOAD Vox,DataPath$+"Ultravox.png"
	GR.BITMAP.SCALE VoxS,Vox,350,sx/sy*350
	GR.BITMAP.DRAW g,VoxS,80,80
	GR.BITMAP.LOAD Lol,DataPath$+"Lol.jpg"
	GR.BITMAP.SCALE LolS,Lol,257,sx/sy*100
	GR.BITMAP.DRAW g,LolS,1000,550
	GR.COLOR 255,0,0,255,1
	GR.TEXT.DRAW g,180,600,"Now Playing...."
	GR.COLOR 255,255,255,0,1
	GR.RECT g,530,200,850,280
	GR.RECT g,530,400,850,430
	GR.COLOR 255,0,0,0,0
	GR.SET.STROKE 5
	GR.RECT g,530,200,850,280
	FOR j=0 TO 2
		GR.RECT g,560+j*100,210,620+j*100,270
	NEXT j
	GR.RECT g,530,400,850,430
	LIST.CREATE N,Tri
	LIST.ADD Tri,565,215,615,240,565,265
	GR.COLOR 255,0,0,0,1
	GR.POLY g,Tri
	GR.RECT g,665,215,715,265
	GR.RECT g,775,215,785,265
	GR.RECT g,795,215,805,265
	GR.SET.STROKE 0
	GR.TEXT.SIZE 15
	GR.TEXT.DRAW g,570,300,"PLAY"
	GR.TEXT.DRAW g,670,300,"STOP"
	GR.TEXT.DRAW g,765,300,"PAUSE"
	GR.TEXT.DRAW g,650,390,"PROGRESS"
RETURN
Tab04:
	T=4:GOSUB OnTop
	AUDIO.ISDONE PlFin
	IF PlFin THEN
		AUDIO.PLAY Ultra:AUDIO.LOOP
	ELSE
		AUDIO.POSITION.CURRENT cP
	ENDIF
	GR.COLOR 255,255,0,128,1
	GR.RECT Progress,533,443,cP/TotTm*315+533,467
	GR.RENDER
	Br=0:TIMER.SET 300
	DO
		GOSUB GetTouch
			IF y>250 & y<310 THEN
				IF x>560 & x<620 THEN
					AUDIO.ISDONE PlFin
					IF PlFin THEN AUDIO.PLAY Ultra:TIMER.SET 300
				ENDIF
				IF x>660 & X<720 THEN
					TIMER.CLEAR
					AUDIO.STOP
					cP=0
					GR.MODIFY Progress,"right",533
					GR.RENDER
				ENDIF
				IF x>760 & X<820 THEN TIMER.CLEAR:AUDIO.PAUSE
			ENDIF
			IF (y>YRes/20 & y<2*YRes/20 & CEIL((x-Taboff)/TW)<4) THEN Br=1
			IF (x>XRes-50 & y<2*YRes/20) THEN
				DO
					DIALOG.MESSAGE "QUIT ???",,q,"YES","NO"
				UNTIL q
				IF q=1 THEN Kap=1:Br=1
			ENDIF
	UNTIL Br=1
	TIMER.CLEAR
	GR.GETDL Order[],1:ARRAY.LENGTH L,Order[]:GR.NEWDL Order[1,L-1]:GR.RENDER:ARRAY.DELETE Order[]
RETURN
READ.DATA 19,1,2,20,23,18,14,9,16,8,21,15,13,11,5,4,10,17,12,22,3,25,24,6,7,25,13,1,19,7,16,9,22,15,3,12,5,18,6,24,8,21,14,2,20,4,17,10,23,11
READ.DATA 23,6,19,2,15,4,12,25,8,16,10,18,1,14,22,11,24,7,20,3,17,5,13,21,9,9,2,25,18,11,3,21,19,12,10,22,20,13,6,4,16,14,7,5,23,15,8,1,24,17
ONTIMER:
	AUDIO.POSITION.CURRENT cP
	GR.MODIFY Progress,"right",cP/TotTm*315+533
	GR.RENDER
TIMER.RESUME