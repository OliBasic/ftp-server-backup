! Aat @2017
BGColor$="0,0,0"
GR.OPEN 255,VAL(WORD$(BGColor$,1,",")),VAL(WORD$(BGColor$,2,",")),VAL(WORD$(BGColor$,3,",")),0,0
GR.SCREEN w,h
XRes=1280:YRes=800
sx=XRes:sy=YRes
sx=w/sx:sy=h/sy
GR.SCALE sx,sy
NumOfTabs=6:TW=200:TabOff=10:Kap=0
DIM STab[NumOfTabs],CTab[NumOfTabs],TabTitles$[NumOfTabs],TabColor$[NumOfTabs],TabOrder[NumOfTabs]
! Title of program
Pr$="TABS in RFO BASIC!"
! Titles and colors for each tab
TabTitles$[1]="Tab01":TabColor$[1]="192,255,192"
TabTitles$[2]="Tab02":TabColor$[2]="192,255,255"
TabTitles$[3]="Tab03":TabColor$[3]="255,192,192"
TabTitles$[4]="Tab04":TabColor$[4]="192,192,255"
TabTitles$[5]="Tab05":TabColor$[5]="255,255,192"
TabTitles$[6]="Tab06":TabColor$[6]="192,192,192"
GOSUB CreateTabs

DO
	DO
		GOSUB GetTouch
	UNTIL x>Taboff & x<XRes & y<2*YRes/20
	TabNr=CEIL((x-Taboff)/TW):Br=0
	IF TabNr<=NumOfTabs & y>YRes/20 THEN GOSUB TabNr,Tab01,Tab02,Tab03,Tab04,Tab05,Tab06
	IF x>XRes-50 THEN
		DO
			DIALOG.MESSAGE "QUIT ???",,q,"YES","NO"
		UNTIL q
		IF q=1 THEN Kap=1
	ENDIF
UNTIL Kap

EXIT

ONBACKKEY:
	DO
		DIALOG.MESSAGE ,"Tap on X in top right corner to exit!",q,"OK"
	UNTIL q
BACK.RESUME

GetTouch:
	DO
		GR.TOUCH Touched,x,y
	UNTIL Touched
	DO
		GR.TOUCH Touched,x,y
	UNTIL !Touched
	x/=sx:y/=sy
RETURN

CreateTabs:
	GR.COLOR 255,255,255,0,1
	GR.TEXT.SIZE 45
	GR.TEXT.BOLD 1
	GR.TEXT.DRAW g,100,35,Pr$
	FOR i=1 TO NumOfTabs
		GR.BITMAP.CREATE CTab[i],XRes,YRes-YRes/20
		GR.BITMAP.DRAWINTO.START CTab[i]
			GR.COLOR 255,VAL(WORD$(TabColor$[i],1,",")),VAL(WORD$(TabColor$[i],2,",")),VAL(WORD$(TabColor$[i],3,",")),1
			GR.RECT g,0,YRes/20,XRes,YRes-YRes/20
			Toff=TabOff+(i-1)*TW
			GR.SET.ANTIALIAS 0
			GR.COLOR 255,VAL(WORD$(TabColor$[i],1,",")),VAL(WORD$(TabColor$[i],2,",")),VAL(WORD$(TabColor$[i],3,",")),0
			GR.ARC g,Toff-XRes/100,0,Toff+XRes/100,YRes/20,0,90,0
			GR.ARC g,Toff+XRes/100,0,Toff+XRes/100+YRes/20,YRes/20,180,90,0
			GR.LINE g,Toff+XRes/100+YRes/40,0,Toff+TW-(XRes/100+YRes/40),0
			GR.ARC g,Toff+TW-XRes/100,0,Toff+TW+XRes/100,YRes/20,90,90,0
			GR.ARC g,Toff+TW-(XRes/100+YRes/20),0,Toff+TW-XRes/100,YRes/20,0,-90,0
			GR.BITMAP.FILL CTab[i],Toff+TW/2,YRes/40
			GR.COLOR 255,VAL(WORD$(BGColor$,1,",")),VAL(WORD$(BGColor$,2,",")),VAL(WORD$(BGColor$,3,",")),0
			GR.ARC g,0,YRes/20,20,YRes/20+20,180,90,0
			GR.ARC g,XRes-20,YRes/20,XRes,YRes/20+20,-90,90,0
			GR.ARC g,0,YRes-40-YRes/20,40,YRes-YRes/20,90,90,0
			GR.ARC g,XRes-40,YRes-40-YRes/20,XRes,YRes-YRes/20,0,90,0
			GR.COLOR 255,VAL(WORD$(BGColor$,1,",")),VAL(WORD$(BGColor$,2,",")),VAL(WORD$(BGColor$,3,",")),1
			GR.BITMAP.FILL CTab[i],1,YRes/20+1
			GR.BITMAP.FILL CTab[i],XRes-1,YRes/20+1
			GR.BITMAP.FILL CTab[i],1,YRes-1-YRes/20
			GR.BITMAP.FILL CTab[i],XRes-1,YRes-1-YRes/20
			GR.SET.ANTIALIAS 1
			GR.TEXT.BOLD 1
			GR.TEXT.SIZE YRes/30
			GR.TEXT.ALIGN 2
			GR.TEXT.DRAW g,Toff+TW/2,YRes/25,TabTitles$[i]
			GR.TEXT.BOLD 0
			GR.SET.STROKE 1
			GR.LINE g,TabOff,YRes/20,TabOff+TW*(i-1),YRes/20
			GR.LINE g,TabOff+TW*i,YRes/20,TabOff+TW*(NumOfTabs),YRes/20
			GR.TEXT.ALIGN 1
			GR.SET.STROKE 0
			GOSUB i,SetUp01,SetUp02,SetUp03,SetUp04,SetUp05,SetUp06
		GR.BITMAP.DRAWINTO.END
		GR.BITMAP.DRAW STab[i],CTab[i],0,YRes/20
		GR.GETDL Order[],1:ARRAY.LENGTH TabOrder[i],Order[]:ARRAY.DELETE Order[]
	NEXT i
	GR.SET.STROKE 3
	GR.TEXT.ALIGN 2
	GR.TEXT.SIZE 50
	GR.COLOR 255,255,255,255,1
	GR.RECT g,1230,2,1278,78
	GR.COLOR 255,255,0,0,0
	GR.RECT g,1230,2,1278,78
	GR.COLOR 255,255,0,0,1
	GR.TEXT.DRAW g,1255,53,"X"
	GR.TEXT.ALIGN 1
	GR.SET.STROKE 0
	GOSUB Tab01
RETURN

OnTop:
	GR.GETDL Order[],1
	ARRAY.SEARCH Order[],TabOrder[T],L1
	SWAP Order[L1],Order[TabOrder[NumOfTabs]]
	GR.NEWDL Order[]
	GR.RENDER:ARRAY.DELETE Order[]
RETURN

SetUp01:
	! Create layout Place text, pics etc. here
	GR.COLOR 255,0,0,0,1
	GR.TEXT.DRAW g,50,100,"My TAB "+INT$(i)
RETURN
Tab01:
	T=1:GOSUB OnTop
	! Place dynamic contents here
	! Manipulate Display List when leaving this TAB (if extra objects have been added)
RETURN

SetUp02:
	! Create layout Place text, pics etc. here
	GR.COLOR 255,0,0,0,1
	GR.TEXT.DRAW g,50,100,"My TAB "+INT$(i)
RETURN
Tab02:
	T=2:GOSUB OnTop
	! Place dynamic contents here
	! Manipulate Display List when leaving this TAB (if extra objects have been added)
RETURN

SetUp03:
	! Create layout Place text, pics etc. here
	GR.COLOR 255,0,0,0,1
	GR.TEXT.DRAW g,50,100,"My TAB "+INT$(i)
RETURN
Tab03:
	T=3:GOSUB OnTop
	! Place dynamic contents here
	! Manipulate Display List when leaving this TAB (if extra objects have been added)
RETURN

SetUp04:
	! Create layout Place text, pics etc. here
	GR.COLOR 255,0,0,0,1
	GR.TEXT.DRAW g,50,100,"My TAB "+INT$(i)
RETURN
Tab04:
	T=4:GOSUB OnTop
	! Place dynamic contents here
	! Manipulate Display List when leaving this TAB (if extra objects have been added)
RETURN

SetUp05:
	! Create layout Place text, pics etc. here
	GR.COLOR 255,0,0,0,1
	GR.TEXT.DRAW g,50,100,"My TAB "+INT$(i)
RETURN
Tab05:
	T=5:GOSUB OnTop
	! Place dynamic contents here
	! Manipulate Display List when leaving this TAB (if extra objects have been added)
RETURN

SetUp06:
	! Create layout Place text, pics etc. here
	GR.COLOR 255,0,0,0,1
	GR.TEXT.DRAW g,50,100,"My TAB "+INT$(i)
RETURN
Tab06:
	T=6:GOSUB OnTop
	! Place dynamic contents here
	! Manipulate Display List when leaving this TAB (if extra objects have been added)
RETURN
