! TimeLapse.bas
! Aat Don @2013
GR.OPEN 255,0,0,0,0,0
GR.SCREEN w,h
ScaleX=960
ScaleY=w/h*ScaleX
sx=h/ScaleX
sy=w/ScaleY
GR.SCALE sx,sy
DataDir$="../../TimeLapse/data/"
ARRAY.LOAD S[],0,4,1,1,3,0,20,0,22,1,23,4,23,40,22,43,20,44,3,44,1,43,0,40
LIST.CREATE n,L1
LIST.ADD.ARRAY L1,S[]
WAKELOCK 2
DIM P$[3]
PictureNum=0
! Defaults
StartDelay=10
Interval=60
NumPics=60
SndClick=0
Flash=0
CamType=0
FN$="Pic"

DO
  GR.CLS
  GR.TEXT.SIZE 30
  GR.TEXT.BOLD 1
  GR.COLOR 255,255,255,255,1
  GR.TEXT.DRAW g,50,50,"Delay before Start: "+REPLACE$(FORMAT$("####",StartDelay)," ","")+" s"
  GR.TEXT.DRAW g,50,100,"Interval: "+REPLACE$(FORMAT$("####",Interval)," ","")+" s"
  GR.TEXT.DRAW g,50,150,"Number of pictures: "+REPLACE$(FORMAT$("####",NumPics)," ","")
  Dur=(NumPics-1)*Interval
  Hrs=FLOOR(Dur/3600)
  Dur=Dur-Hrs*3600
  Mins=FLOOR(Dur/60)
  Secs=Dur-Mins*60
  H$=REPLACE$(FORMAT$("%%",Hrs)," ","")
  M$=REPLACE$(FORMAT$("%%",Mins)," ","")
  S$=REPLACE$(FORMAT$("%%",Secs)," ","")
  GR.TEXT.DRAW g,50,200,"Duration: "+H$+":"+M$+":"+S$
  IF Flash=1 THEN
    Fl$="ON"
  ELSE
    Fl$="OFF"
  ENDIF
  GR.TEXT.DRAW g,50,250,"Flash = "+Fl$
  IF SndClick=1 THEN
    Snd$="ON"
  ELSE
    Snd$="OFF"
  ENDIF
  GR.TEXT.DRAW g,50,300,"Sound = "+Snd$
  IF CamType=0 THEN
    Cam$="Back side"
  ELSE
    Cam$="Front side"
  ENDIF
  GR.CAMERA.SELECT CamType+1
  GR.TEXT.DRAW g,50,350,"Camera: "+Cam$
  GR.TEXT.DRAW g,50,400,"File name: "+FN$+"XXXX"+".jpg"
  FOR i=0 TO 4
    GR.COLOR 255,60-i*15,195+i*15,60-i*15,1
    GR.RECT g,50+4*i,420+4*i,250-4*i,490-4*i
  NEXT i
  FOR i=0 TO 4
    GR.COLOR 255,195+i*15,60-i*15,60-i*15,1
    GR.RECT g,260+4*i,420+4*i,460-4*i,490-4*i
  NEXT i
  GR.COLOR 255,0,0,0,1
  GR.TEXT.DRAW g,130,465,"OK"
  GR.TEXT.DRAW g,300,465,"CHANGE"
  GR.RENDER
  DO
    GR.TOUCH touched,x,y
    GR.RENDER
  UNTIL touched
  DO
    GR.TOUCH touched,x,y
    GR.RENDER
  UNTIL !touched
  x=ROUND(x/sx)
  y=ROUND(y/sy)
  IF x>260 & x<460 & y>420 & y<490 THEN GOSUB GetParms
UNTIL (x>50 & x<250 & y>420 & y<490)
TONE 500,200
IF Flash=0 THEN Flash=2

GR.CAMERA.MANUALSHOOT PicN,Flash
If PicN=0 then
   Popup "Incompatible hardware !",0,0,4
   End "Sorry..."
ENDIF
GR.BITMAP.DELETE PicN
! Calc start time
TIME ,,,Hour$,Minute$,Second$
StrtHr=VAL(Hour$)
StrtMin=VAL(Minute$)
StrtSec=VAL(Second$)
StrtMin=StrtMin+FLOOR((StrtSec+StartDelay)/60)
StrtSec=MOD(StrtSec+StartDelay,60)
StrtHr=StrtHr+FLOOR(StrtMin/60)
StrtMin=MOD(StrtMin,60)
StrtHr=MOD(StrtHr,24)
StH$=REPLACE$(FORMAT$("%%",StrtHr)," ","")
StM$=REPLACE$(FORMAT$("%%",StrtMin)," ","")
StS$=REPLACE$(FORMAT$("%%",StrtSec)," ","")
StTime$=StH$+":"+StM$+":"+StS$
! Calc end time
TotalSeconds=(NumPics-1)*Interval
EMin=StrtMin+FLOOR((StrtSec+TotalSeconds)/60)
ESec=MOD(StrtSec+TotalSeconds,60)
EHr=StrtHr+FLOOR(EMin/60)
EMin=MOD(EMin,60)
EHr=MOD(EHr,24)
EH$=REPLACE$(FORMAT$("%%",EHr)," ","")
EM$=REPLACE$(FORMAT$("%%",EMin)," ","")
ES$=REPLACE$(FORMAT$("%%",ESec)," ","")
FinalTime$=EH$+":"+EM$+":"+ES$
GR.CLS
GOSUB DrawScreen
GR.RENDER
TIMER.SET 1000
CntDwn=StartDelay
GR.COLOR 255,0,0,255,1
GR.RECT g,425,420,800,460
GR.COLOR 255,255,255,0,1
GR.TEXT.DRAW g,450,450,"Start after"+FORMAT$("#####",CntDwn)+" s"
GR.RENDER
DO
UNTIL FLOOR(CntDwn)<=0
TIMER.CLEAR
GR.COLOR 255,0,0,0,1
GR.RECT g,425,420,802,462
GR.COLOR 255,255,255,0,1
FOR i=1 TO NumPics
  CS=CLOCK()
  PictureNum=PictureNum+1
  TIME ,,,Hour$,Minute$,Second$
  T$=Hour$+":"+Minute$+":"+Second$
  IF SndClick=1 THEN
    TONE 1200,200
    TONE 800,200
  ENDIF
  GR.CAMERA.AUTOSHOOT PicN,Flash
  GR.BITMAP.LOAD PicT,"image.png"
  GR.BITMAP.SAVE PicT,DataDir$+FN$+REPLACE$(FORMAT$("%%%%",PictureNum)," ","")+".jpg"
  GR.BITMAP.DELETE PicN
  GR.BITMAP.DELETE PicT
  IF i=1 THEN
    GR.BITMAP.LOAD PicT,DataDir$+FN$+"0001.jpg"
    GR.BITMAP.SCALE Pic1,PicT,375,230
    GR.BITMAP.DRAW PicD1,Pic1,25,70
    GR.BITMAP.DELETE PicT
    P$[1]=REPLACE$(FORMAT$("%%%%",PictureNum)," ","")+" - "+T$
    GR.TEXT.DRAW g1,100,400,"#"+P$[1]
  ENDIF
  IF i=2 THEN
    GR.BITMAP.LOAD PicT,DataDir$+FN$+"0001.jpg"
    GR.BITMAP.SCALE Pic1,PicT,375,230
    GR.MODIFY PicD1,"bitmap",Pic1
    GR.BITMAP.DELETE PicT
    GR.BITMAP.LOAD PicT,DataDir$+FN$+"0002.jpg"
    GR.BITMAP.SCALE Pic2,PicT,375,230
    GR.BITMAP.DRAW PicD2,Pic2,425,70
    GR.BITMAP.DELETE PicT
    P$[2]=REPLACE$(FORMAT$("%%%%",PictureNum)," ","")+" - "+T$
    GR.TEXT.DRAW g2,500,400,"#"+P$[2]
  ENDIF
  IF i=3 THEN
    GR.BITMAP.LOAD PicT,DataDir$+FN$+"0001.jpg"
    GR.BITMAP.SCALE Pic1,PicT,375,230
    GR.MODIFY PicD1,"bitmap",Pic1
    GR.BITMAP.DELETE PicT
    GR.BITMAP.LOAD PicT,DataDir$+FN$+"0002.jpg"
    GR.BITMAP.SCALE Pic2,PicT,375,230
    GR.MODIFY PicD2,"bitmap",Pic2
    GR.BITMAP.DELETE PicT
    GR.BITMAP.LOAD PicT,DataDir$+FN$+"0003.jpg"
    GR.BITMAP.SCALE Pic3,PicT,375,230
    GR.BITMAP.DRAW PicD3,Pic3,825,70
    GR.BITMAP.DELETE PicT
    P$[3]=REPLACE$(FORMAT$("%%%%",PictureNum)," ","")+" - "+T$
    GR.TEXT.DRAW g3,900,400,"#"+P$[3]
  ENDIF
  IF i>3 THEN
    GR.BITMAP.LOAD PicT,DataDir$+FN$+REPLACE$(FORMAT$("%%%%",PictureNum-2)," ","")+".jpg"
    GR.BITMAP.SCALE Pic1,PicT,375,230
    GR.MODIFY PicD1,"bitmap",Pic1
    GR.BITMAP.DELETE PicT
    P$[1]=P$[2]
    GR.BITMAP.LOAD PicT,DataDir$+FN$+REPLACE$(FORMAT$("%%%%",PictureNum-1)," ","")+".jpg"
    GR.BITMAP.SCALE Pic2,PicT,375,230
    GR.MODIFY PicD2,"bitmap",Pic2
    GR.BITMAP.DELETE PicT
    P$[2]=P$[3]
    GR.BITMAP.LOAD PicT,DataDir$+FN$+REPLACE$(FORMAT$("%%%%",PictureNum)," ","")+".jpg"
    GR.BITMAP.SCALE Pic3,PicT,375,230
    GR.MODIFY PicD3,"bitmap",Pic3
    GR.BITMAP.DELETE PicT
    P$[3]=REPLACE$(FORMAT$("%%%%",PictureNum)," ","")+" - "+T$
    GR.MODIFY g1,"text",P$[1]
    GR.MODIFY g2,"text",P$[2]
    GR.MODIFY g3,"text",P$[3]
  ENDIF
  GR.RENDER
  IF i=NumPics THEN F_N.BREAK
  IF i=1 THEN
    GR.BITMAP.DELETE Pic1
  ENDIF
  IF i=2 THEN
    GR.BITMAP.DELETE Pic2
  ENDIF
  IF i=3 THEN
    GR.BITMAP.DELETE Pic3
  ENDIF
  IF i>3 THEN
    GR.BITMAP.DELETE Pic1
    GR.BITMAP.DELETE Pic2
    GR.BITMAP.DELETE Pic3
  ENDIF
  OverHead=(CLOCK()-CS)/1000
  IF OverHead<Interval THEN PAUSE (Interval-OverHead)*1000
NEXT i
GR.COLOR 255,0,255,0,1
GR.RECT g,425,420,800,485
GR.COLOR 255,0,0,0,1
GR.TEXT.DRAW g,450,450,"READY"
GR.TEXT.DRAW g,450,480,"Press Back Key to EXIT"
GR.RENDER
Kap=0
DO
UNTIL Kap=1
GR.CLOSE
WAKELOCK 5
EXIT

DrawScreen:
  ! Draw Film strip
  GR.COLOR 255,255,255,255,1
  FOR j=1 TO 2
    FOR k=1 To 24
      GR.POLY hole,L1,25+(k-1)*50,10+(j-1)*305
    NEXT k
  NEXT j
  FOR k=1 TO 3
    GR.COLOR 255,255,255,255,1
    GR.RECT g,(k-1)*375+k*25,70,k*375+k*25,300
    GR.COLOR 120,255,255,255,1
    GR.RECT g,(k-1)*375+k*25,370,k*375+k*25,410
  NEXT k
  ! Show options
  GR.TEXT.SIZE 30
  GR.TEXT.BOLD 1
  GR.COLOR 255,255,255,0,1
  GR.TEXT.DRAW g,50,450,"Start time:"
  GR.TEXT.DRAW g,250,450,StTime$
  GR.TEXT.DRAW g,50,480,"End time:"
  GR.TEXT.DRAW g,250,480,FinalTime$
  GR.TEXT.DRAW g,50,510,"Interval:"
  GR.TEXT.DRAW g,250,510,LEFT$(STR$(Interval),LEN(STR$(Interval))-2)+" s"
  GR.TEXT.DRAW g,50,540,"# Pictures:"
  GR.TEXT.DRAW g,250,540,LEFT$(STR$(NumPics),LEN(STR$(NumPics))-2)
  GR.TEXT.DRAW g,50,570,"Camera: "+Cam$
  GR.TEXT.DRAW g,450,510,"Flash = "+Fl$
  GR.TEXT.DRAW g,450,540,"Sound = "+Snd$
RETURN

GetParms:
GR.FRONT 0
CLS
INPUT "Delay before start (s)",StartDelay,StartDelay
IF StartDelay>9999 THEN StartDelay=9999
INPUT "Time interval (s)",Interval,Interval
IF Interval>9999 THEN Interval=9999
INPUT "Number of pictures",NumPics,NumPics
IF NumPics>9999 THEN NumPics=9999
INPUT "Camera click (0=off 1=on)",SndClick,SndClick
IF SndClick>1 THEN SndClick=1
INPUT "Flash (0=off 1=on)",Flash,Flash
IF Flash>1 THEN Flash=1
INPUT "Camera (0=Back side 1=Front side)",CamType,CamType
IF CamType>1 THEN CamType=1
INPUT "File name prefix",FN$,FN$
GR.FRONT 1
RETURN

ONTIMER:
  GR.COLOR 255,0,0,255,1
  GR.RECT g,425,420,800,460
  GR.COLOR 255,255,255,0,1
  GR.TEXT.DRAW g,450,450,"Start after"+FORMAT$("#####",CntDwn)+" s"
  GR.RENDER
  CntDwn=CntDwn-1
TIMER.RESUME

ONBACKKEY:
  Kap=1
BACK.RESUME
