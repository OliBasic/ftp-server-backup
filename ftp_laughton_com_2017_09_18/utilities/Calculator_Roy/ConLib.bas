Rem Console Library
Rem With RFO Basic!
Rem September / October 2015
Rem Version 1.00
Rem By Roy Shepherd


!!
This is my 'ConLib.bas' include file.
It is a console mode emulator that works in RFO-BASIC graphic mode.
Works with 'typeface 2' monospace as this font as a fixed-pitch font, the letters 
and characters each occupy the same amount of horizontal space

It is important to insert the  'ConLib.bas' include file in the right place, as shown below.

!----------------------------------------------------------------------
di_height = 1120 % set to your own device
di_width = 720
!----------------------------------------------------------------------
gr.open 255,0,0,0 % Black background colour
gr.color 255,255,255,255,1 % White foreground colour
gr.orientation 1  %  1 = Portrait  0 = Landscape.

pause 1000
WakeLock 3 
!----------------------------------------------------------------------
gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
! Setup screen for drawing.
!----------------------------------------------------------------------
gr.bitmap.create screen, di_width, di_height
gr.bitmap.draw    nn, screen, 0, 0
gr.bitmap.drawinto.start screen
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
! Include ConLib.bas after opening graphics 
! and setting up the bitmap screen and before creating any bundles 

include ConLib.bas % include 'ConLib.bas' here
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
! if you are using bundle's put them here, after the ConLib.bas included  file
! 'global' bundle pointer = 2.
!----------------------------------------------------------------------
bundle.create global % bundle pointer = 2.
bundle.put global, "Divice_W", di_width % example 
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
The Comands:
In RFO Basic console mode  a Newline characters (a CR/LF, or carriage return/line feed, combination)
may be inserted into a string with the escape sequence \n.
Print "Jim\nGiudice"
In graphic mode this causes problems, so to enter a Newline characters use /n.
Example: Con_Print("RFO/nBasic")
!----------------------------------------------------------------------
Con_TextSize(status)
!----------------------------------------------------------------------
Sets the text size. It also sets some internal variables, so set this before anything else.
Example: Con_TextSize(30). 
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_ChrPerLine(status)
!----------------------------------------------------------------------
An alternative to Con_TextSize(status) is Con_ChrPerLine(status)
Sets the number of characters per line. It also sets some internal variables, so set this before anything else
Example: Con_ChrPerLine(40). Will set the text size so that it will print 40 characters across the screen.
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_Locate(text_x, text_y) 
!----------------------------------------------------------------------
Move to text_x and  text_y. The next characters will be printed there.
Example: Con_Locate(5, 10)
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_Print(t$)
!----------------------------------------------------------------------
Prints the string 't$' at the current location, this could have been set with Con_Locate(x, y) or at the top of the screen if just started up, or at the top of the screen if it has just been cleared.
If the text gets to the end of a line it carrys on, on the line below. If the text gets to the bottom of the screen, the screen scroll up a line, providing Con_Scroll(s) is set to on.
Example: Con_Print("RFO Basic!")
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_PrintAt(x, y, t$)
!----------------------------------------------------------------------
Prints the string 't$' at the location, x, y
Example: Con_PrintAt(10 ,5, "RFO Basic!")
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_PrintString(n, s$)
!----------------------------------------------------------------------
You can use this function to print repeated copies of a string. It is useful for printing headings or underlinings.
Example: Con_PrintString(4,"-=*=-")
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_PrintSpc(n)
Print n number of spaces in the current background colour.
Example: Con_PrintSpc(10)
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_RenderOff()
!----------------------------------------------------------------------
Turn rendering off to speeds up printing.
Nothing will be seen until rendering is turned on
Example: Con_RenderOff()
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_RenderOn()
!----------------------------------------------------------------------
Turn rendering on. Printing will be seen. 
Example: Con_RenderOn()
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_Render()
!----------------------------------------------------------------------
If rendering is off, this command will render the screen once, but not turn rendering on permanently.
Example: Con_Render()
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_Italics(status)
!----------------------------------------------------------------------
Turn italics printing on or off
Example:  Con_Italics(1), turns on italics. 
Con_Italics(0), turns off italics.
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_Underline(status)
!----------------------------------------------------------------------
Turns on or off Underline printing
Example: Con_Underline(1), turns on underline. 
Con_Underline(0), turns off underline. 
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_Strike(status)
!----------------------------------------------------------------------
Turns on or off strike printing
Example: Con_Strike(1), turns on Strike. 
Con_Strike(0), turns off Strike. 
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_Bold(status)
!----------------------------------------------------------------------
Turns on or off Bold printing
Example: Con_Bold(1), turns on Bold. 
Con_Bold(0), turns off Bold. 
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_Scroll(status)
!----------------------------------------------------------------------
At start up scrolling is on. Use this command to turn scrolling on and off.
You may want to turn scrolling off, if you and going to print on the bottom
right of the screen  and do not want the screen to scroll.
Example: Con_Scroll(1), turns on Scrolling. 
Con_Scroll(0), turns off Scrolling.
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_Cls()
!----------------------------------------------------------------------
Clears the screen to the current background colour.
Example: Con_Cls()
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_Background(colourNumber)
!----------------------------------------------------------------------
Set the background colour to one of the 18 background colours.
After a change of background colour any characters printed will have that colour 
background. The whole of the screen will not change to the new background colour
until the command Con_Cls() is used.
Example: Con_Background(13), (Blue)
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_BackgroundName(ColourName$)
!----------------------------------------------------------------------
As Con_Background(colourNumber), but use one of the colour names.
Example: Con_BackgroundName("Blue")
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_Colour(colourNumber)
!----------------------------------------------------------------------
Set the foreground colour  to one of the 18 foreground colours.
Example: Con_Colour(17), (Gold)
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_ColourName(colourName$)
!----------------------------------------------------------------------
As Con_Colour(colourNumber), but use one of the colour names.
Example: Con_Colour("Red")
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_SaveScreen() 
!----------------------------------------------------------------------
Save the  screen to a file at "../../ConScreens/"
The file name is set with the time functions,  so will be unique.
The file will be saved as a PNG file.
Example: Con_SaveScreen()
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_GetTouchX()
!----------------------------------------------------------------------
Returns X position touch of the screen.
This command is scaled to the text width, so touching column 10,
will return 10.
Example: Con_GetTouchX() 
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_GetTouchY()
!----------------------------------------------------------------------
Returns Y position touch of the screen.
This command is scaled to the text height , so touching row 12,
will return 12.
Example: Con_GetTouchY() 
do  :  x =  Con_GetTouchX() : until x
do  :  y =  Con_GetTouchX() : until y
Con_Print("You touched column " + int$(x) + " and row " + int$(y))
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_GetMaxX()
!----------------------------------------------------------------------
Returns the maxim character per line.
Call this command after setting Con_TextSize() or Con_ChrPerLine()
Example: x = Con_GetMaxX()
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_GetMaxY()
!----------------------------------------------------------------------
Returns the maxim rows from the top to the bottom of the screen.
Call this command after setting Con_TextSize() or Con_ChrPerLine()
Example: y = Con_GetMaxY()
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_GetCurrenX()
!----------------------------------------------------------------------
Returns the current column number.
Example: x = Con_GetCurrentX()
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
Con_GetCurrenY()
!----------------------------------------------------------------------
Returns the current row number.
Example: y = Con_GetCurrentY()
Con_Locate(10, 12) 
column = Con_GetCurrenX()  : row =  Con_GetCurrenY()
column would hold the value 10 and row would hold the value 12
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
 The are 18 colours:
!----------------------------------------------------------------------
 0 = Black, 1 = Grey, 2 = Silver, 3 = White, 4 = Maroon, 5 = Red, 6 = Olive, 7 = Yellow,
 8 = Green, 9 = Lime, 10 = Teal, 11 = Aqua, 12 = Navy, 13 = Blue, 14 = Purple, 
 15 = Fuchsia, 16 = Brown, 17 = Gold.
!----------------------------------------------------------------------
!!

!-------------------------------------------------
! ConLib.bas starts here
!-------------------------------------------------

! works with 'typeface 2' monospace as this fount as a fixed-pitch fount, the letters 
! and characters each occupy the same amount of horizontal space
gr.text.typeface 2 % has to be this type face
gr.text.size 30 % Default size. Use Con_TextSize() to change this
!gr.set.AntiAlias 1

gr.text.height textHeight
gr.text.width textWidth, " "

maxChr_X = floor(di_width / textWidth)
maxChr_Y = floor(di_height / textHeight)

!-------------------------------------------------
! Set global variables.
!-------------------------------------------------
! 'globalVar' bundle pointer = 1.
bundle.create globalVar
bundle.put globalVar, "ScaleX", scale_x
bundle.put globalVar, "ScaleY", scale_y
bundle.put globalVar, "Device_W", di_width
bundle.put globalVar, "Device_H", di_height
bundle.put globalVar, "Screen", screen

bundle.put globalVar, "Pos_X", 0
bundle.put globalVar, "Pos_Y", textHeight + (textHeight / 5)
bundle.put globalVar, "TextHeight", textHeight 
bundle.put globalVar, "TextWidth", textWidth
bundle.put globalVar, "MaxChrX", maxChr_X
bundle.put globalVar, "MaxChrY", maxChr_Y
bundle.put globalVar, "Render", 1
bundle.put globalVar, "TextSize", 30  % Default text size
bundle.put globalVar, "Background", 0 % Default background colour black
bundle.put globalVar, "Colour", 3     % Default foreground colour white
bundle.put globalVar, "Scroll", 1     % Scrolling is on by default

bundle.put globalVar, "Sc_Red", 0 
bundle.put globalVar, "Sc_Green", 0
bundle.put globalVar, "Sc_Blue", 0 % Default scroll colour colour black

! foreground  colour.
bundle.put globalVar, "Red",   255
bundle.put globalVar, "Green", 255
bundle.put globalVar, "Blue",  255 % all 255 white

! background  colour.
bundle.put globalVar, "BG_Red",   0
bundle.put globalVar, "BG_Green", 0
bundle.put globalVar, "BG_Blue",  0 % all 0 black

!-------------------------------------------------
! Start of the functions
!-------------------------------------------------
!-------------------------------------------------
! Move to text_x, text_y.
!-------------------------------------------------
fn.def Con_Locate(text_x, text_y) 
	bundle.get 1, "TextHeight", textHeight 
	bundle.get 1, "TextWidth", textWidth
	textHeight += textHeight / 5
	text_x -= 1
	text_x *= textWidth 
	text_y *= textHeight 
	
	bundle.put 1, "Pos_X", text_x
	bundle.put 1, "Pos_Y", text_y
	
	gr.move null, text_x, text_y
fn.end

!-------------------------------------------------
! Print text.
!-------------------------------------------------
fn.def Con_Print(t$)
	bundle.get 1, "Pos_X", text_x
	bundle.get 1, "Pos_Y", text_y
	bundle.get 1, "TextWidth", textWidth
	bundle.get 1, "TextHeight", textHeight
	bundle.get 1, "Device_W", di_width
	bundle.get 1, "Device_H", di_height
	bundle.get 1, "Render", render
	bundle.get 1, "Scroll", scroll
	! Get foreground and background colour
	bundle.get 1, "Red",   Red
	bundle.get 1, "Green", Green
	bundle.get 1, "Blue",  Blue 

	bundle.get 1, "BG_Red",   BG_Red
	bundle.get 1, "BG_Green", BG_Green
	bundle.get 1, "BG_Blue",  BG_Blue
	
	textHeight += textHeight / 5
	
	! Get width of the text string t$
	for t = 1 to len(t$)
		if mid$(t$, t, 2) = "/n" then
			if scroll & text_y + textHeight >= di_Height then 
				call ScrollScreen() 
				text_x = 1 : t += 1 
			else 
				text_x = 1 : text_y += textHeight : t += 1 % Go down a line
			endif
		else
			gr.color 255,BG_Red, BG_Green, BG_Blue, 1
			gr.text.draw null, text_x, text_y , chr$(9608)
			gr.color 255,Red, Green, Blue, 1
			gr.text.draw null, text_x, text_y, mid$(t$,t,1)
			text_x += textWidth
			
			if text_x >= di_width then
				if scroll & text_y + textHeight + textHeight >= di_Height then 
					call ScrollScreen() 
					text_x = 1 %: t += 1 
				else 
					text_x = 1 : text_y += textHeight %: t += 1 % Go down a line
				endif
			endif
		endif
	next
	bundle.put 1, "Pos_X", text_x
	bundle.put 1, "Pos_Y", text_y
	if render then gr.render
fn.end

!-------------------------------------------------
! Scroll the screen up one line.
! Lines that go off the top of the screen are lost
!-------------------------------------------------
fn.def ScrollScreen()
	bundle.get 1, "Device_W", di_width 
	bundle.get 1, "Device_H", di_height
	bundle.get 1, "Screen", screen
	bundle.get 1, "TextHeight", textHeight 
	
	bundle.get 1, "Sc_Red", Sc_Red
	bundle.get 1, "Sc_Green", Sc_Green
	bundle.get 1, "Sc_Blue", Sc_Blue
	
	bundle.get 1, "Screen", screen
	!textHeight += textHeight / 5
	textHeight += 4
	gr.bitmap.crop savedScreens,screen,0,0,di_width,di_height
	gr.bitmap.draw obj, savedScreens, 0, - textHeight + 3
	gr.bitmap.delete savedScreens
	
	gr.color 255,Sc_Red, Sc_Green, Sc_Blue, 1
	gr.rect null,0, di_height - textHeight, di_width, di_height
	!if gr.render
fn.end

!-------------------------------------------------
! Print text at x, y
!-------------------------------------------------
fn.def Con_PrintAt(x, y, t$)
	Con_Locate(x, y)
	Con_Print(t$)
fn.end

!-------------------------------------------------
! Print n number of spaces in the current background colour.
!-------------------------------------------------
fn.def Con_PrintSpc(n)
	for x = 1 to n 
		Con_Print(" ")
	next
fn.end

!-------------------------------------------------
! Print string S$ n times.
!-------------------------------------------------
fn.def Con_PrintString(n, s$) 
	for x = 1 to n 
		Con_Print(s$)
	next
fn.end

!-------------------------------------------------
! Turn Italics on or off. status = 0 italics off. 
! status = 1 italics on.
!-------------------------------------------------
fn.def Con_Italics(status)
	gr.text.skew status
fn.end

!-------------------------------------------------
! Turn underline on or off. status = 0 underline off. 
! status = 1 underline on.
!-------------------------------------------------
fn.def Con_Underline(status)
	gr.text.underline status
fn.end

!-------------------------------------------------
! Turn strike on or off. status = 0 strike off. 
! status = 1 strike on.
!-------------------------------------------------
fn.def Con_Strike(status)
	gr.text.strike status
fn.end

!-------------------------------------------------
! Turn bold on or off. status = 0 bold off. 
! status = 1 bold on.
!-------------------------------------------------
fn.def Con_Bold(status)
	gr.text.bold status
fn.end

!-------------------------------------------------
! Turn scrolling on and off. 
! status = 0 turn scrolling off and status = 1 for scrolling on.
!-------------------------------------------------
fn.def Con_Scroll(status)
	bundle.put 1, "Scroll", status
fn.end

!-------------------------------------------------
! Set the number of characters per line
!-------------------------------------------------
fn.def Con_ChrPerLine(status) 
	bundle.get 1, "Device_W", di_width
	target = di_width / status
	textSize = target
	do
		textSize ++
		gr.text.size textSize
		gr.text.width textWidth," "
	until textWidth >= target
	Con_TextSize(textSize)
fn.end

!-------------------------------------------------
! Set the text size 
!-------------------------------------------------
fn.def Con_TextSize(status)
	gr.text.size status
	! Set text height and width
	gr.text.height textHeight
	!textHeight += 4
	gr.text.width textWidth, " "
	! Get maxim text charaters across and down the screen
	bundle.get 1, "Device_W", di_width 
	bundle.get 1, "Device_H", di_height
	maxChr_X = floor(di_width / textWidth)
	maxChr_Y = floor(di_height / textHeight) 
	
	! Put information in the globalVar bundle
	bundle.put 1, "Pos_Y", textHeight
	bundle.put 1, "TextHeight", textHeight 
	bundle.put 1, "TextWidth", textWidth
	bundle.put 1, "MaxChrX", maxChr_X
	bundle.put 1, "MaxChrY", maxChr_Y
	bundle.put 1, "TextSize", status
fn.end

!-------------------------------------------------
! Clear the screen to the current background colour
! and move to 1,1 'top left of the screen'
!-------------------------------------------------
fn.def Con_Cls()
	bundle.get 1, "Device_W", di_width
	bundle.get 1, "Device_H", di_height
	! Get background colour
	bundle.get 1, "BG_Red",   BG_Red
	bundle.get 1, "BG_Green", BG_Green
	bundle.get 1, "BG_Blue",  BG_Blue
	! Set scroll colour
	bundle.put 1, "Sc_Red", BG_Red
	bundle.put 1, "Sc_Green", BG_Green
	bundle.put 1, "Sc_Blue", BG_Blue
	
	! Set and clear screen to background colour
	gr.color 255,BG_Red, BG_Green, BG_Blue, 1
	gr.rect null, 0, 0, di_width, di_height 
	! Get foreground  colour.
	bundle.get 1, "Red",   Red
	bundle.get 1, "Green", Green
	bundle.get 1, "Blue",  Blue
	! Set colour to foreground  colour.
	gr.color 255,Red, Green, Blue, 1
	Con_Locate(1, 1) : Con_RenderOn()
fn.end

!-------------------------------------------------
! Set the background colour
!-------------------------------------------------
fn.def Con_Background(colourNumber)
	call Colour(0, colourNumber)
	bundle.put 1, "Background", colourNumber
fn.end 

!-------------------------------------------------
! Set the foreground colour
!-------------------------------------------------
fn.def Con_Colour(colourNumber)
	call Colour(1, colourNumber)
	bundle.put 1, "Colour", colourNumber
fn.end 

!-------------------------------------------------
! Set the foreground colour by name
!-------------------------------------------------
fn.def Con_ColourName(colourName$)
	array.load c$[],"black","grey","silver","white","maroon","red","olive","yellow","green", ~
					"lime","teal","aqua","navy","blue","purple","fuchsia","brown","gold"
	colour = - 1 : colourName$ = lower$(colourName$)
	for c = 1 to 18 
		if c$[c] = colourName$ then colour = c - 1 : f_n.break
	next
	array.delete c$[]
	if colour > -1 then 
		Con_Colour(colour) 
	else 
		dialog.message colourName$ + " Is not a Colour",,ok,"OK"
	endif
fn.end

!-------------------------------------------------
! Set the background colour by name
!-------------------------------------------------
fn.def Con_BackgroundName(colourName$)
	array.load c$[],"black","grey","silver","white","maroon","red","olive","yellow","green", ~
					"lime","teal","aqua","navy","blue","purple","fuchsia","brown","gold"
	colour = - 1 : colourName$ = lower$(colourName$)
	for c = 1 to 18 
		if c$[c] = colourName$ then colour = c - 1 : f_n.break
	next
	array.delete c$[]
	if colour > -1 then 
		Con_Background(colour) 
	else 
		dialog.message colourName$ + " Is not a Colour",,ok,"OK"
	endif
fn.end

!-------------------------------------------------
! Return the current x position touched
!-------------------------------------------------
fn.def Con_GetTouchX()
	flag = 0
	gr.touch touched, tx, ty 
	if touched then 
		bundle.get 1, "ScaleX", scale_x
		bundle.get 1, "TextWidth", textWidth
		tx /= scale_x : tx /= textWidth
		flag = floor(tx + 1)
	endif
	fn.rtn flag
fn.end

!-------------------------------------------------
! Return the current y position touched
!-------------------------------------------------
fn.def Con_GetTouchY()
	flag = 0
	gr.touch touched, tx, ty 
	if touched then 
		bundle.get 1, "ScaleY", scale_y
		bundle.get 1, "TextHeight", textHeight
		textHeight += textHeight / 5
		ty /= scale_y : ty /= textHeight
		flag = ty + 1
	endif
	fn.rtn floor(flag)
fn.end

!-------------------------------------------------
! Return the maxim charaters across the screen
!-------------------------------------------------
fn.def Con_GetMaxX()
	bundle.get 1, "MaxChrX", maxChr_X
	fn.rtn maxChr_X
fn.end

!-------------------------------------------------
! Return the maxim charaters down the screen
!-------------------------------------------------
fn.def Con_GetMaxY()
	bundle.get 1, "TextHeight", textHeight
	bundle.get 1, "Device_H", di_height
	textHeight += textHeight / 5
	maxChr_Y = di_Height / textHeight
	fn.rtn floor(maxChr_Y)
fn.end

!-------------------------------------------------
! Return the curren position of X
!-------------------------------------------------
fn.def Con_GetCurrentX()
	bundle.get 1, "TextWidth", textWidth
	bundle.get 1, "Pos_X", text_x
	currentX= text_x / textWidth
	fn.rtn floor(currentX)
fn.end

!-------------------------------------------------
! Return the curren position of Y
!-------------------------------------------------
fn.def Con_GetCurrentY()
	bundle.get 1, "TextHeight", textHeight
	textHeight += textHeight / 5
	bundle.get 1, "Pos_Y", text_y
	currentY = text_y / textHeight
	fn.rtn floor(currentY)
fn.end

!-------------------------------------------------
! Return the current text size
!-------------------------------------------------
fn.def Con_GetTextSize()
	bundle.get 1, "TextSize", textSize 
	fn.rtn textSize
fn.end

!-------------------------------------------------
! Return the current background colour
!-------------------------------------------------
fn.def Con_GetBackground()
	bundle.get 1, "Background", backColour
	fn.rtn backColour
fn.end

!-------------------------------------------------
! Return the current colour
!-------------------------------------------------
fn.def Con_GetColour()
	bundle.get 1, "Colour", colour
	fn.rtn colour
fn.end

!-------------------------------------------------
! Set the background and foreground colour
!-------------------------------------------------
fn.def Colour(backORfore, c)
	c = Mod(c,18) % if c  = 18 c = 0 if c = 19 c = 1 and so on
	if c = 0 : Red = 0 : Green = 0 : Blue = 0                % Black
		elseif c = 1 : Red = 128 : Green = 128 : Blue = 128  % Gray 
		elseif c = 2 : Red = 192 : Green = 192 : Blue = 192  % Silver
		elseif c = 3 : Red = 255 : Green = 255 : Blue = 255  % White
		elseif c = 4 : Red = 128 : Green = 0 : Blue = 0      % Maroon 
		elseif c = 5 : Red = 255 : Green = 0 : Blue = 0      % Red
		elseif c = 6 : Red = 128 : Green = 128 : Blue = 0    % Olive
		elseif c = 7 : Red = 255 : Green = 255 : Blue = 0    % Yellow
		elseif c = 8 : Red = 0 : Green = 128 : Blue = 0      % Green
		elseif c = 9 : Red = 0 : Green = 255 : Blue = 0      % Lime
		elseif c = 10 : Red = 0 : Green = 128 : Blue = 128   % Teal
		elseif c = 11 : Red = 0 : Green = 255 : Blue = 255   % Aqua 
		elseif c = 12 : Red = 0 : Green = 0 : Blue = 128     % Navy
		elseif c = 13 : Red = 0 : Green = 0 : Blue = 255     % Blue
		elseif c = 14 : Red = 128 : Green = 0 : Blue = 128   % Purple 
		elseif c = 15 : Red = 255 : Green = 0 : Blue = 255   % Fuchsia  
		elseif c = 16 : Red = 199 : Green = 97 : Blue = 20   % Brown 
		elseif c = 17 : Red = 218 : Green = 165 : Blue = 32  % Gold 
	endif
	!if backORfore = 1 set foreground if backORfore = 0 set background
	if backORfore then
		bundle.put 1, "Red",   Red
		bundle.put 1, "Green", Green
		bundle.put 1, "Blue",  Blue 
	else
		bundle.put 1, "BG_Red",   Red
		bundle.put 1, "BG_Green", Green
		bundle.put 1, "BG_Blue",  Blue 
	endif
fn.end

!-------------------------------------------------
! Save the screen to a file at "../../ConScreens/"
! The file name is set with the time functions, 
! so will be unique.
!-------------------------------------------------
fn.def Con_SaveScreen() 
	bundle.get 1, "Screen", screen
	file.exists pathPresent,"../../ConScreens/"
	if ! pathPresent then File.mkdir"../../ConScreens/"	
	Path$="../../ConScreens/"
	myscreen$ = mid$(str$(TIME()),3) + ".png"
	gr.bitmap.save screen, Path$ + myscreen$
	popup "Screen Saved"
fn.end

!-------------------------------------------------
! Turn rendering on; printing will be seen.
!-------------------------------------------------
fn.def Con_RenderOn()
	bundle.put 1, "Render", 1
	gr.render
fn.end

!-------------------------------------------------
! Turn rendering off; this will speed up printing.
! Printing will not be seen.
!-------------------------------------------------
fn.def Con_RenderOff()
	bundle.put 1, "Render", 0
fn.end

!-------------------------------------------------
! Render to screen all printing that as been done with rendering off.
!-------------------------------------------------
fn.def Con_Render()
	gr.render
fn.end


