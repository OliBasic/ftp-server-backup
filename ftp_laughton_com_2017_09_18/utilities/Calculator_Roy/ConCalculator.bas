Rem Text Mode Calculator 
Rem Using ConLib.bas
Rem With RFO Basic!
Rem October 2015
Rem Version 1.00
Rem By Roy Shepherd

di_height = 1120 % set to my Device
di_width = 720

gr.open 255,0,0,0 % Black background colour
gr.color 255,255,255,255,1 % White foreground colour
gr.orientation 1
pause 1000
WakeLock 3 

gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y

!-------------------------------------------------
! Setup screen for drawing.
!-------------------------------------------------
gr.bitmap.create screen, di_width, di_height
gr.bitmap.draw    nn, screen, 0, 0
gr.bitmap.drawinto.start screen

!-------------------------------------------------
! include ConLib.bas after opening graphics 
! and setting up the bitmap screen and before creating any bundles 
!-------------------------------------------------
include ConLib.bas % include 'ConLib.bas' here

!-------------------------------------------------
! Put bundle's here, after the included ConLib.bas
! 'global' bundle pointer = 2.
!-------------------------------------------------
bundle.create global % bundle pointer = 2.
bundle.put global, "Divice_W", di_width

gosub Functions % Let basic see the functions before they are called
!-------------------------------------------------
! The 18 colours are:
! 0 = Black, 1 = Gray, 2 = Silver, 3 = White, 4 = Maroon, 5 = Red, 6 = Olive, 7 = Yellow,
! 8 = Green, 9 = Lime, 10 = Teal, 11 = Aqua, 12 = Navy, 13 = Blue, 14 = Purple, 
! 15 = Fuchsia, 16 = Brown, 17 = Gold.
!-------------------------------------------------

call PrintCalculator()

do : pause 1 : until 0

onBackKey:
	dialog.message "Exit Calculator",,yn,"Yes","No"
	if yn = 1 then end 
Back.resume

onError:
	!e$ = getError$()
	Con_Locate(3,3) : Con_PrintSpc(14) 
	Con_Locate(3,3) : Con_Print("Error")
	pause 2000
	call GetCalculatorInput()
end

Functions:
!-------------------------------------------------
! Print the calculator
!-------------------------------------------------
fn.def PrintCalculator()
	sx = 3 : sy = 5 : d$= chr$(247) : l$ = chr$(9567) : r$ = chr$(9570)
	Con_ChrPerLine(20) : Con_BackgroundName("Navy") : Con_ColourName("White") : Con_Cls() 
	Con_RenderOff() : Con_ColourName("Red") : call Centre(" Calculator")
	call Box(2,2,19,9,"",0,2) 
	Con_PrintAt(2,4,l$) : Con_PrintString(16,chr$(9472)) : Con_print(r$)
	Con_ColourName("White")
	Con_BackgroundName("Grey") 
	Con_PrintAt(sx, sy, " 7  8  9  ") : Con_BackgroundName("Black") : Con_Print(" " + d$ + "  ( ")
	Con_BackgroundName("Grey")
	Con_PrintAt(sx, sy + 1, " 4  5  6  ") : Con_BackgroundName("Black") : Con_Print(" x  ) ")
	Con_BackgroundName("Grey")
	Con_PrintAt(sx, sy + 2, " 1  2  3  ") : Con_BackgroundName("Black"): Con_Print(" -  ^ ")
	Con_BackgroundName("Grey")
	Con_PrintAt(sx, sy + 3, "   0   .  ") : Con_BackgroundName("Black") : Con_Print(" +  = ")
	Con_BackgroundName("silver") : Con_ColourName("Black") : Con_PrintAt(17,3,chr$(8592))
	Con_BackgroundName("White") : Con_Locate(1,12) 
	Con_PrintSpc(Con_GetMaxX()) : Con_Locate(1,12) : call Centre(" Exit") : Con_RenderOn()
	Con_BackgroundName("silver") : Con_ColourName("Black")
	call GetCalculatorInput()
fn.end

!-------------------------------------------------
! Get the uses input and calculate it
!-------------------------------------------------
fn.def GetCalculatorInput()
	userInput$="" : sym$ = ""
	symbol$ = chr$(247) + "(x)-^+=" : startFlag = 0
	Con_Locate(3,3) : Con_PrintSpc(14) % if an error, remove it and star over
	do
		flag = 0
	! Get user input
		do 
			do : tx = Con_GetTouchX() : until tx 
			do : ty = Con_GetTouchY() : until ty
		until tx & ty 
		do : gr.touch t, nullx, nully : pause 1 : until ! t 
		if ty = 12 then 
			dialog.message "Exit Calculator",,yn,"Yes","No"
			if yn = 1 then end 
		endif
	! As '1' to '9' been tapped
		for y = 5 to 7
			cal = 0
			if y = 5 then cal = 6
			if y = 6 then cal = 3
			for x = 4 to 10 step 3
				cal +=1
				if ty = y then
					if tx = x | tx - 1 = x | tx + 1 = x then 
						! if after a calculation the user taps a number then set userInput$ = "" 
						if startFlag then 
							userInput$ = "" 
							Con_RenderOff() : Con_Locate(3,3) : Con_PrintSpc(14) 
							Con_PrintAt(3,3,userInput$) : Con_RenderOn()
							startFlag = 0
						endif
						userInput$ += int$(cal) : flag = 1
					endif
				endif
				if flag then f_n.break
			next
			if flag then f_n.break
		next
	! As '0' or '.' been tapped
		if ! flag & ty = 8 then 
			if tx > 2 & tx < 9 then 
				If startFlag then 
					userInput$ = "" 
					Con_RenderOff() : Con_Locate(3,3) : Con_PrintSpc(14) 
					Con_PrintAt(3,3,userInput$) : Con_RenderOn()
					startFlag = 0
				endif
				userInput$ += "0" : flag = 1
			endif
			
			if tx > 8 & tx < 12 then 
				if startFlag then 
					userInput$ = "" 
					Con_RenderOff() : Con_Locate(3,3) : Con_PrintSpc(14) 
					Con_PrintAt(3,3,userInput$) : Con_RenderOn()
					startFlag = 0
				endif
				userInput$ += "." : flag = 1
			endif
		endif
	! As a symbol been taped  '/(*)-^+='
		if ! flag then
			for y = 5 to 8
				if y = 8 : cal = 6
					elseif y = 7 : cal = 4 
					elseif y = 6 : cal = 2
					else : cal = 0
				endif
				
				for x = 14 to 17 step 3 
					cal += 1
					if ty = y then
						if tx = x | tx - 1 = x | tx + 1 = x then 
							sym$ =  mid$(symbol$, cal, 1) : flag = 1
							if startFlag & sym$ ="(" | startFlag & sym$ = ")" then 
								userInput$ = "" 
								Con_RenderOff() : Con_Locate(3,3) : Con_PrintSpc(14) 
								Con_PrintAt(3,3,userInput$) : Con_RenderOn()
								startFlag = 0
							endif
							if sym$ <> "=" then  userInput$ += sym$
							startFlag = 0
						endif
					endif
					if flag then f_n.break
				next
				if flag then f_n.break
			next
		endif
	! As Back Arrow been taped
		if ty = 3 & ! flag then 
			if tx > 16 & tx < 20 then 
				userInput$ = left$(userInput$, len(userInput$) - 1) : flag = 1
				Con_RenderOff() : Con_Locate(3,3) : Con_PrintSpc(13) 
				Con_PrintAt(3,3,userInput$): Con_RenderOn()
			endif
		endif
	! if length of userInput > 14 reduce to 14. Print userInput$
		if flag & sym$ <> "=" then 
			if len(userInput$) > 13 then userInput$ = left$(userInput$, 13)
			Con_PrintAt(3,3,userInput$)
		endif 
	! As '=' symbol been tapped, if so do the calculation and reduce numbers after decimal point to three
		if flag & sym$ = "=" then 
			!con_SaveScreen()
			userInput$ = replace$( userInput$, chr$(247), "/")
			userInput$ = replace$( userInput$, "x", "*")
			userInput = Parse(userInput$)
			
			if userInput = int(userInput) then
				userInput$ = int$(userInput)
			else
				userInput$ = str$(userInput)
			endif
			! If number to big then print Infinity
			if is_in("E",userInput$) then userInput$ = "Infinity"
			! Reduce numbers after decimal point to three
			dPoint = is_in(".",userInput$)
			if dPoint then userInput$ = left$(userInput$, dPoint + 3)
			if len(userInput$) > 12 then userInput$ = left$(userInput$, 12)
			Con_RenderOff() : Con_Locate(4,3) : Con_PrintSpc(13) 
			Con_PrintAt(3,3,userInput$) : Con_RenderOn()
			sym$="" : startFlag = 1
		endif
	until 0
fn.end

!-------------------------------------------------
! Print string t$ at the centre of the screen
!-------------------------------------------------
fn.def Centre(t$)
	Con_PrintAt(round(Con_GetMaxX() / 2 - len(t$) / 2 ,, "U"), Con_GetCurrentY(), t$)
fn.end 

!-------------------------------------------------
! Print box with heading, background and foreground colours.
!-------------------------------------------------
fn.def Box(boxUperLeft, top, boxBottomRight, bottom, heading$, colour, BGcolour)
	
	topLeft$ = chr$(9556): topRight$ = chr$(9559) : bottomLeft$ = chr$(9562) : bottomRight$ = chr$(9565)
	LeftRightBar$ = chr$(9553) : topBottomBar$ = chr$(9552) : headingLeftBar$ = chr$(9569) : headingRightBar$ = chr$(9566)
	restoreColour = Con_GetColour() : restoreBGcolour = Con_GetBackground()
	Con_Colour(colour) : Con_Background(BGcolour)
	!  Background colour for the box
	for x = boxUperLeft + 1 to boxBottomRight - 1 
		for y = top + 1 to bottom - 1
			Con_PrintAt(x, y, chr$(32)) 
		next
	next
	! Print each corner 
	!Con_Colour(colour) % Yellow box
	Con_PrintAt(boxUperLeft, top, topLeft$) : Con_PrintAt(boxBottomRight, bottom, bottomRight$)
	Con_PrintAt(boxUperLeft, bottom, bottomLeft$) : Con_PrintAt(boxBottomRight, top, topRight$)
	! Print left and right sides
	for y = top + 1 to bottom - 1
		Con_PrintAt(boxUperLeft, y, LeftRightBar$)
		Con_PrintAt(boxBottomRight, y, LeftRightBar$)
	next
	! Print top and bottom
	for x = boxUperLeft + 1 to boxBottomRight - 1
		Con_PrintAt(x, top, topBottomBar$)
		Con_PrintAt(x, bottom, topBottomBar$)
	next
	! Print the heading if heading$ is not ""
	if heading$ <> "" then 
		s$ = headingLeftBar$ + heading$ + headingRightBar$ 
		headingLenth = len(s$) / 2
		x = (boxBottomRight + boxUperLeft) / 2
		x = x - headingLenth
		Con_PrintAt(x, top, s$)
	endif
fn.end

!-------------------------------------------------
! Start of the functions to evaluate the string to be calculated 
! Thanks to Brochi, the one who converted the VB code to BASIC!
! Thanks to Emile, for pointing me in the right direction
!-------------------------------------------------
fn.def Parse(Exp$)		
	
	 bDebug=0		
	 										
	Exp$ = RSpaces$(Exp$) 
  if bDebug then print " RSpaces ", exp$	
  													       																
	Exp$ = RPlusMinus$(Exp$)  
	 if bDebug then print " RPlusMinus ", exp$   																
	               																
	If is_in(  "[",Exp$) > 0 Then Exp$ = CSTA$(Exp$) 
	  if bDebug then print " CSTA ", exp$ 
	             														             																
	If is_in( "(",Exp$) > 0 Then Exp$ = Parentheses$(Exp$)  
	 if bDebug then print " Parentheses ", exp$  
	   																
	If is_in(  "^",Exp$) > 0 Then Exp$ = Power$(Exp$)  
	 if bDebug then print " Power ", exp$  
	         																
	If is_in( "*",Exp$) > 0 Then Exp$ = Mul$(Exp$)		
	 if bDebug then print " Mul ", exp$ 
	 														
	If is_in( "/",Exp$) > 0 Then Exp$ = Div$(Exp$)	
	 if bDebug then print " Div ", exp$ 	
 																
	If is_in( "+",Exp$) > 0 Then Exp$ = Add$(Exp$)		
	 if bDebug then print " Add ", exp$ 
	 													
	If is_in( "-",Exp$,2) > 0 Then Exp$ = Subt$(Exp$) 
	 if bDebug then print " Subt ", exp$ 	
	
	fn.rtn  val(Exp$)																
fn.end																	
																																	
fn.def Mul$(Exp$)																	
	Exp$ = RPlusMinus$(Exp$)  																
	Place = is_in("*",Exp$)																
	If Place > 0 Then																
	    Y = 0																
	    X = Place																
	    Do    																
	        Y = Y + 1																
	        X = X - 1																
	        If X = 0 Then D_u.break    																
	        If (Mid$(Exp$, X, 1) <> "+") & (Mid$(Exp$, X, 1) <> "-" | Y = 1) & (Mid$(Exp$, X, 1) <> "/") Then																
	            Before$ = Mid$(Exp$, X, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "+") | (Mid$(Exp$, X, 1) = "-" & Y <> 1) | (Mid$(Exp$, X, 1) = "/")   																
	    BeforeStr$ = Mid$(Exp$, 1, X)																
	    Y = 0																
	    X = Place																
	    Do																
	        Y = Y + 1																
	        X = X + 1																
	        If (Mid$(Exp$, X, 1) <> "+") & (Mid$(Exp$, X, 1) <> "-" | Y = 1) & (Mid$(Exp$, X, 1) <> "*") & (Mid$(Exp$, X, 1) <> "/") Then																
	            After$ = Mid$(Exp$, Place + 1, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "+") | (Mid$(Exp$, X, 1) = "-" & Y <> 1) | (Mid$(Exp$, X, 1) = "*") | (Mid$(Exp$, X, 1) = "/") | (X > Len(Exp$))																
	    NewExp$ = BeforeStr$ + str$( val(Before$) * val(After$) ) + Mid$(Exp$, X)																
	    If is_in(  "*", NewExp$ ) > 0 Then																
	        NewExp$ = Mul$(NewExp$)   																
	    End If																
	fn.rtn NewExp$																
	End If																
	fn.rtn Exp$																
fn.end																	
																	
fn.def Add$(Exp$)
!print exp $																
	Exp$ = RPlusMinus$(Exp$)																
	Place = is_in( "+",Exp$)																
	If Place > 0 Then																
	    Y = 0																
	    X = Place																
	    Do      %loop backwards on the string to find out the number Before$ the sign																
	        Y = Y + 1																
	        X = X - 1																
	        If X = 0 Then D_u.break      %incase we got to the start of the string																
	        If (Mid$(Exp$, X, 1) <> "-" | X = 1) & (Mid$(Exp$, X, 1) <> "*") & (Mid$(Exp$, X, 1) <> "/") Then																
	            Before$ = Mid$(Exp$, X, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "*") | (Mid$(Exp$, X, 1) = "-" & X <> 1) | (Mid$(Exp$, X, 1) = "/")																
	    BeforeStr$ = Mid$(Exp$, 1, X)	
	    if BeforeStr$<>""
	     if right $(BeforeStr$, 1 )="-"
	       BeforeStr$ = Mid$(Exp$, 1, X-1)+"+"
	       Before$= "-"+Before$
	     endif
	    endif
	   ! print BeforeStr$, before $	, x, exp $														
	    Y = 0																
	    X = Place																
	    Do																
	        Y = Y + 1																
	        X = X + 1																
	        If (Mid$(Exp$, X, 1) <> "+") & (Mid$(Exp$, X, 1) <> "-") & (Mid$(Exp$, X, 1) <> "*") & (Mid$(Exp$, X, 1) <> "/") Then																
	            After$ = Mid$(Exp$, Place + 1, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "+") | (Mid$(Exp$, X, 1) = "-") | (Mid$(Exp$, X, 1) = "*") | (Mid$(Exp$, X, 1) = "/") | (X > Len(Exp$))																
	    NewExp$ = BeforeStr$ + str$( val(Before$) + val(After$) ) + Mid$(Exp$, X)																
	    If is_in(  "+",NewExp$) > 0 Then																
	        NewExp$ = Add$(NewExp$)      %Recourse incase there are +     %s Left$																
	    End If																
	fn.rtn NewExp$																
	End If																
	fn.rtn Exp$																
fn.end																	
																	
fn.def Subt$(Exp$)																	
	Exp$ = RPlusMinus$(Exp$)	
	!print "--------"	
	!print exp $														
	Place = is_in( "-",Exp$)																
	If Place = 1 Then Place =is_in( "-",Exp$,2)																
	If Place > 1 Then																
	    Y = 0																
	    X = Place																
	    Do      %loop backwards on the string to find out the number Before$ the sign																
	        Y = Y + 1																
	        X = X - 1																
	        If X = 0 Then D_u.break      %incase we got to the start of the string																
	        If (Mid$(Exp$, X, 1) <> "+") & (Mid$(Exp$, X, 1) <> "*") & (Mid$(Exp$, X, 1) <> "/") Then																
	            Before$ = Mid$(Exp$, X, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "*") | (Mid$(Exp$, X, 1) = "+") | (Mid$(Exp$, X, 1) = "/")																
	    BeforeStr$ = Mid$(Exp$, 1, X)																
	    Y = 0																
	    X = Place																
	    Do																
	        Y = Y + 1																
	        X = X + 1																
	        If (Mid$(Exp$, X, 1) <> "+") & (Mid$(Exp$, X, 1) <> "-") & (Mid$(Exp$, X, 1) <> "*") & (Mid$(Exp$, X, 1) <> "/") Then																
	            After$ = Mid$(Exp$, Place + 1, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "+") | (Mid$(Exp$, X, 1) = "-") | (Mid$(Exp$, X, 1) = "*") | (Mid$(Exp$, X, 1) = "/") | (X > Len(Exp$))																
	    NewExp$ = BeforeStr$ + str$( val(Before$) - val(After$) ) + Mid$(Exp$, X)																
	    If is_in(  "-",NewExp$) > 0 Then																
	        NewExp$ = Subt$(NewExp$)      %Recourse incase there are -     %s Left$																
	    End If																
	    fn.rtn NewExp$																
	End If																
	   fn.rtn Exp$																
fn.end																	
																	
fn.def Parentheses$(Exp$)																	
	Exp$ = RPlusMinus$(Exp$)														
	Place = is_in( ")",Exp$)																
	If Place > 0 Then																
	    Y = 0																
	    X = Place
	    Do      %loop to find the inside of the parentheses																
	        Y = Y + 1																
	        X = X - 1																
	        If X = 0 Then D_u.break      %incase we got to the start of the string																
	        If Mid$(Exp$, X, 1) <> "(" Then																
	            MiddleStr$ = Mid$(Exp$, X, Y)																
	        End If																
	    Until Mid$(Exp$, X, 1) = "("																
	    BeforeStr$ = ""      %if it     %s the beginning of the string - there is nothing Before$.																
	    If X > 0 Then BeforeStr$ = Mid$(Exp$, 1, X - 1)															
	    NewExp$ = BeforeStr$ + str$(Parse(MiddleStr$) )+ Mid$(Exp$, Place + 1)																
	    If is_in(")",Exp$) > 0 Then																
	        NewExp$ = Parentheses$(NewExp$)      %Recourse incase there are more Parentheses$																
	    End If																
	   fn.rtn NewExp$																
	End If																
	   fn.rtn Exp$																
fn.end																	
																	
fn.def Div$(Exp$)																	
	!On Error Resume Next																
	Exp$ = RPlusMinus$(Exp$)																
	Place = is_in( "/",Exp$)																
	If Place > 0 Then																
	    Y = 0																
	    X = Place																
	    Do      %loop backwards on the string to find out the number Before$ the sign																
	        Y = Y + 1																
	        X = X - 1																
	        If X = 0 Then D_u.break      %incase we got to the start of the string																
	        If (Mid$(Exp$, X, 1) <> "+") & (Mid$(Exp$, X, 1) <> "-" | Y = 1) & (Mid$(Exp$, X, 1) <> "*") Then																
	            Before$ = Mid$(Exp$, X, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "+") | (Mid$(Exp$, X, 1) = "-" & Y <> 1) | (Mid$(Exp$, X, 1) = "*")																
	    BeforeStr$ = Mid$(Exp$, 1, X)																
	    Y = 0																
	    X = Place																
	    Do																
	        Y = Y + 1																
	        X = X + 1																
	        If (Mid$(Exp$, X, 1) <> "+") & (Mid$(Exp$, X, 1) <> "-" | Y = 1) & (Mid$(Exp$, X, 1) <> "*") & (Mid$(Exp$, X, 1) <> "/") Then																
	            After$ = Mid$(Exp$, Place + 1, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "+") | (Mid$(Exp$, X, 1) = "-" & Y <> 1) | (Mid$(Exp$, X, 1) = "*") | (Mid$(Exp$, X, 1) = "/") | (X > Len(Exp$))																
	    NewExp$ = BeforeStr$ + str$( val(Before$) / val(After$) ) + Mid$(Exp$, X)	
	    
!!															
	    If Err.Number = 11 Then																
	        Div$ = "Impossible"																
	        Exit Function																
	    End If	
!!
	    															
	    If is_in( "/",NewExp$) > 0 Then																
	        NewExp$ = Div$(NewExp$)      %Recourse incase there are /     %s Left$																
	    End If																
	fn.rtn NewExp$																
	End If																
	fn.rtn Exp$																
fn.end																	
																	
fn.def Power$(Exp$)																	
	Exp$ = RPlusMinus$(Exp$)																
	Place = is_in( "^",Exp$)																
	If Place > 0 Then																
	    Y = 0																
	    X = Place																
	    Do      %loop backwards on the string to find out the number Before$ the sign																
	        Y = Y + 1																
	        X = X - 1																
	        If X = 0 Then D_u.break      %incase we got to the start of the string																
	        If (Mid$(Exp$, X, 1) <> "+") & (Mid$(Exp$, X, 1) <> "-" | X = 1) & (Mid$(Exp$, X, 1) <> "*") & (Mid$(Exp$, X, 1) <> "/") Then																
	            Before$ = Mid$(Exp$, X, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "+") | (Mid$(Exp$, X, 1) = "-" & X <> 1) | (Mid$(Exp$, X, 1) = "*") | (Mid$(Exp$, X, 1) = "/")																
	    BeforeStr$ = Mid$(Exp$, 1, X)																
	    Y = 0																
	    X = Place																
	    Do																
	        Y = Y + 1																
	        X = X + 1																
	        If (Mid$(Exp$, X, 1) <> "+") & (Mid$(Exp$, X, 1) <> "-") & (Mid$(Exp$, X, 1) <> "*") & (Mid$(Exp$, X, 1) <> "/") Then																
	            After$ = Mid$(Exp$, Place + 1, Y)																
	        End If																
	    Until (Mid$(Exp$, X, 1) = "+") | (Mid$(Exp$, X, 1) = "-") | (Mid$(Exp$, X, 1) = "*") | (Mid$(Exp$, X, 1) = "/") | (X > Len(Exp$))																
	    NewExp$ = BeforeStr$ + str$( val(Before$) ^ val(After$) ) + Mid$(Exp$, X)																
	    If is_in(  "^",NewExp$) > 0 Then																
	        NewExp$ = Power$(NewExp$)      %Recourse incase there are ^     %s Left$																
	    End If																
	fn.rtn NewExp$																
	End If																
	fn.rtn Exp$																
fn.end																	
																	
fn.def RPlusMinus$(Exp$)																	
	Place = is_in( "+-",Exp$)	
	out $=""															
	If Place > 0 Then																
	    NewExp$ = Left$(Exp$, Place - 1) + "-" + Right$(Exp$, Len(Exp$) - Place - 1)																
	    If is_in(  "+-",NewExp$) > 0 Then																
	        NewExp$ = RPlusMinus$(NewExp$)																
	    End If																
	    out $=  NewExp$																
	End If																
	Place = is_in(  "--",NewExp$)																
	If Place > 0 Then																
	    NewExp$ = Left$(NewExp$, Place - 1) + "+" + Right$(NewExp$, Len(NewExp$) - Place - 1)																
	    If is_in(  "--",NewExp$) > 0 Then																
	        NewExp$ = RPlusMinus$(NewExp$)																
	    End If																
	   out $=  NewExp$																
	End If	
		
	if out $<>"" then fn.rtn  out$			
 	fn.rtn exp$											
fn.end																	
															
																	
fn.def CSTA$(Exp$)																	
	Exp$ = RPlusMinus$(Exp$)																
	Place = is_in( "]",Exp$)																
	If Place > 0 Then																
	    Y = 0																
	    X = Place																
	    Do      %loop to find the inside of the brackets																
	        Y = Y + 1																
	        X = X - 1																
	        If X = 0 Then D_u.break      %incase we got to the start of the string																
	        If Mid$(Exp$, X, 1) <> "[" Then																
	            MiddleStr$ = Mid$(Exp$, X, Y)																
	        End If																
	    Until Mid$(Exp$, X, 1) = "["																
	    BeforeStr$ = ""   
	    If X > 0 Then BeforeStr$ = Mid$(Exp$, 1, X - 4)																
	    If X > 0 Then XType$ = Mid$(Exp$, X - 3, 3)																
	    sw.begin lower$(XType$	)															
	    sw.Case "cos"																
	        NewExp$ = BeforeStr$ + str$(Cos(parse(MiddleStr$))) + Mid$(Exp$, Place + 1)		
	        sw.break													
	    sw.Case "sin"																
	        NewExp$ = BeforeStr$ + str$( Sin(parse(MiddleStr$))) + Mid$(Exp$, Place + 1)		
	        sw.break													
	    sw.Case "tan"																
	        NewExp$ = BeforeStr$ + str$( Tan(parse(MiddleStr$))) + Mid$(Exp$, Place + 1)	
	        sw.break													
	    sw.Case "atn"																
	        NewExp$ = BeforeStr$ + str$( Atan(parse(MiddleStr$))) + Mid$(Exp$, Place + 1)	
	        sw.break													
	    sw.end																
	    If is_in( "]",Exp$) > 0 Then																
	        NewExp$ = CSTA$(NewExp$)															
	    End If																
	    fn.rtn NewExp$																
	End If																
	fn.rtn Exp$																
fn.end																	
																	
fn.def RSpaces$(Exp$)																	
	If Exp$ = "" Then Fn.rtn ""																
	X = 0																
	Do																
	X = X + 1																
	If Mid$(Exp$, X, 1) <> " " Then NewExp$ = NewExp$ + Mid$(Exp$, X, 1)																
	Until X = Len(Exp$)																
	fn.rtn NewExp$																
fn.end	
!-------------------------------------------------
! End of the functions to evaluate the string to be calculated 
!-------------------------------------------------
return