REM Start of BASIC! Program

gr.open 255,255,255,255
gr.color 255,0,0,0,0
gr.orientation 1
gr.text.size 20
x = 10
y = 10
xw = 300 % width of frame
yh = 200 % height of frame
txt$ = "Press text to edit..."

gosub createFrame
goto loop

createFrame:
gr.color 255,123,200,144,0 
!gr.text.size 100
!gr.text.draw tx5,300,500,txt$
gr.orientation 1
gr.text.size 20
gr.color 128,0,0,0,1
gr.rect rc1, x+10,y+10,x+xw+10,y+yh+10
gr.color 255,220,220,220,1
gr.rect rc1, x,y,x+xw,y+yh
gr.color 255,0,0,0,0
gr.rect rc1, x,y,x+xw,y+yh
gr.rect rc2, x+1,y+1,x+xw-2,y+yh-2
gr.color 255,100,100,255,1
gr.rect rc3, x+3, y+3, x+xw-30, y+28
gr.color 255,255,0,0,1
gr.rect rc3, x+xw-28, y+3, x+xw-3, y+28
gr.color 255,0,0,0,0
gr.line ln1,x,y+30,x+xw,y+30
gr.color 255,255,255,255,0
gr.line ln2,x+xw-25,y+5, x+xw-5,y+25
gr.line ln3,x+xw-25,y+25,x+xw-5,y+5
gr.color 255,0,0,0,0
gr.text.draw textdr, x+20,y+21,"Hello Basic"
gr.color 255,255,68,14,0
gr.text.draw tx2, x+10,y+60,"Hello. This is a first sample"
gr.text.draw tx3, x+10,y+85,"of a moveable frame..."
gr.text.draw tx4,x+10,y+110,txt$
gr.color 255,0,0,0,0
gr.line ln1,x,y+yh-31,x+xw,y+yh-31
gr.color255,196,196,196,1
gr.rect rc6,x+2,y+yh-30,x+xw-30,y+yh-4
gr.color 255,100,100,220,1
gr.rect rc5, x+xw-29,y+yh-29,x+xw-4,y+yh-4
gr.color 255,0,0,128,0
gr.line ln2, x+xw-25,y+yh-25,x+xw-8,y+yh-8
gr.line ln3, x+xw-25,y+yh-25,x+xw-15,y+yh-25
gr.line ln4, x+xw-25,y+yh-25,x+xw-25,y+yh-15
gr.line ln5, x+xw-18,y+yh-8,x+xw-8,y+yh-8
gr.line ln6, x+xw-8,y+yh-18,x+xw-8,y+yh-8
gr.render
!pause 40
return

edit_text:
  gr.front 0
  input "Enter some text",txt$,txt$
  !pause 1000
  gr.front 1
  !gr.render
return

loop:
do
gr.bounded.touch kill, x+xw-40,y,x+xw,y+30 
gr.bounded.touch editer, x,y+35,x+xw-30,y+yh
gr.bounded.touch bnd1, x,y,x+xw-50,y+30
gr.bounded.touch sizer, x+xw-29,y+yh-29,x+xw,y+yh
if bnd1
  gr.touch tch1,tx1,ty1
  while tch1
    gr.touch tch1,tx2,ty2
    deltax = tx2-tx1
    deltay = ty2-ty1
    tx1 = tx2
    ty1 = ty2
    x = x + deltax
    y = y + deltay
    gr.cls
    gosub createFrame    
    pause 40
  repeat
endif

if sizer
  gr.touch tch1,tx1,ty1
  while tch1
    gr.touch tch1,tx2,ty2
    deltax = tx2-tx1
    deltay = ty2-ty1
    tx1 = tx2
    ty1 = ty2
    xw = xw + deltax
    yh = yh + deltay
    gr.cls
    gosub createFrame    
    pause 60
  repeat
endif


if editer
  do
  gr.bounded.touch editer,x,y+35,x+xw,y+yh
  until !editer
  gosub edit_text
  gr.cls
  editer = 0
  gosub createFrame
  pause 200
endif
until kill

ext:
gr.close
print"program exit..."
pause 2000
end
 
 
 
 
 
 
 
 
 
 
