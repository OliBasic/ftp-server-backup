REM Start of BASIC! Program

gr.open 255,255,255,255
gr.color 255,0,0,0,0
gr.orientation 1
gr.text.size 20
x = 10
y = 10
xw = 300 % width of frame
yh = 200 % height of frame
txt$ = "Press text to edit..."
x2 = 10
y2 = 210
xw2 = 300 % width of frame
yh2 = 200 % height of frame
txt2$ = "Press another beautiful text to edit..."
Frame1InFront = 0


gosub createFrame1
gosub createFrame2
gr.render
goto loop

createFrame1:
if xw <= 200 then xw = 200
if yh <= 65 then yh =65
if Frame1InFront = 0
  gr.clip clp1, x,y,x+xw,y+yh % set text clip 1
else
  gr.clip clp1, x,y,x+xw,y+yh,4 % set text clip 1
endif
gr.color 255,123,200,144,0 
!gr.text.size 100
!gr.text.draw tx5,300,500,txt$
gr.orientation 1
gr.text.size 20
gr.color 128,0,0,0,1
gr.rect rc1, x+10,y+10,x+xw+10,y+yh+10
gr.color 255,220,220,220,1
gr.rect rc1, x,y,x+xw,y+yh
gr.color 255,0,0,0,0
gr.rect rc1, x,y,x+xw,y+yh
gr.rect rc2, x+1,y+1,x+xw-2,y+yh-2
gr.color 255,100,100,255,1
gr.rect rc3, x+3, y+3, x+xw-30, y+28
gr.color 255,255,0,0,1
gr.rect rc3, x+xw-28, y+3, x+xw-3, y+28
gr.color 255,0,0,0,0
gr.line ln1,x,y+30,x+xw,y+30
gr.color 255,255,255,255,0
gr.line ln2,x+xw-25,y+5, x+xw-5,y+25
gr.line ln3,x+xw-25,y+25,x+xw-5,y+5
gr.color 255,0,0,0,0
gr.text.draw textdr, x+20,y+21,"Hello Basic"
gr.color 255,255,68,14,0

gr.text.draw tx2, x+10,y+60,"Hello. This is a first sample"
gr.text.draw tx3, x+10,y+85,"of a moveable frame..."
gr.text.draw tx4,x+10,y+110,txt$

!gr.hide clp1

gr.color 255,0,0,0,0
gr.line ln1,x,y+yh-31,x+xw,y+yh-31
gr.color255,196,196,196,1
gr.rect rc6,x+2,y+yh-30,x+xw-30,y+yh-4
gr.color 255,100,100,220,1
gr.rect rc5, x+xw-29,y+yh-29,x+xw-4,y+yh-4
gr.color 255,0,0,128,0
gr.line ln2, x+xw-25,y+yh-25,x+xw-8,y+yh-8
gr.line ln3, x+xw-25,y+yh-25,x+xw-15,y+yh-25
gr.line ln4, x+xw-25,y+yh-25,x+xw-25,y+yh-15
gr.line ln5, x+xw-18,y+yh-8,x+xw-8,y+yh-8
gr.line ln6, x+xw-8,y+yh-18,x+xw-8,y+yh-8

return

createFrame2:

if xw2 <= 200 then xw2 = 200
if yh2 <= 65 then yh2 =65
if Frame1InFront = 0
  gr.clip clp11, x2,y2,x2+xw2,y2+yh2,4 % ###########
else
  gr.clip clp11, x2,y2,x2+xw2,y2+yh2
endif
gr.color 255,123,200,144,0 
!gr.text.size 100
!gr.text.draw tx5,300,500,txt$
gr.orientation 1
gr.text.size 20
gr.color 128,0,0,0,1
gr.rect rc11, x2+10,y2+10,x2+xw2+10,y2+yh2+10
gr.color 255,220,220,220,1
gr.rect rc11, x2,y2,x2+xw2,y2+yh2
gr.color 255,0,0,0,0
gr.rect rc11, x2,y2,x2+xw2,y2+yh2
gr.rect rc12, x2+1,y2+1,x2+xw2-2,y2+yh2-2     
gr.color 255,100,255,100,1
gr.rect rc13, x2+3, y2+3, x2+xw2-30, y2+28
gr.color 255,255,0,0,1
gr.rect rc13, x2+xw2-28, y2+3, x2+xw2-3, y2+28
gr.color 255,0,0,0,0
gr.line ln11,x2,y2+30,x2+xw2,y2+30
gr.color 255,255,255,255,0
gr.line ln12,x2+xw2-25,y2+5, x2+xw2-5,y2+25
gr.line ln13,x2+xw2-25,y2+25,x2+xw2-5,y2+5
gr.color 255,0,0,0,0
gr.text.draw textdr11, x2+20,y2+21,"Hello Basic No.2"
gr.color 255,55,200,50,0


gr.text.draw tx12, x2+10,y2+60,"Hello. This is a another sample"
gr.text.draw tx13, x2+10,y2+85,"of a moveable frame..."
gr.text.draw tx14, x2+10,y2+110,txt2$

!gr.hide clp11 % hide clip for second text

gr.color 255,0,0,0,0
gr.line ln11,x2,y2+yh2-31,x2+xw2,y2+yh2-31
gr.color255,196,196,196,1
gr.rect rc16,x2+2,y2+yh2-30,x2+xw2-30,y2+yh2-4
gr.color 255,100,220,100,1
gr.rect rc15, x2+xw2-29,y2+yh2-29,x2+xw2-4,y2+yh2-4
gr.color 255,0,128,0,0
gr.line ln12, x2+xw2-25,y2+yh2-25,x2+xw2-8,y2+yh2-8
gr.line ln13, x2+xw2-25,y2+yh2-25,x2+xw2-15,y2+yh2-25
gr.line ln14, x2+xw2-25,y2+yh2-25,x2+xw2-25,y2+yh2-15
gr.line ln15, x2+xw2-18,y2+yh2-8,x2+xw2-8,y2+yh2-8
gr.line ln16, x2+xw2-8,y2+yh2-18,x2+xw2-8,y2+yh2-8

return


edit_text1:
  gr.front 0
  input "Enter some text",txt$,txt$
  !pause 1000
  gr.front 1
  !gr.render
return

edit_text2:
  gr.front 0
  input "Enter some text",txt2$,txt2$
  !pause 1000
  gr.front 1
  !gr.render
return


getFrame1Touches:
gr.bounded.touch kill, x+xw-40,y,x+xw,y+30 
gr.bounded.touch editer, x,y+35,x+xw-30,y+yh
gr.bounded.touch bnd1, x,y,x+xw-50,y+30
gr.bounded.touch sizer, x+xw-29,y+yh-29,x+xw,y+yh
return

getFrame2Touches:
gr.bounded.touch kill2, x2+xw2-40,y2,x2+xw2,y2+30 
gr.bounded.touch editer2, x2,y2+35,x2+xw2-30,y2+yh2
gr.bounded.touch bnd11, x2,y2,x2+xw2-50,y2+30
gr.bounded.touch sizer2, x2+xw2-29,y2+yh2-29,x2+xw2,y2+yh2
return

loop:
do
if Frame1InFront = 0
gosub getFrame2Touches
gosub getFrame1Touches
else
gosub getFrame1Touches
gosub getFrame2Touches
endif

if bnd1
  Frame1InFront = 1
  gr.touch tch1,tx1,ty1
  while tch1
    gr.touch tch1,tx2,ty2
    deltax = tx2-tx1
    deltay = ty2-ty1
    tx1 = tx2
    ty1 = ty2
    x = x + deltax
    y = y + deltay
    gr.cls
    gosub createFrame2
    gosub createFrame1  
    gr.render
  repeat
endif

if sizer
  Frame1InFront = 1
  gr.touch tch1,tx1,ty1
  while tch1
    gr.touch tch1,tx2,ty2
    deltax = tx2-tx1
    deltay = ty2-ty1
    tx1 = tx2
    ty1 = ty2
    xw = xw + deltax
    yh = yh + deltay
    gr.cls
    gosub createFrame2
    gosub createFrame1   
    gr.render
  repeat
endif


if editer
  Frame1InFront = 1
  do
  gr.bounded.touch editer,x,y+35,x+xw,y+yh
  until !editer
  gosub edit_text1
  gr.cls
  editer = 0
  gosub createFrame2
  gosub createFrame1
  gr.render
  pause 200
endif

if bnd11
  Frame1InFront = 0
  gr.touch tch1,tx1,ty1
  while tch1
    gr.touch tch1,tx2,ty2
    deltax = tx2-tx1
    deltay = ty2-ty1
    tx1 = tx2
    ty1 = ty2
    x2 = x2 + deltax
    y2 = y2 + deltay
    gr.cls
    gosub createFrame1
    gosub createFrame2  
    gr.render
  repeat
endif

if sizer2
  Frame1InFront = 0
  gr.touch tch1,tx1,ty1
  while tch1
    gr.touch tch1,tx2,ty2
    deltax = tx2-tx1
    deltay = ty2-ty1
    tx1 = tx2
    ty1 = ty2
    xw2 = xw2 + deltax
    yh2 = yh2 + deltay
    gr.cls
    gosub createFrame1
    gosub createFrame2   
    gr.render
  repeat
endif


if editer2
  Frame1InFront = 0
  do
  gr.bounded.touch editer2,x,y+35,x+xw,y+yh
  until !editer2
  gosub edit_text2
  gr.cls
  editer2 = 0
  gosub createFrame1
  gosub createFrame2
  gr.render
  pause 200
endif


until kill | kill2

ext:
gr.close
print"program exit..."
pause 2000
end
 
 