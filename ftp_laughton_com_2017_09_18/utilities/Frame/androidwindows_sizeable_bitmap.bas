!* Start of BASIC! Program
!************************************************************
!* Loading icons from Laughton.com and saving in folder DATA

filename$ = "titlebar.png"
byte.open R , rf0, "http://laughton.com/basic/programs/utilities/Frame/" + filename$
byte.copy rf0, filename$

filename$ = "statusbar.png"
byte.open R , rf1, "http://laughton.com/basic/programs/utilities/Frame/" + filename$
byte.copy rf1, filename$

filename$ = "minimize.png"
byte.open R , rf2, "http://laughton.com/basic/programs/utilities/Frame/" + filename$
byte.copy rf2, filename$

filename$ = "maximize.png"
byte.open R , rf3, "http://laughton.com/basic/programs/utilities/Frame/" + filename$
byte.copy rf3, filename$

filename$ = "resize.png"
byte.open R , rf4, "http://laughton.com/basic/programs/utilities/Frame/" + filename$
byte.copy rf4, filename$

filename$ = "close.png"
byte.open R , rf5, "http://laughton.com/basic/programs/utilities/Frame/" + filename$
byte.copy rf5, filename$

!************************************************************
!* Initialize graphics mode

gr.open 255,255,255,255

!************************************************************
!* Settings

gr.color 255,0,0,0,0
gr.orientation 0
gr.text.size 20
x = 10
y = 10
xw = 300 % width of frame
yh = 200 % height of frame
oldx = x
oldy = y
oldxw = xw
oldyh = yh
txt$ = "Press text to edit..."
demotxt$ = "Windows demo on Android"
maximized = 0
minimized = 0
gr.screen screenwidth, screenheight

!************************************************************
!* Correct this for your phone/tablett
!* I have an Acer iconia with toolbar on bottom!

screenheight = screenheight-48  

!************************************************************
!* Load bitmaps

gr.bitmap.load btp1, "titlebar.png"
gr.bitmap.load btp2, "statusbar.png"
gr.bitmap.load btp3, "minimize.png"
gr.bitmap.load btp4, "maximize.png"
gr.bitmap.load btp5, "resize.png"
gr.bitmap.load btp6, "close.png"

!************************************************************
!* Calling procedures and mainloop

gosub createFrame
goto loop

!************************************************************
!************************************************************
!* Procedure to make the window
!************************************************************

createFrame:

!************************************************************
!* Smallest window to Show all Icons and title and statusbar

if xw <= 200 then xw = 200
if yh <= 65 then yh = 65

!************************************************************
!* Text in the background

gr.color 255,123,200,144,0 
gr.text.size 80
gr.text.draw tx5,100,400,demotxt$

!************************************************************
!* Setting orientation

!gr.orientation 1

!************************************************************
!* The Window
!************************************************************

gr.text.size 20

!************************************************************
!* The shadow

gr.color 128,0,0,0,1
gr.rect rc1, x+10,y+10,x+xw+10,y+yh+10

!************************************************************
!* Windows background

gr.color 255,220,220,220,1
gr.rect rc1, x,y,x+xw,y+yh

!************************************************************
!* The frame outside 2 lines

gr.color 255,0,0,0,0
gr.rect rc1, x,y,x+xw,y+yh
gr.rect rc2, x+1,y+1,x+xw-2,y+yh-2

!************************************************************
!* The titlebar

gr.bitmap.scale btp7, btp1, xw-84, 25
gr.bitmap.draw drw1, btp7, x+3,y+3

!************************************************************
!* The minimize icon

gr.bitmap.draw drw5, btp3, x+xw-80,y+3

!************************************************************
!* The maximize icon

gr.bitmap.draw drw6, btp4, x+xw-54,y+3

!************************************************************
!* The red close icon

gr.bitmap.draw drw2, btp6, x+xw-28,y+3

!************************************************************
!* The line under the title

gr.color 255,0,0,0,0
gr.line ln1,x,y+31,x+xw,y+31

!************************************************************
!* The text in the titlebar

gr.color 255,0,0,0,0
gr.text.draw textdr, x+20,y+21,"Hello Basic"

!************************************************************
!* The text in the window

gr.color 255,255,68,14,0
gr.text.draw tx2, x+10,y+60,"Hello. This is a first sample"
gr.text.draw tx3, x+10,y+85,"of a moveable frame..."
gr.text.draw tx4,x+10,y+110,txt$

!************************************************************
!* The line over the statusbar

gr.color 255,0,0,0,0
gr.line ln1,x,y+yh-31,x+xw,y+yh-31

!************************************************************
!* The statusbar

gr.bitmap.scale btp8, btp2, xw-33, 25
gr.bitmap.draw drw3, btp8, x+2,y+yh-30

!************************************************************
!* The resize icon

gr.bitmap.draw drw4, btp5, x+xw-29,y+yh-29

!************************************************************
!* Show it all

gr.render
!pause 40
return

!************************************************************
!* End of window procedure
!************************************************************
!************************************************************


!************************************************************
!* Editing the text
!************************************************************

edit_text:
	gr.front 0
	input "Enter some text",txt$,txt$
	!pause 1000
	gr.front 1
	!gr.render
return

!************************************************************
!* Main loop
!************************************************************

loop:
do
	gr.bounded.touch kill, x+xw-28,y,x+xw,y+30 
	gr.bounded.touch winmaxi, x+xw-54,y,x+xw-29,y+30
	gr.bounded.touch winmini, x+xw-80,y,x+xw-56,y+30
	gr.bounded.touch editer, x,y+35,x+xw-30,y+yh-30
	gr.bounded.touch bnd1, x,y,x+xw-81,y+30
	gr.bounded.touch sizer, x+xw-28,y+yh-30,x+xw,y+yh

!************************************************************
!* Maximize the window

if winmaxi
	do
		 gr.bounded.touch winmaxi, x+xw-54,y,x+xw-29,y+30
	until !winmaxi
if maximized = 0 then
  maximized = 1
  minimized = 0
	oldx = x
	oldy = y
	oldxw = xw
	oldyh = yh
  x = 0
  y = 0
  xw = screenwidth
  yh = screenheight
else
  maximized = 0
	x = oldx
	y = oldy
	xw = oldxw
	yh = oldyh
endif
  gr.cls
	gosub createFrame    
	pause 40
endif

!************************************************************
!* Minimize the window

if winmini
do
	gr.bounded.touch winmini, x+xw-80,y+yh-56,x+xw,y+30
until !winmini
if minimized = O then
  minimized = 1
  maximized = 0
	oldx = x
	oldy = y
	oldxw = xw
	oldyh = yh
  x = 0
  y = screenheight-65
  xw = 200
  yh = 65
else
	x = oldx
	y = oldy
	xw = oldxw
	yh = oldyh
endif
  gr.cls
  gosub createFrame    
  pause 40
endif

!************************************************************
!* Moving the window

if bnd1
		gr.touch tch1,tx1,ty1
		while tch1
		    gr.touch tch1,tx2,ty2
		    deltax = tx2-tx1
		    deltay = ty2-ty1
		    tx1 = tx2
		    ty1 = ty2
		    x = x + deltax
		    y = y + deltay
		    gr.cls
		    gosub createFrame    
		    pause 40
		repeat
endif

!************************************************************
!* Resize the Window

if sizer
	gr.touch tch1,tx1,ty1
	while tch1
		    gr.touch tch1,tx2,ty2
        deltax = tx2-tx1
		    deltay = ty2-ty1
		    tx1 = tx2
		    ty1 = ty2
		    xw = xw + deltax
		    yh = yh + deltay
		    gr.cls
		    gosub createFrame    
		    pause 60
		repeat
endif

!************************************************************
!* Edit the text in the window

if editer
	do
		gr.bounded.touch editer,x,y+35,x+xw,y+yh
	until !editer
	gosub edit_text
	gr.cls
	editer = 0
	gosub createFrame
	pause 200
endif

until kill

!************************************************************
!* End of main loop
!************************************************************

!************************************************************
!* End of program

ext:
gr.close
print"program exit..."
pause 1000
end
 
!************************************************************
 
