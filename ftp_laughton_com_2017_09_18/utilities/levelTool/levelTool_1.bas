GOSUB userfunctions

 ARRAY.LOAD       sensOffer$ [],"orien"
ARRAY.LENGTH      nOffer, sensOffer $[]

SENSORS.LIST      tmp $[]
SENSORS.LIST      tmp2 $[]
ARRAY.LENGTH      sel, tmp $[]
FOR i           = 1 TO sel
 PRINT            tmp $[i]
 tmp $[i]       = lower $(REPLACE$( tmp $[i]," ",""))
 PRINT            tmp $[i]
NEXT

PRINT             "----"
FOR j           = 1 TO nOffer
 flag_break     = 0
 FOR i          = 1 TO sel
  IF              Left $( tmp$[i], 5) = sensOffer $[j]  THEN
   pos          = len ( tmp$[i] )-is_in ("=", tmp$[i] ) 
   sensOri      = val (right $(tmp $[i], pos))
   flag_break   = 1
  ENDIF
  IF              flag_break THEN f_n.break
 NEXT
NEXT

 if 				sensOri
 print sensori
	SENSORS.OPEN         sensOri:1
else
	print "No orientation-sensor found...program stopped"
	end
endif 


flagOri        = 0
refW           = 790
refH           = 1255
IF !flagOri      THEN SWAP refW, refH
centW          = refW/2
centH          = refH/2
GR.OPEN          255,20,30,40,1,flagOri
GR.SCREEN        curW, curH
scaW           = curW / refW
scaH           = curH / refH
GR.SCALE         scaW , scaH
GR.SET.ANTIALIAS 1


!-----------------------------------
! sound ---------------------

wav1$			=  "tone2.wav"
wav2 $			=   "880Hz_500ms.wav"
file.exists			fex1,wav1$
file.exists			fex2,wav1$
flagSound		= fex1 & fex2

IF 					flagSound
 SOUNDPOOL.OPEN       2
 SOUNDPOOL.LOAD       itol, wav1 $
 SOUNDPOOL.LOAD       otol, wav2 $
 PAUSE                200
ENDIf

!-----------------------------------
! params ---------------------

k_f            =  0.2
bsz            =  8

offx           = 50
offy           = 80

von            =-135
bis            = 135
sca            = 55
dy             = 160
dx             = (bis-von)*sca

flagSwapChan   = 1
rad            = 70
dim 			  setp[3]
setp[3]          = 0
setp[2]          = 0
tol              = 0.5
ptrTol		   	= 2
kRate			= 0.05
flagsoundOn    = 1

popup 			"loading screen...",0,0,2

!-----------------------------------
! create ruler&masks -------------
rulerY         = createRuler ( von, bis , 5, sca, dy, 1 ,20)
maskX          =  maskRcr (450 , dy*0.95, rad, 0, 0, refw*1.1, dy*1.05)
maskY          =  maskRcr (dy*0.95 , 450, rad, 0, 0, dy*1.05 , refh*1.1 )


!-----------------------------------
! horizotal ruler ---------------
GR.SET.STROKE     1
GR.BITMAP.DRAW    _rulerY,  rulerY , centw- dx/2, offy
GR.COLOR          100, 20 , 20 , 255 , 1
GR.SET.STROKE     6
GR.LINE           _spy, centw+setp[2] *sca, offy, centw+setp[2] *sca, offy+dy
GR.SET.STROKE     1
GR.COLOR          100,255, 0, 0, 1
GR.RECT           nn, centw-tol*sca, offy , centw+tol*sca , offy+dy
GR.COLOR          225, 20 , 30 , 40 , 1
GR.LINE           nn, centw, offy, centw, offy+dy
GR.POLY           nn, maskx,  centw, offy+dy/2

!-----------------------------------
! vertical ruler ---------------
GR.COLOR          255, 20 , 30 , 40 , 1
GR.ROTATE.START   -90, offx, centh
GR.BITMAP.DRAW    _rulerx,  rulery ,  offx-dx/2, centh
GR.rotate. end

GR.COLOR          100, 20 , 20 , 255 , 1
GR.SET.STROKE     6
GR.LINE           _spx, offx, centh-setp[3] *sca, offx+dy, centh-setp[3] *sca
GR.SET.STROKE     1
gr .COLOR         100,255, 0, 0, 1
GR.RECT           nn, offx, centh-tol*sca , offx+dy, centh+tol*sca
GR.COLOR          225, 20 , 30 , 40 , 1
GR.LINE           nn, offx, centh, offx+dy, centh
GR.POLY           nn, masky,  offx+dy/2 , centh


!-----------------------------------
! frames ---------------
GR.SET.STROKE     8
rcr             = roundCornerRect (450, dy*0.95, rad)
GR.COLOR          255, 210 , 210 , 210 , 0
GR.POLY           nn, rcr,  centw-1, offy+dy/2-1
GR.COLOR          255, 130 , 110 , 90 , 0
GR.POLY           nn, rcr,  centw+1, offy+dy/2+1

rcr             = roundCornerRect ( dy*0.95, 450, rad)
GR.COLOR          255, 210 , 210 , 210 , 0
GR.POLY           nn, rcr,   offx+dy/2-1, centh-1
GR.COLOR          255, 130 , 110 , 90 , 0
GR.POLY           nn, rcr,   offx+dy/2+1, centh+1


!-----------------------------------
! temporary info text ----------

GR.COLOR           255,  255 , 255, 255, 1
txtSz            = 22
GR.TEXT.SIZE       txtSz
GR.TEXT.ALIGN      1
dyTxt            = txtSz*1.1
ARRAY.LOAD         txt$ [], "x: ", "y: ", "z: ", "tLoop: "
FOR i            = 1 TO         4
 GR.TEXT.DRAW       _txt, offx+dy+25, offy+dy + i* dyTxt, txt$[i]
NEXT
_txt             = _txt-4


!-----------------------------------
! controls ----------
anzCtr			=  1
dim 			  ctrl[1,5]
pxx				= 800
pyy				= 600	
rcr             = roundCornerRect (130, 70, 20)
gr.set.stroke     4
GR.COLOR          255,  240, 211, 165, 1
GR.POLY           nn, rcr,  pxx, pyy
GR.COLOR          255, 210 , 210 , 210 , 0
GR.POLY           nn, rcr,  pxx-1, pyy-1
GR.COLOR          255, 130 , 110 , 90 , 0
GR.POLY           nn, rcr,  pxx+1, pyy+1
txtSize          = 28
gr.text.align	  2
gr.text.size	  txtSize
GR.COLOR          255,  0,0,0, 1
gr.text.draw	  nn,  pxx, pyy+txtSize/2, "Settings"

array.load set$[],"sound on/off","swap channels","swap sound channel"

!------------------------------------------
! temp values and filters ------------
DIM                v[4]
DIM                vf[4]
k_f              =  MAX(MIN(k_f,1),0.005)
bsz              =  MAX(MIN(bsz,64),1)
DIM                vb2 [bsz]
DIM                vb3 [bsz]
! init filters ---------------------------
FOR i=1 TO bsz
 SENSORS.READ      sensOri, v[1], v[2] , v[3]
 IF               flagSwapChan THEN swap v[3] , v[2]
 vb2 [MOD(i, bsz)+1] = v [2]
 vb3 [MOD(i, bsz)+1] = v [3]
NEXT
ARRAY.AVERAGE    v [2]  , vb2 []
ARRAY.AVERAGE    v [3]  , vb3 []
vf[2]             = v [2]
vf[3]             = v [3]


!------------------------------------------
! main loop ------------------------
IF                   flagSound
 SOUNDPOOL.PLAY     _itol,itol, 0.99, 0.99, 1,  -1 , 1
 SOUNDPOOL.PLAY     _otol,otol, 0.99, 0.99, 1,  -1 , 1
ENDIF
dtDes            = 60
tic              = CLOCK()

DO

 SENSORS.READ      sensOri, v[1], v[2] , v[3]
 IF               flagSwapChan THEN swap v[3] , v[2]
 v[4]            = tocAll
 ptr             = ptr +1

 vb2 [MOD(ptr, bsz)+1] = v [2]
 vb3 [MOD(ptr, bsz)+1] = v [3]
 ARRAY.AVERAGE     v [2]  , vb2 []
 ARRAY.AVERAGE     v [3]  , vb3 []

 FOR i           = 1 TO 4
  vf[i]          = vf[i] + (v[i]-vf[i]) * k_f
  GR.MODIFY        _txt+i, "text", txt$[i] + STR$(round (vf[i],1))
 NEXT

 GR.MODIFY         _rulerX,"x" , offx-dx/2- vf[2]*sca
 GR.MODIFY         _spX,   "y1", centh -(setp[ 2 ] -vf[2])*sca ,"y2", centh -(setp[ 2 ] -vf[2])*sca

 GR.MODIFY         _rulerY,"x", centw-dx/2- vf[3]*sca
 GR.MODIFY         _spY,   "x1", centw +(setp[3] -vf[3])*sca ,"x2", centw +(setp[3] -vf[3])*sca
 
 gosub             ptrTolcheck ,  tol  x, toly
 tolcheck        = (-vf[ptrTol]+setp[ ptrTol ] >-tol)&( -vf[ptrTol]+setp[ ptrTol ] <tol)
 IF flagSound        THEN gosub   tolcheck+1 ,  __outtol, __intol

 if 			         !background then GR.RENDER
 gr.bounded.touch  tou,(pxx-100)*scaw, (pyy-50)*scah, ~
 (pxx+100)*scaw,(pyy+50)*scah 
 if tou then gosub touch


 tocAll          = CLOCK()-tic
 PAUSE             max (dtDes-tocAll, 1)
 tic             = CLOCK()
UNTIL              0
!-------------------------------------
!-------------------------------------------


__intol:
SOUNDPOOL.PAUSE otol
SOUNDPOOL.RESUME itol
RETURN

__outtol:
SOUNDPOOL.PAUSE itol
SOUNDPOOL.RESUME otol
SOUNDPOOL.SETRATE otol, min (max ( 1+( setp[ ptrTol ]  -vf[ptrTol] )*kRate, 0.51), 1.80)
RETURN


touch:
dialog.select      sel,set$[],"settings"
if sel =1
if flagSoundOn 
 flagsoundOn =0
 soundpool.setvolume otol, 0,0
 soundpool.setvolume itol, 0,0
else
 flagsoundOn =1
 soundpool.setvolume otol, 0.99,0.99
 soundpool.setvolume itol, 0.99,0.99
endif

ELSEIF SEL =2
if flagSwapChan =1 then flagswapchan = 0 else flagswapchan =1

elseif sel=3
if ptrTol =2 then ptrtol =3 else ptrtol=2

ENDIF
return




userfunctions:

FN.DEF             maskRcr (rcrle, rcrhi, rrcr, le, to, ri, bo)
 dx =ri-le
 dy= bo-to
 le=le-dx/2
 to =to-dy/2
 ri = le+dx
 bo =to +dy
 GR.SCREEN         wi, hi
 wi=wi*2
 hi=hi*2
 LIST.CREATE       n, mask
 rcr =             roundCornerRect ( rcrle, rcrhi, rrcr)
 LIST.GET          rcr, 1, x
 LIST.GET          rcr, 2, y
 LIST.ADD          rcr, x, y
 LIST.ADD.LIST      mask, rcr
 LIST.ADD          mask, le , to ,  le , bo,  ri, bo ,  ri ,to
 LIST.ADD          mask, le, to
 FN.RTN            mask
FN.END


FN.DEF              getGrObjCount()
 GR.CIRCLE           grObjCount,-5,-5, 0
 FN.RTN              grObjCount
FN.END




FN.DEF                roundCornerRect (b, h, r)
 IF                   b <2*r THEN r=b/2
 IF                   h <2*r THEN r=h/2
 half_pi            = 3.14159 / 2
 dphi               = half_pi / 12
 LIST.CREATE          N, S1
 FOR phi            = 0 TO half_pi STEP dphi
  LIST.ADD             s1, -b/2+r-COS(phi)*r, -h/2+r-sin (phi)*r
 NEXT
 FOR phi            = 0 TO half_pi STEP dphi
  LIST.ADD            s1, b/2-r+SIN(phi)*r, -h/2+r- cos (phi)*r
 NEXT
 FOR phi            = 0 TO half_pi STEP dphi
  LIST.ADD            s1, b/2-r+COS(phi)*r, h/2-r+sin (phi)*r
 NEXT
 FOR phi            = 0 TO half_pi STEP dphi
  LIST.ADD            s1, -b/2+r-SIN(phi)*r, h/2-r+cos (phi)*r
 NEXT
 FN.RTN s1
FN.END


FN.DEF             createRuler (von, bis, subtic, sca, width, type ,txtSz )

 dx             =  (bis-von)*sca
 dy             =  width

 IF  type = 2      THEN SWAP dx, dy

 GR.BITMAP.CREATE  bmp1, dx, dy
 GR.BITMAP.DRAWINTO.START bmp1
 GR.COLOR          255,  240, 211, 165, 1
 GR.RECT           nn, 0,0, dx,dy
 GR.COLOR          255,  0 , 0, 0, 0

 GR.SET.STROKE     2
 GR.COLOR          255,  0 , 0, 0, 1
 IF                type =  1 THEN
  FOR i           = 0 TO dx  STEP sca/subtic
   GR.LINE           nn, i, 0,i, dy
  NEXT
  GR.COLOR          255,  240, 211, 165, 1
  GR.RECT           nn, 0, dy/6, dx, dy*5/6
 ELSE
  FOR i           = 0 TO dy  STEP sca/subtic
   GR.LINE           nn, 0, i,dx, i
  NEXT
  GR.COLOR          255,  240, 211, 165, 1
  GR.RECT           nn, dx/6, 0 , dx*5/6, dy
 ENDIF

 GR.SET.STROKE     2
 GR.COLOR          255,  0 , 0, 0, 1
 IF                type =  1  THEN
  FOR i           = 0 TO dx  STEP sca
   GR.LINE           nn, i, 0,i, dy
  NEXT
  GR.COLOR          255,  240, 211, 165, 1
  GR.RECT           nn, 0, dy/4, dx, dy*3/4
 ELSE
  FOR i           = 0 TO dy  STEP sca
   GR.LINE           nn, 0, i,dx, i
  NEXT
  GR.COLOR          255,  240, 211, 165, 1
  GR.RECT           nn, dx/4, 0 , dx*3/4, dy
 ENDIF



 GR.COLOR          255,  0 , 0, 0, 1
 GR.SET.STROKE     2
 GR.TEXT.SIZE      txtSz
 GR.TEXT.ALIGN     2
 GR.TEXT.TYPEFACE  1
 GR.TEXT.BOLD  1
 IF                type =1
  FOR i           = 0 TO dx  STEP sca
   val            = i/sca+von
   IF               val <0 THEN ox=- txtSz/4 ELSE ox=0
   txtszOff = 0
   IF               val >-99 THEN txtszOff = 3
   IF               val >-10 THEN txtszOff = 5
   GR.TEXT.SIZE      txtSz+txtszOff
   GR.TEXT.DRAW     nn, i+ox, dy/2+8, INT$( val )
  NEXT
 ELSE
  FOR i           = 0 TO dy  STEP sca
   val            = bis - i/sca
   IF               val <0 THEN ox=- txtSz/4 ELSE ox=0
   GR.TEXT.DRAW     nn,  dx/2+ox, i+txtSz/2-2, INT$( val )
  NEXT
 ENDIF


 GR.BITMAP.DRAWINTO.END

 FN.RTN  bmp1
FN.END


FN.DEF setVolume( incDec)

 keycode$        = "25"
 IF incDec       >  0  THEN keycode$ ="24"

 SYSTEM.OPEN
 SYSTEM.WRITE      "input keyevent " + keycode$
 PAUSE             50
 SYSTEM.CLOSE

FN.END



RETURN
