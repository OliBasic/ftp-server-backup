! key.bas
! displays android keyCode for buttons
! version: 1
! author: ba houghton

gr.open 255,255,255,255,1 % white
gr.orientation 1 % portrait

y=75

gr.color 255,0,0,0,1 % black

gr.text.draw t,5,50,"press any button:"

loop:
  inkey$ key$
  if key$<>"@" % eliminate non-presses
    gr.text.draw n,5,y,"you pressed: "+key$ 
    gr.render
    print key$ % debug - code to output
    y=y+25 % move text graphics down
  endif
goto loop: 
