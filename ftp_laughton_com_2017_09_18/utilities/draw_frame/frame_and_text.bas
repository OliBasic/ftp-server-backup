%***************************************************
%*  Graphic Demo 1  *   V1.00   *    2015/02/22    *
%***************************************************

%---------------------------------------------------
% Init Graphic interface
%---------------------------------------------------

GR.OPEN 255, 128, 128, 128							% Open Screen Window / Gray Background
GR.ORIENTATION 0									% Display Landscape (0) or Portrait (1)

GR.SCREEN Current_Width,Current_Height				% Get Screen Size Info

Screen_Width  = Current_Width
Screen_Height = Current_Height

% Swap Value if you change Orientation
% Screen_Width  = 1280								% Set Required screen size format
% Screen_Height = 720								% Ratio compatible with 1920x1080

PAUSE 1000											% Delay require for Orientation update

Scale_X = Current_Width/Screen_Width
Scale_Y = Current_Height/Screen_Height
GR.SCALE Scale_X,Scale_Y							% Set Scale Ratio for required Screen Size 

%---------------------------------------------------
%               Init Font & Variable
%---------------------------------------------------

GR.TEXT.TYPEFACE 2									% Select Font MonoSpace
GR.TEXT.SIZE 32          							% will give 80 x 26 chrs
GR.TEXT.ALIGN 1										% Align Text Left
GR.TEXT.BOLD 1										% Bold Enable
GR.TEXT.UNDERLINE 0									% Underline Disable
GR.TEXT.STRIKE 0									% StrikeOut Disable

Text$ = "Touch the screen"
GR.TEXT.WIDTH  TWidth, Text$       		   			% get Width  of text
GR.TEXT.HEIGHT THeight,TPixel_Up,TPixel_Down        % get Height of text

WAKELOCK 3											% Bright Screen enable
RANDOMIZE											% Reset pseudorandom number generator

%---------------------------------------------------
%                Function section
%---------------------------------------------------

fn.def Set_Color(c,g)
   Rem g=0 Outline and g=1 Fill
   if c=00 then 
	  GR.COLOR 255,000,000,000,g 					% black
   elseif c=01 then 
      GR.COLOR 255,255,000,000,g 					% Red
   elseif c=02 then 
      GR.COLOR 255,000,255,000,g 					% green
   elseif c=03 then 
      GR.COLOR 255,000,000,255,g 					% blue
   elseif c=04 then 
      GR.COLOR 255,255,255,000,g			 		% yellow
   elseif c=05 then 
      GR.COLOR 255,000,255,255,g 					% cyan
   elseif c=06 then 
      GR.COLOR 255,255,000,255,g			 		% magenta
   elseif c=07 then 
      GR.COLOR 255,064,064,064,g					% Dark Gray 
   elseif c=08 then 
      GR.COLOR 255,128,128,128,g					% Gray 
   elseif c=09 then 
      GR.COLOR 255,192,192,192,g					% Light Gray 
   elseif c=10 then 
      GR.COLOR 255,255,255,255,g					% white
   elseif c=11 then 
      GR.COLOR 255,205,000,000,g 					% mediumblue
   elseif c=12 then 
      GR.COLOR 255,065,105,225,g 					% royalblue
   elseif c=13 then 
      GR.COLOR 255,255,215,000,g 					% gold
   elseif c=14 then 
      GR.COLOR 255,000,250,154,g 					% springgreen
   elseif c=15 then 
      GR.COLOR 255,220,020,060,g 					% crimson
   endif
fn.end

%---------------------------------------------------

fn.def Draw_Frame(x,y,w,h,a,cf,cb)					% Position X, Position Y, Width, Height, Angle, Color Foreground, Color Background

	% Set Color for Box (ForeGround)
	call Set_Color(cf,1)

	% If no Angle then Draw Rectangle
	If a=0 then
		% Draw Box
		GR.RECT Frm , X         , Y         , X+W , Y+H
	Else
		% Draw Colored Box
		GR.RECT Frm , X         , Y         , X+W     , Y+H
		% Set Color for background
		call Set_Color(cb,1)
		% Erase Top Left Box
		GR.RECT Frm , X         , Y         , X+(A/2) , Y+(A/2)
		% Erase Top Right Box
		GR.RECT Frm , X+W-(A/2) , Y         , X+W     , Y+(A/2)
		% Erase Bottom Left Box
		GR.RECT Frm , X         , Y+H-(A/2) , X+(A/2) , Y+H
		% Erase Bottom Right Box
		GR.RECT Frm , X+W-(A/2) , Y+H-(A/2) , X+W     , Y+H
		% Set Color for Box (ForeGround)
		call Set_Color(cf,1)
		% Draw Bottom Right Arc
		GR.ARC  Frm , X+W-A     , Y+H-A     , X+W     , Y+H  , 000 , 090 , 1 
		% Draw Bottom Left Arc
		GR.ARC  Frm , X         , Y+H-A     , X+A     , Y+H  , 090 , 090 , 1	 	
		% Draw Top Left Arc
		GR.ARC  Frm , X         , Y         , X+A     , Y+A  , 180 , 090 , 1
		% Draw Top Right Arc
		GR.ARC  Frm , X+W-A     , Y         , X+W     , Y+A  , 270 , 090 , 1	
	Endif

fn.end

%---------------------------------------------------
%                   Main program
%---------------------------------------------------

GOSUB Display_Info
GOSUB Demo_Frame
GR.CLOSE
END "Program Terminated - Shutdown Normal"
EXIT

%---------------------------------------------------
%                   Gosub section
%---------------------------------------------------

Display_Info:

	PosX = INT((Screen_Width-TWidth)/2)					% Text Center position for X Axe 
	PosY = INT((Screen_Height-THeight)/2)				% Text Center position for Y Axe  

	Frame_PosX   = PosX-16								% Set Frame Position around Text
	Frame_PosY   = INT((PosY+TPixel_Up)-10)
	Frame_Width  = INT(TWidth+THeight)
	Frame_Height = INT(THeight*2)
	Frame_Angle  = 032

	Frame_Color  = 003
	Frame_BackG  = 008
	call Draw_Frame(Frame_PosX-Frame_Angle,Frame_PosY-Frame_Angle,Frame_Width+(Frame_Angle*2),Frame_Height+(Frame_Angle*2),Frame_Angle,3,Frame_BackG)

	Frame_Color  = 004
	Frame_BackG  = 003
	call Draw_Frame(Frame_PosX,Frame_PosY,Frame_Width,Frame_Height,Frame_Angle,Frame_Color,Frame_BackG)

	Set_Color(08,1)										% Set White for Text Message
	GR.TEXT.DRAW MSG, PosX, PosY, Text$					% Draw Text

	call Draw_Frame(25,25,Screen_Width-50,Frame_PosY-Frame_Angle-50,0,15,0)
	call Draw_Frame(25,Frame_PosY+125,Screen_Width-50,Screen_Height-(Frame_PosY+150),0,15,0)

	Set_Color(04,1)										   					% Set Black for Text Info
	GR.TEXT.DRAW Msg, 50, 075, "Device_Width  : "+STR$(Current_Width) +" / Device_Height : "+STR$(Current_Height)
	Set_Color(04,1)										   					% Set Black for Text Info
	GR.TEXT.DRAW Msg, 50, 150, "Screen_Width  : "+STR$(Screen_Width) +" / Screen_Height : "+STR$(Screen_Height)
	Set_Color(05,1)										   					% Set Black for Text Info
	GR.TEXT.DRAW Msg, 50, 225, "Text_Width    : "+STR$(TWidth)        +"  / Text_Height  : "+STR$(THeight)
	Set_Color(05,1)										   					% Set Black for Text Info
	GR.TEXT.DRAW Msg, 50, PosY+175, "Text_Pixel_U  : "+STR$(TPixel_Up)     +"  / Text_Pixel_D : "+STR$(TPixel_Down)
	Set_Color(10,1)										   					% Set Black for Text Info
	GR.TEXT.DRAW Msg, 50, PosY+250, "Frame_PosX    : "+STR$(Frame_PosX)    +"  / Frame_PosY   : "+STR$(Frame_PosY)
	Set_Color(10,1)										   					% Set Black for Text Info
	GR.TEXT.DRAW Msg, 50, PosY+325, "Frame_Width   : "+STR$(Frame_Width)   +"  / Frame_Height : "+STR$(Frame_Height)

	GR.RENDER										% Display Screen update

	do
		GR.TOUCH Touched,Tx,Ty						% Was the screen touched
	until Touched

	RETURN

%---------------------------------------------------

Demo_Frame:

	GR.CLS											% Clear Graphic screen
	do
		gr.touch touched, x, y						% Clear screen touched 
	until !touched
	
	Text2$    = ""
	FPS       = 0									% Frame per Second Variable
	Mem_Clock = CLOCK()								% Memory of System Timer
	
	Do
		% Set Random Value for Colored Frame
		Frm_Width  = INT((RND()*(Screen_Width/2))+10)
		Frm_Height = INT((RND()*(Screen_Height/2))+10)
		Frm_PosX   = INT((RND()*(Screen_Width-Frm_Width))+1)
		Frm_Posy   = INT((RND()*(Screen_Height-Frm_Height))+1)
		Frm_Angle  = INT(RND()*16+1)*4
		Frm_BackG  = INT(RND()*15+1)
		Do 
			Frm_Color  = INT(RND()*15+1)
		Until Frm_Color <> Frm_BackG				% Make sure Colors are different
		call Draw_Frame(Frm_PosX-Frm_Angle,Frm_PosY-Frm_Angle,Frm_Width+(Frm_Angle*2),Frm_Height+(Frm_Angle*2),Frm_Angle,Frm_Color,Frm_BackG)
		Frm_BackG  = Frm_Color
		Do 
			Frm_Color  = INT(RND()*15+1)
		Until Frm_Color <> Frm_BackG				% Make sure Colors are different
		call Draw_Frame(Frm_PosX,Frm_PosY,Frm_Width,Frm_Height,Frm_Angle,Frm_Color,Frm_BackG)

		% Display Text Message
		call Draw_Frame(Frame_PosX,Frame_PosY,Frame_Width,Frame_Height,0,8,Frame_BackG)
		Set_Color(10,1)								% Set White for Text Message
		GR.TEXT.DRAW MSG, PosX, PosY, Text$			% Draw Text Message
		
		% Display FPS
		IF Text2$>"" THEN
		   call Draw_Frame(0,0,120,120,0,0,0)
		   Set_Color(04,1)							% Set Yellow for Text Message
		   GR.TEXT.DRAW MSG2, 10, 60, Text2$		% Draw Text Message
		ENDIF
		
		GR.RENDER									% Display Screen update
		
		FPS+=1										% Number of Frame draw
        IF CLOCK()-Mem_Clock>=1000					% For every Seconds
		  Text2$    = Str$(FPS)						% Set Value to Display
          FPS       = 0								% Reset Counter
		  GR.CLS									% Clear Screen
          Mem_Clock = CLOCK()						% Reset Memory of system Timer 
		ENDIF

		GR.TOUCH Touched,Tx,Ty						% Was the screen touched
	Until Touched

	RETURN
