DEVICE DVINFO
BUNDLE.CONTAIN DVINFO, "Language", RESPONSE
IF RESPONSE THEN BUNDLE.GET DVINFO, "Language", Info$
Language$=UPPER$(Info$)
IF Language$="ENGLISH" THEN
   Message$="Welcome"
ELSEIF Language$="FRAN�AIS" THEN
   Message$="Bienvenue"
ELSEIF Language$="ESPA�OL" THEN
   Message$="Bienvenida"
ELSEIF Language$="DEUTSCH" THEN
   Message$="Willkommen"
ELSE
   Message$=""
ENDIF

Print Language$,Message$
IF Message$>"" THEN	
   TTS.INIT
   TTS.SPEAK Message$
ENDIF   
