! My program to calculate the sides of a triang

gr.open 255, 0, 0, 0
PAUSE 10
gr.orientation 0

% PI VALUE
pi = Atan(1) * 4

% FUNCTIONS
fn.def caad(caop,hipo,ang,pi)
	IF hipo = 0
		hipo = caop/sin(ang*pi/180)
		caad = hipo*cos(ang*pi/180)
	ELSE
		caad = hipo*cos(ang*pi/180)
	ENDIF
	fn.rtn caad
fn.end

fn.def caop(caad,hipo,ang,pi)
	IF hipo = 0
		hipo = caad/cos(ang*pi/180)
		caop = hipo*sin(ang*pi/180)
	ELSE
		caop = hipo*sin(ang*pi/180)
	ENDIF
	fn.rtn caop
fn.end

fn.def hipo(caad,caop,ang,pi)
	IF caop = 0
		hipo = caad/cos(ang*pi/180)
	ELSE
		hipo = caop/sin(ang*pi/180)
	ENDIF
	fn.rtn hipo
fn.end


gr.screen sw, sh

DIM grid_x[40]
DIM grid_y[20]
cell_w = sw/40
cell_h = sh/20

DIM Hipot[10]

FOR i = 1 TO 20
	grid_y[i] = i*cell_h
	FOR j = 1 TO 40
		grid_x[j] = j*cell_w
	NEXT
NEXT

% SCALED
sc_caad = grid_x[22]-grid_x[8]
sc_caop = grid_y[19]-grid_y[1]

reload:
gr.cls

gr.color 255, 255, 255, 255, 0
gr.set.stroke 1
gr.text.typeface 4
gr.text.size cell_w

gr.text.align 3
gr.text.draw adj_val, grid_x[38], grid_y[5]-cell_h/3, "0.00"
gr.text.draw ops_val, grid_x[38], grid_y[8]-cell_h/3, "0.00"
gr.text.draw hip_val, grid_x[38], grid_y[11]-cell_h/3, "0.00"
gr.text.draw ang_val, grid_x[38], grid_y[14]-cell_h/3, "0º"

gr.text.align 1
gr.text.draw Catxt, grid_x[30], grid_y[3]-2, "Catet (a): "
gr.rect InPutAdj, grid_x[30], grid_y[3], grid_x[39]~
, grid_y[5]

gr.text.draw Cotxt, grid_x[30], grid_y[6]-2, "Catet (b): "
gr.rect InPutOps, grid_x[30], grid_y[6], grid_x[39]~
, grid_y[8]

gr.text.draw Hiptxt, grid_x[30], grid_y[9]-2, "Hipotenuse (c): "
gr.rect InPutHip, grid_x[30], grid_y[9], grid_x[39]~
, grid_y[11]

gr.text.draw Angtxt, grid_x[30], grid_y[12]-2, "Angle (x): "
gr.rect InPutAng, grid_x[30], grid_y[12], grid_x[39]~
, grid_y[14]

gr.text.draw Angtxt, grid_x[33], grid_y[17]+cell_h/3, " Go!!"
gr.rect InPutOk, grid_x[32], grid_y[16], grid_x[37], grid_y[18]

gr.rect OutPutA, grid_x[1], grid_y[1], grid_x[29]~
, grid_y[19]

gr.color 255, 0, 0, 110, 1

gr.rect OutPutB, grid_x[1], grid_y[1], grid_x[29]~
, grid_y[19]

AngSpc = 3*cell_w

gr.set.stroke 4
% INITIAL POSITIONS
gr.color 255, 255, 255, 255, 0
gr.rect Adja, grid_x[8], grid_y[10], grid_x[22], grid_y[10]
gr.rect Opos, grid_x[22], -10, grid_x[22], -10
gr.arc AngArc, grid_x[8]-AngSpc, -10, grid_x[8]+AngSpc, -10, 0, 0, 0

gr.set.stroke 1
% VALUES IN THE OUTPUT WINDOW
gr.text.align 2
gr.text.draw TxtAdja, grid_x[15], grid_y[11], "a"
gr.rotate.start 90, grid_x[23], grid_y[10]
gr.text.draw TxtOps, grid_x[23], grid_y[10], "b"
gr.rotate.end
gr.text.align 3
gr.text.draw TxtHip, grid_x[15], grid_y[9], "c"
gr.text.draw TxtAng, grid_x[7], grid_y[10], "xº"

IF loaded = 1 THEN return
loaded = 1

DO
	gr.touch touched, touchx, touchy
	IF touched & touchx > grid_x[29]
		gosub setValues
	ENDIF
	gr.render
	PAUSE 10
UNTIL 0

drawTriangle:
	% SCALING THE VALUES TO FIT THE OUTPUT WINDOW
	caop_drw = FLOOR(caop)*(sc_caad/caad)
	hipo_drw = hipo*(sc_caad/caad)
	sp_caop = (sc_caop-caop_drw)/2
	
	% DRAWING THE TRIANGULE
	gr.modify Adja, "top", grid_y[19]-sp_caop
	gr.modify Adja, "bottom", grid_y[19]-sp_caop
	gr.modify Opos, "top", grid_x[1]+sp_caop
	gr.modify Opos, "bottom", grid_y[19]-sp_caop
	gr.modify AngArc, "top", grid_y[19]-sp_caop-AngSpc
	gr.modify AngArc, "bottom", grid_y[19]-sp_caop+AngSpc
	gr.modify AngArc, "sweep_angle", -angle
	
	gr.set.stroke 4
	gr.rotate.start -angle, grid_x[8], grid_y[19]-sp_caop
	gr.rect Hipot, grid_x[8], grid_y[19]-sp_caop, grid_x[8]+hipo_drw, grid_y[19]-sp_caop
	gr.rotate.end
	
	% VALUES IN THE OUTPUT WINDOW
	gr.modify TxtAdja, "text", caad$
	gr.modify TxtOps, "text", caop$
	gr.modify TxtHip, "text", hipo$
	gr.modify TxtAng, "text", angle$
	
	gr.modify TxtAdja, "y", grid_y[20]-sp_caop
	gr.modify TxtAng, "y", grid_y[19]-sp_caop
	
	gr.render
	
	% ZERANDO OS VALORES
	caad = 0
	caop = 0
	hipo = 0
	angle = 0
return

setValues:
	IF touchy >= grid_y[3] & touchy <= grid_y[5]
		gr.front 0
		INPUT "Adjacent catet?", caad
		gr.front 1
		caop = 0
		hipo = 0
	ELSE IF touchy >= grid_y[6] & touchy <= grid_y[8]
		gr.front 0
		INPUT "Oposite catet?", caop
		gr.front 1
		caad = 0
		hipo = 0
	ELSE IF touchy >= grid_y[9] & touchy <= grid_y[11]
		gr.front 0
		INPUT "Hipotenuse?", hipo
		gr.front 1
		caad = 0
		caop = 0
	ELSE IF touchy >= grid_y[12] & touchy <= grid_y[14]
		gr.front 0
		INPUT "Alpha angle?", angle
		gr.front 1
	ELSE IF touchy >= grid_y[16] & touchy <= grid_y[18]
		IF angle <= 45 & angle > 0 & (caad+caop+hipo) > 0
			IF caad > 0
				caop = caop(caad,hipo,angle,pi)
				hipo = hipo(caad,0,angle,pi)
			ELSE IF caop > 0
				caad = caad(caop,hipo,angle,pi)
				hipo = hipo(0,caop,angle,pi)
			ELSE
				caad = caad(0,hipo,angle,pi)
				caop = caop(0,hipo,angle,pi)
			ENDIF
			gosub reload
			gosub draw_board
			gosub drawTriangle
		ELSE
			POPUP "The angle must be between 1 and 45!", 0, 0, 0
			PAUSE 2000
		ENDIF
	ENDIF
	gosub draw_board
return

draw_board:
	caad$ = FORMAT$("######%.##",caad)
	caop$ = FORMAT$("######%.##",caop)
	hipo$ = FORMAT$("######%.##",hipo)
	angle$ = FORMAT$("#%º",angle)
	gr.modify adj_val, "text", caad$
	gr.modify ops_val, "text", caop$
	gr.modify hip_val, "text", hipo$
	gr.modify ang_val, "text", angle$
return
