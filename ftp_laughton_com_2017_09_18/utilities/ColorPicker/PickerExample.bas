! Picker example
! Aat Don @2015
! Thanks to Brochi for several enhancements !
GR.OPEN 255,192,192,192,0,1
GR.SCREEN w,h
ScaleX=600
ScaleY=h/w*ScaleX
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
GR.SET.ANTIALIAS 1
ColInt=255
CircRad=100
XOffSet=50
YOffSet=50

GR.TEXT.SIZE 20
GR.BITMAP.CREATE Intro,450,345
GR.BITMAP.DRAWINTO.START Intro
	GR.COLOR 255,0,0,0,1
	GR.TEXT.DRAW g,5,30,"Welcome to Picker Example"
GR.BITMAP.DRAWINTO.END
GR.BITMAP.DRAW PicIntro,Intro,50,50
GR.RENDER
GOSUB ShowIntro
PAUSE 3000
FOR qq=1 TO 6
	GOSUB WaitTab
	GOSUB ShowIntro
	GOSUB qq,CreateSeg1,CreateSeg2,CreateSeg3,CreateSeg4,CreateSeg5,CreateSeg6
NEXT qq
GOSUB CreateRest
GOSUB WaitTab
GR.HIDE PicIntro
GR.RENDER
GR.TEXT.SIZE 30
GR.COLOR 255,255,0,0,0
GR.TEXT.DRAW g,80,80,"MAKE A"
GR.COLOR 255,0,127,0,0
GR.TEXT.DRAW g,80,120,"BETTER"
GR.COLOR 255,0,0,255,0
GR.TEXT.DRAW g,80,160,"MONDRIAN"
GR.RENDER
GR.BITMAP.DELETE Intro
PAUSE 5000
GR.SET.STROKE 10
GR.COLOR 255,0,0,0,0
GR.BITMAP.CREATE Piet,450,540
GR.BITMAP.DRAWINTO.START Piet
	GR.RECT g,5,5,445,535
	GR.LINE g,115,5,115,535
	GR.LINE g,5,390,445,390
	GR.LINE g,5,120,115,120
	GR.LINE g,370,390,370,535
	GR.LINE g,370,460,445,460
GR.BITMAP.DRAWINTO.END
GR.BITMAP.DRAW Mondriaan,Piet,50,300
GR.COLOR 255,255,255,255,1
GR.BITMAP.FILL Piet,100,100
GR.BITMAP.FILL Piet,100,270
GR.BITMAP.FILL Piet,300,440
GR.BITMAP.FILL Piet,400,440
GR.COLOR 255,0,0,255,1
GR.BITMAP.FILL Piet,100,440
GR.COLOR 255,255,0,0,1
GR.BITMAP.FILL Piet,300,100
GR.COLOR 255,255,255,0,1
GR.BITMAP.FILL Piet,400,500
GR.TEXT.SIZE 16
GR.COLOR 255,0,0,0,1
GR.TEXT.DRAW g,50,870,"Composition II in RED, BLUE AND YELLOW (1930)"
GR.TEXT.DRAW g,350,150,"Select a color then"
GR.TEXT.DRAW g,350,180,"the area to recolor"
GR.TEXT.DRAW g,450,80,"QUIT"
GR.COLOR 255,0,0,0,0
GR.SET.STROKE 1
GR.RECT g,440,60,500,90
GR.RENDER
GOSUB ShowColorPicker

DO
	GOSUB GetPickedColor
	IF y>300 THEN
		GR.GET.BMPIXEL Piet,x-50,y-300,a,rP,gP,bP
		IF rP+gP+bP>0 THEN
			IF r+g+b=0 THEN
				f=1
			ELSE
				f=0
			ENDIF
			GR.COLOR 255,r,g+f,b,1
			GR.BITMAP.FILL Piet,x-50,y-300
			GR.RENDER
		ENDIF
	ENDIF
UNTIL x>440 & x<500 & y<90 & y>60

GR.CLOSE
EXIT

GetTouch:
	DO
		GR.TOUCH touched,Rx,Ry
	UNTIL touched
	x=ROUND(Rx/sx):y=ROUND(Ry/sy)
RETURN
CreateSeg1:
	! 1st part
	GR.SET.STROKE 2
	GR.BITMAP.CREATE ColCirc,CircRad*2+2,CircRad*2+2
	GR.BITMAP.DRAWINTO.START ColCirc
		GR.COLOR 255,0,0,0,1
		GR.RECT g,0,0,CircRad*2+2,CircRad*2+2
		FOR q=CircRad TO 0 STEP -1
			FOR P=0.0001 TO 60 STEP 100/(q+1)
				CosP=COS(TORADIANS(P)):SinP=SIN(TORADIANS(P))
				GOSUB HueSegment1
				S=q/CircRad:cI=1-S:R=(R*S)+cI:G=(G*S)+cI:B=(B*S)+cI
				GR.COLOR 255,R*255,G*255,B*255,1
				GR.POINT g,CircRad+q*CosP+1,CircRad+q*SinP+1
			NEXT P
		NEXT q
	GR.BITMAP.DRAWINTO.END
RETURN
CreateSeg2:
	! 2nd part
	GR.BITMAP.DRAWINTO.START ColCirc
		FOR q=CircRad TO 0 STEP -1
			FOR P=60.0001 TO 120 STEP 100/(q+1)
				CosP=COS(TORADIANS(P)):SinP=SIN(TORADIANS(P))
				GOSUB int(P/60)+1,HueSegment1,HueSegment2,HueSegment3,HueSegment4,HueSegment5,HueSegment6
				S=q/CircRad:cI=1-S:R=(R*S)+cI:G=(G*S)+cI:B=(B*S)+cI
				GR.COLOR 255,R*255,G*255,B*255,1
				GR.POINT g,CircRad+q*CosP+1,CircRad+q*SinP+1
			NEXT P
		NEXT q
	GR.BITMAP.DRAWINTO.END
RETURN
CreateSeg3:
	! 3rd part
	GR.BITMAP.DRAWINTO.START ColCirc
		FOR q=CircRad TO 0 STEP -1
			FOR P=120.0001 TO 180 STEP 100/(q+1)
				CosP=COS(TORADIANS(P)):SinP=SIN(TORADIANS(P))
				GOSUB int(P/60)+1,HueSegment1,HueSegment2,HueSegment3,HueSegment4,HueSegment5,HueSegment6
				S=q/CircRad:cI=1-S:R=(R*S)+cI:G=(G*S)+cI:B=(B*S)+cI
				GR.COLOR 255,R*255,G*255,B*255,1
				GR.POINT g,CircRad+q*CosP+1,CircRad+q*SinP+1
			NEXT P
		NEXT q
	GR.BITMAP.DRAWINTO.END
RETURN
CreateSeg4:
	! 3rd part
	GR.BITMAP.DRAWINTO.START ColCirc
		FOR q=CircRad TO 0 STEP -1
			FOR P=180.0001 TO 240 STEP 100/(q+1)
				CosP=COS(TORADIANS(P)):SinP=SIN(TORADIANS(P))
				GOSUB int(P/60)+1,HueSegment1,HueSegment2,HueSegment3,HueSegment4,HueSegment5,HueSegment6
				S=q/CircRad:cI=1-S:R=(R*S)+cI:G=(G*S)+cI:B=(B*S)+cI
				GR.COLOR 255,R*255,G*255,B*255,1
				GR.POINT g,CircRad+q*CosP+1,CircRad+q*SinP+1
			NEXT P
		NEXT q
	GR.BITMAP.DRAWINTO.END
RETURN
CreateSeg5:
	! 3rd part
	GR.BITMAP.DRAWINTO.START ColCirc
		FOR q=CircRad TO 0 STEP -1
			FOR P=240.0001 TO 300 STEP 100/(q+1)
				CosP=COS(TORADIANS(P)):SinP=SIN(TORADIANS(P))
				GOSUB int(P/60)+1,HueSegment1,HueSegment2,HueSegment3,HueSegment4,HueSegment5,HueSegment6
				S=q/CircRad:cI=1-S:R=(R*S)+cI:G=(G*S)+cI:B=(B*S)+cI
				GR.COLOR 255,R*255,G*255,B*255,1
				GR.POINT g,CircRad+q*CosP+1,CircRad+q*SinP+1
			NEXT P
		NEXT q
	GR.BITMAP.DRAWINTO.END
RETURN
CreateSeg6:
	! 3rd part
	GR.BITMAP.DRAWINTO.START ColCirc
		FOR q=CircRad TO 0 STEP -1
			FOR P=300.0001 TO 360 STEP 100/(q+1)
				CosP=COS(TORADIANS(P)):SinP=SIN(TORADIANS(P))
				GOSUB int(P/60)+1,HueSegment1,HueSegment2,HueSegment3,HueSegment4,HueSegment5,HueSegment6
				S=q/CircRad:cI=1-S:R=(R*S)+cI:G=(G*S)+cI:B=(B*S)+cI
				GR.COLOR 255,R*255,G*255,B*255,1
				GR.POINT g,CircRad+q*CosP+1,CircRad+q*SinP+1
			NEXT P
		NEXT q
	GR.BITMAP.DRAWINTO.END
	GR.SET.STROKE 0
	GR.BITMAP.DRAW HueCircle,ColCirc,XOffSet,YOffSet
RETURN
CreateRest:
	! Create intensity circle
	GR.COLOR 255,0,0,0,1
	GR.CIRCLE ChgI,XOffSet+CircRad+1,YOffSet+CircRad+1,CircRad+2
	GR.MODIFY ChgI,"alpha",0
	! Create Marker
	GR.COLOR 255,0,0,0,0
	GR.CIRCLE Marker,XOffSet+CircRad+1,YOffSet+CircRad+1,CircRad/20
	! Create Intensity selector
	GR.BITMAP.CREATE ShowInt,CircRad*0.4,CircRad*1.6
	GR.BITMAP.DRAWINTO.START ShowInt
		GR.COLOR 255,0,0,0,1
		GR.RECT g,0,0,CircRad*0.4,CircRad*1.6
		GR.COLOR 255,255,255,255,0
		GR.RECT g,0,0,CircRad*0.4,CircRad*1.6
		GR.COLOR 255,255,255,255,1
		GR.LINE g,CircRad*0.2,CircRad*0.05,CircRad*0.2,CircRad*1.55
	GR.BITMAP.DRAWINTO.END
	GR.BITMAP.DRAW SelInt,ShowInt,XOffSet+CircRad*2.1,YOffSet
	GR.COLOR 255,255,255,255,1
	IntStp=CircRad*1.5/256:KnobTop=CircRad*0.05
	GR.RECT Knob,XOffSet+CircRad*2.2,YOffSet+KnobTop+ColInt*IntStp-CircRad*0.03,XOffSet+CircRad*2.4,YOffSet+KnobTop+ColInt*IntStp+CircRad*0.03
	! Selected color box
	GR.BITMAP.CREATE ShowCol,CircRad*0.4,CircRad*0.4+2
	GR.BITMAP.DRAWINTO.START ShowCol
		GR.COLOR 255,255,255,255,1
		GR.RECT g,0,0,CircRad*0.4,CircRad*0.4+2
		GR.COLOR 255,0,0,0,0
		GR.RECT CBox,0,0,CircRad*0.4,CircRad*0.4+2
	GR.BITMAP.DRAWINTO.END
	GR.BITMAP.DRAW CurCol,ShowCol,XOffSet+CircRad*2.1,YOffSet+CircRad*1.6+2
	! RGB text
	GR.SET.STROKE 1
	GR.TEXT.SIZE CircRad/4
	GR.TEXT.BOLD 1
	GR.TEXT.TYPEFACE 2,2
	GR.COLOR 255,255,255,255,1
	GR.TEXT.DRAW RGBtxt1,XOffSet,YOffSet+CircRad*2+CircRad*0.25,"RGB "
	GR.TEXT.WIDTH tw,"RGB ="
	GR.COLOR 255,0,0,0,0
	GR.TEXT.DRAW RGBtxt2,XOffSet,YOffSet+CircRad*2+CircRad*0.25,"RGB "
	GR.COLOR 255,255,255,255,1
	GR.TEXT.DRAW RGBValues1,XOffSet+tw+CircRad*0.05,YOffSet+CircRad*2+CircRad*0.25,"255,255,255"
	GR.COLOR 255,0,0,0,0
	GR.TEXT.DRAW RGBValues2,XOffSet+tw+CircRad*0.05,YOffSet+CircRad*2+CircRad*0.25,"255,255,255"
	GR.SET.STROKE 0
	GR.GROUP ColorPicker,HueCircle,ChgI,Marker,SelInt,Knob,CurCol,RGBtxt1,RGBtxt2,RGBValues1,RGBValues2
	GR.HIDE ColorPicker
	! Get paints
	GR.COLOR 255,0,0,0,0
	GR.PAINT.GET Black
	GR.COLOR 255,255,255,255,0
	GR.PAINT.GET White
	GR.GETDL DispP[],1
	ARRAY.LENGTH PN,DispP[]
RETURN
HueSegment1::R=1:B=0:G=P/60:RETURN
HueSegment2::G=1:B=0:R=1-((P-60)/60):RETURN
HueSegment3::G=1:R=0:B=(P-120)/60:RETURN
HueSegment4::B=1:R=0:G=1-(P-180)/60:RETURN
HueSegment5::B=1:G=0:R=(P-240)/60:RETURN
HueSegment6::R=1:G=0:B=1-(P-300)/60:RETURN
ShowColorPicker:
	GR.GETDL Temp[],1:ARRAY.LENGTH L,Temp[]
	DIM TempNew[L]
	ARRAY.COPY Temp[PN+1],TempNew[1]:ARRAY.COPY DispP[],TempNew[L-PN+1]
	GR.NEWDL TempNew[]
	ARRAY.DELETE Temp[]:ARRAY.DELETE TempNew[]
	GR.SHOW ColorPicker:GR.RENDER
	! Init colors
	r=255:g=255:b=255
RETURN
GetPickedColor:
	DO
		GOSUB GetTouch
		! Set Intensity
		IF x>XOffSet+CircRad*2.1 & x<XOffSet+CircRad*2.5 & y>YOffSet+CircRad*0.05 & y<YOffSet+CircRad*1.55 THEN
			ColInt=INT((y-(YOffSet+CircRad*0.05))/IntStp)
			GR.MODIFY Knob,"top",YOffSet+KnobTop+ColInt*IntStp-CircRad*0.03
			GR.MODIFY Knob,"bottom",YOffSet+KnobTop+ColInt*IntStp+CircRad*0.03
			GR.MODIFY ChgI,"alpha",255-ColInt
			GR.GET.POSITION Marker,Rx,Ry
			GR.GET.PIXEL ROUND(Rx*sx),ROUND(Ry*sy),a,r,g,b
			GR.COLOR a,r,g,b,1
			GR.BITMAP.FILL ShowCol,CircRad*0.2,CircRad*0.2
			CVal$=INT$(r)+","+INT$(g)+","+INT$(b)
			GR.MODIFY RGBValues1,"text",CVal$
			GR.MODIFY RGBValues2,"text",CVal$
			IF ColInt<128 THEN
				GR.MODIFY Marker,"paint",White
			ELSE
				GR.MODIFY Marker,"paint",Black
			ENDIF
		ENDIF
		! Pick color (from within circle)
		IF (x-CircRad-XOffSet)^2+(y-CircRad-YOffSet)^2<=CircRad^2 THEN
			GR.HIDE Marker
			GR.GET.PIXEL Rx,Ry,a,r,g,b
			GR.MODIFY Marker,"x",x,"y",y
			GR.SHOW Marker
			GR.COLOR a,r,g,b,1
			GR.BITMAP.FILL ShowCol,CircRad*0.2,CircRad*0.2
			CVal$=INT$(r)+","+INT$(g)+","+INT$(b)
			GR.MODIFY RGBValues1,"text",CVal$
			GR.MODIFY RGBValues2,"text",CVal$
		ENDIF
		GR.RENDER
		PAUSE 100
	UNTIL (x>50 & x<500 & y>300 &y<840) | (x>440 & x<500 & y<90 & y>60)
RETURN
ShowIntro:
	GR.BITMAP.DRAWINTO.START Intro
		GR.COLOR 255,192,192,192,1
		GR.RECT g,0,40,450,500
		GR.COLOR 255,0,0,0,1
		FOR i=1 TO 8
			READ.NEXT A$
			GR.TEXT.DRAW g,5,50+i*30,A$
		NEXT i
	GR.BITMAP.DRAWINTO.END
	GR.RENDER
RETURN
WaitTab:
	GR.BITMAP.DRAWINTO.START Intro
		GR.COLOR 255,0,0,0,1
		GR.TEXT.DRAW g,5,340,"TAP SCREEN TO CONTINUE"
		GR.RENDER
	GR.BITMAP.DRAWINTO.END
	GOSUB GetTouch
RETURN
! Text taken from Wikipedia....
READ.DATA "Pieter Cornelis 'Piet' Mondriaan, after 1906"
READ.DATA "Mondrian (March 7, 1872 – February 1, 1944),"
READ.DATA "was a Dutch painter. Mondrian was born in"
READ.DATA "Amersfoort in the Netherlands, the second"
READ.DATA "of his parents' children.He was descended"
READ.DATA "from Christian Dirkzoon Monderyan who lived"
READ.DATA "in The Hague as early as 1670."
READ.DATA ""
READ.DATA "The family moved to Winterswijk in the east"
READ.DATA "of the country when his father, Pieter"
READ.DATA "Cornelius Mondriaan, was appointed Head"
READ.DATA "Teacher at a local primary school."
READ.DATA "Mondrian was introduced to art from a very"
READ.DATA "early age."
READ.DATA ""
READ.DATA ""
READ.DATA "His father was a qualified drawing"
READ.DATA "teacher, and, with his uncle, Fritz Mondriaan"
READ.DATA "(a pupil of Willem Maris of the Hague School"
READ.DATA "of artists), the younger Piet often painted"
READ.DATA "and drew along the river Gein."
READ.DATA "After a strictly Protestant upbringing,"
READ.DATA "in 1892, Mondrian entered the Academy for"
READ.DATA "Fine Art in Amsterdam."
READ.DATA "He already was qualified as a teacher."
READ.DATA "He began his career as a teacher in primary"
READ.DATA "education, but he also practiced painting."
READ.DATA "Most of his work from this period is"
READ.DATA "naturalistic or Impressionistic,"
READ.DATA "consisting largely of landscapes."
READ.DATA ""
READ.DATA ""
READ.DATA "These pastoral images of his native"
READ.DATA "country depict windmills, fields,"
READ.DATA "and rivers, initially in the Dutch"
READ.DATA "Impressionist manner of the Hague School"
READ.DATA "and then in a variety of styles and"
READ.DATA "techniques that attest to his search for"
READ.DATA "a personal style."
READ.DATA ""
READ.DATA "These paintings are most definitely"
READ.DATA "representational, and they illustrate"
READ.DATA "the influence that various artistic"
READ.DATA "movements had on Mondrian, including"
READ.DATA "pointillism and the vivid colors"
READ.DATA "of Fauvism."
READ.DATA ""
READ.DATA ""
READ.DATA "This app offers you the opportunity"
READ.DATA "to improve Mondrian's well known"
READ.DATA "painting 'Composition II'."
READ.DATA "You do this by selecting a color from"
READ.DATA "the offered color picker and tapping"
READ.DATA "on the area you want to 'recolor'."
READ.DATA "Don't worry; your 'Work of Art' will"
READ.DATA "not be saved.... Have fun."
