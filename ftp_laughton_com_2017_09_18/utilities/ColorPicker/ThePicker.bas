! Color picker
! Aat Don @2015
! Thanks to Brochi for several enhancements !
GR.OPEN 255,192,192,192,0,1
GR.SCREEN w,h
ScaleX=600
ScaleY=h/w*ScaleX
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
GR.SET.ANTIALIAS 1
ColInt=255
! ####################################
! Change these vars to suit your needs
CircRad=100
XOffSet=50
YOffSet=50
! ####################################
GOSUB CreateCircle
! Program here
GOSUB ShowColorPicker
GOSUB GetPickedColor
! Use selected color here
GR.CLOSE
EXIT

GetTouch:
	DO
		GR.TOUCH touched,Rx,Ry
	UNTIL touched
	x=ROUND(Rx/sx):y=ROUND(Ry/sy)
RETURN
CreateCircle:
	! Create HSI color circle
	GR.SET.STROKE 2
	GR.BITMAP.CREATE ColCirc,CircRad*2+2,CircRad*2+2
	GR.BITMAP.DRAWINTO.START ColCirc
		GR.COLOR 255,0,0,0,1
		GR.RECT g,0,0,CircRad*2+2,CircRad*2+2
		FOR q=CircRad TO 0 STEP -1
			FOR P=0.0001 TO 360 STEP 100/(q+1)
				CosP=COS(TORADIANS(P)):SinP=SIN(TORADIANS(P))
				GOSUB int(P/60)+1,HueSegment1,HueSegment2,HueSegment3,HueSegment4,HueSegment5,HueSegment6
				S=q/CircRad:cI=1-S:R=(R*S)+cI:G=(G*S)+cI:B=(B*S)+cI
				GR.COLOR 255,R*255,G*255,B*255,1
				GR.POINT g,CircRad+q*CosP+1,CircRad+q*SinP+1
			NEXT P
		NEXT q
	GR.BITMAP.DRAWINTO.END
	GR.BITMAP.SAVE ColCirc,"ColCirc.png"
	GR.SET.STROKE 0
	GR.BITMAP.DRAW HueCircle,ColCirc,XOffSet,YOffSet
	! Create intensity circle
	GR.COLOR 255,0,0,0,1
	GR.CIRCLE ChgI,XOffSet+CircRad+1,YOffSet+CircRad+1,CircRad+2
	GR.MODIFY ChgI,"alpha",0
	! Create Marker
	GR.COLOR 255,0,0,0,0
	GR.CIRCLE Marker,XOffSet+CircRad+1,YOffSet+CircRad+1,CircRad/20
	! Create Intensity selector
	GR.BITMAP.CREATE ShowInt,CircRad*0.4,CircRad*1.6
	GR.BITMAP.DRAWINTO.START ShowInt
		GR.COLOR 255,0,0,0,1
		GR.RECT g,0,0,CircRad*0.4,CircRad*1.6
		GR.COLOR 255,255,255,255,0
		GR.RECT g,0,0,CircRad*0.4,CircRad*1.6
		GR.COLOR 255,255,255,255,1
		GR.LINE g,CircRad*0.2,CircRad*0.05,CircRad*0.2,CircRad*1.55
	GR.BITMAP.DRAWINTO.END
	GR.BITMAP.DRAW SelInt,ShowInt,XOffSet+CircRad*2.1,YOffSet
	GR.COLOR 255,255,255,255,1
	IntStp=CircRad*1.5/256:KnobTop=CircRad*0.05
	GR.RECT Knob,XOffSet+CircRad*2.2,YOffSet+KnobTop+ColInt*IntStp-CircRad*0.03,XOffSet+CircRad*2.4,YOffSet+KnobTop+ColInt*IntStp+CircRad*0.03
	! Selected color box
	GR.BITMAP.CREATE ShowCol,CircRad*0.4,CircRad*0.4+2
	GR.BITMAP.DRAWINTO.START ShowCol
		GR.COLOR 255,255,255,255,1
		GR.RECT g,0,0,CircRad*0.4,CircRad*0.4+2
		GR.COLOR 255,0,0,0,0
		GR.RECT CBox,0,0,CircRad*0.4,CircRad*0.4+2
	GR.BITMAP.DRAWINTO.END
	GR.BITMAP.DRAW CurCol,ShowCol,XOffSet+CircRad*2.1,YOffSet+CircRad*1.6+2
	! RGB text
	GR.SET.STROKE 1
	GR.TEXT.SIZE CircRad/4
	GR.TEXT.BOLD 1
	GR.TEXT.TYPEFACE 2,2
	GR.COLOR 255,255,255,255,1
	GR.TEXT.DRAW RGBtxt1,XOffSet,YOffSet+CircRad*2+CircRad*0.25,"RGB "
	GR.TEXT.WIDTH tw,"RGB ="
	GR.COLOR 255,0,0,0,0
	GR.TEXT.DRAW RGBtxt2,XOffSet,YOffSet+CircRad*2+CircRad*0.25,"RGB "
	GR.COLOR 255,255,255,255,1
	GR.TEXT.DRAW RGBValues1,XOffSet+tw+CircRad*0.05,YOffSet+CircRad*2+CircRad*0.25,"255,255,255"
	GR.COLOR 255,0,0,0,0
	GR.TEXT.DRAW RGBValues2,XOffSet+tw+CircRad*0.05,YOffSet+CircRad*2+CircRad*0.25,"255,255,255"
	GR.SET.STROKE 0
	GR.GROUP ColorPicker,HueCircle,ChgI,Marker,SelInt,Knob,CurCol,RGBtxt1,RGBtxt2,RGBValues1,RGBValues2
	GR.HIDE ColorPicker
	! Get paints
	GR.COLOR 255,0,0,0,0
	GR.PAINT.GET Black
	GR.COLOR 255,255,255,255,0
	GR.PAINT.GET White
	GR.GETDL DispP[],1
	ARRAY.LENGTH PN,DispP[]
RETURN
HueSegment1::R=1:B=0:G=P/60:RETURN
HueSegment2::G=1:B=0:R=1-((P-60)/60):RETURN
HueSegment3::G=1:R=0:B=(P-120)/60:RETURN
HueSegment4::B=1:R=0:G=1-(P-180)/60:RETURN
HueSegment5::B=1:G=0:R=(P-240)/60:RETURN
HueSegment6::R=1:G=0:B=1-(P-300)/60:RETURN
ShowColorPicker:
	GR.GETDL Temp[],1:ARRAY.LENGTH L,Temp[]
	DIM TempNew[L]
	ARRAY.COPY Temp[PN+1],TempNew[1]:ARRAY.COPY DispP[],TempNew[L-PN+1]
	GR.NEWDL TempNew[]
	ARRAY.DELETE Temp[]:ARRAY.DELETE TempNew[]
	GR.SHOW ColorPicker:GR.RENDER
	! Init colors
	r=255:g=255:b=255
RETURN
GetPickedColor:
	DO
		GOSUB GetTouch
		! Set Intensity
		IF x>XOffSet+CircRad*2.1 & x<XOffSet+CircRad*2.5 & y>YOffSet+CircRad*0.05 & y<YOffSet+CircRad*1.55 THEN
			ColInt=INT((y-(YOffSet+CircRad*0.05))/IntStp)
			GR.MODIFY Knob,"top",YOffSet+KnobTop+ColInt*IntStp-CircRad*0.03
			GR.MODIFY Knob,"bottom",YOffSet+KnobTop+ColInt*IntStp+CircRad*0.03
			GR.MODIFY ChgI,"alpha",255-ColInt
			GR.GET.POSITION Marker,Rx,Ry
			GR.GET.PIXEL ROUND(Rx*sx),ROUND(Ry*sy),a,r,g,b
			GR.COLOR a,r,g,b,1
			GR.BITMAP.FILL ShowCol,CircRad*0.2,CircRad*0.2
			CVal$=INT$(r)+","+INT$(g)+","+INT$(b)
			GR.MODIFY RGBValues1,"text",CVal$
			GR.MODIFY RGBValues2,"text",CVal$
			IF ColInt<128 THEN
				GR.MODIFY Marker,"paint",White
			ELSE
				GR.MODIFY Marker,"paint",Black
			ENDIF
		ENDIF
		! Pick color (from within circle)
		IF (x-CircRad-XOffSet)^2+(y-CircRad-YOffSet)^2<=CircRad^2 THEN
			GR.HIDE Marker
			GR.GET.PIXEL Rx,Ry,a,r,g,b
			GR.MODIFY Marker,"x",x,"y",y
			GR.SHOW Marker
			GR.COLOR a,r,g,b,1
			GR.BITMAP.FILL ShowCol,CircRad*0.2,CircRad*0.2
			CVal$=INT$(r)+","+INT$(g)+","+INT$(b)
			GR.MODIFY RGBValues1,"text",CVal$
			GR.MODIFY RGBValues2,"text",CVal$
		ENDIF
		GR.RENDER
		PAUSE 100
	UNTIL x>XOffSet+CircRad*2.1 & x<XOffSet+CircRad*2.1+CircRad*0.4 & y>YOffSet+CircRad*1.6+2 &y<YOffSet+CircRad*1.6+2+CircRad*0.4+2
RETURN
