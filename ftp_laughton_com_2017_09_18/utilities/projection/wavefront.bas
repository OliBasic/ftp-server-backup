include projection.bas

FN.DEF loadmodel(fn$)

 BUNDLE.CREATE o

 TEXT.OPEN r, f, fn$
 WHILE !(in$="EOF")
  TEXT.READLN f, in$
  IF !STARTS_WITH("#", in$) THEN
   SPLIT nu$[], in$, " "
   SW.BEGIN nu$[1]
    SW.CASE "v"
     vertices=vertices+1
     SPLIT pts$[], nu$[2], "/"
     BUNDLE.PUT o, "v"+STR$(vertices)+"x", VAL(pts$[1])
     BUNDLE.PUT o, "v"+STR$(vertices)+"y", VAL(pts$[2])
     BUNDLE.PUT o, "v"+STR$(vertices)+"z", VAL(pts$[3])
     ARRAY.DELETE pts$[]
     SW.BREAK
    SW.CASE "f"
     faces=faces+1
     SPLIT pts$[], nu$[2], "/"
     ARRAY.LENGTH nu, pts$[]
     temp$=pts$[1]
     FOR i=2 TO nu
      temp$=temp$+"/"+pts$[i]
     NEXT i
     ARRAY.DELETE pts$[]
     BUNDLE.PUT o, "f"+STR$(faces), temp$
     SW.BREAK
    SW.CASE "c"
     SPLIT pts$[], nu$[3], "/"
     BUNDLE.PUT o, "cf"+nu$[2]+".0r", VAL(pts$[1])
     BUNDLE.PUT o, "cf"+nu$[2]+".0g", VAL(pts$[2])
     BUNDLE.PUT o, "cf"+nu$[2]+".0b", VAL(pts$[3])
     ARRAY.DELETE pts$[]
     SW.BREAK
   SW.END
   ARRAY.DELETE nu$[]
  ENDIF
 REPEAT

 BUNDLE.PUT o, "faces", faces

 FN.RTN o
FN.END

FN.DEF drawmodel(obj)
 BUNDLE.GET obj, "faces", faces
 FOR i=1 TO faces
  LIST.CREATE n, poly
  BUNDLE.GET obj, "f"+STR$(i), face$
  SPLIT face$[], face$, "/"
  ARRAY.LENGTH nu, face$[]
  FOR j=1 TO nu
   BUNDLE.GET obj, "v"+face$[j]+".0x", nux
   BUNDLE.GET obj, "v"+face$[j]+".0y", nuy
   BUNDLE.GET obj, "v"+face$[j]+".0z", nuz
   LIST.ADD poly, 100*projectx(nux, nuy, nuz, p_vx(), p_vy(), p_vz())
   LIST.ADD poly, 100*projecty(nux, nuy, nuz, p_vx(), p_vy(), p_vz())
  NEXT j
  ARRAY.DELETE face$[]
  BUNDLE.GET obj, "cf"+STR$(i)+"r", cr
  BUNDLE.GET obj, "cf"+STR$(i)+"g", cg
  BUNDLE.GET obj, "cf"+STR$(i)+"b", cb
  GR.COLOR 255, cr, cg, cb, 1
  GR.POLY nu, poly
 NEXT i
FN.END
