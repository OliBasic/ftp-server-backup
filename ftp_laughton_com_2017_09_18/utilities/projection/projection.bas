!if inc_projection_bas goto e_projection_bas else inc_projection_bas=1

FN.DEF projectx(x, y, z, vx, vy, vz)
 IF z=0 THEN FN.RTN x
 FN.RTN (x-vx)*(vz/z)
FN.END

FN.DEF projecty(x, y, z, vx, vy, vz)
 IF z=0 THEN FN.RTN y
 FN.RTN (y-vy)*(vz/z)
FN.END

fn.def p_vx()
 fn.rtn 0
fn.end

fn.def p_vy()
 fn.rtn 0
fn.end

fn.def p_vz()
 fn.rtn -2
fn.end

!e_projection_bas:
