! Simple light level meter : LuxMeter.bas
! Aat Don @2013
SENSORS.LIST List$[]
ARRAY.LENGTH Size, List$[]
SF=0
FOR i = 1 TO Size
  IF IS_IN("Type = 5",List$[i]) THEN SF=1
NEXT i
! Light sensor
IF SF=1 THEN
	SENSORS.OPEN 5
ELSE
	PRINT "Light sensor NOT found"
	END
ENDIF
GR.OPEN 150,0,0,255,0,0
GR.SCREEN w,h
ScaleX=720
ScaleY=h/w*ScaleX
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
GR.SET.ANTIALIAS 0
GR.SET.STROKE 0
gWidth=100
gHeight=50
sR=255
sG=0
sB=0
eR=0
eG=0
eB=255
RStep=(eR-sR)/(gWidth-1)
GStep=(eG-sG)/(gWidth-1)
BStep=(eB-sB)/(gWidth-1)
GR.BITMAP.CREATE PNTR,gWidth,gHeight
GR.BITMAP.DRAWINTO.START PNTR
	FOR x=0 TO gWidth-1
		GR.COLOR 255,sR,sG,sB,0
		GR.LINE gl,x,0,x,gHeight-1
		sR=sR+RStep
		sG=sG+GStep
		sB=sB+BStep
	NEXT x
GR.BITMAP.DRAWINTO.END
GR.SET.ANTIALIAS 1
GR.COLOR 255,255,255,0,0
GR.TEXT.SIZE 55
GR.TEXT.DRAW g,10,80,"Choose interval time"
GR.TEXT.DRAW g,250,300,"LUX Meter"
GR.BITMAP.DRAW BM1,PNTR,10,110
GR.BITMAP.DRAW BM2,PNTR,210,110
GR.BITMAP.DRAW BM3,PNTR,410,110
GR.TEXT.SIZE 30
GR.TEXT.DRAW g,40,145,"1 s"
GR.TEXT.DRAW g,220,145,"1 min"
GR.TEXT.DRAW g,430,145,"1 hr"
GR.RENDER
Interval=0
DO
	GR.TOUCH touched,x,y
  IF x>10*sx & y>110*sy & x<110*sx & y<160*sy THEN
    Unit$="(s)"
    Interval=1000
  ENDIF
  IF x>210*sx & y>110*sy & x<310*sx & y<160*sy THEN
    Unit$="(min)"
    Interval=60000
  ENDIF
  IF x>410*sx & y>110*sy & x<510*sx & y<160*sy THEN
    Unit$="(hr)"
    Interval=3600000
  ENDIF
UNTIL Interval>0
GR.CLS
GR.TEXT.TYPEFACE 2
GOSUB MakeScrn
GR.COLOR 255,255,0,0,1
GR.RENDER
WAKELOCK 3
DIM MeasValues[11]
MaxLux=0
YSc=1
MeasTime=TIME()
SENSORS.READ 5,Lux,Ly,Lz
MeasValues[1]=Lux
IF MeasValues[1]>MaxLux Then MaxLux=MeasValues[1]
IF MaxLux>0 THEN YSc=200/MaxLux
Signal=MeasValues[1]*YSc
GR.COLOR 255,255,255,0,1
GR.TEXT.DRAW g,20,340,STR$(Lux)
GR.TEXT.DRAW g,0,40,RIGHT$(FORMAT$("#######",Lux),7)
GR.COLOR 255,255,0,0,1
GR.CIRCLE g,150,230-Signal,3
GR.RENDER
TEXT.OPEN W, FN1,"Light.txt"
TEXT.WRITELN FN1,"Seconds - Lux"
TEXT.WRITELN FN1,TotalTime/1000;" - ";Lux
KAP=0
TotalTime=0
i=1
DO
  IF TIME()>=MeasTime+Interval THEN
    TotalTime=TotalTime+TIME()-MeasTime
    MeasTime=TIME()
    SENSORS.READ 5,Lux,Ly,Lz
    GR.CLS
    GOSUB MakeScrn
    MeasValues[i+1]=Lux
    ARRAY.MAX MaxLux,MeasValues[]
    GR.TEXT.DRAW g,0,40,RIGHT$(FORMAT$("#######",MaxLux),7)
    GR.TEXT.DRAW g,20,340,STR$(Lux)
    GR.COLOR 255,255,0,0,1
    IF MaxLux>0 THEN YSc=200/MaxLux
    FOR j=1 TO i
      GR.CIRCLE g,150+(j-1)*50,230-MeasValues[j]*YSc,3
      GR.LINE g,150+(j-1)*50,230-MeasValues[j]*YSc,150+(j)*50,230-MeasValues[j+1]*YSc
    NEXT j
    GR.CIRCLE g,150+(j-1)*50,230-MeasValues[j]*YSc,3
    GR.RENDER
    IF i<10 THEN
      i=i+1
    ELSE
      FOR j=1 TO 10
        MeasValues[j]=MeasValues[j+1]
      NEXT j
    ENDIF
    TEXT.WRITELN FN1,TotalTime/1000;" - ";Lux
  ENDIF
	GR.TOUCH touched,x,y
  IF x>250*sx & y>300*sy & x<350*sx & y<350*sy THEN
    KAP=1
  ENDIF
UNTIL KAP=1
GR.CLOSE
TEXT.CLOSE FN1
SENSORS.CLOSE
WAKELOCK 5
PRINT "Your light measurements have been saved to 'Light.txt'"
PRINT "Press Back key to exit"
END

MakeScrn:
GR.SET.STROKE 5
GR.COLOR 92,255,255,0,1
GR.RECT g,150,30,650,230
GR.COLOR 255,255,255,0,0
GR.RECT g,150,30,650,230
GR.LINE g,130,30,150,30
GR.LINE g,130,230,150,230
FOR j=150 TO 650 STEP 50
  GR.LINE g,j,230,j,250
NEXT j
GR.COLOR 255,255,255,0,1
GR.SET.STROKE 2
GR.TEXT.SIZE 30
GR.ROTATE.START 270,80,50
GR.TEXT.DRAW g,0,40,"Lux"
GR.ROTATE.END
GR.TEXT.DRAW g,500,290,"Time"
GR.TEXT.DRAW g,500,320,Unit$
GR.TEXT.DRAW g,110,240,"0"
GR.TEXT.DRAW g,145,280,"0"
GR.TEXT.DRAW g,635,280,"10"
GR.BITMAP.DRAW BM1,PNTR,250,300
GR.TEXT.DRAW g,260,335,"STOP"
GR.TEXT.DRAW g,20,300,"Latest"
RETURN
