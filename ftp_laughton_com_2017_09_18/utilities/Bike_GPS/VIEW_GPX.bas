! View logged GPX files
! Aat Don @2013
DataPath$="../../Bike_GPS/data/"
FILE.EXISTS DirPresent, DataPath$
IF DirPresent=0 THEN
	FILE.MKDIR DataPath$
ENDIF
FILE.DIR DataPath$, Namen$[]
Array.length NumFiles, Namen$[]
IF NumFiles<1 THEN
	PRINT "NO GPX files found !"
	END
ENDIF
SELECT Route, Namen$[], "Select a gpx file"

PRINT "Reading GPX data....."
PRINT "From file "; Namen$[Route]

MinXScale=0.01
MinYScale=0.005

LIST.CREATE N,LonS
LIST.CREATE N,LatS
LIST.CREATE N,PoiLon
LIST.CREATE N,PoiLat

FILE.SIZE GBytes,DataPath$+Namen$[Route]
TEXT.OPEN R,FX,DataPath$+Namen$[Route]
DO
	! Get way points (POI's)
	TEXT.READLN FX,Regel$
	IF IS_IN("<wpt",Regel$,1)>0 THEN
		! Haal  spaties en tabs weg
		Regel$=REPLACE$(Regel$,CHR$(9),"")
		Regel$=REPLACE$(Regel$," ","")
		s=IS_IN("lat=",Regel$,1)
		t=IS_IN("lon=",Regel$,s)
		e=IS_IN(">",Regel$,t)
		! Add data points to list
		LIST.ADD PoiLat,VAL(MID$(Regel$,s+5,t-s-6))
		LIST.ADD PoiLon,VAL(MID$(Regel$,t+5,e-t-6))
	ENDIF
UNTIL IS_IN("<trkseg>",Regel$,1)>0

ApproxPoints=ROUND(GBytes*0.007)
NS=ROUND(ApproxPoints/50)
DO
	! Get Track points
	TEXT.READLN FX,Regel$
	IF IS_IN("<trkpt",Regel$,1)>0 THEN
		! Remove spaces and tabs
		Regel$=REPLACE$(Regel$,CHR$(9),"")
		Regel$=REPLACE$(Regel$," ","")
		s=IS_IN("lat=",Regel$,1)
		t=IS_IN("lon=",Regel$,s)
		e=IS_IN(">",Regel$,t)
		! Add data points to list
		LIST.ADD LatS,VAL(MID$(Regel$,s+5,t-s-6))
		LIST.ADD LonS,VAL(MID$(Regel$,t+5,e-t-6))
		! Speed up by reducing number of points
		FOR i=1 TO NS
			FOR j=1 TO 5
				TEXT.READLN FX,Regel$
				IF IS_IN("</trkseg>",Regel$,1)>0 THEN F_n.BREAK
			NEXT j
			IF IS_IN("</trkseg>",Regel$,1)>0 THEN F_n.BREAK
		NEXT i
	ENDIF
UNTIL IS_IN("</trkseg>",Regel$,1)>0
TEXT.CLOSE FX

LIST.TOARRAY LonS,Longitude[]
LIST.TOARRAY LatS,Latitude[]
Array.MIN MinLon,Longitude[]
Array.MAX MaxLon,Longitude[]
Array.MIN MinLat,Latitude[]
Array.MAX MaxLat,Latitude[]
Array.LENGTH NumOfPoints,Longitude[]
LIST.SIZE POILon,LS
IF LS>0 THEN
	LIST.TOARRAY PoiLon,PLon[]
	LIST.TOARRAY PoiLat,PLat[]
	Array.LENGTH NumPOI,PLon[]
ENDIF
LIST.CLEAR LonS
LIST.CLEAR LatS
LIST.CLEAR PoiLon
LIST.CLEAR PoiLat

TONE 1000, 200
PRINT "Ready !"

! Draw trip
PRINT "Building trip ....."

GR.OPEN 255, 255, 255, 255, 1
GR.ORIENTATION 1
GR.SCREEN w, h

ScreenWidth=900
ScreenHeight=1200
sX=w/ScreenWidth
sY=h/ScreenHeight
GR.SCALE sX,sY

TopMargin=150
BottomMargin=250
LeftMargin=50
RightMargin=50
FieldW=ScreenWidth-LeftMargin-RightMargin
FieldH=ScreenHeight-TopMargin-BottomMargin

GR.TEXT.SIZE 50

GR.CLS
! Outline
GR.COLOR 255,0,0,255,0
GR.RECT p, LeftMargin, TopMargin, FieldW+LeftMargin, FieldH+TopMargin
GR.SET.STROKE 5
GR.TEXT.DRAW p, FieldW/2+LeftMargin, TopMargin-30, "N"
GR.TEXT.DRAW p, 0, FieldH/2+TopMargin, "W"
GR.TEXT.DRAW p, FieldW+LeftMargin+10, FieldH/2+TopMargin, "E"
GR.TEXT.DRAW p, FieldW/2+LeftMargin, FieldH+TopMargin+50, "S"
! Legend
GR.SET.STROKE 0
GR.TEXT.DRAW p, LeftMargin+50, FieldH+TopMargin+100, "Start"
GR.TEXT.DRAW p, LeftMargin+50, FieldH+TopMargin+150, "End"
GR.TEXT.DRAW p, LeftMargin+50, FieldH+TopMargin+200, "Begin of trip"
GR.TEXT.DRAW p, LeftMargin+280, FieldH+TopMargin+120, "POI"
! Start
GR.SET.STROKE 1
GR.COLOR 164,0,0,0,1
GR.CIRCLE p, LeftMargin, FieldH+TopMargin+90, 20
! End
GR.COLOR 164,255,0,0,1	
GR.CIRCLE p, LeftMargin, FieldH+TopMargin+140, 20
!POI
GR.SET.STROKE 10
GR.COLOR 255,0,0,0,0
GR.CIRCLE p, LeftMargin+230, FieldH+TopMargin+105, 25
! Begin
GR.COLOR 255,0,255,0,0
GR.LINE p, 0, FieldH+TopMargin+190, LeftMargin+25, FieldH+TopMargin+190		
GR.RENDER
! Choose x or y for scaling and center
! Correct for difference in length per degree (aspect ratio), well more or less....
! 1 degree Longitude ~ 111 km
! 1 degree Latitude ~ 82 km
Aspect=111/82
IF (MaxLon-MinLon) > (MaxLat-MinLat)*Aspect THEN
	ScaleF=(MaxLon-MinLon)
	XOffset=ROUND((FieldW-((MaxLon-MinLon)/ScaleF*FieldW))/2)
	YOffset=ROUND((FieldH-FieldW)/2)
ELSE
	ScaleF=(MaxLat-MinLat)*Aspect
	XOffset=ROUND((FieldW-((MaxLon-MinLon)/ScaleF*FieldW))/2)
	YOffset=ROUND((FieldW-((MaxLat-MinLat)*Aspect/ScaleF*FieldW))/2)+ROUND((FieldH-FieldW)/2)
ENDIF

! Draw starting point
GR.COLOR 164,0,0,0,1
Gx1=Longitude[1]
Gy1=Latitude[1]
sx=ROUND((Gx1-MinLon)/ScaleF*FieldW)+LeftMargin+XOffset
sy=ROUND(FieldW-(Gy1-MinLat)*Aspect/ScaleF*FieldW)+TopMargin+YOffset
GR.CIRCLE p, sx, sy, 20

! Draw logged trip
GR.COLOR 255,0,255,0,0
GR.SET.STROKE 10
FOR i=1 TO NumOfPoints-1
	Gx1=Longitude[i]
	Gy1=Latitude[i]
	Gx2=Longitude[i+1]
	Gy2=Latitude[i+1]
	x1=ROUND((Gx1-MinLon)/ScaleF*FieldW)+LeftMargin+XOffset
	y1=ROUND(FieldW-(Gy1-MinLat)*Aspect/ScaleF*FieldW)+TopMargin+YOffset
	x2=ROUND(((Gx2-MinLon)/ScaleF)*FieldW)+LeftMargin+XOffset
	y2=ROUND(FieldW-((Gy2-MinLat)*Aspect/ScaleF)*FieldW)+TopMargin+YOffset
	! Draw connecting line, start with green
	IF i>NumOfPoints/10 THEN
		GR.COLOR 255,0,0,255,0
	ENDIF
	GR.LINE p, x1, y1, x2, y2		
	GR.RENDER
NEXT i

GR.SET.STROKE 1
! Draw endpoint
GR.COLOR 164,255,0,0,1	
GR.CIRCLE p, x2, y2, 20

GR.SET.STROKE 10
! Draw POI
GR.COLOR 255,0,0,0,0
FOR i=1 TO NumPoi
	Gx1=PLon[i]
	Gy1=PLat[i]
	x1=ROUND((Gx1-MinLon)/ScaleF*FieldW)+LeftMargin+XOffset
	y1=ROUND(FieldW-(Gy1-MinLat)*Aspect/ScaleF*FieldW)+TopMargin+YOffset
	! Draw POI cirle
	GR.CIRCLE p, x1, y1, 25	
	GR.RENDER
NEXT i

GR.SET.STROKE 1
! Draw END button
GR.COLOR 128,0,0,0,1
l=ScreenWidth-360
t=ScreenHeight-150
r=ScreenWidth-120
b=ScreenHeight-30
GR.RECT p, l, t, r, b
GR.SET.STROKE 5
GR.COLOR 255,0,0,0,0
GR.RECT p, l, t, r, b

! Button Text
GR.SET.STROKE 0
GR.COLOR 255,0,0,0,0
GR.TEXT.DRAW p, ScreenWidth-320, ScreenHeight-100, "TAP for"
GR.TEXT.DRAW p, ScreenWidth-300, ScreenHeight-50, "END"
GR.RENDER

Array.DELETE Longitude[]
Array.DELETE Latitude[]
Array.DELETE PLon[]
Array.DELETE PLat[]

! Tap for End
Finito=0
DO
	DO
		GR.TOUCH touched,x,y
	UNTIL touched
	DO
		GR.TOUCH touched,x,y
	UNTIL !touched
	IF x>=140 & y>=270 THEN
		Finito=1
	ENDIF
UNTIL Finito
TONE 1000, 200
EXIT
