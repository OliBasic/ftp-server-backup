! Edit Logged GPX files
! Aat Don @2013
GR.OPEN 255, 0, 0, 255, 0
GR.ORIENTATION 1
GR.SCREEN w, h
ScaleX=720
ScaleY=960
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy

GR.TEXT.BOLD 3
GR.TEXT.SIZE 60
DataPath$="../../Bike_GPS/data/"
KAP=0

GOTO Start
GetGPX:
	FILE.EXISTS DirPresent, DataPath$
	IF DirPresent=0 THEN
		FILE.MKDIR DataPath$
	ENDIF
	FILE.DIR DataPath$, Namen$[]
	Array.length NumFiles, Namen$[]
	IF NumFiles<1 THEN
		FileName$="None"
		RETURN
	ENDIF
	IF C$<>"" THEN
		Array.COPY Namen$[],NamenPlus$[1]
		NamenPlus$[NumFiles+1]=C$
		C$=""
	ELSE
		Array.COPY Namen$[],NamenPlus$[]
	ENDIF
	Array.DELETE Namen$[]
	SELECT Route, NamenPlus$[], "Select a gpx file"
	FileName$=NamenPlus$[Route]
	Array.DELETE NamenPlus$[]
RETURN

MakeButton:
	! Buttons
	SW.BEGIN Nr
	SW.CASE 1
		Title$="Rename GPX"
		SW.BREAK
	SW.CASE 2
		Title$="Delete GPX"
		SW.BREAK
	SW.CASE 3
		Title$="Repair GPX"
		SW.BREAK
	SW.CASE 4
		Title$="STOP"
		SW.BREAK
	SW.END
	GR.COLOR 128,255,255,0,1
	GR.RECT p,36,150*(Nr-1)+60,684,150*(Nr-1)+150
	GR.COLOR 255,255,255,255,0
	GR.RECT p,36,150*(Nr-1)+60,684,150*(Nr-1)+150
	GR.TEXT.DRAW p,108,150*(Nr-1)+130,Title$
RETURN

GPXRename:
	C$="CANCEL"
	GOSUB GetGPX
	IF FileName$="None" | FileName$="CANCEL" THEN
		POPUP "File NOT found",0,0,0
	ELSE
		! New name
		GR.FRONT 0
		INPUT "Enter new name", NewName$, "BikeTrip"
		IF LOWER$(RIGHT$(NewName$,4))<> ".gpx" THEN NewName$=NewName$+".gpx"
		FILE.RENAME DataPath$+FileName$,DataPath$+NewName$
		GR.FRONT 1
		POPUP "GPX renamed !",0,0,0
	ENDIF
	TONE 440, 200
RETURN

GPXDelete:
	C$="CANCEL"
	GOSUB GetGPX
	IF FileName$="None" | FileName$="CANCEL" THEN
		POPUP "File NOT found",0,0,0
	ELSE
		FILE.DELETE DF, DataPath$+FileName$
		POPUP "GPX deleted !",0,0,0
	ENDIF
	TONE 440,200
RETURN

GPXRepair:
	FILE.EXISTS FF, DataPath$+"wpt.txt"
	FILE.EXISTS SF, DataPath$+"trk.txt"
	IF SF=0 THEN
		POPUP "Data NOT found",0,0,0
	ELSE
		GR.CLS
		GR.COLOR 255,0,255,255,0
		GR.TEXT.DRAW p, 36, 111, "Step 1. Repair POIs"
		GR.RENDER
		! Check wpt.txt
		FILE.SIZE GBytes,DataPath$+"wpt.txt"
		IF FF=0 | GBytes<304 THEN
			! Create new wpt.txt
			TEXT.OPEN W, FN2,DataPath$+"wpt.txt"
			TEXT.WRITELN FN2,"<?xml version=";CHR$(34);"1.0";CHR$(34);" encoding=";CHR$(34);"UTF-8";CHR$(34);" standalone=";CHR$(34);"no";CHR$(34);"?>"
			TEXT.WRITELN FN2,"<gpx version=";CHR$(34);"1.0";CHR$(34);" creator=";CHR$(34);"GPX Bike logger Aat";CHR$(34)
			TEXT.WRITELN FN2,"xmlns:xsi=";CHR$(34);"http://www.w3.org/2001/XMLSchema-instance";CHR$(34)
			TEXT.WRITELN FN2,"xmlns=";CHR$(34);"http://www.topografix.com/GPX/1/0";CHR$(34)
			TEXT.WRITELN FN2,"xsi:schemaLocation=";CHR$(34);"http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd";CHR$(34);">"
			TEXT.CLOSE FN2
		ENDIF
		TEXT.OPEN R, FN2,DataPath$+"wpt.txt"
		FOR i=1 TO 5
			TEXT.READLN FN2,Regel$
		NEXT i
		NumOfPois=0
		DO
			TEXT.READLN FN2,Regel$
			! Read complete POIs
			IF IS_IN("</wpt>",Regel$,1)>0 THEN
				NumOfPois=NumOfPois+1
			ENDIF
		UNTIL Regel$="EOF"
		TEXT.CLOSE FN2
		! Save to start of GPX file
		TIME Year$, Month$, Day$, Hour$, Minute$, Second$
		FName$=Year$ + Month$ + Day$ + "_" + Hour$ + Minute$ + Second$ + ".gpx"
		TEXT.OPEN W, FN1, DataPath$+FName$
			TEXT.OPEN R, FN2,DataPath$+"wpt.txt"
				FOR i=1 TO 5+NumOfPois*3
					TEXT.READLN FN2,Regel$
					TEXT.WRITELN FN1,Regel$
				NEXT i
			TEXT.CLOSE FN2
		TEXT.CLOSE FN1
		! Check trk.txt
		GR.TEXT.DRAW p,36,192,"Step 2. Repair track"
		GR.RENDER
		TEXT.OPEN R, FN2,DataPath$+"trk.txt"
		NumOfTracks=0
		DO
			TEXT.READLN FN2,Regel$
			! Read complete Tracks
			IF IS_IN("</trkpt>",Regel$,1)>0 THEN
				NumOfTracks=NumOfTracks+1
			ENDIF
		UNTIL Regel$="EOF"
		TEXT.CLOSE FN2
		IF NumOfTracks<2 THEN
			GR.TEXT.DRAW p,36,273,"Repair not possible"
			GR.RENDER
			FILE.DELETE DF, DataPath$+FName$
			PAUSE 2000
			TONE 440,200
		ELSE
			GR.TEXT.DRAW p,36,273, "Step 3. Create GPX"
			GR.RENDER
			TEXT.OPEN A, FN1, DataPath$+FName$
				TEXT.OPEN R, FN2,DataPath$+"trk.txt"
					TEXT.READLN FN2,Regel$
					TEXT.WRITELN FN1,Regel$
					TEXT.READLN FN2,Regel$
					TEXT.WRITELN FN1,Regel$
					FOR i=1 TO NumOfTracks
						FOR j=1 TO 5
							TEXT.READLN FN2,Regel$
							TEXT.WRITELN FN1,Regel$
						NEXT j
					NEXT i
				TEXT.CLOSE FN2
				TEXT.WRITELN FN1, CHR$(9); "</trkseg>"
				TEXT.WRITELN FN1, "</trk>"
				TEXT.WRITELN FN1, "</gpx>"
			TEXT.CLOSE FN1		
			GR.TEXT.DRAW p,36,354,"Repair succeeded !"
			GR.TEXT.DRAW p,36,435,"File saved to"
			GR.TEXT.DRAW p,36,516, FName$
			GR.RENDER
			FILE.DELETE DF,DataPath$+"wpt.txt"
			FILE.DELETE DF,DataPath$+"trk.txt"
			PAUSE 3000
		ENDIF
	ENDIF
	TONE 440,200
RETURN

Start:
DO
	GR.CLS
	GR.COLOR 255,255,255,255,0
	GR.TEXT.DRAW p,216,840,"GPX-Tool"
	FOR Nr=1 TO 4
		GOSUB MakeButton
	Next Nr
	GR.RENDER
	Keuze=0
	DO
		DO
			GR.TOUCH touched,x,y
		UNTIL TOUCHED
		DO
			GR.TOUCH touched,x,y
		UNTIL !TOUCHED
		FOR i=1 to 4
			IF y>=(150*(i-1)+60)*sy & y<=(150*(i-1)+150)*sy THEN
				Keuze=i
			ENDIF
		NEXT i
	UNTIL Keuze>0
	TONE 440,200

	SW.BEGIN Keuze
	SW.CASE 1
		! Rename GPX
		GOSUB GPXRename
		SW.BREAK
	SW.CASE 2
		! Delete GPX
		GOSUB GPXDelete
		SW.BREAK
	SW.CASE 3
		! Repair GPX
		GOSUB GPXRepair
		SW.BREAK
	SW.CASE 4
		! End
		KAP=1
		SW.BREAK
	SW.END
UNTIL KAP=1

GR.CLOSE
EXIT