! Merge GPX files
! Aat Don @2013
GR.OPEN 255, 0, 0, 255, 0
GR.ORIENTATION 1
GR.SCREEN w, h
ScaleX=720
ScaleY=960
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
GR.TEXT.BOLD 3
GR.TEXT.SIZE 60
LineNum=81
DataPath$="../../Bike_GPS/data/"
! Radius of the globe in Europe (km)
R=6378

GOTO Start
GetGPX:
	FILE.EXISTS DirPresent,DataPath$
	IF DirPresent=0 THEN
		FILE.MKDIR DataPath$
	ENDIF
	FILE.DIR DataPath$,Namen$[]
	Array.length NumFiles,Namen$[]
	IF NumFiles<1 THEN
		FileName$="None"
		RETURN
	ENDIF
	SELECT Route, Namen$[],"Select a GPX file"
	FileName$=Namen$[Route]
	Array.DELETE Namen$[]
RETURN

Start:
GR.CLS
GR.COLOR 255,0,255,255,0
GR.TEXT.DRAW p,36,LineNum+30,"Select GPX file 1"
GR.RENDER
PAUSE 2000
GOSUB GetGPX
IF FileName$="None" THEN
	POPUP "File NOT found",0,0,0
ELSE
	F1$=FileName$
	GR.CLS
	GR.COLOR 255,0,255,255,0
	GR.TEXT.DRAW p,36,LineNum+30,F1$
	GR.TEXT.DRAW p,36,LineNum*2+30,"Select GPX file 2"
	GR.RENDER
	PAUSE 2000
	GOSUB GetGPX
	F2$=FileName$
	GR.CLS
	GR.COLOR 255,0,255,255,0
	GR.TEXT.DRAW p,36,LineNum*1+30,F1$
	GR.TEXT.DRAW p,36,LineNum*2+30,F2$
	GR.TEXT.DRAW p,36,LineNum*3+30,"Step 1. Get positions"
	GR.RENDER
	FILE.SIZE fs,DataPath$+F1$
	TEXT.OPEN R,FN1,DataPath$+F1$
		! GOTO EOF by placing position pointer (far) beyond the file
		TEXT.POSITION.SET FN1,fs/20
		TEXT.POSITION.GET FN1,LastLine
		! Go back to last track point
		TEXT.POSITION.SET FN1,LastLine-8
		TEXT.READLN FN1,P1$
		! Get Lat/Lon of last point
		! Remove spaces and tabs
		P1$=REPLACE$(P1$,CHR$(9),"")
		P1$=REPLACE$(P1$," ","")
	TEXT.CLOSE FN1
	TEXT.OPEN R,FN1,DataPath$+F2$
		! GOTO Line 6 by placing position pointer
		TEXT.POSITION.SET FN1,6
		DO
			TEXT.READLN FN1,P2$
		UNTIL IS_IN("<trkpt",P2$,1)
		! Get Lat/Lon of 1st point
		! Remove spaces and tabs
		P2$=REPLACE$(P2$,CHR$(9),"")
		P2$=REPLACE$(P2$," ","")
	TEXT.CLOSE FN1
	GR.TEXT.DRAW p,36,LineNum*4+30,"Step 2. Calculate gap"
	GR.RENDER
	! Get Lat/Lon of points
	s=IS_IN("lat=",P1$,1)
	t=IS_IN("lon=",P1$,s)
	e=IS_IN(">",P1$,t)
	LatLast=TORADIANS(VAL(MID$(P1$,s+5,t-s-6)))
	LonLast=TORADIANS(VAL(MID$(P1$,t+5,e-t-6)))
	s=IS_IN("lat=",P2$,1)
	t=IS_IN("lon=",P2$,s)
	e=IS_IN(">",P2$,t)
	LatFirst=TORADIANS(VAL(MID$(P2$,s+5,t-s-6)))
	LonFirst=TORADIANS(VAL(MID$(P2$,t+5,e-t-6)))
	! Calculate distance between end of File 1 and start of File 2
	dLon=LonFirst-LonLast
	dLat=LatFirst-LatLast
	a=((SIN(dLat/2))^2+COS(LatLast)*COS(LatFirst)*(SIN(dLon/2))^2)
	c=(2*ATAN2(SQR(a),SQR(1-a)))
	! d in km
	d=R*c
	GR.TEXT.DRAW p,36,LineNum*6+30,"Gap between GPX 1"
	GR.TEXT.DRAW p,36,LineNum*7+30,"and GPX 2 = "+FORMAT$("##%.##",d)+" km"
	GR.TEXT.DRAW p,36,LineNum*8+30,"Continue ?"
	GR.COLOR 255,0,255,0,0
	GR.RECT p,396,LineNum*7.2+30,528,LineNum*8.2+30
	GR.TEXT.DRAW p,405,LineNum*8+30,"YES"
	GR.COLOR 255,255,0,0,0
	GR.RECT p,576,LineNum*7.2+30,684,LineNum*8.2+30
	GR.TEXT.DRAW p,585,LineNum*8+30,"NO"
	GR.RENDER
	Keuze=0
	DO
		DO
			GR.TOUCH touched,x,y
		UNTIL TOUCHED
		DO
			GR.TOUCH touched,x,y
		UNTIL !TOUCHED
		IF y>=(LineNum*7.2+30)*sy & y<=(LineNum*8.2+30)*sy THEN
			IF x>=396*sx & x<=528*sx THEN
				! Yes
				Keuze=1
			ENDIF
			IF x>=576*sx & x<=684*sx THEN
				! No
				Keuze=2
			ENDIF
		ENDIF
	UNTIL Keuze>0
	TONE 440,200
	IF Keuze=1 THEN
		! Clear part
		GR.COLOR 255,0,0,255,1
		GR.RECT p,0,LineNum*4.5+30,720,LineNum*9+30
		GR.RENDER
		GR.COLOR 255,0,255,255,0
		GR.TEXT.DRAW p,36,LineNum*5+30,"Step 3. New file :"
		! Merge the 2 files into a new one
		TIME Year$,Month$,Day$,Hour$,Minute$,Second$
		FName$=Year$+Month$+Day$+"_"+Hour$+Minute$+Second$+".gpx"
		GR.TEXT.DRAW p,36,LineNum*6+30, FName$
		GR.TEXT.DRAW p,36,LineNum*7+30,"Stitching (5 steps)"
		GR.RENDER
		TEXT.OPEN W,FN1,DataPath$+FName$
		TEXT.OPEN R,FN2,DataPath$+F1$
		TEXT.OPEN R,FN3,DataPath$+F2$
			! Start
			FOR i=1 TO 5
				TEXT.READLN FN2,Regel$
				TEXT.WRITELN FN1,Regel$
			NEXT i
			GR.TEXT.DRAW p,36,LineNum*8+30, "*"
			GR.RENDER
			! Add POIs from file 1
			DO
				TEXT.READLN FN2,Regel$
				IF IS_IN("<trk>",Regel$,1)=0 THEN
					TEXT.WRITELN FN1,Regel$
				ENDIF
			UNTIL IS_IN("<trk>",Regel$,1)
			GR.TEXT.DRAW p,36,LineNum*8+30, "**"
			GR.RENDER
			! Add POIs from file 2
			TEXT.POSITION.SET FN3,6
			DO
				TEXT.READLN FN3,Regel$
				TEXT.WRITELN FN1,Regel$
			UNTIL IS_IN("<trk>",Regel$,1)
			! Skip next line
			TEXT.READLN FN3,Regel$
			GR.TEXT.DRAW p,36,LineNum*8+30, "***"
			GR.RENDER
			! Add TRACK points from file 1
			DO
				TEXT.READLN FN2,Regel$
				IF IS_IN("</trkseg>",Regel$,1)=0 THEN
					TEXT.WRITELN FN1,Regel$
				ENDIF
			UNTIL IS_IN("</trkseg>",Regel$,1)
			GR.TEXT.DRAW p,36,LineNum*8+30, "****"
			GR.RENDER
			! Add TRACK points from file 2
			DO
				TEXT.READLN FN3,Regel$
				TEXT.WRITELN FN1,Regel$
			UNTIL IS_IN("</gpx>",Regel$,1)
			GR.TEXT.DRAW p,36,LineNum*8+30, "*****"
			GR.RENDER
		TEXT.CLOSE FN3
		TEXT.CLOSE FN2
		TEXT.CLOSE FN1
		GR.TEXT.DRAW p,36,LineNum*9+30, "Ready"
		GR.RENDER
		PAUSE 1000
	ENDIF
ENDIF
Tone 440,200
GR.CLOSE
EXIT