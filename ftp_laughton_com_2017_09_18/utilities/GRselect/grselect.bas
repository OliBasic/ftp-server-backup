!!
	grSelect.bas
		Graphics Screen Menu Select Statement
		
		This function displays a list of items "lMenu" with a Title
		it returns the selected item number in the list
		Returns 0 if the heading is selected. 
		
		
		lMenu - list of Menu Items
		Title$ - Title for the first line
		
	Rev. Jonathan C. Watt
	Fourth Sunday after Epiphany
	January 2012
!!

fn.def grSelect ( lMenu, title$ )

	gr.screen w, h 
	tempW = w
	tempH = h
	if w >= h then w = h
	if tempW >= tempH then h=tempW 
	i = 1
	
again:
	gr.cls

! Title Bar
	pl = 36
	gr.color 255,255,0,0,1  
	gr.rect o1,0,0,w,pl 
! Title
	gr.color 255,255,255,255,0
	gr.text.draw o1, 1, pl, title$
	
	! Display the menu up to the max screen size.
	pl = pl + 36
	topline = pl			% save for bounded touch
	list.size lMenu, lMenuSize 
	firstitem = i
	
	! Write the Menu Items
	do 
		list.get lMenu, i, m$
		gr.color 255, 0, 0, 0, 0  	 % black
		gr.text.draw o1, 10, pl, m$
		bottomline = pl
		pl = pl + 35 + 5
		bottommenu = i
		i = i + 1
	until (i>lMenuSize) |	(pl > (h-40))

	! Display the Up Arrow Button
	bPrev = 0
	if firstitem <> 1 then
		! Up button
		gr.color 255,150,150,150,1 				% grey
		gr.rect o1, w-35, 36, w, 36+70
		gr.color 255,255,0,0,1  				% Red
		gr.rect o1, w-31, 36+4, w-4, 36+66

		gr.set.stroke 3
		gr.color 255,150,150,150,1 				% grey
		gr.line o1, w-17, 36+4, w-17, 36+66
		gr.line o1, w-17, 36+4, w-31, 36+20
		gr.line o1, w-17, 36+4, w-4, 36+20
		gr.set.stroke 0
		bPrev = 1
!		firstitem = firstitem - 1
	endif

	! Display the Down Arrow Button
	bMore = 0
	if pl > (h-40) then
		! Down Button
		gr.color 255,150,150,150,1 				% grey
		gr.rect o1, w-35, h-100, w, h-30
		gr.color 255,255,0,0,1  				% Red
		gr.rect o1, w-31, h-96, w-4, h-34

		gr.set.stroke 3
		gr.color 255,150,150,150,1 				% grey
		gr.line o1, w-17, h-96, w-17, h-34
		gr.line o1, w-17, h-30, w-31, h-54 
		gr.line o1, w-17, h-30, w-4, h-54
		gr.set.stroke 0
		bMore = 1
	endif
	
	gr.render

	bDone = 0
	mSelect = 0
	mHeader = 0
	mMore = 0
	mPrev = 0
	do
		gr.bounded.touch mSelect, 0, topline, w, bottomline
		gr.bounded.touch mHeader, 0, 0, w, topline
		gr.bounded.touch mMore, w-35, h-100, w, h-30
		gr.bounded.touch mPrev, w-35, 36, w, 36+70

		if mMore & !bMore then
			mMore = 0
		endif
		
		if mPrev & !bPrev then
			mPrev = 0
		endif

		if mMore | mPrev | mHeader then
			bDone = 1
		elseif mSelect then
			gr.touch touch,x,y
			do
				gr.bounded.touch mSelect2, 0, topline, w, bottomline
			until !mSelect2
			bDone = 1
		endif
	until bDone 

	pause 100 
	if mMore then
		i = bottommenu
		goto again
	endif

	if mPrev then
		i = 1 
		goto again
	endif 
	
	if mHeader then
		fn.rtn 0
	endif
	
	y = round((y / 40) - 0.5) + firstitem - 1
	if y > lMenuSize then
		y = 0 
	endif
	
	
fn.rtn y
fn.end 
