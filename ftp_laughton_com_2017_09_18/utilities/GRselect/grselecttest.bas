include grselect.bas

	gr.open 255, 255, 255, 255

	gr.orientation 1 
	gr.screen w, h 
	tempW = w
	tempH = h
	! flip dimensions if inversed 
	! w & h will now be correct 
	if w >= h then w = h
	if tempW >= tempH then h=tempW 

	gr.color 255,0,0,0,0	
	
	gr.text.size 30
	gr.text.align 1		% align left

	list.create S, lMenu
	for i = 1 to 50
		s$ = "line " + format$("##",i)
		list.add lMenu, s$
	next i

	debug.on
	debug.dump.list lMenu

	x = grSelect( lMenu, "This is the Title" )
	if x = 0 then
		print "Header Selected"
	endif 
	if x <> 0 then 
		list.get lMenu, x, l$
		print l$
	endif

end
 
