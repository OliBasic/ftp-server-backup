REM Start of BASIC! Program
! Frig=1
Hud=1 %1 for norm, -1 for flip
File.root f$
If is_in("RFO-BASIC",upper$(f$)) then isapk=0 else isapk=1
D2r=pi()/180
Units=3.6
Unix$="KPH"
Ori=0
Cum=0
Wakelock 3
! Distance/lat lookups
DIM kks[91]
Kks[1]=110.574
Kks[16]=110.649
Kks[31]=110.852
Kks[46]=111.132
Kks[61]=111.142
Kks[76]=111.618
Kks[91]=111.694
FOR x=1 TO 90 STEP 15
 Poop=(kks[x+15]-kks[x])/15
 Parp=kks[x]
 FOR y= 1 TO 14
  Kks[x+y]=poop+parp
  Parp=parp+poop
 NEXT y
NEXT x
! 
Gr.open 255,0,0,0,0,0
Gr.scale hud,1
Gr.screen w,h
Gr.color 255,128,128,128
Gr.text.align 2
Gr.color 255,255,103,0
! Open gps
Gosub w84gps
Gosub getparas
Timer.set 500
Gosub tz

! MAIN LOOP
Ty:
If bkhit then gosub menu
If rfind
   Gosub reflect
   Rfind=0
Endif
Goto ty
!End of main loop

! 
TZ:
Gr.text.size h/2
Dx=w/2
Gosub getgx
Gr.text.draw fred, gx,h/2,""
Gr.text.size h/15
Gr.text.draw frun,gx,h/2+h/8,unix$
Gr.text.size h/5

Dx=0
Gosub getgx
Gr.text.align 1
Gr.text.draw ttex,gx,h*4/5,"00:00"
Gr.text.align 3
Dx=w
Gosub getgx
Gr.text.draw ktex,gx, h*4/5,"0"
Gr.text.size h/15
Gr.text.draw utex,gx, h*9/10,k$
Gr.Text.align 2
GR.RENDER
Return 

! 
GETGX:
If hud=1 
  Gx=dx
Else
  Gx=-(w-dx)
Endif
Return

! 
W84GPS:
Gr.text.size h/16
Gr.text.draw w8,w/2,h/2,"Waiting for gps"
Gr.render
Gps.open gopok
Pause 200
If! gopok 
 ? Geterror$() 
  end "Gps couldn't be opened"
Endif 
Gpct=1
Gps.time gpst1
Do 
Pause 500
Gps.time gpst2
Gpct++
Gps.accuracy gpsac
Until (gpst2<>gpst1&gpsac<>0)|gpct>60
If gpct>60
  Gr.modify w8,"text","Gps not found.. Closing"
  Gr.render
  Pause 3000
  If isapk then exit else end
Endif
Gps.latitude lastlat
Gps.longitude lastlon
Gr.modify w8,"text","(Tap Back Key for Menu/Settings)"
Gr.render
Pause 2000
Gr.hide w8
Return

! 
REFLECT:
 Hud=-hud
  Gr.cls
  Gr.scale hud, 1
  Gr.cls
  Menudrawn=0
  Gosub tz
  Gr.render
Return 

! 
MENU:
Bkhit=0
Do 
Gr.touch tt, tx, ty
Until !tt
Hwas=hud
If hud=-1 
  gosub reflect
Endif
!must now be in normal mode
If!menudrawn then gosub drawmenu
Gr.show mgroup
Gr.render
Gosub domenu
Gr.hide mgroup
! If hwas=1 then gosub reflect
Gr.render
Return 

! 
DRAWMENU:
Gr.paint.get psafe
Gr.color 255,128,128,128
Gr.text.size h/20
Gr.rect mbox,w*3/4,0,w,h*6/8
Gr.color 255,255,255,255,0
Gr.rect mboxw,w*3/4,0,w,h*6/8
!menu is hud on/off, kph/mph,rotate,ztrip, close menu, exit
Gr.line ml1,w*3/4,h/8,w,h/8
Gr.text.draw mlt1, w*7/8,h/16,"HUD on"
Gr.line ml2,w*3/4,h/4,w,h/4
Gr.text.draw mlt2, w*7/8,h*3/16,"KPH/MPH"
Gr.line ml3,w*3/4,h*3/8,w,h*3/8
Gr.text.draw mlt3,w*7/8,h*5/16,"Rotate"

Gr.line ml3a,w*3/4,h*4/8,w,h*4/8
Gr.text.draw mlt3a,w*7/8,h*7/16,"Zero Trip"
Gr.line ml4,w*3/4,h*5/8,w,h*5/8
Gr.text.draw mlt4,w*7/8,h*9/16,"Close Menu"


Gr.text.draw mlt5,w*7/8,h*11/16,"Exit App"


Gr.paint.copy psafe
Menudrawn=1
Gr.group mgroup,mbox,mboxw,ml1,ml2,ml3,ml3a,mlt3a,mlt1,mlt2,mlt3,ml4,mlt4,mlt5
Gr.hide mgroup
Return 

! 
DOMENU:
Do
Gr.touch tch,tchx,tchy
Until !tch
Do 
Gr.touch tch,tchx,tchy
Until tch&(tchx>w*3/4)&(tchy<h*6/8)
Sw.begin tchy
Sw.case <h/8
  Rfind=1
  Sw.break
Sw.case <h/4  
  If units=3.6
    units=3.6*0.625 
    Unix$="MPH"
    Gr.modify utex,"text","m"
  else 
    units=3.6
    UNIX$="KPH"
    Gr.modify utex,"text","km"
  ENDIF
  Gosub updpars
  Gr.modify frun,"text",unix$
  Gr.render
  Sw.break
Sw.case <h*3/8
   If ori=0 then ori=2 else ori=0
   Gr.orientation ori
   Pause 100
   Sw.break
Sw.case <h*4/8
   Cum=0
   Sw.break
Sw.case <h*5/8
   Gr.hide mgroup
   Gr.render
   Sw.break
Sw.default 
   Timer.clear
   Gosub cumsave
   Gps.close
   If isapk then exit else end
Sw.end
Return

! 
ONTIMER:
Gps.speed tsp
If frig then tsp=30
Gps.latitude glat
GPS.longitude glong
Tsp$=int$(tsp*units) 
Gr.modify fred,"text", tsp$
Time t$,t$,t$,hr$,mi$,t$,t,t
Gr.modify ttex,"text",hr$+":"+mi$
Gosub upcum
Gr.modify ktex,"text",cumtext$
Gr.render
Timer.resume
! 
UPCUM:
Chlat=ABS(lastlat-glat)
Chlon=ABS(lastlon-glong)
Lastlat=glat
Lastlon=glong
! Dateline conundrum
IF chlon>180 THEN chlon=chlon-180
! Lets say 1 degree lat is 111.5km
! No its index abs lat+1 in kks
Kkk=kks[INT(ABS(glat)) +1]
A=chlat*kkk
!a degree long is same as a lat*cos lat
Y=COS(ABS(glat)*d2r)*kkk
B=chlon*y
C=SQR(a^2+b^2)
If frig then C=0.01
! If !frig & c<0.02 THEN return 

Cum=cum+c
Tcum=cum
If unix$="MPH" then tcum=tcum*0.625

cumtext$=format$("%%%.#",tcum)


Return 

! 
ONBACKKEY:
Bkhit=1
Back.resume

! 
GETPARAS:
File.exists fe,"HUDPARS.TXT"
If !fe then return 
Grabfile p$, "HUDPARS.TXT"
If Left$(p$,1)="K" 
  Units=3.6
  Unix$="KPH"
  K$="km"
else
  Units=3.6*0.625
  Unix$="MPH"
  K$="m"
Endif
If len(p$)=3
  Cum=0
else
  Cum=val(right$(p$,len(p$)-1)) 
Endif
Return 
! 
UPDPARS:
Text.open w,f1,"HUDPARS.TXT"
Text.writeln f1,left$(unix$,1)+str$(cum) 
Text.close f1
Return 
! 
CUMSAVE:
Text.open w,f1,"HUDPARS.TXT"
Text.writeln f1,left$(unix$,1)+str$(cum)
Text.close f1
Return 

! THAT'S ALL FOLKS! 









Ï
