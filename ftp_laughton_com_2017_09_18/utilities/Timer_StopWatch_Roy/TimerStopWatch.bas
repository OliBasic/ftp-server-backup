Rem Timer/Stop Watch
Rem For Android
Rem With RFO Basic!
Rem March 2017
Rem Version 1.00
Rem By Roy Shepherd

PORTRAIT = 1 : LANDSCAPE = 0
orientation = PORTRAIT

if orientation = LANDSCAPE then 
    di_height = 672
    di_width = 1150
elseif orientation = PORTRAIT then
    di_height = 1150
    di_width = 672
endif
        
gr.open 255, 117, 117, 255
gr.color 255, 255, 255, 255
gr.text.size 30

gr.orientation orientation
pause 500
WakeLock 3
gr.screen screenWidth, screenHeight
scale_x = screenWidth / di_width
scale_y = screenHeight / di_height
gr.scale scale_x, scale_y

globals.all

gosub Functions            
gosub Initialise
call Render()

do 
    gosub KeyPressed
    gosub ShowTimer
until 0

!------------------------------------------------
! BackKey interrupt. Double tap back key to end
!------------------------------------------------
onBackKey:
    backKeyHit = BackKey(backKeyHit)
back.resume 
 
!------------------------------------------------
! Do once at first run of app
!------------------------------------------------
Initialise:
    !Set Global variables 
    TRUE = 1 : FALSE = 0
    sound = TRUE : buzz = TRUE
    backKeyHit = 1
    backTick = 0 % for double tap back key to end
    s = max(di_width, di_height)
    w = di_width : h = di_height
    stringTime$ = "00:00:00"
    start = FALSE : _stopWatch = FALSE
    _timer = FALSE : _pauseTimer = FALSE
    hour = 0 : minuet = 0 : second = 0 : wait = 0
    lapCount = 0 : scrollPossision = 0 : showLapWindow = FALSE
    redB = 86 : greenB = 119 : blueB = 252
    
    dataPath$ = "TimerStopWatch/data/"
    file$ = "timeData.txt"
    fn$ = dataPath$ + "/" + file$
    
    gosub LoadData
    
    ! Set background colour
    gr.color 255, redB, greenB, blueB, 1
    gr.rect background, 0, 0, s, s
    gr.get.value background, "paint", pBg
    
    ! Heading 
    gr.color 255, 100, 100, 100
    gr.rect null , 0, 0, di_width, 50 
    gr.color 255, 255, 255, 255
    gr.text.align 2 : gr.text.draw null, w / 2, 30, "Timer/Stop Watch"
    
    ! Load and display the options button
    gr.bitmap.load option, dataPath$ + "optionList.png"
    call ImageLoadError(option, "optionList.png")
    gr.bitmap.draw butMenu, option, di_width - 50, 0
    
    ! Timer box
    y = 150
    gr.set.stroke 4 : gr.color 255, 255, 255, 255, 0
    gr.rect null, 50, y, w - 50, y + 150, 20, 20
    gr.set.stroke 1 : gr.color 255, 255, 255, 255, 1
    gr.text.size 140 : gr.text.align 2
    gr.text.draw txtTimer, w / 2, y + 125, stringTime$
    
    ! Load and display the up and down buttons
    gr.bitmap.load upButton, dataPath$ + "arrowUp1.png"
    call ImageLoadError(upButton, "arrowUp1.png")
    gr.bitmap.load downButton, dataPath$ + "arrowDown1.png"
    call ImageLoadError(upButton, "arrowDown1.png")
    
    gr.bitmap.draw butHourUp, upButton, 100, y - 85
    gr.bitmap.draw butMinuetUp, upButton, (w / 2) - 40, y - 85
    gr.bitmap.draw butSecondUp, upButton, w - 175, y - 85
    
    gr.bitmap.draw butHourDown, downButton, 100, y + 160
    gr.bitmap.draw butMinuetDown, downButton, (w / 2) - 40, y + 160
    gr.bitmap.draw butSecondDown, downButton, w - 175, y + 160
    
    ! Set button colours
    gr.color 255, 200, 200, 200 : gr.paint.get gray 
    gr.color 255,100, 100, 100  : gr.paint.get darkGray
    
    gr.text.size 40 : gr.text.align 1
    !Draw the Start/Pause, Lap/Split & Reset/Stop buttons
    ptrText = 1
    butStartPause1 = DrawButton(20, y + 300, 220, y + 375, &ptrText, "Start")
    txtStartPause = ptrText
    butLapSplit1   = DrawButton((w / 2) - 100, y + 300, (w / 2) + 100, y + 375, txtLapSplit, "Lap/Split")
    butResetStop1  = DrawButton( w - 220, y + 300, w - 20, y + 375, txtResetStop, "Reset")
    
    ! Load and display sound on/off buttons
    gr.bitmap.load on, dataPath$ + "soundOn.png"
    call ImageLoadError(on, "soundOn.png")
    gr.bitmap.scale on1, on, 75, 75
    
    gr.bitmap.load off, dataPath$ + "soundOff.png"
    call ImageLoadError(off, "soundOff.png")
    gr.bitmap.scale off1, off, 75, 75
    gr.bitmap.delete on : gr.bitmap.delete off
    
    gr.bitmap.draw soundOn, on1, w - 100, h - 90
    gr.bitmap.draw soundOff, off1, w - 100, h - 90
    if sound then gr.hide soundOff
    
    !Load the buzz on/off buttons
    gr.bitmap.load on, dataPath$ + "buzzOn.png"
    call ImageLoadError(on, "buzzOn.png")
    gr.bitmap.scale on1, on, 100, 100
    
    gr.bitmap.load off, dataPath$ + "buzzOff.png"
    call ImageLoadError(off, "buzzOff.png")
    gr.bitmap.scale off1, off, 100, 100
    gr.bitmap.delete on : gr.bitmap.delete off
    
    gr.bitmap.draw buzzOn, on1, w - 300, h - 110
    gr.bitmap.draw buzzOff, off1, w - 300, h - 110
    gr.hide buzzOn : gr.hide buzzOff
    if buzz then gr.show buzzOn else gr.show buzzOff
    
    ! Draw on/off sound and buzz buttons
    gr.color 0
    gr.rect butSoundOnOff, w - 100, h - 100, w, h
    gr.rect butBuzzOnOff, w - 300, h - 100, w - 200, h
    gr.color 255 
    
    ! Draw & hide dark window
    gr.color 180, 0, 0, 0, 1
    gr.rect darkWindow, 0, 0, s, s
    gr.color 255 : gr.hide darkWindow
    
    gosub DrawOptionsMenu
    gosub DrawAbout
    gosub DrawHelp
    
    ! Load draw and hide lap/split window
    gr.color 255, 255, 255, 255, 0 : gr.set.stroke 2 : gr.text.size 30
    gr.rect lapPanel, 50, (di_height / 2), di_width - 50, (di_height - 100), 30, 30
    gr.color 255, 255, 255, 255, 1 : gr.set.stroke 1 : gr.text.align 1
    gr.text.draw txtLap, 60, (h / 2) - 10, "Lap/Split"
    gr.text.draw txtTime, (w / 2) + 50, (h / 2) - 10, "Time"
    !gr.text.draw txtDiff, 350, (h / 2) - 10, "Diff"
    gr.group lapWindow, lapPanel, txtLap, txtTime, txtDiff
    gr.hide lapWindow
    
    ! Draw 100 laps/splits. 100 is max
    dim laps[100] : dim lapTimes$[100]
    gr.text.size 40 : gr.text.typeface 2
    
    gr.clip lapWin, 0, (h / 2), w, h - 100
    for x = 1 to 100
        gr.text.draw laps[x], 100, - 50, lapTimes$[x]
    next
    
    ! Load the colour picker to change background colour
    gr.bitmap.load c, dataPath$ + "colourPicker.png"
    call ImageLoadError(c, "colourPicker.png")
    gr.bitmap.draw colourPick, c, 190, 400
    gr.hide colourPick
    
    ! Setup for collision for buttons tapper
    gr.color 0
    gr.point collision, -1, -1
    gr.color 255
   
    ! Load button sound
    soundpool.open 1
    soundpool.load buttonClick, dataPath$ + "click.wav"
    if ! buttonClick then call SoundLoadError("click.wav")
return

!------------------------------------------------
! Draw the options menu
!------------------------------------------------
DrawOptionsMenu:
    gr.color 255, 255, 255, 255, 1
    gr.rect optRec, w - 300, 50, w, 320, 10, 10 
    gr.color 0
    gr.rect butBgColour, (w - 275) , 60, (w - 25), 135, 10, 10 
    gr.rect butAbout, (w - 275) , 145, (w - 25), 220, 10, 10
    gr.rect butHelp, (w - 275) , 230, (w - 25), 305, 10, 10
    gr.color 255, 0, 0, 0
    gr.text.draw txtBgColour, w - 265, 110, "Background"
    gr.text.draw txtAbout, w - 210, 195, "About"
    gr.text.draw txtHelp, w - 190, 280, "Help"
    
    gr.group allOptionsMenu, optRec, butBgColour, butAbout, butHelp, txtBgColour, txtAbout, txtHelp
    gr.hide allOptionsMenu
return

!------------------------------------------------
! Draw the About box
!------------------------------------------------
DrawAbout:
    dim aboutBox[5]
    gr.color 255, 255, 255, 255
    gr.rect aboutBox[1], (w / 2) - 200, (h / 2) - 150, (w / 2) + 200, (h / 2) + 100, 15, 15
    gr.color 255, 0, 0, 0 : gr.text.align 2
    gr.text.draw aboutBox[2], w / 2, (h / 2) - 100, "Timer/Stop Watch"
    gr.text.draw aboutBox[3], w / 2, (h / 2) - 40,  "March 2016"
    gr.text.draw aboutBox[4], w / 2, (h / 2) + 20,  "Version: 1.00"
    gr.text.draw aboutBox[5], w / 2, (h / 2) + 80,  "Author: Roy Shepherd"
    gr.group about, aboutBox[1], aboutBox[2], aboutBox[3], aboutBox[4], aboutBox[5] 
    gr.hide about : array.delete aboutBox[]
return

!------------------------------------------------
! Draw Help
!------------------------------------------------
DrawHelp:
    dim helpBox[9], help$[8]
    help$[1] = "A simple countdown timer and stopwatch. To use it"
    help$[2] = "as a stopwatch set the timer to 00:00:00 (default)"
    help$[3] = "and press start."
    
    help$[4] = "To use it as a countdown set the timer to" 
    help$[5] = "anything other then 00:00:00 and press start."
    
    help$[6] = "if you haven't turned sound or vibrate off, your" 
    help$[7] = "device will vibrate and play a tone sound when the"
    help$[8] = "countdown reaches zero."
    
    gr.color 255, 255, 255, 255 : gr.text.size 25
    gr.rect helpBox[1], (w / 2) - 300, (h / 2) - 200, (w / 2) + 300, (h / 2) + 300, 15, 15
    gr.text.align 1 : gr.color 255, 0, 0, 255 : y = (h / 2) - 150
    for x = 1 to 8
        gr.text.draw helpBox[x + 1], (w / 2) - 290, y, help$[x]
        y += 50
        if x = 3 then y += 25 : gr.color 255, 0, 128, 0
        if x = 5 then y += 25 : gr.color 255, 255, 17, 17
    next
    list.create n, elementListPointer
    list.add.array elementListPointer, helpBox[]
    gr.group.list allHelp, elementListPointer
    array.delete help$[] : array.delete helpBox[] 
    gr.hide allHelp
return

!------------------------------------------------
! If a key is pressed then acted accordingly 
!------------------------------------------------
KeyPressed:
    gr.touch t, tx, ty
    if ! t then return
    !do : gr.touch t, tx, ty : until ! t
    tx /= scale_x : ty /= scale_y
    gr.modify collision, "x", tx, "y", ty
    if gr_collision(collision, butStartPause1) then gosub StartPause    : return
    if gr_collision(collision, butLapSplit1)   then gosub LapSpilt      : return
    if gr_collision(collision, butResetStop1)  then gosub ResetStop     : return
    if gr_collision(collision, butMenu)        then gosub OptionsMenu   : return
    if gr_collision(collision, butSoundOnOff)  then gosub SoundOnOff    : return
    if gr_collision(collision, butBuzzOnOff)   then gosub BuzzOnOff     : return
    if gr_collision(collision, lapPanel)       then gosub ScroleLapInfo : return
    gosub ButtonUpDown
    
return

!------------------------------------------------
! Start and Pause the timer
!------------------------------------------------
StartPause:
    call PlaySound(sound, buttonClick) : call Buzzer(buzz)
    if ! start then 
        start = TRUE
        _pauseTimer = FALSE
        gr.modify txtStartPause, "text", "Pause"
    else
        start = FALSE
        _pauseTimer = TRUE
       gr.modify txtStartPause, "text", "Start" 
    endif 
    gr.modify butStartPause1, "paint", gray : call Render()
    gosub WaitKeyUp
    gr.modify butStartPause1, "paint", darkGray : call Render()
    call Render()
return

!------------------------------------------------
! Keep the clock going whilst use is pressed a key
!------------------------------------------------
WaitKeyUp:
    do
        gr.touch t, null, null
        gosub ShowTimer
    until ! t
return

!------------------------------------------------
! Split the timer
!------------------------------------------------
LapSpilt:
    if ! start | lapCount = 100 then return
    call PlaySound(sound, buttonClick) : call Buzzer(buzz)
    gr.modify butLapSplit1, "paint", gray : call Render()
    gosub WaitKeyUp
    gr.modify butLapSplit1, "paint", darkGray : call Render()
    if lapCount = 0 then gr.show lapWindow : showLapWindow = TRUE
    lapCount ++
    y = (di_height / 2) + 30
    scrollPossision = 0
    spc$ = "..............."
    lapTimes$[lapCount] = int$(lapCount) + left$(spc$, 12 - len(int$(lapCount))) + stringTime$
    
    if lapCount < 13 then
        for showLap = 1 to lapCount
            gr.modify laps[showLap], "y", y, "text", lapTimes$[showLap]
            y += 40
        next 
    else 
        y = (di_height - 120)
        for showLap = lapCount to 1 step - 1
            gr.modify laps[showLap], "y", y, "text", lapTimes$[showLap]
            y -= 40
        next
    endif
return

!------------------------------------------------
! Scrole the lap/split info up or down
!------------------------------------------------
ScroleLapInfo:
    if lapCount < 13 then return
    gr.touch t, null, ty1
    if ! t then return
    pause 100
    gr.touch t, null, ty2
    ty1 /= scale_y : ty2 /= scale_y
    
    if ty2 < ty1 then 
        moveUp = hypot(ty1 - ty2, 0 - 0)
        gosub ScrollUp 
    endif
    
    if ty2 > ty1 then 
        moveDown = hypot(ty1 - ty2, 0 - 0)
        gosub ScrollDown
    endif
    if ! start then call Render()
    if _timer then call Render()
return

!------------------------------------------------
! Scrole lap/split up
!------------------------------------------------
ScrollUp:
    scrollPossision  -= moveUp
    y = scrollPossision
    gr.get.position laps[1], xx, yy
    gr.get.position laps[lapCount], xx1, lastY
    if lastY < di_height - 110 then scrollPossision  += moveUp : return

    for x = 1 to lapCount
        gr.modify laps[x], "y", yy - 40
        yy += 40
    next
    
return

!------------------------------------------------
! Scrole lap/split down
!------------------------------------------------
ScrollDown:
    scrollPossision += moveDown
    y = scrollPossision
    gr.get.position laps[1], xx, yy
    if yy > (di_height / 2) + 30 then scrollPossision -= moveDown : return
    
    for x = 1 to lapCount
        gr.modify laps[x], "y", yy + 40
        yy += 40
    next
  
return

!------------------------------------------------
! Reset and stop the timer
!------------------------------------------------
ResetStop:
    call PlaySound(sound, buttonClick) : call Buzzer(buzz)
    gr.modify butResetStop1, "paint", gray
    gosub WaitKeyUp
    gr.modify butResetStop1, "paint", darkGray 
    for x = 1 to lapCount : gr.modify laps[x], "y", - 100, "text", lapTimes$[x] : next
    stringTime$ = "00:00:00"
    start = FALSE : _stopWatch = FALSE
    _timer = FALSE : allSeconds = 0 : lapCount = 0
    hour = 0 : minute = 0 : second = 0 : wait = 0
    scrollPossision = 0 : showLapWindow = FALSE
    gr.modify txtStartPause, "text", "Start" 
    gr.modify txtTimer, "text", stringTime$
    gr.hide lapWindow
    call Render()
return

!------------------------------------------------
! Turn sound on off
!------------------------------------------------
SoundOnOff:
    call PlaySound(sound, buttonClick) : call Buzzer(buzz)
    gosub WaitKeyUp
    if sound then
        sound = FALSE
        gr.show soundOff
    else
        sound = TRUE
        gr.hide soundOff
    endif
    call Render()
return

!------------------------------------------------
! Turn buzz on off
!------------------------------------------------
BuzzOnOff:
    call PlaySound(sound, buttonClick) : call Buzzer(buzz)
    gosub WaitKeyUp
    if buzz then
        buzz = FALSE
        gr.show buzzOff : gr.hide buzzOn
    else
        buzz = TRUE
        gr.hide buzzOff : gr.show buzzOn
    endif
    call Render()
return

!------------------------------------------------
! Display the options menu
!------------------------------------------------
OptionsMenu:
    if showLapWindow then call HideShowLapWindow(FALSE) % hide
    gr.hide lapWin
    gr.show darkWindow
    gr.show allOptionsMenu
   
    call PlaySound(sound, buttonClick) : call Buzzer(buzz)
    call Render()
    gosub WaitKeyUp
    do : gr.touch t, tx, ty : until t
    tx / = scale_x : ty /= scale_y
    gr.modify collision, "x", tx, "y", ty
    
    if gr_collision(collision, butBgColour) then gosub NewBackground
    if gr_collision(collision, butAbout) then call ShowAbout(about)
    if gr_collision(collision, butHelp) then call TimerHelp(allHelp)
    
    call PlaySound(sound, buttonClick) : call Buzzer(buzz)
    gr.hide allOptionsMenu
    gr.hide darkWindow
    gr.show lapWin
    if showLapWindow then call HideShowLapWindow(TRUE) % show
    gosub WaitKeyUp
    call Render()
return

!------------------------------------------------
! Select a new background colour
!------------------------------------------------
NewBackground:
    gr.show colourPick 
    call Render()
    call PlaySound(sound, buttonClick) : call Buzzer(buzz)
    gosub WaitKeyUp
    do 
        gr.touch t, tx, ty
        if t then
            x = tx : y = ty
            tx /= scale_x : ty /= scale_y
            gr.modify collision, "x", tx, "y", ty 
            if gr_collision(collision, colourPick) then
                gr.get.pixel x, y, alpha, redB, greenB, blueB
                gr.color 255, redB, greenB, blueB, , pBg
            endif
        endif
        gosub ShowTimer
    until t
    gr.hide colourPick
return

!------------------------------------------------
! If a up or down button is pressed then alter the time accordingly 
!------------------------------------------------
ButtonUpDown:
    if _timer | _stopWatch then return
    do
        if gr_collision(collision, butHourUp) then
            if hour < 99 then hour ++
        endif
        if gr_collision(collision, butHourDown) then
            if hour > 0 then hour --
        endif
        
        if gr_collision(collision, butMinuetUp) then
            if minute < 59 then minute ++
        endif
        
        if gr_collision(collision, butMinuetDown) then
            if minute > 0 then minute --
        endif
        
        if gr_collision(collision, butSecondUp) then
            if second < 59 then second ++
        endif
        
        if gr_collision(collision, butSecondDown) then
            if second > 0 then second --
        endif
        gosub SetTimer
        pause 150
        gr.touch tt, nul, null
    until ! tt
return

!------------------------------------------------
! Setthe timer
!------------------------------------------------
SetTimer:
    h$ = int$(hour) : m$ = int$(minute) : s$ = int$(second)
    if hour < 10 then h$ = "0" + int$(hour)
    if minute < 10 then m$ = "0" + int$(minute) 
    if second < 10 then s$ = "0" + int$(second)
    stringTime$ = h$ + ":" + m$ + ":" + s$
    gr.modify txtTimer, "text", stringTime$
    call Render()
return

!------------------------------------------------
! Run the timer
!------------------------------------------------
ShowTimer:
    if _stopWatch then gosub AdvanceStopWatch : return
    if _timer then gosub CountDownTime : return
    if start then
        if hour = 0 & minute = 0 & second = 0 then
            _stopWatch = TRUE : now = time()
        else 
            _timer = TRUE : now = time()
            gosub GetAllSeconds
        endif
    endif
return

!------------------------------------------------
! Run the stop watch
!------------------------------------------------
AdvanceStopWatch:
    if _pauseTimer then wait = time() - now - allSeconds : return
   
    allSeconds = time() - now - wait
    hrs = 0 : mins = 0 : secs = 0
    secs = allSeconds / 1000
    gosub ShowHrsMinSec
return

!------------------------------------------------
!Show the hours minuets and seconds
!------------------------------------------------
ShowHrsMinSec:
    while secs >= 60
        secs = secs - 60
        mins = mins + 1
    repeat

    while mins >= 60
        mins = mins - 60 
        hrs = hrs + 1
    repeat
    
    hrs$ = int$(hrs) : mins$ = int$(mins) : secs$ = int$(secs)
    if hrs < 10 then hrs$ = "0" + int$(hrs)
    if mins < 10 then mins$ = "0" + int$(mins)
    if secs < 10 then secs$ = "0" + int$(secs)
    stringTime$ = hrs$ + ":" + mins$ + ":" + secs$
    gr.modify txtTimer, "text", stringTime$
   
    call Render()
return

!------------------------------------------------
! Conver the set time to seconds
!------------------------------------------------
GetAllSeconds:
    hour = val(left$(stringTime$, 2))
    minuet = val(mid$(stringTime$, 4, 2))
    second = val(right$(stringTime$, 2))
    allSeconds = (hour * 60) * 60
    allSeconds += (minute * 60) + second
return

!------------------------------------------------
! Timer counts down to zero
!------------------------------------------------
CountDownTime:
    if _pauseTimer then return
    countDownFlag = FALSE
    if time() - now - wait >= 1000 then
        allSeconds --
        now = time() 
        countDownFlag = TRUE
    endif
    if ! countDownFlag then return
    
    hrs = 0 : mins = 0 : secs = 0
    if allSeconds < 0 then allSeconds = 0 : gosub SoundAlarm
    secs = allSeconds
    gosub ShowHrsMinSec
return

!------------------------------------------------
! Timer counted down to zero. Sound Alarm 
!------------------------------------------------
SoundAlarm:
    if sound then Tone 2000, 500 , 0
    call Buzzer(buzz)
return

!------------------------------------------------
! Save sound, Buzz and background colours
!------------------------------------------------
SaveData:
    file.exists ok, fn$
    if ! ok then file.mkdir dataPath$

    text.open w,hs,fn$
    text.writeln hs,int$(redB)
    text.writeln hs,int$(greenB)
    text.writeln hs,int$(blueB)
    text.writeln hs,int$(sound)
    text.writeln hs,int$(buzz)
    text.close hs
return

!------------------------------------------------
! Load sound, Buzz and background colours
!------------------------------------------------
LoadData:
    file.exists ok, fn$
    if ok then
        text.open r,hs, fn$
        text.readln hs,redB$
        text.readln hs,greenB$
        text.readln hs,blueB$
        text.readln hs,sound$
        text.readln hs,buzz$
        text.close hs
        redB = val(redB$)
        greenB = val(greenB$)
        blueB = val(blueB$)
        sound = val(sound$)
        buzz = val(buzz$)
    endif
return

!------------------------------------------------
! All functions below
!------------------------------------------------

Functions:
!------------------------------------------------
! Draw a button with text
!------------------------------------------------
    fn.def DrawButton(bx, by, xw, yh, ptrText, butText$)
        locals.off
        gr.rect ptrButton, bx, by, xw, yh, 15, 15
        gr.modify ptrButton, "paint", darkGray
        ! White boarder around buttons
        gr.set.stroke 2 : gr.color 255, 255, 255, 255, 0
        gr.rect null, bx, by, xw, yh, 15, 15
        gr.set.stroke 1 : gr.color 255, 255, 255, 255, 1
        
        ! Text on Button
        diff = HYPOT(bx - xw, 0 - 0)
        gr.text.width txtLen, butText$
        tx = bx + (diff / 2) - (txtLen / 2)
        gr.text.draw ptrText, tx, by + 50, butText$
        
        locals.on
        fn.rtn ptrButton
    fn.end

!------------------------------------------------
! Hide or show the lap/split window
!------------------------------------------------
    fn.def HideShowLapWindow(hideShow)
        locals.off
        if hideShow then 
            gr.show lapWin
            gr.show lapWindow
            for show = 1 to lapCount
                gr.show laps[show]
            next
        else
            gr.hide lapWin
            gr.hide lapWindow
            for hide = 1 to lapCount
                gr.hide laps[hide]
            next
        endif
        call Render()
        locals.on
    fn.end
    
!------------------------------------------------
! Display about box
!------------------------------------------------
    fn.def ShowAbout(about)
        locals.off
        call PlaySound(sound, buttonClick) : call Buzzer(buzz)
        gosub WaitKeyUp
        if showLapWindow then call HideShowLapWindow(FALSE) % hide
        gr.hide lapWin
        gr.show about
        call Render()
        do 
            gr.touch t, null, null 
            if ! t then gosub ShowTimer 
        until t
        gr.hide about
        gr.show lapWin
        if showLapWindow then call HideShowLapWindow(TRUE) % show
        locals.on
    fn.end

!------------------------------------------------
! Display help for the timer
!------------------------------------------------ 
    fn.def TimerHelp(allHelp)
        locals.off
        call PlaySound(sound, buttonClick) : call Buzzer(buzz)
        gosub WaitKeyUp
        if showLapWindow then call HideShowLapWindow(FALSE) % hide
        gr.hide lapWin
        gr.show allHelp
        call Render()
        do 
            gr.touch t, null, null 
            if ! t then gosub ShowTimer 
        until t
        gr.hide allHelp
        gr.show lapWin
        if showLapWindow then call HideShowLapWindow(TRUE) % show
        locals.on
    fn.end
    
!------------------------------------------------
! Replace a character in a string
!------------------------------------------------    
    fn.def ReplaceChr$(string$, place, character$)
        string$ = left$(string$, place - 1) + character$ + mid$(string$, place + 1)
        fn.rtn string$
    fn.end

!------------------------------------------------
! Render if not in background
!------------------------------------------------
    fn.def Render()
        if ! background() then gr.render
    fn.end
    
!------------------------------------------------
! if sound then Play sound (ptr)
!------------------------------------------------
    fn.def PlaySound(sound, ptr)
        if sound then soundpool.play s,ptr,0.99,0.99,1,0,1
    fn.end
    
!------------------------------------------------
! if buzz then  vibrate
!------------------------------------------------
    fn.def Buzzer(buzz)
        if buzz then 
            array.load buzzGame[], 1, 100
            vibrate buzzGame[], -1
            array.delete buzzGame[]
        endif
    fn.end
  
!------------------------------------------------
! Load Sound files not found
!------------------------------------------------
    fn.def SoundLoadError(ptr, e$)
        if ptr = 0 then
            dialog.message "Sound file " + e$ + " not found", "App will end",OK,"OK" 
            end
        endif
    fn.end
    
!------------------------------------------------
! Load image file not found
!------------------------------------------------
    fn.def ImageLoadError(ptr, e$)
        if ptr =- 1 then
            dialog.message "Image file " + e$ + " not found", "App will end",OK,"OK" 
            end
        endif
    fn.end
    
!------------------------------------------------
! Double tap Back Key to end
!------------------------------------------------
    fn.def BackKey(backKeyHit)
        locals.off
        inTime = 3 % Three seconds 
        if backKeyHit = 3 then backKeyHit = 0
        
        if backKeyHit = 1 then 
            outTime = time() - backTick 
            outTime /= 1000 
            if outTime > inTime then backKeyHit = 0
        endif
        
        backKeyHit ++
        
        if backKeyHit = 1 then 
            backTick = time() 
            popup "Press again to Exit",, screenHeight / 2
        endif
       
        if backKeyHit = 2 then 
            backTock = time() - backTick
            backTock /= 1000
            if backTock <= inTime then gosub SaveData : exit
        endif
        locals.on
        fn.rtn backKeyHit
    fn.end
return


