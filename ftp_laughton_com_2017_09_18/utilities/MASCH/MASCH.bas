INCLUDE "GW.bas"
PAUSE 500

Init:
!'== == == == == == == == == == == ==
LIST.CREATE s, files %' Used by ListRecursiveFiles
LIST.CREATE s, folders %' Used by ListFolders
LIST.CREATE s, filenames %' Used to analyze doubles
ARRAY.LOAD folders$[], ".."
sdpath$="../.."
src$="/"

Functions:
!'== == == == == == == == == == == ==
FN.DEF FileName$(fullpath$)
  i=IS_IN("/", fullpath$, -1)
  IF !i THEN FN.RTN fullpath$
  FN.RTN MID$(fullpath$, i+1)
FN.END

FN.DEF Short$(e$)
  IF e$<>"/" THEN e$=LEFT$(e$,-1)
  IF LEN(e$)>20 THEN
    e$=LEFT$(e$,10)+"..."+RIGHT$(e$,10)
  END IF
  FN.RTN e$
FN.END

FN.DEF MoveFile(src$, tgt$)
  IF src$=tgt$ THEN FN.RTN 0
  FILE.DELETE f, tgt$
  BYTE.OPEN r, fid, src$
  BYTE.COPY fid, tgt$
  FILE.DELETE f, src$
FN.END

FN.DEF ListRecursiveFiles(path$, list)
  FILE.DIR path$, all$[], "/"
  ARRAY.LENGTH al, all$[]
  FOR i=1 TO al
    BUNDLE.GET 1, "LRF_state", state$ %' trick to cancel a recursive function
    IF state$="canceled" THEN FN.RTN 0
    IF GW_ACTION$()="BACK" THEN %' canceled by user
      BUNDLE.PUT 1, "LRF_state", "canceled"
      FN.RTN 0
    END IF
    IF LEFT$(all$[i], 1)="." THEN F_N.CONTINUE %' skip hidden/system files/folders
    IF RIGHT$(all$[i], 1)="/" THEN
      ListRecursiveFiles(path$+all$[i], list) %' it's a folder
    ELSE
      LIST.ADD list, path$+all$[i] %' it's a file
    END IF
  NEXT
  FN.RTN 1
FN.END

GW Pages Definition:
!'== == == == == == == == == == == ==
fldr=GW_NEW_PAGE()
GW_USE_THEME_CUSTO_ONCE("color=b inline iconpos=notext icon=recycle")
tb=GW_ADD_TITLEBAR(fldr, ~
   GW_ADD_BAR_LBUTTON$ (">REFRESH") ~
  +GW_ADD_BAR_TITLE$("MASCH: "+src$) ~
  +GW_ADD_BAR_RBUTTON$ ("Go!>GO"))
GW_ADD_TEXT(fldr, "Choose your root folder then hit Go!\n" ~
  +"It will <b>M</b>ove <b>A</b>ll <b>S</b>ubfolders <b>C</b>ontent <b>H</b>ere ")
lv = GW_ADD_LISTVIEW (fldr, folders$[])

scan=GW_NEW_PAGE()
GW_START_CENTER(scan)
tx=GW_ADD_TEXTBOX(scan, "Listing files in <b>"+src$+"</b> and subfolders")
GW_ADD_TEXT(scan, "")
GW_ADD_TEXT(scan, "Please be patient...")
GW_STOP_CENTER(scan)
GW_USE_THEME_CUSTO_ONCE("color=a")
spin=GW_ADD_SPINNER(scan, "Scanning...")

anlz=GW_NEW_PAGE()
GW_ADD_TITLEBAR(anlz, ~
   GW_ADD_BAR_TITLE$("MASCH") ~
  +GW_ADD_BAR_RBUTTON$ ("Skip>SKIP"))
GW_ADD_TEXT(anlz, "")
res=GW_ADD_TEXTBOX(anlz, "N files found in <b>"+src$+"</b> and subfolders")
GW_ADD_TEXT(anlz, "")
pgb=GW_ADD_PROGRESSBAR(anlz, "Analyzing doubles...")
GW_ADD_TEXT(anlz, "")
dbl=GW_ADD_TEXTBOX(anlz, "0 doubles found")
spin2=GW_ADD_SPINNER(anlz, "")

choice=GW_NEW_PAGE()
GW_ADD_TITLEBAR(choice, ~
   GW_ADD_BAR_LBUTTON$ ("Back>BACK") ~
  +GW_ADD_BAR_TITLE$("MASCH"))
GW_ADD_TEXT(choice, "")
GW_ADD_TEXT(choice, "<b>Analysis of doubles incomplete!</b>")
GW_ADD_TEXT(choice, "Press <b>Back</b> to continue analysis -or-")
GW_ADD_TEXT(choice, "Rename files by appending subfolder name before moving them (safer choice):")
GW_USE_THEME_CUSTO_ONCE ("style='background:green; color:white'")
GW_ADD_BUTTON (choice, "RENAME FILES", "RENAME")
GW_ADD_TEXT(choice, "Or force copy original filenames. Any conflicting file may be overwritten and lost!")
GW_USE_THEME_CUSTO_ONCE ("style='background:red; color:white'")
GW_ADD_BUTTON (choice, "FORCE COPY", "FORCE")

sure=GW_NEW_PAGE()
GW_ADD_TITLEBAR(sure, ~
   GW_ADD_BAR_LBUTTON$ ("Cancel>CANCEL") ~
  +GW_ADD_BAR_TITLE$("MASCH"))
GW_ADD_TEXT(sure, "")
GW_USE_THEME_CUSTO_ONCE("style='color:red'")
suti=GW_ADD_TEXTBOX(sure, "Do you confirm moving N files from subfolders to root: <b>"+src$+"</b> ?")
sute=GW_ADD_TEXT(sure, "")
GW_USE_THEME_CUSTO_ONCE ("style='background:red; color:white'")
GW_ADD_BUTTON (sure, "MOVE FILES", "MOVE")

move=GW_NEW_PAGE()
GW_ADD_TITLEBAR(move, GW_ADD_BAR_TITLE$("MASCH"))
GW_ADD_TEXT(move, "")
mvt=GW_ADD_TEXTBOX(move, "Moving N files from its subfolders to root folder <b>"+src$+"</b>")
GW_ADD_TEXT(move, "")
mvpg=GW_ADD_PROGRESSBAR(move, "")
GW_ADD_TEXT(move, "")
mvm=GW_ADD_TEXTBOX(move, "0 files moved")
GW_ADD_TEXT(move, "")
GW_USE_THEME_CUSTO_ONCE ("style='background:green; color:white'")
mvb=GW_ADD_BUTTON(move, "Done! Quit MASCH", "QUIT")


Main Program Loop:
!'== == == == == == == == == == == ==
ChooseSrc:
GOSUB ListFolders
GW_RENDER(fldr)
GW_MODIFY(tb, "title", "MASCH: "+Short$(src$))
GW_AMODIFY(lv, "content", folders$[])
DO
  r$=GW_WAIT_ACTION$()
  IF r$="REFRESH" THEN GOTO ChooseSrc
  IF LEFT$(r$,1)="@" THEN
    IF r$="@.." THEN
      src$=LEFT$(src$, IS_IN("/", src$, -2))
    ELSE
      src$+=MID$(r$,2)
    ENDIF
    GOTO ChooseSrc
  ELSEIF r$="BACK" THEN
    IF src$<>"/" THEN
      src$=LEFT$(src$, IS_IN("/", src$, -2))
      GOTO ChooseSrc
    ELSE
      POPUP "Bye!"
      EXIT
    END IF
  END IF
UNTIL r$="GO"

Scan:
GW_RENDER(scan)
GW_MODIFY(tx, "text", "Listing files in <b>"+src$+"</b> and subfolders")
GW_SHOW_SPINNER(spin)
LIST.CLEAR files
BUNDLE.PUT global, "LRF_state", "running" %' trick to cancel a recursive function
IF !ListRecursiveFiles(sdpath$+src$, files) THEN
  GOTO ChooseSrc
END IF

Analyze:
GW_RENDER(anlz)
LIST.SIZE files, nfiles
GW_MODIFY(res, "text", INT$(nfiles)+" files found in <b>"+src$+"</b>")
GOSUB AnalyzeDoubles
IF ax$="BACK" THEN GOTO ChooseSrc
IF ax$="SKIP" THEN
  IF dbls>0 THEN GOTO Confirm ELSE GOTO ChooseRename
END IF
PAUSE 1500

Confirm:
GW_RENDER(sure)
GW_MODIFY(suti, "text", "Do you confirm moving "+INT$(nfiles)+" files" ~
  +" from subfolders to root: <b>"+src$+"</b> ?")
IF dbls>0 THEN
  GW_MODIFY(sute, "text", "Doubles were found (files with same name, in different folders).\n" ~
    +"Thus, files will be renamed with leading subfolder name before being moved.")
END IF
r$=GW_WAIT_ACTION$()
IF r$="CANCEL" | r$="BACK" THEN GOTO ChooseSrc

Move:
GW_RENDER(move)
GW_DISABLE(mvb)
GW_MODIFY(mvt, "text", "Moving "+INT$(nfiles)+" files from its subfolders to root folder <b>"+src$+"</b>")
GOSUB DoTheMoving
GW_WAIT_ACTION$()
POPUP "Bye!"
EXIT

ChooseRename:
GW_RENDER(choice)
r$=GW_WAIT_ACTION$()
IF r$="BACK" THEN GOTO Analyze
IF r$="RENAME" THEN dbls=1
IF r$="FORCE" THEN dbls=0
GOTO Confirm

SUBs:
!'== == == == == == == == == == == ==

ListFolders:
!'-- -- -- -- -- -- -- -- -- -- -- --
LIST.CLEAR folders
FILE.DIR sdpath$+src$, all$[], "/"
LIST.ADD.ARRAY folders, all$[]
UNDIM all$[]
LIST.SIZE folders, nfolders
FOR i=nfolders TO 1 STEP -1
  LIST.GET folders, i, f$
  IF RIGHT$(f$, 1)<>"/" | LEFT$(f$, 1)="." THEN
    LIST.REMOVE folders, i
  ELSE
    LIST.REPLACE folders, i, LEFT$(f$,-1)+">@"+f$
  END IF
NEXT
IF src$<>"/" THEN LIST.INSERT folders, 1, "..>@.."
UNDIM folders$[]
LIST.TOARRAY folders, folders$[]
RETURN

AnalyzeDoubles:
!'-- -- -- -- -- -- -- -- -- -- -- --
GW_SHOW_SPINNER(spin2)
LIST.CLEAR filenames
FOR i=1 TO nfiles
  LIST.GET files, i, a$
  LIST.ADD filenames, FileName$(a$)
NEXT
LIST.TOARRAY filenames, fina$[]
GW_HIDE_SPINNER()
FOR i=1 TO nfiles
  ARRAY.SEARCH fina$[], fina$[i], found, i+1
  WHILE found
    dbls++
    GW_MODIFY(dbl, "text", INT$(dbls)+" doubles found")
    ARRAY.SEARCH fina$[], fina$[i], found, found+1
  REPEAT
  progress=INT(100*i/nfiles+1)
  GW_SET_PROGRESSBAR(pgb, progress)
  ax$=GW_ACTION$()
  IF ax$="BACK" | ax$="SKIP" THEN F_N.BREAK %' canceled by user
NEXT
UNDIM fina$[]
RETURN

DoTheMoving:
!'-- -- -- -- -- -- -- -- -- -- -- --
FOR i=1 TO nfiles
  LIST.GET files, i, from$
  LIST.GET filenames, i, fname$
  IF dbls>0 THEN %' append subfolder name
    fname$=REPLACE$(from$, src$, "")
    fname$=REPLACE$(dest$, "/", " ")
  END IF
  dest$=sdpath$+src$+fname$
  MoveFile(from$, dest$)
  progress=INT(100*i/nfiles+1)
  GW_SET_PROGRESSBAR(mvpg, progress)
  GW_MODIFY(mvm, "text", INT$(i)+" files moved")
NEXT
FOR i=2 TO nfolders
  LIST.GET folders, i, f$
  f$=MID$(f$, IS_IN("@", f$, -1)+1)
  FILE.DELETE f, sdpath$+src$+f$
NEXT
GW_ENABLE(mvb)
RETURN
