! This is the INCLUDE file "str_utils.bas" for RFO-BASIC! available under the terms of a GNU GPL V3 license.
! It contains useful functions to handle strings. Download at http://laughton.com/basic/programs/utilities/mougino
!
! nb = TALLY(main$, sub$)
!     - return the number of occurence of sub$ inside main$
! a$ = BUILD$(e$, n)
!     - return a string composed of n occurences of e$
! a$ = PARSE$(main$, sep$, n)
!     - return the delimited field #n of the string main$ split on the delimiter sep$
!     - e.g. PARSE$("a/b/c", "/", 2) returns "b"
! a$ = InsertBefore$(buf$, where$, ins$)
!     - return a string with ins$ inserted in buf$ before the first occurence of where$ (search is case insensitive)
! a$ = InsertAfter$(buf$, where$, ins$)
!     - return a string with ins$ inserted in buf$ after the first occurence of where$ (search is case insensitive)
! a$ = Isolate$(buf$, chunk$)
!     - return the first substring inside buf$ containing chunk$ delimited by 2 end-of-line characters
!     - e.g. Isolate$("abc \n def \n hij", "e") will return " def "

FN.DEF TALLY(main$, sub$)
  i=IS_IN(sub$, main$)
  WHILE i
    n++
    i=IS_IN(sub$, main$, i+1)
  REPEAT
  FN.RTN n
FN.END

FN.DEF BUILD$(e$, n)
  FOR i=1 TO n
    r$+=e$
  NEXT
  FN.RTN r$
FN.END

FN.DEF PARSE$(main$, sep$, n)
  main$=sep$ + TRIM$(main$, sep$) + sep$
  FOR k=1 TO n
    i=IS_IN(sep$, main$, i+1)
  NEXT
  IF !i THEN FN.RTN ""
  j=IS_IN(sep$, main$, i+1)
  IF !j THEN FN.RTN ""
  FN.RTN MID$(main$, i+1, j-i-1)
FN.END

FN.DEF InsertBefore$(buf$, where$, ins$)
    nbuf$ = buf$
    p = IS_IN(LOWER$(where$), LOWER$(buf$))
    IF p THEN nbuf$ = LEFT$(nbuf$, p-1) + ins$ + MID$(nbuf$, p)
    FN.RTN nbuf$
FN.END

FN.DEF InsertAfter$(buf$, where$, ins$)
    nbuf$ = buf$
    p = IS_IN(LOWER$(where$), LOWER$(buf$))
    IF p THEN p += LEN(where$) : nbuf$ = LEFT$(nbuf$, p) + ins$ + MID$(nbuf$, p+1)
    FN.RTN nbuf$
FN.END

FN.DEF Isolate$(buf$, chunk$)
  p = IS_IN(LOWER$(chunk$), LOWER$(buf$))
  IF p
    i = IS_IN("\n", buf$, p - LEN(buf$))
    IF i > 0
      j = IS_IN("\n", buf$, p)
      IF j > 0 THEN FN.RTN MID$(buf$, i+1, j-i-1)
    END IF
  END IF
FN.END

