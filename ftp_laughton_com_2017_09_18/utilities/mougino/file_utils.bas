! This is the INCLUDE file "file_utils.bas" for RFO-BASIC! available under the terms of a GNU GPL V3 license.
! It contains useful functions to handle files. Download at http://laughton.com/basic/programs/utilities/mougino
!
! PutFile(buf$, path$)
!     - put the binary string buf$ inside the file at path$. if the file already exists, it is overwritten
! buf$ = GetFile$(path$)
!     - return the content (binary string) of the file at path$
! FileCopy(src$, tgt$)
!     - copy the source file at src$ to the destination file 'tgt$'. both paths are relative to rfo-basic/data
! test = SysExist(fileName$)
!     - return 1 if the file named fileName$ exists in /data/data/com.rfo.compiler/ or 0 otherwise
! RecursiveDir(path$, list)
!     - list recursively the files and folders inside path$. put the result into the string list 'list'
! MakeRecursivePath(path$)
!     - recursively create the folder tree starting in rfo-basic/data and up to path$
!     - e.g. MakeRecursivePath("a/b/c") will create rfo-basic/data/a/b/c/
! DelRecursivePath(path$, list, del_root_too)
!     - recursively delete the files and folders inside path$
!     - user must provide a string list as second parameter to hold the list of files and folders to delete
!     - if del_root_too is true (non zero) the root folder at path$ is also deleted
! CopyFromDataToSys(prog$)
!     - copy the file named prog$ from /sdcard/rfo-compiler/data/ to /data/data/com.rfo.compiler/
! GrantExecPerm(prog$)
!     - give the execution permission (555) to the file named prog$ inside /data/data/com.rfo.compiler/

INCLUDE "str_utils.bas" % some of the functions below need functions defined in "str_utils.bas"

FN.DEF PutFile(buf$, path$)
  i = IS_IN("/", path$, -1) % create target folder if it doesn't exist
  IF i THEN MakeRecursivePath(LEFT$(path$, i))
  BYTE.OPEN w, bid, path$
  BYTE.WRITE.BUFFER bid, buf$
  BYTE.CLOSE bid
FN.END

FN.DEF GetFile$(path$)
  FILE.EXISTS ok, path$
  IF !ok THEN FN.RTN ""
  FILE.SIZE lof, path$
  BYTE.OPEN r, bid, path$
  BYTE.READ.BUFFER bid, lof, buf$
  BYTE.CLOSE bid
  FN.RTN buf$
FN.END

FN.DEF FileCopy(src$, tgt$)
  BYTE.OPEN r, fid, src$
  BYTE.COPY fid, tgt$
FN.END

FN.DEF SysExist(fileName$)
  sys$ = "/data/data/com.rfo.compiler/"
  bsys$ = "../../../../../../.." + sys$
  IF LEFT$(fileName$,1) = "+" THEN fileName$ = MID$(fileName$,2)
  FILE.EXISTS ex, bsys$ + fileName$
  FN.RTN ex
FN.END

FN.DEF RecursiveDir(path$, list)
  IF RIGHT$(path$, 1) <> "/" THEN path$ += "/"
  FILE.DIR path$, all$[], "/"
  ARRAY.LENGTH al, all$[]
  FOR i=1 TO al
    IF RIGHT$(all$[i], 1) = "/" THEN
      LIST.ADD list, path$ + all$[i]
      RecursiveDir(path$ + all$[i], list) %' it's a folder
    ELSE
      LIST.ADD list, path$ + all$[i] %' it's a file
    END IF
  NEXT
FN.END

FN.DEF MakeRecursivePath(path$)
  path$ = RTRIM$(path$, "/")
  FOR i=1 TO TALLY(path$, "/")
    cpath$ += "/" + PARSE$(path$, "/", i)
    FILE.MKDIR MID$(cpath$, 2)
  NEXT
FN.END

FN.DEF DelRecursivePath(path$, list, del_root_too)
  LIST.CLEAR list
  RecursiveDir(path$, list)
  LIST.SIZE list, nfiles
  FOR i=nfiles TO 1 STEP -1
    LIST.GET list, i, file$
    FILE.DELETE fid, file$
  NEXT
  IF del_root_too THEN FILE.DELETE fid, path$
FN.END

FN.DEF CopyFromDataToSys(prog$)
  data$ = "/sdcard/rfo-compiler/data/"
  sys$ = "/data/data/com.rfo.compiler/"
  SHELL("cat " + data$ + prog$ + " > " + sys$ + prog$)
FN.END

FN.DEF GrantExecPerm(prog$)
  sys$ = "/data/data/com.rfo.compiler/"
  SHELL("chmod 555 " + sys$ + prog$)
FN.END

