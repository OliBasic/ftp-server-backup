
INCLUDE "GW.bas"
GW_DEFAULT_TRANSITIONS("page=pop")
INCLUDE "GW_PICK_FILE.bas"

FN.DEF filename$(path$)
 i=IS_IN("/", path$, -1)
 IF !i THEN FN.RTN path$
 FN.RTN MID$(path$, i+1)
FN.END

FN.DEF asc$(a, b)
 FOR i=a TO b
  e$+=CHR$(i)+" "
 NEXT
 FN.RTN e$
FN.END

fox$="The quick brown fox jumps over the lazy dog"

IF IS_APK() THEN fnt$=COMMAND$()

Start:
IF fnt$=""
  POPUP "Pick a Font file\n   (*.ttf, *.otf)"
  fnt$ = GW_PICK_FILE$("*.ttf,*.otf")
END IF

IF fnt$="" THEN GOTO Bye

p=gw_new_page()
gw_add_titlebar(p, filename$(fnt$))
slider=gw_add_slider(p,"Font size in pixels:",8,72,1,16)
gw_add_listener(p, slider, "change", "SIZ")

resizable=gw_new_class("resizable")
myfnt$=gw_add_font$(p, fnt$)
custo=gw_new_theme_custo("class='resizable' font="+myfnt$)

gw_use_theme_custo(custo)
gw_add_text(p, fox$)
gw_add_text(p, asc$(65,90))  % A...Z
gw_add_text(p, asc$(97,122)) % a...z
gw_add_text(p, asc$(48,57))  % 0...9
gw_add_text(p, asc$(33,43)+asc$(58,64)) % *$#..
gw_reset_theme_custo()

gw_add_button(p, "PICK ANOTHER FONT", "NEW")

gw_render(p)

DO
 r$=gw_wait_action$()
 IF r$="SIZ"
  size$=gw_get_value$(slider)
  gw_modify(resizable, "style:font-size", size$+"px")
 END IF
UNTIL r$="NEW" | r$="BACK"

IF r$="NEW" THEN fnt$="" : GOTO Start

Bye:
IF IS_APK() THEN EXIT ELSE END
