! YesNo dialog box demo
! Aat Don @2013
FN.DEF CreateDialog$(sx,sy,Title$,Message1$,Message2$,Yes$,No$)
  ARRAY.LOAD a[],0,7,1,4,2,3,3,2,4,1,7,0,492,0,495,1,496,2,497,3,498,4,499,7,499,292,498,295,497,296,496,297,495,298,492,299,7,299,4,298,3,297,2,296,1,295,0,292
  LIST.CREATE n, OutLine
  LIST.ADD.ARRAY OutLine, a[]
  ARRAY.LOAD b[],438,90,438,86,427,86,425,84,428,81,428,79,453,66,457,72,441,81,444,87,456,81,468,103,426,126,437,147,422,156,423,159,426,159,426,174,433,186,425,186,420,178,415,186,406,186,414,171,415,158,410,151,421,144,414,131,384,147,372,126
  LIST.CREATE n, Telescope
  LIST.ADD.ARRAY Telescope, b[]
  ARRAY.LOAD c[],400,147,403,152,406,150,406,147,404,145
  LIST.CREATE n, P1
  LIST.ADD.ARRAY P1, c[]
  ARRAY.LOAD d[],433,170,441,184,457,176,449,162
  LIST.CREATE n, P2
  LIST.ADD.ARRAY P2, d[]
  GR.BITMAP.CREATE dialog,500,300
  GR.BITMAP.DRAWINTO.START dialog
    GR.SET.ANTIALIAS 0
    GR.SET.STROKE 4
    RStep=(255-130)/497
    GStep=(140-200)/497
    BStep=(120-255)/497
    ! Rectangle
    FOR x=1 TO 4
      GR.COLOR 255,sR,sG,sB,0
      GR.LINE gl,x,6-x,x,293+x
      sR=x*RStep+130
      sG=x*GStep+200
      sB=x*BStep+255
    NEXT x
    FOR x=5 TO 8
      GR.COLOR 255,sR,sG,sB,0
      GR.LINE gl,x,2,x,297
      sR=x*RStep+130
      sG=x*GStep+200
      sB=x*BStep+255
    NEXT x
    FOR x=9 TO 490
      GR.COLOR 255,sR,sG,sB,0
      GR.LINE gl,x,1,x,298
      sR=x*RStep+130
      sG=x*GStep+200
      sB=x*BStep+255
    NEXT x
    FOR x=491 TO 494
      GR.COLOR 255,sR,sG,sB,0
      GR.LINE gl,x,2,x,297
      sR=x*RStep+130
      sG=x*GStep+200
      sB=x*BStep+255
    NEXT x
    FOR x=495 TO 498
      GR.COLOR 255,sR,sG,sB,0
      GR.LINE gl,x,x-492,x,792-x
      sR=x*RStep+130
      sG=x*GStep+200
      sB=x*BStep+255
    NEXT x
    
    GR.SET.ANTIALIAS 1
    GR.COLOR 255,0,0,0,0
    GR.POLY g,OutLine
    GR.COLOR 255,255,255,255,1
    GR.RECT g,14,55,485,285
    GR.COLOR 255,0,0,0,0
    GR.RECT g,14,55,485,285
    GR.COLOR 255,255,192,255,1
    GR.RECT g,15,190,484,284
    FOR i=0 TO 4
      GR.COLOR 255,60-i*15,195+i*15,60-i*15,1
      GR.RECT g,28+2*i,204+2*i,236-2*i,270-2*i
    NEXT i
    FOR i=0 TO 4
      GR.COLOR 255,195+i*15,60-i*15,60-i*15,1
      GR.RECT g,262+2*i,204+2*i,470-2*i,270-2*i
    NEXT i
    GR.COLOR 255,166,166,166,1
    GR.RECT g,354,58,482,186
    GR.COLOR 255,255,255,255,1
    GR.CIRCLE g,367,88,3
    GR.CIRCLE g,379,94,3
    GR.CIRCLE g,384,77,3
    GR.CIRCLE g,388,87,3
    GR.CIRCLE g,400,68,3
    GR.CIRCLE g,418,66,3
    GR.COLOR 255,0,0,0,1
    GR.POLY g,Telescope
    GR.POLY g,P1
    GR.POLY g,P2
    GR.SET.STROKE 2
    GR.LINE g,419,140,404,147
    GR.SET.STROKE 5
    GR.LINE g,433,150,452,186
  GR.TEXT.ALIGN 2
  GR.TEXT.SIZE 50
  GR.TEXT.BOLD 1
  GR.COLOR 255,0,0,0,1
  GR.TEXT.DRAW g,250,45,Title$
  GR.TEXT.DRAW g,133,255,Yes$
  GR.TEXT.DRAW g,369,255,No$
  GR.TEXT.SIZE 40
  GR.TEXT.DRAW g,190,110,Message1$
  GR.TEXT.DRAW g,190,155,Message2$
  GR.TEXT.ALIGN 1
  GR.TEXT.BOLD 0
  GR.BITMAP.DRAWINTO.END
  GR.BITMAP.DRAW nDialog,dialog,100,200
  GR.RENDER
  DO
    DO
      GR.TOUCH touched,x,y
      GR.RENDER
    UNTIL touched
    DO
      GR.TOUCH touched,x,y
      GR.RENDER
    UNTIL !touched
    x=ROUND(x/sx)
    y=ROUND(y/sy)
  UNTIL (x>126 & x<339 & y>403 & y<475) | (x>362 & x<575 & y>403 & y<475)
  IF x>126 & x<339 & y>403 & y<475 THEN
    Answer$="Yes"
  ELSE
    Answer$="No"
  ENDIF
  GR.HIDE nDialog
  GR.RENDER
  UNDIM a[]
  UNDIM b[]
  UNDIM c[]
  UNDIM d[]
  FN.RTN Answer$
FN.END

GR.OPEN 255,0,0,0,0,-1
  GR.SCREEN w,h
  ScaleX=720
  ScaleY=w/h*ScaleX
  sx=h/ScaleX
  sy=w/ScaleY
  GR.SCALE sx,sy
  GR.CLS
  WAKELOCK 3
  Kap=0
  pi=3.1415
  L1=225
  L2=165
  n=5
  DO
    d=0
    GR.CLS
    GR.TEXT.SIZE 30
    GR.COLOR 255,255,255,0,1
    GR.TEXT.DRAW g,135,300,"Back key to quit"
    GR.SET.STROKE 5
    FOR q=0 TO 8
      GR.COLOR 255,128+RND()*128,128+RND()*128,128+RND()*128,1
      FOR angle=0 TO 360-n STEP n
        x1=L1*COS(angle*pi/180)+256
        y1=L2*SIN(angle*pi/180)+L2+d
        x2=L1*COS((angle+n)*pi/180)+256
        y2=L2*SIN((angle+n)*pi/180)+L2+d
        d=d+0.4
        GR.LINE g,x1,y1,x2,y2
        GR.RENDER
        IF Kap THEN F_N.BREAK
      NEXT angle
      IF Kap THEN F_N.BREAK
    NEXT q
  UNTIL Kap
  WAKELOCK 5
GR.CLOSE
END "Thanks for having a look !\nPress Back again to quit !"

ONBACKKEY:
  Answer$=CreateDialog$(sx,sy,"QUIT","Are you","sure ?","Yes","No")
  IF Answer$="Yes" THEN
    Answer$=CreateDialog$(sx,sy,"EINDE","Weet je dit","zeker ?","Ja","Nee")
    IF Answer$="Yes" THEN
      Answer$=CreateDialog$(sx,sy,"FIN","Tu en es","certain ?","Oui","Non")
      IF Answer$="Yes" THEN
        Answer$=CreateDialog$(sx,sy,"ENDE","Weißt du das","sicher ?","Ja","Nein")
        IF Answer$="Yes" THEN
          Kap=1
          BACK.RESUME
        ELSE
          BACK.RESUME
        ENDIF
      ELSE
        BACK.RESUME
      ENDIF
    ELSE
      BACK.RESUME
    ENDIF
  ELSE
    BACK.RESUME
  ENDIF
    
    
