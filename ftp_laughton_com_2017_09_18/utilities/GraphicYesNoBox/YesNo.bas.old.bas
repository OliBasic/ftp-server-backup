! YesNo dialog box demo
! Aat Don @2013
FN.DEF CreateDialog$(sx,sy,Title$,Message1$,Message2$,Yes$,No$)
  GR.BITMAP.LOAD bmp,"dialog.png"
  GR.BITMAP.CREATE dialog,500,300
  GR.BITMAP.DRAWINTO.START dialog
  GR.BITMAP.DRAW g,bmp,0,0
  GR.TEXT.ALIGN 2
  GR.TEXT.SIZE 50
  GR.TEXT.BOLD 1
  GR.COLOR 255,0,0,0,1
  GR.TEXT.DRAW g,250,45,Title$
  GR.TEXT.DRAW g,133,255,Yes$
  GR.TEXT.DRAW g,369,255,No$
  GR.TEXT.SIZE 40
  GR.TEXT.DRAW g,190,110,Message1$
  GR.TEXT.DRAW g,190,155,Message2$
  GR.TEXT.ALIGN 1
  GR.TEXT.BOLD 0
  GR.BITMAP.DRAWINTO.END
  GR.BITMAP.DRAW nDialog,dialog,100,200
  GR.RENDER
  GR.BITMAP.DELETE bmp
  DO
    DO
      GR.TOUCH touched,x,y
      GR.RENDER
    UNTIL touched
    DO
      GR.TOUCH touched,x,y
      GR.RENDER
    UNTIL !touched
    x=ROUND(x/sx)
    y=ROUND(y/sy)
  UNTIL (x>126 & x<339 & y>403 & y<475) | (x>362 & x<575 & y>403 & y<475)
  IF x>126 & x<339 & y>403 & y<475 THEN
    Answer$="Yes"
  ELSE
    Answer$="No"
  ENDIF
  GR.HIDE nDialog
  GR.RENDER
  FN.RTN Answer$
FN.END

GR.OPEN 255,0,0,0,0,-1
  GR.SCREEN w,h
  ScaleX=720
  ScaleY=w/h*ScaleX
  sx=h/ScaleX
  sy=w/ScaleY
  GR.SCALE sx,sy
  GR.CLS
  WAKELOCK 3
  Kap=0
  pi=3.1415
  L1=225
  L2=165
  n=5
  DO
    d=0
    GR.CLS
    GR.TEXT.SIZE 30
    GR.COLOR 255,255,255,0,1
    GR.TEXT.DRAW g,135,300,"Back key to quit"
    GR.SET.STROKE 5
    FOR q=0 TO 8
      GR.COLOR 255,128+RND()*128,128+RND()*128,128+RND()*128,1
      FOR angle=0 TO 360-n STEP n
        x1=L1*COS(angle*pi/180)+256
        y1=L2*SIN(angle*pi/180)+L2+d
        x2=L1*COS((angle+n)*pi/180)+256
        y2=L2*SIN((angle+n)*pi/180)+L2+d
        d=d+0.4
        GR.LINE g,x1,y1,x2,y2
        GR.RENDER
        IF Kap THEN F_N.BREAK
      NEXT angle
      IF Kap THEN F_N.BREAK
    NEXT q
  UNTIL Kap
  WAKELOCK 5
GR.CLOSE
END "Thanks for having a look !\nPress Back again to quit !"

ONBACKKEY:
  Answer$=CreateDialog$(sx,sy,"QUIT","Are you","sure ?","Yes","No")
  IF Answer$="Yes" THEN
    Answer$=CreateDialog$(sx,sy,"EINDE","Weet je dit","zeker ?","Ja","Nee")
    IF Answer$="Yes" THEN
      Answer$=CreateDialog$(sx,sy,"FIN","Tu en es","certain ?","Oui","Non")
      IF Answer$="Yes" THEN
        Answer$=CreateDialog$(sx,sy,"ENDE","Weißt du das","sicher ?","Ja","Nein")
        IF Answer$="Yes" THEN
          Kap=1
          BACK.RESUME
        ELSE
          BACK.RESUME
        ENDIF
      ELSE
        BACK.RESUME
      ENDIF
    ELSE
      BACK.RESUME
    ENDIF
  ELSE
    BACK.RESUME
  ENDIF
    
    
