Rem Unicode Characters Demo
Rem For Androud
Rem October 2014
Rem By Roy
Rem

Rem Open Graphics
di_width = 1196
di_height = 720 

gr.open 255,0,255,0 % Green
gr.orientation 0
pause 1000
gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y

Rem Variables

array.load chessMen[],9820,9822,9821,9818,9819,9821,9822,9820
array.load dice[],9856,9857,9858,9859,9860,9861

Rem Start of Functions

fn.def Gcol(c,g)
	Rem g=0 Outline
	Rem g=1 Fill
	if c=0 then gr.color 255,0,0,0,g % black
	if c=1 then gr.color 255,255,0,0,g % Red
	if c=2 then gr.color 255,0,255,0,g % green
	if c=3 then gr.color 255,0,0,255,g % blue
	if c=4 then gr.color 255,255,255,0,g % yellow
	if c=5 then gr.color 255,0,255,255,g % cyan
	if c=6 then gr.color 255,255,0,255,g % magenta
	if c=7 then gr.color 255,192,192,192,g % gray
	if c=8 then gr.color 255,255,255,255,g % white
fn.end

fn.def ChessBoard(chessMen[],scale_x,scale_y)
	Array.length s, chessMen[]
	Rem Draw Chess Board
	gr.text.size 50
	call Gcol(0,1)
	gr.rect r,90,90,510,510
	call Gcol(8,0)
	gr.rect r,92,92,508,508
	gr.color 255,80,80,80,1
	gr.rect r,100,100,500,500
	call Gcol(7,1)
	n=100
	for y=100 to 450 step 50
		for x=n to 450 step 100
			gr.rect r,x,y,x+50,y+50
		next
		if n=100 then
			n=150
		else
			n=100
		endif
	next
	Rem Display Black Chess Men
	for g=1 to 0 step - 1
		x=100
		y=140
		call Gcol(0,1)
		if g=0 then call Gcol(8,0)
		for n=1 to s
			gr.text.draw d,x,y,chr$(chessMen[n])
			x+=50
		next
		x=100
		y+=50
		for n=1 to s
			gr.text.draw d,x,y,chr$(9823) % Pawns
			x+=50
		next
	next
	Rem Display White Chess Men
	for g=1 to 0 step - 1
		x=100
		y=490
		call Gcol(8,1)
		if g=0 then call Gcol(0,0)
		for n=1 to s
			gr.text.draw d,x,y,chr$(chessMen[n])
			x+=50
		next
		x=100
		y-=50
		for n=1 to s
			gr.text.draw d,x,y,chr$(9823) % Pawns
			x+=50
		next
	next
	Rem draw button to end
	call Gcol(6,1)
	gr.rect r,400,530,700,630
	call Gcol(0,1)
	call TextChr(480,600,0,"tap")
	gr.render
	do
		gr.touch touched,tx,ty
	until touched &tx>400*scale_x & ty>530*scale_y & tx<700*scale_x & ty<630*scale_y
	do
		gr.touch touched,tx,ty
	until !touched
fn.end

fn.def DraughtBoard()
	Rem Draw Draught Board
	gr.text.size 70
	call Gcol(0,1)
	gr.rect r,590,90,1010,510
	call Gcol(8,0)
	gr.rect r,592,92,1008,508
	gr.color 255,80,80,80,1
	gr.rect r,600,100,1000,500
	call Gcol(7,1)	
	n=600
	for y=100 to 450 step 50
		for x=n to 950 step 100
			gr.rect r,x,y,x+50,y+50
		next
		if n=600 then
			n=650
		else
			n=600
		endif
	next
		
	Rem Display Red Draughts
	call Gcol(1,1)
	n=605
	for y=150 to 250 step 50 
		for x=n to 955 step 100
			gr.text.draw d,x,y,chr$(9679)
		next
		if n=605 then
			n=655
		else
			n=605
		endif
	next

	Rem Display Blue Draughts
	call Gcol(3,1)
	n=655
	for y=400 to 500 step 50 
		for x=n to 955 step 100
			gr.text.draw d,x,y,chr$(9679)
		next
		if n=655 then
			n=605
		else
			n=655
		endif
	next
fn.end

fn.def RollDice(dice[],scale_x,scale_y)
	gr.cls
	gr.text.size 50
	flag=0
	Rem Set Dice at random
	call Gcol(6,1)
	gr.rect r,450,400,750,500
	call Gcol(0,1)
	gr.text.draw r,460,60,"Rolling Dice"
	call TextChr(520,470,0,"tap")
	gr.render
	gr.text.size 150
	Rem Dice Backing colour
	x=142
	y=79
	call Gcol(8,1)
	for n=1 to 6
		gr.rect s,x,y,x+100,y+100
		x+=150
	next
	Rem Place the dice 
	x=120
	y=180
	call Gcol(0,1)
	for n=1 to 6
		dice$=chr$(dice[6 * rnd()+1])
		gr.text.draw s,x,y,dice$
		x+=150
	next
	Rem Change dice at random and display
	do
		for n=10 to 15
			for x=1 to 6
				pause 100
				dice$=chr$(dice[6 * rnd()+1])
				gr.modify n ,"text",dice$
				gr.render
				gr.touch touched,tx,ty
				if touched & tx>450*scale_x & ty>400*scale_y & tx<750*scale_x & ty<500*scale_y then 
					flag=1
					f_n.break
				endif
			next
			if flag then f_n.break
		next
	until flag
	do
		gr.touch touched,tx,ty
	until !touched
fn.end

fn.def ScreenBoarder(ch,r,col,di_width,di_height)
	gr.text.size 50
	call Gcol(col,1)
	s$=chr$(ch)
	gr.text.width w,s$
	for x=1 to di_width step w
		s$=chr$(ch + (r * rnd() + 1))
		gr.text.draw d,x,w,s$
		s$=chr$(ch + (r * rnd() + 1))
		gr.text.draw d,x,di_height-w/2,s$
	next
	for y=w+w to di_height-w step w
		s$=chr$(ch + (r * rnd() + 1))
		gr.text.draw d,1,y,s$
		s$=chr$(ch + (r * rnd() + 1))
		gr.text.draw d,di_width-w+5,y,s$
	next
	gr.render
fn.end

fn.def DemoEnd(di_width,di_height)
	gr.cls
	gr.text.size 50
	s$=chr$(9618)
	gr.text.width w,s$
	for c = 6 to 7
	call Gcol(c,1)
	rem Do background
	if c=7 then s$=chr$(9619)
		for y=1 to di_height+w step w+w
			for x=1 to di_width step w
				gr.text.draw d,x,y,s$
			next
		next
	next
	Rem Do black Bars
	call Gcol(0,1)
	s$=chr$(9552)
	gr.text.width w1,s$
	for x=1 to di_width step w1
		gr.text.draw d,x,w1,s$
		gr.text.draw d,x,w1+160,s$
		gr.text.draw d,x,di_height-160,s$
		gr.text.draw d,x,di_height,s$
		!gr.render
	next
	Rem Do Love
	gr.text.size 200
	s$=chr$(9829)
	gr.text.width w,s$
	call Gcol(0,0)
	for c=0 to 1
	if c=1 then call Gcol(4,1)
		for x=1 to di_width step w
			gr.text.draw d,x,(w1*5)+10,s$
			gr.text.draw d,x,di_height-w1,s$
		next
	next
	gr.text.size 150
	call Gcol(1,1)
	for x=1 to di_width step w
		gr.text.draw d,x+25,(w1*5)-10,s$
		gr.text.draw d,x+25,di_height-w1-(w1/2),s$
	next
	Rem Do Text
	gr.text.size 200
	call Gcol(0,0)
	x=10
	y=400
	title$="End of Demo"
	gr.text.bold 1
	gr.text.draw t,x,y,title$
	call Gcol(1,1)
	gr.text.draw t,x,y,title$
	call Gcol(0,1)
	gr.text.size 50
	gr.text.draw d,190,500,chr$(9758)+" Tap to Repeat or Back Key to End "+chr$(9756)
	gr.render
	gr.text.bold 0
	gr.render
	call WaitForTouch()
	
fn.end

fn.def StartDemo(di_width,di_height)
	gr.cls
	gr.text.size 50
	s$=chr$(9618)
	gr.text.width w,s$
	for c = 4 to 5
	call Gcol(c,1)
	rem Do background
	if c=5 then s$=chr$(9619)
		for y=1 to di_height+w step w+w
			for x=1 to di_width step w
				gr.text.draw d,x,y,s$
			next
		next
	next
	
	call ScreenBoarder(9823,8,0,di_width,di_height)
	gr.text.size 300
	call Gcol(1,1)
	gr.text.draw d,45,450,chr$(9830)
	!call Gcol(0,0)
	gr.text.draw d,45,450,chr$(9830)
	call Gcol(1,1)
	gr.text.draw d,870,450,chr$(9830)
	!call Gcol(0,0)
	gr.text.draw d,870,450,chr$(9830)
	
	gr.text.size 150
	call Gcol(4,1)
	gr.text.draw d,165,395,chr$(9830)
	gr.text.draw d,75,395,chr$(9830)
	
	gr.text.draw d,990,395,chr$(9830)
	gr.text.draw d,900,395,chr$(9830)
	
	gr.text.size 300
	call Gcol(3,1)
	gr.text.draw d,50,250,chr$(9680)
	call Gcol(6,1)
	gr.text.draw d,50,250,chr$(9681)

	call Gcol(3,1)
	gr.text.draw d,970,250,chr$(9681)
	call Gcol(6,1)
	gr.text.draw d,970,250,chr$(9680)
	
	call Gcol(3,1)
	gr.text.draw d,50,680,chr$(9680)
	call Gcol(6,1)
	gr.text.draw d,50,680,chr$(9681)

	call Gcol(3,1)
	gr.text.draw d,970,680,chr$(9681)
	call Gcol(6,1)
	gr.text.draw d,970,680,chr$(9680)
		
	gr.text.size 50
	call Gcol(0,1)
	gr.text.draw d,310,360,"Unicode Demo, tap to Start"
	
	gr.render
	call WaitForTouch()
	gr.cls
fn.end

fn.def TextChr(x,y,c,s$)
	Rem Lower case only
	ns$=""
	call Gcol(c,1)
	w=len(s$)
	for n=1 to w 
		u=ucode(mid$(s$,n,1))
		if u=32 then
			ns$+=" "
		else
			ns$+=chr$(u+9327)
		endif
	next
	gr.text.draw d,x,y,ns$
	gr.render
fn.end

fn.def WaitForTouch()
	do
		gr.touch touched,tx,ty
	until touched
	do
		gr.touch touched,tx,ty
	until !touched
fn.end

Rem End of Functions

Rem Main
do
	gr.cls
	call StartDemo(di_width,di_height)
	call ScreenBoarder(9817,5,0,di_width,di_height)
	call DraughtBoard()
	call ChessBoard(chessMen[],scale_x,scale_y)
	call RollDice(dice[],scale_x,scale_y)
	call DemoEnd(di_width,di_height)

until 0

onBackKey:

!onError:

end"Thanks for trying Unicode character Demo"

	
