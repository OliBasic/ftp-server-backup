Rem UniCodeChar
Rem For Androud
Rem October 2014
Rem By Roy
Rem

console.title"Some UniCode Characters"

wakelock 3

Rem Start of Functions

fn.def Menu()
	cls
	!b$=chr$(9786)
	b$=chr$(9821)
	s$="    "
	print s$ + b$;".....M E N U.....";b$
	print s$ + b$;"......Boxes......";b$
	print s$ + b$;".....Numbers.....";b$
	print s$ + b$;".....Letters.....";b$
	print s$ + b$;".....Symbols.....";b$
	print s$ + b$;".ColouredSymbols.";b$
	print s$ + b$;".....Arrows......";b$
	print s$ + b$;"..Miscellaneous..";b$
	print s$ + b$;"......Help.......";b$
	print s$ + b$;"-+-+-+-+-+-+-+-+-";b$
	
fn.end

fn.def Help()
	call Menu()
	pb$="          "+chr$(9829)+"   "+chr$(9824)+"   "+chr$(9829)
	print"Help:"
	print"Unicode characters can be used in ";
	print"text or graphic mode. Select the ";
	print"category you want from the menu ";
	print"and the characters will be displayed ";
	print"along with their Unicode numbers."
	print pb$
	print"When finished scroll to the top ";
	print"of the screen to select another ";
	print"category or use the back key to end."
	print pb$
	print"Some devices will not display all the ";
	print"Unicode characters and a square ";
	print"box will be displayed instead. For ";
	print"example my old Samsung Galaxies ";
	print"dosen't displays as many characters ";
	print"as my new phone. None of the coloured ";
	print"symbols are displayed by me old phone. "
	print pb$
fn.end

fn.def Boxes()
	call menu()
	print"Boxes:"
	c=1
	for n=9472 to 9584
		if n<9548 | n>9551 then
			print chr$(n);" ";int$(n);"   ";
			c++
			if c=4 then
				print
				c=1
			endif
		endif
	next
	for n=9601 to 9621
		if n<9616 | n > 9617 then
			print chr$(n);" ";int$(n);"   ";
			c++
			if c=4 then
				print
				c=1
			endif
		endif
	next
	array.load numbers[],11035,11036,11093
	Array.length s, numbers[]
	!print
	c=2
	for n=1 to s
		print chr$(numbers[n]);" ";int$(numbers[n]);"   ";
		c++
		if c=3 then
			print
			c=1
		endif
	next
	print
fn.end

fn.def Numbers()
	call Menu()
	print"Numbers:"
	c=1
	for n=9312 to 9371
		print chr$(n);" ";int$(n);"   ";
		c++
		if c=4 then
			print
			c=1
		endif
	next
	for n=10102 to 10111
		print chr$(n);" ";int$(n);"   ";
		c++
		if c=4 then
			print
			c=1
		endif
	next
	print
	print
fn.end

fn.def Letters()
	call Menu()
	print"Letters:"
	c=1
	for n=9372 to 9449
		if n<9398 | n>9423 then
			print chr$(n);" ";int$(n);"   ";
			c++
			if c=4 then
				print
				c=1
			endif
		endif
	next
	print
	print
fn.end

fn.def Symbols()
	call Menu()
	print"Symbols:"
	array.load numbers[],8853,8855,8857
	array.length s,numbers[]
	c=1
	for n=1 to s 
		print chr$(numbers[n]);" ";int$(numbers[n]);"   ";
		c++
		if c=3 then
			c=1
			print
		endif
	next
	
	for n=9632 to 9643
		print chr$(n);" ";int$(n);"    ";
		c++
		if c=3 then
			c=1
			print
		endif
	next
	for n=9670 to 9681
		print chr$(n);" ";int$(n);"    ";
		c++
		if c=3 then
			c=1
			print
		endif
	next

	for n=9991 to 10066
		if n<9994 | n>10002 & n <> 10024 & n <> 10060 & n <> 10062 then
			print chr$(n);" ";int$(n);"   ";
			c++
			if c=3 then
				c=1
				print
			endif
		endif
	next
	print
fn.end

fn.def ColouredSymbols()
	call Menu()
	print"Coloured Symbols:"
	array.load numbers[],9729,9748,9749,9786,9851,9855,9875,9888,9889,9917,9918,9924,9925, ~
						9934,9940,9962,9970,9971,9973,9978,9981,9989,9994,9995,9996,8419,8986, ~
						8987,9200,9203,9757,9193,9194,9195,9196,10060,10062,10067,10068,10069,10024
	array.length s, numbers[]
	c=1
	for n=1 to s
		print chr$(numbers[n]);" ";int$(numbers[n]);"   ";
		c++
		if c=3 then
			print
			c=1
		endif
	next
	for n=9800 to 9811
		print chr$(n);" ";int$(n);"   ";
		c++
		if c=3 then
			c=1
			print
		endif
	next
	print
	print
fn.end

fn.def Miscellaneous()
	call Menu()
	print"Miscellaneous:"
	print"  Telephones:"
	print chr$(9742);" ";int$(9742);"   ";chr$(9743);" ";int$(9743)
	print chr$(9990);" ";int$(9990)
	
	print"  Chess:"
	c=1
	for n=9812 to 9823
		print chr$(n);" ";int$(n);"   ";
		c++
		if c=3 then
			c=1
			print
		endif
	next
	c=1
	print"  Cards:"
	for n=9824 to 9831
		print chr$(n);" ";int$(n);"   ";
		c++
		if c=3 then
			c=1
			print
		endif
	next
	c=1
	print"  Music:"
	for n=9833 to 9839
		if n<>9838 then 
			print chr$(n);" ";int$(n);"   ";
			c++
			if c=3 then
				c=1
				print
			endif
		endif
	next
	c=1
	print"  Dice:"
	for n=9856 to 9861
		print chr$(n);" ";int$(n);"   ";
		c++
		if c=3 then
			c=1
			print
		endif
	next
	c=1
	print"  Scissors:"
	for n=9985 to 9988
		print chr$(n);" ";int$(n);"   ";
		c++
		if c=3 then
			c=1
			print
		endif
	next
	print"  Pens:"
	for n=9997 to 10002
		print chr$(n);" ";int$(n);"   ";
		c++
		if c=3 then
			c=1
			print
		endif
	next
	print"  Others"
	array.load numbers[],169,174,186,247,64114,65533
	array.length s,numbers[]
	
	for n=1 to s 
		print chr$(numbers[n]);" ";int$(numbers[n]);"   ";
		c++
		if c=3 then
			c=1
			print
		endif
	next
	for n=4960 to 4968
		print chr$(n);" ";int$(n);"   ";
		c++
		if c=3 then
			c=1
			print
		endif
	next
	
	print
fn.end

fn.def Arrows()
	call Menu()
	print"Arrows:"
	c=1
	array.load numbers[],9650,9651,9654,9655,9658,9660,9661,9664,9665
	for x=1 to 9
		n=numbers[x]
		print chr$(n);"  ";int$(n);"   ";
		c++
		if c=3 then
			c=1
			print
		endif
	next
	for n=9698 to 9701
		print chr$(n);" ";int$(n);"   ";
		c++
		if c=3 then
			c=1
			print
		endif
	next
	
	for n=9756 to 9758
		print chr$(n);" ";int$(n);"   ";
		c++
		if c=3 then
			c=1
			print
		endif
	next

	for n=10136 to 10174
		if n<>10160 then
			print chr$(n);" ";int$(n);"   ";
			c++
			if c=3 then
				c=1
				print
			endif
		endif
	next
	for n=65513 to 65516
		print chr$(n);" ";int$(n);"   ";
		c++
		if c=3 then
			c=1
			print
		endif
	next
	print
fn.end

Rem End of functions

Rem Main

call Menu()
Rem Wait for menu item to be Selected
do
	pause 100
until 0

OnConsoleTouch:
Console.line.touched lineNumber

sw.begin lineNumber
		sw.case 2
			call Boxes()
			Sw.break
		sw.case 3
			call Numbers()
			Sw.break
		sw.case 4
			Call Letters()
			sw.break
		sw.case 5 
			call Symbols()
			sw.break
		sw.case 6
			call ColouredSymbols()			
			sw.break
		sw.case 7
			call Arrows()
			sw.break
		sw.case 8
			call Miscellaneous()
			sw.break
		sw.case 9
			call Help()
			sw.break
	sw.end


ConsoleTouch.Resume

onBackKey:

wakelock 5
!onError:

end"Thank you for Using Unicode Characters"