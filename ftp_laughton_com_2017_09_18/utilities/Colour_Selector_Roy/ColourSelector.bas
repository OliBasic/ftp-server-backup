Rem Text Mode Colour Selector
Rem Using ConLib.bas
Rem With RFO Basic!
Rem October 2015
Rem Version 1.00
Rem By Roy Shepherd

!di_height = 1120 % set to my Device
!di_width = 720
di_height = 720 % set to my Device
di_width = 1120

gr.open 255,0,0,0 % Black background colour
gr.color 255,255,255,255,1 % White foreground colour
gr.orientation 0
pause 1000
WakeLock 3 

gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y

!-------------------------------------------------
! Setup screen for drawing.
!-------------------------------------------------
gr.bitmap.create screen, di_width, di_height
gr.bitmap.draw    nn, screen, 0, 0
gr.bitmap.drawinto.start screen

!-------------------------------------------------
! include ConLib.bas after opening graphics 
! and setting up the bitmap screen and before creating any bundles 
!-------------------------------------------------
include ConLib.bas % include 'ConLib.bas' here

!-------------------------------------------------
! Put bundle's here, after the included ConLib.bas
! 'global' bundle pointer = 2.
!-------------------------------------------------
bundle.create global % bundle pointer = 2.
bundle.put global, "Divice_W", di_width
bundle.put global, "Divice_H", di_height

gosub Functions % Let basic see the functions before they are called
!-------------------------------------------------
! The 18 colours are:
! 0 = Black, 1 = Gray, 2 = Silver, 3 = White, 4 = Maroon, 5 = Red, 6 = Olive, 7 = Yellow,
! 8 = Green, 9 = Lime, 10 = Teal, 11 = Aqua, 12 = Navy, 13 = Blue, 14 = Purple, 
! 15 = Fuchsia, 16 = Brown, 17 = Gold.
!-------------------------------------------------

call BuildScreen()
call SelectColour()
do : pause 1 : until 0

onBackKey:
dialog.message "Exit Colour Selector",,yn,"Yes","No"
	if yn = 1 then end 
Back.resume
end

!-------------------------------------------------
! Start of the functions
!-------------------------------------------------
Functions:

!-------------------------------------------------
! Setup the screen
!-------------------------------------------------
fn.def BuildScreen()
	Con_ChrPerLine(60)
	Con_BackgroundName("Grey") : Con_Cls() : Con_Scroll(0)
	columns = Con_GetMaxX() : rows = Con_GetMaxY() : Con_RenderOff()
	call Box(1, 1, columns, 5, "Colour Selected","Yellow")
	call Box(1, 8, columns, 10, "Alpha","Black")
	call Box(1, 11, columns, 13, "Red","Red")
	call Box(1, 14, columns, 16, "Green","Green")
	call Box(1, 17, columns, 19, "Blue","Blue")
	
	Con_Locate(4,9)  : Con_BackGroundName("Black") : Con_PrintSpc(52)
	Con_Locate(4,12) : Con_BackGroundName("Red") : Con_PrintSpc(52)
	Con_Locate(4,15) : Con_BackGroundName("Green") : Con_PrintSpc(52)
	Con_Locate(4,18) : Con_BackGroundName("Blue") : Con_PrintSpc(52)
	
fn.end

!-------------------------------------------------
! Used slids the sliders to set the colour
!-------------------------------------------------
fn.def SelectColour()
	bundle.get 2, "Divice_W", di_width
	alpha = 255 : red = 255 : green = 0 : blue = 0
	oldAlpha = 55 : oldRed = 55 : oldGreen = 4 : oldBlue = 4
	colourPicker$="I"
	Con_RenderOn() : Con_ColourName("White")
	
	Con_BackGroundName("Black") : Con_PrintAt(55, 9, colourPicker$)  % Alpha
	Con_BackGroundName("Red") : Con_PrintAt(55, 12, colourPicker$) % Red
	Con_BackGroundName("Green") : Con_PrintAt(4, 15, colourPicker$) % Green
	Con_BackGroundName("Blue") : Con_PrintAt(4, 18, colourPicker$) % Blue
	
	Con_BackGroundName("Grey")
	Con_PrintAt(5,6,"Alpha = " + int$(alpha) + " Red = " + int$(red) + " Green = " + int$(green) + " Blue = " + int$(blue) + "   ")
	Con_RenderOn()
	gr.color alpha, red, green, blue, 1
	gr.rect null, 20, 50, di_width - 40, 150 : gr.render
	do
		flag = 0
		do : row = Con_GetTouchY() : until row
		if row = 9 | row = 12 | row = 15 | row = 18 then do : column = Con_GetTouchX() : until column
		if column > 3 & column < 56 then 
		
			! Set Alpha
			if row = 9 then 
				Con_BackGroundName("Black")
				Con_PrintAt(oldAlpha, row, " ") : Con_PrintAt(column, row, colourPicker$)
				oldAlpha = column
				alpha = (column - 4) * 5
				flag = 1
			endif
			
			! Set Red
			if row = 12 then 
				Con_BackGroundName("Red")
				Con_PrintAt(oldRed, row, " ") : Con_PrintAt(column, row, colourPicker$)
				oldRed = column
				red = (column - 4) * 5
				flag = 1
			endif
			
			! Set Green
			if row = 15 then 
				Con_BackGroundName("Green")
				Con_PrintAt(oldGreen, row, " ") : Con_PrintAt(column, row, colourPicker$)
				oldGreen = column
				green = (column - 4) * 5
				flag = 1
			endif
			
			! Set Blue
			if row = 18 then 
				Con_BackGroundName("Blue")
				Con_PrintAt(oldBlue, row, " ") : Con_PrintAt(column, row, colourPicker$)
				oldBlue = column
				blue = (column - 4) * 5
				flag = 1
			endif
			
		endif
		! Update colour panel
		if flag then 
			gr.color 255,0,0,0,1 : gr.rect null, 20, 50, di_width - 40, 150 
			gr.color alpha, red, green, blue, 1
			gr.rect null, 20, 50, di_width - 40, 150 : gr.render
			Con_BackGroundName("Grey")
			Con_PrintAt(5,6,"Alpha = " + int$(alpha) + " Red = " + int$(red) + " Green = " + int$(green) + " Blue = " + int$(blue) + "   ")
		endif
	until 0
fn.end

!-------------------------------------------------
! Print box with heading, background and foreground colours.
!-------------------------------------------------
fn.def Box(boxUperLeft, top, boxBottomRight, bottom, heading$, colour$)
	
	topLeft$ = chr$(9556): topRight$ = chr$(9559) : bottomLeft$ = chr$(9562) : bottomRight$ = chr$(9565)
	LeftRightBar$ = chr$(9553) : topBottomBar$ = chr$(9552) : headingLeftBar$ = chr$(9569) : headingRightBar$ = chr$(9566)
	!restoreColour = Con_GetColour() : restoreBGcolour = Con_GetBackground()
	Con_ColourName(colour$) 
	
	! Print each corner 
	Con_PrintAt(boxUperLeft, top, topLeft$) : Con_PrintAt(boxBottomRight, bottom, bottomRight$)
	Con_PrintAt(boxUperLeft, bottom, bottomLeft$) : Con_PrintAt(boxBottomRight, top, topRight$)
	
	! Print left and right bars
	for y = top + 1 to bottom - 1
		Con_PrintAt(boxUperLeft, y, LeftRightBar$)
		Con_PrintAt(boxBottomRight, y, LeftRightBar$)
	next
	! Print top and bottom bars
	for x = boxUperLeft + 1 to boxBottomRight - 1
		Con_PrintAt(x, top, topBottomBar$)
		Con_PrintAt(x, bottom, topBottomBar$)
	next
	
	! Print the heading, if heading$ is not ""
	if heading$ <> "" then 
		s$ = headingLeftBar$ + heading$ + headingRightBar$ 
		headingLenth = len(s$) / 2
		x = (boxBottomRight + boxUperLeft) / 2
		x = x - headingLenth
		Con_PrintAt(x, top, s$)
	endif
fn.end
 
return