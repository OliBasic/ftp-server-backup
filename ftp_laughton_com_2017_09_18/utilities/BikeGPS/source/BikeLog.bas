! BikeLog
! Aat Don @2015
FN.DEF DashedLine(x1,y1,x2,y2)
	s=20:XStep=(x2-x1)/s:YStep=(y2-y1)/s:x2=x1+XStep:y2=y1+YStep
	FOR i=1 TO s/2
		GR.LINE g,x1,y1,x2,y2:x1=x2+XStep:y1=y2+YStep:x2=x2+XStep*2:y2=y2+YStep*2
	NEXT i
FN.END
FN.DEF getStaticMap$(lat,lon,Zoom,sizex,sizey,fName$)
	URL$="http://maps.google.com/maps/api/staticmap?"
	URL$=URL$+"center="+FORMAT$("##%.######",lat)+","+FORMAT$("##%.######",lon)
	URL$=URL$+"&zoom="+FORMAT$("##",Zoom)+"&scale=2"
	URL$=URL$+"&size="+FORMAT$("####",sizex)+"x"+FORMAT$("####",sizey)
	URL$=URL$+"&maptype=roadmap"
	URL$=URL$+"&sensor=false"
	URL$=replace$(URL$," ","")
	BYTE.OPEN r,xx,URL$
	BYTE.COPY xx,fName$
	FN.RTN URL$
FN.END
FN.DEF Beep()
	TONE 659.25,200,0
	TONE 440,300,0
FN.END
GR.OPEN 255,0,0,0,0,1
GR.SCREEN w,h
ScaleX=600
ScaleY=h/w*ScaleX
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
! Radius of the globe in Europe (km)
ER=6378
Kap=0
CrLf$=CHR$(13,10)
LogBusy=0
MapPath$="../../BikeLog/data/"
DBPath$="../../BikeLog/databases/"
DBName$=DBPath$+"MapBase.db"
TableName$="Maps"
C1$="MapName"
C2$="FileName"
C3$="CenterLat"
C4$="CenterLon"
C5$="Zoom"
C6$="MapWidth"
C7$="DefMap"
FILE.EXISTS DBPresent,DBName$
SQL.OPEN DB_Ptr,DBName$
IF !DBPresent THEN
	SQL.NEW_TABLE DB_Ptr,TableName$,C1$,C2$,C3$,C4$,C5$,C6$,C7$
ENDIF
FILE.EXISTS IsDir,DBPath$+"import"
IF !IsDir THEN FILE.MKDIR DBPath$+"import"
FILE.EXISTS IsDir,DBPath$+"export"
IF !IsDir THEN FILE.MKDIR DBPath$+"export"
GR.BITMAP.CREATE BG,600,600
GR.BITMAP.DRAWINTO.START BG
	GR.SET.STROKE 0
	GR.COLOR 255,40,80,40,1
	FOR i=1 TO 300
		x=RND()*600
		y=RND()*600
		GR.CIRCLE g,x,y,(RND()+0.01)*5
	NEXT i
	GR.SET.STROKE 2
	GR.COLOR 255,30,30,30,1
	GR.CIRCLE g,300,300,210
	GR.COLOR 255,255,255,0,0
	FOR i=0 TO 210 STEP 70
		GR.SET.STROKE 0
		GR.COLOR 255,80,80,80,0
		GR.CIRCLE g,300,300,i-35
		GR.SET.STROKE 2
		GR.COLOR 255,255,255,0,0
		GR.CIRCLE g,300,300,i
	NEXT i
	GR.COLOR 255,80,80,80,1
	GR.RECT g,5,5,80,120
	GR.RECT g,5,430,80,590
	GR.RECT g,350,5,550,40
	GR.RECT g,500,525,590,590
	GR.COLOR 255,0,0,0,1
	GR.LINE g,5,60,80,60
	FOR i=0 TO 360 STEP 15
		GR.ROTATE.START i,300,300
			IF i/30<>INT(i/30) THEN
				GR.SET.STROKE 0
				GR.COLOR 255,80,80,80,0
				GR.LINE g,300,300,90,300
			ELSE
				GR.SET.STROKE 2
				GR.COLOR 255,255,255,0,0
				GR.LINE g,300,300,80,300
			ENDIF
		GR.ROTATE.END
	NEXT i
	GR.TEXT.ALIGN 2
	GR.TEXT.SIZE 20
	GR.COLOR 255,0,255,255,0
	T$="azimuth---->"
	FOR i=1 TO LEN(T$)
		x=260*COS(((i-1)*3+180)*pi()/180)+300
		y=260*SIN(((i-1)*3+180)*pi()/180)+300
		GR.ROTATE.START -90+(i-1)*3,x,y
			GR.TEXT.DRAW g,x,y,MID$(T$,i,1)
		GR.ROTATE.END
	NEXT i
	GR.TEXT.DRAW g,148,295,"elevation--->"
	GR.COLOR 255,255,255,0,0
	GR.TEXT.SIZE 15
	GR.TEXT.ALIGN 2
	FOR i=0 TO 330 STEP 30
		x=240*COS((i+270)*pi()/180)+300
		y=240*SIN((i+270)*pi()/180)+300
		IF i=0 | i=90 | i=180 | i=270 THEN
			IF i=0 THEN D$="North"
			IF i=90 THEN D$="West"
			IF i=180 THEN D$="South"
			IF i=270 THEN D$="East"
		ELSE
			D$=INT$(i)
		ENDIF
		GR.TEXT.DRAW g,x,y,D$
	NEXT i
	GR.TEXT.DRAW g,20,450,"100"
	GR.TEXT.DRAW g,20,556,"0"
	GR.RECT g,45,450,65,551
	FOR i=0 TO 100
		GR.COLOR 255,INT((255-i*8.5)*(i<31)),INT((i*6.2)*(i<31)+(i>30)*(i+155)),0,1
		GR.RECT g,45,550-i,65,551-i
	NEXT i
	GR.COLOR 255,0,255,0,1
	GR.TEXT.DRAW g,40,20,"sat"
	GR.TEXT.DRAW g,40,80,"fixed"
	GR.TEXT.SIZE 25
	GR.COLOR 255,255,255,255,1
	GR.TEXT.DRAW g,50,580,"S/N"
	GR.TEXT.DRAW g,400,30,"local:"
	GR.COLOR 255,255,255,0,1
	GR.TEXT.DRAW g,550,550,"Busy"
	GR.TEXT.ALIGN 1
GR.BITMAP.DRAWINTO.END
GR.BITMAP.CREATE DrawSat,300,300
GR.BITMAP.DRAWINTO.START DrawSat
	GR.COLOR 255,100,255,100,0
	GR.ROTATE.START 45,50,20
		GR.SET.STROKE 20
		GR.RECT g,50,20,150,70
		GR.RECT g,250,20,350,70
		GR.SET.STROKE 10
		GR.LINE g,50,45,350,45
		GR.LINE g,83,20,83,70
		GR.LINE g,117,20,117,70
		GR.LINE g,283,20,283,70
		GR.LINE g,317,20,317,70
		GR.COLOR 255,100,255,100,1
		GR.CIRCLE g,200,90,20
		GR.RECT g,170,20,230,90
		GR.LINE g,200,0,200,20
		GR.ARC g,130,-80,270,0,0,180,1
		GR.CIRCLE g,200,-55,10
	GR.ROTATE.END
	GR.TEXT.SIZE 40
	GR.COLOR 255,255,255,0,1
	GR.TEXT.DRAW g,20,250,"BikeLog"
GR.BITMAP.DRAWINTO.END
ARRAY.LOAD MainMenu$[],"Start Logging","Select Map for Logging","Manage Route(s) on Map","Manage Maps","Exit"
ARRAY.LOAD MapMenu$[],"View Maps/Routes present","Delete Map","Download new Map","Import Map","Export Map","Start Screen"
ARRAY.LOAD LogMenu$[],"Show Large Data","Insert POI","PAUSE Logging","STOP Logging","Cancel"
ARRAY.LOAD RouteOnMap$[],"Select Route(s)","UnSelect Route(s)","Import a Route","Start Screen"
ARRAY.LOAD SelectMap$[],"Use this map","Cancel"
ARRAY.LOAD Bear$[],"N","NNE","NE","ENE","E","ESE","SE","SSE","S","SSW","SW","WSW","W","WNW","NW","NNW","N"
! Menu
DO
	GOSUB GetNumOfMaps
	GOSUB GetDefMap
	GR.CLS
	GR.TEXT.SIZE 25
	GR.COLOR 255,255,255,0,1
	GR.TEXT.ALIGN 1
	GR.TEXT.DRAW g,20,30,INT$(NumMaps)+" maps present"
	IF DefSet$="" THEN
		GR.COLOR 255,255,255,255,1
		M$=MapDescribe$
	ELSE
		GR.COLOR 255,255,255,0,1
		M$="Plot route on "+MapDescribe$
		GOSUB GetMapData
		NR=0
		FOR i=0 TO 99
			FILE.EXISTS g,MapPath$+LEFT$(MapName$,LEN(MapName$)-4)+"_Route"+REPLACE$(FORMAT$("%%",i)," ","")+".SOM"
			IF g THEN NR=NR+1
		NEXT i
		IF NR>0 THEN
			M2$="Including "+INT$(NR)+" shown route(s) on Map"
		ELSE
			M2$="NO routes shown on Map"
		ENDIF
	ENDIF
	GR.TEXT.DRAW g,20,60,M$
	GR.TEXT.DRAW g,20,90,M2$
	GR.BITMAP.DRAW g,DrawSat,150,95
	GR.TEXT.DRAW g,120,400,"Tap screen for MENU"
	GR.RENDER
	GOSUB GetTouch
	DIALOG.SELECT Keuze,MainMenu$[],"BikeLog - Main Menu"
	IF Keuze>0 THEN
		Beep()
		GR.CLS
		GR.RENDER
		IF Keuze=1 THEN GOSUB LogStart
		IF Keuze=2 THEN GOSUB SetDefMap
		IF Keuze=3 THEN GOSUB SetRoute
		IF Keuze=4 THEN GOSUB Maps
		IF Keuze=5 THEN Kap=1
	ENDIF
UNTIL Kap=1
GR.CLOSE
SQL.CLOSE DB_Ptr
EXIT
Maps:
DO
	GR.CLS
	GR.RENDER
	DIALOG.SELECT Keuze,MapMenu$[],"BikeLog - Map Menu"
	IF Keuze>0 THEN
		Beep()
		IF Keuze=1 THEN GOSUB ViewMaps
		IF Keuze=2 THEN GOSUB DeleteMap
		IF Keuze=3 THEN GOSUB GetMap
		IF Keuze=4 THEN GOSUB ImportMap
		IF Keuze=5 THEN GOSUB ExportMap
		IF Keuze=6 THEN Kap=2
	ENDIF
UNTIL Kap=2
RETURN
ViewMaps:
	GOSUB GetAllMaps
	IF !n THEN RETURN
	DIALOG.SELECT Answer,Places$[],"BikeLog - View Maps/Routes"
	IF Answer THEN
		Index$=WORD$(Places$[Answer],2,CHR$(30))
		GOSUB GetMapData
		GR.TEXT.ALIGN 1
		GR.TEXT.BOLD 1
		GR.TEXT.SIZE 35
		GR.COLOR 255,255,255,0,1
		GR.TEXT.DRAW g,20,30,"Map "+MapDescribe$
		GR.TEXT.DRAW g,20,65,"File Name "+MapName$
		GR.TEXT.DRAW g,20,100,"Zoom "+Zoom$
		GR.TEXT.DRAW g,20,135,"Map Width "+FORMAT$("##%.##",VAL(MapWidth$))+" km"
		GR.TEXT.DRAW g,20,170,"Default map "+MapIsDef$
		GR.TEXT.DRAW g,20,275,"Tap screen to view map"
		GR.RENDER
		FILE.DIR MapPath$,AllFiles$[]
		ARRAY.LENGTH NumFiles,AllFiles$[]
		NumRoutes=0
		LIST.CREATE S,RP
		FOR gf=1 TO NumFiles
			IF RIGHT$(AllFiles$[gf],3)="gpx" & LEFT$(AllFiles$[gf],LEN(AllFiles$[gf])-6)=LEFT$(MapName$,LEN(MapName$)-4)+"_Route" THEN
				NumRoutes=NumRoutes+1
				LIST.ADD RP,LEFT$(AllFiles$[gf],LEN(AllFiles$[gf])-4)
			ENDIF
		NEXT gf
		IF NumRoutes>0 THEN
			M$=""
			IF NumRoutes>1 THEN M$="s"
			GR.TEXT.DRAW g,20,205,INT$(NumRoutes)+" Route"+M$+" present for this map"
			GR.RENDER
		ENDIF
		Kap=0
		GOSUB GetTouch
		GR.CLS
		IF Kap=0 THEN
			IF MapName$="WithoutMap00.png" THEN
				GR.BITMAP.CREATE MapView,600,600
				GR.BITMAP.DRAWINTO.START MapView
					GR.COLOR 255,50,50,100,1
					GR.RECT g,0,0,600,600
					GR.COLOR 255,255,255,100,0
					GR.RECT g,0,0,600,600
				GR.BITMAP.DRAWINTO.END
			ELSE
				GR.BITMAP.LOAD MapView,DBPath$+MapName$
			ENDIF
			GR.BITMAP.DRAW ViewMap,MapView,0,0
			GOSUB DrawScaleBar
			GR.COLOR 255,255,255,0,1
			GR.TEXT.DRAW Tapg,20,655,""
			GR.RENDER
			LineCol=0
			IF NumRoutes>0 THEN
				GR.MODIFY Tapg,"text","Tap screen for Route Menu"
				GR.RENDER
				scaleLon=256*2^zlev/360
				scaleLat=scaleLon/cos(toradians(latCent))
				LIST.ADD RP,"Cancel"
				DO
					LIST.SIZE RP,ml
					IF ml=1 THEN D_U.BREAK
					GOSUB GetTouch
					IF Kap=1 THEN D_U.BREAK
					GR.MODIFY Tapg,"text",""
					GR.RENDER
					DIALOG.SELECT Answer,RP,"Select Route to View"
					IF Answer=NumRoutes+1 | Answer=0 THEN D_U.BREAK
					Delay=Val(USING$("","%tQ",time()))+10000
					GR.MODIFY Tapg,"text","Please Wait, retrieving route..."
					GR.RENDER
					LIST.GET RP,Answer,F$
					GRABFILE Gpx$,MapPath$+F$+".gpx"
					LIST.REMOVE RP,Answer
					NumRoutes=NumRoutes-1
					! Add Route Points
					SPLIT GPXData$[],Gpx$,"<trkpt"
					ARRAY.LENGTH L,GPXData$[]
					! Show info (while waiting)
					GR.GETDL Temp[],1
					GR.COLOR 200,255,255,255,1
					GR.RECT g,30,30,590,430
					GR.COLOR 255,0,0,0,1
					Info$=WORD$(WORD$(GPXData$[L],2,"<desc>"),1,"</desc>")
					FOR i=2 TO 10
						GR.TEXT.DRAW g,40,50+(i-1)*40,WORD$(Info$,i,CHR$(13)+CHR$(10)+CHR$(9)+CHR$(9))
					NEXT i
					GR.RENDER
					GR.NEWDL Temp[]
					ARRAY.DELETE Temp[]
					LineCol$=BIN$(LineCol)
					WHILE LEN(LineCol$)<3
						LineCol$="0"+LineCol$
					REPEAT
					LineCol=LineCol+1
					IF IS_IN("ele>",GPXData$[2]) THEN
						DIM Altitude[L-1]
						ContainsAlt=1
					ELSE
						ContainsAlt=0
					ENDIF
					DIM Driven[L-1]
					DistDriven=0
					GR.SET.STROKE 3
					GR.COLOR 255,VAL(LEFT$(LineCol$,1))*255,VAL(MID$(LineCol$,2,1))*255,VAL(RIGHT$(LineCol$,1))*255,1
					PrevXPnt=299+(VAL(WORD$(GPXData$[2],4,CHR$(34)))-lonCent)*scaleLon
					PrevYPnt=299-(VAL(WORD$(GPXData$[2],2,CHR$(34)))-latCent)*scaleLat
					PrevLat=TORADIANS(VAL(WORD$(GPXData$[2],2,CHR$(34))))
					PrevLon=TORADIANS(VAL(WORD$(GPXData$[2],4,CHR$(34))))
					FOR j=2 TO L
						XPnt=299+(VAL(WORD$(GPXData$[j],4,CHR$(34)))-lonCent)*scaleLon
						YPnt=299-(VAL(WORD$(GPXData$[j],2,CHR$(34)))-latCent)*scaleLat
						GR.LINE g,PrevXPnt,PrevYPnt,XPnt,YPnt
						PrevXPnt=XPnt
						PrevYPnt=YPnt
						IF ContainsAlt THEN Altitude[j-1]=VAL(WORD$(WORD$(GPXData$[j],2,"ele>"),1,"<"))
						CurLat=TORADIANS(VAL(WORD$(GPXData$[j],2,CHR$(34))))
						CurLon=TORADIANS(VAL(WORD$(GPXData$[j],4,CHR$(34))))
						dLon=CurLon-PrevLon
						dLat=CurLat-PrevLat
						a=((SIN(dLat/2))^2+COS(PrevLat)*COS(CurLat)*(SIN(dLon/2))^2)
						c=(2*ATAN2(SQR(a),SQR(1-a)))
						DistDriven=DistDriven+ER*c
						Driven[j-1]=DistDriven
						PrevLat=CurLat
						PrevLon=CurLon
					NEXT j
					ARRAY.DELETE GPXData$[]
					! Add Poi's
					SPLIT GPXPOIData$[],Gpx$,"<wpt"
					ARRAY.LENGTH L,GPXPOIData$[]
					FOR i=2 TO L
						XPnt=299+(VAL(WORD$(GPXPOIData$[i],4,CHR$(34)))-lonCent)*scaleLon
						YPnt=299-(VAL(WORD$(GPXPOIData$[i],2,CHR$(34)))-latCent)*scaleLat
						IF i=2 | i=L THEN
							IF i=2 THEN
								GR.COLOR 255,255,255,0,1
								GR.ARC g,XPnt-10,YPnt-10,XPnt+10,YPnt+10,90,180,1
								GR.COLOR 255,255,0,0,0
								GR.ARC g,XPnt-10,YPnt-10,XPnt+10,YPnt+10,90,180,1
							ELSE
								GR.COLOR 255,0,255,255,1
								GR.ARC g,XPnt-10,YPnt-10,XPnt+10,YPnt+10,270,180,1
								GR.COLOR 255,255,0,0,0
								GR.ARC g,XPnt-10,YPnt-10,XPnt+10,YPnt+10,270,180,1
							ENDIF
						ELSE
							GR.COLOR 255,255,0,0,0
							GR.CIRCLE g,XPnt,Ypnt,7
						ENDIF
					NEXT i
					DO
						! Pause till approx. 10s
						GDelay=Val(USING$("","%tQ",time()))
					UNTIL GDelay>Delay
					ARRAY.DELETE GPXData$[]
					ARRAY.DELETE GPXPOIData$[]
					Gpx$=""
					GR.MODIFY Tapg,"text",""
					GR.RENDER
					IF ContainsAlt THEN
						DIALOG.MESSAGE "Plot Profile","View height profile of this route",q,"Yes","No"
						IF q=1 THEN
							! Plot height profile
							GR.GETDL NewDispl[],1
							ARRAY.MIN MinHeight,Altitude[]
							ARRAY.MAX MaxHeight,Altitude[]
							ARRAY.LENGTH L,Altitude[]
							MaxDist=Driven[L]
							GR.COLOR 200,255,255,255,1
							GR.RECT g,0,160,600,510
							GR.COLOR 255,0,0,0,1
							GR.TEXT.DRAW g,200,190,"Height Profile"
							GR.LINE g,100,200,100,470
							FOR i=200 TO 450 STEP 62.5
								GR.LINE g,80,i,100,i
							NEXT i
							GR.LINE g,80,450,550,450
							FOR i=100 TO 550 STEP 45
								GR.LINE g,i,450,i,470
							NEXT i
							GR.TEXT.DRAW g,340,500,"[km]"
							GR.ROTATE.START 270,50,350
								GR.TEXT.DRAW g,50,350,"[m]"
							GR.ROTATE.END
							GR.TEXT.DRAW g,15,210,INT$(MaxHeight)
							GR.TEXT.DRAW g,15,460,INT$(MinHeight)
							GR.TEXT.DRAW g,90,500,"0"
							GR.TEXT.DRAW g,530,500,INT$(MaxDist)
							FOR i=2 TO L
								! Moving average over 50 pnts
								IF i>50 THEN
									ARRAY.AVERAGE M1,Altitude[i-50,50]
									ARRAY.AVERAGE M2,Altitude[i-49,50]
									IF Driven[i]=Driven[i-1] THEN Driven[i]=Driven[i]+0.01
									Stijging=(M2-M1)/((Driven[i]-Driven[i-1])*10)
									! Scale up value
									Stijging=Stijging*25
									SR=INT(Stijging/16)*17
									R=ABS(SR*(SR>0))
									IF R>255 THEN R=255
									SB=(INT(Stijging/16)+1)*17
									B=ABS(-SB*(SB<0))
									IF B>255 THEN B=255
								ELSE
									R=0
									B=0
								ENDIF
								! Colour depends on (averaged) climbing up/down
								GR.COLOR 255,R,0,B,1
									GR.LINE g,Driven[i-1]*450/MaxDist+100,450-(Altitude[i-1]-MinHeight)*250/(MaxHeight-MinHeight),Driven[i]*450/MaxDist+100,450-(Altitude[i]-MinHeight)*250/(MaxHeight-MinHeight)
							NEXT i
							GR.MODIFY Tapg,"text","Tap screen to continue"
							GR.RENDER
							GOSUB GetTouch
							GR.NEWDL NewDispl[]
							GR.RENDER
							ARRAY.DELETE NewDispl[]
						ENDIF
						ARRAY.DELETE Altitude[]
					ENDIF
					ARRAY.DELETE Driven[]
					GR.MODIFY Tapg,"text","Tap screen for Route Menu"
					GR.RENDER
				UNTIL NumRoutes=0
			ENDIF
			GR.MODIFY Tapg,"text","Tap screen to Return to Map Menu"
			GR.RENDER
			GOSUB GetTouch
			GR.CLS
		ENDIF
		GR.RENDER
		LIST.CLEAR RP
	ENDIF
	ARRAY.DELETE Places$[]
	ARRAY.DELETE AllFiles$[]
RETURN
DeleteMap:
	GOSUB GetAllMaps
	IF !n THEN RETURN
	DIALOG.SELECT Answer,Places$[],"BikeLog - Select Map to Delete"
	IF Answer>0 THEN
		Index$=WORD$(Places$[Answer],2,CHR$(30))
		GOSUB GetMapData
		PS$=WORD$(Places$[Answer],1,CHR$(30))
		PS$=LEFT$(PS$,LEN(PS$)-1)
		DIALOG.MESSAGE "Are you sure ?","Delete "+PS$+" ?",q,"Yes","No"
		IF q=1 THEN
			Where$="_id="+"'"+Index$+"'"
			SQL.DELETE DB_Ptr,TableName$,Where$
			FILE.DELETE d,DBPath$+MapName$
			POPUP "Map "+PS$+" deleted",0,0,0
		ENDIF
	ENDIF
	ARRAY.DELETE Places$[]
RETURN
GetMap:
	SOCKET.MyIP Connected$
	IF Connected$="" THEN
		POPUP "NO INTERNET CONNECTION, turn it on and try again !",0,0,1
		RETURN
	ENDIF
	INPUT "Enter a place name, the center of the map you want to create....",PlaceName$,"",IsCanceled
	IF IsCanceled THEN RETURN
	IF PlaceName$="" THEN PlaceName$="London,UK"
	IF IS_IN(",",PlaceName$) THEN
		Destination$=WORD$(PlaceName$,1,",")
	ELSE
		Destination$=PlaceName$
	ENDIF
	Destination$=REPLACE$(UPPER$(LEFT$(Destination$,1))+RIGHT$(Destination$,LEN(Destination$)-1)," ","")
	! Get Lat/Lon of destination
	BYTE.OPEN r,xx,"http://maps.googleapis.com/maps/api/geocode/json?address="+REPLACE$(PlaceName$," ","+")+"&sensor=false"
	BYTE.COPY xx,DBPath$+"Loc.txt"
	BYTE.CLOSE xx
	TEXT.OPEN r,rl,DBPath$+"Loc.txt"
		DO
			TEXT.READLN rl,RLine$
			RLine$=REPLACE$(RLine$," ","")
			IF IS_IN(CHR$(34)+"location"+chr$(34)+":{",RLine$) THEN
				TEXT.READLN rl,RLine$
				LatCent$=WORD$(RLine$,2,":")
				LatCent$=LEFT$(LatCent$,LEN(LatCent$)-1)
				LatCent=VAL(LatCent$)
				TEXT.READLN rl,RLine$
				LonCent$=WORD$(RLine$,2,":")
				LonCent=VAL(LonCent$)
			ENDIF
		UNTIL RLine$="EOF"
	TEXT.CLOSE rl
	FILE.DELETE xx,DBPath$+"Loc.txt"
	GR.TEXT.ALIGN 2
	GR.TEXT.BOLD 1
	GR.TEXT.SIZE 15
	MapSelect$=DBPath$+"tempMap.png"
	DSize=300
	zlev=7
	FOR i=1 TO 3
		FOR j=1 TO 3
			zlev=zlev+1
			tmp$=getStaticMap$(latCent,lonCent,zlev,DSize,DSize,MapSelect$)
			GR.BITMAP.LOAD NwMap,MapSelect$
			GR.BITMAP.SCALE SmallMap,NwMap,200,200
			GR.BITMAP.DRAW ViewMap,SmallMap,0+(j-1)*200,(i-1)*200
			MapKm=(COS(TORADIANS(latCent))*2*PI()*ER)/(256*2^(zlev))*300
			GR.COLOR 200,255,0,0,1
			GR.TEXT.DRAW g,(j-1)*200+100,(i-1)*200+15,"Zoom "+INT$(zlev)
			GR.TEXT.DRAW g,(j-1)*200+100,(i-1)*200+190,"Map Width "+FORMAT$("##%.##",MapKm)+" km"
			GR.COLOR 255,0,0,0,1
		NEXT j
	NEXT i
	GR.LINE g,0,199,599,199
	GR.LINE g,0,399,599,399
	GR.LINE g,199,0,199,599
	GR.LINE g,399,0,399,599
	GR.RENDER
	DO
		GOSUB GetTouch
	UNTIL y<600 | Kap=1
		IF Kap=1 THEN
			Kap=0
			RETURN
		ENDIF
	x=INT(x/200)
	y=INT(y/200)
	zlev=x+y*3+8
	tmp$=getStaticMap$(latCent,lonCent,zlev,DSize,DSize,MapSelect$)
	GR.BITMAP.LOAD NwMap,MapSelect$
	GR.BITMAP.DRAW ViewMap,NwMap,0,0
	MapKm=(COS(TORADIANS(latCent))*2*PI()*ER)/(256*2^(zlev))*300
	GR.COLOR 200,255,0,0,1
	GR.TEXT.DRAW g,(j-1)*200+100,(i-1)*200+15,"Zoom "+INT$(zlev)
	GR.TEXT.DRAW g,(j-1)*200+100,(i-1)*200+190,"Map Width "+FORMAT$("##%.##",MapKm)+" km"
	GR.COLOR 255,0,0,0,1
	GR.RENDER
	Kap=0
	DO
	DIALOG.SELECT Answer,SelectMap$[],"BikeLog - Map Select"
		IF Answer=1 THEN
			INPUT "Enter a descriptive name for this map",MapDescribe$,Destination$+" Zoom "+INT$(zlev),isCanceled
			IF !isCanceled THEN
				MapDescribe$=UPPER$(LEFT$(MapDescribe$,1))+RIGHT$(MapDescribe$,LEN(MapDescribe$)-1)
				Destination$=Destination$+"00"
				! Check for duplicates
				NoDup=0
				DO
					Where$="FileName='"+Destination$+".png"+"'"
					Columns$="_id,"+C2$
					SQL.QUERY Cursor,DB_Ptr,TableName$,Columns$,Where$
					SQL.NEXT xdone,Cursor,Index$,MapName$
					IF MapName$=Destination$+".png" THEN
						PostFix=Val(RIGHT$(Destination$,2))+1
						Destination$=Left$(Destination$,LEN(Destination$)-2)+REPLACE$(FORMAT$("%%",PostFix)," ","")
					ELSE
						NoDup=1
					ENDIF
				UNTIL NoDup
				Destination$=Destination$+".png"
				FILE.RENAME MapSelect$,DBPath$+Destination$
				DefMap$="N"
				IF NumMaps=0 THEN DefMap$="Y"
				SQL.INSERT DB_Ptr,TableName$,C1$,MapDescribe$,C2$,Destination$,C3$,latCent$,C4$,lonCent$,C5$,INT$(zlev+1),C6$,STR$(MapKm),C7$,DefMap$
				Kap=2
			ENDIF
		ENDIF
		IF Answer=2 THEN
			GR.CLS
			Kap=3
		ENDIF
	UNTIL Kap	
RETURN
ImportMap:
	FILE.DIR DBPath$+"import/",AllFiles$[]
	ARRAY.LENGTH NumFiles,AllFiles$[]
	NumMapFound=0
	LIST.CREATE S,RP
	LIST.CREATE S,DP
	FOR gf=1 TO NumFiles
		IF RIGHT$(AllFiles$[gf],3)="map" THEN
			NumMapFound=NumMapFound+1
			LIST.ADD RP,LEFT$(AllFiles$[gf],LEN(AllFiles$[gf])-4)
			TEXT.OPEN r,import,DBPath$+"import/"+LEFT$(AllFiles$[gf],LEN(AllFiles$[gf])-4)+".map"
				TEXT.READLN import,ED$
			TEXT.CLOSE import
			LIST.ADD DP,ED$
		ENDIF
	NEXT gf
	IF NumMapFound>0 THEN
		LIST.TOARRAY RP,RPlaces$[]
		LIST.TOARRAY DP,RDescr$[]
		DIALOG.SELECT Answer,RDescr$[],"BikeLog - Import Map"
		IF Answer THEN
			TEXT.OPEN r,import,DBPath$+"import/"+RPlaces$[Answer]+".map"
				TEXT.READLN import,MapDescribe$
				TEXT.READLN import,Dest$
				TEXT.READLN import,latCent$
				TEXT.READLN import,lonCent$
				TEXT.READLN import,Zoom$
				TEXT.READLN import,MapWidth$
			TEXT.CLOSE import
			BYTE.OPEN r,FC,DBPath$+"import/"+Dest$
			BYTE.COPY FC,DBPath$+Dest$
			BYTE.CLOSE FC
			DefMap$="N"
			SQL.INSERT DB_Ptr,TableName$,C1$,MapDescribe$,C2$,Dest$,C3$,latCent$,C4$,lonCent$,C5$,Zoom$,C6$,MapWidth$,C7$,DefMap$
			POPUP "Map "+MapDescribe$+" imported !",0,0,0
			FILE.DELETE x,DBPath$+"import/"+RPlaces$[Answer]+".map"
			FILE.DELETE x,DBPath$+"import/"+Dest$
			ARRAY.DELETE RPlaces$[]
			ARRAY.DELETE RDescr$[]
		ENDIF
	ELSE
		POPUP "NO maps found to Import !",0,0,0
	ENDIF
	LIST.CLEAR RP
	LIST.CLEAR DP
	ARRAY.DELETE AllFiles$[]
RETURN
ExportMap:
	GOSUB GetAllMaps
	IF !n THEN RETURN
	DIALOG.SELECT Answer,Places$[],"BikeLog - Export Map"
	IF Answer THEN
		Index$=WORD$(Places$[Answer],2,CHR$(30))
		GOSUB GetMapData
		TEXT.OPEN w,export,DBPath$+"export/"+LEFT$(MapName$,LEN(MapName$)-3)+"map"
			TEXT.WRITELN export,MapDescribe$
			TEXT.WRITELN export,MapName$
			TEXT.WRITELN export,latCent$
			TEXT.WRITELN export,lonCent$
			TEXT.WRITELN export,Zoom$
			TEXT.WRITELN export,MapWidth$
		TEXT.CLOSE export
		BYTE.OPEN r,FC,DBPath$+MapName$
		BYTE.COPY FC,DBPath$+"export/"+MapName$
		BYTE.CLOSE FC
		POPUP "Map "+MapDescribe$+" exported !",0,0,0
	ENDIF
	ARRAY.DELETE Places$[]
RETURN
SetRoute:
	DO
		DIALOG.SELECT Answer,RouteOnMap$[],"BikeLog - Route(s) on Map "
		IF Answer=1 THEN
			FILE.DIR MapPath$,AllFiles$[]
			ARRAY.LENGTH NumFiles,AllFiles$[]
			NumRoutes=0
			LIST.CREATE S,RP
			FOR gf=1 TO NumFiles
				IF (RIGHT$(AllFiles$[gf],3)="gpx" | RIGHT$(AllFiles$[gf],3)="SOM") & IS_IN("_Route",AllFiles$[gf]) THEN
					FCh$=LEFT$(AllFiles$[gf],LEN(AllFiles$[gf])-4)
					IF RIGHT$(AllFiles$[gf],3)="SOM" THEN
						FILE.EXISTS NoDel,MapPath$+FCh$+".gpx"
						IF NoDel=0 THEN FILE.DELETE MapPath$+FCh$+".SOM"
					ELSE
						NumRoutes=NumRoutes+1
						FILE.EXISTS NoDel,MapPath$+FCh$+".SOM"
						IF NoDel>0 THEN FCh$=FCh$+" [SET]"
						LIST.ADD RP,FCh$
					ENDIF
				ENDIF
			NEXT gf
			IF NumRoutes=0 THEN
				POPUP "NO Routes present !"
			ELSE
				LIST.ADD RP,"Done"
				LIST.TOARRAY RP,SelFiles$[]
				LIST.SIZE RP,L
				DO
					DIALOG.SELECT SelFile,SelFiles$[],"Pick Route to show on its Map"
					IF SelFile<L & SelFile>0 THEN
						IF RIGHT$(SelFiles$[SelFile],5)<>"[SET]" THEN
							TEXT.OPEN W,SetRoute,MapPath$+SelFiles$[SelFile]+".SOM"
								TEXT.WRITELN SetRoute,"Show "+SelFiles$[SelFile]+".gpx"+" on map"
							TEXT.CLOSE SetRoute
							SelFiles$[SelFile]=SelFiles$[SelFile]+" [SET]"
							NumRoutes=NumRoutes-1
						ENDIF
					ENDIF
				UNTIL NumRoutes=0 | SelFile=L
				ARRAY.DELETE SelFiles$[]
			ENDIF
			LIST.CLEAR RP
			ARRAY.DELETE AllFiles$[]
		ENDIF
		IF Answer=2 THEN
			FILE.DIR MapPath$,AllFiles$[]
			ARRAY.LENGTH NumFiles,AllFiles$[]
			NumRoutes=0
			LIST.CREATE S,RP
			FOR gf=1 TO NumFiles
				IF RIGHT$(AllFiles$[gf],3)="SOM" & IS_IN("_Route",AllFiles$[gf]) THEN
					FCh$=LEFT$(AllFiles$[gf],LEN(AllFiles$[gf])-4)
					LIST.ADD RP,FCh$
					NumRoutes=NumRoutes+1
				ENDIF
			NEXT gf
			IF NumRoutes=0 THEN
				POPUP "NO Routes present !"
			ELSE
				LIST.ADD RP,"Done"
				LIST.TOARRAY RP,SelFiles$[]
				LIST.SIZE RP,L
				DO
					DIALOG.SELECT SelFile,SelFiles$[],"Pick Route NOT to show on its Map"
					IF SelFile<L & SelFile>0 THEN
						IF RIGHT$(SelFiles$[SelFile],9)<>"[REMOVED]" THEN
							FILE.DELETE g,MapPath$+SelFiles$[SelFile]+".SOM"
							SelFiles$[SelFile]=SelFiles$[SelFile]+" [REMOVED]"
							NumRoutes=NumRoutes-1
						ENDIF
					ENDIF
				UNTIL NumRoutes=0 | SelFile=L
				ARRAY.DELETE SelFiles$[]
			ENDIF
			LIST.CLEAR RP
			ARRAY.DELETE AllFiles$[]
		ENDIF
		IF Answer=3 THEN
			FILE.DIR MapPath$,AllFiles$[]
			ARRAY.LENGTH NumFiles,AllFiles$[]
			NumRoutes=0
			LIST.CREATE S,RP
			FOR gf=1 TO NumFiles
				IF RIGHT$(AllFiles$[gf],3)="gpx" & MID$(AllFiles$[gf],LEN(AllFiles$[gf])-11,6)<>"_Route" THEN
					FCh$=LEFT$(AllFiles$[gf],LEN(AllFiles$[gf])-4)
					LIST.ADD RP,FCh$
					NumRoutes=NumRoutes+1
				ENDIF
			NEXT gf
			IF NumRoutes=0 THEN
				POPUP "NO Routes present !"
			ELSE
				LIST.ADD RP,"Done"
				LIST.TOARRAY RP,SelFiles$[]
				LIST.SIZE RP,L
				DO
					DIALOG.SELECT SelFile,SelFiles$[],"Pick 'foreign' Route to Import"
					IF SelFile<L & SelFile>0 & RIGHT$(SelFiles$[SelFile],10)<>"[IMPORTED]" THEN
						ImPort$=SelFiles$[SelFile]
						GOSUB GetAllMaps
						IF !n THEN ENDIF
						ARRAY.COPY Places$[],PlusPlaces$[1]
						ARRAY.LENGTH Nep,PlusPlaces$[]
						PlusPlaces$[Nep]="Download New Map on which this Route FITS"
						ARRAY.DELETE Places$[]
						DIALOG.SELECT SelMap,PlusPlaces$[],"Select Map to show Route "+ImPort$+" on"
						! Show progress
						GR.TEXT.SIZE 25
						GR.COLOR 255,255,255,0,1
						GR.TEXT.ALIGN 1
						GR.TEXT.DRAW Prg,20,30,"Calculating map parameters..."
						GR.RENDER
						GRABFILE Gpx$,MapPath$+Import$+".gpx"
						SPLIT GPXData$[],Gpx$,"<trkpt"
						ARRAY.LENGTH L,GPXData$[]
						MinLat=VAL(WORD$(GPXData$[2],2,CHR$(34))):MaxLat=VAL(WORD$(GPXData$[2],2,CHR$(34)))
						MinLon=VAL(WORD$(GPXData$[2],4,CHR$(34))):MaxLon=VAL(WORD$(GPXData$[2],4,CHR$(34)))
						PrevLat=TORADIANS(MinLat):PrevLon=TORADIANS(MinLon):Driven=0
						FOR i=3 TO L
							IF VAL(WORD$(GPXData$[i],2,CHR$(34)))<MinLat THEN MinLat=VAL(WORD$(GPXData$[i],2,CHR$(34)))
							IF VAL(WORD$(GPXData$[i],2,CHR$(34)))>MaxLat THEN MaxLat=VAL(WORD$(GPXData$[i],2,CHR$(34)))
							IF VAL(WORD$(GPXData$[i],4,CHR$(34)))<MinLon THEN MinLon=VAL(WORD$(GPXData$[i],4,CHR$(34)))
							IF VAL(WORD$(GPXData$[i],4,CHR$(34)))>MaxLon THEN MaxLon=VAL(WORD$(GPXData$[i],4,CHR$(34)))
							! Find km driven
							CurLat=TORADIANS(VAL(WORD$(GPXData$[i],2,CHR$(34))))
							CurLon=TORADIANS(VAL(WORD$(GPXData$[i],4,CHR$(34))))
							dLon=CurLon-PrevLon:dLat=CurLat-PrevLat
							a=((SIN(dLat/2))^2+COS(PrevLat)*COS(CurLat)*(SIN(dLon/2))^2)
							c=(2*ATAN2(SQR(a),SQR(1-a)))
							! Driven in km
							Driven=Driven+ER*c:PrevLat=CurLat:PrevLon=CurLon
						NEXT i
						IF SelMap=Nep THEN
							! Download new map
							latCent=(MaxLat+MinLat)/2:lonCent=(MaxLon+MinLon)/2
							A1=(MaxLon-MinLon)*1.5:A2=(MaxLat-MinLat)*1.5
							IF A2>A1 THEN A1=A2
							IF A1<0 THEN A1=A1+360
							DSize=300
							zlev=FLOOR(LOG(DSize*360/A1/256/0.69314718056))+3
							IF zlev>18 THEN zlev=18
							MapKm=(COS(TORADIANS(latCent))*2*PI()*ER)/(256*2^(zlev))*300
							! Get new Map
							GR.MODIFY Prg,"text","Download in progress..."
							GR.RENDER
							tmp$=getStaticMap$(latCent,lonCent,zlev,DSize,DSize,DBPath$+Import$+".png")
							! Save to DataBase
							GR.MODIFY Prg,"text","Saving Map..."
							GR.RENDER
							MapDescribe$="Imported map of "+Import$
							MapName$=Import$+"00"
							! Check for duplicates
							NoDup=0
							DO
								Where$="FileName='"+MapName$+".png"+"'"
								Columns$="_id,"+C2$
								SQL.QUERY Cursor,DB_Ptr,TableName$,Columns$,Where$
								SQL.NEXT xdone,Cursor,Index$,MapName$
								IF MapName$=MapName$+".png" THEN
									PostFix=Val(RIGHT$(MapName$,2))+1
									MapName$=Left$(MapName$,LEN(MapName$)-2)+REPLACE$(FORMAT$("%%",PostFix)," ","")
								ELSE
									NoDup=1
								ENDIF
							UNTIL NoDup
							MapName$=MapName$+".png"
							FILE.RENAME DBPath$+Import$+".png",DBPath$+MapName$
							DefMap$="N"
							IF NumMaps=0 THEN DefMap$="Y"
							SQL.INSERT DB_Ptr,TableName$,C1$,MapDescribe$,C2$,MapName$,C3$,STR$(latCent),C4$,STR$(lonCent),C5$,STR$(zlev+1),C6$,STR$(MapKm),C7$,DefMap$
							GR.MODIFY Prg,"text","Adjusting GPX file..."
							GR.RENDER
							POPUP "Map "+Import$+" imported !",0,0,0
							GOSUB GetAllMaps
							ARRAY.DELETE PlusPlaces$[]
							ARRAY.COPY Places$[],PlusPlaces$[]
							ARRAY.LENGTH Nep,PlusPlaces$[]
							ARRAY.DELETE Places$[]
						ENDIF
						IF SelMap<=Nep & SelMap>0 THEN
							Index$=WORD$(PlusPlaces$[SelMap],2,CHR$(30))
							Where$="_id="+"'"+Index$+"'"
							Columns$="_id,"+C2$
							SQL.QUERY Cursor,DB_Ptr,TableName$,Columns$,Where$
							SQL.NEXT xdone,Cursor,Index$,MapSel$
							Route$=LEFT$(MapSel$,LEN(MapSel$)-4)+"_Route00"
							ARRAY.DELETE PlusPlaces$[]
							! Check if filename exists, increase postfix if needed
							NoDup=0
							DO
								FILE.EXISTS UsedName,MapPath$+Route$+".gpx"
								IF UsedName>0 THEN
									PostFix=Val(RIGHT$(Route$,2))+1
									Route$=Left$(Route$,LEN(Route$)-2)+REPLACE$(FORMAT$("%%",PostFix)," ","")
								ELSE
									NoDup=1
								ENDIF
							UNTIL NoDup
							FILE.RENAME MapPath$+ImPort$+".gpx",MapPath$+Route$+".gpx"
							SelFiles$[SelFile]=SelFiles$[SelFile]+" [IMPORTED]"
							NumRoutes=NumRoutes-1
						ENDIF
						! Find start/end time
						StrtT=IS_IN("<time>",Gpx$,IS_IN("<trkpt",Gpx$))
						IF StrtT THEN
							StartTime$=REPLACE$(MID$(Gpx$,StrtT+6,19),"T"," ")
						ELSE
							StartTime$="NOT Found"
						ENDIF
						Gpx2$=Right$(Gpx$,1000):StpT2=0:StpT3=0
						DO
							StpT=StpT2:StpT2=IS_IN("<trkpt",Gpx2$,StpT3):StpT3=StpT2+1
						UNTIL StpT2=0
						StpT=IS_IN("<time>",Gpx2$,StpT)
						IF StpT THEN
							StopTime$=REPLACE$(MID$(Gpx2$,StpT+6,19),"T"," ")
						ELSE
							StopTime$="NOT Found"
						ENDIF
						Gpx2$=""
						! Find number of POI's
						NumP=0:NumP2=0:NumP3=0
						DO
							NumP2=IS_IN("<wpt",Gpx$,NumP3):NumP3=NumP2+1:NumP=NumP+1
						UNTIL NumP2=0
						BYTE.OPEN w,ar,MapPath$+Route$+".gpx"
							Gpx$=REPLACE$(Gpx$,"</gpx>","<rte>")
							Gpx$=Gpx$+CHR$(9)+"<desc>Info about this route"+CrLf$
							Gpx$=Gpx$+CHR$(9)+CHR$(9)+ImPort$+CrLf$
							Gpx$=Gpx$+CHR$(9)+CHR$(9)+Route$+".gpx"+CrLf$
							Gpx$=Gpx$+CHR$(9)+CHR$(9)+"Start route (YY-MM-DD HH:MM)"+CrLf$
							Gpx$=Gpx$+CHR$(9)+CHR$(9)+StartTime$+CrLf$
							Gpx$=Gpx$+CHR$(9)+CHR$(9)+"End route (YY-MM-DD HH:MM)"+CrLf$
							Gpx$=Gpx$+CHR$(9)+CHR$(9)+StopTime$+CrLf$
							Gpx$=Gpx$+CHR$(9)+CHR$(9)+INT$(L-1)+" Route points"+CrLf$
							Gpx$=Gpx$+CHR$(9)+CHR$(9)+INT$(NumP-1)+" POI's"+CrLf$
							Gpx$=Gpx$+CHR$(9)+CHR$(9)+REPLACE$(FORMAT$("##%.##",Driven)," ","")+" Km driven"+CrLf$
							Gpx$=Gpx$+CHR$(9)+CHR$(9)+"End of Info"+CrLf$
							Gpx$=Gpx$+CHR$(9)+"</desc>"+CrLf$
							Gpx$=Gpx$+"</rte>"+CrLf$
							Gpx$=Gpx$+"</gpx>"+CrLf$
							BYTE.WRITE.BUFFER ar,Gpx$
						BYTE.CLOSE ar
						GR.CLS
						GR.RENDER
						ARRAY.DELETE GPXData$[]
					ENDIF
				UNTIL NumRoutes=0 | SelFile=L | SelMap=Nep
				ARRAY.DELETE SelFiles$[]
			ENDIF
			LIST.CLEAR RP
			ARRAY.DELETE AllFiles$[]
		ENDIF
	UNTIL Answer=4
RETURN
LoGStart:
	GPS.OPEN
	GR.BITMAP.DRAW Diagram,BG,0,0
	GR.COLOR 255,0,255,0,1
	GR.TEXT.DRAW NumInView,35,50,"0"
	GR.TEXT.DRAW NumInFix,35,110,"0"
	GR.COLOR 255,255,255,255,1
	GR.TEXT.DRAW LocalTime,440,30,USING$("","%tT",TIME())
	GR.COLOR 255,255,255,0,1
	GR.TEXT.DRAW WaitTime,520,580,"00:00"
	GR.COLOR 255,255,0,0,1
	GR.SET.STROKE 5
	GR.CIRCLE SC,300,300,10
	GR.LINE Scan,80,300,300,300
	GR.SET.STROKE 2
	GR.RENDER
	ScanStart=VAL(USING$("","%ts",TIME()))
	Kap=0
	DO
		x1=220*COS((Angle+180)*pi()/180)+300
		y1=220*SIN((Angle+180)*pi()/180)+300
		GR.MODIFY Scan,"x1",x1,"y1",y1
		Angle=Angle+10
		IF Angle=360 THEN Angle=0
		GR.MODIFY LocalTime,"text",USING$("","%tT",TIME())
		Duration=VAL(USING$("","%ts",TIME()))-ScanStart
		WaitTime$=USING$("","%02d",INT(MOD(Duration/60,60)))+":"+USING$("","%02d",INT(MOD(Duration,60)))
		GR.MODIFY WaitTime,"text",WaitTime$
		GPS.STATUS Status,InFix,InView,SatList
		GR.MODIFY NumInView,"text",INT$(InView)
		GR.MODIFY NumInFix,"text",INT$(InFix)
		IF InView THEN
			FOR i=1 TO InView
				LIST.GET SatList,i,SatNum
				BUNDLE.CONTAIN SatNum,"elevation",Bfilled
				IF BFilled THEN
					GOSUB ShowSats
				ENDIF
			NEXT i
			GPS.ACCURACY Signal
			GR.RENDER
			GR.GETDL Temp[],1
			ARRAY.LENGTH L,Temp[]
			GR.NEWDL Temp[1,L-InView*2]
			! stable Signal ?
			IF Infix>4 & Signal>0 & Signal<20 THEN
				Kap=2
			ENDIF
			ARRAY.DELETE Temp[]
		ELSE
			GR.RENDER
		ENDIF
		PAUSE 200
	UNTIL Kap
	GR.HIDE Scan
	GR.HIDE SC
	FOR i=1 TO InView
		LIST.GET SatList,i,SatNum
		GOSUB ShowSats
	NEXT i
	GR.RENDER
	IF Kap=2 THEN
		PAUSE 3000
	ENDIF
	GR.CLS
	IF Kap=1 THEN
		Kap=0
		RETURN
	ENDIF
	Kap=0
	Beep()
	GPS.LOCATION ,,NumSat,Signal,latitude,longitude,altitude,bearing,speed
	GR.BITMAP.CREATE ThisMap,600,600
	IF DefSet$="" THEN
		! Log without map
		MapDescribe$="NOT Present"
		MapName$="WithoutMap00.png"
		LatCent=latitude
		LonCent=longitude
		zlev=10
		MapKm=(COS(TORADIANS(latCent))*2*PI()*ER)/(256*2^(zlev))*300
		GR.BITMAP.DRAWINTO.START ThisMap
			GR.COLOR 255,50,50,100,1
			GR.RECT g,0,0,600,600
			GR.COLOR 255,255,255,100,0
			GR.RECT g,0,0,600,600
		GR.BITMAP.DRAWINTO.END
		! Add to DataBase if not yet present....
		Where$=C2$+"='"+MapName$+"'"
		Columns$=C2$
		SQL.QUERY Cursor,DB_Ptr,TableName$,Columns$,Where$
		SQL.NEXT xdone,Cursor,MapPresent$
		IF MapName$<>MapPresent$ THEN
			SQL.INSERT DB_Ptr,TableName$,C1$,MapDescribe$,C2$,MapName$,C3$,STR$(latitude),C4$,STR$(longitude),C5$,INT$(zlev),C6$,STR$(MapKm),C7$,"N"
		ELSE
			SQL.UPDATE DB_Ptr,TableName$,C3$,STR$(latitude),C4$,STR$(longitude)
		ENDIF
	ELSE
		GOSUB GetMapData
		GR.BITMAP.LOAD Map,DBPath$+MapName$
		GR.BITMAP.DRAWINTO.START ThisMap
			GR.BITMAP.DRAW g,Map,0,0
		GR.BITMAP.DRAWINTO.END
	ENDIF
	scaleLon=256*2^zlev/360
	scaleLat=scaleLon/cos(toradians(latCent))
	TIMEZONE.GET TZ$
	TIMEZONE.SET "GMT"
	NumOfPoints=0
	PNr=0
	PrevLat=TORADIANS(latitude)
	PrevLon=TORADIANS(Longitude)
	StartTime$=USING$("","%tF",time())+" "+USING$("","%tT",time())
	LogStart=VAL(USING$("","%ts",TIME()))
	TIME Year$,Month$,Day$,Hour$,Minute$,Second$
	Stamp$=Year$+"-"+Month$+"-"+Day$+"T"+Hour$+":"+Minute$+":"+Second$
	Route$=REPLACE$(MapName$,".png","_Route00")
	! Check if filename exists, increase postfix if needed
	NoDup=0
	DO
		FILE.EXISTS UsedName,MapPath$+Route$+".gpx"
		IF UsedName>0 THEN
			PostFix=Val(RIGHT$(Route$,2))+1
			Route$=Left$(Route$,LEN(Route$)-2)+REPLACE$(FORMAT$("%%",PostFix)," ","")
		ELSE
			NoDup=1
		ENDIF
	UNTIL NoDup
	Route$=Route$+".gpx"
	TEXT.OPEN W, FN1,MapPath$+Route$
	TEXT.WRITELN FN1,"<gpx version=";CHR$(34);"1.1";CHR$(34);" creator=";CHR$(34);"GPX Bike logger Aat";CHR$(34);">"
	GOSUB DataToString
	TEXT.WRITELN FN1,"<wpt lat=";CHR$(34);LA$;CHR$(34);" lon=";CHR$(34);LO$;CHR$(34);"><name>Track Start</name></wpt>"
	TEXT.WRITELN FN1,"<trk><name>";MapDescribe$;"</name>"
	TEXT.WRITELN FN1,CHR$(9);"<trkseg>"
!	GOSUB TrackWrite
	TEXT.CLOSE FN1
	GPSData$=""
	Distance=0
	GR.BITMAP.DRAW TheMap,ThisMap,0,0
	FILE.DIR MapPath$,AllFiles$[]
	ARRAY.LENGTH NumFiles,AllFiles$[]
	NumRoutes=0
	LIST.CREATE S,RP
	FOR gf=1 TO NumFiles
		IF RIGHT$(AllFiles$[gf],3)="SOM" & LEFT$(AllFiles$[gf],LEN(MapName$)-4)=LEFT$(MapName$,LEN(MapName$)-4) THEN
			FCh$=LEFT$(AllFiles$[gf],LEN(AllFiles$[gf])-4)+".gpx"
			FILE.EXISTS g,MapPath$+FCh$
			IF g THEN
				NumRoutes=NumRoutes+1
				LIST.ADD RP,FCh$
			ENDIF
		ENDIF
	NEXT gf
	ARRAY.DELETE AllFiles$[]
	LineCol=0
	! Show Route(s) if present
	FOR i=1 TO NumRoutes
		LIST.GET RP,i,F$
		GRABFILE Gpx$,MapPath$+F$
		SPLIT GPXData$[],Gpx$,"<trkpt"
		ARRAY.LENGTH L,GPXData$[]
		GR.SET.STROKE 3
		LineCol$=BIN$(LineCol)
		WHILE LEN(LineCol$)<3
			LineCol$="0"+LineCol$
		REPEAT
		LineCol=LineCol+1
		GR.COLOR 128,VAL(LEFT$(LineCol$,1))*128,VAL(MID$(LineCol$,2,1))*128,VAL(RIGHT$(LineCol$,1))*128,0
		PrevXPnt=299+(VAL(WORD$(GPXData$[2],4,CHR$(34)))-lonCent)*scaleLon
		PrevYPnt=299-(VAL(WORD$(GPXData$[2],2,CHR$(34)))-latCent)*scaleLat
		FOR j=2 TO L
			XPnt=299+(VAL(WORD$(GPXData$[j],4,CHR$(34)))-lonCent)*scaleLon
			YPnt=299-(VAL(WORD$(GPXData$[j],2,CHR$(34)))-latCent)*scaleLat
			IF SQR((XPnt-PrevXPnt)^2+(YPnt-PrevYPnt)^2)>10 THEN
				DashedLine(PrevXPnt,PrevYPnt,XPnt,YPnt)
				PrevXPnt=XPnt:PrevYPnt=YPnt
			ENDIF
		NEXT j
		ARRAY.DELETE GPXData$[]
		! Add Poi's
		SPLIT GPXPOIData$[],Gpx$,"<wpt"
		ARRAY.LENGTH L,GPXPOIData$[]
		GR.COLOR 128,128,0,0,1
		FOR j=2 TO L
			XPnt=299+(VAL(WORD$(GPXPOIData$[j],4,CHR$(34)))-lonCent)*scaleLon
			YPnt=299-(VAL(WORD$(GPXPOIData$[j],2,CHR$(34)))-latCent)*scaleLat
			GR.CIRCLE g,XPnt,YPnt,8
		NEXT j
		ARRAY.DELETE GPXPOIData$[]
	NEXT i
	LIST.CLEAR RP
	! Convert Lon, Lat to x,y for 1st point on map
	XPnt=299+(longitude-lonCent)*scaleLon
	YPnt=299-(latitude-latCent)*scaleLat
	GOSUB DrawScaleBar
	GR.COLOR 255,0,0,255,1
	GR.RECT g,0,600,600,635
	GR.COLOR 255,255,255,255,1
	GR.TEXT.ALIGN 2
	GR.TEXT.DRAW ViewDat,300,630,""
	GR.TEXT.ALIGN 1
	GR.COLOR 255,255,255,0,1
	GR.TEXT.DRAW g,50,655,"Back Key for Log Menu"
	GR.BITMAP.DRAWINTO.START ThisMap
		GR.COLOR 255,0,255,0,1
		GR.CIRCLE g,XPnt,YPnt,7
	GR.BITMAP.DRAWINTO.END
	GR.TEXT.SIZE 100
	GR.TEXT.BOLD 1
	GR.COLOR 255,0,0,255,1
	GR.TEXT.DRAW BigDat1,20,110,""
	GR.TEXT.DRAW BigDat2,20,220,""
	GR.TEXT.DRAW BigDat3,20,330,""
	GR.TEXT.DRAW BigDat4,20,440,""
	GR.TEXT.DRAW BigDat5,20,550,""
	GR.RENDER
	PrevX=XPnt:PrevY=YPnt:LogBusy=1
	! Timer
	Wait=Val(USING$("","%tQ",time()))+2000
	ShowDat=0:hh=0:dev=0.1
	DO
		Delay=Val(USING$("","%tQ",time()))+500
		! Time Stamp 
		TIME Year$,Month$,Day$,Hour$,Minute$,Second$
		Stamp$=Year$+"-"+Month$+"-"+Day$+"T"+Hour$+":"+Minute$+":"+Second$
		! Local Time
		LocTime$=USING$("","%tR",time())
		! Duration
		LogDur=VAL(USING$("","%ts",TIME()))-LogStart
		DrTime$=USING$("","%02d",INT(MOD(LogDur/3600,24)))+":"+USING$("","%02d",INT(MOD(LogDur/60,60)))
		GPS.LOCATION ,,NumSat,Signal,latitude,longitude,altitude,bearing,speed
		! Calculate distance from previous point (Haversine formulae without correction for imperfect sphere)
		! Save coordinates if distance >= 10 m
		CurLat=TORADIANS(latitude)
		CurLon=TORADIANS(longitude)
		dLon=CurLon-PrevLon
		dLat=CurLat-PrevLat
		a=((SIN(dLat/2))^2+COS(PrevLat)*COS(CurLat)*(SIN(dLon/2))^2)
		c=(2*ATAN2(SQR(a),SQR(1-a)))
		! d in km
		d=ER*c
		IF d>=0.01 & Signal>0 & Signal<20 THEN
			Distance=Distance+d
			! Save point if the next one deviates from a straight line
			IF CurLon=PrevLon THEN CurLon=CurLon+0.0000001
			IF (CurLat-PrevLat)/(CurLon-PrevLon)<hh-dev | (CurLat-PrevLat)/(CurLon-PrevLon)>hh+dev THEN
				hh=(CurLat-PrevLat)/(CurLon-PrevLon)
				NumOfPoints=NumOfPoints+1
				latitude=TODEGREES(PrevLat):longitude=TODEGREES(PrevLon):altitude=PrevAlt:speed=PrevSpeed
				GOSUB DataToString
				IF NumOfPoints/50=INT(NumOfPoints/50) THEN
					GPSData$=GPSData$+CHR$(9)+CHR$(9)+"<trkpt lat="+CHR$(34)+LA$+CHR$(34)+" lon="+CHR$(34)+LO$+CHR$(34)+"><ele>"+EL$+"</ele><time>"+Stamp$+"</time><speed>"+SP$+"</speed></trkpt>"+CrLf$
					! Save data (at approx. every 4Kb)
					TEXT.OPEN A, FN1,MapPath$+Route$
						TEXT.WRITELN FN1,LEFT$(GPSData$,LEN(GPSData$)-2)
					TEXT.CLOSE FN1
					GPSData$=""
				ELSE
					GPSData$=GPSData$+CHR$(9)+CHR$(9)+"<trkpt lat="+CHR$(34)+LA$+CHR$(34)+" lon="+CHR$(34)+LO$+CHR$(34)+"><ele>"+EL$+"</ele><time>"+Stamp$+"</time><speed>"+SP$+"</speed></trkpt>"+CrLf$
				ENDIF
			ENDIF
			PrevLat=CurLat:PrevLon=CurLon:PrevAlt=altitude:PrevSpeed=speed
			! Draw point on map
			XPnt=299+(longitude-lonCent)*scaleLon:YPnt=299-(latitude-latCent)*scaleLat
			GR.BITMAP.DRAWINTO.START ThisMap
				GR.SET.STROKE 3
				GR.COLOR 255,255,0,0,1
				GR.LINE g,PrevX,PrevY,XPnt,YPnt
			GR.BITMAP.DRAWINTO.END
			GR.RENDER
			PrevX=XPnt:PrevY=YPnt
		ENDIF
		! Show data
		GWait=Val(USING$("","%tQ",time()))
		IF GWait>Wait THEN
			! Timer - Change text every 2000 ms
			Wait=Val(USING$("","%tQ",time()))+2000
			IF Signal>100 THEN Signal=100
			ShowDat=ShowDat+1
			IF ShowDat=1 THEN NwDat$="Current Time: "+LocTime$+" - POI's: "+INT$(PNr)
			IF ShowDat=2 THEN NwDat$="Points Logged: "+INT$(NumOfPoints)+" - Accuracy: "+INT$(Signal)+" m"
			IF ShowDat=3 THEN NwDat$="Speed: "+REPLACE$(FORMAT$("##%.#",speed*3.6)," ","")+" km/h - Bearing: "+Bear$[ROUND(bearing/22.5,2)+1]
			IF ShowDat=4 THEN
				NwDat$="Time Driven: "+DrTime$+" - Distance: "+REPLACE$(FORMAT$("%%%.##",Distance)," ","")+" km"
				ShowDat=0
			ENDIF
			GR.MODIFY ViewDat,"text",NwDat$
			GR.RENDER
		ENDIF
		IF Kap=1 THEN
			DIALOG.SELECT Answer,LogMenu$[],"BikeLog - Log Menu"
			IF Answer=1 THEN
				! Show Large text if screen touched
				GR.MODIFY BigDat1,"text",INT$(NumOfPoints)+" pnts."
				GR.MODIFY BigDat2,"text","Acc: "+INT$(Signal)+" m"
				GR.MODIFY BigDat3,"text","Dir:"+Bear$[ROUND(bearing/22.5,2)+1]
				GR.MODIFY BigDat4,"text",DrTime$+" driven"
				GR.MODIFY BigDat5,"text",REPLACE$(FORMAT$("%%%.##",Distance)," ","")+" km"
				GR.RENDER
				PAUSE 3000
				GR.MODIFY BigDat1,"text",""
				GR.MODIFY BigDat2,"text",""
				GR.MODIFY BigDat3,"text",""
				GR.MODIFY BigDat4,"text",""
				GR.MODIFY BigDat5,"text",""
			ENDIF
			IF Answer=2 THEN
				! INSERT POI
				PNr=PNr+1
				PN$="POI"+REPLACE$(FORMAT$("###",PNr)," ","")
				INPUT "Enter description for this POI",PoiName$,PN$,IsCanceled
				IF IsCanceled THEN
					PNr=Pnr-1
				ELSE
					GPSData$=GPSData$+"<wpt lat="+CHR$(34)+LA$+CHR$(34)+" lon="+CHR$(34)+LO$+CHR$(34)+"><name>"+PoiName$+"</name></wpt>"+CrLf$
					GR.BITMAP.DRAWINTO.START ThisMap
						GR.COLOR 255,0,0,255,1
						GR.CIRCLE g,XPnt,YPnt,7
					GR.BITMAP.DRAWINTO.END
					GR.RENDER
				ENDIF
			ENDIF
			IF Answer=3 THEN
				! Pause/resume
				DIALOG.MESSAGE "PAUSE LOG","Logging suspended",q,"RESUME"
			ENDIF
			Kap=0
			IF Answer=4 THEN Kap=2
		ENDIF
		DO
			! Pause till approx. 500ms
			GDelay=Val(USING$("","%tQ",time()))
		UNTIL GDelay>Delay
	UNTIL Kap=2
	TEXT.OPEN A, FN1,MapPath$+Route$
	IF GPSData$<>"" THEN TEXT.WRITELN FN1,LEFT$(GPSData$,LEN(GPSData$)-2)
	GPS.LOCATION ,,NumSat,Signal,latitude,longitude,altitude,bearing,speed
	GPS.CLOSE
	XPnt=299+(longitude-lonCent)*scaleLon
	YPnt=299-(latitude-latCent)*scaleLat
	GR.BITMAP.DRAWINTO.START ThisMap
		GR.COLOR 255,0,0,255,1
		GR.CIRCLE g,XPnt,YPnt,7
	GR.BITMAP.DRAWINTO.END
	GR.RENDER
	! Close the GPX file .............
	GOSUB DataToString
	GOSUB TrackWrite
	TEXT.WRITELN FN1,CHR$(9);"</trkseg>"
	TEXT.WRITELN FN1,"</trk>"
	TEXT.WRITELN FN1,"<wpt lat=";CHR$(34);LA$;CHR$(34);" lon=";CHR$(34);LO$;CHR$(34);"><name>Track End</name></wpt>"
	TEXT.WRITELN FN1,"<rte>"
	TEXT.WRITELN FN1,CHR$(9);"<desc>Info about this route"
	TEXT.WRITELN FN1,CHR$(9);CHR$(9);MapDescribe$
	TEXT.WRITELN FN1,CHR$(9);CHR$(9);Route$
	TEXT.WRITELN FN1,CHR$(9);CHR$(9);"Start route (YY-MM-DD HH:MM)"
	TEXT.WRITELN FN1,CHR$(9);CHR$(9);StartTime$
	TEXT.WRITELN FN1,CHR$(9);CHR$(9);"End route (YY-MM-DD HH:MM)"
	StopTime$=USING$("","%tF",time())+" "+USING$("","%tT",time())
	TEXT.WRITELN FN1,CHR$(9);CHR$(9);StopTime$
	TEXT.WRITELN FN1,CHR$(9);CHR$(9);INT$(NumOfPoints+1);" Route points"
	TEXT.WRITELN FN1,CHR$(9);CHR$(9);INT$(PNr+2);" POI's"
	TEXT.WRITELN FN1,CHR$(9);CHR$(9);REPLACE$(FORMAT$("##%.##",Distance)," ","");" Km driven"
	TEXT.WRITELN FN1,CHR$(9);CHR$(9);"End of Info"
	TEXT.WRITELN FN1,CHR$(9);"</desc>"
	TEXT.WRITELN FN1,"</rte>"
	TEXT.WRITELN FN1,"</gpx>"
	TEXT.CLOSE FN1
	TIMEZONE.SET TZ$
	IF NumOfPoints<2 THEN
		! Delete file
		FILE.DELETE ND,MapPath$+REPLACE$(Route$,"png","gpx")
		POPUP "NO log data saved !",0,0,0
	ELSE
		POPUP "Log data SAVED !",0,0,0
	ENDIF
	LogBusy=0
	Beep()
	PAUSE 3000
	GR.CLS
	GR.BITMAP.DELETE ThisMap
RETURN
SetDefMap:
	GOSUB GetAllMaps
	IF !n THEN RETURN
	DIALOG.SELECT Answer,Places$[],"BikeLog - Select Map to use for Logging"
	IF Answer>0 THEN
		Index$=WORD$(Places$[Answer],2,CHR$(30))
		SQL.UPDATE DB_Ptr,TableName$,C7$,"N":"DefMap='Y'"
		SQL.UPDATE DB_Ptr,TableName$,C7$,"Y":"_id="+"'"+Index$+"'"
		PS$=WORD$(Places$[Answer],1,CHR$(30))
		POPUP "Map "+LEFT$(PS$,LEN(PS$)-1)+" set as default",0,0,0
	ENDIF
	ARRAY.DELETE Places$[]
RETURN
GetDefMap:
	Where$="DefMap='Y'"
	Columns$="_id,"+C1$+","+C2$
	SQL.QUERY Cursor,DB_Ptr,TableName$,Columns$,Where$
	SQL.NEXT xdone,Cursor,Index$,MapDescribe$,DefSet$
	IF xdone | NumMaps=0 THEN
		MapDescribe$="No Map selected for Logging !"
		DefSet$=""
	ENDIF
RETURN
onBackKey:
	Beep()
	IF LogBusy=1 THEN
		Kap=1
	ELSE
		DIALOG.MESSAGE "BikeLog","Interrupt Action ???",Answer,"Yes","No"
		IF Answer>0 THEN
			IF Answer=1 THEN Kap=1
		ENDIF
	ENDIF
BACK.RESUME
GetTouch:
DO
	GR.TOUCH Touched,x,y
	IF Kap=1 THEN
		D_U.BREAK
		RETURN
	ENDIF
UNTIL Touched
DO
	GR.TOUCH Touched,x,y
UNTIL !Touched
x/=sx
y/=sy
RETURN
DrawScaleBar:
	kmperpixel=MapKm/600
	GR.SET.STROKE 2
	GR.COLOR 255,200,200,200,1
	GR.RECT g,0,560,190,600
	GR.COLOR 255,255,0,0,1
	GR.LINE g,20,595,170,595
	GR.LINE g,20,590,20,600
	GR.LINE g,170,590,170,600
	GR.TEXT.DRAW g,15,590,FORMAT$("#%.##",kmperpixel*150)+" km"
RETURN
DataToString:
	LA$=REPLACE$(FORMAT$("##%.######",latitude)," ","")
	LO$=REPLACE$(FORMAT$("###%.######",longitude)," ","")
	EL$=REPLACE$(FORMAT$("#####%.###",altitude)," ","")
	SP$=REPLACE$(FORMAT$("##%.##",speed)," ","")
RETURN
TrackWrite:
	TEXT.WRITELN FN1,CHR$(9);CHR$(9);"<trkpt lat=";CHR$(34);LA$;CHR$(34);" lon=";CHR$(34);LO$;CHR$(34);"><ele>";EL$;"</ele><time>";Stamp$;"</time><speed>";SP$;"</speed></trkpt>"
RETURN
ShowSats:
	BUNDLE.GET SatNum,"prn",SatID
	BUNDLE.GET SatNum,"elevation",Elevation
	BUNDLE.GET SatNum,"azimuth",Azimuth
	BUNDLE.GET SatNum,"snr",StoN
	BUNDLE.GET SatNum,"infix",Fixed
	GR.COLOR 255,INT((255-StoN*8.5)*(StoN<31)),INT((StoN*6.2)*(StoN<31)+(StoN>30)*(StoN+155)),0,1
	r=210-(ABS(Elevation)/90*210)
	x=r*COS((Azimuth+270)*pi()/270)+300
	y=r*SIN((Azimuth+270)*pi()/270)+300
	GR.CIRCLE g,x,y,25
	GR.COLOR 255,255,255,255,1
	GR.TEXT.ALIGN 2
	GR.TEXT.DRAW g,x,y+10,INT$(SatID)
	GR.TEXT.ALIGN 1
RETURN
GetAllMaps:
	ARRAY.DELETE Places$[]
	LIST.CREATE S,AllMaps
	Columns$="_id,"+C1$
	SQL.QUERY Cursor,DB_Ptr,TableName$,Columns$
	xdone = 0
	DO
		SQL.NEXT xdone,Cursor,Index$,V1$
		IF !xdone THEN
			LIST.ADD AllMaps,V1$+" ("+CHR$(30)+Index$+CHR$(30)+")"
		ENDIF
	UNTIL xdone
	LIST.SIZE AllMaps,n
	IF n THEN
		LIST.TOARRAY AllMaps,Places$[]
		LIST.CLEAR AllMaps
	ELSE
		POPUP "NO MAPS PRESENT !!!",0,0,0
	ENDIF
RETURN
GetMapData:
	Where$="_id="+"'"+Index$+"'"
	Columns$="_id,"+C1$+","+C2$+","+C3$+","+C4$+","+C5$+","+C6$+","+C7$
	SQL.QUERY Cursor,DB_Ptr,TableName$,Columns$,Where$
	SQL.NEXT xdone,Cursor,Index$,MapDescribe$,MapName$,latCent$,lonCent$,Zoom$,MapWidth$,MapIsDef$
	latCent=VAL(latCent$)
	lonCent=VAL(lonCent$)
	zlev=VAL(Zoom$)
	MapKm=VAL(MapWidth$)
RETURN
GetNumOfMaps:
	Columns$="_id"
	SQL.QUERY Cursor,DB_Ptr,TableName$,Columns$
	SQL.QUERY.LENGTH NumMaps,Cursor
RETURN
