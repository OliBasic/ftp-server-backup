! chooseFileDir
! Vladimir Renard, 2011

fn.def endswith(searchfor$, searchin$)
	! return 0 if doesnt match, len(searchin$) if it matches
	long = len(searchfor$)
	if len(searchin$)<long then
		fn.rtn 0
	endif
	if right$(searchin$,long) = searchfor$ then
		fn.rtn long
	else
		fn.rtn 0
	endif
fn.end

fn.def startswith(searchfor$, searchin$)
	! return 0 if doesnt match, len(searchin$) if it matches
	long = len(searchfor$)
	if len(searchin$)<long then
		fn.rtn 0
	endif
	if left$(searchin$,long) = searchfor$ then
		fn.rtn long
	else
		fn.rtn 0
	endif
fn.end

fn.def simplifyPath$(rep$)
	if rep$ = "../../../../../" then
		fn.rtn "../../../../"
	endif
	ew = endswith("/../", rep$)
	if ew = 0 then
		fn.rtn rep$
	endif
	if rep$ = "/../" then
		fn.rtn rep$
	endif
	! cherche l'avant dernier répertoire
	i = len(rep$)-4
	while ((mid$(rep$,i,1)<>"/")&(i>1))
		i = i - 1
	repeat
	if i=1 then
		i = 0
	endif
	a$ = mid$(rep$,i+1,len(rep$)-i-4)
	if a$=".." then
		fn.rtn rep$
	else
		fn.rtn left$(rep$,i)
	endif
fn.end

fn.def chooseFileDir$(rep$, dironly)
	! rep$ is the starting directory
	! dironly=1 => displays only directories
	!	each directory is displayed twice: once for navigation and once for selection
	! dironly=0 => displays files + directories
	!	only files can be selected, directories are displayed for navigation only
	! returns the full path of chosen file/directory or "<cancel>" if user used the back button
	fin = 0
	do
		file.dir rep$, a$[]
		array.length n, a$[]
		dim b$[2*n+2]
		b$[1] = "Current Directory: " + rep$
		b$[2] = "..(d)"
		j = 2
		for i=1 to n
			ajoute = 1
			if dironly then
				ajoute= endswith("(d)",a$[i])
			endif
			if ajoute then
				j = j+1
				b$[j] = a$[i]
				if dironly then
					j = j+1
					b$[j] = "  -> select "+ a$[i]
				endif
			endif
		next i
		undim a$[]
		dim c$[j]
		for i = 1 to j
			c$[i] = b$[i]
		next i
		undim b$[]
		select choix, c$[], rep$
		if choix>1 then
			d$ = c$[choix]
			sw = startswith("  -> select ",d$)
			ew = endswith("(d)", d$)
			if sw then
				e$ = c$[choix-1]
				e$ = left$(e$, len(e$)-3)
				e$ = rep$ + e$ + "/"
				e$ = simplifyPath$(e$)
				fin = 1
			elseif ew then
				e$ = d$
				e$ = left$(e$, len(e$)-3)
				rep$ = simplifyPath$(rep$ + e$ + "/")
			else
				e$ = rep$ + d$
				fin = 1
			endif
		elseif choix = 0 then
			e$ = "<cancel>"
			fin = 1
		endif
		undim c$[]
	until fin
	fn.rtn e$
fn.end

a$ = chooseFileDir$("",0)
print a$
a$ = chooseFileDir$("",1)
print a$
