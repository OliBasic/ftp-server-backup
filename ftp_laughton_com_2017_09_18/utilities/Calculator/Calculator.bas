! Calculator with Dot Matrix Display
! AatDon @2013

CONSOLE.TITLE "RFO BASIC Calculator"
GR.OPEN 255,0,0,0,0,1
GR.SCREEN w,h
ScreenWidth=720
ScreenHeight=h/w*ScreenWidth
sX=w/ScreenWidth
sY=h/ScreenHeight
GR.SCALE sX,sY
GR.FRONT 0
PRINT "Initializing......"
WAKELOCK 3
DIM Chars[40]
DIM Keys$[24]
K$="H789/^B456*VD123-PL0.=+C"
FOR i=1 TO 24
  Keys$[i]=MID$(K$,i,1)
NEXT i
GR.CLS
GR.TEXT.BOLD 1
GR.COLOR 200,255,255,255,1
PRINT "1.Case"
FOR i=18 TO 82
  GR.LINE g,0,120+i*9,720,120+i*9
NEXT i
GR.COLOR 128,191,191,191,1
GR.ARC g,0,30,120,150,180,90,1
GR.ARC g,600,30,720,150,270,90,1
GR.ARC g,0,780,120,900,90,90,1
GR.ARC g,600,780,720,900,0,90,1
GR.RECT g,60,30,660,90
GR.RECT g,60,840,660,900
GR.RECT g,0,90,720,840
GR.COLOR 120,0,191,191,1
GR.RECT g,50,290,460,350
GR.COLOR 255,255,255,0,0
GR.RECT g,50,290,460,350
GR.TEXT.SIZE 40
GR.TEXT.DRAW g,55,335,"RFO-Basic! Calculator"
GR.SET.STROKE 4
! Screws
GR.COLOR 255,255,255,125,1
GR.CIRCLE g,25,90,10
GR.CIRCLE g,695,90,10
GR.CIRCLE g,25,230,10
GR.CIRCLE g,695,230,10
GR.CIRCLE g,25,850,10
GR.CIRCLE g,695,850,10
GR.COLOR 255,0,0,0,0
GR.LINE g,17,91,33,89
GR.LINE g,687,89,703,91
GR.LINE g,24,222,26,238
GR.LINE g,689,224,701,236
GR.LINE g,24,842,26,858
GR.LINE g,696,843,693,858
PRINT "2.Characters"
GR.SET.ANTIALIAS 0
For c=1 TO 40
  GR.BITMAP.CREATE Chars[c],46,66
  GR.BITMAP.DRAWINTO.START Chars[c]
  GR.COLOR 255,0,255,0,1
  FOR i=1 TO 7
    READ.NEXT dot
    FOR j=1 TO 5
      IF MID$(BIN$(dot),j+1,1)="1" THEN
        GR.COLOR 255,0,255,0,1
      ELSE
        GR.COLOR 255,60,60,60,1
      ENDIF
      GR.CIRCLE g,3+(j-1)*10,3+(i-1)*10,3
    NEXT j
  NEXT i
  GR.BITMAP.DRAWINTO.END
Next c
! OFF Button
GR.COLOR 255,163,163,163,1
GR.OVAL g,500,340,600,410
GR.COLOR 255,255,0,0,0
GR.OVAL g,500,340,600,410
GR.COLOR 255,0,0,255,1
GR.TEXT.DRAW g,515,390,"OFF"
GR.TEXT.SIZE 60
GR.TEXT.TYPEFACE 2
GR.COLOR 255,0,50,0,1
GR.RECT g,50,50,665,260
GR.COLOR 255,255,255,255,0
GR.RECT g,50,50,665,260
PRINT "3.Display"
! 2 Lines 5x7 chars
GR.COLOR 255,60,60,60,1
FOR l=0 TO 1
  FOR k=0 TO 9
    FOR i=1 TO 5
      FOR j=1 TO 7
        GR.CIRCLE g,55+i*10+k*60,60+j*10+l*100,3
      NEXT j
    NEXT i
  NEXT k
NEXT l
PRINT "4.Buttons"
! Buttons
FOR i=1 TO 5
  FOR j=1 TO 4
    IF i=3 & j=4 THEN
      GR.COLOR 255,0,255,0,1
    ELSE
      GR.COLOR 255,255,255,255,1
    ENDIF
    GR.CIRCLE g,100+i*100,400+j*100,40
    GR.COLOR 255,255,0,0,0
    GR.CIRCLE g,100+i*100,400+j*100,40
  NEXT j
NEXT i
FOR j=1 TO 4
  GR.COLOR 255,255,255,0,1
  GR.RECT g,60,360+j*100,140,440+j*100
  GR.COLOR 255,255,0,0,0
  GR.RECT g,60,360+j*100,140,440+j*100
NEXT j
! Buttons print
GR.COLOR 255,0,0,255,1
GR.TEXT.SIZE 40
GR.TEXT.DRAW g,63,515,"HEX"
GR.TEXT.DRAW g,63,615,"BIN"
GR.TEXT.DRAW g,63,715,"DEC"
GR.TEXT.DRAW g,63,815,"LOG"
GR.TEXT.SIZE 60
P$="7410"
FOR i=1 TO 4
  GR.TEXT.DRAW g,180,420+i*100,MID$(P$,i,1)
NEXT i
P$="852."
FOR i=1 TO 4
  GR.TEXT.DRAW g,280,420+i*100,MID$(P$,i,1)
NEXT i
P$="963="
FOR i=1 TO 4
  GR.TEXT.DRAW g,380,420+i*100,MID$(P$,i,1)
NEXT i
P$="/x-+"
FOR i=1 TO 4
  GR.TEXT.DRAW g,480,420+i*100,MID$(P$,i,1)
NEXT i
GR.TEXT.DRAW g,580,520,"^"
GR.LINE g,580,595,595,615
GR.LINE g,595,615,610,580
GR.LINE g,610,580,620,580
GR.TEXT.DRAW g,580,720,CHR$(177)
GR.TEXT.DRAW g,580,820,"C"
PRINT "5.Ready"
PAUSE 500
CLS
GR.FRONT 1
GR.RENDER
Row=0
N$=""
GOSUB DrawString
Row=1
N$="READY."
GOSUB DrawString

NwCal:
PR$=""
DO
  GR.TOUCH touched,x,y
UNTIL touched
DO
  GR.TOUCH touched,x,y
UNTIL !touched
IF x/sX>500 & x/sX<600 & y/sY>340 & y/sY<410 THEN PR$="Q"
IF PR$="Q" THEN
  TONE 800,200,0
  GR.COLOR 200,255,163,163,1
  GR.OVAL g,500,340,600,410
  Row=1
  N$="BYE      "
  GOSUB DrawString
  FOR b=1 TO 6
    N$=" "+LEFT$(N$,9)
    GOSUB DrawString
  NEXT b
  GOTO KAP
ENDIF
x=FLOOR(x/sX/100+0.4)
y=FLOOR((y/sY-400)/100+0.4)
KeyPressed=(y-1)*6+x
IF KeyPressed>0 & KeyPressed<25 THEN
  PR$=Keys$[KeyPressed]
ELSE
  GOTO NwCal
ENDIF
! PR$ holds valid key pressed
TONE 600,200,0

SW.BEGIN PR$
  SW.CASE "H"
    IF Num$<>"" THEN
      OP$=PR$
      Row=1
      N$="HEX"
      GOSUB DrawString
    ENDIF
  SW.BREAK
  SW.CASE "B"
    IF Num$<>"" THEN
      OP$=PR$
      Row=1
      N$="BIN"
      GOSUB DrawString
    ENDIF
  SW.BREAK
  SW.CASE "D"
    IF Num$<>"" THEN
      OP$=PR$
      Row=1
      N$="DEC"
      GOSUB DrawString
    ENDIF
  SW.BREAK
  SW.CASE "L"
    IF Num$<>"" THEN
      OP$=PR$
      Row=1
      N$="LOG"
      GOSUB DrawString
    ENDIF
  SW.BREAK
  SW.CASE "^"
    IF Num$<>"" THEN
      Num1$=Num$
      Num$=""
      Row=1
      N$="POW"
      GOSUB DrawString
      OP$=PR$
    ENDIF
  SW.BREAK
  SW.CASE "V"
    IF Num$<>"" THEN
      OP$=PR$
      Row=1
      N$="SQR"
      GOSUB DrawString
    ENDIF
  SW.BREAK
  SW.CASE "P"
    IF Num$<>"" THEN
      Row=0
      Num$=STR$(-VAL(Num$))
      N$=Num$
      GOSUB DrawString
    ENDIF
  SW.BREAK
  SW.CASE "C"
    Num1$=""
    Num$=""
    PR$=""
    OP$=""
    Row=0
    N$=""
    GOSUB DrawString
    Row=1
    N$="READY."
    GOSUB DrawString
  SW.BREAK
  SW.CASE "-"
    IF Num$<>"" THEN
      Num1$=Num$
      Num$=""
      OP$=PR$
      Row=1
      N$="SUB"
      GOSUB DrawString
    ENDIF
  SW.BREAK
  SW.CASE "+"
    IF Num$<>"" THEN
      Num1$=Num$
      Num$=""
      OP$=PR$
      Row=1
      N$="ADD"
      GOSUB DrawString
    ENDIF
  SW.BREAK
  SW.CASE "/"
    IF Num$<>"" THEN
      Num1$=Num$
      Num$=""
      OP$=PR$
      Row=1
      N$="DIV"
      GOSUB DrawString
    ENDIF
  SW.BREAK
  SW.CASE "*"
    IF Num$<>"" THEN
      Num1$=Num$
      Num$=""
      OP$=PR$
      Row=1
      N$="MUL"
      GOSUB DrawString
    ENDIF
  SW.BREAK
  SW.CASE "="
    IF OP$="+" & Num$<>"" THEN
      Num$=STR$(VAL(Num1$)+VAL(Num$))
      N$=Num$
      GOSUB DrawString
    ENDIF
    IF OP$="-" & Num$<>"" THEN
      Num$=STR$(VAL(Num1$)-VAL(Num$))
      N$=Num$
      GOSUB DrawString
    ENDIF
    IF OP$="/" & Num$<>"" THEN
      Num$=STR$(VAL(Num1$)/VAL(Num$))
      N$=Num$
      GOSUB DrawString
    ENDIF
    IF OP$="*" & Num$<>"" THEN
      Num$=STR$(VAL(Num1$)*VAL(Num$))
      N$=Num$
      GOSUB DrawString
    ENDIF
    IF OP$="^" & Num$<>"" THEN
      Num$=STR$(POW(VAL(Num1$),VAL(Num$)))
      N$=Num$
      GOSUB DrawString
    ENDIF
    IF OP$="H" THEN
      Row=0
      N$=HEX$(VAL(Num$))
      GOSUB DrawString
      Num$=STR$(Floor(VAL(Num$)))
    ENDIF
    IF OP$="B" THEN
      Row=0
      IF LEN(BIN$(VAL(Num$)))>10 THEN
        N$="OVERFLOW "
      ELSE
        N$=BIN$(VAL(Num$))
      ENDIF
      GOSUB DrawString
    ENDIF
    IF OP$="D" THEN
      Row=0
      N$=Num$
      GOSUB DrawString
    ENDIF
    IF OP$="L" THEN
      Row=0
      Num$=STR$(LOG10(VAL(Num$)))
      N$=Num$
      GOSUB DrawString
    ENDIF
    IF OP$="V" THEN
      Row=0
      Num$=STR$(SQR(VAL(Num$)))
      N$=Num$
      GOSUB DrawString
    ENDIF
    Row=1
    N$="RESULT"
    GOSUB DrawString
    OP$="="
  SW.BREAK
  SW.DEFAULT
    IF PR$="." & IS_IN(".",Num$)<>0 THEN SW.BREAK
    IF OP$="=" THEN
      OP$=""
      Num$=""
    ENDIF
    IF N$="    RESULT" THEN
      N$="READY"
      GOSUB DrawString
    ENDIF
    Num$=Num$+PR$
    Row=0
    N$=Num$
    GOSUB DrawString
SW.END
GOTO NwCal
KAP:
WAKELOCK 5
GR.CLOSE
EXIT

DrawString:
  WHILE LEN(N$)<10
      N$=" "+N$
  REPEAT
  FOR k=1 TO 10
    C$=MID$(N$,k,1)
    Pos=k-1
    GOSUB DrawChar
  NEXT k
  GR.RENDER
RETURN

DrawChar:
  ! Pos=0 through 9
  ! Row=0 / 1
  ! C$=Characters 0 (ASCII 48) through Z (ASCII 90) and SPACE,+,-,.
  C$=UPPER$(C$)
  IF ASCII(C$)=32 THEN Q=37
  IF ASCII(C$)=43 THEN Q=38
  IF ASCII(C$)=45 THEN Q=39
  IF ASCII(C$)=46 THEN Q=40
  IF ASCII(C$)>47 & ASCII(C$)<58 THEN Q=ASCII(C$)-47
  IF ASCII(C$)>64 & ASCII(C$)<91 THEN Q=ASCII(C$)-54
  GR.BITMAP.DRAW g,Chars[Q],62+Pos*60,67+Row*100
RETURN

READ.DATA 46,49,51,53,57,49,46
READ.DATA 36,44,36,36,36,36,36
READ.DATA 62,33,33,63,48,48,63
READ.DATA 62,33,33,63,33,33,62
READ.DATA 48,49,49,63,33,33,33
READ.DATA 63,48,48,63,33,33,62
READ.DATA 46,48,48,62,49,49,46
READ.DATA 63,33,33,34,36,36,36
READ.DATA 46,49,49,46,49,49,46
READ.DATA 46,49,49,47,33,33,62
READ.DATA 46,49,49,49,63,49,49
READ.DATA 62,49,49,62,49,49,62
READ.DATA 46,49,48,48,48,49,46
READ.DATA 62,49,49,49,49,49,62
READ.DATA 63,48,48,62,48,48,63
READ.DATA 63,48,48,62,48,48,48
READ.DATA 46,49,48,48,51,49,46
READ.DATA 49,49,49,63,49,49,49
READ.DATA 46,36,36,36,36,36,46
READ.DATA 33,33,33,33,49,49,46
READ.DATA 49,50,52,56,52,50,49
READ.DATA 48,48,48,48,48,48,63
READ.DATA 49,59,53,49,49,49,49
READ.DATA 49,49,57,53,51,49,49
READ.DATA 46,49,49,49,49,49,46
READ.DATA 62,49,49,49,62,48,48
READ.DATA 46,49,49,49,53,51,47
READ.DATA 62,49,49,62,52,50,49
READ.DATA 46,49,48,46,33,49,46
READ.DATA 63,36,36,36,36,36,36
READ.DATA 49,49,49,49,49,49,46
READ.DATA 49,49,49,49,49,42,36
READ.DATA 49,49,49,49,53,59,49
READ.DATA 49,49,42,36,42,49,49
READ.DATA 49,49,49,42,36,36,36
READ.DATA 63,33,34,36,40,48,63
READ.DATA 32,32,32,32,32,32,32
READ.DATA 32,36,36,63,36,36,32
READ.DATA 32,32,32,63,32,32,32
READ.DATA 32,32,32,32,32,32,36
