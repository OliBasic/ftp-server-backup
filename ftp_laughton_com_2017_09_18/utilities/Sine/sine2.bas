! sine graphic
! using the circle command
! by Antonis [tony_gr]

gr.open 255,0,0,0,0,1
gr.orientation 1
pause 1000
GR.screen w, h
gr.color 255,255,255,255,1
gr.line  Xaxis,0,h/2,w,h/2 
gr.line  Yaxis,0,h/2-h/5,0,h/2+h/5
x=clock()
 for x=0 to 2*3.1459 step 0.01
    x1=x
    y1=sin(x)
   ! scale properly
   x1=x1*w/(2*3.1459)
   y1=h/2+y1*h/5
     
   gr.circle cir,x1,y1,1
 next x
 
gr.render
print (clock()-x)/1000
do
until 0
  
  
