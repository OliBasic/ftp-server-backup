! sine graphic
! using the line command
! by Antonis [tony_gr]

gr.open 255,0,0,0,0,1
gr.orientation 1
pause 1000
GR.screen w, h
gr.color 255,255,255,255,1
gr.line  Xaxis,0,h/2,w,h/2 
gr.line  Yaxis,0,h/2-h/5,0,h/2+h/5

 for x=0.1 to 2*3.1459+0.1 step 0.1
   x1=x-0.1
   y1=sin(x1)
   x2=x
   y2=sin(x)
   ! scale properly
   x1=x1*w/(2*3.1459)
   y1=h/2+y1*h/5
   x2=x2*w/(2*3.1459)
   y2=h/2+y2*h/5
   
   gr.line lin,x1,y1,x2,y2
 next x
 
gr.render
do
until 0
  
  
