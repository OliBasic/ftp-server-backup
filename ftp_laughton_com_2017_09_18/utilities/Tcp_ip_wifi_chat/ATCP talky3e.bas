WAKELOCK 1
GR.OPEN 25,50,50,255,1,1
GR.STATUSBAR sbh
GR.SCREEN sw,sh
sh=sh-sbh
ts=sh/20
FILE.EXISTS fi, "../../mytcpchat/data/pip"
IF fi<1
 FILE.MKDIR "../../mytcpchat/data/"
ELSE
 GRABFILE ip$,"../../mytcpchat/data/pip"
ENDIF


mkscreen:
GR.BITMAP.CREATE tpwt,sw,sh/2
GR.BITMAP.DRAWINTO.START tpwt
GR.COLOR 195,200,50,50,1
GR.RECT trect,0,0,sw,sh/2
GR.COLOR 255,200,200,200,1
GR.TEXT.SIZE ts
GR.TEXT.ALIGN 2
GR.TEXT.DRAW ttxt,sw/2,sh/4,"Please Wait"
GR.COLOR 255,200,200,200,0
gr.set.stroke ts/4
GR.RECT trect,0,0,sw,sh/2

GR.BITMAP.DRAWINTO.END
GR.BITMAP.CREATE tmyscr,sw,sh
GR.BITMAP.DRAWINTO.START tmyscr
GR.COLOR 255,50,200,200,1
GR.SET.STROKE ts/15
GR.COLOR 255,200,200,200,1
GR.LINE tline,0,ts*2.5,sw,ts*2.5
GR.TEXT.SIZE ts*0.75
GR.LINE tline,0,ts*4.5,sw,ts*4.5
GR.TEXT.ALIGN 2
GR.TEXT.TYPEFACE 4,2
GR.TEXT.DRAW ttxt,sw/2,ts*5.75,"Contacts"
GR.LINE tline,0,ts*6.5,sw,ts*6.5
GR.BITMAP.DRAWINTO.END
GR.BITMAP.DRAW myscr,tmyscr,0,sbh
GR.COLOR 255,50,150,50,1
GR.RECT okrect,ts*2,ts*10,sw-(ts*2),ts*18
GR.COLOR 255,150,50,50,0
GR.SET.STROKE ts
GR.RECT recrect ,ts*2,ts*10,sw-(ts*2),ts*18
GR.COLOR 255,150,50,50,1
GR.RECT nokrect, ts*2,ts*10,sw-(ts*2),ts*18
GR.HIDE nokrect
GR.BITMAP.LOAD lbmp,"speak.png"
GR.BITMAP.SCALE sbmp,lbmp,ts*6,ts*6
GR.BITMAP.DRAW micbmp,sbmp,(sw/2)-ts*3,ts*11
GR.HIDE recrect
GR.COLOR 255,50,200,200,1
GR.TEXT.SIZE ts*1.75
GR.TEXT.ALIGN 2
GR.TEXT.TYPEFACE 4,2
GR.TEXT.DRAW thd,sw/2,(ts*1.75)+sbh,"Me Chat"
GR.COLOR 255,200,200,200,1
GR.TEXT.SIZE ts*0.75
GR.TEXT.DRAW cntct ,sw/2,sh-(ts/2),""
GR.TEXT.DRAW ttxtip ,sw/2,(ts*3.75)+sbh,"My IP: "+ip$
GR.BITMAP.DRAW pwt,tpwt,0,sh/4
GR.HIDE pwt
GR.RENDER

GOSUB gtip

DO
 DO 
  GOSUB server
 UNTIL tc$<>""
 SOCKET.SERVER.CLOSE
 IF tc$="speak"
 ENDIF
UNTIL b<0


append:
IF emsg$<>""
 PAUSE 1500
ENDIF
exit
END


ONGRTOUCH:
tc$="Stop"
DO
 GR.TOUCH tch,tcx,tcy
UNTIL tch>0 
stim=TIME()
DO
 GR.TOUCH tch1,tcx1,tcy1
UNTIL tch1<1 |  ABS(TIME()-stim)>500
IF ABS(TIME()-stim)>500
 IF tcx1>ts*2 & tcx1< sw-(ts*2)
  IF tcy1>ts*10 & tcy1<ts*18
   tc$="speak"
   IF cip$<>""
    GOSUB record
    GOSUB send
   ELSE
    POPUP "Select a contact first",0,0,0
   ENDIF
   tc$=""
  ENDIF
 ENDIF
ENDIF
IF ABS(tcy-tcy1)<ts & ABS(tcx-tcx1)<ts
 GOSUB touched
ENDIF
GR.ONGRTOUCH.RESUME


touched:
tcy1=tcy1-sbh
IF tcy1>(ts*4.5) & tcy1<(ts*6.5)
 GOSUB contacts
ENDIF
IF tcy1>(ts*2.5) & tcy1<(ts*4.5)
 GOSUB mynewip
ENDIF
RETURN


mynewip:
GOSUB gtip
DIALOG.SELECT dsip,myip$[],"Select You IP"
IF dsip>0
 UNDIM tmp$[]
 SPLIT tmp$[],myip$[dsip],": "
 ip$=tmp$[2]
 BYTE.OPEN w,f, "../../mytcpchat/data/pip"
 BYTE.WRITE.BYTE f,ip$
 BYTE.CLOSE f
 GR.MODIFY ttxtip,"text","My IP: "+ip$
 GR.RENDER
ENDIF
RETURN


gtip:
GR.SHOW pwt
GR.RENDER
SOCKET.MYIP myip$[],mn
ip$=myip$[mn]
IF pip$<>ip$
 BYTE.OPEN w,f, "../../mytcpchat/data/pip"
 BYTE.WRITE.BYTE f,ip$
 BYTE.CLOSE f
ENDIF
GRABURL wip$, "http://icanhazip.com",2000
IF wip$<>""
 ip$="Wan: "+TRIM$(wip$)
 DIM myip$[1]
 myip$[1]=ip$
ELSE
 FOR t=1 TO mn
  myip$[t]="Lan: "+myip$[t]
 NEXT t
ENDIF
GR.HIDE pwt
GR.RENDER
RETURN


contacts:
FILE.EXISTS fi, "../../mytcpchat/data/contacts"
IF fi<1
 FILE.MKDIR "../../mytcpchat/data/contacts"
ELSE
 FILE.DIR "../../mytcpchat/data/contacts", cntclist$[],""
 ARRAY.LENGTH clen, cntclist$[]
ENDIF
IF cntclist>0
 LIST.CLEAR cntclist
ENDIF
LIST.CREATE s,cntclist
LIST.ADD cntclist,"Add a new contact"
IF clen>0
 LIST.ADD.ARRAY cntclist, cntclist$[]
ENDIF
DIALOG.SELECT dsc, cntclist, "Contacts"
IF dsc=1
 GOSUB edco
ENDIF
IF dsc>1
 LIST.GET cntclist ,dsc,cnam$
 GRABFILE cip$, "../../mytcpchat/data/contacts/"+cnam$
 DIALOG.MESSAGE "Contact Maintenance",~
 "Select an Option"+CHR$(10)+CHR$(10)+~
 cnam$+" @ "+cip$,dm,~
 "Select","Edit","Remove"
 IF dm=1
 ENDIF
 IF dm=2
  GOSUB edco
 ENDIF
 IF dm=3
  FILE.DELETE dltd, "../../mytcpchat/data/contacts/"+cnam$
  cip$=""
  cnam$=""
 ENDIF
ENDIF
IF cnam$<>"" & cip$<>""
 GR.MODIFY cntct ,"text",cnam$+" @ "+cip$
ELSE
 GR.MODIFY cntct ,"text",""
ENDIF
GR.RENDER
RETURN


edco:
INPUT "Contact Name",cnam$,cnam$,cncld
cnam$=TRIM$(cnam$)
IF cnam$<>""
 INPUT "Contact IP",cip$,cip$
 cip$=TRIM$(cip$)
 IF cip$<>""
  BYTE.OPEN w,f, "../../mytcpchat/data/contacts/"+cnam$
  BYTE.WRITE.BYTE f,cip$
  BYTE.CLOSE f
 ENDIF
ENDIF
RETURN


server:
SOCKET.SERVER.CREATE 1080


listen:
SOCKET.SERVER.CONNECT 0
DO
 SOCKET.SERVER.STATUS stat
UNTIL stat>2 | tc$="speak"
bb=bb+1
IF stat=3 & bb=1
 BYTE.OPEN w,f, "../../mytcpchat/data/"+ "test.3gp"
 SOCKET.SERVER.READ.FILE f
 BYTE.CLOSE f
 SOCKET.SERVER.DISCONNECT
 a=1
 GOSUB play
 tc$=""
 stat=0
ELSE
 SOCKET.SERVER.CLOSE
 IF bb=2
  bb=0
 ENDIF
 RETURN
ENDIF
GOTO listen


send:
SOCKET.CLIENT.CONNECT cip$,1080,0
stim=TIME()+1500
DO
 SOCKET.CLIENT.STATUS stat
UNTIL stat>2 | TIME()>stim
IF stat=3
 BYTE.OPEN r,f, "../../mytcpchat/data/"+ "test.3gp"
 SOCKET.CLIENT.WRITE.FILE f
 BYTE.CLOSE f
 POPUP"Chat Sent",0,0,0
ELSE
 POPUP"Contact not Connected",0,0,0
ENDIF
SOCKET.CLIENT.CLOSE
RETURN


record:
AUDIO.RECORD.START "../../mytcpchat/data/"+ "test.3gp"
GR.SHOW recrect
GR.RENDER
DO
 GR.TOUCH tch2,tcx2,tcy2
UNTIL tch2<1
GR.HIDE recrect
GR.RENDER
AUDIO.RECORD.STOP
GOSUB send
RETURN


play:
GR.SHOW nokrect
GR.RENDER
IF a=1
 AUDIO.LOAD f, "../../mytcpchat/data/"+ "test.3gp"
 AUDIO.LENGTH l,f
 AUDIO.PLAY f
 DO
  AUDIO.ISDONE ll
 UNTIL ll>0
 AUDIO.STOP
 AUDIO.RELEASE f
 a=0
 FILE.DELETE dltd, "../../mytcpchat/data/"+ "test.3gp"
ENDIF
GR.HIDE nokrect
GR.RENDER
RETURN


ONERROR:
emsg$=GETERROR$()
?"Unexpected error occurred, please send error copied in your clipboard to developper"
CLIPBOARD.PUT emsg$
?
?emsg$
GOTO append


ONBACKKEY:
DIALOG.MESSAGE "Quit",~
"Are you sure?",~
dmq,"Yes","Cancel"
b=-1
IF dmq=1
 GOTO append
ENDIF
BACK.RESUME
