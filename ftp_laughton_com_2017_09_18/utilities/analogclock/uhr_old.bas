!!
Analog watch with date function portrait only
should work on different resolutions
!!
gosub cl_prepare
wakelock 1 % runs in background also

height = 800 % Galaxy S2
width = 480
GR.OPEN 255,30,30,30,1,1
GR.SCREEN maxx, maxy,density
statusBarHeight = Round(25*density/160)

scale_width = maxx / width 
scale_height = maxy / height
! Set the scale
gr.scale scale_width, scale_height

gosub button_ini
gr.text.size 35
call button_add(button$[],"say time!",10,80)
gr.text.size 35
!Gr.text.align 2 or 3 does not work!!!
call button_add(button$[],"Exit",maxx-70,80)
gr.text.size 35
call button_add(button$[],"_ON",10,130)
gr.text.size 21
gr.text.draw n,80,130," every hour sound from 08:00 till 21:00"
if !background() then gr.render

GR.COLOR 255, 255,255, 255, 0
gr.set.stroke 2
!gr.line ln,0,statusBarHeight,maxx,statusBarHeight % testing statusbar
gr.rect ln,0,statusBarHeight,maxx,150

!every hour sound hard coded from 8 till 22 o'clock
sound=1 % true on, all other off
!Radius
radius=maxx/2-20
!Coordinates of the clock
mx=maxx/2
my=maxy/2
!5 Minutes lines
mins=90 % means 100-90=10 percent for the 5 Minutes lines
mins1=95 % normal minutes lines
!clock face
gr.circle n, mx,my,radius % outer circle
gr.circle n, mx,my,radius/100*3 % inner circle

array.load hrs$[],"2","1","12","11","10","9","8","7","6","5","4","3"
array.load weekday$[],"So","Mo","Di","Mi","Do","Fr","Sa"
xx=0
for i=6 to 360 step 6
	y=sin(toradians(i))*radius
	y1=sin(toradians(i))*(radius/100*mins1)
	x=y/tan(toradians(i))
	x1=y1/tan(toradians(i))
	a$=word$(format$("###",i),1)+"," % workaround for the "6.0" etc and whitespaces (trim)
	GR.COLOR 255, 255,255, 255, 0
	!for all numbers from 1 .. 12
	if is_in(a$,"30,60,90,120,150,180,210,240,270,300,330,360,") then
		xx=xx+1
		! dx & dy is for the positioning delta of x & y coordinates of hour text
		if val(hrs$[xx])>0 & val(hrs$[xx])<6 then
			Gr.text.align 3
			dx=-15
		elseif val(hrs$[xx])>6 & val(hrs$[xx])<12 then
			Gr.text.align 1
			dx=+15
		else
			Gr.text.align 2
			dx=0
		endif
		if (val(hrs$[xx])>0 & val(hrs$[xx])<3) | (val(hrs$[xx])>9 & val(hrs$[xx])<=12) then
			dy=20
		elseif (val(hrs$[xx])=3 | val(hrs$[xx])=9) then
			dy=5
		elseif val(hrs$[xx])=6 then
			dy=-10
		else
			dy=0
		endif
		y1=sin(toradians(i))*(radius/100*mins)
		x1=y1/tan(toradians(i))
		Gr.text.size 21
		gr.text.draw txt, mx+x1+dx,my-y1+dy,hrs$[xx]
		!just once for date
		if val(hrs$[xx])=3 then
			Gr.text.align 3
			GR.COLOR 255, 255,0, 0, 0
			gr.text.draw datum, mx+x1+dx*4*scale_width,my-y1+dy,""
		endif

		GR.COLOR 255, 255,0, 0, 0
	endif
	!the objects are NOT stored into an array, because no need
	gr.line ln, mx+x1,my-y1,mx+x,my-y
next

gosub clock_init

if !background() then gr.render

!==========
!mainloop
!==========
timer.set 900
do
	if Background() then
		Pause 500
	else
		pause 100 % for CPU idle performance!
	endif
	gr.touch touched, x, y
	if touched then
		!wait till not touch so if moved during touching
		!it will check last position, so last action be "undone"
		do
			gr.touch touched, x, y
		until !touched
	
		x=button_check(button$[],x/scale_width,y/scale_height)
		if x>0 then
			!touched area
			if x=1 then
				txt$=Hour$+":"+Minute$
				TTS.INIT
				TTS.SPEAK txt$
				TTS.STOP
			elseif x=2 then
				d_u.break
			elseif x=3 then
				if sound=1 then
					sound=0
					gr.modify val(button$[x,5]),"text", "OFF"
				else
					sound=1
					gr.modify val(button$[x,5]),"text", " ON"
					tone 450,110
				endif
				if !background() then gr.render
			endif
		endif
		touched=0
	endif
until 0
timer.clear
gr.close
!end
exit

!===============================
clock_init:
!draw hour,minute,second
GR.COLOR 225, 255,255, 255, 1
gr.set.stroke 4
gr.line mpointer, mx,my,mx,my-(radius/100*90)
gr.set.stroke 6
gr.line hpointer, mx,my,mx,my-(radius/100*65)
GR.COLOR 255, 255,0,0, 1
gr.set.stroke 0
gr.line spointer, mx,my,mx,my-(radius/100*99)
return

!===============================
onTimer:
Time Year$, Month$, Day$, Hour$, Minute$, Second$, weekday

!seconds
grad=ti2degree(val(Second$))
y=sin(toradians(grad))*radius
x=y/tan(toradians(grad))
yn=my-y % add to absolute coordinates
xn=mx+x
gr.modify spointer,"x2", xn
gr.modify spointer,"y2", yn

!minutes
grad=ti2degree(val(Minute$))
y=sin(toradians(grad))*(radius/100*90)
x=y/tan(toradians(grad))
yn=my-y % add to absolute coordinates
xn=mx+x
gr.modify mpointer,"x2", xn
gr.modify mpointer,"y2", yn

!hour
h=val(Hour$)
if h>12 then h=h-12
diff=val(Minute$)/12-0.3 % take the hourpointer smooth to the next hour
grad=ti2degree(h*5+diff)
y=sin(toradians(grad))*(radius/100*65)
x=y/tan(toradians(grad))
yn=my-y % add to absolute coordinates
xn=mx+x
gr.modify hpointer,"x2", xn
gr.modify hpointer,"y2", yn

!date
gr.modify datum,"text",weekday$[weekday]+". "+Day$+"."+Month$+"."

if !background() then gr.render

if sound<>0 then
	if val(Minute$)=0 & val(Second$)=0 then %every hour short sound
		if val(Hour$)>7 & val(Hour$)<23 then
			tone 450,110
		endif
	endif
endif
timer.resume
return

!=========================================
cl_prepare:
!translate the time given into degrees for sin and tan functions
fn.def ti2degree(unit)
if unit>=0 & unit<15 then
	ggrad=90-unit*6
else %if sekunden >14 & sekunden<60 then
	ggrad=360-(unit-15)*6
endif
fn.rtn ggrad
fn.end

return

!==========================================
button_ini:

fn.def button_add(button$[],label$,posx,posy)
ok=0
for i=1 to 10
	!find free place up to 10 buttons
	if button$[i,2]="" then
		button$[i,1]="1"
		button$[i,2]=label$
		button$[i,3]=str$(posx)
		button$[i,4]=str$(posy)
		s$ = button$[i,2]
		gr.get.textbounds s$,l,t,r,b
		l=l-5
		t=t-5
		r=r+5
		b=b+5
		GR.COLOR 255, 255,0, 0, 1
		gr.rect x,l+posx,t+posy,r+posx,b+posy
		GR.COLOR 255, 255,255,255, 1
		gr.text.draw x,posx,posy,s$
		button$[i,5]=str$(x)
		button$[i,6]=str$(l+posx)
		button$[i,7]=str$(t+posy)
		button$[i,8]=str$(r+posx)
		button$[i,9]=str$(b+posy)
		ok=1
		f_n.break
	endif
next
fn.rtn ok
fn.end

fn.def button_check(button$[],x,y)
nbutton=0
for i=1 to 10
	if button$[i,2]<>"" then
		if x>=val(button$[i,6]) & x<=val(button$[i,8]) & y>=val(button$[i,7]) & y<=val(button$[i,9]) then
			nbutton=i
			f_n.break
		endif
	endif
next
fn.rtn nbutton
fn.end

dim button$[10,9] % 1 ... active/inactive, 2...text, 3... x, 4....y ,5 ... nText, 6/7/8/9 x1,y1,x2,y2
for i=1 to 10
	button$[i,1]="0"
	button$[i,2]=""
next
return

!===============
! Events
!===============
onBackKey:
home % go to background
back.resume