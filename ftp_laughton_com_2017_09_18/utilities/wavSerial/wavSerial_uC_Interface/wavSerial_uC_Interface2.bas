GOSUB              userfunctions
!GOTO xxx
baud             = 19200
z$               = "00000000"

patternTime      = 5000
dt1              = 100
dt2              = 1000
dt3              = 50



HEADSET            hstate, htype$, hmic ic
tic              = clock ()
clockDes         = tic + dtDes

DO

 GOSUB pattern1
 GOSUB pattern2
 GOSUB pattern3
 GOSUB pattern2

UNTIL 0

END



pattern1:
tt              = CLOCK() + patternTime
dtDes           = dt1
clockDes        = CLOCK() + dtDes
xx              = bin ("10000000")
DO
 xx             = shift (xx,-1)
 IF xx          > bin ("10000000") THEN xx = bin ("00000001")
 GOSUB            playItAgain
UNTIL             CLOCK() >= tt
RETURN

pattern2:
tt              = CLOCK()+ patternTime
dtDes           = dt2
clockDes        = CLOCK() + dtDes
xx              = 240
DO
 IF xx          = 240 THEN  xx = 15 ELSE  xx = 240
 GOSUB            playItAgain
UNTIL             CLOCK() >= tt
RETURN

pattern3:
tt              = CLOCK()+ patternTime
dtDes           = dt3
clockDes        = CLOCK() + dtDes
xx              = bin ("00000001")
DO
 xx             = shift (xx,1)
 IF xx          < bin ("00000001") THEN xx = bin ("10000000")
 GOSUB            playItAgain
UNTIL             CLOCK() >= tt
RETURN



playItAgain:

CALL              sendString$ ( CHR$(xx) ,baud, 1 )

hstateOld       = hstate
HEADSET           hstate, htype$, hmic
IF                (hstate & !hstateOld) THEN call setVolume( 15 )

PRINT             "        ";LEFT$(z$,8-LEN(BIN$(xx)))+bin $(xx);
PRINT             "   ";FORMAT$("%%%",xx);"  ";FORMAT$("%%%%", dt )

DO
UNTIL CLOCK()   >= clockDes
tic             =  CLOCK()
dt              =  tic - (clockDes-dtDes)
clockDes        =  tic + dtDes

RETURN


xxx:
CALL  wavSerModify$ ( chr $ (0) , 0)
END

!---------------------------------------------------------
userfunctions:


FN.DEF               sendString$ ( strToSend$ ,sRate, flagSendImm)
 
 wavOut$           = "serOutTmp.wav"
 
 numChan           = 1
 nSamp             = len ( strToSend$ ) *10
 nbytes            = nSamp * numChan 
 n$                = CHR$(0)
 x1                = 36 + nbytes
 x3                = sRate * numChan

 header$ = ~
 "RIFF" + CHR$(x1) + CHR$(x1/2^8) + CHR$(x1/2^16) + CHR$(x1/2^24)+ ~
 "WAVEfmt " + CHR$(16) +n$+n$+n$ +CHR$(1) + n$ + CHR$(numChan) + n$ + ~
 CHR$(sRate) + CHR$(sRate /2^8) + CHR$(sRate/2^16) + CHR$(sRate/2^24)+~
 CHR$(x3) + CHR$(x3/2^8) + CHR$(x3/2^16) + CHR$(x3/2^24) +~  CHR$(numChan) + n$ +CHR$(8)+n$ + "data"+ ~                              CHR$(nbytes)+CHR$(nbytes/2^8)+CHR$(nbytes/2^16)+CHR$(nbytes/2^24)

 FOR k             = 1 TO len ( strToSend$ )
  byteToSend$      = bin$ ( ascii ( mid$ ( strToSend$, k, 1 ) ))
  tmp$             = LEFT$("00000000",8-LEN(byteToSend$))+ byteToSend$
  data$            = data$ + chr$(255)
  FOR i            = 8 TO 1 STEP -1
   IF MID$( tmp$,i,1) = "1" THEN samp$=chr$(0) ELSE samp$=chr$(255)
   data$           = data$ + samp$
  NEXT
  data$            = data$ + chr$(0)
 NEXT
 
 BYTE.OPEN           w, fid, wavOut$
 BYTE.WRITE.BUFFER   fid,header$ + data$
 BYTE.CLOSE          fid

 IF  flagSendImm
  AUDIO.STOP
  AUDIO.LOAD          fid, wavOut$
  IF fid            > 1 THEN AUDIO.RELEASE fid-1
  AUDIO.PLAY          fid
 ENDIF

 FN.RTN              wavOut$
FN.END



FN.DEF setVolume( desSysVol )

 SYSTEM.OPEN
 SYSTEM.WRITE       "getprop persist.audio.sysvolume"
 DO
  SYSTEM.READ.READY rReady
 UNTIL              rReady
 SYSTEM.READ.LINE   sysVol$
 SYSTEM.CLOSE

 incDec          =  desSysVol - val ( sysVol$ )
 IF incDec       >  0  THEN keycode$ ="24" ELSE keycode$="25"
 cnt             =  abs (incDec)
 IF cnt          >  15 THEN cnt =  15
 FOR i=1 TO cnt
  SYSTEM.OPEN
  SYSTEM.WRITE      "input keyevent " + keycode$
  PAUSE             50
  SYSTEM.CLOSE
 NEXT
FN.END


RETURN
!---------------------------------------------------------





