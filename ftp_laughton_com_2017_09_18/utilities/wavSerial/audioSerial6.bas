! test it ------------------------------------------------

GOSUB               userfunctions


baudRate          = 19200
strToSend$        = "basic! for ever!"+chr$(10)
!strToSend$       = "U"


PRINT               "please wait ..."
tic               = clock ()
wavFile1$         = sendString$ ( strToSend$ , baudRate )
PRINT               clock ()-tic


SOUNDPOOL.OPEN      1
SOUNDPOOL.LOAD      fid1, wavFile1$
DO
  SOUNDPOOL.PLAY     nn, fid1, 0.9, 0.9,1,0,1
 PAUSE 500
UNTIL flagTouch
SOUNDPOOL.RESUME


END


ONCONSOLETOUCH:
flagTouch        = 1
CONSOLETOUCH.RESUME


!---------------------------------------------------------
userfunctions:

FN.DEF               sendString$ ( strToSend$ ,  sampRate )
 numChan           = 1
 wavOut$           = "serOutTmp.wav"
 bitsPerSamp       = 8
 flagInvert        = 1
 high              = 255
 low               = 0
 IF flagInvert       THEN swap high, low
 zeros$            = "00000000"

 FOR k             = 1 TO len ( strToSend$ )
  byteToSend$      = bin$ ( ascii ( mid$ ( strToSend$, k, 1 ) ))
  tmp$             = LEFT$(zeros$ , 8-LEN(byteToSend$))+ byteToSend$
  tmp2$            = ""
  FOR i            = 8 TO 1 STEP -1
   tmp2$           = tmp2$ + MID$(tmp$, i, 1)
  NEXT
  output$          = output$  +  "0" + tmp2$ + "1"
 NEXT

 nSamp             = len ( output$ ) 
 nbytes            = nSamp * numChan * bitsPerSamp/8
 PRINT               output$, nSamp, tmp$ , tmp2$

 BYTE.OPEN           w, fid, wavOut$
 BYTE.WRITE.BUFFER   fid, "RIFF"
 x                 = 36 + nbytes
 BYTE.WRITE.BUFFER   fid, CHR$(x)+CHR$(x/2^8)+CHR$(x/2^16)+CHR$(x/2^24)
 BYTE.WRITE.BUFFER   fid, "WAVEfmt "
 BYTE.WRITE.BUFFER   fid, CHR$(16) +CHR$(0) +CHR$(0) +CHR$(0)
 BYTE.WRITE.BUFFER   fid, CHR$(1) +CHR$(0)
 BYTE.WRITE.BUFFER   fid, CHR$(numChan) +CHR$(0)
 x                 = sampRate
 BYTE.WRITE.BUFFER   fid, CHR$(x)+CHR$(x/2^8)+CHR$(x/2^16)+CHR$(x/2^24)
 x                 = sampRate * numChan * bitsPerSamp/8
 BYTE.WRITE.BUFFER   fid, CHR$(x)+CHR$(x/2^8)+CHR$(x/2^16)+CHR$(x/2^24)
 BYTE.WRITE.BUFFER   fid, CHR$(numChan * bitsPerSamp/8 ) +CHR$(0)
 BYTE.WRITE.BUFFER   fid, CHR$(bitsPerSamp) +CHR$(0)
 BYTE.WRITE.BUFFER   fid, "data"
 x                 = nbytes
 BYTE.WRITE.BUFFER   fid, CHR$(x)+CHR$(x/2^8)+CHR$(x/2^16)+CHR$(x/2^24)

 tic=clock ()
  FOR t            = 1 TO nSamp
   IF MID$( output$ ,t, 1) = "1" THEN samp=high ELSE samp=low
   BYTE.WRITE.BYTE   fid , samp
  NEXT
 PRINT               clock ()-tic
 BYTE.CLOSE          fid
 FN.RTN              wavOut$
FN.END


RETURN
!---------------------------------------------------------





