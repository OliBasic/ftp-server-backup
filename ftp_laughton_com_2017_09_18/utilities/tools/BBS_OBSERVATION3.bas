REM Start of BASIC! Program
gosub checkfirst 
gosub loaduserarray 
gosub loadsec1 
goto main

checkfirst:
  file.exists olde,"BBS_User.txt"
    if olde=0 
      cls
      print
      print 
      print
      print
      print " New User Detected."
      print " Complete Setup."
      pause 3500
      cls
      print"YourNameHere*"
      print"Emailaddress*"
      print"ObserverID*"
      print"Discipline*"
      print"(Default)Location*"
      print"SAVE&CONTINUE*"
      console.save "BBS_User.txt"
      cls
    endif
  return 


edituser:
  cls
  msg$="edit user"
  select ch2,user$[],msg$
    if ch2<6 then
      input user$[ch2],inpu$,user$[ch2]
      inpu1$=word$(inpu$,1)
      user$[ch2]=inpu1$
  

      goto edituser
    endif
 cls
 for t =1 to 5
   print user$[t]+"*"
 next t
 print"SAVE&CONTINUE*"
 console.save "BBS_User.txt"
 cls


return


loaduserarray:
  array.load user$[],"","","","","",""
return 



getuser:
  grabfile s$,"BBS_User.txt"
  print s$
  tt=0
  ltr$=""
  wrd$=""
  l=len(s$)
    for t =1 to l
      ltr$=mid$(s$,t,1)
        if ltr$="*" then
          tt=tt+1
          user$[tt]=word$(wrd$,1)
          print wrd$+"-"
          wrd$=""
        else
          wrd$=wrd$+ltr$
        endif
    next t
 cls
return


newbbs:
  cls
msg$="New BBS"
ms$="newwbbs"

 gosub loadingcatlist 
 cl=0
 time year$, mnth$, day$,~
   hrs$,min$,sec$
 today$=mnth$+"/"+day$+"/"~
   +year$
 input "Observation Date",~
   obsdate$,today$
 input "Location",~
   loc$,user$[5]
msg$="Day of the Week"
input "NO. of People Observed",observed$,"2"
select ch3,wkd$[],msg$
input "Time of Day", tod$,"12:00"
msg$="Work Status"
select ch4,sts$[],msg$
newbbs1:
msg$="choose category"
select ch5,cate$[],msg$

  if ch5=1 then
    msg$="1 -body position"
    select ch5a,bodypos$[],msg$
    myvar$=bodypos$[ch5a]
    gosub addtocatlist 
  endif
 
  if ch5=2 then
    msg$="1 -body use/ergonomics"
    select ch5b,bodyuse$[],msg$
    myvar$=bodyuse$[ch5b]
    gosub addtocatlist 
  endif 
  
  if ch5=9 then
    goto newbbs2
  endif

  if ch5=3 then
    msg$="tools/equipment"
    select ch5c, tools$[],msg$
    myvar$=tools$[ch5c]
    gosub addtocatlist 
  endif

  if ch5=4 then
    msg$="procedures"
    select ch5d, proced$[],msg$
    myvar$=proced$[ch5d]
    gosub addtocatlist 
  endif 
    
 if ch5=5 then
    msg$="ppe"
    select ch5e, ppe$[],msg$
    myvar$=ppe$[ch5e]
    gosub addtocatlist 
  endif 

  if ch5=6 then
    msg$="environment"
    select ch5f, env$[],msg$
    myvar$=env$[ch5f]
    gosub addtocatlist 
  endif 

  if ch5=7 then
    msg$="office ergonomics"
    select ch5g, office$[],msg$
    myvar$=office$[ch5g]
    gosub addtocatlist 
  endif 

  if ch5=8 then
    msg$="other"
    select ch5h, other$[],msg$
    myvar$=other$[ch5h]
    gosub addtocatlist 
  endif 
goto newbbs1
newbbs2:
print "Obs. Date : "+obsdate$
print "Location : "+loc$ 
print "Observer ID : "+user$[3]
print "NO. of People Observed : "+observed$
print "Day of the Week : "+wkd$[ch3]
print "Time of Day ; "+tod$
print "Discipline : "+user$[4]
print "Work Status : "+ sts$[ch4]
msg$="specify safe/at risk"
select ch6,catlist$[],msg$
 for t =1 to cl
    msg$= catlist$[t] 
    
    select ch6a,safeunsafe$[],msg$
    
      print catlist$[t]
      print safeunsafe$[ch6a]
      print

  
  next t


pause 5000

return
addtocatlist:

 
cl=cl+1
catlist$[cl]=myvar$
return
loadsec1: 
array.load cate$[],~
   "1 -Body Position",~
   "2 -Body use/Ergonomics",~
   "3 -Tools & Equipment",~
   "4 -Procedures",~
   "5 -PPE",~
   "6 -Environment",~
   "7 -Office Ergonomics",~
   "9 -Other",~
   "Continue"

  array.load bodypos$[],~
   "1.1 -Line of fire",~
   "1.2 -Pinch points",~
   "1.3 -Eyes on path",~
   "1.4 -Eyes on task / hands",~
   "1.5 -Ascending / Descending"
   
 
 array.load bodyuse$[],~
   "2.1 -Lifting/Lowering/Twisting/Pushing/Pulling",~
   "2.2 -Overextended/Cramped/Posture",~
   "2.3 -Assistance",~
   "2.4 -Respose to Ergo Risk",~
   "2.5 -Grip Force",~
   "2.6 -Contact Stressors"

 array.load tools$[],~
   "3.1 -Selection/Condition/Use/Storage",~
   "3.2 -Vehicle/Selection/Condition/Use",~
   "3.3 -Barricades & Warnings"

 array.load proced$[],~
   "4.1 -Lock Out/Tag Out-Energy Isolation",~
   "4.2 -Confined space Entry",~
   "4.3 -Hot Work",~
   "4.4 -Communication of Hazards",~
   "4.5 -Pre/Post-job Inspection",~
   "4.6 -Excavation Work"

 array.load ppe$[],~
   "5.1 -Head",~
   "5.2 -Eyes and Face",~
   "5.3 -Hearing",~
   "5.4 -Respiratory",~
   "5.5 -Hands",~
   "5.6 -Body",~
   "5.7 -Fall",~
   "5.8 -Foot"

 array.load env$[],~
   "6.1 -Walking/Working Surfaces",~
   "6.2 -Housekeeping",~
   "6.3 -Lighting",~
   "6.4 -Temperature Extremes",~
   "6.5 -Industrial Hygiene",~
   "6.6 -Air Polution",~
   "6.7 -Land polution",~
   "6.8 -Water Polution",~
   "6.9 -Waste Management"

 array.load office$[],~
   "7.1 -Office Deviation"
 
 array.load other$[],~
   "9.1 -Other" 
array.load wkd$[] ,"Monday","Thuesday","Wednesday","Thursday","Friday","Saterday","Sunday"
array.load sts$[],"Planned","Urgent","Emergency","Shutdown"
array.load ma$[],"Setup","Info",~
"New BBS","History",~
"Submit to Database",~
"Share","Exit" 



array.load safeunsafe$[],"Safe","At-Risk"


return

loadingcatlist:

array.load catlist$[],"","","","","","",~
 "","","","","","","",""
return 
onerror:
 goto main

onbackkey:
print" back"
pause 500
  if msg$="main" then
    quit=1
  endif
  if ms$="newbbs"
     goto newbbs
  endif

back.resume  
main:
  gosub getuser

main1:
ch1=0
msg$="main"
select ch1,ma$[],msg$
  if ch1=1 then
    gosub edituser
  endif
 
  if ch1=3 then
    gosub newbbs
  endif
   
  if ch1=7 then
    goto ends
  endif
 


  goto main1

ends:

cls

