!!! Activity Log
This app records the time spent at doing an activity, and generates weekly reports of the log.

It uses BASIC! for the backend data management, and HTML with JQM for the frontend.

The UI is presenting several pages:
- The main page shows the activities and provides the main interactions.
- The report page shows the weekly logs and allows for e-mailing a log.
- The management page shows the activities and provides management actions.
- The options page shows the application options.
- The info pages shows basic instructions, information and credits.

Buttons and navigation bars provide for moving through the different pages.

= The main page
It presents a searchable list of all registered activities.
By tapping on the list an activity can be started or stopped.
Starting an activity stops the previously running one if any.
The running activity is highlighted and reported in status bar of all pages.
By tapping on the activity name in the status bar the current activity time is shown.

On this page the user can also add activities and edit their names.
While the list is loaded in alphabetical order
, adding or editin activities don't rearrange the sort order.
A reload of the list, hence its reordering, can be obtained by moving to other pages.

= The report page
It presents the aggregated weekly activity log, starting from the current one
, and providing a way to move to previous and following weeks.
The report presents the data grouped by day, and each activity log duration is aggregated.
The aggregated duration value is rounded according to the configuration (see options).
The send button allows to e-mail the displayes report in textual format.

= The management page
It presents presents a first section with the list of activities, providing for selecting one or more of them.
The user can then decide to merge or delete the selected ones.
The merge results in all data log being reassigned to one of the selected activities.
This activity is then renamed to include the name of all the other ones, and these are deleted.
The delete action first removes all related data log, and finally all selected activities.

The second section of the page provides action on the whole data log:
- for sending it via e-mail.
- for removing it completely.

= The options page
It presents the following application options.

- The timer interval in minutes.
This is the only function of the application that requires it to be running in order to work.
By setting it to 0, the timer is disabled.
By setting it to anything up to 60, it runs and a beep is emitted any given minutes.
This might be used as a sort of reminder or attention catcher.

- The recording threshold in seconds.
Any recording below the threshold is discarded.

- Default E-Mail to send reports and data to.

- The field separator.
A character (or more) used to separate the fields. It defaults to comma.

- The first day of your week.
The reports display weeks taking this into consideration.
It defaults to Monday, but in some instances it might be useful to have it set to a different day.

- The Duration interval and the rounding method.
The aggregated duration can be rounded to intervals of 1, 5, 15, or 30 minutes.
The rounding can be to the nearest interval, always up to, or always down to.
!!

BUNDLE.CREATE G % Contains all globally available values. Referenced as 1!

GOSUB FUNCTION_DEFITIONS

CALL CheckEnvironment()
CALL OpenDB()
CALL LoadSound()
CALL LoadAppHtml()

DO

	DO
		CALL ShowNotification()
		HTML.GET.DATALINK data$
		PAUSE 200
	UNTIL data$ <> ""

	type$ = LEFT$(data$, 4)
	data$ = MID$(data$, 5)

	IF type$ = "DAT:" THEN
		type$ = LEFT$(data$, 4)
		data$ = MID$(data$, 5)
	ENDIF

	IF type$ = "BAK:" THEN
		IF data$ = "1" THEN HTML.GO.BACK ELSE D_U.BREAK

	ELSEIF type$ = "LOD:" THEN
		CALL PushActivityList(data$)
		IF data$ = "main" THEN
			CALL PushRunningActivity()
			BUNDLE.CONTAIN G, "active_timer", active
			IF !active THEN
				CALL LoadOptions()
				CALL StartStopTimer()
			ENDIF
		ELSEIF data$ <> "mgmt" THEN
			PRINT "Unexpected datalink "; type$; data$
			D_U.BREAK
		ENDIF

	ELSEIF type$ = "TAP:" THEN
		CALL StartStopActivity(data$)

	ELSEIF type$ = "RUN:" THEN
		CALL ShowRunningTime(data$)

	ELSEIF type$ = "ADD:" THEN
		CALL AddActivity(data$)

	ELSEIF type$ = "SAV:" THEN
		CALL UpdateActivity(data$)
		CALL PushRunningActivity()

	ELSEIF type$ = "OPT:" THEN
		CALL UpdateOptions(data$)
		CALL LoadOptions()
		CALL StartStopTimer()

	ELSEIF type$ = "RPD:" THEN
		CALL PushReportData(VAL(data$))

	ELSEIF type$ = "RPS:" THEN
		CALL SendWeeklyReport(VAL(data$))

	ELSEIF type$ = "ALL:" THEN
		IF data$ = "Send" THEN
			CALL SendAllData()
		ELSEIF data$ = "Delete" THEN
			CALL DeleteAllData()
		ELSE
			PRINT "Unexpected datalink "; type$; data$
			D_U.BREAK
		ENDIF

	ELSEIF type$ = "UPD:" THEN
		IF data$ = "load" THEN
			CALL PushLastRecord()
		ELSE
			CALL UpdateLastRecord(data$)
		ENDIF

	ELSEIF type$ = "SMG:" THEN
		CALL MergeSelectedActivities(data$)

	ELSEIF type$ = "SDL:" THEN
		CALL DeleteSelectedActivities(data$)

	ELSEIF type$ = "POP:" THEN
		POPUP data$, 0, 0, 0

	ELSEIF type$ = "LNK:" THEN
		BROWSE data$

	ELSE
		PRINT "Unexpected datalink "; type$; data$
		D_U.BREAK

	ENDIF

UNTIL 0

CALL CloseAll()
END

FUNCTION_DEFITIONS:

FN.DEF CheckEnvironment()
! Check if running as APK.
! Set home accordingly.

	FILE.ROOT tmp$
	BUNDLE.PUT 1, "isAPK", IS_IN("rfo-basic", tmp$) = 0
	BUNDLE.GET 1, "isAPK", IS_APK
	BUNDLE.PUT 1, "assets", "activitylog/"
	IF IS_APK THEN
		BUNDLE.PUT 1, "home", ""
		FILE.EXISTS found, home$ + "version.txt"
		IF !found THEN
			TEXT.OPEN w, tmp, home$ + "version.txt"
			TEXT.WRITELN tmp, "1.0.0"
			TEXT.CLOSE tmp
		ENDIF
		FILE.EXISTS found, "../databases"
		IF !found THEN FILE.MKDIR "../databases"
	ELSE
		BUNDLE.PUT 1, "home", "activitylog/"
	ENDIF
FN.END

FN.DEF OpenDB()
! Open and create the DB with default values.

	SQL.OPEN DB, "activitylog.db"
	BUNDLE.PUT 1, "DB", DB

	SQL.QUERY cur, DB, "sqlite_master", "name", "type='table' AND name='activity'"
	SQL.QUERY.LENGTH found, cur
	IF !found THEN
		SQL.NEW_TABLE DB, "activity", "name"
		SQL.INSERT DB, "activity", "name", "Idle"
		SQL.INSERT DB, "activity", "name", "Administration"
		SQL.INSERT DB, "activity", "name", "Meeting"
	ENDIF

	SQL.QUERY cur, DB, "sqlite_master", "name", "type='table' AND name='log'"
	SQL.QUERY.LENGTH found, cur
	IF !found THEN
		SQL.NEW_TABLE DB, "log", "activity", "start_time", "end_time", "duration"
	ENDIF

	SQL.QUERY cur, DB, "sqlite_master", "name", "type='table' AND name='option'"
	SQL.QUERY.LENGTH found, cur
	IF !found THEN
		SQL.NEW_TABLE DB, "option", "name", "value"
		SQL.INSERT DB, "option", "name", "timer_interval", "value", "0"
		SQL.INSERT DB, "option", "name", "record_threshold", "value", "60"
		SQL.INSERT DB, "option", "name", "report_weekstart", "value", "2"
		SQL.INSERT DB, "option", "name", "report_interval", "value", "15"
		SQL.INSERT DB, "option", "name", "report_round", "value", "round"
		SQL.INSERT DB, "option", "name", "default_email", "value", ""
		SQL.INSERT DB, "option", "name", "field_separator", "value", ","
		SQL.INSERT DB, "option", "name", "show_notify", "value", "No"
	ENDIF

	! This option has been added in version 1.03
	SQL.QUERY cur, DB, "option", "_id", "name='show_notify'"
	SQL.NEXT eoq, cur, id$
	IF eoq THEN
		SQL.INSERT DB, "option", "name", "show_notify", "value", "No"
	ENDIF

	Fn.RTN DB
FN.END

FN.DEF LoadOptions()
! Load the options and push them to html.

	BUNDLE.GET 1, "DB", DB
	SQL.QUERY cur, DB, "option", "_id, name , value"
	opts$ = ""
	DO
		SQL.NEXT eoq, cur, id$, name$, value$
		IF !eoq THEN
			BUNDLE.PUT 1, "opt." + name$, value$
			IF LEN(opts$) > 0 THEN opts$ += ", "
			opts$ += "'" + name$ + "':'" + value$ + "'"
		ENDIF
	UNTIL eoq
	HTML.LOAD.URL "javascript:doSetOptions({" + opts$ + "})"
FN.END

FN.DEF StartStopTimer()
! Start the timer.

	BUNDLE.CONTAIN 1, "active_timer", active
	IF active THEN
		BUNDLE.GET 1, "active_timer", active_timer
	ELSE
		active_timer = 0
	ENDIF

	BUNDLE.GET 1, "opt.timer_interval", tmp$
	tmp = VAL(tmp$)

	IF active_timer <> tmp THEN
		IF active_timer THEN TIMER.CLEAR
		IF tmp > 0 THEN
			tmp = tmp * 60 * 1000
			TIMER.SET tmp
		ENDIF
		BUNDLE.PUT 1, "active_timer", tmp
	ENDIF
FN.END

FN.DEF LoadSound()
! Load the sound for the timer.

	BUNDLE.GET 1, "home", home$
	SOUNDPOOL.OPEN 1
	SOUNDPOOL.LOAD tmp, home$ + "beep.mp3"
	BUNDLE.PUT 1, "sound.beep", tmp
FN.END

FN.DEF LoadAppHtml()
! Load the html part.

	BUNDLE.GET 1, "assets", home$
	POPUP "Loading...", 0, 0, 0
	HTML.OPEN
	HTML.LOAD.URL home$ + "activitylog.html"
FN.END

FN.DEF PushActivityList(data$)
! Push the activity list to html.

	BUNDLE.GET 1, "DB", DB
	SQL.QUERY cur, DB, "activity", "_id, name", "", "name ASC"
	DO
		SQL.NEXT eoq, cur, id$, name$
		IF !eoq THEN HTML.LOAD.URL "javascript:doAddActivity('" + data$ + "', " + id$ + ", '" + name$ + "')"
	UNTIL eoq
FN.END

FN.DEF PushReportData(offset_week)
! Collect and push to html the week report data.
! The selected week is the current - offset_week.

	BUNDLE.GET 1, "DB", DB
	BUNDLE.GET 1, "opt.report_weekstart", week_start$
	wkst = VAL(week_start$)

	! Calculate the start of the week to report.
	now = TIME()
	TIME now, Year$, Month$, Day$, Hour$, Minute$, Second$, WeekDay, isDST
	Range = (wkst - WeekDay)
	IF wkst > WeekDay THEN Range -= 7
	RepDay = now + Range * 24 * 60 * 60 * 1000
	TIME RepDay, Year$, Month$, Day$, Hour$, Minute$, Second$, WeekDay, isDST
	RepDay = TIME(Year$, Month$, Day$, 0, 0, 0)
	IF offset_week THEN RepDay += offset_week * 7 * 24 * 60 * 60 * 1000

	! Prepare all dates to be reported.
	BUNDLE.CREATE Dates

	Filter$ = "DATETIME(start_time/1000, 'unixepoch') >= DATETIME(" + STR$(RepDay/1000) + ", 'unixepoch')"
	FOR i = 1 TO 7
		TIME RepDay, Year$, Month$, Day$, Hour$, Minute$, Second$, WeekDay, isDST
		Date$ = Year$ + "/" + Month$ + "/" + Day$
		BUNDLE.CREATE DayActivities
		BUNDLE.PUT Dates, Date$, DayActivities
		RepDay += 24 * 60 * 60 * 1000
	NEXT
	Filter$ += " AND DATETIME(start_time/1000, 'unixepoch') < DATETIME(" + STR$(RepDay/1000) + ", 'unixepoch')"

	! Collect activities in the date range of the selected week.
	SQL.QUERY cur, DB, "log", "activity, start_time, duration", Filter$
	DO
		SQL.NEXT eoq, cur, act_id$, start$, duration$
		IF !eoq & duration$ <> "" THEN
			TIME VAL(start$), Year$, Month$, Day$, Hour$, Minute$, Second$, WeekDay, isDST
			Date$ = Year$ + "/" + Month$ + "/" + Day$
			BUNDLE.GET Dates, Date$, DayActivities
			BUNDLE.CONTAIN DayActivities, act_id$, found
			IF !found
				BUNDLE.PUT DayActivities, act_id$, VAL(duration$)
			ELSE
				BUNDLE.GET DayActivities, act_id$, tmp
				BUNDLE.PUT DayActivities, act_id$, tmp + VAL(duration$)
			ENDIF
		ENDIF
	UNTIL eoq

	! Retrieve the options and the activity names.
	BUNDLE.GET 1, "opt.report_interval", rep_interval$
	BUNDLE.GET 1, "opt.report_round", method$
	interval = VAL(rep_interval$)

	BUNDLE.CREATE Activities
	SQL.QUERY cur, DB, "activity", "_id, name"
	DO
		SQL.NEXT eoq, cur, act_id$, act_name$
		IF !eoq THEN BUNDLE.PUT Activities, act_id$, act_name$
	UNTIL eoq

	! Sort the dates.
	BUNDLE.KEYS Dates, dk
	LIST.TOARRAY dk, dts$[]
	ARRAY.SORT dts$[]
	ARRAY.REVERSE dts$[]

	BUNDLE.GET 1, "opt.field_separator", fs$

	reportSubject$ = "Activity log for week ending on " + dts$[1]
	reportBody$ = "\"DATE\"" + fs$ + "\"ACTIVITY\"" + fs$ + "\"DURATION\"\n"

	dates_ja$ = "" % sorted array of dates.
	daily_ja$ = "" % hash of dates -> hours.
	daydo_ja$ = "" % hash of dates -> hash of activities -> hours
	FOR i = 1 TO 7
		IF i > 1 THEN dates_ja$ += ", "
		dates_ja$ += "'" + dts$[i] + "'"
		BUNDLE.GET Dates, dts$[i], DayActivities
		BUNDLE.KEYS DayActivities, dk
		LIST.SIZE dk, cnt
		day_tot = 0
		IF i > 1 THEN daydo_ja$ += ", "
		daydo_ja$ += "'" + dts$[i] + "':{"
		FOR j = 1 TO cnt
			LIST.GET dk, j, act_id$
			BUNDLE.GET DayActivities, act_id$, tmp
			BUNDLE.GET Activities, act_id$, act_name$
			tmp = DurationRound(tmp, interval, method$) % Round as by options.
			tmp = ROUND(tmp * 100) / 100 % Ensure to have at most 2 decimals.
			IF j > 1 THEN daydo_ja$ += ", "
			daydo_ja$ += "'" + act_name$ + "':" + STR$(tmp)
			day_tot += tmp
			reportBody$ += dts$[i] + fs$ + "\"" + act_name$ + "\"" + fs$ + STR$(tmp) + "\n"
		NEXT
		daydo_ja$ += "}"
		IF i > 1 THEN daily_ja$ += ", "
		day_tot = ROUND(day_tot * 100) / 100 % Ensure to have at most 2 decimals.
		daily_ja$ += "'" + dts$[i] + "':" + STR$(day_tot)
	NEXT

	BUNDLE.PUT 1, "report.subject", reportSubject$
	BUNDLE.PUT 1, "report.body", reportBody$

	ARRAY.DELETE dts$[]

	HTML.LOAD.URL "javascript:doReportData([" + dates_ja$ + "], {" + daily_ja$ + "}, {" + daydo_ja$ + "})"
FN.END

FN.DEF SendWeeklyReport(offset_week)
! Send email with weekly report.
! Input is ignored.

	BUNDLE.GET 1, "opt.default_email", to$
	BUNDLE.GET 1, "report.subject", subject$
	BUNDLE.GET 1, "report.body", body$

	EMAIL.SEND to$, subject$, body$
FN.END

FN.DEF SendAllData()
! Send all log data via e-mail.

	BUNDLE.GET 1, "DB", DB

	BUNDLE.CREATE Activities
	SQL.QUERY cur, DB, "activity", "_id, name"
	DO
		SQL.NEXT eoq, cur, act_id$, act_name$
		IF !eoq THEN BUNDLE.PUT Activities, act_id$, act_name$
	UNTIL eoq

	BUNDLE.GET 1, "opt.field_separator", fs$
	body$ = "\"ACTIVITY\"" + fs$ + "\"START\"" + fs$ + "\"END\"" + fs$ + "\"DURATION\"\n"

	SQL.QUERY cur, DB, "log", "activity, start_time, end_time, duration", ""
	DO
		SQL.NEXT eoq, cur, act_id$, start$, end$, duration$
		IF !eoq & end$ <> "" THEN
			TIME VAL(start$), Year$, Month$, Day$, Hour$, Minute$, Second$, WeekDay, isDST
			start$ = Year$ + "/" + Month$ + "/" + Day$ + " " + Hour$ + ":" + Minute$ + ":" + Second$
			TIME VAL(end$), Year$, Month$, Day$, Hour$, Minute$, Second$, WeekDay, isDST
			LET end$ = Year$ + "/" + Month$ + "/" + Day$ + " " + Hour$ + ":" + Minute$ + ":" + Second$
			BUNDLE.GET Activities, act_id$, act_name$
			body$ += "\"" + act_name$ + "\"" + fs$ + start$ + fs$ + end$ + fs$ + duration$ + "\n"
		ENDIF
	UNTIL eoq

	BUNDLE.GET 1, "opt.default_email", to$
	EMAIL.SEND to$, "Activity log data", body$
FN.END

FN.DEF DeleteAllData()
! Remove all log data.

	BUNDLE.GET 1, "DB", DB
	SQL.DELETE DB, "log", ""
	POPUP "All log data deleted.", 0, 0, 0
FN.END

FN.DEF PushLastRecord()
! Find and return the last complete record for possible update.

	BUNDLE.GET 1, "DB", DB
	SQL.QUERY cur, DB, "log", "_id, activity, duration", "end_time NOT NULL", "_id DESC LIMIT 1"
	SQL.NEXT eoq, cur, log$, id$, duration$
	IF !eoq THEN
		tmp = VAL(duration$) / 3600
		tmp = ROUND(tmp * 10000) / 10000 % Ensure to have at most 4 decimals.
		SQL.QUERY cur, DB, "activity", "name", "_id = " + id$
		SQL.NEXT eoq, cur, name$
		IF !eoq THEN
			HTML.LOAD.URL "javascript:doEditLastRecord('" + name$ + "', " + STR$(tmp) + ")"
		ELSE
			POPUP "Unexpected error loading data.", 0, 0, 0
		ENDIF
	ELSE
		POPUP "No recorded data found.", 0, 0, 0
	ENDIF
FN.END

FN.DEF UpdateLastRecord(data$)
! Update the last complete record with the value in data$.

	BUNDLE.GET 1, "DB", DB
	SQL.QUERY cur, DB, "log", "_id", "end_time NOT NULL", "_id DESC LIMIT 1"
	SQL.NEXT eoq, cur, id$
	IF !eoq THEN
		dur$ = STR$(VAL("0" + data$) * 3600)
		SQL.UPDATE DB, "log", "duration", dur$: "_id='" + id$ + "'"
	ENDIF
	POPUP "Last record updated.", 0, 0, 0
FN.END

FN.DEF MergeSelectedActivities(data$)
! Merge the actvities for which the ids are provided in data$.

	BUNDLE.GET 1, "DB", DB

	SPLIT ids$[], data$, ","
	ARRAY.SORT ids$[]
	ARRAY.LENGTH count, ids$[]

	first$ = ids$[1]
	SQL.QUERY cur, DB, "activity", "name", "_id='" + first$ + "'"
	SQL.NEXT eoq, cur, new_name$

	FOR i = 2 TO count
		SQL.UPDATE DB, "log", "activity", first$: "activity='" + ids$[i] + "'"
		SQL.QUERY cur, DB, "activity", "_id, name", "_id='" + ids$[i] + "'"
		SQL.NEXT eoq, cur, act_id$, act_name$
		new_name$ += "/" + act_name$
		SQL.DELETE DB, "activity", "_id = '" + ids$[i] + "'"
	NEXT

	SQL.UPDATE DB, "activity", "name", new_name$: "_id='" + first$ + "'"

	POPUP "Activities merged.", 0, 0, 0
	CALL PushActivityList("mgmt")
FN.END

FN.DEF DeleteSelectedActivities(data$)
! Delete the actvities for which the ids are provided in data$.

	BUNDLE.GET 1, "DB", DB

	SPLIT ids$[], data$, ","
	ARRAY.LENGTH count, ids$[]

	FOR i = 1 TO count
		SQL.DELETE DB, "log", "activity = '" + ids$[i] + "'"
		SQL.DELETE DB, "activity", "_id = '" + ids$[i] + "'"
	NEXT

	POPUP "Activities deleted.", 0, 0, 0
	CALL PushActivityList("mgmt")
FN.END

FN.DEF PushRunningActivity()
! Check the running activity and push it to html.

	BUNDLE.GET 1, "DB", DB
	SQL.QUERY cur, DB, "log", "activity", "end_time isnull"
	SQL.NEXT eoq, cur, id$
	IF eoq THEN
		HTML.LOAD.URL "javascript:doUpdateStatus('', '')"
	ELSE
		HTML.LOAD.URL "javascript:doUpdateStatus('" + id$ + "', '+')"
	ENDIF
FN.END

FN.DEF StartStopActivity(data$)
! Stop running activity if any, and starts the new one if any.
! Delete running activity if below threshold.

	BUNDLE.GET 1, "DB", DB
	t = TIME()
	now$ = STR$(t)
	SQL.QUERY cur, DB, "log", "_id, activity, start_time", "end_time isnull"
	SQL.NEXT eoq, cur, log$, id$, start$
	IF !eoq THEN
		BUNDLE.GET 1, "opt.record_threshold", tmp$
		threshold = VAL(tmp$)
		duration = ROUND((t - VAL(start$)) / 1000)
		IF duration < threshold THEN
			POPUP "Duration below threshold. Record not saved.", 0, 0, 0
			SQL.DELETE DB, "log", "_id='" + log$ + "'"
		ELSE
			dur$ = STR$(duration)
			SQL.UPDATE DB, "log", "end_time", now$, "duration", dur$: "_id='" + log$ + "'"
		ENDIF
		HTML.LOAD.URL "javascript:doUpdateStatus('" + id$ + "', '-')"
	ENDIF
	IF data$ <> id$ THEN
		SQL.INSERT DB, "log", "activity", data$, "start_time", now$
		HTML.LOAD.URL "javascript:doUpdateStatus('" + data$ + "', '+')"
		SQL.QUERY cur, DB, "activity", "name", "_id='" + data$ + "'"
		SQL.NEXT eoq, cur, name$
	ENDIF

	BUNDLE.PUT 1, "notified", 0
FN.END

FN.DEF ShowNotification()
! Check for the running activity and show the notification in statusbar.

	BUNDLE.CONTAIN 1, "opt.show_notify", loaded
	IF !loaded THEN FN.RTN 0

	BUNDLE.GET 1, "opt.show_notify", tmp$
	IF tmp$ = "No" THEN FN.RTN 0

	BUNDLE.CONTAIN 1, "notified", notified
	IF notified THEN BUNDLE.GET 1, "notified", notified

	BUNDLE.GET 1, "opt.record_threshold", tmp$
	threshold = VAL(tmp$) * 1000
	IF notified & CLOCK() - notified < threshold THEN FN.RTN 0

	BUNDLE.GET 1, "DB", DB
	SQL.QUERY cur, DB, "log", "activity", "end_time isnull"
	SQL.NEXT eoq, cur, id$
	IF eoq THEN
		rec$ = "Not recording"
	ELSE
		SQL.QUERY cur, DB, "activity", "name", "_id='" + id$ + "'"
		SQL.NEXT eoq, cur, name$
		rec$ = "Recording " + name$
	ENDIF
	NOTIFY "Activity Log", rec$, rec$, 0

	BUNDLE.PUT 1, "notified", CLOCK()

	FN.RTN 1
FN.END

FN.DEF ShowRunningTime(data$)
! Calculate and display the running activity time.

	BUNDLE.GET 1, "DB", DB
	t = TIME()
	now$ = STR$(t)
	SQL.QUERY cur, DB, "log", "_id, activity, start_time", "end_time isnull"
	SQL.NEXT eoq, cur, log$, id$, start$
	duration = 0
	IF !eoq THEN
		duration = ROUND((t - VAL(start$)) / 1000)
	ENDIF
	IF duration > 60 THEN
		minutes = duration / 60
		min = FLOOR(minutes)
		sec = ROUND((minutes - min) * 60)
		IF min > 60 THEN
			hours = min / 60
			h = FLOOR(hours)
			min = ROUND((hours - h) * 60)
			tt$ = STR$(h) + FORMAT$(":%#", min) + FORMAT$(":%#", sec)
		ELSE
			tt$ = "00" + FORMAT$(":%#", min) + FORMAT$(":%#", sec)
		ENDIF
	ELSE
		tt$ = "00:00" + FORMAT$(":%#", ROUND(duration))
	ENDIF
	POPUP "Current activity running for " + REPLACE$(tt$, ".0", ""), 0, 0, 0
FN.END

FN.DEF AddActivity(data$)
! Add a new activity, and push it to html.

	BUNDLE.GET 1, "DB", DB
	SQL.INSERT DB, "activity", "name", data$
	SQL.QUERY cur, DB, "activity", "_id, name", "", "_id DESC LIMIT 1"
	SQL.NEXT eoq, cur, id$, name$
	HTML.LOAD.URL "javascript:doAddActivity('main', " + id$ + ", '" + name$ + "')"
FN.END

FN.DEF UpdateActivity(data$)
! Updates the name of the selected activity.
! Input is in the form "id\nname".

	BUNDLE.GET 1, "DB", DB
	SPLIT parts$[], data$, "\n"
	SQL.UPDATE DB, "activity", "name", parts$[2]: "_id='" + parts$[1] + "'"
	UNDIM parts$[]
FN.END

FN.DEF UpdateOptions(data$)
! Update options.
! Input is in the form "name1=value1\nname2=value2\n...".

	BUNDLE.GET 1, "DB", DB
	SPLIT parts$[], data$, "\n"
	ARRAY.LENGTH count, parts$[]
	FOR i = 1 TO count
		eq = IS_IN("=", parts$[i])
		name$ = LEFT$(parts$[i], eq - 1)
		value$ = MID$(parts$[i], eq + 1)
		SQL.UPDATE DB, "option", "value", value$: "name='" + name$ + "'"
	NEXT
	UNDIM parts$[]
FN.END

FN.DEF DurationRound(seconds, interval, method$)
! Adjust duration to the interval, by means of the selected method.
! Used at time of reporting on aggregated values.

	hours = seconds / 60 / 60
	h = FLOOR(hours)
	m = (hours - h) * 60

	IF method$ = "ceil" THEN
		FN.RTN h + CEIL(m / interval) * interval / 60
	ELSEIF method$ = "floor" THEN
		FN.RTN h + FLOOR(m / interval) * interval / 60
	ELSE
		FN.RTN h + ROUND(m / interval) * interval / 60
	ENDIF
FN.END

FN.DEF CloseAll()
! Close all open handles.

	POPUP "Closing...", 0, 0, 0
	TIMER.CLEAR
	SOUNDPOOL.RELEASE
	HTML.CLOSE
	BUNDLE.GET 1, "DB", DB
	SQL.CLOSE DB
	BUNDLE.GET 1, "isAPK", IS_APK
	IF IS_APK THEN EXIT
FN.END

RETURN

ONTIMER:
! Play the sound every given amount of minutes - see StartStopTimer().

	BUNDLE.GET G, "sound.beep", snd
	SOUNDPOOL.PLAY tmp, snd, 0.99, 0.99, 1, 0, 1
TIMER.RESUME
