! System info v0.4 July 2013
! (c) [Antonis] (tony_gr)
! This program is free for personal use
! Use it at your own risk

! added battery info
! added processor info

flagOri = 1
refW = 800
refH = 1232
IF !flagOri THEN SWAP refW, refH
centW = refW/2
centH = refH/2
GR.OPEN 255,20,0,0,0,flagOri
PAUSE 1000
GR.SCREEN curW, curH
scaW = curW / refW
scaH = curH / refH
GR.SCALE scaW , scaH
GR.RENDER
k=200
maxTout=5000

GR.TEXT.SIZE 30
GR.COLOR 255,255,55,255,1
GR.TEXT.DRAW LOGO,50,1200,"System Info v0.4 by...[Antonis]  (tony_gr)"
GR.COLOR 255,255,255,25,1
GR.TEXT.DRAW Title,50,50,"Retrieving System Information, pls wait..."
GR.COLOR 255,255,255,255,1

! starting with sensors
GR.TEXT.DRAW title,50,k,"Gathering sensor info..."
GR.RENDER
SENSORS.LIST list$[]
ARRAY.LENGTH SensorsL, list$[]
s$=" #"
s$=s$+"[SENSORS]#"
FOR i = 1 TO SensorsL
 s$=s$+list$[i]+"#"
NEXT i
s$=s$+"#"
! GPS
k=k+50
GR.TEXT.DRAW tx,50,k,"Gathering gps info..."
GR.RENDER
PAUSE 200
s$=s$+"[GPS]#"
GPS.OPEN
PAUSE 200
GPS.PROVIDER g$
s$=s$+"provider: "+g$+"#"
GPS.ACCURACY g
g$=STR$(g)
s$=s$+"accuracy: "+g$+"#"
GPS.LATITUDE g
g$=STR$(g)
s$=s$+"latitude: "+g$+"#"
GPS.LONGITUDE g
g$=STR$(g)
s$=s$+"longitude: "+g$+"#"
GPS.ALTITUDE g
g$=STR$(g)
s$=s$+"altitude: "+g$+"#"
GPS.BEARING g
g$=STR$(g)
s$=s$+"bearing: "+g$+"#"
GPS.SPEED g
g$=STR$(g)
s$=s$+"speed: "+g$+"#"
GPS.TIME g
g$=STR$(g)
s$=s$+"time: "+g$+"##"
GPS.CLOSE

! device
k=k+50
GR.TEXT.DRAW tx,50,k,"Gathering device info..."
GR.RENDER
s$=s$+"[DEVICE]#"
DEVICE dv$
dv$=REPLACE$(dv$,CHR$(10),"#")
s$=s$+dv$+"#"
MYPHONENUMBER ph$
s$=s$+"phone number: "+ph$+"#"
TIMEZONE.GET g$
s$=s$+"TimeZone: "+g$+"##"

! screen
k=k+50
GR.TEXT.DRAW tx,50,k,"Gathering screen info..."
GR.RENDER
s$=s$+"[SCREEN]#"
GR.SCREEN w,h,dpi
w$="Program Screen width: "+REPLACE$(STR$(w),".0","")+"#"
h$="Program Screen height: "+REPLACE$(STR$(h),".0","")+"#"
dpi$="Dots per inch: "+REPLACE$(STR$(dpi),".0","")+"#"
s$=s$+w$+h$+dpi$
! *********
SYSTEM.OPEN
! *********
PAUSE 200
! screen modes
SYSTEM.WRITE "cat /sys/class/graphics/fb0/modes"
s$=s$+"Real resolution: "
GOSUB getSystemData
SYSTEM.WRITE "cat /sys/class/graphics/fb0/bits_per_pixel"
s$=s$+"color depth in bits: "
GOSUB getSystemData
SYSTEM.WRITE "cat /sys/class/graphics/fb0/name"
s$=s$+"name: "
GOSUB getSystemData
s$=s$+"#"

! net info
k=k+50
GR.TEXT.DRAW tx,50,k,"Gathering net info..."
GR.RENDER
s$=s$+"[NET INFO]#"
SOCKET.MYIP g$
s$=s$+"Local IP: "+g$+"#"
IF g$<>"" THEN
 GRABURL ss$,"http://www.findmyip.org/"
 a1=IS_IN("IP Address:",ss$ )
 a1=IS_IN("<b>" ,ss$,a1)
 a2=IS_IN("</b>" ,ss$,a1)
 ip$=MID$(ss$,a1+3,a2-a1-3)
 s$=s$+"Internet IP: "+ip$+"#"
ELSE
 s$=s$+"Internet IP: no internet data." + "#"
ENDIF
SYSTEM.WRITE "netcfg"
GOSUB waitforData
IF !ttimedOut THEN
 s$=s$+"$$"
 DO
  SYSTEM.READ.LINE l$
  l$=REPLACE$(l$,CHR$(10),"")
  l$=REPLACE$(l$,"  ","")
  s$=s$+l$+"#"
  SYSTEM.READ.READY ready
 UNTIL !ready
ENDIF
s$=s$+"@@#"
PAUSE 200

! memory
k=k+50
GR.TEXT.DRAW tx,50,k,"Gathering memory info..."
GR.RENDER
s$=s$+"[MEMORY]#"
s$=s$+"Heap growth limit: "
SYSTEM.WRITE "getprop dalvik.vm.heapgrowthlimit"
GOSUB getSystemData
PAUSE 100
s$=s$+"Heap Memory Size: "
SYSTEM.WRITE "getprop dalvik.vm.heapsize"
GOSUB getSystemData
PAUSE 100
s$=s$+"Heap start size: "
SYSTEM.WRITE "getprop dalvik.vm.heapstartsize"
GOSUB getSystemData
PAUSE 100
SYSTEM.WRITE "cat /proc/meminfo"
GOSUB getSystemData
s$=s$+"#"

! file system
k=k+50
GR.TEXT.DRAW tx,50,k,"Gathering file system info..."
GR.RENDER
s$=s$+"[FILE SYSTEM]#"
SYSTEM.WRITE "df"
GOSUB getSystemData
s$=s$+"@@#"
PAUSE 200

! battery
k=k+50
GR.TEXT.DRAW tx,50,k,"Gathering battery info..."
GR.RENDER
s$=s$+"[BATTERY]#"
s$=s$+"Capacity %: "
SYSTEM.WRITE "cat /sys/class/power_supply/battery/capacity"
GOSUB getSystemData
PAUSE 100
s$=s$+"Health: "
SYSTEM.WRITE "cat /sys/class/power_supply/battery/health"
GOSUB getSystemData
PAUSE 100
s$=s$+"Status: "
SYSTEM.WRITE "cat /sys/class/power_supply/battery/status"
GOSUB getSystemData
PAUSE 100
s$=s$+"Technology: "
SYSTEM.WRITE "cat /sys/class/power_supply/battery/technology"
GOSUB getSystemData
PAUSE 100
s$=s$+"Temperature (F): "
SYSTEM.WRITE "cat /sys/class/power_supply/battery/temp"
GOSUB getSystemData
PAUSE 100
s$=s$+"Type: "
SYSTEM.WRITE "cat /sys/class/power_supply/battery/type"
GOSUB getSystemData
PAUSE 100
s$=s$+"Voltage (mV): "
SYSTEM.WRITE "cat /sys/class/power_supply/battery/voltage_now"
GOSUB getSystemData
PAUSE 100
s$=s$+"#"

! CPU
k=k+50
GR.TEXT.DRAW tx,50,k,"Gathering processor info..."
GR.RENDER
s$=s$+"[PROCESSOR]#"
s$=s$+"@@"
SYSTEM.WRITE "cat /proc/cpuinfo"
GOSUB waitforData
IF !ttimedOut THEN
 DO
  SYSTEM.READ.LINE l$
  a1=IS_IN("Processor",l$)
  a2=IS_IN("CPU architecture",l$)
  a3=IS_IN("Hardware",l$)
  IF a1>0 | a2>0 | a3>0 THEN s$=s$+l$+"#"
  SYSTEM.READ.READY ready
 UNTIL !ready
 FILE.DIR "../../../../../sys/devices/system/cpu/", FileArray$[]
 ARRAY.LENGTH ll,FileArray$[]
 count=0
 FOR i=1 TO ll
  k$=FileArray$[i]
  a=IS_IN("cpu",k$)
  IF RIGHT$(k$,3)="(d)" & a>0 & LEN(k$)<9 THEN count=count+1
 NEXT i
 s$=s$+"Number of cores: "+ REPLACE$(STR$(count),".0","")+"#"
 s$=s$+"Max processor frequency (Hz): "
 SYSTEM.WRITE "cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq"
 GOSUB getSystemData
 s$=s$+"Min processor frequency (Hz): "
 SYSTEM.WRITE "cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq"
 GOSUB getSystemData
ENDIF
s$=s$+"#"
PAUSE 100


! disk stats
k=k+50
GR.TEXT.DRAW tx,50,k,"Gathering disk statistics info..."
GR.RENDER
s$=s$+"[DISK STATISTICS]#"
s$=s$+"$$"
SYSTEM.WRITE "dumpsys diskstats"
GOSUB getSystemData
s$=s$+"@@#"
PAUSE 200

! harware-software
k=k+50
GR.TEXT.DRAW tx,50,k,"Gathering harware-software info..."
GR.RENDER
aux$=""
s$=s$+"[HARDWARE-SOFTWARE FEATURES]#"
SYSTEM.WRITE "pm list features"
GOSUB waitForData
IF !ttimedOut THEN
 DO
  SYSTEM.READ.LINE l$
  aux$=aux$+l$+"#"
  SYSTEM.READ.READY ready
 UNTIL !ready
 aux$=REPLACE$(aux$,CHR$(10),"")
 aux$=REPLACE$(aux$,"feature:android.","")
 aux$=REPLACE$(aux$,"feature:","")
 aux$=REPLACE$(aux$,"hardware.","hardware: ")
 aux$=REPLACE$(aux$,"software.","software: ")
 s$=s$+aux$
ENDIF
s$=s$+"#"

! up-time
k=k+50
GR.TEXT.DRAW tx,50,k,"Gathering up-time info..."
GR.RENDER
s$=s$+"[UPTIME SINCE LAST BOOT]#"
SYSTEM.WRITE "uptime"
GOSUB waitForData
IF !ttimedOut THEN
 DO
  SYSTEM.READ.LINE l$
  l$=REPLACE$(l$,", ","#")
  s$=s$+l$+"#"
  SYSTEM.READ.READY ready
 UNTIL !ready
ENDIF
s$=s$+"#"

! system properties
k=k+50
GR.TEXT.DRAW tx,50,k,"Gathering system properties info..."
GR.RENDER
s$=s$+"[SYSTEM PROPERTIES]#"
SYSTEM.WRITE "getprop"
GOSUB waitForData
IF !ttimedOut THEN
 DO
  SYSTEM.READ.LINE l$
  l$=REPLACE$(l$,"[","<")
  l$=REPLACE$(l$,"]",">")
  a1=IS_IN("dalvik",l$)
  a2=IS_IN("status.battery.level",l$)
  IF a1=0 & a2=0 THEN s$=s$+l$+"#"
  SYSTEM.READ.READY ready
 UNTIL !ready
 s$=s$+"@@#"
ENDIF

! processes
k=k+50
GR.TEXT.DRAW tx,50,k,"Gathering processes info..."
GR.RENDER
s$=s$+"[PROCESSES]#"
SYSTEM.WRITE "ps"
GOSUB waitForData
IF !ttimedOut THEN
 DO
  SYSTEM.READ.LINE l$
  l$=REPLACE$(l$,"  "," ")
  s$=s$+l$+"#"
  SYSTEM.READ.READY ready
 UNTIL !ready
 s$=s$+"@@#"
ENDIF

! services info
k=k+50
GR.TEXT.DRAW tx,50,k,"Gathering services info..."
GR.RENDER
s$=s$+"[SERVICES RUNNING]#"
SYSTEM.WRITE "service list"
GOSUB getSystemData
s$=s$+"@@#"

! packages info
k=k+50
GR.TEXT.DRAW tx,50,k,"Gathering packages info..."
GR.RENDER
s$=s$+"[INSTALLED PACKAGES]#"
SYSTEM.WRITE "pm list packages -f"
GOSUB waitForData
IF !ttimedOut THEN
 j=0
 DO
  SYSTEM.READ.LINE l$
  j=j+1
  j$=REPLACE$(STR$(j),".0"," ")
  l$=REPLACE$(l$,"package:",j$)
  s$=s$+l$+"#"
  SYSTEM.READ.READY ready
 UNTIL !ready
 s$=s$+"@@#"
ENDIF

! **********
SYSTEM.CLOSE
! **********

GR.CLS
k=k+50
GR.TEXT.DRAW tx,50,50,"Done! Sorting data..."
GR.RENDER

! GR.HIDE logo
! GR.HIDE title
tmp$=s$
GOSUB userfunctions

SPLIT inp$[], tmp$ , "#"
nn = listview1(inp$[], 1, refh*0.03, refw*0.01, refw*0.98, refh*0.97, "EXIT PROGRAM", refw, refh)

END
! --------------------------------------------
userfunctions:
! ------------------------------
FN.DEF listview1 (dd$[], scheme, listTop, listLef, listWid , listHei, topStr$ , refw, refh )
 GR.COLOR 55,0,0,125,1
 gr.rect rct, 700,0, 800,1232
 GR.COLOR 255,0,255,25,1
 GR.TEXT.DRAW tx1,60,100, "Sorting data, pls wait..."
 GR.COLOR 255,255,155,25,1
 GR.TEXT.DRAW tx2,60,300, "Use rightest part of screen for fast scrolling -->"
 GR.TEXT.DRAW tx3,60,250, "Scroll down to view info!"
 GR.RENDER
 movingFriction = 0.94
 bounceFriction = 0.6
 vlistmin = 17
 dtSelect = 300
 flagShowClocking= 0
 moveAmplify = 1.2
 moveAmplify2 = 20
 ! colors/propertys ------------
 IF scheme = 1
  txtAlign = 1
  txtSz = 28
  txtBold = 1
  txtSkew = 0
  txtTypeface = 2
  flagLineNo = 0
  flagFadeout = 0
  flagReturnImm = 0
  spacingOffs = txtSz * 0.75
  topBotRectH = 2 * spacingOffs
  botStr$ = "EXIT PROGRAM"
  ARRAY.LOAD cs[],~
  180 , 090 , 090 , 090 , 1 ~ % Background
  155 , 150 , 150 , 150 , 1 ~ % Top/bottom Rect
  255 , 220 , 220 , 220 , 1 ~ % Top/bottom Text
  255 , 255 , 255 , 000 , 1 ~ % ListText Selected
  255 , 255 , 255 , 255 , 1 ~ % ListText UnSelected
  020 , 000 , 000 , 180 , 1 % quickScrollRect
 ENDIF
 GR.HIDE tx1
 GR.HIDE tx2
 GR.HIDE tx3
 gr.hide rct
 GR.GETDL curDl[]
 GOSUB setupList
 TONE 350,100,0
 ! mainFNLoop ------------------------------------------
 DO
  IF ABS(vListY)<= vlistmin THEN vListY=0
  GR.TOUCH touch, xx, yy
  IF touch THEN GOSUB touchHandles
  IF flagBreak THEN D_U.BREAK
  vListY = vListY * movingFriction
  listY = listY + vListY * toc/50
  flagTmp = listY > listCentY | listY+ssy < listCentY
  IF flagTmp THEN GOSUB borderHandles
  t1 = clock ()
  GOSUB moveList
  t2 = clock ()
  GR.RENDER
  t3 = clock ()
  IF flagShowClocking THEN
   GR.MODIFY topTxt, "text", FORMAT$("%%%",t3-tic) +" /"+FORMAT$("%%%",t2-t1) +" /"+FORMAT$("%%%",t3-t2)
  ENDIF
  DO
   toc = CLOCK() - tic
  UNTIL toc >= 50
  tic = CLOCK()
 UNTIL 0
 ! ------------------------------------------------------
 LIST.CREATE s, selection
 FOR i = 1 TO lenlistBas
  IF result[i] > 0 THEN LIST.ADD selection, dd$[i]
 NEXT
 LIST.SIZE selection, sz
 IF !sz THEN LIST.ADD selection, "(noItemsSelected)"
 GR.NEWDL curDl[]
 GR.RENDER
 ! RETURN
 FN.RTN selection
 ! ------------------------------
 touchHandles:
 GR.BOUNDED.TOUCH trect1, listLef*scaw , p1 *scah, xend *scaw , p2 *scah
 GR.BOUNDED.TOUCH trect2, listLef*scaw , p3 *scah, xend *scaw , p4 *scah
 IF trect1 | trect2 THEN flagBreak=1
 GR.BOUNDED.TOUCH touchB, listLef * scaw , 0, xend * scaw , 1200
 IF touchB THEN GOSUB pickHandles
 RETURN
 ! ------------------------------
 borderHandles:
 vListY = -vListY * bounceFriction
 IF listY > listCentY THEN listY= listCentY-space/2+txtsz*0.7
 IF listY+ssy < listCentY THEN listY = listCentY -ssy - space/2+txtsz*0.7
 RETURN
 ! ------------------------------
 pickHandles:
 ticPick = clock ()
 ptrTmp = 0
 GR.TOUCH touch2 , xs ,ys
 ys = ys / scah
 xs = xs / scah
 moveAmpli = moveAmplify
 IF xs > quickScrollRng THEN
  moveAmpli = moveAmplify2
  GR.SHOW quickSrollRect
 ENDIF
 ctr = 0
 dlistMx = listCentY - listy + 0 * space
 dlistMn = listCentY - listy - ssy - 1.5 * space
 dlistMoveMn = 10
 flagmove = 0
 flagselect = 0
 DO % -----------
  ctr += 1
  yold = y
  GR.TOUCH touch2 , x ,y
  x = x / scaw
  y = y / scah
  ptrTmp += 1
  IF ptrTmp > lenVBuff THEN ptrTmp =1
  VBuff[ptrTmp] = yold-y
  dlisty = (y-ys)* moveAmpli
  IF dlisty > dlistMx THEN dlisty = dlistMx
  IF dlisty < dlistMn THEN dlisty = dlistMn
  IF ABS(dlisty)> dlistMoveMn THEN flagmove=1
  flagtmp = !flagmove & !flagselect & CLOCK()-ticPick>dtSelect & ys>listy & ys<listy+ssy+space
  ! IF flagtmp THEN GOSUB SELECThandles
  IF flagBreak THEN D_U.BREAK
  GOSUB moveList
  GR.RENDER
  DO
   toc = CLOCK() - tic
  UNTIL toc >= 50
  tic = CLOCK()
 UNTIL !touch2 %-----------
 listY = listY + dlistY
 dlistY = 0
 ARRAY.AVERAGE vlist , VBuff[]
 vListY = vlist * - moveAmplify
 GR.HIDE quickSrollRect
 RETURN
 ! ------------------------------
 moveList:
 idxMoveSold = idxMoveS
 idxMoveEold = idxMoveE
 idxMoveS = ceil ((listTop-listy-dlistY+topBotRectH)/space)
 idxMoveE = idxMoveS + lenlistIdx
 IF idxMoveS < 0 THEN idxMoveS = 0
 IF idxMoveE > lenlist THEN idxMoveE = lenlist
 dytmp = listy + dlistY - space/2
 FOR i = idxMoveS+1 TO idxMoveE
  yact = dytmp + i* space
  GR.SHOW grlist[i]
  GR.MODIFY grlist[i], "y" , yact
  IF flagFadeout THEN GR.MODIFY grlist[i],"alpha",255-ABS(yact- listCenty)*liTmp1
 NEXT
 FOR i = idxMoveE+1 TO idxMoveEold+1
  GR.HIDE grlist[i]
 NEXT
 FOR i = idxMoveSold+1 TO idxMoveS
  GR.HIDE grlist[i]
 NEXT
 RETURN
 ! ------------------------------
 setupList:
 centW = refW/2
 centH = refH/2
 GR.SCREEN curW, curH
 scaW = curW / refW
 scaH = curH / refH
 lenVBuff = 5
 DIM VBuff[ lenVBuff ]
 ARRAY.LOAD vib [], 0, 30
 ARRAY.LENGTH lenlistBas, dd$[]
 DIM grlist [lenlistBas*2]
 DIM result [lenlistBas]
 listBot = listTop + listHei
 listCenty = listTop + listHei/2
 listCentx = listLef + listWid/2
 space = txtSz + spacingOffs
 lenlistIdx = (listBot - listTop - 2*topBotRectH) / space
 listy = listCentY - txtsz*0.3
 listy= 50 % here
 quickScrollRng = listLef + listWid *0.9
 liTmp1 = 255 / (listHei/2)
 p1 = listTop - 0
 p2 = listTop + topBotRectH
 p3 = listbot - topBotRectH
 p4 = listbot + 0
 xend = listLef + listWid
 ! Background ----------
 no = 0 * 5
 GR.COLOR cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
 GR.RECT backGrRect , listLef, p1 , xend , p4
 no = 5 * 5
 GR.COLOR cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
 GR.RECT quickSrollRect , quickScrollRng , p2 , xend , p3
 GR.HIDE quickSrollRect
 ! ListText ----------
 GR.TEXT.ALIGN txtAlign
 GR.TEXT.SIZE txtSz
 GR.TEXT.BOLD txtBold
 GR.TEXT.SKEW txtSkew
 GR.TEXT.TYPEFACE txtTypeface
 no = 3 * 5
 GR.COLOR cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
 GR.PAINT.GET selectedPaint
 no = 4 * 5
 GR.COLOR cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
 GR.PAINT.GET notSelectedPaint
 LIST.CREATE n,idxList
 lineCtr = 0
 IF txtAlign = 1 THEN txtXpos = listLef + 5
 IF txtAlign = 2 THEN txtXpos = listLef + listWid/2
 IF txtAlign = 3 THEN txtXpos = listLef + listWid - 5

 FOR i = 1 TO lenlistBas
  IF flagLineNo THEN noStr$ =REPLACE$(FORMAT$("####",i)," ","")+" "
  tmp$ = noStr$ + dd$[i]
  ! linebreakStuff part1 --
  GR.TEXT.WIDTH txtwidth, tmp$
  IF txtwidth+5 > listWid
   GR.TEXT.WIDTH oxTmp , noStr$
   maxLentmp = FLOOR((listWid-oxTmp-5)/(txtwidth)*LEN(tmp$)*1.0)
   tmp2$ = RIGHT$(tmp$, LEN(tmp$)-maxLentmp)
   tmp$ = left$ (tmp$, maxLentmp)
   GR.TEXT.WIDTH txtwidth, tmp2$
   IF txtwidth+5 > listWid
    maxLentmp = FLOOR((listWid-oxTmp-5)/(txtwidth)*LEN(tmp2$)*1.0)
    tmp2$ = left$ (tmp2$, maxLentmp)
   ENDIF
  ENDIF
  lineCtr += 1
  IF LEFT$(tmp$,1)="[" THEN GR.COLOR 255,255,255,0,1 ELSE GR.COLOR 255,255,255,255,1
  IF LEFT$(tmp$,5)="[FILE" | LEFT$(tmp$,5)="[PROC" | LEFT$(tmp$,5)= "[SYST" | LEFT$(tmp$,5)= "[SERV" | LEFT$(tmp$,5)="[INST" THEN mark=1
  IF LEFT$(tmp$,2)="$$" THEN
   GR.TEXT.SIZE 25
   tmp$=REPLACE$(tmp$,"$$","")
  ENDIF
  IF LEFT$(tmp$,2)="@@" THEN
   GR.TEXT.SIZE 28
   mark=0
   tmp$=REPLACE$(tmp$,"@@","")
  ENDIF

  GR.TEXT.DRAW grlist[lineCtr] , txtXpos , 0 , tmp$
  GR.HIDE grlist[lineCtr]
  LIST.ADD idxList,i
  ! linebreakStuff part2 --
  IF tmp2$ <> ""
   lineCtr += 1
   tmpx = txtXpos
   IF txtAlign = 1 THEN tmpx = txtXpos + oxTmp+5
   GR.TEXT.DRAW grlist[lineCtr] , tmpx , 0,tmp2$
   GR.HIDE grlist[lineCtr]
   LIST.ADD idxList,i
   tmp2$=""
  ENDIF
  IF mark=1 THEN GR.TEXT.SIZE 20
 NEXT
 LIST.TOARRAY idxList , idxList[]
 lenlist = linectr
 ssy = space * lenlist
 ! Top/bottomRect & Text -----------
 no = 1 * 5
 GR.COLOR cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
 GR.RECT topRect , listLef , p1 , xend , p2
 GR.RECT botRect , listLef , p3 , xend , p4
 no = 2 * 5
 GR.COLOR cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
 szTmp = topBotRectH *0.4
 tmp2 = szTmp/2*1.4
 GR.TEXT.SIZE szTmp
 GR.TEXT.ALIGN 2
 GR.TEXT.DRAW topTxt , listLef + listWid /2 ,p2 - tmp2 , topStr$
 GR.TEXT.DRAW botTxt , listLef + listWid /2 ,p4 - tmp2 , botStr$
 RETURN
 ! ------------------------------
FN.END
RETURN



getSystemData:
X=CLOCK()
DO
 PAUSE 200
 SYSTEM.READ.READY ready
UNTIL ready | CLOCK()-x>maxTout
IF CLOCK()-x<maxTout THEN
 DO
  SYSTEM.READ.LINE l$
  s$=s$+l$+"#"
  SYSTEM.READ.READY ready
 UNTIL !ready
ELSE
 s$=s$+"No data available!"+"#"
ENDIF
RETURN

waitForData:
ttimedOut=0
x=CLOCK()
DO
 PAUSE 200
 SYSTEM.READ.READY ready
UNTIL ready | CLOCK()-x>maxTout
IF CLOCK()-x>maxTout THEN ttimedOut=1 ELSE ttimedOut=0
IF ttimedOut=1 THEN s$=s$+"No data available!"+"#"
RETURN
