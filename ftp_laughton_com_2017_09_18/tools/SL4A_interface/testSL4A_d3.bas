GOSUB userfunctions
pTime              = 1500

DO

 PRINT                "-----------------"
 PRINT                "1. testVideo....
 PRINT                " duration 20s...on Background, look for: /sdcard/sl4aTestVid.mpg "
 CALL                 sl4a_recorderCaptureVideo ("/sdcard/sl4aTestVid.mpg",20)
 PAUSE 1000
 CALL                 sl4a_launch( "com.rfo.basic.Basic" )  % bring basic! back to front


 PAUSE                pTime
 PRINT                "-----------------"
 PRINT                "2. testDatePicker...."
 datPick$          =  sl4a_datePicker$()
 datPick$          =  replace $(datPick$,",","/")
 PRINT                "you picked : " +chr $(10)+ datPick$


 PAUSE                pTime
 PRINT                "-----------------"
 PRINT                "3. testTimePicker...."
 ttPick$           =  sl4a_timePicker$()
 UNDIM                tt $[]
 SPLIT                tt $[],  ttPick$  , ","
 PRINT                "you picked : "
 PRINT                 FORMAT$("%%",val (tt$[1])) + ":"+FORMAT$("%%",val (tt$[2]))


 PAUSE                pTime
 PRINT                "-----------------"
 PRINT                "4. setMediaVolume ...."
 INPUT                "desiredMediaVol...", desVol
 CALL                 sl4a_setMediaVolume( desVol )
 PRINT                "volume set to : " ; desVol


 PAUSE                pTime
 PRINT                "-----------------"
 PRINT                "5. getMediaVolume ...."
 curVol             = sl4a_getMediaVolume( )
 PRINT                "curMediaVolume : " ; curVol


 PAUSE                pTime
 PRINT                "-----------------"
 PRINT                "6. list-SingleChoice ...."
 UNDIM                 dd$[]
 FILE.DIR              "../data/",  dd$[]
 tmp                =  sl4a_SetSingleChoice( dd$[] )
 PRINT                 "your selection : " +CHR$(10) + dd$[tmp]


 PAUSE                pTime
 PRINT                "-----------------"
 PRINT                "7. list-MultiChoice ...."
 tmp                =  sl4a_SetMultiChoice( dd$[] )
 LIST.SIZE              tmp, sz
 print                "your selection : "
 FOR i              =  1 TO sz
   LIST.GET            tmp,i, xx
    print              dd$[xx]
  NEXT
  
     
 PAUSE                pTime
 PRINT                "-----------------"
 PRINT                "8. toggleWifiState...."
 tmp$               =  sl4a_toggleWifiState$()
 print                 tmp$


 PAUSE                pTime
 PRINT                "-----------------"
 PRINT                "9. getLaunchableApplications ...."
 apps$               = sl4a_ getLaunchableApplications$()
 apps$               = replace $( apps$,":","    :    ")
 TEXT.INPUT            apps2$, apps$, "all launchable apps:..."
   
 print               CHR$(10) +CHR$(10)+ "...thanks for testing"

end







!---------------------------------------------------------
userfunctions:

!---------------------------------------------------------
! public function(s) -------------------------------------
!---------------------------------------------------------


!---------------------------------------------------------
FN.DEF sl4a_launch( toLaunch$ )

 py$        = ~
 "li = droid.launch ( '" +  toLaunch$ + "' )"

 CALL                       sl4a_callBasicPy$( py$, "" , 1 )

FN.END
!---------------------------------------------------------

!---------------------------------------------------------
FN.DEF sl4a_ getLaunchableApplications$()

 py$        = ~
 "li = droid.getLaunchableApplications().result "

 writeStr$  = ~
 "fo.write (str (li))"

 recieve$                     =  sl4a_callBasicPy$( py$, writeStr$ ,1)
 recieve$                     =  replace $( recieve$, " " , "" )
 recieve$                     =  replace $( recieve$, "u'", "" )
 recieve$                     =  replace $( recieve$, "'" , "" )
 recieve$                     =  replace $( recieve$, "{" , "" )
 recieve$                     =  replace $( recieve$, "}" , "" )
 recieve$                     =  replace $( recieve$, "," ,CHR$(10) )
 FN.RTN                          recieve$

FN.END
!---------------------------------------------------------

!---------------------------------------------------------
FN.DEF sl4a_SetSingleChoice ( items $[] )

 ARRAY.LENGTH len, items $[]
 tmp$="'"
 FOR i=1 TO len -1
  tmp$=tmp$ + items $[i]+"' , '"
 NEXT
 tmp$=tmp$ + items $[i] +"'"

 py$ = ~
 "title = 'Please Select...'"          +CHR$(10)+~
 "droid.dialogCreateAlert(title)"      +CHR$(10)+~
 "droid.dialogSetSingleChoiceItems([" +  tmp$ + "])"   +CHR$(10)+~
 "droid.dialogSetPositiveButtonText('Yay!')"           +CHR$(10)+~
 "droid.dialogShow()"      +CHR$(10)+~
 "response = droid.dialogGetResponse().result"        +CHR$(10)+~
 "response2 = droid.dialogGetSelectedItems().result"

 writeStr$  = ~
 "fo.write ( str (response2[0]))"

 recieve$                     =  sl4a_callBasicPy$( py$, writeStr$ ,1)
 FN.RTN                          VAL(recieve$)+1

FN.END
!---------------------------------------------------------


!---------------------------------------------------------
FN.DEF sl4a_SetMultiChoice ( items $[] )

 ARRAY.LENGTH len, items $[]
 tmp$="'"
 FOR i=1 TO len -1
  tmp$=tmp$ + items $[i]+"' , '"
 NEXT
 tmp$=tmp$ + items $[i] +"'"

 py$ = ~
 "title = 'Please Select....'"                     +CHR$(10)+~
 "droid.dialogCreateAlert(title)"      +CHR$(10)+~
 "droid.dialogSetMultiChoiceItems([" +  tmp$ + "])"   +CHR$(10)+~
 "droid.dialogSetPositiveButtonText('Yay!')"           +CHR$(10)+~
 "droid.dialogShow()"      +CHR$(10)+~
 "response = droid.dialogGetResponse().result"        +CHR$(10)+~
 "response2 = droid.dialogGetSelectedItems().result"

 writeStr$  = ~
 "for i in response2:"         +CHR$(10)+~
 "  fo.write (str (i)+',')"

 recieve$                     =  sl4a_callBasicPy$( py$, writeStr$ ,1)
 SPLIT                           recieve$[], recieve$,","
 ARRAY.LENGTH                    len, recieve$[]
 LIST.CREATE                     n,out
 FOR i                           =1 TO len
  LIST.ADD                        out, val ( recieve$[i] )+1
 NEXT
 FN.RTN                          out

FN.END
!---------------------------------------------------------


!---------------------------------------------------------
FN.DEF sl4a_recorderCaptureVideo ( vidPath$,recDuration)
 tmp $=str $( recDuration)
 py$ = ~
 "droid.recorderCaptureVideo('"+vidPath$ +"',"+ tmp $ + ",1)"
 CALL               sl4a_callBasicPy$( py$, "" ,0)
FN.END
!---------------------------------------------------------


!---------------------------------------------------------
FN.DEF sl4a_datePicker$()

 py$ = ~
 "droid.dialogCreateDatePicker(2013) "       +CHR$(10)+~
 "droid.dialogShow()"                        +CHR$(10)+~
 "date = droid.dialogGetResponse().result"   +CHR$(10)+~
 "droid.dialogCreateAlert('xx', 'yy')"

 writeStr$  = ~
 "fo.write (str (date['day'])+','+str(date['month'])+',' + str(date['year']))"

 recieve$                     =  sl4a_callBasicPy$( py$, writeStr$ ,1)
 FN.RTN                          recieve$

FN.END
!---------------------------------------------------------


!---------------------------------------------------------
FN.DEF sl4a_timePicker$()

 py$ = ~
 "droid.dialogCreateTimePicker() "           +CHR$(10)+~
 "droid.dialogShow()"                        +CHR$(10)+~
 "time = droid.dialogGetResponse().result"   +CHR$(10)+~
 "droid.dialogCreateAlert('xx', 'yy')"

 writeStr$  = ~
 "fo.write ( str(time['hour'])+','+ str(time['minute'])+','+ time['which'] )"


 recieve$                     =  sl4a_callBasicPy$( py$,writeStr$ ,1)
 FN.RTN                          recieve$

FN.END
!---------------------------------------------------------


!---------------------------------------------------------
FN.DEF sl4a_getMediaVolume()

 py$        = ~
 "tmp=droid.getMediaVolume()"

 writeStr$  = ~
 "fo.write (str ( tmp[1] ))"

 recieve$                     =  sl4a_callBasicPy$( py$, writeStr$ ,1)
 FN.RTN                          val (recieve$)

FN.END
!---------------------------------------------------------


!---------------------------------------------------------
FN.DEF sl4a_toggleWifiState$ ()

 py$        = ~
 "tmp=droid. toggleWifiState().result"

 writeStr$  = ~
 "fo.write (str ( tmp ))"

 recieve$                     =  sl4a_callBasicPy$( py$, writeStr$,1 )
 FN.RTN                          recieve$

FN.END
!---------------------------------------------------------


!---------------------------------------------------------
FN.DEF sl4a_setScreenBrightness( brNess )
 py$ = ~
 "droid.setScreenBrightness(" +str $( brNess ) + ")"
 CALL               sl4a_callBasicPy$( py$ ,"",1)
FN.END
!---------------------------------------------------------



!---------------------------------------------------------
FN.DEF sl4a_setMediaVolume(vol)
 py$ = ~
 "droid.setMediaVolume(" +str $(vol) + ")"
 CALL               sl4a_callBasicPy$( py$ ,"",1)
FN.END
!---------------------------------------------------------


!---------------------------------------------------------
! private function(s) ------------------------------------
!---------------------------------------------------------

FN.DEF sl4a_callBasicPy$( py$, writeStr$ , waitForExit )

 fileTx$         =   "../../sl4a/scripts/basicTx.py"
 intentPath$     = "/sdcard/sl4a/scripts/basicTx.py"
 fileRx$         =   "../../sl4a/scripts/basicRx.txt"
 FILE.EXISTS       feRx, fileRx$
 IF feRx           THEN FILE.DELETE  nn, fileRx$
 
 IF !waitForExit   then un$="#" else un$=""
 
 py$ = ~
 "import android" +CHR$(10)+~
 "droid = android.Android()"+CHR$(10) + py$ +CHR$(10)+~
 un$+"fo = open( '/sdcard/sl4a/scripts/basicRx.txt' , 'w')" +CHR$(10)+~
 un$+ writeStr$  +CHR$(10)+~
 un$+ "fo.close ()"
 !print py $

 TEXT.OPEN          w, fid, fileTx$
 TEXT.WRITELN       fid, py$
 TEXT.CLOSE         fid
 cmd$ =        "am start "
 cmd$ = cmd$ + "-a com.googlecode.android_scripting.action.LAUNCH_BACKGROUND_SCRIPT "
 cmd$ = cmd$ + "-n com.googlecode.android_scripting/.activity.ScriptingLayerServiceLauncher "
 cmd$ = cmd$ + "-e com.googlecode.android_scripting.extra.SCRIPT_PATH "
 cmd$ = cmd$ + intentPath$
 
 SYSTEM.OPEN
 SYSTEM.WRITE         cmd$
 PAUSE                100
 SYSTEM.CLOSE

 IF waitForExit
  DO
   FILE.EXISTS        fe2, fileRx$
  UNTIL               fe2
  pause               10
  GRABFILE            output$ , fileRx$
  pause               10
  !PRINT               output$
 ENDIF
 
 FN.RTN              output$
 
FN.END
!---------------------------------------------------------


RETURN
!---------------------------------------------------------






