!!

SHORT USAGE GUIDE

before usage:

- copy the file 'sendemailTemplate.py' to this folder: /sdcard/sl4a/scripts/basicTx.py


general things related to email account:

- enter your email-smtp-server and -port into related vars below (it's ready for googlemail right now)
- emName$ is only a alias that appears in the mail, normally your name
- emUser$ is your mail-login username
- emPwd$ is your mail-login password


things / remarks for individual mail:

- mailto$ : reciever adress, only one single address poss. right now
- subject$ : subjectstring
- body$ : bodystring, only one line poss. right now
- attachments$ : attachment-file with full path, 
   path in absolute/python form (see example)


for further info:
https://groups.google.com/forum/m/?fromgroups#!topic/tasker/5b5PFQ2fqqs
http://tasker.wikidot.com/email


!!



GOSUB userfunctions


smtp_server$     = "smtp.gmail.com "
smtp_port$       = "587"
emName$          = "Xxx Yxx"
emUser$          = ""
emPwd$           = ""

mailto$          = ""
subject$         = "BASIC!/Py4a testmail"
body$            = "Hi, this is a BASIC!/Py4a-testmail"
attachments$     = "/sdcard/rfo-basic/data/whee.mp3"


CALL               py4a_sendMail ( ~
                   smtp_server$, smtp_port$, emName$, emUser$, ~
                   emPwd$, mailto$, subject$, body$, attachments$)

END


!---------------------------------------------------------
userfunctions:

!---------------------------------------------------------
FN.DEF py4a_sendMail ( smtp_server$, smtp_port$, emName$, emUser$, emPwd$, mailto$, subject$, body$, attachments$)

 smpt$  = ~
 "  smtp_server = '" + smtp_server$    +"'"  +CHR$(10)+~
 "  smtp_port = "    + smtp_port$

 mess$ = ~
 "email_name = '"   + emName$ +"'"   +CHR$(10)+~
 "email_user = '"   + emUser$ +"'"   +CHR$(10)+~
 "email_pswd = '"   + emPwd$ +"'"    +CHR$(10)+~
 "mailto = '"       + mailto$ +"'"   +CHR$(10)+~
 "subject = '"      + subject$ +"'"  +CHR$(10)+~
 "body = '"        + body$ +"'"    +CHR$(10)+~
 "attachments = ['" + attachments$ + "']"

 p1$              = "../../sl4a/scripts/sendemailTemplate.py"
 GRABFILE           tmplPy$ , p1$
 tmplPy$          = REPLACE$( tmplPy$ , "###smptEntry###", smpt$ )
 tmplPy$          = REPLACE$( tmplPy$ , "###messageData###", mess$ )

 recieve$                =  sl4a_callBasicPy$( tmplPy$ ,"",0)

FN.END
!---------------------------------------------------------



!---------------------------------------------------------
FN.DEF sl4a_callBasicPy$( py$, writeStr$ ,  tOut )

 fileTx$         =   "../../sl4a/scripts/basicTx.py"
 intentPath$     = "/sdcard/sl4a/scripts/basicTx.py"
 fileRx$         =   "../../sl4a/scripts/basicRx.txt"
 FILE.EXISTS       feRx, fileRx$
 IF feRx           THEN FILE.DELETE  nn, fileRx$

 IF ! tOut         THEN un$="#" ELSE un$=""

 py$ = ~
 "import android" +CHR$(10)+~
 "droid = android.Android()"+CHR$(10) + py$ +CHR$(10)+~
 un$+"fo = open( '/sdcard/sl4a/scripts/basicRx.txt' , 'w')" +CHR$(10)+~
 un$+ writeStr$  +CHR$(10)+~
 un$+ "fo.close ()"
 !print py $

 TEXT.OPEN          w, fid, fileTx$
 TEXT.WRITELN       fid, py$
 TEXT.CLOSE         fid
 cmd$ =        "am start "
 cmd$ = cmd$ + "-a com.googlecode.android_scripting.action.LAUNCH_BACKGROUND_SCRIPT "
 cmd$ = cmd$ + "-n com.googlecode.android_scripting/.activity.ScriptingLayerServiceLauncher "
 cmd$ = cmd$ + "-e com.googlecode.android_scripting.extra.SCRIPT_PATH "
 cmd$ = cmd$ + intentPath$

 SYSTEM.OPEN
 SYSTEM.WRITE         cmd$
 PAUSE                100
 SYSTEM.CLOSE

 tOut                =  tOut*1000
 tic=clock ()
 DO
  FILE.EXISTS        fe2, fileRx$
 UNTIL               fe2 | clock ()-tic> tOut

 PAUSE               30
 IF fe2              THEN GRABFILE  output$ , fileRx$

 !PRINT               output$


 FN.RTN              output$

FN.END
!---------------------------------------------------------


RETURN
!---------------------------------------------------------











