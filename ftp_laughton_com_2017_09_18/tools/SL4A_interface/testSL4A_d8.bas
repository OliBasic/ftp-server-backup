GOSUB userfunctions
pTime              = 1000

!goto testtt




DO

 PRINT                "-----------------"
 PRINT                "1. testVideo....
 PRINT                " duration 20s...on Background, look for: /sdcard/sl4aTestVid.mpg "
 CALL                 sl4a_recorderCaptureVideo ("/sdcard/sl4aTestVid.mpg",20)
 PAUSE 600
 CALL                 sl4a_launch( "com.rfo.basic.Basic" )  % bring basic! back to front


 PAUSE                pTime
 PRINT                "-----------------"
 PRINT                "2. testDatePicker...."
 datPick$          =  sl4a_datePicker$()
 datPick$          =  replace $(datPick$,",","/")
 PRINT                "you picked : " +chr $(10)+ datPick$


 PAUSE                pTime
 PRINT                "-----------------"
 PRINT                "3. testTimePicker...."
 ttPick$           =  sl4a_timePicker$()
 UNDIM                tt $[]
 SPLIT                tt $[],  ttPick$  , ","
 PRINT                "you picked : "
 PRINT                 FORMAT$("%%",val (tt$[1])) + ":"+FORMAT$("%%",val (tt$[2]))


 PAUSE                pTime
 PRINT                "-----------------"
 PRINT                "4. setMediaVolume ...."
 INPUT                "desiredMediaVol...", desVol
 CALL                 sl4a_setMediaVolume( desVol )
 PRINT                "volume set to : " ; desVol


 PAUSE                pTime
 PRINT                "-----------------"
 PRINT                "5. getMediaVolume ...."
 curVol             = sl4a_getMediaVolume( )
 PRINT                "curMediaVolume : " ; curVol


 PAUSE                pTime
 PRINT                "-----------------"
 PRINT                "6. list-SingleChoice ...."
 UNDIM                 dd$[]
 FILE.DIR              "../data/",  dd$[]
 tmp                =  sl4a_SetSingleChoice( dd$[] )
 PRINT                 "your selection : " +CHR$(10) + dd$[tmp]


 PAUSE                pTime
 PRINT                "-----------------"
 PRINT                "7. list-MultiChoice ...."
 tmp                =  sl4a_SetMultiChoice( dd$[] )
 LIST.SIZE              tmp, sz
 PRINT                "your selection : "
 FOR i              =  1 TO sz
  LIST.GET            tmp,i, xx
  PRINT              dd$[xx]
 NEXT


 PAUSE                pTime
 PRINT                "-----------------"
 PRINT                "8. toggleWifiState...."
 tmp$               =  sl4a_toggleWifiState$()
 PRINT                 tmp$


 PAUSE                pTime
 PRINT                "-----------------"
 PRINT                "9. getLaunchableApplications ...."
 apps$               = sl4a_ getLaunchableApplications$()
 apps$               = replace $( apps$,":","    :    ")
 TEXT.INPUT            apps2$, apps$, "all launchable apps:..."


 PAUSE                pTime
 PRINT                "-----------------"
 PRINT                "10. getUserInput ...."
 tmp$               =  sl4a_getUserInput$ ( "username" , 0)
 PRINT                 tmp $
 tmp$               =  sl4a_getUserInput$ ( "pw" , 1)
 PRINT                 tmp $


PAUSE                pTime
 PRINT                "-----------------"
 PRINT                "11. alertWithBut ...."
 tmp$               =  sl4a_alertWithBut$ ( "ALERT 1", "select","yes","no","dont know")
 tmp$               =  sl4a_alertWithBut$ ( "ALERT 2", "select","yes","no","")
 tmp$               =  sl4a_alertWithBut$ ( "ALERT 3", "select","ok","","")
 
 
 PAUSE                pTime
 PRINT                "-----------------"
 PRINT                 "12. togglers ...."
 tmp$              =   sl4a_ toggleBluetoothState $ ()
 PRINT                 " BluetoothState: "; tmp $
 tmp$              =   sl4a_ toggleAirplaneMode $ ()
 PRINT                 " AirplaneMode :"; tmp $
 tmp$              =   sl4a_ toggleRingerSilentMode$ ()
 PRINT                 " RingerSilentMode : "; tmp $
 tmp$              =   sl4a_ toggleWifiState$ ()
 PRINT                 " WifiState : "; tmp $
 tmp$              =   sl4a_ toggleVibrateMode $()
 PRINT                 " VibrateMode : "; tmp $

 

 PAUSE                pTime
 PRINT                "-----------------"
 PRINT                 "13. togglers ...."
 tmp$              =  sl4a_ getAllScalars$ ()
  UNDIM               tmp $[]
  SPLIT               tmp $[], tmp$, ":"
  PRINT               tmp$[1]
  PRINT               tmp$[2]
 testtt:
 
 
 PRINT               CHR$(10) +CHR$(10)+ "...thanks for testing"
 


 END

 !---------------------------------------------------------
 userfunctions:

!!

states:  -----------------
sl4a_ getAllScalars$
 items$ = ~
" 'checkAirplaneMode' , 'checkRingerSilentMode' ,' checkScreenOn ' , ' getScreenTimeout ' , ' getScreenBrightness ' , ' getRingerVolume ' , ' getMediaVolume ' , ' getVibrateMode ' , ' checkWifiState ' , ' checkBluetoothState ' , ' checkNetworkRoaming ' "

togglers: -----------------
sl4a_ toggleWifiState$ ()
sl4a_ toggleRingerSilentMode$ ()
sl4a_ toggleBluetoothState $()
sl4a_ toggleAirplaneMode $()
sl4a_ toggleVibrateMode $()


settings: -----------------
sl4a_ setScreenTimeout(  scrTimeout )
sl4a_ setMediaVolume(vol)
sl4a_ setRingerVolume (vol)
sl4a_ setScreenBrightness( brNess )


UIs: -----------------

sl4a_alertWithBut$ ( title$, message$,posTxt$,negTxt$,neuTxt$)
sl4a_getUserInput$ ( title$, flagGetPw )
sl4a_timePicker$()
sl4a_datePicker$()
sl4a_alertWithBut$ ( title$, message$,posTxt$,negTxt$,neuTxt$)
sl4a_getUserInput$ ( title$, flagGetPw )
sl4a_SetMultiChoice ( items $[] )
sl4a_SetSingleChoice ( items $[] )


divers: -----------------
sl4a_getUserInput$ ( title$, flagGetPw )
sl4a_ getLaunchableApplications$()
sl4a_launch( toLaunch$ )
sl4a_getMediaVolume()
sl4a_recorderCaptureVideo ( vidPath$,recDuration)
sl4a_ sendEmail ( addr$ , subj$ , text$ , attach$ )

!!



 !---------------------------------------------------------
 ! public function(s) -------------------------------------
 !---------------------------------------------------------

 !---------------------------------------------------------

 FN.DEF sl4a_ sendEmail ( addr$ , subj$ , text$ , attach$ )
 
   py$    = "tmp=droid. sendEmail ('" + addr$ +"','"+  subj$ + "','" + text$ +  "')"
  IF attach$<>""
   py$    = "tmp=droid. sendEmail ('" + addr$ +"','"+ subj$ +"','" + text$ + "','" + attach$ + "')"
  ENDIF
  print py $
  recieve$                     =  sl4a_callBasicPy$( py$, "",0 )
  
 FN.END
 !---------------------------------------------------------


 !---------------------------------------------------------
 FN.DEF sl4a_ setScreenTimeout(  scrTimeout )
  py$ = "droid. setScreenTimeout( (" +str $( scrTimeout ) + ")"
  CALL               sl4a_callBasicPy$( py$ ,"",0)
 FN.END

 FN.DEF sl4a_ setRingerVolume (vol)
  py$ = "droid. setRingerVolume (" +str $(vol) + ")"
  CALL               sl4a_callBasicPy$( py$ ,"",0)
 FN.END

 FN.DEF sl4a_setMediaVolume(vol)
  py$ = "droid. setMediaVolume (" +str $(vol) + ")"
  CALL               sl4a_callBasicPy$( py$ ,"",0)
 FN.END

 FN.DEF sl4a_setScreenBrightness( brNess )
  py$ = ~
  "droid.setScreenBrightness(" +str $( brNess ) + ")"
  CALL               sl4a_callBasicPy$( py$ ,"",0)
 FN.END
 !---------------------------------------------------------


 !---------------------------------------------------------
 FN.DEF sl4a_ toggleBluetoothState $()
  py$        = "tmp=droid. toggleBluetoothState ().result"
  writeStr$  = "fo.write (str ( tmp ))"
  recieve$                     =  sl4a_callBasicPy$( py$, writeStr$,10 )
  FN.RTN                          recieve$
 FN.END

 FN.DEF sl4a_ toggleAirplaneMode $()
  py$        = "tmp=droid. toggleAirplaneMode ().result"
  writeStr$  = "fo.write (str ( tmp ))"
  recieve$                     =  sl4a_callBasicPy$( py$, writeStr$,3 )
  FN.RTN                          recieve$
 FN.END

 FN.DEF sl4a_ toggleVibrateMode $()
  py$        = "tmp=droid. toggleVibrateMode ().result"
  writeStr$  = "fo.write (str ( tmp ))"
  recieve$                     =  sl4a_callBasicPy$( py$, writeStr$,3 )
  FN.RTN                          recieve$
 FN.END

 FN.DEF sl4a_ toggleRingerSilentMode$ ()
  py$        = "tmp=droid. toggleRingerSilentMode ().result"
  writeStr$  = "fo.write (str ( tmp ))"
  recieve$                     =  sl4a_callBasicPy$( py$, writeStr$,3 )
  FN.RTN                          recieve$
 FN.END

 FN.DEF sl4a_ toggleWifiState$ ()
  py$        = "tmp=droid. toggleWifiState ().result"
  writeStr$  = "fo.write (str ( tmp ))"
  recieve$                     =  sl4a_callBasicPy$( py$, writeStr$,3 )
  FN.RTN                          recieve$
 FN.END
 !---------------------------------------------------------


 !---------------------------------------------------------
 FN.DEF sl4a_getUserInput$ ( title$, flagGetPw )
  pyStr$   = "inp = droid.dialogGetInput('" + title$ + "').result "
  IF flagGetPw THEN
   pyStr$ = "inp = droid.dialogGetPassword('" + title$ + "').result "
  ENDIF
  py$        = pyStr$
  writeStr$  = ~
  "fo.write ( inp )"
  recieve$                  =  sl4a_callBasicPy$( py$, writeStr$ ,6000)
  FN.RTN                       recieve$
 FN.END
 !---------------------------------------------------------


 !---------------------------------------------------------
 FN.DEF sl4a_alertWithBut$ ( title$, message$,posTxt$,negTxt$,neuTxt$)

  IF negTxt$<>"" THEN negStr$= "droid.dialogSetNegativeButtonText(' " + negTxt$ + "')"+CHR$(10)
  IF neuTxt$<>"" THEN neuStr$= "droid.dialogSetNeutralButtonText(' " + neuTxt$ + "')"+CHR$(10)

  py$        = ~
  "title = '" +   title$   + "'"     +CHR$(10)+~
  "message = '" +  message$ + "'"     +CHR$(10)+~
  "droid.dialogCreateAlert(title, message)"   +CHR$(10)+~
  "droid.dialogSetPositiveButtonText('" + posTxt$ + "')" +CHR$(10)+~
  negStr$ + neuStr$ +"droid.dialogShow()"               +CHR$(10)+~
  "response = droid.dialogGetResponse().result"

  writeStr$  = ~
  "fo.write (str ( response ))"

  recieve$                =  sl4a_callBasicPy$( py$, writeStr$ ,6000)
  IF is_in ("pos",recieve$)    THEN ret $="pos"
  IF is_in ("neu",recieve$)    THEN ret $="neu"
  IF is_in ("neg",recieve$)    THEN ret $="neg"
  FN.RTN                       ret$

 FN.END
 !---------------------------------------------------------



 !---------------------------------------------------------
 FN.DEF sl4a_ getAllScalars$ ()

  items$ = ~
  " 'checkAirplaneMode' , 'checkRingerSilentMode' ,' checkScreenOn ' , ' getScreenTimeout ' , ' getScreenBrightness ' , ' getRingerVolume ' , ' getMediaVolume ' , ' getVibrateMode ' , ' checkWifiState ' , ' checkBluetoothState ' , ' checkNetworkRoaming ' "

  py$        = ~
  "li2=['init'] "                +CHR$(10)+~
  "items = [" +  items$  + "]"   +CHR$(10)+~
  "for i in items:"              +CHR$(10)+~
  "  eval ( 'li2.append (droid.' +i+ '().result) ')" +CHR$(10)+~
  "del (li2 [0])"
  writeStr$  = ~
  "fo.write (str ( li2 ))"
  recieve$                     =  sl4a_callBasicPy$( py$, writeStr$ ,3)
  FN.RTN                          items$ + " : " +  recieve$
 FN.END
 !---------------------------------------------------------

 !---------------------------------------------------------
 FN.DEF sl4a_getMediaVolume()
  py$        = "tmp=droid.getMediaVolume()"
  writeStr$  = "fo.write (str ( tmp[1] ))"
  recieve$                     =  sl4a_callBasicPy$( py$, writeStr$ ,3)
  FN.RTN                          val (recieve$)
 FN.END
 !---------------------------------------------------------

 !---------------------------------------------------------
 FN.DEF sl4a_launch( toLaunch$ )
  py$        = ~
  "li = droid.launch ( '" +  toLaunch$ + "' )"
  CALL                       sl4a_callBasicPy$( py$, "" , 0)
 FN.END
 !---------------------------------------------------------

 !---------------------------------------------------------
 FN.DEF sl4a_ getLaunchableApplications$()

  py$        = ~
  "li = droid.getLaunchableApplications().result "

  writeStr$  = ~
  "fo.write (str (li))"

  recieve$                     =  sl4a_callBasicPy$( py$, writeStr$ ,3)
  recieve$                     =  replace $( recieve$, " " , "" )
  recieve$                     =  replace $( recieve$, "u'", "" )
  recieve$                     =  replace $( recieve$, "'" , "" )
  recieve$                     =  replace $( recieve$, "{" , "" )
  recieve$                     =  replace $( recieve$, "}" , "" )
  recieve$                     =  replace $( recieve$, "," ,CHR$(10) )
  FN.RTN                          recieve$

 FN.END
 !---------------------------------------------------------

 !---------------------------------------------------------
 FN.DEF sl4a_SetSingleChoice ( items $[] )

  ARRAY.LENGTH len, items $[]
  tmp$="'"
  FOR i=1 TO len -1
   tmp$=tmp$ + items $[i]+"' , '"
  NEXT
  tmp$=tmp$ + items $[i] +"'"

  py$ = ~
  "title = 'Please Select...'"          +CHR$(10)+~
  "droid.dialogCreateAlert(title)"      +CHR$(10)+~
  "droid.dialogSetSingleChoiceItems([" +  tmp$ + "])"   +CHR$(10)+~
  "droid.dialogSetPositiveButtonText('Yay!')"           +CHR$(10)+~
  "droid.dialogShow()"      +CHR$(10)+~
  "response = droid.dialogGetResponse().result"        +CHR$(10)+~
  "response2 = droid.dialogGetSelectedItems().result"

  writeStr$  = ~
  "fo.write ( str (response2[0]))"

  recieve$                     = sl4a_callBasicPy$( py$,writeStr$ ,6000)
  FN.RTN                          VAL(recieve$)+1

 FN.END
 !---------------------------------------------------------


 !---------------------------------------------------------
 FN.DEF sl4a_SetMultiChoice ( items $[] )

  ARRAY.LENGTH len, items $[]
  tmp$="'"
  FOR i=1 TO len -1
   tmp$=tmp$ + items $[i]+"' , '"
  NEXT
  tmp$=tmp$ + items $[i] +"'"

  py$ = ~
  "title = 'Please Select....'"                     +CHR$(10)+~
  "droid.dialogCreateAlert(title)"      +CHR$(10)+~
  "droid.dialogSetMultiChoiceItems([" +  tmp$ + "])"   +CHR$(10)+~
  "droid.dialogSetPositiveButtonText('Yay!')"           +CHR$(10)+~
  "droid.dialogShow()"      +CHR$(10)+~
  "response = droid.dialogGetResponse().result"        +CHR$(10)+~
  "response2 = droid.dialogGetSelectedItems().result"

  writeStr$  = ~
  "for i in response2:"         +CHR$(10)+~
  "  fo.write (str (i)+',')"

  recieve$                   =  sl4a_callBasicPy$( py$, writeStr$ ,6000)
  SPLIT                           recieve$[], recieve$,","
  ARRAY.LENGTH                    len, recieve$[]
  LIST.CREATE                     n,out
  FOR i                           =1 TO len
   LIST.ADD                        out, val ( recieve$[i] )+1
  NEXT
  FN.RTN                          out

 FN.END
 !---------------------------------------------------------


 !---------------------------------------------------------
 FN.DEF sl4a_recorderCaptureVideo ( vidPath$,recDuration)
  tmp $=str $( recDuration)
  py$ = ~
  "droid.recorderCaptureVideo('"+vidPath$ +"',"+ tmp $ + ",1)"
  CALL               sl4a_callBasicPy$( py$, "" ,0)
 FN.END
 !---------------------------------------------------------


 !---------------------------------------------------------
 FN.DEF sl4a_datePicker$()

  py$ = ~
  "droid.dialogCreateDatePicker(2013) "       +CHR$(10)+~
  "droid.dialogShow()"                        +CHR$(10)+~
  "date = droid.dialogGetResponse().result"   +CHR$(10)+~
  "droid.dialogCreateAlert('xx', 'yy')"

  writeStr$  = ~
  "fo.write (str (date['day'])+','+str(date['month'])+',' + str(date['year']))"

  recieve$                 =  sl4a_callBasicPy$( py$, writeStr$,6000)
  FN.RTN                          recieve$

 FN.END
 !---------------------------------------------------------


 !---------------------------------------------------------
 FN.DEF sl4a_timePicker$()

  py$ = ~
  "droid.dialogCreateTimePicker() "           +CHR$(10)+~
  "droid.dialogShow()"                        +CHR$(10)+~
  "time = droid.dialogGetResponse().result"   +CHR$(10)+~
  "droid.dialogCreateAlert('xx', 'yy')"

  writeStr$  = ~
  "fo.write ( str(time['hour'])+','+ str(time['minute'])+','+ time['which'] )"


  recieve$                   = sl4a_callBasicPy$(py$,writeStr$,6000)
  FN.RTN                          recieve$

 FN.END
 !---------------------------------------------------------




 !---------------------------------------------------------
 ! private function(s) ------------------------------------
 !---------------------------------------------------------

 FN.DEF sl4a_callBasicPy$( py$, writeStr$ ,  tOut )

  fileTx$         =   "../../sl4a/scripts/basicTx.py"
  intentPath$     = "/sdcard/sl4a/scripts/basicTx.py"
  fileRx$         =   "../../sl4a/scripts/basicRx.txt"
  FILE.EXISTS       feRx, fileRx$
  IF feRx           THEN FILE.DELETE  nn, fileRx$

  IF ! tOut         THEN un$="#" ELSE un$=""

  py$ = ~
  "import android" +CHR$(10)+~
  "droid = android.Android()"+CHR$(10) + py$ +CHR$(10)+~
  un$+"fo = open( '/sdcard/sl4a/scripts/basicRx.txt' , 'w')" +CHR$(10)+~
  un$+ writeStr$  +CHR$(10)+~
  un$+ "fo.close ()"
  !print py $

  TEXT.OPEN          w, fid, fileTx$
  TEXT.WRITELN       fid, py$
  TEXT.CLOSE         fid
  cmd$ =        "am start "
  cmd$ = cmd$ + "-a com.googlecode.android_scripting.action.LAUNCH_BACKGROUND_SCRIPT "
  cmd$ = cmd$ + "-n com.googlecode.android_scripting/.activity.ScriptingLayerServiceLauncher "
  cmd$ = cmd$ + "-e com.googlecode.android_scripting.extra.SCRIPT_PATH "
  cmd$ = cmd$ + intentPath$

  SYSTEM.OPEN
  SYSTEM.WRITE         cmd$
  PAUSE                100
  SYSTEM.CLOSE

  tOut                =  tOut*1000
  tic=clock ()
  DO
   FILE.EXISTS        fe2, fileRx$
  UNTIL               fe2 | clock ()-tic> tOut

  PAUSE               30
  IF fe2              THEN GRABFILE  output$ , fileRx$

  !PRINT               output$


  FN.RTN              output$

 FN.END
 !---------------------------------------------------------


 RETURN
 !---------------------------------------------------------






