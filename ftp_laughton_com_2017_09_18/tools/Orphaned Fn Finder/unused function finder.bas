! orphan function finder
!
! must combine all includes into one .bas file
!
!

INCLUDE choosefile.bas

f$=choosefile$("/../")

LIST.CREATE s,fl

GRABFILE         dat$,f$
dat$           = LOWER$(REPLACE$(dat$," ",""))

tic            = CLOCK()

DO

 pos           = IS_IN("fn.def",dat$,pos)

 IF pos 

  pos2         = IS_IN ( "(",  dat$,  pos)
  funName$     = MID$  ( dat$, pos+6, pos2-pos-6)

! uncomment for sorted list
!  LIST.SIZE fl,siz
!  IF !siz
   LIST.ADD fl,funName$
!  ELSE
!   j=0
!   DO 
!    j++
!    LIST.GET fl,j,l$
!   UNTIL LEN(funName$)>=LEN(l$) | j>=siz
!   LIST.INSERT fl,j,funName$
!  ENDIF

  pos          = pos2

 ENDIF

UNTIL            !pos


! PRINT            CLOCK()-tic


LIST.SIZE fl,z

? INT$(z) + " functions"
IF z=0 THEN EXIT

tic            = CLOCK()


GRABFILE         dat$,f$
dat$           = LOWER$(REPLACE$(dat$," ",""))
LIST.CREATE S, unused

FOR i=1 TO z
 LIST.GET fl,i,n$
 pos=IS_IN(n$+"(", dat$)
 IF pos>0 & MID$(dat$,pos-6,6)="fn.def" 
  pos += 6+LEN(n$)
  pos=IS_IN(n$+"(", dat$, pos)
 ENDIF
 IF !pos THEN LIST.ADD unused,n$
NEXT i

PRINT    CLOCK()-tic


LIST.SIZE unused,z
IF !z 
 ? "no orphans found"
 END
ENDIF

?INT$(z)+" possible orphan fns"

FOR i=1 TO z
 LIST.GET unused,i,n$
 ? n$
NEXT i



END
