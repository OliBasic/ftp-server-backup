 FN.DEF roundButtons(Txt$,TxtSize)
	! TxT$ is comma separated text (number of words = number of buttons)
 	SPLIT T$[],Txt$,","
	ARRAY.Length L,T$[]
	LL=0
	LT=1
	FOR i=1 TO L
		IF LEN(T$[i])>LL THEN
			LL=LEN(T$[i])
			LT=i
		ENDIF
	NEXT i
	GR.TEXT.SIZE TxtSize*1.3
	GR.GET.TEXTBOUNDS T$[LT],l1,t1,r1,b1
	Stroke=TxTSize/4
	b=1.5*r1
	h=2.3*-t1
	x=b/2+Stroke/2
	y=h/2+Stroke/2
	r=30
	GR.SET.STROKE Stroke
	GR.TEXT.ALIGN 2
	GR.TEXT.BOLD 1
	LIST.CREATE N,Buttons
	LIST.ADD Buttons,L
	FOR i=1 TO L
		GR.BITMAP.CREATE P,b+Stroke+1,h+Stroke+1
		GR.BITMAP.DRAWINTO.START P
			if b<2*r then r=b/2
			if  h<2*r then r=h/2
			half_pi=3.14159/2
			dphi=half_pi/8
			LIST.CREATE N,S1
			mx=-b/2+r
			my=-h/2+r
			FOR phi=0 TO half_pi STEP dphi
				LIST.ADD s1,mx-COS(phi)*r,my-sin(phi)*r
			NEXT phi
			mx=b/2-r
			my=-h/2+r
			FOR phi=0 TO half_pi STEP dphi
				LIST.ADD s1,mx+SIN(phi)*r,my-cos(phi)*r
			NEXT phi
			mx=b/2-r
			my=h/2-r
			FOR phi=0 TO half_pi STEP dphi
				LIST.ADD s1,mx+COS(phi)*r,my+sin(phi)*r
			NEXT phi
			mx=-b/2+r
			my=h/2-r
			FOR phi=0 TO half_pi STEP dphi
				LIST.ADD s1,mx-SIN(phi)*r,my+cos(phi)*r
			NEXT phi
			C$=RIGHT$("000"+BIN$(i),3)
			! Inside
			GR.COLOR 255,VAL(MID$(C$,2,1))*255,VAL(MID$(C$,3,1))*255,VAL(MID$(C$,1,1))*255,1
			GR.POLY nn0,s1,x,y
			! Border shadow
			GR.COLOR 255,255,255,255,0
			GR.POLY nn1,s1,x,y
			! Border
			GR.COLOR 255,120,120,140,0
			GR.POLY nn2,s1,x+1,y+1
			! Text
			GR.COLOR 255,VAL(MID$(C$,1,1))*255,VAL(MID$(C$,2,1))*255,VAL(MID$(C$,3,1))*255,1
			GR.TEXT.DRAW g,b/2+Stroke/2,h/2+h/6+Stroke/2,T$[i]
			GR.COLOR 255,255,0,0,0
		GR.BITMAP.DRAWINTO.END
		LIST.ADD Buttons,P
	NEXT i
	UNDIM T$[]
	LIST.ADD Buttons,b+Stroke+1,h+Stroke+1
	! Buttons contains: number of pointers + pointers to drawable bitmaps + their width + their height
	FN.RTN Buttons
 FN.END

GR.OPEN 255,0,0,0,0,0
PAUSE 1000
GR.SCREEN ws,hs
ScaleX=720
ScaleY=hs/ws*ScaleX
sx=ws/ScaleX
sy=hs/ScaleY
GR.SCALE sx,sy
WAKELOCK 3
BtnTxt$="YES,NO,Cancel"
Buttons=roundButtons(BtnTxt$,25)
GR.SET.STROKE 3
GR.TEXT.ALIGN 1
GR.TEXT.BOLD 0
GR.TEXT.SIZE 50
GR.COLOR 255,0,255,0,1
Q$="DO YOU LIKE THIS ?"
GR.TEXT.DRAW g,104,104,Q$
GR.COLOR 255,255,255,255,1
GR.TEXT.DRAW g,100,100,Q$
GR.COLOR 255,255,0,0,0
GR.TEXT.DRAW g,100,100,Q$
GR.RENDER
! Retrieve number of bitmaps
LIST.GET Buttons,1,NumOfPntrs
FOR i=1 TO NumOfPntrs
	! Retrieve pointers to the bitmaps
	LIST.GET Buttons,i+1,BMP
	GR.BITMAP.DRAW g1,BMP,50+(i-1)*200,300
NEXT i
GR.RENDER
! Retrieve width and height of bitmaps
LIST.GET Buttons,NumOfPntrs+2,BWidth
LIST.GET Buttons,NumOfPntrs+3,BHeight
Tapped=0
DO
	DO
		GR.TOUCH touched,x,y
	UNTIL touched
	DO
		GR.TOUCH touched,x,y
	UNTIL !touched
	x/=sx
	y/=sy
	FOR i=1 TO NumOfPntrs
		IF x>50+(i-1)*200 & x<50+(i-1)*200+BWidth & Y>300 & Y<300+BHeight THEN Tapped=i
	NEXT i
UNTIL Tapped>0
GR.CLS
A$=WORD$(BtnTxt$,Tapped,",")
Q$="WOW, you tapped "+A$
GR.COLOR 255,255,0,0,0
GR.TEXT.DRAW g,50,100,Q$
GR.RENDER
PAUSE 2000
WAKELOCK 5
EXIT
