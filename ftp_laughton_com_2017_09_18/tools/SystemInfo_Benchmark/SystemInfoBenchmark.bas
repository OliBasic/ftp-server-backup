REM ***************************************
! System Info / Benchmark
! System info + Speed Test v1.0 August 2013
! (C) [Antonis] (tony_gr)
! This program is free for personal use
! Use it at your own risk

urlErr=0
WAKELOCK 4

FN.DEF xtime()
 FN.RTN CLOCK()/1000
FN.END

LIST.CREATE N, pointers % 1 pointers to graphic ents
LIST.CREATE N, bodytext1 % 2 pointers to text fontSize=30
LIST.CREATE N, bodytext2 % 3 pointers to text fontSize=23
LIST.CREATE S, bodytext % 4 text in both fontSize cases
LIST.CREATE s, auxList

LIST.CREATE S, devList % basic
LIST.CREATE S, senList % basic
LIST.CREATE S, memList % % cat /proc/meminfo, dalvik getprop
LIST.CREATE S, stoList % df /sdcard, df
LIST.CREATE S, gpsList % basic
LIST.CREATE S, netList % netcfg +tcp getprop
LIST.CREATE S, cpuList % cat /proc/cpuinfo
LIST.CREATE S, scrList % cat /...
LIST.CREATE S, batList % basic
LIST.CREATE S, gsmList % get prop
LIST.CREATE S, buiList % get prop
LIST.CREATE S, feaList % pm list features
LIST.CREATE S, SysList % get prop
LIST.CREATE S, proList % ps, processes
LIST.CREATE S, serList % services list
LIST.CREATE S, pmaList % pm list packages -f
LIST.CREATE S, SystemList % above 3
LIST.CREATE S, pmmList % pm list features

ARRAY.LOAD section1$[], "DEVICE","SCREEN","MEMORY", "PROCESSOR","BUILD"~
"INTERNET", "BATTERY", "STORAGE"
ARRAY.LOAD section2$[], "UP TIME", "SENSORS", "GPS", "GSM"~
"CAMERA","FEATURES", "COUNTRY", "SYSTEM"

ARRAY.LOAD Phones$[],"Huawei Joy/Vodafone 845", "Samsung Galaxy Ace 2", "Sony Xperia neo V", "Samsung Galaxy I5500", "LG Optimus L5 E610","  my device"
ARRAY.LOAD Tablets$[], "Motorola Xoom 10.1\"", "Nova Pad 70 C124FC", "Asus Transformer TF101", "Google Nexus 7\"", "Samsung Galaxy Note 10.1","  my device"

ARRAY.LOAD sectDet1$[], "Details...","More...","Advanced...", "More...","More..."~
"Advanced...", "More...", "Advanced..."

ARRAY.LOAD sectDet2$[], "", "More...", "More...", "More..."~
"","More...", "", "Details..."

ARRAY.LOAD deys$[],"Sun ","Mon ","Tue ","Wed ","Thu ","Fri ","Sat "

DIM Section1Ptr[8] % titles
DIM Section2Ptr[8] % titles
DIM Section1more[8] % texts
DIM Section2more[8] % texts

DIM resPh[6], resTab[6] % values in secs
DIM normPh[6], normTab[6] % y values of bars
DIM androidPng[200], rct[200],text[200],cman[100]

! for window routine
topX=50
topY=130
wth=700
hei=900
fontSize=30 % 30 pix or 24 pix (34 chars or 47) (28 lines or 36 lines)

! values of tests
resPH[1]=539.2
resPH[2]=64
resPH[3]=65.6
resPH[4]=176.7
resPH[5]=88.4
resPH[6]=0
resTAB[1]=55.1
resTAB[2]=78.3
resTAB[3]=51.8
resTAB[4]=43.7
resTAB[5]=38.4
resTAB[6]=0

up$=CHR$(8679)
daun$=CHR$(8681)
clos$=CHR$(88) % 10060,10062,10005,6
lhand$=CHR$(9758)
rhand$=CHR$(9756)
ck$=CHR$(8730)
ck1$=CHR$(10004)
ar$=CHR$(10149)
ar2$=CHR$(10151)
mail$=CHR$(9993)
bg1$=CHR$(8750)+"  "+CHR$(8721)+"  "+ CHR$(8734)+" "+CHR$(8704)+" "+ CHR$(8532)+" "+ CHR$(8895)
menu$=CHR$(10064)
addFlag=1
GR.OPEN 255,30,0,30,0,1
PAUSE 800

ARRAY.LOAD extSd$[],"/mnt/extsd", "/mnt/extSdCard","/mnt/external_sd","/mnt/sdcard/external_sd","/mnt/sdcard2","/mnt/sdcard/ext_sd","/mnt/external","/mnt/sd_external1","/mnt/extsd","/mnt/ext_sd","/Removable/MicroSD","/mnt/ext_card"

ARRAY.LENGTH lExt, extSd$[]

GR.SCREEN curW, curH
sX = curW / 800
sY = curH / 1232
GR.SCALE sX , sY
! bitmaps
GR.BITMAP.LOAD glanceBmp,"ataglance.png"
GR.BITMAP.LOAD glanceBmp2,"ataglance2.png"
GR.BITMAP.LOAD sysBmp,"systeminfo.png"

! ******************************
restart:
k=180
maxTout=2000 % for system commands
host$=" "

! MENU
GR.CLS
GR.TEXT.TYPEFACE 4
GR.COLOR 255,255,255,255,1
GR.TEXT.SIZE 87
FOR i=0 TO 20
 GR.COLOR 10+i*5, 205,164,57,1
 GR.RECT r, 0,i*60, 800, i*60+60
NEXT
GR.SET.STROKE 0
GR.TEXT.BOLD 1
GR.COLOR 255,255,255,255,1
GR.TEXT.DRAW t, 270,300 ,"Speed Test"
GR.TEXT.BOLD 0
GR.COLOR 255,164,196,57,1
GR.TEXT.DRAW t, 270,300 ,"Speed Test"
GR.TEXT.BOLD 1
GR.COLOR 255,255,255,255,1
GR.TEXT.DRAW t, 270,550 ,"System Info"
GR.TEXT.BOLD 0
GR.COLOR 255,164,196,57,1
GR.TEXT.DRAW t, 270,550 ,"System Info"

GR.TEXT.TYPEFACE 1
GR.TEXT.BOLD 1
GR.BITMAP.DRAW syst,sysBmp,40,200
GR.COLOR 255,127,127,127,0
GR.RENDER
! pause 100
! gr.render

quitFlag=0
tme:

GOSUB waitForTouch

IF y>170*sY & y<370*sY  THEN
 ! Benchmark
 GR.COLOR 255,164,196,57,1
 GR.TEXT.SIZE 50
 GR.TEXT.DRAW tt,15, 250,CHR$(8688)
 GR.RENDER
 TONE 350,150,0
 ag=glanceBmp
 GR.BITMAP.CROP graph, ag, 400,95,100,100
 GR.BITMAP.CROP proc, ag, 405,330,95,90
 GR.BITMAP.CROP sd, ag, 420,795,75,90
 GR.BITMAP.CROP andro, ag, 10,550,90,110
 GR.BITMAP.SAVE andro, "android.png"

 ! PAUSE 400
 GR.CLS
 gr.color 30,255,255,0,1
 GR.TEXT.SIZE 120
 GR.TEXT.BOLD 1
 k=10
 j=150
 FOR l=1 TO 5
  GR.TEXT.DRAW tt,10,j,bg1$
  j=j+250
 NEXT
 GR.TEXT.BOLD 0
 GR.TEXT.SIZE 45
 GR.COLOR 255,255,255,0,1

 GR.TEXT.DRAW tt,10,150, "For best results:"
 GR.COLOR 255,255,55,255,1
 GR.TEXT.DRAW tt,10,250, CHR$(9312)+ " Turn off WiFi/Data connection."
 GR.TEXT.DRAW tt,10,350, CHR$(9313)+ " Close other opened apps."
 GR.TEXT.DRAW tt,10,450, CHR$(9314)+ " Run test before accessing Info."
 ! GR.TEXT.DRAW tt,10,550, CHR$(9315)+ " Run full screen/no multiwindow."
 GR.TEXT.SIZE 36
 GR.COLOR 255,155,255,95,1
 GR.TEXT.DRAW tt,10,975, " Note:"
 GR.TEXT.DRAW tt,10,1015, " Result depends on device load at test time."
 X=105
 Y=750
 Bhei=90
 Bwth=550
 text$="I'm ready!"
 txtsize=57
 type=4 % 1=Default font, 2=Monospace font, 3=San Serif font, 4=Serif
 shadow=3 % pixels
 GR.TEXT.BOLD 1
 GOSUB roundedbutton
 GR.RENDER
 DO
  GOSUB waitForTouch
 UNTIL x>105*sx & y>sy*750 & x<675*sx & y<sy*840
 GR.SHOW pressedBtnText
 TONE 350,100,0
 GR.RENDER
 PAUSE 200

 ! start test
 GR.CLS
 gr.color 30,255,255,0,1
 GR.TEXT.SIZE 120
 GR.TEXT.BOLD 1
 GR.TEXT.TYPEFACE 3
 k=10
 j=150
 FOR l=1 TO 5
  GR.TEXT.DRAW tt,10,j,bg1$
  j=j+250
 NEXT
 GR.TEXT.BOLD 0
 GR.COLOR 255,255,255,0,1
 GR.BITMAP.DRAW aag, proc,130,320
 GR.BITMAP.DRAW aag, graph,130,470
 GR.BITMAP.DRAW aag, sd,140,620
 GR.RENDER

 GOSUB speedTest

 GOSUB graphicsTest

 GR.COLOR 255,30,0,30,1
 GR.RECT r,0,0,800,310
 gr.color 30,255,255,0,1
 GR.TEXT.SIZE 120
 GR.TEXT.BOLD 1
 GR.TEXT.DRAW tt,10,150,bg1$
 GR.TEXT.BOLD 0

 GOSUB SdTest
 GR.SET.STROKE 5
 GR.COLOR 255, 205, 205, 205, 1
 GR.RECT r, 25,750,775, 815
 GR.TEXT.BOLD 1
 GR.COLOR 255, 25, 155, 255, 1
 GR.TEXT.SIZE 45
 TotSecs=TotSd+TotGr+mTimeUsed
 TotSecs=ROUND(totSecs*10)/10
 TotSecs$="Total: "+STR$(TotSecs)+" secs"
 GR.COLOR 255, 25, 155, 255, 0
 GR.RECT r, 25,750,775, 815
 GR.RECT r, 25,200,775, 265
 GR.COLOR 255, 25, 155, 255, 1
 GR.TEXT.ALIGN 2
 GR.TEXT.DRAW tx,400,250,"Test completed!"
 GR.COLOR 255, 255, 55, 25, 1
 GR.TEXT.DRAW tx,400,800,TotSecs$
 GR.TEXT.ALIGN 1

 GR.RENDER
 TONE 350,100,0
 text$="Next"
 GOSUB btnClick
 GR.CLS
 gr.render
 resPH[6]= TotSecs
 resTAB[6]=TotSecs

 GOSUB viewResults

 GR.BITMAP.DELETE graph
 GR.BITMAP.DELETE proc
 GR.BITMAP.DELETE sd
 quitFlag=2
 text$="Menu"
 GOSUB btnClick
ENDIF % speed test

IF y>370*sY & y<670*sY  & !quitFlag THEN
 ! System info
 GR.COLOR 255,164,196,57,1
 GR.TEXT.SIZE 50
 GR.TEXT.DRAW tt,15, 530,CHR$(8688)
 GR.RENDER
 TONE 350,150,0
 PAUSE 400
 GR.CLS
 GOSUB systemInfo
ENDIF

IF quitFlag=2 THEN goto restart
GOTO tme

RETURN


! ******************************
! SYSTEM INFO
systemInfo:
! background
GR.CLS

GR.TEXT.SIZE 129
GR.COLOR 55,127,127,127,1

FOR j=1 TO 12
 GR.TEXT.DRAW tx, 0,(j-1)*155+110,"1 0 1 0 1 0 1 0"
NEXT j
GR.TEXT.BOLD 0
GR.TEXT.ALIGN 2
! GR.TEXT.SIZE 20
! GR.COLOR 255,255,55,255,1
! GR.TEXT.DRAW LOGO,400,1225,"EASY SYSTEM INFO v1.0"
GR.TEXT.SIZE 42
GR.COLOR 255,255,255,25,1
GR.TEXT.DRAW InfoTitle,400,55,"Retrieving System Information, pls wait..."
GR.TEXT.SIZE 37
GR.TEXT.ALIGN 1
GR.COLOR 255,105,20,15,1
GR.RECT r0, 100,85,700,135
GR.COLOR 255,105,20,15,1
GR.ARC arc1, 680,89,720,131 ,270,180, 0
GR.ARC arc1, 80,89,120,131 , 90,180, 0
GR.COLOR 255,255,255,255,0
GR.SET.STROKE 6
! GR.RECT r1, 100,85,700,135
GR.LINE l, 100,85,700,85
GR.LINE l, 100,135,700,135
GR.COLOR 255,0,205,0,1
GR.RECT progessRect, 100,85,100,135
! GR.COLOR 255,0,255,255,1
! GR.COLOR 255,0,165,120,1 % light green
! GR.COLOR 255,0,150,195,1 % light blue
GR.COLOR 255,255,255,255,0
GR.ARC arc1, 680,85,720,135 ,270,180, 0
GR.ARC arc1, 80,85,120,135 , 90,180, 0

GR.COLOR 255,0,205,0,1
GR.ARC arc1, 80,85,120,135 , 90,180, 0
GR.ARC arc2, 680,85,720,135 ,270,180, 0
GR.HIDE arc1
GR.HIDE arc2

GR.COLOR 255,240,180,30,1 % orange

d=600/23
progress=100 % starting pixel

! GOTO here

! *********
SYSTEM.OPEN
! *********

! sdcard
xx=CLOCK()
GR.TEXT.DRAW tx,50,k,"Gathering SD card info..."
GR.RENDER
card$="/sdcard"
GOSUB sdcard
GR.SHOW arc1
GOSUB updateProgress
GR.MODIFY tx,"text","Gathering SD card info..."+ck1$

! starting with sensors 1
k=k+40
GR.TEXT.DRAW tx,50,k,"Gathering Sensor info..."
GR.RENDER
PAUSE 200
ARRAY.DELETE list$[]
ARRAY.DELETE Slist$[]
SENSORS.LIST list$[]
ARRAY.LENGTH SensorsL, list$[]
DIM Slist$[SensorsL]
FOR i=1 TO SensorsL
 LIST.ADD SenList, list$[i]
 a=IS_IN(",", list$[i])
 Slist$[i]=LEFT$(list$[i],a-1)
NEXT i
SENSORS.CLOSE

batPath$="battery"
FILE.EXISTS ex,"../../../../../../sys/class/power_supply/battery/"
IF ex=0 THEN
 ARRAY.DELETE batArray$[]
 FILE.DIR "../../../../../../sys/class/power_supply/" , batArray$[]
 ARRAY.LENGTH bl, batArray$[]
 batPath$=""
 FOR i=1 TO bl
  k$=batArray$[i]
  a=IS_IN("battery",k$)
  IF a>0 THEN
   aka=IS_IN("(d)",k$)
   IF aka>0 THEN
    batPath$=REPLACE$(k$,"(d)","")
    F_N.BREAK
   ENDIF
  ENDIF
 NEXT i
ENDIF

! used later
ll$=""
MoreGetProp$=""
SYSTEM.WRITE "getprop"
GOSUB waitForData
IF !ttimedOut THEN
 DO
  SYSTEM.READ.LINE l$
  MoreGetProp$=MoreGetProp$+l$
  a=IS_IN("[net.hostname]:",l$ )
  IF a>0 THEN
   host$=REPLACE$(l$,"[net.hostname]:","")
   host$=MID$(host$,3,LEN(host$)-3)
  ENDIF
  a=IS_IN("[gps.version.driver]:",l$)
  IF a>0 THEN  gpsdrv$=REPLACE$(l$,"[gps.version.driver]","Driver version")
  l$=REPLACE$(l$,"[","<")
  l$=REPLACE$(l$,"]",">")
  ll$=ll$+l$+"#"
  SYSTEM.READ.READY ready
 UNTIL !ready
ENDIF
GR.MODIFY tx,"text", "Gathering Sensor info..."+ck1$
GOSUB updateProgress

! later use
ARRAY.DELETE lines$[]
ARRAY.DELETE MoreLines$[]
SPLIT lines$[], ll$ , "#"
ARRAY.LENGTH lines,lines$[]
SPLIT MoreLines$[], Morell$ , "#"
ARRAY.LENGTH MoreLines,MoreLines$[]
! later use

! GPS 2
k=k+40
GR.TEXT.DRAW tx,50,k,"Gathering Gps info..."
GR.RENDER
PAUSE 100
GPS.OPEN
PAUSE 300
GPS.PROVIDER g$
lGps=LEN(g$)
LIST.ADD gpsList, "Provider: "+g$
prGps$=g$
gps=LEN(g$)
IF gps<>0 THEN gps=2
GPS.ACCURACY g
g$=STR$(g)
LIST.ADD gpsList, "Accuracy: "+g$
GPS.LATITUDE g
g$=STR$(g)
LIST.ADD gpsList, "Latitude: "+g$
laGps$=g$
GPS.LONGITUDE g
g$=STR$(g)
loGps$=g$
LIST.ADD gpsList, "Longitude: "+g$
GPS.ALTITUDE g
g$=STR$(g)
LIST.ADD gpsList, "Altitude: "+g$
GPS.BEARING g
g$=STR$(g)
LIST.ADD gpsList, "Bearing: "+g$
GPS.SPEED g
g$=STR$(g)
LIST.ADD gpsList, "Speed: "+g$
GPS.TIME g
g$=STR$(g)
LIST.ADD gpsList, "Gps time: "+g$
GPS.CLOSE
GR.MODIFY tx,"text", "Gathering Gps info..." +ck1$
GOSUB updateProgress
LIST.ADD gpsList, gpsdrv$


! device 3
k=k+40
GR.TEXT.DRAW tx,50,k,"Gathering Device info..."
GR.RENDER
DEVICE dv$
a1=IS_IN("Pro",dv$)
a2=IS_IN("OS",dv$,a1)
pro$=MID$(dv$,a1+10,a2-a1-10)
ddv$=dv$
a2=IS_IN(CHR$(10),ddv$)
bra$=MID$(ddv$,9,a2-9)
ddv$=RIGHT$(ddv$,LEN(ddv$)-a2)
a2=IS_IN(CHR$(10),ddv$)
mdl$=MID$(ddv$,9,a2-9)
ddv$=RIGHT$(ddv$,LEN(ddv$)-a2)
a1=IS_IN(CHR$(10),ddv$)
dev$=MID$(ddv$,10,a1-10)
ddv$=RIGHT$(ddv$,LEN(ddv$)-a2)
a1=IS_IN("OS",ddv$)+5
aver$=MID$(ddv$,a1,LEN(ddv$)-a1+1)
dv$=REPLACE$(dv$,CHR$(10),"#")
MYPHONENUMBER ph$
IF LEFT$(ph$,3)="Get" THEN ph$="No data."
TIMEZONE.GET tz$
! LIST.ADD devList, "**Start of Info**"
FOR i=1 TO lines
 l$=lines$[i]
 a1=IS_IN(".product.",l$)
 IF a1>0 THEN
  nt$=REPLACE$(l$,".product.","")
  nt$=REPLACE$(nt$,">","")
  nt$=REPLACE$(nt$,"<","")
  nt$=REPLACE$(nt$,"ro","")
  LIST.ADD devList, nt$
 ENDIF
NEXT i
fontsize=30
lstToSplit=devList
GOSUB spltList
GR.MODIFY tx,"text", "Gathering Device info..." +ck1$
GOSUB updateProgress


! screen 4
k=k+40
GR.TEXT.DRAW tx,50,k,"Gathering Screen info..."
GR.RENDER
GR.SCREEN w,h,dpi
w$="Program Screen width: "+REPLACE$(STR$(w),".0","")
h$="Program Screen height: "+REPLACE$(STR$(h),".0","")
LIST.ADD scrList,w$
LIST.ADD scrList,h$
dpi$=REPLACE$(STR$(dpi),".0","")
! screen modes
SYSTEM.WRITE "cat /sys/class/graphics/fb0/modes"
listPtr=scrList
GOSUB getSystemData
res$=l$
SYSTEM.WRITE "cat /sys/class/graphics/fb0/bits_per_pixel"
GOSUB getSystemData
LIST.REPLACE scrList,3, "Color depth in bits: "+l$
SYSTEM.WRITE "cat /sys/class/graphics/fb0/name"
GOSUB getSystemData
LIST.REPLACE scrList,4, "Screen Name: "+l$
scn$=l$
LIST.REMOVE scrList,5
GR.MODIFY tx,"text", "Gathering Screen info..." +ck1$
GOSUB updateProgress

! net info 5
k=k+40
GR.TEXT.DRAW tx,50,k,"Gathering Net info..."
GR.RENDER
SOCKET.MYIP lip$
IF lip$="" THEN lip$=" No data."
! g$=""
UrlErr=1
IF lip$<>" No data." THEN
 GRABURL ss$,"http://whatsmyip.net/",3000 
 a1=IS_IN("address is:",ss$ )
 a2=IS_IN("/></h1>",ss$,a1)
 a1=a1+38
 a2=a2-2
 ip$=MID$(ss$,a1,a2-a1)
 ip$=REPLACE$(ip$," ","")
 UrlErr=0
 ! print ip$
 ! print ss$
 ! end
ELSE
 ip$=" No data."
ENDIF

grbUrlErr:
IF UrlErr=1 THEN
 ip$=" -"
 UrlErr=0
ENDIF

LIST.ADD netList, "**Start of Info**"
FOR i=1 TO lines
 l$=lines$[i]
 a1=IS_IN("net.",l$)
 a2=IS_IN("dhcp.",l$)
 a3=IS_IN("net.tcp",l$)
 IF (a1>0 | a2>0) & a3=0 THEN
  nt$=REPLACE$(l$,">","")
  nt$=REPLACE$(nt$,"<","")
  LIST.ADD netList, nt$
 ENDIF
NEXT i
! LIST.ADD memList, "-----------------------------------"
LIST.ADD netList, ""
LIST.ADD netList, "----Net configuration----"

listPtr=netList
SYSTEM.WRITE "netcfg"
GOSUB waitforData
IF !ttimedOut THEN
 DO
  SYSTEM.READ.LINE l$
  l$=REPLACE$(l$,"   "," ")
  l$=REPLACE$(l$,"   "," ")
  l$=REPLACE$(l$,"<","")
  l$=REPLACE$(l$,">","")
  LIST.ADD netList, l$
  SYSTEM.READ.READY ready
 UNTIL !ready
ENDIF
GR.MODIFY tx,"text", "Gathering Net info..." +ck1$
GOSUB updateProgress

! memory 6
k=k+40
GR.TEXT.DRAW tx,50,k,"Gathering Memory info..."
GR.RENDER
LIST.ADD memList, "**Start of Info**"
FOR i=1 TO lines
 l$=lines$[i]
 a1=IS_IN("<dalvik.vm.heapgrowthlimit>",l$)
 a2=IS_IN("<dalvik.vm.heapstartsize>",l$)
 a3=IS_IN("<dalvik.vm.heapsize>",l$)
 IF a1>0 THEN LIST.ADD memList, "Heap Growth Limit"+REPLACE$(l$,"<dalvik.vm.heapgrowthlimit>","")
 IF a2>0 THEN LIST.ADD memList, "Heap Start size"+REPLACE$(l$,"<dalvik.vm.heapstartsize>","")
 IF a3>0 THEN LIST.ADD memList, "Heap Size"+REPLACE$(l$,"<dalvik.vm.heapsize>","")
 IF a3>0 THEN
  memH$=REPLACE$(l$,"<dalvik.vm.heapsize>","")
  memH$=REPLACE$(memH$,">","")
  memH$=REPLACE$(memH$,"<","")
  memH$=REPLACE$(memH$,":","")
  memH$=memH$+"b"
 ENDIF
NEXT i
LIST.ADD memList, "-----------------------------------"
SYSTEM.WRITE "cat /proc/meminfo"
X=CLOCK()
DO
 PAUSE 200
 SYSTEM.READ.READY ready
UNTIL ready | CLOCK()-x>maxTout
IF CLOCK()-x<maxTout THEN
 DO
  SYSTEM.READ.LINE l$
  a1=IS_IN("MemTotal",l$)
  a2=IS_IN("MemFree",l$)
  IF a1>0 THEN
   memT$=REPLACE$(l$," ","")
   memT$=REPLACE$(memT$,"MemTotal:","")
   indx=0
   s$=""
   FOR i=1 TO LEN(memT$)
    IF ASCII(MID$(memT$,i,1))<48 | ASCII(MID$(memT$,i,1))>57 THEN
     indx=i
     s$=MID$(memT$,i,1)
     F_N.BREAK
    ENDIF
   NEXT i
   IF indx>0 THEN memT$=REPLACE$(memT$,s$," "+s$)
  ENDIF
  IF a2>0 THEN
   memF$=REPLACE$(l$," ","")
   memF$=REPLACE$(memF$,"MemFree:","")
   indx=0
   s$=""
   FOR i=1 TO LEN(memF$)
    IF ASCII(MID$(memF$,i,1))<48 | ASCII(MID$(memF$,i,1))>57 THEN
     indx=i
     s$=MID$(memF$,i,1)
     F_N.BREAK
    ENDIF
   NEXT i
   IF indx>0 THEN memF$=REPLACE$(memF$,s$," "+s$)
  ENDIF
  LIST.ADD memList, l$
  SYSTEM.READ.READY ready
 UNTIL !ready
ELSE
 LIST.ADD memList, "No data."
ENDIF
LIST.ADD memList, ""
LIST.ADD memList, "**End of Info**"
PAUSE 100
GOSUB updateProgress
GR.MODIFY tx,"text", "Gathering Memory info..." +ck1$

! file system 7
k=k+40
GR.TEXT.DRAW tx,50,k,"Gathering File system info..."
GR.RENDER
listPtr=stoList
SYSTEM.WRITE "df"
GOSUB getSystemData
LIST.SIZE stoList,S
i=0
DO
 i=i+1
 LIST.GET stoList,i,s$
 IF LEFT$(s$,10)="/mnt/asec/" THEN
  LIST.REMOVE stoList,i
  i=i-1
  s=s-1
 ENDIF
UNTIL i=s
! s$=REPLACE$(s$,"Blksize","BlkS")
LIST.SIZE stoList,S
sdFl=0
FOR i=1 TO S
 LIST.GET stoList,i,s$
 ! check for external SD
 IF sdFl=0 THEN
  FOR m=1 TO lExt
   e$=extSd$[m]
   lEx=LEN(e$)
   IF LEFT$(s$,lex)=e$ THEN
    pthToExt$=e$
    sdFl=1
    F_N.BREAK
   ENDIF
  NEXT m
 ENDIF
 ! s$=REPLACE$(s$,"  "," ")
 FOR j=1 TO LEN(s$)
  IF MID$(s$,j,1)=" " THEN indx=j
 NEXT j
 s$=LEFT$(s$,indx-1)
 LIST.REPLACE stoList,i,s$
NEXT i
IF sdFl=1 THEN  % ext sd found
 s1$=sd1$
 s2$=sd2$
 s3$=sd3$
 card$=pthToExt$
 GOSUB sdcard
 sd1$=s1$+" | "+sd1$
 sd2$=s2$+" | "+sd2$
 sd3$=s3$+" | "+sd3$
ENDIF
PAUSE 200
GOSUB updateProgress
GR.MODIFY tx,"text", "Gathering File system info..." +ck1$

! battery 8
maxTout=300
k=k+40
listPtr=batList
GR.TEXT.DRAW tx,50,k,"Gathering Battery info..."
GR.RENDER
SYSTEM.WRITE "cat /sys/class/power_supply/"+batPath$+"/capacity"
GOSUB getSystemData
IF l$<>"No data." THEN cap$=l$+"%" ELSE cap$=""
LIST.REPLACE batList,1,"Capacity: "+cap$

SYSTEM.WRITE "cat /sys/class/power_supply/"+batPath$+"/health"
GOSUB getSystemData
LIST.REPLACE batList,2,"Health: "+l$

SYSTEM.WRITE "cat /sys/class/power_supply/"+batPath$+"/status"
GOSUB getSystemData
LIST.REPLACE batList,3,"Status: "+l$

SYSTEM.WRITE "cat /sys/class/power_supply/"+batPath$+"/technology"
GOSUB getSystemData
tec$=l$
LIST.REPLACE batList,4,"Technology: "+l$

SYSTEM.WRITE "cat /sys/class/power_supply/"+batPath$+"/temp"
GOSUB getSystemData
IF LEN(l$)>2 & l$<>"No data."THEN
 ce=((VAL(l$)-32)*5)/9
 ce$=REPLACE$(FORMAT$("###.#",ce)," ","")
 temp$=l$+" "+CHR$(8457)
ELSE
 temp$="  "
 ce$=" "
ENDIF
LIST.REPLACE batList,5,"Temperature ("+CHR$(8457)+"): "+l$+" ("+ce$+CHR$(8451)+")"

SYSTEM.WRITE "cat /sys/class/power_supply/"+batPath$+"/type"
GOSUB getSystemData
LIST.REPLACE batList,6,"Type: "+l$

SYSTEM.WRITE "cat /sys/class/power_supply/"+batPath$+"/voltage_now"
GOSUB getSystemData
GR.MODIFY tx,"text", "Gathering Battery info..." +ck1$
GOSUB updateProgress

vol$=l$
IF  vol$<>"No data." THEN
 vol$=LEFT$(vol$,4)
 vol$=STR$(VAL(vol$)/1000)+"V"
ENDIF
LIST.REPLACE batList,7, "Voltage: "+vol$
maxTout=2000


! CPU 9
k=k+40
GR.TEXT.DRAW tx,50,k,"Gathering Processor info..."
GR.RENDER
listPtr=cpuList
SYSTEM.WRITE "cat /proc/cpuinfo"
GOSUB waitforData
IF !ttimedOut THEN
 DO
  SYSTEM.READ.LINE l$
  LIST.ADD cpuList,l$
  a1=IS_IN("Processor",l$)
  IF a1>0 THEN
   proc$=REPLACE$(l$,"Processor","")
   proc$=REPLACE$(proc$,": ","")
  ENDIF
  SYSTEM.READ.READY ready
 UNTIL !ready
ENDIF
ARRAY.DELETE FileArray$[]
FILE.DIR "../../../../../sys/devices/system/cpu/", FileArray$[]
ARRAY.LENGTH ll,FileArray$[]
count=0
FOR i=1 TO ll
 k$=FileArray$[i]
 a=IS_IN("cpu",k$)
 aka=IS_IN("(d)",k$)
 IF aka>0 & a>0 & LEN(k$)<9 THEN count=count+1
NEXT i
cores$=REPLACE$(STR$(count),".0","")

SYSTEM.WRITE "cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq"
GOSUB getSystemData
! LIST.add cpuList,l$
LIST.SIZE cpuList, SS
freq$=l$
LIST.REPLACE cpuList,ss, "Max proc. frequency(Hz): "+l$
SYSTEM.WRITE "cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq"
GOSUB getSystemData
! LIST.add cpuList,l$
LIST.REPLACE cpuList,ss+1, "Min proc. frequency(Hz): "+l$
fontSize=30
lstToSplit=cpuList
GOSUB spltList
GR.MODIFY tx,"text", "Gathering Processor info..." +ck1$
GOSUB updateProgress
DEBUG.OFF

! features
k=k+40
GR.TEXT.DRAW tx,50,k,"Gathering System features info..."
GR.RENDER

! used later
ll$=""
i=0
maxTout=3500
SYSTEM.WRITE "pm list features"
GOSUB waitForData
IF !ttimedOut THEN
 DO
  SYSTEM.READ.LINE l$
  LIST.ADD pmmList,l$
  k$=REPLACE$(l$,"feature:android.","")
  k$=REPLACE$(k$,"feature:","")
  a1=IS_IN("camera",k$)
  ! a2=IS_IN("bluetooth",k$)
  a3=IS_IN("sensor",k$)
  a4=IS_IN("reqGl",k$)
  a5=IS_IN("location",k$)
  IF a1=0 & a3=0 & a4=0 THEN
   k$= REPLACE$(k$,"hardware.","")
   k$= REPLACE$(k$,"software.","")
   k$= REPLACE$(k$,"."," ")
   LIST.ADD feaList, k$
  ENDIF
  SYSTEM.READ.READY ready
 UNTIL !ready
ENDIF
LIST.SIZE  feaList, Sysfeatures
IF Sysfeatures>0 THEN LIST.GET feaList,1,fea1$
IF Sysfeatures>1 THEN LIST.GET feaList,2,fea2$
IF Sysfeatures>2 THEN LIST.GET feaList,3,fea3$
ARRAY.DELETE PMlines$[]
LIST.TOARRAY pmmlist, PMlines$[]
ARRAY.LENGTH PMlines,PMlines$[] % for later use

fontSize=30
lstToSplit=feaList
GOSUB spltList
maxTout=2000
GR.MODIFY tx,"text", "Gathering System features info..." +ck1$
GOSUB updateProgress

! Camera 11
addFlag=0
k=k+40
GR.TEXT.DRAW tx,50,k,"Gathering Camera info..."
GR.RENDER
! camera and some bt
cc=0
camF1$=""
camF2$=""
camF3$=""
camF4$=""
bt$="No"
FOR i=1 TO PMlines
 l$=PMlines$[i]
 a=IS_IN("camera",l$)
 IF a>0 THEN
  cam$=l$
  IF cam$="feature:android.hardware.camera" THEN campres$="Yes."
  IF LEFT$(cam$,32)="feature:android.hardware.camera." THEN
   cc=cc+1
   cam$=REPLACE$(l$,"feature:android.hardware.camera.","")
   IF cc=1 THEN camF1$=cam$
   IF cc=2 THEN camF2$=cam$
   IF cc=3 THEN camF3$=cam$
   IF cc=4 THEN camF4$=cam$
   l$=REPLACE$(l$,"feature:android.","")
   l$=REPLACE$(l$,"feature:","")
   l$=REPLACE$(l$,"hardware.","")
  ENDIF
 ENDIF
 ! IF l$="feature:android.hardware.bluetooth" THEN bt$="Yes"
NEXT i

PAUSE 100

GR.MODIFY tx,"text", "Gathering Camera info..." +ck1$
GOSUB updateProgress
addFlag=1

! disk stats 12
k=k+40
GR.TEXT.DRAW tx,50,k,"Gathering Disk statistics info..."
GR.RENDER
LIST.ADD stoList,""
maxTout=500
listPtr=stoList
SYSTEM.WRITE "dumpsys diskstats"
GOSUB getSystemData
LIST.SIZE stoList,S
FOR i=1 TO S
 LIST.GET stoList,i,s$
 IF LEFT$(s$,7)="Latency" THEN
  LIST.INSERT stoList,i+1," "
  s=s+1
 ENDIF
NEXT
LIST.ADD stoList, ""
LIST.ADD stoList,"*** End of Info ***"
LIST.SIZE stoList,S
FOR i=1 TO s
 LIST.GET stoList,i,s$
 IF s$="No data." THEN LIST.REPLACE stoList,i,""
 s$=REPLACE$(s$," / ","/")
 s$=REPLACE$(s$," : ",":")
 s$=REPLACE$(s$," = ","=")
 LIST.REPLACE stoList,i,s$
NEXT i
fontSize=24
lstToSplit=stoList
GOSUB spltList
PAUSE 200
GR.MODIFY tx,"text", "Gathering Disk statistics info..." +ck1$
GOSUB updateProgress
maxTout=2000

! up-time 13
k=k+40
GR.TEXT.DRAW tx,50,k,"Gathering Up-time info..."
GR.RENDER
upt1$=""
SYSTEM.WRITE "uptime"
GOSUB waitForData
IF !ttimedOut THEN
 DO
  SYSTEM.READ.LINE l$
  upt$=l$
  l$=REPLACE$(l$,", ","#")
  SYSTEM.READ.READY ready
 UNTIL !ready
ENDIF
upt1$=WORD$(upt$,1,",")
upt2$=WORD$(upt$,2,",")
upt3$=WORD$(upt$,3,",")
upt2$=RIGHT$(upt2$,LEN(upt2$)-1)
upt3$=RIGHT$(upt3$,LEN(upt3$)-1)
upt1$=REPLACE$(upt1$,"up time:","")
upt2$=REPLACE$(upt2$,"idle time:","")
upt3$=REPLACE$(upt2$,"sleep time:","")
uptFlag=1
! upt1$=""
IF upt1$="" THEN
 mn=0
 ho=0
 da=0
 screst=0
 uptFlag=0
 sc=CLOCK()/1000 % secs
 mn=FLOOR(sc/60)
 screst=MOD(sc,60)
 IF mn>=60 THEN
  ho=FLOOR(mn/60)
  mn=MOD(mn,60)
  IF ho>=24
   da=FLOOR(ho/24)
   ho=MOD(ho,24)
  ENDIF
 ENDIF
 upt1$=REPLACE$(STR$(da),".0","")+" d."
 upt2$=REPLACE$(STR$(ho),".0","")+" h."
 upt3$=REPLACE$(STR$(mn),".0","")+" m."
ENDIF
GR.MODIFY tx,"text", "Gathering Up-time info..." +ck1$
GOSUB updateProgress


! gsm info 14
k=k+40
GR.TEXT.DRAW tx,50,k,"Gathering Gsm info..."
GR.RENDER
gsmFlag=0
cc=0
FOR i=1 TO lines
 l$=lines$[i]
 a=IS_IN("gsm.",l$)
 aa=IS_IN("bodysargsm",l$)
 IF a>0 & aa=0 THEN
  aka=IS_IN("gsm.operator.alpha",l$)
  IF aka>0 THEN
   gsm1$=REPLACE$(l$,"gsm.operator.alpha","")
   gsm1$=REPLACE$(gsm1$,">","")
   gsm1$=REPLACE$(gsm1$,"<","")
   gsm1$=REPLACE$(gsm1$,":","")
  ENDIF
  aka=IS_IN("gsm.sim.operator.alpha",l$)
  IF aka>0 THEN
   gsm2$=REPLACE$(l$,"gsm.sim.operator.alpha","")
   gsm2$=REPLACE$(gsm2$,">","")
   gsm2$=REPLACE$(gsm2$,"<","")
   gsm2$=REPLACE$(gsm2$,":","")
  ENDIF
  gsmFlag=1
  l$=REPLACE$(l$,"gsm.","")
  l$=REPLACE$(l$,">","")
  l$=REPLACE$(l$,"<","")
  LIST.ADD gsmList,l$
 ENDIF
NEXT i
IF gsmFlag=0 THEN List.add gsmList, "GSM: No data."
fontSize=30
lstToSplit=gsmList
GOSUB spltList
GR.MODIFY tx,"text", "Gathering Gsm info..." +ck1$
GOSUB updateProgress

! build info 16
k=k+40
GR.TEXT.DRAW tx,50,k,"Gathering Build info..."
GR.RENDER
FOR i=1 TO lines
 l$=lines$[i]
 a=IS_IN("<ro.build.",l$)
 IF a>0 THEN
  mb$=REPLACE$(l$,"<ro.build.","")
  mb$=REPLACE$(mb$,">","")
 ENDIF
 a=IS_IN("<ro.build.version.sdk>",l$)
 IF a>0 THEN
  bver$=REPLACE$(l$,"<ro.build.version.sdk>","")
  bver$=REPLACE$(bver$,">","")
  bver$=REPLACE$(bver$,"<","")
  bver$=REPLACE$(bver$,":","")
 ENDIF

 a=IS_IN("<ro.build.date>",l$)
 IF a>0 THEN
  k$=""
  FOR m=1 TO LEN(l$)
   IF ASCII(MID$(l$,m,1))>31 & ASCII(MID$(l$,m,1))<127 THEN k$=k$+ MID$(l$,m,1)
  NEXT m
  bdat$=k$
  bdat$=REPLACE$(bdat$,"<ro.build.date>:","")
  bdat$=REPLACE$(bdat$,"<","")
  bdat$=REPLACE$(bdat$,">","")
  bdat$=REPLACE$(bdat$," UTC","")
  bdat$=REPLACE$(bdat$," CST","")
  bdat$=REPLACE$(bdat$," PDT","")
  bdat$=REPLACE$(bdat$," HKT","")
  a1=IS_IN(":",bdat$)
  k1=a1-2
  k2=a1+6
  baux$=MID$(bdat$,k1,k2-k1)
  bdat$=REPLACE$(bdat$,baux$,"") % time is out
  ind=0
  FOR j=1 TO 7
   a1=IS_IN(deys$[j],bdat$)
   IF a1>0 THEN
    ind=j
    F_N.BREAK
   ENDIF
  NEXT j
  IF ind>0 THEN bdat$=REPLACE$(bdat$, deys$[ind] ,"") % day is out
 ENDIF
 IF LEN(bdat$)>14 THEN bdat$=LEFT$(bdat$,5)
 a=IS_IN("ro.build.",l$)
 IF a>0 THEN
  l$=REPLACE$(l$,"ro.build.","")
  l$=REPLACE$(l$,"<","")
  l$=REPLACE$(l$,">","")
  LIST.ADD buiList, l$
 ENDIF
NEXT i
fontSize=30
lstToSplit=buiList
GOSUB spltList
GR.MODIFY tx,"text", "Gathering Build info..." +ck1$
GOSUB updateProgress

! harware-software 17
k=k+40
moreFeat$=""
GR.TEXT.DRAW tx,50,k,"Gathering Harware-software info..."
GR.RENDER
FOR i=1 TO PMlines
 l$=PMlines$[i]
 a=IS_IN("camera",l$)
 IF a=0 THEN
  l$=REPLACE$(l$,"feature:android.","")
  l$=REPLACE$(l$,"feature:","")
  l$=REPLACE$(l$,"hardware.","hardware: ")
  l$=REPLACE$(l$,"software.","software: ")
 ENDIF
NEXT i
GR.MODIFY tx,"text", "Gathering Harware-software info..." +ck1$
GOSUB updateProgress

! system properties 18
k=k+40
GR.TEXT.DRAW tx,50,k,"Gathering System properties info..."
GR.RENDER
FOR i=1 TO lines
 l$=lines$[i]
 l$=REPLACE$(l$,"[","<")
 l$=REPLACE$(l$,"]",">")
 a1=IS_IN("dalvik.",l$)
 a2=IS_IN("status.battery.level",l$)
 a3=IS_IN("gsm.",l$)
 a4=IS_IN("build.",l$)
 a5=IS_IN("net.tcp.",l$)
 IF a1=0 & a2=0 & a3=0 & a4=0 & a5=0 THEN
  LIST.ADD SysList, l$
 ENDIF
NEXT i
GR.MODIFY tx,"text", "Gathering System properties info..." +ck1$
GOSUB updateProgress

! processes 19
k=k+40
moreProc$=""
GR.TEXT.DRAW tx,50,k,"Gathering Processes info..."
GR.RENDER
MaxTout=8000
SYSTEM.WRITE "ps"
GOSUB waitForData
LIST.ADD proList, "*** PROCESSES ***"
IF !ttimedOut THEN
 j=0
 DO
  SYSTEM.READ.LINE l$
  j=j+1
  l$=REPLACE$(l$,"  "," ")
  LIST.ADD proList, REPLACE$(STR$(j),".0","")+" "+l$
  SYSTEM.READ.READY ready
 UNTIL !ready
 j$=REPLACE$(STR$(j),".0"," ")
 nProc$=j$
ENDIF
GR.MODIFY tx,"text", "Gathering Processes info..." +ck1$
GOSUB updateProgress
LIST.ADD proList, "--------------"

! services info 20
k=k+40
GR.TEXT.DRAW tx,50,k,"Gathering Services info..."
GR.RENDER
listPtr=serList
LIST.ADD proList, "*** SERVICES ***
SYSTEM.WRITE "service list"
GOSUB getSystemData
j$=REPLACE$(STR$(num),".0","")
nServ$=j$
LIST.ADD serList, "--------------"
GR.MODIFY tx,"text", "Gathering Services info..." +ck1$
GOSUB updateProgress

! packages info 21
k=k+40
GR.TEXT.DRAW tx,50,k,"Gathering Packages info..."
GR.RENDER

SYSTEM.WRITE "pm list packages -f"
LIST.ADD pmaList, "*** PACKAGES ***"
GOSUB waitForData
IF !ttimedOut THEN
 j=0
 DO
  SYSTEM.READ.LINE l$
  j=j+1
  j$=REPLACE$(STR$(j),".0"," ")
  l$=REPLACE$(l$,"package:",j$)
  LIST.ADD pmaList,l$
  SYSTEM.READ.READY ready
 UNTIL !ready
ENDIF
j$=REPLACE$(STR$(j),".0"," ")
nPack$=j$
LIST.ADD pmaList, "--------------"
GR.MODIFY tx,"text", "Gathering Packages info..." +ck1$
GOSUB updateProgress
Tout=1000

! tcpip 22
k=k+40
GR.TEXT.DRAW tx,50,k,"Gathering TCP/IP info..."
GR.RENDER
LIST.ADD netList, ""
LIST.ADD netList, "-----TCP/IP info----"
FOR i=1 TO lines
 l$=lines$[i]
 a=IS_IN("net.tcp.",l$)
 IF a>0 THEN
  l$=REPLACE$(l$,"net.tcp.","")
  LIST.ADD netList,l$
 ENDIF
NEXT i
LIST.ADD netList, ""
LIST.ADD netList, "**End of info**"
fontSize=24
lstToSplit=netList
GOSUB spltList
GR.MODIFY tx,"text", "Gathering TCP/IP info..." +ck1$
GOSUB updateProgress

! locale info 23
k=k+40
GR.TEXT.DRAW tx,50,k,"Gathering Locale info..."
GR.RENDER
FOR i=1 TO lines
 l$=lines$[i]
 a1=IS_IN("<persist.sys.country>:",l$)
 a2=IS_IN("<persist.sys.language>:",l$)
 a3=IS_IN("<ro.product.locale.region>:",l$)
 IF a1>0 | a2>0 | a3>0 THEN
  l$=REPLACE$(l$,"<","")
  l$=REPLACE$(l$,">","")
 ENDIF
 IF a1>0 THEN lang1$=REPLACE$(l$,"persist.sys.country:","")
 IF a2>0 THEN lang2$=REPLACE$(l$,"persist.sys.language:","")
 IF a3>0 THEN lang3$=REPLACE$(l$,"ro.product.locale.region:","")
NEXT i
lang4$=tz$

! **********
SYSTEM.CLOSE
! **********
TONE 350,150,0
GR.COLOR 255,255,155,25,1
GR.MODIFY InfoTitle,"text","Done!"
GR.MODIFY tx,"text", "Gathering Locale info..." +ck1$
GR.SHOW arc2
GOSUB updateProgress

here:
POPUP "Sorting Data...",0,0,0
GR.MODIFY progessRect,"right", 700
GR.RENDER
LIST.SIZE pmaList,S
FOR i=1 TO S
 LIST.GET pmaList,i,a$
 LIST.ADD SystemList, a$
NEXT i
LIST.SIZE proList,S
FOR i=1 TO S
 LIST.GET proList,i,a$
 LIST.ADD SystemList, a$
NEXT i
LIST.SIZE serList,S
FOR i=1 TO S
 LIST.GET serList,i,a$
 LIST.ADD SystemList, a$
NEXT i
fontSize=24
lstToSplit=SystemList
GOSUB spltList

! PRINT CLOCK()-xx

! ******************************
! xk
page1:
index=0
GR.CLS
GR.TEXT.TYPEFACE 3
GR.TEXT.SIZE 80
GR.TEXT.BOLD 0

GR.COLOR 255,190,90,30,1
GR.RECT r,0,0,200,110
GR.RECT r,600,0,800,110

GR.COLOR 255,0,95,255,1
GR.RECT r,200,-1,600,110

GR.COLOR 255,205,205,50,1
GR.TEXT.DRAW tx,60,65,menu$
GR.TEXT.SIZE 100
GR.TEXT.DRAW tx,670,70,lhand$
GR.TEXT.SIZE 37
GR.COLOR 255,105,205,255,1
GR.TEXT.BOLD 1
GR.TEXT.DRAW tx1,50,100,"Menu"
GR.TEXT.DRAW tx2,620,100,"Next page"

GR.COLOR 255,15,55,55,1
GR.TEXT.DRAW tx1a,50,100,"Menu"
GR.TEXT.DRAW tx2a,620,100,"Next page"
GR.HIDE tx1a
GR.HIDE tx2a
GR.COLOR 255,255,255,255,1
GR.TEXT.SIZE 42
GR.TEXT.TYPEFACE 4
GR.TEXT.DRAW tx,270,70,"SYSTEM INFO"
GR.COLOR 255,255,255,255,1
GR.BITMAP.DRAW glance, glanceBmp, 0,40

GR.COLOR 255,20,20,250,1
j=0
FOR i=1 TO 4
 k=MOD(i,2)
 j=j+1
 GR.TEXT.DRAW section1Ptr,130,(i-1)*230+180,section1$[j]
 ! gr.rect r, 20,(i-1)*230+130, 300, (i-1)*230+240
 j=j+1
 GR.TEXT.DRAW section1Ptr,520,(i-1)*230+180,section1$[j]
 ! gr.rect r, 420,(i-1)*230+130, 700, (i-1)*230+240
NEXT i
GR.COLOR 255,164,196,57,1
j=0
FOR i=1 TO 4
 k=MOD(i,2)
 j=j+1
 GR.TEXT.DRAW section1Ptr[j],130,(i-1)*230+180,section1$[j]
 ! gr.rect r, 20,(i-1)*230+130, 300, (i-1)*230+240
 j=j+1
 GR.TEXT.DRAW section1Ptr[j],520,(i-1)*230+180,section1$[j]
 ! gr.rect r, 420,(i-1)*230+130, 700, (i-1)*230+240
NEXT i


GR.TEXT.SIZE 31
GR.TEXT.BOLD 1
! GR.COLOR 255,255,255,255,1
GR.COLOR 255,255,255,0,1
j=0
FOR i=1 TO 4
 k=MOD(i,2)
 j=j+1
 GR.TEXT.DRAW section1More[j],130,(i-1)*230+220,sectDet1$[j]
 j=j+1
 GR.TEXT.DRAW section1More[j],520,(i-1)*230+220,sectDet1$[j]
NEXT i
GR.TEXT.SIZE 31
GR.TEXT.BOLD 0
GR.TEXT.TYPEFACE 3
GR.COLOR 255,164,196,57,1
! device
GR.TEXT.DRAW elem1,10,260,"Brand: "
GR.TEXT.WIDTH s1, "Brand: "
GR.TEXT.DRAW elem1,10,290,"Model: "
GR.TEXT.WIDTH s2, "Model: "
GR.TEXT.DRAW elem1,10,320,"Device: "
GR.TEXT.WIDTH s3, "Device: "
GR.TEXT.DRAW elem1,10,350,"Product: " % here elem1
GR.TEXT.WIDTH s4, "Product: "
! screen
GR.TEXT.DRAW elem1,410,260,"Dots per inch: "
GR.TEXT.WIDTH s6, "Dots per inch: "
GR.TEXT.DRAW elem1,410,290,"Resolution: "
GR.TEXT.WIDTH s7, "Resolution: "
IF scn$<>"" THEN
 GR.TEXT.DRAW elem1,410,320,"Name: "
 GR.TEXT.WIDTH s5, "Name: "
ENDIF
! memory
GR.TEXT.DRAW elem1,10,490,"Memory Total: "
GR.TEXT.WIDTH s8, "Memory Total: "
GR.TEXT.DRAW elem1,10,520,"Memory Free: "
GR.TEXT.WIDTH s9, "Memory Free: "
GR.TEXT.DRAW elem1,10,550,"Heap Memory: "
GR.TEXT.WIDTH s10, "Heap Memory: "
! processor

GR.TEXT.DRAW elem1,410,520,"Frequency (Hz): "
GR.TEXT.WIDTH s11, "Frequency (Hz): "
GR.TEXT.DRAW elem1,410,550,"Number of cores: "
GR.TEXT.WIDTH s12, "Number of cores: "
! build
GR.TEXT.DRAW elem1,10,730,"Android version: "
GR.TEXT.WIDTH s13, "Android version: "
GR.TEXT.DRAW elem1,10,760,"API level: "
GR.TEXT.WIDTH s14, "API level: "
GR.TEXT.DRAW elem1,10,790,"Build Date: "
GR.TEXT.WIDTH s15, "Build Date: "
! internet
GR.TEXT.DRAW elem1,410,730,"Local IP:"
GR.TEXT.WIDTH s16, "Local IP:"
GR.TEXT.DRAW elem1,410,760,"Internet IP:"
GR.TEXT.WIDTH s17, "Internet IP:"
! GR.TEXT.DRAW elem1,410,790,host$

! battery
GR.TEXT.DRAW elem1,10,950,"Temperature: "
GR.TEXT.WIDTH s18, "Temperature: "
GR.TEXT.DRAW elem1,10,980,"Technology: "
GR.TEXT.WIDTH s19, "Technology: "

! SD
GR.TEXT.DRAW elem1,410,950,"Total: "
GR.TEXT.WIDTH s21, "Total: "
GR.TEXT.DRAW elem1,410,980,"Used: "
GR.TEXT.WIDTH s22, "Used: "
GR.TEXT.DRAW elem1,410,1010,"Free: "
GR.TEXT.WIDTH s23, "Free: "

! GR.COLOR 255,30,225,225,1
! GR.COLOR 255,0,225,255,1
GR.COLOR 255,255,255,255,1
! device
GR.TEXT.DRAW elem1,10+s1,260,bra$
GR.TEXT.DRAW elem1,10+s2,290,mdl$
GR.TEXT.DRAW elem1,10+s3,320,dev$
GR.TEXT.DRAW elem1,10+s4,350,pro$ % here elem1
! screen
GR.TEXT.DRAW elem1,410+s6,260,dpi$
GR.TEXT.DRAW elem1,410+s7,290,res$
IF scn$<>"" THEN
 GR.TEXT.DRAW elem1,410+s5,320,scn$
ENDIF
! memory
GR.TEXT.DRAW elem1,10+s8,490,memT$
GR.TEXT.DRAW elem1,10+s9,520,memF$
GR.TEXT.DRAW elem1,10+s10,550,memH$
! processor
GR.TEXT.DRAW elem1,410,490,proc$
GR.TEXT.DRAW elem1,410 +s11 ,520,freq$
GR.TEXT.DRAW elem1,410 +s12 ,550,cores$
! build
GR.TEXT.DRAW elem1,10 +s13 ,730,aver$
GR.TEXT.DRAW elem1,10 +s14 ,760,bver$
GR.TEXT.DRAW elem1,10 +s15 ,790,bdat$
! internet
GR.TEXT.DRAW elem1,410 +s16 ,730,lip$
GR.TEXT.SIZE 30
GR.TEXT.DRAW elem1,410 +s17 ,760,ip$
GR.TEXT.DRAW elem1,410 ,790,host$
GR.TEXT.SIZE 31
! battery
GR.TEXT.DRAW elem1,10 +s18 ,950,temp$
GR.TEXT.DRAW elem1,10 +s19 ,980,tec$
! GR.TEXT.DRAW elem1,10 +s20 ,1010,vol$

! SD
GR.TEXT.DRAW elem1,410 +s21 ,950,sd1$
GR.TEXT.DRAW elem1,410 +s22 ,980,sd2$
GR.TEXT.DRAW elem1,410 +s23 ,1010,sd3$
gr.render

! setup da window now
GOSUB setupWindow

GR.RENDER
pause 100
GR.RENDER

! check icons pressed
GOSUB waitForIcon
IF quitflag=2 THEN
 GOSUB clrLists
 GR.CLS
 GR.RENDER
 RETURN % system info
ENDIF

!  PAGE 2
page2:
index=8
GR.CLS
GR.TEXT.TYPEFACE 3
GR.TEXT.SIZE 80
GR.TEXT.BOLD 0

GR.COLOR 255,190,90,30,1
GR.RECT r,0,0,200,110
GR.RECT r,600,0,800,110
GR.COLOR 255,0,95,255,1
GR.RECT r,200,-1,600,110

GR.COLOR 255,205,205,50,1
GR.TEXT.DRAW tx,60,65,menu$
GR.TEXT.SIZE 100
GR.TEXT.DRAW tx,660,70,rhand$
GR.TEXT.SIZE 37
GR.TEXT.BOLD 1
GR.COLOR 255,105,205,255,1
GR.TEXT.DRAW tx1,50,100,"Menu"
GR.TEXT.DRAW tx2,613,100,"Prev. page"

GR.COLOR 255,15,55,55,1
GR.TEXT.DRAW tx1a,50,100,"Menu"
GR.TEXT.DRAW tx2a,613,100,"Prev. page"
GR.HIDE tx1a
GR.HIDE tx2a

GR.COLOR 255,255,255,255,1
GR.TEXT.SIZE 42
GR.TEXT.TYPEFACE 4
GR.TEXT.DRAW tx,270,70,"SYSTEM INFO"
GR.COLOR 255,255,255,255,1
GR.BITMAP.DRAW glance2, glanceBmp2, 0,40


GR.COLOR 255,20,20,250,1

j=0
FOR i=1 TO 4
 k=MOD(i,2)
 j=j+1
 GR.TEXT.DRAW section2Ptr,130,(i-1)*230+180,section2$[j]
 ! gr.rect r, 20,(i-1)*230+130, 300, (i-1)*230+240
 j=j+1
 GR.TEXT.DRAW section2Ptr,520,(i-1)*230+180,section2$[j]
 ! gr.rect r, 420,(i-1)*230+130, 700, (i-1)*230+240
NEXT i
GR.COLOR 255,164,196,57,1
j=0
FOR i=1 TO 4
 j=j+1
 GR.TEXT.DRAW section2Ptr[j],130,(i-1)*230+180, section2$[j]
 j=j+1
 GR.TEXT.DRAW section2Ptr[j],520,(i-1)*230+180, section2$[j]
NEXT i

! GR.COLOR 255,255,255,255,1
GR.COLOR 255,255,255,0,1
GR.TEXT.SIZE 31
GR.TEXT.BOLD 0
j=0
FOR i=1 TO 4
 j=j+1
 GR.TEXT.DRAW section2More[j],130,(i-1)*230+220, sectDet2$[j]
 j=j+1
 GR.TEXT.DRAW section2More[j],520,(i-1)*230+220, sectDet2$[j]
NEXT i
GR.COLOR 255,164,196,57,1
GR.TEXT.TYPEFACE 3

! up time
IF uptFlag=1 THEN
 GR.TEXT.DRAW elem2,10,260,"Up time:"
 GR.TEXT.WIDTH s1, "Up time:"
 GR.TEXT.DRAW elem2,10,290,"Idle time:"
 GR.TEXT.WIDTH s2, "Idle time:"
 GR.TEXT.DRAW elem2,10,320,"Sleep time:"
 GR.TEXT.WIDTH s3, "Sleep time:"
ELSE
 GR.TEXT.DRAW elem2,10,260,"Days: "
 GR.TEXT.WIDTH s1, "Days: "
 GR.TEXT.DRAW elem2,10,290,"Hours: "
 GR.TEXT.WIDTH s2, "Hours: "
 GR.TEXT.DRAW elem2,10,320,"Minutes: "
 GR.TEXT.WIDTH s3, "Minutes: "
ENDIF

! sensors
! gps
GR.TEXT.DRAW elem2,10,490,"Provider: "
GR.TEXT.WIDTH s7, "Provider: "
GR.TEXT.DRAW elem2,10,520,"Latitude: "
GR.TEXT.WIDTH s8, "Latitude: "
GR.TEXT.DRAW elem2,10,550,"Longitude: "
GR.TEXT.WIDTH s9, "Longitude: "
! gsm
GR.TEXT.DRAW elem2,410,490,"Number: "
GR.TEXT.WIDTH s10, "Number: "
GR.TEXT.DRAW elem2,410,520,"Operator: "
GR.TEXT.WIDTH s11, "Operator: "
GR.TEXT.DRAW elem2,410,550,"Sim Operator: "
GR.TEXT.WIDTH s12, "Sim Operator: "
! language
GR.TEXT.DRAW elem2,10,950, "System Country: "
GR.TEXT.WIDTH s13, "System Country: "
GR.TEXT.DRAW elem2,10,980, "System Language: "
GR.TEXT.WIDTH s14, "System Language: "
GR.TEXT.DRAW elem2,10,1010, "Product region: "
GR.TEXT.WIDTH s15, "Product region: "
GR.TEXT.DRAW elem2,10,1040, "TimeZone: "
GR.TEXT.WIDTH s16, "TimeZone: "
! system
GR.TEXT.DRAW elem2,410,950, "Installed packages: "
GR.TEXT.WIDTH s17, "Installed packages: "
GR.TEXT.DRAW elem2,410,980, "Running processes: "
GR.TEXT.WIDTH s18, "Running processes: "
GR.TEXT.DRAW elem2,410,1010, "Running services: "
GR.TEXT.WIDTH s19, "Running services: "
! camera
GR.TEXT.DRAW elem2,10,720, "Feature: "
GR.TEXT.WIDTH s20, "Feature: "
GR.TEXT.DRAW elem2,10,750, "Feature: "
IF camF3$<>"" THEN GR.TEXT.DRAW elem2,10,780, "Feature: "
IF camF4$<>"" THEN GR.TEXT.DRAW elem2,10,810, "Feature: "
! features
GR.TEXT.DRAW elem2,410,720, "H/W: "
GR.TEXT.WIDTH s21, "H/W: "
GR.TEXT.DRAW elem2,410,750, "H/W: "
GR.TEXT.DRAW elem2,410,780, "H/W: "

! GR.COLOR 255,30,225,225,1
GR.COLOR 255,255,255,225,1
! up time
GR.TEXT.DRAW elem2,10+s1,260,upt1$
GR.TEXT.DRAW elem2,10+s2,290,upt2$
GR.TEXT.DRAW elem2,10+s3,320,upt3$
! sensors
IF SensorsL>0 THEN GR.TEXT.DRAW elem2,410,260, Slist$[1]
IF SensorsL>1 THEN GR.TEXT.DRAW elem2,410,290, Slist$[2]
IF SensorsL>2 THEN GR.TEXT.DRAW elem2,410,320, Slist$[3]
! gps
GR.TEXT.DRAW elem2,10+s7,490,prGps$
GR.TEXT.DRAW elem2,10+s8,520,laGps$
GR.TEXT.DRAW elem2,10+s9,550,loGps$
! gsm
GR.TEXT.DRAW elem2,410+s10,490,ph$
GR.TEXT.DRAW elem2,410+s11,520,gsm1$
GR.TEXT.DRAW elem2,410+s12,550,gsm2$
! camera
GR.TEXT.DRAW elem2,10+s20,720,camF1$
GR.TEXT.DRAW elem2,10+s20,750,camF2$
IF camF3$<>"" THEN GR.TEXT.DRAW elem2,10+s20,780,camF3$
IF camF4$<>"" THEN GR.TEXT.DRAW elem2,10+s20,810,camF4$
! features
GR.TEXT.DRAW elem2,410+s21,720,fea1$
GR.TEXT.DRAW elem2,410+s21,750,fea2$
GR.TEXT.DRAW elem2,410+s21,780,fea3$
! language
GR.TEXT.DRAW elem2,10+s13,950, lang1$
GR.TEXT.DRAW elem2,10+s14,980, lang2$
GR.TEXT.DRAW elem2,10+s15,1010,lang3$
GR.TEXT.DRAW elem2,10+s16,1040,lang4$
! system
GR.TEXT.DRAW elem2,410+s17,950, npack$
GR.TEXT.DRAW elem2,410+s18,980, nProc$
GR.TEXT.DRAW elem2,410+s19,1010,nServ$
gr.render

GOSUB setupWindow
GR.RENDER
pause 100
GR.RENDER

GOSUB waitForIcon
IF quitflag=2 THEN
 GOSUB clrLists
 GR.CLS
 GR.RENDER
 RETURN % system info
ENDIF
IF quitflag=3 THEN GOTO page1


! ******************************
updateProgress:
progress=progress+d
GR.MODIFY progessRect,"right", progress
GR.RENDER
RETURN

! ******************************
getSystemData:
num=0
X=CLOCK()
DO
 PAUSE 200
 SYSTEM.READ.READY ready
UNTIL ready | CLOCK()-x>maxTout
IF CLOCK()-x<maxTout THEN
 DO
  SYSTEM.READ.LINE l$
  IF addFlag=1 THEN LIST.ADD listPtr, l$
  num=num+1
  SYSTEM.READ.READY ready
 UNTIL !ready
ELSE
 l$="No data."
 LIST.ADD listPtr,"No data."
ENDIF
PAUSE 100
RETURN

! ******************************
waitForData:
ttimedOut=0
x=CLOCK()
DO
 PAUSE 200
 SYSTEM.READ.READY ready
UNTIL ready | CLOCK()-x>maxTout
IF CLOCK()-x>maxTout THEN ttimedOut=1 ELSE ttimedOut=0
RETURN

! ******************************
findString:
FOR i=1 TO LEN(l$)
 IF MID$(l$,i,1)<>" " THEN F_N.BREAK
NEXT i
l$=RIGHT$(l$,LEN(l$)-i+1)

my$=""
j=0
i=0
DO
 i=i+1
 m$=MID$(l$,i,1)
 IF m$<>" " THEN my$=my$+m$
 j=j+1
UNTIL m$=" "
l$=RIGHT$(l$,LEN(l$)-j+1)
RETURN
xx:

REM **********************************************************
sdTest:
GR.COLOR 255, 55, 255, 255, 1
! GR.TEXT.SIZE 120
! GR.TEXT.DRAW tx0,0,700 ,lhand$
GR.MODIFY tx0,"y",700
GR.TEXT.SIZE 46
! sd card
GR.COLOR 255, 255, 255, 55, 1
GR.TEXT.DRAW tx1,250,650,"Storage Benchmark"
GR.COLOR 255, 255, 255, 255, 1
GR.TEXT.SIZE 35
GR.TEXT.DRAW tx2,250,700, "Pass 1/2: writing to Storage.."
GR.RENDER

test$=""
a$="01234567890qwertyuioplkjhgfdsazxcvbnm"
TotSd=0
xx2=0
FOR i=1 TO 300
 test$=test$+a$
NEXT i
FOR j=1 TO 2
 TEXT.OPEN w,file,  "test"
 x=CLOCK()
 FOR i=1 TO 300
  TEXT.WRITELN file, test$
 NEXT i
 xx1=CLOCK()-x
 TEXT.CLOSE file
 FILE.DELETE l, "test"
 xx2=xx2+xx1
NEXT j
TotSd=xx2
test$=""

GR.RENDER

! 2 loading
GR.MODIFY tx2, "text", "Pass 2/2: reading from Storage.."
GR.RENDER
x=CLOCK()
FOR i=1 TO 50
 GR.BITMAP.LOAD cman[i],"android.png"
NEXT i
xx2=CLOCK()-x
GR.RENDER
FOR i=1 TO 50
 GR.BITMAP.DELETE cman[i]
NEXT i

TotSd=TotSD+xx2
TotSd=ROUND(TotSD/100)/10
GR.MODIFY tx2, "text", "Total Storage: "+REPLACE$(STR$(TotSd),".0","")+" secs"
GR.HIDE tx0
GR.RENDER

RETURN

REM **********************************************************
graphicsTest:

GR.MODIFY tx0,"y",550
GR.TEXT.SIZE 46
GR.COLOR 255, 255, 255, 55, 1
GR.TEXT.DRAW tx3,250,500 ,"Graphics Benchmark"
GR.COLOR 255, 255, 255, 255, 1
GR.TEXT.SIZE 35
GR.TEXT.DRAW tx4,250,550 ,"Pass 1/9: Draw "
GR.COLOR 255, 255, 255, 255, 1
GR.RENDER
GR.BITMAP.LOAD cman,"android.png"

! 1 draw
x=CLOCK()
FOR i=1 TO 200
 GR.TEXT.DRAW text[i], 40,40+i,"Benchmark"
NEXT i
x1=CLOCK()-x
GR.COLOR 255, 155, 0, 25, 1
GR.TEXT.DRAW text, 40,40+i,"Benchmark"
GR.RENDER

! 2 render
GR.MODIFY tx4,"text" ,"Pass 2/9: Rendering "
GR.RENDER
x =CLOCK()
FOR i=1 TO 100
 GR.BITMAP.DRAW androidPng[i], cman,i,i
 GR.RENDER
NEXT i
x2=CLOCK()-x
GR.RENDER

! 3 shapes
GR.MODIFY tx4,"text" ,"Pass 3/9: Shapes "
GR.RENDER
x=CLOCK()
GR.COLOR 255,155,0,0,0
FOR i=1 TO 200
 GR.RECT rct[i], 180,0, 180+i,120
 GR.ARC ar, 420,0,420+i,20+i,0,i,0
NEXT i
x3=CLOCK()-x

! 4 display list
GR.MODIFY tx4,"text" ,"Pass 4/9: Display list"
GR.RENDER
x=CLOCK()
FOR i=1 TO 200
 GR.HIDE androidPng[i]
 GR.HIDE rct[i]
 GR.HIDE text[i]
NEXT i
GR.RENDER
FOR i=1 TO 200
 GR.SHOW androidPng[i]
 GR.SHOW rct[i]
 GR.SHOW text[i]
NEXT i
x4=CLOCK()-x

! 5 modify objects
GR.MODIFY tx4,"text" ,"Pass 5/9: Modify objects"
GR.RENDER
x=CLOCK()
FOR i=1 TO 200
 GR.MODIFY androidPng[i], "alpha" ,i
 GR.MODIFY rct[i],"top",i
 GR.MODIFY text[i],"text","test"
NEXT i
x5=CLOCK()-x

! 6 create bitmaps
GR.MODIFY tx4,"text" ,"Pass 6/9: Bitmaps "
GR.RENDER
FOR i=1 TO 50
 GR.BITMAP.CREATE bmp,200,200
 GR.BITMAP.DELETE bmp
NEXT i
x6=CLOCK()-x

! 7 rotate
GR.MODIFY tx4,"text" ,"Pass 7/9: Rotation "
GR.RENDER
x=CLOCK()
FOR i=1 TO 360
 GR.ROTATE.START i,i,i/2
 GR.BITMAP.DRAW androidPng,cman,i,i/2
 GR.ROTATE.END
NEXT i
x7=CLOCK()-x


! 8 text commands
GR.MODIFY tx4,"text" ,"Pass 8/9: Text "
GR.RENDER
x=CLOCK()
FOR i=1 TO 200
 GR.COLOR 255,0,0,0,1
 GR.TEXT.TYPEFACE 3
 GR.TEXT.SIZE 35
 GR.TEXT.BOLD 1
 GR.TEXT.TYPEFACE 2
 GR.TEXT.SKEW -0.20
 GR.TEXT.UNDERLINE 1
NEXT i
x9=CLOCK()-x

GR.TEXT.BOLD 0
GR.TEXT.TYPEFACE 3
GR.TEXT.SKEW 0
GR.TEXT.UNDERLINE 0
GR.COLOR 255,255,255,255,1

! 9 pixel commands
GR.MODIFY tx4,"text" ,"Pass 9/10: Pixel operations"
GR.RENDER
x=CLOCK()
FOR i=1 TO 200
 GR.TEXT.WIDTH s,"Benchmark"
 GR.GET.TEXTBOUNDS "Benchmark" , left, top, right, bottom
 GR.GET.POSITION androidPng,  xx, yy
NEXT i
x9=CLOCK()-x

! 10 3d
REM 3d sphere
GR.COLOR 255,255,255,55,1
GR.MODIFY tx4,"text" ,"Pass 10/10: 3d Rendering"
GR.RENDER
nOfPoints = 1000
zoomLevel = 0
ARRAY.DELETE points[]
DIM points[nofpoints,3]
x=CLOCK()
r=50
x0=600
y0=200
z0=300
pi=3.14159
f=0
t=0
i=1
FOR f=0 TO pi STEP 0.35
 FOR t=0 TO 2*pi STEP 0.06
  points[i,1]=x0+r*COS(t)*SIN(f)
  points[i,2]=y0+r*SIN(f)*SIN(t)
  points[i,3]=z0+r*COS(f)
  i=i+1
 NEXT t
NEXT f
FOR pointIdx = 1 TO i-1
 IF points[pointIdx,3] + zoomLevel > 0 THEN
  flatX = points[pointIdx,1] * 256 / (points[pointIdx,3] + zoomLevel)
  flatY = points[pointIdx,2] * 256 / (points[pointIdx,3] + zoomLevel)
  GR.CIRCLE cir, flatX, flatY,2
 ENDIF
NEXT
GR.RENDER
x10=CLOCK()-x
PAUSE 2000
GR.COLOR 255,0,0,0,1
TotGr=x1+x2+x3+ x4+x5+x6+ x7+x8+x9+x10
TotGr=ROUND(TotGr/100)/10
GR.MODIFY tx4,"text","Total Graphics: " +  REPLACE$(STR$(TotGr),".0","")+ " secs"
GR.RENDER
RETURN


REM **********************************************************
speedTest:
GR.COLOR 255, 55, 255, 255, 1
GR.TEXT.SIZE 120
GR.TEXT.DRAW tx0,0,400 ,CHR$(9758)
GR.TEXT.SIZE 46
GR.COLOR 255, 255, 255, 55, 1
GR.TEXT.DRAW tx1, 250,350,"Processor Benchmark"
GR.COLOR 255, 255, 255, 255, 1
GR.TEXT.SIZE 35
GR.TEXT.DRAW tx2, 250,400,""
GR.RENDER
UNDIM ex[]
UNDIM results[]
UNDIM ztime[]
UNDIM heading$[]
UNDIM ops[]
UNDIM flops[]
DIM ex[4]
DIM results[8]
DIM ztime[8]
DIM heading$[8]
DIM ops[8]
DIM flops[8]
icount = 10
calibrate = 0
ixtra = 1
ix100 = 1
REM Passes to average
Passes = 10
GR.COLOR 255,255,255,255,1
GR.TEXT.SIZE 40
GR.RENDER
mTimeUsed = 0
Check = 0
FOR Pass = 1 TO Passes
 GOSUB Whetstones
NEXT Pass
mTimeUsed=ROUND(10*mTimeUsed)/10
GR.MODIFY tx2,"text",  "Total Processor: "+REPLACE$(STR$(mTimeUsed),".0","")+" secs"
IF Check = 0 THEN
 POPUP "Calculation Error Detected",0,0,0
ELSE
 POPUP "Calculations Correct",0,0,0
ENDIF
GR.RENDER
TotCalc=mTimeUsed
RETURN

Whetstones:
REM   INITIALISE CONSTANTS
t = 0.49999975
t0 = t
t1 = 0.50000025
t2 = 2
n1 = 12 * ix100
n2 = 14 * ix100
n3 = 345 * ix100
n4 = 210 * ix100
n5 = 32 * ix100
n6 = 899 * ix100
n7 = 616 * ix100
n8 = 93 * ix100
n1mult = 10
GR.MODIFY tx2,"text","Pass: "+REPLACE$(STR$(Pass),".0","")+" of "+ REPLACE$(STR$(Passes),".0","")
GR.RENDER
REM MODULE 1 - ARRAY ELEMENTS
stime = xTime()
ex[1] = 1
ex[2] = -1
ex[3] = -1
ex[4] = -1
FOR ix = 1 TO ixtra
 FOR i = 1 TO n1 * n1mult
  ex[1] = (ex[1] + ex[2] + ex[3] - ex[4]) * t
  ex[2] = (ex[1] + ex[2] - ex[3]+ ex[3]) * t
  ex[3] = (ex[1] - ex[2] + ex[3] + ex[4]) * t
  ex[4]= (-ex[1] + ex[2] + ex[3] + ex[4]) * t
 NEXT i
 t = 1.0 - t
NEXT ix
t = t0
checsum = ex[4]
rtime = (xTime() - stime)
smflops = n1 * 16
title$ = "N1 floating point"
atype = 1
section = 1
REM N1 * 16 floating point calculations
GOSUB Pout
REM MODULE 2 - ARRAY AS PARAMETER
stime = xTime()
FOR ix = 1 TO ixtra
 FOR i = 1 TO n2
  GOSUB PA
 NEXT i
 t = 1.0 -t
NEXT ix
t = t0
checsum = ex[4]
rtime = xTime() - stime
smflops = n2 * 96
title$ = "N2 floating point"
atype = 1
section = 2
REM  N2 * 96 floating point calculations
GOSUB Pout
REM MODULE 3 - CONDITIONAL JUMPS
stime = xTime()
j = 1
FOR ix = 1 TO ixtra
 FOR i = 1 TO n3
  IF j <> 1 THEN
   j = 3
  ELSE
   j = 2
  ENDIF
  IF j <= 2 THEN
   j = 1
  ELSE
   j = 0
  ENDIF
  IF j >= 1 THEN
   j = 0
  ELSE
   j = 1
  ENDIF
 NEXT i
NEXT ix
checsum = j
rtime = xTime() - stime
smflops = n3 * 3
title$ = "N3 if then else"
atype = 2
section = 3
REM  N3 * 3 IF THEN ELSE
GOSUB Pout
REM MODULE 4 - INTEGER ARITHMETIC
stime = xTime()
j = 1
k = 2
l = 3
FOR ix = 1 TO ixtra
 FOR i = 1 TO n4
  j = j * (k - j) * (l - k)
  k = l * k - (l - j) * k
  l = (l - k) * (k + j)
  ex[l - 1] = j + k + l
  ex[k - 1] = j * k * l
 NEXT i
NEXT ix
checsum = ex[2] + ex[1]
rtime = xTime() - stime
smflops = n4 * 15
title$ = "N4 fixed point"
atype = 2
section = 4
REM  N4 * 15 fixed point operations
GOSUB Pout
REM MODULE 5 - TRIG. FUNCTIONS
stime = xTime()
X = 0.5
Y = 0.5
FOR ix = 1 TO ixtra
 FOR i = 1 TO n5
  X = t * ATAN(t2 * SIN(X) * COS(X) / (COS(X + Y) + COS(X - Y) - 1))
  Y = t * ATAN(t2 * SIN(Y) * COS(Y) / (COS(X + Y) + COS(X - Y) - 1))
 NEXT i
 t = 1.0 - t
NEXT ix
t = t0
checsum = Y
rtime = xTime() - stime
smflops = n5 * 26
title$ = "N5 sin,cos etc."
atype = 2
section = 5
REM  N5 * 26 function calls and floating point operations
GOSUB Pout
REM MODULE 6 - PROCEDURE CALLS
stime = xTime()
X = 1
Y = 1
Z = 1
FOR ix = 1 TO ixtra
 FOR i = 1 TO n6
  GOSUB P3
 NEXT i
NEXT ix
checsum = Z
rtime = xTime() - stime
smflops = n6 * 6
title$ = "N6 floating point"
atype = 1
section = 6
REM N6 * 6 floating point operations
GOSUB Pout
REM MODULE 7 - ARRAY REFERENCES
stime = xTime()
j = 1
k = 2
l = 3
ex[1] = 1
ex[2] = 2
ex[3] = 3
FOR ix = 1 TO ixtra
 FOR i = 1 TO n7
  GOSUB PO
 NEXT i
NEXT ix
checsum = ex[3]
rtime = xTime() - stime
smflops = n7 * 3
title$ = "N7 assigns"
atype = 2
section = 7
REM N7 * 3 assignments
GOSUB Pout
REM MODULE 8 - STANDARD FUNCTIONS
stime = xTime()
X = 0.75
FOR ix = 1 TO ixtra
 FOR i = 1 TO n8
  X = SQR(LOG(X) / t1)
 NEXT i
NEXT ix
checsum = X
rtime = xTime() - stime
smflops = n8 * 4
title$ = "N8 exp,sqrt etc."
atype = 2
section = 8
REM N8 * 4 function calls and floating point operations
GOSUB Pout
RETURN
REM END OF MAIN ROUTINE
PA:
REM PROCEDURE PA
j = 0
DO
 ex[1] = (ex[1] + ex[2] + ex[3] - ex[4]) * t
 ex[2] = (ex[1] + ex[2] - ex[3]+ ex[3]) * t
 ex[3] = (ex[1] - ex[2] + ex[3] + ex[4]) * t
 ex[4]= (-ex[1] + ex[2] + ex[3] + ex[4]) * t/2
 j = j + 1
UNTIL j = 6
RETURN
PO:
REM PROCEDURE P0
ex[j] = ex[k]
ex[k] = ex[l]
ex[l] = ex[j]
RETURN
P3:
REM PROCEDURE P3
X = Y
Y = Z
X = t * (X + Y)
Y = t1 * (X + Y)
Z = (X + Y) / t2
RETURN
Pout:
Check = Check + checsum
ztime[section] = rtime
heading$[section] = title$
mTimeUsed = mTimeUsed + rtime
IF calibrate = 1 THEN
 GR.MODIFY cal1,"text","#"
 GR.RENDER
ENDIF
IF calibrate = 0 THEN
 k1$= REPLACE$(STR$(Pass),".0","")
 k2$= REPLACE$(STR$(Passes),".0","")
 k3$= REPLACE$(heading$[section],".0","")
 k4$= REPLACE$(STR$(ztime[section]),".0","")
 GR.MODIFY tx2,"text", "Pass: "+k1$+" of "+ k2$+"  " + k3$
 GR.RENDER
ENDIF
RETURN

REM *****************************************
! SHOW SPEED RESULTS
viewResults:
! find best device
minPH=1000
minTAB=1000
bestPh=1
bestTab=1
FOR i=1 TO 6
 IF resPH[i]<minPH THEN
  bestPh=i
  minPH=resPH[i]
 ENDIF
 IF resTAB[i]<minTAB THEN
  bestTAB=i
  minTAB=resTAB[i]
 ENDIF
NEXT I

! normalize
f1= resPH[bestPh]
f2= resTAB[bestTab]
FOR i=1 TO 6
 IF i<> bestPh
  norm=f1/resPH[i] % 0..1 best
  normPh[i]=50+350*(1-norm) % starting y point of bar
 ELSE
  normPh[i]=50
 ENDIF
NEXT i
FOR i=1 TO 6
 IF i<> bestTab
  norm=f2/resTab[i] % 0..1 best
  normTab[i]=550+350*(1-norm) % starting y point of bar
 ELSE
  normTab[i]=550
 ENDIF
NEXT i

GR.COLOR 255,227,0,227,1
GR.TEXT.SIZE 37
GR.SET.STROKE 1
GR.TEXT.DRAW t, 10,440, "PHONES"
GR.TEXT.DRAW t, 10,940, "TABLETS"
GR.COLOR 255,155,95,185,1
GR.TEXT.DRAW t, 100,480, "Relative performance compared"
GR.TEXT.DRAW t, 100,520, "to other devices (%)."
! thin lines, %
GR.TEXT.SIZE 31
FOR i=0 TO 3
 GR.COLOR 255,17,127,127,1
 GR.LINE l, 5,50+87.5*i,800, 50+87.5*i
 GR.COLOR 255,0,0,197,1
 GR.TEXT.DRAW t, 8,47+87.5*i, REPLACE$(STR$(100-(i)*25),".0","")+"%"
 GR.COLOR 255,17,127,127,1
 GR.LINE l, 5,550+87.5*i,800, 550+87.5*i
 GR.COLOR 255,0,0,197,1
 GR.TEXT.DRAW t, 8,547+87.5*i, REPLACE$(STR$(100-(i)*25),".0","")+"%"
NEXT
GR.COLOR 255,17,127,127,1
FOR i=0 TO 6
 GR.LINE l, 30+130*i, 50, 30+130*i, 400
 GR.LINE l, 30+130*i ,550, 30+130*i,900
NEXT
GR.LINE l, 799,50,799,400
GR.LINE l, 799,550,799,900


! results bars
GR.SET.STROKE 3
GR.TEXT.SIZE 28
FOR i=1 TO 6
 GR.COLOR 255,227,127,27,1
 IF i=6 THEN GR.COLOR 255,0,227,27,1
 GR.TEXT.DRAW ssec, 55+130*(i-1), normPh[i]-5, STR$(resPh[i])
 GR.RECT r,60+130*(i-1) , normPh[i], 110+130*(i-1), 400
 GR.COLOR 255,27,197,27,0
 IF i=6 THEN GR.COLOR 255,200,200,200,0
 GR.RECT r,60+130*(i-1) , normPh[i] , 110+130*(i-1), 403
NEXT i

! axis
GR.COLOR 255,127,127,127,1
GR.SET.STROKE 6
GR.LINE l, 5,403,800,403
GR.LINE l, 5,50,5,406

! phones names
GR.TEXT.SIZE 31
GR.COLOR 255,227,227,27,1
FOR i=1 TO 6
 GR.ROTATE.START 270, 140+130*(i-1) , 400
 GR.TEXT.DRAW t,140+130*(i-1) , 400, phones$[i] %, tablets[i]
 GR.ROTATE.END
NEXT i
GR.TEXT.SIZE 37

! results bars
GR.TEXT.SIZE 28
GR.SET.STROKE 3
FOR i=1 TO 6
 GR.COLOR 255,227,127,27,1
 IF i=6 THEN GR.COLOR 255,0,227,27,1
 GR.TEXT.DRAW ssec, 55+130*(i-1), normTab[i]-5, STR$(resTab[i])
 GR.RECT r,60+130*(i-1) , normTab[i], 110+130*(i-1), 900
 GR.COLOR 255,27,197,27,0
 IF i=6 THEN GR.COLOR 255,200,200,200,0
 GR.RECT r,60+130*(i-1) , normTab[i], 110+130*(i-1), 903
NEXT i
! axis
GR.SET.STROKE 6
GR.COLOR 255,127,127,127,1
GR.LINE l, 5,903,800,903
GR.LINE l, 5,550,5,906

! tablets
GR.TEXT.SIZE 31
GR.COLOR 255,227,227,27,1
FOR i=1 TO 6
 GR.ROTATE.START 270, 140+130*(i-1) , 900
 GR.TEXT.DRAW t,140+130*(i-1) , 900, tablets$[i]
 GR.ROTATE.END
NEXT i
GR.TEXT.SIZE 37
GR.RENDER
RETURN

! ******************************
! window sub
setupWindow:
LIST.CLEAR 1
LIST.CLEAR 2
LIST.CLEAR 3
GR.TEXT.TYPEFACE 2
GR.TEXT.SIZE fontSize
! body
GR.SET.STROKE 4
GR.COLOR 255,104,96,57,1
GR.RECT bodyR, topX, topY, topX+wth, topY+hei
GR.COLOR 255, 255,255,255,0
GR.RECT bodyR2, topX+2, topY+2, topX+wth-50-2, topY+hei-2
LIST.ADD 1, bodyR
LIST.ADD 1, bodyR2
! title
GR.COLOR 255,0,55,255,1
GR.RECT titleR, topX, topY, topX+wth, topY+50
GR.COLOR 255,255,255,255,0
GR.RECT titleR2, topX+2, topY+2, topX+wth-2, topY+50-2
GR.SET.STROKE 0
GR.TEXT.ALIGN 2
GR.COLOR 255,255,255,255,1
GR.TEXT.SIZE 35
GR.TEXT.DRAW ttext, 400, topY+35, ""
GR.TEXT.SIZE fontSize
GR.TEXT.ALIGN 1
LIST.ADD 1, ttext
LIST.ADD 1, titleR
LIST.ADD 1, titleR2
LIST.ADD 1, ttext
! scroll bar
GR.COLOR 255,175,55,0,1
GR.RECT scrollR, topX+wth-50, topY+50, topX+wth, topY+hei
LIST.ADD 1, scrollR
! arrows & rects for scroll bar
GR.COLOR 255,255,255,255,0
GR.TEXT.SIZE 60
xc1=topX+wth-52
yc1=topY+48
xc2=topX+wth-2
yc2=topY+140
yy1=topY+hei-90
yy2=topY+hei-2
GR.TEXT.DRAW arrD, topX+wth-57, topY+110,up$
GR.TEXT.DRAW arrU, topX+wth-57, topY+hei-30,daun$
GR.SET.STROKE 4
GR.RECT upScrl, xc1, yc1, xc2, yc2
GR.RECT dowScrl, xc1, yy1, xc2, yy2
GR.LINE ll, xc2, yc2,xc2,yy2
LIST.ADD 1, arrD
LIST.ADD 1, arrU
LIST.ADD 1, upScrl
LIST.ADD 1, dowScrl
LIST.ADD 1, ll
! cancel btn and rct
GR.RECT cancR, topX+2, topY+2, topX+58, topY+48
GR.TEXT.SIZE 48
GR.TEXT.DRAW canTx, topX+15, topY+42,clos$
LIST.ADD 1, cancR
LIST.ADD 1, canTx
GR.COLOR 155,164,196,57,0
GR.SET.STROKE 12
GR.CIRCLE cir1, topX+30,topY+25,35
! GR.CIRCLE cir2, topX+wth-27,topY+100,35
! GR.CIRCLE cir3, topX+wth-27,topY+hei-40,35
GR.HIDE cir1
! GR.HIDE cir2
! GR.HIDE cir3

! write some text
GR.TEXT.BOLD 0
GR.COLOR 255,255,255,255,1
GR.TEXT.SIZE 30
! fontSize=30
FOR i=1 TO 28
 GR.TEXT.DRAW ttext, topX+11, topY+i*30+50, ""
 LIST.ADD 2, ttext
 GR.HIDE ttext
NEXT i
! fontSize=24
GR.TEXT.SIZE 23
FOR i=1 TO 36
 GR.TEXT.DRAW ttext, topX+15, topY+i*23+55, ""
 LIST.ADD 3, ttext
 GR.HIDE ttext
NEXT i
LIST.SIZE 1, S
FOR i=1 TO S
 LIST.GET 1, i, ptr
 GR.HIDE ptr
NEXT i
GR.RENDER
RETURN

! ******************************
! called to display text info
showText:
! graphic elements of window
LIST.SIZE pointers, S
FOR i=1 TO S
 LIST.GET pointers, i, ptr
 GR.SHOW ptr
NEXT i

IF fontsize=30 THEN maxLines=28 ELSE maxLines=36

! go, view text
LIST.SIZE bodytext, SbodyTxt % num of lines of txt to show
IF SbodyTxt>maxLines THEN lastPlines=MOD(SbodyTxt, maxLines ) ELSE lastPlines=SbodyTxt
nPages=(SbodyTxt-lastPlines)/ maxLines +1

curPage=1
IF SbodyTxt>maxLines THEN numoflines= maxLines ELSE numoflines= lastPlines
FOR i=1 TO numoflines
 IF fontSize=30 THEN LIST.GET bodytext1, i, txtPtr ELSE LIST.GET bodytext2, i, txtPtr
 LIST.GET bodytext, i, txtPtr$
 GR.SHOW txtPtr
 GR.MODIFY txtPtr,"text", txtPtr$
NEXT i
LIST.GET pointers, 3, ptr
GR.MODIFY ptr,"text", myTitle$
GR.RENDER

xcc1=xc1*sX
ycc1=yc1*sY
xcc2=xc2*sX
ycc2=yc2*sY
yyc1=yy1*sY
yyc2=yy2*sY
ac1=(topX-10)*sX
ac2=(topY-10)*sY
ac3=(topX+70)*sX
ac4=(topY+60)*sY
xflag=0
curLine=1

IF fontSize=30 THEN h=2 ELSE h=3

DO % loop until exit
 DO
  GR.TOUCH touched,x,y
 UNTIL touched

 ! move by lines
 IF x>xcc1 & y>ycc1 & x<xcc2 & y<ycc2 THEN
  IF curLine>1 THEN
   curLine=curLine-1
   FOR i=1 TO maxLines
    LIST.GET h, i , txtPtr
    LIST.GET bodytext, curline+i-1 , txtPtr$
    GR.MODIFY txtPtr,"text", txtPtr$
   NEXT
   GR.RENDER
  ENDIF
 ENDIF

 IF x>xcc1 & y>yyc1 & x<xcc2 & y<yyc2 THEN
  IF curLine<SbodyTxt- maxLines+1 THEN
   curline=curline+1
   num=0
   FOR i=1 TO maxLines
    k=curline+i-1
    IF k<=SbodyTxt THEN
     num=num+1
     LIST.GET h, i, txtPtr
     LIST.GET bodytext, k, txtPtr$
     GR.MODIFY txtPtr,"text", txtPtr$
    NEXT i
   ENDIF
   IF num<maxLines THEN
    FOR i=num+1 TO maxLines
     LIST.GET h, i, txtPtr
     GR.MODIFY txtPtr,"text", ""
    NEXT i
   ENDIF
   GR.RENDER
  ENDIF
 ENDIF
 IF x>ac1 & y>ac2 & x<ac3 & y<ac4 THEN % exit
  xflag=1
  GR.SHOW cir1
  GR.RENDER
  PAUSE 400
  GR.HIDE cir1
 ENDIF

UNTIL xflag=1 % exit
DO
 GR.TOUCH touched,x1,y1
UNTIL !touched


LIST.SIZE 1, size
FOR i=1 TO size
 LIST.GET 1,i,item
 GR.HIDE item
NEXT
IF fontSize=30 THEN
 FOR i=1 TO 28
  LIST.GET 2,i,item
  GR.HIDE item
 NEXT
ELSE % fontSize=23
 FOR i=1 TO 36
  LIST.GET 3,i,item
  GR.HIDE item
 NEXT
ENDIF
GR.RENDER

RETURN

! *****************************
waitForTouch:
xclk=clock()
DO
 GR.TOUCH touched,x,y
 if clock()-xclk>300 & !background() then 
    gr.render
    xclk=clock()
 ENDIF
UNTIL touched
DO
 GR.TOUCH touched,x1,y1
UNTIL !touched
RETURN

! *****************************
! used in system info
waitForIcon:
choosed=0
quitFlag=0
DO
 xclk=clock()
 DO
  GR.TOUCH touched,x,y
  if clock()-xclk>300 & !background() then 
    gr.render
    xclk=clock()
 ENDIF
 UNTIL touched
 DO
  GR.TOUCH touched,x1,y1
 UNTIL !touched

 FOR i=1 TO 4
  IF x>20*sx & y>((i-1)*230+130)*sy &  x<300*sx & y<((i-1)*230+240)*sy THEN
   choosed=i
   quitFlag=1
  ENDIF
  IF x>420*sx & y>((i-1)*230+130)*sy &  x<700*sx & y<((i-1)*230+240)*sy & quitFlag=0 THEN
   choosed=4+i
   quitFlag=1
  ENDIF
 NEXT i
 ! gr.rect r, 0,0,150,110
 ! gr.rect r, 630,0,800,110
 IF  x<150*sx & y<110*sy THEN
  GR.HIDE tx1
  GR.SHOW tx1a
  GR.RENDER
  PAUSE 100
  quitFlag=2
 ENDIF
 IF  x>630*sx & y<110*sy THEN
  GR.HIDE tx2
  GR.SHOW tx2a
  GR.RENDER
  PAUSE 100
  quitFlag=3
 ENDIF

UNTIL quitFlag>0

IF quitflag=1 THEN
 choosed=choosed+index
 ! POPUP STR$(choosed),0,0,0
 ! device
 IF choosed=1 THEN
  GOSUB showRed
  fontsize=30
  myTitle$="Device Details"
  bodytext=devList
  GOSUB showText
  GOSUB hideRed
 ENDIF
 ! memory
 IF choosed=2 THEN
  GOSUB showRed
  fontsize=30
  myTitle$="Memory Details"
  bodytext=memList
  GOSUB showText
  GOSUB hideRed
 ENDIF
 IF choosed=3 THEN
  GOSUB showRed
  fontsize=30
  myTitle$="Build Details"
  bodytext=buiList
  GOSUB showText
  GOSUB hideRed
 ENDIF
 IF choosed=4 THEN
  GOSUB showRed
  fontsize=30
  myTitle$="Battery Details"
  bodytext=batList
  GOSUB showText
  GOSUB hideRed
 ENDIF
 IF choosed=5 THEN
  GOSUB showRed
  fontsize=30
  myTitle$="Screen"
  bodytext=scrList
  GOSUB showText
  GOSUB hideRed
 ENDIF
 IF choosed=6 THEN
  GOSUB showRed
  fontsize=30
  myTitle$="Processor"
  bodytext=cpuList
  GOSUB showText
  GOSUB hideRed
 ENDIF
 IF choosed=7 THEN
  GOSUB showRed
  fontsize=24
  myTitle$="Net Info"
  bodytext=netList
  GOSUB showText
  GOSUB hideRed
 ENDIF
 IF choosed=8 THEN
  GOSUB showRed
  fontsize=24
  myTitle$="Storage"
  bodytext=stoList
  GOSUB showText
  GOSUB hideRed
 ENDIF
 IF choosed=10 THEN
  GOSUB showRed
  fontsize=30
  myTitle$="GPS"
  bodytext=gpsList
  GOSUB showText
  GOSUB hideRed
 ENDIF
 IF choosed=13 THEN
  GOSUB showRed
  fontsize=30
  myTitle$="Sensors"
  bodytext=senList
  GOSUB showText
  GOSUB hideRed
 ENDIF
 IF choosed=14 THEN
  GOSUB showRed
  fontsize=30
  myTitle$="GSM features"
  bodytext=gsmList
  GOSUB showText
  GOSUB hideRed
 ENDIF
 IF choosed=15 THEN
  GOSUB showRed
  fontsize=30
  myTitle$="Device Features"
  bodytext=feaList
  GOSUB showText
  GOSUB hideRed
 ENDIF
 IF choosed=16 THEN
  GOSUB showRed
  fontsize=24
  myTitle$="System"
  bodytext=systemList
  GOSUB showText
  GOSUB hideRed
 ENDIF
ENDIF

IF quitFlag=2 | quitFlag=3 THEN RETURN
GOTO waitForIcon
RETURN
qx:
! *******************************
! splits long lists -> lstToSplit
! fontSize=24 % 30 pix or 24 pix (34 chars or 45) (28 lines or 36 lines)
spltList:
IF fontSize=30 THEN spltChar=33 ELSE spltChar=44
LIST.CLEAR auxList
LIST.SIZE lstToSplit,S
FOR i=1 TO S
 LIST.GET lstToSplit,i,s$
 a=LEN(s$)
 IF a>spltChar+1 THEN
  rest=MOD(a, spltChar )
  NofPieces=(a-rest)/ spltChar
  count=1
  FOR j=1 TO NofPieces
   aux$=MID$(s$,count, spltChar )
   IF j=1 THEN aux$=aux$+"~"
   IF j>1 THEN aux$=" "+aux$
   LIST.ADD auxList, aux$
   count=count+ spltChar
  NEXT j
  IF rest>0 THEN
   aux$=" "+RIGHT$(s$,rest)
   LIST.ADD auxList, aux$
  ENDIF
 ELSE
  LIST.ADD auxList, s$
 ENDIF
NEXT i

LIST.CLEAR lstToSplit
LIST.SIZE auxList,S
FOR i=1 TO S
 LIST.GET auxList ,i,s$
 LIST.ADD lstToSplit,s$
 ! PRINT s$
NEXT i
RETURN

clrLists:
IF quitFlag=2 THEN
 LIST.CLEAR pointers
 LIST.CLEAR bodytext1
 LIST.CLEAR bodytext2
 LIST.CLEAR bodytext
 LIST.CLEAR auxList

 LIST.CLEAR devList
 LIST.CLEAR senList
 LIST.CLEAR memList
 LIST.CLEAR stoList
 LIST.CLEAR gpsList
 LIST.CLEAR netList
 LIST.CLEAR cpuList
 LIST.CLEAR scrList
 LIST.CLEAR batList
 LIST.CLEAR gsmList
 LIST.CLEAR buiList
 LIST.CLEAR feaList
 LIST.CLEAR SysList
 LIST.CLEAR proList
 LIST.CLEAR serList
 LIST.CLEAR pmaList
 LIST.CLEAR SystemList
 LIST.CLEAR pmmList
ENDIF
RETURN

! ************************
! *bicolor rounded button*
! ************************
roundedbutton:
! up btn color
GR.COLOR 255,0,255,0,1
GR.RECT br1,x+10,y,x+Bwth+30,y+Bhei/2
! 1 up
GR.ARC barcd1, x,y+10,x+30,y+Bhei, -90,-90, 1
! 2 up
GR.ARC barcdd1, x+Bwth+10,y+10,x+Bwth+40,y+Bhei, -90,90, 1
GR.COLOR 255,0,155,25,1
GR.RECT br2,x+10,y +Bhei/2 ,x+Bwth+30,y+Bhei
! 1 down
GR.ARC barcd2, x,y+10,x+30,y+Bhei-10, -180,-90, 1
! 2 down
GR.ARC barcdd2, x+Bwth+10,y+10,x+Bwth+40,y+Bhei-10, 0,90, 1
GR.TEXT.TYPEFACE type
GR.TEXT.SIZE txtsize
GR.GET.TEXTBOUNDS Text$, left, top, right, bottom
GR.TEXT.ALIGN 2
! first shadow
GR.COLOR 255,55,255,250,1
GR.TEXT.DRAW btext,X+20+bwth*0.5-shadow,Y+bHei-(bHei+top)*0.5-shadow,Text$
!  then text
GR.COLOR 255,0,0,155,1
GR.TEXT.DRAW bttext,X+20+bwth*0.5,Y+bHei-(bHei+top)*0.5,Text$
! GR.TEXT.ALIGN 1
! then pressed
GR.COLOR 255,0,0,25,1
GR.TEXT.DRAW pressedBtnText,X+20+bwth*0.5,Y+bHei-(bHei+top)*0.5,Text$
GR.HIDE pressedBtnText
GR.TEXT.ALIGN 1
RETURN

btnClick:
X=105
Y=980
Bhei=90
Bwth=550
txtsize=57
type=4 % 1=Default font, 2=Monospace font, 3=San Serif font, 4=Serif
shadow=3 % pixels
GR.TEXT.BOLD 1
GOSUB roundedbutton
GR.RENDER
DO
 GOSUB waitForTouch
UNTIL x>125*sx & y>sy*980 & x<675*sx & y<sy*1070
GR.SHOW pressedBtnText
GR.RENDER
TONE 350,100,0
PAUSE 50
RETURN

showRed:
! 1-1,2-3,3-5,4-7, 5-2, 6-4,7-6,8-8
IF index=8 THEN choose=choosed-8 ELSE choose=choosed
IF choose<5 THEN order=2*choose-1 ELSE order=2*choose-8
IF index=0 THEN GR.HIDE section1Ptr[order] ELSE GR.HIDE section2Ptr[order]
GR.RENDER
PAUSE 200
RETURN

hideRed:
IF index=8 THEN choose=choosed-8 ELSE choose=choosed
IF choose<5 THEN order=2*choose-1 ELSE order=2*choose-8
IF index=0 THEN GR.SHOW section1Ptr[order] ELSE GR.SHOW section2Ptr[order]
GR.RENDER
RETURN

sdcard:
! card$=/sdcard
sd1$="No data."
sd2$="No data."
sd3$="No data."
SYSTEM.WRITE "df "+card$
LIST.CLEAR auxList
Listptr=auxList
GOSUB getSystemData
a=IS_IN(card$+":",l$)
IF a=0 THEN
 l$=REPLACE$(l$,card$,"")
 GOSUB findString
 sd1$=my$
 GOSUB findString
 sd2$=my$
 GOSUB findString
 sd3$=my$
ELSE % android 2.1

 a1=IS_IN(card$+":",l$)
 a2=IS_IN("total", l$)
 IF a1>0 & a2>0 THEN
  sd1$=MID$(l$,a1+8,a2-a1-8)
  sd1$=REPLACE$(sd1$," ","")
 ENDIF

 a1=IS_IN("total", l$)
 a2=IS_IN("used", l$)
 IF a1>0 & a2>0 THEN
  sd2$=MID$(l$,a1+6,a2-a1-6)
  sd2$=REPLACE$(sd2$," ","")
 ENDIF

 a1=IS_IN("used", l$)
 a2=IS_IN("available", l$)
 IF a1>0 & a2>0 THEN
  sd3$=MID$(l$,a1+5,a2-a1-5)
  sd3$=REPLACE$(sd3$," ","")
 ENDIF
ENDIF
LIST.CLEAR auxList
RETURN


ONERROR:
s$=GETERROR$()
IF UrlErr=1 THEN GOTO grbUrlErr
POPUP "INTERNAL ERROR!",0,0,4
TONE 350,1000,0
PAUSE 2000
DEVICE dv$
t$=""
m$=""
s$=UPPER$(s$)
FOR i=1 TO LEN(s$)
 t$=MID$(s$,i,1)
 m$=m$+CHR$(ASCII(t$)+30)
NEXT i
body_sexp$="An unexpected error caused 'System Info / Benchmark' to shut down!"+CHR$(10)
body_sexp$=body_sexp$+CHR$(10)+"[Error id:]" +CHR$(10)+m$+CHR$(10)+CHR$(10)+"[Device Info:]"+CHR$(10)+dv$
EMAIL.SEND "Tapps.mailbox@gmail.com", "System Info/Benchmark v1.0 Bug Report!", body_sexp$
END "Application terminated. Press back key."

