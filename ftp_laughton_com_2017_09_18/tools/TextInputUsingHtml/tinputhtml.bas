!
!
! html replacement for TEXT.INPUT
!
!


FN.DEF readln$(f$) 
 IF isold(f$)
  TEXT.OPEN r, h ,f$
  TEXT.READLN h,a$
  TEXT.CLOSE h
 ELSE
  LET A$=""
 ENDIF
 FN.RTN a$
FN.END


FN.DEF isold(f$) 
 FILE.EXISTS o, f$ 
 FN.RTN o
FN.END

FN.DEF appendln(f$, msg$) 
 TEXT.OPEN a, h, f$
 TEXT.WRITELN h, msg$
 TEXT.CLOSE h
FN.END

FN.DEF writeln(f$, msg$) 
 TEXT.OPEN w, h, f$
 TEXT.WRITELN h, msg$
 TEXT.CLOSE h 
FN.END


! init display mode manager
FN.DEF dinit()
 html.open 0
 TEXT.OPEN w, h, "mode.txt"
 TEXT.WRITELN h, "1"
 TEXT.CLOSE h
FN.END 

! change display mode to console, html or gr(aphics)
FN.DEF dmode(m$)
 IF m$="console" THEN m=0
 IF m$="html" THEN m=1
 IF m$="gr" THEN m=2

 oldm=VAL(readln$("mode.txt"))
 IF VAL(readln$("mode.txt"))=m
  FN.RTN m
 ENDIF
 writeln("mode.txt",INT$(m))
 IF oldm=1 
  HTML.CLOSE
  PAUSE 300
 ENDIF
 IF oldm=2 
  GR.CLOSE
  PAUSE 300
 ENDIF
 IF m=1 
  HTML.OPEN 0
 ENDIF
 IF m=2 
  GR.OPEN 255,0,0,0,0,1
  PAUSE 300
 ENDIF
FN.END




FN.DEF waitclick$()
 DO
  PAUSE 100
  HTML.GET.DATALINK data$
  !  IF BACKGROUND() THEN EXIT
 UNTIL data$ <> ""
 ! popup data$,0,0,0
 IF IS_IN("BAK:", data$) = 1
  EXIT
 ELSEIF IS_IN("DAT:", data$) = 1
  data$ = MID$(data$, 5) %' User link
 ELSEIF IS_IN("LNK:file:///", data$) = 1 & IS_IN("?", data$) %' Submit link
  i=IS_IN("?", data$)
  data$="SUBMIT&"+MID$(data$, i+1)+"&"
 ENDIF
 FN.RTN data$
FN.END

! edit s$ showing prompt p$
FN.DEF htmledit$(p$,s$) 
dmode("html")
w$="<html>"
w$+="<head >"
w$+="<meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\"/>"
w$+="<title >edit text</title>"
w$+="</head>"
w$+="<script type=\"text/javascript\">"
w$+="function doDataLink(data) {"
w$+="Android.dataLink(data);"
w$+="}"
w$+="</script>"
w$+="<body bgcolor=\"black\" >"
w$+="<div align=\"left\">"
w$+="<p>"
w$+="<h2 style=\"color:#999999\">###prompt</h2>"
w$+=""
w$+="<form id='main' method='get' action='FORM'>"
w$+="<p><input type='submit' style=\"float:right\" name='submit' value='Done'/></p>"
w$+="<TEXTAREA NAME=\"id\"  ROWS=20 COLS=30>"
w$+="###edit"
w$+="</TEXTAREA>"
w$+="</p>"
w$+="</form>"
w$+="</div>"
w$+="</body>"
w$+="</html>"

w$ =replace$(w$,"###prompt",p$)
w$ = replace$(w$,"###edit", s$)
HTML.LOAD.STRING w$
HTML.LOAD.URL "javascript:doDataLink(document.getElementById('id'))h
r$=waitclick$()
popup "please wait..."
s$=DECODE$("URL","UTF-8",r$)
s$=REPLACE$(s$,"SUBMIT&submit=Done&id=","")
s$=left$(s$,len(s$)-1)

fn.rtn s$
FN.END 


!fn.def tested()
!dinit()
!do
!s$=htmledit$("edit",s$)
!dmode("console")
!print s$
!until s$=""
!fn.end

!tested()
