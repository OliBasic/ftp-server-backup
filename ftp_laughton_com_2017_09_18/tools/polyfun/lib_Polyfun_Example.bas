


flagOri            = 0

GOSUB                openScreen
GOSUB                lib_Polyfun


!define colors---------
ARRAY.LOAD c1[] , ~
050 , 050 , 080 , ~
255 , 255 , 255 , ~
100 , 100 , 100

ARRAY.LOAD c2[] , ~
100 , 080 , 080 , ~
255 , 000 , 000 , ~
065 , 000 , 000

ARRAY.LOAD c3[] , ~
100 , 120 , 100 , ~
000 , 255 , 000 , ~
000 , 065 , 000

ARRAY.LOAD c4[] , ~
100 , 100 , 120 , ~
000 , 000 , 255 , ~
000 , 000 , 055





FN.DEF zz ( min,max )
 FN.RTN  floor (RND()*max) + min
FN.END

FN.DEF zz2 ( min,max )
 FN.RTN  RND()*max + min
FN.END


DIM             grObj [500]
DIM             poly [500]

h             = 70

x1            = centw - centw/2
y1            = centh - centh/2
x2            = x1 + centw
y2            = y1
x3            = x1 
y3            = y1 + centh
x4            = x1 + centw
y4            = y1 + centh


rcr           = roundCornerRect   (400, 150, 20 )
rct           = roundCornerTriang (100,  50, 30  )
star          = star              ( 80, 180, 12, 0, 0, 0)
arr           = arrow             ( 350, 40, 0.25 , 0.3 , 1)

rcrDraw       = draw3d (rcr,  x1, y1, 6, 200, c1[])
rctDraw       = draw3d (rct,  x2, y2, 8, 200, c2[])
starDraw      = draw3d (star, x3, y3, 4, 200, c3[])
arrDraw       = draw3d (arr,  x4, y4, 4, 200, c4[])

GR.RENDER


pause 100


for i=1 to 25

rcr     = roundCornerRect   (zz(50,300), zz(50,300) , zz(2,100) )
rct     = roundCornerTriang (zz(5,100) ,zz(5,100) , 30  )
star    = star              (80 , zz(90,300) , zz(3,12) ,0,0,30)
arr     = arrow             ( zz(80, 300) , zz(20, 60), zz2(0.1,0.6) , zz2(0.1,0.8) , zz2(0.5,0.8) )

call      replace3d (  rcr, rcrDraw )
call      replace3d (  rct, rctDraw )
call      replace3d (  star, starDraw )
call      replace3d (  arr, arrDraw )
GR.RENDER
pause 100
next


pause 300


for i=100 to 0 step-1
call rotatePoly (rcr,(i/20)^1.2)
gr.render
next

for i=100 to 0 step-1
call rotatePoly (rct,(i/20)^1.2)
gr.render
next

for i = 100 to 0 step-1
call rotatePoly (star,(i/20)^1.2)
gr.render
next

for i=200 to 0 step-1
call rotatePoly (arr,(i/20)^1.2)
gr.render
next


gr.cls






DO


 y             = RND()* h

 DO
  x=0
  DO
   ptrB ++
   w            = h+RND()*3*h
   GOSUB          randomColor
   poly[ptrB]   = roundcornerrect ( w, h, RND()*h/2)
   grObj[ptrB]  = draw3d(poly[ ptrB ],x+w/2,y,RND()*8+1,RND()*100+150,cAct[])
   x            = x+w+12
   GR.RENDER
  UNTIL x       > curw
  y             = y+h+12
 UNTIL y        > curh+h*2



 GOSUB rotatePolys


 FOR i=1 TO ptrB
  poly [i]         = roundcornertriang ( h/3*(1+RND()), (RND()+0.1)*30, 0 )
  CALL               replace3d ( poly[i] , grObj[i] )
  GR.RENDER
 NEXT



 GOSUB rotatePolys




 FOR i=1 TO ptrB
  poly[i]            = star ( h/4*(1+RND()), h/2*(1+RND()) ,round (rnd ()*8+4), 0, 0,RND()*200 )
  CALL               replace3d ( poly[i] , grObj[i] )
  GR.RENDER
 NEXT



 GOSUB rotatePolys



 FOR i=1 TO ptrB
  poly[i]            = arrow ( h*(1+RND()), h/7 *(1+RND()) , 0.25 , 0.8, 0.8)
  CALL                 replace3d ( poly[i] , grObj[i] )
  GR.RENDER
 NEXT



 GOSUB rotatePolys




 PAUSE 2000


 DO
 UNTIL               0



randomColor:

 UNDIM               cAct[]
 anzcol=4
 ptrCol            = floor (RND()*4) +1
 IF ptrCol         = 1 THEN array.copy c1 [], cAct[]
 IF ptrCol         = 2 THEN ARRAY.COPY c2 [], cAct[]
 IF ptrCol         = 3 THEN array.copy c3 [], cAct[]
 IF ptrCol         = 4 THEN array.copy c4 [], cAct[]

 RETURN

 

 rotatePolys:

 FOR i=1 TO ptrB
  CALL              rotatepoly ( poly [i] , i)
  GR. render
 NEXT

 RETURN


 lib_Polyfun:

 FN.DEF                draw3d (poly,x, y, stroke, alpha , col[] )
  GR.SET.STROKE        stroke
  GR.COLOR             alpha , col[1] , col[2] , col[3] , 1
  GR.POLY              nn0, poly, x, y
  GR.COLOR             255, col[4] , col[5] , col[6] , 0
  GR.POLY              nn1, poly, x, y
  GR.COLOR             255, col[7] , col[8] , col[9] , 0
  GR.POLY              nn2, poly, x+1,y+1

  FN.RTN               nn0
 FN.END


 FN.DEF                 replace3d ( poly, ptr )
  GR.MODIFY             ptr     , "list", poly
  GR.MODIFY             ptr+1   , "list", poly
  GR.MODIFY             ptr+2   , "list", poly
 FN.END


 FN.DEF                 rotatePoly(poly, phi)
  sinphi              = sin (toradians (phi))
  cosphi              = cos (toradians (phi))
  LIST.SIZE             poly,sz
  FOR i               = 1 TO sz STEP 2
   list .get            poly, i  , x
   list .get            poly, i+1, y
   list .replace        poly, i  ,  x*cosphi + y*sinphi
   list .replace        poly, i+1, -x*sinphi + y*cosphi
  NEXT
 FN.END


 FN.DEF                 translatePoly(poly, dx, dy)
  LIST.SIZE             poly,sz
  FOR i=1 TO sz STEP 2
   list .get            poly, i  , x
   list .get            poly, i+1, y
   list .replace        poly, i  , x+dx
   list .replace        poly, i+1, y+dy
  NEXT
 FN.END


 FN.DEF arrow ( arrLen , arrWid, headRelLen, headRelWid,facCorner)
  IF                     arrHeadRelWid< arrRelWidth THEN  arrHeadRelWid = arrRelWidth
  p0y	                 =	 arrLen * 0.5
  p0x	                 =	 arrWid * 0.5
  p1                   = -p0y    * (1-headRelLen*2)
  p2                   =  p0x    +  arrWid* headRelWid
  p3                   =  p1     + (1-facCorner) * headRelLen *  arrLen
  ARRAY.LOAD              a[], p0x,p0y,  p0x,p1 ,   p2,p3   , 0,-p0y ~
  -p2,p3  , -p0x,p1 , -p0x,p0y
  LIST.CREATE             n, poly
  LIST.ADD.ARRAY          poly, a[]
  FN.RTN                  poly
 FN.END



 FN.DEF                roundCornerTriang ( h, r, dphiGlob)
  dphiGlob           = toradians ( dphiGlob )
  n                  = 10
  phioffs            = 20
  phiStart           = toradians ( phioffs )
  phiend             = toradians (180- phioffs )
  dphi               = (phiend-phistart) / n
  LIST.CREATE          N, S1
  FOR j               = 0 TO 240 STEP 120
   mx                 = h*sin (toradians (j) +dphiGlob )
   my                 = -h*cos (toradians (j) +dphiGlob )
   dphi2              = toradians (j)
   FOR phi            = phiStart TO phiEnd STEP dphi
    phii              = phi + dphi2 + dphiGlob
    LIST.ADD            s1, mx-COS(phii)*r, my-sin (phii )*r
    ctr++
   NEXT
  NEXT
  FN.RTN s1
 FN.END


 FN.DEF                roundCornerRect (b, h, r)
  if                   b <2*r then r=b/2
  if                   h <2*r then r=h/2
  half_pi            = 3.14159 / 2
  dphi               = half_pi / 8
  LIST.CREATE          N, S1
  mx                 = -b/2+r
  my                 = -h/2+r
  FOR phi            = 0 TO half_pi STEP dphi
   LIST.ADD             s1, mx-COS(phi)*r, my-sin (phi)*r
  NEXT
  mx                 = b/2-r
  my                 = -h/2+r
  FOR phi            = 0 TO half_pi STEP dphi
   LIST.ADD            s1, mx+SIN(phi)*r, my- cos (phi)*r
  NEXT
  mx                 = b/2-r
  my                 = h/2-r
  FOR phi            = 0 TO half_pi STEP dphi
   LIST.ADD            s1, mx+COS(phi)*r, my+sin (phi)*r
  NEXT
  mx                 = -b/2+r
  my                 =  h/2-r
  FOR phi            = 0 TO half_pi STEP dphi
   LIST.ADD            s1, mx-SIN(phi)*r, my+cos (phi)*r
  NEXT
  FN.RTN s1
 FN.END


 FN.DEF       star (innerRad, outerRad, anzSpikes,rndRad,rndPhi, dphi )
  anzCor           = anzSpikes *2
  phiStep          = TORADIANS( 360/anzCor)
  dphi             = TORADIANS( dphi)
  LIST.CREATE        N, S1
  FOR i            = 1 TO anzCor
   phi             = i*phiStep
   radbas          = innerRad
   IF                !MOD(i,2)      THEN
    radbas         = outerRad
   ENDIF
   IF rndRad         THEN radbas = radbas *  (1 + (RND()-0.5)*2*rndRad)
   IF rndPhi         THEN phi    = phi + phiStep* (RND()-0.5)*2*rndPhi
   LIST.ADD          s1, radbas* cos (phi+dphi)
   LIST.ADD          s1, radbas* SIN (phi+dphi)
   !PRINT            ctr, i,radbas, tmp   %, coo [i*2-1], coo [i*2]
  NEXT
  FN.RTN s1
 FN.END



 RETURN





 openScreen:

 refW       = 780
 refH       = 1280
 IF !flagOri  THEN SWAP refW, refH
 centW      = refW/2
 centH      = refH/2
 GR.OPEN      255,0,0,30,0,flagOri
 GR.SCREEN    curW, curH
 scaW       = curW / refW
 scaH       = curH / refH
 GR.SCALE     scaW , scaH
 desLoopTime= 50

 RETURN

