!-------------------------------------------
 lib_Polyfun:

 FN.DEF                draw3d (poly,x, y, stroke, alpha , col[] )
  GR.SET.STROKE        stroke
  GR.COLOR             alpha , col[1] , col[2] , col[3] , 1
  GR.POLY              nn0, poly, x, y
  GR.COLOR             255, col[4] , col[5] , col[6] , 0
  GR.POLY              nn1, poly, x, y
  GR.COLOR             255, col[7] , col[8] , col[9] , 0
  GR.POLY              nn2, poly, x+1,y+1

  FN.RTN               nn0
 FN.END


 FN.DEF                 replace3d ( poly, ptr )
  GR.MODIFY             ptr     , "list", poly
  GR.MODIFY             ptr+1   , "list", poly
  GR.MODIFY             ptr+2   , "list", poly
 FN.END


 FN.DEF                 rotatePoly(poly, phi)
  sinphi              = sin (toradians (phi))
  cosphi              = cos (toradians (phi))
  LIST.SIZE             poly,sz
  FOR i               = 1 TO sz STEP 2
   list .get            poly, i  , x
   list .get            poly, i+1, y
   list .replace        poly, i  ,  x*cosphi + y*sinphi
   list .replace        poly, i+1, -x*sinphi + y*cosphi
  NEXT
 FN.END


 FN.DEF                 translatePoly(poly, dx, dy)
  LIST.SIZE             poly,sz
  FOR i=1 TO sz STEP 2
   list .get            poly, i  , x
   list .get            poly, i+1, y
   list .replace        poly, i  , x+dx
   list .replace        poly, i+1, y+dy
  NEXT
 FN.END


 FN.DEF arrow ( arrLen , arrWid, headRelLen, headRelWid,facCorner)
  IF                     arrHeadRelWid< arrRelWidth THEN  arrHeadRelWid = arrRelWidth
  p0y	                 =	 arrLen * 0.5
  p0x	                 =	 arrWid * 0.5
  p1                   = -p0y    * (1-headRelLen*2)
  p2                   =  p0x    +  arrWid* headRelWid
  p3                   =  p1     + (1-facCorner) * headRelLen *  arrLen
  ARRAY.LOAD              a[], p0x,p0y,  p0x,p1 ,   p2,p3   , 0,-p0y ~
  -p2,p3  , -p0x,p1 , -p0x,p0y
  LIST.CREATE             n, poly
  LIST.ADD.ARRAY          poly, a[]
  FN.RTN                  poly
 FN.END



 FN.DEF                roundCornerTriang ( h, r, dphiGlob)
  dphiGlob           = toradians ( dphiGlob )
  n                  = 10
  phioffs            = 20
  phiStart           = toradians ( phioffs )
  phiend             = toradians (180- phioffs )
  dphi               = (phiend-phistart) / n
  LIST.CREATE          N, S1
  FOR j               = 0 TO 240 STEP 120
   mx                 = h*sin (toradians (j) +dphiGlob )
   my                 = -h*cos (toradians (j) +dphiGlob )
   dphi2              = toradians (j)
   FOR phi            = phiStart TO phiEnd STEP dphi
    phii              = phi + dphi2 + dphiGlob
    LIST.ADD            s1, mx-COS(phii)*r, my-sin (phii )*r
    ctr++
   NEXT
  NEXT
  FN.RTN s1
 FN.END


 FN.DEF                roundCornerRect (b, h, r)
  if                   b <2*r then r=b/2
  if                   h <2*r then r=h/2
  half_pi            = 3.14159 / 2
  dphi               = half_pi / 8
  LIST.CREATE          N, S1
  mx                 = -b/2+r
  my                 = -h/2+r
  FOR phi            = 0 TO half_pi STEP dphi
   LIST.ADD             s1, mx-COS(phi)*r, my-sin (phi)*r
  NEXT
  mx                 = b/2-r
  my                 = -h/2+r
  FOR phi            = 0 TO half_pi STEP dphi
   LIST.ADD            s1, mx+SIN(phi)*r, my- cos (phi)*r
  NEXT
  mx                 = b/2-r
  my                 = h/2-r
  FOR phi            = 0 TO half_pi STEP dphi
   LIST.ADD            s1, mx+COS(phi)*r, my+sin (phi)*r
  NEXT
  mx                 = -b/2+r
  my                 =  h/2-r
  FOR phi            = 0 TO half_pi STEP dphi
   LIST.ADD            s1, mx-SIN(phi)*r, my+cos (phi)*r
  NEXT
  FN.RTN s1
 FN.END


 FN.DEF       star (innerRad, outerRad, anzSpikes,rndRad,rndPhi, dphi )
  anzCor           = anzSpikes *2
  phiStep          = TORADIANS( 360/anzCor)
  dphi             = TORADIANS( dphi)
  LIST.CREATE        N, S1
  FOR i            = 1 TO anzCor
   phi             = i*phiStep
   radbas          = innerRad
   IF                !MOD(i,2)      THEN
    radbas         = outerRad
   ENDIF
   IF rndRad         THEN radbas = radbas *  (1 + (RND()-0.5)*2*rndRad)
   IF rndPhi         THEN phi    = phi + phiStep* (RND()-0.5)*2*rndPhi
   LIST.ADD          s1, radbas* cos (phi+dphi)
   LIST.ADD          s1, radbas* SIN (phi+dphi)
   !PRINT            ctr, i,radbas, tmp   %, coo [i*2-1], coo [i*2]
  NEXT
  FN.RTN s1
 FN.END

 RETURN
!-------------------------------------------



 
