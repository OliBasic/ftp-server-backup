The PreProcessor program allows to write BASIC! programs with multiple commands per line
by using :: as command separator. Example:

	FOR i=1 TO 10 :: PRINT i :: NEXT

It allows to write and run such BASIC! programs from the BASIC! integrated editor.

In order to use the feature:

- 1. The file preproc.bas must be in the same folder as your BASIC! program;

- 2. Remember to save your program before running it in the BASIC! Editor;

- 3. Use // instead of !! in your program for multi-line comments;

- 4. If your program is named "xxx.bas" then it must have the following structure (it's important that
     the 1st line contains RUN and that the 2nd and last lines begin with !!):

                RUN "preproc.bas","xxx.bas"
                !!

                ...your program goes here...

                !!

- 5. For your convenience you could use the program preproc_template.bas as a template for writing your
     programs (remember to replace ***** with the name of yor program and change the file name when
     saving it):

                RUN "preproc.bas","*****.bas"
                !! Program start

                

                !! Program end


Example: test.bas
-----------------

RUN "preproc.bas","test.bas"
!! Program start

! Counting
do :: i=i+1 :: print i
until i = 10

!! Program end


Technical Note
--------------

The preprocessor does the following:
        - Generates a new program (xxx_1.bas) in which:
                - The first two lines of xxx.bas (RUN and !!) are dropped;
                - For all the remaining lines: 
			- As soon as a line beginning with !! is found, such line is dropped and the 
                          new program is considered complete;
                        - If the line begins with // then // is replaced with !!
                        - Then, if the line doesn't begin with ! it is split into multiple lines
                          considering :: as a line separator.
        - Runs xxx_1.bas

	
