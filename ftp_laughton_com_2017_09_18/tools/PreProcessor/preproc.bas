! ---------------
!  PREPROCESSOR
!  Version 1.2
!  by luca_italy
! ---------------

in$="../source/"+##$
out$="../source/"+LEFT$(in$,LEN(in$)-4)+ "_1.bas"
TEXT.OPEN R,in,in$
TEXT.OPEN W,out,out$
TEXT.READLN in,line$
TEXT.READLN in,line$
TEXT.WRITELN out,"PAUSE 500"
DO
 TEXT.READLN in,line$
 IF LEFT$(line$,2)="!!" THEN D_U.BREAK
 IF LEFT$(line$,2)="//" THEN
  line$ = "!!"+MID$(line$,3)
  incomment = !incomment
 ENDIF
 IF LEFT$(line$,1)="!" | incomment THEN
  TEXT.WRITELN out,line$
 ELSE
  line1$=""
  FOR i=1 TO LEN(line$)
   c$=MID$(line$,i,1)
   IF c$="\"" & (i=1 | MID$(line$,i-1,1)<>CHR$(92)) THEN
    inquote = !inquote
    line1$ = line1$+c$
   ELSEIF c$="%" & !inquote THEN
    F_N.BREAK
   ELSEIF c$=":" & inquote THEN
    line1$ = line1$+CHR$(92)+":"
   ELSE
    line1$ = line1$+c$
   ENDIF
  NEXT
  line1$ = REPLACE$(line1$,":::",": ::")
  SPLIT cmd$[],line1$,"::"
  ARRAY.LENGTH l,cmd$[]
  FOR i=1 TO l
   TEXT.WRITELN out,cmd$[i]
  NEXT
  UNDIM cmd$[]
 ENDIF
UNTIL 0
TEXT.CLOSE in
TEXT.CLOSE out
RUN out$
END
