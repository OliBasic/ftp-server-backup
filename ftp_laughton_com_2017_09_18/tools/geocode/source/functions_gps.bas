latitude=0
longitude=0
address$=""
found=0

fn.def my_address_to_gps(address$,latitude,longitude,found)
tlen =len(address$)
!? ">" + address$+"<"
address$=trim$(address$)
!address$=replace$( address$," ","+") 
!address$=replace$( address$,"++","+") 
address$=replace$( address$,chr$(10),"+")
address$=replace$( address$,",","+")
address$ =replace$( address$," ","") 
 ! 
!
!
! ">" + address$+"<" 
url$ = "https://maps.googleapis.com/maps/api/geocode/xml?address="+address$
graburl location$, url$
undim location$[]
split location$[],location$,"/>"
l=len(location$)
a=is_in("<location>",location$)
newtxt$= mid$(location$,a,68)
newtxt$=replace$(newtxt$,"<location>","")
newtxt$=replace$(newtxt$,"</location>","")
newtxt$=replace$(newtxt$,"<","")
newtxt$=replace$(newtxt$,">","")
newtxt$=replace$(newtxt$,"lat","")
newtxt$=replace$(newtxt$,"/lat","")
newtxt$=replace$(newtxt$,"lng","")
newtxt$=replace$(newtxt$,"/lng","") 
newtxt$=replace$(newtxt$,"lat","")
newtxt$=replace$(newtxt$,"/",",") 
undim locgps$[] 

split locgps$[],newtxt$,","

array.length tlen,locgps$[]
if tlen>1
found=1
latitude=val(locgps$[1])
longitude=val(locgps$[2])
 
else
found=0
latitude=0
longitude=0
endif
!fn.return latitude
fn.end


fn.def my_gps_to_address(latitude,longitude,address$,found)
urll$="http://maps.googleapis.com/maps/api/geocode/json?latlng="+str$(latitude)+","+str$(longitude)+"&sensor=false"
graburl address$, urll$
alen=len(address$)
!rint "initial address "+address$
address$=replace$(address$,"\"","")

address$=replace$(address$,"{","") 

address$=replace$(address$,"[","") 
 
address$=replace$(address$,"}","") 

address$=replace$(address$,"]","") 

address$=replace$(address$,":","") 
 
a=is_in("formatted_address",address$)
b=is_in("geometry",address$)

address$=replace$(mid$(address$,a,b-a),"formatted_address","")

address$ =trim$(address$)

alen=len(address$)
if alen>0
found=1
else
address$="Address Not Found"
found =0
endif 

fn.end 


fn.def my_get_map_url(latitude,longitude,zoom,maptype$,imgname$,url$,sw,sh)
mlurl$="https://maps.googleapis.com/maps/api/staticmap?center="+~
 FORMAT$("###.########",latitude)+~
  ","+FORMAT$("###.########",longitude)+"&zoom="+FORMAT$("##",zoom)+~
  "&scale="+FORMAT$("#",1)+"&size="+FORMAT$("#####",sw)+"x"+FORMAT$("#####",sh)+~
"&maptype="+maptype$
 url$=REPLACE$(mlurl$," ","") 
url$=url$+"&markers=color:blue%7Clabel:H%7c"+~
"%7c"+str$(latitude)+","+str$(longitude)
 

 

fn.end 

 
fn.def my_get_map_image(fname$,url$)
BYTE.OPEN r,xx,url$
 !PAUSE 10000
 FILE.DELETE del,fname$
! fname$,url$ 
 BYTE.COPY xx,fname$

 DO
  FILE.EXISTS iif,fname$
 UNTIL iif>0

 BYTE.CLOSE xx 
!popup"ok",0,0,0
fn.end
