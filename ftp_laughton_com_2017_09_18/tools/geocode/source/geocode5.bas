INCLUDE "functions_gr.bas"
INCLUDE "functions_gps.bas"



defaults: 
ARRAY.LOAD mtype$[],~
"Roadmap",~
"Satellite",~
"Terrain",~
"Hybrid"

myzoom =15
mymaptype$="satellite"

file.exists fi,"settings"
if fi>0
grabfile data$,"settings"
split data$[],data$,","
myzoom=val(data$[1])
mymaptype$=data$[2]
endif 

ARRAY.LOAD menu$[],~
"Address to Lat/Lng",~
"Lat/Lng to Address",~
"Exit" 

ARRAY.LENGTH mlen,menu$[]



vrs:
a=255:r=80:g=60:b=50:ss=0:pt=1
a1=255:r1=20:g1=120:b1=170
frame_num=0
appname$="GeoCode"


my_gr_start(a,r,g,b,&sw,&sh,&sd,&ts,ss,pt)
mh=(ts*mlen*2)+(ts*2) 
mh1=ts*8 %sh-mh-ts

GOSUB mk_menu 

GR.BITMAP.DRAW menu,menu_num,~
0,ts*2
GR.BITMAP.DRAW wt,twait,0,(sh/2)-(mh/2)+ts
GR.BITMAP.DRAW small,tsmall, (sw/2)-(mh1/2),mh+(ts/2)
GR.BITMAP.DRAW big,tbig,0,ts*2
GR.BITMAP.DRAW frame,frame_num,0,0
GR.HIDE wt
gr.hide small
gr.hide big

GR.RENDER

DO
 GOSUB wft
IF m=0 & tcx1>ts*2
 GOSUB chs_menu
ENDIF 
UNTIL b=1

goto append

wft:
DO
 GR.TOUCH tch,tcx,tcy
UNTIL tch>0

DO
 GR.TOUCH tch1,tcx1,tcy1
UNTIL tch1<1

IF tcx1<ts*2 & tcy1<ts*2
 GOSUB help
ENDIF 

RETURN

help:
!POPUP"Help screen",0,(sh/2)+ts,0
do
dialog.message "Settings & Information",~
"This small app will convert a street address to a gps location and vice versa",~
dm1,~
"Settings","More","OK"
if dm1=1
do
input "Big Image Zoom (1 to 19)",~
myzoom,myzoom
until myzoom>0 & myzoom<20
do 
dialog.select dm2,mtype$[],~
"Map Type"
until dm2>0
mymaptype$=mtype$[dm2]
ttext$=str$(myzoom)+","+~
mymaptype$
byte.open w,tnum,"settings"
byte.write.byte tnum,ttext$
byte.close tnum


endif

if dm1=2
dialog.message "More Help",~
"Address Fields"+chr$(10)+~
"Use <Enter> for New lines"+chr$(10)+~
"Input as Much Info as Possible"+chr$(10)+~
"Don't Use Funny Characters"+chr$(10)+~
"Exit Keypad and Tap <Finish> to Search",~
dm2,~
"More","OK"
if dm2=1
dialog.message "More Help",~
"Latitude/Longitude Fields"+chr$(10)+~
"Use a comma <,> to separate coordinates",~
dm3,~
"OK" 
endif

endif
until dm1<>1 | dm1<>2

RETURN


chs_menu: 
slctd=FLOOR(tcy1/(ts*2))
IF slctd>4 THEN slctd=0
IF slctd<2 THEN slctd=0
slctd =slctd -1

IF slctd>0

 GOSUB anm1
 IF slctd = 1
  GOSUB a_to_g
  clipboard.put location1$

 ENDIF

 IF slctd = 2
  GOSUB g_to_a
  clipboard.put address1$
 ENDIF 

 IF menu$[slctd]="Exit"
  b=1
 ENDIF

ENDIF

GOSUB anm1r 

RETURN 

a_to_g:
!POPUP"Address to GPS",0,(sh/2)+ts,0

TEXT.INPUT address1$, address1$, "Address"
address$=address1$ 
GOSUB anw1

my_address_to_gps(address$,&latitude,&longitude,&found)
!? found,latitude,longitude

my_gps_to_address(latitude,longitude,&address$,&found)

gosub mspa



IF found>0
location1$=str$(latitude)+" , "+str$(longitude)
gosub shs
ENDIF
GOSUB anw1r 
! "returned "+location1$
! "returned "+location$
 
RETURN

mspa:
!? address$,address1$
!address$=replace$(address$,","," ")
undim tarry$[]
tarry$=""
split tarry$[], address$,","
array.length tlen ,tarry$[]
for t=1 to tlen
tarry$=tarry$+trim$(tarry$[t])+chr$(10)

next t
address1$= tarry$
return
 

g_to_a:

!POPUP"GPS to Address",0,(sh/2)+ts,0

 TEXT.INPUT location1$, location1$, "GPS Location  {latitude , longitude}"
if location1$=""
location1$="0.0000 , 0.0000"
endif
 location1$= REPLACE$(location1$,CHR$(10),",")
 location1$= REPLACE$(location1$,"{","")
 location1$= REPLACE$(location1$,"}","")


 UNDIM tarray$[]
 SPLIT tarray$[],location1$,","
 ARRAY.LENGTH llen,tarray$[]

latitude1= VAL(tarray$[1])
longitude1= VAL(tarray$[2]) 
latitude=latitude1
longitude=longitude1

GOSUB anw1 

my_gps_to_address(latitude,longitude,&address$,&found)
GOSUB gt_img_s 
GOSUB gt_img_b 

!PRINT found,address$

IF found>0
gosub mspa
! "sent "+location1$
! latitude1,longitude1
! found
! 51.167857 , 6.935225 
 
gosub shs 
ENDIF 
GOSUB anw1r 
RETURN


shs:
gr.show text_info
undim ttarray$[]
split ttarray$[],replace$(address1$,chr$(10),","),","
!dialog.select ttt,ttarray$[],""


if tlen>0
gr.modify h1,"text",tarry$[1]
endif
if tlen>1
gr.modify h2,"text",tarry$[2]
endif 
if tlen>2
gr.modify h3,"text",tarry$[3]
endif 
if tlen>3
gr.modify h4,"text",tarry$[4]
endif
!end 
gr.modify h5,"text","Latitude "+str$(latitude)

gr.modify h6,"text","Longitude "+str$(longitude)



 

 GOSUB gt_img_s
 GOSUB gt_img_b 
 GR.BITMAP.LOAD timg,"small.png"
 GR.BITMAP.SCALE simg,timg,mh1,mh1
 GR.MODIFY small,"bitmap",simg


!? address1$,latitude,longitude
!dialog.select tarry,tarry$[],"Address" 

GOSUB anw1r 
 gr.show small
 GR.RENDER
 m=1
 GOSUB wft
 IF tcx1>(sw-mh1)/2 & tcx1<sw-((sw-mh1)/2) 
  IF tcy1>mh
   GR.BITMAP.LOAD timg,"big.png"
   GR.BITMAP.SCALE simg,timg,sw,sw
   GR.MODIFY big,"bitmap",simg
   GR.HIDE small
   gr.show big
   GR.RENDER 
   GOSUB wft 
  ENDIF
 ENDIF
 m=0
 b=0

GR.HIDE big
GR.HIDE small 
   gr.hide text_info 
   GR.RENDER 
return 

gt_img_s:
tsw =sw
tsh =sh
sw=mh1
sh=mh1
zoom=17
url$=""
fname$="small.png"
maptype$=mtype$[1]
my_get_map_url(latitude,longitude,zoom,maptype$,imgname$,&url$,sw,sh) 
my_get_map_image(fname$,url$)
sw=tsw
sh=tsh

RETURN 

gt_img_b:
tsw =sw
tsh =sh
sw=sw
sh=sw
zoom=myzoom
maptype$=lower$(mymaptype$)
url$=""
fname$="big.png"
!maptype$=mtype$[1]
my_get_map_url(latitude,longitude,zoom,maptype$,imgname$,&url$,sw,sh) 
my_get_map_image(fname$,url$)
sw=tsw
sh=tsh

RETURN 



anw1: 
GR.SHOW wt
FOR t=0 TO sw STEP 20 *(sd/120) 
 GR.MODIFY wt,"x",-sw+t
 GR.RENDER
NEXT t 
RETURN

anw1r: 

FOR t=0 TO sw STEP 20 *(sd/120)
 GR.MODIFY wt,"x",0-t
 GR.RENDER
NEXT t 

RETURN 

anm1:
FOR t=0 TO sw STEP 20 *(sd/120) 
 GR.MODIFY menu,"x",0-t
 GR.RENDER
NEXT t
RETURN 

anm1r:
FOR t=0 TO sw STEP 20 *(sd/120) 
 GR.MODIFY menu,"x",-sw+t
 GR.RENDER
NEXT t

RETURN 


mk_menu:
GR.BITMAP.CREATE menu_num,sw,mh
GR.BITMAP.DRAWINTO.START menu_num
GR.COLOR a-50,r-50,g-50,b-50,1
GR.RECT trect,0,0,sw,mh 
GR.COLOR a1,r1,g1,b1,1
GR.TEXT.SIZE ts*1.2
GR.TEXT.DRAW ttext,ts/2,ts*1.5,"Options"
GR.TEXT.SIZE ts*0.8 
FOR t=1 TO mlen
 GR.TEXT.DRAW tc,ts,(ts*1.25)+(t*ts*2),menu$[t]
 GR.LINE tl, ~
 ts/2,(t*ts*2),~
 sw-(ts/2),(t*ts*2) 
NEXT t 
GR.BITMAP.DRAWINTO.END

my_make_appframe~
(&frame_num,sw,sh,ts,appname$,a,r,g,b,a1,r1,g1,b1)

GR.BITMAP.CREATE tsmall,mh1 ,mh1

GR.BITMAP.CREATE tbig,sw,sw

GR.TEXT.SIZE ts*0.8
GR.COLOR a1,r1,g1,b1,1
GR.TEXT.DRAW h1,ts,ts*3,"Information "
GR.TEXT.DRAW h2,~
ts,ts*4,"Address  "
GR.TEXT.DRAW h3,~
ts,ts*5,"  "
GR.TEXT.DRAW h4,~
ts,ts*6,"  "
GR.TEXT.DRAW h5,~
ts,ts*7,"Latitude  " 
GR.TEXT.DRAW h6,~
ts,ts*8,"Longitude  " 

GR.GROUP text_info,~
h1,h2,h3,h4,h5,h6

GR.HIDE text_info 


GR.BITMAP.CREATE twait,sw,sh-mh-(ts*2)
GR.BITMAP.DRAWINTO.START twait
GR.COLOR 180,180,180,180,1
GR.RECT trect,0,0,sw,sh
GR.COLOR 255,180,0,60,1
text$="Please Wait"
t=ts
DO
 t=t+1
 GR.TEXT.SIZE t
 GR.TEXT.WIDTH tw,text$
UNTIL tw>sw-(ts*2)
GR.TEXT.DRAW ttext,(sw/2)-(tw/2),mh/2,text$
GR.BITMAP.DRAWINTO.END


RETURN

append:
!exit
