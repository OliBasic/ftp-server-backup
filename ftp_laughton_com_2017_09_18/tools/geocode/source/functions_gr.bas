sw=0 %screen width
sh=0 % height
sd=0 % density 
ts=0 % statusbar height
ss=0 % show statusbar
pt=0 % portrait 

a=0 % background alpha
r=0 % red
g=0 % green
b=0 % blue

 
fn.def my_gr_start(a,r,g,b,sw,sh,sd,ts,ss,pt)
gr.open a,r,g,b,ss,pt
gr.screen sw,sh,sd
gr.statusbar ts
fn.end

!my_gr_start(a,r,g,b,&sw,&sh,&sd,&ts,ss,pt)
!print sw,sh,sd,ts

!pause 3000


!!
makes a frame with appname and  button

!!

fn.def my_make_appframe~
(frame_num,sw,sh,ts,appname$,a,r,g,b,a1,r1,g1,b1)
gr.bitmap.create frame_num,sw,sh
gr.bitmap.drawinto.start frame_num
gr.color a1,r1,g1,b1,0
gr.set.stroke ts/2
gr.rect trect,0,0,sw,sh
gr.set.stroke ts/4
gr.rect trect,0,0,sw,ts*2
gr.color a1,r1,g1,b1,1 
gr.text.size ts*1.2
gr.set.stroke 1
gr.text.width tw,appname$
gr.text.draw ttext,(sw/2)-(tw/2),ts*1.5,appname$
gr.text.draw ttext,ts,ts*1.5,"?"
gr.bitmap.drawinto.end
fn.end 
