GR.OPEN 50,50,50,150,0,1
GR.SCREEN sw,sh
ts=sh/16

GOSUB funcs


GR.BITMAP.CREATE tmain,sw,sh
GR.BITMAP.DRAWINTO.START tmain
GR.COLOR 255,160,160,190,1
GR.TEXT.ALIGN 3
GR.TEXT.TYPEFACE 4,2
GR.TEXT.SIZE ts*2
GR.TEXT.DRAW ttxt,sw-ts,ts*2,"Geo"

GR.COLOR 255,140,140,170,1
GR.TEXT.TYPEFACE 3,3
GR.TEXT.SIZE ts*0.9
GR.TEXT.DRAW ttxt,sw-ts,ts*2.8,"Code"

GR.TEXT.ALIGN 2
GR.COLOR 255,140,140,170,1
GR.TEXT.TYPEFACE 4,3
GR.TEXT.SIZE ts
tt=ts

txt$="Find by GPS Coordinates"


!gr.text.width tw,txt$
DO
 GR.TEXT.WIDTH tw,txt$
 IF tw >= sw*0.66
  tt=tt-1
  GR.TEXT.SIZE tt
 ENDIF
UNTIL tw< sw*0.66
txt$="Find Me"
GR.TEXT.DRAW ttxt,sw/2,ts*4,txt$
txt$= "Find by GPS Coordinates"
GR.TEXT.DRAW ttxt,sw/2,ts*6,txt$
txt$="Find by Street Address"
GR.TEXT.DRAW ttxt,sw/2,ts*8,txt$
txt$="Save Loc."
GR.TEXT.DRAW ttxt,sw/4,ts*10,txt$
txt$="View Loc. List"
GR.TEXT.DRAW ttxt,sw-(sw/4),ts*10,txt$

txt$="Navigate to Location"
GR.TEXT.DRAW ttxt,sw/2,ts*12,txt$
txt$="Copy Address"
GR.TEXT.DRAW ttxt,sw/4,ts*14,txt$
txt$="Copy Coordinates"
GR.TEXT.DRAW ttxt,sw-(sw/4),ts*14,txt$
GR.BITMAP.DRAWINTO.END
GR.BITMAP.DRAW main,tmain,0,0



GR.TEXT.SIZE tt*1.2
txt$="GPS Information"
GR.TEXT.DRAW gl1a ,sw/2,ts*1,txt$

GR.TEXT.SIZE tt*0.8
txt$="Accuracy"
GR.TEXT.DRAW gl2a,sw/2,ts*2,txt$

txt$="Satellite Count"
GR.TEXT.DRAW gl3a,sw/2,ts*3,txt$

txt$="Latitude"
GR.TEXT.DRAW gl4a,sw/2,ts*4,txt$

txt$="Longitude"
GR.TEXT.DRAW gl5a,sw/2,ts*5,txt$

txt$="Altitude"
GR.TEXT.DRAW gl6a,sw/2,ts*6,txt$

txt$="Speed"
GR.TEXT.DRAW gl7a,sw/4,ts*7,txt$

txt$="Bearing"
GR.TEXT.DRAW gl8a,sw-(sw/4),ts*7,txt$

GR.TEXT.SIZE tt*1.2
txt$="Street Address"
GR.TEXT.DRAW gl1b,sw/2,ts*9,txt$

GR.TEXT.SIZE tt*0.8
txt$="Address Line 1"
GR.TEXT.DRAW gl2b,sw/2,ts*10,txt$

txt$="Address Line 2"
GR.TEXT.DRAW gl3b,sw/2,ts*11,txt$

txt$="Address Line 3"
GR.TEXT.DRAW gl4b,sw/2,ts*12,txt$

txt$="Address Line 4"
GR.TEXT.DRAW gl5b,sw/2,ts*13,txt$

txt$="Address Line 5"
GR.TEXT.DRAW gl6b,sw/2,ts*14,txt$

txt$="BACK"
GR.TEXT.DRAW gl7b,sw/2,ts*15,txt$


GR.GROUP xgpslabls ,gl1a,~
gl2a ,gl3a ,gl4a ,gl5a ,gl6a ,gl7a ,gl8a, gl7b


GR.GROUP adreslabls ,gl1b,~
gl2b ,gl3b ,gl4b ,gl5b ,gl6b

!gr.hide main
GR.HIDE adreslabls
GR.HIDE xgpslabls


GR.COLOR 150,200,50,50,1
GR.TEXT.SIZE ts*2
GR.TEXT.DRAW xpwt,sw/2,sh/2,"Wait"
GR.TEXT.SIZE ts*1
GR.TEXT.DRAW xpwt1 ,sw/2,(sh/2)+ts,"10 sec"

GR.HIDE xpwt
GR.HIDE xpwt1
GR.RENDER

DO
 GOSUB wft
 IF mode=0
  GOSUB xdo
  xbtn=0
tch=0
tch1=0
  PAUSE 300
 ENDIF


UNTIL bck=1
!end
exit

wft:
DO
 DO
  GR.TOUCH tch,tcx,tcy
 UNTIL tch>0 | bck=1
 DO
  GR.TOUCH tch1,tcx1,tcy1
 UNTIL tch1<1 | bck=1
UNTIL bck=1 | (ABS(tcy-tcy1)+ABS(tcx-tcx1))<ts
xbtn=(tcy/ts)
!POPUP STR$(xbtn),0,0,0
RETURN

xdo:
IF bck=0
 IF xbtn>3 & xbtn<5
  GR.HIDE main
  GR.SHOW xgpslabls
  !gr.show adreslabls
  GR.RENDER
  moda=1
  mode=1
  GOSUB findme
  mode=0
  moda=0
  GR.SHOW main
  GR.HIDE xgpslabls
  GR.RENDER

 ENDIF
 IF xbtn>5 & xbtn<7
  GOSUB findgps
 ENDIF
 IF xbtn>7 & xbtn<9
  GOSUB findaddress
 ENDIF
 IF xbtn>9 & xbtn<11
  !?xbtn
  !end
  IF tcx1<sw/2

   GOSUB saveplace
  ELSE
   GOSUB viewplacelist
  ENDIF
 ENDIF
 IF xbtn>11 & xbtn<13
  GR.HIDE main
  GR.SHOW xgpslabls
  GR.SHOW adreslabls
  GR.RENDER
  GOSUB navto
  GR.SHOW main
  GR.HIDE xgpslabls
  GR.HIDE adreslabls
  GR.RENDER
 ENDIF
 IF xbtn>13 & xbtn<14.5
  IF tcx1<sw/2
   CLIPBOARD.PUT gadr$
   POPUP gadr$+" Copied to Clipboard",0,0,0

   !gosub copyaddress
  ELSE
   ctxt$= "Lat. "+STR$(glat)+"° , Lng. "+STR$(glng)+"°"

   CLIPBOARD.PUT ctxt$
   POPUP ctxt$+" Copied to Clipboard",0,0,0


   !gosub copycoords
  ENDIF
 ENDIF
ENDIF
RETURN

navto:
IF bck=0
 moda=0
 mode=1
 dstadr$=gadr$
 dstlat$=STR$(glat)
 dstlng$=STR$(glng)
 dstalt=galt

 GOSUB findme
 glat$=TRIM$(STR$(glat))
 glng$=TRIM$(STR$(glng))
 z=18
 !"geo:" +glat$+ ","+glat$+ "?q="+dstlat$+","+dstlng$+"("+dstadr$+")"

 !? "geo:" +glat$+ ","+glng$+"?z=15"
 APP.START "android.intent.action.VIEW" ,~
 "geo:" +glat$+ ","+glat$+"?q="+dstlat$+","+dstlng$

 GR.HIDE gl7b
 GR.RENDER

 !url$= "https://maps.google.com?saddr="+~
 !gadr$+"&daddr="+dstadr$


 !browse url$
 mode=0
 moda=0
ENDIF
!?main,"$"

RETURN


viewplacelist:
IF bck=0
 mode=1
 FILE.EXISTS fi,"saves"
 IF fi<1
  BYTE.OPEN w,fnum,"saves"
  BYTE.CLOSE fnum
 ENDIF

 GRABFILE osave$,"saves"
oslen=len(osave$)
if oslen>5
 UNDIM save$[]
 SPLIT save$[],osave$,":"
 ARRAY.LENGTH slen,save$[]
 !dialog.select ss,save$[],""
 UNDIM s$[]: UNDIM s1$[]: UNDIM s2$[]: UNDIM s3$[]
 DIM s$[slen]
 DIM s1$[slen]
 DIM s2$[slen]
 DIM s3$[slen]
 !?t,save$[1]
 FOR t=1 TO slen
  !?t,save$[t]
  ttxt$=REPLACE$(save$[t],"_",":")
  !? "$",ttxt$
  SPLIT s$[],ttxt$,":"

  s1$[t] =s$[1]
  s2$[t] =s$[2]
  s3$[t] =s$[3]


 NEXT t
 DIALOG.SELECT s,s1$[],"Saved Locations"
 IF s>0
  pname$=s1$[s]
  gadr$=s2$[s]
  SPLIT loc$[],s3$[s],","
  glat=VAL(loc$[1])
  glng=VAL(loc$[2])
  galt=VAL(loc$[3])
  DIALOG.MESSAGE "Saved Location",~
  "Address"+CHR$(10)+~
  REPLACE$(gadr$,",",CHR$(10))+CHR$(10)+~
  CHR$(10)+"GPS"+CHR$(10)+~
  "Latitude "+STR$(glat)+"°"+CHR$(10)+~
  "Longitude "+STR$(glng)+"°"+CHR$(10)+~
  "Altitude "+STR$(galt)+" m",~
  dmsl,"Remove","Load"

  IF dmsl=1
   rname$=pname$+"_"+gadr$+"_"+ s3$[s]+":"
   !?
   txt$=REPLACE$(osave$,rname$,"")
   !txt$=osave$+npname$
   BYTE.OPEN w,fnum,"saves"
   BYTE.WRITE.BYTE fnum,txt$
   BYTE.CLOSE fnum
  ELSE
   GOSUB spadres
   GOSUB updtgps
   GR.HIDE main
   GR.SHOW xgpslabls
   GR.SHOW adreslabls
   GR.RENDER
   !GR.MODIFY gl4a,"text","Latitude "+ STR$(ROUND(glat,6))+"°"
   ! GR.MODIFY gl5a,"text","Latitude "+ STR$(ROUND(glng,6))+"°"
   ! GR.MODIFY gl6a,"text","Altitude "+ STR$(ROUND(galt,0))+" m"

   ! GR.SHOW gl1a
   ! GR.SHOW gl4a
   ! GR.SHOW gl5a
   ! GR.SHOW gl6a
   GR.RENDER
   DO
    GOSUB wft
    !?xbtn,"xbut"

   UNTIL xbtn>=15


   GR.SHOW main
   GR.HIDE xgpslabls
   GR.HIDE adreslabls
   GR.RENDER

  ENDIF
 ENDIF
endif
 mode=0

ENDIF
RETURN

saveplace:
IF bck=0
 mode=1
 FILE.EXISTS fi,"saves"
 IF fi<1
  BYTE.OPEN w,fnum,"saves"
  BYTE.CLOSE fnum
 ENDIF

 GRABFILE osave$,"saves"


 !undim save$[]
 !split save$[],osave$,":"
 !array.length slen,save$[]
 !undim s$[]
 !dim s$[slen,
 !for t=1 to slen
 !next t

 INPUT "Place Name" ,pname$ ,gadr$,cncld
 IF cncld<1
  npname$=pname$+"_"+gadr$+"_"+STR$(glat)+","+STR$(glng)+","+STR$(galt)+":"
  txt$=osave$+npname$
  BYTE.OPEN w,fnum,"saves"
  BYTE.WRITE.BYTE fnum,txt$
  BYTE.CLOSE fnum
 ENDIF
 PAUSE 300
 mode=0
ENDIF
RETURN


findme:
GR.SHOW xpwt
GR.SHOW xpwt1
GR.RENDER
GR.HIDE gl7b
GR.RENDER
IF bck=0

 gacc=1000
 timout=10000
 minacc=1
 GPS.OPEN
 sttime=TIME()
 DO
  ctime=TIME()
  GPS.TIME gtime
  GPS.ACCURACY gacc
spent=round(time()-sttime,0,"d")
lft=round((timout-spent)/1000,0,"d")
ytxt$=replace$(str$(lft),".0","")+" sec"
gr.modify xpwt1,"text",ytxt$
gr.render

 UNTIL ctime-gtime<1000 | spent>timout
 !popup "1",0,0,0
 DO
  GPS.ACCURACY gacc
  GPS.LOCATION gtime,,gcount,~
  gacc,glat,glng,galt,gbrn,gspd
  ctime=TIME()
  GOSUB updtgps

 UNTIL gacc<=minacc & ctime-gtime<1000 | TIME()-sttime>timout
 GPS.CLOSE
 !popup "2",0,0,0
 GR.HIDE xpwt
GR.hide xpwt1
 GR.SHOW gl7b
 GR.RENDER
 IF moda=1
  DO
   GOSUB wft
  UNTIL xbtn>14
 ENDIF

ENDIF

RETURN

updtgps:
GR.MODIFY gl2a,"text","Accuracy "+ STR$(ROUND(gacc,0))+" m"
GR.MODIFY gl3a,"text","Satelte Count "+ STR$(ROUND(gcount,0))
GR.MODIFY gl4a,"text","Latitude "+ STR$(ROUND(glat,6))+"°"
GR.MODIFY gl5a,"text","Latitude "+ STR$(ROUND(glng,6))+"°"
GR.MODIFY gl6a,"text","Altitude "+ STR$(ROUND(galt,0))+" m"
GR.MODIFY gl7a,"text","Speed "+ STR$(ROUND(gspd,0))+" m/s"
GR.MODIFY gl8a,"text","Bearing "+ STR$(ROUND(gbrn,0))+"°"
spent=round(time()-sttime,0,"d")
lft=round((timout-spent)/1000,0,"d")
ytxt$=replace$(str$(lft),".0","")+" sec"
gr.modify xpwt1,"text",ytxt$
?
?ytxt$
?replace$(str$(TIME()-sttime>timout),".0","")+"
GR.RENDER
RETURN

findgps:
IF bck=0
 mode =1
 GR.HIDE main
 GR.SHOW xgpslabls
 GR.SHOW adreslabls
 DIALOG.MESSAGE "FIND","By GPS Coordinates",~
 dmfg,"This Location","Input a Location"
 IF dmfg=1 
  GOSUB findme
  found = 0
  !gadr$=""
  GR.SHOW xpwt
  GR.RENDER
  my_gps_to_address(glat,glng,&gadr$,&found)
  GR.HIDE xpwt
  GR.RENDER
  IF found=1
   GOSUB spadres
  ENDIF
 ENDIF

 IF dmfg=2
  DO
   DIALOG.MESSAGE "FIND",~
   "Latitude "+STR$(ROUND(glat,6))+CHR$(10)+~
   "Longitude "+STR$(ROUND(glng,6))+CHR$(10),~
   dmf,"Latitude","Longitude","Okay"
   IF dmf=1
    INPUT "Latitude",glat,glat
   ENDIF
   IF dmf=2
    INPUT "Longitude",glng,glng
   ENDIF
   IF dmf=3
    my_gps_to_address(glat,glng,&gadr$,&found)
    dmf=0
    IF found=1
     GOSUB spadres
     GR.MODIFY gl2a,"text", ""
     GR.MODIFY gl3a,"text", ""
     GR.MODIFY gl4a,"text","Latitude "+ STR$(ROUND(glat,6))+"°"
     GR.MODIFY gl5a,"text","Latitude "+ STR$(ROUND(glng,6))+"°"
     GR.MODIFY gl6a,"text", ""
     GR.MODIFY gl7a,"text", ""
     GR.MODIFY gl8a,"text", ""

     GR.RENDER
    ENDIF
   ENDIF

  UNTIL dmf=0
 ENDIF

 GR.RENDER
 DO
  GOSUB wft
 UNTIL xbtn>14
 GR.SHOW main
 GR.HIDE xgpslabls
 GR.HIDE adreslabls
 GR.RENDER
 mode =0
ENDIF
RETURN

spadres:
UNDIM gadr$[]
SPLIT gadr$[],gadr$,","
ARRAY.LENGTH alen,gadr$[]
IF alen>0
 GR.MODIFY gl2b,"text",gadr$[1]
ELSE
 GR.HIDE gl2b
ENDIF
IF alen>1
 GR.MODIFY gl3b,"text",gadr$[2]
ELSE
 GR.HIDE gl3b
ENDIF
IF alen>2
 GR.MODIFY gl4b,"text",gadr$[3]
ELSE
 GR.HIDE gl4b
ENDIF
IF alen>3
 GR.MODIFY gl5b,"text",gadr$[4]
ELSE
 GR.HIDE gl5b
ENDIF
IF alen>4
 GR.MODIFY gl6b,"text",gadr$[5]
ELSE
 GR.HIDE gl6b
ENDIF
RETURN

findaddress:
IF bck=0
 mode =1
 GR.HIDE main
 !GR.SHOW xgpslabls
 GR.SHOW adreslabls
 GR.RENDER
 DIALOG.MESSAGE "FIND","By Street Address",~
 dmfg,"This Location","Input a Location"
 IF dmfg=1 
  moda=0
  GR.SHOW xgpslabls
  GR.RENDER
  GOSUB findme
  moda=0
  found = 0
  gadr$=""
  found=0
  GR.SHOW xpwt
  GR.RENDER

  my_gps_to_address(glat,glng,&gadr$,&found)
  GR.HIDE xpwt
  GR.RENDER
  GOSUB spadres
  GR.RENDER
  found=0

  GR.RENDER
  DO
   GOSUB wft
  UNTIL xbtn>14
 ENDIF
 IF dmfg=2
  INPUT "Street Address",gadr$,gadr$
  found=0
  my_address_to_gps(gadr$,&glat,&glng,&found)
  IF found=1
   GR.MODIFY gl4a,"text","Latitude "+ STR$(ROUND(glat,6))+"°"
   GR.MODIFY gl5a,"text","Latitude "+ STR$(ROUND(glng,6))+"°"
   GR.SHOW gl1a
   GR.SHOW gl4a
   GR.SHOW gl5a

   GR.RENDER
   my_gps_to_address(glat,glng,&gadr$,&found)
   GOSUB spadres
   GR.SHOW gl7b

   GR.RENDER
   DO
    GOSUB wft
   UNTIL xbtn>14

  ENDIF
 ENDIF


 GR.SHOW main
 GR.HIDE xgpslabls
 GR.HIDE adreslabls
 GR.RENDER
 mode =0

ENDIF
RETURN


funcs:
found=0
glat=0:glng=0:galt=0:gacc=100:gspd=0:gcnt=0:gbrn=0:gadr$=""
FN.DEF my_gps_to_address(glat,glng,gadr$,found)
 urll$="http://maps.googleapis.com/maps/api/geocode/json?latlng="+~
 STR$(glat)+","+STR$(glng)+"&sensor=false"

 GRABURL address$, urll$
 alen=LEN(address$)
 address$=REPLACE$(address$,"\"","")
 address$=REPLACE$(address$,"{","") 
 address$=REPLACE$(address$,"[","") 
 address$=REPLACE$(address$,"}","") 
 address$=REPLACE$(address$,"]","") 
 address$=REPLACE$(address$,":","") 
 a=IS_IN("formatted_address",address$)
 b=IS_IN("geometry",address$)
 address$=REPLACE$(MID$(address$,a,b-a),"formatted_address","")
 address$ =TRIM$(address$)
 alen=LEN(address$)
 IF alen>0
  found=1
  gadr$=REPLACE$(address$,",",CHR$(10))
  gadr$=TRIM$(gadr$)
  gadr$=REPLACE$(gadr$,CHR$(10),",")
 ELSE
  gadr$="Address Not Found"
  found =0
 ENDIF 
FN.END 

FN.DEF my_address_to_gps(gadr$,glat,glng,found)
 tlen =LEN(gadr$)
 address$=REPLACE$(gadr$," ","+") 
 address$=REPLACE$(address$,CHR$(10),"+")
 address$=REPLACE$(address$,",","+")
 address$=REPLACE$(address$,"++","+")
 address$ =TRIM$(address$)  
 url$ = "https://maps.googleapis.com/maps/api/geocode/xml?address="+address$
 GRABURL location$, url$
 UNDIM location$[]
 SPLIT location$[],location$,"/>"
 l=LEN(location$)
 a=IS_IN("<location>",location$)
 newtxt$= MID$(location$,a,68)
 newtxt$=REPLACE$(newtxt$,"<location>","")
 newtxt$=REPLACE$(newtxt$,"</location>","")
 newtxt$=REPLACE$(newtxt$,"<","")
 newtxt$=REPLACE$(newtxt$,">","")
 newtxt$=REPLACE$(newtxt$,"lat","")
 newtxt$=REPLACE$(newtxt$,"/lat","")
 newtxt$=REPLACE$(newtxt$,"lng","")
 newtxt$=REPLACE$(newtxt$,"/lng","") 
 newtxt$=REPLACE$(newtxt$,"lat","")
 newtxt$=REPLACE$(newtxt$,"/",",") 
 UNDIM locgps$[] 

 SPLIT locgps$[],newtxt$,","

 ARRAY.LENGTH tlen,locgps$[]
 IF tlen>1
  found=1
  glat=VAL(locgps$[1])
  glng=VAL(locgps$[2])

 ELSE
  found=0
  glat=0
  glng=0
 ENDIF
 gadr$=REPLACE$(address$,"+",",")
FN.END


RETURN


ONBACKKEY:

IF mode=0
 DO
  DIALOG.MESSAGE "EXIT","Are You Sure?",~
  dmex,"Exit","Cancel"
  IF dmex=1
   bck=1
  ENDIF
 UNTIL dmex=1 | dmex=2

ENDIF
BACK.RESUME
