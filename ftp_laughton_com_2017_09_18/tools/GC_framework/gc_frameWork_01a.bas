! ui base framework
! for BASIC!
! mod by B A Houghton
! based on Graphic Controls by Jeff Kerr
! Graphic Controls version 1.04

!*************************************
!***  start of ui base framework   ***
!*************************************

include GraphicConstants.bas
include GraphicControls.bas

!***  Open graphics screen
!***  set opacity, bg color, orientation 
!***  & statusbar visibility
rc=InitGraphics(bcOPAQUE,bcWHITE,bcPORTRAIT,bcSHOWSTATUSBAR)

!*************************************
!***  set variables                ***
!************************************* 
curPath$="../../../../"

!***  get screen dimensions
!***  datesize used for messagebox & calendar 
Bundle.Get 1,"DateSize",DateSize
Bundle.Get 1,"ScreenWidth",ScreenWidth
Bundle.Get 1,"ScreenHeight",ScreenHeight

!***  note: opting to forego frmScale variable
!***  replacing with dynamic UI (below)
!***  datesize calc
DateSize=ScreenWidth/15
if DateSize>50 then
  DateSize=50
endif 
gr.render
height=ScreenHeight
width=ScreenWidth

!***  define UI grid coodinates
!***  positioning gives 48 usable positions
!***  44 if statusbar is visible!
!***  should be orientation independent

!***  UI coordinate
!***  top, left
  yt1=height-height
  yt2=(height/12)+5
  yt3=((height/12)*2)+5
  yt4=((height/12)*3)+5
  yt5=((height/12)*4)+5
  yt6=((height/12)*5)+5
  yt7=((height/12)*6)+5
  yt8=((height/12)*7)+5
  yt9=((height/12)*8)+5
  yt10=((height/12)*9)+5
  yt11=((height/12)*10)+5
  yt12=((height/12)*11)+5

!***  UI coordinate
!***  side, left (upper corner)
  xt1=(width-width)+10
  xt2=(width/4)+5
  xt3=(width/2)+5
  xt4=((width/4)*3)+5

!***  width of UI control
!***  right side of control
  ctrlWidth=(width/4)-10

!***  height of UI control
!***  bottom coordinate
  ctrlHeight=(height/12)-10

!***  set 'about' variables
!***  change these as needed
title$="  G.C. Framework"
author$="  B A Houghton"
version$="  0.1"
website$=""

!*************************************
!***  define ui                    ***
!*************************************
!***  define UI
!***  datesize needed for msgbox
!***  sound off by default (clicks)
!***  replace frmScale w/ 1
DefineUIMainForm:
rc=StartNewForm(DateSize,bcSOUNDOFF$,1)

!***  define main form
gosub mainForm:  

!***  define menu
gosub menu: 

 
!*************************************
!***  draw ui                      ***
!************************************* 
DrawUIMainForm:
rc=DrawForm("Loading...",picPath$)

gosub hideMenu: 
gosub displayMainForm:

!*************************************
!***  event loop                   ***
!************************************* 
TestWaitForTouch:
selCtrl=TouchCheck(Period)

!***  Check which type of control was touched.
sw.begin selCtrl
  sw.case ctrlmenuButton1
      gosub hideMenu:
      gosub displayMainForm:
    sw.break
  sw.case ctrlmenuButton2
      !***  action to initiate
    sw.break
  sw.case ctrlmenuButton3
      !***  action to initiate
    sw.break
  sw.case ctrlmenuButton4
      !***  action to initiate
    sw.break
  sw.case ctrlmenuButton5
      gosub hideMenu:
      gosub displayMainForm: 
      Button$=MsgBox$("Program Name:"+title$+bcRECBREAK$+"Author:"+author$+bcRECBREAK$+"Version:"+version$+bcRECBREAK$+"Website:"+website$,bcOKONLY$+bcINFORMATION$+bcNOBORDER$,"ABOUT...")
    sw.break
  sw.case ctrlmenuButton6
      !***  use end to quit runtime
      !***  use exit to quit basic!
      !***  use latter in apk or shortcut
      end
      !exit
    sw.break
  sw.case ctrlButton
      !*** show menu
      gosub displayMenu:
    sw.break
  sw.end

!***  enter/reenter loop
goto TestWaitForTouch

!*************************************
!***  define menu                  ***
!*************************************
menu:
!***  mimic Android menu UI
!***  6 buttons
!***  
  !***  control reference:
  !controlID=AddControl(control type,caption,
                        !caption text color,
                        !caption bg color,
                        !data text color, 
                        !data bg color,
                        !top,left,middle,width,
                        !height,
                        !font size,style)
  

!***  define frame
!***  use frame as the menu structure
!***  supports 6 buttons: 2 rows x 3 columns
!***  bottom 1/4 of display
ctrlMenuFrame=AddControl(bcFRMFRAME,"a",bcWHITE,bcgray,0,0,(height/4)*3,0,0,width,height/4,20,bcNOBORDER$+bcDATBOLD$) 
  
!***  add UI controls to form
!***  items are parented to frame by default
!***  since they are within the frame 
ctrlmenuButton1=AddControl(bcFRMBUTTON,"a",bcBLACK,bcWHITE,0,0,(height/4)*3,0,0,width/3,height/8,20,bcNOBORDER$)

ctrlmenuButton2=AddControl(bcFRMBUTTON,"",bcBLACK,bcWHITE,0,0,(height/4)*3,width/3,0,width/3,height/8,20,bcNOBORDER$)

ctrlmenuButton3=AddControl(bcFRMBUTTON,"",bcBLACK,bcWHITE,0,0,(height/4)*3,(width/3)*2,0,width/3,height/8,20,bcNOBORDER$)

ctrlmenuButton4=AddControl(bcFRMBUTTON,"",bcBLACK,bcWHITE,0,0,(height/8)*7,0,0,width/3,height/8,20,bcNOBORDER$)

ctrlmenuButton5=AddControl(bcFRMBUTTON,"About",bcBLACK,bcWHITE,0,0,(height/8)*7,width/3,0,width/3,height/8,20,bcNOBORDER$)

ctrlmenuButton6=AddControl(bcFRMBUTTON,"Exit",bcBLACK,bcWHITE,0,0,(height/8)*7,(width/3)*2,0,width/3,height/8,20,bcNOBORDER$)

RETURN
!*************************************

!*************************************
!***  define UI form 1: mainForm   ***
!*************************************
mainForm:
!***  define controls here
  
  !***  control reference:
  !controlID=AddControl(control type,caption,
                        !caption text color,
                        !caption bg color,
                        !data text color, 
                        !data bg color,
                        !top,left,middle,width,
                        !height,
                        !font size,style)
  
!***  define frame
!***  using frames as basis of form
ctrlMainFormFrame=AddControl(bcFRMFRAME,"MENU",bcWHITE,bcWHITE,0,0,0,0,0,width,height,20,bcNOBORDER$+bcDATBOLD$)
  
!***  add UI controls to form
!***  items are parented to frame by default
!***  since they are within the frame 

ctrlButton=AddControl(bcFRMBUTTON,"Menu",bcBLACK,bcLGRAY,0,0,yt3,xt4,0,ctrlWidth,ctrlHeight,30,bcNOBORDER$)

RETURN
!*************************************

!*************************************
!***  display menu                 ***
!*************************************
displayMenu:

  rc=showCtrl(ctrlMenuFrame,4) 
  gr.render
RETURN
!*************************************

!*************************************
!***  hide menu                 ***
!*************************************
hideMenu:

  rc=hideCtrl(ctrlMenuFrame,1) 
  gr.render
RETURN
!*************************************

!*************************************
!***  display main form            ***
!*************************************
displayMainForm:

  rc=showCtrl(ctrlMainFormFrame,1) 
  gr.render
RETURN
!*************************************

!*************************************
!***  hide main form               ***
!*************************************
hideMainForm:

  rc=hideCtrl(ctrlMainFormFrame,1) 
  gr.render
RETURN
!*************************************

 
!*************************************
!***   end of ui base framework    ***
!*************************************
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
