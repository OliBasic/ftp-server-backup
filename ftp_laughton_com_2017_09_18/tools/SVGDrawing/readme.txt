A 'graphic language' where the commands look as much as possible like RFOBASIC!'s commands.
From the programme an SVG file is created and shown on HTML screen.
If this file is saved it can be read by a HTML5 supported browser. Probably best to use is Chrome,
latest version. Other programmes exist which may read SVG as well,like IrfanView (plugin needed).

A short guide, the supplied programme hopefully helps a bit more.

All needed routines are at the back of the demo prog (below EXIT command).
All x,y parameters represent millimeters if the page is printed on A4 paper.

Overview of example commands. Note: no object_pointers

- GOSUB GrInit ____________________________________________ initialize functions and variables (use only once)
- GrOpen$="255,255,255,255":GOSUB GrOpen __________________ like GR.OPEN(alpha,red,green,blue) (no extra's)
- GrColor$="255,255,0,0,1" ________________________________ like GR.COLOR(alpha,red,green,blue,style)
- GrSetAntiAlias$="0" _____________________________________ like GR.SET.ALIAS(0/1)
- GrSetStroke$="3" ________________________________________ like GR.SET.STROKE(>0)
- GOSUB GrRender __________________________________________ like GR.RENDER
- GOSUB GrClose ___________________________________________ like GR.CLOSE

- GrLine$="0.2,5,209.8,5":GOSUB GrLine ____________________ like GR.LINE(x1,y1,x2,y2)
- GrRect$="100,10,180,50":GOSUB GrRect ____________________ like GR.RECT(
left,top,right,bottom)
- GrOval$="5,70,205,120":GOSUB GrOval
 _____________________ like GR.OVAL(
left,top,right,bottom)
- GrCircle$="30,30,20":GOSUB GrCircle
 _____________________ like GR.CIRCLE(x,y,radius)
- GrPoly=PolyPtr:GOSUB GrPoly
 _____________________________ like GR.POLY list_pointer

- GrTextAlign$="1" ________________________________________ like GR.TEXT.ALIGN 1/2/3
- GrTextSize$="30" ________________________________________ like GR.TEXT.SIZE nexp
- GrTextTypeface$="1,4" ___________________________________ like GR.TEXT.TYPEFACE number.style
- GrTextBold$="1" _________________________________________ like GR.TEXT.BOLD 0/1
- GrTextSkew$="1" _________________________________________ like GR.TEXT.SKEW 0/1
- GrTextUnderline$="1" ____________________________________ like GR.TEXT.UNDERLINE 0/1
- GrTextStrike$="0" _______________________________________ like GR.TEXT.STRIKE 0/1
- GrTextDraw$="10,280,This is TEXT":GOSUB GrTextDraw
 ______ like GR.TEXT.DRAW x,y,text

- GrRotate$="start,45,105,150"
 ____________________________ like GR.ROTATE.START angle,x,y
- GrRotate$="end"
 _________________________________________ like GR.ROTATE.END
