! Create a calibrated (xy) drawing
! Requires BASIC! v1.86 and Android v.4
! Aat Don @2015

DataPath$="../../svgdrawing/data/"
! x,y List for Polygon (star)
LIST.CREATE N,PolyPtr
LIST.ADD PolyPtr,117,145,126,174,156,174,132,192,141,220,117,203,92,220,101,192,77,174,107,174

! ----- Initialize --------------------------------------
GOSUB GrInit
! =======================================================
! ----- Open "alpha,red,green,blue"----------------------
GrOpen$="255,255,255,255":GOSUB GrOpen
! =======================================================

! General graphic settings ##############################
! ----- AntiAlias "0" 0r "1" ----------------------------
GrSetAntiAlias$="0"
! =======================================================
! ----- Stroke ">=0" ------------------------------------
GrSetStroke$="3"
! =======================================================
! ----- Color "alpha,red,green,blue,style" --------------
GrColor$="255,255,0,0,1"
! =======================================================
! ----- Rotate "start,angle,x,y" ------------------------
GrRotate$="start,45,105,150"
! =======================================================
! ----- Rotate "end" ------------------------------------
GrRotate$="end"
! =======================================================

! #######################################################

GrColor$="255,255,0,0,2"
! ----- Line "x1,y1,x2,y2" ------------------------------
GrLine$="0,5,210,5":GOSUB GrLine
GrLine$="0,292,210,292":GOSUB GrLine
GrLine$="5,0,5,297":GOSUB GrLine
GrLine$="205,0,205,297":GOSUB GrLine
! =======================================================
GrColor$="255,0,255,0,2"
! ----- Rectangle "left,top,right,bottom" ---------------
GrRect$="100,10,180,50":GOSUB GrRect
! =======================================================
GrColor$="255,0,255,255,1"
! ----- Oval "left,top,right,bottom" --------------------
GrOval$="5,70,205,120":GOSUB GrOval
! =======================================================
GrColor$="255,255,255,0,0"
! ----- Circle "x,y,radius" -----------------------------
GrCircle$="30,30,20":GOSUB GrCircle
! =======================================================
GrColor$="255,255,0,0,0"
! ----- Closed Polygon list pointer ---------------------
GrPoly=PolyPtr:GOSUB GrPoly
! =======================================================

! Text attributes #######################################
! ----- Size ">0" ---------------------------------------
GrTextSize$="30"
! =======================================================
! ----- Typeface "1-4",Style "1-4" -(default = arial)----
GrTextTypeface$="1,4"
! =======================================================
! ----- Bold "0" 0r "1" ---------------------------------
GrTextBold$="1"
! =======================================================
! ----- Italic "0" 0r "1" -------------------------------
GrTextSkew$="1"
! =======================================================
! ----- Underline "0" 0r "1" ----------------------------
GrTextUnderline$="1"
! =======================================================
! ----- Strike-through "0" 0r "1" -----------------------
GrTextStrike$="0"
! =======================================================
! ----- Align "1", "2" 0r "3" ---------------------------
GrTextAlign$="1"
! =======================================================
! #######################################################

GrColor$="255,0,0,255,0"
GrSetStroke$="2"
! ----- Draw Text "x,y,TXT" -----------------------------
GrTextDraw$="10,280,This is TEXT":GOSUB GrTextDraw
! =======================================================

! ******** Create a calibration bar (cm) horizontally ************
GrColor$="255,0,0,0,2"
GrSetStroke$="0.3"
GrTextAlign$="2"
GrTextSize$="8"
GrTextBold$="0"
GrTextSkew$="0"
GrTextUnderline$="0"
GrLine$="20,240,120,240":GOSUB GrLine
FOR i= 0 TO 10
	X$=INT$(i*10+20)
	GrLine$=X$+",235,"+X$+",245":GOSUB GrLine
	GrTextDraw$=X$+",233,"+INT$(i):GOSUB GrTextDraw
NEXT i
GrTextDraw$="135,240,cm":GOSUB GrTextDraw
! ****************************************************************
! ******** Create a calibration bar (mm) verticallly *************
GrTextAlign$="1"
GrLine$="180,10,180,50":GOSUB GrLine
FOR i=10 TO 50 STEP 10
	GrLine$="180,"+INT$(i)+",185,"+INT$(i):GOSUB GrLine
	GrTextDraw$="190,"+INT$(i+2.5)+","+INT$(i-10):GOSUB GrTextDraw
NEXT i
GrTextDraw$="185,60,mm":GOSUB GrTextDraw
! ****************************************************************

! ----- Render the scene --------------------------------
GOSUB GrRender
! =======================================================

! Wait for back key
POPUP "Hit Back Key to continue"
DO
	HTML.GET.DATALINK D$
UNTIL D$<>""

! ----- Close graphics -(and save file)------------------
GOSUB GrClose
! =======================================================


! vvvvvvvvvvvvvvvvvv copy the created demo file to Windows and print it from chrome vvvvvvv
! Create a demo more suitable for printing
GrOpen$="255,255,255,255":GOSUB GrOpen
GrColor$="255,0,0,0,0"
GrRect$="12.5,12.5,197.5,284.5":GOSUB GrRect
GrRect$="130,260.5,197.5,284.5":GOSUB GrRect
GrLine$="130,272.5,197.5,272.5":GOSUB GrLine
GrTextAlign$="2"
GrTextDraw$="164,270,Drawing #0001":GOSUB GrTextDraw
GrTextDraw$="164,280,@AD2015":GOSUB GrTextDraw
GrRect$="30,15,180,45":GOSUB GrRect
GrLine$="30,35,180,35":GOSUB GrLine
FOR i=30 TO 180
	GrLine$=STR$(i)+",35,"+STR$(i)+",40":GOSUB GrLine
	IF i/10=INT(i/10) THEN
		GrLine$=STR$(i)+",34,"+STR$(i)+",43":GOSUB GrLine
	ENDIF
NEXT i
GrColor$="255,0,0,0,1"
GrTextSize$="5"
FOR i=1 TO 14
	GrTextDraw$=STR$(i*10+30)+",33,"+INT$(i):GOSUB GrTextDraw
NEXT i
GrTextDraw$="50,20,ruler 15 cm":GOSUB GrTextDraw
GrColor$="255,0,0,0,0"
GrSetStroke$="2"
GrCircle$="165,23,4":GOSUB GrCircle
GrSetStroke$="0.3"
GrColor$="255,0,0,0,0"
GrTextSize$="8"
GrCircle$="105,150,80":GOSUB GrCircle
GrCircle$="105,150,90":GOSUB GrCircle
FOR i=0 TO 345 STEP 15
	x1=80*COS((i)*pi()/180)+105
	y1=80*SIN((i)*pi()/180)+150
	x2=90*COS((i)*pi()/180)+105
	y2=90*SIN((i)*pi()/180)+150
	GrLine$=STR$(x1)+","+STR$(y1)+","+STR$(x2)+","+STR$(y2):GOSUB GrLine
NEXT i
GrRotate$="start,45,105,150"
GrTextDraw$="105,150,Print this drawing from (Chrome) Browser":GOSUB GrTextDraw
GrRotate$="end"
GrLine$="105,70,105,130":GOSUB GrLine
GrLine$="105,70,102,75":GOSUB GrLine
GrLine$="105,70,108,75":GOSUB GrLine
GrLine$="105,160,105,230":GOSUB GrLine
GrLine$="105,230,102,225":GOSUB GrLine
GrLine$="105,230,108,225":GOSUB GrLine
GrRotate$="start,90,108,100"
GrTextDraw$="108,100,16 cm":GOSUB GrTextDraw
GrRotate$="end"
GrColor$="255,0,0,0,1"
GrTextDraw$="105,250,set printer margins to 0 !":GOSUB GrTextDraw
GOSUB GrRender
POPUP "Hit Back Key to continue"
DO
	HTML.GET.DATALINK D$
UNTIL D$<>""
GOSUB GrClose
! ^^^^^^^^^^^^^^^^ end of printable demo^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

EXIT

! $$$$$$$$$$$$$$$$$$$$$$$$ Needed routines for the above programme $$$$$$$$$$$$$$$$$$$$$$$$
GrInit:
	FN.DEF GetColor$(GrColor$)
		RGBa$=WORD$(GrColor$,2,",")+","+WORD$(GrColor$,3,",")+","+WORD$(GrColor$,4,",")+","+STR$(VAL(WORD$(GrColor$,1,","))/255)
		FN.RTN RGBa$
	FN.END
	FN.DEF FillOption$(Fill$,GrSetStroke$,RGBa$)
		IF Fill$="0" THEN F$="stroke-width:"+GrSetStroke$+";fill:none;"
		IF Fill$="1" THEN F$="stroke-width:0;fill:rgba("+RGBa$+");"
		IF Fill$="2" THEN F$="stroke-width:"+GrSetStroke$+";fill:rgba("+RGBa$+");"
		FN.RTN F$
	FN.END
	FN.DEF Rotate$(GrRotate$)
		R$=""
		IF LOWER$(WORD$(GrRotate$,1,","))<>"end" THEN
			R$=" transform='rotate("+WORD$(GrRotate$,2,",")+" "+WORD$(GrRotate$,3,",")+" "+WORD$(GrRotate$,4,",")+")'"
		ENDIF
		FN.RTN R$
	FN.END
	FN.DEF SetAntiAlias$(GrSetAntiAlias$)
		IF GrSetAntiAlias$="0" THEN
			AA$="GRshape-rendering:crispEdges'/>"
		ELSE
			AA$="GRshape-rendering:geometricPrecision'/>"
		ENDIF
		FN.RTN AA$
	FN.END
	ARRAY.LOAD FontFamily$[],"arial","monospace","sans-serif","serif"
	ARRAY.LOAD TxtBold$[],"normal","bold"
	ARRAY.LOAD TxtSkew$[],"normal","italic"
	ARRAY.LOAD TxtLine$[],"","underline","line-through","underline,line-through"
	ARRAY.LOAD TxtAlign$[],"start","middle","end"
	! Set Print Size (mm) to A4
	X_PrintSize=210
	Y_PrintSize=297
	Cr$=CHR$(13)
	! Set defaults - ALL ARGUMENTS ARE NOT OPTIONAL
	GrColor$="255,0,0,0,1"
	GrSetAntiAlias$="1"
	GrSetStroke$="0.3"
	GrTextAlign$="1"
	GrTextSize$="8"
	GrTextTypeface$="1,1"
	GrTextBold$="0"
	GrTextSkew$="0"
	GrTextUnderline$="0"
	GrTextStrike$="0"
	GrRotate$="start,0,0,0"
	GrRotate$="end"
RETURN
GrOpen:
	HTML.OPEN
	RGBa$=GetColor$(GrOpen$)
	HTML$="<svg xmlns='http://www.w3.org/2000/svg' width='"+INT$(X_PrintSize)+"mm' height='"+INT$(Y_PrintSize)+"mm' viewBox='0 0 "+INT$(X_PrintSize)+" "+INT$(Y_PrintSize)+"' preserveAspectRatio='xMinyMin meet'>"
	HTML$=HTML$+"<rect width='"+INT$(X_PrintSize)+"' height='"+INT$(Y_PrintSize)+"' style='fill:rgba("+RGBa$+")'/>"+Cr$
RETURN
GrLine:
	RGBa$=GetColor$(GrColor$)
	HTML$=HTML$+"<line x1='"+WORD$(GrLine$,1,",")+"' y1='"+WORD$(GrLine$,2,",")+"' x2='"+WORD$(GrLine$,3,",")+"' y2='"+WORD$(GrLine$,4,",")+"'"+Rotate$(GrRotate$)+" style='stroke:rgba("+RGBa$+");"
	HTML$=HTML$+FillOption$(WORD$(GrColor$,5,","),GrSetStroke$,RGBa$)+SetAntiAlias$(GrSetAntiAlias$)+Cr$
RETURN
GrRect:
	RGBa$=GetColor$(GrColor$)
	l=VAL(WORD$(GrRect$,1,","))
	t=VAL(WORD$(GrRect$,2,","))
	r=VAL(WORD$(GrRect$,3,","))
	b=VAL(WORD$(GrRect$,4,","))
	HTML$=HTML$+"<rect x='"+INT$(l)+"' y='"+INT$(t)+"' width='"+INT$(r-l)+"' height='"+INT$(b-t)+"'"+Rotate$(GrRotate$)+" style='stroke:rgba("+RGBa$+");"
	HTML$=HTML$+FillOption$(WORD$(GrColor$,5,","),GrSetStroke$,RGBa$)+SetAntiAlias$(GrSetAntiAlias$)+Cr$
RETURN
GrOval:
	RGBa$=GetColor$(GrColor$)
	l=VAL(WORD$(GrOval$,1,","))
	t=VAL(WORD$(GrOval$,2,","))
	r=VAL(WORD$(GrOval$,3,","))
	b=VAL(WORD$(GrOval$,4,","))
	HTML$=HTML$+"<ellipse cx='"+INT$(l+(r-l)/2)+"' cy='"+INT$(t+(b-t)/2)+"' rx='"+INT$((r-l)/2)+"' ry='"+INT$((b-t)/2)+"'"+Rotate$(GrRotate$)+" style='stroke:rgba("+RGBa$+");"
	HTML$=HTML$+FillOption$(WORD$(GrColor$,5,","),GrSetStroke$,RGBa$)+SetAntiAlias$(GrSetAntiAlias$)+Cr$
RETURN
GrCircle:
	RGBa$=GetColor$(GrColor$)
	HTML$=HTML$+"<circle cx='"+WORD$(GrCircle$,1,",")+"' cy='"+WORD$(GrCircle$,2,",")+"' r='"+WORD$(GrCircle$,3,",")+"' style='stroke:rgba("+RGBa$+");"
	HTML$=HTML$+FillOption$(WORD$(GrColor$,5,","),GrSetStroke$,RGBa$)+SetAntiAlias$(GrSetAntiAlias$)+Cr$
RETURN
GrPoly:
	LIST.SIZE GrPoly,L
	P$=""
	FOR i=1 TO L-1 STEP 2
		LIST.GET GrPoly,i,x
		LIST.GET GrPoly,i+1,y
		P$=P$+INT$(x)+","+INT$(y)+" "
	NEXT i
	RGBa$=GetColor$(GrColor$)
	HTML$=HTML$+"<polygon points='"+P$+"'"+Rotate$(GrRotate$)+" style='stroke:rgba("+RGBa$+");"
	HTML$=HTML$+FillOption$(WORD$(GrColor$,5,","),GrSetStroke$,RGBa$)+SetAntiAlias$(GrSetAntiAlias$)+Cr$
RETURN
GrTextDraw:
	RGBa$=GetColor$(GrColor$)
	HTML$=HTML$+"<text x='"+WORD$(GrTextDraw$,1,",")+"' y='"+WORD$(GrTextDraw$,2,",")+"'"+Rotate$(GrRotate$)+" style='font-family:"+FontFamily$[VAL(WORD$(GrTextTypeface$,1,","))]+";font-size:"+GrTextSize$+";"
	HTML$=HTML$+"font-weight:"+TxtBold$[VAL(GrTextBold$)+1]+";font-style:"+TxtSkew$[VAL(GrTextSkew$)+1]+";text-decoration:"+TxtLine$[(VAL(GrTextUnderline$)+1)+(VAL(GrTextStrike$)*2)]
	HTML$=HTML$+";text-anchor:"+TxtAlign$[VAL(GrTextAlign$)]+";stroke:rgba("+RGBa$+");"
	HTML$=HTML$+FillOption$(WORD$(GrColor$,5,","),GrSetStroke$,RGBa$)+SetAntiAlias$(GrSetAntiAlias$)
	HTML$=LEFT$(HTML$,-2)+">"+WORD$(GrTextDraw$,3,",")+"</text>"+Cr$
RETURN
GrRender:
	HTML$=HTML$+"</svg>"
	HTML.LOAD.STRING HTML$
RETURN
GrClose:
	DO
		DIALOG.MESSAGE "Calibrated Drawing","Save this drawing ?",Answer,"YES","NO"
	UNTIL Answer>0
	IF Answer=1 THEN
		F$="Drawing"
		INPUT "Enter a name for this drawing",F$,F$,IsCancelled
		IF !IsCancelled THEN
			IF UPPER$(Right$(F$,4))<>".SVG" THEN
				F$=F$+".SVG"
			ENDIF
			TEXT.OPEN W,W,DataPath$+F$
				TEXT.WRITELN W,HTML$
			TEXT.CLOSE W
		ENDIF
	ENDIF
	HTML$=""
	HTML.CLOSE
RETURN
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
