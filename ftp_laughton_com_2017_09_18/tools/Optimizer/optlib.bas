! optimizer library

FN.DEF hx$(x)
let a$="1234567890abcdefghijklmnopqrstuvwxyz"
let z=LEN(a$)

if x>z*z 
 popup "too many functions!"
exit
endif
let d1$=MID$(a$,MOD(x,z)+1,1)
let x=FLOOR(x/z)
 IF x
let d2$=MID$(a$,MOD(x,z)+1,1)
 ENDIF
 FN.RTN d2$+d1$
FN.END

FN.DEF optfn(p$,o$)
 TEXT.OPEN r, h1, p$
 LIST.CREATE s, funcr

 ! scan for function names
 DO 
  TEXT.READLN h1, a$
let  eof=(a$="EOF")
  IF eof THEN D_U.BREAK
let  a$=TRIM$(a$)
  IF starts_with("fn.def ",LOWER$(a$)) 
   fcount++
 let a$=nocasereplace$(a$,"fn.def ","")
 let i=IS_IN("(",a$)
let fname$=LOWER$(MID$(a$,1,i-1))

   IF LEN(fname$)>3
    LIST.SIZE funcr,siz
    IF siz>1
 let j=0
     DO 
      j++
      LIST.GET funcr,j,l$
     UNTIL LEN(fname$)>=LEN(l$) | j>=siz
     LIST.INSERT funcr,j,fname$
    ELSE
     LIST.ADD funcr,fname$
    ENDIF
!    PRINT fname$
   ENDIF 
  ENDIF
 UNTIL eof 
 PRINT "done!"
 TEXT.CLOSE h1
 GRABFILE prog$, p$
let oldl=LEN(prog$)
 ! replace function names 
 LIST.SIZE funcr, size
 FOR i = 1 TO size
  LIST.GET funcr, i, oldf$
 let newf$="_"+HX$(i)
  IF IS_IN("$",oldf$) THEN newf$+="$"
  prog$=nocasereplace$(prog$, oldf$+"(", newf$+"(")

 ! PRINT "replaced "+oldf$
 NEXT 
let newl=LEN(prog$)
 TEXT.OPEN w, h, o$
 TEXT.WRITELN h, prog$
 TEXT.CLOSE h
 PRINT INT$(100*(oldl-newl)/newl)+"% saved!"
FN.END

fn.def stripws(f$)
grabfile b$,f$
split bb$[],b$,"\n"
array.length z,bb$[]
for i=1 to z
if starts_with("!!",bb$[i])
skip=!skip
f_n.continue
endif
if skip then f_n.continue

if starts_with("!",bb$[i])| len(bb$[i])<2
f_n.continue
endif
x$+=bb$[i]+"\n"
next 
writeln("ezeztemp.bas",x$)
writeln(f$,x$)
fn.end


FN.DEF opt(f$)
o$=f$
 o$=REPLACE$(o$,".bas","")
 o$=REPLACE$(o$,".BAS","")
if !is_in("dev",o$)
popup "error.. filename must have dev in it!"
fn.rtn 0
endif
o$=replace$(o$,"dev","")
 o$="../../rfo-basic/source/"+o$+".bas"
 TEXT.OPEN r,FN,f$
FILE.DELETE e,o$
 TEXT.OPEN a, ofile, o$
 q$=CHR$(34)
 Do
  TEXT.READLN FN, a$
  IF a$="EOF" THEN D_U.BREAK
  let a$=TRIM$(a$)
  let hasinc=( starts_with("INCLUDE",A$) | starts_with("include",a$))
 let hasbas= (IS_IN(".BAS",A$) | IS_IN(".bas",a$))
  IF hasinc & hasbas & !starts_with("!",a$)
   let A$=REPLACE$(A$,"INCLUDE","")
   let A$=REPLACE$(A$,"include","")
  let A$=REPLACE$(A$,Q$,"")
   let a$=trim$(a$)
   let a$="/../source/"+A$
   PRINT a$+"...merging"
   GRABFILE i$,A$
  let A$=I$
  ENDIF
!if !STARTS_WITH("!",A$) & !STARTS_WITH("REM ",A$) & !STARTS_WITH("rem ",A$) & len(a$)>1
  TEXT.WRITELN ofile, a$
!endif
 UNTIL A$="EOF"
 TEXT.CLOSE ofile 
st=clock()
 optfn(o$,o$)
popup str$((clock()-st)/1000)+" secs"
cr$=chr$(13)
lf$=chr$(10)
grabfile t$,o$
t$=replace$(t$, cr$+lf$, lf$)
writeln(o$,t$)
stripws(o$)
 FILE.DELETE e,tmp$
 POPUP "done"
 PAUSE 2000
FN.END
