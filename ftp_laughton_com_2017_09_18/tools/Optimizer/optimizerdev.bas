! optimizer
! BASIC! program to automatically merge and optimize INCLUDE files 
! from development .bas file into one production file
! for use with QuickAPK encryption
!
!
! Note: Only merges INCLUDEs of main .bas file, not nested ones.
! Limitations:
!  double exclamation comment sections are not taken out
!  function names in original .bas files should not begin with  _ underscores 
! function names inside quotes are converted too
!       
! be careful not too overwrite your original file or 
! other important .bas files!
!
!  .bas file must have "dev" in it.. i.e   myprogdev.bas
! output is .bas file without dev..  i.e.  myprog.bas
!
! function optimizer/obfuscator
!
include "optlib.bas"
!

FN.DEF nocasereplace$(s$,f$, r$)
let start=1
let zf=len(f$)
let zr=len(r$)
let ff$=lower$(f$)
let ss$=lower$(s$)
 DO 
let i=IS_IN(ff$,ss$,start)

  IF !i THEN D_U.BREAK
news$+=mid$(s$,start,i-start)+r$
let start=i+zf
 UNTIL !i
news$+=mid$(s$,start,len(s$)-start+1)
 FN.RTN news$

FN.END

FN.DEF cask(m$)
 DIALOG.MESSAGE,m$,c,"yes","no"
 IF c=1 THEN FN.RTN 1
 FN.RTN 0
FN.END

FN.DEF writeln(f$, msg$) 
 TEXT.OPEN w, fh, f$
 TEXT.WRITELN fh, msg$
 TEXT.CLOSE fh 
FN.END

FN.DEF readln$(f$) 
 FILE.EXISTS isoldfile, f$ 
 IF isoldfile
  TEXT.OPEN r,fh3,f$
  TEXT.READLN fh3, a$
 ELSE
  LET A$=""
 ENDIF
 FN.RTN a$
FN.END

FN.DEF choosefile$(path$)
!!
This program can be used to
explore the file system on an
Android device. 

Any filename with a (d) after
it is a directory. Tap on that
entry to open that directory.

Tap on the top entry, "..", to
move up one directory level.

Press the BACK key to exit.

This program will list all the
way up to the file system root.

The initial directory shown is
"/sdcard/rfo-basic/data/"

!!

 ! Lists a directory. Gets and
 ! returns the user's selection

dloop:

 ! Get the listing from
 ! the path directory
 ! and sort it

 ARRAY.DELETE d1$[]
 FILE.DIR path$, d1$[]
 ARRAY.LENGTH length, d1$[]

 ! Copy the file list
 ! adding the top ",,"
 ! entry

 ARRAY.DELETE d2$[]
 DIM d2$[length+1]
 d2$[1] = ".."
 FOR i = 1 TO length
  d2$[i + 1] = d1$[i]
 NEXT i

 ! Present the list
 ! and get the user's
 ! choice
 s=0
 LIST.CREATE s,l
 ARRAY.LENGTH z,d2$[]
 FOR i=1 TO z
  LIST.ADD l,d2$[i]
 NEXT
 !asklist$(l,"select file",&s)
 SELECT s, d2$[], ""
 IF s = 0 THEN END

 ! If top entry, "..", not
 ! selected then append
 ! the selected directory
 ! name to the path

 IF s>1
  n = IS_IN("(d)", d2$[s])
  IF n = 0
   FN.RTN path$+d2$[s]
   END
  ENDIF
  dname$ = LEFT$(d2$[s],n-1)
  path$=path$+dname$+"/"
  GOTO dloop
 ENDIF

 ! If s = 1 then must back
 ! one level

 ! if at start path then
 ! back up one level

 IF path$ = ""
  path$ = "../"
  GOTO dloop
 ENDIF

 ! Not at start path
 ! split the path by
 ! the "/" chars

 ARRAY.DELETE p$[]
 SPLIT p$[], path$, "/"
 ARRAY.LENGTH length, p$[]

 ! If the last entry is
 ! ".." then add "../"
 ! to back up one level

 IF p$[length] = ".."
  path$ = path$ + "../"
  GOTO dloop
 ENDIF

 ! Last entry is not ".."
 ! so must delete the
 ! last directory from
 ! the path

 ! If only one entry
 ! then path is back
 ! to the base path

 IF length = 1
  path$ = ""
  GOTO dloop
 ENDIF

 ! Reconstruct path without
 ! the last directory

 path$ = ""
 FOR i = 1 TO length - 1
  path$ = path$ + p$[i] + "/"
 NEXT i

 GOTO dloop
FN.END

FN.DEF ISOLD(f$) 
 FILE.EXISTS isoldfile, f$ 
 FN.RTN ISOLDFILE 
FN.END

FN.DEF pickfile$()
 ok=2
 f$=readln$("lastmerge.txt")
 IF f$<>""
  DIALOG.MESSAGE , "use "+f$,ok,"yes","no"
 ENDIF
 IF ok=2

  F$=ChooseFile$("../../rfo-basic/source/")  
  writeln("lastmerge.txt",f$)
 ENDIF
 IF !IS_IN("dev",f$)
  POPUP "error...filename  must have dev in it"
  EXIT
 ENDIF

 IF !ISOLD(f$) 
  PRINT F$+" not existing.."
  EXIT
 ENDIF
 FN.RTN f$
FN.END


FN.DEF main()
 f$=pickfile$()
 POPUP f$
st=clock()
 opt(f$)
FN.END

main()
EXIT
