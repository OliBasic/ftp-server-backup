! Button Creator with code generator v1.0
! by Antonis (tony_gr)
! Created on 10/08/12 
! basic! v1.67

A tedious work for any coder is to create buttons, position those in the right places, and write the related code.
This tool will help you create the bitmaps of the buttons 
of your app, and will also generate the necessary basic code.
It uses a horizzontal or vertical gradient technique to draw the buttons.

[How it works]
On top-left you see a preview of your work on a 50% scale.
There is a graphical menu on the top.
The color you select within this menu is the text color of your button text.
The first button (text icon) will generate a basic program 
called "program.bas" in your rfo-basic/source folder.
The second (save icon) will generate all the bitmaps of the buttons you created from time to time, when you pressed 
"GENERATE! Ver.Grd |" or "GENERATE! Hor.Grd. _".
The third one on the first row, (X) immediately quits program.

[The text menu]
All other options can be accessed by pressing on some text
or on "+" and "-" to increase or decrease values.
You have to choose a basecolor(red,gre,blue) for your button, and it's value. (255 works well). Then the gradient start & end values.
LeftUpX, LeftUpY are the top-left coordinates of your button.
Press "Text=", to insert the button caption.
When finished with the parameters, press "GENERATE! Ver.Grd |" or "GENERATE! Hor.Grd. _" to create your button. Now you can also move it by changing the StartX, StartY values and pressing "move!".
Generate all your buttons and then press the save icon to save the bitmaps into your rfo-basic/data directory.
They will be named button1.png, button2.png, and so on.
Press the (first) text icon to generate the source for program.bas
Now you can just load & run this generated program & continue your work on it.
If program.bas or buttonX.png already exist, they will be overwritten.
Initially, play with the basecolor & gradient parameters to see the effects.
