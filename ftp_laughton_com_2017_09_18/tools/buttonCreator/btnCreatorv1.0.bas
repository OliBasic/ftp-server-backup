! Button Creator with code generator v1.0
! by Antonis (tony_gr)
! Created on 10/08/12 
! basic! v1.67

LIST.CREATE N,btnArray
LIST.CREATE S,btnArrayText
DIM tobj1[26],tobj2[10],tobj3[10],robj[50] % keyboard pointers

GR.OPEN 255,0,0,0,0,1
PAUSE 1000
GR.SCREEN w,h
Sx=w/800
Sy=h/1232
GR.SCALE sx,sy
GR.COLOR 255,0,0,0,1
GR.RECT prv, 0,0,800,1232
GR.HIDE prv
s$=VERSION$()
IF LEFT$(s$,2)="08" THEN
 dtPath$="../../rfo-basic/data/"
 srcPath$="../../rfo-basic/source/"
ELSE
 dtPath$=""
 srcPath$="../source/"
ENDIF

basecolor1=255
basecolor2=255
basecolor3=255

cleared=0

otherColorsStart=100 % gradient
otherColorsEnd=200 % gradient
btnWth=200 % button width
btnHei=50  % button height
bcolor=1 % red gradient
c1=0 % text color of button
c2=0 % text color
c3=0 % text color
hor=1
txtSize=40
textcolor$="Text Color: Black"
startX=0 % of button
StartY=0 % of button
btnText$="myText"  % button text
GR.BITMAP.LOAD ui,"ui_menu.jpg"
GR.BITMAP.DRAW uiMenu,ui,0,0

refresh:
firsttime=0
firstkeyb=0

GR.SET.STROKE 0
GR.SET.ANTIALIAS 0
GR.TEXT.SIZE 40
GR.TEXT.ALIGN 1
kFlag=0

GR.COLOR 255,155,95,185,0
GR.TEXT.DRAW tx, 170,940 ,"B a s e   C o l o r"
GR.TEXT.DRAW tx, 640,940 ,"Gradient"
GR.TEXT.SIZE 20
GR.TEXT.DRAW tx, 160,1210 ,"Button Creator v1.0 by Antonis"
GR.COLOR 255,255,175,185,0
GR.TEXT.SIZE 40

! text color
displX=430
displY=230
GR.TEXT.DRAW Tcolor,displX,displY,textcolor$

! button text
displX=405
displY=290
GR.TEXT.DRAW Txt,displX,displY,"Text="
GR.GET.TEXTBOUNDS "Text=", left, top, right, bottom
tx1=left+displX
ty1=top+displY
tx2=right+displX
ty2=bottom+displY
! GR.RECT r, tx1, ty1, tx2, ty2
GR.TEXT.DRAW myText,tx2+10,ty2,btnText$

! button text Size
displX=405
displY=350
GR.TEXT.DRAW TxtS,displX,displY,"+ TextSize - "+REPLACE$(STR$(txtSize),".0","")
GR.GET.TEXTBOUNDS "+ TextSize -", left, top, right, bottom
txS1=left+displX
tyS1=top+displY
txS2=right+displX
tyS2=bottom+displY
! GR.RECT r, txS1, tyS1, txS2, tyS2

! refresh button
GR.COLOR 255,255,255,0,0
displX=405
displY=420
GR.TEXT.DRAW tx,displX,displY,"Clear All!"
GR.GET.TEXTBOUNDS "Clear All!", left, top, right, bottom
x1=left+displX
y1=top+displY
x2=right+displX
y2=bottom+displY
! GR.RECT r, x1, y1, x2, y2

! move last
GR.COLOR 255,255,0,0,0
displY=490
GR.TEXT.DRAW tx,displX,displY,"Move last button!"
GR.GET.TEXTBOUNDS "Move last button!", left, top, right, bottom
Gx1=left+displX
Gy1=top+displY
Gx2=right+displX+10
Gy2=bottom+displY
! GR.RECT r, Gx1, Gy1, Gx2, Gy2

! generate button vertically
GR.COLOR 255,0,0,255,0
displY=550
GR.TEXT.DRAW tx,displX,displY,"GENERATE! Ver.Grd. |"
GR.GET.TEXTBOUNDS "GENERATE! Ver.Grd |", left, top, right, bottom
xx1=left+displX
yy1=top+displY
xx2=right+displX+10
yy2=bottom+displY
! GR.RECT r, xx1, yy1, xx2, yy2

! generate button horizontally
GR.COLOR 255,0,0,255,0
displY=610
GR.TEXT.DRAW tx,displX,displY,"GENERATE! Hor.Grd. _"
GR.GET.TEXTBOUNDS "GENERATE! Hor.Grd. _", left, top, right, bottom
xxx1=left+displX
yyy1=top+displY
xxx2=right+displX+10
yyy2=bottom+displY
! GR.RECT r, xxx1, yyy1, xxx2, yyy2

! size vertically : Height
GR.COLOR 255,0,255,255,0
displX=0
displY=670
GR.TEXT.DRAW tx,displX,displY,"+ Height -" % Vert_Size
GR.GET.TEXTBOUNDS "+ Height -", left, top, right, bottom
xv1=left+displX
yv1=top+displY
xv2=right+displX+10
yv2=bottom+displY
! GR.RECT r, xv1, yv1, xv2, yv2
GR.TEXT.DRAW btnH, 200, displY,REPLACE$(STR$(btnHei),".0","")

! size horizzontally : Width
displX=0
displY=790
GR.TEXT.DRAW tx,displX,displY,"+ Width -" % Hor_Size
GR.GET.TEXTBOUNDS "+ Width -", left, top, right, bottom
xh1=left+displX
yh1=top+displY
xh2=right+displX+10
yh2=bottom+displY
! GR.RECT r, xh1, yh1, xh2, yh2
GR.TEXT.DRAW btnW, 200, displY,REPLACE$(STR$(btnWth),".0","")

! X+/-
GR.COLOR 255,0,255,0,0
displX=405
displY=670
GR.TEXT.DRAW tx,displX,displY,"+ LeftUpX -"
GR.GET.TEXTBOUNDS "+ LeftUpX -", left, top, right, bottom
Lx1=left+displX
Ly1=top+displY
Lx2=right+displX+10
Ly2=bottom+displY
! GR.RECT r, Lx1, Ly1, Lx2, Ly2
GR.TEXT.DRAW StX, 630, displY,REPLACE$(STR$(StartX),".0","")

! Y+/-
displX=405
displY=790
GR.TEXT.DRAW tx,displX,displY,"+ LeftUpY -"
GR.GET.TEXTBOUNDS "+ LeftUpY -", left, top, right, bottom
LLx1=left+displX
LLy1=top+displY
LLx2=right+displX+10
LLy2=bottom+displY
! GR.RECT r, LLx1, LLy1, LLx2, LLy2
GR.TEXT.DRAW StY, 630, displY,REPLACE$(STR$(StartY),".0","")


! base color red
GR.COLOR 255,255,0,0,1
GR.RECT bcol1,100,1000,200,1100
GR.TEXT.DRAW plus1, 140, 980,"+"
GR.TEXT.DRAW minus1, 140, 1170,"-"
GR.TEXT.DRAW basecolorPTR1, 5, 1050,REPLACE$(STR$(basecolor1),".0","")

! base color green
GR.COLOR 255,0,255,0,1
GR.RECT bcol2,300,1000,400,1100
GR.TEXT.DRAW plus2, 340, 980,"+"
GR.TEXT.DRAW minus2, 340, 1170,"-"
GR.TEXT.DRAW basecolorPTR2, 215, 1050,REPLACE$(STR$(basecolor2),".0","")

! base color blue
GR.COLOR 255,0,0,255,1
GR.RECT bcol3,500,1000,600,1100
GR.TEXT.DRAW plus3, 540, 980,"+"
GR.TEXT.DRAW minus3, 540, 1170,"-"
GR.TEXT.DRAW basecolorPTR3, 420, 1050 ,REPLACE$(STR$(basecolor3),".0","")

! base color selection rectangle
GR.COLOR 255,255,255,255,0
IF bcolor=1 THEN GR.RECT bcolrect, 90,990,210,1110
IF bcolor=2 THEN GR.RECT bcolrect, 290,990,410,1110
IF bcolor=3 THEN GR.RECT bcolrect, 490,990,610,1110

! screen preview rect
GR.RECT screenRct,-1,-1,401,617

! -start+, colors
GR.COLOR 255,255,0,255,0
displX=640
displY=990
GR.TEXT.DRAW st1,displX,displY,"+ Start -"
GR.GET.TEXTBOUNDS "+ Start -", left, top, right, bottom
Sx1=left+displX
Sy1=top+displY
Sx2=right+displX+10
Sy2=bottom+displY
! GR.RECT r, Sx1,Sy1,Sx2,Sy2
GR.TEXT.DRAW st3,680,1030,REPLACE$(STR$(otherColorsStart),".0","")
! GR.RECT r, Sx1, Sy1, Sx1+40, Sy1+40
! GR.RECT r, Sx2-40, Sy1, Sx2, Sy2

! -end+
displX=640
displY=1150
GR.TEXT.DRAW st4,displX,displY,"+ End -"
GR.GET.TEXTBOUNDS "+ End -", left, top, right, bottom
Ssx1=left+displX
Ssy1=top+displY
Ssx2=right+displX+10
Ssy2=bottom+displY
! GR.RECT r, Ssx1,Ssy1,Ssx2,Ssy2
GR.TEXT.DRAW st6,680,1190,REPLACE$(STR$(otherColorsEnd),".0","")

GR.RENDER

! wait for a touch
DO
 DO
  GR.TOUCH touched,x,y
 UNTIL touched

 ! button text input
 IF x>=tx1*sx & y>=ty1*sy & x<=tx2*sx & y<=ty2*sy THEN
  GOSUB txtInput
 ENDIF

 ! text size +
 IF  x>=txS1*sx & y>=tyS1*sy & x<=(txS1+40)*sx & y<=(tyS1+40)*sy  THEN
  txtSize=txtSize+1
  GR.MODIFY txtS ,"text","+ TextSize - "+REPLACE$(STR$(txtSize),".0","")
  GR.RENDER
 ENDIF

 ! text size -
 IF  x>=(txS2-40)*sx & y>=tyS1*sy & x<=txS2*sx & y<=tyS2*sy  & txtSize>5  THEN
  txtSize=txtSize-1
  GR.MODIFY txtS ,"text","+ TextSize - "+REPLACE$(STR$(txtSize),".0","")
  GR.RENDER
 ENDIF

 ! clear
 IF x>=x1*sx & y>=y1*sy & x<=x2*sx & y<=y2*sy THEN
  GR.CLS
  GR.BITMAP.DRAW uiMenu,ui,0,0
  LIST.CLEAR btnArray
  LIST.CLEAR btnArrayText
  POPUP "Cleared!",0,0,0
  cleared=1
  GOTO refresh
 ENDIF

 ! move btn
 IF x>=Gx1*sx & y>=Gy1*sy & x<=Gx2*sx & y<=Gy2*sy THEN
  GR.MODIFY nb,"x",StartX/2
  GR.MODIFY nb,"y",StartY/2
  GR.RENDER
  LIST.SIZE btnArray, LbtnArray
  LIST.SIZE btnArrayText, l
  LIST.REPLACE btnArray,(l-1)*15+6,startX
  LIST.REPLACE btnArray,(l-1)*15+7,startY
  LIST.GET btnArray,(l-1)*15+6,elementstartX
  LIST.GET btnArray,(l-1)*15+7,elementstartY
  POPUP "moved!",0,0,0
 ENDIF

 ! generate -
 IF x>=xx1*sx & y>=yy1*sy & x<=xx2*sx & y<=yy2*sy THEN
  hor=1
  GOSUB updateButtonHor
 ENDIF
 ! generate |
 IF x>=xxx1*sx & y>=yyy1*sy & x<=xxx2*sx & y<=yyy2*sy THEN
  hor=0
  GOSUB updateButtonVert
 ENDIF

 ! basecolor red
 IF x>=100*sx & y>=1000*sy & x<=200*sx & y<=1100*sy THEN
  GR.MODIFY bcolrect,"left",90
  GR.MODIFY bcolrect,"right",210
  bcolor=1
  GR.RENDER
 ENDIF
 ! basecolor green
 IF x>=300*sx & y>=1000*sy & x<=400*sx & y<=1100*sy THEN
  GR.MODIFY bcolrect,"left",290
  GR.MODIFY bcolrect,"right",410
  bcolor=2
  GR.RENDER
 ENDIF
 ! basecolor blue
 IF x>=500*sx & y>=1000*sy & x<=600*sx & y<=1100*sy THEN
  GR.MODIFY bcolrect,"left",490
  GR.MODIFY bcolrect,"right",610
  bcolor=3
  GR.RENDER
 ENDIF

 ! vert +,hei
 IF x>=xv1*sx & y>=yv1*sy & x<=(xv1+40)*sx & y<=(yv1+40)*sy THEN
  btnhei=btnhei+1
  GR.MODIFY btnH ,"text",REPLACE$(STR$(btnhei),".0","")
  GR.RENDER
 ENDIF
 ! vert - hei
 IF x>=(xv2-40)*sx & y>=yv1*sy & x<=xv2*sx & y<=yv2*sy & btnhei>5 THEN
  btnhei=btnhei-1
  GR.MODIFY btnH ,"text",REPLACE$(STR$(btnhei),".0","")
  GR.RENDER
 ENDIF

 ! hor + wth
 IF x>=xh1*sx & y>=yh1*sy & x<=(xh1+40)*sx & y<=(yh1+40)*sy THEN
  btnwth=btnwth+1
  GR.MODIFY btnW,"text",REPLACE$(STR$(btnwth),".0","")
  GR.RENDER
 ENDIF
 ! hor - wth
 IF x>=(xh2-40)*sx & y>=yh1*sy & x<=xh2*sx & y<=yh2*sy & btnwth>5 THEN
  btnwth=btnwth-1
  GR.MODIFY btnW,"text",REPLACE$(STR$(btnwth),".0","")
  GR.RENDER
 ENDIF

 ! X +
 IF x>=Lx1*sx & y>=Ly1*sy & x<=(Lx1+40)*sx & y<=(Ly1+40)*sy & startX<w/2 THEN
  startX=startX+1
  GR.MODIFY StX,"text",REPLACE$(STR$(startX),".0","")
  GR.RENDER
 ENDIF
 ! X -
 IF x>=(Lx2-40)*sx & y>=Ly1*sy & x<=Lx2*sx & y<=Ly2*sy & startX>0 THEN
  startX=startX-1
  GR.MODIFY StX,"text",REPLACE$(STR$(startX),".0","")
  GR.RENDER
 ENDIF

 ! Y +
 IF x>=LLx1*sx & y>=LLy1*sy & x<=(LLx1+40)*sx & y<=(LLy1+40)*sy & startY<w/2 THEN
  startY=startY+1
  GR.MODIFY StY,"text",REPLACE$(STR$(startY),".0","")
  GR.RENDER
 ENDIF
 ! Y -
 IF x>=(LLx2-40)*sx & y>=LLy1*sy & x<=LLx2*sx & y<=LLy2*sy & startY>0 THEN
  startY=startY-1
  GR.MODIFY StY,"text",REPLACE$(STR$(startY),".0","")
  GR.RENDER
 ENDIF

 ! otherColorsStart increase Start+
 IF x>=Sx1*sx & y>=Sy1*sy & x<=(40+Sx1)*sx & y<=(40+Sy1)*sy & otherColorsStart<255 THEN
  otherColorsStart=otherColorsStart+1
  GR.MODIFY st3,"text",REPLACE$(STR$(otherColorsStart),".0","")
  GR.RENDER
 ENDIF
 ! otherColorsStart decrease Start-
 IF x>=(Sx2-40)*sx & y>=Sy1*sy & x<=Sx2*sx & y<=Sy2*sy &  otherColorsStart>0 THEN
  otherColorsStart=otherColorsStart-1
  GR.MODIFY st3,"text",REPLACE$(STR$(otherColorsStart),".0","")
  GR.RENDER
 ENDIF
 ! otherColorsEnd increase End+
 IF x>=Ssx1*sx & y>=Ssy1*sy & x<=(40+Ssx1)*sx & y<=(40+Ssy1)*sy & otherColorsEnd<255 THEN
  otherColorsEnd=otherColorsEnd+1
  GR.MODIFY st6,"text",REPLACE$(STR$(otherColorsEnd),".0","")
  GR.RENDER
 ENDIF
 ! otherColorsEnd decrease End-
 IF x>=(Ssx2-40)*sx & y>=Ssy1*sy & x<=Ssx2*sx & y<=Ssy2*sy &  otherColorsEnd>0 THEN
  otherColorsEnd=otherColorsEnd-1
  GR.MODIFY st6,"text",REPLACE$(STR$(otherColorsEnd),".0","")
  GR.RENDER
 ENDIF

 ! red + -
 IF (x>=140*sx & y>=930*sy & x<=190*sx & y<=980*sy) & bcolor=1 & basecolor1<255 THEN
  basecolor1=basecolor1+1
  GR.MODIFY basecolorPTR1,"text",REPLACE$(STR$(basecolor1),".0","")
  GR.RENDER
 ENDIF
 IF (x>=140*sx & y>=1120*sy & x<=190*sx & y<=1170*sy) & bcolor=1 & basecolor1>0 THEN
  basecolor1=basecolor1-1
  GR.MODIFY basecolorPTR1,"text",REPLACE$(STR$(basecolor1),".0","")
  GR.RENDER
 ENDIF
 ! green + -
 IF (x>=340*sx & y>=930*sy & x<=390*sx & y<=980*sy) & bcolor=2 & basecolor2<255 THEN
  basecolor2=basecolor2+1
  GR.MODIFY basecolorPTR2,"text",REPLACE$(STR$(basecolor2),".0","")
  GR.RENDER
 ENDIF
 IF (x>=340*sx & y>=1120*sy & x<=390*sx & y<=1170*sy) & bcolor=2 & basecolor2>0 THEN
  basecolor2=basecolor2-1
  GR.MODIFY basecolorPTR2,"text",REPLACE$(STR$(basecolor2),".0","")
  GR.RENDER
 ENDIF
 ! blue + -
 IF (x>=540*sx & y>=930*sy & x<=580*sx & y<=980*sy) & bcolor=3 & basecolor3<255 THEN
  basecolor3=basecolor3+1
  GR.MODIFY basecolorPTR3,"text",REPLACE$(STR$(basecolor3),".0","")
  GR.RENDER
 ENDIF
 IF (x>=540*sx & y>=1120*sy & x<=580*sx & y<=1170*sy) & bcolor=3 & basecolor3>0 THEN
  basecolor3=basecolor3-1
  GR.MODIFY basecolorPTR3,"text",REPLACE$(STR$(basecolor3),".0","")
  GR.RENDER
 ENDIF

 ! generate and save code
 IF x>466*sX & y>0*sY & x<530*sX & y<57*sY THEN
  PAUSE 500
  GOSUB SrcGenerator
  POPUP "Code generated!",0,0,0
 ENDIF

 IF x>466*sX & y>63*sY & x<530*sX & y<120*sY THEN
  PAUSE 500
  GOSUB saveButtonALL
  POPUP "Bitmap(s) saved!",0,0,0
 ENDIF

 IF x>466*sX & y>123*sY & x<530*sX & y<180*sY THEN
  ! exit
  POPUP "Quitting..",0,0,0
  TONE 800,800
  EXIT
 ENDIF

 IF x>531*sX & y>0*sY & x<590*sX & y<57*sY THEN
  ! Selected=black
  textcolor$= "Text Color: black"
  c1=0
  c2=0
  c3=0
  GR.MODIFY Tcolor,"text",textcolor$
  GR.RENDER
 ENDIF

 IF x>531*sX & y>63*sY & x<590*sX & y<120*sY THEN
  ! Selected=white
  textcolor$= "Text Color: white"
  c1=255
  c2=255
  c3=255
  GR.MODIFY Tcolor,"text",textcolor$
  GR.RENDER
 ENDIF

 IF x>531*sX & y>123*sY & x<590*sX & y<180*sY THEN
  ! Selected=pink
  textcolor$= "Text Color: pink"
  c1=255
  c2=175
  c3=185
  GR.MODIFY Tcolor,"text",textcolor$
  GR.RENDER
 ENDIF

 IF x>595*sX & y>0*sY & x<654*sX & y<57*sY THEN
  ! Selected=red
  textcolor$= "Text Color: red"
  c1=255
  c2=0
  c3=0
  GR.MODIFY Tcolor,"text",textcolor$
  GR.RENDER
 ENDIF

 IF x>595*sX & y>63*sY & x<654*sX & y<120*sY THEN
  ! Selected=blue
  textcolor$= "Text Color: blue"
  c1=0
  c2=0
  c3=255
  GR.MODIFY Tcolor,"text",textcolor$
  GR.RENDER
 ENDIF

 IF x>595*sX & y>123*sY & x<654*sX & y<180*sY THEN
  ! Selected=magenta
  textcolor$= "Text Color: magenta"
  c1=155
  c2=95
  c3=185
  GR.MODIFY Tcolor,"text",textcolor$
  GR.RENDER
 ENDIF

 IF x>660*sX & y>0*sY & x<718*sX & y<57*sY THEN
  ! Selected=yellow
  textcolor$= "Text Color: yellow"
  c1=255
  c2=255
  c3=0
  GR.MODIFY Tcolor,"text",textcolor$
  GR.RENDER
 ENDIF

 IF x>660*sX & y>63*sY & x<718*sX & y<120*sY THEN
  ! Selected=green
  textcolor$= "Text Color: green"
  c1=0
  c2=255
  c3=0
  GR.MODIFY Tcolor,"text",textcolor$
  GR.RENDER
 ENDIF

 IF x>660*sX & y>123*sY & x<718*sX & y<180*sY THEN
  ! Selected=cyan
  textcolor$= "Text Color: cyan"
  c1=0
  c2=255
  c3=255
  GR.MODIFY Tcolor,"text",textcolor$
  GR.RENDER
 ENDIF

 IF x>725*sX & y>0*sY & x<782*sX & y<57*sY THEN
  ! Selected=gray
  textcolor$= "Text Color: gray"
  c1=128
  c2=128
  c3=128
  GR.MODIFY Tcolor,"text",textcolor$
  GR.RENDER
 ENDIF

 IF x>725*sX & y>63*sY & x<782*sX & y<120*sY THEN
  ! Selected=brown
  textcolor$= "Text Color: brown"
  c1=150
  c2=75
  c3=0
  GR.MODIFY Tcolor,"text",textcolor$
  GR.RENDER
 ENDIF

 IF x>725*sX & y>123*sY & x<782*sX & y<180*sY THEN
  ! Selected=orange
  textcolor$= "Text Color: orange"
  c1=255
  c2=140
  c3=0
  GR.MODIFY Tcolor,"text",textcolor$
  GR.RENDER
 ENDIF

UNTIL 0

! save all
saveButtonALL:
GR.BITMAP.CREATE myBitmap, 800, 1232
GR.TEXT.SIZE txtSize
LIST.SIZE btnArray, LbtnArray
LIST.SIZE  btnArrayText, LbtnArrayText
FOR i=1 TO LbtnArrayText
 GR.BITMAP.DRAWINTO.START mybitmap
 LIST.GET btnArray,(i-1)*15+1,elementbtnHei
 LIST.GET btnArray,(i-1)*15+2,elementbtnWth
 LIST.GET btnArray,(i-1)*15+3,elementbcolor
 LIST.GET btnArray,(i-1)*15+4,elementstartcolor
 LIST.GET btnArray,(i-1)*15+5,elementendcolor
 LIST.GET btnArray,(i-1)*15+6,elementstartX
 LIST.GET btnArray,(i-1)*15+7,elementstartY
 LIST.GET btnArray,(i-1)*15+8,elementhor
 LIST.GET btnArray,(i-1)*15+9,elementc1
 LIST.GET btnArray,(i-1)*15+10,elementc2
 LIST.GET btnArray,(i-1)*15+11,elementc3
 LIST.GET btnArray,(i-1)*15+12,elementbasecolor
 LIST.GET btnArray,(i-1)*15+13,elementotherColorsStart
 LIST.GET btnArray,(i-1)*15+14,elementotherColorsEnd
 LIST.GET btnArray,(i-1)*15+15, elementtxtSize
 GR.TEXT.SIZE elementtxtSize
 diff1=1
 IF elementhor=1 THEN ll=elementbtnHei ELSE ll=elementbtnWth
 diff=(elementotherColorsEnd- elementotherColorsStart)/ll
 FOR ii=1 TO ll
  diff1=ii*diff
  IF elementbcolor=1 THEN
   GR.COLOR 255,elementbasecolor,elementotherColorsEnd-diff1,elementotherColorsEnd-diff1,1
  ELSEIF elementbcolor=2 then
   GR.COLOR 255,elementotherColorsEnd-diff1,elementbasecolor,elementotherColorsEnd-diff1,1
  ELSE
   GR.COLOR 255,elementotherColorsEnd-diff1,elementotherColorsEnd-diff1,elementbasecolor,1
  ENDIF
  IF elementhor=1 THEN GR.LINE l, elementstartX+0 , elementstartY+ii , elementstartX+elementbtnWth , ii+elementstartY
  IF elementhor=0 THEN GR.LINE l,elementstartX+ii, elementstartY+0, elementstartX+ii, elementstartY+elementbtnHei
 NEXT ii
 LIST.GET btnArrayText,i,elementbtnText$
 GR.GET.TEXTBOUNDS elementbtnText$, left, top, right, bottom
 GR.TEXT.ALIGN 2
 GR.COLOR 255,elementC1,elementC2,elementC3,1
 GR.TEXT.DRAW text,elementStartx+elementbtnwth/2,elementStartY+elementbtnHei-(elementbtnHei+top)/2,elementbtnText$
 GR.TEXT.ALIGN 1
 GR.BITMAP.DRAWINTO.END
 GR.BITMAP.CROP button, myBitmap, elementStartX, elementStartY , elementbtnWth, elementbtnHei
 GR.BITMAP.SAVE button, dtPath$+"button"+REPLACE$(STR$(i),".0","")+".png"
 GR.BITMAP.DELETE button
NEXT i
POPUP "Saved!",0,0,0
TONE 700,700
GR.BITMAP.DELETE myBitmap
GR.RENDER
RETURN

! begin drawing button Horizz
updateButtonHor:
GR.BITMAP.CREATE myBitmap,800, 1232
GR.BITMAP.DRAWINTO.START mybitmap
GR.TEXT.SIZE txtSize
diff=(otherColorsEnd- otherColorsStart)/btnHei
! begin drawing button
diff1=1
FOR i=1 TO btnHei
 diff1=i*diff
 IF bcolor=1 THEN
  basecolor=basecolor1
  GR.COLOR 255,basecolor,otherColorsEnd-diff1,otherColorsEnd-diff1,1
 ELSEIF bcolor=2 then
  basecolor=basecolor2
  GR.COLOR 255,otherColorsEnd-diff1,basecolor,otherColorsEnd-diff1,1
 ELSE
  basecolor=basecolor3
  GR.COLOR 255,otherColorsEnd-diff1,otherColorsEnd-diff1,basecolor,1
 ENDIF
 GR.LINE l, startX+0 , startY+i , startX+btnWth , i+startY
NEXT
GR.GET.TEXTBOUNDS btnText$, left, top, right, bottom
GR.TEXT.ALIGN 2
GR.COLOR 255,C1,C2,C3,1
GR.TEXT.DRAW text,Startx+btnwth/2,StartY+btnHei-(btnHei+top)/2,btnText$
GR.TEXT.ALIGN 1
GR.BITMAP.DRAWINTO.END
GR.BITMAP.CROP source, myBitmap, StartX, StartY , btnWth, btnHei
GR.BITMAP.SCALE button, source, btnWth/2, btnHei/2
GR.BITMAP.DRAW nb,button, StartX/2, StartY/2
GR.BITMAP.DELETE mybitmap
GR.BITMAP.DELETE source
GR.RENDER
GOSUB addButton
RETURN

! begin drawing button Vert
updateButtonVert:
GR.BITMAP.CREATE myBitmap,800, 1232
GR.BITMAP.DRAWINTO.START mybitmap
GR.TEXT.SIZE txtSize
diff=(otherColorsEnd- otherColorsStart)/btnWth
diff1=1
FOR i=1 TO btnwth
 diff1=i*diff
 IF bcolor=1 THEN
  basecolor=basecolor1
  GR.COLOR 255,basecolor,otherColorsEnd-diff1,otherColorsEnd-diff1,1
 ELSEIF bcolor=2 then
  basecolor=basecolor2
  GR.COLOR 255,otherColorsEnd-diff1,basecolor,otherColorsEnd-diff1,1
 ELSE
  basecolor=basecolor3
  GR.COLOR 255,otherColorsEnd-diff1,otherColorsEnd-diff1,basecolor,1
 ENDIF
 GR.LINE l,startX+i, startY+0, startX+i, startY+btnHei
NEXT
GR.GET.TEXTBOUNDS btnText$, left, top, right, bottom
GR.TEXT.ALIGN 2
GR.COLOR 255,C1,C2,C3,1
GR.TEXT.DRAW text,Startx+btnwth/2,StartY+btnHei-(btnHei+top)/2,btnText$
GR.TEXT.ALIGN 1
GR.BITMAP.DRAWINTO.END
GR.BITMAP.CROP source, myBitmap, StartX, StartY , btnWth, btnHei
GR.BITMAP.SCALE button, source, btnWth/2, btnHei/2
GR.BITMAP.DRAW nb,button, StartX/2, StartY/2
GR.BITMAP.DELETE mybitmap
GR.BITMAP.DELETE source
GR.RENDER
GOSUB addButton
RETURN

! text for button
txtInput:
! ****keyboard chars****
GR.TEXT.BOLD 0
GR.TEXT.ALIGN 1
GR.SET.STROKE 0
GR.COLOR 255,0,0,255,1
GR.RENDER
IF firsttime=0 THEN
 str0$="qwertyuiopasdfghjklzxcvbnm"
 str1$="1234567890"
 str2$="@.()+-_!<>"
ENDIF

! ****draw keyboard rectangle****
GR.TEXT.SIZE 40
GR.COLOR 255,0,0,255,1
IF firsttime=0 THEN
 GR.RECT rectObj, 0, 601, 799, 1230
ELSE
 GR.SHOW rectObj
ENDIF
GR.COLOR 255,0,255,0,0
IF firsttime=0 THEN
 GR.TEXT.DRAW txtObj, 10, 1175, "Chars left:"
 counter=30
 k=ENDS_WITH( ".0", STR$(counter))
 counter$=LEFT$(STR$(counter),k-1)
 GR.TEXT.DRAW countobj, 210, 1175,counter$
ELSE
 GR.SHOW txtObj
 GR.SHOW countobj
ENDIF
GR.COLOR 255,255,255,255,0
IF firsttime=0 THEN
 GR.TEXT.DRAW txp, 0, 1225,"Press [A] to toggle caps, [Enter] when ready"
ELSE
 GR.SHOW txp
ENDIF
GR.TEXT.SIZE 60

! ****PRINT characters of keyboard****
! the writed$ str contains my text
! letters "qwertyuiopasdfghjklzxcvbnm"
ydisp=0
xdisp=0
j=0
k=1
l=0
FOR i=1 TO 26
 strg$=MID$(str0$,i,1)
 IF  i=11 | i=20  THEN
  xdisp=35*k
  ydisp=70*k
  j=0
  k=k+1
 ENDIF
 j=j+1
 l=l+1
 IF firsttime=0 THEN
  GR.COLOR 255,255,255,255,1
  GR.TEXT.DRAW tobj1[i],(10+(j-1)*81+xdisp),(810+ydisp),strg$
  GR.COLOR 255,255,255,255,0
  GR.RECT robj[l],(3+(j-1)*81+xdisp),(760+ydisp),(68+(j-1)*81+xdisp),(820+ydisp)
 ELSE
  GR.SHOW tobj1[i]
  GR.SHOW robj[i]
 ENDIF
NEXT i

! ****numbers****
FOR j=1 TO LEN(str1$)
 strg$=MID$(str1$,j,1)
 l=l+1
 IF firsttime=0 THEN
  GR.COLOR 255,255,255,255,1
  GR.TEXT.DRAW tobj2[j],(10+(j-1)*81),740,strg$
  GR.COLOR 255,255,255,255,0
  GR.RECT robj[l],(3+(j-1)*81),690,(68+(j-1)*81), 750
 ELSE
  GR.SHOW tobj2[j]
  GR.SHOW robj[l]
 ENDIF
NEXT j

! ****special chars ",.():;-'!?"****
FOR j=1 TO LEN(str2$)
 strg$=MID$(str2$,j,1)
 l=l+1
 IF firsttime=0 THEN
  GR.COLOR 255,255,255,255,1
  GR.TEXT.DRAW tobj3[j],(10+(j-1)*81),1020,strg$
  GR.COLOR 255,255,255,255,0
  GR.RECT robj[l],(3+(j-1)*81),970,(68+(j-1)*81),1030
 ELSE
  GR.SHOW tobj3[j]
  GR.SHOW robj[l]
 ENDIF
NEXT j

! ****capslock, space, delete, RETURN****
GR.TEXT.BOLD 1
GR.COLOR 255,255,255,255,1
IF firsttime=0 THEN
 GR.TEXT.DRAW t1,80,1100,"A"
 GR.TEXT.DRAW t2,250,1100,"   [________] "
 GR.TEXT.DRAW t3,140,1100,"Del"
 GR.TEXT.DRAW t4,600 ,1100,"ENTER"
 ! GR.text.draw tstop,600 ,1190,"STOP"
 GR.TEXT.SIZE 40
 GR.COLOR 255,255,255,255,1
 GR.TEXT.DRAW tobj,0,700, ""
 GR.TEXT.SIZE 60
ELSE
 GR.SHOW t1
 GR.SHOW t2
 GR.SHOW t3
 GR.SHOW t4
 GR.SHOW tobj
 GR.MODIFY tobj,"text",""
ENDIF

GR.TEXT.BOLD 0
GR.COLOR 255,255,255,255,0

IF firsttime=0 THEN
 GR.RECT rob1,70,1040,125,1120
 GR.RECT rob2,240,1040,580,1120
 GR.RECT rob3,135,1040,230,1120
 GR.RECT rob4,590,1040,790,1120
 ! GR.rect rob4,590,1130,755,1210
ELSE
 GR.SHOW rob1
 GR.SHOW rob2
 GR.SHOW rob3
 GR.SHOW rob4
ENDIF

GR.COLOR 255,255,255,255,1

GR.RENDER
! ---PRINTing characters finished---

! ****wait for user input****

writed$=""
touchme:
DO % check FOR max lenght
 touched = -1
 GR.TOUCH touched, x, y
 PAUSE 50
UNTIL touched>0
IF  LEN(writed$)=30 THEN
 POPUP "maximum length, press ENTER",0,0,2
 touchsend:
 DO
  touched = -1
  GR.TOUCH touched, x, y
 UNTIL touched>0
 IF  x>590*sx & y>1040*sy & x<755*sx &y<1120*sy THEN
  GOTO enterpressed
 ELSE
  GOTO touchsend
 ENDIF
ENDIF


! ****scan keyboard for numbers****
FOR j=1 TO 10
 IF  x>(3+(j-1)*81)*sx & x<((j-1)*81+68)*sx & y>690*sy & y<750*sy THEN
  TONE 1200,400
  writed$=writed$+MID$(str1$,j,1)
  GOSUB printmystring
  GR.RENDER
 ENDIF
NEXT j

! scan keyboard for letters
ydisp=0
xdisp=0
j=0
k=1
FOR i=1 TO 26
 IF  i=11 | i=20  THEN
  xdisp=35*k
  ydisp=70*k
  j=0
  k=k+1
 ENDIF
 j=j+1
 !GR.rect robj[l],(3+(j-1)*81+xdisp),(760+ydisp),(68+(j-1)*81+xdisp),(820+ydisp)
 IF  x>(3+(j-1)*81+xdisp)*sx & y>(760+ydisp)*sy & x<(68+(j-1)*81+xdisp)*sx &y<(820+ydisp)*sy THEN
  writed$=writed$+MID$(str0$,i,1)
  TONE 1200,400
  GOSUB printmystring
  GR.RENDER
 ENDIF
NEXT i

! ****scan keyboard for CAPS****
IF  x>70*sx & y>1040*sy & x<125*sx &y<1120*sy THEN
 ! CAPS ON OFF invert str0$
 IF  LEFT$(str0$,1)="q" THEN str0$=UPPER$(str0$) ELSE str0$=LOWER$(str0$)
 TONE 1200,400
 ydisp=0
 xdisp=0
 j=0
 k=1
 FOR i=1 TO 26
  strg$=MID$(str0$,i,1)
  IF  i=11 | i=20  THEN
   xdisp=35*k
   ydisp=70*k
   j=0
   k=k+1
  ENDIF
  j=j+1
  GR.HIDE tobj1[i]
  GR.COLOR 255,255,255,255,1
  GR.TEXT.DRAW tobj1[i],(10+(j-1)*81+xdisp),(810+ydisp),strg$
  GR.COLOR 255,255,255,255,0
 NEXT i
 GR.COLOR 255,255,255,255,1
 GR.RENDER
ENDIF

! ****scan keyboard for space****
IF  x>240*sx & y>1040*sy & x<580*sx &y<1120*sy THEN
 ! SPACE
 writed$=writed$+" "
 TONE 1200,400
 GOSUB printmystring
 GR.RENDER
ENDIF

! ****scan keyboard for DEL****
IF  x>135*sx & y>1040*sy & x<230*sx &y<1120*sy THEN
 ! DEL
 IF  LEN(writed$)>=1 THEN
  writed$=LEFT$(writed$,LEN(writed$)-1)
  TONE 1200,400
  GOSUB printmystring
  GR.RENDER
 ENDIF
ENDIF

! ****scan keyboard FOR ENTER****
IF  x>590*sx & y>1040*sy & x<755*sx &y<1120*sy THEN
 IF LEN(writed$)>0 THEN
  GOTO enterpressed
 ELSE
  POPUP "empty text",0,0,0
 ENDIF
ENDIF

! ****scan keyboard for special chars****
FOR j=1 TO 10
 IF  x>(3+(j-1)*81)*sx & x<((j-1)*81+68)*sx & y>970*sy & y<1030*sy THEN
  TONE 1200,400
  writed$=writed$+MID$(str2$,j,1)
  GOSUB printmystring
  GR.RENDER
 ENDIF
NEXT i

! *** loop FOR NEXT char ***
GOTO touchme

! ****subroutines****

! ****prints on screen my text****
PRINTmystring:
GR.TEXT.SIZE 40
GR.COLOR 255,25,255,0,1

l=LEN(writed$)
wrtd$=writed$

p1=640

GR.MODIFY tobj,"text", wrtd$
GR.MODIFY tobj,"y", p1
GR.MODIFY tobj,"x", 0

GR.RENDER

counter=30-l
k=ENDS_WITH( ".0", STR$(counter))
counter$=LEFT$(STR$(counter),k-1)
GR.MODIFY countobj,"text",counter$
GR.TEXT.SIZE 60
GR.RENDER
RETURN

enterpressed:
! clear traces...
TONE 1200,400
GR.HIDE keyp
GR.HIDE tobj
GR.HIDE tobjj
GR.HIDE tobjjj
FOR i=1 TO 10
 GR.HIDE tobj2[i]
 GR.HIDE tobj3[i]
NEXT i
FOR i=1 TO 26
 GR.HIDE tobj1[i]
NEXT i
GR.HIDE t1
GR.HIDE t2
GR.HIDE t3
GR.HIDE t4
GR.HIDE rectobj
FOR i=1 TO 46
 GR.HIDE robj[i]
NEXT i
GR.HIDE rob1
GR.HIDE rob2
GR.HIDE rob3
GR.HIDE rob4
GR.HIDE txtobj
GR.HIDE txp
GR.HIDE countobj
GR.TEXT.ALIGN 2
GR.COLOR 255,255,255,0,1
GR.SET.STROKE 4
GR.TEXT.SIZE 25
GR.TEXT.BOLD 0
firsttime=1
y=-1
GR.MODIFY myText,"text",writed$
btntext$=writed$

GR.RENDER
RETURN

! adds btn to list
addButton:
IF bcolor=1 THEN
 Bbasecolor=basecolor1
ELSEIF bcolor=2 then
 Bbasecolor=basecolor2
ELSE
 Bbasecolor=basecolor3
ENDIF
LIST.ADD btnArray,btnHei,btnWth,bcolor,startcolor,endcolor,startX,startY,hor,c1,c2,c3,Bbasecolor,otherColorsStart,otherColorsEnd,txtSize
LIST.ADD btnArrayText,btntext$
LIST.SIZE btnArray, LbtnArray
LIST.SIZE btnArrayText, LbtnArrayText % n. of buttons
POPUP "Button added!",0,0,0
RETURN

! src Generator
SrcGenerator:
TEXT.OPEN w, File, srcPath$+"program.bas"
a1$="! Code generated by Button Creator"+CHR$(10)
a1$=a1$+"GR.OPEN 255,0,0,0,0,1"+CHR$(10)
a1$=a1$+"gr.orientation 1"+CHR$(10)
a1$=a1$+"PAUSE 1000"+CHR$(10)
a1$=a1$+"GR.SCREEN w,h" +CHR$(10)
a1$=a1$+"sX=w/s10#"+CHR$(10)
a1$=a1$+"sY=h/s11#"+CHR$(10)
a1$=a1$+"GR.SCALE Sx,Sy"+CHR$(10)
a1$=a1$+"GR.COLOR 255,255,0,0,1"+CHR$(10)
a1$=a1$+"Gr.text.size 30"+CHR$(10)
a1$=a1$+"Gr.set.stroke 0"+CHR$(10)

a$=REPLACE$(STR$(w),".0","")
a1$=REPLACE$(a1$,"s10#",a$)
a$=REPLACE$(STR$(h),".0","")
a1$=REPLACE$(a1$,"s11#",a$)
! PRINT a1$
TEXT.WRITELN File, a1$

a2$="GR.BITMAP.LOAD " +"s1# ," +CHR$(34)+"s2#"+ ".png"+CHR$(34)+ CHR$(10) + "GR.BITMAP.DRAW s3#, "+ "s1#"+" ,s12#,s13#"
LIST.SIZE btnArray, LbtnArray
LIST.SIZE  btnArrayText, LbtnArrayText
FOR i=1 TO LbtnArrayText
 a$= "buttonbmp"+ REPLACE$(STR$(i),".0","")
 aa2$=REPLACE$(a2$,"s1#",a$)
 a$= "button"+ REPLACE$(STR$(i),".0","")
 aa2$=REPLACE$(aa2$,"s2#",a$)
 a$= "buttonPTR"+ REPLACE$(STR$(i),".0","")
 aa2$=REPLACE$(aa2$,"s3#",a$)
 LIST.GET btnArray,(i-1)*15+6,elementstartX
 a$= REPLACE$(STR$(elementstartX),".0","")
 aa2$= REPLACE$(aa2$,"s12#",a$)
 LIST.GET btnArray,(i-1)*15+7,elementstartY
 a$= REPLACE$(STR$(elementstartY),".0","")
 aa2$= REPLACE$(aa2$,"s13#",a$)
 ! PRINT aa2$
 TEXT.WRITELN File, aa2$
NEXT i

TEXT.WRITELN File, "GR.RENDER"

a3$="DO"+CHR$(10)
aa3$=a3$+" DO"+CHR$(10)
aa3$=aa3$+"  GR.TOUCH touched,x,y"+CHR$(10)
aa3$=aa3$+" UNTIL touched"+CHR$(10)
aa3$=aa3$+" DO"+CHR$(10)
aa3$=aa3$+"  GR.TOUCH touched,x,y"+CHR$(10)
aa3$=aa3$+" UNTIL !touched"+CHR$(10)
! PRINT aa3$
TEXT.WRITELN File, aa3$

FOR i=1 TO LbtnArrayText
 a4$=" ! Button #"+ REPLACE$(STR$(i),".0","") +CHR$(10)+" IF x>=s5#*Sx & y>=s6#*Sy & x<=s7#*Sx & y<=s8#*Sy THEN"+CHR$(10)
 a4$= a4$+" ! Code for your actions goes here"+CHR$(10)
 a4$= a4$+" ENDIF"+CHR$(10)

 LIST.GET btnArray,(i-1)*15+1,elementbtnHei
 LIST.GET btnArray,(i-1)*15+2,elementbtnWth
 LIST.GET btnArray,(i-1)*15+6,elementstartX
 LIST.GET btnArray,(i-1)*15+7,elementstartY
 x2=elementstartX+elementbtnWth
 y2=elementstartY+elementbtnHei
 a$= REPLACE$(STR$(elementstartX),".0","")
 aa4$=REPLACE$(a4$,"s5#",a$)
 a$= REPLACE$(STR$(elementstartY),".0","")
 aa4$=REPLACE$(aa4$,"s6#",a$)
 a$= REPLACE$(STR$(x2),".0","")
 aa4$=REPLACE$(aa4$,"s7#",a$)
 a$= REPLACE$(STR$(y2),".0","")
 aa4$=REPLACE$(aa4$,"s8#",a$)
 ! PRINT aa4$
 TEXT.WRITELN File, aa4$
NEXT i

a5$="UNTIL 0"
! PRINT a5$
TEXT.WRITELN File, a5$
TEXT.CLOSE File
TONE 700,700
RETURN
