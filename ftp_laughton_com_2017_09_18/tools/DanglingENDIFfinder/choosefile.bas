REM Start of BASIC! Program

FN.DEF startswith(f$,i$)
 ! return 0 if doesnt match,len(i$) if it matches
 LET l=LEN(f$)
 IF LEN(i$)<l THEN
  FN.RTN 0
 ENDIF
 IF LEFT$(i$,l)=f$ THEN
  FN.RTN l
 ELSE
  FN.RTN 0
 ENDIF
FN.END


FN.DEF endswith(searchfor$, searchin$)
 ! return 0 if doesnt match, len(searchin$) if it matches
 long = LEN(searchfor$)
 IF LEN(searchin$)<long THEN
  FN.RTN 0
 ENDIF
 IF RIGHT$(searchin$,long) = searchfor$ THEN
  FN.RTN long
 ELSE
  FN.RTN 0
 ENDIF
FN.END



FN.DEF simplifyPath$(rep$)
 IF rep$ = "../../../../../" THEN
  FN.RTN "../../../../"
 ENDIF
 ew = endswith("/../", rep$)
 IF ew = 0 THEN
  FN.RTN rep$
 ENDIF
 IF rep$ = "/../" THEN
  FN.RTN rep$
 ENDIF

 i = LEN(rep$)-4
 WHILE ((MID$(rep$,i,1)<>"/")&(i>1))
  i = i - 1
 REPEAT
 IF i=1 THEN
  i = 0
 ENDIF
 a$ = MID$(rep$,i+1,LEN(rep$)-i-4)
 IF a$=".." THEN
  FN.RTN rep$
 ELSE
  FN.RTN LEFT$(rep$,i)
 ENDIF
FN.END

FN.DEF chooseFileDir$(rep$, dironly)
 ! rep$ is the starting directory
 ! dironly=1 => displays only directories
 !	each directory is displayed twice: once for navigation and once for selection
 ! dironly=0 => displays files + directories
 !	only files can be selected, directories are displayed for navigation only
 ! returns the full path of chosen file/directory or "<cancel>" if user used the back button
 fin = 0
 DO
  FILE.DIR rep$, a$[]
  ARRAY.LENGTH n, a$[]
  DIM b$[2*n+2]
  b$[1] = "Current Directory: " + rep$
  b$[2] = "..(d)"
  j = 2
  FOR i=1 TO n
   ajoute = 1

   IF ajoute THEN
    j = j+1
    b$[j] = a$[i]

   ENDIF
  NEXT i
  UNDIM a$[]

  LIST.CREATE s, clist

  LIST.ADD clist,  b$[1]
  FOR i = 2 TO j

   IF endswith(".csv", b$[i] )| endswith(".txt", b$[i] )| endswith("(d)", b$[i] ) THEN
    LIST.ADD clist, b$[i]
   ENDIF

  NEXT i

  LIST.TOARRAY clist,c$[]

  UNDIM b$[]
  SELECT choix, c$[], rep$
  IF choix>1 THEN
   d$ = c$[choix]
   sw = startswith("  -> select ",d$)
   ew = endswith("(d)", d$)
   IF sw THEN
    e$ = c$[choix-1]
    e$ = LEFT$(e$, LEN(e$)-3)
    e$ = rep$ + e$ + "/"
    e$ = simplifyPath$(e$)
    fin = 1
   ELSEIF ew THEN
    e$ = d$
    e$ = LEFT$(e$, LEN(e$)-3)
    rep$ = simplifyPath$(rep$ + e$ + "/")
   ELSE
    e$ = rep$ + d$
    fin = 1
   ENDIF
  ELSEIF choix = 0 THEN
   e$ = "<cancel>"
   fin = 1
  ENDIF
  UNDIM c$[]
 UNTIL fin
 FN.RTN e$
FN.END

Fn.def ChooseFile$(path$) 
	! From f25_dir.bas
! 	Path$ = ""
	LOOP:
	ARRAY.DELETE d1$[]
	FILE.DIR Path$, d1$[]
	ARRAY.LENGTH length, d1$[]
	ARRAY.DELETE d2$[]
	DIM d2$[length+1]
	d2$[1] = ".."
	FOR i = 1 TO length
	 d2$[i + 1] = d1$[i]
	NEXT i
	SELECT s, d2$[], path$+" Select Datafile..."
	IF s = 0 THEN Fn.rtn ""
	IF s>1 THEN
		n = IS_IN("(d)", d2$[s])
		IF n = 0
			Fn.rtn path$+d2$[s]
		ENDIF
		dname$ = LEFT$(d2$[s],n-1)
		Path$=Path$+dname$+"/"
		GOTO LOOP
	ENDIF
	IF Path$ = "" THEN
		Path$ = "../"
		GOTO LOOP
	ENDIF
	ARRAY.DELETE p$[]
	SPLIT p$[], Path$, "/"
	ARRAY.LENGTH length, p$[]
	IF p$[length] = ".." THEN
		Path$ = Path$ + "../"
		GOTO LOOP
	ENDIF
	IF length = 1 THEN
		Path$ = ""
		GOTO LOOP
	ENDIF
	Path$ = ""
	FOR i = 1 TO length - 1
		Path$ = Path$ + p$[i] + "/"
	NEXT i
	GOTO LOOP
Fn.rtn path$
Fn.end


