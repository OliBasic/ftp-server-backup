!
! DANGLING ENDIF finder
! dangle.bas
!
INCLUDE choosefile.bas

f$=choosefile$("/../")

TEXT.OPEN R,H,f$
DO
 TEXT.READLN H,A$
 a$=TRIM$(LOWER$(a$))
 line++
 IF startswith("!! ",a$)
  DO
   TEXT.READLN H,A$
   a$=TRIM$(LOWER$(a$))
   line++
  UNTIL startswith("!! ",a$) | a$="eof"
  TEXT.READLN h,a$
  a$=TRIM$(LOWER$(a$))
  line++
 ENDIF


 IF a$="eof" THEN D_U.BREAK

 IF startswith("if ",a$) & !is_in(" then ",a$) THEN nest++

 IF (IS_IN("endif",a$)|is_in("end if",a$)) & !startswith("!",a$) & !startswith("rem ",a$) THEN nest--

 IF nest < 0 
  PRINT "dangling ENDIF at line "+INT$(line)
  errs++
  nest=0
 ENDIF
UNTIL A$="eof"
PRINT INT$(errs)+" possible errors"
TEXT.CLOSE h
END
