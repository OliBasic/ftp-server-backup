!ezBAS BASIC! editor
!

Bundle.create globals
Bundle.put globals,"mode","console"

FN.DEF VERSION$()
 FN.RTN "ezBAS! editor 0.11"
FN.END

FN.DEF IsApk()
 FILE.EXISTS editorMode, "../source/ezbas.bas"
 FN.RTN !editorMode
FN.END

FN.DEF dmode(m$)
Bundle.get 1,"mode",oldm$
 IF oldm$=m$ THEN FN.RTN 0
 Bundle.put 1,"mode",m$
 IF oldm$="html" THEN HTML.CLOSE
 IF oldm$="gr" THEN GR.CLOSE
 IF m$="html" THEN HTML.OPEN 0
 IF m$="gr" THEN GR.OPEN 255,0,0,0,0
 IF m$="console" THEN CLS
FN.END

FN.DEF readln$(f$)
FILE.EXISTS e,f$
if e then grabfile a$,f$
 FN.RTN TRIM$(a$)
FN.END

FN.DEF writeln(f$,msg$)
 TEXT.OPEN w,h,f$
 TEXT.WRITELN h,msg$
 TEXT.CLOSE h
FN.END

FN.DEF get_text$(p$,d$)
 INPUT p$,s$,d$,canc
 IF canc THEN FN.RTN ""
 FN.RTN s$
FN.END

FN.DEF promptreplace$(s$,f$,r$)
 ! case insensitive replace with prompts
 LET start=1
 LET zf=LEN(f$)
 LET zr=LEN(r$)
 LET ff$=LOWER$(f$)
 LET lwr$=LOWER$(s$)
 DO 
  LET i=IS_IN(ff$,lwr$,start)
  IF !i 
   IF ct
    POPUP INT$(ct)+" replaced"
   ELSE
    POPUP ff$+" not found!"
   ENDIF
   D_U.BREAK
  ENDIF
  IF askyn("replace? "+MID$(s$,i-50,100))
   news$+=MID$(s$,start,i-start)+r$
   ct++
  ELSE
   news$+=MID$(s$,start,i-start)+ff$
  ENDIF
  LET start=i+zf
 UNTIL !i
 news$+=MID$(s$,start,LEN(s$)-start+1)
 FN.RTN news$
FN.END

FN.DEF nocasereplace$(s$,f$, r$)
 ! case insensitive replace$()
 LET start=1
 LET zf=LEN(f$)
 LET zr=LEN(r$)
 LET ff$=LOWER$(f$)
 LET lwr$=LOWER$(s$)
 DO 
  LET i=IS_IN(ff$,lwr$,start)
  IF !i THEN D_U.BREAK
  news$+=MID$(s$,start,i-start)+r$
  LET start=i+zf
 UNTIL !i
 news$+=MID$(s$,start,LEN(s$)-start+1)
 FN.RTN news$

FN.END

FN.DEF dangle$(f$)
 ! search for endifs without a matching if
 ! doesn't work perfectly but might find something
 TEXT.OPEN R,H,f$
 DO
  TEXT.READLN H,A$
  a$=TRIM$(LOWER$(a$))
  line++
  IF STARTS_WITH("!! ",a$)
   DO
    TEXT.READLN H,A$
    a$=TRIM$(LOWER$(a$))
    line++
   UNTIL STARTS_WITH("!! ",a$) | a$="eof"
   TEXT.READLN h,a$
   a$=TRIM$(LOWER$(a$))
   line++
  ENDIF
  IF a$="eof" THEN D_U.BREAK

  IF STARTS_WITH("if ",a$) & !IS_IN(" then ",a$) THEN nest++

  IF STARTS_WITH("endif",a$) & !STARTS_WITH("!",a$) & !STARTS_WITH("rem ",a$) THEN nest--

  IF nest < 0 
   d$+= "dangling ENDIF at line "+INT$(line)+"\n"
   errs++
   nest=0
  ENDIF
 UNTIL A$="eof"
 d$+=INT$(errs)+" possible errors\n\n"
 TEXT.CLOSE h
 FN.RTN d$
FN.END

FN.DEF orphans$(dat$,fl)
 ! search for unused functions
 dmode("console")
 dat$ = LOWER$(REPLACE$(dat$," ",""))
 DO
  pos  = IS_IN("fn."+"def",dat$,pos)
  IF pos 
   pos2 = IS_IN ( "(",  dat$,  pos)
   funName$   = MID$  ( dat$, pos+6, pos2-pos-6)
   LIST.ADD fl,funName$
   pos = pos2
  ENDIF
 UNTIL   !pos

 LIST.SIZE fl,z

 IF z=0 THEN FN.RTN "no orphans"
 dat$  = LOWER$(REPLACE$(dat$," ",""))
 LIST.CREATE S, unused

 FOR i=1 TO z
  LIST.GET fl,i,n$
  pos=IS_IN(n$+"(", dat$)
  IF pos>0 & MID$(dat$,pos-6,6)="fn.def" 
   pos += 6+LEN(n$)
   pos=IS_IN(n$+"(", dat$, pos)
  ENDIF
  IF !pos THEN LIST.ADD unused,n$
 NEXT
 LIST.SIZE unused,z
 IF !z 
  s$="no orphans found"
  FN.RTN s$
 ENDIF
 s$=INT$(z)+" possible orphan fns\n"

 FOR i=1 TO z
  LIST.GET unused,i,n$
  s$+= n$+"\n"
 NEXT
 FN.RTN s$
FN.END

FN.DEF waitclick$()
 DO
   PAUSE 50
  HTML.GET.DATALINK data$
 UNTIL data$ <> ""

 IF data$="STT" THEN FN.RTN data$

 IF IS_IN("BAK:",data$)=1
   EXIT
 ELSEIF IS_IN("DAT:",data$)=1
   LET data$=MID$(data$,5)
 ELSEIF IS_IN("LNK:file:///",data$)=1&IS_IN("?",data$) %' Submit link
  LET i=IS_IN("?",data$)
   LET data$="SUBMIT&"+MID$(data$,i+1)+"&"
 ENDIF
 FN.RTN data$
FN.END

FN.DEF htmledit$(p$,s$)
 dmode("html")
 s$=REPLACE$(s$,"&","&amp;")
 s$=REPLACE$(s$,"<","&lt;")
 s$=REPLACE$(s$,">","&gt;")
 w$="<html>"
 w$+="<head>"
 w$+="<meta http-equiv=\"content-type\"content=\"text/html;charset=UTF-8\"/>"
 w$+="<title>edit text</title>"
 w$+="</head>"
 w$+="<style> textarea { font-family:\"Times New Roman\", Times, serif; font-size: 16px; }</style>"
 w$+="<script type=\"text/javascript\">"
 w$+="function DL(data) {"
 w$+="Android.dataLink(data);"
 w$+="}"
 w$+="</script>"
 w$+="<body bgcolor=\"black\">"
 w$+="<div align=\"left\">"
 w$+="<h2 style=\"color:#999999\">###prompt</h2>"
 w$+=""
 w$+="<form id='main' method='get' accept-charset='ISO-8859-1' action='FORM'>"
 w$+="<input type='submit' style=\"float:left;\" name='submit' value='Done'/>"
 w$+="<TEXTAREA charset=\"UTF-8\" NAME=\"id\" ROWS=15 COLS=30>"
 w$+="###edit"
 w$+="</TEXTAREA>"
 w$+="</form></div></body></html>"
 w$=REPLACE$(w$,"###prompt",p$)
 w$=REPLACE$(w$,"###edit",TRIM$(s$))
 HTML.LOAD.STRING w$
 HTML.LOAD.URL "javascript:DL(document.getElementById('id'))"
 r$=waitclick$()
 r$=REPLACE$(r$,"SUBMIT&submit=Done&id=","")
 r$=REPLACE$(r$,"&amp;","&")
 r$=REPLACE$(r$,"&lt;","<")
 r$=REPLACE$(r$,"&gt;",">")
 r$=DECODE$("URL","UTF-8",r$)
 FN.RTN LEFT$(r$,LEN(r$)-1)
FN.END

FN.DEF asklist$(l,msg$,c)
 ! drop down menu version
 ! returns user choice from list l with prompt msg$, c=0 if cancel or error
 r$="<br>"
 h$="<!DOCTYPE html><html lang=~en~>"
 h$+="<style>"
 h$+="header{position:fixed;width:100%;height:50px;top:0;z-index:1;margin-left:0px;background:00f;}"
 h$+="#menu-bar {width:100%;margin:0px 0px 0px 0px;padding:1px 1px 1px 2px;height:32px;"
 h$+="line-height:100%;border-radius:0px;background:#00c;border:solid 0px #ffffff;position:relative;z-index:999;} "
 h$+="#menu-bar li {margin:0px 0px 2px 0px;padding:0px 3px 2px 3px;float:left;position:relative;list-style:none;}"
 h$+="#menu-bar a {font-weight:bold;font-family:arial;font-style:normal;font-size:12px;color:#fff;text-decoration:none;display:block;padding:6px 7px 3px 8px;margin:0;margin-bottom:2px;border-radius:0px;text-shadow:2px 2px 3px #000000;} "
 h$+="#menu-bar li ul li a {margin:0;} "
 h$+="#menu-bar .active a, #menu-bar li:hover > a {background:#0000ff;color:#000000;box-shadow:0 1px 1px rgba(0,0,0,.2);text-shadow:0px 0px 0px #000000;} "
 h$+="#menu-bar ul li:hover a, #menu-bar li:hover li a {background:none;border:none; color:#666;-box-shadow:none;} "
 h$+="#menu-bar ul a:hover {} "
 h$+="#menu-bar li:hover > ul { display:block;} "
 h$+="#menu-bar ul {background:#cccccc;display:none;margin:0; padding:0;width:185px;position:absolute;top:27px;left:0;border:solid 1px #333344;border-radius:10px;box-shadow:0px 0px 4px #ffffff;} "
 h$+="#menu-bar ul li {float:none;margin:0;padding:0;} "
 h$+="#menu-bar ul a {padding:10px 0px 9px 9px;color:#000 !important;font-size:20px;font-style:normal;font-family:arial;font-weight:normal;text-shadow:2px 2px 0px #ffffff;} "
 h$+="#menu-bar ul li:first-child > a {border-top-left-radius:10px;border-top-right-radius:10px;} "
 h$+="#menu-bar ul li:last-child > a {border-bottom-left-radius:10px;border-bottom-right-radius:10px;} "
 h$+="#menu-bar:after { content:~.~;display:block;clear:both;visibility:hidden;line-height:0;height:0;} "
 h$+="#menu-bar { display:inline-block;}html[xmlns] #menu-bar {display:block;}* html #menu-bar {height:1%;}"
 h$+="a {color:#fff;} "
 h$+="</style>"
 h$+="<head>"
 h$+="<meta charset=~utf-8~ /> <title>Home</title>"
 h$+="<meta content='width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0' name='viewport' />"
 h$+="<meta name=~viewport~ content=~width=device-width; initial-scale=1.0; maximum-scale=1.0~ />"
 h$+="</head>"
 h$+="<div id=~wrap~>"
 h$+="<header style=~background-color:#fff;border-right:0px~> "
 h$+="<div><ul id=~menu-bar~>"
 h$+="<li><a href=~#~ style=~width:20px;height:20px;~/> &#128315;</a><ul>"
 h$+="<li><a hrf=~#~ onClick=~DL('###CHECK')~>Check</a></li> 
 h$+="<li><a hrf=~#~ onClick=~DL('###DEL')~>Delete</a></li> 
 h$+="<li><a hrf=~#~ onClick=~DL('###TAG')~>Tag</a></li> 
 h$+="<li><a hrf=~#~ onClick=~DL('###REPLACE')~>Replace</a></li> 
 h$+="<li><a hrf=~#~ onClick=~DL('###COPY')~>Copy</a></li> 
 h$+="</ul>"
 IF 1  % !IsApk()
  h$+="<li><a href=~#~ onClick=~DL('###RUN')~>RUN</a></li>"
 ENDIF
 h$+="<li><a href=~#~ onClick=~DL('###SAVE')~>SAVE</a></li>"
 h$+="<li><a hrf=~#~ onClick=~DL('###LOAD')~>LOAD</a></li> 
 h$+="<li><a onClick=~DL('###QUIT')~>[x]</a></li>"
 h$+="</ul></div></header>"
 h$+="<h1 style=~color:#000;font-size:18px;~>"+msg$ +"</h1>"
 h$+="<body style=~background-color:#fff; margin-top:60px;text-align:center;font-size:10;~>"
 LIST.SIZE l,z
 h$+="<div>"
 FOR i=1 TO z
  LIST.GET l,i,s$
  s$=REPLACE$(s$,"'","'")
  col$="#005500"
  high$="#fff"
  IF IS_IN("###",s$)
   col$="#004"
   high$="#ffa"
  ENDIF
  IF IS_IN("$$red",s$)
   col$="#400"
  ENDIF
  IF IS_IN("$$tag",s$)
   high$="#aff"
  ENDIF
  s$=REPLACE$(s$,"$$red","")
  s$=REPLACE$(s$,"$$tag","")
  but$="<button type=~button~  style=~background-color:"+high$+";color:"+col$+";text-align:center;font-size:20px;width:300px;height:70px;moz-border-radius: 15px; -webkit-border-radius: 15px;~ onclick=~DL('"+STR$(i)+"')~;>"+s$+"</button><br>"
  h$+=but$
 NEXT
 h$+="</div><script type=~text/javascript~>"
 h$+="function DL(data) {Android.dataLink(data);}</script></body> </html>"
 h$=REPLACE$(h$,"~","\"")
 dmode("html")
 HTML.LOAD.STRING h$
 DO
  DO
   PAUSE 50
   HTML.GET.DATALINK data$
   PAUSE 100
  UNTIL data$ <> ""
  IF IS_IN("DAT:",data$)=1
   LET data$=MID$(data$,5)
  ELSEIF IS_IN("LNK:file:///",data$)=1&IS_IN("?",data$) %' Submit link
   LET i=IS_IN("?",data$)
   LET data$="SUBMIT&"+MID$(data$,i+1)+"&"
  ENDIF
  LET r$=data$
  IF IS_NUMBER(r$)
   c=VAL(r$)
  ELSE 
   c=0
   FN.RTN r$
  ENDIF
 UNTIL c>0 | r$=""
 !popup r$
 IF r$="" THEN FN.RTN ""
 LIST.GET l,c,s$
 s$=REPLACE$(s$,"$$blue","")
 s$=REPLACE$(s$,"$$red","")
 s$=REPLACE$(s$,"$$tag","")
 FN.RTN s$
FN.END

FN.DEF oldaskyn(p$)
 LIST.CREATE s,m
 LIST.ADD m,"yes","no"
 c=0
 asklist$(m,p$,&c)
 LIST.CLEAR m
 IF c=1 THEN FN.RTN 1
 FN.RTN 0
FN.END

FN.DEF askyn(m$)
 CALL dmode("console")
 DIALOG.MESSAGE,m$,c,"yes","no"
 IF c=1 THEN FN.RTN 1
 FN.RTN 0
FN.END

FN.DEF choosefile$(path$)

dloop:
 ARRAY.DELETE d1$[]
 FILE.DIR path$,d1$[]
 ARRAY.LENGTH length,d1$[]
 ARRAY.DELETE d2$[]
 DIM d2$[length+1]
 d2$[1]=".."
 FOR i=1 TO length
  d2$[i+1]=d1$[i]
 NEXT
 s=0
 LIST.CREATE s,l
 ARRAY.LENGTH z,d2$[]
 FOR i=1 TO z
  LIST.ADD l,d2$[i]
 NEXT
 asklist$(l,VERSION$()+"<br>select file",&s)
 IF s>1
  n=IS_IN("(d)",d2$[s])
  IF n=0
   FN.RTN path$+d2$[s]
   END
  ENDIF
  dname$=LEFT$(d2$[s],n-1)
  path$=path$+dname$+"/"
  GOTO dloop
 ENDIF
 IF path$=""
  path$="../"
  GOTO dloop
 ENDIF
 ARRAY.DELETE p$[]
 SPLIT p$[],path$,"/"
 ARRAY.LENGTH length,p$[]
 IF p$[length]=".."
  path$+="../"
  GOTO dloop
 ENDIF
 IF length=1
  path$=""
  GOTO dloop
 ENDIF
 path$=""
 FOR i=1 TO length-1
  path$+=p$[i]+"/"
 NEXT 
 GOTO dloop
FN.END

FN.DEF edit$(p$,s$)
 dmode("console")
 s$+="\n\n\n\n\n"
 

 s$=htmledit$(p$,s$)
 

 !TEXT.INPUT s$,s$,p$
 s$=TRIM$(s$)
 SPLIT a$[],s$,"\n"
 ARRAY.LENGTH z,a$[]
 FOR i=1 TO z
  IF STARTS_WITH(":",TRIM$(a$[i]))
   a$[i]=REPLACE$(a$[i],":","FN.DEF ")
  ENDIF
  IF STARTS_WITH(";",TRIM$(a$[i]))
   a$[i]=REPLACE$(a$[i],";","FN.END")
  ENDIF
  IF TRIM$(UPPER$(a$[i]))="E"
   a$[i]="ENDIF"
  ENDIF
 NEXT
 JOIN A$[],x$,"\n"
 X$=nocasereplace$(x$,"FN.DEF","FN.DEF")
 FN.RTN TRIM$(x$)
FN.END

FN.DEF ParseProgram(a$,l)
 ! break program in a$ into string list l
 SPLIT r$[],a$,"\n"
 ARRAY.LENGTH z,r$[]
 LET s$=""
 FOR i=1 TO z
  LET rr$=r$[i]+"\n"
  LET tr$=TRIM$(UPPER$(rr$))
  LET fd=STARTS_WITH("FN.DEF",tr$)
  LET fe=STARTS_WITH("FN.END",tr$) 
  LET go2=(!STARTS_WITH("!",tr$) & ENDS_WITH(":",tr$) ) 
  LET rt=STARTS_WITH("RETURN",tr$) 
  IF fd |fe |go2 |rt
   IF fd | go2
    LET section$=TRIM$(s$)
    IF LEN(section$) THEN LIST.ADD l,section$
    LET s$=rr$
   ELSE
    s$+=rr$: LET rr$="":LET section$=TRIM$(s$)
    IF LEN(section$) THEN LIST.ADD l,section$
    LET s$=""
   ENDIF
  ELSE 
   s$+=rr$
  ENDIF
 NEXT
 LET section$=TRIM$(s$)
 IF LEN(section$)>0 THEN LIST.ADD l,section$
FN.END

! MAIN

Asked=0

another:
dmode("console")
f$=readln$("ez.txt")
FILE.EXISTS fe,f$
IF !fe | code$="###LOAD"
 f$=choosefile$("/../../rfo-basic/source/")
 writeln("ez.txt",f$)
ENDIF
let a$=readln$(f$)
!GRABFILE a$,f$

DO 
 LET a$=REPLACE$(a$,"\n\n\n","\n")
UNTIL !IS_IN("\n\n\n",a$)

LIST.CREATE s,l
LIST.CREATE s,flist
LIST.CREATE s,menu
LIST.CREATE s,unused
LIST.CREATE s,tags

refresh:
LIST.CLEAR l
LIST.CLEAR flist
LIST.CLEAR menu
LIST.CLEAR unused
ParseProgram(a$,l)
UNDIM fnc$[]
LIST.TOARRAY l,fnc$[]
ARRAY.LENGTH z,fnc$[]
LIST.CLEAR l
FOR i=1 TO z
 LET m$=WORD$(fnc$[i],1,"\n")
 m$=REPLACE$(M$,"FN.DEF ","")
 LIST.ADD flist,m$
 LIST.SEARCH TAGS,M$,tagged
 IF tagged THEN m$+="$$tag"
 LIST.ADD menu,m$
NEXT
LET c=0
lf$=readln$("lastfn")
LIST.SEARCH flist,lf$,lf
IF !asked & lf > 0 & lf <= z
 asked=1
 IF askyn("jump to section "+lf$+"?")
  LET fnc$[lf]=TRIM$(edit$("edit",fnc$[lf]))
 ENDIF
ELSE
 asked=1
 LET code$=asklist$(menu,VERSION$()+"<br>"+f$,&c)
 !popup code$
 IF c>0 & c<=z & !STARTS_WITH("###",code$) % edit code section
  fe$=TRIM$(edit$("edit",fnc$[c]))
  LET fnc$[c]=fe$
  LIST.GET flist,c,lf$
  writeln("lastfn",lf$)
 ENDIF
ENDIF
IF CODE$="###DEL"
 LET x$= asklist$(flist,"delete which?",&c)
 IF askyn("delete "+x$)
  IF askyn("are you sure?") THEN fnc$[c]=""
 ENDIF
ENDIF
IF CODE$="###COPY"
 clip$=""
 DO
  LET x$=asklist$(flist,"COPY which?",&c)
  IF askyn("COPY "+x$)
   CLIP$+=fnc$[c]+"\n\n"
  ENDIF
 UNTIL !askyn("clip more?")
 CLIPBOARD.PUT clip$
 POPUP "copied to clipboard"
ENDIF
IF code$="###CHECK"
 edit$("unused fns",orphans$(a$,unused))
 tmp$="/../source/ezbastemp.bas"
 writeln(tmp$,a$)
 edit$("dangling endifs",dangle$(tmp$))
ENDIF
IF code$="###TAG"
 dmode("console")
 section$=trim$(get_text$("search for?",""))
 IF section$<>""
  ARRAY.LENGTH z,fnc$[]
  LIST.CLEAR tags
  FOR i=1 TO z
   IF IS_IN(LOWER$(section$),LOWER$(fnc$[i]))
    LIST.GET flist,i,t$
    LIST.ADD tags,t$
   ENDIF
  NEXT
 ENDIF
ENDIF
JOIN fnc$[],a$,"\n\n"
IF code$="###REPLACE"
 dmode("console")
 srch$=get_text$( "search for?","")
 IF srch$<>""
  rep$=get_text$("replace?","")
  a$=promptreplace$(a$,srch$,rep$)
 ENDIF
ENDIF
IF code$="###SAVE" | code$="###LOAD" | code$="###RUN" | code$="###QUIT" | code$="###OPTIMIZE"
 IF askyn("save?")
  dmode("console")
  INPUT "save to file:",f$,f$
  writeln(f$,a$)
  writeln("ez.txt",f$)
  POPUP "saved!"
 ENDIF
ENDIF
IF code$="###LOAD" 
 writeln("lastfn","")
 GOTO another
ENDIF
IF code$="###QUIT" THEN EXIT
IF code$="###RUN"
 dmode("console")
 writeln("/../source/ezbastemp.bas",a$)
 RUN "/../source/ezbastemp.bas"
ENDIF
GOTO refresh
