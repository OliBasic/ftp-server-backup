REM BEGIN_LIBC_CLOADSCREEN

FN.DEF cLoadScreen(max)

 BUNDLE.CREATE inst
 BUNDLE.PUT inst, "max", max
 BUNDLE.PUT inst, "val", 0

 FN.RTN inst
FN.END

FN.DEF cLoadScreen_SetValue(inst, val)
 BUNDLE.PUT inst, "val", val
FN.END

FN.DEF cLoadScreen_Draw(inst)

 BUNDLE.GET inst, "max", max
 BUNDLE.GET inst, "val", val

 GR.SCREEN w, h

 GR.COLOR 255*(val/max), 255, 255, 255, 1
 GR.TEXT.SIZE h/7
 GR.TEXT.ALIGN 2

 GR.TEXT.DRAW capt, w/2, h/2, FORMAT$("###", 100*(val/max))+"%"
 GR.RECT bar, 0, h*0.9, w*(val/max), h

 BUNDLE.PUT inst, "capt", capt
 BUNDLE.PUT inst, "bar", bar
FN.END

FN.DEF cLoadScreen_Update(inst)

 BUNDLE.GET inst, "max", max
 BUNDLE.GET inst, "val", val
 BUNDLE.GET inst, "bar", bar
 BUNDLE.GET inst, "capt", capt

 GR.SCREEN w, h

 GR.MODIFY capt, "text", FORMAT$("###", 100*(val/max))+"%"
 GR.MODIFY bar, "right", w*(val/max)

 FN.RTN 0
FN.END

REM END_LIBC_CLOADSCREEN
