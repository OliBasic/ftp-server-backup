REM BEGIN_LIBC_CLOADSCREEN

FN.DEF cLoadScreen(max)

 BUNDLE.CREATE inst
 BUNDLE.PUT inst, "max", max
 BUNDLE.PUT inst, "val", 0
 BUNDLE.PUT inst, "color", "255/255/255"

 FN.RTN inst
FN.END

FN.DEF cLoadScreen_SetValue(inst, val)
 BUNDLE.PUT inst, "val", val
FN.END

FN.DEF cLoadScreen_Draw(inst)

 BUNDLE.GET inst, "max", max
 BUNDLE.GET inst, "val", val
 BUNDLE.GET inst, "color", color$

 IF color$="255/255/255" THEN
  r=255
  g=255
  b=255
 ELSE
  SPLIT nu$[], color$, "/"
  r=VAL(nu$[1])
  g=VAL(nu$[2])
  b=VAL(nu$[3])
 ENDIF

 GR.SCREEN w, h

 GR.COLOR 255*(val/max), r, g, b, 1
 GR.TEXT.SIZE h/7
 GR.TEXT.ALIGN 2

 GR.TEXT.DRAW capt, w/2, h/2, FORMAT$("###", 100*(val/max))+"%"
 GR.RECT bar, 0, h*0.9, w*(val/max), h

 BUNDLE.PUT inst, "capt", capt
 BUNDLE.PUT inst, "bar", bar
FN.END

FN.DEF cLoadScreen_SetColor(inst, r, g, b)
 BUNDLE.PUT inst, "color", STR$(r)+"/"+STR$(g)+"/"+STR$(b)
FN.END

REM END_LIBC_CLOADSCREEN
