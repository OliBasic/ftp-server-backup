! INCPIC.Bas
! Aat Don @2014
GR.OPEN 255,0,0,0,0,0
PAUSE 1000
GR.SCREEN w,h
ScaleX=720
ScaleY=w/h*ScaleX
sx=h/ScaleX
sy=w/ScaleY
GR.SCALE sx,sy
WAKELOCK 3
READ.FROM 1
GR.CLS
! Start screen
GOSUB Makecartman
ARRAY.LOAD Intro$[],"INCPIC (INClude PICs)","The next screen will show the pics in your","../../rfo-basic/data directory.","After running this App you'll find your result","(a new App with the chosen pic INCLUDED) named","../../rfo-basic/source/YOURPICNAME.bas"
ARRAY.LOAD m$[],"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"
TIME Year$,Month$,Day$,Hour$,Minute$,Second$
GR.TEXT.SIZE 40
GR.TEXT.BOLD 1
GR.COLOR 255,255,255,0,1
GR.TEXT.DRAW tx,100,600,"Hit screen to skip intro"
GR.RENDER
q=TIME()
DO
  GR.TOUCH touched,x,y
UNTIL touched | TIME()>q+3000
IF touched THEN
  GR.MODIFY tx,"text","Please, wait..."
  GR.RENDER
  GOTO Skip
ENDIF
GR.COLOR 255,0,0,255,1
GR.RECT gR,90,550,610,620
GR.RENDER
GR.BITMAP.DRAW Pic01,cartmanPic,100,460
FOR i=100 TO 550
  j=j+0.1
  GR.COLOR 255,255,255,0,1
  GR.MODIFY Pic01,"x",i
  GR.MODIFY Pic01,"y",SIN(j)*50+460
  IF (i-100)/80=FLOOR((i-100)/80) THEN
    IF i<101 THEN
      GR.TEXT.UNDERLINE 1
    ELSE
      GR.TEXT.UNDERLINE 0
      GR.TEXT.SKEW -0.2
    ENDIF
    GR.TEXT.DRAW g,10,i,Intro$[FLOOR((i-100)/80)+1]
  ENDIF
  IF GR_COLLISION(Pic01,gR) THEN
    GR.COLOR 255,0,0,0,1
    GR.RECT g,i+10,550,i+40,565
  ENDIF
  GR.RENDER
NEXT i
GR.TEXT.SKEW 0
GR.COLOR 255,255,255,0,1
FOR i=460 to 60 step -40
  GR.MODIFY Pic01,"y",i
  GR.RENDER
  PAUSE 50
NEXT i
GR.MODIFY Pic01,"x",410
GR.RENDER
Skip:
GOSUB MakeButton1
GOSUB MakeButton2
GR.HIDE tx
GR.TEXT.DRAW g,100,600,"Hit screen to select a picture"
GR.RENDER
GOSUB GetTouch
GR.CLS
GR.SET.ANTIALIAS 0
GR.TEXT.BOLD 1
! Get picture
FILE.EXISTS DirPresent,"../../rfo-basic/data/"
IF DirPresent=0 THEN
	POPUP "Data directory NOT found !",-300,0,1
  PAUSE 4000
  EXIT
ENDIF
FILE.DIR "../../rfo-basic/data/",Namen$[]
Array.length NumFiles,Namen$[]
IF NumFiles<1 THEN
	POPUP "No files found !",-300,0,1 
  PAUSE 4000
  EXIT
ENDIF
NumPics=0
FOR i=1 TO NumFiles
  IF UPPER$(RIGHT$(Namen$[i],4))=".PNG" | UPPER$(RIGHT$(Namen$[i],4))=".JPG" THEN
    NumPics=NumPics+1
  ENDIF
NEXT i
DIM PicName$[NumPics]
j=0
FOR i=1 TO NumFiles
  IF UPPER$(RIGHT$(Namen$[i],4))=".PNG" | UPPER$(RIGHT$(Namen$[i],4))=".JPG" THEN
    j=j+1
    PicName$[j]=Namen$[i]
  ENDIF
NEXT i
SELECT art, PicName$[], "Select a Picture"
FName$=PicName$[art]
Array.DELETE Namen$[],PicName$[]
GR.BITMAP.LOAD PicIn,"../../rfo-basic/data/"+FName$
Ext$=RIGHT$(FName$,4)
FName$=LEFT$(FName$,LEN(FName$)-4)
GR.BITMAP.SIZE PicIn,PWidth,PHeight
GR.BITMAP.DRAW ShowPic,PicIn,20,0
GR.RENDER
GR.TEXT.SIZE 40
GR.BITMAP.CREATE Menu,500,200
GS=0
! Save as greyscale ?
GR.BITMAP.CREATE Menu,500,200
GR.BITMAP.DRAWINTO.START Menu
  GR.COLOR 255,0,0,0,1
  GR.RECT g,0,0,499,199
  GR.COLOR 255,255,255,255,0
  GR.RECT g,2,2,497,197
  GR.BITMAP.DRAW Pic02,Button1Pic,50,130
  GR.BITMAP.DRAW Pic03,Button2Pic,350,130
  GR.COLOR 255,255,255,0,0
  GR.TEXT.DRAW g,30,50,"Save as greyscale ?"
  GR.TEXT.DRAW g,60,175,"YES"
  GR.TEXT.DRAW g,370,175,"NO"
GR.BITMAP.DRAWINTO.END
GR.RENDER
GR.BITMAP.DRAW ShowMenu,Menu,50,501
GR.RENDER
GOSUB GetTap
Colorise=0
rc=0
gc=0
bc=0
IF (x>=100*sy & x<=200*sy) THEN
  ! YES save as greyscale
  GS=1
  ! Colourise greyscale image ?
  Q$="Colourise image ?"
  GOSUB Dialog
  GOSUB GetTap
  IF (x>=100*sy & x<=200*sy) THEN 
    Colorise=1
    ! Show 6 colours
    GR.BITMAP.CREATE Col6,500,200
    GR.BITMAP.DRAWINTO.START Col6
    FOR i=0 TO 5
      GR.COLOR 255,255,255,255,0
      GR.RECT g,2+i*60,2,62+i*60,62
      rc=VAL(MID$(FORMAT$("%%%",VAL(BIN$(I+1))),3,1))*255
      gc=VAL(MID$(FORMAT$("%%%",VAL(BIN$(I+1))),4,1))*255
      bc=VAL(MID$(FORMAT$("%%%",VAL(BIN$(I+1))),5,1))*255
      FOR j=0 TO 255 STEP 10
        GR.COLOR 255,j*(rc=255),j*(gc=255),j*(bc=255),1
        GR.RECT g,4+i*60,4+j/4.8,60+i*60,8+j/4.8
      NEXT j
    NEXT i
    GR.TEXT.DRAW g,10,150,"Tap colour to select"
    GR.BITMAP.DRAWINTO.END
    GR.RENDER
    GR.BITMAP.DRAW ShowCol6,Col6,50,501
    GR.RENDER
    DO
      DO
        GOSUB GetTouch
      UNTIL y>=503*sy & y<=563*sy
    UNTIL (x>=52*sx & x<=472*sx)
    GR.HIDE ShowCol6
    ColChoice= FLOOR((x/sx-52)/60)+1
    rc=VAL(MID$(FORMAT$("%%%",VAL(BIN$(ColChoice))),3,1))*255
    gc=VAL(MID$(FORMAT$("%%%",VAL(BIN$(ColChoice))),4,1))*255
    bc=VAL(MID$(FORMAT$("%%%",VAL(BIN$(ColChoice))),5,1))*255
  ENDIF
ENDIF
TrnsP=0
rt=0
gt=0
bt=0
! Save a transparent colour ?
Q$="Set transparent colour ?"
GOSUB Dialog
GOSUB GetTap
GR.BITMAP.CREATE SetCol,500,200
GR.BITMAP.DRAWINTO.START SetCol
  GR.COLOR 255,0,0,0,1
  GR.RECT g,0,0,499,199
  GR.COLOR 255,255,255,255,0
  GR.RECT g,2,2,497,197
  GR.BITMAP.DRAW Pic02,Button1Pic,80,130
  GR.RECT g,300,70,400,170
  GR.COLOR 255,255,255,0,0
  GR.TEXT.DRAW g,100,50,"Tap colour to set"
  GR.TEXT.DRAW g,100,175,"OK"
GR.BITMAP.DRAWINTO.END
GR.RENDER
IF (x>=100*sy & x<=200*sy) THEN 
  ! YES set transparent colour
  TrnsP=1
  GR.BITMAP.SCALE LargePic,PicIn,500/PHeight*PWidth,500,0
  GR.BITMAP.DRAW ShowLargePic,LargePic,20,0
  GR.BITMAP.DRAW ShowSetCol,SetCol,50,501
  GR.COLOR 255,0,0,0,1
  GR.RECT gColour,355,575,446,666
  GR.RENDER
  DO
    GOSUB GetTouch
    IF x>=20*sx & x<=(500/PHeight*PWidth+20)*sx & y<=500*sy THEN
      GR.GET.BMPIXEL LargePic,x/sx,y/sy,a,rt,gt,bt
      GR.COLOR 255,rt,gt,bt,1
      GR.PAINT.GET PickCol
      GR.MODIFY gColour,"paint",PickCol
      GR.RENDER
    ENDIF
  UNTIL x>=100*sx & x<=250*sx & y>=630*sy & y<=690*sy
  GR.SAVE "INCPICScreenShot"
  GR.HIDE ShowLargePic
  GR.HIDE ShowSetCol
  GR.HIDE gColour
  GR.RENDER
ENDIF
IF GS=1 THEN
  GV=(rt+gt+bt)/3
  rt=GV
  gt=GV
  bt=GV
ENDIF
Hor=1
GR.COLOR 255,255,255,0,1
GR.TEXT.DRAW gs1,50,600,"Optimise......"
GR.TEXT.DRAW gs4,300,640," "
GR.RENDER
GOSUB Optimise
OL=StrLen
Hor=0
GOSUB Optimise
IF OL<=StrLen THEN
  Hor=1
ELSE
  Hor=0
ENDIF
GR.HIDE gs1
GR.HIDE gs2
GR.HIDE gs3
GR.HIDE gs4
GR.RENDER
GR.TEXT.SIZE 18
IF Hor=1 THEN
  PrBar=717/(PHeight-1)
ELSE
  PrBar=717/(PWidth-1)
ENDIF
GR.COLOR 255,255,255,255,0
GR.RECT g,0,0,18,719
GR.COLOR 255,0,0,255,1
GR.RECT PG,1,1,17,1
GR.COLOR 255,255,255,0,0
GR.ROTATE.START 270,17,400
  GR.TEXT.DRAW PB,17,400,FORMAT$("##%",Py/(PHeight-1)*100)+" %"
GR.ROTATE.END
GR.RENDER
GOSUB Encode
TONE 600,300,0
POPUP "Ready, touch screen to exit",-300,0,1
GOSUB GetTouch
WAKELOCK 5
GR.CLOSE
EXIT
Optimise:
  IF Hor=1 THEN
    GR.TEXT.DRAW gs2,50,640,"STEP 1 (of 2)"
  ELSE
    GR.TEXT.DRAW gs3,50,680,"STEP 2 (of 2)"
    GR.MODIFY gs4,"y",680
  ENDIF
  GR.RENDER
  Px=0
  Py=0
  RLECnt=0
  StrLen=0
  DO
    RLECnt=RLECnt+1
    GR.GET.BMPIXEL PicIn,Px,Py,ap1,rp1,gp1,bp1
    IF GS=1 THEN
      GV=FLOOR((rp1+gp1+bp1)/3)
      rp1=GV
      gp1=GV
      bp1=GV
    ENDIF
    NxtRLE=0
    DO
      IF Hor=1 THEN
        Px=Px+1
        IF Px=PWidth THEN
          Px=0
          Py=Py+1
          IF Py=PHeight THEN
            D_U.BREAK
          ELSE
            IF Py/2=FLOOR(Py/2) THEN
              GR.MODIFY gs4,"text","<>"
              GR.RENDER
            ELSE
              GR.MODIFY gs4,"text","><"
              GR.RENDER
            ENDIF
          ENDIF
        ENDIF
      ELSE
        Py=Py+1
        IF Py=PHeight THEN
          Py=0
          Px=Px+1
          IF Px=PWidth THEN
            D_U.BREAK
          ELSE
            IF Px/2=FLOOR(Px/2) THEN
              GR.MODIFY gs4,"text","<>"
              GR.RENDER
            ELSE
              GR.MODIFY gs4,"text","><"
              GR.RENDER
            ENDIF
          ENDIF
        ENDIF
      ENDIF
      GR.GET.BMPIXEL PicIn,Px,Py,ap2,rp2,gp2,bp2
      IF GS=1 THEN
        GV=FLOOR((rp2+gp2+bp2)/3)
        rp2=GV
        gp2=GV
        bp2=GV
      ENDIF
      IF rp1=rp2 & gp1=gp2 & bp1=bp2 THEN
        RLECnt=RLECnt+1
      ELSE
        NxtRLE=1
      ENDIF
    UNTIL NxtRLE=1
    ! Measure string length
    IF GS=0 THEN
      StrLen=StrLen+4
    ELSE
      StrLen=StrLen+2
    ENDIF
    RLECnt=0
    IF Hor=1 THEN
      UNTIL Py=PHeight
    ELSE
      UNTIL Px=PWidth
    ENDIF
RETURN
Encode:
POPUP "Creating data from picture",-300,0,1
TEXT.OPEN w,EnCoded,"../../rfo-basic/source/"+FName$+".bas"
  TEXT.WRITELN EnCoded,"! Created with INCPIC "
  TEXT.WRITELN EnCoded,"! "+m$[VAL(Month$)]+" "+Day$+", "+Year$
  TEXT.WRITELN EnCoded,"GR.OPEN 255,92,0,0,0,0"
  TEXT.WRITELN EnCoded,"PAUSE 1000"
  TEXT.WRITELN EnCoded,"GR.SCREEN w,h"
  TEXT.WRITELN EnCoded,"ScaleX=720"
  TEXT.WRITELN EnCoded,"ScaleY=w/h*ScaleX"
  TEXT.WRITELN EnCoded,"sx=h/ScaleX"
  TEXT.WRITELN EnCoded,"sy=w/ScaleY"
  TEXT.WRITELN EnCoded,"GR.SCALE sx,sy"
  TEXT.WRITELN EnCoded,"WAKELOCK 3"
  TEXT.WRITELN EnCoded,"READ.FROM 1"
  TEXT.WRITELN EnCoded,"GR.CLS"
  TEXT.WRITELN EnCoded,"GR.SET.ANTIALIAS 0"
  TEXT.WRITELN EnCoded,"GR.TEXT.SIZE 18"
  TEXT.WRITELN EnCoded,"GR.TEXT.BOLD 1"
  TEXT.WRITELN EnCoded,"POPUP "+CHR$(34)+"Creating picture from data"+CHR$(34)+",-300,0,0"
  TEXT.WRITELN EnCoded,"GOSUB Make"+FName$
  TEXT.WRITELN EnCoded,"GR.BITMAP.DRAW ShowPic,"+FName$+"Pic,20,0"
  TEXT.WRITELN EnCoded,"GR.RENDER"
  TEXT.WRITELN EnCoded,"FOR i=20 TO 700 STEP 10"
  TEXT.WRITELN EnCoded,"  j=j+10"
  TEXT.WRITELN EnCoded,"  GR.MODIFY ShowPic,"+CHR$(34)+"x"+CHR$(34)+",i"
  TEXT.WRITELN EnCoded,"  GR.MODIFY ShowPic,"+CHR$(34)+"y"+CHR$(34)+",j"
  TEXT.WRITELN EnCoded,"  GR.RENDER"
  TEXT.WRITELN EnCoded,"NEXT i"
  TEXT.WRITELN EnCoded,"FOR i=700 TO 20 STEP -10"
  TEXT.WRITELN EnCoded,"  j=j-10"
  TEXT.WRITELN EnCoded,"  GR.MODIFY ShowPic,"+CHR$(34)+"x"+CHR$(34)+",i"
  TEXT.WRITELN EnCoded,"  GR.MODIFY ShowPic,"+CHR$(34)+"y"+CHR$(34)+",j"
  TEXT.WRITELN EnCoded,"  GR.RENDER"
  TEXT.WRITELN EnCoded,"NEXT i"
  TEXT.WRITELN EnCoded,"PAUSE 1000"
  TEXT.WRITELN EnCoded,"GR.BITMAP.SCALE LargePic,"+FName$+"Pic,500/PHeight*PWidth,500,0"
  TEXT.WRITELN EnCoded,"GR.BITMAP.DRAW ShowLargePic,LargePic,PWidth+20,0"
  TEXT.WRITELN EnCoded,"GR.TEXT.SIZE 30"
  TEXT.WRITELN EnCoded,"GR.COLOR 255,255,255,255,1"
  TEXT.WRITELN EnCoded,"GR.TEXT.DRAW g,20,540,"+CHR$(34)+"Original file : "+FName$+Ext$+CHR$(34)
  TEXT.WRITELN EnCoded,"GR.TEXT.DRAW g,20,580,"+CHR$(34)+"Resolution : "+CHR$(34)+"+FORMAT$("+CHR$(34)+"##%"+CHR$(34)+",PWidth)+"+CHR$(34)+" x"+CHR$(34)+"+FORMAT$("+CHR$(34)+"##%"+CHR$(34)+",PHeight)"
  TEXT.WRITELN EnCoded,"IF GS=0 THEN"
  TEXT.WRITELN EnCoded,"  T1$="+CHR$(34)+"Colour image"+CHR$(34)
  TEXT.WRITELN EnCoded,"ELSE"
  TEXT.WRITELN EnCoded,"  T1$="+CHR$(34)+"Greyscale image"+CHR$(34)
  TEXT.WRITELN EnCoded,"ENDIF"
  TEXT.WRITELN EnCoded,"IF TrnsP=0 THEN"
  TEXT.WRITELN EnCoded,"  T2$="+CHR$(34)+"No transparent colour"+CHR$(34)
  TEXT.WRITELN EnCoded,"ELSE"
  TEXT.WRITELN EnCoded,"  T2$="+CHR$(34)+"Transparent Colour (RGB) :"+CHR$(34)+"+FORMAT$("+CHR$(34)+"##%"+CHR$(34)+",rt)+"+CHR$(34)+","+CHR$(34)+"+FORMAT$("+CHR$(34)+"##%"+CHR$(34)+",gt)+"+CHR$(34)+","+CHR$(34)+"+FORMAT$("+CHR$(34)+"##%"+CHR$(34)+",bt)"
  TEXT.WRITELN EnCoded,"ENDIF"
  TEXT.WRITELN EnCoded,"IF Colorise=0 THEN"
  TEXT.WRITELN EnCoded,"  T3$="+CHR$(34)+CHR$(34)
  TEXT.WRITELN EnCoded,"ELSE"
  TEXT.WRITELN EnCoded,"  T3$="+CHR$(34)+"Colourised with (RGB) :"+CHR$(34)+"+FORMAT$("+CHR$(34)+"##%"+CHR$(34)+",rc)+"+CHR$(34)+","+CHR$(34)+"+FORMAT$("+CHR$(34)+"##%"+CHR$(34)+",gc)+"+CHR$(34)+","+CHR$(34)+"+FORMAT$("+CHR$(34)+"##%"+CHR$(34)+",bc)"
  TEXT.WRITELN EnCoded,"ENDIF"
  TEXT.WRITELN EnCoded,"GR.TEXT.DRAW g,20,620,T1$"
  TEXT.WRITELN EnCoded,"GR.TEXT.DRAW g,20,660,T2$"
  TEXT.WRITELN EnCoded,"GR.TEXT.DRAW g,20,700,T3$"
  TEXT.WRITELN EnCoded,"GR.RENDER"
  TEXT.WRITELN EnCoded,"TONE 600,300,0"
  TEXT.WRITELN EnCoded,"POPUP "+CHR$(34)+"Ready, touch screen to exit"+CHR$(34)+",-300,0,1"
  TEXT.WRITELN EnCoded,"DO"
  TEXT.WRITELN EnCoded,"  GR.TOUCH touched,x,y"
  TEXT.WRITELN EnCoded,"UNTIL touched"
  TEXT.WRITELN EnCoded,"DO"
  TEXT.WRITELN EnCoded,"  GR.TOUCH touched,x,y"
  TEXT.WRITELN EnCoded,"UNTIL !touched"
  TEXT.WRITELN EnCoded,"WAKELOCK 5"
  TEXT.WRITELN EnCoded,"GR.CLOSE"
  TEXT.WRITELN EnCoded,"END"
  TEXT.WRITELN EnCoded,"Make"+FName$+":"
  TEXT.WRITELN EnCoded,"READ.NEXT Hor,GS,PWidth,PHeight"
  TEXT.WRITELN EnCoded,"IF GS=0 THEN"
  TEXT.WRITELN EnCoded,"  READ.NEXT TrnsP,rt,gt,bt"
  TEXT.WRITELN EnCoded,"ELSE"
  TEXT.WRITELN EnCoded,"  READ.NEXT TrnsP,rt"
  TEXT.WRITELN EnCoded,"  gt=rt"
  TEXT.WRITELN EnCoded,"  bt=rt"
  TEXT.WRITELN EnCoded,"ENDIF"
  TEXT.WRITELN EnCoded,"READ.NEXT Colorise,rc,gc,bc"
  TEXT.WRITELN EnCoded,"GOSUB Get"+FName$+"Data"
  TEXT.WRITELN EnCoded,"GR.BITMAP.CREATE "+FName$+"Pic,PWidth,PHeight"
  TEXT.WRITELN EnCoded,"GR.BITMAP.DRAWINTO.START "+FName$+"Pic"
  TEXT.WRITELN EnCoded,"GOSUB Decode"
  TEXT.WRITELN EnCoded,"RETURN"
  TEXT.WRITELN EnCoded,"Decode:"
  TEXT.WRITELN EnCoded,"x1=0"
  TEXT.WRITELN EnCoded,"y1=0"
  TEXT.WRITELN EnCoded,"x2=0"
  TEXT.WRITELN EnCoded,"y2=0"
  TEXT.WRITELN EnCoded,"Counter=1"
  TEXT.WRITELN EnCoded,"DO"
  ! Read string of codes (subtract 128) using UCODE()
  TEXT.WRITELN EnCoded,"  IF GS=0 THEN"
  TEXT.WRITELN EnCoded,"    n=UCODE(MID$(PDat$,Counter,1))-128"
  TEXT.WRITELN EnCoded,"    Counter=Counter+1"
  TEXT.WRITELN EnCoded,"    r=UCODE(MID$(PDat$,Counter,1))-128"
  TEXT.WRITELN EnCoded,"    C ounter=Counter+1"
  TEXT.WRITELN EnCoded,"    g=UCODE(MID$(PDat$,Counter,1))-128"
  TEXT.WRITELN EnCoded,"    Counter=Counter+1"
  TEXT.WRITELN EnCoded,"    b=UCODE(MID$(PDat$,Counter,1))-128"
  TEXT.WRITELN EnCoded,"    Counter=Counter+1"
  TEXT.WRITELN EnCoded,"  ELSE"
  TEXT.WRITELN EnCoded,"    n=UCODE(MID$(PDat$,Counter,1))-128"
  TEXT.WRITELN EnCoded,"    Counter=Counter+1"
  TEXT.WRITELN EnCoded,"    r=UCODE(MID$(PDat$,Counter,1))-128"
  TEXT.WRITELN EnCoded,"    Counter=Counter+1"
  TEXT.WRITELN EnCoded,"    g=r"
  TEXT.WRITELN EnCoded,"    b=r"
  TEXT.WRITELN EnCoded,"  ENDIF"
  TEXT.WRITELN EnCoded,"  IF r=rt & g=gt & b=bt & TrnsP=1 THEN"
  TEXT.WRITELN EnCoded,"    d=1"
  TEXT.WRITELN EnCoded,"  ELSE"
  TEXT.WRITELN EnCoded,"    d=0"
  TEXT.WRITELN EnCoded,"  ENDIF"
  TEXT.WRITELN EnCoded,"  IF Colorise=1 THEN"
  TEXT.WRITELN EnCoded,"    GR.COLOR 255,MAX(r,rc),MAX(g,gc),MAX(b,bc),1"
  TEXT.WRITELN EnCoded,"  ELSE"
  TEXT.WRITELN EnCoded,"    GR.COLOR 255,r,g,b,1"
  TEXT.WRITELN EnCoded,"  ENDIF"
  TEXT.WRITELN EnCoded,"  IF Hor=1 THEN"
  TEXT.WRITELN EnCoded,"    x2=x2+n-1"
  TEXT.WRITELN EnCoded,"    IF x2<PWidth THEN"
  TEXT.WRITELN EnCoded,"      IF d=0 THEN GR.LINE g,x1,y1,x2+1,y2"
  TEXT.WRITELN EnCoded,"      x2=x2+1"
  TEXT.WRITELN EnCoded,"      x1=x2"
  TEXT.WRITELN EnCoded,"    ELSE"
  TEXT.WRITELN EnCoded,"      WHILE x2>=PWidth-1"
  TEXT.WRITELN EnCoded,"        IF d=0 THEN GR.LINE g,x1,y1,PWidth,y2"
  TEXT.WRITELN EnCoded,"        x2=x2-PWidth"
  TEXT.WRITELN EnCoded,"        x1=0"
  TEXT.WRITELN EnCoded,"        y1=y1+1"
  TEXT.WRITELN EnCoded,"        y2=y1"
  TEXT.WRITELN EnCoded,"      REPEAT"
  TEXT.WRITELN EnCoded,"      IF d=0 THEN GR.LINE g,x1,y1,x2+1,y2"
  TEXT.WRITELN EnCoded,"        x2=x2+1"
  TEXT.WRITELN EnCoded,"        x1=x2"
  TEXT.WRITELN EnCoded,"    ENDIF"
  TEXT.WRITELN EnCoded,"  UNTIL y1>=PHeight"
  TEXT.WRITELN EnCoded,"  ELSE"
  TEXT.WRITELN EnCoded,"    y2=y2+n-1"
  TEXT.WRITELN EnCoded,"    IF y2<PHeight THEN"
  TEXT.WRITELN EnCoded,"      IF d=0 THEN GR.LINE g,x1,y1,x2,y2+1"
  TEXT.WRITELN EnCoded,"      y2=y2+1"
  TEXT.WRITELN EnCoded,"      y1=y2"
  TEXT.WRITELN EnCoded,"    ELSE"
  TEXT.WRITELN EnCoded,"      WHILE y2>=PHeight-1"
  TEXT.WRITELN EnCoded,"        IF d=0 THEN GR.LINE g,x1,y1,x2,PHeight"
  TEXT.WRITELN EnCoded,"        y2=y2-PHeight"
  TEXT.WRITELN EnCoded,"        y1=0"
  TEXT.WRITELN EnCoded,"        x1=x1+1"
  TEXT.WRITELN EnCoded,"        x2=x1"
  TEXT.WRITELN EnCoded,"      REPEAT"
  TEXT.WRITELN EnCoded,"      IF d=0 THEN GR.LINE g,x1,y1,x2,y2+1"
  TEXT.WRITELN EnCoded,"        y2=y2+1"
  TEXT.WRITELN EnCoded,"        y1=y2"
  TEXT.WRITELN EnCoded,"    ENDIF"
  TEXT.WRITELN EnCoded,"  UNTIL x1>=PWidth"
  TEXT.WRITELN EnCoded,"  ENDIF"
  TEXT.WRITELN EnCoded,"GR.BITMAP.DRAWINTO.END"
  TEXT.WRITELN EnCoded,"GR.RENDER"
  TEXT.WRITELN EnCoded,"RETURN"
  W$="READ.DATA "
  TEXT.WRITELN EnCoded,W$+REPLACE$(FORMAT$("%",Hor)," ","")+","+REPLACE$(FORMAT$("%",GS)," ","")+","+REPLACE$(FORMAT$("##%",PWidth)," ","")+","+REPLACE$(FORMAT$("##%",PHeight)," ","")
  IF GS=0 THEN
    TEXT.WRITELN EnCoded,W$+REPLACE$(FORMAT$("%",TrnsP)," ","")+","+REPLACE$(FORMAT$("##%",rt)," ","")+","+REPLACE$(FORMAT$("##%",gt)," ","")+","+REPLACE$(FORMAT$("##%",bt)," ","")
  ELSE
    TEXT.WRITELN EnCoded,W$+REPLACE$(FORMAT$("%",TrnsP)," ","")+","+REPLACE$(FORMAT$("##%",rt)," ","")
  ENDIF
  TEXT.WRITELN EnCoded,W$+REPLACE$(FORMAT$("%",Colorise)," ","")+","+REPLACE$(FORMAT$("##%",rc)," ","")+","+REPLACE$(FORMAT$("##%",gc)," ","")+","+REPLACE$(FORMAT$("##%",bc)," ","")
  TEXT.WRITELN EnCoded,"Get"+FName$+"Data:" 
  W$="PDat$="+CHR$(34)
  Px=0
  Py=0
  RLECnt=0
  DO
    RLECnt=RLECnt+1
    GR.GET.BMPIXEL PicIn,Px,Py,ap1,rp1,gp1,bp1
    IF GS=1 THEN
      GV=FLOOR((rp1+gp1+bp1)/3)
      rp1=GV
      gp1=GV
      bp1=GV
    ENDIF
    NxtRLE=0
    DO
      IF Hor=1 THEN
        Px=Px+1
        IF Px=PWidth THEN
          Px=0
          Py=Py+1
          IF Py=PHeight THEN
            D_U.BREAK
          ELSE
            GR.MODIFY PG,"bottom",Py*PrBar+1
            GR.MODIFY PB,"text",FORMAT$("##%",Py/(PHeight-1)*100)+" %"
            GR.RENDER
          ENDIF
        ENDIF
      ELSE
        Py=Py+1
        IF Py=PHeight THEN
          Py=0
          Px=Px+1
          IF Px=PWidth THEN
            D_U.BREAK
          ELSE
            GR.MODIFY PG,"bottom",Px*PrBar+1
            GR.MODIFY PB,"text",FORMAT$("##%",Px/(PWidth-1)*100)+" %"
            GR.RENDER
          ENDIF
        ENDIF
      ENDIF
      GR.GET.BMPIXEL PicIn,Px,Py,ap2,rp2,gp2,bp2
      IF GS=1 THEN
        GV=FLOOR((rp2+gp2+bp2)/3)
        rp2=GV
        gp2=GV
        bp2=GV
      ENDIF
      IF rp1=rp2 & gp1=gp2 & bp1=bp2 THEN
        RLECnt=RLECnt+1
      ELSE
        NxtRLE=1
      ENDIF
    UNTIL NxtRLE=1
    ! do not use unprintable chars
    RLECnt=RLECnt+128
    rp1=rp1+128
    gp1=gp1+128
    bp1=bp1+128
    IF GS=0 THEN
      W$=W$+CHR$(RLECnt)+CHR$(rp1)+CHR$(gp1)+CHR$(bp1)
    ELSE
      W$=W$+CHR$(RLECnt)+CHR$(rp1)
    ENDIF
    RLECnt=0
    IF Hor=1 THEN
      UNTIL Py=PHeight
    ELSE
      UNTIL Px=PWidth
    ENDIF
  TEXT.WRITELN EnCoded,W$+CHR$(34)
  TEXT.WRITELN EnCoded,"RETURN"
TEXT.CLOSE EnCoded
RETURN
Dialog:
  GR.BITMAP.DRAWINTO.START Menu
    GR.COLOR 255,0,0,0,1
    GR.RECT g,5,5,495,100
    GR.COLOR 255,255,255,0,0
    GR.TEXT.DRAW g,30,50,Q$
  GR.BITMAP.DRAWINTO.END
  GR.SHOW ShowMenu
  GR.RENDER
RETURN
GetTap:
DO
  DO
  GOSUB GetTouch
  UNTIL y>=630*sy & y<=690*sy
UNTIL (x>=100*sx & x<=200*sx) | (x>=400*sx & x<=500*sx)
GR.HIDE ShowMenu
RETURN
GetTouch:
  DO
    GR.TOUCH touched,x,y
  UNTIL touched
  DO
    GR.TOUCH touched,x,y
  UNTIL !touched
RETURN
Makecartman:
READ.NEXT Hor,GS,PWidth,PHeight
IF GS=0 THEN
  READ.NEXT TrnsP,rt,gt,bt
ELSE
  READ.NEXT TrnsP,rt
  gt=rt
  bt=rt
ENDIF
READ.NEXT Colorise,rc,gc,bc
GOSUB GetcartmanData
GR.BITMAP.CREATE cartmanPic,PWidth,PHeight
GR.BITMAP.DRAWINTO.START cartmanPic
GOSUB Decode
RETURN
MakeButton1:
READ.NEXT Hor,GS,PWidth,PHeight
IF GS=0 THEN
  READ.NEXT TrnsP,rt,gt,bt
ELSE
  READ.NEXT TrnsP,rt
  gt=rt
  bt=rt
ENDIF
READ.NEXT Colorise,rc,gc,bc
GOSUB GetButtonData
GR.BITMAP.CREATE Button1Pic,PWidth,PHeight
GR.BITMAP.DRAWINTO.START Button1Pic
GOSUB Decode
RETURN
MakeButton2:
rc=255
gc=0
bc=0
GOSUB GetButtonData
GR.BITMAP.CREATE Button2Pic,PWidth,PHeight
GR.BITMAP.DRAWINTO.START Button2Pic
GOSUB Decode
RETURN
Decode:
x1=0
y1=0
x2=0
y2=0
Counter=1
  DO
    IF GS=0 THEN
        n=UCODE(MID$(PDat$,Counter,1))-128
        Counter=Counter+1
        r=UCODE(MID$(PDat$,Counter,1))-128
        Counter=Counter+1
        g=UCODE(MID$(PDat$,Counter,1))-128
        Counter=Counter+1
        b=UCODE(MID$(PDat$,Counter,1))-128
        Counter=Counter+1
    ELSE
        n=UCODE(MID$(PDat$,Counter,1))-128
        Counter=Counter+1
        r=UCODE(MID$(PDat$,Counter,1))-128
        Counter=Counter+1
        g=r
        b=r
    ENDIF
    IF r=rt & g=gt & b=bt & TrnsP=1 THEN
      d=1
    ELSE
      d=0
    ENDIF
    IF Colorise=1 THEN
      GR.COLOR 255,MAX(r,rc),MAX(g,gc),MAX(b,bc),1
    ELSE
      GR.COLOR 255,r,g,b,1
    ENDIF
    IF Hor=1 THEN
      x2=x2+n-1
      IF x2<PWidth THEN
        IF d=0 THEN GR.LINE g,x1,y1,x2+1,y2
          x2=x2+1
          x1=x2
        ELSE
          WHILE x2>=PWidth-1
            IF d=0 THEN GR.LINE g,x1,y1,PWidth,y2
              x2=x2-PWidth
              x1=0
              y1=y1+1
              y2=y1
          REPEAT
          IF d=0 THEN GR.LINE g,x1,y1,x2+1,y2
            x2=x2+1
            x1=x2
          ENDIF
      UNTIL y1>=PHeight
    ELSE
      y2=y2+n-1
      IF y2<PHeight THEN
        IF d=0 THEN GR.LINE g,x1,y1,x2,y2+1
          y2=y2+1
          y1=y2
        ELSE
          WHILE y2>=PHeight-1
            IF d=0 THEN GR.LINE g,x1,y1,x2,PHeight
              y2=y2-PHeight
              y1=0
              x1=x1+1
              x2=x1
          REPEAT
          IF d=0 THEN GR.LINE g,x1,y1,x2,y2+1
            y2=y2+1
            y1=y2
          ENDIF
      UNTIL x1>=PWidth
    ENDIF
GR.BITMAP.DRAWINTO.END
RETURN
READ.DATA 1,0,48,48
READ.DATA 1,255,255,255
READ.DATA 0,0,0,0
GetcartmanData:
PDat$="ŗſſſſŷÊ¬ſſſſŷÊ¨ſſſſŷÊ¤ſſſĀŔŹſŷÊĀŔŹſſſĀŔŹſŷÊĀŔŹſſſĀŔŹſſſĀŔŹſſſĀŔŹſſſĀŔŹſſſĀŔŹſŷÊĀŔŹſſſſŷÊſſſſŷÊſŗļžžžſŗļžžžſŗļſŷÊſſſſŗļžžžſŗļžžžſŗļſſſſŗļžžžſŗļſſſſŗļžžž÷éáſŗļžžž÷éáſŗļžžžſŗļſſſſŗļžžž÷éáſŗļžžž÷éáſŗļžžžſŗļſſſſŗļžžžſŗļžžžſŗļſſſſŗļžžžſŗļžžžſŗļſſſſŗļžžžſŗļžžžſŗļſſſ¡ſŗļſſſŬ£ſŗļŬ£ſſſŬ£ſŗļŬ£ſſſŬ£ſŗļ÷éáſŗļŬ£ſſſŬ£ſŗļſŗļŬ£ſſſŬ£ſŗļſŗļŬ£ſſſŬ£ſŗļ÷éáſŗļŬ£ſſſŬ£ſŗļŬ£ſŷÊſſſſŷÊŬ£ſŗļŬ£ſŷÊſſſſŷÊŬ£ſŗļŬ£ſŷÊſſſſŷÊŬ£Ŭ£ſŷÊſſſſŷÊŬ£÷éáŬ£ſŷÊſſſſŷÊŬ£÷éáŬ£ſŷÊſſſŬ£÷éáŬ£ſſſŬ£Ŭ£ſſſŬ£Ŭ£ſſſŬ£Ŭ£ſſſŬ£Ŭ£ſſſ÷éáŬ£÷éáŬ£÷éáſſſ÷éáŬ£÷éáŬ£÷éáſſſ÷éáſſſęſſſ"
RETURN
READ.DATA 1,1,100,60
READ.DATA 1,0
READ.DATA 1,0,255,0
GetButtonData:
PDat$="ÒņØņÛņÝņņÒĀņņ×ĀņņÙĀņņĀÈĀņņĀÌĀņĀÎĀņĀÐĀņĀÒĀņĀÒĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÔĀņĀÓĀņĀÒĀņĀÑĀņĀÐĀņĀÌĀņĀÊĀņņÚĀņņØĀņņÔĀņÞņÜņØņÔņ"
RETURN
