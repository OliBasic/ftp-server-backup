INCPIC - INC(lude)PIC(ture)

Notes on pictures.

This programme is meant to be used on small pictures with a limited number of colours !
It's better to use PNG not JPG pictures. JPG artifacts make these type of pictures unsuitable for RLE compression.
The more colours the less the benefit of compression and the longer the decoding will take.
Use icon-like or sprite-like pictures.
Use a paint programme to count and/or reduce the number of colours and maybe to reduce resolution.
A PNG of 50x50 pixels and 4 to 8 colours will work fine.