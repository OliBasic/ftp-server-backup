! system benchmark graphics+sd+processor v0.1
! by Antonis [tony_gr]
! July 2013
! this program is free for personal use.
! use it at your own risk.

DIM cartmanPng[200], rct[200],text[200],cman[100]
DIM resPh[6], resTab[6] % values in secs
DIM normPh[6], normTab[6] % y values of bars
ARRAY.LOAD Phones$[],"fake Motorola", "sony xperia neo V", "fake htc", "fake samsung", "fake LG","my device"
ARRAY.LOAD Tablets$[], "TWD_MID 7\"", "Nova Pad 70 C124FC", "Asus Transformer TF101", "fake Google nexus 7\"", "fake samsung 10\"","my device"

resPH[1]=190
resPH[2]=53.5
resPH[3]=250
resPH[4]=100
resPH[5]=42.1
resPH[6]=0
resTAB[1]=121.2
resTAB[2]=73.5
resTAB[3]=44.9
resTAB[4]=80
resTAB[5]=36.5
resTAB[6]=0

FN.DEF xtime()
 TIME Y$, M$, D$, H$, M$, S$
 FN.RTN VAL(H$) * 3600 + VAL(M$)*60 + VAL(S$)
FN.END

GR.OPEN 255,0,55,0,0,1
PAUSE 1000
GR.ORIENTATION 1
GR.SCREEN w, h
sx = w / 800
sy = h / 1232
GR.SCALE sx,sy
GR.RENDER

bg1$=CHR$(8750)+"  "+CHR$(8721)+"  "+ CHR$(8734)+" "+CHR$(8704)+" "+ CHR$(8532)+" "+ CHR$(8895)
GR.COLOR 20,255,255,0,1
GR.TEXT.SIZE 120
GR.TEXT.BOLD 1
k=10
j=150
FOR l=1 TO 5
 GR.TEXT.DRAW tt,10,j,bg1$
 j=j+250
NEXT
GR.COLOR 255, 255, 255, 55, 1
GR.TEXT.BOLD 0
GR.TEXT.SIZE 20
GR.TEXT.DRAW tt,110,1200,"System benchmark v0.1 by Antonis [tony_gr]"

GOSUB speedTest
GOSUB graphicsTest
GOSUB sdTest

GR.COLOR 255,0,55,0,1
GR.RECT r,0,0,800,310
GR.COLOR 20,255,255,0,1
GR.TEXT.SIZE 120
GR.TEXT.BOLD 1
GR.TEXT.DRAW tt,10,150,bg1$
GR.COLOR 255, 25, 155, 255, 1
GR.TEXT.SIZE 45
TotSecs=TotSd+TotGr+mTimeUsed
TotSecs$="Total: "+REPLACE$(STR$(TotSecs),".0","")+" secs"
GR.TEXT.DRAW tx,200,950,TotSecs$
GR.RENDER
TONE 350,100,0
POPUP "Tap screen to go!",0,0,4
DO
 GR.TOUCH touched,x,y
UNTIL touched
DO
 GR.TOUCH touched,x,y
UNTIL !touched


GR.CLS
resPH[6]= TotSecs
resTAB[6]=TotSecs
GOSUB viewResults

DO
UNTIL 0

REM **********************************************************
sdTest:
GR.COLOR 255, 55, 255, 255, 1
GR.TEXT.SIZE 120
GR.TEXT.DRAW tx0,50,700 ,CHR$(9758)
GR.TEXT.SIZE 35
! sd card
GR.COLOR 255, 255, 255, 55, 1
GR.TEXT.DRAW tx1,200,650,"SD card Benchmark"
GR.COLOR 255, 255, 255, 255, 1
GR.TEXT.DRAW tx2,200,700, "Pass 1/2: writing to SD.."
GR.RENDER

test$=""
a$="01234567890qwertyuioplkjhgfdsazxcvbnm"
TotSd=0
xx2=0
FOR i=1 TO 300
 test$=test$+a$
NEXT i
FOR j=1 TO 2
 TEXT.OPEN w,file,  "test"
 x=CLOCK()
 FOR i=1 TO 300
  TEXT.WRITELN file, test$
 NEXT i
 xx1=CLOCK()-x
 TEXT.CLOSE file
 FILE.DELETE l, "test"
 xx2=xx2+xx1
NEXT j
TotSd=xx2
test$=""

GR.RENDER

! 2 loading
GR.MODIFY tx2, "text", "Pass 2/2: reading from SD.."
GR.RENDER
x=CLOCK()
FOR i=1 TO 50
 GR.BITMAP.LOAD cman[i],"cartman.png"
NEXT i
xx2=CLOCK()-x
GR.RENDER
FOR i=1 TO 50
 GR.BITMAP.DELETE cman[i]
NEXT i

TotSd=TotSD+xx2
TotSd=ROUND(TotSD/100)/10
GR.MODIFY tx2, "text", "Total SD: "+REPLACE$(STR$(TotSd),".0","")+" secs"
GR.RENDER
PAUSE 2000
RETURN

REM **********************************************************
graphicsTest:
! graphics tests
GR.TEXT.SIZE 120
GR.COLOR 255, 55, 255, 255, 1
GR.TEXT.DRAW tx0,50,550 ,CHR$(9758)
GR.TEXT.SIZE 35
GR.COLOR 255, 255, 255, 55, 1
GR.TEXT.DRAW tx3,200,500 ,"Graphics Benchmark"
GR.COLOR 255, 255, 255, 255, 1
GR.TEXT.DRAW tx4,200,550 ,"Pass 1/9: Draw "
GR.COLOR 255, 255, 255, 255, 1
GR.RENDER
GR.BITMAP.LOAD cman,"cartman.png"


! 1 draw
x=CLOCK()
FOR i=1 TO 200
 GR.TEXT.DRAW text[i], 40,40+i,"Benchmark"
NEXT i
x1=CLOCK()-x
GR.COLOR 255, 155, 0, 25, 1
GR.TEXT.DRAW text, 40,40+i,"Benchmark"
GR.RENDER

! 2 render
GR.MODIFY tx4,"text" ,"Pass 2/9: Rendering "
GR.RENDER
x =CLOCK()
FOR i=1 TO 100
 GR.BITMAP.DRAW cartmanPng[i], cman,i,i
 GR.RENDER
NEXT i
x2=CLOCK()-x
GR.RENDER

! 3 shapes
GR.MODIFY tx4,"text" ,"Pass 3/9: Shapes "
GR.RENDER
x=CLOCK()
GR.COLOR 255,155,0,0,0
FOR i=1 TO 200
 GR.RECT rct[i], 180,0, 180+i,120
 GR.ARC ar, 420,0,420+i,20+i,0,i,0
NEXT i
x3=CLOCK()-x

! 4 display list
GR.MODIFY tx4,"text" ,"Pass 4/9: Display list'
GR.RENDER
x=CLOCK()
FOR i=1 TO 200
 GR.HIDE cartmanPng[i]
 GR.HIDE rct[i]
 GR.HIDE text[i]
NEXT i
GR.RENDER
FOR i=1 TO 200
 GR.SHOW cartmanPng[i]
 GR.SHOW rct[i]
 GR.SHOW text[i]
NEXT i
x4=CLOCK()-x

! 5 modify objects
GR.MODIFY tx4,"text" ,"Pass 5/9: Modifies "
GR.RENDER
x=CLOCK()
FOR i=1 TO 200
 GR.MODIFY cartmanPng[i], "alpha" ,i
 GR.MODIFY rct[i],"top",i
 GR.MODIFY text[i],"text","test"
NEXT i
x5=CLOCK()-x

! 6 create bitmaps
GR.MODIFY tx4,"text" ,"Pass 6/9: Bitmaps "
GR.RENDER
FOR i=1 TO 50
 GR.BITMAP.CREATE bmp,200,200
 GR.BITMAP.DELETE bmp
NEXT i
x6=CLOCK()-x

! 7 rotate
GR.MODIFY tx4,"text" ,"Pass 7/9: Rotation "
GR.RENDER
x=CLOCK()
FOR i=1 TO 360
 GR.ROTATE.START i,i,i/2
 GR.BITMAP.DRAW cartmanPng,cman,i,i/2
 GR.ROTATE.END
NEXT i
x7=CLOCK()-x


! 8 text commands
GR.MODIFY tx4,"text" ,"Pass 8/9: Text "
GR.RENDER
x=CLOCK()
FOR i=1 TO 200
 GR.COLOR 255,0,0,0,1
 GR.TEXT.TYPEFACE 3
 GR.TEXT.SIZE 35
 GR.TEXT.BOLD 1
 GR.TEXT.TYPEFACE 2
 GR.TEXT.SKEW -0.20
 GR.TEXT.UNDERLINE 1
NEXT i
x9=CLOCK()-x

GR.TEXT.BOLD 0
GR.TEXT.TYPEFACE 3
GR.TEXT.SKEW 0
GR.TEXT.UNDERLINE 0
GR.COLOR 255,255,255,255,1

! 9 pixel commands
GR.MODIFY tx4,"text" ,"Pass 9/10: pixel "
GR.RENDER
x=CLOCK()
FOR i=1 TO 200
 GR.TEXT.WIDTH s,"Benchmark"
 GR.GET.TEXTBOUNDS "Benchmark" , left, top, right, bottom
 GR.GET.POSITION cartmanPng,  xx, yy
NEXT i
x9=CLOCK()-x

! 10 3d
REM 3d sphere
GR.COLOR 255,255,255,55,1
GR.MODIFY tx4,"text" ,"Pass 10/10: 3d Rendering"
GR.RENDER
nOfPoints = 1000
zoomLevel = 0
ARRAY.DELETE points[]
DIM points[nofpoints,3]
x=CLOCK()
r=50
x0=600
y0=200
z0=300
pi=3.14159
f=0
t=0
i=1
FOR f=0 TO pi STEP 0.35
 FOR t=0 TO 2*pi STEP 0.06
  points[i,1]=x0+r*COS(t)*SIN(f)
  points[i,2]=y0+r*SIN(f)*SIN(t)
  points[i,3]=z0+r*COS(f)
  i=i+1
 NEXT t
NEXT f
FOR pointIdx = 1 TO i-1
 IF points[pointIdx,3] + zoomLevel > 0 THEN
  flatX = points[pointIdx,1] * 256 / (points[pointIdx,3] + zoomLevel)
  flatY = points[pointIdx,2] * 256 / (points[pointIdx,3] + zoomLevel)
  GR.CIRCLE cir, flatX, flatY,2
 ENDIF
NEXT
GR.RENDER
x10=CLOCK()-x
PAUSE 2000
GR.COLOR 255,0,0,0,1
TotGr=x1+x2+x3+ x4+x5+x6+ x7+x8+x9+x10
TotGr=ROUND(TotGr/100)/10
GR.MODIFY tx4,"text","Total Graphics: " +  REPLACE$(STR$(TotGr),".0","")+ " secs"
GR.RENDER
RETURN


REM **********************************************************
speedTest:
GR.COLOR 255, 55, 255, 255, 1
GR.TEXT.SIZE 120
GR.TEXT.DRAW tx0,50,400 ,CHR$(9758)
GR.TEXT.SIZE 35
GR.COLOR 255, 255, 255, 55, 1
GR.TEXT.DRAW tx1, 200,350,"Processor Whetstone Benchmark"
GR.COLOR 255, 255, 255, 255, 1
GR.TEXT.DRAW tx2, 200,400,""
GR.RENDER
UNDIM ex[]
UNDIM results[]
UNDIM ztime[]
UNDIM headings[]
UNDIM ops[]
UNDIM flops[]
DIM ex[4]
DIM results[8]
DIM ztime[8]
DIM heading$[8]
DIM ops[8]
DIM flops[8]
icount = 10
calibrate = 0
ixtra = 1
ix100 = 1
REM Passes to average
Passes = 10
GR.COLOR 255,255,255,255,1
GR.TEXT.SIZE 40
GR.RENDER
mTimeUsed = 0
Check = 0
FOR Pass = 1 TO Passes
 GOSUB Whetstones
NEXT Pass
GR.MODIFY tx2,"text",  "Total Processor: "+REPLACE$(STR$(mTimeUsed),".0","")+" secs"
IF Check = 0 THEN
 POPUP "Calculation Error Detected",0,0,4
ELSE
 POPUP "Calculations Correct",0,0,0
ENDIF
GR.RENDER
TotCalc=mTimeUsed
RETURN

Whetstones:
REM   INITIALISE CONSTANTS
t = 0.49999975
t0 = t
t1 = 0.50000025
t2 = 2
n1 = 12 * ix100
n2 = 14 * ix100
n3 = 345 * ix100
n4 = 210 * ix100
n5 = 32 * ix100
n6 = 899 * ix100
n7 = 616 * ix100
n8 = 93 * ix100
n1mult = 10
GR.MODIFY tx2,"text","Pass: "+REPLACE$(STR$(Pass),".0","")+" of "+ REPLACE$(STR$(Passes),".0","")
GR.RENDER
REM MODULE 1 - ARRAY ELEMENTS
stime = xTime()
ex[1] = 1
ex[2] = -1
ex[3] = -1
ex[4] = -1
FOR ix = 1 TO ixtra
 FOR i = 1 TO n1 * n1mult
  ex[1] = (ex[1] + ex[2] + ex[3] - ex[4]) * t
  ex[2] = (ex[1] + ex[2] - ex[3]+ ex[3]) * t
  ex[3] = (ex[1] - ex[2] + ex[3] + ex[4]) * t
  ex[4]= (-ex[1] + ex[2] + ex[3] + ex[4]) * t
 NEXT i
 t = 1.0 - t
NEXT ix
t = t0
checsum = ex[4]
rtime = (xTime() - stime)
smflops = n1 * 16
title$ = "N1 floating point"
atype = 1
section = 1
REM N1 * 16 floating point calculations
GOSUB Pout
REM MODULE 2 - ARRAY AS PARAMETER
stime = xTime()
FOR ix = 1 TO ixtra
 FOR i = 1 TO n2
  GOSUB PA
 NEXT i
 t = 1.0 -t
NEXT ix
t = t0
checsum = ex[4]
rtime = xTime() - stime
smflops = n2 * 96
title$ = "N2 floating point"
atype = 1
section = 2
REM  N2 * 96 floating point calculations
GOSUB Pout
REM MODULE 3 - CONDITIONAL JUMPS
stime = xTime()
j = 1
FOR ix = 1 TO ixtra
 FOR i = 1 TO n3
  IF j <> 1 THEN
   j = 3
  ELSE
   j = 2
  ENDIF
  IF j <= 2 THEN
   j = 1
  ELSE
   j = 0
  ENDIF
  IF j >= 1 THEN
   j = 0
  ELSE
   j = 1
  ENDIF
 NEXT i
NEXT ix
checsum = j
rtime = xTime() - stime
smflops = n3 * 3
title$ = "N3 if then else"
atype = 2
section = 3
REM  N3 * 3 IF THEN ELSE
GOSUB Pout
REM MODULE 4 - INTEGER ARITHMETIC
stime = xTime()
j = 1
k = 2
l = 3
FOR ix = 1 TO ixtra
 FOR i = 1 TO n4
  j = j * (k - j) * (l - k)
  k = l * k - (l - j) * k
  l = (l - k) * (k + j)
  ex[l - 1] = j + k + l
  ex[k - 1] = j * k * l
 NEXT i
NEXT ix
checsum = ex[2] + ex[1]
rtime = xTime() - stime
smflops = n4 * 15
title$ = "N4 fixed point"
atype = 2
section = 4
REM  N4 * 15 fixed point operations
GOSUB Pout
REM MODULE 5 - TRIG. FUNCTIONS
stime = xTime()
X = 0.5
Y = 0.5
FOR ix = 1 TO ixtra
 FOR i = 1 TO n5
  X = t * ATAN(t2 * SIN(X) * COS(X) / (COS(X + Y) + COS(X - Y) - 1))
  Y = t * ATAN(t2 * SIN(Y) * COS(Y) / (COS(X + Y) + COS(X - Y) - 1))
 NEXT i
 t = 1.0 - t
NEXT ix
t = t0
checsum = Y
rtime = xTime() - stime
smflops = n5 * 26
title$ = "N5 sin,cos etc."
atype = 2
section = 5
REM  N5 * 26 function calls and floating point operations
GOSUB Pout
REM MODULE 6 - PROCEDURE CALLS
stime = xTime()
X = 1
Y = 1
Z = 1
FOR ix = 1 TO ixtra
 FOR i = 1 TO n6
  GOSUB P3
 NEXT i
NEXT ix
checsum = Z
rtime = xTime() - stime
smflops = n6 * 6
title$ = "N6 floating point"
atype = 1
section = 6
REM N6 * 6 floating point operations
GOSUB Pout
REM MODULE 7 - ARRAY REFERENCES
stime = xTime()
j = 1
k = 2
l = 3
ex[1] = 1
ex[2] = 2
ex[3] = 3
FOR ix = 1 TO ixtra
 FOR i = 1 TO n7
  GOSUB PO
 NEXT i
NEXT ix
checsum = ex[3]
rtime = xTime() - stime
smflops = n7 * 3
title$ = "N7 assigns"
atype = 2
section = 7
REM N7 * 3 assignments
GOSUB Pout
REM MODULE 8 - STANDARD FUNCTIONS
stime = xTime()
X = 0.75
FOR ix = 1 TO ixtra
 FOR i = 1 TO n8
  X = SQR(LOG(X) / t1)
 NEXT i
NEXT ix
checsum = X
rtime = xTime() - stime
smflops = n8 * 4
title$ = "N8 exp,sqrt etc."
atype = 2
section = 8
REM N8 * 4 function calls and floating point operations
GOSUB Pout
RETURN
REM END OF MAIN ROUTINE
PA:
REM PROCEDURE PA
j = 0
DO
 ex[1] = (ex[1] + ex[2] + ex[3] - ex[4]) * t
 ex[2] = (ex[1] + ex[2] - ex[3]+ ex[3]) * t
 ex[3] = (ex[1] - ex[2] + ex[3] + ex[4]) * t
 ex[4]= (-ex[1] + ex[2] + ex[3] + ex[4]) * t/2
 j = j + 1
UNTIL j = 6
RETURN
PO:
REM PROCEDURE P0
ex[j] = ex[k]
ex[k] = ex[l]
ex[l] = ex[j]
RETURN
P3:
REM PROCEDURE P3
X = Y
Y = Z
X = t * (X + Y)
Y = t1 * (X + Y)
Z = (X + Y) / t2
RETURN
Pout:
Check = Check + checsum
ztime[section] = rtime
heading$[section] = title$
mTimeUsed = mTimeUsed + rtime
IF calibrate = 1 THEN
 GR.MODIFY cal1,"text","#"
 GR.RENDER
ENDIF
IF calibrate = 0 THEN
 k1$= REPLACE$(STR$(Pass),".0","")
 k2$= REPLACE$(STR$(Passes),".0","")
 k3$= REPLACE$(heading$[section],".0","")
 k4$= REPLACE$(STR$(ztime[section]),".0","")
 GR.MODIFY tx2,"text", "Pass: "+k1$+" of "+ k2$+"  " + k3$
 GR.RENDER
ENDIF
RETURN

REM *****************************************
! SHOW SPEED RESULTS
viewResults:
! find best device
minPH=1000
minTAB=1000
bestPh=1
bestTab=1
FOR i=1 TO 6
 IF resPH[i]<minPH THEN
  bestPh=i
  minPH=resPH[i]
 ENDIF
 IF resTAB[i]<minTAB THEN
  bestTAB=i
  minTAB=resTAB[i]
 ENDIF
NEXT I

! normalize
f1= resPH[bestPh]
f2= resTAB[bestTab]
FOR i=1 TO 6
 IF i<> bestPh
  norm=f1/resPH[i] % 0..1 best
  normPh[i]=50+350*(1-norm) % starting y point of bar
 ELSE
  normPh[i]=50
 ENDIF
NEXT i
FOR i=1 TO 6
 IF i<> bestTab
  norm=f2/resTab[i] % 0..1 best
  normTab[i]=550+350*(1-norm) % starting y point of bar
 ELSE
  normTab[i]=550
 ENDIF
NEXT i


GR.COLOR 255,227,0,227,1
GR.TEXT.SIZE 37
GR.TEXT.DRAW t, 100,450, "PHONES"
GR.TEXT.DRAW t, 100,950, "TABLETS"
GR.COLOR 255,155,95,185,1
GR.TEXT.DRAW t, 100,1020, "Relative performance compared"
GR.TEXT.DRAW t, 100,1070, "to other devices (%)."
! thin lines, %
GR.TEXT.SIZE 31
FOR i=0 TO 3
 GR.COLOR 255,17,127,127,1
 GR.LINE l, 5,50+87.5*i,800, 50+87.5*i
 GR.COLOR 255,0,0,197,1
 GR.TEXT.DRAW t, 8,47+87.5*i, REPLACE$(STR$(100-(i)*25),".0","")+"%"
 GR.COLOR 255,17,127,127,1
 GR.LINE l, 5,550+87.5*i,800, 550+87.5*i
 GR.COLOR 255,0,0,197,1
 GR.TEXT.DRAW t, 8,547+87.5*i, REPLACE$(STR$(100-(i)*25),".0","")+"%"
NEXT
GR.COLOR 255,17,127,127,1
FOR i=0 TO 6
 GR.LINE l, 30+130*i, 50, 30+130*i, 400
 GR.LINE l, 30+130*i ,550, 30+130*i,900
NEXT
GR.LINE l, 799,50,799,400
GR.LINE l, 799,550,799,900


! results bars
GR.SET.STROKE 3
GR.TEXT.SIZE 28
FOR i=1 TO 6
 GR.COLOR 255,227,127,27,1
 IF i=6 THEN gr.color 255,0,227,27,1
 GR.TEXT.DRAW ssec, 55+130*(i-1), normPh[i]-5, STR$(resPh[i])
 GR.RECT r,60+130*(i-1) , normPh[i], 110+130*(i-1), 400
 GR.COLOR 255,27,197,27,0
 IF i=6 THEN gr.color 255,200,200,200,0
 GR.RECT r,60+130*(i-1) , normPh[i] , 110+130*(i-1), 403
NEXT i

! axis
GR.COLOR 255,127,127,127,1
GR.SET.STROKE 6
GR.LINE l, 5,403,800,403
GR.LINE l, 5,50,5,406

! phones names
GR.TEXT.SIZE 31
GR.COLOR 255,227,227,27,1
FOR i=1 TO 6
 GR.ROTATE.START 270, 140+130*(i-1) , 400
 GR.TEXT.DRAW t,140+130*(i-1) , 400, phones$[i] %, tablets[i]
 GR.ROTATE.END
NEXT i
GR.TEXT.SIZE 37

! results bars
GR.TEXT.SIZE 28
GR.SET.STROKE 3
FOR i=1 TO 6
 GR.COLOR 255,227,127,27,1
 IF i=6 THEN gr.color 255,0,227,27,1
 GR.TEXT.DRAW ssec, 55+130*(i-1), normTab[i]-5, STR$(resTab[i])
 GR.RECT r,60+130*(i-1) , normTab[i], 110+130*(i-1), 900
 GR.COLOR 255,27,197,27,0
 IF i=6 THEN gr.color 255,200,200,200,0
 GR.RECT r,60+130*(i-1) , normTab[i], 110+130*(i-1), 903
NEXT i
! axis
GR.SET.STROKE 6
GR.COLOR 255,127,127,127,1
GR.LINE l, 5,903,800,903
GR.LINE l, 5,550,5,906

! tablets
GR.TEXT.SIZE 31
GR.COLOR 255,227,227,27,1
FOR i=1 TO 6
 GR.ROTATE.START 270, 140+130*(i-1) , 900
 GR.TEXT.DRAW t,140+130*(i-1) , 900, tablets$[i]
 GR.ROTATE.END
NEXT i
GR.TEXT.SIZE 37
GR.RENDER
RETURN

