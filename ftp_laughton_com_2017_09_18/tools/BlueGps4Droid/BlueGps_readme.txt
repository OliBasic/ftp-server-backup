http://sourceforge.net/p/bluegps4droid/home/Home/

Bluetooth GPS for Android is an application for using an external bluetooth GPS on Android devices.
The application starts a service, then connects to a Bluetooth device (NMEA GPS) and creates a mock GPS provider which can be used to replace the internal GPS.
It's also possible to log the external GPS NMEA data in a file on the device.
It's an open source software distributed free of charge under the terms of the GNU General Public License
