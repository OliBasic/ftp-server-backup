!***  ***********************************************
!***  ***   Start of GraphicControlsTestBed.bas   ***
!***  ***********************************************
gTBVers$="v1.17"

INCLUDE GraphicConstants.bas
INCLUDE GraphicControls.bas

!***  Open graphics screen

rc=InitGraphics(bcOPAQUE,bcLGRAY,bcLANDSCAPE,bcHIDESTATUSBAR,"GCClick.ogg")
!rc=InitGraphics(bcOPAQUE,bcLGRAY,bcPORTRAIT,bcHIDESTATUSBAR,"GCClick.ogg")

!***  Attempt to use the actual screen size to auto size
!***  design size (1280 x 752) so forms fit on device screen.

CALL SetScalingFactor(1280,752,&DateSize,&FrmScale,&MsgBoxFontSize)

!***  Set ListBox Style and State plus various view variables

LET LBStyle$=bcDATBOLD$ % +bcICON$+bcLISTVIEW$
LET LBState$="Enabled"
LET BS$=bcALIGNLEFT$
LET MarkState$="Show"
LET CalColSet$="Dft"
LET optionSel$=""
LET ModCapTest=0
LET ModTxtTest=0
LET capfont1$=""
LET datfont1$=""
CALL LoadUserFont("GC-TestFont1.ttf",&capfont1$,&datfont1$)

!***  Example Form.

!#######################
!### DisplayTestForm ###
!#######################
DisplayTestForm:

LET curPath$="../../../../"

!*** Initialise New Form Data.
!*** Change bcSOUNDON$ to bcSOUNDOFF$ to suppress "click" noises.

CALL StartNewForm(DateSize,bcSOUNDON$,FrmScale,MsgBoxFontSize,bcLYELLOW)
LET bMenuLoaded=0
LET picCartman=AddControl(bcFRMPICTURE,"", ~
      bcBLUE,bcGRAY,0,0, ~
      200,400,0,200,200,20,BS$) %#### +bcNOTCLICKABLE$)
LET dspMark=AddControl(bcFRMDISPLAY,"", ~
      1,1,bcYELLOW,bcRED, ~
      200,400,0,30,30,20,bcCAPITALIC$+bcDATBOLD$+BS$)
LET lblTitle=AddControl(bcFRMLABEL,"Cartman.png", ~
      bcWHITE,1,0,0, ~
      340,400,0,200,20,15,bcALIGNCENTRE$+BS$)
LET cmdSwap=AddControl(bcFRMBUTTON,"Swap", ~
      bcYELLOW,bcBLUE,0,0, ~
      370,520,0,80,30,20,bcCAPITALIC$+bcDATBOLD$+bcROUND$+BS$)
LET chkIcon=AddControl(bcFRMCHECKBOX,"< Show Icons", ~
      bcWHITE,bcGRAY,bcBLACK,bcLCYAN, ~
      100,400,160,200,40,20,bcCAPITALIC$+bcDATBOLD$+BS$)
LET cmdMenu=AddControl(bcFRMBUTTON,"A"+bcRECBREAK$ ~
      +"Test ListBox"+bcRECBREAK$+"Test Picture"+bcRECBREAK$+"-"+bcRECBREAK$ ~
      +"¬Not Available"+bcRECBREAK$+"-"+bcRECBREAK$ ~
      +"About"+bcRECBREAK$+"Help"+bcRECBREAK$+"-"+bcRECBREAK$+"Exit", ~
      bcWHITE,bcBLACK,bcYELLOW,bcLCYAN, ~
      6,1210,200,40,50,20, ~
      bcMENUBTN$+bcMENULIST$+bcFLAT$+bcCAPITALIC$+bcDATBOLD$+BS$)
LET lblColour1=AddControl(bcFRMLABEL,"Predefined", ~
      bcBLACK,1,0,0, ~
      220,700,0,100,0,20,bcDATBOLD$+BS$)
LET lblColour2=AddControl(bcFRMLABEL,"Colours", ~
      bcBLACK,1,0,0, ~
      250,700,0,100,0,20,bcDATBOLD$+BS$)
LET lblHeading=AddControl(bcFRMLABEL,"Label", ~
      bcBLACK,1,0,0, ~
      16,20,0,580,40,40, ~
      bcALIGNCENTRE$+bcDATBOLD$+bcCAPUNDERLINE$+BS$+capfont1$)
LET lblHeadingV=AddControl(bcFRMLABEL,"Version Number: "+gTBVers$, ~
      bcBLACK,1,0,0, ~
      16,630,0,400,40,40, ~
      bcALIGNCENTRE$+bcDATBOLD$+bcCAPUNDERLINE$+BS$+capfont1$)
LET lstListBox=AddControl(bcFRMLISTBOX,"ListBox", ~
      bcWHITE,bcGRAY,bcBLACK,bcLCYAN, ~
      70,20,0,360,180,20,LBStyle$+bcQUICKNAV$+BS$+bcBANDING$)
LET ctrlOption=AddControl(bcFRMOPTBUTTON,"Option", ~
      bcWHITE,bcGRAY,bcBLACK,bcLCYAN, ~
      260,20,120,340,120,20,bcCAPITALIC$+bcDATBOLD$+BS$+bc3DBORDER$)
LET ctrlCheckBox=AddControl(bcFRMCHECKBOX,"CheckBox", ~
      bcWHITE,bcGRAY,bcBLACK,bcLCYAN, ~
      390,20,120,160,40,20,bcCAPITALIC$+bcDATBOLD$+BS$)
LET chkSound=AddControl(bcFRMCHECKBOX,"Sound", ~
      bcWHITE,bcGRAY,bcBLACK,bcLCYAN, ~
      390,200,120,160,40,20,bcCAPITALIC$+bcDATBOLD$+BS$)
LET ctrlOptionH=AddControl(bcFRMOPTBUTTON,"Option", ~
      bcBLACK,bcLGRAY,bcBLACK,bcLGRAY, ~
      294,800,100,430,100,16,bcCAPITALIC$+bcDATBOLD$+BS$)
LET dspDisplay=AddControl(bcFRMDISPLAY,"Display", ~
      bcWHITE,bcGRAY,bcGRAY,bcLGRAY, ~
      440,20,120,360,40,20, ~
      bcCAPITALIC$+bcDATBOLD$+BS$) % +bcALIGNCENTRE$)
LET ctrlString=AddControl(bcFRMSTRING,"String", ~
      bcLRED,bcGRAY,bcRED,bcWHITE, ~
      490,20,120,360,40,24, ~
      bcCAPITALIC$+bcDATBOLD$+BS$+datfont1$) % +bcALIGNCENTRE$)
LET ctrlText=AddControl(bcFRMTEXT,"Text", ~
      bcBLACK,bcLYELLOW,bcMAGENTA,bcWHITE, ~
      540,20,120,360,40,20, ~
      bcCAPITALIC$+bcDATBOLD$+BS$+bcDATABORDER$+bcALIGNRIGHT$)
LET ctrlSelect=AddControl(bcFRMSELECT,"Select", ~
      bcWHITE,bcGRAY,bcBLACK,bcLCYAN, ~
      590,20,120,360,40,20, ~
      bcALLOWNEW$+bcCAPITALIC$+bcDATBOLD$+BS$)
LET ctrlDate=AddControl(bcFRMDATE,"Date", ~
      bcWHITE,bcGRAY,bcBLACK,bcWHITE, ~
      640,20,120,300,40,20,bcCAPITALIC$+bcDATBOLD$+BS$)
LET ctrlHelp=AddControl(bcFRMBUTTON,"GC-Question.png", ~
      bcBLACK,bcLCYAN,0,0, ~
      670,780,0,60,60,0,bcCAPBOLD$+bcGRAPHIC$+bcROUND$+BS$)
LET ctrlSpinButton=AddControl(bcFRMSPINBUTTON,"SpinButton", ~
      bcBLACK,bcLYELLOW,bcBLACK,bcLCYAN, ~
      440,400,130,360,40,20,bcCAPITALIC$+bcDATBOLD$+BS$+bcDATABORDER$)
LET ctrlFrame=AddControl(bcFRMFRAME,"Frame", ~
      bcWHITE,bcGRAY,bcBLACK,bcLGRAY, ~
      490,400,0,360,190,20,bcCAPITALIC$+bcDATBOLD$+BS$)
LET ctrlFString1=AddControl(bcFRMSTRING,"FString1", ~
      bcWHITE,bcGRAY,bcBLACK,bcWHITE, ~
      530,420,120,320,40,20,bcCAPITALIC$+bcDATBOLD$+BS$)
LET ctrlFString2=AddControl(bcFRMSTRING,"FString2", ~
      bcWHITE,bcGRAY,bcBLACK,bcWHITE, ~
      580,420,120,320,40,20, ~
      bcCAPITALIC$+bcDATBOLD$+BS$+bcDISABLED$)
LET ctrlFString3=AddControl(bcFRMSTRING,"FString3", ~
      bcWHITE,bcGRAY,bcBLACK,bcWHITE, ~
      630,420,120,320,40,20, ~
      bcCAPITALIC$+bcDATBOLD$+BS$+bcHIDE$)
LET shpRect=AddControl(bcFRMSHAPE,"Rect", ~
      bcBLACK,bcYELLOW,0,0, ~
      140,830,0,220,140,0,bcSHAPEFILL$+BS$)
LET shpRectR=AddControl(bcFRMSHAPE,"rndRect", ~
      bcBLACK,bcBLUE,0,0, ~
      150,840,0,200,120,0,bcSHAPEFILL$+BS$)
LET shpOval=AddControl(bcFRMSHAPE,"Oval", ~
      bcBLACK,bcYELLOW,0,0, ~
      150,840,0,200,120,0,bcSHAPEFILL$+BS$)
LET shpCirc=AddControl(bcFRMSHAPE,"Circle", ~
      bcBLACK,bcRED,0,0, ~
      150,840,0,200,120,0,bcSHAPEFILL$+BS$)
LET shpLineV=AddControl(bcFRMSHAPE,"Line", ~
      bcWHITE,bcWHITE,0,0, ~
      150,940,0,0,120,10,BS$)
LET shpLineH=AddControl(bcFRMSHAPE,"Line", ~
      bcWHITE,bcWHITE,0,0, ~
      210,840,0,200,0,1,BS$)
LET lblColour3=AddControl(bcFRMLABEL,"Shapes", ~
      bcBLACK,1,0,0, ~
      200,1050,0,100,0,20,bcDATBOLD$+BS$)
LET ctrlButtonList=AddControl(bcFRMBUTTON,"List"+bcRECBREAK$+"This is a sample"+bcRECBREAK$ ~
      +"drop down list"+bcRECBREAK$+"invoked from a"+bcRECBREAK$+"button with the" ~
      +bcRECBREAK$+"bcMENULIST style.", ~
      bcBLACK,bcLYELLOW,bcBLACK,bcCYAN, ~
      170,1150,300,60,50,20,bcCAPBOLD$+bcROUND$+BS$+bcMENULIST$)
LET ctrlButton=AddControl(bcFRMBUTTON,"Close", ~
      bcBLACK,bcLYELLOW,0,0, ~
      690,20,0,120,40,20,bcCAPBOLD$+bcROUND$+BS$)
LET cmdModCapTest=AddControl(bcFRMBUTTON,"ModCapTest", ~
      bcLRED,bcRED,0,0, ~
      690,420,0,140,40,20,bcCAPBOLD$+bcFLAT$+BS$)
LET cmdModDatTest=AddControl(bcFRMBUTTON,"ModDatTest", ~
      bcLRED,bcRED,0,0, ~
      690,600,0,140,40,20, ~
      bcCAPBOLD$+bcDISABLED$+bcFLAT$+bcBEVEL$+BS$)
LET ctrlComboBox1=AddControl(bcFRMCOMBOBOX,"ComboBox1", ~
      bcWHITE,bcGRAY,bcBLACK,bcLCYAN, ~
      70,840,130,410,40,20, ~
      bcCAPITALIC$+bcDATBOLD$+bcALLOWNEW$+bcQUICKNAV$+BS$)
LET ctrlComboBox2=AddControl(bcFRMCOMBOBOX,"ComboBox2", ~
      bcWHITE,bcGRAY,bcBLACK,bcLCYAN, ~
      420,800,130,410,40,20, ~
      bcCAPITALIC$+bcDATBOLD$+bcQUICKNAV$+BS$)
LET ctrlTime=AddControl(bcFRMTIME,"Time (h:m:s)", ~
      bcBLACK,bcLYELLOW,bcBLACK,bcLCYAN, ~
      480,800,130,140,40,20, ~
      bcCAPITALIC$+bcDATBOLD$+BS$+bcDATABORDER$)
LET ctrlTime2=AddControl(bcFRMTIME,"Time (h:m)", ~
      bcWHITE,bcGRAY,bcBLACK,bcLCYAN, ~
      540,800,130,140,40,20, ~
      bcCAPITALIC$+bcDATBOLD$+bcHHMMONLY$+BS$)
LET ctrlChkButton=AddControl(bcFRMCHKBUTTON,"Check Button"+bcRECBREAK$ ~
      +"Apple"+bcRECBREAK$+"Banana"+bcRECBREAK$+"Grape", ~
      bcBLACK,bcLYELLOW,bcBLACK,bcLCYAN, ~
      600,800,130,450,40,20, ~
      bcCAPITALIC$+bcDATBOLD$+BS$+bcDATABORDER$+bcALIGNRIGHT$)
LET cmdTogBord=AddControl(bcFRMBUTTON,"Toggle Borders", ~
      bcLMAGENTA,bcMAGENTA,0,0, ~
      690,860,0,160,40,20,bcCAPBOLD$+bcBEVEL$+BS$)
LET cmdTogCaln=AddControl(bcFRMBUTTON,"Toggle Cal.Colours", ~
      bcLCYAN,bcCYAN,0,0, ~
      690,1040,0,190,40,20,bcCAPBOLD$+bcROUND$+BS$)

!***  Load Select,Listbox and OptionButton label value
!***  strings (they could have been defined in above
!***  AddControl commands but for readability they have
!***  been defined here seperately).
!***  As mentioned earlier format is the label text
!***  followed by the value list.
!***  Two consecutive bcRECBREAK$ characters in the value
!***  section will indicate a blank value.

GOSUB LoadControlValueStrings

CALL SetCtrlCap(ctrlSelect,"Select"+bcRECBREAK$+SelVals$)
CALL SetCtrlCap(ctrlOption,"Options"+bcRECBREAK$+"Toggle CheckBox"+bcRECBREAK$+"Toggle FileDialog" ~
     +bcRECBREAK$ +"Toggle Listview")
CALL SetCtrlCap(ctrlOptionH,"Options:"+bcRECBREAK$+"Show Wait" ~
     +bcRECBREAK$+"Show/Hide Mark"+bcRECBREAK$+"OKOnly" ~
     +bcRECBREAK$+"YesNo"+bcRECBREAK$+"YesNoCancel" ~
     +bcRECBREAK$+"10 lines-No Icon")
CALL SetCtrlCap(ctrlSpinButton,"SpinButton"+bcRECBREAK$+"1" ~
     +bcRECBREAK$+"10"+bcRECBREAK$+"1"+bcRECBREAK$+"100")
CALL SetCtrlCap(ctrlComboBox1,"ComboBox1"+bcRECBREAK$+CBox1Vals$)
CALL SetCtrlCap(ctrlComboBox2,"ComboBox2"+bcRECBREAK$+CBox2Vals$)

!***  Load required Data Field Values (eg: Loading values
!***  retrieved via an SQL call or by splitting a File
!***  Line(s)/record(s))

CALL SetCtrlData(dspMark,"N")
CALL SetCtrlData(dspDisplay,"Test Harness")
CALL SetCtrlData(ctrlString,"Any Old Text...")
CALL SetCtrlData(ctrlText,"Rubbish Text"+bcStrCRLF$ ~
     +"sgsgsg fhfhfh gagag fjffk skskssk" ~
     +bcStrCRLF$+"dkfkffk ddkfkkf ffkfkfk" ~
     +bcStrCRLF$+bcStrCRLF$+"dhddjdjjdj shshshsh")
CALL SetCtrlData(ctrlSelect,"Kiwi")
CALL SetCtrlData(ctrlDate,"2011-08-21-Sun")
CALL SetCtrlData(ctrlButton,"Close")
CALL SetCtrlData(picCartman,"Cartman.png")
CALL SetCtrlCap(lblHeading,"Graphic Controls Test Harness")
CALL SetCtrlData(ctrlButton,"N")
CALL SetCtrlData(ctrlOption,optionSel$)
CALL SetCtrlData(ctrlOptionH,"Show/Hide Mark")
CALL SetCtrlData(ctrlSpinButton,"1")
CALL SetCtrlData(ctrlFString1,"Frame String 1")
CALL SetCtrlData(ctrlFString2,"Frame String 2")
CALL SetCtrlData(ctrlCheckBox,"N")
CALL SetCtrlData(chkSound,"Y")
CALL SetCtrlData(ctrlComboBox1,"Grape")
CALL SetCtrlData(ctrlComboBox2,"Kiwi2")
CALL SetCtrlData(ctrlChkButton,"Banana")

!***  Set ListBox contents depending on current style.
!***  Long "LapTop" entry is to test the Clip action.

IF IS_IN(bcFILEDIALOG$,LBStyle$)=0 THEN
 IF IS_IN(bcLISTVIEW$,LBStyle$)<>0 THEN
  IF IS_IN(bcICON$,LBStyle$)<>0 THEN
   CALL SetCtrlCap(lstListBox,LstBoxLvwIconVals$)
  ELSE
   CALL SetCtrlCap(lstListBox,LstBoxLvwVals$)
  ENDIF
  CALL SetCtrlData(lstListBox,"")
 ELSE
  IF IS_IN(bcICON$,LBStyle$)<>0 THEN
   CALL SetCtrlCap(lstListBox,"ListBox"+bcRECBREAK$+LstBoxIconVals$)
   CALL SetCtrlData(lstListBox,"Mouse")
  ELSE
   CALL SetCtrlCap(lstListBox,"ListBox"+bcRECBREAK$+LstBoxVals$)
   CALL SetCtrlData(lstListBox,"Disk")
  ENDIF
 ENDIF
ENDIF
IF IS_IN(bcICON$,LBStyle$)=0 THEN
 CALL SetCtrlData(chkIcon,"N")
ELSE
 CALL SetCtrlData(chkIcon,"Y")
ENDIF

!***  Finally Draw Form - routine starts with a gr.CLS to
!***  clear any previous form/bitmap data.

CALL DrawForm("Loading Test Form...",picPath$)

!***  Draw Colour Pallette (no function - just displays the
!***  various colours defined within Graphic Controls include).

LET x=630/FrmScale  % --.
LET ys=90/FrmScale  %   |-- Used to draw colour samples.
LET s=34/FrmScale   % --'
LET y=ys
FOR i=1 TO 16
 CALL SetColor(bcOPAQUE,bcRGBCOLOR[i],bcFILL)
 GR.RECT GONum,x,y,x+s,y+s
 CALL SetColor(bcOPAQUE,bcBLACK,bcNOFILL)
 GR.RECT GONum,x,y,x+s,y+s
 IF i=8 THEN
  LET x=x+s
  LET y=ys
 ELSE
  LET y=y+s
 ENDIF
NEXT i
GR.RENDER

!***  The Graphic Controls package knows nothing about the above
!***  Colour Pallette graphic objects, as they were drawn using
!***  direct graphic commands. This means that the Graphic Controls
!***  copy of all the graphic objects currently in use needs to be
!***  updated to keep things in line. This is done by updating the
!***  two variables 'frmobjlst' (list of graphic objects) and
!***  'FrmObjCnt' (count of graphic objects). If this is not done
!***  the pallette graphic objects will be included as part of the
!***  next drawn control (e.g.: Calendar, MsgBox$ as they are only
!***  drawn when required). If pallette objects are included as part
!***  of a later control the only effect would be the pallette will
!***  appear/disappear along with the control they have become part of.

BUNDLE.GET 1,"FrmObjCnt",i
BUNDLE.GET 1,"frmobjlst",frmobjlst
LIST.CLEAR frmobjlst
UNDIM i[]
GR.GETDL i[],1
LIST.ADD.ARRAY frmobjlst,i[]
BUNDLE.PUT 1,"frmobjlst",frmobjlst
ARRAY.LENGTH i,i[]
BUNDLE.PUT 1,"FrmObjCnt",i

FileListReload:

!***  Following code is only used to reload Listbox when it has
!***  the style 'bcFILEDIALOG$'.

IF IS_IN(bcFILEDIALOG$,LBStyle$) > 0 THEN
 CALL ModCtrlData(chkIcon,"N",0)
 CALL DisableCtrl(chkIcon,0)
 UNDIM dfile$[]
 DIR curPath$,dfile$[]
 ARRAY.LENGTH ActFileCnt,dfile$[]

 !***  Only show first 200 entries otherwise it can take quite
 !***  a while to load data.

 IF ActFileCnt>200 THEN LET DispFileCnt=200 ELSE LET DispFileCnt=ActFileCnt
 UNDIM dfl$[]
 DIM dfl$[DispFileCnt]
 FOR i=1 TO DispFileCnt
  LET dfl$[i]=dfile$[i]
 NEXT i
 IF LEN(curPath$)=12 THEN
  LET sDotDot$=""
 ELSE
  LET sDotDot$="..  <Goto Parent Folder>"
  IF DispFileCnt>0 & dfl$[1]<> " " THEN LET sDotDot$=sDotDot$+bcRECBREAK$
 ENDIF
 LET sTemp$="Folder: "+MID$(curPath$,12)
 LET sTemp$=sTemp$+bcRECBREAK$+sDotDot$+join$(dfl$[],bcRECBREAK$)
 UNDIM dfl$[]
 CALL SetCtrlData(lstListBox,"")
 CALL ModCtrlCap(lstListBox,sTemp$,1)
 GR.RENDER
ELSE
 CALL EnableCtrl(chkIcon,2)
ENDIF

!***  Main Loop processing "Touches".
!***  GraphicControls.bas will handle all control processing
!***  and then return here so any additional control
!***  action can be taken. On return from TouchCheck,the
!***  selCtrl variable will contain the number of the control
!***  just processed and the function GetCtrlData$ can be
!***  called at any time to return the updated data value
!***  (if relevant).

!*** Processing Notes:
!***  * If no other action involved the code below just
!***    reports which control was touched and the current
!***    value of the controls data (if relevant).
!***  * Pressing the Control Button causes the program to end.
!***  * Pressing one of the Option Buttons labelled "Disable"
!***    or "Enable" will cause the ListBox and Control Button
!***    to be disabled/enabled.
!***  * Pressing the Option Button labelled "FileDialog" will
!***    cause the ListBox to toggle between Normal and
!***    FileDialog styles.
!***  * Pressing the CheckBox causes the program to
!***    disable/enable the Frame control.
!***  * Pressing the Option Buttons "Show Wait" and "Hide Wait"
!***    toggles display of the HourGlass to dsiplay.
!***  * Pressing one of the Option Buttons labelled "OKOnly",
!***    "YesNo" or "YesNoCancel" will cause a MessageBox to
!***    be displayed.

LET FirstCtrl=picCartman
LET LastCtrl=cmdTogCaln

LET bSWExit=0
WHILE bSWExit=0
 LET selCtrl=TouchCheck(Period,FirstCtrl,LastCtrl)

!***  Check which control was touched and process as required.

 SW.BEGIN selCtrl
   SW.CASE ctrlButton
    POPUP "Close Button Clicked",0,0,0
    PAUSE 2000
    LET bSWExit=1 % goto CloseTest 
    SW.BREAK
   SW.CASE dspDisplay
    POPUP "Display Field clicked"+ sTemp$,0,0,0
    SW.BREAK
   SW.CASE ctrlString
    LET sTemp$=GetCtrlData$(ctrlString)
    POPUP "String="+ sTemp$,0,0,0
    SW.BREAK
   SW.CASE ctrlText
    LET sTemp$=GetCtrlData$(ctrlText)
    LET sTemp$=REPLACE$(sTemp$,bcStrCRLF$,bcRECBREAK$)
    LET Button$=MsgBox$(sTemp$,bcOKONLY$,"Text Field Clicked (and possibly changed)")
    SW.BREAK
   SW.CASE ctrlSelect
    LET sTemp$=GetCtrlData$(ctrlSelect)
    LET sCap$=GetCtrlCap$(ctrlSelect)
    LET sCap$=REPLACE$(sCap$,bcRECBREAK$,bcCRLF$)
    POPUP "Select="+sTemp$+bcCRLF$+bcCRLF$+sCap$,0,0,1
    SW.BREAK
   SW.CASE ctrlDate
    LET sTemp$=GetCtrlData$(ctrlDate)
    POPUP "Date="+ sTemp$,0,0,0
    SW.BREAK
   SW.CASE ctrlTime
    LET sTemp$=GetCtrlData$(ctrlTime)
    POPUP "Time="+ sTemp$,0,0,0
    SW.BREAK
   SW.CASE ctrlTime2
    LET sTemp$=GetCtrlData$(ctrlTime2)
    POPUP "Time="+ sTemp$,0,0,0
    SW.BREAK
   SW.CASE ctrlButtonList
    LET sTemp$=GetCtrlData$(ctrlButtonList)
    POPUP "ButtonList Entry="+ sTemp$,0,0,0
    SW.BREAK
   SW.CASE ctrlComboBox1
    LET sTemp$=GetCtrlData$(ctrlComboBox1)
    LET sCap$=GetCtrlCap$(ctrlComboBox1)
    LET sCap$=REPLACE$(sCap$,bcRECBREAK$,bcCRLF$)
    POPUP "Select ComboBox1="+ sTemp$+bcCRLF$+bcCRLF$+sCap$,0,0,0
    SW.BREAK
   SW.CASE ctrlComboBox2
    LET sTemp$=GetCtrlData$(ctrlComboBox2)
    POPUP "ComboBox2="+ sTemp$,0,0,0
    SW.BREAK
   SW.CASE ctrlChkButton
    LET sTemp$=GetCtrlData$(ctrlChkButton)
    POPUP "ChkButton="+ sTemp$,0,0,0
    SW.BREAK
   SW.CASE picCartman
    POPUP "Picture Clicked",0,0,0
    SW.BREAK
   SW.CASE cmdSwap
    LET sTemp$=GetCtrlData$(picCartman)
    IF sTemp$="Cartman.png" THEN LET sTemp$="GC-Information.png" ELSE LET sTemp$="Cartman.png"
    CALL ModCtrlCap(lblTitle,sTemp$,0)
    CALL ModCtrlData(picCartman,sTemp$,1)
    SW.BREAK
   SW.CASE lstListBox
    LET sTemp$=GetCtrlData$(lstListBox)
    IF IS_IN(bcFILEDIALOG$,LBStyle$) > 0 THEN
     IF LEFT$(sTemp$,2)=".." THEN
      LET curPath$=LEFT$(curPath$,LEN(curPath$)-1)
      WHILE RIGHT$(curPath$,1)<>"/"
       LET curPath$=LEFT$(curPath$,LEN(curPath$)-1)
      REPEAT
      LET bSWExit=2  % goto FileListReload
     ELSEIF LEN(sTemp$)>3 then
      IF RIGHT$(sTemp$,3)="(d)" THEN
       LET sTemp$=LEFT$(sTemp$,LEN(sTemp$)-3)
       LET curPath$=curPath$+sTemp$+"/"
       LET bSWExit=2  % goto FileListReload
      ENDIF
     ENDIF
    ENDIF
    IF bSWExit=0 THEN
     LET sCap$=GetCtrlCap$(lstListBox)
     LET sCap$=REPLACE$(sCap$,bcRECBREAK$,bcCRLF$)
     POPUP "Listbox="+sTemp$+bcCRLF$+bcCRLF$+sCap$,0,0,1
    ENDIF
    SW.BREAK
   SW.CASE lstListBox+3000
    LET sTemp$=GetCtrlData$(lstListBox)
    POPUP "Checkbox Value Changed"+bcCRLF$+bcCRLF$+"Listbox Row="+sTemp$,0,0,1
    SW.BREAK
   SW.CASE ctrlCheckBox
    LET sTemp$=GetCtrlData$(ctrlCheckBox)
    IF sTemp$="Y" THEN
     CALL DisableCtrl(ctrlFrame,1)
    ELSE
     CALL EnableCtrl(ctrlFrame,1)
    ENDIF
    POPUP "CheckBox="+ sTemp$,0,0,0
    SW.BREAK
   SW.CASE chkSound
    LET sTemp$=GetCtrlData$(chkSound)
    CALL SetSound(sTemp$)
    SW.BREAK
   SW.CASE chkIcon
    LET sTemp$=GetCtrlData$(chkIcon)
    IF sTemp$="Y" THEN
     IF IS_IN(bcICON$,LBStyle$)=0 THEN
      LET LBStyle$=LBStyle$+bcICON$
     ENDIF
     IF IS_IN(bcFILEDIALOG$,LBStyle$)=0 THEN
      LET LBStyle$=REPLACE$(LBStyle$,bcFILEDIALOG$,"")
      LET optionSel$=""
     ENDIF
    ELSE
     LET LBStyle$=REPLACE$(LBStyle$,bcICON$,"")
    ENDIF
    LET bSWExit=3 % goto DisplayTestForm
    SW.BREAK
   SW.CASE ctrlOption
    GOSUB SetListBoxStyle
    LET bSWExit=3 % goto DisplayTestForm
    SW.BREAK
   SW.CASE cmdTogBord
    IF IS_IN(bcNOBORDER$,BS$)=0 THEN LET BS$=BS$+bcNOBORDER$ ELSE LET BS$=REPLACE$(BS$,bcNOBORDER$,"")
    LET bSWExit=3 % goto DisplayTestForm
    SW.BREAK
   SW.CASE cmdTogCaln
    IF CalColSet$="Dft" THEN
     LET CalColSet$="New"
     CALL SetCalColours(bcLRED,bcRED,bcLRED,bcRED,bcLRED, ~
        bcRED,bcLRED,bcLRED,bcRED,bcRED,bcWHITE,bcRED, ~
        bcLRED)
    ELSE
     CalColSet$="Dft"
     CALL SetCalColours(bcCYAN,bcLBLUE,bcWHITE,bcBLACK,bcLMAGENTA,bcMAGENTA,bcLGREEN,bcWHITE, ~
          bcCYAN,bcBLACK,bcYELLOW,bcBLACK,bcLBLUE)
    ENDIF
    LET bSWExit=3 % goto DisplayTestForm
    SW.BREAK
   SW.CASE ctrlOptionH
    GOSUB DisplayOptionButtonDetails
    SW.BREAK
   SW.CASE ctrlSpinButton
    LET sTemp$=GetCtrlData$(ctrlSpinButton)
    POPUP "SpinButton="+ sTemp$,0,0,0
    SW.BREAK
   SW.CASE ctrlFString1
     LET sTemp$=GetCtrlData$(ctrlFString1)
     POPUP "FString1="+ sTemp$,0,0,0
     SW.BREAK
   SW.CASE ctrlFString2
     LET sTemp$=GetCtrlData$(ctrlFString2)
     POPUP "FString2="+ sTemp$,0,0,0
     SW.BREAK
   SW.CASE cmdModCapTest
     GOSUB SetCaptionColours
     SW.BREAK
   SW.CASE cmdModDatTest
     GOSUB SetDataColours
     SW.BREAK
   SW.CASE ctrlHelp
     Button$=MsgBox$(HelpString$,bcOKONLY$+bcQUESTION$+bcNOBORDER$,"Graphic Controls TestBed Help")
     SW.BREAK
   SW.CASE 1082
     LET i$=GetCtrlData$(cmdMenu)
     IF i$="Test ListBox" THEN
      LET bSWExit=4 % goto DisplayTLBForm
     ELSEIF i$="Test Picture" THEN
      LET bSWExit=5 % goto DisplayPICForm
     ELSEIF i$="About" THEN
      LET i$=getGCVer$()
      CALL MsgBox$("Graphic Controls Test Bed "+gTBVers$+bcRECBREAK$+bcRECBREAK$ ~
           +"(c) Jeff Kerr - 2012"+bcRECBREAK$+bcRECBREAK$ ~
           +"Sample program using the GraphicControls.bas"+bcRECBREAK$ ~
           +"include. As usual send bug reports and/or"+bcRECBREAK$ ~
           +"suggestions to author via Basic! forum."+bcRECBREAK$ ~
           +bcRECBREAK$ ~
           +"Running BASIC! Version "+ VERSION$()+bcRECBREAK$ ~
           +"with Graphic Controls Version "+i$, ~
           bcOKONLY$+bcINFORMATION$,"About")
     ELSEIF i$="Help" THEN
      LET Button$=MsgBox$(HelpString$,bcOKONLY$+bcQUESTION$+bcNOBORDER$, ~
         "Graphic Controls TestBed Help")
     ELSEIF i$="Exit" THEN
      POPUP "Menu Exit Button Clicked",0,0,0
      PAUSE 2000
      LET bSWExit=1 % goto CloseTest
     ENDIF
     SW.BREAK
   SW.CASE 1084
     POPUP "Find Key Pressed.",0,0,0
     SW.BREAK
   SW.CASE 1024
     POPUP "Volume Up Key Pressed.",0,0,0
     SW.BREAK
   SW.CASE 1025
     POPUP "Volume Down Key Pressed.",0,0,0
     SW.BREAK
 SW.END
REPEAT
GOTO bSWExit,CloseTest,FileListReload,DisplayTestForm,DisplayTLBForm,DisplayPICForm

!######################
!### DisplayTLBForm ###
!######################
DisplayTLBForm:

!***  Load Listbox Testing Form.

CALL StartNewForm(DateSize,bcSOUNDON$,FrmScale,MsgBoxFontSize,bcLBLUE)
LET w=(1280-38)/2 % Calculate Listbox widths
LET ctrlTBLLabel=AddControl(bcFRMLABEL,"List Box Test Panel", ~
      bcBLACK,1,0,0, ~
      6,20,0,1240,40,30, ~
      bcALIGNLEFT$+bcDATBOLD$+bcCAPUNDERLINE$+bcCAPITALIC$+BS$)
LET ctrlTBLCSRT=AddControl(bcFRMLABEL,"Click a Column Heading to sort by that Column.", ~
      bcGREEN,1,0,0, ~
      20,w+60,0,w,40,20, ~
      bcDATBOLD$+bcCAPITALIC$+BS$)
LET ctrlTBLMenu=AddControl(bcFRMBUTTON,"",bcGRAY,bcBLACK,0,0, ~
      6,1236,0,40,40,20, ~
      bcMenuBtn$+bcFLAT$+bcCAPITALIC$+bcDATBOLD$+bcBEVEL$+BS$)
LET ctrlTBLListBox=AddControl(bcFRMLISTBOX,"ListBox", ~
      bcBLACK,bcLGREEN,bcBLACK,bcLCYAN, ~
      62,10,0,w,680,20, ~
      bcCAPBOLD$+bc3DBORDER$+bcMULTILINE$+bcHIDEGRID$+bcEDITABLE$)
LET ctrlStdListBox=AddControl(bcFRMLISTBOX,"ListBox", ~
      bcWHITE,bcGRAY,bcBLACK,bcLCYAN, ~
      58,w+32,0,w,480,20, ~
      bcCAPBOLD$+bcDATBOLD$+bcLISTVIEW$+bcCHECKBOX$+bcMULTSEL$+bcQUICKNAV$+bcCOLSORT$ ~
      +bcCAPITALIC$+bcBANDING$)
LET ctrlLBTestOpt=AddControl(bcFRMOPTBUTTON,"Option", ~
      bcWHITE,bcGRAY,bcBLACK,bcLCYAN, ~
      544,w+32,130,w,200,20,bcCAPITALIC$+bcDATBOLD$+BS$)

!***  Define a vertical two button menu

LET bwidth=140
LET bheight=80
LET x=(1280-bwidth-4)/2
LET bleft=x+2
LET btop=752-(2*bheight)-(2*2)
LET fraTBLMenu=AddControl(bcFRMFRAME,"", ~
      bcBLACK,bcGRAY,bcBLACK,bcGRAY, ~
      btop-2,x,0,bwidth+4,(2*bheight)+(2*2),20, ~
      bcCAPBOLD$+bcHIDE$+bcFADEBACK$)
LET cmdMnuTBLHelp=AddControl(bcFRMBUTTON,"Help", ~
      bcYELLOW,bcBLACK,0,0, ~
      btop,bleft,0,bwidth,bheight,20, ~
      bcFLAT$+bcCAPBOLD$)
LET cmdMnuTBLExit=AddControl(bcFRMBUTTON,"Exit", ~
      bcLGREEN,bcBLACK,0,0, ~
      btop+bheight+2,bleft,0,bwidth,bheight,20, ~
      bcFLAT$+bcCAPBOLD$)
CALL SetCtrlCap(ctrlTBLListBox, ~
      "Multiline List Box (Click Text area to View/Edit Source)" ~
      +bcRECBREAK$+LstBoxTextVal$)
CALL SetCtrlCap(ctrlLBTestOpt,"Tests"+bcRECBREAK$ ~
      +"Get Row Count"+bcRECBREAK$ ~
      +"Get Selected Index"+bcRECBREAK$ ~
      +"Get Row Data"+bcRECBREAK$ ~
      +"Get Column Clicked"+bcRECBREAK$ ~
      +"Get Checked State"+bcRECBREAK$ ~
      +"Get Checked Count"+bcRECBREAK$ ~
      +"Add New Row"+bcRECBREAK$ ~
      +"Delete Selected Row"+bcRECBREAK$ ~
      +"Update Selected Row")

!***  Generate some Listbox Test Data and load it.

LET w=(w-70)/4
LET ColumnDefs$="Column-1"+bcFLDBREAK$+STR$(w)+bcFLDBREAK$+"1"+bcCOLBREAK$ ~
               +"Column-2"+bcFLDBREAK$+STR$(w)+bcFLDBREAK$+"1"+bcCOLBREAK$ ~
               +"Column-3"+bcFLDBREAK$+STR$(w)+bcFLDBREAK$+"1"+bcCOLBREAK$ ~
               +"Column-4"+bcFLDBREAK$+STR$(w)+bcFLDBREAK$+"1"
LET ColumnVals$=""
LET j=64
LET k=ASCII("Z")+1
FOR r=1 TO 20
 LET r$=NumToStr$(r,0,0,2)
 LET j=j+1
 LET k=k-1
 LET ColumnVals$=ColumnVals$+CHR$(j)+"-"
 FOR c=1 TO 4
  LET c$=NumToStr$(c,0,0,2)
  IF c=1 THEN LET ColumnVals$=ColumnVals$+"Cell ("+r$+","+c$+")" ~
    ELSE LET ColumnVals$=ColumnVals$+CHR$(k)+"ell ("+r$+","+c$+")"
  IF c<4 THEN LET ColumnVals$=ColumnVals$+bcCOLBREAK$
 NEXT c
 IF r<20 THEN LET ColumnVals$=ColumnVals$+bcRECBREAK$
NEXT r
CALL SetCtrlCap(ctrlSTDListBox, ~
     "Listview Listbox (Click this area to activate Quicknav)" ~
     +bcCOLBREAK$+ColumnDefs$ ~
     +bcRECBREAK$+ColumnVals$)
LET rowid=65
CALL DrawForm("Loading Listbox Test Form...",picPath$)
LET FirstCtrl=ctrlTBLMenu
LET LastCtrl=ctrlLBTestOpt

!***  Processing Loop for Listbox Testing Form
LET bSWExit=0
WHILE bSWExit=0
 LET selCtrl=TouchCheck(Period,FirstCtrl,LastCtrl)
 SW.BEGIN selCtrl
  SW.CASE ctrlTBLListBox
   LET sTemp$=GetCtrlCap$(ctrlTBLListBox)
   LET sTemp$=REPLACE$(sTemp$,bcRECBREAK$,bcCRLF$)
   POPUP "MultiLine ListBox:"+bcCRLF$+bcCRLF$+sTemp$,0,0,0
   SW.BREAK
  SW.CASE ctrlStdListBox
   LET sTemp$=GetCtrlCap$(ctrlStdListBox)
   LET sTemp$=REPLACE$(sTemp$,bcRECBREAK$,bcCRLF$)
   POPUP "Test ListBox:"+bcCRLF$+bcCRLF$+sTemp$,0,0,0
   SW.BREAK
  SW.CASE ctrlLBTestOpt
   LET sTemp$=GetCtrlData$(ctrlLBTestOpt)
   IF sTemp$="Get Row Count" THEN
    LET i=GetRowCount(ctrlStdListBox)
    POPUP "Test: "+sTemp$+" - Count="+STR$(i),0,0,0
   ELSEIF sTemp$="Get Selected Index" THEN
    LET i=GetSelectedIdx(ctrlStdListBox)
    IF i=0 THEN
     POPUP "Test: "+sTemp$+" - No Selected Row - rc="+STR$(i),0,0,0
    ELSE
     POPUP "Test: "+sTemp$+" - Index="+STR$(i),0,0,0
    ENDIF
   ELSEIF sTemp$="Get Row Data" THEN
    LET i=GetSelectedIdx(ctrlStdListBox)
    IF i=0 THEN
     POPUP "Test: "+sTemp$+" - No Selected Row - rc="+STR$(i),0,0,0
    ELSE
     LET i$=GetRowData$(ctrlStdListBox,i)
     POPUP "Test: "+sTemp$+" - Data='"+i$+"'",0,0,0
    ENDIF
   ELSEIF sTemp$="Get Column Clicked" THEN
    LET i=GetSelectedIdx(ctrlStdListBox)
    IF i=0 THEN
     POPUP "Test: "+sTemp$+" - No Selected Row - rc="+STR$(i),0,0,0
    ELSE
     LET i=GetColClicked(ctrlStdListBox)
     POPUP "Test: "+sTemp$+" - Col="+STR$(i),0,0,0
    ENDIF
   ELSEIF sTemp$="Get Checked State" THEN
    LET i=GetSelectedIdx(ctrlStdListBox)
    IF i=0 THEN
     POPUP "Test: "+sTemp$+" - No Selected Row - rc="+STR$(i),0,0,0
    ELSE
     LET i=GetCheckedState(ctrlStdListBox,i)
     POPUP "Test: "+sTemp$+" - State="+STR$(i),0,0,0
    ENDIF
   ELSEIF sTemp$="Get Checked Count" THEN
    LET i=GetCheckedCount(ctrlStdListBox)
    POPUP "Test: "+sTemp$+" - Count="+STR$(i),0,0,0
   ELSEIF sTemp$="Add New Row" THEN
    LET i=GetSelectedIdx(ctrlStdListBox)
    IF i=0 THEN
     POPUP "Test: "+sTemp$+" - No Selected Row to Add row before",0,0,0
    ELSE
     LET i$=CHR$(rowid)+CHR$(rowid)+CHR$(rowid)
     LET rowid=rowid+1
     LET i$="Added "+i$+bccolbreak$+i$+bccolbreak$+i$+bccolbreak$+i$
     CALL AddListRow(ctrlStdListBox,i,i$,1)
     POPUP "Test: "+sTemp$+" Row "+STR$(i)+" Added",0,0,0
    ENDIF
   ELSEIF sTemp$="Delete Selected Row" THEN
    LET i=GetSelectedIdx(ctrlStdListBox)
    IF i=0 THEN
     POPUP "Test: "+sTemp$+" - No Selected Row to Delete",0,0,0
    ELSE
     CALL DeleteListRow(ctrlStdListBox,i,1)
     POPUP "Test: "+sTemp$+" Row "+STR$(i)+" Deleted",0,0,0
    ENDIF
   ELSEIF sTemp$="Update Selected Row" THEN
    LET i=GetSelectedIdx(ctrlStdListBox)
    IF i=0 THEN
     POPUP "Test: "+sTemp$+" - No Selected Row to Update",0,0,0
    ELSE
     LET i$=CHR$(rowid)+CHR$(rowid)+CHR$(rowid)
     LET rowid=rowid+1
     LET i$="Updated "+i$+bccolbreak$+i$+bccolbreak$+i$+bccolbreak$+i$
     CALL UpdateListRow(ctrlStdListBox,i,i$,1)
     POPUP "Test: "+sTemp$+" Row "+STR$(i)+" Updated",0,0,0
    ENDIF
   ENDIF
   SW.BREAK
  SW.CASE 1082
   POPUP "Menu",0,0,0
   LET FirstCtrl=cmdMnuTBLHelp
   LET LastCtrl=cmdMnuTBLExit
   CALL ShowCtrl(fraTBLMenu,1)
   CALL ShowCtrl(cmdMnuTBLHelp,0)
   CALL ShowCtrl(cmdMnuTBLExit,1)
   SW.BREAK
  SW.CASE cmdMnuTBLHelp
   LET FirstCtrl=ctrlTBLMenu
   LET LastCtrl=ctrlLBTestOpt
   CALL HideCtrl(fraTBLMenu,1)
   LET Button$=MsgBox$(HelpString$,bcOKONLY$+bcQUESTION$+bcNOBORDER$,"Graphic Controls TestBed Help")
   SW.BREAK
  SW.CASE cmdMnuTBLExit
   bSWExit=1 % goto DisplayTestForm
   SW.BREAK
 SW.END
REPEAT
GOTO DisplayTestForm

!######################
!### DisplayPICForm ###
!######################
DisplayPICForm:

!***  Load Picture Testing Form Data.
!***  Generate 3 test pictures from 'cartman.png' picture supplied with Basic! package

FILE.EXISTS i,"GCTestPicS.png"
IF i<>1 THEN
 GR.BITMAP.LOAD bm_obj,"cartman.png"
 GR.BITMAP.SIZE bm_obj,PicWidth,PicHeight
 GR.BITMAP.SCALE ScalePtr,bm_obj,PicWidth*12,PicHeight*12
 GR.BITMAP.SAVE ScalePtr,"GCTestPicS.png",75
 GR.BITMAP.DELETE ScalePtr
 GR.BITMAP.SCALE ScalePtr,bm_obj,PicWidth*12,PicHeight*8
 GR.BITMAP.SAVE ScalePtr,"GCTestPicL.png",75
 GR.BITMAP.DELETE ScalePtr
 GR.BITMAP.SCALE ScalePtr,bm_obj,PicWidth*8,PicHeight*12
 GR.BITMAP.SAVE ScalePtr,"GCTestPicP.png",75
 GR.BITMAP.DELETE ScalePtr
 GR.BITMAP.DELETE bm_obj
ENDIF

!***  Initialise Picture Testing Form Data.

LET PicCaption$=""
LET PicStyle$=""
LET PicAngle$="0"
LET PicColour=1
LET PicClr$="Black"
LET TestPic$="GCTestPicS.png"

ReloadPicForm:
CALL StartNewForm(DateSize,bcSOUNDON$,FrmScale,MsgBoxFontSize,bcLBLUE)
LET lblPicHead=AddControl(bcFRMLABEL,"Picture Test Panel", ~
      bcBLACK,1,0,0, ~
      6,20,0,1240,40,30, ~
      bcALIGNCENTRE$+bcDATBOLD$+bcCAPUNDERLINE$)
LET picSourceS=AddControl(bcFRMPICTURE,"", ~
      bcBLACK,bcLGREEN,bcBLACK,bcLCYAN, ~
      80,30,0,200,200,20,"")
LET picSourceL=AddControl(bcFRMPICTURE,"", ~
      bcBLACK,bcLGREEN,bcBLACK,bcLCYAN, ~
      300,10,0,240,160,20,"")
LET picSourceP=AddControl(bcFRMPICTURE,"", ~
      bcBLACK,bcLGREEN,bcBLACK,bcLCYAN, ~
      480,50,0,160,240,20,"")
LET picTestPic=AddControl(bcFRMPICTURE,"", ~
      PicColour,bcLGREEN,bcBLACK,bcLCYAN, ~
      60,350,3,400,400,20,PicStyle$)
LET optSrcPic=AddControl(bcFRMOPTBUTTON,"Source Picture"+bcRECBREAK$ ~
      +"Square"+bcRECBREAK$+"Landscape"+bcRECBREAK$+"Portrait", ~
      bcBLACK,bcLBLUE,bcBLACK,bcLCYAN, ~
      510,300,120,520,40,20,bcDATABORDER$+bcCAPITALIC$+bcALIGNRIGHT$) 
LET ckbPicStyle=AddControl(bcFRMCHKBUTTON,"Picture Style"+bcRECBREAK$ ~
      +"Size"+bcRECBREAK$+"Scroll"+bcRECBREAK$+"Crop"+bcRECBREAK$+"Rotate"+bcRECBREAK$+"Border", ~
      bcBLACK,bcLBLUE,bcBLACK,bcLCYAN, ~
      570,300,120,620,40,20,bcDATABORDER$+bcCAPITALIC$+bcALIGNRIGHT$)
LET spnPicAngle=AddControl(bcFRMSPINBUTTON,"Rotate Angle" ~
      +bcRECBREAK$+"5" +bcRECBREAK$+"30" +bcRECBREAK$+"-180" +bcRECBREAK$+"180", ~
      bcBLACK,bcLBLUE,bcBLACK,bcLCYAN, ~
      630,300,120,340,40,20,bcDATABORDER$+bcCAPITALIC$+bcALIGNRIGHT$+bcDISABLED$+bcSPINDN$)

LET cboPicCaption=AddControl(bcFRMCOMBOBOX,"Caption" ~
      +bcRECBREAK$+bcBLANK$+bcRECBREAK$+"Short Cap"+bcRECBREAK$+"Long Test Caption containing Rubbish.", ~
      bcBLACK,bcLBLUE,bcBLACK,bcLCYAN, ~
      630,660,80,280,40,20,bcDATABORDER$+bcCAPITALIC$+bcALIGNRIGHT$+bcALLOWNEW$)

LET cboPicColor=AddControl(bcFRMCOMBOBOX,"Border - Colour"+bcRECBREAK$ ~
      +"Black"+bcCOLBREAK$+"1"+bcRECBREAK$+"Blue"+bcCOLBREAK$+"2"+bcRECBREAK$ ~
      +"Green"+bcCOLBREAK$+"3"+bcRECBREAK$+"Cyan"+bcCOLBREAK$+"4"+bcRECBREAK$ ~
      +"Red"+bcCOLBREAK$+"5"+bcRECBREAK$+"Magenta"+bcCOLBREAK$+"6"+bcRECBREAK$ ~
      +"Yellow"+bcCOLBREAK$+"7"+bcRECBREAK$+"Gray"+bcCOLBREAK$+"8"+bcRECBREAK$ ~
      +"Light Gray"+bcCOLBREAK$+"9"+bcRECBREAK$+"Light Blue"+bcCOLBREAK$+"10"+bcRECBREAK$ ~
      +"Light Green"+bcCOLBREAK$+"11"+bcRECBREAK$+"Light Cyan"+bcCOLBREAK$+"12"+bcRECBREAK$ ~
      +"Light Red"+bcCOLBREAK$+"13"+bcRECBREAK$+"Light Magenta"+bcCOLBREAK$+"14"+bcRECBREAK$ ~
      +"Light Yellow"+bcCOLBREAK$+"15"+bcRECBREAK$+"White"+bcCOLBREAK$+"16" , ~
      bcBLACK,bcLBLUE,bcBLACK,bcLCYAN, ~
      690,300,120,300,40,20,bcDATABORDER$+bcCAPITALIC$+bcALIGNRIGHT$+bcDISABLED$)
LET selPicBdr=AddControl(bcFRMSELECT,"Width"+bcRECBREAK$+"0"+bcRECBREAK$+"1"+bcRECBREAK$ ~
      +"2"+bcRECBREAK$+"3"+bcRECBREAK$+"4"+bcRECBREAK$+"5"+bcRECBREAK$+"6"+bcRECBREAK$ ~
      +"7"+bcRECBREAK$+"8"+bcRECBREAK$+"9", ~
      bcBLACK,bcLBLUE,bcBLACK,bcLCYAN, ~
      690,600,80,200,40,20,bcDATABORDER$+bcCAPITALIC$+bcALIGNRIGHT$+bcDISABLED$)
LET cmdPicClose=AddControl(bcFRMBUTTON,"Close", ~
      bcBLACK,bcLGREEN,0,0, ~
      680,820,0,120,50,20,bcBEVEL$)
LET lblSourceS=AddControl(bcFRMLABEL,"Square Picture", ~
      bcBLACK,bcLGREEN,bcBLACK,bcLCYAN, ~
      80,30,0,200,30,20,bcALIGNCENTRE$)
LET lblSourceL=AddControl(bcFRMLABEL,"Landscape Picture", ~
      bcBLACK,bcLGREEN,bcBLACK,bcLCYAN, ~
      300,10,0,240,30,20,bcALIGNCENTRE$)
LET lblSourceP=AddControl(bcFRMLABEL,"Portrait Picture", ~
      bcBLACK,bcLGREEN,bcBLACK,bcLCYAN, ~
      480,50,0,160,30,20,bcALIGNCENTRE$)
IF TestPic$="GCTestPicS.png" THEN
 LET i$="Square"
ELSEIF TestPic$="GCTestPicL.png" THEN
 LET i$="Landscape"
ELSE
 LET i$="Portrait"
ENDIF
CALL SetCtrlData(optSrcPic,i$)
LET i$=""
IF IS_IN(bcPICSIZE$,PicStyle$)<>0 THEN LET i$="Size"
IF IS_IN(bcPICSCROLL$,PicStyle$)<>0 THEN
 IF i$<>"" THEN LET i$=i$+bcRECBREAK$
 LET i$=i$+"Scroll"
ENDIF 
IF IS_IN(bcPICCROP$,PicStyle$)<>0 THEN
 IF i$<>"" THEN LET i$=i$+bcRECBREAK$
 LET i$=i$+"Crop"
ENDIF
IF IS_IN(bcNOBORDER$,PicStyle$)=0 THEN
 IF i$<>"" THEN LET i$=i$+bcRECBREAK$
 LET i$=i$+"Border"
 LET f=1
ELSE
 LET f=0
ENDIF
IF IS_IN(bcPICROTATE$,PicStyle$)<>0 THEN
 IF i$<>"" THEN LET i$=i$+bcRECBREAK$
 LET i$=i$+"Rotate"
 LET i=VAL(PicAngle$)
 CALL SetPicRotate(picTestPic,i)
 LET e=1
ELSE
 LET e=0
ENDIF
CALL SetCtrlData(spnPicAngle,PicAngle$)
CALL SetCtrlData(cboPicCaption,PicCaption$)
CALL SetCtrlData(cboPicColor,PicClr$)
CALL SetCtrlData(ckbPicStyle,i$)
CALL SetCtrlData(selPicBdr,"3")
CALL SetCtrlData(picSourceS,"GCTestPicS.png")
CALL SetCtrlData(picSourceL,"GCTestPicL.png")
CALL SetCtrlData(picSourceP,"GCTestPicP.png")
CALL SetCtrlCap(picTestPic,PicCaption$)
CALL SetCtrlData(picTestPic,TestPic$)

CALL DrawForm("Loading Picture Test Form...",picPath$)

CALL CtrlEnable(spnPicAngle,e,0)
CALL CtrlEnable(cboPicColor,f,1)
CALL CtrlEnable(selPicBdr,f,1)

!***  Processing Loop for Picture Testing Form

LET bSWExit=0
WHILE bSWExit=0
 LET selCtrl=TouchCheck(Period,picTestPic,cmdPicClose)
 SW.BEGIN selCtrl
  SW.CASE optSrcPic
   LET i$=GetCtrlData$(optSrcPic)
   IF i$="Square" THEN
    LET TestPic$="GCTestPicS.png"
   ELSEIF i$="Landscape" THEN
    LET TestPic$="GCTestPicL.png"
   ELSE
    LET TestPic$="GCTestPicP.png"
   ENDIF
   LET bSWExit=1 % goto ReloadPicForm
   SW.BREAK
  SW.CASE ckbPicStyle
   LET i$=GetCtrlData$(ckbPicStyle)
   LET PicStyle$=""
   IF IS_IN("Size",i$)<>0 THEN LET PicStyle$=bcPICSIZE$
   IF IS_IN("Scroll",i$)<>0 THEN
    IF PicStyle$<>"" THEN LET PicStyle$=PicStyle$+bcRECBREAK$
    LET PicStyle$=PicStyle$+bcPICSCROLL$
   ENDIF
   IF IS_IN("Crop",i$)<>0 THEN
    IF PicStyle$<>"" THEN LET PicStyle$=PicStyle$+bcRECBREAK$
    LET PicStyle$=PicStyle$+bcPICCROP$
   ENDIF
   IF IS_IN("Rotate",i$)<>0 THEN
    IF PicStyle$<>"" THEN LET PicStyle$=PicStyle$+bcRECBREAK$
    LET PicStyle$=PicStyle$+bcPICROTATE$
   ENDIF
   IF IS_IN("Border",i$)=0 THEN
    IF PicStyle$="" THEN LET PicStyle$=PicStyle$+bcRECBREAK$
    LET PicStyle$=PicStyle$+bcNOBORDER$
   ENDIF
   LET bSWExit=1 % goto ReloadPicForm
   SW.BREAK
  SW.CASE spnPicAngle
   LET PicAngle$=GetCtrlData$(spnPicAngle)
   CALL ModPicRotate(picTestPic,VAL(PicAngle$),1)
   SW.BREAK
  SW.CASE cboPicCaption
   LET PicCaption$=GetCtrlData$(cboPicCaption)
   IF PicCaption$=bcBLANK$ THEN LET PicCaption$=""
   CALL SetCtrlCap(picTestPic,PicCaption$)
   CALL ModCtrlData(picTestPic,TestPic$,1)
   SW.BREAK
  SW.CASE cboPicColor
   LET i$=GetCtrlData$(cboPicColor)
   LET p=IS_IN(bcCOLBREAK$,i$)
   LET PicClr$=LEFT$(i$,p-1)
   LET PicColour=VAL(MID$(i$,p+1))
   LET bSWExit=1 % goto ReloadPicForm
   SW.BREAK
  SW.CASE selPicBdr
   LET i$=GetCtrlData$(selPicBdr)
   CALL ModPicBdrWidth(picTestPic,VAL(i$),1)
   SW.BREAK
  SW.CASE cmdPicClose
   LET bSWExit=2 % goto DisplayTestForm
   SW.BREAK
 SW.END
REPEAT
GOTO bSWExit,ReloadPicForm,DisplayTestForm

LoadControlValueStrings:
LET HelpString$= ~
  "Basically just attempt to press any of the controls and" ~
  +bcRECBREAK$ ~
  +"decide is the action it invokes (if any), is what you expect." ~
  +bcRECBREAK$+bcRECBREAK$ ~
  +"If the action is not what you expect then complain to the" ~
  +bcRECBREAK$~
  +"author, explain the problem and tell him to correct it." ~
  +bcRECBREAK$+bcRECBREAK$ ~
  +"If there is a missing action or function which would add to" ~
  +bcRECBREAK$ ~
  +"the usefulness of GraphicControls, again, tell the" ~
  +bcRECBREAK$ ~
  +"author and it may get included." ~
  +bcRECBREAK$ ~
  +"(Is 10 lines enough?)"

LET SelVals$="Apple"+bcRECBREAK$+"Banana"+bcRECBREAK$ ~
  +"Grapefruit"+bcRECBREAK$+"Kiwi"+bcRECBREAK$ ~
  +"Orange" +bcRECBREAK$+"Peach"

LET SelModVals$="Mod Apple"+bcRECBREAK$+"Mod Banana"+bcRECBREAK$ ~
  +"Mod Grapefruit"+bcRECBREAK$+"Mod Kiwi"+bcRECBREAK$ ~
  +"Mod Orange"+bcRECBREAK$+"Mod Peach"

LET CBox1Vals$="Apple"+bcRECBREAK$+"Banana"+bcRECBREAK$ ~
  +"Cherry"+bcRECBREAK$+"Peach"+bcRECBREAK$ ~
  +"Plum"+bcRECBREAK$+"Tangerine"

LET CBox2Vals$="Apple2"+bcRECBREAK$+"Banana2"+bcRECBREAK$ ~
  +"Cherry2"+bcRECBREAK$+"Grape2"+bcRECBREAK$ ~
  +"Grapefruit2"+bcRECBREAK$+"Kiwi2"+bcRECBREAK$ ~
  +"Orange2"+bcRECBREAK$+"Mandarin2"+bcRECBREAK$ ~
  +"Melon2"+bcRECBREAK$+"Peach2"+bcRECBREAK$ ~
  +"Plum2"+bcRECBREAK$+"Tangerine2"

LET CBox2ModVals$="Mod Apple2"+bcRECBREAK$ ~
  +"Mod Banana2"+bcRECBREAK$+"Mod Cherry2"+bcRECBREAK$ ~
  +"Mod Grape2"+bcRECBREAK$+"Mod Grapefruit2"+bcRECBREAK$ ~
  +"Mod Kiwi2"+bcRECBREAK$+"Mod Orange2"+bcRECBREAK$ ~
  +"Mod Mandarin2"+bcRECBREAK$+"Mod Melon2"+bcRECBREAK$ ~
  +"Mod Peach2"+bcRECBREAK$+"Mod Plum2"+bcRECBREAK$ ~
  +"Mod Tangerine2"

LET LstBoxLvwIconVals$="ListBox"+bcCOLBREAK$ ~
  +"Author"+bcFLDBREAK$+STR$(86)+bcFLDBREAK$+"1"+bcCOLBREAK$ ~
  +"Title"+bcFLDBREAK$+STR$(80)+bcFLDBREAK$+"1"+bcCOLBREAK$ ~
  +"Price"+bcFLDBREAK$+STR$(70)+bcFLDBREAK$+"3"+bcCOLBREAK$ ~
  +"Stock"+bcFLDBREAK$+STR$(54)+bcFLDBREAK$+"2" ~
  +bcRECBREAK$ ~
  +"GC-Information.png"+bcCOLBREAK$ ~
  +"Cookson"+bcCOLBREAK$+"Northumberland"+bcCOLBREAK$ ~
  +"£3.00"+bcCOLBREAK$+"Y" ~
  +bcRECBREAK$ ~
  +"GC-Critical.png"+bcCOLBREAK$ ~
  +"Deighton"+bcCOLBREAK$+"Quiller"+bcCOLBREAK$ ~
  +"£10.00"+bcCOLBREAK$+"N" ~
  +bcRECBREAK$ ~
  +"GC-Exclamation.png"+bcCOLBREAK$ ~
  +"Fry"+bcCOLBREAK$+"Washboard"+bcCOLBREAK$ ~
  +"£6.00"+bcCOLBREAK$+"Y" ~
  +bcRECBREAK$ ~
  +"GC-Question.png"+bcCOLBREAK$ ~
  +"King"+bcCOLBREAK$+"Cell It"+bcCOLBREAK$ ~
  +"£12.00"+bcCOLBREAK$+"N" ~
  +bcRECBREAK$ ~
  +"GC-Information.png"+bcCOLBREAK$ ~
  +"McCaffrey"+bcCOLBREAK$+"Pern Dragron"+bcCOLBREAK$ ~
  +"£5.50"+bcCOLBREAK$+"Y" ~
  +bcRECBREAK$ ~
  +"GC-Critical.png"+bcCOLBREAK$ ~
  +"MacLean"+bcCOLBREAK$+"Navaronne Eagle"+bcCOLBREAK$ ~
  +"£8.00"+bcCOLBREAK$+"Y" ~
  +bcRECBREAK$ ~
  +"GC-Exclamation.png"+bcCOLBREAK$ ~
  +"Pratchett"+bcCOLBREAK$+"Plateworld"+bcCOLBREAK$ ~
  +"£4.75"+bcCOLBREAK$+"N" ~
  +bcRECBREAK$ ~
  +"GC-Question.png"+bcCOLBREAK$ ~
  +"Tolkien"+bcCOLBREAK$+"Mordor Tales"+bcCOLBREAK$ ~
  +"£1.25"+bcCOLBREAK$+"Y"

LET LstBoxLvwVals$="ListBox"+bcCOLBREAK$ ~
  +"Author"+bcFLDBREAK$+STR$(96)+bcFLDBREAK$+"1"+bcCOLBREAK$ ~
  +"Title"+bcFLDBREAK$+STR$(100)+bcFLDBREAK$+"1"+bcCOLBREAK$ ~
  +"Price"+bcFLDBREAK$+STR$(70)+bcFLDBREAK$+"3" +bcCOLBREAK$ ~
  +"Stock"+bcFLDBREAK$+STR$(54)+bcFLDBREAK$+"2" ~
  +bcRECBREAK$ ~
  +"Cookson"+bcCOLBREAK$+"Northumberland"+bcCOLBREAK$ ~
  +"£3.00"+bcCOLBREAK$+"Y" ~
  +bcRECBREAK$ ~
  +"Deighton"+bcCOLBREAK$+"Quiller"+bcCOLBREAK$ ~
  +"£10.00"+bcCOLBREAK$+"N" ~
  +bcRECBREAK$ ~
  +"Fry"+bcCOLBREAK$+"Washboard"+bcCOLBREAK$ ~
  +"£6.00"+bcCOLBREAK$+"Y" ~
  +bcRECBREAK$ ~
  +"King"+bcCOLBREAK$+"Cell It"+bcCOLBREAK$ ~
  +"£12.00"+bcCOLBREAK$+"N" ~
  +bcRECBREAK$ ~
  +"McCaffrey"+bcCOLBREAK$+"Pern Dragron"+bcCOLBREAK$ ~
  +"£5.50"+bcCOLBREAK$+"Y" ~
  +bcRECBREAK$ ~
  +"MacLean"+bcCOLBREAK$+"Navaronne Eagle"+bcCOLBREAK$ ~
  +"£8.00"+bcCOLBREAK$+"Y" ~
  +bcRECBREAK$ ~
  +"Pratchett"+bcCOLBREAK$+"Plateworld"+bcCOLBREAK$ ~
  +"£4.75"+bcCOLBREAK$+"N" ~
  +bcRECBREAK$ ~
  +"Tolkien"+bcCOLBREAK$+"Mordor Tales"+bcCOLBREAK$ ~
  +"£1.25"+bcCOLBREAK$+"Y"

LET LstBoxLvwModVals$="MListBox"+bcCOLBREAK$ ~
  +"Author"+bcFLDBREAK$+STR$(100)+bcFLDBREAK$+"1"+bcCOLBREAK$ ~
  +"MTitle"+bcFLDBREAK$+STR$(106)+bcFLDBREAK$+"1"+bcCOLBREAK$ ~
  +"Price"+bcFLDBREAK$+STR$(70)+bcFLDBREAK$+"3"+bcCOLBREAK$ ~
  +"MStock"+bcFLDBREAK$+STR$(54)+bcFLDBREAK$+"2" ~
  +bcRECBREAK$ ~
  +"MCookson"+bcCOLBREAK$+"MNorthumberland" ~
  +bcCOLBREAK$+"£3.00"+bcCOLBREAK$+"Y" ~
  +bcRECBREAK$ ~
  +"MDeighton"+bcCOLBREAK$+"MQuiller"+bcCOLBREAK$ ~
  +"£10.00"+bcCOLBREAK$+"N" ~
  +bcRECBREAK$ ~
  +"MFry"+bcCOLBREAK$+"MWashboard"+bcCOLBREAK$ ~
  +"£6.00"+bcCOLBREAK$+"Y" ~
  +bcRECBREAK$ ~
  +"MKing"+bcCOLBREAK$+"MCell It"+bcCOLBREAK$ ~
  +"£12.00"+bcCOLBREAK$+"N" ~
  +bcRECBREAK$ ~
  +"MMcCaffrey"+bcCOLBREAK$+"MPern Dragron"+bcCOLBREAK$ ~
  +"£5.50"+bcCOLBREAK$+"Y" ~
  +bcRECBREAK$ ~
  +"MMacLean"+bcCOLBREAK$+"MNavaronne Eagle"+bcCOLBREAK$ ~
  +"£8.00"+bcCOLBREAK$+"Y" ~
  +bcRECBREAK$ ~
  +"MPratchett"+bcCOLBREAK$+"MPlateworld"+bcCOLBREAK$ ~
  +"£4.75"+bcCOLBREAK$+"N" ~
  +bcRECBREAK$ ~
  +"MTolkien"+bcCOLBREAK$+"MMordor Tales"+bcCOLBREAK$ ~
  +"£1.25"+bcCOLBREAK$+"Y"

LET LstBoxIconVals$= ~
  "GC-Question.png"+bcCOLBREAK$ ~
  +"Central Processing Unit"+bcRECBREAK$ ~
  +"GC-Information.png"+bcCOLBREAK$+"Disk"+bcRECBREAK$ ~
  +"GC-Critical.png"+bcCOLBREAK$ ~
  +"Laptop Laptop Laptop Laptop Laptop Laptop Laptop Laptop Laptop" ~
  +bcRECBREAK$ ~
  +"GC-Exclamation.png"+bcCOLBREAK$+"Keyboard"+bcRECBREAK$ ~
  +"GC-Question.png"+bcCOLBREAK$+"Mouse"+bcRECBREAK$ ~
  +"GC-Information.png"+bcCOLBREAK$+"Monitor"+bcRECBREAK$ ~
  +"GC-Critical.png"+bcCOLBREAK$+"Netbook"+bcRECBREAK$ ~
  +"GC-Exclamation.png"+bcCOLBREAK$+"Tower"

LET LstBoxVals$="Central Processing Unit"+bcRECBREAK$ ~
  +"Disk"+bcRECBREAK$ ~
  +"Laptop Laptop Laptop Laptop Laptop Laptop Laptop Laptop Laptop" ~
  +bcRECBREAK$ ~
  +"Keyboard"+bcRECBREAK$ ~
  +"Mouse"+bcRECBREAK$ ~
  +"Monitor"+bcRECBREAK$ ~
  +"Netbook"+bcRECBREAK$ ~
  +"Tower"

LET LstBoxModVals$="Mod Processing Unit"+bcRECBREAK$ ~
  +"Mod Disk"+bcRECBREAK$ ~
  +"Mod Laptop"+bcRECBREAK$ ~
  +"Mod Keyboard"+bcRECBREAK$ ~
  +"Mod Mouse"+bcRECBREAK$ ~
  +"Mod Monitor"+bcRECBREAK$ ~
  +"Mod Netbook"+bcRECBREAK$ ~
  +"Mod Tower"

LET LstBoxTextVal$=bcRECBREAK$+"<b><B><4><O>Changes in this release (Mono Font):" ~
  +bcRECBREAK$+bcRECBREAK$ ~
  +"<N><B><m><A>Notes (Sans-Serif Font)"+bcRECBREAK$ ~
  +"<BS>"+bcRECBREAK$ ~
  +"<BL><N><I><B><E>(Serif Font)The following text is just for testing purposes " ~
  +"only and is probably not factually correct." ~
  +bcRECBREAK$ ~
  +"<BL><N><I>(Default Font)Although this text is hard coded as part of the " ~
  +"program, it could just as easily been in a separate text " ~
  +"file loaded using the 'grabfile' command." ~
  +bcRECBREAK$ ~
  +"<BL><T>(User Font)Clicking on this text area will invoke the Text " ~
  +"Editor enabling the source text to be viewed and/or " ~
  +"changed so different format commands can be tested. It would " ~
  +"be easy to implement a 'save' function if required." ~
  +bcRECBREAK$ ~
  +"<BE>"+bcRECBREAK$+bcRECBREAK$ ~
  +"<N><B><m>Changes"+bcRECBREAK$ ~
  +"<NS>"+bcRECBREAK$ ~
  +"<NL><N>Now uses a graphic routine instead of Basic! 'Input' " ~
  +"command to handle text input." ~
  +bcRECBREAK$ ~
  +"<NL>New Frame Style bcFADEBACK$ - darkens screen display " ~
  +"behind frame so it's easier to see." ~
  +bcRECBREAK$ ~
  +"Above style is automatically adopted by the various control " ~
  +"'popups' (i.e.: MessageBox, Calendar, QuickNav Panel, and " ~
  +"ComboBox Dropdown Lists)." ~
  +bcRECBREAK$ ~
  +"<NL>Added the following functions for use with a ListBox " ~
  +"Control." ~
  +bcRECBREAK$ ~
  +"<BS>"+bcRECBREAK$ ~
  +"<BL><I>FN.DEF GetColClicked(pctrlno) [0=None]." ~
  +bcRECBREAK$ ~
  +"<BL>FN.DEF GetCheckedState(pctrlno, pIdx) [1=Yes, 0=No]." ~
  +bcRECBREAK$ ~
  +"<BL>FN.DEF DeleteListRow(pctrlno, pIdx, prender) [only redraws " ~
  +"if row deleted is within display range]." ~
  +bcRECBREAK$ ~
  +"<BL>FN.DEF UpdateListRow(pCtrlNo, pIdx, pRowData$, pRender) " ~
  +"[only redraws if updated row within display range]." ~
  +bcRECBREAK$ ~
  +"<BE><N>"+bcRECBREAK$ ~
  +"<NE>"+bcRECBREAK$ ~
  +"<r><B>Bugs Fixed - click on controls with no caption - ComboBox Bug " ~
  +"handling short lists." ~
  +bcRECBREAK$ ~
  +"<NS>"+bcRECBREAK$ ~
  +"<NL><N><m>bcMultiLine$ - Useful for displaying a block of text (eg: help" ~
  +"information)." ~
  +bcRECBREAK$ ~
  +"<NL>bcEditable$ - applies to bcMULTILINE$ Listboxes only. If present " ~
  +"and the listbox data area is clicked, data text can be edited. " ~
  +"Once editing is complete TouchCheck will return the control id " ~
  +"to the calling routine so updated text can be saved if " ~
  +"required." ~
  +bcRECBREAK$ ~
  +"<NL>Added new Menu button to top right corner of Test Panel. " ~
  +"When pressed a menu will pop up with the options 'Test Listbox', " ~
  +"'Help' and 'Exit'. The 'Help' and 'Exit' options have the same " ~
  +"action as the existing buttons." ~
  +bcRECBREAK$ ~
  +"The 'Test Listbox' options brings up another form used to test " ~
  +"various listbox styles. On the left is a listbox with the new " ~
  +"style 'bcMultiline$' listbox and on the right is a standard " ~
  +"Listbox used the test the following functions:" ~
  +bcRECBREAK$ ~
  +"<BS>"+bcRECBREAK$ ~
  +"<BL><I><L><a>GetColClicked."+bcRECBREAK$ ~
  +"<BL><C>GetCheckedState."+bcRECBREAK$ ~
  +"<BL><R>GetCheckedCount."+bcRECBREAK$ ~
  +"<BE><N><L>"+bcRECBREAK$ ~
  +"<NE>"+bcRECBREAK$
RETURN

SetListBoxStyle:
LET sTemp$=GetCtrlData$(ctrlOption)
LET optionSel$=sTemp$
IF sTemp$="Toggle FileDialog" THEN
 IF IS_IN(bcFILEDIALOG$,LBStyle$)=0 THEN
  LET LBStyle$=LBStyle$+bcFILEDIALOG$
  LET LBStyle$=REPLACE$(LBStyle$,bcLISTVIEW$,"")
  IF IS_IN(bcICON$,LBStyle$)<>0 THEN
   LET LBStyle$=REPLACE$(LBStyle$,bcICON$,"")
   CALL ModCtrlData(chkIcon,"N",0)
  ENDIF
 ELSE
  LET LBStyle$=REPLACE$(LBStyle$,bcFILEDIALOG$,"")
  LET optionSel$=""
 ENDIF
ELSEIF sTemp$="Toggle CheckBox" then
 IF IS_IN(bcCHECKBOX$,LBStyle$)=0 THEN
  LET LBStyle$=LBStyle$+bcCHECKBOX$
 ELSE
  LET LBStyle$=REPLACE$(LBStyle$,bcCHECKBOX$,"")
  LET optionSel$=""
 ENDIF
ELSEIF sTemp$="Toggle Listview" then
 IF IS_IN(bcLISTVIEW$,LBStyle$)=0 THEN
  LET LBStyle$=LBStyle$+bcLISTVIEW$
  LET LBStyle$=REPLACE$(LBStyle$,bcFILEDIALOG$,"")
 ELSE
  LET LBStyle$=REPLACE$(LBStyle$,bcLISTVIEW$,"")
  LET optionSel$=""
 ENDIF
ENDIF
RETURN

DisplayOptionButtonDetails:
LET sTemp$=GetCtrlData$(ctrlOptionH)
IF sTemp$="OKOnly" THEN
 LET Button$=MsgBox$("OKOnly Option with Information Icon.", ~
         bcOKONLY$+bcINFORMATION$, ~
         "Message Box Test")
 POPUP "MsgBox="+Button$,0,0,0
ELSEIF sTemp$="YesNo" then
 CALL SetMsgBoxColours(bcBLUE,bcYELLOW,bcRED,bcMAGENTA, ~
      bcLGREEN,bcCYAN,bcLMAGENTA)
 LET Button$=MsgBox$("YesNo OptionButton Pressed."+bcRECBREAK$ ~
         +"This MsgBox$ uses the Critical Icon.", ~
         bcYESNO$+bcCRITICAL$, ~
         "Message Box Test")
 POPUP "MsgBox="+Button$,0,0,0
 CALL SetMsgBoxColours(bcLBLUE,bcLCYAN,bcWHITE,bcLBLUE, ~
      bcBLACK,bcBLACK,bcBLACK)
ELSEIF sTemp$="YesNoCancel" then
 LET Button$=MsgBox$("YesNoCancel OptionButton Pressed."+bcRECBREAK$ ~
         +"Exclamation Icon used with this MagBox$"+bcRECBREAK$ ~
         +"MsgBox should have no border.", ~
         bcYESNOCANCEL$+bcEXCLAMATION$+bcNOBORDER$, ~
         "Message Box Test")
 POPUP "MsgBox="+Button$,0,0,0
ELSEIF sTemp$="10 lines-No Icon" then
 LET Button$=MsgBox$("DrawForm(pLoadMsg$,pPicPath$)" ~
   +bcRECBREAK$+bcRECBREAK$ ~
   +"Draws a form using currently loaded Control Details." ~
   +bcRECBREAK$+bcRECBREAK$ ~
   +"The string pLoadMsg$,if not empty,will be displayed in a PopUp" ~
   +bcRECBREAK$ ~
   +"at the start of the DrawForm processing. Useful when form" ~
   +bcRECBREAK$ ~
   +"complexity means DrawForm processing takes some time and the user" ~
   +bcRECBREAK$ ~
   +"would otherwise be staring at a blank screen wondering if anything" ~
   +bcRECBREAK$ ~
   +"is happening."+bcRECBREAK$ ~
   +"pPicPath$ is Path to be used when pictures are loaded onto the form.", ~
   bcCUSTOM$, ~
   "Message Box Test"+bcRECBREAK$+"Red"+bcRECBREAK$+"Green"+bcRECBREAK$+"Blue")
 POPUP "MsgBox="+Button$,0,0,0
ELSEIF sTemp$="Show Wait" then
 CALL HourGlass_Show(1,1)
 PAUSE 2000
 CALL HourGlass_Show(2,0)
 PAUSE 2000
 CALL HourGlass_Show(3,0)
 PAUSE 2000
 CALL HourGlass_Hide(1)
ELSEIF sTemp$="Show/Hide Mark" then
 IF MarkState$="Show" THEN
  CALL HideCtrl(dspMark,1)
  LET MarkState$="Hide"
 ELSE
  CALL ShowCtrl(dspMark,1)
  LET MarkState$="Show"
 ENDIF
ENDIF
RETURN

SetCaptionColours:
LET LBStyle$=REPLACE$(LBStyle$,bcICON$,"")
BUNDLE.GET 1,"ctrldata",CtrlData
BUNDLE.GET ctrldata,"CP"+STR$(lstListBox),ptrctrl
BUNDLE.GET ptrCtrl,"style",Style$
LET Style$=REPLACE$(Style$,bcICON$,"")
BUNDLE.PUT ptrCtrl,"style",Style$
CALL SetCtrlData(lstListBox,"")
IF ModCapTest=0 THEN
 CALL ModCtrlCapTxtClr(dspDisplay,bcBLUE,0)
 CALL ModCtrlCapTxtClr(ctrlString,bcBLUE,0)
 CALL ModCtrlCapTxtClr(ctrlText,bcBLUE,0)
 CALL ModCtrlCapTxtClr(ctrlSelect,bcBLUE,0)
 CALL ModCtrlCapTxtClr(ctrlDate,bcBLUE,0)
 CALL ModCtrlCapTxtClr(ctrlTime,bcBLUE,0)
 CALL ModCtrlCapTxtClr(ctrlButton,bcBLUE,0)
 CALL ModCtrlCapTxtClr(lblHeading,bcBLUE,0)
 CALL ModCtrlCapTxtClr(ctrlComboBox2,bcBLUE,0)
 CALL ModCtrlCapTxtClr(lstListBox,bcBLUE,0)
 CALL ModCtrlCapTxtClr(ctrlCheckBox,bcBLUE,0)
 CALL ModCtrlCapTxtClr(ctrlOption,bcBLUE,0)
 CALL ModCtrlCapTxtClr(ctrlSpinButton,bcBLUE,0)
 CALL ModCtrlCapTxtClr(ctrlFrame,bcBLUE,0)
 CALL ModCtrlCapTxtClr(shpRect,bcWHITE,0)
 CALL ModCtrlCapTxtClr(shpOval,bcWHITE,0)
 CALL ModCtrlCapTxtClr(shpCirc,bcWHITE,0)
 CALL ModCtrlCapTxtClr(shpLineV,bcBLACK,0)
 CALL ModCtrlCapTxtClr(shpLineH,bcBLACK,0)
 CALL ModCtrlCapBakClr(shpRect,bcLGREEN,0)
 CALL ModCtrlCapBakClr(shpOval,bcMAGENTA,0)
 CALL ModCtrlCapBakClr(shpCirc,bcCYAN,0)
 CALL ModCtrlCapBakClr(shpLineV,bcBLUE,0)
 CALL ModCtrlCapBakClr(shpLineH,bcRED,0)
 CALL ModCtrlCapBakClr(dspDisplay,bcYELLOW,0)
 CALL ModCtrlCapBakClr(ctrlString,bcYELLOW,0)
 CALL ModCtrlCapBakClr(ctrlText,bcYELLOW,0)
 CALL ModCtrlCapBakClr(ctrlSelect,bcYELLOW,0)
 CALL ModCtrlCapBakClr(ctrlDate,bcYELLOW,0)
 CALL ModCtrlCapBakClr(ctrlTime,bcYELLOW,0)
 CALL ModCtrlCapBakClr(ctrlButton,bcYELLOW,0)
 CALL ModCtrlCapBakClr(lblHeading,bcYELLOW,0)
 CALL ModCtrlCapBakClr(ctrlComboBox2,bcYELLOW,0)
 CALL ModCtrlCapBakClr(lstListBox,bcYELLOW,0)
 CALL ModCtrlCapBakClr(ctrlCheckBox,bcYELLOW,0)
 CALL ModCtrlCapBakClr(ctrlOption,bcYELLOW,0)
 CALL ModCtrlCapBakClr(ctrlSpinButton,bcYELLOW,0)
 CALL ModCtrlCapBakClr(ctrlFrame,bcYELLOW,0)
 CALL ModCtrlCap(dspDisplay,"Mod Disp",0)
 CALL ModCtrlCap(ctrlString,"Mod Str",0)
 CALL ModCtrlCap(ctrlText,"Mod Text",0)
 CALL ModCtrlCap(ctrlSelect,"Mod Sel"+bcRECBREAK$+SelModVals$,0)
 CALL ModCtrlCap(ctrlDate,"Mod Date",0)
 CALL ModCtrlCap(ctrlTime,"Mod Time",0)
 CALL ModCtrlCap(ctrlButton,"Mod But",0)
 CALL ModCtrlCap(lblHeading,"Mod Controls Test Harness",0)
 CALL ModCtrlCap(ctrlComboBox2,"Mod CBox2"+bcRECBREAK$+CBox2ModVals$,0)
 IF IS_IN(bcLISTVIEW$,LBStyle$)<>0 THEN
  CALL ModCtrlCap(lstListBox,LstBoxLvwModVals$,0)
 ELSE
  IF IS_IN(bcFILEDIALOG$,LBStyle$)<>0 THEN
   LET sData$=GetCtrlCap$(lstListBox)
   UNDIM sTemp$[]
   SPLIT sTemp$[],sData$,bcRECBREAK$
   ARRAY.LENGTH j,sTemp$[]
   FOR i=1 TO j
    LET sTemp$[i]="Mod "+sTemp$[i]
   NEXT i
   LET sData$=join$(sTemp$[],bcRECBREAK$)
   CALL ModCtrlCap(lstListBox,sData$,0)
  ELSE
   CALL ModCtrlCap(lstListBox,"Mod ListBox"+bcRECBREAK$+LstBoxModVals$,0)
  ENDIF
 ENDIF
 CALL ModCtrlCap(ctrlCheckBox,"Mod Chk",0)
 CALL ModCtrlCap(ctrlOption,"Mod Opts"+bcRECBREAK$+"Mod CheckBox"+bcRECBREAK$ ~
    +"Mod FileDialog"+bcRECBREAK$+"Mod Listview",0)
 CALL ModCtrlCap(ctrlSpinButton,"Mod Spin"+bcRECBREAK$+"100"+bcRECBREAK$+"1000"+bcRECBREAK$+"1000" ~
    +bcRECBREAK$+"10000",0)
 CALL ModCtrlCap(ctrlFrame,"Mod Frame",0)
 CALL EnableCtrl(cmdModDatTest,1)
 LET ModCapTest=1
ELSE
 CALL ModCtrlCapTxtClr(dspDisplay,bcLGRAY,0)
 CALL ModCtrlCapTxtClr(ctrlString,bcLGRAY,0)
 CALL ModCtrlCapTxtClr(ctrlText,bcLGRAY,0)
 CALL ModCtrlCapTxtClr(ctrlSelect,bcLGRAY,0)
 CALL ModCtrlCapTxtClr(ctrlDate,bcLGRAY,0)
 CALL ModCtrlCapTxtClr(ctrlTime,bcLGRAY,0)
 CALL ModCtrlCapTxtClr(ctrlButton,bcBLACK,0)
 CALL ModCtrlCapTxtClr(lblHeading,bcBLACK,0)
 CALL ModCtrlCapTxtClr(ctrlComboBox2,bcLGRAY,0)
 CALL ModCtrlCapTxtClr(lstListBox,bcLGRAY,0)
 CALL ModCtrlCapTxtClr(ctrlCheckBox,bcLGRAY,0)
 CALL ModCtrlCapTxtClr(ctrlOption,bcLGRAY,0)
 CALL ModCtrlCapTxtClr(ctrlSpinButton,bcLGRAY,0)
 CALL ModCtrlCapTxtClr(ctrlFrame,bcLGRAY,0)
 CALL ModCtrlCapTxtClr(shpRect,bcBLACK,0)
 CALL ModCtrlCapTxtClr(shpOval,bcBLACK,0)
 CALL ModCtrlCapTxtClr(shpCirc,bcBLACK,0)
 CALL ModCtrlCapTxtClr(shpLineV,bcWHITE,0)
 CALL ModCtrlCapTxtClr(shpLineH,bcWHITE,0)
 CALL ModCtrlCapBakClr(shpRect,bcBLUE,0)
 CALL ModCtrlCapBakClr(shpOval,bcYELLOW,0)
 CALL ModCtrlCapBakClr(shpCirc,bcRED,0)
 CALL ModCtrlCapBakClr(shpLineV,bcWHITE,0)
 CALL ModCtrlCapBakClr(shpLineH,bcWHITE,0)
 CALL ModCtrlCapBakClr(dspDisplay,bcGRAY,0)
 CALL ModCtrlCapBakClr(ctrlString,bcGRAY,0)
 CALL ModCtrlCapBakClr(ctrlText,bcGRAY,0)
 CALL ModCtrlCapBakClr(ctrlSelect,bcGRAY,0)
 CALL ModCtrlCapBakClr(ctrlDate,bcGRAY,0)
 CALL ModCtrlCapBakClr(ctrlTime,bcGRAY,0)
 CALL ModCtrlCapBakClr(ctrlButton,bcLYELLOW,0)
 CALL ModCtrlCapBakClr(lblHeading,bcBLUE,0)
 CALL ModCtrlCapBakClr(ctrlComboBox2,bcGRAY,0)
 CALL ModCtrlCapBakClr(lstListBox,bcGRAY,0)
 CALL ModCtrlCapBakClr(ctrlCheckBox,bcGRAY,0)
 CALL ModCtrlCapBakClr(ctrlOption,bcGRAY,0)
 CALL ModCtrlCapBakClr(ctrlSpinButton,bcGRAY,0)
 CALL ModCtrlCapBakClr(ctrlFrame,bcGRAY,0)
 CALL ModCtrlCap(dspDisplay,"Display",0)
 CALL ModCtrlCap(ctrlString,"String",0)
 CALL ModCtrlCap(ctrlText,"Text",0)
 CALL ModCtrlCap(ctrlSelect,"Select"+bcRECBREAK$+SelVals$,0)
 CALL ModCtrlCap(ctrlDate,"Date",0)
 CALL ModCtrlCap(ctrlTime,"Time (d:m:s)",0)
 CALL ModCtrlCap(ctrlButton,"Button",0)
 CALL ModCtrlCap(lblHeading,"Graphic Controls Test Harness",0)
 CALL ModCtrlCap(ctrlComboBox2,"ComboBox2"+bcRECBREAK$+CBox2Vals$ ,0)
 IF IS_IN(bcLISTVIEW$,LBStyle$)<>0 THEN
  CALL ModCtrlCap(lstListBox,LstBoxLvwVals$,0)
 ELSE
  IF IS_IN(bcFILEDIALOG$,LBStyle$)<>0 THEN
   LET sData$=GetCtrlCap$(lstListBox)
   UNDIM sTemp$[]
   SPLIT sTemp$[],sData$,bcRECBREAK$
   ARRAY.LENGTH j,sTemp$[]
   FOR i=1 TO j
    LET sTemp$[i]=MID$(sTemp$[i],5)
   NEXT i
   LET sData$=join$(sTemp$[],bcRECBREAK$)
   CALL ModCtrlCap(lstListBox,sData$,0)
  ELSE
   CALL ModCtrlCap(lstListBox,"ListBox"+bcRECBREAK$+LstBoxVals$,0)
  ENDIF
 ENDIF
 CALL ModCtrlCap(ctrlCheckBox,"CheckBox",0)
 CALL ModCtrlCap(ctrlOption,"Options"+bcRECBREAK$+"Toggle CheckBox"+bcRECBREAK$ ~
    +"Toggle FileDialog"+bcRECBREAK$+"Toggle Listview",0)
 CALL ModCtrlCap(ctrlSpinButton,"SpinButton"+bcRECBREAK$ ~
    +"1"+bcRECBREAK$+"10"+bcRECBREAK$+"1"+bcRECBREAK$+"100",0)
 CALL ModCtrlCap(ctrlFrame,"Frame",0)
 CALL DisableCtrl(cmdModDatTest,1)
 LET ModCapTest=0
ENDIF
RETURN

SetDataColours:
IF ModTxtTest=0 THEN
 CALL ModCtrlDatTxtClr(dspDisplay,bcRED,0)
 CALL ModCtrlDatTxtClr(ctrlString,bcRED,0)
 CALL ModCtrlDatTxtClr(ctrlText,bcRED,0)
 CALL ModCtrlDatTxtClr(ctrlSelect,bcRED,0)
 CALL ModCtrlDatTxtClr(ctrlDate,bcRED,0)
 CALL ModCtrlDatTxtClr(ctrlTime,bcRED,0)
 CALL ModCtrlDatTxtClr(ctrlButton,bcRED,0)
 CALL ModCtrlDatTxtClr(lblHeading,bcRED,0)
 CALL ModCtrlDatTxtClr(ctrlComboBox2,bcRED,0)
 CALL ModCtrlDatTxtClr(lstListBox,bcRED,0)
 CALL ModCtrlDatTxtClr(ctrlCheckBox,bcRED,0)
 CALL ModCtrlDatTxtClr(ctrlOption,bcRED,0)
 CALL ModCtrlDatTxtClr(ctrlSpinButton,bcRED,0)
 CALL ModCtrlDatTxtClr(ctrlFrame,bcRED,0)
 CALL ModCtrlDatBakClr(dspDisplay,bcMAGENTA,0)
 CALL ModCtrlDatBakClr(ctrlString,bcMAGENTA,0)
 CALL ModCtrlDatBakClr(ctrlText,bcMAGENTA,0)
 CALL ModCtrlDatBakClr(ctrlSelect,bcMAGENTA,0)
 CALL ModCtrlDatBakClr(ctrlDate,bcMAGENTA,0)
 CALL ModCtrlDatBakClr(ctrlTime,bcMAGENTA,0)
 CALL ModCtrlDatBakClr(ctrlButton,bcMAGENTA,0)
 CALL ModCtrlDatBakClr(lblHeading,bcMAGENTA,0)
 CALL ModCtrlDatBakClr(ctrlComboBox2,bcMAGENTA,0)
 CALL ModCtrlDatBakClr(lstListBox,bcMAGENTA,0)
 CALL ModCtrlDatBakClr(ctrlCheckBox,bcMAGENTA,0)
 CALL ModCtrlDatBakClr(ctrlOption,bcMAGENTA,0)
 CALL ModCtrlDatBakClr(ctrlSpinButton,bcMAGENTA,0)
 CALL ModCtrlDatBakClr(ctrlFrame,bcMAGENTA,0)
 CALL ModCtrlData(dspDisplay,"Mod Harness",0)
 CALL ModCtrlData(ctrlString,"Mod Number",0)
 CALL ModCtrlData(ctrlText,"Mod Text"+bcRECBREAK$ ~
    +"Mod-sgsgsg fhfhfh gagag fjffk skskssk"+bcStrCRLF$ ~
    +"Mod-dkfkffk ddkfkkf ffkfkfk"+bcStrCRLF$+bcStrCRLF$+"dhddjdjjdj shshshsh",0)
 CALL ModCtrlData(ctrlDate,"2001-01-01-Fri",0)
 CALL ModCtrlData(ctrlTime,"22:22:22",0)
 LET sData$=GetCtrlCap$(ctrlSelect)
 UNDIM sTemp$[]
 SPLIT sTemp$[],sData$,bcRECBREAK$
 CALL ModCtrlData(ctrlSelect,sTemp$[2],0)
 LET sData$=GetCtrlCap$(lstListBox)
 UNDIM sTemp$[]
 SPLIT sTemp$[],sData$,bcRECBREAK$
 CALL ModCtrlData(lstListBox,sTemp$[3],0)
 CALL ModCtrlData(ctrlCheckBox,"N",0)
 LET sData$=GetCtrlCap$(ctrlOption)
 UNDIM sTemp$[]
 SPLIT sTemp$[],sData$,bcRECBREAK$
 CALL ModCtrlData(ctrlOption,sTemp$[2],0)
 LET sData$=GetCtrlCap$(ctrlSpinButton)
 UNDIM sTemp$[]
 SPLIT sTemp$[],sData$,bcRECBREAK$
 LET sTemp$=STR$(VAL(sTemp$[4])+VAL(sTemp$[2]))
 LET i=IS_IN(".",sTemp$)
 CALL ModCtrlData(ctrlSpinButton,LEFT$(sTemp$,i-1),0)
 LET sData$=GetCtrlCap$(ctrlComboBox2)
 UNDIM sTemp$[]
 SPLIT sTemp$[],sData$,bcRECBREAK$
 CALL ModCtrlData(ctrlComboBox2,sTemp$[2],1)
 LET ModTxtTest=1
ELSE
 CALL ModCtrlDatTxtClr(dspDisplay,bcBLACK,0)
 CALL ModCtrlDatTxtClr(ctrlString,bcBLACK,0)
 CALL ModCtrlDatTxtClr(ctrlText,bcBLACK,0)
 CALL ModCtrlDatTxtClr(ctrlSelect,bcBLACK,0)
 CALL ModCtrlDatTxtClr(ctrlDate,bcBLACK,0)
 CALL ModCtrlDatTxtClr(ctrlTime,bcBLACK,0)
 CALL ModCtrlDatTxtClr(ctrlButton,bcBLACK,0)
 CALL ModCtrlDatTxtClr(lblHeading,bcBLACK,0)
 CALL ModCtrlDatTxtClr(ctrlComboBox2,bcBLACK,0)
 CALL ModCtrlDatTxtClr(lstListBox,bcBLACK,0)
 CALL ModCtrlDatTxtClr(ctrlCheckBox,bcBLUE,0)
 CALL ModCtrlDatTxtClr(ctrlOption,bcBLACK,0)
 CALL ModCtrlDatTxtClr(ctrlSpinButton,bcBLACK,0)
 CALL ModCtrlDatTxtClr(ctrlFrame,bcBLACK,0)
 CALL ModCtrlDatBakClr(dspDisplay,bcWHITE,0)
 CALL ModCtrlDatBakClr(ctrlString,bcWHITE,0)
 CALL ModCtrlDatBakClr(ctrlText,bcWHITE,0)
 CALL ModCtrlDatBakClr(ctrlSelect,bcWHITE,0)
 CALL ModCtrlDatBakClr(ctrlDate,bcWHITE,0)
 CALL ModCtrlDatBakClr(ctrlTime,bcWHITE,0)
 CALL ModCtrlDatBakClr(ctrlButton,bcWHITE,0)
 CALL ModCtrlDatBakClr(lblHeading,bcWHITE,0)
 CALL ModCtrlDatBakClr(ctrlComboBox2,bcWHITE,0)
 CALL ModCtrlDatBakClr(lstListBox,bcWHITE,0)
 CALL ModCtrlDatBakClr(ctrlCheckBox,bcWHITE,0)
 CALL ModCtrlDatBakClr(ctrlOption,bcWHITE,0)
 CALL ModCtrlDatBakClr(ctrlSpinButton,bcWHITE,0)
 CALL ModCtrlDatBakClr(ctrlFrame,bcWHITE,0)
 CALL ModCtrlData(dspDisplay,"Test Harness",0)
 CALL ModCtrlData(ctrlString,"Version Number",0)
 CALL ModCtrlData(ctrlText,"Text-Key"+bcRECBREAK$+"sgsgsg fhfhfh gagag fjffk skskssk"+bcStrCRLF$ ~
      +"dkfkffk ddkfkkf ffkfkfk"+bcStrCRLF$+bcStrCRLF$+"dhddjdjjdj shshshsh",0)
 CALL ModCtrlData(ctrlDate,"2011-08-21-Sun",0)
 CALL ModCtrlData(ctrlTime,"11:11:11",0)
 LET sData$=GetCtrlCap$(ctrlSelect)
 UNDIM sTemp$[]
 SPLIT sTemp$[],sData$,bcRECBREAK$
 CALL ModCtrlData(ctrlSelect,sTemp$[3],0)
 LET sData$=GetCtrlCap$(lstListBox)
 UNDIM sTemp$[]
 SPLIT sTemp$[],sData$,bcRECBREAK$
 CALL ModCtrlData(lstListBox,sTemp$[4],0)
 CALL ModCtrlData(ctrlCheckBox,"Y",0)
 LET sData$=GetCtrlCap$(ctrlOption)
 UNDIM sTemp$[]
 SPLIT sTemp$[],sData$,bcRECBREAK$
 CALL ModCtrlData(ctrlOption,sTemp$[3],0)
 LET sData$=GetCtrlCap$(ctrlSpinButton)
 UNDIM sTemp$[]
 SPLIT sTemp$[],sData$,bcRECBREAK$
 LET sTemp$=STR$(VAL(sTemp$[5])+VAL(sTemp$[2]))
 LET i=IS_IN(".",sTemp$)
 CALL ModCtrlData(ctrlSpinButton,LEFT$(sTemp$,i-1),0)
 LET sData$=GetCtrlCap$(ctrlComboBox2)
 UNDIM sTemp$[]
 SPLIT sTemp$[],sData$,bcRECBREAK$
 CALL ModCtrlData(ctrlComboBox2,sTemp$[3],1)
 LET ModTxtTest=0
ENDIF
RETURN

CloseTest:
CALL DeleteBitmaps()
GR.CLOSE

END

!******************************************
!***   End GraphicControlsTestBed.bas   ***
!******************************************
