!#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
!#-=__Start_of_GC-Optimiser.bas___-=
!#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
GCVer$="v1.16"

INCLUDE GraphicConstants.bas
INCLUDE GraphicControls.bas

CALL InitGraphics(bcOPAQUE,bcLGRAY,bcLANDSCAPE,bcHIDESTATUSBAR,"GCClick.ogg")
!CALL InitGraphics(bcOPAQUE,bcLGRAY,bcPORTRAIT,bcHIDESTATUSBAR,"GCClick.ogg")
CALL SetScalingFactor(1200,750,&DateSize,&FrmScale,&MsgBoxFontSize)
LET curPath$="../../../../sdcard/rfo-basic/source/"
LET bProgPanel=0

!=================================================
!===   D I S P L A Y   F I L E   S E L E C T   ===
!=================================================
DisplayFileSelect:
CALL StartNewForm(DateSize,bcSOUNDON$,FrmScale,MsgBoxFontSize,bcLMAGENTA)
LET lblFSHead=AddControl(bcFRMLABEL,"GC Optimiser",bcBLUE,0,0,0, ~
  20,0,2,1200,0,30,bcCAPBOLD$+bcALIGNCENTRE$+bcCTRLCENTRE$)
LET lblFSSHead=AddControl(bcFRMLABEL,"Select Source File",bcGREEN,0,0,0, ~
  50,0,2,1200,0,20,bcCAPBOLD$+bcALIGNCENTRE$+bcCTRLCENTRE$)
LET dspFSFolder=AddControl(bcFRMDISPLAY,"Selected Folder",bcBLACK,bcGRAY,bcBLACK,bcLCYAN, ~
  70,50,160,1100,40,20,bcCAPBOLD$+bcCTRLCENTRE$)
LET lstFSContents=AddControl(bcFRMLISTBOX,"Contents",bcWHITE,bcGRAY,bcBLACK,bcWHITE, ~
  120,50,50,1100,550,20,bcFILEDIALOG$+bcCTRLCENTRE$+bc3DBORDER$+bcQUICKNAVN$+bcBANDING$)
LET cmdFSView=AddControl(bcFRMBUTTON,"Process Selected",bcBLACK,bcLBLUE,0,0, ~
  684,40,0,180,50,20,bcCTRLLEFT$)
LET cmdFSClose=AddControl(bcFRMBUTTON,"Close Viewer",bcBLACK,bcLBLUE,0,0, ~
  684,40,0,180,50,20,bcCTRLRIGHT$)
CALL DrawForm("",curPath$)
!
SelReload:
UNDIM dfile$[]
LET sTemp$=MID$(curPath$,12)
CALL SetCtrlData(dspFSFolder,sTemp$)
CALL ModCtrlData(dspFSFolder,sTemp$,0)
DIR curPath$,dfile$[]
ARRAY.LENGTH afc,dfile$[]
IF afc>200 THEN
 LET fc=200
ELSE
 LET fc=afc
ENDIF
UNDIM dfl$[]
DIM dfl$[fc]
LET j=1
FOR i=1 TO afc
 IF j<=fc THEN
  IF RIGHT$(dFile$[i],3)="(d)" THEN
   LET dfl$[j]=dfile$[i]
   LET j=j+1
  ELSEIF LOWER$(RIGHT$(dFile$[i],4))=".bas" THEN
   LET dfl$[j]=dfile$[i]
   LET j=j+1
  ENDIF
 ENDIF
NEXT i
IF LEN(curPath$)=12 THEN
 LET sDotDot$=""
ELSE
 LET sDotDot$="..  <Goto Parent Folder>"+bcRECBREAK$
ENDIF
LET sTemp$="Contents"+bcRECBREAK$+sDotDot$+join$(dfl$[],bcRECBREAK$)
UNDIM dfl$[]
CALL SetCtrlData(lstFSContents,"")
CALL ModCtrlCap(lstFSContents,sTemp$,0)
CALL CtrlEnable(cmdFSView,0,1)
!
LET fsx=0
WHILE fsx=0
 LET selCtrl=TouchCheck(Period,0,0)
 SW.BEGIN selCtrl
  SW.CASE cmdFSClose
   UNDIM dfile$[]
   LET fsx=1
   SW.BREAK
  SW.CASE cmdFSView
   LET fsx=2
   SW.BREAK
  SW.CASE lstFSContents
   LET sVal$=GetCtrlData$(lstFSContents)
   IF LEFT$(sVal$,2)=".." THEN
    LET curPath$=LEFT$(curPath$,LEN(curPath$)-1)
    WHILE RIGHT$(curPath$,1)<>"/"
     LET curPath$=LEFT$(curPath$,LEN(curPath$)-1)
    REPEAT
    LET fsx=3
   ELSEIF RIGHT$(sVal$,3)="(d)" THEN
    LET sVal$=LEFT$(sval$,LEN(sval$)-3)
    LET curPath$=curPath$+sVal$+"/"
    LET fsx=3
   ELSE
    IF LOWER$(RIGHT$(sVal$,4))=".bas" THEN
     CALL CtrlEnable(cmdFSView,1,1)
     LET selFile$=sVal$
    ELSE
     CALL CtrlEnable(cmdFSView,0,1)
    ENDIF
   ENDIF
   SW.BREAK
 SW.END
REPEAT
GOTO fsx,ClosePgm,SBR_OptimiseRtn,SelReload
!
ClosePgm:
GR.CLOSE
END ""
!
SBR_OptimiseRtn:
IF bProgPanel=0 THEN
 LET fraPP=AddControl(bcFRMFRAME,"Optimise Progress", ~
     bcBLUE,bcLGRAY,bcBLACK,bcLGRAY, ~
     20,30,0,550,720,30, ~
     bcHIDE$+bcCAPBOLD$+bcALIGNCENTRE$+bc3DBORDER$+bcFADEBACK$+bcCTRLCENTRE$+bcCAPITALIC$)
 LET lstPP=AddControl(bcFRMLISTBOX," ", ~
     bcLGRAY,bcGRAY,bcBLACK,bcLGRAY, ~
     20,30,30,550,650,20,bcALIGNCENTRE$+bcHIDEGRID$+bcCTRLCENTRE$+bcNOHEADBOX$)
 LET cmdPP=AddControl(bcFRMBUTTON,"GC-Close.png", ~
     bcBLACK,bcWHITE,0,0, ~
     680,40,0,100,50,24,bcCTRLCENTRE$+bcBEVEL$+bcCAPITALIC$+bcGRAPHIC$)
 CALL DrawForm("HG","-")
 LET bProgPanel=1
ENDIF
LET pp$=""
CALL SetCtrlCap(lstPP,pp$)
CALL CtrlEnable(cmdPP,0,1)
CALL CtrlVisible(fraPP,1,1)
GOSUB SBR_PPRtn
CALL CtrlEnable(cmdPP,1,1)
LET exPP=0
WHILE exPP=0
 LET selCtrl=TouchCheck(Period,lstPP,cmdPP)
 SW.BEGIN selCtrl
  SW.CASE cmdPP
   CALL CtrlVisible(fraPP,0,1)
   LET exPP=1
   SW.BREAK
 SW.END
REPEAT
GOTO SelReload

SBR_PPRtn:
LET pp$=pp$+bcRECBREAK$+"*** Processing File: '"+selFile$+"'"
LET pp$=pp$+bcRECBREAK$+"*** Reading Constants File"
CALL ModCtrlCap(lstPP,pp$,1)
GrabFile i$,"../source/"+"GraphicConstants.bas"
SPLIT t$[],i$,chr$(10)
ARRAY.LENGTH tn,t$[]
LET cn=0
FOR i=1 TO tn
 IF LEFT$(t$[i],1)<>"!" THEN
  IF IS_IN("=",t$[i])<>0 THEN LET cn=cn+1
 ENDIF
NEXT i
DIM cdef$[cn]
DIM cnam$[cn]
DIM cval$[cn]
DIM ccod$[cn]
LET cn=0
FOR i=1 TO tn
 IF LEFT$(t$[i],1)<>"!" THEN
  IF LEFT$(t$[i],8)="FrmScale" THEN F_N.BREAK
  LET p=IS_IN("=",t$[i])
  IF p<>0 THEN
   LET cn=cn+1
   LET q=IS_IN(" ",t$[i])
   IF q=0 THEN LET cdef$[cn]=t$[i] ELSE LET cdef$[cn]=LEFT$(t$[i],q-1)
   LET cnam$[cn]=LEFT$(cdef$[cn],p-1)
   LET i$=MID$(cdef$[cn],p+1)
   IF q<>0 THEN
    LET cval$[cn]=LEFT$(i$,q-1)
   ELSE
    LET cval$[cn]=i$
   ENDIF
   LET ccod$[cn]=cval$[cn]
   IF cnam$[cn]<>"bcCRLF$" & cnam$[cn]<>"bcLF$" & cnam$[cn]<>"bcDBLQUOTE$" ~
   & cnam$[cn]<>"bcBACKSLASH$" & cnam$[cn]<>"bcPOUNDSIGN$" & cnam$[cn]<>"bcNOTSIGN$" THEN
    IF UPPER$(LEFT$(cval$[cn],5))="CHR$(" THEN
     IF MID$(cval$[cn],8,1)=")" THEN LET m=2 ELSE LET m=3
     LET k=VAL(MID$(cval$[cn],6,m))
     LET ccod$[cn]=CHR$(34)+CHR$(k)+CHR$(34)
    ENDIF
   ENDIF
  ENDIF
 ENDIF
NEXT i
!
LET pp$=pp$+bcRECBREAK$+"    - Number of Constants: "+INT$(cn)
CALL ModCtrlCap(lstPP,pp$,1)
!
LIST.CREATE s,wlst
LIST.CREATE s,elst
!
LET pp$=pp$+bcRECBREAK$+"*** Reading Basic File: "+selFile$
CALL ModCtrlCap(lstPP,pp$,1)
!
GrabFile i$,curPath$+selFile$
!
LET pp$=pp$+bcRECBREAK$+"*** Removing Leading Spaces"
CALL ModCtrlCap(lstPP,pp$,1)
!
DO
 LET j$=i$
 LET i$=REPLACE$(i$,chr$(10)+" ",chr$(10))
UNTIL j$=i$
!
LET pp$=pp$+bcRECBREAK$+"*** Removing Trailing Spaces"
CALL ModCtrlCap(lstPP,pp$,1)
!
DO
 LET j$=i$
 LET i$=REPLACE$(i$," "+chr$(10),chr$(10))
UNTIL j$=i$
!
LET pp$=pp$+bcRECBREAK$+"*** Combining non-commented Split Lines"
CALL ModCtrlCap(lstPP,pp$,1)
!
DO
 LET j$=i$
 LET i$=REPLACE$(i$," ~"+chr$(10),"~"+chr$(10))
UNTIL j$=i$
DO
 LET j$=i$
 LET i$=REPLACE$(i$,"~"+chr$(10),"")
UNTIL j$=i$
!
LET pp$=pp$+bcRECBREAK$+"*** Renaming Internal Functions"
CALL ModCtrlCap(lstPP,pp$,1)
!
LET fc=0
LET p=1
DO
 LET i=IS_IN("FN.DEF @",i$,p)
 IF i<>0 THEN
  LET j=IS_IN("(",i$,i)
  LET j$=MID$(i$,i+7,j-i-7)
  LET fc=fc+1
  LET k$="X"+RIGHT$("0"+INT$(fc),2)
  LET i$=REPLACE$(i$,j$,k$)
  LET p=j
 ENDIF
UNTIL i=0
!
LET pp$=pp$+bcRECBREAK$+"*** Replacing Constants"
CALL ModCtrlCap(lstPP,pp$,1)
!
LET i$=REPLACE$(i$,"chr$(","CHR$(")
LET r=0
LET s=0
LET u=0
FOR i=1 TO cn
 LET p=1
 LET p=IS_IN(cnam$[i]+"=",i$,1)
 WHILE p<>0
  LET p=p+LEN(cnam$[i])+1
  IF MID$(i$,p,LEN(cval$[i]))<>cval$[i] THEN
   LIST.ADD elst,"*** Constant Value Wrong: Include: "+cdef$[i] ~
            +" - Program: "+cnam$[i]+"="+MID$(i$,p,LEN(cval$[i]))
  ENDIF
  LET p=IS_IN(cnam$[i]+"=",i$,p)
 REPEAT
NEXT i
FOR i=1 TO cn
 LET i$=REPLACE$(i$,cdef$[i],"")
 LET i$=REPLACE$(i$,cnam$[i],ccod$[i])
NEXT i
UNDIM t$[]
SPLIT t$[],i$,chr$(10)
ARRAY.LENGTH tn,t$[]
LET j$=CHR$(34)+CHR$(160)+CHR$(34)
LET comblock=0
!
LET pp$=pp$+bcRECBREAK$+"*** Processing "+INT$(tn)+" Source lines..." 
CALL ModCtrlCap(lstPP,pp$,1)
!
LET qq$=bcRECBREAK$
FOR i=1 TO tn
 IF LEFT$(t$[i],2)="!!" THEN
  IF comblock=0 THEN LET comblock=1 ELSE LET comblock=0
 ENDIF
 IF comblock=1 THEN
  LET t$[i]=""
 ELSE
  IF t$[i]<>"" THEN 
   IF LEFT$(t$[i],1)="!" THEN
    LET t$[i]=""
   ELSE
    LET q=0
    IF IS_IN("bc",t$[i]) THEN
     LET s$=LOWER$(t$[i])
     FOR w=1 TO cn
      IF IS_IN(LOWER$(cnam$[w]),s$)<>0 THEN
       LIST.ADD wlst,"  - Possible Constant: "+t$[i]
      ENDIF
     NEXT w
    ENDIF
    LET j=1
    DO
     IF MID$(t$[i],j,1)=CHR$(34) THEN
      LET q=1
      DO
       LET j=j+1
       IF j>LEN(t$[i]) THEN D_U.BREAK
      UNTIL MID$(t$[i],j,1)=CHR$(34)
      IF MID$(t$[i],j,1)=CHR$(34) THEN
       LET q=0
       LET j=j+1
      ENDIF
     ELSE
      IF MID$(t$[i],j,1)=" " THEN
       LET t$[i]=LEFT$(t$[i],j-1)+MID$(t$[i],j+1)
       LET j=j-1
      ELSEIF MID$(t$[i],j,1)="%" THEN
       LET t$[i]=LEFT$(t$[i],j-1)
      ELSEIF MID$(t$[i],j,1)="+" THEN
       LET t$[i]=LEFT$(t$[i],j-1)+CHR$(160)+MID$(t$[i],j+1)
      ENDIF
      LET j=j+1
     ENDIF
    UNTIL j>LEN(t$[i])
    IF q=1 THEN
     LIST.ADD elst,"   - Missing Closing Quote: "+t$[i]
    ELSE
     LET t$[i]=REPLACE$(t$[i],j$,"")
     LET t$[i]=REPLACE$(t$[i],CHR$(160),"+")
     IF RIGHT$(t$[i],1)="~" THEN
      IF i=tn THEN
       LIST.ADD elst,"   - Missing Continuation Line: "+t$[i]
      ELSEIF LEFT$(t$[i+1],1)="!" THEN
       LIST.ADD elst,"   - Continuation Line is a comment?: "+t$[i]
      ELSE
       LET t$[i+1]=LEFT$(t$[i],LEN(t$[i])-1)+t$[i+1]
       LET t$[i]=""
      ENDIF
     ENDIF
    ENDIF
   ENDIF
  ENDIF
 ENDIF
 LET r=r+1
 IF r=100 THEN
  LET s=s+100
  LET r$=FORMAT$("%%%%%",s)+" "
  LET qq$=qq$+r$
  CALL ModCtrlCap(lstPP,pp$+qq$,1)
  LET r=0
  LET u=u+1
  IF u=10 THEN
   LET pp$=pp$+qq$
   LET qq$=bcRECBREAK$
   CALL ModCtrlCap(lstPP,pp$,1)
   LET u=0
  ENDIF
 ENDIF
NEXT i
!
LET r$=FORMAT$("%%%%%",s+r)+" "
LET pp$=pp$+qq$+r$
CALL ModCtrlCap(lstPP,pp$,1)
IF comblock=1 THEN LIST.ADD elst,"   - Missing Closing Comment Block ('!!')"
LIST.SIZE elst,j
LIST.SIZE wlst,k
!
LET pp$=pp$+bcRECBREAK$+"*** "+INT$(k)+" Warning Messages:"
CALL ModCtrlCap(lstPP,pp$,1)
!
FOR i=1 to k
 LIST.GET wlst,i,i$
 LET pp$=pp$+bcRECBREAK$+i$
NEXT i
CALL ModCtrlCap(lstPP,pp$,1)
!
LET pp$=pp$+bcRECBREAK$+"*** "+INT$(j)+" Errors Found:"
CALL ModCtrlCap(lstPP,pp$,1)
!
IF j>0 THEN
 LET pp$=pp$+bcRECBREAK$+"   - No output file written"
 CALL ModCtrlCap(lstPP,pp$,1)
 FOR i=1 to j
  LIST.GET elst,i,i$
  LET pp$=pp$+bcRECBREAK$+i$
 NEXT i
 CALL ModCtrlCap(lstPP,pp$,1)
ELSE
 TEXT.OPEN w,fout,curPath$+"run-"+selFile$
 LET cb=0
 FOR i=1 TO tn
  IF t$[i]<>"" THEN
   TEXT.WRITELN fout,t$[i]
   LET cb=cb+1
  ENDIF
 NEXT i
 TEXT.CLOSE fout
 LET pp$=pp$+bcRECBREAK$+"*** Output file 'Run-"+selFile$+"' written"
 LET pp$=pp$+bcRECBREAK$+"   - containing "+INT$(cb)+" Source lines" 
 CALL ModCtrlCap(lstPP,pp$,1)
ENDIF
RETURN
