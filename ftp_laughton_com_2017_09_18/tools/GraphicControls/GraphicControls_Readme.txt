Info about this program can be found here:
http://rfobasic.freeforums.org/graphic-form-generator-handler-t245.html
http://rfobasic.freeforums.org/graphic-controls-generator-handler-t317.html

FOLDER CONTENTS
---------------

GRAPHIC CONTROLS FILES

 * GraphicControls.bas   ---+--- Required. Place in Basic Source folder.
 * GraphicConstants.bas  ---'

 * GC-Critical.png       ---.
 * GC-Exclamation.png    ---+--- Optional. Only required if MsgBox$ function uses icons,
 * GC-Information.png    ---|    Place in Basic Data folder.
 * GC-Question.png       ---'          

 * GC-HourGlass1.png     ---.
 * GC-HourGlass2.png     ---+--- Optional. Only required if HourGlass function used,
 * GC-HourGlass3.png     ---'    Place in Basic Data folder.

 * GG-Click.ogg          ------- Optional, Sample "Click" sound file.
                                 Place in Basic Data folder.

OTHER FILES

 - Sample Programs:

   * Calcuator.bas       --------- Simple Calculator.
                                   Place in Basic Source folder.

   * GC-Optimiser.bas    --------- Removes spaces, replaces Graphic Control constants with
                                   their literal value, etc., in order to minimise size of
                                   programs using Graphic Controls. 
                                   Place in Basic Source folder.

   * GraphicControlsTestBed.bas -- Graphic Controls Demo/Test Bed. Shows examples all the
                                   available controls.
                                   Place in Basic Source folder.

   * SourceViewer,bas    --------- Simple viewer for Basic Source Files.
                                   Place in Basic Source folder.

   * Temperature,bas     --------- Very Simple Temperature Converter with lots of comments
                                   within the code. Equivalent of a 'Hello World' program.
                                   Place in Basic Source folder.

   * GraphicControlsKB.bas ------- Reference only. Contains removed Graphic Keyboard code.

 - Support Files:

   * GC-Close.png          ---.
   * GC-Help.png           ---+--- Optional. Used by some of the Sample Programs
   * GC-Save.png           ---|    Place in Basic Data folder.
   * GC-Tablet.png         ---'

   * GC-TestFont1.ttf      ------- Optional. Used for testing in TestBed program.
                                   Place in Basic Data folder.

   * GC-Copy.png           ---.
   * GC-Cut.png            ---+--- No longer required unless GraphicControls Keyboard code is
   * GC-Paste.png          ---+    re-instated.
   * GC-Delete.png         ---'

 - Documentation:

   * GraphicControls_Readme.txt -- This file.
   * GraphicControls.pdf  -------- Description of Graphic Controls commands. Needs a lot of
                                   updating. See release notes for details of changes made
                                   since last update
   * GC-QuickRef.pdf      -------- 4 Page guide detailing all the Graphic Controls commands
                                   and constants.
   * GC-FormatingHelp.txt -------- Optional. Shows all Multiline Listbox formatting commands
                                   for using within a Multiline Listbox control.
                                   Place in Basic Data folder.

   * Screenshot_Calculator.jpg                 ---.
   * Screenshot_GC-Optimiser.jpg               ---|
   * Screenshot_SourceViewer.jpg               ---|
   * Screenshot_Temperature.jpg                ---+--- Various Screen Shots.
   * Screenshot_TestBed-ListboxTestPanel.jpg   ---|
   * Screenshot_TestBed-MainScreen.jpg         ---|
   * Screenshot_TestBed-PictureTestScreen.jpg  ---'
