!***  *******************************************
!***  ***   Start of SourceViewer.bas v1.06   ***
!***  *******************************************

INCLUDE GraphicConstants.bas
INCLUDE GraphicControls.bas

!***  Open graphics screen

CALL InitGraphics(bcOPAQUE,bcLGRAY,bcLANDSCAPE,bcHIDESTATUSBAR,"GCClick.ogg")
!CALL InitGraphics(bcOPAQUE,bcLGRAY,bcPORTRAIT,bcHIDESTATUSBAR,"GCClick.ogg")

!Bundle.Put 1,"swidth",1200  % Override actual screen size for
!Bundle.Put 1,"sheight",750  % testing purposes only

!***  Attempt to use the design size (1200 x 750) & actual screen size to auto size forms
!***  by setting FrmScale value

CALL SetScalingFactor(1200,750,&DateSize,&FrmScale,&MsgBoxFontSize)

LET curPath$="../../../../sdcard/rfo-basic/source/"
LET SectionKey$="!#@#@#@# "
LET sViewCap$=bcCOLBREAK$+"LineNo"+bcFLDBREAK$+"80"+bcFLDBREAK$+"3" ~
  +bcCOLBREAK$+"Code"+bcFLDBREAK$+"830"+bcFLDBREAK$+"1" ~
  +bcCOLBREAK$+"Nest"+bcFLDBREAK$+"50"+bcFLDBREAK$+"3" ~
  +bcCOLBREAK$+"ErrorMsg"+bcFLDBREAK$+"160"+bcFLDBREAK$+"1"+bcRECBREAK$
LET sColSettings$="Source Code: "+selFile$ +bcCOLBREAK$ ~
  +"LineNo"+bcFLDBREAK$+"80"+bcFLDBREAK$+"3"+bcCOLBREAK$ ~
  +"Code"+bcFLDBREAK$+"860"+bcFLDBREAK$+"1"+bcCOLBREAK$ ~
  +"Nest"+bcFLDBREAK$+"50"+bcFLDBREAK$+"3"+bcCOLBREAK$ ~
  +"ErrorMsg"+bcFLDBREAK$+"160"+bcFLDBREAK$+"1"+bcRECBREAK$+" "
LET sIndentSet$="Indent"+bcRECBREAK$+"None"+bcRECBREAK$+"1"+bcRECBREAK$+"2"+bcRECBREAK$+"3" ~
  +bcRECBREAK$+"4"+bcRECBREAK$+"5"+bcRECBREAK$+"6"+bcRECBREAK$+"7"+bcRECBREAK$+"8" ~
  +bcRECBREAK$+"9"+bcRECBREAK$+"10"

!=================================================
!===   D I S P L A Y   F I L E   S E L E C T   ===
!=================================================
DisplayFileSelect:
CALL StartNewForm(DateSize,bcSOUNDON$,FrmScale,MsgBoxFontSize,bcLMAGENTA)
LET lblFSHead=AddControl(bcFRMLABEL,"Basic! Source File Viewer",bcBLACK,0,0,0, ~
  20,0,2,1200,0,30, bcCAPBOLD$+bcALIGNCENTRE$+bcCTRLCENTRE$)
LET lblFSSHead=AddControl(bcFRMLABEL,"Select Source File",bcBLACK,0,0,0, ~
  50,0,2,1200,0,20, bcCAPBOLD$+bcALIGNCENTRE$+bcCTRLCENTRE$)
LET dspFSFolder=AddControl(bcFRMDISPLAY,"Selected Folder",bcBLACK,bcGRAY,bcBLACK,bcLCYAN, ~
  70,50,160,1100,40,20, bcCAPBOLD$+bcCTRLCENTRE$)
LET lstFSContents=AddControl(bcFRMLISTBOX,"Contents",bcWHITE,bcGRAY,bcBLACK,bcWHITE, ~
  120,50,50,1100,550,20, bcFILEDIALOG$+bcCTRLCENTRE$+bc3DBORDER$+bcQUICKNAVN$+bcBANDING$)
LET cmdFSView=AddControl(bcFRMBUTTON,"View Selected",bcBLACK,bcLBLUE,0,0, ~
  684,40,0,180,50,20,bcCTRLLEFT$)
LET cmdFSClose=AddControl(bcFRMBUTTON,"Close Viewer",bcBLACK,bcLBLUE,0,0, ~
  684,40,0,180,50,20,bcCTRLRIGHT$)
CALL DrawForm("",curPath$)
!
SelReload:
UNDIM dfile$[]
LET sTemp$=MID$(curPath$,12)
CALL SetCtrlData(dspFSFolder,sTemp$)
CALL ModCtrlData(dspFSFolder,sTemp$,0)
DIR curPath$,dfile$[]
ARRAY.LENGTH afc,dfile$[]
IF afc>200 THEN
 LET fc=200
ELSE
 LET fc=afc
ENDIF
UNDIM dfl$[]
DIM dfl$[fc]
LET j=1
FOR i=1 TO afc
 IF j<=fc THEN
  IF RIGHT$(dFile$[i],3)="(d)" THEN
   LET dfl$[j]=dfile$[i]
   LET j=j+1
  ELSEIF LOWER$(RIGHT$(dFile$[i],4))=".bas" THEN
   LET dfl$[j]=dfile$[i]
   LET j=j+1
  ENDIF
 ENDIF
NEXT i
IF LEN(curPath$)=12 THEN
 LET sDotDot$=""
ELSE
 LET sDotDot$="..  <Goto Parent Folder>"+bcRECBREAK$
ENDIF
LET sTemp$="Contents"+bcRECBREAK$+sDotDot$+join$(dfl$[],bcRECBREAK$)
UNDIM dfl$[]
CALL SetCtrlData(lstFSContents,"")
CALL ModCtrlCap(lstFSContents,sTemp$,0)
CALL CtrlEnable(cmdFSView,0,1)
!
LET fsx=0
WHILE fsx=0
 LET selCtrl=TouchCheck(Period,0,0)
 SW.BEGIN selCtrl
  SW.CASE cmdFSClose
   UNDIM dfile$[]
   LET fsx=1
   SW.BREAK
  SW.CASE cmdFSView
   LET fsx=2
   SW.BREAK
  SW.CASE lstFSContents
   LET sVal$=GetCtrlData$(lstFSContents)
   IF LEFT$(sVal$,2)=".." THEN
    LET curPath$=LEFT$(curPath$,LEN(curPath$)-1)
    WHILE RIGHT$(curPath$,1)<>"/"
     LET curPath$=LEFT$(curPath$,LEN(curPath$)-1)
    REPEAT
    LET fsx=3
   ELSEIF RIGHT$(sVal$,3)="(d)" THEN
    LET sVal$=LEFT$(sval$,LEN(sval$)-3)
    LET curPath$=curPath$+sVal$+"/"
    LET fsx=3
   ELSE
    IF LOWER$(RIGHT$(sVal$,4))=".bas" THEN
     CALL CtrlEnable(cmdFSView,1,1)
     LET selFile$=sVal$
    ELSE
     CALL CtrlEnable(cmdFSView,0,1)
    ENDIF
   ENDIF
   SW.BREAK
 SW.END
REPEAT
GOTO fsx,CloseViewer,DisplayViewerForm,SelReload
!
DisplayViewerForm:
LET ReadFile=1
GOTO LoadViewForm
!
RedisplayViewerForm:
LET ReadFile=0
!
LoadViewForm:
CALL StartNewForm(DateSize,bcSOUNDON$,FrmScale,MsgBoxFontSize,bcLCYAN)
LET lblVFHead=AddControl(bcFRMLABEL,"Source Viewer / Reformater",bcBLACK,bcLBLUE,0,bcLRED, ~
  2,5,0,1200,40,24,bcCAPBOLD$+bcALIGNCENTRE$+bc3DBORDER$)
LET lstVFSrcCode=AddControl(bcFRMLISTBOX,sColSettings$,bcWHITE,bcGRAY,bcBLACK,bcLCYAN, ~
  40,10,30,1190,630,20,bcLISTVIEW$+bcALIGNCENTRE$+bcQUICKNAVN$+bcDATFONTMONO$+bcBANDING$)
LET selVFIndent=AddControl(bcFRMSELECT,sIndentSet$,bcBLACK,bcLMAGENTA,bcBLACK,bcLMAGENTA, ~
  686,10,80,230,50,20, bcCAPBOLD$+bcDATBOLD$)
LET cmdVFFormat=AddControl(bcFRMBUTTON,"Format",bcBLACK,bcLMAGENTA,0,0, ~
  686,260,0,120,50,20,bcCAPBOLD$)
LET lblVFErrCount=AddControl(bcFRMLABEL,"",bcBLACK,0,0,0, ~
  676,400,0,120,50,20,bcCAPBOLD$+bcALIGNCENTRE$)
LET lblVFLineCount=AddControl(bcFRMLABEL,"",bcBLACK,0,0,0, ~
  700,400,0,120,50,20,bcCAPBOLD$+bcALIGNCENTRE$)
LET cmdVFSave=AddControl(bcFRMBUTTON,"Save",bcBLACK,bcLGREEN,0,0, ~
  686,540,0,120,50,20,bcCAPBOLD$+bcDISABLED$)
LET cmdVFView=AddControl(bcFRMBUTTON,"Change View",bcBLACK,bcLYELLOW,0,0, ~
  686,680,0,160,50,20,bcCAPBOLD$)
LET cmdVFSelect=AddControl(bcFRMBUTTON,"Select New Srce",bcBLACK,bcLBLUE,0,0, ~
  686,860,0,180,50,20,bcCAPBOLD$)
LET cmdVFClose=AddControl(bcFRMBUTTON,"Close Viewer",bcBLACK,bcLRED,0,0, ~
  686,1060,0,140,50,20,bcCAPBOLD$)
LET cmdVFFirst=AddControl(bcFRMBUTTON,"",bcBLACK,bcLYELLOW,0,0, ~
  686,20,0,180,50,20,bcCAPBOLD$+bcCTRLLEFT$+bcHIDE$)
LET cmdVFNext=AddControl(bcFRMBUTTON,"",bcBLACK,bcLYELLOW,0,0, ~
  686,20,0,180,50,20,bcCAPBOLD$+bcCTRLRIGHT$+bcHIDE$)
LET cmdVFEndView=AddControl(bcFRMBUTTON,"Exit View Mode",bcBLACK,bcLRED,0,0, ~
  686,240,0,180,50,20,bcCAPBOLD$+bcCTRLRIGHT$+bcHIDE$)
!
LET fraViewOpts=AddControl(bcFRMFRAME,"View Options",bcBLACK,bcLGREEN,0,bcLBLUE, ~
  5,5,0,1200,706,20,bcCAPBOLD$+bc3DBORDER$+bcALIGNCENTRE$+bcHIDE$+bcFADEBACK$)
LET optVOOpts=AddControl(bcFRMOPTBUTTON,"",bcBLACK,bcGRAY,bcBLACK,bcLGRAY, ~
  42,10,100,1080,50,20,bcCAPBOLD$+bcDISABLED$)
LET lvwVOView=AddControl(bcFRMLISTBOX,"Function"+sViewCap$,bcGRAY,bcLGRAY,bcBLACK,bcLCYAN, ~
  100,10,30,1190,300,20,bcCAPBOLD$+bcLISTVIEW$+bcCHECKBOX$+bcALIGNCENTRE$+bcQUICKNAVN$+bcDATFONTMONO$)
LET optVOView=AddControl(bcFRMOPTBUTTON,"",bcBLACK,bcLBLUE,bcBLACK,bcLBLUE, ~
  430,10,80,1180,120,20,bcCAPBOLD$+bcNOBORDER$)
LET txtVOView=AddControl(bcFRMSTRING,"String",bcBLACK,bcLGRAY,bcBLACK,bcWHITE, ~
  486,560,100,380,40,20,"")
LET lblVOSection=AddControl(bcFRMLABEL,"Sections:",bcBLACK,0,0,0, ~
  590,10,0,400,20,20,bcCAPBOLD$+bcDISABLED$)
LET lblVOSecInst=AddControl(bcFRMLABEL,"Select required sections to be extracted.",bcBLACK,0,0,0, ~
  590,200,0,500,20,20,bcDISABLED$)
LET cmdVOAction=AddControl(bcFRMBUTTON,"Enter View Mode",bcBLACK,bcLGREEN,0,0, ~
  640,100,0,400,50,20,"")
LET cmdVOCancel=AddControl(bcFRMBUTTON,"Close",bcBLACK,bcLRED,0,0, ~
  640,560,0,200,50,20,bcCAPBOLD$)
CALL SetCtrlCap(optVOOpts,"Options:"+bcRECBREAK$+"Set View Type"+bcRECBREAK$+"Extract Sections")
CALL SetCtrlCap(optVOView,"Views:"+bcRECBREAK$+"Function (Show List of Functions)"+bcRECBREAK$ ~
  +"Error (Show List of Errors)"+bcRECBREAK$+"String (Show Lines containing entered Text)")
CALL SetCtrlData(selVFIndent,"2")
CALL SetCtrlData(optVOOpts,"Set View Type")
CALL SetCtrlData(optVOView,"String (Show Lines containing entered Text)")
CALL DrawForm("",curPath$)
!
LET FirstMatch$=""
IF ReadFile=0 THEN
 GOTO LoadViewData
ENDIF
LET IndentStr$=" "
!
LoadSourceCode:
CALL ModCtrlCap(lblVFErrCount,"Loading",1)
LET HGNo=1
CALL HourGlass_Show(HGNo,1)
LET curIndent$=""
LET nxtIndent$=""
LET Comment=0
LET CntIfs=0
LET CntFor=0
LET CntSW=0
LET CntFN=0
LET CntWhile=0
LET CntDo=0
LET curNest=0
LET nxtNest=0
LET incFN=0
LET zeroChk=0
LET CntErr=0
LET sDat$=""
GRABFILE fDat$,curPath$+selFile$
LET bcChr10$=CHR$(10)
LET fDat$=REPLACE$(fDat$,bcCRLF$,bcRECBREAK$)
LET fDat$=REPLACE$(fDat$,bcChr10$,bcRECBREAK$)
UNDIM fLine$[]
SPLIT fLine$[],fDat$,bcRECBREAK$
ARRAY.LENGTH LCnt,fLine$[]
LET Err$=" "
FOR lNo=1 TO LCnt
 LET sLine$=LOWER$(REPLACE$(fLine$[lNo],CHR$(9),""))
 IF sLine$<>"" THEN
  LET i=1
  WHILE MID$(sLine$,i,1)=" " & i<=LEN(sLine$)
   LETi=i+1
  REPEAT
  IF i>LEN(sLine$) THEN F_N.CONTINUE
  LET sLine$=MID$(sLine$,i)
  IF LEFT$(sLine$,2)="!!" THEN
   IF Comment=0 THEN
    LET Comment=1
   ELSE
    LET Comment=0
   ENDIF
  ENDIF
  IF Comment=0 THEN
   IF LEFT$(sLine$,1)<>"!" THEN
    IF IndentStr$<>"" THEN
      IF LEFT$(sLine$,6)="fn.def" THEN
       LET incFN=2
       LET zeroChk=1
       GOSUB SetNxtIndent
      ELSEIF LEFT$(sLine$,2)="if" THEN % check for then
       LET j=0
       LET i=IS_IN("then",sLine$)
       IF i<>0 THEN
        LET i=i+4
        WHILE MID$(sLine$,i,1)=" " & i<=LEN(sLine$)
         LET i=i+1
        REPEAT
        IF i<=LEN(sLine$) THEN
         IF MID$(sLine$,i,1)<>"%" THEN
          LET j=1
         ENDIF
        ENDIF
       ENDIF
       IF j=0 THEN
        LET CntIfs=CntIfs+1
        GOSUB SetNxtIndent
       ENDIF
      ELSEIF LEFT$(sLine$,3)="for" THEN
       LET CntFor=CntFor+1
       GOSUB SetNxtIndent
      ELSEIF LEFT$(sLine$,5)="while" THEN
       LET CntWhile=CntWhile+1
       GOSUB SetNxtIndent
      ELSEIF LEFT$(sLine$,2)="do" THEN
       LET CntDO=CntDO+1
       GOSUB SetNxtIndent
      ELSEIF LEFT$(sLine$,8)="sw.begin" THEN
       LET nxtIndent$=nxtIndent$+IndentStr$
       LET curNest=curNest+1
       IF CntSW>0 THEN
        LET Err$="Nested SW.BEGIN"
       ENDIF
       LET CntSW=1
       GOSUB SetNxtIndent
      ELSEIF LEFT$(sLine$,6)="elseif" THEN
       GOSUB SetCurNxtIndents
      ELSEIF LEFT$(sLine$,4)="else" THEN
       GOSUB SetCurNxtIndents
      ELSEIF LEFT$(sLine$,7)="sw.case" THEN
       GOSUB SetCurNxtIndents
      ELSEIF LEFT$(sLine$,10)="sw.default" THEN
       GOSUB SetCurNxtIndents
      ELSEIF LEFT$(sLine$,6)="fn.rtn" THEN
       LET CntFN=CntFN-1
       IF CntFN<>1 THEN
        LET Err$="Unmatched FN.RTN"
        LET CntFN=0
       ENDIF
      ELSEIF LEFT$(sLine$,6)="fn.end" THEN
       LET CntFN=CntFN-1
       IF CntFN<>0 THEN
        LET Err$="Un FN.END "+STR$(cntfn)
        LET CntFN=0
       ENDIF
       LET zeroChk=1
       GOSUB SetCurIndent
      ELSEIF LEFT$(sLine$,5)="endif" THEN
       LET CntIfs=CntIfs-1
       IF CntIfs<0 THEN
        LET Err$="Unmatched ENDIF"
        LET CntIfs=0
       ENDIF
       GOSUB SetCurIndent
      ELSEIF LEFT$(sLine$,6)="end if" THEN
       LET CntIfs=CntIfs-1
       IF CntIfs<0 THEN
        LET Err$="Unmatched ENDIF"
        LET CntFN=0
       ENDIF
       GOSUB SetCurIndent
      ELSEIF LEFT$(sLine$,4)="next" THEN
       LET CntFor=CntFor-1
       IF CntFor<0 THEN
        LET Err$="Unmatched NEXT"
        LET CntFor=0
       ENDIF
       GOSUB SetCurIndent
      ELSEIF LEFT$(sLine$,6)="repeat" THEN
       LET CntWhile=CntWhile-1
       IF CntIfs<0 THEN
        LET Err$="Unmatched REPEAT"
        LET CntWhile=0
       ENDIF
       GOSUB SetCurIndent
      ELSEIF LEFT$(sLine$,5)="until" THEN
       LET CntDO=CntDO-1
       IF CntDO<0 THEN
        LET Err$="Unmatched UNTIL"
        LET CntDO=0
       ENDIF
       GOSUB SetCurIndent
      ELSEIF LEFT$(sLine$,6)="sw.end" THEN
       LET curIndent$=LEFT$(curIndent$,LEN(curIndent$)-LEN(IndentStr$))
       LET curNest=curNest-1
       LET CntSW=CntSW-1
       IF CntSW<0 THEN
        LET Err$="Unmatched SW.END"
        LET CntSW=0
       ENDIF
       GOSUB SetCurIndent
      ENDIF
    ENDIF
   ENDIF
  ENDIF
 ENDIF
 LET num$=RIGHT$(FORMAT$("%%%%",lNo),4)
 LET sDat$=sDat$+num$+bcCOLBREAK$+curIndent$+fLine$[lNo]+bcCOLBREAK$+FORMAT$("#%",curNest) ~
   +bcCOLBREAK$+Err$ +bcRECBREAK$
 IF MOD(lNo,100)=0 THEN
  CALL ModCtrlCap(lblVFLineCount,"Lines:"+FORMAT$("###%",lNo),1)
  IF HGNo=3 THEN
   LET HGNo=1
  ELSE
   LET HGNo=HGNo+1
  ENDIF
  CALL HourGlass_Show(HGNo,0)
 ENDIF
 IF Err$<>" " THEN
  LET CntErr=CntErr+1
  LET Err$=" "
 ENDIF
 IF zeroChk=1 THEN
  IF curNest<>0 THEN
   LET sDat$=sDat$+"****"+bcCOLBREAK$+"Error: Nest Level not zero."+bcCOLBREAK$+bcCOLBREAK$+bcRECBREAK$
   LET CntErr=CntErr+1
  ENDIF
  IF CntIfs<>0 THEN
   LET sDat$=sDat$+"****"+bcCOLBREAK$+"Error: Missing ENDIF."+bcCOLBREAK$+bcCOLBREAK$+bcRECBREAK$
   LET CntErr=CntErr+1
  ENDIF
  IF CntFor<>0 THEN
   LET sDat$=sDat$+"****"+bcCOLBREAK$+"Error: Missing NEXT."+bcCOLBREAK$+bcCOLBREAK$+bcRECBREAK$
   LET CntErr=CntErr+1
  ENDIF
  IF CntSW<>0 THEN
   LET sDat$=sDat$+"****"+bcCOLBREAK$+"Error: Missing SW.END."+bcCOLBREAK$+bcCOLBREAK$+bcRECBREAK$
   LET CntErr=CntErr+1
  ENDIF
  IF CntFN<>0 THEN
   LET sDat$=sDat$+"****"+bcCOLBREAK$+"Error: Missing FN.END."+bcCOLBREAK$+bcCOLBREAK$+bcRECBREAK$
   LET CntErr=CntErr+1
  ENDIF
  IF CntWhile<>0 THEN
   LET sDat$=sDat$+"****"+bcCOLBREAK$+"Error: Missing REPEAT."+bcCOLBREAK$+bcCOLBREAK$+bcRECBREAK$
   LET CntErr=CntErr+1
  ENDIF
  IF CntDo<>0 THEN
   LET sDat$=sDat$+"****"+bcCOLBREAK$+"Error: Missing UNTIL."+bcCOLBREAK$+bcCOLBREAK$+bcRECBREAK$
   LET CntErr=CntErr+1
  ENDIF
  IF Comment<>0 THEN
   LET sDat$=sDat$+"****"+bcCOLBREAK$+"Error: Missing End Comment (!!)."+bcCOLBREAK$ ~
     +bcCOLBREAK$+bcRECBREAK$
   LET CntErr=CntErr+1
  ENDIF
  LET zeroChk=0
 ENDIF
 LET curIndent$=nxtIndent$
 LET curNest=nxtNest
 LET CntFN=CntFN+IncFN
 LET incFN=0
NEXT LCnt
CALL HourGlass_Hide(1)
!
LoadViewData:
IF CntErr=0 THEN
 LET Err$="No Errors"
 LET e=0
ELSE
 LET Err$=RIGHT$(FORMAT$("%%%%",CntErr),4)+" Errors"
 LET e=1
ENDIF
CALL CtrlEnable(cmdVFSave,e,0)
CALL ModCtrlCap(lblVFErrCount,Err$,0)
CALL ModCtrlCap(lblVFLineCount,"Lines:"+FORMAT$("###%",lNo-1),0)
CALL SetCtrlData(lstVFSrcCode,SelMatch$)
LET sCap$=sColSettings$+sDat$
CALL ModCtrlCap(lstVFSrcCode,sCap$,1)
UNDIM sLn$[]
SPLIT sLn$[],sDat$,bcRECBREAK$
ARRAY.LENGTH LnCnt,sLn$[]
LET curLine=GetSelectedIdx(lstVFSrcCode)
GOTO ViewWaitForTouch
!
LoadViewList:
CALL SetCtrlData(lvwVOView,"")
LET rcOption$=GetCtrlData$(optVOOpts)
IF rcOption$="Extract Sections" THEN
 LET sCap$="Sections"
 CALL ModCtrlCap(cmdVOAction,"Extract & Save Selected Sections",1)
ELSE
 CALL ModCtrlCap(cmdVOAction,"Enter View Mode",1)
 LET rcView$=GetCtrlData$(optVOView)
 IF LEFT$(rcView$,8)="Function" THEN
  LET sCap$="Functions"
  LET bc1$="First Function"
  LET bc2$="Next Function"
 ELSEIF LEFT$(rcView$,5)="Error" THEN
  LET sCap$="Error Lines"
  LET bc1$="First Error"
  LET bc2$="Next Error"
 ELSE % "String"
  LET sViewString$=GetCtrlData$(txtVOView)
  LET sViewString$=LOWER$(sViewString$)
  LET sCap$="Lines containing String '"+sViewString$+"'"
  LET bc1$="First String Match"
  LET bc2$="Next String Match"
 ENDIF
 CALL ModCtrlCap(cmdVFFirst,bc1$,0)
 CALL ModCtrlCap(cmdVFNext,bc2$,0)
ENDIF
LET sTemp$=REPLACE$(sViewCap$,bcCOLBREAK$+"Code"+bcFLDBREAK$,bcCOLBREAK$+sCap$+bcFLDBREAK$)
LET sCap$=sCap$+sTemp$
LET capLen=LEN(sCap$)
LET SelMatch$=""
LET i$=""
FOR i=1 TO lnCnt
 UNDIM fld$[]
 SPLIT fld$[],sLn$[i],bcCOLBREAK$
 LET lcfld$=LOWER$(fld$[2])
 IF rcOption$="Extract Sections" THEN
  LET j=IS_IN(SectionKey$,fld$[2])
  IF j=0 THEN
   F_N.CONTINUE
  ENDIF
  LET fld$[2]=MID$(fld$[2],LEN(SectionKey$)+1)
 ELSE
  IF rcView$="Function" THEN
   IF LEFT$(lcfld$,6)<>"fn.def" THEN
    F_N.CONTINUE
   ENDIF
  ELSEIF rcView$="Error" THEN
   IF fld$[4]=" " THEN
    F_N.CONTINUE
   ENDIF
  ELSE % "String"
   IF sViewString$="" THEN
    F_N.CONTINUE
   ELSE
    LET j=IS_IN(sViewString$,lcfld$)
    IF j=0 THEN
     F_N.CONTINUE
    ENDIF
   ENDIF
  ENDIF
 ENDIF
 IF i$<>"" THEN LET i$=i$+bcRECBREAK$
 LET i$=i$+fld$[1]+bcCOLBREAK$+fld$[2]+bcCOLBREAK$+fld$[3]+bcCOLBREAK$+fld$[4]
NEXT i
CALL ModCtrlCap(lvwVOView,sCap$+i$,0)
IF i$<>"" THEN LET e=1 ELSE LET e=0
CALL CtrlEnable(cmdVOAction,e,1)
!
ViewWaitForTouch:
LET vfx=0
WHILE vfx=0
 LET selCtrl=TouchCheck(Period,0,0)
 SW.BEGIN selCtrl
  SW.CASE cmdVFSelect
   LET vfx=1
   SW.BREAK
  SW.CASE cmdVFClose
   LET vfx=2
   SW.BREAK
  SW.CASE cmdVFSave
   LET outFile$=LEFT$(selFile$,LEN(selFile$)-4)+" (fmt)"+RIGHT$(selFile$,4)
   TEXT.OPEN W, FoOut, curPath$+outFile$
   UNDIM sLin$[]
   SPLIT sLin$[],sDat$,bcRECBREAK$
   ARRAY.LENGTH lcnt,sLin$[]
   FOR i=1 TO lcnt
    SPLIT col$[],sLin$[i],bcCOLBREAK$
    TEXT.WRITELN FoOut,col$[2]
    UNDIM col$[]
   NEXT i
   TEXT.CLOSE FoOut
   CALL MsgBox$("Formatted version of Source File:"+bcRECBREAK$+bcRECBREAK$ ~
        +MID$(curPath$,12)+selFile$+bcRECBREAK$+bcRECBREAK$+"written to new file:"+bcRECBREAK$ ~
        +bcRECBREAK$+MID$(curPath$,12)+outFile$,bcOKONLY$+bcINFORMATION$+bcCLRINFO$,"Save File")
   SW.BREAK
  SW.CASE cmdVFFormat
   LET i$=GetCtrlData$(selVFIndent)
   IF i$="None" THEN
    LET IndentStr$=""
   ELSE
    LET IndentStr$=LEFT$("          ",VAL(i$)-1)
   ENDIF
   LET vfx=3
   SW.BREAK
  SW.CASE cmdVFView
   CALL CtrlVisible(fraViewOpts,1,0)
   IF LOWER$(selFile$)="xgraphiccontrols.bas" THEN LET e=1 ELSE LET e=0
   CALL CtrlEnable(optVOOpts,e,1)
   LET vfx=4
   SW.BREAK
  SW.CASE lvwVOView
   LET SelMatch$=GetCtrlData$(lvwVOView)
   SW.BREAK
  SW.CASE optVOOpts
   LET rcOption$=GetCtrlData$(optVOOpts)
   IF rcOption$="Extract Sections" THEN
    LET e=0
    LET f=1  
    CALL ModCtrlCap(cmdVOAction,"Extract & Save Selected Sections",1)
   ELSE
    LET e=1
    LET f=0  
    CALL ModCtrlCap(cmdVOAction,"Enter View Mode",1)
   ENDIF
   CALL CtrlEnable(optVOView,e,0)
   CALL CtrlEnable(txtVOView,e,0)
   CALL CtrlEnable(lblVOSection,f,0)
   CALL CtrlEnable(lblVOSecInst,f,0)
  SW.CASE txtVOView
  SW.CASE optVOView
   LET vfx=4
   SW.BREAK
  SW.CASE cmdVOAction
   IF rcOption$="Set View Type" THEN
    LET i$=GetCtrlData$(lvwVOView)
    IF i$<>"" THEN
     CALL CtrlVisible(fraViewOpts,0,0)
     LET i$=MID$(i$,IS_IN(bcRECBREAK$,i$)+1)
     CALL ModCtrlData(lstVFSrcCode,i$,0)
     LET e=0
     LET f=1
     GOSUB SetViewButtons
    ENDIF
   ELSE
    LET i$=GetCtrlCap$(lvwVOView)
    UNDIM sExt$[]
    SPLIT sExt$[],i$,bcRECBREAK$
    ARRAY.LENGTH ECnt,sExt$[]
    LET outFile$=LEFT$(selFile$,LEN(selFile$)-4)+" (ext)"+RIGHT$(selFile$,4)
    TEXT.OPEN W, FoOut, curPath$+outFile$
    UNDIM sLin$[]
    SPLIT sLin$[],sDat$,bcRECBREAK$
    ARRAY.LENGTH LCnt,sLin$[]
    LET k=0
    FOR i=1 TO LCnt
     SPLIT col$[],sLin$[i],bcCOLBREAK$
     IF LEFT$(col$[2],10)="!=-=-=-=-=" THEN
      LET k=1
     ELSE
      LET p=IS_IN(SectionKey$,col$[2])
      IF p<>0 THEN
       LET k=0
       FOR j=1 TO ECnt
        UNDIM sSec$[]
        SPLIT sSec$[],sExt$[j],bcCOLBREAK$
        IF sSec$[2]=MID$(col$[2],p+LEN(SectionKey$)) THEN
         IF LEFT$(sSec$[1],3)="[X]" THEN
          LET k=1
         ENDIF
         LET j=ECnt
        ENDIF
       NEXT j
      ENDIF
     ENDIF
     IF k=1 THEN
      TEXT.WRITELN FoOut,col$[2]
     ENDIF
     UNDIM col$[]
    NEXT i
    TEXT.CLOSE FoOut
    CALL MsgBox$("Customised version of GraphicControls.bas"+bcRECBREAK$+bcRECBREAK$ ~
          +"written to new file:"+bcRECBREAK$+bcRECBREAK$+MID$(curPath$,12)+outFile$, ~
          bcOKONLY$+bcINFORMATION$+bcCLRINFO$,"Extract Graphic Controls")
   ENDIF
   SW.BREAK
  SW.CASE cmdVOCancel
   CALL CtrlVisible(fraViewOpts,0,1)
   SW.BREAK
  SW.CASE cmdVFFirst
   LET curLine=0
   GOSUB FirstNextRtn
   SW.BREAK
  SW.CASE cmdVFNext
   LET i$=GetCtrlData$(lstVFSrcCode)
   LET i$=MID$(i$,IS_IN(bcRECBREAK$,i$)+1)
   FOR i=1 TO lnCnt
    IF sLn$[i]=i$ THEN
     LET curLine=i
     LET i=lnCnt
    ENDIF
   NEXT i
   GOSUB FirstNextRtn
   SW.BREAK
  SW.CASE cmdVFEndView
   LET e=1
   LET f=0
   GOSUB SetViewButtons
   SW.BREAK
 SW.END
REPEAT
GOTO vfx,DisplayFileSelect,CloseViewer,LoadSourceCode,LoadViewList
!
SetViewButtons:
CALL CtrlVisible(selVFIndent,e,0)
CALL CtrlVisible(cmdVFFormat,e,0)
CALL CtrlVisible(lblVFErrCount,e,0)
CALL CtrlVisible(lblVFLineCount,e,0)
CALL CtrlVisible(cmdVFSave,e,0)
CALL CtrlVisible(cmdVFView,e,0)
CALL CtrlVisible(cmdVFSelect,e,0)
CALL CtrlVisible(cmdVFClose,e,0)
CALL CtrlVisible(cmdVFFirst,f,0)
CALL CtrlVisible(cmdVFNext,f,0)
CALL CtrlVisible(cmdVFEndView,f,1)
RETURN
!
SetNxtIndent:
LET nxtIndent$=nxtIndent$+IndentStr$
LET nxtNest=curNest+1
RETURN
!
SetCurNxtIndents:
LET nxtIndent$=curIndent$
LET curIndent$=LEFT$(curIndent$,LEN(curIndent$)-LEN(IndentStr$))
LET nxtNest=curNest
LET curNest=curNest-1
RETURN
!
SetCurIndent:
LET curIndent$=LEFT$(curIndent$,LEN(curIndent$)-LEN(IndentStr$))
LET nxtIndent$=curIndent$
LET curNest=curNest-1
LET nxtNest=curNest
RETURN
!
FirstNextRtn:
LET i$=""
!
RestartFNRtn:
FOR i=curLine+1 TO lnCnt
 UNDIM fld$[]
 SPLIT fld$[],sLn$[i],bcCOLBREAK$
 LET lcfld$=LOWER$(fld$[2])
 IF rcView$="Function" THEN
  IF LEFT$(lcfld$,6)="fn.def" THEN
   LET i$=sLn$[i]
   F_N.BREAK
  ENDIF
 ELSEIF rcView$="Error" THEN
  IF fld$[4]<>" " THEN
   LET i$=sLn$[i]
   F_N.BREAK
  ENDIF
 ELSE % "String"
  IF sViewString$<>"" THEN
   LET j=IS_IN(sViewString$,lcfld$)
   IF j<>0 THEN
    LET i$=sLn$[i]
    F_N.BREAK
   ENDIF
  ENDIF
 ENDIF
NEXT i
IF i$="" THEN
 IF curLine<>0 THEN
  LET curLine=0
  GOTO RestartFNRtn
 ENDIF
ELSE
 CALL SetCtrlData(lstVFSrcCode,i$)
 CALL ModCtrlCap(lstVFSrcCode,sColSettings$+sDat$,1)
ENDIF
RETURN
!
CloseViewer:
GR.CLOSE
END ""

!********************************
!***   End SourceViewer.bas   ***
!********************************
