REM Calculator v2.1 Sample Program

INCLUDE GraphicConstants.bas
INCLUDE GraphicControls.bas

GOTO StartProgram
!
ONBACKKEY:
LET i=getvkeybstate()
IF i=1 THEN
 GOTO bkpstart
ELSE
 BACK.RESUME
ENDIF
!
StartProgram:
!CALL InitGraphics(bcOPAQUE,bcLGRAY,bcLANDSCAPE,bcHIDESTATUSBAR,"GCClick.ogg")
CALL InitGraphics(bcOPAQUE,bcLGRAY,bcPORTRAIT,bcHIDESTATUSBAR,"GCClick.ogg")
!
CALL SetScalingFactor(440,752,&DateSize,&FrmScale,&MsgBoxFontSize)
!
LET dblOperand1=0
LET dblOperand2=0
LET dblMemory=0
LET bOptions=0
LET Operator$=""
LET bClearDisplay=0
LET sDisplayValue$="0"
LET Space1$=CHR$(160)+CHR$(160)+CHR$(160)
LET Space2$=Space1$+CHR$(160)+CHR$(160)
FILE.ROOT i$
LET curPath$=""
FOR i=1 TO LEN(i$)
 IF MID$(i$,i,1)="/" THEN LET curPath$=curPath$+"../"
NEXT i
LET RootPathLen=LEN(curPath$)
LET HistCap$="Calc History"+bcRECBREAK$
LET HistTxt$=string$(29,bcRECBREAK$)+CHR$(160)
LET Space50$=string$(50," ")
!
CALL StartNewForm(DateSize,bcSOUNDON$,FrmScale,MsgBoxFontSize,bcLGRAY)
LET shpCalc=AddControl(bcFRMSHAPE,"rect", ~
    bcYELLOW,bcRED,bcBLUE,bcLRED,10,10,0,419,732,0,bcSHAPEFILL$)
LET dspCalc=AddControl(bcFRMDISPLAY,"GC Calculator Demo", ~
    bcYELLOW,bcBLUE,0,0,10,10,419,419,39,24,bcALIGNCENTRE$)
LET lstCalcHist=AddControl(bcFRMLISTBOX,HistCap$+HistTxt$, ~
    bcBLACK,bcLGRAY,bcBLACK,bcLCYAN,68,20,0,400,234,19, ~
    bc3DBORDER$+bcDATBOLD$+bcLISTBOXRIGHT$+bcLISTBOXLAST$)
LET dspDisplay=AddControl(bcFRMDISPLAY,sDisplayValue$, ~
    bcBLACK,bcLCYAN,0,0,322,20,400,400,58,29, ~
    bcALIGNRIGHT$+bcDATBOLD$+bc3DBORDER$)
LET dspMark=AddControl(bcFRMDISPLAY,"", ~
    bcBLACK,bcLMAGENTA,bcYELLOW,bcBLUE,415,34,0,29,29,19, ~
    bcALIGNDATCENTRE$+bcCAPITALIC$+bcDATBOLD$+bcNOBORDER$+bcHIDE$)
LET cmdOptions=AddControl(bcFRMBUTTON,"GC-Information.png", ~
    bcBLACK,bcLRED,0,0,10,10,0,39,39,0,bcGRAPHIC$)
LET cmdExit=AddControl(bcFRMBUTTON,"GC-Critical.png", ~
    bcBLACK,bcLRED,0,0,10,389,0,39,39,0,bcGRAPHIC$)
LET cmdC=AddControl(bcFRMBUTTON,"C",bcBLACK,bcLGREEN,0,0,400,88,0,58,58,19,"")
LET cmdCE=AddControl(bcFRMBUTTON,"CE",bcBLACK,bcLGREEN,0,0,400,156,0,58,58,19,"")
LET cmdBS=AddControl(bcFRMBUTTON,"Backspace",bcBLACK,bcLGREEN,0,0,400,224,0,126,58,19,"")
LET cmdSQ=AddControl(bcFRMBUTTON,"Sqrt",bcBLACK,bcLBLUE,0,0,400,361,0,58,58,19,"")
LET cmdMC=AddControl(bcFRMBUTTON,"MC",bcBLACK,bcLMAGENTA,0,0,468,19,0,58,58,19,"")
LET cmd1=AddControl(bcFRMBUTTON,"1",bcBLACK,bcWHITE,0,0,468,88,0,58,58,19,"")
LET cmd2=AddControl(bcFRMBUTTON,"2",bcBLACK,bcWHITE,0,0,468,156,0,58,58,19,"")
LET cmd3=AddControl(bcFRMBUTTON,"3",bcBLACK,bcWHITE,0,0,468,224,0,58,58,19,"")
LET cmdD=AddControl(bcFRMBUTTON,"/",bcBLACK,bcLBLUE,0,0,468,293,0,58,58,19,"")
LET cmdPC=AddControl(bcFRMBUTTON,"%",bcBLACK,bcLBLUE,0,0,468,361,0,58,58,19,"")
LET cmdMR=AddControl(bcFRMBUTTON,"MR",bcBLACK,bcLMAGENTA,0,0,537,19,0,58,58,19,"")
LET cmd4=AddControl(bcFRMBUTTON,"4",bcBLACK,bcWHITE,0,0,537,88,0,58,58,19,"")
LET cmd5=AddControl(bcFRMBUTTON,"5",bcBLACK,bcWHITE,0,0,537,156,0,58,58,19,"")
LET cmd6=AddControl(bcFRMBUTTON,"6",bcBLACK,bcWHITE,0,0,537,224,0,58,58,19,"")
LET cmdX=AddControl(bcFRMBUTTON,"*",bcBLACK,bcLBLUE,0,0,537,293,0,58,58,19,"")
LET cmdPM=AddControl(bcFRMBUTTON,"+/-",bcBLACK,bcLBLUE,0,0,537,361,0,58,58,19,"")
LET cmdMM=AddControl(bcFRMBUTTON,"M-",bcBLACK,bcLMAGENTA,0,0,605,19,0,58,58,19,"")
LET cmd7=AddControl(bcFRMBUTTON,"7",bcBLACK,bcWHITE,0,0,605,88,0,58,58,19,"")
LET cmd8=AddControl(bcFRMBUTTON,"8",bcBLACK,bcWHITE,0,0,605,156,0,58,58,19,"")
LET cmd9=AddControl(bcFRMBUTTON,"9",bcBLACK,bcWHITE,0,0,605,224,0,58,58,19,"")
LET cmdM=AddControl(bcFRMBUTTON,"-",bcBLACK,bcLBLUE,0,0,605,293,0,58,58,19,"")
LET cmdDX=AddControl(bcFRMBUTTON,"1/x",bcBLACK,bcLBLUE,0,0,605,361,0,58,58,19,"")
LET cmdMP=AddControl(bcFRMBUTTON,"M+",bcBLACK,bcLMAGENTA,0,0,673,19,0,58,58,19,"")
LET cmd0=AddControl(bcFRMBUTTON,"0",bcBLACK,bcWHITE,0,0,673,88,0,58,58,19,"")
LET cmdDP=AddControl(bcFRMBUTTON,".",bcBLACK,bcWHITE,0,0,673,156,0,58,58,19,"")
LET cmdP=AddControl(bcFRMBUTTON,"+",bcBLACK,bcLBLUE,0,0,673,224,0,58,58,19,"")
LET cmdEQ=AddControl(bcFRMBUTTON,"=",bcBLACK,bcLGRAY,0,0,673,293,0,126,58,19,"")
CALL SetCtrlData(dspMark,"M")
CALL DrawForm("Drawing Calculator...",picPath$)
!
LET lFirstCtrl=cmdOptions
LET lLastCtrl=cmdEQ
!
FileListReload:
IF bOptions=1 THEN
 UNDIM dfile$[]
 DIR curPath$,dfile$[]
 ARRAY.LENGTH ActFileCnt,dfile$[]
 IF ActFileCnt>200 THEN % show first 200 entries only
  LET DispFileCnt=200
 ELSE
  LET DispFileCnt=ActFileCnt
 ENDIF
 IF LEN(curPath$)=RootPathLen THEN LET i$="" ELSE LET i$="..  <Goto Parent Folder>"
 FOR i=1 TO DispFileCnt
  IF dfile$[1]<>" " THEN
   IF i$<>"" THEN LET i$=i$+bcRECBREAK$
   LET i$=i$+dfile$[i]
  ENDIF
 NEXT i
 LET sTemp$="Folder: "+MID$(curPath$,12)
 LET sTemp$=sTemp$+bcRECBREAK$+i$
 CALL SetCtrlData(lstFolders,"")
 CALL ModCtrlCap(lstFolders,sTemp$,1)
ENDIF
!
CalcWaitForTouch:
cwx=0
WHILE cwx=0
 LET selCtrl=TouchCheck(Period,lFirstCtrl,lLastCtrl)
 SW.BEGIN selCtrl
  SW.CASE cmdExit
   cwx=1
   SW.BREAK
  SW.CASE cmd1
   LET NewTxt$="1"
   GOSUB NewDigit
   SW.BREAK
  SW.CASE cmd2
   LET NewTxt$="2"
   GOSUB NewDigit
   SW.BREAK
  SW.CASE cmd3
   LET NewTxt$="3"
   GOSUB NewDigit
   SW.BREAK
  SW.CASE cmd4
   LET NewTxt$="4"
   GOSUB NewDigit
   SW.BREAK
  SW.CASE cmd5
   LET NewTxt$="5"
   GOSUB NewDigit
   SW.BREAK
  SW.CASE cmd6
   LET NewTxt$="6"
   GOSUB NewDigit
   SW.BREAK
  SW.CASE cmd7
   LET NewTxt$="7"
   GOSUB NewDigit
   SW.BREAK
  SW.CASE cmd8
   LET NewTxt$="8"
   GOSUB NewDigit
   SW.BREAK
  SW.CASE cmd9
   LET NewTxt$="9"
   GOSUB NewDigit
   SW.BREAK
  SW.CASE cmd0
   LET NewTxt$="0"
   GOSUB NewDigit
   SW.BREAK
  SW.CASE cmdDP
   IF IS_IN(".",sDisplayValue$)=0 THEN
    IF bClearDisplay=1 THEN
     LET sDisplayValue$="0"
     LET bClearDisplay=0
    ENDIF
    LET sDisplayValue$=sDisplayValue$+"."
    CALL ModCtrlCap(dspDisplay,sDisplayValue$,1)
   ENDIF
   SW.BREAK
  SW.CASE cmdEQ
   LET bEquals=1
   GOSUB EqualsRoutine
   SW.BREAK
  SW.CASE cmdP
   LET NewOp$="+"
   GOSUB OperationRtn
   SW.BREAK
  SW.CASE cmdM
   LET NewOp$="-"
   GOSUB OperationRtn
   SW.BREAK
  SW.CASE cmdX
   LET NewOp$="*"
   GOSUB OperationRtn
   SW.BREAK
  SW.CASE cmdD
   LET NewOp$="/"
   GOSUB OperationRtn
   SW.BREAK
  SW.CASE cmdSQ
   IF VAL(sDisplayValue$) < 0 THEN
    CALL MsgBox$("No Square Root for Negative numbers!",bcOKONLY$+bcEXCLAMATION$+bcCLRERR$, ~
        "Square Root Operation")
   ELSE
    LET dblWork=SQR(VAL(sDisplayValue$))
    LET sWork$=STR$(dblWork)
    LET NewTxt$="(Sqrt of "+sDisplayValue$+")= "+sWork$+Space2$
    GOSUB HistoryUpdate
    LET sDisplayValue$=sWork$
    CALL ModCtrlCap(dspDisplay,sDisplayValue$,1)
   ENDIF
   SW.BREAK
  SW.CASE cmdPM
   LET sDisplayValue$=STR$(-VAL(sDisplayValue$))
   CALL ModCtrlCap(dspDisplay,sDisplayValue$,1)
   SW.BREAK
  SW.CASE cmdDX
   IF VAL(sDisplayValue$)=0 THEN
    CALL MsgBox$("Divide by Zero Error!",bcOKONLY$+bcEXCLAMATION$+bcCLRERR$,"1/x Operation")
   ELSE
    LET dblWork=1/VAL(sDisplayValue$)
    LET NewTxt$="(1 / "+sDisplayValue$+") = "+STR$(dblWork)+Space2$
    GOSUB HistoryUpdate
    LET sDisplayValue$=STR$(dblWork)
    CALL ModCtrlCap(dspDisplay,sDisplayValue$,1)
   ENDIF
   SW.BREAK
  SW.CASE cmdPC
   IF dblOperand1 <> 0 THEN
    LET dblWork=VAL(sDisplayValue$)
    LET dblWork=(dblOperand1*dblWork)/100
    LET sWork$=STR$(dblWork)
    LET NewTxt$="("+sDisplayValue$+"% of "+STR$(dblOperand1)+") = "+sWork$+Space2$
    GOSUB HistoryUpdate
    LET sDisplayValue$=STR$(dblWork)
    CALL ModCtrlCap(dspDisplay,sDisplayValue$,1)
   ENDIF
   SW.BREAK
  SW.CASE cmdC
   LET sDisplayValue$="0"
   CALL ModCtrlCap(dspDisplay,sDisplayValue$,1)
   LET dblOperand1=0
   LET dblOperand2=0
   LET Operator$=""
   LET NewTxt$="---- Clear ----"+Space2$
   GOSUB HistoryUpdate
   SW.BREAK
  SW.CASE cmdCE
   LET sDisplayValue$="0"
   CALL ModCtrlCap(dspDisplay,sDisplayValue$,1)
   LET bClearDisplay=1
   SW.BREAK
  SW.CASE cmdBS
   IF LEN(sDisplayValue$) > 1 THEN
    LET sDisplayValue$=LEFT$(sDisplayValue$,LEN(sDisplayValue$)-1)
   ELSE
    LET sDisplayValue$="0"
   ENDIF
   CALL ModCtrlCap(dspDisplay,sDisplayValue$,1)
   SW.BREAK
  SW.CASE cmdMC
   LET dblMemory=0
   CALL CtrlVisible(dspMark,0,1)
   SW.BREAK
  SW.CASE cmdMR
   LET sDisplayValue$=STR$(dblMemory)
   CALL ModCtrlCap(dspDisplay,sDisplayValue$,1)
   SW.BREAK
  SW.CASE cmdMP
   LET dblMemory=dblMemory+VAL(sDisplayValue$)
   CALL CtrlVisible(dspMark,1,1)
   SW.BREAK
  SW.CASE cmdMM
   LET dblMemory=dblMemory-VAL(sDisplayValue$)
   CALL CtrlVisible(dspMark,1,1)
   SW.BREAK
  SW.CASE cmdOptions
   IF bOptions=0 THEN
    GOSUB LoadOptions
    CALL DrawForm("Loading Options...",picPath$)
   ENDIF
   LET bOptions=1
   CALL CtrlVisible(fraOptions,1,0)
   LET r=0
   GOSUB SetCtrlState
   LET lFirstCtrl=optSound
   LET lLastCtrl=cmdClose
   cwx=2
   SW.BREAK
  SW.CASE optSound
   LET rc$=GetCtrlData$(optSound)
   CALL SetSound(LEFT$(rc$,1))
   SW.BREAK
  SW.CASE lstFolders
   LET sTemp$=GetCtrlData$(lstFolders)
   IF LEFT$(sTemp$,2)=".." THEN
    LET curPath$=LEFT$(curPath$,LEN(curPath$)-1)
    WHILE RIGHT$(curPath$,1)<>"/"
     LET curPath$=LEFT$(curPath$,LEN(curPath$)-1)
    REPEAT
    cwx=2
   ELSE
    IF RIGHT$(sTemp$,3)="(d)" THEN
     LET sTemp$=LEFT$(sTemp$,LEN(sTemp$)-3)
     LET curPath$=curPath$+sTemp$+"/"
     cwx=2
    ENDIF
   ENDIF
   SW.BREAK
  SW.CASE cmdSaveHistory
   CALL CtrlEnable(optSound,0,0)
   CALL CtrlEnable(spnHistory,0,0)
   CALL CtrlEnable(cmdSaveHistory,0,0)
   CALL CtrlEnable(cmdAbout,0,0)
   CALL CtrlEnable(cmdClose,0,0)
   CALL CtrlEnable(lstFolders,1,0)
   CALL CtrlEnable(cmdCancel,1,0)
   CALL CtrlEnable(cmdSave,1,0)
   CALL CtrlEnable(lblSave,1,1)
   SW.BREAK
  SW.CASE cmdAbout
   LET i$=getGCVer$()
   CALL MsgBox$("Calculator v2.00"+bcRECBREAK$+bcRECBREAK$ ~
       +"(c) Jeff Kerr - 2011"+bcRECBREAK$+bcRECBREAK$ ~
       +"Sample program using the GraphicControls.bas"+bcRECBREAK$ ~
       +"include. As usual send bug reports and/or"+bcRECBREAK$ ~
       +"suggestions to author via Basic! forum."+bcRECBREAK$ ~
       +bcRECBREAK$ ~
       +"Running BASIC! Version "+ VERSION$()+bcRECBREAK$ ~
       +"with Graphic Controls Version "+i$, ~
       bcOKONLY$+bcINFORMATION$+bcCLRINFO$,"About")
   SW.BREAK
  SW.CASE cmdClose
   LET rc$=GetCtrlData$(spnHistory)
   LET ch$=GetCtrlCap$(lstCalcHist)
   LET i=IS_IN(bcRECBREAK$,ch$)
   LET sWork$=LEFT$(ch$,i)
   LET ch$=MID$(ch$,i+1)
   UNDIM aTemp$[]
   SPLIT aTemp$[],ch$,bcRECBREAK$
   ARRAY.LENGTH hc,aTemp$[]
   IF hc<>VAL(rc$) THEN
    UNDIM sNew$[]
    IF VAL(rc$)>hc THEN
     LET i=VAL(rc$)-hc
     ARRAY.COPY aTemp$[],sNew$[-i]
    ELSE
     ARRAY.COPY aTemp$[hc-VAL(rc$)+1,VAL(rc$)],sNew$[]
    ENDIF
    LET ch$=swork$+Join$(sNew$[],bcRECBREAK$)
    CALL SetCtrlData(lstCalcHist,"")
    CALL ModCtrlCap(lstCalcHist,ch$,0)
   ENDIF
   CALL CtrlVisible(fraOptions,0,1)
   LET bOptions=2
   LET lFirstCtrl=cmdOptions
   LET lLastCtrl=cmdEQ
   SW.BREAK
  SW.CASE cmdSave
   LET ch$=GetCtrlCap$(lstCalcHist)
   UNDIM aTemp$[]
   SPLIT aTemp$[],ch$,bcRECBREAK$
   ARRAY.LENGTH hc,aTemp$[]
   LET j=0
   FOR i=2 TO hc
    IF aTemp$[i]<>"" THEN
     IF j=0 THEN
      TIME yr$,mth$,day$,hr$,min$,sec$
      LET filename$="Calc-Hist @"+yr$+ "-"+mth$+"-"+day$ ~
                +"_"+hr$+"-"+min$+"-"+sec$+".txt"
      TEXT.OPEN w, FTNo, curPath$+filename$
      LET j=1
     ENDIF
     LET sTemp$=RIGHT$(Space50$+aTemp$[i],50)
     TEXT.WRITELN FTNo,sTemp$
    ENDIF
   NEXT i
   IF j=1 THEN
    TEXT.CLOSE FTNo
    LET sTemp$="Calculator History written to file:"+bcRECBREAK$+filename$
   ELSE
    LET sTemp$="No History Details Found."+bcRECBREAK$+"No output file created."
   ENDIF
   CALL MsgBox$(sTemp$,bcOKONLY$+bcINFORMATION$+bcCLRINFO$,"Save Calculator History")
  SW.CASE cmdCancel
   LET r=1
   GOSUB SetCtrlState
   SW.BREAK
 SW.END
REPEAT
IF cwx=2 THEN GOTO FileListReload
!
GR.CLOSE
LET i$=version$()
IF LEFT$(i$,2)="08" THEN
 EXIT    % For APK run
ELSE
 END ""  % For Basic! run
ENDIF
!
SetCtrlState:
CALL CtrlEnable(optSound,1,0)
CALL CtrlEnable(spnHistory,1,0)
CALL CtrlEnable(cmdSaveHistory,1,0)
CALL CtrlEnable(cmdAbout,1,0)
CALL CtrlEnable(cmdClose,1,0)
CALL CtrlEnable(lstFolders,0,0)
CALL CtrlEnable(cmdCancel,0,0)
CALL CtrlEnable(cmdSave,0,0)
CALL CtrlEnable(lblSave,0,r)
RETURN
!
NewDigit:
IF bClearDisplay=1 THEN
 LET sDisplayValue$=""
 LET bClearDisplay=0
ENDIF
IF sDisplayValue$="0" THEN
 LET sDisplayValue$=""
ENDIF
LET sDisplayValue$=sDisplayValue$+NewTxt$
CALL ModCtrlCap(dspDisplay,sDisplayValue$,1)
RETURN
!
OperationRtn:
IF Operator$<>"" THEN
 IF dblOperand1<>0 THEN
  LET bEquals=0
  GOSUB EqualsRoutine
 ENDIF
ELSE
 LET NewTxt$=sDisplayValue$+Space1$+NewOp$
 GOSUB HistoryUpdate
 LET dblOperand1=VAL(sDisplayValue$)
 LET bClearDisplay=1
ENDIF
LET Operator$=NewOp$
RETURN
!
HistoryUpdate:
LET HistTxt$=HistTxt$+bcRECBREAK$+NewTxt$
LET HistTxt$=MID$(HistTxt$,IS_IN(bcRECBREAK$,HistTxt$)+1)
CALL ModCtrlCap(lstCalcHist,HistCap$+HistTxt$,1)
RETURN
!
EqualsRoutine:
LET dblOperand2=VAL(sDisplayValue$)
IF dblOperand2=0 & Operator$="/" THEN
 CALL MsgBox$("Divide by Zero Error!",bcOKONLY$+bcEXCLAMATION$+bcCLRERR$,"Divide Operation")
ELSE
 IF bEquals=1 THEN
  IF bClearDisplay=1 THEN
   LET dblOperand2=0
  ELSE
   LET NewTxt$=sDisplayValue$+Space2$
   GOSUB HistoryUpdate
  ENDIF
 ELSE
  LET NewTxt$=sDisplayValue$+Space1$+Operator$
  GOSUB HistoryUpdate
 ENDIF
 IF Operator$="+" THEN
  LET dblResult=dblOperand1+dblOperand2
 ELSEIF Operator$="-" Then
  LET dblResult=dblOperand1-dblOperand2
 ELSEIF Operator$="*" Then
  LET dblResult=dblOperand1*dblOperand2
 ELSEIF Operator$="/" Then
  LET dblResult=dblOperand1/dblOperand2
 ELSE
  LET dblResult=dblOperand2
 ENDIF
 LET sDisplayValue$=STR$(dblResult)
 CALL ModCtrlCap(dspDisplay,sDisplayValue$,1)
 IF bEquals=1 THEN
  LET NewTxt$=sDisplayValue$+Space1$+"="
  GOSUB HistoryUpdate
  LET dblOperand1=0
  LET Operator$=""
 ELSE
  LET dblOperand1=dblResult
 ENDIF
 LET bClearDisplay=1
ENDIF
RETURN
!
LoadOptions:
LET fraOptions=AddControl(bcFRMFRAME,"Options", ~
    bcBLACK,bcLYELLOW,bcBLACK,bcLGREEN,68,20,40,400,664,29, ~
    bcDATBOLD$+bc3DBORDER$+bcFADEBACK$+bcHIDE$+bcALIGNCENTRE$)
LET optSound=AddControl(bcFRMOPTBUTTON, ~
    "Sound"+bcRECBREAK$+"Yes"+bcRECBREAK$+"No", ~
    bcWHITE,bcGRAY,bcBLACK,bcLGRAY, ~
    127,29,117,380,43,19,"")
LET spnHistory=AddControl(bcFRMSPINBUTTON,"List Size" ~
    +bcRECBREAK$+"1"+bcRECBREAK$+"5"+bcRECBREAK$ ~
    +"10"+bcRECBREAK$+"100", ~
    bcWHITE,bcGRAY,bcBLACK,bcLGRAY, ~
    180,29,117,380,43,19,"")
LET cmdSaveHistory=AddControl(bcFRMBUTTON,"Save History", ~
    bcBLACK,bcLGREEN,0,0,234,29,0,175,43,19,bcBEVEL$)
LET lstFolders=AddControl(bcFRMLISTBOX,"Folder: ", ~
    bcBLACK,bcLGRAY,bcBLACK,bcLCYAN,288,29,0,380,292,19, ~
    bcFILEDIALOG$)
LET cmdCancel=AddControl(bcFRMBUTTON,"Cancel", ~
    bcBLACK,bcLGREEN,0,0,590,254,0,117,43,19,bcBEVEL$)
LET cmdSave=AddControl(bcFRMBUTTON,"Save", ~
    bcBLACK,bcLGREEN,0,0,590,88,0,117,43,19,bcBEVEL$)
LET lblSave=AddControl(bcFRMLABEL,"Select Folder & Press Save.", ~
    bcBLACK,bcLYELLOW,0,0,634,20,0,400,43,19,bcALIGNCENTRE$)
LET cmdAbout=AddControl(bcFRMBUTTON,"About", ~
    bcBLACK,bcLBLUE,0,0,683,29,0,117,43,19,bcBEVEL$)
LET cmdClose=AddControl(bcFRMBUTTON,"Close", ~
    bcBLACK,bcLBLUE,0,0,683,293,0,117,43,19,bcBEVEL$)
CALL SetCtrlData(OptSound,"Yes")
CALL SetCtrlData(spnHistory,"30")
RETURN

!******************************
!***   End Calculator.bas   ***
!******************************





