!!
As suggested by the previous post, I think the best way to learn how to use the
Graphic Controls package is to first view the various sample programs to see how
they work and then start playing around changing things and seeing what effect
the changes make,

Brief Introduction.

The Graphic Controls include can be used to create and control multiple forms,
each of which can be made up of any number of controls. Each form will use the
same steps in order to be created and processed:
1. Call the StartNewForm function.
2. Define all the controls required on the form.
3. Call the DrawForm function to draw the form onto the screen.
4. Call the TouchCheck routine and wait for changes to be made to the form's
   Data values.
5. If a control's Data value is changed do any additional processing required.
6. Goto step 4 and wait for the next data change.
7. When required either goto to a step 1 of a new form or close program. As only
   one form exists at a time, new forms are always created even if they have
   been previously used.

The manual supplied with the package contains full details of the controls, 
functions and their parameters available within the Graphic Controls package.
It's not up to date (see latest release note(s) for updates/changes)
but it's a start.

The following is a recommented version of the Temperature Converter Sample
code. It does not do much, not much more than the standard "Hello World"
program but it does show the basic steps used in constructing a progam
using the Graphic Controls package, 
!!

REM Temperature Converter v1.0

INCLUDE GraphicConstants.bas                                                 % Note 1
INCLUDE GraphicControls.bas                                                  % Note 2

CALL InitGraphics(bcOPAQUE,bcLGRAY,bcLANDSCAPE,bcHIDESTATUSBAR,"GCClick.ogg")  % Note 3

CALL StartNewForm(DateSize,bcSOUNDON$,1,MsgBoxFontSize,bcLGRAY)              % Note 4

fraTemp=AddControl(bcFRMFRAME,"Temperature Converter", ~
    bcYELLOW,bcBLUE,bcRED,bcLBLUE, ~
    100,0,0,420,340,36, ~
    bcCTRLCENTRE$+bc3DBORDER$+bcCAPITALIC$+bcCAPBOLD$+bcALIGNCENTRE$)        % Note 5
txtCentigrade=AddControl(bcFRMSTRING,"Temp (C) :", ~
    bcBLACK,bcLGRAY,bcBLACK,bcWHITE, ~
    200,0,160,260,50,30, ~
    bcCTRLCENTRE$+bcALIGNDATCENTRE$)                                         % Note 6
dspFarenheit=AddControl(bcFRMDISPLAY,"Temp (F) :", ~
    bcBLACK,bcLBLUE,bcBLACK,bcLBLUE, ~
    280,0,160,260,50,30, ~
    bcCTRLCENTRE$+bcNOBORDER$+bcALIGNDATCENTRE$)                             % Note 7
cmdClose=AddControl(bcFRMBUTTON,"Close", ~
    bcBLACK,bcLGREEN,0,0, ~
    360,0,0,120,50,26, ~
    bcCTRLCENTRE$+bcBEVEL$)                                                  % Note 8

CALL SetCtrlData(txtCentigrade,"0")                                          % Note 9
CALL SetCtrlData(dspFarenheit,"32")                                          % Note 10

CALL DrawForm("","")                                                         % Note 11

WaitForTouch:
LET twx=0
WHILE twx=0
 LET selCtrl=TouchCheck(0,0,0)                                               % Note 12
 SW.BEGIN selCtrl
   SW.CASE txtCentigrade                                                     % Note 13
      LET Cent$=GetCtrlData$(txtCentigrade)
      LET FTemp$=REPLACE$(FORMAT$("##%",((VAL(Cent$)*9)/5)+32)," ","")
      CALL ModCtrlData(dspFarenheit,FTemp$,1)
      SW.BREAK
   SW.CASE cmdClose                                                          % Note 14
      LET twx=1
      SW.BREAK
 SW.END
REPEAT
!
GR.CLOSE
END ""

!!
Program Notes:  
 1 - GraphicConstants.bas contains definitions of all the data values
     (e.g.: Control Styles, Colours, etc) used by the Graphic Controls.

 2 - GraphicsControls,bas contain the actual graphic controls routines.

 3 - Initialises the graphics screen setting transparency to opaque, background
     colour to Light Gray, orientation to Landscape, hides the Status Bar (only
     works on some devices) and names the Sound File to be played when a control
     is clicked (can be an empty string, in which case a default Tone will be
     sounded).

 4 - Starts a new form. If required, all control definitions and data used by
     the previous form will be cleared out and then the various data stores will
     be initalised ready to store the form's control definitions and data.
     The Datesize and MsgBoxFontSize parameters are used to set the size of the
     Calendar and MsgBox controls, neither of which are used in this program.
     The bcSOUNDON$ set the sound on so any clicks on controls will cause either
     a beep or a Sound File to be played. The third parameter ("1") is the form
     Scaling Parameter which can be used to resize all the form controls so they
     fit the device the program is being run on. As no sizing is required here
     the value of 1 is used. The bcLGRAY parameter sets the background colour of
     this new form to Light Gray.

 5 - Defines a Frame control, horizontally centered on the screen with a 3D
     border and a centered Caption "Temperature Converter" in Bold and Italic.
     The Caption is in Yellow on a BLUE background. The Background colour of the
     frame is set to Light Blue.
     Note: The AddControl function always returns the ID of the newly defined
           control.

 6 - Defines a String control, horizontally centered on the screen with a
     Caption of "Temp (C) :" in Black on Light Gray background and a centered
     Data value in Black on a White background. 

 7 - Defines a Display control with no border horizontally centered on the
     screen with a Caption of "Temp (F) :" in Black on a Light Blue background
     and a centered Data value in Black on Light Blue background. 

 8 - Defines a Button control, horizontally centered on the screen with a
     caption of "Close" in black on light green.

 9 - Sets initial Data value of txtCentigrade control to "0".

10 - Sets initial Data value of dspFarenheit control to "32".

11 - Draws the defined controls onto screen. The two parameters (not used here)
     are a text string used in a popup message to inform user what is happening
     and a string defining a picture path which tells the graphic control
     routines where to look for any pictures used on the form.

12 - The TouchCheck function continually scans looking for screen clicks on one
     of the defined controls. The three parameters (again, not used here) are
     FirstCtrl, LastCtrl and Period. The FirstCtrl and LastCtrl parameters can
     be used to define which subset of a form's controls are to be scanned for
     clicks. The Period parameter can be used to force the TouchCheck to return
     even if no control is clicked (zero means only return if a control is
     clicked).
     When a control is clicked, the Graphic Controls will perform any processing
     required and if this results in a change in the controls Data value, the
     function will return to ID of the control clicked to the calling program.
     Certain controls which require no processing by the Graphic Controls
     routines will always the control ID to the calling program (eg: Buttons).
     As an example of the processing provided by the Graphic Controls, when a
     String type control is clicked a graphic Input Screen will be displayed
     allowing the user to enter or amend the control's Data string value. Only
     if the Data string value is changed during this process will the function
     exit and return the control's ID to the calling program.

13 - When the txtCentigrade control is clicked and it's value changed by the
     Graphic Control routines the selCtrl will be set to the ID of the
     txtCentigrade control.
     The GetCtrlData$ function retrieves the new Data value which is used to
     calculate the correspnding Farenheit value. The ModCtrlData function then
     sets the dspFarenheit's Data text to this new value.

14 - The ID of the cmdClose button will be returned by the TouchCheck function
     when it is clicked. In this case the additional processing is simply a
     "goto close program" command.

15 - Close graphics and end program.
!!

