!#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
!#-=__Start_of_GraphicControlsKB.bas__-=
!#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
GCVer$="v1.16"

!
@DrawInpPanel:
!============
LET bcFILL=1
LET bcNOFILL=0
LET bcRECBREAK$=CHR$(174)
LET bcBLACK=1
LET bcBLUE=2
LET bcCYAN=4
LET bcMAGENTA=6
LET bcGRAY=8
LET bcLGRAY=9
LET bcLBLUE=10
LET bcLGREEN=11
LET bcLCYAN=12
LET bcLRED=13
LET bcLMAGENTA=14
LET bcWHITE=16
LET bcFRMDISPLAY=1
LET bcFRMBUTTON=6
LET bcFRMOPTBUTTON=11
LET bcFRMFRAME=13
LET bcFRMSHAPE=14
LET bcFRMCOMBOBOX=15
LET bcALIGNCENTRE$="C"
LET bcCTRLLEFT$="m"
LET bcCTRLCENTRE$="d"
LET bcCTRLRIGHT$="s"
LET bcGRAPHIC$="G"
LET bcHIDE$="H"
LET bcDISABLED$="D"
LET bcFADEBACK$="+"
LET bcBLAKBACK$="#"
LET bcSHAPEFILL$="f"
LET bcBEVEL$="("
LET bcTRANSPARENT=0
LET bcOPAQUE=255
GOSUB @LoadRGBData
BUNDLE.GET 1,"swidth",swidth
BUNDLE.GET 1,"sheight",sheight
BUNDLE.GET 1,"dwidth",designstr
LET dwidth=1280
BUNDLE.PUT 1,"dwidth",dwidth
GR.CLIP gonum,0,0,swidth,sheight,2
BUNDLE.PUT 1,"kbfirstgo",gonum
BUNDLE.GET 1,"frmscale",strscale
LET KBScale=dwidth/swidth
LET KBTSize=30/KBScale
BUNDLE.PUT 1,"frmscale",KBScale
BUNDLE.PUT 1,"kbtsize",KBTSize
GR.TEXT.SIZE KBTSize
GR.SET.STROKE 0
GR.GET.TEXTBOUNDS "Ag",x,y,cx,cy
GR.GET.TEXTBOUNDS "A g",x,y,dx,cy
LET ITHome=16/KBScale
LET ICSpace=dx-cx
LET ITWidth=1246/KBScale
BUNDLE.GET 1,"whitespace",WhiteSpace$
LET fraKBoard=AddControl(bcFRMFRAME,"Heading",bcBLACK,bcLBLUE,bcBLACK,bcLGRAY, ~
    0,0,0,1280,280,30,bcBLAKBACK$+bcALIGNCENTRE$+bcHIDE$)
LET dspKBText=AddControl(bcFRMDISPLAY,"",bcBLACK,bcLCYAN,bcBLACK,bcLCYAN,60,10,0,1260,60,30,"")
LET cmdKBCaps=AddControl(bcFRMBUTTON,"Caps Lock",bcBLACK,bcLCYAN,0,0,130,10,0,120,60,20,bcBEVEL$)
LET cmdKBShift=AddControl(bcFRMBUTTON,"Shift",bcBLACK,bcLCYAN,0,0,130,150,0,100,60,20,bcBEVEL$)
LET cmdKBHome=AddControl(bcFRMBUTTON,"Home",bcWHITE,bcBLUE,0,0, ~
    130,190,0,100,60,20,bcCTRLLEFT$+bcBEVEL$)
LET cmdKBLeft=AddControl(bcFRMBUTTON,"<",bcBLUE,bcLBLUE,0,0,130,70,0,100,60,0,bcCTRLLEFT$+bcBEVEL$)
LET cmdKBRight=AddControl(bcFRMBUTTON,">",bcBLUE,bcLBLUE,0,0,130,0,0,100,60,0,bcCTRLCENTRE$+bcBEVEL$)
LET cmdKBEnd=AddControl(bcFRMBUTTON,"End",bcWHITE,bcBLUE,0,0,130,70,0,100,60,20,bcCTRLRIGHT$+bcBEVEL$)
LET cmdKBInsert=AddControl(bcFRMBUTTON,"Insert",bcBLACK,bcLMAGENTA, ~
    0,0,130,190,0,100,60,20,bcCTRLRIGHT$+bcBEVEL$)
LET cmdKBDel=AddControl(bcFRMBUTTON,"GC-Delete.png",bcBLACK,bcLCYAN,0,0, ~
    130,350,0,60,60,12,bcCTRLRIGHT$+bcGRAPHIC$+bcBEVEL$)
LET cmdKBCut=AddControl(bcFRMBUTTON,"GC-Cut.png",bcBLACK,bcLCYAN,0,0, ~
    130,420,0,60,60,12,bcCTRLRIGHT$+bcGRAPHIC$+bcBEVEL$)
LET cmdKBCopy=AddControl(bcFRMBUTTON,"GC-Copy.png",bcBLACK,bcLCYAN,0,0, ~
    130,490,0,60,60,12,bcCTRLRIGHT$+bcGRAPHIC$+bcBEVEL$)
LET cmdKBPaste=AddControl(bcFRMBUTTON,"GC-Paste.png",bcBLACK,bcLCYAN,0,0, ~
    130,560,0,60,60,12,bcCTRLRIGHT$+bcGRAPHIC$+bcBEVEL$)
LET cmdKBSwap=AddControl(bcFRMBUTTON,"Swap Case",bcBLACK,bcCYAN,0,0,200,10,0,120,60,20,bcBEVEL$)
LET cboKBFormat=AddControl(bcFRMCOMBOBOX,"Format"+bcRECBREAK$+"Normalise Text"+bcRECBREAK$ ~
    +"Uppercase Text"+bcRECBREAK$+"Lower Case Text"+bcRECBREAK$+"-"+bcRECBREAK$ ~
    +"Clear"+bcRECBREAK$+"Copy All Text"+bcRECBREAK$+"Cut All Text",bcWHITE,bcGRAY,bcBLACK,bcLCYAN, ~
    214,160,80,280,54,20,"")
LET cmdKBEnter=AddControl(bcFRMBUTTON,"Enter",bcBLACK,bcLGREEN,0,0, ~
    214,20,0,120,54,24,bcCTRLLEFT$+bcBEVEL$)
LET cmdKBCancel=AddControl(bcFRMBUTTON,"Cancel",bcBLACK,bcLRED,0,0, ~
    214,20,0,120,54,24,bcCTRLRIGHT$+bcBEVEL$)
LET cmdKBSelect=AddControl(bcFRMBUTTON,"Select Mode",bcBLACK,bcCYAN,0,0, ~
    200,406,0,160,60,20,bcCTRLRIGHT$+bcBEVEL$)
LET shpKBCur=AddControl(bcFRMSHAPE,"rect",bcBLACK,bcBLACK,bcBLACK,bcBLACK, ~
    70,ITHome,0,1,30,1,bcSHAPEFILL$)
CALL DrawForm("","-")
BUNDLE.GET 1,"FrmObjCnt",FrmObjCnt
BUNDLE.PUT 1,"kblastgo",FrmObjCnt
BUNDLE.GET 1,"ctrldata",ctrldata
BUNDLE.GET ctrldata,"CP"+STR$(dspKBText),i
BUNDLE.GET i,"dattxtobj",ITxtObj
BUNDLE.GET ctrldata,"CP"+STR$(shpKBCur),i
BUNDLE.GET i,"captxtobj",ICTObj
BUNDLE.GET i,"capbakobj",ICBObj
BUNDLE.PUT 1,"ITHome",ITHome
BUNDLE.PUT 1,"ITWidth",ITWidth
BUNDLE.PUT 1,"ITxtObj",ITxtObj
BUNDLE.PUT 1,"ICSpace",ICSpace
BUNDLE.PUT 1,"CurTObj",ICTObj
BUNDLE.PUT 1,"CurBObj",ICBObj
BUNDLE.PUT 1,"CurTop",70/KBScale
BUNDLE.PUT 1,"CurBot",40/KBScale
BUNDLE.PUT 1,"fraKBoard",fraKBoard
BUNDLE.PUT 1,"dspKBText",dspKBText
BUNDLE.PUT 1,"cmdKBCaps",cmdKBCaps
BUNDLE.PUT 1,"cmdKBShift",cmdKBShift
BUNDLE.PUT 1,"cmdKBHome",cmdKBHome
BUNDLE.PUT 1,"cmdKBLeft",cmdKBLeft
BUNDLE.PUT 1,"cmdKBRight",cmdKBRight
BUNDLE.PUT 1,"cmdKBEnd",cmdKBEnd
BUNDLE.PUT 1,"cmdKBInsert",cmdKBInsert
BUNDLE.PUT 1,"cmdKBDel",cmdKBDel
BUNDLE.PUT 1,"cmdKBCut",cmdKBCut
BUNDLE.PUT 1,"cmdKBCopy",cmdKBCopy
BUNDLE.PUT 1,"cmdKBPaste",cmdKBPaste
BUNDLE.PUT 1,"cmdKBSwap",cmdKBSwap
BUNDLE.PUT 1,"cboKBFormat",cboKBFormat
BUNDLE.PUT 1,"cmdKBEnter",cmdKBEnter
BUNDLE.PUT 1,"cmdKBCancel",cmdKBCancel
BUNDLE.PUT 1,"cmdKBSelect",cmdKBSelect
BUNDLE.PUT 1,"shpKBCur",shpKBCur
BUNDLE.PUT 1,"inpdrawn",1
BUNDLE.PUT 1,"frmscale",strscale
BUNDLE.PUT 1,"dwidth",designstr
RETURN
!
KeyRoutine:
!=========
LET sctrlptr=ctrlptr
LET bcOPAQUE=255
LET bcSEMIOPAQUE=128
LET bcTRANSPARENT=0
LET bcBLACK=1
LET bcBLUE=2
LET bcLGREEN=11
LET bcLCYAN=12
LET bcCOLBREAK$=CHR$(169)
LET bcBlank$="--Blank--"
CALL @LoadKBDKeyVal()
BUNDLE.GET 1,"charkey",LstCKey
BUNDLE.GET 1,"charval",LstCVal
BUNDLE.GET 1,"shiftkey",LstSKey
BUNDLE.GET 1,"shiftval",LstSVal
UNDIM CharVal$[]
LIST.TOARRAY LstCVal,CharVal$[]
UNDIM ShiftVal$[]
LIST.TOARRAY LstSVal,ShiftVal$[]
BUNDLE.GET 1,"inpdrawn",ki
IF ki=0 THEN
 GOSUB @DrawInpPanel
ELSE
 BUNDLE.GET 1,"FrmObjCnt",FrmObjCnt
 BUNDLE.GET 1,"kblastgo",kblastgo
 IF FrmObjCnt<>kblastgo THEN
  BUNDLE.GET 1,"kbfirstgo",kbfirstgo
  UNDIM DLst[]
  DIM DLst[FrmObjCnt]
  LET kj=1
  FOR ki=1 TO FrmObjCnt
   IF ki<kbfirstgo | ki>kblastgo THEN
    LET DLst[kj]=ki
    LET kj=kj+1
   ENDIF
  NEXT ki
  LET kk=0
  FOR ki=kbfirstgo TO kblastgo
   LET DLst[kj]=kbfirstgo+kk
   LET kj=kj+1
   LET kk=kk+1
  NEXT ki
  GR.NEWDL DLst[]
 ENDIF
 BUNDLE.GET 1,"kbtsize",KBTSize
 BUNDLE.GET 1,"ITHome",ITHome
 BUNDLE.GET 1,"ITWidth",ITWidth
 BUNDLE.GET 1,"ITxtObj",ITxtObj
 BUNDLE.GET 1,"ICSpace",ICSpace
 BUNDLE.GET 1,"CurTObj",ICTObj
 BUNDLE.GET 1,"CurBObj",ICBObj
 BUNDLE.GET 1,"fraKBoard",fraKBoard
 BUNDLE.GET 1,"dspKBText",dspKBText
 BUNDLE.GET 1,"cmdKBCaps",cmdKBCaps
 BUNDLE.GET 1,"cmdKBShift",cmdKBShift
 BUNDLE.GET 1,"cmdKBHome",cmdKBHome
 BUNDLE.GET 1,"cmdKBLeft",cmdKBLeft
 BUNDLE.GET 1,"cmdKBRight",cmdKBRight
 BUNDLE.GET 1,"cmdKBEnd",cmdKBEnd
 BUNDLE.GET 1,"cmdKBInsert",cmdKBInsert
 BUNDLE.GET 1,"cmdKBDel",cmdKBDel
 BUNDLE.GET 1,"cmdKBCut",cmdKBCut
 BUNDLE.GET 1,"cmdKBCopy",cmdKBCopy
 BUNDLE.GET 1,"cmdKBPaste",cmdKBPaste
 BUNDLE.GET 1,"cmdKBSwap",cmdKBSwap
 BUNDLE.GET 1,"cboKBFormat",cboKBFormat
 BUNDLE.GET 1,"cmdKBEnter",cmdKBEnter
 BUNDLE.GET 1,"cmdKBCancel",cmdKBCancel
 BUNDLE.GET 1,"cmdKBSelect",cmdKBSelect
 BUNDLE.GET 1,"shpKBCur",shpKBCur
 BUNDLE.GET 1,"whitespace",WhiteSpace$
ENDIF
BUNDLE.GET 1,"CurTop",ICTop
BUNDLE.GET 1,"CurBot",ICBot
CALL ModCtrlCap(fraKBoard,inpheader$,0)
CALL ModCtrlCapBakClr(cmdKBShift,bcLCYAN,0)
CALL ModCtrlCapBakClr(cmdKBCaps,bcLCYAN,0)
CALL ModCtrlCapBakClr(cmdKBSelect,bcLCYAN,0)
CALL ShowCtrl(fraKBoard,0)
CALL HideCtrl(shpKBCur,1)
LET ITVal$=inptext$
IF ITVal$=bcBlank$ THEN LET ITVal$=""
KB.TOGGLE
BUNDLE.PUT 1,"vkeyb",1
LET ITOrig=ITHome
GR.TEXT.SIZE KBTSize
GR.TEXT.WIDTH kx,ITVal$
LET ICPos=kx
LET KBInsert=0
LET KBSelect=0
LET ICAlp=255
LET ICCnt=0
LET ICEnd=0
LET KBShift=0
LET KBCaps=0
LET KBCtrl=cmdKBEnd
GOSUB @adjustcursor
LET curTimer=CLOCK()
!
bkpstart:
!------- % After back key press, restart from here, if program in background mode hide keyboard
DO
 LET bg=BACKGROUND()
 IF bg=1 THEN
  KB.HIDE
  BUNDLE.PUT 1,"vkeyb",0
 ELSE
  KB.TOGGLE
  BUNDLE.PUT 1,"vkeyb",1
 ENDIF
 INKEY$ s1$
 INKEY$ s2$
 IF s1$="key 59" THEN
  LET r=1
  GOSUB @SetShiftKey
  LET s1$="@"
 ENDIF
 IF s1$<>"@" & (KBShift=1 | KBCaps=1) THEN
  LET s2$="key 59"
  LET KBShift=0
  CALL ModCtrlCapBakClr(cmdKBShift,bcLCYAN,0)
 ENDIF
 IF s2$="key 59" THEN
  IF STARTS_WITH("key ",s1$)=0 & ASCII(s1$)>=97 & ASCII(s1$)<=122 THEN
   LET IKey$=CHR$(ASCII(s1$)-32)
  ELSE
   LIST.SEARCH LstSKey,s1$,ki
   IF ki<>0 THEN LET IKey$=ShiftVal$[ki] ELSE LET IKey$="@"
  ENDIF
  LET s2$="@"
 ELSE
  LET IKey$=s1$
  IF LEFT$(s1$,3)="key" THEN
   LIST.SEARCH LstCKey,s1$,ki
   IF ki<>0 THEN LET IKey$=CharVal$[ki] ELSE LET IKey$="@"
  ENDIF
 ENDIF
 IF IKey$="Caps Lock" THEN
  LET r=0
  GOSUB @SetCapsLock
  LET IKey$="@"
 ENDIF
 IF (IKey$<>"@" | s1$="2" | s1$="key 75") & s1$<>"key 59" THEN
  IF IKey$="left" THEN
   LET KBCtrl=cmdKBLeft
   GOSUB @adjustcursor
  ELSEIF IKey$="right" THEN
   LET KBCtrl=cmdKBRight
   GOSUB @adjustcursor
  ELSEIF KBSelect=0 THEN
   IF IKey$="Del" THEN
    IF ITVal$<>"" & ICPos<>0 THEN
     IF LEN(ITVal$)=1 THEN
      LET ITVal$=""
      LET ICPos=0
     ELSE
      IF ICPos<LEN(ITVal$) THEN
       LET ITVal$=LEFT$(ITVal$,ICPos-1)+MID$(ITVal$,ICPos+1)
      ELSE
       LET ITVal$=LEFT$(ITVal$,LEN(ITVal$)-1)
      ENDIF
      LET ICPos=ICPos-1
      LET KBCtrl=0
      GOSUB @adjustcursor
     ENDIF
    ENDIF
   ELSEIF IKey$="Enter" THEN
    LET KBCtrl=cmdKBSave
    D_U.BREAK
   ELSEIF LEN(IKey$)=1 THEN
    IF KBInsert=0 THEN
     LET ITVal$=LEFT$(ITVal$,ICPos)+IKey$+MID$(ITVal$,ICPos+1)
    ELSE
     LET ITVal$=LEFT$(ITVal$,ICPos)+IKey$+MID$(ITVal$,ICPos+2)
    ENDIF
    IF ICPos<LEN(ITVal$) THEN
     LET ICPos=ICPos+1
    ENDIF
    GR.TEXT.SIZE KBTSize
    GR.TEXT.WIDTH i,LEFT$(ITVal$,ICPos)
    IF ITOrig+i+ICSpace>ITHome+ITWidth THEN
     LET ITOrig=ITHome+ITWidth-i-ICSpace
     GR.MODIFY ITxtObj,"x",ITOrig
    ENDIF
   ENDIF
   CALL ModCtrlData(dspKBText,ITVal$,0)
  ENDIF
  IF KBSelect=0 THEN
   LET ICCnt=0
   LET ICAlp=255
   GOSUB @ShowCursor
  ELSE
   GOSUB @ShowSelectMark
  ENDIF
  GR.RENDER
 ELSE
  LET KBCtrl=0
  LET touched=0
  GR.TOUCH touched,dx,dy
  IF touched<>0 THEN
   FOR ctrlID=dspKBText TO cmdKBSelect
    GOSUB @getcontrolcoords
    IF ctrlstate$="" THEN
     LET cright=ctrlleft+ctrlwidth
     LET cbottom=ctrltop+ctrlheight
     GR.BOUNDED.TOUCH tch,ctrlleft,ctrltop,cright,cbottom
     IF tch=1 THEN
      LET KBCtrl=CtrlID
      CALL DisableCtrl(KBCtrl,1)
      LET dwntime=CLOCK()
      WHILE touched=1
       IF KBCtrl=cmdKBLeft | KBCtrl=cmdKBRight THEN
        IF CLOCK()-dwntime>50 THEN W_R.BREAK
       ENDIF
       GR.TOUCH touched,x,y
      REPEAT
      CALL EnableCtrl(ctrlID,1)
      F_N.BREAK
     ENDIF
    ENDIF
   NEXT ctrlID
   IF KBCtrl<>0 THEN
    IF KBCtrl=cmdKBEnter | KBCtrl=cmdKBCancel THEN
     D_U.BREAK
    ELSEIF KBctrl=cboKBFormat THEN
     LET rw=ctrlleft+ctrlwidth-ctrlheight
     IF dx>rw THEN
      BUNDLE.GET ptrctrl,"sizecap",ctrlsizecap$
      BUNDLE.GET ptrctrl,"datalst",ctrldatalst
      BUNDLE.GET ptrctrl,"data",ctrldata$
      BUNDLE.GET ptrctrl,"dattxtobj",ctrldattxtobj
      BUNDLE.GET ptrctrl,"top",ctrltop
      BUNDLE.GET ptrctrl,"left",ctrlleft
      BUNDLE.GET ptrctrl,"height",ctrlheight
      BUNDLE.GET ptrctrl,"middle",ctrlmiddle
      BUNDLE.GET ptrctrl,"width",ctrlwidth
      BUNDLE.GET ptrctrl,"font",ctrlfont
      CALL @clickcombobox(ctrlsizecap$,ctrldatalst,&ctrldata$,ctrldattxtobj,ptrctrl, ~
           ctrltop,ctrlleft,ctrlheight,ctrlmiddle,ctrlwidth,ctrlfont)
      BUNDLE.PUT ptrctrl,"data",ctrldata$
     ENDIF
    ENDIF
    GOSUB @AdjustCursor
    IF KBSelect=1
     GOSUB @ShowSelectMark
     GR.RENDER
    ENDIF
   ENDIF
  ELSE
   IF KBSelect=0 THEN
    IF CLOCK()-curTimer>50 THEN
     LET ICCnt=ICCnt+1
     IF ICCnt=10 THEN
      LET ICCnt=0
      IF ICAlp=0 THEN LET ICAlp=255 ELSE LET ICAlp=0
      GOSUB @ShowCursor
      GR.RENDER
     ENDIF
     LET curTimer=CLOCK()
    ENDIF
   ENDIF
  ENDIF
 ENDIF
UNTIL 0
CALL HideCtrl(shpKBcur,0)
KB.HIDE
BUNDLE.PUT 1,"vkeyb",0
CALL HideCtrl(fraKBoard,1)
IF KBCtrl=cmdKBEnter THEN
 IF ITVal$="" THEN LET ITVal$=bcBlank$
 LET inptext$=ITVal$
ELSE
 LET inptext$=bcCOLBREAK$
ENDIF
LET ctrlptr=sctrlptr
RETURN
!
@ShowSelectMark:
!==============
IF ICPos<=ICSPos THEN
 LET ki=ICPos
 LET kj=ICSPos
ELSE
 LET ki=ICSPos
 LET kj=ICPos
ENDIF
GR.TEXT.SIZE KBTSize
GR.TEXT.WIDTH kx,LEFT$(ITVal$,ki-1)
GR.TEXT.WIDTH ky,MID$(ITVal$,ki,kj-ki+1)
CALL MoveCtrl(shpKBCur,"A",ITOrig+kx,ICTop,ky,ICBot,0)
GR.MODIFY ICTObj,"alpha",100
GR.MODIFY ICBObj,"alpha",100
RETURN
!
@AdjustCursor:
!============
LET ICOldPos=ICPos
SW.BEGIN KBCtrl
 SW.CASE cmdKBInsert
  LET k1$=GetCtrlCap$(KBCtrl)
  IF k1$="Insert" THEN
   LET KBInsert=1
   LET k1$="Over"
  ELSE
   LET KBInsert=0
   LET k1$="Insert"
  ENDIF
  CALL ModCtrlCap(KBCtrl,k1$,0)
  LET ICOldPos=-1
  SW.BREAK
 SW.CASE cmdKBRight
  IF ICPos<LEN(ITVal$) THEN
   LET ICPos=ICPos+1
   GOSUB @RCurAdjust
  ENDIF
  SW.BREAK
 SW.CASE cmdKBLeft
  IF ICPos>0 THEN
   LET ICPos=ICPos-1
   GOSUB @LCurAdjust
  ENDIF
  SW.BREAK
 SW.CASE cmdKBHome
  LET ICPos=0
  GOSUB @LCurAdjust
  SW.BREAK
 SW.CASE cmdKBEnd
  LET ICPos=LEN(ITVal$)
  GOSUB @RCurAdjust
  SW.BREAK
 SW.CASE dspKBText
  CALL GetCtrlClickXY(ctrlID,dspobj,bmpobj,dx,dy)
  LET dx=dx-ITOrig
  GR.TEXT.SIZE KBTSize
  LET ICPos=LEN(ITVal$)
  FOR ki=1 TO LEN(ITVal$)
   GR.TEXT.WIDTH kx,LEFT$(ITVal$,ki)
   IF kx>dx THEN
    LET ICPos=ki-1
    F_N.BREAK
   ENDIF
  NEXT ki
  SW.BREAK
 SW.CASE cmdKBCut
 SW.CASE cmdKBCopy
 SW.CASE cmdKBDel
  IF ICPos<ICSPos THEN
   LET ki=ICPos
   LET kj=ICSPos
  ELSE
   LET ki=ICSPos
   LET kj=ICPos
  ENDIF
  IF KBCtrl=cmdKBCut | KBCtrl=cmdKBCopy THEN
   LET k1$=MID$(ITVal$,ki,kj-ki+1)
   CLIPBOARD.PUT k1$
   IF KBCtrl=cmdKBCut THEN LET kj$="Cut" ELSE LET kj$="Copy"
   POPUP kj$+" Text: '"+k1$+"'",0,0,0
   CALL EnableCtrl(cmdKBPaste,0)
  ENDIF
  IF KBCtrl=cmdKBCut | KBCtrl=cmdKBDel THEN
   LET ITVal$=LEFT$(ITVal$,ki-1)+MID$(ITVal$,kj+1)
   LET ICPos=ki
   LET ICSPos=ICPos
   GOSUB @RCurAdjust
  ENDIF
  SW.BREAK
 SW.CASE cmdKBSwap
  IF ICPos<LEN(ITVal$) THEN
   LET k1$=MID$(ITVal$,ICPos+1,1)
   IF LOWER$(k1$)=k1$ THEN LET k1$=UPPER$(k1$) ELSE LET k1$=LOWER$(k1$)
   LET ITVal$=LEFT$(ITVal$,ICPos)+k1$+MID$(ITVal$,ICPos+2)
   GOSUB @RCurAdjust
  ENDIF
  SW.BREAK
 SW.CASE cmdKBPaste
  CLIPBOARD.GET k1$
  IF KBInsert=0 THEN
   LET ITVal$=LEFT$(ITVal$,ICPos)+k1$+MID$(ITVal$,ICPos+1)
  ELSE
   LET ITVal$=LEFT$(ITVal$,ICPos)+k1$+MID$(ITVal$,ICPos+2)
  ENDIF
  LET ICPos=ICPos+LEN(k1$)
  GOSUB @LCurAdjust
  SW.BREAK
 SW.CASE cboKBFormat
  LET k1$=GetCtrlData$(KBCtrl)
  IF k1$="Normalise Text" THEN
   LET k1$=""
   LET ITVal$=LOWER$(ITVal$)
   FOR ki=1 TO LEN(ITVal$)
    IF ki=1 THEN
     LET k1$=UPPER$(LEFT$(ITVal$,1))
    ELSE
     IF IS_IN(MID$(ITVal$,ki,1),WhiteSpace$)>0 THEN
      LET k1$=k1$+MID$(ITVal$,ki,1)
     ELSE
      IF IS_IN(MID$(ITVal$,ki-1,1),WhiteSpace$)>0 THEN
       LET k1$=k1$+UPPER$(MID$(ITVal$,ki,1))
      ELSE
       LET k1$=k1$+MID$(ITVal$,ki,1)
      ENDIF
     ENDIF
    ENDIF
   NEXT ki
   LET ITVal$=k1$
   GOSUB @RCurAdjust
  ELSEIF k1$="Uppercase Text" THEN
   LET ITVal$=UPPER$(ITVal$)
   GOSUB @RCurAdjust
  ELSEIF k1$="Lower Case Text" THEN
   LET ITVal$=LOWER$(ITVal$)
   GOSUB @RCurAdjust
  ELSEIF k1$="Clear" | k1$="Cut All Text" THEN
   IF k1$="Cut All Text" THEN CLIPBOARD.PUT ITVal$
   LET ITVal$=""
   LET ICPos=0
   GOSUB @LCurAdjust
  ELSEIF k1$="Copy All Text" THEN
   CLIPBOARD.PUT ITVal$
  ENDIF
  CALL ModCtrlData(cboKBFormat,"",0)
  SW.BREAK
 SW.CASE cmdKBSelect
  IF KBSelect=0 THEN
   CALL ModCtrlCapBakClr(cmdKBSelect,bcLGREEN,0)
   FOR ki=cmdKBHome TO cmdKBCancel
    CALL DisableCtrl(ki,0)
   NEXT ki
   CALL modctrlcaptxtclr(shpKBCur,bcBLUE,0)
   CALL modctrlcapbakclr(shpKBCur,bcBLUE,0)
   LET KBSelect=1
   LET ICPos=ICPos+1
   LET ICSPos=ICPos
   GR.MODIFY ICTObj,"alpha",100
   GR.MODIFY ICBObj,"alpha",100
  ELSE
   FOR ki=cmdKBHome TO cmdKBCancel
    CALL EnableCtrl(ki,0)
   NEXT ki
   CALL ModCtrlCapBakClr(cmdKBSelect,bcLCYAN,0)
   CALL modctrlcaptxtclr(shpKBCur,bcBLACK,0)
   CALL modctrlcapbakclr(shpKBCur,bcBLACK,0)
   CALL hidectrl(shpKBcur,0)
   LET KBSelect=0
   LET ICSPos=0
  ENDIF
  SW.BREAK
 SW.CASE cmdKBShift
  LET r=0
  GOSUB @SetShiftKey
  SW.BREAK
 SW.CASE cmdKBCaps
  LET r=0
  GOSUB @SetCapsLock
  SW.BREAK
SW.END
LET KBCtrl=0
CLIPBOARD.GET k1$
IF k1$="" THEN LET e=0 ELSE LET e=1
CALL CtrlEnable(cmdKBPaste,e,0)
IF KBSelect=0 THEN
 CALL DisableCtrl(cmdKBDel,0)
 CALL DisableCtrl(cmdKBCut,0)
 CALL DisableCtrl(cmdKBCopy,0)
 IF ICOldPos<>ICPos THEN
  LET ICCnt=0
  LET ICAlp=255
  GOSUB @ShowCursor
 ENDIF
ELSE
 IF ICSPos<>ICPos THEN
  CALL EnableCtrl(cmdKBDel,0)
  CALL EnableCtrl(cmdKBCut,0)
  CALL EnableCtrl(cmdKBCopy,0)
 ELSE
  CALL DisableCtrl(cmdKBDel,0)
  CALL DisableCtrl(cmdKBCut,0)
  CALL DisableCtrl(cmdKBCopy,0)
 ENDIF
ENDIF
GR.RENDER
RETURN
!
@SetShiftKey:
!===========
IF KBShift=0 THEN
 LET KBShift=1
 CALL ModCtrlCapBakClr(cmdKBShift,bcLGREEN,r)
ELSE
 LET KBShift=0
 CALL ModCtrlCapBakClr(cmdKBShift,bcLCYAN,r)
ENDIF
LET r=0
RETURN
!
@SetCapsLock:
!===========
IF KBCaps=0 THEN
 LET KBShift=0
 CALL ModCtrlCapBakClr(cmdKBShift,bcLCYAN,0)
 CALL DisableCtrl(cmdKBShift,0)
 LET KBCaps=1
 CALL ModCtrlCapBakClr(cmdKBCaps,bcLGREEN,r)
ELSE
 LET KBCaps=0
 CALL ModCtrlCapBakClr(cmdKBCaps,bcLCYAN,0)
 CALL EnableCtrl(cmdKBShift,r)
ENDIF
LET r=0
RETURN
!
@RCurAdjust:
!==========
GR.TEXT.SIZE KBTSize
GR.TEXT.WIDTH i,LEFT$(ITVal$,ICPos)
IF ITOrig+i+ICSpacec>ITHome+ITWidth THEN
 LET ITOrig=ITHome+ITWidth-i-ICSpace
 CALL MoveCtrl(shpKBCur,"A",ITOrig,ICTop,1,ICBot,0)
 GR.MODIFY ITxtObj,"x",ITOrig
ENDIF
GR.MODIFY ITxtObj,"text",ITVal$
RETURN
!
@LCurAdjust:
!==========
GR.TEXT.SIZE KBTSize
GR.TEXT.WIDTH i,LEFT$(ITVal$,ICPos)
IF ITOrig+i<ITHome+(5*ICSpace) THEN
 GR.TEXT.WIDTH i,LEFT$(ITVal$,ICPos-2)
 LET ITOrig=ITHome-i
 CALL MoveCtrl(shpKBCur,"A",ITOrig,ICTop,1,ICBot,0)
 GR.MODIFY ITxtObj,"x",ITOrig
ENDIF
GR.MODIFY ITxtObj,"text",ITVal$
RETURN
!
@ShowCursor:
!==========
IF KBSelect=0 THEN
 IF ICAlp=0 THEN
  GR.MODIFY ICTObj,"alpha",bcTRANSPARENT
  GR.MODIFY ICBObj,"alpha",bcTRANSPARENT
 ELSE
  GR.MODIFY ICTObj,"alpha",100
  GR.MODIFY ICBObj,"alpha",100
 ENDIF
ENDIF
GR.TEXT.SIZE KBTSize
IF ICPos=0 THEN
 IF KBInsert=0 THEN
  CALL MoveCtrl(shpKBCur,"A",ITOrig,ICTop,1,ICBot,0)
 ELSE
  IF ITVal$="" THEN
   LET dx=0
  ELSE
   IF LEFT$(ITVal$,1)=" " THEN LET dx=ICSpace ELSE GR.TEXT.WIDTH dx,LEFT$(ITVal$,1)
  ENDIF
  CALL MoveCtrl(shpKBCur,"A",ITOrig,ICTop,dx+1,ICBot,0)
 ENDIF
ELSEIF ICPos=LEN(ITVal$) THEN
 GR.TEXT.WIDTH cx,ITVal$
 CALL MoveCtrl(shpKBCur,"A",ITOrig+cx,ICTop,1,ICBot,0)
ELSE
 GR.TEXT.WIDTH cx,LEFT$(ITVal$,ICPos)
 IF KBInsert=0 THEN
  CALL MoveCtrl(shpKBCur,"A",ITOrig+cx,ICTop,1,ICBot,0)
 ELSE
  IF MID$(ITVal$,ICPos+1,1)=" " THEN LET dx=ICSpace ELSE GR.TEXT.WIDTH dx,MID$(ITVal$,ICPos+1,1)
  CALL MoveCtrl(shpKBCur,"A",ITOrig+cx,ICTop,dx+1,ICBot,0)
 ENDIF
ENDIF
RETURN
!
!  L O A D _ K B D _ K E Y _ V A L
!
FN.DEF @LoadKBDKeyVal()
 LET bcDBLQUOTE$=CHR$(34)
 LET bcBACKSLASH$=CHR$(92)
 LET bcPOUNDSIGN$=CHR$(163)
 LET bcNOTSIGN$=CHR$(172)
 ARRAY.LOAD CharKey$[],"key 68","key 69","key 70","key 71","key 72","key 74","key 75","key 73", ~
   "key 55","key 56","key 76","key 188","key 24","key 25","key 61","key 66","key 67","key 82", ~
   "Search","key 115"
 ARRAY.LOAD CharVal$[],"`","-","=","[","]",";","'","#",",",".","/",bcBACKSLASH$,"Volume +", ~
   "Volume -","Tab","Enter","Del","Menu","Search","Caps Lock"
 ARRAY.LOAD ShiftKey$[],"key 68","1","2","3","4","5","6","7","8","9","0","key 69","key 70", ~
   "key 71","key 72","key 74","key 75","key 73","key 55","key 56","key 76","key 188","key 115"
 ARRAY.LOAD ShiftVal$[],bcNOTSIGN$,"!","@","#","$","%","^","&","*","(",")","_","+","{","}",":", ~
   bcDBLQUOTE$,"~","<",">","?","|","Caps Lock" % US
 BUNDLE.GET 1,"kbdlang",pLang$
 IF pLang$="UK" THEN
  LET ShiftVal$[3]=bcDBLQUOTE$
  LET ShiftVal$[4]=bcPOUNDSIGN$
  LET ShiftVal$[17]="@"
 ENDIF
 LIST.CREATE s,LstCKey
 LIST.ADD.ARRAY LstCKey,CharKey$[]
 LIST.CREATE s,LstCVal
 LIST.ADD.ARRAY LstCVal,CharVal$[]
 LIST.CREATE s,LstSKey
 LIST.ADD.ARRAY LstSKey,ShiftKey$[]
 LIST.CREATE s,LstSVal
 LIST.ADD.ARRAY LstSVal,ShiftVal$[]
 BUNDLE.PUT 1,"charkey",LstCKey
 BUNDLE.PUT 1,"charval",LstCVal
 BUNDLE.PUT 1,"shiftkey",LstSKey
 BUNDLE.PUT 1,"shiftval",LstSVal
 FN.RTN 0
FN.END
!
!  S E T _ K B D _ L A N G
!
FN.DEF SetKBDLang(pLang$)
 BUNDLE.PUT 1,"kbdlang",pLang$
 FN.RTN 0
FN.END
!
!  G E T _ V K E Y B _ S T A T E
!
FN.DEF getvkeybstate()
 BUNDLE.GET 1,"vkeyb",i
 FN.RTN i
FN.END
!=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
!=-=__end_graphiccontrolsKB.bas__=-=
!=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
