gformfill.bas

An attempt to do a graphics version of formfill.bas.
Limitations
	No-scolling.
	Max 10 items.

---------------------------------
** gformfill has been converted to fit the HEW framework 
   with re-usable objects.
   See HEW framework for code;
	(humpty.drivehq.com/promotes/rfo-basic/hew/hew.html)

---------------------------------
Changelog

2014-07-19
	Project Conversion to HEW framework.
	gformfill_make ()
	gformfill (itmtyp, itmlab, itmval)

2014-01-25
	Discontinued.
	v1.0 Last Standalone version.

