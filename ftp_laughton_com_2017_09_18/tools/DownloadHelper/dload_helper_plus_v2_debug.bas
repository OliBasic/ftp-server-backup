!* Download Helper Plus! ver. 2.1 *

URL$ = "http://laughton.com"
let source_path$ =  URL$ + "/basic/programs/"

cls
print "Welcome to the RFO Basic! Download Helper Plus! ver 2.1"
print "Please wait while the file list is being downloaded."
print ""

relist:

GrabURL result$, source_path$
split pre_list$[], result$, "<a href=" + chr$(34)

List.create S, program_list

array.length ln, pre_list$[]
for i= 1 to ln
	List.add program_list, left$(pre_list$[i], is_in(chr$(34), pre_list$[i])-1)
next i

! Have a choice to quit at end
List.add program_list, "QUIT!"

! OK, now list all the filenames
List.size program_list, slist

! First 7 lines are junk, so we will skip them
line_pad = 7

List.ToArray program_list, pre_show_array$[]
Array.copy pre_show_array$[line_pad], show_array$[]

Array.length length, show_array$[]
for i = 1 to length
	! Make sure spaces are displayed accurately and dir's are skipped
	temp_name$ = replace$(show_array$[i],"%20"," ")
	show_array$[i]=temp_name$
next i

do
	select dfnum, show_array$[], "Choose File to Download"
	List.get program_list, line_pad-1+dfnum, dfname$
	
	! see if user quits
	if dfname$ = "QUIT!" then end
	
	! see if it is a directory
	if is_in("/",dfname$) > 0 
		print "DEBUG: "; dfname$
		! See if user clicked on previous directory
		if is_in("/basic/programs/",dfname$) then 
			source_path$ = URL$ + dfname$
		elseif is_in("/basic/",dfname$) then
			source_path$ = URL$ + "/basic/programs/"
			else
			source_path$ = source_path$ + dfname$
		endif
		undim pre_show_array$[]
		undim show_array$[]
		undim pre_list$[]
		List.clear program_list
		print "Now loading directory "; source_path$
		goto relist
	endif
	
	! guess the destination directory based on the filename
	default_dest$ = "../data/"
	if is_in(".bas", dfname$) > 0 then default_dest$ = "../source/"
	if is_in(".db", dfname$) > 0 then default_dest$ = "../databases/"
	
	input "Enter Destination Directory", ddir$, default_dest$
	
	print ""
	print "Downloading "; source_path$; " to "; ddir$ + dfname$
	
	byte.open R, FN, source_path$+dfname$
	byte.copy FN, ddir$+dfname$	
	
	print "FILE DOWNLOAD COMPLETE!"
	
	pause 2000
	
until 0

end