!* Download Helper Plus! *

URL$ = "http://laughton.com"
source_path$ =  URL$ + "/basic/programs/"

cls
print "Welcome to the RFO Basic! Download Helper Plus!"
print "Please wait while the file list is being downloaded."
print ""

relist:
List.create S, program_list
Byte.open r, file_number, source_path$ 
Do 
	Byte.read file_number, Byte  
	! finds a quotation mark which is the start of a filename
	if Byte = 34 
		fname$=""
		Do
			Byte.read file_number, Byte 
			if Byte <> 34 then fname$ = fname$ + chr$(Byte)
		Until Byte = 34
		! print fname$
		List.add program_list, fname$
	endif
Until Byte < 0  
Byte.close file_number

! Have a choice to quit
List.add program_list, "QUIT!"

! OK, now list all the filenames
List.size program_list, slist

! First 7 lines are junk, so we will skip them
line_pad = 7

List.ToArray program_list, pre_show_array$[]
Array.copy pre_show_array$[line_pad], show_array$[]

Array.length length, show_array$[]
for i = 1 to length
	! Make sure spaces are displayed accurately and dir's are skipped
	temp_name$ = replace$(show_array$[i],"%20"," ")
	show_array$[i]=temp_name$
next i

do
	select dfnum, show_array$[], "Choose File to Download"
	List.get program_list, line_pad-1+dfnum, dfname$
	
	! see if user quits
	if dfname$ = "QUIT!" then end
	
	! see if it is a directory
	if is_in("/",dfname$) > 0 
		! See if user clicked on previous directory
		if is_in("/basic/programs/",dfname$) then 
			source_path$ = URL$ + dfname$
		else
			source_path$ = source_path$ + dfname$
		endif
		undim pre_show_array$[]
		undim show_array$[]
		List.clear program_list
		print "Now loading directory "; source_path$
		goto relist
	endif
	
	! guess the destination directory based on the filename
	default_dest$ = "../data/"
	if is_in(".bas", dfname$) > 0 then default_dest$ = "../source/"
	if is_in(".db", dfname$) > 0 then default_dest$ = "../databases/"
	
	input "Enter Destination Directory", ddir$, default_dest$
	
	print ""
	print "Downloading "; source_path$; " to "; ddir$ + dfname$
	
	byte.open R, FN, source_path$+dfname$
	byte.copy FN, ddir$+dfname$	
	
	pause 2000
	
until 0

end