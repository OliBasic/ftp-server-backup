Rem List Shuffle and List Swap
Rem With RFO Basic!
Rem December 2015
Rem Version 1.00
Rem By Roy Shepherd

gosub Functions

list.create N, numShuffle
list.add numShuffle, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10

list.create N, numSwap 
list.add numSwap, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10

print"List 'numShuffle' Not Suffled "
for x = 1 to 10 
	list.get numShuffle, x, thisNum 
	print thisNum 
next

call ListShuffle(numShuffle)
print"List 'numShuffle' Suffled "
for x = 1 to 10 
	list.get numShuffle, x, thisNum 
	print thisNum 
next
print"List 'numSwap' Not Swaped"
for x = 1 to 10 
	list.get numSwap, x, thisNum 
	print thisNum 
next
print"List 'numSwap' Swaped 2 with 8"
call ListSwap(numSwap, 2, 8)
for x = 1 to 10
	list.get numSwap, x, thisNum 
	print thisNum 
next

end"End of List Shuffle and List Swap demo"

Functions:
!-------------------------------------------------
! Put the list into an array and Shuffle the Array then put the Shuffled Array back into the List
!-------------------------------------------------
fn.def ListShuffle(ptr)
	list.toarray ptr, tempArray[]
	array.shuffle tempArray[]
	list.clear ptr 
	list.add.array ptr, tempArray[]
fn.end

!-------------------------------------------------
! using list.replace to Swap s1 with s2 
!-------------------------------------------------
fn.def ListSwap(ptr, s1, s2)
	List.get ptr, s1, n1
	List.get ptr, s2, n2
	
	list.replace ptr, s1, n2
	list.replace ptr, s2, n1
fn.end

return
