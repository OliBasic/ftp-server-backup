flagOri      = 0
GOSUB          openScreen
GOSUB          userfunctions




!someBackground ------------

tileSize     = 110
GOSUB          someBackground




!input data: file (adjust path if necessary) ------------

GRABFILE       tmp$  , "../source/wipelistviewdraw0.5.bas"
SPLIT          inp$[], tmp$ , chr $(10)

nn           = listview1 ( inp$[]     , 1         , ~
                           refh*0.03  , refw*0.01 , ~
                           refw*0.98  , refh*0.95 , ~
                           "Back"     , refw     , refh )


!mainLoop --------------------------

DO


 UNDIM         inp$[]
 FILE.DIR      "../data/", inp$[]

 sel         = listview1( inp$[]    , 0         , ~
                          refh*0.05 , refw*0.05 , ~
                          refw*0.4  , refh*0.9  , ~
                          "Back"    , refw      , refh  )

 UNDIM         selection$[]
 LIST.TOARRAY  sel , selection$[]
 nn          = listview1( SELECTion$[]        , 2          , ~
                          refh*0.55           , refw*0.55 , ~
                          refw *0.4           , refh *0.35 , ~
                          "your selection..." , refw       , refh  )



 POPUP         " brief stop in mainLoop..." , 0, 0, 0
 PAUSE         3000


UNTIL          0

! ---------------------------------







!--------------------------------------------
userfunctions:


!------------------------------
FN.DEF             listview1 (dd$[], scheme,  listTop, listLef, listWid , listHei, topStr$ , refw, refh  )
 
 movingFriction  = 0.94
 bounceFriction  = 0.4
 vlistmin        = 17
 dtSelect        = 300
 flagShowClocking= 0
 moveAmplify     = 1.2
 moveAmplify2    = 20


 ! colors/propertys ------------

 IF scheme       = 0
  txtAlign       = 1
  txtSz          = 20
  txtBold        = 1
  txtSkew        = -0.2
  txtTypeface    = 1
  flagLineNo     = 1
  flagFadeout    = 1
  flagReturnImm  = 0
  spacingOffs    = txtSz * 1.5
  topBotRectH    = 2     * spacingOffs
  botStr$        = "BACK"
  ARRAY.LOAD       cs[],~
  000 , 030 , 090 , 090 , 1   ~  % Background
  085 , 060 , 120 , 120 , 1   ~  % Top/bottom Rect
  255 , 030 , 090 , 090 , 1   ~  % Top/bottom Text
  255 , 000 , 255 , 000 , 1   ~  % ListText Selected
  255 , 255 , 255 , 255 , 1   ~  % ListText UnSelected
  060 , 000 , 150 , 150 , 1      % quickScrollRect
 ENDIF

 IF scheme       = 1
  txtAlign       = 1
  txtSz          = 18
  txtBold        = 1
  txtSkew        = 0
  txtTypeface    = 2
  flagLineNo     = 1
  flagFadeout    = 0
  flagReturnImm  = 0
  spacingOffs    = txtSz * 0.75
  topBotRectH    = 3   * spacingOffs
  botStr$        = "BACK"
  ARRAY.LOAD       cs[],~
  180 , 090 , 090 , 090 , 1   ~  % Background
  255 , 150 , 150 , 150 , 1   ~  % Top/bottom Rect
  255 , 220 , 220 , 220 , 1   ~  % Top/bottom Text
  255 , 255 , 255 , 000 , 1   ~  % ListText Selected
  255 , 255 , 255 , 255 , 1   ~  % ListText UnSelected
  020 , 000 , 000 , 180 , 1      % quickScrollRect
 ENDIF

 IF scheme       = 2
  txtAlign       = 2
  txtSz          = 18
  txtBold        = 1
  txtSkew        = -0.2
  txtTypeface    = 2
  flagLineNo     = 0
  flagFadeout    = 0
  flagReturnImm  = 0
  spacingOffs    = txtSz * 0.5
  topBotRectH    = 2.5  * txtSz
  botStr$        = "BACK"
  ARRAY.LOAD       cs[],~
  090 , 090 , 090 , 200 , 1   ~  % Background
  090 , 090 , 090 , 200 , 1   ~  % Top/bottom Rect
  255 , 220 , 220 , 220 , 1   ~  % Top/bottom Text
  255 , 255 , 255 , 000 , 1   ~  % ListText Selected
  255 , 255 , 255 , 255 , 1   ~  % ListText UnSelected
  020 , 000 , 000 , 180 , 1      % quickScrollRect
 ENDIF

 GR.GETDL         curDl[]
 GOSUB            setupList

 !mainFNLoop ------------------------------------------

 DO

  IF ABS(vListY)<= vlistmin  THEN vListY=0

  GR.TOUCH        touch, xx, yy
  IF touch        THEN GOSUB touchHandles

  IF flagBreak    THEN D_U.BREAK

  vListY        = vListY * movingFriction
  listY         = listY  + vListY * toc/50

  flagTmp       = listY > listCentY | listY+ssy < listCentY
  IF flagTmp      THEN GOSUB borderHandles

  t1            = clock ()
  GOSUB           moveList
  t2            = clock ()
  GR.RENDER
  t3            = clock ()

  IF flagShowClocking THEN
   GR.MODIFY       topTxt, "text",  FORMAT$("%%%",t3-tic) +"   /"+ ~
   FORMAT$("%%%",t2-t1)  +" /"+ ~
    FORMAT$("%%%",t3-t2)
    ENDIF

    DO
     toc          =  CLOCK() - tic
    UNTIL toc     >= 50
    tic           =  CLOCK()

   UNTIL 0
   !------------------------------------------------------

   LIST.CREATE      s, selection

   FOR i          = 1 TO lenlistBas
    IF result[i]  > 0 THEN LIST.ADD selection, dd$[i]
   NEXT
   LIST.SIZE        selection, sz
   IF !sz           THEN LIST.ADD selection, "(noItemsSelected)"

   GR.NEWDL         curDl[]
   GR.RENDER

   !RETURN
   FN.RTN           selection
   !=======================================================

   !------------------------------
   touchHandles:

   GR.BOUNDED.TOUCH trect1, listLef*scaw , p1 *scah, xend *scaw , p2 *scah
   GR.BOUNDED.TOUCH trect2, listLef*scaw , p3 *scah, xend *scaw , p4 *scah
   IF trect1 | trect2 THEN flagBreak=1

   GR.BOUNDED.TOUCH  touchB, listLef * scaw , 0, xend * scaw , 1200
   IF touchB THEN      GOSUB pickHandles

   RETURN
   !------------------------------


   !------------------------------
   SELECThandles:
   VIBRATE vib [],-1
   ! extra stuff for linebreak ----
   tmp               = ceil ( ( ys -listY ) / space )
   pos               = idxList[tmp]
   tmp2              = tmp
   IF tmp            > 1
    IF idxList[tmp-1]= pos THEN tmp2 = tmp-1
   ENDIF
   IF tmp            < lenlist
    IF idxList[tmp+1]= pos THEN tmp2 = tmp+1
   ENDIF
   !------
   result[pos]       = !result [pos]
   IF                  result [pos]
    GR.MODIFY          grlist[tmp]  ,"paint", selectedPaint
    GR.MODIFY          grlist[tmp2] ,"paint", selectedPaint
   ELSE
    GR.MODIFY          grlist[tmp]  ,"paint", notSelectedPaint
    GR.MODIFY          grlist[tmp2] ,"paint", notSelectedPaint
   ENDIF
   !sel$             = sel$ +"_"+STR$(pos)
   !GR.MODIFY          topTxt , "text" , sel$

   flagselect        =  1

   IF flagReturnImm     THEN flagBreak=1

   RETURN
   !------------------------------


   !------------------------------
   borderHandles:

   vListY            = -vListY * bounceFriction
   IF listY          > listCentY THEN listY= listCentY-space/2+txtsz*0.7
   IF listY+ssy      < listCentY THEN listY = listCentY -ssy - space/2+txtsz*0.7
   !IF ABS(vListY)   > vibBorderVal THEN VIBRATE vib[],-1

   RETURN
   !------------------------------


   !------------------------------

   pickHandles:

   ticPick        = clock ()
   ptrTmp         =  0
   GR.TOUCH          touch2 , xs ,ys
   ys             =  ys / scah
   xs             =  xs / scah
   moveAmpli      =  moveAmplify
   IF xs          >  quickScrollRng THEN
    moveAmpli     =  moveAmplify2
    GR.SHOW          quickSrollRect
   ENDIF
   ctr            =  0
   dlistMx        =  listCentY - listy       + 0 * space
   dlistMn        =  listCentY - listy - ssy - 1.5 * space
   dlistMoveMn    =  10
   flagmove       =  0
   flagselect     =  0
   DO            %-----------
    ctr           += 1
    yold          =  y
    GR.TOUCH         touch2 , x ,y
    x             =  x / scaw
    y             =  y / scah
    ptrTmp        += 1
    IF ptrTmp     >  lenVBuff THEN ptrTmp =1
    VBuff[ptrTmp] =  yold-y
    dlisty        =  (y-ys)* moveAmpli
    IF dlisty     >  dlistMx THEN dlisty = dlistMx
    IF dlisty     <  dlistMn THEN dlisty = dlistMn
    IF ABS(dlisty)>  dlistMoveMn THEN flagmove=1

    flagtmp       =  !flagmove & !flagselect & CLOCK()-ticPick>dtSelect & ys>listy & ys<listy+ssy+space
    IF flagtmp       THEN GOSUB SELECThandles

    IF flagBreak    THEN D_U.BREAK
    GOSUB            moveList
    GR.RENDER
    DO
     toc          =  CLOCK() - tic
    UNTIL toc     >= 50
    tic = CLOCK()
   UNTIL !touch2  %-----------

   listY          =  listY + dlistY
   dlistY         =  0
   ARRAY.AVERAGE     vlist , VBuff[]
   vListY         =  vlist * - moveAmplify
   GR.HIDE           quickSrollRect

   RETURN
   !------------------------------


   !------------------------------
   moveList:

   idxMoveSold     = idxMoveS
   idxMoveEold     = idxMoveE
   idxMoveS        = ceil ((listTop-listy-dlistY)/space) +1
   idxMoveE        = idxMoveS + lenlistIdx - 1
   IF idxMoveS     < 0         THEN idxMoveS = 0
   IF idxMoveE     > lenlist   THEN idxMoveE = lenlist

   dytmp           = listy + dlistY - space/2
   FOR i           = idxMoveS+1 TO idxMoveE
    yact           = dytmp + i* space
    GR.SHOW          grlist[i]
    GR.MODIFY        grlist[i], "y"     , yact
    IF flagFadeout   THEN GR.MODIFY grlist[i],"alpha",255-ABS(yact- listCenty)*liTmp1
   NEXT
   FOR i           = idxMoveE+1 TO idxMoveEold+1
    GR.HIDE          grlist[i]
   NEXT
   FOR i           = idxMoveSold+1 TO idxMoveS
    GR.HIDE          grlist[i]
   NEXT

   RETURN
   !------------------------------



   !------------------------------
   setupList:

   centW           = refW/2
   centH           = refH/2
   GR.SCREEN         curW, curH
   scaW            = curW / refW
   scaH            = curH / refH

   lenVBuff        = 5
   DIM               VBuff[ lenVBuff ]
   ARRAY.LOAD        vib [], 0, 30
   ARRAY.LENGTH      lenlistBas, dd $[]
   DIM               grlist [lenlistBas*2]
   DIM               result [lenlistBas]
   listBot         = listTop + listHei
   listCenty       = listTop + listHei/2
   listCentx       = listLef + listWid/2
   space           = txtSz   + spacingOffs
   lenlistIdx      = (listBot  - listTop -2*topBotRectH) / space +1
   listy           = listCentY - txtsz*0.3
   quickScrollRng  = listLef  + listWid *0.9
   liTmp1          = 255 / (listHei/2)

   p1              = listTop - 0
   p2              = listTop + topBotRectH
   p3              = listbot - topBotRectH
   p4              = listbot + 0
   xend            = listLef + listWid


   ! Background      ----------
   no              =  0 * 5
   GR.COLOR           cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
   GR.RECT            backGrRect , listLef, p1 , xend , p4

   no              =  5 * 5
   GR.COLOR           cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
   GR.RECT            quickSrollRect , quickScrollRng , p2 , xend , p3
   GR.HIDE            quickSrollRect



   !ListText     ----------
   GR.TEXT.ALIGN      txtAlign
   GR.TEXT.SIZE       txtSz
   GR.TEXT.BOLD       txtBold
   GR.TEXT.SKEW       txtSkew
   GR.TEXT.TYPEFACE   txtTypeface
   no               = 3 * 5
   GR.COLOR           cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
   GR.PAINT.GET       selectedPaint
   no               = 4 * 5
   GR.COLOR           cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
   GR.PAINT.GET       notSelectedPaint

   LIST.CREATE        n,idxList
   lineCtr          = 0
   IF txtAlign      = 1 THEN txtXpos = listLef + 5
   IF txtAlign      = 2 THEN txtXpos = listLef + listWid/2
   IF txtAlign      = 3 THEN txtXpos = listLef + listWid - 5


   FOR i            = 1 TO lenlistBas

    IF flagLineNo     THEN noStr$ =REPLACE$(FORMAT$("####",i)," ","")+"  "
    tmp$            = noStr$ + dd$[i]

    !linebreakStuff part1 --
    GR.TEXT.WIDTH     txtwidth, tmp$
    IF txtwidth+5   > listWid
     GR.TEXT.WIDTH    oxTmp ,  noStr$
     maxLentmp      = FLOOR((listWid-oxTmp-5)/(txtwidth)*LEN(tmp$)*0.90)
     tmp2$          = RIGHT$(tmp$, LEN(tmp$)-maxLentmp)
     tmp$           = left$ (tmp$, maxLentmp)
     GR.TEXT.WIDTH    txtwidth, tmp2$
     IF txtwidth+5  > listWid
      maxLentmp     = FLOOR((listWid-oxTmp-5)/(txtwidth)*LEN(tmp2$)*0.90)
      tmp2$         = left$ (tmp2$, maxLentmp)
     ENDIF
    ENDIF

    lineCtr         += 1
    GR.TEXT.DRAW      grlist[lineCtr] , txtXpos        , 0 , tmp$
    GR.HIDE           grlist[lineCtr]
    LIST.ADD          idxList,i

    !linebreakStuff part2 --
    IF tmp2$       <> ""
     lineCtr       += 1
     tmpx          =  txtXpos
     IF txtAlign   =  1 THEN tmpx  = txtXpos + oxTmp+5
     GR.TEXT.DRAW     grlist[lineCtr] , tmpx , 0,tmp2$
     GR.HIDE          grlist[lineCtr]
     LIST.ADD         idxList,i
     tmp2$=""
    ENDIF

   NEXT

   LIST.TOARRAY      idxList , idxList[]
   lenlist         = linectr
   ssy             = space * (lenlist-1)



   !Top/bottomRect & Text -----------
   no              = 1 * 5
   GR.COLOR          cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
   GR.RECT           topRect , listLef , p1 , xend , p2
   GR.RECT           botRect , listLef , p3 , xend , p4

   no              = 2 * 5
   GR.COLOR          cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
   szTmp           = topBotRectH *0.4
   tmp2            = szTmp/2*1.4
   GR.TEXT.SIZE      szTmp
   GR.TEXT.ALIGN     2
   GR.TEXT.DRAW      topTxt  , listLef + listWid /2 ,p2 - tmp2 , topStr$
   GR.TEXT.DRAW      botTxt  , listLef + listWid /2 ,p4 - tmp2 , botStr$

   RETURN
   !------------------------------


  FN.END
  !------------------------------



  RETURN
  !--------------------------------------------



  !--------------------------------------------
  openScreen:

  refW       = 780
  refH       = 1280
  IF !flagOri  THEN SWAP refW, refH
  centW      = refW/2
  centH      = refH/2
  GR.OPEN      255,20,0,0,1,flagOri
  GR.SCREEN    curW, curH
  scaW       = curW / refW
  scaH       = curH / refH
  GR.SCALE     scaW , scaH

  RETURN
  !--------------------------------------------



  !--------------------------------------------
  someBackground:
  csBas=80
  sz = tileSize
  GR.set. stroke 2
  szObj = sz*0.6/2
  FOR i=0 TO refh STEP sz
   FOR k=0 TO refw STEP sz
    ctr+=1
    GR.COLOR  255, 30, 30, 30, 0
    IF RND() > 0.9 THEN GR.COLOR 90, csBas+RND()*(255-csBas) , csBas+RND()*(255-csBas)  , csBas+RND()*(255-csBas) , 0
    IF rnd ()> 0.5 THEN

     GR.RECT nn, k-szObj , i-szObj , k+szObj , i+szObj
     !   gr.hide nn
    ELSE
     GR.CIRCLE nn, k, i, szObj
     !   gr.hide nn
    ENDIF
   NEXT
  NEXT
  PRINT ctr
  pop$ = "count grObj main loop: "+STR$(ctr)
  POPUP pop$, 0, 0, 0

  RETURN
  !--------------------------------------------




