!open graphics -----------------------
flagOri                = 1
refW                   = 790
refH                   = 1255
IF !flagOri              THEN SWAP refW, refH
GR.OPEN                  255, 0 , 0 , 0 ,1,flagOri
GR.SCREEN                curW, curH
scaW                   = curW / refW
scaH                   = curH / refH
GR.SCALE                 scaW , scaH
!PAUSE                    500


!canvas size/position/color --------------
cvx                    = refw * 0.95
cvy                    = refh * 0.85
pcx                    = (refw-cvx)/2
pcy                    = (refh-cvy)/2
ARRAY.LOAD               cvC[], 000, 20 , 70

GR.BITMAP.CREATE         canvas, cvx, cvy
GOSUB                    performGrCls


!data to display ---------------------------------------
pa$                    = "../source/Sample_Programs/"
dat$                   = "f23_breakout.bas"
GRABFILE                 li$, pa$ +dat$
SPLIT                    li$[], li$, CHR$(10)
!file.dir                  "", li$[]
ARRAY.LENGTH             nlines, li$[]
DIM                      tc [nlines, 3]    % color of each element

!propertys -------------
space                  = 35
txtsz                  = space * 0.55
txtTyface              = 1
txtBold                = 1
flagApplyLiNo          = 1
moveAmpli              = 3
flagDrawLines          = 1
cli                    = 190     % (gray)color of lines
selTime                = 200     % time w/o movement for select a line
!free-run dynamics -----
vListStop              = 3
moveFriction           = 0.95
freeRunAmpli           = moveAmpli * 0.7
!c1/c2: text-colors | csel: textCol when selected -----
ARRAY.LOAD               c1   [], 255 , 255 , 255
ARRAY.LOAD               c2   [], 190 , 190 , 255
ARRAY.LOAD               csel [], 255 , 160 , 120
!'constants'--------
k1                     = INT(cvy/space+1)


!initialize list --------
GR.TEXT.SIZE             txtsz
GR.TEXT.BOLD             txtBold
GR.TEXT.TYPEFACE         txtTyface
GR.GET.TEXTBOUNDS        "W", lef , t, rig, b
txtOffs                = space/2 - ABS(t)/2
LIST.CREATE              n, selected

!format .bas-file ------
FOR i                  = 1 TO nlines
 flagCo1               = LEFT$(REPLACE$(li$[i]," ",""),1) = "!"
 flagCo2               = IS_IN("!!", li$[i])
 flagRise              = 0
 IF                      flagCo2   & !flagComm THEN
  flagComm             = 1
  flagRise             = 1
 ENDIF
 IF        flagCo2 & !flagRise & flagComm THEN flagComm =0

 IF  flagApplyLiNo       THEN
  IF li$[i]<>"" & !flagCo1 & !flagComm THEN
   prgCtr ++
   li$[i] = REPLACE$(FORMAT$("%%%%", prgCtr ) ," ","") +"   " + li$[i]
  ELSE
   li$[i] = "    "+ li$[i]
  ENDIF
 ENDIF

 IF  flagCo1 | flagComm THEN
  LET tc [i,1]         = c2 [1]
  LET tc [i,2]         = c2 [2]
  LET tc [i,3]         = c2 [3]
 ELSE
  LET tc [i,1]         = c1 [1]
  LET tc [i,2]         = c1 [2]
  LET tc [i,3]         = c1 [3]
 END if

NEXT


! keep(copy) original colors -----
ARRAY.COPY              tc[], tcOrg[]


!initual drawing ----------
GOSUB                   _letsRoll


!divers -------------------------------
ARRAY.LOAD               vib1 [], 0, 50
flaginfo               = 1
dtdes                  = 50
toc                    = 30
tocfilt                = toc
tic                    = CLOCK()
flagStarted            = 1


!main --------------------------------------------
DO

 GR.TOUCH                tou, toux, touy
 IF                      tou           THEN GOSUB _letsRoll
 IF                     !tou & touOld  THEN GOSUB _released
 IF                     !tou & vList   THEN GOSUB _keepRollin
 IF                      flaginfo      THEN gosub  info

 GR.RENDER

 LET tocfilt           = tocfilt+(toc-tocfilt) * 0.05
 LET toc               = CLOCK()-tic
 PAUSE                   MAX(dtdes-toc, 1)
 LET tic               = CLOCK()

UNTIL0
!--------------------------------------------------


!-------------------------------
touchedIni:
LET x0                 = toux
LET y0                 = touy
LET ticTou0            = CLOCK()
LET touOld             = tou
RETURN
!-------------------------------
_letsRoll:
IF                       !touOld THEN gosub touchedIni
IF                       !flagMovedOnce THEN gosub _selector

LET dyli               = (touy-y0) *moveAmpli
LET yliTmp             = MAX(MIN(yli+dyli, 0), -nlines*space)
LET sIdx               = INT(-yliTmp /space+1)
LET eIdx               = MIN(k1+sIdx, nlines)

GR.COLOR                 255, cvc[1], cvc[2], cvc[3] , 1
GR.RECT                  nn, 0, 0, cvx, cvy
GR.COLOR                 255, tc[1,1], tc[1,2], tc[1,3], 1
FOR i                  = sIdx TO eIdx
 LET ytmp              = i*space + yliTmp
 IF flagDrawLines
  GR.COLOR               255, cli , cli , cli, 0
  GR.LINE                 nn, 0 , ytmp   , cvx , ytmp
 END if
 GR.COLOR                 255, tc[i,1], tc[i,2], tc[i,3], 1
 GR.TEXT.DRAW            nn, 5 , ytmp -txtOffs , li$[i]
NEXT
LET vList              = touy-touOldy
LET touOldy            = touy
RETURN
!-------------------------------
_released:
yli                    = yliTmp
dyli                   = 0
flagMovedOnce          = 0
touOld                 = 0
GOSUB                    performGrCls
RETURN
!-------------------------------


!-------------------------------
_selector:
LET dtTou              = CLOCK()-ticTou0
LET flagMovedOnce      = ABS(touy-y0)>6|ABS(toux-x0)>6
IF                       dtTou>selTime & !flagMovedOnce THEN
 idxSel                = INT((touy/scah-pcy-yli)/space+1)
 VIBRATE                 vib1[],-1
 LIST.SEARCH             selected ,idxSel, res
 IF res THEN
  LIST.REMOVE            selected , res
  tc[ idxSel ,1]       = tcOrg [idxSel*3-2]
  tc[ idxSel ,2]       = tcOrg [idxSel*3-1]
  tc[ idxSel ,3]       = tcOrg [idxSel*3]
 ELSE
  LIST.ADD               selected , idxSel
  tc[ idxSel ,1]       = csel [1]
  tc[ idxSel ,2]       = csel [2]
  tc[ idxSel ,3]       = csel [3]
 ENDIF
 flagMovedOnce         = 1
ENDIF
RETURN
!-------------------------------


!-------------------------------
_keepRollin:
LET vList              = vList * moveFriction
LET dyli               = vlist * freeRunAmpli
LET yliTmp             = MAX(MIN(yli+dyli, 0), -nlines*space)
LET sIdx               = INT(-yliTmp /space+1)
LET eIdx               = MIN(k1+sIdx, nlines)

GR.COLOR                 255, cvc[1], cvc[2], cvc[3] , 1
GR.RECT                  nn, 0, 0, cvx, cvy
!GR.COLOR                 255, tc[1,1], tc[1,2], tc[1,3], 1
FOR i                  = sIdx TO eIdx
 LET ytmp              = i*space + yliTmp
 IF flagDrawLines
  GR.COLOR               255, cli , cli , cli, 0
  GR.LINE                 nn, 0 , ytmp   , cvx , ytmp
 END if
 GR.COLOR                 255, tc[i,1], tc[i,2], tc[i,3], 1
 GR.TEXT.DRAW            nn, 5 , ytmp -txtOffs , li$[i]
NEXT
LET yli                 = yliTmp
LET dyli                = 0
IF                        ABS(vList)< vListStop THEN vList=0
RETURN
!-------------------------------


!-------------------------------
performGrCls :

GR.BITMAP.DRAWINTO.END

GR.CLS
GR.BITMAP.DRAW           cvs, canvas, pcx , pcy

GR.COLOR                 255, 255, 255 , 255 , 1
GR.TEXT.SIZE             25
GR.TEXT.BOLD             1
GR.TEXT.DRAW             info1, 20,  75,  "xx"
GR.TEXT.DRAW             info2, 20,  refh-50,"xx"

IF flagstarted
 GR.TEXT.SIZE             txtsz
 GR.TEXT.BOLD             txtBold
 GR.TEXT.TYPEFACE         txtTyface
ENDIF

GR.BITMAP.DRAWINTO.START canvas

RETURN
!-------------------------------



!-------------------------------
info:

GR.MODIFY        info1, "text", ~
INT$(tocfilt)                + " [ms]  /  " + ~
INT$(ROUND(1000/tocfilt, 0)) + " [fps] /   " +~
STR$(ROUND(vlist, 1)) + " /   " + INT$(t3-t2)

GR.MODIFY        info2, "text", ~
"dyli: " + INT$(dyli) + ~
" / yli: " + INT$(yli)  + ~
" / yliTmp : " + INT$( yliTmp ) + ~
" / startIdx: " + INT$( sIdx )

RETURN
!-------------------------------
