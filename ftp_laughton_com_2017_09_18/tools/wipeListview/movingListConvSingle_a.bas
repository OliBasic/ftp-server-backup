!open graphics -----------------------
flagOri                = 1
refW                   = 790
refH                   = 1255
IF !flagOri              THEN SWAP refW, refH
GR.OPEN                  255, 0 , 0 , 0 ,1,flagOri
GR.SCREEN                curW, curH
scaW                   = curW / refW
scaH                   = curH / refH
GR.SCALE                 scaW , scaH
!PAUSE                    500


!canvas size/position/color --------------
cvx                    = refw * 1.0
cvy                    = refh * 0.75
pcx                    = 20
pcy                    = (refh-cvy)/2

GR.COLOR                 255, 255, 255, 000 , 1
GR.TEXT.SIZE             22
GR.TEXT.DRAW             info1,20,  70,  "xx"
GR.TEXT.DRAW             info2,20,  refh-30,"xx"




!data to display ---------------------------------------
pa$                    = "../source/Sample_Programs/"
dat$                   = "f01_commands.bas"
GRABFILE                 li$, pa$ +dat$
SPLIT                    li$[], li$, CHR$(10)
!file.dir                  "", li$[]
ARRAY.LENGTH             nlines, li$[]
DIM                      tc [nlines, 3]    % color of each element

!propertys -------------
space                  = 33
cvy                    = INT(cvy/space)*space
txtsz                  = space*0.55
txtTyface              = 2
txtBold                = 1
flagApplyLiNo          = 1
moveAmpli              = 7
flagDrawLines          = 1
cli                    = 50     % (gray)color of lines
selTime                = 200     % time w/o movement for select a line
txtPosx                = 35
!free-run dynamics -----
vListStop              = 3
moveFriction           = 0.92
freeRunAmpli           = moveAmpli * 0.5
!c1/c2: text-colors | csel: textCol when selected -----
ARRAY.LOAD               c1   [], 255 , 255 , 255
ARRAY.LOAD               c2   [], 190 , 190 , 255
ARRAY.LOAD               csel [], 255 , 160 , 120
!'constants'--------
k1                     = INT(cvy/space+1)
k2                     = -((nlines-INT(cvy/space)-1 )*space)
k3                     = pcy-txtoffs %- space/2
nrows                  = INT(cvy/space)+1

!initialize list --------
GR.TEXT.SIZE             txtsz
GR.TEXT.TYPEFACE         txtTyface
GR.TEXT.BOLD             txtBold
GR.GET.TEXTBOUNDS        "W", lef , t, rig, b
txtOffs                = space/2 - ABS(t)/2
!GR.COLOR                 255, txc[1], txc[2], txc[3] , 1

GR.COLOR                 255 , 255 , 255 , 255 , 1
GR.TEXT.SIZE              txtsz
DIM pai [nlines], pai2 [nlines], ptrTxt[nlines]


! static background ------------------
GR.TEXT.SIZE              15
GR.BITMAP.CREATE          listBack  , cvx, cvy+space
GR.BITMAP.DRAW           _listBack, listBack, pcx, pcy
GR.BITMAP.DRAWINTO.START  listBack
FOR i                   = 1 TO nrows
 GR.COLOR                  255,  cli, cli +MOD(i,2)*5-3 , cli , 1
 GR.RECT                   jj ,  0, (i-1)*space, cvx, i*space
 GR.COLOR                  255,  99, 99, 99 , 1
 GR.TEXT.DRAW              nn, 5, i*space-txtoffs, int $(i)
NEXT
GR.BITMAP.DRAWINTO.END
GR.RENDER
GR.TEXT.SIZE             txtsz
! --------------------------------------





!format ----------------------------------
i                     = 1
GR.COLOR                255, c2[1], c2 [2], c2 [3], 1 
GR.PAINT.GET            pai [i]
GR.TEXT.DRAW            ptrTxt[i] , pcx+txtPosx, pcy+i*space-txtOffs , li$[i]

FOR i                 = 2 TO nlines
 IF                     LOWER$(LEFT$(li$[i-1],1)) <> LOWER$(LEFT$(li$[i],1)) THEN ctrcol++
 IF                     !mod(ctrcol,2) THEN GR.COLOR  255, c2[1], c2 [2], c2 [3], 1 ELSE GR.COLOR   255, c1[1], c1 [2], c2 [3], 1
 GR.PAINT.GET pai [i]
 IF                     i <= nrows THEN GR.TEXT.DRAW  ptrTxt[i] , pcx+txtPosx, pcy+i*space-txtOffs , li$[i]
 IF                     i=nrows+1 THEN gr.render
NEXT


! countdown main ---------------------------
flaginfo               = 1
dtdes                  = 50
toc                    = 30
tocfilt                = toc
tic                    = CLOCK()
!main---------------------------------------
DO

 touOld                = tou
 GR.TOUCH                tou, toux, touy
 IF                      tou&!touOld THEN gosub touched
 IF                      tou&touOld  THEN gosub holded
 IF                     !tou&touOld  THEN gosub released

 IF flaginfo             THEN GOSUB info

 IF !BACKGROUND() THEN   GR.RENDER

 tocfilt               = tocfilt+(toc-tocfilt)*0.05
 toc                   = CLOCK()-tic
 PAUSE                   MAX(dtdes-toc, 1)
 tic                   = CLOCK()

UNTIL0
!------------------------------------------


!-------------------------------
touched:
x0                     = toux
y0                     = touy
RETURN
!-------------------------------
holded:
LET dyli               = (touy-y0) *moveAmpli
LET yliTmp             = MAX(MIN(yli+dyli, 0), k2)
LET sIdx               = -yliTmp /space +1
FOR i                  = 1 TO nrows
 GR.MODIFY      ptrTxt[i], "text",li$[i+sIdx-1],"paint", pai[i+sidx-1]
NEXT
RETURN
!-------------------------------
released:
yli                    = yliTmp
dyli                   = 0
RETURN
!-------------------------------


!-------------------------------
info:

GR.MODIFY        info1, "text", ~
INT$(tocfilt)                + " [ms]  /  " + ~
INT$(ROUND(1000/tocfilt, 0)) + " [fps]  "

GR.MODIFY        info2, "text", ~
"dyli: " + INT$(dyli) + ~
" / yli: " + INT$(yli)  + ~
" / sum: " + INT$( yliTmp ) + ~
" / startIdx: " + INT$(sIdx)

RETURN
!-------------------------------
