flagOri     = 1
GOSUB openScreen
GOSUB userfunctions



!someBackground ------------

tileSize    = 200
GOSUB someBackground



!input data: file (adjust path if necessary) ------------

GRABFILE tmp $,"../source/wipelistviewdraw0.4.bas"
SPLIT inp$[], tmp$,chr $(10)
scheme      = 1
sel         = listview1 ( inp$[] , scheme , refw, refh )

POPUP "starting  main loop...." , 0, 0, 0
PAUSE 1000


scheme      = 0

DO


 UNDIM inp$[]
 FILE.DIR "../data/", inp$[]

 sel       = listview1 ( inp$[]       , scheme , refw, refh )


 UNDIM selection$[]
 LIST.TOARRAY sel , selection$[]

 nn        = listview1 ( selection$[] , scheme , refw, refh )



 POPUP "back in main loop...." , 0, 0, 0
 PAUSE 2000


UNTIL 0
















!--------------------------------------------
userfunctions:


!------------------------------
FN.DEF listview1 (dd$[], scheme, refw, refh  )
 !lview:


 movingFriction = 0.95
 bounceFriction = 0.4
 shotAmplify    = 2
 moveAmplify    = 2
 vlistmin       = 17


 ! colors/propertys ------------

 IF scheme       = 0
  txtAlign       = 2
  txtSz          = 24
  txtBold        = 1
  txtSkew        = -0.2
  txtTypeface    = 1
  flagLineNo     = 0
  listTop        = refh  * 0.2
  listBot        = refh  * 0.8
  spacingOffs    = txtSz * 2
  ssx            = refw  * 0.75
  topBotRectH    = 1 * txtSz
  ARRAY.LOAD       cs[],~
  255 , 030 , 090 , 090 , 1   ~  % Background
  000 , 100 , 100 , 100 , 1   ~  % light-gray coverRects
  255 , 060 , 120 , 120 , 1   ~  % Top/bottom Rect
  255 , 030 , 090 , 090 , 1   ~  % Top/bottom Text
  255 , 200 , 200 , 200 , 0   ~  % selectRect snapped
  255 , 090 , 090 , 090 , 0   ~  % selectRect moving
  255 , 000 , 255 , 000 , 1   ~  % ListText Selected
  255 , 255 , 255 , 255 , 1      % ListText UnSelected
 ENDIF

 IF scheme       = 1
  txtAlign       = 1
  txtSz          = 20
  txtBold        = 1
  txtSkew        = -0.2
  txtTypeface    = 2
  flagLineNo     = 1
  listTop        = refh  * 0.05
  listBot        = refh  * 0.95
  spacingOffs    = txtSz * 1.2
  ssx            = refw  * 0.98
  topBotRectH    = 1 * txtSz
  ARRAY.LOAD       cs[],~
  080 , 090 , 090 , 090 , 1   ~  % Background
  060 , 070 , 070 , 070 , 1   ~  % light-gray coverRects
  255 , 150 , 150 , 150 , 1   ~  % Top/bottom Rect
  255 , 220 , 220 , 220 , 1   ~  % Top/bottom Text
  255 , 200 , 200 , 200 , 0   ~  % selectRect snapped
  255 , 090 , 090 , 090 , 0   ~  % selectRect moving
  255 , 255 , 255 , 000 , 1   ~  % ListText Selected
  255 , 000 , 000 , 255 , 1      % ListText UnSelected
 ENDIF

 GR.GETDL curDl[]
 GOSUB setupList


 !mainFNLoop ------------------------------------------

 DO

  IF ABS(vListY)<=vlistmin  THEN GOSUB vlistminHandles

  GR.TOUCH        touch, xx, yy
  IF touch      > 0 THEN GOSUB touchHandles

  IF flagBreak    THEN D_U.BREAK

  vListY        = vListY * movingFriction
  listY         = listY  + vListY

  flagTmp       = listY > centh | listY+ssy < centh
  IF flagTmp      THEN GOSUB borderHandles

  GOSUB           moveList

  GR.RENDER

  DO
   toc          =  CLOCK() - tic
  UNTIL toc     >= 50
  tic           =  CLOCK()

 UNTIL 0
 !------------------------------------------------------

 DO
  GR.TOUCH        tt, xx, yy
 UNTIL! tt
 LIST.CREATE      s, selection
 FOR i          = 1 TO lenlistBas
  IF result[i]  > 0 THEN LIST.ADD selection, dd$[i]
 NEXT
 LIST.SIZE        selection, sz
 IF !sz           THEN LIST.ADD selection, "(noItemsSelected)"


 ! test recursive calling
 ! LIST.TOARRAY selection , selection$[]
 ! nn        = listview1 ( selection$[], 0 , refw, refh )

 GR.NEWDL curDl[]
 GR.RENDER

 !RETURN
 FN.RTN         selection
 !=======================================================

 !------------------------------
 touchHandles:

 GR.BOUNDED.TOUCH trect1, listX*scaw , p1 *scah, xend *scaw , p2 *scah
 GR.BOUNDED.TOUCH trect2, listX*scaw , p3 *scah, xend *scaw , p4 *scah
 IF trect1| trect2 THEN flagBreak=1

 GR.BOUNDED.TOUCH trect, ~
 listX * scaw   , (centh - space/2 )*scah, ~
 xend * scaw    , (centh + space/2 )*scah
 IF trect THEN
  IF vlisty =0 THEN GOSUB selecthandles
 ENDIF

 GR.BOUNDED.TOUCH  touchB, listX * scaw , 0, xend * scaw , 1200
 IF touchB THEN GOSUB pickHandles

 RETURN
 !------------------------------


 !------------------------------
 SELECThandles:

 DO
  GR.TOUCH tt, nn, mm
 UNTIL ! tt
 VIBRATE vib [],-1
 ! extra stuff for linebreak ----
 tmp               = ceil ( ( centh -listY) / space ) + 1
 pos               = idxList[tmp]
 tmp2              = tmp
 IF tmp            > 1
  IF idxList[tmp-1] = pos THEN tmp2 = tmp-1
 ENDIF
 IF tmp            < lenlist
  IF idxList[tmp+1] = pos THEN tmp2 = tmp+1
 ENDIF
 !------
 result[pos]       = !result [pos]
 IF result [pos]
  GR.MODIFY          grlist[tmp]  ,"paint", selectedPaint
  GR.MODIFY          grlist[tmp2] ,"paint", selectedPaint
 ELSE
  GR.MODIFY         grlist[tmp]  ,"paint", notSelectedPaint
  GR.MODIFY         grlist[tmp2] ,"paint", notSelectedPaint
 ENDIF
 !sel$            =  sel$ +"_"+STR$(pos)
 !GR.MODIFY          topTxt , "text" , sel$
 RETURN
 !------------------------------


 !------------------------------
 vlistminHandles:

 vListYold = vListY
 vListY    = 0
 IF !vListY & vListYold
  f1            =  round ( ( centh -listY + txtsz/2) / space )
  listY         =  centh - f1 * space + txtsz/2
  GR.MODIFY        selRect,"paint", rectSnapped
 ENDIF

 RETURN
 !------------------------------


 !------------------------------
 borderHandles:

 vListY          = -vListY * bounceFriction
 IF listY        > centh THEN listY = centh  + txtsz/2
 IF listY+ssy    < centh THEN listY = centh -ssy + txtsz/2
 !IF ABS(vListY) > vibBorderVal THEN VIBRATE vib[],-1

 RETURN
 !------------------------------


 !------------------------------
 pickHandles:

 GR.MODIFY         selRect ,"paint", rectMoving
 ptrTmp         =  0
 GR.TOUCH         touch2 , xs ,ys
 ys             =  ys / scah
 ctr            =  0
 dlistMx        =  centh - listy       + space
 dlistMn        =  centh - listy - ssy - space
 DO            %-----------
  ctr           += 1
  yold          =  y
  GR.TOUCH         touch2 , x ,y
  y             =  y / scah
  ptrTmp        += 1
  IF ptrTmp     >  lenVBuff THEN ptrTmp =1
  VBuff[ptrTmp] =  yold-y
  dlisty        =  (y-ys)* moveAmplify
  IF dlisty     >  dlistMx THEN dlisty = dlistMx
  IF dlisty     <  dlistMn THEN dlisty = dlistMn
  GOSUB            moveList
  GR.RENDER
  DO
   toc          =  CLOCK() - tic
  UNTIL toc     >= 50
  tic = CLOCK()
 UNTIL !touch2  %-----------

 listY         =  listY + dlistY
 dlistY        =  0
 ARRAY.AVERAGE    vlist , VBuff[]
 IF ctr        <  lenVBuff+1 THEN vlist = 1
 vListY        =  vlist * - shotAmplify
 IF ABS(vListY)< vlistmin THEN GOSUB vlistminHandles

 RETURN
 !------------------------------


 !------------------------------
 moveList:

 idxMoveSold     = idxMoveS
 idxMoveEold     = idxMoveE
 idxMoveS        = ceil ((listTop-listy-dlistY)/space)
 idxMoveE        = idxMoveS + lenlistIdx - 1
 IF idxMoveS     < 0         THEN idxMoveS = 0
 IF idxMoveE     > lenlist   THEN idxMoveE = lenlist

 dytmp           = listy + dlistY - space
 FOR i           = idxMoveS+1 TO idxMoveE
  yact           = dytmp + i* space
  GR.SHOW          grlist[i]
  GR.MODIFY        grlist[i], "y" , yact
 NEXT
 FOR i           = idxMoveE+1 TO idxMoveEold+1
  GR.HIDE          grlist[i]
 NEXT
 FOR i           = idxMoveSold+1 TO idxMoveS
  GR.HIDE          grlist[i]
 NEXT
 IF listy+ssy    > listbot THEN  gr.hide grlist[lenlist]

 RETURN
 !------------------------------



 !------------------------------
 setupList:

 centW           = refW/2
 centH           = refH/2
 GR.SCREEN         curW, curH
 scaW            = curW / refW
 scaH            = curH / refH

 lenVBuff        = 5
 DIM               VBuff[ lenVBuff ]
 ARRAY.LOAD        vib [], 0, 30
 ARRAY.LENGTH      lenlistBas, dd $[]
 DIM               grlist [lenlistBas*2]
 DIM               result [lenlistBas]
 space           = txtSz + spacingOffs
 !ssy            = space * lenlist
 lenlistIdx      = ( listBot - listTop ) / space +1
 listx           = centw - ssx/2
 listy           = centh + txtSz/2


 p1              = listTop - topBotRectH
 p2              = listTop + topBotRectH
 p3              = listbot - topBotRectH
 p4              = listbot + topBotRectH
 xend            = listX+ssx


 ! Background      ----------
 no              = 0 * 5
 GR.COLOR          cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
 GR.RECT           backGrRect , listX, p1 , xend , p4


 ! centerRect      ----------
 GR.SET.STROKE     2
 no              = 5 * 5
 GR.COLOR          cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
 GR.PAINT.GET      rectMoving
 GR.SET.STROKE     2
 no              = 4 * 5
 GR.COLOR          cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
 GR.PAINT.GET      rectSnapped
 GR.RECT           selRect  , listX ,centh-space/2,xend,centh+space/2

 !ListText     ----------
 GR.TEXT.ALIGN     txtAlign
 GR.TEXT.SIZE      txtSz
 GR.TEXT.BOLD      txtBold
 GR.TEXT.SKEW      txtSkew
 GR.TEXT.TYPEFACE  txtTypeface
 no              = 6 * 5
 GR.COLOR          cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
 GR.PAINT.GET      selectedPaint
 no              = 7 * 5
 GR.COLOR          cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
 GR.PAINT.GET      notSelectedPaint

 LIST.CREATE       n,idxList
 lineCtr         = 0
 IF txtAlign     = 1 THEN txtXpos = listx + 5
 IF txtAlign     = 2 THEN txtXpos = listx + ssx/2
 IF txtAlign     = 3 THEN txtXpos = listx + ssx - 5


 FOR i           = 1 TO lenlistBas

  IF flagLineNo    THEN noStr$ =REPLACE$(FORMAT$("####",i)," ","")+"  "
  tmp$           = noStr$ + dd$[i]

  !linebreakStuff part1 --
  GR.TEXT.WIDTH    txtwidth, tmp$
  IF txtwidth+5   > ssx
   GR.TEXT.WIDTH    oxTmp ,  noStr$
   maxLentmp      = FLOOR((ssx-oxTmp-5)/(txtwidth)*LEN(tmp$)*0.90)
   tmp2$          = RIGHT$(tmp$, LEN(tmp$)-maxLentmp)
   tmp$           = left$ (tmp$, maxLentmp)
   GR.TEXT.WIDTH    txtwidth, tmp2$
   IF txtwidth+5  > ssx
    maxLentmp     = FLOOR((ssx-oxTmp-5)/(txtwidth)*LEN(tmp2$)*0.90)
    tmp2$         = left$ (tmp2$, maxLentmp)
   ENDIF
  ENDIF

  lineCtr         += 1
  GR.TEXT.DRAW      grlist[lineCtr] , txtXpos        , 0 , tmp$
  GR.HIDE           grlist[lineCtr]
  LIST.ADD          idxList,i

  !linebreakStuff part2 --
  IF tmp2$       <> ""
   lineCtr       += 1
   tmpx          =  txtXpos
   IF txtAlign   =  1 THEN tmpx  = txtXpos + oxTmp+5
   GR.TEXT.DRAW     grlist[lineCtr] , tmpx , 0,tmp2$
   GR.HIDE          grlist[lineCtr]
   LIST.ADD         idxList,i
   tmp2$=""
  ENDIF

 NEXT

 LIST.TOARRAY      idxList , idxList[]
 lenlist         = linectr
 ssy             = space * (lenlist-1)
 


 !light-gray coverRects -----------
 no              = 1 * 5
 GR.COLOR          cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
 GR.RECT           covRect1 , listX , p2, xend , centh-space/2
 GR.RECT           covRect2 , listX , centh+space/2, xend , p3


 !Top/bottomRect & Text -----------
 no              = 2 * 5
 GR.COLOR          cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
 GR.RECT           topRect , listX , p1 , xend , p2
 GR.RECT           botRect , listX , p3 , xend , p4

 no              = 3 * 5
 GR.COLOR          cs[no+1], cs[no+2] , cs[no+3] , cs[no+4] , cs[no+5]
 szTmp           = topBotRectH *0.8
 GR.TEXT.SIZE      szTmp
 GR.TEXT.ALIGN     2
 GR.TEXT.DRAW      topTxt  , listx + ssx /2 ,listTop+szTmp/2 , "Back"
 GR.TEXT.DRAW      botTxt  , listx + ssx /2 ,listBot+szTmp/2 , "Back"

 RETURN
 !------------------------------


 !RETURN
FN.END
!------------------------------



RETURN
!--------------------------------------------







!--------------------------------------------
openScreen:

refW       = 780
refH       = 1280
IF !flagOri THEN SWAP refW, refH
centW      = refW/2
centH      = refH/2
GR.OPEN      255,20,0,0,1,flagOri
GR.SCREEN    curW, curH
scaW       = curW / refW
scaH       = curH / refH
GR.SCALE     scaW , scaH

RETURN
!--------------------------------------------



!--------------------------------------------
someBackground:

sz = tileSize
GR.set. stroke 2
szObj = sz*0.6/2
FOR i=0 TO refh+sz STEP sz
 FOR k=0 TO refw+sz STEP sz
  ctr+=1
  GR.COLOR 155, 120+MOD(ctr,135) , 0 , 0 , 0
  IF rnd ()> 0.5 THEN
   GR.RECT nn, k-szObj , i-szObj , k+szObj , i+szObj
  ELSE
   GR.CIRCLE nn, k, i, szObj
  ENDIF
 NEXT
NEXT
PRINT ctr
pop$ = "count grObj main loop: "+STR$(ctr)
POPUP pop$, 0, 0, 0

RETURN
!--------------------------------------------




