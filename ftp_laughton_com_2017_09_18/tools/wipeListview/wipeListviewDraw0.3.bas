flagOri     = 0
GOSUB openScreen
GOSUB userfunctions




DO

 UNDIM dd$[]
 FILE.DIR "../data/",dd$[]
 aw = listview1 (dd$[], refw, refh  )


 UNDIM dd $[]
 LIST.TOARRAY aw, dd$[]
 aw = listview1 (dd$[], refw, refh )


 DO
  toc= CLOCK() - tic
 UNTIL toc >= 30
 tic = CLOCK()

UNTIL 0





!--------------------------------------------
openScreen:

refW       = 780
refH       = 1280
IF !flagOri THEN SWAP refW, refH
centW      = refW/2
centH      = refH/2
GR.OPEN      255,30,0,0,10,flagOri
GR.SCREEN    curW, curH
scaW       = curW / refW
scaH       = curH / refH
GR.SCALE     scaW , scaH

RETURN
!--------------------------------------------




!--------------------------------------------
userfunctions:


!------------------------------
FN.DEF listview1 (dd$[], refw, refh  )

 centW          = refW/2
 centH          = refH/2
 GR.SCREEN        curW, curH
 scaW           = curW / refW
 scaH           = curH / refH

 lenVBuff       = 5
 DIM              VBuff[ lenVBuff ]

 ARRAY.LOAD       vib [], 0, 30

 ARRAY.LENGTH     lenlist, dd $[]
 DIM              result [lenlist]
 DIM              grlist [lenlist]
 DIM              grline [lenlist]

 txtSz          = 20
 spacingOffs    = txtSz/0.75
 ssx            = refw * 0.75
 pixSpaceing    = txtSz + spacingOffs
 listx          = centw - ssx/2
 listy          = centh + txtSz/2
 vlisty         = 0
 movingFriction = 0.95
 bounceFriction = 0.9
 shotAmplify    = 1.5
 vlistmin       = 25


 listTop        = refh * 0.05
 listBot        = refh * 0.96
 listVisTop     = listTop + txtSz + spacingOffs
 listVisBot     = listBot - txtSz - spacingOffs
 movRange       = listTop - listBot


 GOSUB drawGrobjs


 !mainFNLoop ------------------------------------------
 DO

  IF ABS(vListY) <= vlistmin  THEN GOSUB vlistminHandles

  GR.TOUCH touch, xx, yy
  IF touch THEN GOSUB touchHandles

  if flagBreak then D_U.BREAK

  vListY    = vListY * movingFriction
  listY     = listY  + vListY

  !  IF listY>centh | listY+ssy<centh THEN GOSUB borderHandles

  GOSUB moveList

  GR.RENDER

  DO
   toc = CLOCK() - tic
  UNTIL toc >= 50
  tic = CLOCK()

 UNTIL 0
 !------------------------------------------------------

 DO
  GR.TOUCH tt, xx, yy
 UNTIL! tt
 LIST.CREATE s, selection
 FOR i=1 TO lenlist
  IF result[i]>0 THEN LIST.ADD selection, dd$[i]
 NEXT
 LIST.SIZE selection, sz
 IF !sz THEN LIST.ADD selection, "(noItemsSelected)"
 PRINT "=========", sz
 GR.CLS
 GR.RENDER

 FN.RTN selection
!=======================================================

 !------------------------------
  touchHandles:

  GR.BOUNDED.TOUCH trect1, ~
  listX*scaw          , refh* 0.0 *scah, ~
  (listX+ssx)*scaw    , refh* 0.1 *scah
  GR.BOUNDED.TOUCH trect2, ~
  listX*scaw          , refh* 0.9 *scah, ~
  (listX+ssx)*scaw    , refh* 1.0 *scah
  IF trect1| trect2 THEN flagBreak=1 

  GR.BOUNDED.TOUCH trect, ~
  listX*scaw          , (centh - pixSpaceing/2 )*scah, ~
  (listX+ssx)*scaw    , (centh + pixSpaceing/2 )*scah
  IF trect THEN  GOSUB selecthandles

  p1x            =  centw - ssx/2
  GR.BOUNDED.TOUCH  touchB, p1x, 0, p1x+ssx, 1200
  IF touchB THEN GOSUB pickHandles

 RETURN
 !------------------------------


 !------------------------------
 SELECThandles:

 DO
  GR.TOUCH tt, nn, mm
 UNTIL ! tt
 VIBRATE vib [],-1
 pos             = ceil ( ( centh -listY) / pixSpaceing ) + 1
 result[pos]     = !result [pos]
 IF result [pos]
  GR.MODIFY         grlist[pos] ,"paint", selectedPaint
 ELSE
  GR.MODIFY         grlist[pos] ,"paint", notSelectedPaint
 ENDIF

 RETURN
 !------------------------------


 !------------------------------
 vlistminHandles:
 vListYold = vListY
 vListY = 0
 IF !vListY & vListYold
  VIBRATE          vib [],-1
  f1            =  ceil ( ( centh -listY) / pixSpaceing )
  listY         =  centh - f1 * pixSpaceing + txtsz/2
  GR.MODIFY        selRect,"paint", rectSnapped
 ENDIF
 RETURN
 !------------------------------


 !------------------------------
 borderHandles:
 vListY         = -vListY * bounceFriction
 IF listY       > centh THEN listY = centh      - pixSpaceing/2
 IF listY+ssy   < centh THEN listY = centh -ssy + pixSpaceing/2
 IF ABS(vListY) > vibBorderVal THEN VIBRATE vib[],-1

 RETURN
 !------------------------------


 !------------------------------
 pickHandles:

  GR.MODIFY        selRect ,"paint", rectMoving
  ptrTmp =0
  GR.TOUCH         touch2 , xs ,ys
  ys            =  ys / scah
  ctr           =  0

  DO
   ctr          += 1
   yold         =  y
   GR.TOUCH        touch2 , x ,y
   y            =  y / scah
   ptrTmp       += 1
   IF ptrTmp    >  lenVBuff THEN ptrTmp =1
   VBuff[ptrTmp] =  (yold-y)
   dlisty        =  y-ys
 !  if listy+dlisty > centh then dlisty= listy-centh - txtSz/2
   GOSUB moveList
   GR.RENDER
  UNTIL !touch2

  listY         =  listY + dlistY
  dlistY        =  0
  ARRAY.AVERAGE    vlist , VBuff[]
  IF ctr        <  lenVBuff +1 THEN vlist=0
  vListY        =  vlist * -shotAmplify
  IF ABS(vListY)<= vlistmin THEN vListY=vlistmin

 RETURN
 !------------------------------


 !------------------------------
 moveList:

 idxMoveStart    = floor ( (listTop-listy - dlistY ) / pixSpaceing ) -3
 idxMoveEnd      = ceil  ( (listBot-listy - dlistY ) / pixSpaceing ) +3
 IF idxMoveStart < 1       THEN idxMoveStart = 1
 IF idxMoveEnd   > lenlist THEN idxMoveEnd   = lenlist

 FOR i= idxMoveStart TO idxMoveEnd
  yact           = listy + (i-1)* pixSpaceing + dlistY
  flagVisible    = yact>listTop & yact<listBot
  IF flagVisible THEN
   GR.SHOW       grlist[i]
   !   GR.SHOW       grline[i]
  ELSE
   GR.HIDE       grlist[i]
   !   GR.HIDE       grline[i]
  ENDIF
  GR.MODIFY        grlist[i], "y" , yact
  !  GR.MODIFY        grline[i], "y1", yact+ lOffs
  !  GR.MODIFY        grline[i], "y2", yact+ lOffs
 NEXT

 RETURN
 !------------------------------



 !------------------------------
 drawGrobjs:

 !Background -----------
 GR.COLOR       255,10,60,60,1
 GR.RECT        backGrRect, listX, 0, listX+ssx , refh


 !Text properties-----------
 GR.SET.STROKE    1
 GR.TEXT.SIZE     txtSz
 GR.TEXT.BOLD     0
 GR.TEXT.SKEW     -0.1
 GR.TEXT.TYPEFACE 3

 GR.COLOR         255, 0,255,0,1
 GR.PAINT.GET     selectedPaint

 GR.COLOR         255, 255,255,255,1
 GR.PAINT.GET     notSelectedPaint


 !List -----------
 lOffs          = pixSpaceing/2
 FOR i=1 TO lenlist
  tmp$           = replace$(FORMAT$("####",i)," ","")+ "  " + dd$[i]
  yact           = listy + (i-1)* pixSpaceing
  IF yact        > listBot THEN yact=listBot
  GR.TEXT.DRAW     grlist[i], listx +txtsz/2 ,  yact+txtsz/2 , tmp$
  GR.HIDE          grlist[i]
  !GR.LINE          grline[i],listx +txtsz/2 , yact+lOffs, listx+ssx, yact+lOffs
  !GR.HIDE          grline[i]
 NEXT


 !selectRect (center of screen) --------
 GR.SET.STROKE  3
 GR.COLOR       240, 240,240,240, 0
 GR.RECT        selRect  , ~
 listX      , centh - pixSpaceing/2 , ~
 listX + ssx, centh + pixSpaceing/2
 GR.PAINT.GET   rectSnapped

 GR.SET.STROKE  2
 GR.COLOR       150, 2.150,150,150, 0
 GR.PAINT.GET   rectMoving



 !light-gray coverRects -----------
 GR.COLOR       110 , 100,100,100, 1
 GR.RECT        covRect1  , listX   , 0                     , ~
 listX + ssx, centh - pixSpaceing/2
 GR.RECT        covRect2  , listX   , centh + pixSpaceing/2 , ~
 listX + ssx, refh


 !Top/bottomRect & Text -----------
 GR.COLOR       255,10,60,60,1
 GR.RECT        topRect , ~
 listX        , refh* 0.0 ,~
 listX+ssx    , refh* 0.1
 GR.RECT        botRect , ~
 listX        , refh* 0.9 ,~
 listX+ssx    , refh* 1.0
 GR.COLOR       255,50,120,120,1
 GR.TEXT.ALIGN 2
 GR.TEXT.DRAW  txt1, listX+ssx/2  , refh* 0.06 , "Back"
 GR.TEXT.DRAW  txt2, listX+ssx/2  , refh* 0.96 , "Back"
 GR.TEXT.ALIGN 1

 RETURN
 !------------------------------

FN.END
!------------------------------



RETURN
!--------------------------------------------








