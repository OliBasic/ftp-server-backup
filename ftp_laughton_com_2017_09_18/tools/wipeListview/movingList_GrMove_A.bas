!open graphics -----------------------
flagOri                = -1
refW                   = 790
refH                   = 1255
IF !flagOri              THEN SWAP refW, refH
GR.OPEN                  255, 0 , 0 , 60 ,1,flagOri
GR.SCREEN                curW, curH
scaW                   = curW / refW
scaH                   = curH / refH
GR.SCALE                 scaW , scaH


!canvas size/position/color --------------
cvx                    = refw * 1
cvy                    = refh * 0.85
pcx                    = 0
pcy                    = 100


!data to display ---------------------------------------
dat$                  = "../source/Sample_Programs/f01_commands.bas"
dat1$                 = "../source/gw.bas"
FILE.EXISTS              fe, dat1$
IF                       fe THEN dat$ = dat1$
GRABFILE                 li$, dat$
SPLIT                    li$[], li$, CHR$(10)
!file.dir                  "", li$[]
!ARRAY.SORT               li$[]
ARRAY.LENGTH             nlines, li$[]

!propertys -------------
space                  = 20
cvy                    = INT(cvy/space)*space
txtsz                  = space*0.8
txtTyface              = 2
txtBold                = 1
moveAmpli              = 8
txtPosx                = 5
!text-colors ----------
ARRAY.LOAD               ctxt   [], ~
255 , 255 , 000, ~
255 , 255 , 255


!set text params / get txtBounds --------
GR.TEXT.SIZE             txtsz
GR.TEXT.TYPEFACE         txtTyface
GR.TEXT.BOLD             txtBold
GR.GET.TEXTBOUNDS        "W", lef , t, rig, b


!'constants'---------------
k1                     = INT(cvy/space+1)
k2                     = -((nlines-INT(cvy/space)-1 )*space)
k3                     = pcy-txtoffs %- space/2
nrows                  = INT(cvy/space)+1
yref                   = pcy+i*space-txtOffs
txtOffs                = space/2 - ABS(t)/2


!format & draw text ---------------
GR.COLOR                255, ctxt[1], ctxt[2] , ctxt[3] , 1
GR.TEXT.DRAW            nn, pcx+txtPosx, yref , li$[1]

FOR i                 = 2 TO nlines
 tmp1$                = LOWER$(LEFT$(li$[i-1],1))
 tmp2$                = LOWER$(LEFT$(li$[i  ],1))
 IF                     tmp1$ <> tmp2$  THEN ctrcol++
 cc                   = MOD(ctrcol,2)*3
 GR.COLOR               255, ctxt[cc+1], ctxt[cc+2] , ctxt[cc+3] , 1
 GR.TEXT.DRAW           nn, pcx+txtPosx, pcy+i*space-txtOffs, li$[i]
 !gr.line                nn, pcx, pcy+i*space, cvx, pcy+i*space
 IF                     i = nrows THEN gr.render
NEXT


! prepare display list / group --------------
LIST.CREATE             n, dlList
GR.GETDL                dlcomplete[]
LIST.ADD.ARRAY          dlList , dlcomplete[]
GR.group                gr1, dlList

GR.COLOR                 255, 255, 125, 000 , 1
GR.TEXT.SIZE             24
GR.TEXT.BOLD             1
GR.TEXT.TYPEFACE         1
GR.TEXT.DRAW             info1,20,  70,  "xx"
GR.TEXT.DRAW             info2,20,  refh-30,"xx"

DIM                     dltmp [nrows+2]
dltmp [nrows+1]       = info1
dltmp [nrows+2]       = info2
ARRAY.COPY              dlcomplete[ 1, nrows] , dltmp []
GR.NEWDL                dltmp[]



! countdown main ---------------------------
flaginfo               = 1
dtdes                  = 50
toc                    = 30
tocfilt                = toc
tic                    = CLOCK()

!main---------------------------------------
DO

 GR.TOUCH                tou, toux, touy
 IF                      tou&!touOld THEN gosub touched
 IF                      tou&touOld  THEN gosub holded
 IF                     !tou&touOld  THEN gosub released

 IF flaginfo             THEN GOSUB info

 IF !BACKGROUND() THEN   GR.RENDER

 tocfilt               = tocfilt+(toc-tocfilt)*0.05
 toc                   = CLOCK()-tic
 PAUSE                   MAX(dtdes-toc, 1)
 tic                   = CLOCK()

UNTIL0
!------------------------------------------


!-------------------------------
touched:
x0                     = toux
y0                     = touy
touOld                 = tou
RETURN
!-------------------------------
holded:

LET dyli               = INT((touy-y0) *moveAmpli)
LET yliTmp             = MAX(MIN(yli+dyli, 0), k2)
LET sIdx               = INT(-yliTmp /space) +1

GR.GET.POSITION          1, curPosx, curPosy
GR.move                  gr1, 0, yref-curPosy + yliTmp
ARRAY.COPY               dlcomplete[ sIdx, nrows] , dltmp[]
GR.NEWDL                 dltmp[]

RETURN
!-------------------------------
released:
yli                    = round (yliTmp)
dyli                   = 0
touOld                 = tou
RETURN
!-------------------------------


!-------------------------------
info:

GR.MODIFY        info1, "text", ~
INT$( tocfilt )                + " [ms]  /  " + ~
STR$(ROUND(1000/ tocfilt , 1)) + " [fps]  "+" / "+version $()

GR.MODIFY        info2, "text", ~
"dyli: " + INT$(dyli) + ~
" / yli: " + INT$(yli)  + ~
" / sum: " + INT$( yliTmp ) + ~
" / startIdx: " + INT$(sIdx)

RETURN
!-------------------------------

[/code]
