flagOri                = 0
refW                   = 790
refH                   = 1255
IF !flagOri              THEN SWAP refW, refH
GR.OPEN                  255, 0 , 0 , 0 ,1,flagOri
GR.SCREEN                curW, curH
scaW                   = curW / refW
scaH                   = curH / refH
GR.SCALE                 scaW , scaH
!PAUSE                    500

cvx                    = refw * 0.95
cvy                    = refh * 0.75
pcx                    = (refw-cvx)/2
pcy                    = (refh-cvy)/2
ARRAY.LOAD               cvC[], 000,100,120


GR.BITMAP.CREATE         canvas, cvx, cvy
GR.BITMAP.DRAW           cvs, canvas, pcx , pcy

GR.COLOR                 255, 255, 255, 255 , 1
GR.TEXT.SIZE             22
GR.TEXT.DRAW             info1,20,  75,  "xx"
GR.TEXT.DRAW             info2,20,  refh-50,"xx"

GR.BITMAP.DRAWINTO.START canvas
GR.COLOR                 255, cvc [1], cvc [2], cvc [3], 1
GR.RECT                  nn,0, 0, cvx, cvy


!data to display ---------------------------------------
pa$                    = "../source/Sample_Programs/"
dat$                   = "f09_sql.bas"
GRABFILE                 li$, pa$ +dat$
SPLIT                    li$[], li$, CHR$(10)
ARRAY.LENGTH             nlines, li$[]

!props -------------
space                  = 40
txtsz                  = space*0.55
txtTyface              = 2
flagLiNo               = 1
moveAmpli              = 3
ARRAY.LOAD               txc [], 255, 255, 255

!initialize list --------
GR.TEXT.SIZE             txtsz
GR.TEXT.TYPEFACE         txtTyface
GR.GET.TEXTBOUNDS        "W", lef , t, rig, b
txtOffs                = space/2 - ABS(t)/2
GR.COLOR                 255, txc[1], txc[2], txc[3] , 1
FOR i                  = 1 TO nlines
 IF  flagLiNo            THEN  li$[i]= REPLACE$(FORMAT$("%%%%", i)," ","") +" " + li$[i]
 GR.TEXT.DRAW            nn, 5          , i*space- txtOffs , li$[i]
 GR. line                nn, 0          , i*space       , 1500, i*space
NEXT


!main-------------------------------
flaginfo               = 1
dtdes                  = 50
toc                    = 30
tocfilt                = toc
tic                    = CLOCK()

DO

 touOld                = tou
 GR.TOUCH                tou, toux, touy
 IF                      tou&!touOld THEN gosub touched
 IF                      tou&touOld  THEN gosub holded
 IF                     !tou&touOld  THEN gosub released

 IF flaginfo             THEN gosub info

 GR.RENDER

 tocfilt               = tocfilt+(toc-tocfilt)*0.05
 toc                   = CLOCK()-tic
 PAUSE                   MAX(dtdes-toc, 1)
 tic                   = CLOCK()

UNTIL0
!-------------------------------


!-------------------------------
touched:
x0                     = toux
y0                     = touy
RETURN
!-------------------------------
holded:
LET dyli               = ROUND((touy-y0) *moveAmpli)
LET yliTmp             = MAX(MIN(yli+dyli, 0), -nlines*space)
LET sIdx               = INT(-yliTmp /space) +1
LET eIdx               = MIN(INT(cvy/space) +sIdx +1, nlines)

GR.COLOR                255, cvc [1], cvc [2], cvc [3] , 1
GR.RECT                 nn,0, 0, cvx, cvy
GR.COLOR                255, txc [1], txc [2], txc [3] , 1

FOR i                 = sIdx TO eIdx
 LET ytmp             = i*space + yliTmp
 GR.TEXT.DRAW           nn, 5 , ytmp -txtOffs , li$[i]
 GR.LINE                nn, 0 , ytmp          , cvx , ytmp
NEXT
RETURN
!-------------------------------
released:
yli                    = yliTmp 
dyli                   = 0
RETURN
!-------------------------------


!-------------------------------
info:

GR.MODIFY        info1, "text", ~
INT$(tocfilt)                + " [ms]  /  " + ~
INT$(ROUND(1000/tocfilt, 0)) + " [fps]  "

GR.MODIFY        info2, "text", ~
"dyli: " + INT$(dyli) + ~
" / yli: " + INT$(yli)  + ~
" / sum: " + INT$( yliTmp ) + ~
" / startIdx: " + INT$(sIdx)

RETURN
!-------------------------------
