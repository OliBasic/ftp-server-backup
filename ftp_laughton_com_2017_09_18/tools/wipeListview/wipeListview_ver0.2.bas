flagOri     = 1
GOSUB openScreen
GOSUB userfunctions


!max size for list display in  Megapixel
maxBmpSize = 7.0    

DO


 UNDIM dd $[]
 FILE.DIR "../data/",dd$[]
 aw = listview1 (dd$[], refw, refh, maxBmpSize )

 UNDIM dd $[]
 LIST.TOARRAY aw, dd$[]
 aw = listview1 (dd$[], refw, refh, maxBmpSize )



UNTIL 0







!-----------------------------
userfunctions:

FN.DEF listview1 (dd$[], refw, refh , maxBmpSize )

 centW      = refW/2
 centH      = refH/2
 GR.SCREEN    curW, curH
 scaW       = curW / refW
 scaH       = curH / refH


 ARRAY.LENGTH     listLen , dd$[]
 txtSz          = 22
 spacingOffs    = 20
 ssx            = refw * 0.75
 movingFriction = 0.92
 bounceFriction = 0.9
 shotAmplify    = 2
 vlistmin       = 15

 pixSpaceing = txtSz + spacingOffs 
 listX       = centw -ssx/2
 listY       = centh   - pixSpaceing/2
 offsy       = txtSz/2 - pixSpaceing/2
 maxListLen  = floor ( maxBmpSize*1000000 / ( ssx* pixSpaceing ) )
 if listLen  > maxListLen then listLen = maxListLen
 ssy         = listLen * pixSpaceing


 ARRAY.LOAD vib[],0,20
 vibBorderVal  = 8
 lenBuff       = 5
 DIM buff [lenBuff]

 GOSUB list2bmp


 !mainFNLoop ------------------------------------
 DO

  GR.BOUNDED.TOUCH trect1, ~
  listX*scaw          , refh* 0.0 *scah, ~
  (listX+ssx)*scaw    , refh* 0.1 *scah
  GR.BOUNDED.TOUCH trect2, ~
  listX*scaw          , refh* 0.9 *scah, ~
  (listX+ssx)*scaw    , refh* 1.0 *scah
  IF trect1| trect2 THEN D_U.BREAK

  GR.BOUNDED.TOUCH trect, ~
  listX*scaw          , (centh - pixSpaceing/2 )*scah, ~
  (listX+ssx)*scaw    , (centh + pixSpaceing/2 )*scah
  IF trect THEN  GOSUB selecthandles

  GR.TOUCH touch, xx, yy
  IF touch THEN GOSUB pickHandles

  IF ABS(vListY) <= vlistmin  THEN GOSUB vlistminHandles

  vListY   = vListY * movingFriction
  listY     = listY   + vListY

  IF listY>centh | listY+ssy<centh THEN GOSUB borderHandles

  GR.MODIFY  p, "y", listY
  GR.RENDER

  DO
   toc = CLOCK() - tic
  UNTIL toc>=30
  tic = CLOCK()
 UNTIL 0
 !--------------------------------------------

 DO
  GR.TOUCH tt, xx, yy
 UNTIL! tt
 LIST.CREATE s, selection
 FOR i=1 TO listLen
  IF LEFT$(dd$[i],1)="&" THEN LIST.ADD selection, MID$(dd$[i], 2, 10000)
 NEXT
 LIST.SIZE selection, sz
 IF !sz THEN LIST.ADD selection, "(noItemsSelected)"

 GR.HIDE backGrRect
 GR.HIDE selRect
 GR.HIDE topRect
 GR.HIDE botRect
 GR.HIDE covRect1
 GR.HIDE covRect2
 GR.HIDE txt1
 GR.HIDE txt2
 GR.HIDE p
 GR.BITMAP.DELETE  bmp1
 GR.RENDER
 FN.RTN selection

 SELECThandles:
 VIBRATE vib [],-1
 pos = ROUND( (centh-listY) / pixSpaceing )
 y=pos* pixSpaceing

 GR.BITMAP.DRAWINTO.START bmp1
 tmp $=left $( dd$[pos], 1)
 IF tmp$ = "#" THEN
  GR.COLOR      255, 0,180,0,1
  dd$[pos] = "&" + MID$(dd$[pos], 2, 10000)
  GR.TEXT.DRAW nn,  5, y + offsy , replace $(FORMAT$("###",pos)," ","") +"  "+ MID$(dd$[pos], 2, 10000)
 ELSE
  GR.COLOR      255, 255,255,255,1
  dd$[pos] = "#" + MID$(dd$[pos], 2, 10000)
  GR.TEXT.DRAW nn,  5, y + offsy , replace $(FORMAT$("###",pos)," ","") +"  "+ MID$(dd$[pos], 2, 10000)
 ENDIF
 GR.BITMAP.DRAWINTO.END
 RETURN

 vlistminHandles:
 vListYold = vListY
 vListY = 0
 IF !vListY & vListYold
  f1            = ROUND( (centh-listY) / pixSpaceing )
  listY         = centh - f1 * pixSpaceing + pixSpaceing/2
  GR.MODIFY       rect,"paint", rectSnapped
 ENDIF
 RETURN

 borderHandles:
 vListY         = -vListY * bounceFriction
 IF listY       > centh THEN listY = centh      - pixSpaceing/2
 IF listY+ssy   < centh THEN listY = centh -ssy + pixSpaceing/2
 IF ABS(vListY) > vibBorderVal THEN VIBRATE vib[],-1
 RETURN

 pickHandles:
 p1x            =  centw - ssx/2
 GR.BOUNDED.TOUCH  touchB, p1x, 0, p1x+ssx, 1200
 IF touchB THEN
  GR.MODIFY        selRect ,"paint", rectMoving
  ptrTmp =0
  GR.TOUCH         touch2 , xs ,ys
  ys            =  ys / scah
  ctr           =  0
  DO
   ctr          += 1
   yold         =  y
   GR.TOUCH        touch2 , x ,y
   y            =  y / scah
   ptrTmp       += 1
   IF ptrTmp    >  lenBuff THEN ptrTmp =1
   buff[ptrTmp] =  (yold-y)
   GR.MODIFY        p, "y",   listY + y-ys
   GR.RENDER
  UNTIL !touch2
  GR.MODIFY        selRect ,"paint", rectSnapped
  ARRAY.AVERAGE    vlist , buff[]
  IF ctr        <  lenBuff +1 THEN vlist=0
  vListY        =  vlist * -shotAmplify
  IF ABS(vListY)<= vlistmin THEN vListY=vlistmin
  listY         =  listY + y-ys - pixSpaceing/2
 ENDIF
 RETURN


 list2bmp:

 GR.BITMAP.CREATE bmp1, ssx, ssy
 GR.BITMAP.DRAWINTO.START bmp1
 GR.SET.STROKE 4
 GR.COLOR      255,10,60,60,1
 GR.RECT       nn, 0, 0, ssx, ssy

 GR.COLOR      255, 255,255,255,1
 GR.SET.STROKE 1
 GR.TEXT.SIZE  txtSz
 GR.TEXT.BOLD 2
 GR.TEXT.SKEW -0.1
 GR.TEXT.TYPEFACE 2
 FOR i=1 TO listLen
  GR.COLOR       180, 80, 80, 80,1
  y            = i* pixSpaceing
  GR.LINE        nn, 0, y , ssx, y
  GR.COLOR       255, 255,255,255,1
  GR.TEXT.DRAW   nn,  5, y + offsy , replace $(FORMAT$("###",i)," ","") +"  "+ dd$[i]
  dd$[i]       = "#" + dd$[i]
 NEXT
 GR.BITMAP.DRAWINTO.END

 GR.COLOR       255,10,60,60,1
 GR.RECT        backGrRect, listX, 0, listX+ssx , refh
 GR.BITMAP.DRAW p, bmp1, listX , listY

 GR.SET.STROKE  3
 GR.COLOR       240, 240,240,240, 0
 GR.RECT        selRect  , ~
 listX      , centh - pixSpaceing/2 , ~
 listX + ssx, centh + pixSpaceing/2
 GR.PAINT.GET   rectSnapped
 GR.SET.STROKE  2
 GR.COLOR       150, 2.150,150,150, 0
 GR.PAINT.GET   rectMoving

 GR.COLOR       110 , 100,100,100, 1
 GR.RECT        covRect1  , listX   , 0                     , ~
 listX + ssx, centh - pixSpaceing/2
 GR.RECT        covRect2  , listX   , centh + pixSpaceing/2 , ~
 listX + ssx, refh


 GR.COLOR       255,10,60,60,1
 GR.RECT        topRect , ~
 listX        , refh* 0.0 ,~
 listX+ssx    , refh* 0.1
 GR.RECT        botRect , ~
 listX        , refh* 0.9 ,~
 listX+ssx    , refh* 1.0
 GR.COLOR       255,30,90,90,1
 GR.TEXT.ALIGN 2
 GR.TEXT.DRAW  txt1, listX+ssx/2  , refh* 0.06 , "Back"
 GR.TEXT.DRAW  txt2, listX+ssx/2  , refh* 0.96 , "Back"
 GR.TEXT.ALIGN 1
 RETURN

FN.END

RETURN
!--------------------------------------------







!--------------------------------------------
openScreen:

refW       = 780
refH       = 1280
IF !flagOri THEN SWAP refW, refH
centW      = refW/2
centH      = refH/2
GR.OPEN      255,30,0,0,10,flagOri
GR.SCREEN    curW, curH
scaW       = curW / refW
scaH       = curH / refH
GR.SCALE     scaW , scaH

RETURN
!--------------------------------------------

