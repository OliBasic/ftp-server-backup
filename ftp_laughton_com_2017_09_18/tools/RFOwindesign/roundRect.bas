rem rounded rectangles
rem -Hotwrench-
fn.def rrect(x1,y1,x2,y2,r)  %define function 
 gr.line lnel,x1,y1+r , x1,y2-r 
 gr.line lnet, x1+r,y1 , x2-r,y1 
 gr.line lner, x2,y1+r , x2,y2-r
 gr.line lneb, x2-r,y2 , x1+r,y2 

 gr.arc arctl, x1,y1,x1+r*2,y1+r*2,270,-90,0  %1= pie shape
 gr.arc arcbr, x2-r*2,y2-r*2,x2,y2,0,90,0
 gr.arc ar0cbl, x1,y2-r*2,x1+r*2,y2,-270,90,0 
 gr.arc arctr, x2-r*2,y1,x2,y1+r*2,0,-90,0  %change 90's to 360 for fill
!gr.rect rct,x1+5,y1+5,x2-5,y2-5  % for fill
 fn.rtn(1) %returns true
fn.end 

rem program start

gr.open 255,0,0,0
gr.color 255,255,255,255,0 %1 for fill
gr.set.stroke 3

x1=50  %x1 y1 = top left postion of rectangle
y1=50
x2=250  %if x2 and y2 are equal it's square
y2=350
r=y2/10  %change 10 for radius
call rrect(x1,y1,x2,y2,r)
gr.render
 
do
until 0
