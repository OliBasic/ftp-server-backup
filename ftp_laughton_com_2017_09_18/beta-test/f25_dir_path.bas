!!

Note: This is variation on
f25_dir.bas. If you do a long
press on a file, the program
will print the path to the file
AND place the path string into
the clipbord.

This program can be used to
explore the file system on an
Android device. 

Any filename with a (d) after
it is a directory. Tap on that
entry to open that directory.

Tap on the top entry, "..", to
move up one directory level.

Press the BACK key to exit.

This program will list all the
way up to the file system root.

The initial directory shown is
"/sdcard/rfo-basic/data/"

!!

! Lists a directory. Gets and
! returns the user's selection

path$ = ""
loop:

! Get the listing from
! the path directory
! and sort it

array.delete d1$[]
file.dir path$, d1$[]
array.length length, d1$[]

! Copy the file list
! adding the top ",,"
! entry

array.delete d2$[]
dim d2$[length+1]
d2$[1] = ".."
for i = 1 to length
 d2$[i + 1] = d1$[i]
next i

! Present the list
! and get the user's
! choice

select s, d2$[], "", long

if s = 0 then END

! if long press, then print
! path, put into clipbord
! and end

if long
 p$ = path$ + d2$[s]
 print p$
 clipboard.put p$
 end
endif

! If top entry, "..", not
! selected then append
! the selected directory 
! name to the path

if s>1
 n = is_in("(d)", d2$[s])
 if n = 0 
  goto loop
  endif
 dname$ = left$(d2$[s],n-1)
 path$=path$+dname$+"/"
 goto loop
endif

! If s = 1 then must back
! one level

! if at start path then
! back up one level

 if path$ = ""
  path$ = "../"
  goto loop
 endif

! Not at start path
! split the path by
! the "/" chars

 array.delete p$[]
 split p$[], path$, "/"
 array.length length, p$[]

! If the last entry is
! ".." then add "../"
! to back up one level

 if p$[length] = ".."
  path$ = path$ + "../"
  goto loop
  endif

! Last entry is not ".."
! so must delete the
! last directory from
! the path

! If only one entry
! then path is back
! to the base path

 if length = 1
  path$ = ""
  goto loop
  endif

! Reconstruct path without
! the last directory

 path$ = ""
 for i = 1 to length - 1
  path$ = path$ + p$[i] + "/"
 next i
 
 goto loop

 





 

 
