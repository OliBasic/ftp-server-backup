f26_array_copy.bas
! This program demonstrates
! the array.copy command

! This function prints a numeric
! Array

fn.def array_print(a[],msg$)
 print msg$
 array.length l,a[]
 for i = 1 to l
  print a[i]
 next i
 fn.rtn 0
fn.end

array.load n1[],1,2,3,4,5
n=array_print (n1[],"Original array")

array.copy n1[],n2[]
n=array_print (n2[], "Straight copy")

array.copy  n1[],n3[2]
n=array_print (n3[], "2 extras at end")

array.copy n1[],n4[-2]
n=array_print (n4[], "2 extras at front")

array.copy n1[2,3], n5[]
n=array_print (n5[], "trim to middle three"

array.copy n1[1,2],n6[-1]
n=array_print (n6[], "trim to first 2 and add 1 at start")

! This function prints a String
! Array

fn.def array_print$(a$[],msg$)
 print msg$
 array.length l,a$[]
 for i = 1 to l
  if a$[i] = "" then print "(empty)" else print a$[i]
 next i
fn.rtn ""
fn.end

array.load str$[],"a","b","c","d","e"
n$=array_print$ (str$[],"Original array")

array.copy str$[],n2$[]
n$=array_print$ (n2$[], "Straight copy")

array.copy  str$[],n3$[2]
n$=array_print$(n3$[], "2 extra at end")

array.copy str$[],n4$[-2]
n$=array_print$(n4$[], "2 extra at front")

array.copy str$[2,3], n5$[]
n$=array_print$(n5$[], "trim to middle three"

array.copy str$[1,2],n6$[-1]
n$=array_print$ (n6$[], "trim to first 2 and add 1 at start")

end
