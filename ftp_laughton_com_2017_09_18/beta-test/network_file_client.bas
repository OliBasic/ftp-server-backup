input "Save Filename", filename$, "test.jpg"
input "Connect-to IP", ip$,"192.168.1.136"
input "Port number", port, 2345
socket.client.connect ip$, port
print "Connected"
maxclock = clock() + 30000
do
 socket.client.read.ready flag
 if clock() > maxclock
   print "Process timed out. Ending."
   end
 endif
until flag
byte.open W, fw, filename$
socket.client.read.file fw
byte.close fw
socket.client.close
print "Done"
