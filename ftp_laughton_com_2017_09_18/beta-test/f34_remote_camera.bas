!!

******  READ!  *******

See f32_tcp-ip_socket.bas
introduction for details
about setting up a server
and a client.

This program must be run
on two different Android
devices.

In this program, it is
a server that takes a picture
under command from a client. It
is a client that retrieves the
image and displays it.

The viewer saves the received
image in 
/sdcard/rfo-basic/data/rimage.jpg
and displays the image

!!

array.load type$[], "Remote Camera", "Remote Viewer"
msg$ = "Select Remote Type"
select type, type$[], msg$
if type = 0
 "Thanks for playing"
 end
elseif type = 2
 goto doViewer:
endif

!***********  Camera Server Server Demo  **************

ServerErrorCount = 0
ServerMaxError = 100
RecoveryState = -1;

input "Enter the port number", port, 1080

socket.myip ip$
print "LAN IP: " + ip$
graburl ip$, "http://automation.whatismyip.com/n09230945.asp"
print "WAN IP: " + ip$

! Create the server on the specified port
socket.server.create port
RecoveryState = 0

newConnection:

! Connect to the next Client
! and print the Client IP
print ""
Print "Waiting for viewer client connect"
socket.server.connect
socket.server.client.ip ip$
print "Connected to ";ip$
RecoverState = 0

! Connected to a Client
! Wait for Client to send a message
! or time out after 10 seconds

maxclock = clock() + 60000
do
 socket.server.read.ready flag
 if clock() > maxclock 
   print "Read time out"
   goto OnError
 endif
until flag

! Message received. Read it.

socket.server.read.line msg$
print "Viewer command = ";msg$

! The message is a string with
! single digit

! Open Graphics
gr.open 255, 0, 0, 0 % Black Background

sw.begin msg$

sw.case "0" % 0 = Quit
 Print "Viewer says to shutdown"
 end
 sw.break
 

sw.case "1" % Automatic mode auto flash
 gr.camera.autoshoot bm_ptr, 0
sw.break

sw.case "2" % Automatic mode without flash
 gr.camera.autoshoot bm_ptr, 2
sw.break

sw.case "3" % Automatic mode with flash
 gr.camera.autoshoot bm_ptr, 1
sw.break

sw.default
 print "Do some more debugging"
 end
 
sw.end

! We do not need the bitmap,
! so delete it.

gr.bitmap.delete bm_ptr

! Close graphics
gr.close

! The autoshoot mode places an
! image file, image.jpg in the 
! /sdcard/rfo-basic/data/
! directory
! 
! We will send that image to 
! the client

byte.open R, fr, "image.jpg"
print "Sending image"
socket.server.write.file fr
print "Image sent"

! Finished this capture.
! Disconnect from Client

socket.server.disconnect
print "Disconnected from viewer"

! Loop to get the next Client

goto newConnection

! Server Error Recovery Code

onError:

! If not in server mode, quit
if type <> 1
 print "Client error. Terminating"
 end
endif

if RecoverState = -1
 Print "Device not connected to LAN or WAN"
 END
endif

! Limit the number of server errors
! to avoid endless looping


if RecoverState = 0
	ServerErrorCount = ServerErrorCount + 1
	Print "Server Error. Recovering " + str$(ServerErrorCount)
	if ServerErrorCount > ServerMaxError
	 print "Exceeded Max Server Error Count. Terminating"
 	end
endif

! Try to disconnect and close
! Server. If one fails, do 
! not try it again until
! and Server is created

if RecoverState = 0
 Print "Attempting to disconnect"
 RecoverState = 1
 socket.server.disconnect
 print "Disconnected"
 goto newConnection
elseif
 Print "Attempting to close Server"
 RecoverState =2 
 socket.server.close
 Print "Closed"
endif

! Try to crate and new server.

Print "Attempting to create new Server"
socket.server.create port
RecoveryState = 0
goto newConnection

! ****************** Client Demo ****************

doViewer:

! Load an array with the 
! Select Options
array.load method$[], "Quit" ~
"Capture with auto flash", "Capture without flash", "Capture with flash"~
"Shut down remote camera"

! Set the Popup Message
msg$ ="Connected. Select A Capture Mode"

input "Enter the connect-to IP", ip$
input "Enter the port number", port, 1080


viewerAgain:

! Connect to the specified IP on the
! specified Port

socket.client.connect ip$, port
print ""
print "Connected to Camera Server"


again:

! Ask the user what to do
select mode, method$[], msg$

! A choice has been made
! Act on that choice
sw.begin mode

sw.case 0 % Back Key
sw.case 1 % Quit
 Print "Done taking pictures"
 end
sw.break

sw.case 2 % Capture with auto flash
 socket.client.write.line "1"
sw.break

sw.case 3 % Automatic mode without flash
 socket.client.write.line "2"
sw.break

sw.case 4 % Automatic mode with flash
 socket.client.write.line "3"
sw.break

sw.case 5 % Shut down camera
 socket.client.write.line "0"
 print "Done taking pictures"
 socket.client.close
end
sw.break

sw.end

! and then wait for Server to respond
! or time out after 30 seconds

Print "Waiting for image"
maxclock = clock() + 30000
do
 socket.client.read.ready flag
 if clock() > maxclock 
   print "Read time out"
   end
 endif
until flag

! Server has sent the image file.
! Read it. Print it.

byte.open W, fw, "rimage.jpg"
socket.client.read.file fw
byte.close fw
Print "Image received"

! The image is now in the file
! "rimage.jpg"

! Close the client

socket.client.close
print "Disconnected from server"

! Open Graphics
gr.open 255, 0, 0, 0 % Black Background

!Reset orientation to Landscape
gr.orientation 0 % Force Landscape

! Load the file into a bitmap

gr.bitmap.load bm_ptr, "rimage.jpg"

! Scale the bitmap to fit the
! screen with the proper
! aspect ratio

gr.bitmap.size bm_ptr, bw,bh
if bh > bw
 gr.orientation 1
 gr.screen sw, sh
 ar = bw/bh
 gr.bitmap.scale scaled_bm, bm_ptr, sw, sh*ar
else
 gr.screen sw, sh
 ar = bh/bw
 gr.bitmap.scale scaled_bm, bm_ptr, sw*ar, sh
endif

! Draw the scaled image bitmap
gr.bitmap.draw obj_ptr, scaled_bm, 0, 0

! Add some text 

gr.text.size sw/25
gr.text.align 1
xleft = sw/12
xtop = sh - 2*(sw/25)
gr.color 255, 255, 255, 255, 1
gr.text.draw P, xleft,  xtop, "Tap screen to take another picture."

! Now render the picture and text
gr.render

! Wait until touch and then untouch

flag = 0
do
 gr.touch flag, xx, yy
 until flag
do
 gr.touch flag, xx, yy
 until !flag

! Delete the old bitmaps
gr.bitmap.delete bm_ptr
gr.bitmap.delete scaled_bm

! Close graphics
gr.close

! Ready for the next capture
goto viewerAgain

