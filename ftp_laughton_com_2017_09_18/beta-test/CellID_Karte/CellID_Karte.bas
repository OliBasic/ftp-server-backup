REM Programm CellID - Karte by Daniel Schlapa 2015

apptitel$ = "CellID - Karte"
appversion$ = "0.70"

INCLUDE GW.bas

PHONE.INFO werte
DEVICE infos
CONSOLE.TITLE apptitel$ + " - " + appversion$

BUNDLE.GET werte, "CID", zelle
BUNDLE.GET werte, "LAC", lac
BUNDLE.GET werte, "MCC/MNC", mccmnc$

mcc$ = MID$(mccmnc$,1,3)
mnc$ = MID$(mccmnc$,4)

TIME y$, m$, d$, h$, n$, s$
uhrzeit$ = h$ + ":" + n$ + ":" + s$
datum$ = d$ +"."+ m$ +"."+ y$

HTML.Orientation 1
page1 = GW_NEW_PAGE()
page2 = GW_NEW_PAGE()
GW_ADD_TITLEBAR(page1,apptitel$)
GW_ADD_TEXT(page1, "Daten vom Smartphone:")
GW_ADD_TEXT(page1, "Datum: " + datum$)
GW_ADD_TEXT(page1, "Uhrzeit: " + uhrzeit$ + " Uhr")
GW_ADD_TEXT(page1, "Handyzelle: " + INT$(zelle))
GW_ADD_TEXT(page1, "LAC: " + INT$(lac))
GW_ADD_TEXT(page1, "MCC/MNC: " + mcc$ + "/" + mnc$)
GW_ADD_SUBMIT(page1, "In einer Karte anzeigen.")
GW_RENDER(page1)

GW_ADD_TITLEBAR(page2,apptitel$)
GW_ADD_TEXT(page2, "Der Standort in der Karte: ")
!!
HERE SHOW THIS MAP: url$= "http://cellid.schlapa.net/cellid.php?lac=" +INT$(lac)+"&cid="+INT$(zelle) 
!!

GW_RENDER(page2)