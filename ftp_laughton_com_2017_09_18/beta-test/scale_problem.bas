! Testing a problem with scale
gr.open 255,0,0,0
gr.screen real_w, real_h
di_w=320
di_h=200
scale_w=real_w/di_w
scale_h=real_h/di_h
gr.scale scale_w, scale_h

gr.color 255,255,255,255,0
gr.rect tmp, 0, 0, 320, 200
gr.line tmp, 0, 0, 320, 200
gr.line tmp, 0, 200, 320, 0
gr.render

Popup "Please press the POWER button in order to put your cellphone in Sleep mode",0,0,1

do
  pause 1000
until Background()

do
  pause 1000
until !Background()

pause 2000
Popup "Back from Sleep mode : the box should not take full screen now...",0,0,1

do
until 0
