! Socket server test program.
! To be used with socketClientTest.bas

! Start this server before the client

! get my ip and print it
! my ip needed for socket client

socket.myip ip$
print ip$

! Create the server on the specified port
! This program  uses port 8888

socket.server.create 8888

do

! Connect to the next Client
! and print the Client IP

socket.server.connect
socket.server.client.ip ip$
print "Connected to ";ip$

! Connected to a Client
! Wait for Client to send a message
! or time out after 10 seconds

maxclock = clock() + 10000
do
 socket.server.read.ready flag
 if clock() > maxclock 
   print "Read time out"
   end
 endif
until flag

! Message recieved. Read it.
! Print it

socket.server.read.line line$
print line$

! Send a message back to client

socket.server.write.line "Server to client message"

! Finished this protocol
! Disconnect from Client

socket.server.disconnect

! Loop to get the next Client

until 0
 
