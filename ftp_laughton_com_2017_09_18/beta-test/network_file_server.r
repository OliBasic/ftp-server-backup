rebol []
selected-file: request-file/only
port-num: request-text/title/default "Port number:" "2345"
print rejoin ["Server started on " (read join dns:// read dns://) ":" port-num]
if error? try [port: first wait open/binary/no-wait join tcp://: port-num] [quit]
file: read/binary selected-file
insert port file
close port
print "Done" halt