Scope of Variables

The files in this folder are designed to show how certain aspects of handling variables should operate properly but fail to execute correctly under the current version of the rfoBASIC! programming language.  These files are intended to help with future development of the language and are not a criticism of the developer or his fine work.

globalVarsTest.bas - From the developer of the rfoBASIC! language, this file was built to show how global variables can be used in the language. Instead, it illustrates how parameters can either be passed by value or by reference, which is a completely different concept from global variables. Passing parameters by value or by reference affects how parameters are treated and returned by a function; whereas defining variables to be global in scope means having access to those variables at any and all locations in the program following the global definition without having to pass them as parameters at all.

globaltest.bas -  Illustrates the proper use of global variables and how the current design of rfoBASIC! fails to execute it correctly. 

arraytest.bas - Shows how and why a simple library of common array manipulation functions do not operate properly.

arraykill.bas - A very simple example of array manipulation failure.
