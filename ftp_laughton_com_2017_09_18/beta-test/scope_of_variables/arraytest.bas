!!
---- rfoBASIC! program ----

Filename: arraytest.bas

Objective:
Test array manipulation

Design:
* Declare a variable to be global in scope,
* Create a local scope level in a function,
* Test global variable accessibility.
---------------------------
!!

! Prints and returns length of array
fn.def ArraySize(location$,arr[])
	array.length arrlen,arr[]
	print "array.length at location "+location$;
	print ":"+format$("##",arrlen)
fn.rtn	arrlen
fn.end

! Simple function to copy contents from source
! array to destination array, no error checking.
! dst[] must be at least the size of src[]
fn.def CopyArray(src[],dst[])
	arrlen=ArraySize("3",src[])
	for i=1 to arrlen
		dst[i]=src[i]
	next
fn.rtn 1
fn.end

! Increase array size by one element
! * Copy array to temporary array
! * Redefine array with larger size
! * Copy temporary array into new array
fn.def IncrArray(arr[])
	arrlen=ArraySize("2",arr[])
	
	dim tmp[arrlen]
	t=CopyArray(arr[],tmp[])
	
	! This function fails because undim
	! deletes the reference to the array
	! instead of the actual array
	undim arr[]
	
	! dim makes a new local array instead
	! of redim-ing the original array
	dim arr[arrlen+1]
	t=CopyArray(tmp[],arr[])
	
	! shows arr[] has one more element
	! but arr[] is now local so changes do
	! not get passed back to the calling
	! function
	arrlen=ArraySize("4",arr[])
fn.rtn 1
fn.end


array.load a[], 1,2,4,8,16,32,64,128,256
t=ArraySize("1",a[])

! Should increase size of a[] by one element.
t=IncrArray(a[])
t=ArraySize("5",a[])  % same size as original

end
