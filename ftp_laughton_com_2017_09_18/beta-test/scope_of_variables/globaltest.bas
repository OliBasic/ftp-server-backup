!!
---- rfoBASIC! program ----

Filename: globaltest.bas

Objective:
Test the scope of global variables

Design:
* Declare a variable to be global in scope,
* Create a local scope level in a function,
* Test global variable accessibility.
---------------------------
!!

! Since no dedicated keyword exists to declare
! a variable as having a global scope, attempt
! to define a global variable as one declared
! at the top-most scope level.
gTst$ = "Global variable test."
print "1:"+gTst$

! Global variables should be accessible within a
! function without passing them as parameters.
fn.def fTst(a)
print "2:"+gTst$
fn.rtn 0
fn.end

r = fTst(1)
print "3:"+gTst$
print "All three values should be the same."

end
 
