flagUseBackgrBmp = 0
flagAccSens      = 0
IF flagAccSens  THEN SENSORS.OPEN 1



GR.OPEN 255, 0, 0, 35, 0, 1
PAUSE 100
GR.ORIENTATION 1               %Portrait-mode

GR.SCREEN actual_w, actual_h
di_width = 780
di_height = 1280

scale_width  = actual_w /di_width
scale_height = actual_h /di_height
GR.SCALE scale_width, scale_height

!screen center
cx = di_width/2
cy = di_height/2



radBall = 40
diaBall = radBall * 2



bask1X   = radBall * 0.99
bask2X   = bask1X +  1.6 * diaBall
bask1Y   = di_height / 3
bask2Y   = bask1Y
bask1Rad = 7
bask2Rad = bask1Rad



lirand =  0
rerand =  di_width  - diaBall
obrand = -di_height
unrand =  di_height - diaBall



ballx   = cx
bally   = cy

ballxri = 0
ballyri = 0

ballAccx    = 0
ballAccy    = 9.81



movingFriction = 0.975
bounceFriction = 0.90
accAmplify     = 0.45
shotAmplify    = 0.55
ballAccy       = ballAccy * accAmplify



ARRAY.LOAD vib[],0,20
vibBorderVal   = 8



GR.BITMAP.LOAD     fuss_ptr, "fussb.png"
GR.BITMAP.SCALE    fuss_ptr2, fuss_ptr, diaBall , diaBall



IF flagUseBackgrBmp
 GR.BITMAP.LOAD     backgrBmp, "brickwall.jpg"
 GR.BITMAP.SCALE    backGrBmpScaled,    backgrBmp, di_width, di_height
ENDIF



lenTimeArray = 30
DIM calcTimeArray [lenTimeArray]



infoStr1$ = "shots / hits        : "
infoStr2$ = "calc- / looptime: "



GOSUB userFunctions
GOSUB redraw


!mainLoop ------------------------------------

DO


 !check for pick-up-the-ball ----------------
 GR.TOUCH touch, xx, yy
 IF touch
  p2x = (ballx + diaBall)* scale_width
  p2y = (bally + diaBall)* scale_height
  GR.BOUNDED.TOUCH touchB, ballx * scale_width , bally * scale_height , p2x , p2y
  IF touchB THEN GOSUB performeShot
 ENDIF


 !update ball Accel Velocity Position ------
 GOSUB Ballrun



 !update graphic ---------------------------
 curInfoStr1$ = infoStr1$ + FORMAT$("###", shotCtr )     + "   / " + FORMAT$("###",hitCtr )
 curInfoStr2$ = infoStr2$ + FORMAT$("###", calcTimeAvg ) + "   / " + FORMAT$("###", toc )
 GR.MODIFY infoStr1, "text", curInfoStr1$
 GR.MODIFY infoStr2, "text", curInfoStr2$
 GR.MODIFY  p, "x", ballx
 GR.MODIFY  p, "y", bally



 !check collisions ---------------------------
 hitChk1  = gr_collision (p, hitChckRect)
 IF hitChk1
  ticHitChkOld = ticHitChk
  ticHitChk    = clock ()
  IF (ticHitChk-ticHitChkOld) > 2000 THEN
   IF ballyri > 0 THEN hitCtr += 1
  ENDIF
 ENDIF

 collChk1 = gr_collision (p, bask1)
 collChk2 = gr_collision (p, bask2)
 IF collChk1 THEN coll = calcBounceFixCircle ( ~
        bask1Rad , radBall , bask1X , bask1Y , ~
        ballx, bally, ballxri, ballyri, fnOut[] )
 IF collChk2 THEN coll = calcBounceFixCircle ( ~
        bask2Rad , radBall , bask2X , bask2Y , ~
        ballx, bally, ballxri, ballyri, fnOut[] )

 IF coll THEN
  coll = 0
  ballx   = fnOut[1]
  bally   = fnOut[2]
  ballxri = fnOut[3] %* bounceFriction
  ballyri = fnOut[4] %* bounceFriction
  GR.MODIFY  p, "x", ballx
  GR.MODIFY  p, "y", bally
  IF ABS(ballxri) | ABS(ballyri) > vibBorderVal THEN VIBRATE vib[],-1
 ENDIF



 !everything should be fine now .....--------
 GR.RENDER



 !time measurement (only for info) ----------
 IF !flagShot
  ptrcalcTime                += 1
  IF ptrcalcTime             > lenTimeArray THEN ptrcalcTime=1
  calcTimeArray[ptrcalcTime] = calcTime
  ARRAY.AVERAGE                calcTimeAvg, calcTimeArray[]
  calcTime                   = CLOCK()-tic
 ENDIF


 !(re)settings -------------------------------
 flagShot=0


 !create constant time step ------------------
 DO
  toc = CLOCK() - tic
 UNTIL toc>=30
 tic = CLOCK()




UNTIL 0
!--------------------------------------------




!--------------------------------------------
redraw:

!GR.CLS
GR.TEXT.SIZE 30
GR.COLOR 150, 0,0,0, 1
IF flagUseBackgrBmp THEN GR.BITMAP.DRAW backgr, backGrBmpScaled, 0, 0

GR.TEXT.SIZE 30
GR.COLOR 220, 220,0,0, 1
GR.TEXT.DRAW infoStr1, 50,  50 , infoStr1$
GR.COLOR 115, 128,128,128, 1
GR.TEXT.DRAW infoStr2, 50, 100 , infoStr2$


GR.COLOR 240, 100,100,100, 1
GR.RECT   nn  , bask1X , bask1Y-bask1Rad, bask2X, bask2Y+bask1Rad

GR.COLOR 255, 150,00,00, 1
GR.CIRCLE bask1, bask1X , bask1Y         , bask1Rad
GR.CIRCLE bask2, bask2X , bask2Y         , bask2Rad

GR.COLOR 0, 255,0,0, 1
GR.RECT   hitChckRect  , bask1X + radBall ,  bask1Y+1.2 * radBall, ~
bask2X - radBall  , bask2Y+1.2 * radBall+5

GR.COLOR 255, 255,0,0, 1
GR.BITMAP.DRAW  p, fuss_ptr2, ballx , bally


GR.SET.STROKE 5
GR.COLOR 255, 0,250,0, 1
GR.LINE line1, 0, 0, 0, 0

RETURN
!--------------------------------------------





!--------------------------------------------
Ballrun:

!Read the acclerometer
IF flagAccSens THEN
 SENSORS.READ 1, accSensx , accSensy , accSensz
 ballAccx = accSensx * accAmplify * -1
 ballAccy = accSensy * accAmplify *  1
ENDIF

ballxri   = (ballxri +  ballAccx ) * movingFriction
ballyri   = (ballyri +  ballAccy ) * movingFriction


ballx     = ballx + ballxri
bally     = bally + ballyri

IF ballx  <= lirand | ballx >= rerand THEN
 ballxri  = -ballxri * bounceFriction
 IF ballx <= lirand THEN ballx = lirand
 IF ballx >= rerand THEN ballx = rerand
 IF ABS(ballxri) > vibBorderVal THEN VIBRATE vib[],-1
ENDIF

IF bally  <= obrand | bally >= unrand THEN
 ballyri  = -ballyri * bounceFriction
 IF bally <= obrand THEN bally = obrand
 IF bally >= unrand THEN bally = unrand
 IF ABS(ballyri) > vibBorderVal THEN VIBRATE vib[],-1
ENDIF

RETURN
!--------------------------------------------








!--------------------------------------------
performeShot:


!sliding around with the ball....
DO
 GR.TOUCH touch2 , sx, sy
 sx = sx / scale_width
 sy = sy / scale_height
 GR.MODIFY  p, "x", sx - radBall
 GR.MODIFY  p, "y", sy - radBall
 GR.RENDER
UNTIL !touch2


! wait for another touch ........
DO
 GR.TOUCH touch2 , nn, nn
UNTIL touch2


! now show the "rubberband"...
GR.MODIFY line1 , "x1", sx
GR.MODIFY line1 , "y1", sy
GR.MODIFY line1 , "x2", sx
GR.MODIFY line1 , "y2", sy
GR.SHOW line1


!move the ball for aiming.....
DO
 GR.TOUCH touch2 , x ,y
 x = x / scale_width
 y = y / scale_height
 GR.MODIFY  p,     "x",  x - radBall
 GR.MODIFY  p,     "y",  y - radBall
 GR.MODIFY line1 , "x2", x
 GR.MODIFY line1 , "y2", y
 GR.RENDER
UNTIL !touch2


! after final release .....
ballx   = x - radBall
bally   = y - radBall
ballxri = (sx-x) * shotAmplify
ballyri = (sy-y) * shotAmplify

GR.HIDE line1
shotCtr += 1
flagShot=1

RETURN
!--------------------------------------------




!--------------------------------------------
userFunctions:


!--------------------------------------------
DIM fnOut[10]
FN.DEF calcBounceFixCircle( rad1, rad2 ,x1, y1, x2, y2, vx, vy, fn[] )
 nomDist    = rad1+rad2
 x2         = x2+ rad2 -x1
 y2         = y2+ rad2 -y1
 curDist    = SQR(x2^2+y2^2)
 IF curDist >= nomDist THEN FN.RTN 0
 m          = vy/vx
 n          = y2 - m*x2
 fac        = 1
 IF vx      <=0 THEN fac=-1
 xs         = - ( m*n + fac*SQR( nomDist ^2 * (m^2+1) - n^2 ) ) / (m^2 + 1)
 ys         = m*xs + n

 nx         = -ys / nomDist
 ny         =  xs / nomDist
 n_dot_v    = nx * vx + ny * vy

 fn[1]      = x1 + xs -rad2
 fn[2]      = y1 + ys -rad2
 fn[3]      = -(vx - 2 * n_dot_v * nx)
 fn[4]      = -(vy - 2 * n_dot_v * ny)

 FN.RTN 1
FN.END
!--------------------------------------------




RETURN
!--------------------------------------------
