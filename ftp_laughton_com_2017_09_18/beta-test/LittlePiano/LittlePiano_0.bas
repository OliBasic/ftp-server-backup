! Little piano...
! http://rfobasic.freeforums.org/a-little-piano-t1794.html

gr.open 255,85,93,93,1,0
gr.screen sw,sh
oy=32
dw=1280
dh=800-oy
scw=sw/dw
sch=sh/dh
gr.scale scw,sch
!  ****  bitmaps
path$="Piano/"
gr.bitmap.load btnBmp, path$+"ButtonGray.png"
gr.bitmap.scale btBmp, btnBmp, 80,80
gr.bitmap.load srcBmp, path$+"Touches.png"
gr.bitmap.scale srcScBmp, srcBmp, dw,dh-120
pY= oy+120
gr.bitmap.draw piano, srcScBmp, 0, pY
!  ****  design
gr.color 255,255,255,0,1
gr.text.size 35
gr.text.draw mess, dw-490,116," 0"
gr.set.stroke 3
gr.color 255,0,0,255,1   % blue line
gr.line nul, 0,oy+120,dw,oy+120
gr.color 255,255,255,255,0  % white
gr.text.size 70
gr.text.draw nul,150,115,"L i t t l e   P i a n o"
gr.set.stroke 1
gr.color 255,0,200,0,1  % green
gr.text.draw nul,147,112,"L i t t l e   P i a n o"
gr.color 255,255,255,255,0
gr.text.size 30
gr.text.align 2
!  ****   buttons
DIM bt[3],txbt[3],pt[4] % 4 pre-defined paint colors
gr.color 255,255,0,0,1  % red
gr.paint.get pt[1]
gr.color 255,0,255,0,1  % green
gr.paint.get pt[2]
gr.color 255,0,0,0,1    % black  (default)
gr.paint.get pt[3]
gr.color 255,0,230,250,1  % blue
gr.paint.get pt[4]
for b=1 to 3
  gr.bitmap.draw bt[b], btBmp, dw-410+(b-1)*140, oy+20
  gr.text.draw txbt[b], dw-410+(b-1)*140+40, oy+70, word$("Rec Play Stop",b)
  gr.modify txbt[b],"paint",pt[3]
next
gr.render
!   **** sounds
tlw= (sw/8)/scw    % white keys
offsetX= tlw/2     % black keys
DIM snd[13]
sndName$= "do_ re_ mi_ fa_ so_ la_ si_ doo dod red fad sod lad"
Soundpool.open 4
for s=1 to 13
  Soundpool.load snd[s], path$+word$(sndName$,s)+".wav"
next
pause 300
DIM mysnd$[10]
ctsnd=0

!   ****  principal loop
stgap= clock()
DO
  do
    gr.touch touched, tx, ty
  until touched
  tc=clock()
  tx/= scw
  ty/= sch
  if ty>pY      %  piano keys
    snd= floor(tx/tlw)+1   %  white keys
    snd2= snd
    if ty< dh-220
      snd= floor((tx-offsetX)/tlw)+9    %  black keys
      if snd=11 | snd>14 | snd=8 then snd=snd2
      if snd>10 then snd-=1
    endif
    if snd>0 & snd<14 & snd<>oldsnd     % play corresponding sound
      ! in order to record as a script : note + gap + note + gap + etc...
      gap= clock()-stgap-20   % gap between too sounds (silences)
      stgap= clock()
      Soundpool.play nul, snd[snd], 0.99, 0.99, 1,0,1
      oldsnd= snd
      if recOn
         mysnd$[ ctsnd ]= mysnd$[ ctsnd ] + str$(gap)+" "+str$(snd) + " "
         nbn+=2  % count notes
      endif
    endif
    gr.touch touched, ttx, tty
    if !touched then oldsnd=0    % key released
    
   elseif ty<pY    %  buttons ?
     btt= floor((tx-(dw-410))/140)+1
     
     if btt=1 &!recOn & !playOn                              %  button REC
       gr.modify txbt[btt],"paint",pt[btt]
       gr.modify txbt[2],"paint",pt[3]
       gr.modify txbt[3],"paint",pt[4]
       recOn= 1
       nbn=0
       ctsnd+=1
       ctt= ctsnd
       gr.modify mess,"text",format$("##", ctsnd )
       gr.render
       
     elseif btt=2 & !playOn & !recOn                          %  button PLAY
       gr.modify txbt[btt],"paint",pt[btt]
       gr.modify txbt[1],"paint",pt[3]
       gr.render
       if ctt
         do
           gr.touch touched, ttx, tty
           if clock()-tc>700      % change works if touch "PLAY" during more than 0.7s
             ctt = ctt+1-ctt*(ctt=ctsnd)
             gr.modify mess,"text", format$("##", ctt )
             gr.render
             tc= clock()
           endif
         until !touched
       endif
       playOn= 1
       ! play the works selected
       if ctt
         gr.modify txbt[3],"paint",pt[4]  % light "Stop"
         gr.render
         nbn= val(word$( mysnd$[ ctt ],1))  % nber of notes
         for n=3 to nbn step 2    % start to 3 because jumps the first time gap.
           gr.touch touched, tx, ty
           if floor((tx-(dw-410))/140)+1 =3 then F_n.break  % stop
           snd= val (word$( mysnd$[ ctt ],n))
           pse= val (word$( mysnd$[ ctt ],n+1))
           Soundpool.play nul, snd[snd], 0.99, 0.99, 1,0,1
           pause pse
         next
         pause 200
       endif
       goto stp
       
     elseif btt=3                           %  button STOP
       stp:
       gr.modify txbt[1],"paint",pt[3]
       gr.modify txbt[2],"paint",pt[3]
       gr.modify txbt[3],"paint",pt[3]
       gr.render
       if recOn then mysnd$[ ctsnd ]= str$(nbn+2)+" "+mysnd$[ ctsnd ]+"21.0"
       playOn= 0
       recOn = 0
     
     endif
   endif
UNTIL 0

OnBackKey:
Soundpool.release
gr.close
end "Bye..."
