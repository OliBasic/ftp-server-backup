!Waits for a connection from a client
!After connect: displays, what it receives
!
dim zeilen$[5] 
zlrptr=1

!bt.open                  % Before graphic. Open as server

gr.open 255, 188, 250, 169
gr.Orientation 0
gr.Color 255, 0, 0, 100, 0
stattxt$="----------"
gr.text.size 20
gr.text.draw Zeile5, 100, 100, stattxt$
gr.text.draw Zeile4, 100, 140, stattxt$
gr.text.draw Zeile3, 100, 180, stattxt$
gr.text.draw Zeile2, 100, 220, stattxt$
gr.text.draw Zeile1, 100, 260, stattxt$
gr.text.draw empfang, 100, 400, stattxt$
gr.render

!%gr.front 0

bt.open                   % After graphic. Open as server
!bt.close                 %  these two are
!bt.open                  %  not allowed

!%gr.front 1

!----------------------------------------
listen:
!bt.connect                   % Only for Master-mode
yhlr=0
DO
 yhlr=yhlr+1
 BT.STATUS s
 IF s = 1
      stattxt$= "Listening  "+ str$(yhlr)
 ELSEIF s = 2
      stattxt$= "Connecting"
 ELSEIF s = 3
      bt.device.name device$
      stattxt$= "Connected  "+ device$
 ENDIF
 PAUSE 1000
 gosub scrolline
UNTIL s =3
!----------------------------------------
!Here is Connected
!----------------------------------------
msgout$=""

receive:
 BT.STATUS s
 IF s<> 3
      stattxt$= "Connection lost"
      gosub scrolline
      GOTO listen
 ENDIF

msg$=""
do
  BT.READ.READY rr
  IF rr
    BT.READ.BYTES msg$
    msgout$=msgout$+msg$
    if len(msgout$)>50 then msgout$=""
  ENDIF
UNTIL rr = 0
gr.modify empfang, "text", msgout$
gr.render
pause 100
if left$(msgout$, 3) = "end" then bt.disconnect
goto receive
!-----------------------------------------------
scrolline:
zlrptr=zlrptr-1
if zlrptr=0 then zlrptr=5
zeilen$[zlrptr]=stattxt$

 gr.modify zeile1, "text", zeilen$[zlrptr]
 zlrptr=zlrptr+1
 if zlrptr=6 then zlrptr=1
 gr.modify zeile2, "text", zeilen$[zlrptr]
 zlrptr=zlrptr+1
 if zlrptr=6 then zlrptr=1
 gr.modify zeile3, "text", zeilen$[zlrptr]
 zlrptr=zlrptr+1
 if zlrptr=6 then zlrptr=1
 gr.modify zeile4, "text", zeilen$[zlrptr]
 zlrptr=zlrptr+1
 if zlrptr=6 then zlrptr=1
 gr.modify zeile5, "text", zeilen$[zlrptr]
 zlrptr=zlrptr+1
 if zlrptr=6 then zlrptr=1
 gr.render
return
!------------------------------------------------
onbackkey:
bt.close
end
