!!
The program implements a
Superuser terminal.

Your device must be rooted
to successfully run this
program.

!!

! Get Superuser permission
su.open

! Main loop
loop:

! get the command from the user
tget r$, "cmd: "

! if cmd is "exit"
if r$ = "exit"
 su.close
 print "Exited"
 end
endif

! write the command
su.write r$

! Give system time to respond
pause 500

! check for a response
su.read.ready ready

! if no input, do next command
if !ready then goto loop

! read responses
do
 su.read.line l$
 print l$
 su.read.ready ready
until !ready
 
! Go for the next command
goto loop

