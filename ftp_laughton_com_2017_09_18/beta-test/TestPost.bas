!!
TestPost.bas

Tests the http.post command by
posting to the BASIC! Guestbook.

!!

! set the url

url$="http://laughton.com/basic/guestbook/infobook.cgi"

! Load the name/value pairs
! into an array

array.load a$[], "realname","Paul Laughton"~
"username", "paul@laughton.com"~
"city", "San Jose"~
"state", "CA"~
"country", "USA"~
"comments", "BASIC! Testing http.post 1"~
"password", "GZ7"

! Add the array to new list
list.create s, list
list.add.array list,a$[]

! Do the post

http.post url$,list, r$

! Print the server response

print r$
end
 
 
