!!
Test Bluetooth commands in GR mode.

This test requires two Android devices.
On one device, run f35_bluetooth.bas and
select "Continue to listen for connection".
On the second device, run this program.

This program opens graphics mode, then opens
Bluetooth. It will prompt you to connect to the
first device. When it connects, the second device
posts post a notification from BASIC! naming the
device it connected to.

On the first device, select "Send".
Enter and send a digit.

The second device receives the digit.
If it is 0 - 4, the second device will draw
a large colored rectangle. If it is anything
else, the rectangle will be the same gray as
the background.

To exit this program on the second device,
tap the back key or anywhere on the screen.
!!

FN.DEF setColor(color)
 SW.BEGIN color
  SW.CASE 0  %black
   GR.COLOR 255, 0,0,0, 1
   SW.BREAK
  SW.CASE 1  %white
   GR.COLOR 255, 255,255,255, 1
   SW.BREAK
  SW.CASE 2  %red
   GR.COLOR 255, 255,0,0, 1
   SW.BREAK
  SW.CASE 3  %green
   GR.COLOR 255, 0,255,0, 1
   SW.BREAK
  SW.CASE 4  %blue
   GR.COLOR 255, 0,0,255, 1
   SW.BREAK
  SW.DEFAULT %unknown, make it gray
   GR.COLOR 255, 100, 100, 100, 1
   SW.BREAK
 SW.END
FN.END

FN.DEF startBluetooth()
 BT.OPEN
 BT.CONNECT
 DO
  BT.STATUS bts
 UNTIL bts = 3
 BT.DEVICE.NAME device$
 msg$ = "Connected to " + device$
 NOTIFY msg$, "", msg$, 0
FN.END

! Program starts here

GR.OPEN 255, 100, 100, 100, 1, -1
GR.SCREEN width, height
left = width/10
right = 9*left
top = height/10
bottom = 9*top
color = 1

DO
 GR.CLS
 CALL setColor(color)
 GR.RECT r, left, top, right, bottom
 GR.RENDER

!!
 Doing the BT connection here is artificial,
 but the point is to test BT in GR mode.
!!
 BT.STATUS bts
 IF bts <> 3
  CALL startBluetooth()
 ENDIF

 wait = 1
 DO
 UNTIL wait = 0		% wait for OnBTReadReady

UNTIL 0

ONBTREADREADY:
BT.READ.BYTES msg$
color = ASCII(msg$) - ASCII("0")
IF color >= 0 & color <= 9 THEN wait = 0
BT.ONREADREADY.RESUME

ONBACKKEY:
ONGRTOUCH:
GR.CLOSE
END

ONERROR:
PRINT GETERROR$()
END
