FN.DEF fn_topo_srtm3(curLat,curLon)


 ! filename of needed srtm3-file(*.hgt) -------------------------

 hgt$ = REPLACE$(~
   "N" + FORMAT$("%%" ,FLOOR(curLat))~
 + "E" + FORMAT$("%%%",FLOOR(curLon))," ","")+".hgt"
 FILE.EXISTS fileExistsHgt, hgt$
 IF !fileExistsHgt THEN FN.RTN -9999


 ! read values ---------------------------------------------------
 
 BYTE.OPEN r, no1,  hgt$
 BYTE.READ.BUFFER no1, 2 884 802 , hgtBuf$
 BYTE.CLOSE no1


 ! determine the position of the "upper left" (north-west) corner
 !of the surrounding square of current lat/lon
 
 gridWidth	= 1/1200			% each .hgt file  contains 1200 values per 1 deg
 refLat 		= FLOOR(curLat)+1
 refLon 		= FLOOR(curLon)
 posLat 		= FLOOR((refLat - curLat) / gridWidth)
 posLon 		= FLOOR((curLon - refLon) / gridWidth)
 ptrUpperLeft= FLOOR((posLat * 1201 + posLon) * 2 + 1)


 ! read height values from hgt-file --------------------------------
 
 !northern 4 bytes,  read northern height-values into hnw, hne
 
 tmp$	  =	MID$(hgtBuf$, ptrUpperLeft,4)
 hnw		=	ASCII(MID$(tmp$,1,1))*256 + ASCII(MID$(tmp$,2,1))
 hne		=	ASCII(MID$(tmp$,3,1))*256 + ASCII(MID$(tmp$,4,2))
 
 !southern 4 bytes,  read southern height-values into hsw, hse
 
 tmp$  	=	MID$(hgtBuf$, ptrUpperLeft+2402,4)
 hsw		=	ASCII(MID$(tmp$,1,1))*256 + ASCII(MID$(tmp$,2,1))
 hse		=	ASCII(MID$(tmp$,3,1))*256 + ASCII(MID$(tmp$,4,2))


 ! linear interpolation --------------------------------------------
 
 yo = refLat - posLat * gridWidth
 yu = yo - gridWidth
 xu = refLon + posLon * gridWidth
 xo = xu + gridWidth
 hn = (curLon - xu) / (xo - xu) * (hne - hnw) + hnw
 hs = (curLon - xu) / (xo - xu) * (hse - hsw) + hsw
 curHeight = (curLat - yu) / (yo - yu) * (hn - hs) + hs

 FN.RTN curHeight
FN.END



! testing ---------------------------------------

lat=51.53294
lon=9.6026

DO
 ctr=ctr+1
 tic=CLOCK()
 curH=fn_topo_srtm3(lat,lon)
 toc=CLOCK()-tic
 PRINT ctr; " ";toc; " "; curH
 PAUSE 1000-toc
UNTIL 0



