! Socket client test program
! To be used with socketServerTest.bas

! Start the server before starting this
! program

! Connect to the specified IP on the
! specified Port

socket.client.connect "192.168.0.103", 8888

! When the connection is established,
! send the server a meessage

socket.client.write.line "Client to server message"

! and then wait for Server to respond
! or time out after 10 seconds

maxclock = clock() + 10000
do
 socket.server.read.ready flag
 if clock() > maxclock 
   print "Read time out"
   end
 endif
until flag

! Server has sent message.
! Read it. Print it.

socket.client.read.line line$
print line$

! Close the client

socket.client.close

end
 
