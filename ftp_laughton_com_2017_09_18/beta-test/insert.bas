!!
This program demonstrats some 
of the possibilities
when using the BASIC! html commands.
!!

! html must be opened before doing 
! any html command
html.open

! Load the file, htmlDemo1.html

html.load.url "file:///sdcard/rfo-basic/data/insert.html"

!The user now sees the html

! We can now monitor the user
! actions

xnextUserAction:

! loop until data$ is not ""

do
html.get.datalink data$
until data$ <> ""

! The first four characters of data$
! identify the type of data returned.
! Extract those three characters to
! use in a switch statement

type$ = left$(data$, 4)

! Trim the first four characters from
! data$

data$ = mid$(data$,5)

! Act on the data type
! Shown are all the current data types

sw.begin type$

! Back Key hit.
! if we can go back then do it
 sw.case "BAK:"
  print "BACK key: " + data$
  if data$ = "1" then html.go.back else end
  sw.break
  
 ! A hyperlink was clicked on
 sw.case "LNK:"
   print "Hyperlink selected: "+ data$
   html.load.url data$
   sw.break
   
 ! An error occured
 sw.case "ERR:"
   print "Error: " + data$
   sw.break
   
  ! User data returned
  sw.case "DAT:"
    print "User data: " + data$
	   if  left$(data$, 5) = "image" 
	    print "Inserting image"
     url$="Cartman.png"
		   l$ = "javascript:image(\""+url$+"\")"
		   print l$
		   html.load.url l$
	    endif
	  sw.break
	
! Form data returned.
! Note: Form data returning
! always exits the html.

sw.case "FOR:"
 print "Form data: "+data$
 END
 sw.break

! download requested
! extract the filename
! tell user download starting
! download it

sw.case "DNL:"
  print "Download: " + data$
  array.delete p$[]
  split p$[], data$, "/"
  array.length l, p$[]
  fn$ = p$[l]
  html.load.string "<html> Starting download of " + fn$ + "</html>"
  byte.open r,f,data$
  pause 2000
  html.go.back
  byte.copy f,fn$
  byte.close f
  sw.break

 
sw.default
 print "Unexpected data type:", type$ + data$
 END
 
 sw.end
 
 goto xnextUserAction
