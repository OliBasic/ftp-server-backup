REM Start of BASIC! Program
!Title: MVTs Tarot (M.A.)
!Summary: A simple program that displays a major aracana tarot card interpretation.
!The tarot interpretations are from the 1888 book THE TAROT by S.L. MacGregor Mathers, which in the public domain in the USA.
!Copyright (c) 2016 by Michael and Vivian Thomas.
!Released under the MIT license, a copy of which can be found in the license.txt file

CLS
PRINT"MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs TarotMVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot MVTs Tarot"
INPUT"Please enter your question.", a$
Dialog.Message"Please wait.", "I have to consult the cards.", ok, "OK"
PAUSE(1000)
i = randomNumber
i = FLOOR(RND()*22)+1
IF i = 1 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "The Juggler\nHe represents will, will-power, and dexterity.", ok, "OK" 
ELSEIF i = 2 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "The High Priestess\nShe represents science, wisdom, knowledge, and education.", ok, "OK" 
ELSEIF i = 3 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "The Empress\nShe represents action, plan, undertaking movement in a matter, and initiative.", ok, "OK" 
ELSEIF i = 4 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "The Emperor\nHe represents realisation, effect, and development.", ok, "OK" 
ELSEIF i = 5 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "The Hierophant\nHe represents mercy, beneficence kindness, and goodness.", ok, "OK" 
ELSEIF i = 6 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "The Lovers\nThey represent wise dispositions, proof, trials surmounted.", ok, "OK" 
ELSEIF i = 7 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "The Chariot\nIt represents triumph, victory, overcoming obstacles.", ok, "OK" 
ELSEIF i = 8 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "Themis or Justice\nThis represents equilibrium, balance, and justice.", ok, "OK" 
ELSEIF i = 9 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "The Hermit\nHe represents prudence, caution, deliberation.", ok, "OK" 
ELSEIF i = 10 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "The Wheel of Fortune\nIt represents good fortune, success, unexpected luck.", ok, "OK" 
ELSEIF i = 11 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "Strength or Fortitude\nThis represents power, might, force, strength, fortitude.", ok, "OK" 
ELSEIF i = 12 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "The Hanged Man\nHe represents self-sacrifice, sacrifice, devotion, and (being) bound.", ok, "OK" 
ELSEIF i = 13 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "Death\nHe represents death, change, transformation, and alteration for the worse.", ok, "OK" 
ELSEIF i = 14 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "Temperance\nShe represents combination, conformation, uniting.", ok, "OK" 
ELSEIF i = 15 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "The Devil\nHe represents fatality for good.", ok, "OK" 
ELSEIF i = 16 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "The Lightning-struck Tower\nThis represents ruin, disruption, over-throw, loss and bankruptcy.", ok, "OK" 
ELSEIF i = 17 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "The Star\nThis represents hope, expectation, and bright promises.", ok, "OK" 
ELSEIF i = 18 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "The Moon\nThis represents twilight, deception, and error.", ok, "OK" 
ELSEIF i = 19 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "The Sun\nThis represents happiness, and (being) content.", ok, "OK" 
ELSEIF i = 20 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "The Last Judgement\nThis represents renewal, result and determination of a matter.", ok, "OK" 
ELSEIF i = 21 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "The Foolish Man\nHe represents folly, expiation, and wavering.", ok, "OK" 
ELSEIF i = 22 THEN
 Dialog.Message"Your Major Arcana Tarot Card Is:", "The Universe\nThis represents completion, and good reward.", ok, "OK" 
ENDIF
Dialog.Message"", "Thank you for playing, but remember that this is only a game. Do not make life decisions based on these results.", ok, "OK"
Dialog.Message"", "Press OK then BACK to exit.", ok, "OK"
DO
UNTIL 0
ONBACKKEY:
EXIT
