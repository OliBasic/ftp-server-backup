! **************************************
! acuario
! **************************************


DEVICE dev$  %devuelve informacion sobre la tableta
numfichas=116
DIM numero[116],familia$[116],cientifico$[116],vulgar$[116]
DIM numr[348], expli$[348]
FOR i=1 TO numfichas
 READ.NEXT numero[i],familia$[i],cientifico$[i],vulgar$[i]
NEXT
numliteratura=348
FOR i=1 TO numliteratura
 READ.NEXT numr[i], expli$[i]
NEXT

GR.OPEN 255,255,255,255,0,0  %modo grafico colores orientacion landscape
PAUSE 1000     % ponerlo siempre despues de gr.open

GR.SET.STROKE 4     % ancho de linea de los objetos
GR.SCREEN w,h   % devuelve el width y height de la pantalla
sx=w/1280
sy=h/752
esc=sx
if sy<sx then esc=sy
GR.SCALE esc,esc  % escala todos los comandos graficos
GR.BITMAP.LOAD masuno,"masuno.jpg"
GR.BITMAP.LOAD menosuno,"menosuno.jpg"
GR.BITMAP.LOAD masdiez,"masdiez.jpg"
GR.BITMAP.LOAD menosdiez,"menosdiez.jpg"
GR.BITMAP.LOAD ultimo,"ultimo.jpg"
GR.BITMAP.LOAD primero,"primero.jpg"
GR.BITMAP.LOAD fin,"fin.jpg"
x1=1020
y1=15
GR.BITMAP.DRAW space2,masuno,x1,y1
x2=1150
y2=15
GR.BITMAP.DRAW space3,menosuno,x2,y2
x3=1020
y3=152
GR.BITMAP.DRAW space4,masdiez,x3,y3
x4=1150
y4=152
GR.BITMAP.DRAW space5,menosdiez,x4,y4
x5=1020
y5=289
GR.BITMAP.DRAW space6,ultimo,x5,y5
x6=1150
y6=289
GR.BITMAP.DRAW space7,primero,x6,y6
x7=1020
y7=426
GR.BITMAP.DRAW space8,fin,x7,y7

! GR.BITMAP.CREATE img,1012,644
GR.BITMAP.LOAD bm_ptr, "001.jpg"
GR.BITMAP.DRAW obj_ptr,bm_ptr,0,0

FN.DEF fichero$(fichero)
 fichero$=LEFT$("00"+STR$(fichero),LEN("00"+STR$(fichero))-2)+".jpg"
 fichero$=RIGHT$(fichero$,7)
 FN.RTN fichero$
FN.END
GR.COLOR 255,0,0,0,1
GR.TEXT.SIZE 21
gr.text.size 21
GR.TEXT.DRAW posicion, 1080,580,"click en la foto"

GR.TEXT.SIZE 33
GR.TEXT.ALIGN 1      % alinea el texto respecto a gr.text.draw. 2=centrado
fichero=1
GR.TEXT.DRAW tony,0,710, "   "+LEFT$(fichero$(fichero),3)+" - "+familia$[fichero]+~
" - "+cientifico$[fichero]+" - "+vulgar$[fichero]

GR.RENDER  % muestra el area grafica. no hace falta pausa despues
gr.bitmap.delete bm_ptr
GR.TEXT.SIZE 25
GR.TEXT.ALIGN 2
GR.TEXT.DRAW puntero1,512,120,""
GR.TEXT.DRAW puntero2,512,170, ""
GR.TEXT.DRAW puntero3,512,220, ""
GR.TEXT.DRAW puntero4,512,270, ""
GR.TEXT.DRAW puntero5,512,320, ""
GR.TEXT.DRAW puntero6,512,370, ""

! bucle principal infinito
! hasta 6 lineas de texto para la descripcion

foto_o_texto=1  % 1=esta en foto  2= esta en texto
DO 
    accion$="nada"
    IF (x>x1 & x<x1+122 & y>y1 & y<y1+122) THEN accion$="masuno"
    IF (x>x2 & x<x2+122 & y>y2 & y<y2+122) THEN accion$="menosuno"
    IF (x>x3 & x<x3+122 & y>y3 & y<y3+122) THEN accion$="masdiez"
    IF (x>x4 & x<x4+122 & y>y4 & y<y4+122) THEN accion$="menosdiez"
    IF (x>x5 & x<x5+122 & y>y5 & y<y5+122) THEN accion$="ultimo"
    IF (x>x6 & x<x6+122 & y>y6 & y<y6+122) THEN accion$="primero"
    IF (x>x7 & x<x7+122 & y>y7 & y<y7+122) THEN ~
                 browse "http://mangriton.royalwebhosting.net"
    IF (x>0 & x<1012 & y>0 & y<644) & foto_o_texto=1 THEN accion$="vertexto"
    IF (x>0 & x<1012 & y>0 & y<644) & foto_o_texto=2 THEN accion$="verfoto"
    IF accion$="masuno" & fichero<numfichas THEN fichero=fichero+1
    IF accion$="menosuno" & fichero>1 THEN fichero=fichero-1
    IF accion$="masdiez" & fichero<numfichas-9 THEN fichero=fichero+10
    IF accion$="menosdiez" & fichero>10 THEN fichero=fichero-10
    IF accion$="primero" THEN fichero=1
    IF accion$="ultimo" THEN fichero=numfichas
    IF accion$="fin" THEN D_U.BREAK
    IF accion$<>"nada" & accion$<>"vertexto" & accion$<>"verfoto" THEn
          GOSUB borra_literatura
          GR.BITMAP.LOAD bm_ptr2,fichero$(fichero)
          GR.MODIFY obj_ptr,"bitmap",bm_ptr2
          ! ahora el texto de abajo
          nuevo$="   "+LEFT$(fichero$(fichero),3)+" - "+familia$[fichero]+~
          " - "+cientifico$[fichero]+" - "+vulgar$[fichero]
          GR.TEXT.SIZE 33
          GR.MODIFY tony,"text",nuevo$
          GR.RENDER
          GR.BITMAP.DELETE bm_ptr2
           foto_o_texto=1
    ENDIF
    IF accion$="vertexto" THEN
       GR.BITMAP.LOAD bm_ptr2,"rectangulo.jpg"
       GR.MODIFY obj_ptr,"bitmap",bm_ptr2
       GR.TEXT.SIZE 25
       GR.TEXT.ALIGN 2
       J=1
       FOR i=1 TO numliteratura
           IF numr[i]>fichero THEN f_n.break
           IF numr[i]=fichero THEN
           IF j=1 THEN GR.MODIFY puntero1,"text",expli$[I]
           IF j=2 THEN GR.MODIFY puntero2,"text",expli$[I]
           IF j=3 THEN GR.MODIFY puntero3,"text",expli$[I]
           IF j=4 THEN GR.MODIFY puntero4,"text",expli$[I]
           IF j=5 THEN GR.MODIFY puntero5,"text",expli$[I]
           IF j=6 THEN GR.MODIFY puntero6,"text",expli$[I]
           j=J+1
          ENDIF   
        NEXT i
        GR.RENDER
gr.bitmap.delete bm_ptr2
        GR.TEXT.SIZE 33
        foto_o_texto=2
    Endif
    if accion$="verfoto"
      GOSUB borra_literatura
      GR.BITMAP.LOAD bm_ptr2,fichero$(fichero)
      GR.MODIFY obj_ptr,"bitmap",bm_ptr2
      GR.RENDER
      GR.BITMAP.DELETE bm_ptr2
      foto_o_texto=1
    ENDIF
     

 DO
  GR.TOUCH touched ,x,y
 UNTIL touched
 DO
  GR.TOUCH touched,x,y
 UNTIL !touched
 % x,y son las coordenadas al tocar pantalla
  x=x/esc
  y=y/esc
UNTIL 0
ONERROR:
end
! datas


borra_literatura:
GR.MODIFY puntero1,"text", ""
GR.MODIFY puntero2,"text", ""
GR.MODIFY puntero3,"text", ""
GR.MODIFY puntero4,"text", ""
GR.MODIFY puntero5,"text", ""
GR.MODIFY puntero6,"text", ""
RETURN

READ.DATA 1	,"CYPRINIDAE"	,"Barbus conchonius","Barbo rosado"
READ.DATA 2	,"CYPRINIDAE"	,"Barbus everetti","Barbo payaso"
READ.DATA 3	,"CYPRINIDAE"	,"Barbus oligolepis","	  "
READ.DATA 4	,"CYPRINIDAE"	,"Barbus fasciatus","Barbo rayado"
READ.DATA 5	,"CYPRINIDAE"	,"Barbus nigrofasciatus","Barbo de cabeza purpurea"
READ.DATA 6	,"CYPRINIDAE"	,"Barbus Odessa","Barbo de Odessa"
READ.DATA 7	,"CYPRINIDAE"	,"Barbus schwanefeldi","Barbo de Schwanefeldi"
READ.DATA 8	,"CYPRINIDAE"	,"Barbus schuberti","Barbo de schuberti"
READ.DATA 9	,"CYPRINIDAE"	,"Barbus tetrazona tetrazona","Barbo de Sumatra o tigre"
READ.DATA 10	,"CYPRINIDAE"	,"Barbus titteya","Barbo cereza"
READ.DATA 11	,"CYPRINIDAE"	,"Brachydanio nigrofasciatus","Danio punteado"
READ.DATA 12	,"CYPRINIDAE"	,"Brachydanio rerio","Danio cebra"
READ.DATA 13	,"CYPRINIDAE"	,"Rasbora heteromorpha","Pez arlequin"
READ.DATA 14	,"CYPRINIDAE"	,"Rasbora trilineata","Tijerita"
READ.DATA 15	,"CYPRINIDAE"	,"Labeo bicolor","Tiburon negro de cola roja"
READ.DATA 16	,"CYPRINIDAE"	,"Tanichthys albonubes","Neon chino"
READ.DATA 17	,"CYPRINIDAE"	,"Anaecypris hispanica","Jarabugo"
READ.DATA 18	,"CHARACIDAE"	,"Aphyocharax anisitsi","Tetra de cola roja"
READ.DATA 19	,"CHARACIDAE"	,"Astyanax mexicanus","Tetra ciego"
READ.DATA 20	,"CHARACIDAE"	,"Cheirodon axelrodi","Tetra cardenal"
READ.DATA 21	,"CHARACIDAE"	,"Gymnocorymbus ternetzi","Monjita"
READ.DATA 22	,"CHARACIDAE"	,"Hemigrammus caudovittatus","Tetra de Buenos Aires"
READ.DATA 23	,"CHARACIDAE"	,"Hemigrammus erythrozonus","Tetra brillante"
READ.DATA 24	,"CHARACIDAE"	,"Hemigrammus ocellifer"	,"Tetra linterna"
READ.DATA 25	,"CHARACIDAE"	,"Hemigrammus rhodostomus","Tetra de hocico rojo. Borrachito."
READ.DATA 26	,"CHARACIDAE"	,"Hyphessobrycon erysthrostigma"	,"Tetra de corazon sangrante"
READ.DATA 27	,"CHARACIDAE"	,"Hyphessobrycon heterorhabdus","Tetra bandera belga"
READ.DATA 28	,"CHARACIDAE"	,"Hyphessobrycon pulchripinnis","Tetra limon"
READ.DATA 29	,"CHARACIDAE"	,"Megalamphodus megalopterus","Tetra fantasma negro"
READ.DATA 30	,"CHARACIDAE"	,"Micralestes interruptus","Tetra del Congo"
READ.DATA 31	,"CHARACIDAE"	,"Moenkhausia pittieri","Tetra diamante"
READ.DATA 32	,"CHARACIDAE"	,"Moenkhausia sanctafilomenae","Tetra espejo o de ojos rojos"
READ.DATA 33	,"CHARACIDAE"	,"Nematobrycon palmeri","Tetra emperador"
READ.DATA 34	,"CHARACIDAE"	,"Paracheirodon innesi","Tetra neon"
READ.DATA 35	,"CHARACIDAE"	,"Pristella maxillaris","Pez rayos X o Jilguero"
READ.DATA 36	,"ANOSTOMIDAE"	,"Anostomus anostomus","Anostomo rayado"
READ.DATA 37	,"LEBIASINIDAE"	,"Copella arnoldi","Copeina rociadora"
READ.DATA 38	,"LEBIASINIDAE"	,"Nannostomus trifasciatus","Pez lapiz de tres lineas"
READ.DATA 39	,"GASTEROPELECIDAE","Carnegiella strigata","Pez hacha"
READ.DATA 40	,"SERRASALMIDAE","Serrasalmus nattereri","Piraña"
READ.DATA 41	,"CICHLIDAE"	,"Aequidens curviceps","Ciclido bandera"
READ.DATA 42	,"CICHLIDAE"	,"Aequidens maronii","Ciclido cerradura"
READ.DATA 43	,"CICHLIDAE"	,"Aequidens pulcher","Acara azul"
READ.DATA 44	,"CICHLIDAE"	,"Apistogramma agassizi","Ciclido enano de Agassiz"
READ.DATA 45	,"CICHLIDAE"	,"Astronotus ocellatus","Oscar"
READ.DATA 46	,"CICHLIDAE"	,"Thorichthys meeki","Boca de fuego"
READ.DATA 47	,"CICHLIDAE"	,"Julidochromis ornatus","Julie dorado"
READ.DATA 48	,"CICHLIDAE"	,"Labeotropheus fuelleborni","Ciclido de fuelleborn"
READ.DATA 49	,"CICHLIDAE"	,"Cichlasoma citrinellum","Ciclido Midas"
READ.DATA 50	,"CICHLIDAE"	,"Cichlasoma labiatum","Ciclido labiado"
READ.DATA 51	,"CICHLIDAE"	,"Cichlasoma managuense","Jaguar.Managüense"
READ.DATA 52	,"CICHLIDAE"	,"Cichlasoma nigrofasciatum","Cicido cebra"
READ.DATA 53	,"CICHLIDAE"	,"Cichlasoma octofasciatum","Jack Dempsey"
READ.DATA 54	,"CICHLIDAE"	,"Neetroplus nematopus","Ciclido enano de ojos verdes"
READ.DATA 55	,"CICHLIDAE"	,"Microgeophagus ramirezi","Ciclido enano de Ramirez"
READ.DATA 56	,"CICHLIDAE"	,"Pelvicachromis pulcher","Kribensis"
READ.DATA 57	,"CICHLIDAE"	,"Pterophyllum scalare","Pez angel.Escalar"
READ.DATA 58	,"CICHLIDAE"	,"Symphysodon aequifasciatus","	Pez disco "
READ.DATA 59	,"BELONTIIDAE"	,"Betta splendens","Combatiente siames"
READ.DATA 60	,"BELONTIIDAE"	,"Colisa lalia","Gurami enano"
READ.DATA 61	,"BELONTIIDAE"	,"Helostoma temmincki","Gurami besucón"
READ.DATA 62	,"BELONTIIDAE"	,"Macropodus opercularis","Pez paraiso"
READ.DATA 63	,"CYPRINODONTIDAE","Aphyosemion australe","Cabo Lopez"
READ.DATA 64	,"CYPRINODONTIDAE","Aplocheilus panchax","Panchax azul"
READ.DATA 65	,"CYPRINODONTIDAE","Cynolebias belloti","Pavito"
READ.DATA 66	,"CYPRINODONTIDAE","Jordanella floridae","Jordanella"
READ.DATA 67	,"CYPRINODONTIDAE","Valencia hispanica","Samarugo"
READ.DATA 68	,"CYPRINODONTIDAE","Aphanius iberus","Fartet"
READ.DATA 69	,"CYPRINODONTIDAE","Aphanius baeticus","Salinete"
READ.DATA 70	,"CALLICHTHYIDAE","Brochis splendens","Brochis esmeralda"
READ.DATA 71	,"CALLICHTHYIDAE","Corydoras adolfoi","Coridora de Adolfo"
READ.DATA 72	,"CALLICHTHYIDAE","Corydoras aeneus","Coridora bronceada"
READ.DATA 73	,"CALLICHTHYIDAE","Corydoras paleatus","Coridora punteada"
READ.DATA 74	,"LORICARIIDAE"	,"Hyposthomus plecostomus","Pleco"
READ.DATA 75	,"LORICARIIDAE"	,"Otocinclus vestitus","Otocinclo dorado"
READ.DATA 76	,"LORICARIIDAE"	,"Ancistrus dolichopterus","Ancistro"
READ.DATA 77	,"SILURIDAE"	,"Kryptopterus bicirrhis","Pez gato de cristal"
READ.DATA 78	,"COBITIDAE"	,"Acanthophthalmus kuhli","Kuhli"
READ.DATA 79	,"COBITIDAE"	,"Botia macracanta","Locha payaso"
READ.DATA 80	,"POECILIDAE"	,"Poecilia latipinna","Molly de aletas de vela"
READ.DATA 81	,"POECILIDAE"	,"Poecilia reticulata","Guppy"
READ.DATA 82	,"POECILIDAE"	,"Xiphophorus helleri","Xifo. Cola de espada"
READ.DATA 83	,"POECILIDAE"	,"Xiphophorus maculatus","Platy"
READ.DATA 84	,"POECILIDAE"	,"Xiphophorus variatus","Platy variado"
READ.DATA 85	,"POECILIDAE"	,"Belonesox belizanus","Aguja"
READ.DATA 86	,"POECILIDAE"	,"Gambusia affinis","Gambusia"
READ.DATA 87	,"POECILIDAE"	,"Heterandria formosa","Pez mosquito"
READ.DATA 88	,"MOCHOKIDAE"	,"Brachysynodontis batensoda","	Pez gato invertido gigante"
READ.DATA 89	,"MOCHOKIDAE"	,"Synodontis alberti","Synodontis de Albert"
READ.DATA 90	,"ANABLEPIDAE"	,"Anableps anableps","Cuatro ojos"
READ.DATA 91	,"GOODEIDAE"	,"Ameca splendens","Pez mariposa"
READ.DATA 92	,"GOODEIDAE"	,"Characodon audax","  "
READ.DATA 93	,"GOODEIDAE"	,"Xenotoca eiseni","Cola roja"
READ.DATA 94	,"CENTRARCHIDAE","Lepomis gibbosus","Perca sol"
READ.DATA 95	,"TOXOTIDAE"	,"Toxotes jaculatrix","Pez arquero"
READ.DATA 96	,"PLANTAS DE ACUARIO","Cryptocoryne wendtii","Aracea de Wendt"
READ.DATA 97	,"PLANTAS DE ACUARIO","Alternanthera reineckii","   "
READ.DATA 98	,"PLANTAS DE ACUARIO"	,"Hygrophila polysperma","Limonero mediano"
READ.DATA 99	,"PLANTAS DE ACUARIO"	,"Echinodorus bleheri",	"Espada del amazonas"
READ.DATA 100	,"PLANTAS DE ACUARIO"	,"Microsorum pteropus","Helecho de Java"
READ.DATA 101	,"PLANTAS DE ACUARIO"	,"Sagittaria sabulata","Sagitaria"
READ.DATA 102	,"PLANTAS DE ACUARIO"	,"Anubias gracilis","Anubias"
READ.DATA 103	,"PLANTAS DE ACUARIO"	,"Cabomba caroliniana","Carolina"
READ.DATA 104	,"PLANTAS DE ACUARIO"	,"Ceratophyllum demersum","Cola de zorro"
READ.DATA 105	,"PLANTAS DE ACUARIO"	,"Vesicularia dubyana","Musgo de Java"
READ.DATA 106	,"PLANTAS DE ACUARIO"	,"Vallisneria espiralis","Vallisneria"
READ.DATA 107 ,"ACUARIO COMUNITARIO","Plantas","Peces"
READ.DATA 108 , "ACUARIO COMUNITARIO","Plantas","Peces"
READ.DATA 109 , "ACUARIO COMUNITARIO","Plantas","Peces"
READ.DATA 110 , "ACUARIO COMUNITARIO","Plantas","Peces"
READ.DATA 111 , "ACUARIO COMUNITARIO","Plantas","Peces"
READ.DATA 112 , "ACUARIO COMUNITARIO","Plantas","Peces"
READ.DATA 113 , "ACUARIO COMUNITARIO","Plantas","Peces"
READ.DATA 114 , "ACUARIO COMUNITARIO","Plantas","Peces"
READ.DATA 115 , "ACUARIO COMUNITARIO","Plantas","Peces"
READ.DATA 116 , "ACUARIO COMUNITARIO","Plantas","Peces"
! vienen 354 lineas mas con dos datos cada linea

READ.DATA  	1	,"Cuerpo de forma ovalada, color rojizo tirando a rosado sobre fondo plateado, con una"
READ.DATA  	1       ,"mancha  característica de color negro cerca de la aleta anal. El macho tiene un color"
READ.DATA  	1       ,"rosa púrpura y sus aletas dorsales, caudal y anal tienen ápices negros. La hembra"
READ.DATA  	1       ,"tiene el vientre redondeado y es menos coloreada. Habitan en ríos, estanques y fosas"
READ.DATA  	1       ,"de agua dulce del norte de la India y Bangladés. Son peces gregarios, es conveniente"
READ.DATA  	1       ,"tener varios ejemplares viviendo juntos."
READ.DATA  	2	,"Es un pez muy resistente, ideal para acuarios. Tiene dos barbilones. Las hembras"
READ.DATA  	2       ,"son de mayor tamaño que los machos y más pálidas de color, como en general en todos"
READ.DATA  	2       ,"los barbos. Necesita acuarios de al menos 80 litros para poder nadar, con muchas"
READ.DATA  	2       ,"plantas de hoja dura. Es muy difícil de criar en acuarios. Se alimenta de alimento"
READ.DATA  	2       ,"vegetal, insectos y comida desecada."
READ.DATA  	3	,"Natural de Indonesia, Sumatra y Malasia . Ahí se reproducen los peces en los ríos y"
READ.DATA  	3       ,"en lagos. En Sumatra, vive principalmente en el altiplano, donde la temperatura del"
READ.DATA  	3       ,"agua varía de 19 a 25 grados ."
READ.DATA  	4	,"Originario de Sumatra y Borneo. Es un pez muy inquieto que puede llegar a producir"
READ.DATA  	4       ,"estrés al resto de peces del acuario. El acuario debe ser espacioso, de  al menos 60"
READ.DATA  	4       ,"litros, con abundancia de vegetación, pero con espacio libre para favorecer su libertad."
READ.DATA  	5	,"El barbo de cabeza purpúrea es un pez  un tanto agresivo, que puede atacar al resto"
READ.DATA  	5       ,"de peces de igual o menor tamaño. Por ello hacen falta acuarios amplios. Se caracteriza"
READ.DATA  	5       ,"por el color dorado con franjas verticales de color oscuro que recorren su cuerpo."
READ.DATA  	5       ,"Se caracteriza por el color dorado con franjas verticales de color oscuro que recorren"
READ.DATA  	5       ,"su cuerpo."
READ.DATA  	6	,"Pequeño barbo de un  color espectacular, puede molestar a los guppis y morderles las"
READ.DATA  	6       ,"colas.  Parece que los primeros ejemplares aparecieron en 1971 en un mercado de la"
READ.DATA  	6       ,"ciudad de Odessa. Su origen no esta claro, puede ser derivado de mezcla de varias"
READ.DATA  	6       ,"especies de barbos."
READ.DATA  	7	,"Nativo de Asia sudoriental. Necesita mucho espacio libre para la natación en grupo."
READ.DATA  	7       ,"Son vivaces, activos, hasta molestos con peces de hábitos tranquilos."
READ.DATA  	8	,"Es un pequeño barbo muy pacifico, originario de Asia, específicamente del suroeste"
READ.DATA  	8       ,"de China en el río Rojo. Puede convivir con practicamente cualquier otro pez, siempre"
READ.DATA  	8       ,"que se los mantengan en cardumen."
READ.DATA  	9	,"Este pez es una de las especies más comunes en los acuarios ya que es muy fácil de"
READ.DATA  	9       ,"mantener. Las 4 bandas verticales dibujadas en su cuerpo le dan su nombre, ya que"
READ.DATA  	9       ,"tetra significa cuatro en griego."
READ.DATA  	10	,"Asiático, procedente de Sri Lanka, ha sido introducido en Mejico y Colombia."
READ.DATA  	10      ,"Pueden incluirse en un acuario comunitario, ya que, a diferencia de otros barbos para"
READ.DATA  	10      ,"nada son peces problemáticos."
READ.DATA  	11	,"Es un pequeño pez tropical procedente del norte de  Birmania. Suele encontrarse a"
READ.DATA  	11      ,"menudo en tanques comunitarios, dado que es muy pacifico. Crece hasta un maximo de"
READ.DATA  	11      ,"3.5 cm."
READ.DATA  	12	,"Este pequeño pez, de forma estilizada que procede de la India oriental y Bangladesh,"
READ.DATA  	12      ,"vive normalmente en arroyos y debe mantenerse en cardumen. Presentan una coloración"
READ.DATA  	12      ,"coloracion dorada de base por la que atraviesan cuatro bandas longitudinales azules. "
READ.DATA  	12      ,"Cuenta con dos pares de barbillones. Se reproduce con mucha facilidad."
READ.DATA  	13	,"Naturales del Sudeste de Asia: Malasia occidental, Singapur, Sumatra, Tailandia."
READ.DATA  	13      ,"Peces de cardumen, muy activos. Es util colocar muchas plantas flotantes para que"
READ.DATA  	13      ,"se encuentren mas a gusto."
READ.DATA  	14	,"Natural de amplias zonas del sudeste de Asia.  Es muy activo pero a la vez pacífico,"
READ.DATA  	14      ,"no atacará a ningún otro pez o invertebrado que esté en su mismo acuario. Es ideal"
READ.DATA  	14      ,"para un acuario comunitario."
READ.DATA  	15	,"Natural de Tailandia, aunque parece que esta en peligro de extincion en su medio"
READ.DATA  	15      ,"natural. Solitario y muy territorial. Hostil hacia el resto de peces, pero especialmente"
READ.DATA  	15      ,"agresivo con los de su misma especie. Muy dificil de reproducir en acuarios comunitarios."
READ.DATA  	16	,"Este pez fue descubierto en la Montaña de la nube blanca al sur de China, por"
READ.DATA  	16      ,"un boy-scout, el siglo pasado. De ahí su nombre. Es activo, vive en cardumen, y se"
READ.DATA  	16      ,"reproduce con suma facilidad."
READ.DATA  	17	,"Es una especie endémica española,  de la cuenca del río Guadiana y sus afluentes, "
READ.DATA  	17      ,"ríos de corriente lenta y con mucha vegetación acuática. Especie protegida, vulnerable,"
READ.DATA  	17      ,"en peligro de extincion. "
READ.DATA  	18	,"Originario de Argentina y Paraguay. Vive en grandes cardumenes en los acuarios. Pez  "
READ.DATA  	18      ,"vivaz , delicado. Vive mas de 10 años."
READ.DATA  	19	,"El astyanax mexicanus habita en Norteamérica y Centroamérica, sobre todo en Mexico. "
READ.DATA  	19      ,"Se introdujo  artificialmente en Filipinas. Hasta hace bien poco, se identificaba con el"
READ.DATA  	19      ,"tetra ciego (Astyanax Jordani), que vive en cavernas de Mexico, pero la realidad es "
READ.DATA  	19      ,"que son especies diferentes."
READ.DATA  	20	,"Vive en zonas umbrías de aguas claras. Desde el  Río Orinoco en Venezuela a través de"
READ.DATA  	20      ,"Brasil hasta Colombia en aguas negras. En acuarios, en grandes "
READ.DATA  	20      ,"cardumenes con muchas plantas. Especie vistosa y pacifica."
READ.DATA  	21	,"Natural del rio Negro de Bolivia. Especie muy popular en acuariofilia. Con el tiempo "
READ.DATA  	21      ,"pierden el color negro y se vuelven completamente incoloros. Existen especies velo"
READ.DATA  	21      ,"con las aletas mas largas."
READ.DATA  	22	,"Natural de cerca de Buenos Aires, extendido ampliamente en ríos, arroyos, lagunas y"
READ.DATA  	22      ,"lagos naturales y artificiales. Un tanto pendenciero con otros peces pacificos. Hay una"
READ.DATA  	22      ,"variedad albina."
READ.DATA  	23	,"Especie que vive en cardumen muy decorativa y apreciada. Vive cómoda en acuarios"
READ.DATA  	23      ,"dotados de una iluminación tenue y compañeros tranquilos.Procede de la"
READ.DATA  	23      ,"Guayana, donde vive en aguas oscuras."
READ.DATA  	24	,"Vive en ríos de Guyana, Surinam, Guayana Francesa. Pez tranquilo, ideal para acuarios con"
READ.DATA  	24      ,"muchas plantas y pequeñas cuevas."
READ.DATA  	25	,"Natural de la cuenca del rio Orinoco y bajo Amazonas. Le gustan los acuarios de medianos"
READ.DATA  	25      ,"a grandes, al menos de  80 litros (ya que gustan de estar en grupo y tener mucho sitio "
READ.DATA  	25      ,"para nadar). Acuarios con luz tamizada por plantas flotantes y poca corriente."
READ.DATA  	26	,"Especie de cuerpo ancho, forma de rombo y aleta adiposa. Le gusta nadar en grupo. Dorso"
READ.DATA  	26      ,"de color verde amarronado o grisáceo. Es corriente que en periodos de excitación se "
READ.DATA  	26      ,"vuelva rojizo. Dificial de reproducir en acuario. Vive en la cuenca amazonica superior, Peru "
READ.DATA  	26      ,"y Colombia."
READ.DATA  	27	,"Vive en pequeños arroyos de la zona de Brasil central. Pez de cuerpo ancho y translúcido "
READ.DATA  	27      ,"de coloración amarilla. Tiene aleta adiposa. De ojos grandes cuya parte superior es de "
READ.DATA  	27      ,"color rojo vivo. Costados plateados y franja lateral poco perceptible. Aletas de coloración"
READ.DATA  	27      ,"amarillenta. "
READ.DATA  	28	,"Su hábitat natural radica en el Río San Francisco, en el Brasil central.  Es una de las "
READ.DATA  	28      ,"especies mas fáciles de mantener en acuarios, ya que no requieren de mucho cuidado.  Es"
READ.DATA  	28      ,"un pez pacifico de cardumen, mantenible también por parejas, es perfecto para el acuario"
READ.DATA  	28      ,"comunitario. Es frecuente que ocurran peleas entre los macho, pero no se causan daño."
READ.DATA  	29	,"Su nombre proviene de su color y aspecto."
READ.DATA  	29      ,"Especie pacífica de cardumen, tímida si se encuentra asociada con peces agresivos"
READ.DATA  	29      ,"y no muy activa. Le gusta esconderse entre las plantas"
READ.DATA  	30	,"De bonitos colores, necesita un acuario espacioso."
READ.DATA  	30      ,"Pertenece a los llamados tetras africanos."
READ.DATA  	30      ,"Pacifica y  no muy activa. Crece hasta los 14 cm."
READ.DATA  	31	,"Natural de algunos rios y lagos de Venezuela. La principal particularidad de esta especie "
READ.DATA  	31      ,"la presenta la cola caudal que en los machos se presenta más desarrollada. No es un pez"
READ.DATA  	31      ,"indicado para el principiante por su excesiva timidez. Es especialmente sensible a aguas "
READ.DATA  	31      ,"de poca calidad y a los nitritos."
READ.DATA  	32	,"Tambien es natural de Venezuela. Peces comunitarios, se adaptan muy bien al acuario, son"
READ.DATA  	32      ,"pacíficos, vivaces , pez de cardumen."
READ.DATA  	33	,"Vive en el norte de Colombia. Tiene una bella librea y gusta de nadar en aguas medias o "
READ.DATA  	33      ,"profundas. Posee el cuerpo relativamente alto y carece de la típica aleta adiposa "
READ.DATA  	33      ,"representativa de los Carácidos."
READ.DATA  	34	,"Natural del Amazonas superior (Perú, Brasil y Colombia). Vive en aguas negras, oscuras "
READ.DATA  	34      ,"debido a los taninos, pero limpias. Al ser un pez gregario  cuantos más individuos mejor, "
READ.DATA  	34      ,"se recomienda un acuario de no menos de 80 litros"
READ.DATA  	35	,"Vive en la zona norte de Sudamerica, Guayana, Surinam y Venezuela. Su cuerpo"
READ.DATA  	35      ,"es totalmente traslucido, unicamente presenta una pequeña manchita negra tras las branquias."
READ.DATA  	35      ,"La zona en la que se encuentras los organos vitales, parece estar recubierta por una."
READ.DATA  	35     ,"membrana plateada."
READ.DATA  	36	,"Vive en toda la Amazonia y en el norte hasta Guayana.  Especie relativamente pacífica que "
READ.DATA  	36      ,"se caracteriza por su cuerpo alargado y su peculiar posición a la hora de nadar de forma "
READ.DATA  	36      ,"oblicua con la cabeza baja, al igual que otras especies del género."
READ.DATA  	37	,"Vive en la parte inferior del Amazonas. Las parejas desovan en la parte inferior de la hojas"
READ.DATA  	37      ,"o raices qe cuelgan de la superficie. La distancia de la hoja o raiz y la superficie"
READ.DATA  	37      ,"del agua no debe ser mayor de 6cm. Depositan alli los huevos, que se pegan a "
READ.DATA  	37      ,"las hojas, y el macho los rocia con agua para mantenerlos humedos."
READ.DATA  	38	,"Se encuentra en ríos de cursos lentos y en pequeños lagos de Brasil, Guayana y Perú."
READ.DATA  	38      ,"Especie muy popular por su pacífico comportamiento, idónea para el acuario comunitario."
READ.DATA  	39	,"Viven en casi toda Sudamérica, en ríos y arroyos con abundante vegetación y aguas "
READ.DATA  	39      ,"lentas. Se trata de un pez de cardumen que se encontrará cómodo en grupo, en un acuario  "
READ.DATA  	39      ,"sombreado con poca luz y plantas flotantes. Puede saltar fuera del acuario."
READ.DATA  	40	,"Vive en el Amazonas y sus afluentes. Son peces ovalados con una gruesa cabeza"
READ.DATA  	40      ,"dotada de una poderosa mandíbula con dientes triangulados muy afilados que usan para"
READ.DATA  	40      ,"triturar la carne de sus presas."
READ.DATA  	41	,"Habita en toda la cuenca del Amazonas, en zonas tranquilas y protegidas, de corriente débil."
READ.DATA  	41      ,"Es una especie de costumbres pacíficas, pero agresiva durante el desove. Son "
READ.DATA  	41      ,"monógamos y cuidan de los huevos y de los alevines durante mucho tiempo. Las"
READ.DATA  	41      ,"parejas jovenes se comen a menudo los huevos de sus primeros desoves. Una"
READ.DATA  	41     ," hembra madura pone hasta 1.000 huevos en una piedra plana."
READ.DATA  	42	,"Originario de Guyana. Los machos un poco mas grandes que las hembras. Necesita "
READ.DATA  	42      ,"cuevas y  escondites, y espacio libre para natación. Su reproduccion es la "
READ.DATA  	42      ,"típica de los cíclidos cuidadores de desoves."
READ.DATA  	43	,"Natural de  Venezuela, Colombia y Panama. La coloración de Aequidens pulcher es  "
READ.DATA  	43      ,"espectacular: el cuerpo es de color gris llegando al negro, con bandas verticales, en la"
READ.DATA  	43      ,"cara tiene unas manchas fosforescentes de color azul. En epoca de celo la coloración es"
READ.DATA  	43      ,"muy intensa. Desovan sobre piedras planas, troncos, hojas grandes de plantas o incluso"
READ.DATA  	43      ,"en el cristal del acuario. La puesta la defienden con bastante celo."
READ.DATA  	44	,"Vive en casi toda la Amazonia. La hembra es más pequeña que el macho y sus "
READ.DATA  	44      ,"colores ienen menos contraste. Los machos tienen las aletas dorsal y caudal más."
READ.DATA  	44      ,"alargadas. Es pez de comportamiento pacífico, aunque algo territorial como la gran"
READ.DATA  	44      ,"mayoría de cíclidos. Se puede asociar con peces de cardumen de nivel superior."
READ.DATA  	45	,"Vive en los rios Orinoco, Amazonas, Paraná y Negro. Es una especie pacífica pero "
READ.DATA  	45      ,"extremadamente territorial, fácilmente se apropia de la totalidad del acuario, aceptando"
READ.DATA  	45      ,"a su lado sólo a su pareja. En estado libre es una especie piscívora. En condiciones de"
READ.DATA  	45      ,"acuario es omnívora pero debe ofrecérsele una alta dosis de carne o incluso "
READ.DATA  	45      ,"peces pequeños, su falta produce un decaimiento en la extraordinaria coloración del"
READ.DATA  	45      ,"animal. Llega a conocer a sus dueños y comer de su mano."
READ.DATA  	46	,"Vive en la vertiente atlantica de Mexico, sobre todo en la peninsula del Yucatán. "
READ.DATA  	46      ,"Cuerpo gris azulado con iridiscencias violetas. Tiene varias bandas verticales"
READ.DATA  	46      ,"negras mas o menos visibles y una estría horizontal que recorre todo su cuerpo. "
READ.DATA  	46      ,"Tiene un  bonito color escarlata en la parte "
READ.DATA  	46      ,"baja del cuerpo, desde la cabeza al bajo vientre. Se reproduce con mucha facilidad "
READ.DATA  	46      ,"y cuida a sus alevines con mucha responsabilidad, como la mayoria de los ciclidos."
READ.DATA  	47	,"Endémico de la zona norte del Lago Tanganika, en Africa.Puede intentarse mantener con,"
READ.DATA  	47      ,"plantas aunque probablemente las arrancará. Mantener con troncos huecos, rocas"
READ.DATA  	47      ,"formando cuevas y raices o lajas para marcar límites territoriales. Solitario y territorial, "
READ.DATA  	47      ,"establece su territorio tomando una cueva en las rocas como centro del mismo. Es agresivo"
READ.DATA  	47      ,"pero esa agresividad se diluye en un acuario de grandes dimensiones con espacio"
READ.DATA  	47      ,"suficiente para que cada macho establezca un territorio. "
READ.DATA  	48	,"Endémico del Lago Malawi. Acuario con ambientación típica para peces del Lago Malawi con"
READ.DATA  	48      ,"rocas y troncos. Muy territorial. Es un incubador bucal. Incuban los huevos en la boca "
READ.DATA  	48     ," y cuidan las crías."
READ.DATA  	49	,"De America Central: Nicaragua, Costa Rica y Mexico. Cuida muy bien de sus crías, y es de "
READ.DATA  	49      ,"facil reproducción.  Se cree que los cromosomas de este pez se usaron para la creación "
READ.DATA  	49      ,"del extraño y conocido Bloody parrot."
READ.DATA  	50	,"Costa atlántica de Nicaragua, en los lagos Nicaragua y Managua.  La mayoría de los "
READ.DATA  	50      ,"ejemplares que se encuentran en la naturaleza son de color gris verdoso. Sin embargo, "
READ.DATA  	50      ,"hay algunos de ellos que son amarillos, blancos, rojos o anaranjados con brillantes colores. "
READ.DATA  	50      ,"La forma roja con labios gruesos es muy escasa en la naturaleza y muy demandada en"
READ.DATA  	50      ," la acuariofilia."
READ.DATA  	51	,"Vive desde Honduras hasta Costa Rica. Bello pez de color amarillo dorado con marcas"
READ.DATA  	51      ,"negras. Puede alcanzar un tamaño de 40 cm. El Managüense tiene una "
READ.DATA  	51      ,"reputacion de ser muy agresivo, y muy territorial. Las parejas deben"
READ.DATA  	51     ,"mantenerse solas. Protegen a sus crias con tesón."
READ.DATA  	52	,"Vive en toda Centroamerica. Pacifico cuando no esta en celo, los padres ponen los"
READ.DATA  	52      ,"huevos en superficies planas o en hojas (como en la foto). Defienden a las crias a"
READ.DATA  	52      ,"ultranza. Muy facil de reproducir en acuarios."
READ.DATA  	53	,"Vive en toda centroamerica. Se llama asi en honor al famoso boxeador de los años 20. Es "
READ.DATA  	53      ,"agresivo y territorial. Se dice que tiene personalidad propia."
READ.DATA  	54	,"Natural de Nicaragua y Costa Rica, es uno de los cuclidos americanos mas agresivos, sobre"
READ.DATA  	54      ,"todo cuando estan en celo o criando. Ahuyentara y amedrentara a peces mucho"
READ.DATA  	54     ,"mayores que él."
READ.DATA  	55	,"Oriundo de Venezuela, Colombia y Brasil en aguas de corriente lenta. Uno de los ciclidos"
READ.DATA  	55      ,"mas pequeños y tranquilos que hay, es tambien uno de los mas bonitos. Delicado,"
READ.DATA  	55      ,"necesita gua muy pura y controlada. "
READ.DATA  	56	,"El kribensis es un ciclido natural de África, en la zona occidental del sur de Nigeria."
READ.DATA  	56      ,"En su estado silvestre esta especie tambien vive en aguas salobres. Es un pez precioso,"
READ.DATA  	56      ,"muy fácil de reproducir en acuario y muy amante de su prole."
READ.DATA  	57	,"Natural de la cuenca del rio Amazona, en Peru, Brasil y Colombia. Es uno de los peces mas"
READ.DATA  	57      ,"elegantes que podemos tener en casa. Dificil de reproducir, no debe mantenerse con"
READ.DATA  	57      ,"peces mucho mas pequeños que él."
READ.DATA  	58	,"Considerado el rey del acuario, es natural de algunas zonas del rio Amazonas y rio"
READ.DATA  	58      ,"Negro. Dificil de mantener en acuario, ya que necesita condiciones de agua un tanto"
READ.DATA  	58      ,"particulares. Sin embargo se dice que en el Amazonas se le encuentra a veces en aguas"
READ.DATA  	58      ,"contaminadas. Es también muy dificil de reproducir en acuario. Los alevines durante"
READ.DATA  	58      ,"primeros dias de vida se alimentan de una mucosa que segregan los padres por la piel."
READ.DATA  	59	,"Es un pez natural de los arrozales de extremo oriente. El macho es mucho mas vistoso"
READ.DATA  	59      ,"que la hembra, y muy agresivo con otros machos. Pelean a muerte si se encuentra. Tienen"
READ.DATA  	59      ,"un órgano especial en la cabeza por el que toman aire de la superficie para"
READ.DATA  	59      ,"respirar. Construyen un nudo de burbujas en la superficie para poner los huevos"
READ.DATA  	60	,"Habitan en el noroeste de la India,en el rio Ganges y otros. Se debe asociar"
READ.DATA  	60      ,"exclusivamente con peces de comportamiento sosegado, ya que su carácter es pacífico"
READ.DATA  	60      ,"y súmamente tímido. Puede reproducirse en acuario no sin cierta dificultad."
READ.DATA  	61	,"Habita en Borneo, Sumatra, Tailandia y Java.Se le conoce popularmente como besucon"
READ.DATA  	61      ,"ya que con frecuencia estos peces se besan en la boca y en la cara, tambien besan"
READ.DATA  	61      ,"las plantas y los cristales. Se desconoce el motivo."
READ.DATA  	62	,"Habita en China oriental, Taiwan y Vietnam. No tan conocido como el Betta, tiene su"
READ.DATA  	62      ,"mismo carácter. Fue uno de los primeros peces de acuario."
READ.DATA  	63	,"Es un pequeño killi africano, no mayor de cinco centímetros. Especie ovípara , de gran"
READ.DATA  	63      ,"adaptabilidad y resistencia. Originaria del África Occidental, se encuentra en pequeños "
READ.DATA  	63      ,"cauces de agua de los bosques de Gabón, Camerún y Congo, siempre en zonas de  "
READ.DATA  	63    ,"abundante vegetación. "
READ.DATA  	64	,"Viven libres en los cursos fluviales de la India, Malasia, Ceylan, Tailandia, etc. "
READ.DATA  	64      ,"Es un bonito pez de no mas de 8 cm, tranquilo, aunque totalmente carnivoro. "
READ.DATA  	64      ,"Necesita alimentarse fundamentalmente de gusanos."
READ.DATA  	65	,"Habita en Argentina, en pequeños arroyos de escasa corriente. Es un precioso pez, que"
READ.DATA  	65      ,"suele reñir con sus congeneres, tambien con las hembras."
READ.DATA  	66	,"Vive en Florida y en Yucatan (Mexico). Es un pez , como otros killis, semianual, cuyos"
READ.DATA  	66      ,"huevos deben sufrir un periodo de desecacion. Durante este periodo los aficionados se"
READ.DATA  	66      ,"intercambian los huevos de los killis por correo postal."
READ.DATA  	67	,"Especie endemica española, protegida y en grave peligro de extincion. Habita en algunas"
READ.DATA  	67      ,"zonas de la cuenca mediterranea. "
READ.DATA  	68	,"Al igual que el samaruc, endemico de algunas zonas de la costa mediterranea,  y una de"
READ.DATA  	68      ,"las especies de vertebrados mas amenazadas y en mas grave peligro de extincion."
READ.DATA  	68      ,"Vive en charcas y salinas. "
READ.DATA  	69	,"Especie similar al fartet pero de otra linea evolutiva, por lo tanto especie diferente. Igual"
READ.DATA  	69      ,"de protegida y con el mismo peligro de extincion. Habita en la cuenca del rio "
READ.DATA  	69     ,"Guadalquivir."
READ.DATA  	70	,"Es una de las coridoras mas populares, que habita en toda la cuenca del rio Amazonas."
READ.DATA  	71	,"Vive en afluentes del rio Negro en Brasil, en aguas claras. Al igual que el resto de las"
READ.DATA  	71      ,"coridoras, habitualmente se la encuentra en el fondo, alimentandose de restos de"
READ.DATA  	71      ,"comida (pero no de basura). Desova en grupos."
READ.DATA  	72	,"Como el resto de las coridoras es un pez gregario que conviene mantener en grup.o "
READ.DATA  	72      ,"Vive en America del Sur, desde el norte hasta el Rio de la Plata."
READ.DATA  	73	,"Vive en el centro de Brasil, Paraguay, Uruguay y Bolivia. Es una de las coridoras mas"
READ.DATA  	73      ,"populares."
READ.DATA  	74	,"Vive en multitud de rios de America del Sur. Se alimenta basicamente de algas, en los"
READ.DATA  	74      ,"acuarios se fija a los cristales al tener una especie de ventosa en la boca, donde "
READ.DATA  	74      ,"ramonea algas. Puede alcanzar facilmente los 30 cm de longitud."
READ.DATA  	75	,"Vive en Bolivia, Peru, Paraguay y Argentina. Se alimenta exclusivamente de algas vedes,"
READ.DATA  	75      ,"por lo que si no hay suficientes en el acuario, deberemos suministrarle espinacas "
READ.DATA  	75 ,"lechuga."
READ.DATA  	76	,"Vive en los rios Amazonas y Orinoco. Es inconfundible al tener un especie de"
READ.DATA  	76      ,"ramificaciones en la cabeza. Se reproduce en acuario con facilidad."
READ.DATA  	77	,"Habita en rios de aguas turbias de Malasia, Borneo, India, Java y Thailandia. Este pez es"
READ.DATA  	77      ,"extraordinario, ya que es practicamente transparente. Vive en cardúmenes."
READ.DATA  	78	,"Esta locha es originaria de Borneo, Sumatra e Indonesia. Pasa casi todo el tiempo"
READ.DATA  	78      ,"escondida en recovecos del fondo. Casi imposible de reproducir en acuario."
READ.DATA  	79	,"La locha payaso es originaria de Sumatra y Borneo. Este precioso pez es muy pacifico,"
READ.DATA  	79      ,"vive en cardumenes, y alcanza un buen tamaño en el acuario."
READ.DATA  	80	,"Oriundo del norte de Mexico, texas y Florida. Al igual que el resto de los poecilidos, es un"
READ.DATA  	80      ,"pez viviparo, las hembras dan a luz a las crias."
READ.DATA  	81	,"Quizas el mas popular de los peces de acuario. El guppy, o pez millon, se reproduce"
READ.DATA  	81      ,"con muchisima facilidad, presenta una amplisima variedad de colores, de formas de,"
READ.DATA  	81      ,"aletas, de variedades,? Especialmente los machos, mucho mas coloridos y vistosos"
READ.DATA  	81      ," que las hembras."
READ.DATA  	82	,"El macho tiene un apendice en la aleta caudal, de ahí su nombre. Como otros poecilidos,"
READ.DATA  	82      ,"una sola inseminacion del macho sirve para varias gestaciones de las hembras."
READ.DATA  	83	,"Al igual que el guppy, es muy apreciado u existen multitud de variedades: coral, Mickey"
READ.DATA  	83      ,"Mouse, piña, tuxedó, etc"
READ.DATA  	84	,"Existen multitud de variedades de esta especie. En la foto la variedad papagayo"
READ.DATA  	84      ,"Se puede ver el gonopodio, especie de organo sexual masculino de esta especie ovipara."
READ.DATA  	85	,"Vivíparo que alcanza los 15 cm y vive desde Mexico hasta Nicaragua. Es un feroz carnivoro."
READ.DATA  	86	,"Nativa de sur de Estados Unidos, es una pez gran comedor de larvas de mosquito. Se"
READ.DATA  	86      ,"introdujo el siglo pasado en muchos paises, entre ellos España, para combatir el "
READ.DATA  	86      ,"paludismo. Ahora se le encuentra en practicamente cualquier habitat de aguas dulces "
READ.DATA  	86      ,"o salobres, siendo una especie invasora."
READ.DATA  	87	,"Es uno de los peces mas pequeños del mundo. Originaria de Estados Unidos. Los machos "
READ.DATA  	87      ,"presentan un gonopodio muy desproporcionado, en comparacion al de otros poecilidos "
READ.DATA  	87      ,"como el guppy."
READ.DATA  	88	,"Originario del Nilo (Chad, Niger), llega a alcanzar los 50 cm de longitud, muchisimo menos"
READ.DATA  	88      ,"en acuario. Habitualmente nada en posicion invertida."
READ.DATA  	89	,"Este siluro vive en todos los rios de la cuenca del rio Congo, es nocturno y agresivo con"
READ.DATA  	89      ,"otras especies de peces-gato."
READ.DATA  	90	,"Vive en  Guayana, Venezuela y Amazonia  brasileña. Alcanza los 30 cm. Puede convivir con "
READ.DATA  	90      ,"otros peces de acuerdo a su tamaño y exigencias de agua.  Pez extremadamente voraz, "
READ.DATA  	90      ,"que se encuentra siempre por la superficie, con los ojos incluso en el exterior del agua"
READ.DATA  	90      ,"(los tiene divididos en 4 zonas, para ver el interior y el exterior del agua simultaneamente)."
READ.DATA  	90      ,"Muy dificil de reproducir."
READ.DATA  	91	,"Natural de la cuenca del rio Ameca, en Mexico. Se cree casi extinguido en la naturaleza."
READ.DATA  	92	,"Este pez solo se encuentra en la naturaleza en una charca llamada Ojo de agua"
READ.DATA  	92      ,"de las mujeres, en la localidad de El Toboso,en el estado de Durango, México. Los  "
READ.DATA  	92      ,"Goodeidos son vivíparos, pero no emparentados con los poecilidos."
READ.DATA  	93	,"Son unos peces poco comercializados, pero de mucho interes. Bastante pacificos, aunque"
READ.DATA  	93      ,"pueden morder las colas de otros peces."
READ.DATA  	94	,"Originario de America del Norte (desde Canada a Carolina del Sur), ha sido introducido e"
READ.DATA  	94      ,"otros paises, como España, donde es una especie invasora. Vive en grupos y es muy voraz."
READ.DATA  	95	,"Vive en zonas costeras del Ocenano Indico. Lanza un fuerte chorro de agua a insectos que,
READ.DATA  	95      ,"están posados en plantas por encima de la superficie del agua, haciendolos caer para"
READ.DATA  	95      ," comérselos."
READ.DATA  	96	,"En general, son poco exigentes en lo que respecta a las condiciones del agua y a la luz,  lo"
READ.DATA  	96      ,"que hace de esta planta idonea  para los que se inician en la acuariofilia. Llegan a medir "
READ.DATA  	96      ,"hasta 25 cm de longitud y 15 cm de altura. Son originarias del sudeste asiatico."
READ.DATA  	97	,"Planta de zonas pantanosas de America del Sur, de pequeño tamaño de color rojo. "
READ.DATA  	97      ,"Se reproduce por esquejes."
READ.DATA  	98	,"Muy resistente, ideal para acuarios con mucha luz. Natural del sudeste de Asia."
READ.DATA  	99	,"Originaria del Amazonas, sus hojas alcanzan los 20 cm de longitud. Ideal para"
READ.DATA  	99,"acuarios comunitarios."
READ.DATA  	100	,"Es una muy popular planta de rizoma, tallo horizontal de donde salen las hojas y las raices."
READ.DATA  	100     ,"Estas se adhieren a rocas y objetos con facilidad."
READ.DATA  	101	,"Planta muy poco exigente con las condiciones de agua y que se reproduce"
READ.DATA  	101,"con mucha facilidad."
READ.DATA  	102	,"Planta originaria de Sierra Leona y Guinea. De hojas anchas, es un tanto delicada. Se"
READ.DATA  	102     ,"multiplica por rizomas."
READ.DATA  	103	,"Es una especie de hortiga acuatica natural de Argentina. Puede llegar a alcanzar mas"
READ.DATA  	103     ,"de un metro de altura."
READ.DATA  	104      ,"Planta muy resistente y con una gran capacidad de adaptación, natural de amplias "
READ.DATA  	104     ,"zonas subtropicales y templadas. Compite muy bien con las algas."
READ.DATA  	105	,"El musgo de Java es una de las plantas mas sencillas y recomendadas para nuevos"
READ.DATA  	105     ,"acuaristas. No precisa ningun requerimiento especial en cuanto a luz y sustrato."
READ.DATA  	105     ,"Se usa para tapizar troncos y piedras, creando atmosferas mucho mas naturales."
READ.DATA  	106	,"Vive en los cursos y lagos de las regiones templadas y cálidas de todo el mundo. Introducida"
READ.DATA  	106     ,"artificialmente en los hábitat acuáticos de otras zonas menos benignas, incluso "
READ.DATA  	106     ,"en mplias zonas de España. Existen diferentes variedades,siendo"
READ.DATA  	106     ,"una planta robusta, tapizante, y que se adapta muy bien a cualquier acuario."
READ.DATA  	107, "Los acuarios son muy relajantes para todas las personas que los observan. Gustan a "
READ.DATA  	107, " todo el mundo, tranquilizan, son instructivos y variables. Cambian con el tiempo. No son "
READ.DATA  	107, "  caros ni costosos de mantener, apenas roban un poco de tiempo para su cuidado. "
READ.DATA  	108,"De no mucho tamaño, ideal para iniciarse en la acuariofilia. Este acuario está habitado"
READ.DATA  	108," por vivíparos: plattys, guppys, xifos. Ideales para principiantes, de fácil reproducción."
READ.DATA  	108,"Nunca nos defraudarán. Y no dejarán de sorprendernos. "
READ.DATA  	109,"La unión de peces y plantas es lo mas buscado en la acuariofilia. En este acuario"
READ.DATA  	109," tenemos peces discos, en buena combinación con plantas robustas. Los discos son "
READ.DATA  	109,"  difíciles de reproducir, pero por contra se les conoce por el rey del acuario, por su"
READ.DATA  	109, " majestuosidad"
READ.DATA  	110, "Los acuarios llamados holandeses se centran en las plantas, mas que en los peces"
READ.DATA  	110, " que contienen. En la foto vemos solo plantas, y troncos sumergidos. Y ningún"
READ.DATA  	110, " pez. No es necesario tener peces para disfrutar de la vida acuática."
READ.DATA  	111,"Los acuarios requieren muchos menos cuidados que los que se piensa en general,"
READ.DATA  	111,"son fáciles de mantener. Aquí tenemos un acuario con muchas plantas robustas, "
READ.DATA  	111,"poblado también con algunos tetras neón y vivíparos. Una buena combinación. "
READ.DATA  	112,"Un bello acuario, centrado en las plantas. De tipo holandés. Las plantas prosperan"
READ.DATA  	112," mejor si están rodeadas de peces, pero no es necesario. Existen sistemas de adición"
READ.DATA  	112," de CO2 para ayudar al mantenimiento de acuarios de este tipo. "
READ.DATA  	113,"Otro ejemplo de acuario, con gran abundancia de peces. En este caso barbos"
READ.DATA  	113,"y guramis, no es una combinación habitual pero también funciona perfectamente. Es "
READ.DATA  	113," placentero verlos nadar pacíficamente, sin riñas y disputas entre ellos. Solo es"
READ.DATA  	113,"  cuestión de probar cual es la combinación que mejor se adapta a nuestro hogar. "
READ.DATA  	114,"  Este acuario cabe en cualquier rincón"
READ.DATA  	115,"  Fascinante y relajante acuario"
READ.DATA  	116,"  Acuarios fascinantes en casa."
