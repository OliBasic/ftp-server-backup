DO
 TGET typed$, "What do you want factored (enter a negative to exit, zero to clear)? "
 x=VAL(typed$)
 IF x<=0 THEN GOTO loop_end

 i=0

 PRINT FORMAT$("##############",x)
 WHILE x/2 = FLOOR(x/2)
  PRINT FORMAT$("########",2);
  i=MOD(i+1,6)
  IF i=0 THEN PRINT " "
  x=x/2
 REPEAT

 n=3
 WHILE n^2 <= x
  WHILE x/n = FLOOR(x/n)
   PRINT FORMAT$("########", n);
   i=MOD(i+1,6)
   IF i=0 THEN PRINT " "
   x=x/n
  REPEAT
  n=n+2
 REPEAT

 IF x>1 THEN PRINT FORMAT$("########",x);

 PRINT " "
 loop_end:
 IF x=0 THEN CLS
UNTIL x<0
END
