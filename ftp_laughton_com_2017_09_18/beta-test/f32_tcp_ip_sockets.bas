!!

******  READ!  *******

Sample program demonstrating
TCP/IP sockets. The program 
has two parts: a server side
and a client side. Both sides
need to be running on different
Android devices.

The Server will wait for a Client
to connect to it. When the
connection is made, the Server
will wait for a message from
the Client. Once the message
has been recieved, the Server
will send a message back
to the Client.

Some devices/networks do not
allow smart phone servers onto
their data network.

In that case, the server must be
inside a LAN.

LAN = Local Area Network (WiFi)
WAN = Wide Area Network (Internet)

If the Client is on the same LAN
as the Server, the Client can 
connect directly to the Server
using the Server's LAN IP.

If the Client is outside of the
Server LAN then the Client must
must connect using the Server's
WAN IP.

It will be necessary to program
the Server's LAN router to pass
a specific port through from the
WAN to Server's LAN IP

!!

array.load type$[], "Server", "Client"
msg$ = "Select TCP/IP socket type"
select type, type$[], msg$
if type = 0
 "Thanks for playing"
 end
elseif type = 2
 goto doClient:
endif

!***********  Server Demo  **************

input "Enter the port number", port, 1080

socket.myip ip$
print "LAN IP: " + ip$
graburl ip$, "http://automation.whatismyip.com/n09230945.asp"
print "WAN IP: " + ip$

! Create the server on the specified port
socket.server.create port

newConnection:

! Connect to the next Client
! and print the Client IP
Print "Waiting for client connect"
socket.server.connect
socket.server.client.ip ip$
print "Connected to ";ip$

! Connected to a Client
! Wait for Client to send a message
! or time out after 10 seconds

maxclock = clock() + 10000
do
 socket.server.read.ready flag
 if clock() > maxclock 
   print "Read time out"
   end
 endif
until flag

! Message received. Read it.
! Print it

socket.server.read.line line$
print line$

! Send a message back to client

socket.server.write.line "Server to client message"

! Finished this protocol
! Disconnect from Client

socket.server.disconnect
print "Disconnected from client"

! Loop to get the next Client

goto newConnection

! ****************** Client Demo ****************

doClient:

input "Enter the connect-to IP", ip$
input "Enter the port number", port, 1080

! Connect to the specified IP on the
! specified Port

clientAgain:

socket.client.connect ip$, port
print "Connected"

! When the connection is established,
! send the server a message

socket.client.write.line "Client to server message"

! and then wait for Server to respond
! or time out after 10 seconds

maxclock = clock() + 10000
do
 socket.client.read.ready flag
 if clock() > maxclock 
   print "Read time out"
   end
 endif
until flag

! Server has sent message.
! Read it. Print it.

socket.client.read.line line$
print line$

! Close the client

socket.client.close
print "Disconnected from server"

input "Connect again? Y or N", again$, "Y"
if again$ = "Y" then goto clientAgain

end


 
