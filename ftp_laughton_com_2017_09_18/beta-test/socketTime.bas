! Demo TCP Client by getting
! current time from Time Server
! in Mt. View, CA

! Other time servers can be found at
! http://tf.nist.gov/tf-cgi/servers.cgi

socket.client.connect "207.200.81.113", 13

! Wait for first line (a blank line)
! or time out after 10 seconds

maxclock = clock() + 10000
do
 socket.client.read.ready flag
 if clock() > maxclock 
   print "Read time out"
   end
 endif
until flag

! Read the blank line and print it

socket.client.read.line line$
print line$

! Wait for the time data line
! Read it. Print it.

socket.client.read.line line$
print line$

! Protocol done. Close Client

socket.client.close

end
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
