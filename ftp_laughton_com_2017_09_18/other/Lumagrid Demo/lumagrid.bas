
FN.DEF n()
 x=RND()*3+1
 IF RND()<0.5 THEN x=-x
 FN.RTN x
FN.END

FN.DEF f$(flag)
 IF flag THEN FN.RTN "on" ELSE FN.RTN "off"
FN.END

rf=1:gf=1:bf=1

GR.OPEN 255,0,0,0,0,0
WAKELOCK 2
GR.SCREEN w,h
bx=FLOOR(w/13):hh=110
wx=FLOOR(w/bx):wy=FLOOR(h/bx)
DIM grid[wx,wy]
GR.SET.ANTIALIAS 0
GR.COLOR 255,255,255,255
d=0.1
FOR y=1 TO wy
 FOR x=1 TO wx
  GR.RECT grid[x,y],(x-1)*bx,(y-1)*bx,x*bx,y*bx
 NEXT
NEXT
GR.RENDER
again:
ag=0

LET xb=w/n()
LET yb=-w/n()
LET xr=w/n()
LET yr=w/n()
LET xg=w/n()
LET yg=w/n()
LET xbb=w/n()
LET ybb=-w/n()
LET xrr=w/n()
LET yrr=w/n()
LET xgg=w/n()
LET ygg=w/n()


d1=RND()*d
d2=RND()*d
d3=RND()*d
d4=RND()*d
d5=RND()*d
d5=RND()*d

loop:
st=CLOCK():t++
FOR y=1 TO wy
 FOR x=1 TO wx
  LET xc=x/w:LET yc=y/h
  LET bb=SIN(xb*xc-yb*yc-d1*t)+1
!LET bb+=0.5*SIN(xbb*xc-ybb*yc-d6*t)+1.5
  LET rr=SIN(xr*xc-yr*yc-d2*t)+1
!LET rr+=0.5*SIN(xrr*xc-yrr*yc-d4*t)+1.5
  LET gg=SIN(xg*xc+yg*yc-d3*t)+1
!LET gg+=0.5*SIn(xgg*xc+ygg*yc-d5*t)+1.5
  GR.COLOR 255,hh*rr*rf,hh*gg*gf,hh*bb*bf
  GR.PAINT.GET p:GR.MODIFY grid[x,y],"paint",p
 NEXT
NEXT

GR.RENDER

GR.TOUCH tt,xx,yy
IF tt THEN GOSUB change
IF ag THEN GOTO again 
GOTO loop

change:
LIST.CREATE s,menu
LIST.ADD menu,"new","speed","red:"+f$(rf),"green:"+f$(gf),"blue:"+f$(bf),"quit"

DIALOG.SELECT s,menu
SW.BEGIN s
 SW.CASE 6
  gr.close
  EXIT
  SW.BREAK
 SW.CASE 2
  DO
   INPUT "enter speed (0.01-1)",d,d,can
  UNTIL d>=0.01 & d <=1
  d1=RND()*d
  d2=RND()*d
  d3=RND()*d
  SW.BREAK
 SW.CASE 1
  ag=1 
  SW.BREAK
 SW.CASE 3
  rf=!rf
  SW.BREAK
 SW.CASE 4
  gf=!gf
  SW.BREAK
 SW.CASE 5
  bf=!bf
  SW.BREAK
SW.END

RETURN
