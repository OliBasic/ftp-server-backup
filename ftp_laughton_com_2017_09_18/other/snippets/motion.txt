! untested motion detector

fn.def md_cell(image, mode, x1,y1,x2,y2,pixel_step)
  x2=floor((x2-x1)/pixel_step)*pixel_step+x1
  y2=floor((y2-y1)/piyel_step)*piyel_step+y1
  for y = y1 to y2 step pixel_step
  for x = x1 to x2 step pixel_step
	Gr.get.bmpixel IMAGE, x, y, a, r, g, b
	v=0
  	if mode = 0 then v = R * -.168736 + G * -.331264 + B *  .500000 + 128
	if mode = 1 then v = R *  .299000 + G *  .587000 + B *  .114000
	if mode = 2 then V = R *  .500000 + G * -.418688 + B * -.081312 + 128
	if mode = 3 then v = r
	if mode = 4 then v = g
	if mode = 5 then v = b
	sum+=v
  next x
  next y
  fn.rtn sum
fn.end

fn.def md_detect_motion( image, cols,rows,mode,threshold,region,c1,r1,c2,r2,pixel_step,arr[])
!
!  -- Motion detection variables
!  -- a=6		 -- columns to split picture into 
!  -- b=6		 -- rows to split picture into 
!  c=1			-- measure mode (Y,U,V R,G,B) U=0, Y=1, V=2, R=3, G=4, B=5
!  d=300000	-- timeout (mSec) 
!  e=200		-- comparison interval (msec) - less than 100 will slow down other CHDK functions
!  -- f=5		   -- threshold (difference in cell to trigger detection)
!  -- g=1		 -- draw grid (0=no, 1=yes)	  
!  h=0			-- not used in LUA - in uBasic is the variable that gets loaded with the number of cells with motion detected
!  -- i=0		   -- region masking mode: 0=no regions, 1=include, 2=exclude
!  -- j=0		   --	   first column
!  -- k=0		   --	   first row
!  -- l=0		   --	   last column
!  -- m=0		   --	   last row
!  n=0			-- optional parameters	(1=shoot immediate)
!  o=2			-- pixel step
!  p=0			-- triggering delay (msec) 
!  print("Typical Motion Threshold")
!  print("12=Sun 24=Cloudy 36=Dawn")
!
	if region = 0 then
		cols = 1
		rows = 1
		region = 1
	endif
	region = and(region,1)
	r=rows
	c=cols
	cols+=0.0000000001
	rows+=0.0000000001
	gr.bitmap.size image,w,h
	w_=floor(w/cols)
	h_=floor(h/rows)
	for y = 0 to floor(rows)-1
	for x = 0 to floor(cols)-1
		a = 0
		b = 0
		if y >= r1 & y <= r2 then a=1
		if x >= c1 & x <= c2 then b=1
		a=and(a,b)
		b=!region
		region=xor(a,b)
		if !region then f_n.continue
		v=md_cell(image,mode,floor(w/cols*x)+1,floor(h/rows*y)+1,floor(w/cols*x)+w_,floor(h/rows*y)+h_,pixel_step) 
		if threshold < abs(arr[y*r+x]-v) then ret = 1
		arr[y*r+x]=v
	next x
	next y
	fn.rtn ret
fn.end