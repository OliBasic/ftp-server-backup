console.title "Snippets Index Updater (nisma)"

fn.def html$(txt$)
	txt$=replace$(txt$,"<","&lt;")
	txt$=replace$(txt$,">","&gt;")
	do 
		i=is_in("[",txt$)
		if i > 0 then
			j=is_in("]",txt$,i)
			link$=mid$(txt$,i+1,j-i-1)
			first$=mid$(txt$,1,i-1)
			last$=mid$(txt$,j+1)
			txt$=first$ + "<a href=\"" + link$  + "\">" + link$ + "</a>" + last$
		endif
	until i <> 0
	fn.rtn replace$(txt$,"\n","<br>")
fn.end

err$ = "unable to connect to snippet server"

ftp.open "ftp.laughton.com", 21, "basic", "basic"
ftp.cd "other/snippets"


print "Connected"

ftp.dir file_list
list.size file_list,size
 
err$ = "unable to process 0index.txt file"

ftp.get 			"0readme.txt" , "0index.txt"
grabfile header$,	"0index.txt"
text.open w,fp,		"0index.txt"
text.open w,fd,		"0index.htm"

print "loaded header file"

dim arr$[5000]
text.writeln fp,	header$
text.writeln fd,	"<html><title>Snippet index</title><body>"
text.writeln fd,	"<center><h2>Snippet collection</h2></center><hr><p>"
!text.writeln fd,	replace$(replace$(replace$(header$,"\n","<br>"),"\[","<a href=\"\1\"><\a>"),"xxx","yyy") + "</p><hr><br><ul>"
text.writeln fd,	html$(header$) + "</p><hr><br><ul>"

err$="error processing files"
print "processing snippet files"
count = 0
				
for i = 1 to size
                list.get file_list,i,name$
				if  left$(name$,1) = "0" 	then f_n.continue
				if right$(name$,3) = "(d)" 	then f_n.continue
				if right$(name$,4) <>".txt" then f_n.continue
				count++
				arr$[count]=name$
next i

array.sort arr$[]
array.length l, arr$[]
for a=1+l-count to l 
				name$=arr$[a]
				line$=name$
				if len(name$) < 20 then
					for i = len(name$) to 20
						line$ = line$ +  " "
					next i
				else
						line$ = line$ +  " "
				endif
				ftp.get name$, 	"0file.txt"
				text.open r, t,	"0file.txt"
				text.readln t,s$
				if left$(s$,1) <> "!" then s$=""
				s$ = mid$(s$,2)
				line$=line$+s$
				text.writeln fp, line$
				s$= "<li><a href=\"" + name$ + "\">" + left$(name$,len(name$)-4) + "</a>" + mid$(line$,len(name$)+1)
				text.writeln fd, s$
				print ".";
next a

print ""
print "processed "; count ; " Snippets"

text.writeln fd,	"</ul><br><hr>"
time year$,month$,day$,hour$,minute$
text.writeln fd,	"Updated "; count ; " files on " + year$ + "/" + month$+ "/" + day$+ " " + hour$+ ":" + minute$ + " "
text.writeln fp , "\n\n"
text.writeln fp,	"Updated "; count ; " files on " + year$ + "/" + month$+ "/" + day$+ " " + hour$+ ":" + minute$ + " "


	err$="error updating files"

text.writeln fd,	"</body></html>"

text.close fp
text.close fd

	err$="error updating files"

! look file
i = 0
do
	text.open w,fp,"0file.txt"
	text.writeln fp, 0
	text.close fp
	ftp.get		"0look.txt" , "0file.txt"
	grabfile header$,	"0file.txt"
	if len(header$) = 0 then d_u.break
	if val(header$) = 0 then D_u.break
	i++
	pause 3000
	if i=20 | i = 30 | i = 35 then print "unable to aquire look, traying again"
until i=40
	if i=40  then		print "overriding look"
	text.open w, fp,"0file.txt"
    text.writeln fp, 1
	text.close fp
	ftp.put		"0file.txt" , "0look.txt"
	


ftp.get		"0look.txt" , "0file.txt"
ftp.put 	"0index.txt", "0index.txt"
ftp.put		"0index.htm", "index.html"

	text.open w, fp,"0file.txt"
    text.writeln fp, 0
	text.close fp
	ftp.put		"0file.txt" , "0look.txt"

ftp.close



err$=""
print "done"

onError:
s$ = geterror$()

print " "
if err$ <> "" then print "Error:" + err$
if err$ <> "" then print s$
print "press back key to quit"
do
pause 3000
until 0
