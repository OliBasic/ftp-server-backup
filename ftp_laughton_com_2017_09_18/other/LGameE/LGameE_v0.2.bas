FN.DEF Graphics_On(color$, ori$)
	SW.BEGIN color$
		SW.CASE "red"
			cA = 150
			cR = 255
			cG = 0
			cB = 0
		SW.BREAK
		SW.CASE "green"
			cA = 150
			cR = 0
			cG = 255
			cB = 0
		SW.BREAK
		SW.CASE "blue"
			cA = 150
			cR = 0
			cG = 0
			cB = 255
		SW.BREAK
		SW.DEFAULT
			cA = 255
			cR = 0
			cG = 0
			cB = 0
	SW.END
	SW.BEGIN Ori$
		SW.CASE "land"
			Ori = 0
		SW.BREAK
		SW.CASE "port"
			Ori = 1
		SW.BREAK
		SW.DEFAULT
			Ori = -1
	SW.END
	GR.OPEN cA, cR, cG, cB, 0, Ori
	PAUSE 500
FN.END

FN.DEF Rotate3D_X(vec_reflist[], vec_ptlist[], angY)
	ARRAY.LENGTH tmp_nvar, vec_reflist[]
	FOR i = 4 TO tmp_nvar STEP 3
		py = vec_reflist[i+1]
		pz = vec_reflist[i+2]

		vec_ptlist[i+1] = (py*cos(angX))+(pz*-sin(angX))
		vec_ptlist[i+2] = (py*sin(angX))+(pz*cos(angX))
	NEXT
FN.END

FN.DEF Rotate3D_Y(vec_reflist[], vec_ptlist[], angY)
	ARRAY.LENGTH tmp_nvar, vec_reflist[]
	FOR i = 4 TO tmp_nvar STEP 3
		px = vec_reflist[i]
		pz = vec_reflist[i+2]

		vec_ptlist[i] = (px*cos(angY))+(pz*-sin(angY))
		vec_ptlist[i+2] = (px*sin(angY))+(pz*cos(angY))
	NEXT
FN.END

FN.DEF Rotate3D_Z(vec_reflist[], vec_ptlist[], angZ)
	ARRAY.LENGTH tmp_nvar, vec_reflist[]
	FOR i = 4 TO tmp_nvar STEP 3
		px = vec_reflist[i]
		py = vec_reflist[i+1]

		vec_ptlist[i] = (px*cos(angZ))+(py*-sin(angZ))
		vec_ptlist[i+1] = (px*sin(angZ))+(py*cos(angZ))
	NEXT
FN.END

FN.DEF Project2D(vec_ptlist[], r2d_ptlist[], size, CamX, CamY, CamZ)
	sigCh = 1-2
	ARRAY.LENGTH tmp_nvar, vec_ptlist[]
	FOR i = 4 TO tmp_nvar STEP 3	
		px = vec_ptlist[1]+(vec_ptlist[i]*size)
		py = vec_ptlist[2]+(vec_ptlist[i+1]*size)
		pz = vec_ptlist[3]+(vec_ptlist[i+2]*size)

		psx = CamX+(px-CamX)/((CamZ+(pz*sigCh))/CamZ)
		psy = CamY+(py-CamY)/((CamZ+(pz*sigCh))/CamZ)
		
		r2d_ptlist[i-3-tmp_id] = psx
		r2d_ptlist[i-2-tmp_id] = psy

		tmp_id += 1
	NEXT
FN.END
		
		