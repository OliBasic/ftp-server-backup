INCLUDE LGameE/LGameE_v0.1.bas

! --------------------------------------

LGameE = Graphics_On("black", "land")

GR.COLOR 255, 255, 255, 255, 2
GR.SCREEN sWd, sHe

CamX = sWd/2
CamY = sHe/2
CamZ = sWd
Pi = atan(1)*4

Unit = sWd/10
CubeH = sWd/4
CubeV = 0

tSize = sHe/24
tLine = sHe/26
GR.TEXT.SIZE tSize

DIM vec_ptlist[27]
ARRAY.LOAD vec_reflist[], CamX+CubeH, CamY-CubeV, -200~
, -2, -2, 2~
, 2, -2, 2~
, -2, 2, 2~
, 2, 2, 2~
, -2, -2, -2~
, 2, -2, -2~
, -2, 2, -2~
, 2, 2, -2

DIM r2d_ptlist[16]
DIM PolyP[6, 4]
DIM PolyP2[8]
DIM Face[6]

FOR i = 1 TO 6
	LIST.CREATE n, Face[i]
NEXT

PolyP[1,1] = 1
PolyP[1,2] = 2
PolyP[1,3] = 4
PolyP[1,4] = 3

PolyP[2,1] = 5
PolyP[2,2] = 1
PolyP[2,3] = 3
PolyP[2,4] = 7

PolyP[3,1] = 5
PolyP[3,2] = 1
PolyP[3,3] = 2
PolyP[3,4] = 6

PolyP[4,1] = 2
PolyP[4,2] = 6
PolyP[4,3] = 8
PolyP[4,4] = 4

PolyP[5,1] = 3
PolyP[5,2] = 4
PolyP[5,3] = 8
PolyP[5,4] = 7

PolyP[6,1] = 5
PolyP[6,2] = 6
PolyP[6,3] = 8
PolyP[6,4] = 7

FOR i = 1 TO 27
	vec_ptlist[i] = vec_reflist[i]
NEXT

DO
FOR ang = 0 TO 360
	GR.CLS

	angle = ((ang*1)*pi)/180

	LGameE = Rotate3D_X(vec_reflist[], vec_ptlist[], angle)

	LGameE = Project2D(vec_ptlist[], r2d_ptlist[], Unit, CamX, CamY, CamZ)

	FOR i = 1 TO 6
		FOR j = 1 TO 4
			PolyP2[(j*2)-1] = r2d_ptlist[(PolyP[i,j]*2)-1]
			PolyP2[j*2] = r2d_ptlist[PolyP[i,j]*2]
		NEXT
		LIST.ADD.ARRAY Face[i], PolyP2[]
	NEXT

	FOR i = 6 TO 1 STEP -1
		GR.COLOR 150, 255, 255, 255, 0
		GR.POLY Cube3D, Face[i]
	NEXT

	FOR i = 1 TO 8
		tx = r2d_ptlist[i*2-1]
		ty = r2d_ptlist[i*2]
		GR.COLOR 255, 255, 255, 0, 1
		GR.TEXT.DRAW pID,  tx, ty, format$("#", i)
	NEXT

	tmp_id = 0
	FOR i = 1 TO 8
		GR.COLOR 255, 180, 180, 180, 1
		tmp_str$ = format$("#", i)
		tmp_str$ = "P "+tmp_str$+~
		" = x: "+format$("%.##",vec_ptlist[3+i+tmp_id])+~
		" # y: "+format$("%.##",vec_ptlist[3+i+1+tmp_id])+~
		" # z: "+format$("%.##",vec_ptlist[3+i+2+tmp_id])
		GR.TEXT.DRAW Info, 5, i*tLine,  tmp_str$
		tmp_id += 2
		
		tmp_str$ = "Angle: "+format$("##%",ceil(angle*180/pi))+" degrees"
		GR.TEXT.DRAW Info, 5, 10*tLine, tmp_str$
	NEXT

	GR.RENDER
	FOR i = 1 TO 6
		LIST.CLEAR Face[i]
	NEXT
NEXT
UNTIL 0
