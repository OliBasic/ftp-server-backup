REM WavMaker.bas
REM -Hotwrench-

ws$="wave.wav"      % name of file to make   
DIM wavdata[8000]   % file size, change to suit 
PRINT "writing file ";ws$
! your sound data starts here
! data of 128 = silence
! data must be between 0 and 255

FOR tt=1TO4
 FOR t=1TO255
  a=t
  h++                   % inc data pointer
  wavdata[44+h]=a       % write data to array
  a2=255-t
  h++                   % inc data pointer
  wavdata[44+h]=a2      % write.data to array
 NEXT
NEXT
!your sound data ends here

fl=h+44                % file size
PRINT "file size = ";fl

BYTE.OPEN w,wf,ws$     % read data
FOR t = 1 TO 44        
 READ.NEXT dat
 wavdata[t]=dat
NEXT

h=h+44-8              % file length

hr=MOD(h,256)
b=(h-hr)/256
wavdata[5]=hr
wavdata[6]=b

h=h-44                 % data length
hr=MOD(h,256)
b=(h-hr)/256
wavdata[41]=hr
wavdata[42]=b


FOR t2=1 TO fl         % write data to file
 BYTE.WRITE.BYTE wf,wavdata[t2]
NEXT
BYTE.CLOSE wf

PRINT "playing your .wav"
AUDIO.LOAD playw,ws$
AUDIO.PLAY playw

DO
 AUDIO.ISDONE isdone
UNTIL isdone
END

! header info  44 bytes
READ.DATA 82,73,70,70     % "RIFF"
READ.DATA 37,4,0,0        % file length -8
READ.DATA 87,65,86,69     % "WAVE"
READ.DATA 102,109,116,32  % "fmt "
READ.DATA 16,0,0,0        % fmtlength = 8-32 bit
READ.DATA 1,0             % 1=pcm
READ.DATA 1,0             % 1=mono 2=stereo
READ.DATA 17,43           % 17,43=11025  64,31=8000
READ.DATA 0,0             % bytes per sec.
READ.DATA 17,43           % 17,43=11025  64,31=8000
READ.DATA 0,0             % fmt spefic
READ.DATA 1,0,8,0         % padding
READ.DATA 100,97,116,97   % "data"
READ.DATA 0,4,0,0         % file length-44

