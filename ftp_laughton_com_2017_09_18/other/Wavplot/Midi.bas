Rem midi.bas
Rem -Hotwrench-
Rem midi file breakdown
Rem makes type-0 midi file - one track

dim midi[100]
mf$="midi.mid"     %file name to write
for t = 1 to 42
read.next midi[t]  %read in the midi file data
next
print "writing"
byte.open w,md,mf$ %open the file
for t = 1 to 42
byte.write.byte md,midi[t] %write the Midi file
next
byte.close md      %close the file
print "playing ";mf$
Audio.load midf, mf$ 
Audio.play midf
pause 5000  
Audio.stop
print "done"
end
 Rem  Midi Header
read.data 77,84,104,100    % "MThd" 
read.data 0,0,0,6          % length of remaining header; always 6 
read.data 0,0              % MIDI format:  type-0 = 0 type-1 = 1
read.data 0,1              % number of tracks,   type-0 has one track
read.data 0,80             % tempo; ticks per quarter note                     
read.data 77,84,114,107    % "MTrk"   start of track
read.data 0,0,0,20         %length of music data + 4*

 !note data starts here
read.data 0,144,60,127,127,144,62,127,127,144,64,127,144,176,123,0
 !note data ends here
 
read.data 0,255,47,0       % end of track ; the + 4*

!!
above: 0=time;144=note on,60=note,127=volume,127=time,144=note on,62=note etc... 
60 = middle C; 62 = D therefore 61 = D flat or a C sharp
127,183,176,0 = all notes off
 
note data:
byte 1: time, 1st note = 0; unless you want a pause at start
byte 2: 144=note on; trk 0, 132=note off; trk 0
        192 = instrument; trk 0 , there are others
byte 3: pitch aka note or instrument
byte 4: volume 0-127
!!