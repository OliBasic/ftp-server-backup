REM  Wavplot
REM -Hotwrench-
GR.OPEN 255,0,0,0
GR.SCREEN w, h
x1=w/800
y1=h/480
GR.SCALE x1,y1
s$="meow.wav"
GR.COLOR 255,255,255,255,0
GR.TEXT.SIZE 30
GR.TEXT.DRAW txt,100,120,"Loading...  "+s$
GR.RENDER
GR.COLOR 255,255,0,0,0
d=45   % don't plot .wav header
BYTE.OPEN r,fn,s$
BYTE.POSITION.SET fn,d
FILE.SIZE fl,s$
dd=CEIL(fl/800)  % sample rate
x = 0
amp = 2  % amplitude

DO
 c=0
 y=0
 d=d+dd
 BYTE.POSITION.SET fn,d
 BYTE.READ.BYTE fn, y
 y=y/amp
 GR.LINE lne,x, y+180 ,x2,y2
 y2 = y + 180
 x2 = x
 x = x + 1

 IF x >= 800
  GR.HIDE txt
  GR.COLOR 255,100,100,100,1
  GR.RECT ext,700,420,780,460
  GR.COLOR 255,0,0,0,1
  GR.TEXT.DRAW ext,715,450,"Exit"
  GR.RENDER
  BYTE.CLOSE fn
  AUDIO.LOAD ap,s$
  AUDIO.PLAY ap

  DO
   GR.TOUCH touched,x,y
   x/=x1
   y/=y1
   IF touched & x>700 & y>420 THEN END
  UNTIL 0
 END if

 !gr.render  % unrem to watch plot  slow
UNTIL 0


