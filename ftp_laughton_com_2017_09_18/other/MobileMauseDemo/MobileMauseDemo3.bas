! This is the demonstration program which shows
! how it is possible to imitate work of a computer mouse.
! Main image in the program is a screenshot of the desktop screen.
! The cursor can be moved freely on the virtual monitor.
! The monitor can zoom in and out using the zoom buttons.
! There are left and right mouse buttons.

! The code of the program has no comments because
! the program is not intended for studying.



!################################################
GR.OPEN 255, 0, 40, 50, 0, 0

PAUSE 500

GR.SCREEN myScrWidth, myScrHeight, myScrDensity
GR.COLOR  255, 255, 0, 0, 0
GR.SET.ANTIALIAS 0

myKoefScaling = myScrWidth / 1280
GR.SCALE myKoefScaling, myKoefScaling


myScrWidth            = myScrWidth / myKoefScaling
myScrHeight           = myScrHeight / myKoefScaling
myImageScale          = 1
myImage_X             = 0
myImage_Y             = 0
myDownTouch1          = 0
myCursor_X            = myScrWidth / 2
myCursor_Y            = myScrHeight / 2
myZoomBtnPlus_Y       = myScrHeight / 10
myZoomBtnMinus_Y      = myScrHeight / 2
myMauseBtnLeftGr_X    = myScrWidth * 0.9
myMauseBtnLeftGr_Y    = myScrHeight / 10
myMauseBtnRight_X     = myMauseBtnLeftGr_X
myMauseBtnRight_Y     = myScrHeight / 2
myZoomBtnPlus_sclY    = myZoomBtnPlus_Y * myKoefScaling
myZoomBtnMinus_sclY   = myZoomBtnMinus_Y * myKoefScaling
myMauseBtnLeftGr_sclX = myMauseBtnLeftGr_X * myKoefScaling
myMauseBtnLeftGr_sclY = myMauseBtnLeftGr_Y * myKoefScaling


GR.BITMAP.LOAD  myPoleImg, "PoleHiAsmLand.png"
GR.BITMAP.SIZE  myPoleImg, myPoleWidth, myPoleHeight
GR.BITMAP.SCALE myScaledPoleImg, myPoleImg, myPoleWidth, myPoleHeight, 1
GR.BITMAP.DRAW  myPoleGrPtr, myScaledPoleImg, myImage_X, myImage_Y

GR.BITMAP.LOAD myCursorImg, "Cursor.png"
GR.BITMAP.DRAW myCursorGrPtr, myCursorImg, myCursor_X, myCursor_Y

GR.BITMAP.LOAD myZoomBtnPlusImg, "ZoomPlus.png"
GR.BITMAP.SIZE myZoomBtnPlusImg, a, b
myZoomBtnPlusScalWidth = a * myKoefScaling
myZoomBtnPlusScalHeight_Y = myZoomBtnPlus_sclY + b * myKoefScaling
GR.BITMAP.DRAW myZoomBtnPlusGrPtr, myZoomBtnPlusImg, 0, myZoomBtnPlus_Y

GR.BITMAP.LOAD myZoomBtnMinusImg, "ZoomMinus.png"
GR.BITMAP.SIZE myZoomBtnMinusImg, a, b
myZoomBtnMinusScalWidth = a * myKoefScaling
myZoomBtnMinusScalHeight_Y = myZoomBtnMinus_sclY + b * myKoefScaling
GR.BITMAP.DRAW myZoomBtnMinusGrPtr, myZoomBtnMinusImg, 0, myZoomBtnMinus_Y

GR.BITMAP.LOAD myMauseBtnLeftGrImg, "MauseButtonLeftGr.png"
GR.BITMAP.SIZE myMauseBtnLeftGrImg, a, b
myMauseBtnLeftScalWidth_X = myMauseBtnLeftGr_sclX + a * myKoefScaling
myMauseBtnLeftScalHeight_Y = myMauseBtnLeftGr_sclY + b * myKoefScaling
GR.BITMAP.DRAW myMauseBtnLeftGrGrPtr, myMauseBtnLeftGrImg, myMauseBtnLeftGr_X, myMauseBtnLeftGr_Y

GR.BITMAP.LOAD myMauseBtnRightImg, "MauseButtonRight.png"
GR.BITMAP.SIZE myMauseBtnRightImg, a, b
myMauseBtnRightScalWidth_X = myMauseBtnRight_X + a * myKoefScaling
myMauseBtnRightScalHeight_Y = myMauseBtnRight_Y + b * myKoefScaling
GR.BITMAP.DRAW myMauseBtnRightGrPtr, myMauseBtnRightImg, myMauseBtnRight_X, myMauseBtnRight_Y


IF !BACKGROUND() THEN GR.RENDER ELSE END

!###########################################
DO
	GR.BOUNDED.TOUCH myTouchZoomBtnPlus, 0, myZoomBtnPlus_sclY, myZoomBtnPlusScalWidth, myZoomBtnPlusScalHeight_Y
	IF myTouchZoomBtnPlus THEN
		GOSUB myOnTouchZoomBtnPlus
		GR.RENDER
		GOTO myUntilPoint
	ENDIF
	
	GR.BOUNDED.TOUCH myTouchZoomBtnMinus, 0, myZoomBtnMinus_sclY, myZoomBtnMinusScalWidth, myZoomBtnMinusScalHeight_Y
	IF myTouchZoomBtnMinus THEN
		GOSUB myOnTouchZoomBtnMinus
		GR.RENDER
		GOTO myUntilPoint
	ENDIF
	
	GR.BOUNDED.TOUCH myTouchMauseBtnLeftGr, myMauseBtnLeftGr_sclX, myMauseBtnLeftGr_sclY, myMauseBtnLeftScalWidth_X, myMauseBtnLeftScalHeight_Y
	IF myTouchMauseBtnLeftGr THEN
		myImage_X = 0
		myImage_Y = 0
		GR.MODIFY myPoleGrPtr, "x", myImage_X
		GR.MODIFY myPoleGrPtr, "y", myImage_Y
		GR.RENDER
		GOTO myUntilPoint
	ENDIF
	
	GR.TOUCH  myFlagTouchScr1, myTouch1_X, myTouch1_Y
	IF myFlagTouchScr1 & !myDownTouch1 THEN
		GOSUB myStartTouchScrEvent
	
	ELSEIF myFlagTouchScr1 & myDownTouch1 THEN
		GOSUB myContinTouchScrEvent
		GR.RENDER
	
	ELSE
		myDownTouch1 = 0

	ENDIF
	
myUntilPoint:
UNTIL 0

END
!<<<<<<<



myOnTouchZoomBtnPlus:
!===========================================

a = myImageScale * 1.14
IF a >= 2 THEN RETURN

myX_virtCursor = (myCursor_X - myImage_X) / myImageScale
myY_virtCursor = (myCursor_Y - myImage_Y) / myImageScale

b = myImageScale
myImageScale = a

GR.CLS
GR.BITMAP.DELETE myScaledPoleImg
GR.BITMAP.DELETE myPoleImg

myImage_X = myCursor_X - myX_virtCursor * myImageScale
myImage_Y = myCursor_Y - myY_virtCursor * myImageScale

GR.BITMAP.LOAD  myPoleImg, "PoleHiAsmLand.png"
GR.BITMAP.SCALE myScaledPoleImg, myPoleImg, myPoleWidth * myImageScale, myPoleHeight * myImageScale, 1
GR.BITMAP.DRAW  myPoleGrPtr, myScaledPoleImg, myImage_X, myImage_Y

GR.BITMAP.DRAW myCursorGrPtr, myCursorImg, myCursor_X, myCursor_Y
GR.BITMAP.DRAW myZoomBtnPlusGrPtr, myZoomBtnPlusImg, 0, myZoomBtnPlus_Y
GR.BITMAP.DRAW myZoomBtnMinusGrPtr, myZoomBtnMinusImg, 0, myZoomBtnMinus_Y
GR.BITMAP.DRAW myMauseBtnLeftGrGrPtr, myMauseBtnLeftGrImg, myMauseBtnLeftGr_X, myMauseBtnLeftGr_Y
GR.BITMAP.DRAW myMauseBtnRightGrPtr, myMauseBtnRightImg, myMauseBtnRight_X, myMauseBtnRight_Y

RETURN
!<<<<<<<



myOnTouchZoomBtnMinus:
!===========================================

a = myImageScale * 0.9
IF a <= 0.5 THEN RETURN

myX_virtCursor = (myCursor_X - myImage_X) / myImageScale
myY_virtCursor = (myCursor_Y - myImage_Y) / myImageScale

b = myImageScale
myImageScale = a

GR.CLS
GR.BITMAP.DELETE myScaledPoleImg
GR.BITMAP.DELETE myPoleImg

myImage_X = myCursor_X - myX_virtCursor * myImageScale
myImage_Y = myCursor_Y - myY_virtCursor * myImageScale

GR.BITMAP.LOAD  myPoleImg, "PoleHiAsmLand.png"
GR.BITMAP.SCALE myScaledPoleImg, myPoleImg, myPoleWidth * myImageScale, myPoleHeight * myImageScale, 1
GR.BITMAP.DRAW  myPoleGrPtr, myScaledPoleImg, myImage_X, myImage_Y

GR.BITMAP.DRAW myCursorGrPtr, myCursorImg, myCursor_X, myCursor_Y
GR.BITMAP.DRAW myZoomBtnPlusGrPtr, myZoomBtnPlusImg, 0, myZoomBtnPlus_Y
GR.BITMAP.DRAW myZoomBtnMinusGrPtr, myZoomBtnMinusImg, 0, myZoomBtnMinus_Y
GR.BITMAP.DRAW myMauseBtnLeftGrGrPtr, myMauseBtnLeftGrImg, myMauseBtnLeftGr_X, myMauseBtnLeftGr_Y
GR.BITMAP.DRAW myMauseBtnRightGrPtr, myMauseBtnRightImg, myMauseBtnRight_X, myMauseBtnRight_Y

RETURN
!<<<<<<<



myStartTouchScrEvent:
!===========================================

myStartTouch1x = myTouch1_X
myStartTouch1y = myTouch1_Y
myDownTouch1 = 1

RETURN
!<<<<<<<



myContinTouchScrEvent:
!===========================================

myDeltaX = myTouch1_X - myStartTouch1x
myDeltaY = myTouch1_Y - myStartTouch1y
IF myDeltaX * myDeltaY = 0 THEN RETURN

myStartTouch1x = myTouch1_X
myStartTouch1y = myTouch1_Y


a = 0
IF myCursor_X <= myScrWidth * 0.25 THEN
	a = 1
ELSEIF myCursor_X >= myScrWidth * 0.75 THEN
	a = 2
ENDIF

IF myCursor_Y <= myScrHeight * 0.25 THEN
	a = a + 4
ELSEIF myCursor_Y >= myScrHeight * 0.75 THEN
	a = a + 8
ENDIF


b = 0
IF myDeltaX < 0 THEN
	b = 1
ELSEIF myDeltaX = 0 THEN
	b = 2
ELSE
	b = 4
ENDIF

IF myDeltaY < 0 THEN
	b = b + 8
ELSEIF myDeltaY = 0 THEN
	b = b + 16
ELSE
	b = b + 32
ENDIF


IF a = 0 THEN
	GOSUB myCursorHorizontMove
	GOSUB myCursorVerticalMove
	
ELSEIF a = 1 THEN
	GOSUB myCursorVerticalMove
	
	IF myDeltaX < 0 THEN
		GOSUB myImageHorizontMove
		
	ELSE
		GOSUB myCursorHorizontMove
		
	ENDIF
ELSEIF a = 2 THEN
	GOSUB myCursorVerticalMove
	
	IF myDeltaX > 0 THEN
		GOSUB myImageHorizontMove
		
	ELSE
		GOSUB myCursorHorizontMove
		
	ENDIF
ELSEIF a = 4 THEN
	GOSUB myCursorHorizontMove
	
	IF myDeltaY < 0 THEN
		GOSUB myImageVerticalMove
		
	ELSE
		GOSUB myCursorVerticalMove
		
	ENDIF
ELSEIF a = 8 THEN
	GOSUB myCursorHorizontMove
	
	IF myDeltaY > 0 THEN
		GOSUB myImageVerticalMove
		
	ELSE
		GOSUB myCursorVerticalMove
		
	ENDIF
ELSEIF a = 5 THEN
	IF b = 9 THEN
		GOSUB myImageHorizontMove
		GOSUB myImageVerticalMove
		
	ELSEIF b = 10 THEN
		GOSUB myImageVerticalMove
	
	ELSEIF b = 12 THEN
		GOSUB myImageVerticalMove
		GOSUB myCursorHorizontMove
	
	ELSEIF b = 17 THEN
		GOSUB myImageHorizontMove
	
	ELSEIF b = 33 THEN
		GOSUB myImageHorizontMove
		GOSUB myCursorVerticalMove
	
	ELSE
		GOSUB myCursorVerticalMove
		GOSUB myCursorHorizontMove
	
	ENDIF
ELSEIF a = 6 THEN
	IF b = 9 THEN
		GOSUB myImageVerticalMove
		GOSUB myCursorHorizontMove
	
	ELSEIF b = 10 THEN
		GOSUB myImageVerticalMove
	
	ELSEIF b = 12 THEN
		GOSUB myImageHorizontMove
		GOSUB myImageVerticalMove
	
	ELSEIF b = 20 THEN
		GOSUB myImageHorizontMove
	
	ELSEIF b = 36 THEN
		GOSUB myImageHorizontMove
		GOSUB myCursorVerticalMove
	
	ELSE
		GOSUB myCursorVerticalMove
		GOSUB myCursorHorizontMove
		
	ENDIF
ELSEIF a = 9 THEN
	IF b = 9 THEN
		GOSUB myImageHorizontMove
		GOSUB myCursorVerticalMove
	
	ELSEIF b = 17 THEN
		GOSUB myImageHorizontMove
	
	ELSEIF b = 33 THEN
		GOSUB myImageHorizontMove
		GOSUB myImageVerticalMove
	
	ELSEIF b = 34 THEN
		GOSUB myImageVerticalMove
	
	ELSEIF b = 36 THEN
		GOSUB myImageVerticalMove
		GOSUB myCursorHorizontMove
	
	ELSE
		GOSUB myCursorVerticalMove
		GOSUB myCursorHorizontMove
	
	ENDIF
ELSE
	IF b = 33 THEN
		GOSUB myImageVerticalMove
		GOSUB myCursorHorizontMove
	
	ELSEIF b = 34 THEN
		GOSUB myImageVerticalMove
	
	ELSEIF b = 36 THEN
		GOSUB myImageHorizontMove
		GOSUB myImageVerticalMove
	
	ELSEIF b = 20 THEN
		GOSUB myImageHorizontMove
	
	ELSEIF b = 12 THEN
		GOSUB myImageHorizontMove
		GOSUB myCursorVerticalMove
	
	ELSE
		GOSUB myCursorVerticalMove
		GOSUB myCursorHorizontMove
	
	ENDIF
ENDIF

RETURN
!<<<<<<<



myCursorVerticalMove:
!===========================================

myCursor_Y = myCursor_Y + myDeltaY
GR.MODIFY myCursorGrPtr, "y", myCursor_Y

RETURN
!<<<<<<<



myCursorHorizontMove:
!===========================================

myCursor_X = myCursor_X + myDeltaX
GR.MODIFY myCursorGrPtr, "x", myCursor_X

RETURN
!<<<<<<<



myImageVerticalMove:
!===========================================

myImage_Y = myImage_Y - myDeltaY
GR.MODIFY myPoleGrPtr, "y", myImage_Y

RETURN
!<<<<<<<



myImageHorizontMove:
!===========================================

myImage_X = myImage_X - myDeltaX
GR.MODIFY myPoleGrPtr, "x", myImage_X

RETURN
!<<<<<<<
