GOSUB           userfunctions

flagOri       = 0
refW          = 790
refH          = 1255
IF !flagOri     THEN SWAP refW, refH
centW         = refW/2
centH         = refH/2
GR.OPEN         255, 255 , 255 , 255 ,1,flagOri
GR.SCREEN       curW, curH
scaW          = curW / refW
scaH          = curH / refH
GR.SCALE        scaW , scaH


FONT.LOAD       fo,   pa$+ "STRSL___.ttf"
FONT.LOAD       miFo, pa$+ "STRSLMI_.ttf"


px            = 200
py            = centh/2
txtsz         = 130
s$            = "BASIC!  mirror"
align         = 1
offsh         = 3
cBack$        = " 255 , 255 , 255 "
cFore$        = " 000 , 000 , 000 "

dummy         = mirroredTxt( s$, px, py, txtsz, align, offsh, cBack$, cFore$, fo, miFo)

GR.COLOR        255 , 000 , 000 , 000 , 1
GR.RECT         nn, 0, centh , refw , refh
ptrTxt        = mirroredTxt( "1              9", px, py+centh, txtsz+5, align, offsh+1, cFore$, cBack$, fo, miFo)


dtDes         = 1000
tic           = clock ()
DO
 time           ye$, mo$, da$, ho$, mi$, se$
 st $         = "    "+ ho$ +":"+ mi$ +":"+ se$
 GR.MODIFY      ptrTxt  , "text" , st$
 GR.MODIFY      ptrTxt+2, "text" , flipText$(st $)
 GR.RENDER

 PAUSE          max (dtdes-(CLOCK()-tic), 1)
 tic          = clock ()
UNTIL 0




userfunctions:

FN.DEF            mirroredTxt( s$, px, py, txtsz, align, offsh, cBack$, cFore$, fo, miFo)

 SPLIT             cF $[], cFore$, ","
 SPLIT             cB $[], cBack$, ","
 GR.TEXT.SIZE      txtsz
 IF                align =1 THEN miAlign =3 ELSE miAlign=1

 GR.COLOR          255, VAL(cF$[1]), VAL(cF$[2]), VAL(cF$[3]), 1
 GR.TEXT.ALIGN     align
 GR.TEXT.SETFONT   fo
 GR.TEXT.DRAW      t1, px , py, s$

 GR.TEXT.SETFONT   miFo
 GR.TEXT.ALIGN     miAlign
 GR.ROTATE.START   -180, px-0, py+ offsh
 GR.TEXT.DRAW      nn, px , py, flipText$(s$)
 GR.ROTATE.END

 GR.GET.TEXTBOUNDS s$, le, to, ri, bo
 hh              = -to+offsh+5
 FOR i           = 1 TO hh
  GR.COLOR         i/hh*120+15, VAL(cB$[1]), VAL(cB$[2]), VAL(cB$[3]), 1
  GR.RECT          nn, px, py+i, px+ri*1.01, py+i+3
 NEXT

 FN.RTN            t1

FN.END

FN.DEF            flipText$(s$)
 FOR i           = len (s$) TO 1 STEP-1
  sfli$          = sfli$ + mid $( s$, i, 1)
 NEXT
 FN.RTN             sfli $
FN.END


RETURN
