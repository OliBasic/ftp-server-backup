Rem Bouncing balls2
Rem Without Using Funtions
Rem With RFO Basic!
Rem December 2014
Rem Version 1.02
Rem By Roy
Rem

console.title"Bouncing balls"
Rem Open Graphics
di_width = 1280 % set to my tablet
di_height = 800

gr.open 255,0,0,0 % Black
gr.orientation 0
pause 1000
gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y
gr.text.typeface 2
gr.text.size 27 % will give 80 x 26 chrs
wakelock 3

Rem Globel Vars
maxBalls = 100
ballNext=1 
ballEnd=1
dim mess[1]

dim c[maxBalls] % circle colour
dim cn[maxBalls] %circle number
dim x[maxBalls] % circle y
dim y[maxBalls] % circle x
dim r[maxBalls] % radius
dim xd[maxBalls] % x direction and speed
dim yd[maxBalls] % y direction and speed
dim mx[maxBalls] % x for moving
dim my[maxBalls] % y for moving


Rem start of functions

fn.def Gcol(c,g)
	Rem g=0 Outline and g=1 Fill
	if c=0 then gr.color 255,0,0,0,g % black
	if c=1 then gr.color 255,255,0,0,g % Red
	if c=2 then gr.color 255,0,255,0,g % green
	if c=3 then gr.color 255,0,0,255,g % blue
	if c=4 then gr.color 255,255,255,0,g % yellow
	if c=5 then gr.color 255,0,255,255,g % cyan
	if c=6 then gr.color 255,255,0,255,g % magenta
	if c=7 then gr.color 255,192,192,192,g % gray
	if c=8 then gr.color 255,255,255,255,g % white
	
    if c=9 then gr.color 255,205,0,0,g % mediumblue
	if c=10 then gr.color 255,65,105,225,g % royalblue
	if c=11 then gr.color 255,255,215,0,g % gold
	if c=12 then gr.color 255,0,250,154,g % springgreen
	if c=13 then gr.color 255,220,20,60,g % crimson
fn.end

fn.def CurvyWindow(x,y,l,h,c)
	call Gcol(c,1)
	gr.rect r,x,y+25,l,h-25 
	gr.rect r,x+25,y,l-25,h 
	gr.circle c, x+25,y+25,25 
	gr.circle c,l-25,y+25,25 
	gr.circle c,x+25,h-25,25
	gr.circle c,l-25,h-25,25
	gr.render
fn.end

fn.def PrintTab(x,y,bgc,fgc,t$)
	x--
	gr.text.width t,t$
	gr.text.width w,"W"
	gr.text.height h,up,dn
	x*=w 
	y*=abs(up)+dn
	Call Gcol(bgc,1)
	gr.rect r,x,y+up,x+t,y+dn
	call Gcol(fgc,1)
	gr.text.draw d,x,y,t$
	gr.render
fn.end

Rem End of Functions

Rem Main

rem SetUp Arrays
	for s=1 to maxBalls
		c[s]=int(13 * rnd()+1)
		x[s]=int(800 * Rnd()+200)
		y[s]=int(600 * rnd()+100)
		r[s]=int(10 * rnd() + 30)
		xd[s]=int(40 * rnd()-20)
		yd[s]=int(40 * rnd()-20)
		mx[s]=x[s]
		my[s]=y[s]
		if xd[s]=0 then xd[s]=10
		if yd[s]=0 then yd[s]=-10
	next

Rem SetUp Screen
	call CurvyWindow(1,1,di_width,di_height,11)
	call CurvyWindow(50,50,di_width-50,di_height-50,0)
	call Gcol(0,1)
	gr.text.bold 1
	gr.text.draw mess[1],500,35,"1 Bouncing ball"
	gr.text.bold 0
	call PrintTab(5,25,13,0," More Balls ")
	call PrintTab(65,25,13,0," Less Balls ")
	call PrintTab(38,25,13,0," Pause ")
	
Rem First ball
	s=ballNext
	call Gcol(c[s],1)
	gr.circle cn[s],x[s],y[s],r[s]

Rem Main Loop	
do 
	do
		for s=1 to ballEnd
			let mx[s]=mx[s]+xd[s] 
			let my[s]=my[s]+yd[s]
			
			gr.modify cn[s],"x",mx[s],"y",my[s]
			gosub CheckWalls
			
		next
		gr.render
		gr.touch touched,tx,ty
	until touched
	
	Rem Get Next Ball
	if tx>70*scale_x & tx<350*scale_x & ty>700*scale_y & ty<800*scale_y then
		if ballEnd<maxBalls then
			ballEnd++
			ballNext++
			s=ballNext
			call Gcol(c[s],1)
			if s>1 then
				gr.modify mess[1],"text",int$(s)+" Bouncing Balls"
			else
				gr.modify mess[1],"text",int$(s)+" Bouncing Ball"
			endif
			gr.circle cn[s],x[s],y[s],r[s]
		else
			tone 600,100 
		endif
	endif
	
	Rem Diminish number of balls
	if tx>1000*scale_x & tx<1250*scale_x & ty>700*scale_y & ty<800*scale_y then
		if ballEnd>1 then 
			s=ballEnd
			gr.hide cn[s]
			if s>2 then
				gr.modify mess[1],"text",int$(s-1)+" Bouncing Balls"
			else
				gr.modify mess[1],"text",int$(s-1)+" Bouncing Ball"
			endif
			gr.render
			ballEnd-- 
			ballNext--
		endif
	endif
	
	Rem Pause
	if tx>500*scale_x & tx<700*scale_x & ty>700*scale_y & ty<800*scale_y then
		call PrintTab(38,25,13,0," Start ")
		do 
			gr.touch touched,tx,ty 
		until !touched
		do 
			gr.touch touched,tx,ty 
		until  touched & tx>500*scale_x & tx<700*scale_x & ty>700*scale_y & ty<800*scale_y
		call PrintTab(38,25,13,0," Pause ")
		do 
			gr.touch touched,tx,ty 
		until !touched
	endif

until 0
Rem end of main loop

Rem Start of subs

CheckWalls:
	if mx[s]>1190 then xd[s]=-xd[s]
	if mx[s]<90 then xd[s]=abs(xd[s])
	
	if my[s]>710 then yd[s]=-yd[s]
	if my[s]<90 then yd[s]=abs(yd[s])
return

Rem End of subs

onBackKey:
do
	Dialog.Message "Exit:", "Are you sure you want to leave Bouncing Balls", yn, "Yes","No"
	if yn=0 then tone 400,400
until yn=1 | yn=2

if yn=2 then back.resume

wakelock 5 
gr.close

print
print"Thanks for trying Bouncing Balls Demo"
pause 5000
exit

