REM Bouncing balls2
REM Without Using Functions
REM With RFO Basic!
REM December 2014
REM Version 1.02
REM By Roy
REM

CONSOLE.TITLE"Bouncing balls"
REM Open Graphics
di_width = 1280 % set to my tablet
di_height = 800

GR.OPEN 255,0,0,0 % Black
GR.ORIENTATION 0
PAUSE 1000
GR.SCREEN screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
GR.SCALE scale_x,scale_y
GR.TEXT.TYPEFACE 2
GR.TEXT.SIZE 27 % will give 80 x 26 chrs
WAKELOCK 3

REM Globel Vars
maxBalls = 100
ballNext=1
ballEnd=1
DIM mess[1]

DIM c[maxBalls] % circle colour
DIM cn[maxBalls] %circle number
DIM x[maxBalls] % circle y
DIM y[maxBalls] % circle x
DIM r[maxBalls] % radius
DIM xd[maxBalls] % x direction and speed
DIM yd[maxBalls] % y direction and speed
DIM mx[maxBalls] % x for moving
DIM my[maxBalls] % y for moving


REM start of functions

FN.DEF Gcol(c,g)
 REM g=0 Outline and g=1 Fill
 IF c=0 THEN gr.color 255,0,0,0,g % black
 IF c=1 THEN gr.color 255,255,0,0,g % Red
 IF c=2 THEN gr.color 255,0,255,0,g % green
 IF c=3 THEN gr.color 255,0,0,255,g % blue
 IF c=4 THEN gr.color 255,255,255,0,g % yellow
 IF c=5 THEN gr.color 255,0,255,255,g % cyan
 IF c=6 THEN gr.color 255,255,0,255,g % magenta
 IF c=7 THEN gr.color 255,192,192,192,g % gray
 IF c=8 THEN gr.color 255,255,255,255,g % white

 IF c=9 THEN gr.color 255,205,0,0,g % mediumblue
 IF c=10 THEN gr.color 255,65,105,225,g % royalblue
 IF c=11 THEN gr.color 255,255,215,0,g % gold
 IF c=12 THEN gr.color 255,0,250,154,g % springgreen
 IF c=13 THEN gr.color 255,220,20,60,g % crimson
FN.END

FN.DEF CurvyWindow(x,y,l,h,c)
 CALL Gcol(c,1)
 GR.RECT r,x,y+25,l,h-25
 GR.RECT r,x+25,y,l-25,h
 GR.CIRCLE c, x+25,y+25,25
 GR.CIRCLE c,l-25,y+25,25
 GR.CIRCLE c,x+25,h-25,25
 GR.CIRCLE c,l-25,h-25,25
 GR.RENDER
FN.END

FN.DEF PrintTab(x,y,bgc,fgc,t$)
 x--
 GR.TEXT.WIDTH t,t$
 GR.TEXT.WIDTH w,"W"
 GR.TEXT.HEIGHT h,up,dn
 x*=w
 y*=ABS(up)+dn
 CALL Gcol(bgc,1)
 GR.RECT r,x,y+up,x+t,y+dn
 CALL Gcol(fgc,1)
 GR.TEXT.DRAW d,x,y,t$
 GR.RENDER
FN.END

REM End of Functions

REM Main

REM SetUp Arrays
FOR s=1 TO maxBalls
 c[s]=INT(13 * RND()+1)
 x[s]=INT(800 * RND()+200)
 y[s]=INT(600 * RND()+100)
 r[s]=INT(10 * RND() + 30)
 xd[s]=INT(40 * RND()-20)
 yd[s]=INT(40 * RND()-20)
 mx[s]=x[s]
 my[s]=y[s]
 IF xd[s]=0 THEN xd[s]=10
 IF yd[s]=0 THEN yd[s]=-10
NEXT

REM SetUp Screen
CALL CurvyWindow(1,1,di_width,di_height,11)
CALL CurvyWindow(50,50,di_width-50,di_height-50,0)
CALL Gcol(0,1)
GR.TEXT.BOLD 1
GR.TEXT.DRAW mess[1],500,35,"1 Bouncing ball"
GR.TEXT.BOLD 0
CALL PrintTab(5,25,13,0," More Balls ")
CALL PrintTab(65,25,13,0," Less Balls ")
CALL PrintTab(38,25,13,0," Pause ")

REM First ball
s=ballNext
CALL Gcol(c[s],1)
GR.CIRCLE cn[s],x[s],y[s],r[s]

REM Main Loop
sp        = 140
GR.COLOR    255 , 190, 0, 0, 0
GR.RECT     checkRect , sp, sp, di_width-sp, di_height-sp

DO

 DO

  LET ctr = ctr+1
  tic     = CLOCK()

  FOR s=1 TO ballEnd
   LET mx[s] = mx[s] + xd[s]
   LET my[s] = my[s] + yd[s]
   GR.MODIFY cn[s],"x",mx[s],"y",my[s]
   IF ! gr_collision ( cn[s], checkRect ) THEN  GOSUB checkWall2
  NEXT
  GR.RENDER
  GR.TOUCH touched,tx,ty

  LET  ticSum = ticSum + (CLOCK()-tic)
  IF            !Mod (ctr, 30) THEN GOSUB tMeas

 UNTIL touched

 ! times Samsung Galay  note 10.1 ---
 !50 balls : around 72
 !100 balls : around 110


 REM Get Next Ball
 IF tx>70*scale_x & tx<350*scale_x & ty>700*scale_y & ty<800*scale_y THEN
  IF ballEnd<maxBalls THEN
   ballEnd++
   ballNext++
   s=ballNext
   CALL Gcol(c[s],1)
   IF s>1 THEN
    GR.MODIFY mess[1],"text",INT$(s)+" Bouncing Balls"
   ELSE
    GR.MODIFY mess[1],"text",INT$(s)+" Bouncing Ball"
   ENDIF
   GR.CIRCLE cn[s],x[s],y[s],r[s]
  ELSE
   TONE 600,100
  ENDIF
 ENDIF

 REM Diminish number of balls
 IF tx>1000*scale_x & tx<1250*scale_x & ty>700*scale_y & ty<800*scale_y THEN
  IF ballEnd>1 THEN
   s=ballEnd
   GR.HIDE cn[s]
   IF s>2 THEN
    GR.MODIFY mess[1],"text",INT$(s-1)+" Bouncing Balls"
   ELSE
    GR.MODIFY mess[1],"text",INT$(s-1)+" Bouncing Ball"
   ENDIF
   GR.RENDER
   ballEnd--
   ballNext--
  ENDIF
 ENDIF

 REM Pause
 IF tx>500*scale_x & tx<700*scale_x & ty>700*scale_y & ty<800*scale_y THEN
  CALL PrintTab(38,25,13,0," Start ")
  DO
   GR.TOUCH touched,tx,ty
  UNTIL !touched
  DO
   GR.TOUCH touched,tx,ty
  UNTIL  touched & tx>500*scale_x & tx<700*scale_x & ty>700*scale_y & ty<800*scale_y
  CALL PrintTab(38,25,13,0," Pause ")
  DO
   GR.TOUCH touched,tx,ty
  UNTIL !touched
 ENDIF

UNTIL 0

REM end of main loop


! wallCheck, 2 alternatives -------
checkWall:
IF mx[s]> 1190 THEN LET xd[s]=-xd[s]
IF mx[s]< 90   THEN LET xd[s]= ABS(xd[s])
IF my[s]> 710  THEN LET yd[s]=-yd[s]
IF my[s]< 90   THEN LET yd[s]= ABS(yd[s])
RETURN

checkWall2:
IF mx[s]>1190 |  mx[s]<90 THEN LET xd[s]= -xd[s]
IF my[s]>710  |  my[s]<90 THEN LET yd[s]= -yd[s]
RETURN

tMeas:
tavg   = ticSum/30
ticSum = 0
ctr2++
PRINT  ctr2, ballEnd, tavg
RETURN
!----------------------------------

ONBACKKEY:
DO
 DIALOG.MESSAGE "Exit:", "Are you sure you want to leave Bouncing Balls", yn, "Yes","No"
 IF yn=0 THEN tone 400,400
UNTIL yn=1 | yn=2

IF yn=2 THEN back.resume

WAKELOCK 5
GR.CLOSE

PRINT
PRINT"Thanks for trying Bouncing Balls Demo"
PAUSE 50
END
