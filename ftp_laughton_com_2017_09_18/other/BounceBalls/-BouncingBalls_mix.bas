Rem Bouncing balls2
Rem Without Using Functions
Rem With RFO Basic!
Rem December 2014
Rem Version 1.02
Rem By Roy, Nicolas, brochi, Gilles
Rem

console.title"Bouncing balls"
Rem Open Graphics
di_width = 1280 % set to my tablet
di_height = 800

gr.open 255,0,0,0 % Black
gr.orientation 0
pause 100
gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y
gr.text.typeface 2
gr.text.size 27 % will give 80 x 26 chrs
wakelock 3

Rem Globel Vars
maxBalls = 100
ballNext=1
ballEnd=1
dim mess[3]

dim c[maxBalls] % circle colour
dim cn[maxBalls] %circle number
dim x[maxBalls] % circle y
dim y[maxBalls] % circle x
dim r[maxBalls] % radius
dim xd[maxBalls] % x direction and speed
dim yd[maxBalls] % y direction and speed
dim mx[maxBalls] % x for moving
dim my[maxBalls] % y for moving


Rem start of functions

fn.def Gcol(c,g)
   Rem g=0 Outline and g=1 Fill
   if c=0 then gr.color 255,0,0,0,g % black
   if c=1 then gr.color 255,255,0,0,g % Red
   if c=2 then gr.color 255,0,255,0,g % green
   if c=3 then gr.color 255,0,0,255,g % blue
   if c=4 then gr.color 255,255,255,0,g % yellow
   if c=5 then gr.color 255,0,255,255,g % cyan
   if c=6 then gr.color 255,255,0,255,g % magenta
   if c=7 then gr.color 255,192,192,192,g % gray
   if c=8 then gr.color 255,255,255,255,g % white
   if c=9 then gr.color 255,205,0,0,g % mediumblue
   if c=10 then gr.color 255,65,105,225,g % royalblue
   if c=11 then gr.color 255,255,215,0,g % gold
   if c=12 then gr.color 255,0,250,154,g % springgreen
   if c=13 then gr.color 255,220,20,60,g % crimson
fn.end

fn.def CurvyWindow(x,y,l,h,c)
   call Gcol(c,1)
   gr.rect nul,x,y+25,l,h-25
   gr.rect nul,x+25,y,l-25,h
   gr.circle nul,x+25,y+25,25
   gr.circle nul,l-25,y+25,25
   gr.circle nul,x+25,h-25,25
   gr.circle nul,l-25,h-25,25
   gr.render
fn.end

fn.def PrintTab(x,y,bgc,fgc,t$)
   x--
   gr.text.width t,t$
   gr.text.width w,"W"
   gr.text.height h,up,dn
   x*=w
   y*=(abs(up)+dn)
   Call Gcol(bgc,1)
   gr.rect r,x,y+up,x+t,y+dn
   call Gcol(fgc,1)
   gr.text.draw d,x,y,t$
   gr.render
   fn.rtn d
fn.end

Rem End of Functions

Rem Main

rem SetUp Arrays
   for s=1 to maxBalls
      c[s]=int(13 * rnd()+1)
      x[s]=int(800 * Rnd()+200)
      y[s]=int(600 * rnd()+100)
      r[s]=int(10 * rnd() + 30)
      xd[s]=int(20 * rnd()+5)
      yd[s]=int(20 * rnd()+5)
      mx[s]=x[s]
      my[s]=y[s]
      if xd[s]<15 then xd[s]=-xd[s]
      if yd[s]<15 then yd[s]=-yd[s]
   next

Rem SetUp Screen
gr.bitmap.create fond, di_width, di_height
gr.bitmap.drawinto.start fond
   call CurvyWindow(1,1,di_width,di_height,11)
   call CurvyWindow(50,50,di_width-50,di_height-50,0)
   call PrintTab(5,25,13,0," More Balls ")
   call PrintTab(65,25,13,0," Less Balls ")
gr.bitmap.drawinto.end
gr.bitmap.draw nul,fond,0,0
mess[2] =PrintTab(38,25,13,0," Pause ")
gr.text.draw mess[1],500,35,"1 Bouncing ball"

Rem First ball
   s=ballNext
   call Gcol(c[s],1)
   gr.circle cn[s],x[s],y[s],r[s]
   gr.color 255,200,0,0,1
   gr.text.draw mess[3], 80, 35, "fps"
   gr.color 255,190,0,0,0
   gr.rect checkRect, 140,140,di_width-140,di_height-140
   
Rem Main Loop
do
   do
     ! ta =clock()
      for s=1 to ballEnd
        gr.modify cn[s], "x",mx[s], "y",my[s]
        if !gr_collision(cn[s],checkRect) then gosub checkWall2
        mx[s]+=xd[s]
        my[s]+=yd[s]
      next
      if !background()
        gr.render
        fp = fp+1
        if clock()-td>=1000
          td =clock()
          gr.modify mess[3], "text", int$(fp)+"  fps"
          fp =0
        endif
      endif
      gr.touch touched,tx,ty
    !  print clock()-ta
   until touched
   tx/= scale_x
   ty/= scale_y
   if ty>700 & ty<800  % menu
    if tx>70 & tx<350      % Get Next Ball
      if ballEnd<maxBalls
         ballEnd++
         ballNext++
         s=ballNext
         gr.modify mess[1],"text",int$(s)+" Bouncing Ball(s)"
         if !cn[s]     % no need to generate if it already exist...!
           call Gcol(c[s],1)
           gr.circle cn[s],x[s],y[s],r[s]
         else
           gr.show cn[s]
         endif
      else
         tone 600,100
      endif
   
    elseif tx>1000 & tx<1250 & ballEnd>1  % Diminish number of balls
      s=ballEnd
      gr.hide cn[s]
      gr.modify mess[1],"text",int$(s-1)+" Bouncing Ball(s)"
      ballEnd--
      ballNext--

    elseif tx>500 & tx<700    % Pause
      gr.modify mess[2], "text", " Start "
      gr.render
      do
         gr.touch touched,tx,ty
      until !touched
      do
         gr.touch touched,tx,ty
         tx/=scale_x
         ty/=scale_y
      until touched & tx>500 & tx<700
      gr.modify mess[2], "text", " Pause "
      gr.render
      do
         gr.touch touched,tx,ty
      until !touched
    endif
   endif
until 0

checkWall2:
  if mx[s]>1190 | mx[s]<100 then xd[s]=-xd[s]
  if my[s]>700  | my[s]<100 then yd[s]=-yd[s]
return

Rem end of main loop

onBackKey:
do
   Dialog.Message "Exit:", "Are you sure you want to leave Bouncing Balls", yn, "Yes","No"
   if yn=0 then tone 400,400
until yn=1 | yn=2

if yn=2 then back.resume

wakelock 5
gr.close

END "Thanks for trying Bouncing Balls Demo"
