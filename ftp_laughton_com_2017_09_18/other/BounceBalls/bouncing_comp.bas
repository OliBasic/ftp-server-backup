Rem Bouncing balls comparison
Rem gr.modify Vs gr.bitmap.drawinto
Rem With RFO Basic!
Rem December 2014
Rem Version 1.02
Rem By Roy, adapted by mougino


Rem start of functions

fn.def Gcol(c,g)
   Rem g=0 Outline and g=1 Fill
   if c=0 then gr.color 255,0,0,0,g % black
   if c=1 then gr.color 255,255,0,0,g % Red
   if c=2 then gr.color 255,0,255,0,g % green
   if c=3 then gr.color 255,0,0,255,g % blue
   if c=4 then gr.color 255,255,255,0,g % yellow
   if c=5 then gr.color 255,0,255,255,g % cyan
   if c=6 then gr.color 255,255,0,255,g % magenta
   if c=7 then gr.color 255,192,192,192,g % gray
   if c=8 then gr.color 255,255,255,255,g % white
   if c=9 then gr.color 255,205,0,0,g % mediumblue
   if c=10 then gr.color 255,65,105,225,g % royalblue
   if c=11 then gr.color 255,255,215,0,g % gold
   if c=12 then gr.color 255,0,250,154,g % springgreen
   if c=13 then gr.color 255,220,20,60,g % crimson
fn.end

fn.def CurvyWindow(x,y,l,h,c)
   call Gcol(c,1)
   gr.rect r,x,y+h/40,l,h-h/40
   gr.rect r,x+h/40,y,l-h/40,h
   gr.circle c, x+h/40,y+h/40,h/40
   gr.circle c,l-h/40,y+h/40,h/40
   gr.circle c,x+h/40,h-h/40,h/40
   gr.circle c,l-h/40,h-h/40,h/40
fn.end

fn.def PrintTab(x,y,bgc,fgc,t$)
   x--
   gr.text.width t,t$
   gr.text.width w,"W"
   gr.text.height h,up,dn
   x*=w
   y*=abs(up)+dn
   if bgc>=0 then
     Call Gcol(bgc,1)
     gr.rect r,x,y+up,x+t,y+dn
   end if
   call Gcol(fgc,1)
   gr.text.draw d,x,y,t$
   fn.rtn d
fn.end

Rem End of Functions

Rem Main
console.title"Bouncing balls"

Rem Open Graphics
di_width = 1280 % set to my tablet
di_height = 800

gr.open 255,0,0,0 % Black
gr.orientation 0
pause 1000
gr.screen screenWidth,screenHeight
scale_x=screenWidth/di_width
scale_y=screenHeight/di_height
gr.scale scale_x,scale_y
gr.text.typeface 2
gr.text.size 27 % will give 80 x 26 chrs
wakelock 3

Rem Global Vars
maxBalls=100
lastBall=5
bb$=" bouncing ball"
dim mess[4]
method=1 % 'gr.modify' method

dim c[maxBalls] % circle colour
dim cn[maxBalls] %circle number
dim x[maxBalls] % circle y
dim y[maxBalls] % circle x
dim r[maxBalls] % radius
dim xd[maxBalls] % x direction and speed
dim yd[maxBalls] % y direction and speed

Rem SetUp Screen
gr.bitmap.create bgnBmp,1280,800
gr.bitmap.drawinto.start bgnBmp
   call CurvyWindow(1,1,di_width,di_height,11) % gold outer window
   call Gcol(0,1) % black full
   gr.rect nul,50,50,di_width-50,di_height-50 % black inner window
   call CurvyWindow(5,50,45,di_height-50,0) % black scroll bar on the left
gr.bitmap.drawinto.end
gr.bitmap.draw nul,bgnBmp,0,0
call Gcol(8,1) % white full
gr.rect ballRect,7,55+(di_height-110)*(101-lastBall)/100,43,di_height-54
call Gcol(0,1) % black full
gr.text.bold 1
e$=int$(lastBall)+bb$
if lastBall>1 then e$+="s"
gr.text.draw mess[1],500,35,e$
gr.text.bold 0
call PrintTab(3,25,-1,0,chr$(8598)+" drag to set nb of balls")
mess[2]=PrintTab(38,25,13,0," Pause ")
mess[3]=PrintTab(65,25,13,0," gr.modify  ")
gr.text.draw mess[4],1000,35,""
gr.bitmap.create myBmp,di_width-100,di_height-100
gr.bitmap.draw myPtr,myBmp,50,50
gr.hide myPtr

rem SetUp Arrays (Define Balls)
for s=1 to maxBalls
  c[s]=int(13 * rnd()+1) % color
  x[s]=int(800 * Rnd()+200) % x coordinate
  y[s]=int(600 * rnd()+100) % y coordinate
  r[s]=int(10 * rnd() + 30) % radius
  xd[s]=int(40 * rnd()-20) % x direction and speed
  yd[s]=int(40 * rnd()-20) % y direction and speed
  if xd[s]=0 then xd[s]=10
  if yd[s]=0 then yd[s]=-10
  call Gcol(c[s],1)
  gr.circle cn[s], x[s], y[s], r[s]
  if s>lastBall then gr.hide cn[s]
next

Rem Main Loop   
do
   t0=Clock()
   frame=0
   do
      if method=1 then % 'gr.modify' method
         for s=1 to lastBall
            let x[s]=x[s]+xd[s]
            let y[s]=y[s]+yd[s]
            gr.modify cn[s], "x",x[s], "y",y[s]
            if x[s]>1190 then
               let xd[s]=-xd[s]
            elseif x[s]<90 then
               let xd[s]=abs(xd[s])
            end if
            if y[s]>710 then
               let yd[s]=-yd[s]
            elseif y[s]<90 then
               let yd[s]=abs(yd[s])
            end if
         next
      else % method=2 'gr.bitmap.drawinto' method
         gr.bitmap.delete myBmp
         gr.bitmap.create myBmp,di_width-100,di_height-100
         gr.bitmap.drawinto.start myBmp
            for s=1 to lastBall
               let x[s]=x[s]+xd[s]
               let y[s]=y[s]+yd[s]
               call Gcol(c[s],1)
               gr.circle nul, x[s], y[s], r[s]
               if x[s]>di_width-140 then
                  let xd[s]=-xd[s]
               elseif x[s]<40 then
                  let xd[s]=abs(xd[s])
               end if
               if y[s]>di_height-140 then
                  let yd[s]=-yd[s]
               elseif y[s]<40 then
                  let yd[s]=abs(yd[s])
               end if
            next
         gr.bitmap.drawinto.end
         gr.modify myPtr,"bitmap",myBmp
      end if
      let frame=frame+1
      gr.modify mess[4],"text",int$(frame*1000/(Clock()-t0))+" fps"
      gr.render
      if Clock()-t0>2000 then
         frame=0
         t0=Clock()
      end if
      gr.touch touched,tx,ty
   until touched
   tx /= scale_x
   ty /= scale_y

   Rem Increase/decrease number of balls
   if tx>=7 & tx<=43 & ty>=45 & ty<=735 then
      ty-=10
      do
         s=min(100, 101-100*(ty-55)/(745-55)) % target number of balls
         if s<>lastBall then
            lastBall=s
            e$=int$(s)+bb$
            if s>1 then e$+="s"
            gr.modify mess[1],"text",e$
            gr.modify ballRect, "top", ty
            for s=1 to lastBall
               gr.show cn[s]
            next
            for s=lastBall+1 to maxBalls
               gr.hide cn[s]
            next
            gr.render
         end if
         gr.touch touched,tx,ty
      until !touched
   end if

   Rem Switch mode: gr.modify Vs gr.bitmap.drawinto
   if tx>1000 & tx<1250 & ty>700 & ty<800 & lastBall>1 then
      method=3-method
      if method=1 then e$=" gr.modify  " else e$="bmp.drawinto"
      gr.modify mess[3],"text",e$
      if method=1 then % 'gr.modify' method
         gr.hide myPtr
         for s=1 to lastBall
            gr.show cn[s]
         next
      else % method=2 'gr.bitmap.drawinto' method
         gr.show myPtr
         for s=1 to maxBalls
            gr.hide cn[s]
         next
      end if
      gr.render
      do
         gr.touch touched,tx,ty
      until !touched
   end if
   
   Rem Pause
   if tx>500 & tx<700 & ty>700 & ty<800 then
     gr.modify mess[2], "text", " Start "
     gr.render
     do
        gr.touch touched,tx,ty
     until !touched
     do
       gr.touch touched,tx,ty
       tx /= scale_x
       ty /= scale_y
     until  touched & tx>500 & tx<700 & ty>700 & ty<800
     gr.modify mess[2], "text", " Pause "
     gr.render
      do
         gr.touch touched,tx,ty
      until !touched
   end if

until 0
Rem end of main loop

onBackKey:
do
   Dialog.Message "Exit:", "Are you sure you want to leave Bouncing Balls", yn, "Yes","No"
   if yn=0 then tone 400,400
until yn>0
if yn=2 then back.resume
wakelock 5
gr.close
print
end"Thanks for trying Bouncing Balls Demo"

onError:
Rem Do nothing