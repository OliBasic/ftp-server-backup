!!
Talking Clock
Helmuth Porschen, Cyprus (HELPO)
Version 1.0.0
!!
! DEBUG.ON
! DEBUG.ECHO.ON

GR.OPEN 255,0,0,0
GR.ORIENTATION 0  % Force landscape
GR.SCREEN w, h
hs = 1
diff = h/18
rad = diff/2
hbasic = h/2 - 3*(diff*1.2)
DIM arw[4]
DIM arn[7,4]
DIM art[4,7,4]
ARRAY.LOAD aro[], 99, 99, 99, 99
arw[1] = w/2 - 8*(diff*1.2) - diff
arw[2] = w/2 - 4*(diff*1.2) - diff/2
arw[3] = w/2 + diff*1.2 + diff/2
arw[4] = w/2 + 5*(diff*1.2) + diff
GR.COLOR 255, 255, 0, 0, 1
FOR ind = 1 TO 4
 hact = hbasic
 FOR ind1 = 1 TO 7
  wact = arw[ind]
  FOR ind2 = 1 TO 4
   GR.CIRCLE cc, wact, hact, rad
   art[ind, ind1, ind2] = cc
   GR.HIDE art[ind, ind1, ind2]
   wact = wact + diff*1.2
  NEXT ind2
  hact = hact + diff*1.2
  wact = arw[ind]
 NEXT ind1
NEXT ind
wact = w/2
hact = h/2 - diff*1.2
GR.CIRCLE cd1, wact, hact, rad
GR.HIDE cd1
hact = h/2 + diff*1.2
GR.CIRCLE cd2, wact, hact, rad
GR.HIDE cd2
GR.RENDER
DO
 TIME yr$, mo$, dy$, hour$, min$, sec$

 ! hour$="11" % for test only
 ! min$="11"  % for test only

 arw[4] = VAL(RIGHT$(min$, 1))
 txt$ = LEFT$(min$, LEN(min$)-1)
 arw[3] = VAL(txt$)
 arw[2] = VAL(RIGHT$(hour$, 1))
 txt$ = LEFT$(hour$, LEN(hour$)-1)
 arw[1] = VAL(txt$)

 FOR ind = 1 TO 4
  IF arw[ind] <> aro[ind]
   aro[ind] = arw[ind]
   GOSUB Numbers
  END IF
 NEXT ind
 IF hs = 0
  GR.HIDE cd1
  GR.HIDE cd2
  hs = 1
 ELSE
  GR.SHOW cd1
  GR.SHOW cd2
  hs = 0
 END IF
 GR.RENDER
 FOR ind = 1 TO 99
  GR.TOUCH touched, tx, ty
  IF touched
   txt$ = "The time is now "
   IF arw[1]=0
    txt$ = txt$ + RIGHT$(hour$, 1) + " "
   ELSE
    txt$ = txt$ + hour$ + " "
   END IF
   SW.BEGIN arw[3]
    SW.CASE 0
     IF arw[4] = 0
      txt$ = txt$ + "o'clock"
     ELSE
      txt$ = txt$ + "o " + RIGHT$(min$, 1)
     END if
     SW.BREAK
    SW.DEFAULT
     txt$ = txt$ + min$
     SW.BREAK
   SW.END
   TTS.INIT
   TTS.SPEAK txt$
   touched = 0
  END if
  PAUSE 10
 NEXT ind
UNTIL terminate > 0
END

Numbers:
FOR ind1 = 1 TO 7
 FOR ind2 = 1 TO 4
  GR.HIDE art[ind, ind1, ind2]
 NEXT ind2
NEXT ind1
SW.BEGIN arw[ind]
 SW.CASE 0
  GR.SHOW art[ind, 1, 2]
  GR.SHOW art[ind, 1, 3]
  GR.SHOW art[ind, 2, 1]
  GR.SHOW art[ind, 2, 4]
  GR.SHOW art[ind, 3, 1]
  GR.SHOW art[ind, 3, 4]
  GR.SHOW art[ind, 4, 1]
  GR.SHOW art[ind, 4, 4]
  GR.SHOW art[ind, 5, 1]
  GR.SHOW art[ind, 5, 4]
  GR.SHOW art[ind, 6, 1]
  GR.SHOW art[ind, 6, 4]
  GR.SHOW art[ind, 7, 2]
  GR.SHOW art[ind, 7, 3]
  SW.BREAK
 SW.CASE 1
  GR.SHOW art[ind, 1, 3]
  GR.SHOW art[ind, 2, 2]
  GR.SHOW art[ind, 2, 3]
  GR.SHOW art[ind, 3, 1]
  GR.SHOW art[ind, 3, 3]
  GR.SHOW art[ind, 4, 3]
  GR.SHOW art[ind, 5, 3]
  GR.SHOW art[ind, 6, 3]
  GR.SHOW art[ind, 7, 1]
  GR.SHOW art[ind, 7, 2]
  GR.SHOW art[ind, 7, 3]
  GR.SHOW art[ind, 7, 4]
  SW.BREAK
 SW.CASE 2
  GR.SHOW art[ind, 1, 2]
  GR.SHOW art[ind, 1, 3]
  GR.SHOW art[ind, 2, 1]
  GR.SHOW art[ind, 2, 4]
  GR.SHOW art[ind, 3, 4]
  GR.SHOW art[ind, 4, 3]
  GR.SHOW art[ind, 5, 2]
  GR.SHOW art[ind, 6, 1]
  GR.SHOW art[ind, 7, 1]
  GR.SHOW art[ind, 7, 2]
  GR.SHOW art[ind, 7, 3]
  GR.SHOW art[ind, 7, 4]
  SW.BREAK
 SW.CASE 3
  GR.SHOW art[ind, 1, 1]
  GR.SHOW art[ind, 1, 2]
  GR.SHOW art[ind, 1, 3]
  GR.SHOW art[ind, 2, 4]
  GR.SHOW art[ind, 3, 4]
  GR.SHOW art[ind, 4, 2]
  GR.SHOW art[ind, 4, 3]
  GR.SHOW art[ind, 4, 3]
  GR.SHOW art[ind, 5, 4]
  GR.SHOW art[ind, 6, 4]
  GR.SHOW art[ind, 7, 1]
  GR.SHOW art[ind, 7, 2]
  GR.SHOW art[ind, 7, 3]
  SW.BREAK
 SW.CASE 4
  GR.SHOW art[ind, 1, 4]
  GR.SHOW art[ind, 2, 3]
  GR.SHOW art[ind, 2, 4]
  GR.SHOW art[ind, 3, 2]
  GR.SHOW art[ind, 3, 4]
  GR.SHOW art[ind, 4, 1]
  GR.SHOW art[ind, 4, 4]
  GR.SHOW art[ind, 5, 1]
  GR.SHOW art[ind, 5, 2]
  GR.SHOW art[ind, 5, 3]
  GR.SHOW art[ind, 5, 4]
  GR.SHOW art[ind, 6, 4]
  GR.SHOW art[ind, 7, 4]
  SW.BREAK
 SW.CASE 5
  GR.SHOW art[ind, 1, 1]
  GR.SHOW art[ind, 1, 2]
  GR.SHOW art[ind, 1, 3]
  GR.SHOW art[ind, 1, 4]
  GR.SHOW art[ind, 2, 1]
  GR.SHOW art[ind, 3, 1]
  GR.SHOW art[ind, 3, 2]
  GR.SHOW art[ind, 3, 3]
  GR.SHOW art[ind, 4, 4]
  GR.SHOW art[ind, 5, 4]
  GR.SHOW art[ind, 6, 4]
  GR.SHOW art[ind, 7, 1]
  GR.SHOW art[ind, 7, 2]
  GR.SHOW art[ind, 7, 3]
  SW.BREAK
 SW.CASE 6
  GR.SHOW art[ind, 1, 4]
  GR.SHOW art[ind, 2, 3]
  GR.SHOW art[ind, 3, 2]
  GR.SHOW art[ind, 3, 3]
  GR.SHOW art[ind, 4, 1]
  GR.SHOW art[ind, 4, 4]
  GR.SHOW art[ind, 5, 1]
  GR.SHOW art[ind, 5, 4]
  GR.SHOW art[ind, 6, 1]
  GR.SHOW art[ind, 6, 4]
  GR.SHOW art[ind, 7, 2]
  GR.SHOW art[ind, 7, 3]
  SW.BREAK
 SW.CASE 7
  GR.SHOW art[ind, 1, 1]
  GR.SHOW art[ind, 1, 2]
  GR.SHOW art[ind, 1, 3]
  GR.SHOW art[ind, 1, 4]
  GR.SHOW art[ind, 2, 4]
  GR.SHOW art[ind, 3, 4]
  GR.SHOW art[ind, 4, 3]
  GR.SHOW art[ind, 5, 2]
  GR.SHOW art[ind, 6, 1]
  GR.SHOW art[ind, 7, 1]
  SW.BREAK
 SW.CASE 8
  GR.SHOW art[ind, 1, 2]
  GR.SHOW art[ind, 1, 3]
  GR.SHOW art[ind, 2, 1]
  GR.SHOW art[ind, 2, 4]
  GR.SHOW art[ind, 3, 1]
  GR.SHOW art[ind, 3, 4]
  GR.SHOW art[ind, 4, 2]
  GR.SHOW art[ind, 4, 3]
  GR.SHOW art[ind, 5, 1]
  GR.SHOW art[ind, 5, 4]
  GR.SHOW art[ind, 6, 1]
  GR.SHOW art[ind, 6, 4]
  GR.SHOW art[ind, 7, 2]
  GR.SHOW art[ind, 7, 3]
  SW.BREAK
 SW.CASE 9
  GR.SHOW art[ind, 1, 2]
  GR.SHOW art[ind, 1, 3]
  GR.SHOW art[ind, 2, 1]
  GR.SHOW art[ind, 2, 4]
  GR.SHOW art[ind, 3, 1]
  GR.SHOW art[ind, 3, 4]
  GR.SHOW art[ind, 4, 1]
  GR.SHOW art[ind, 4, 4]
  GR.SHOW art[ind, 5, 2]
  GR.SHOW art[ind, 5, 3]
  GR.SHOW art[ind, 6, 2]
  GR.SHOW art[ind, 7, 1]
  SW.BREAK
SW.END
RETURN
ONBACKKEY:
terminate = 1
BACK.RESUME
