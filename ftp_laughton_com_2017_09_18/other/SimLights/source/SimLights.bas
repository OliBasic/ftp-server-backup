GR.OPEN 255,128,128,128,0,1
GR.SCREEN w,h
ScaleX=600
ScaleY=h/w*ScaleX
sx=w/ScaleX
sy=h/ScaleY
GR.SCALE sx,sy
DataPath$="../../SimLights/data/"
! Minimum NumCars=12
NumCars=40
ARRAY.LOAD AllColors$[],"0,0,0","255,255,255","255,0,0","0,255,0","0,0,255","255,255,0","0,255,255","255,0,255","192,192,192","128,128,128","128,0,0","128,128,0","0,128,0","128,0,128","0,128,128","0,0,128"
DIM GreenL[4],OrangeL[4],RedL[4]
DIM WestX[382,3],WestY[382,3],WestR[382,3]
DIM NorthX[382,3],NorthY[382,3],NorthR[382,3]
DIM EastX[382,3],EastY[382,3],EastR[382,3]
DIM SouthX[382,3],SouthY[382,3],SouthR[382,3]
DIM Cbmp[NumCars],RotCar[NumCars],Car[NumCars],BlinkLeftCar[NumCars],BlinkRightCar[NumCars],CarDir[NumCars],CarBlink[NumCars]
DIM Counter[NumCars],GoCar[NumCars],CarDist[NumCars],CarStart[NumCars]
DIM WT[4],Klok[4],Delay[4]
GR.BITMAP.LOAD TheCar,DataPath$+"Car.png"
GR.CLS
GR.SET.STROKE 8
GR.TEXT.ALIGN 2
GR.TEXT.SIZE 40
GR.COLOR 255,255,255,255,1
! Draw Streets
GR.BITMAP.LOAD TL,DataPath$+"LeftTop.png"
GR.BITMAP.LOAD TR,DataPath$+"RightTop.png"
GR.BITMAP.LOAD BL,DataPath$+"LeftBottom.png"
GR.BITMAP.LOAD BR,DataPath$+"RightBottom.png"
GR.BITMAP.DRAW g,TL,0,0
GR.BITMAP.DRAW g,TR,335,0
GR.BITMAP.DRAW g,BL,0,335
GR.BITMAP.DRAW g,BR,335,335
GR.LINE g,0,604,600,604
GR.BITMAP.CREATE Street,600,600
GR.BITMAP.DRAWINTO.START Street
	GR.LINE g,0,260,200,260
	GR.LINE g,260,0,260,200
	GR.LINE g,340,0,340,200
	GR.LINE g,400,260,600,260
	GR.LINE g,400,340,600,340
	GR.LINE g,340,400,340,600
	GR.LINE g,260,400,260,600
	GR.LINE g,0,340,200,340
	GR.COLOR 255,255,255,255,0
	GR.ARC g,120,120,260,260,0,90,0
	GR.ARC g,340,120,480,260,90,90,0
	GR.ARC g,340,340,480,480,180,90,0
	GR.ARC g,120,340,260,480,270,90,0
	GR.COLOR 255,128,128,128,1
	GR.BITMAP.FILL Street,250,300
	GR.COLOR 255,255,255,255,1
	GR.SET.STROKE 3
	FOR i=15 TO 195 STEP 30
		GR.LINE g,i,300,i+15,300
		GR.LINE g,i+390,300,i+375,300
		GR.LINE g,300,i,300,i+15
		GR.LINE g,300,i+390,300,i+375
	NEXT i
	GR.LINE g,220,305,220,330
	GR.LINE g,270,220,295,220
	GR.LINE g,380,270,380,295
	GR.LINE g,305,380,330,380
GR.BITMAP.DRAWINTO.END
GR.BITMAP.DRAW g,Street,0,0

GR.COLOR 70,0,0,255,1
GR.TEXT.DRAW g,130,295,"West Street"
GR.ROTATE.START 90,305,130
	GR.TEXT.DRAW g,305,130,"North Street"
GR.ROTATE.END
GR.ROTATE.START 180,470,305
	GR.TEXT.DRAW g,470,305,"East Street"
GR.ROTATE.END
GR.ROTATE.START 270,295,470
	GR.TEXT.DRAW g,295,470,"South Street"
GR.ROTATE.END

GR.COLOR 80,0,255,0,1
GR.PAINT.GET GreenOff
GR.COLOR 255,0,255,0,1
GR.PAINT.GET GreenOn
GR.COLOR 100,255,128,0,1
GR.PAINT.GET OrangeOff
GR.COLOR 255,255,128,0,1
GR.PAINT.GET OrangeOn
GR.COLOR 100,255,0,0,1
GR.PAINT.GET RedOff
GR.COLOR 255,255,0,0,1
GR.PAINT.GET RedOn

GR.COLOR 255,0,0,0,1
GR.RECT g,210,348,230,362
GR.CIRCLE g,230,355,7
GR.CIRCLE g,210,355,7
GR.RECT g,223,225,237,245
GR.CIRCLE g,230,225,7
GR.CIRCLE g,230,245,7
GR.RECT g,370,238,390,252
GR.CIRCLE g,370,245,7
GR.CIRCLE g,390,245,7
GR.RECT g,363,355,377,375
GR.CIRCLE g,370,355,7
GR.CIRCLE g,370,375,7
GR.COLOR 255,0,255,0,1
GR.CIRCLE GreenL[1],210,355,4
GR.CIRCLE GreenL[2],230,225,4
GR.CIRCLE GreenL[3],390,245,4
GR.CIRCLE GreenL[4],370,375,4
GR.COLOR 255,255,128,0,1
GR.CIRCLE OrangeL[1],220,355,4
GR.CIRCLE OrangeL[2],230,235,4
GR.CIRCLE OrangeL[3],380,245,4
GR.CIRCLE OrangeL[4],370,365,4
GR.COLOR 255,255,0,0,1
GR.CIRCLE RedL[1],230,355,4
GR.CIRCLE RedL[2],230,245,4
GR.CIRCLE RedL[3],370,245,4
GR.CIRCLE RedL[4],370,355,4
! All lights red
FOR i=1 TO 4
	GR.MODIFY GreenL[i],"paint",GreenOff
	GR.MODIFY OrangeL[i],"paint",OrangeOff
	GR.MODIFY RedL[i],"paint",RedOn
NEXT i

GR.COLOR 0,255,255,255,0
GR.RECT XBox,220,220,380,380
GR.COLOR 255,255,255,255,1
GR.TEXT.ALIGN 1
GR.TEXT.SIZE 20
GR.TEXT.DRAW g,30,630,"Cars from West"
GR.TEXT.DRAW CW,200,630,"0"
GR.TEXT.DRAW g,30,650,"Cars from North"
GR.TEXT.DRAW CN,200,650,"0"
GR.TEXT.DRAW g,30,670,"Cars from East"
GR.TEXT.DRAW CE,200,670,"0"
GR.TEXT.DRAW g,30,690,"Cars from South"
GR.TEXT.DRAW CS,200,690,"0"
GR.TEXT.ALIGN 2
! Clip region
GR.CLIP g,0,0,600,600

Count=0
FOR i=-74 TO 260 STEP 2
	Count=Count+1
	! route 1 from West to North
	WestX[Count,1]=i:WestY[Count,1]=317:WestR[Count,1]=0
	! route 2 from West to East
	WestX[Count,2]=i:WestY[Count,2]=317:WestR[Count,2]=0
	! route 3 from West to South
	WestX[Count,3]=i-60:WestY[Count,3]=317:WestR[Count,3]=0
	! route 1 from North to East
	NorthX[Count,1]=283:NorthY[Count,1]=i:NorthR[Count,1]=90
	! route 2 from North to South
	NorthX[Count,2]=283:NorthY[Count,2]=i:NorthR[Count,2]=90
	! route 3 from North to West
	NorthX[Count,3]=283:NorthY[Count,3]=i-60:NorthR[Count,3]=90
	! route 1 from East to South
	EastX[Count,1]=600-i:EastY[Count,1]=282:EastR[Count,1]=180
	! route 2 from East to West
	EastX[Count,2]=600-i:EastY[Count,2]=282:EastR[Count,2]=180
	! route 3 from East to North
	EastX[Count,3]=660-i:EastY[Count,3]=282:EastR[Count,3]=180
	! route 1 from South to West
	SouthX[Count,1]=317:SouthY[Count,1]=600-i:SouthR[Count,1]=270
	! route 2 from South to North
	SouthX[Count,2]=317:SouthY[Count,2]=600-i:SouthR[Count,2]=270
	! route 3 from South to East
	SouthX[Count,3]=317:SouthY[Count,3]=660-i:SouthR[Count,3]=270
NEXT i
FOR angle=90 TO 0 STEP -2
	Count=Count+1
	! route 1 from West to North
	WestX[Count,1]=57*COS(angle*PI()/180)+260:WestY[Count,1]=57*SIN(angle*PI()/180)+260:WestR[Count,1]=-(90-angle)
	! route 2 from West to East
	WestX[Count,2]=260+(90-angle):WestY[Count,2]=317:WestR[Count,2]=0
	! route 3 from West to South
	WestX[Count,3]=83*COS(-angle*PI()/180)+200:WestY[Count,3]=83*SIN(-angle*PI()/180)+400:WestR[Count,3]=(90-angle)
	! route 1 from North to East
	NorthX[Count,1]=57*COS((angle+90)*PI()/180)+340:NorthY[Count,1]=57*SIN((angle+90)*PI()/180)+260:NorthR[Count,1]=angle
	! route 2 from North to South
	NorthX[Count,2]=283:NorthY[Count,2]=260+(90-angle):NorthR[Count,2]=90
	! route 3 from North to West
	NorthX[Count,3]=83*COS((90-angle)*PI()/180)+200:NorthY[Count,3]=83*SIN((90-angle)*PI()/180)+200:NorthR[Count,3]=180-angle
	! route 1 from East to South
	EastX[Count,1]=57*COS((angle+180)*PI()/180)+340:EastY[Count,1]=57*SIN((angle+180)*PI()/180)+340:EastR[Count,1]=90+angle
	! route 2 from East to West
	EastX[Count,2]=340-(90-angle):EastY[Count,2]=282:EastR[Count,2]=180
	! route 3 from East to North
	EastX[Count,3]=83*COS(-(180+angle)*PI()/180)+400:EastY[Count,3]=83*SIN(-(180+angle)*PI()/180)+200:EastR[Count,3]=180+(90-angle)
	! route 1 from South to West
	SouthX[Count,1]=57*COS(-(90-angle)*PI()/180)+260:SouthY[Count,1]=57*SIN(-(90-angle)*PI()/180)+340:SouthR[Count,1]=angle+180
	! route 2 from South to North
	SouthX[Count,2]=317:SouthY[Count,2]=248+angle:SouthR[Count,2]=270
	! route 3 from South to East
	SouthX[Count,3]=83*COS((270-angle)*PI()/180)+400:SouthY[Count,3]=83*SIN((270-angle)*PI()/180)+400:SouthR[Count,3]=360-angle
NEXT angle
FOR i=260 TO -74 STEP -2
	Count=Count+1
	! route 1 from West to North
	WestX[Count,1]=317:WestY[Count,1]=i:WestR[Count,1]=-90
	! route 2 from West to East
	WestX[Count,2]=350+260-i:WestY[Count,2]=317:WestR[Count,2]=0
	! route 3 from West to South
	WestX[Count,3]=282:WestY[Count,3]=660-i:WestR[Count,3]=90
	! route 1 from North to East
	NorthX[Count,1]=600-i:NorthY[Count,1]=317:NorthR[Count,1]=0
	! route 2 from North to South
	NorthX[Count,2]=283:NorthY[Count,2]=610-i:NorthR[Count,2]=90
	! route 3 from North to West
	NorthX[Count,3]=i-60:NorthY[Count,3]=282:NorthR[Count,3]=180
	! route 1 from East to South
	EastX[Count,1]=283:EastY[Count,1]=600-i:EastR[Count,1]=90
	! route 2 from East to west
	EastX[Count,2]=i-10:EastY[Count,2]=282:EastR[Count,2]=180
	! route 3 from East to North
	EastX[Count,3]=317:EastY[Count,3]=i-60:EastR[Count,3]=270
	! route 1 from South to West
	SouthX[Count,1]=i:SouthY[Count,1]=283:SouthR[Count,1]=180
	! route 2 from South to North
	SouthX[Count,2]=317:SouthY[Count,2]=i-14:SouthR[Count,2]=270
	! route 3 from South to East
	SouthX[Count,3]=660-i:SouthY[Count,3]=317:SouthR[Count,3]=0
NEXT i

FOR i=1 TO NumCars
	GR.BITMAP.CREATE Cbmp[i],75,35
	GR.BITMAP.DRAWINTO.START Cbmp[i]
		GR.BITMAP.DRAW g,TheCar,0,0
	GR.BITMAP.DRAWINTO.END
	GR.ROTATE.START 0,-75,317-35/2,RotCar[i]
		GR.BITMAP.DRAW Car[i],Cbmp[i],-75,317-35/2
		GR.COLOR 255,255,128,0,1
		GR.CIRCLE BlinkLeftCar[i],75/4,317-35/2,5
		GR.CIRCLE BlinkRightCar[i],75/4,+35/2,5
	GR.ROTATE.END
	GR.HIDE BlinkLeftCar[i]
	GR.HIDE BlinkRightCar[i]
NEXT i

! MAIN LOOP
	! Make colors random and unique
	ARRAY.SHUFFLE AllColors$[]
	FOR i=1 TO NumCars
		! Get car color (1 of 16)
		GR.COLOR 255,VAL(WORD$(AllColors$[MOD(i-1,16)+1],1,",")),VAL(WORD$(AllColors$[MOD(i-1,16)+1],2,",")),VAL(WORD$(AllColors$[MOD(i-1,16)+1],3,",")),1
		GR.BITMAP.FILL Cbmp[i],60,17
		! Dir 1,2,3 = left,straight,right
		CarDir[i]=FLOOR(3*RND()+1)
		IF CarDir[i]=1 THEN CarBlink[i]=BlinkLeftCar[i]
		IF CarDir[i]=3 THEN CarBlink[i]=BlinkRightCar[i]
		Counter[i]=1
		GoCar[i]=0
		CarDist[i]=FLOOR(21*RND())
		CarStart[i]=FLOOR(81*RND())
	NEXT i
	! Determine how many cars from each direction (order West, North, East, South)
	WestStart=1
	WestEind=INT(NumCars/4+INT(RND()*NumCars/3-NumCars/6))
	NorthStart=WestEind+1
	NorthEind=INT(2*NumCars/4+INT(RND()*NumCars/3-NumCars/6))
	EastStart=NorthEind+1
	EastEind=INT(3*NumCars/4+INT(RND()*NumCars/3-NumCars/6))
	SouthStart=EastEind+1
	SouthEind=NumCars
	! Show numbers
	GR.MODIFY CW,"text",INT$(WestEind-WestStart+1)
	GR.MODIFY CN,"text",INT$(NorthEind-NorthStart+1)
	GR.MODIFY CE,"text",INT$(EastEind-EastStart+1)
	GR.MODIFY CS,"text",INT$(SouthEind-SouthStart+1)
	GoCar[WestStart]=1:GoCar[NorthStart]=1:GoCar[EastStart]=1:GoCar[SouthStart]=1
	LCar01=WestStart:LCar02=NorthStart:LCar03=EastStart:LCar04=SouthStart
	RCar01=WestStart:RCar02=NorthStart:RCar03=EastStart:RCar04=SouthStart
	ARRAY.FILL Klok[],0
	ARRAY.FILL Delay[],1
	WaitGreen=0
	RouteDone=0
	GR.RENDER

	TIMER.SET 500
	DO

		FOR i=WestStart TO WestEind-1
			! Drive up to car in front
			IF Delay[1]=1 & WestX[Counter[i+1],CarDir[i+1]]+70+CarDist[i]<WestX[Counter[i],CarDir[i]] & WestX[Counter[i],CarDir[i]]<220 & GoCar[i]=1 THEN Counter[i+1]=Counter[i+1]+1:RouteDone=RouteDone+1
			! Start next car
			IF WestX[Counter[i],CarDir[i]]>CarDist[i]+CarStart[i] & GoCar[i+1]=0 THEN GoCar[i+1]=1
			! Next car for a stop
			IF WestX[Counter[i],CarDir[i]]>180 THEN LCar01=i+1
		NEXT i
		FOR i=WestStart TO WestEind
			IF WestX[Counter[i],CarDir[i]]>180 THEN RCar01=i
			IF Counter[i]=382 THEN GoCar[i]=0
			! Drive up to light
			IF Delay[1]=1 & i=LCar01 & WestX[Counter[i],CarDir[i]]<180 THEN Counter[i]=Counter[i]+1:RouteDone=RouteDone+1
			IF Delay[1]=0 & GoCar[i]=1 THEN Counter[i]=Counter[i]+1:RouteDone=RouteDone+1
			IF Delay[1]=1 & WestX[Counter[i],CarDir[i]]>180 & GoCar[i]=1 THEN Counter[i]=Counter[i]+1:RouteDone=RouteDone+1
			GR.MODIFY Car[i],"x",WestX[Counter[i],CarDir[i]]-75/2
			GR.MODIFY Car[i],"y",WestY[Counter[i],CarDir[i]]-35/2
			GR.MODIFY RotCar[i],"angle",WestR[Counter[i],CarDir[i]]
			GR.MODIFY RotCar[i],"x",WestX[Counter[i],CarDir[i]]
			GR.MODIFY RotCar[i],"y",WestY[Counter[i],CarDir[i]]
			GR.MODIFY BlinkLeftCar[i],"x",WestX[Counter[i],CarDir[i]]+75/4
			GR.MODIFY BlinkLeftCar[i],"y",WestY[Counter[i],CarDir[i]]-35/2
			GR.MODIFY BlinkRightCar[i],"x",WestX[Counter[i],CarDir[i]]+75/4
			GR.MODIFY BlinkRightCar[i],"y",WestY[Counter[i],CarDir[i]]+35/2
		NEXT i

		FOR i=NorthStart TO NorthEind-1
			! Drive up to car in front
			IF Delay[2]=1 & NorthY[Counter[i+1],CarDir[i+1]]+70+CarDist[i]<NorthY[Counter[i],CarDir[i]] & NorthY[Counter[i],CarDir[i]]<220 & GoCar[i]=1 THEN Counter[i+1]=Counter[i+1]+1:RouteDone=RouteDone+1
			! Start next car
			IF NorthY[Counter[i],CarDir[i]]>CarDist[i]+CarStart[i] & GoCar[i+1]=0 THEN GoCar[i+1]=1
			! Next car for a stop
			IF NorthY[Counter[i],CarDir[i]]>180 THEN LCar02=i+1
		NEXT i
		FOR i=NorthStart TO NorthEind
			IF NorthY[Counter[i],CarDir[i]]>180 THEN RCar02=i
			IF Counter[i]=382 THEN GoCar[i]=0
			! Drive up to light
			IF Delay[2]=1 & i=LCar02 & NorthY[Counter[i],CarDir[i]]<180 THEN Counter[i]=Counter[i]+1:RouteDone=RouteDone+1
			IF Delay[2]=0 & GoCar[i]=1 THEN Counter[i]=Counter[i]+1:RouteDone=RouteDone+1
			IF Delay[2]=1 & NorthY[Counter[i],CarDir[i]]>180 & GoCar[i]=1 THEN Counter[i]=Counter[i]+1:RouteDone=RouteDone+1
			GR.MODIFY Car[i],"x",NorthX[Counter[i],CarDir[i]]-75/2
			GR.MODIFY Car[i],"y",NorthY[Counter[i],CarDir[i]]-35/2
			GR.MODIFY RotCar[i],"angle",NorthR[Counter[i],CarDir[i]]
			GR.MODIFY RotCar[i],"x",NorthX[Counter[i],CarDir[i]]
			GR.MODIFY RotCar[i],"y",NorthY[Counter[i],CarDir[i]]
			GR.MODIFY BlinkLeftCar[i],"x",NorthX[Counter[i],CarDir[i]]+75/4
			GR.MODIFY BlinkLeftCar[i],"y",NorthY[Counter[i],CarDir[i]]-35/2
			GR.MODIFY BlinkRightCar[i],"x",NorthX[Counter[i],CarDir[i]]+75/4
			GR.MODIFY BlinkRightCar[i],"y",NorthY[Counter[i],CarDir[i]]+35/2
		NEXT i

		FOR i=EastStart TO EastEind-1
			! Drive up to car in front
			IF Delay[3]=1 & EastX[Counter[i+1],CarDir[i+1]]>EastX[Counter[i],CarDir[i]]+70+CarDist[i] & EastX[Counter[i+1],CarDir[i+1]]>420 & GoCar[i+1]=1 THEN Counter[i+1]=Counter[i+1]+1:RouteDone=RouteDone+1
			! Start next car
			IF EastX[Counter[i],CarDir[i]]<500+CarDist[i]+CarStart[i] & GoCar[i+1]=0 THEN GoCar[i+1]=1
			! Next car for a stop
			IF EastX[Counter[i],CarDir[i]]<420 THEN LCar03=i+1
		NEXT i
		FOR i=EastStart TO EastEind
			IF EastX[Counter[i],CarDir[i]]<420 THEN RCar03=i
			IF Counter[i]=382 THEN GoCar[i]=0
			! Drive up to light
			IF Delay[3]=1 & i=LCar03 & EastX[Counter[i],CarDir[i]]>420 THEN Counter[i]=Counter[i]+1:RouteDone=RouteDone+1
			IF Delay[3]=0 & GoCar[i]=1 THEN Counter[i]=Counter[i]+1:RouteDone=RouteDone+1
			IF Delay[3]=1 & EastX[Counter[i],CarDir[i]]<420 & GoCar[i]=1 THEN Counter[i]=Counter[i]+1:RouteDone=RouteDone+1
			GR.MODIFY Car[i],"x",EastX[Counter[i],CarDir[i]]-75/2
			GR.MODIFY Car[i],"y",EastY[Counter[i],CarDir[i]]-35/2
			GR.MODIFY RotCar[i],"angle",EastR[Counter[i],CarDir[i]]
			GR.MODIFY RotCar[i],"x",EastX[Counter[i],CarDir[i]]
			GR.MODIFY RotCar[i],"y",EastY[Counter[i],CarDir[i]]
			GR.MODIFY BlinkLeftCar[i],"x",EastX[Counter[i],CarDir[i]]+75/4
			GR.MODIFY BlinkLeftCar[i],"y",EastY[Counter[i],CarDir[i]]-35/2
			GR.MODIFY BlinkRightCar[i],"x",EastX[Counter[i],CarDir[i]]+75/4
			GR.MODIFY BlinkRightCar[i],"y",EastY[Counter[i],CarDir[i]]+35/2
		NEXT i
		FOR i=SouthStart TO SouthEind-1
			! Drive up to car in front
			IF Delay[4]=1 & SouthY[Counter[i+1],CarDir[i+1]]>SouthY[Counter[i],CarDir[i]]+70+CarDist[i] & SouthY[Counter[i+1],CarDir[i+1]]>420 & GoCar[i+1]=1 THEN Counter[i+1]=Counter[i+1]+1:RouteDone=RouteDone+1
			! Start next car
			IF SouthY[Counter[i],CarDir[i]]<500+CarDist[i]+CarStart[i] & GoCar[i+1]=0 THEN GoCar[i+1]=1
			! Next car for a stop
			IF SouthY[Counter[i],CarDir[i]]<420 THEN LCar04=i+1
		NEXT i
		FOR i=SouthStart TO SouthEind
			IF SouthY[Counter[i],CarDir[i]]<420 THEN RCar04=i
			IF Counter[i]=382 THEN GoCar[i]=0
			! Drive up to light
			IF Delay[4]=1 & i=LCar04 & SouthY[Counter[i],CarDir[i]]>420 THEN Counter[i]=Counter[i]+1:RouteDone=RouteDone+1
			IF Delay[4]=0 & GoCar[i]=1 THEN Counter[i]=Counter[i]+1:RouteDone=RouteDone+1
			IF Delay[4]=1 & SouthY[Counter[i],CarDir[i]]<420 & GoCar[i]=1 THEN Counter[i]=Counter[i]+1:RouteDone=RouteDone+1
			GR.MODIFY Car[i],"x",SouthX[Counter[i],CarDir[i]]-75/2
			GR.MODIFY Car[i],"y",SouthY[Counter[i],CarDir[i]]-35/2
			GR.MODIFY RotCar[i],"angle",SouthR[Counter[i],CarDir[i]]
			GR.MODIFY RotCar[i],"x",SouthX[Counter[i],CarDir[i]]
			GR.MODIFY RotCar[i],"y",SouthY[Counter[i],CarDir[i]]
			GR.MODIFY BlinkLeftCar[i],"x",SouthX[Counter[i],CarDir[i]]+75/4
			GR.MODIFY BlinkLeftCar[i],"y",SouthY[Counter[i],CarDir[i]]-35/2
			GR.MODIFY BlinkRightCar[i],"x",SouthX[Counter[i],CarDir[i]]+75/4
			GR.MODIFY BlinkRightCar[i],"y",SouthY[Counter[i],CarDir[i]]+35/2
		NEXT i

		IF WaitGreen=1 THEN GOSUB SetGreen
		GR.TOUCH Touched,x,y
		IF Touched THEN
			x/=sx:y/=sy
			IF x<300 & y>300 & y<450 THEN LightOn=1:GOSUB SetOrange
			IF x<300 & y>450 & y<600 THEN LightOff=1:GOSUB SetGreen
			IF x<300 & y>0 & y<150 THEN LightOn=2:GOSUB SetOrange
			IF x<300 & y>150 & y<300 THEN LightOff=2:GOSUB SetGreen
			IF x>300 & y>0 & y<150 THEN LightOn=3:GOSUB SetOrange
			IF x>300 & y>150 & y<300 THEN LightOff=3:GOSUB SetGreen
			IF x>300 & y>300 & y<450 THEN LightOn=4:GOSUB SetOrange
			IF x>300 & y>450 & y<600 THEN LightOff=4:GOSUB SetGreen
		ENDIF
		FOR i=1 TO 4
			IF CLOCK()>WT[i]+2000 & Klok[i]=1 THEN
				GR.MODIFY OrangeL[i],"paint",OrangeOff
				GR.MODIFY RedL[i],"paint",RedOn
				Klok[i]=0
			ENDIF
		NEXT i

		GR.RENDER
	UNTIL RouteDone=NumCars*381
	TIMER.CLEAR
! MAIN LOOP


GR.COLOR 255,255,0,0,1
GR.TEXT.DRAW g,300,300,"The END"
GR.RENDER

GOSUB GetTouch
EXIT

GetTouch:
	DO
		GR.TOUCH Touched,x,y
	UNTIL Touched
	DO
		GR.TOUCH Touched,x,y
	UNTIL !Touched
	x/=sx:y/=sy
RETURN
SetOrange:
	GR.MODIFY GreenL[LightOn],"paint",GreenOff
	GR.MODIFY OrangeL[LightOn],"paint",OrangeOn
	GR.MODIFY RedL[LightOn],"paint",RedOff
	WT[LightOn]=CLOCK()
	Klok[LightOn]=1
	Delay[LightOn]=1
RETURN
SetGreen:
	! Crossing free?
	IF GR_COLLISION(XBox,Car[RCar01])+GR_COLLISION(XBox,Car[RCar02])+GR_COLLISION(XBox,Car[RCar03])+GR_COLLISION(XBox,Car[RCar04])>0 THEN
		WaitGreen=1
	ELSE
		GR.MODIFY GreenL[LightOff],"paint",GreenOn
		GR.MODIFY RedL[LightOff],"paint",RedOff
		Delay[LightOff]=0
		WaitGreen=0
	ENDIF
RETURN
ONTIMER:
	FOR qt=1 TO NumCars
		IF Counter[qt]<220 & CarDir[qt]<>2 THEN GR.SHOW.TOGGLE CarBlink[qt] ELSE GR.HIDE CarBlink[qt]
	NEXT qt
TIMER.RESUME