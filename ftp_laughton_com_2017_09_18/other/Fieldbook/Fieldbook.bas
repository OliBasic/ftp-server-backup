Dim m$[12]

m$[1] = "January"
m$[2] = "February"
m$[3] = "March"
m$[4] = "April"
m$[5] = "May"
m$[6] = "June"
m$[7] = "July"
m$[8] = "August"
m$[9] = "September"
m$[10] = "October"
m$[11] = "November"
m$[12] = "December"


file.exists fl1,"dlevel.txt"
if fl1=0 then
Text.open w,fl1,"dlevel.txt"
text.writeln fl1,"------ Diff Level Data 
------"
text.close fl1
endif

file.exists fl2,"jobs.txt"
if fl2=0 then
text.open w,fl2,"jobs.txt"
text.writeln fl2,"New Job"
text.close fl2
endif


gr.open 255,0,0,0,1,1

gr.orientation 1


gr.text.size 32

gr.text.bold 1
gr.text.align 2

gr.screen w,h

gr.bitmap.load bit1, "bmetal2.png"
gr.bitmap.scale bit2,bit1,300,90
gr.bitmap.scale bit3,bit1,80,80

gr.bitmap.draw btn1, bit2, w/2-150,100
gr.bitmap.draw btn2, bit2, w/2-150,200
gr.bitmap.draw btn3, bit2, w/2-150,300
gr.bitmap.draw btn4, bit2, w/2-150,400

gr.color 255,0,0,0,1

gr.text.draw btn_txt1,w/2,155,"Add Note"
gr.text.draw btn_txt2,w/2,255,"View Notes"
gr.text.draw btn_txt3,w/2,355,"Project List"
gr.text.draw btn_txt4,w/2,455,"Exit"

gr.bitmap.load logo1,"Russo2.jpg"
gr.bitmap.size logo1,lw,lh
gr.bitmap.draw logoa,logo1,w/2-lw/2,525



gr.render

do
gr.bounded.touch tch1, w/2-150,100,w+150,180
gr.bounded.touch tch2, w/2-150,200,w+150,280
gr.bounded.touch tch3, w/2-150,300,w+150,380
gr.bounded.touch tch4, w/2-150,400,w+150,480

if tch1=1
gr.hide btn1
gr.render
pause 100
gr.show btn1
gr.render

text.open r,fl3,"jobs.txt"
list.create S,job
do
text.readln fl3,ln$
list.add job,ln$
until ln$="EOF"
text.close fl3
gr.front 0
select jn,job,"Select Job"
if jn=1 then
input "New Job ID",jb$
endif

list.get job,jn,j$


TIME Year$, Month$, Day$, Hour$, Minute$, Second$

da$= "The Date Is:  "+ m$[VAL(Month$)]+"  "+ Day$+",  "+Year$


AMPM $= " AM"
Hour = VAL(Hour$)

IF Hour = 0 THEN
 theHour = 12
ELSE
 IF Hour = 12
  AMPM$ = " PM"
  theHour = 12
 ELSE
  IF Hour < 13 THEN
   theHour = Hour
  ELSE
   AMPM$ = " PM"
   theHour = MOD(Hour, 12)
  ENDIF
 ENDIF
ENDIF

tm$="The Time Is:  "+FORMAT$("##",theHour)+ ":"+Minute$+ AMPM$


Text.open a,fl2,"dlevel.txt"
text.writeln fl2, "-----------------------------------"
text.writeln fl2,da$
text.writeln fl2,tm$
text.writeln fl2," "
text.writeln fl2,"Project:  "+j$
text.writeln fl2," "
text.close fl2




s$=""
r$=""
gr.front 0
grabfile s$,"dlevel.txt"
text.input r$,s$

text.open w,fl1,"dlevel.txt"
text.writeln fl1,r$
text.close fl1
cls
s$=""
r$=""
gr.front 1
gr.render

endif

if tch2=1
gr.hide btn2
gr.render
pause 100
gr.show btn2
gr.render
s$=""
r$=""



grabfile s$,"dlevel.txt"
text.input r$,s$
gr.front 0
print r$
text.open w,fl1,"dlevel.txt"
text.writeln fl1,r$
text.close fl1
cls
s$=""
r$=""
gr.front 1
gr.render

endif

if tch3=1 then
gr.hide btn3
gr.render
pause 100
gr.show btn3
gr.render
text.open r,fl3,"jobs.txt"
list.create S,job
do
text.readln fl3,ln$
list.add job,ln$
until ln$="EOF"
text.close fl3
gr.front 0
select jn,job,"Select Job"
if jn=1 then
input "New Job ID",jb$
text.open a,fl4,"jobs.txt"
text.writeln fl4,jb$
text.close fl4
endif
gr.front 1
gr.render

endif


if tch4=1 then exit

gr.render
until 0


