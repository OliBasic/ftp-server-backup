REM Start of BASIC! Program
rem wavy
rem time variant wave function/oscilloscope


fn.def f(x,t)
fn.rtn 0.2*sin(x+0.1*t)+0.3*sin(2*x+0.4*t)+0.2*sin(3*x+0.5*t)
fn.end

gr.open 255,0,0,0
pi2=3.14*2
gr.screen w,h
gr.set.stroke 2
gr.color 255,255,255,255,1
loop:
st=clock()
t++
for x=0 to pi2 step 0.2
let nx=(x/pi2)*w:let ny=f(x,t)*h/2+h/2
if x then gr.line p,ox,oy,nx,ny:gr.show p
let ox=nx:let oy=ny
next
gr.render
pause max(5,5-clock()+st)
gr.cls
goto loop

ongrtouch:
exit


