REM Start of BASIC! Program
REM wavy
REM time variant wave function/oscilloscope


! normalize first n coefficients
FN.DEF normalize(a[])
 ARRAY.SUM x,a[]
 ARRAY.LENGTH n,a[]
 FOR i=1 TO n
  a[i]=a[i]/x
 NEXT
FN.END


! main


hh=10

LIST.CREATE n,c1
LIST.CREATE n,c2
LIST.CREATE n,v1
LIST.CREATE n,v2

a=0.6
FOR i=1 TO hh
 LIST.ADD c1,RND()*a
 LIST.ADD v1,0.5*(RND()-0.5)
 LIST.ADD c2,RND()*a
 LIST.ADD v2,0.5*(RND()-0.5)
NEXT 

LIST.TOARRAY c1,c1[]
LIST.TOARRAY c2,c2[]
LIST.TOARRAY v1,v1[]
LIST.TOARRAY v2,v2[]


LIST.CREATE s,menu
LIST.ADD menu,"exit","1","2","3","4","5","6","7","8","9","10"

GR.OPEN 255,0,0,0
p=3.14*2
GR.SCREEN w,h


h2=h/2
GR.SET.STROKE 4
hh=3
ARRAY.COPY c1[1,hh],cc1[]
ARRAY.COPY c2[1,hh],cc2[]
normalize(&cc1[])
normalize(&cc2[])

loop:
GR.CLS
s=0.13
LET st=CLOCK():t++
FOR x=0 TO p STEP s
 LET xx=w*x/p
 LET y1=0:LET y2=0
 LET i=0
 DO
  i++
  IF bk THEN F_N.BREAK
  y1+=cc1[i]*SIN(i*x-v1[i]*t)
  y2+=cc2[i]*SIN(i*x-v2[i]*t)
 UNTIL i=hh
 LET y1=h2*(y1+1):LET y2=h2*(y2+1)

 IF x 
  GR.COLOR 255,0,255,0,1:GR.LINE l,ox,oy2,xx,y2:GR.SHOW l

  GR.COLOR 255,255,0,0,1: GR.LINE l,ox,oy1,xx,y1:GR.SHOW l
 ENDIF

 LET ox=xx:LET oy1=y1:LET oy2=y2

 IF bk THEN F_N.BREAK
NEXT
LET bk=0
GR.RENDER

GOTO loop

ONGRTOUCH:
GR.TOUCH tt,tx,ty
IF tt
 DIALOG.SELECT s,menu,"choose # of harmonics"


 LIST.GET menu,s,c$
 IF c$="exit" THEN EXIT
 IF IS_NUMBER(c$)

  hh=VAL(c$)
  POPUP INT$(hh)+" harmonics"
  UNDIM cc1[]
  UNDIM cc2[]
  DIM cc1[hh]:DIM cc2[hh]
  ARRAY.COPY c1[1,hh],cc1[]
  ARRAY.COPY c2[1,hh],cc2[]
  normalize(&cc1[])
  normalize(&cc2[])
  i=MIN(i,hh)
  bk=1
 ENDIF
ENDIF

GR.ONGRTOUCH.RESUME

EXIT
