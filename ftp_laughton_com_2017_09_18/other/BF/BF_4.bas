prgPtr       = 1
GOSUB          load_Prg
DIALOG.SELECT  prgPtr, prgNames$[ ]
prg $        = prgs$ [ prgPtr]

prg$         = REPLACE$(REPLACE$(prg$,CHR$(10),"")," ","")
PRINT          "program: "; prgNames$[ prgPtr ]
PRINT           prg $
PAUSE          1000
lenPrg       = LEN(prg$)

ARRAY.LOAD     cmds$ [], "+", "-", "<", ">", ".", ",", "[", "]"
DIM            idxCmds [255]
FOR i        = 1 TO 8
 idxCmds [ASCII(cmds$[i])] = i
NEXT

_1           = 1
memSize      = 150
DIM            m[ memSize ]



!main --------------------------------------

tic          = clock ()

GOSUB          loopIdx

DIM            prg [ len ( prg$) ]
FOR i        = 1 TO len ( prg$)
 prg [i]     = ASCII(prg$,i)
NEXT

mPtr         = 1
pPtr         = 1
div          = ROUND( cyclesNeeed[prgPtr]/20)

!----------------------------
DO

 IF            ctrGlob++ >= divv THEN gosub update

 GOSUB         idxCmds[ prg[pPtr] ] , ~
 inc, dec, mPtrDec, mPtrInc, output, input, loopStart, loopEnd

UNTIL          ++pPtr   > lenPrg
!------------------------------

GOSUB          update
PRINT          "Finished!"
END




!------------------------------
loopIdx:

DIM                loops  [ lenPrg ]
p$               = prg$

DO
 posclose        = is_in ("]", p$, posclose +1 )
 IF                !posclose THEN d_u.break
 posopen         = 0
 DO
  poOld          = posopen
  posopen        = is_in ("[", p$, posopen+1 )
 UNTIL             posopen> posclose | !posopen
 p$              = LEFT$(p$,poOld-1)+"*"+ RIGHT$(p$,LEN(p$)- poOld )
 loops[posclose] = poOld
 loops[poOld]    = posclose
UNTIL 0

RETURN
!------------------------------



!command-handles ---------------

inc:
LET m[mPtr]       ++
RETURN

dec:
LET m[mPtr]       --
RETURN

mPtrDec:
LET mPtr          --
!LET mPtr          = MAX ( mPtr, _1 )
RETURN

mPtrInc:
LET mPtr          ++
!LET mPtr          = MIN( mPtr, memSize)
RETURN

output:
LET out$          = out$  + CHR$( m[mPtr])
LET out2$         = out2$ + INT$( m[mPtr])+","
RETURN

INPUT:
INPUT              "Input Number  (0-255)", inp
LET   m[mPtr]     = MIN(MAX(inp, 0), 255)
RETURN

loopStart:
IF                 !m[mPtr] THEN LET pPtr = loops[ pptr ]
RETURN

loopEnd:
IF                  m[mPtr] THEN LET pPtr = loops[ pptr ]
RETURN

!------------------------------



!------------------------------
update:
toc           = clock ()
divv         += div
CLS
PRINT "-------------"
PRINT "ctrGlob:         "; INT$(ctrGlob) ;" / ";
PRINT  INT$(ROUND(ctrGlob/cyclesNeeed[prgPtr]*100, 0));" %"
PRINT "speed:           "; round (ctrGlob/(toc-tic) ,3);"  [loops/ms]"

PRINT "memory-dump:"
FOR i         = 1 TO memsize-1
 PRINT          int $( m [i]);",";
NEXT
PRINT           int $(m [i])

PRINT "_______________current output_______________
IF out$= "" THEN print "> nothing <" ELSE print out$
IF out2$="" THEN print "> nothing <" ELSE print out2$
RETURN
!------------------------------




!------------------------------
load_Prg:

DIM                prgs$ [99]
ARRAY.LOAD         prgNames$[]   , ~
"HelloWorld(1)", "HelloWorld(2)", "Fibonacci", "PI", "Fibonacci(2)"
ARRAY.LOAD         cyclesNeeed[] , 907 , 390, 6026 , 412000, 30000


prgs$ [1] ="++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++."

prgs$ [2] =" ++++++++++[>+++++++>++++++++++>+++>+<<<<-] >++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>."

prgs$ [3] = "+.>+.>>>+++++ +++++[<<<[->>+<<]>>[-<+<+>>]<<<[->+<]>>[-<<+>>]<.>>>-]"

prgs$ [4] = " >  +++++ [<+>>>>>>>>++++++++++<<<<<<<-]>+++++[<+++++++++>-]+>>>>>>+[<<+++[>>[-<]<[>]<-]>>[>+>]<[<]>]>[[->>>>+<<<<]>>>+++>-]<[<<<<]<<<<<<<<+[->>>>>>>>>>>>[<+[->>>>+<<<<]>>>>>]<<<<[>>>>>[<<<<+>>>>-]<<<<<-[<<++++++++++>>-]>>>[<<[<+<<+>>>-]<[>+<-]<++<<+>>>>>>-]<<[-]<<-<[->>+<-[>>>]>[[<+>-]>+>>]<<<<<]>[-]>+<<<-[>>+<<-]<]<<<<+>>>>>>>>[-]>[<<<+>>>-]<<++++++++++<[->>+<-[>>>]>[[<+>-]>+>>]<<<<<]>[-]>+>[<<+<+>>>-]<<<<+<+>>[-[-[-[-[-[-[-[-[-<->[-<+<->>]]]]]]]]]]<[+++++[<<<++++++++<++++++++>>>>-]<<<<+<->>>>[>+<<<+++++++++<->>>-]<<<<<[>>+<<-]+<[->-<]>[>>.<<<<[+.[-]]>>-]>[>>.<<-]>[-]>[-]>>>[>>[<<<<<<<<+>>>>>>>>-]<<-]]>>[-]<<<[-]<<<<<<<<]++++++++++. "

prgs$ [5] = " >++++++++++>+>+[ [+++++[>++++++++<-]>.<++++++[>--------<-]+<<<]>.>>[[-]<[>+<-]>>[<<+>+>-]<[>+<-[>+<-[>+<-[>+<-[>+<-[>+<-[>+<-[>+<-[>+<-[>[-]>+>+<<<-[>+<-]]]]]]]]]]]+>>>]<<<]"


RETURN
!------------------------------
