prgPtr       = 2
GOSUB          load_Prg
DIALOG.SELECT  prgPtr, prgNames$[ ]
prg $        = prgs$ [ prgPtr ]

prg$         = REPLACE$(REPLACE$(prg$,CHR$(10),"")," ","")
PRINT          "program: "; prgNames$[ prgPtr ]
PRINT           prg $
PAUSE          1000
lenPrg       = LEN(prg$)

GOSUB          loopIdx

ARRAY.LOAD     cmds$ [], "+", "-", "<", ">", ".", ",", "[", "]"
DIM            idxCmds [127]
FOR i        = 1 TO 8
 idxCmds [ASCII(cmds$[i])] = i
NEXT

memSize      = 150
DIM            m[ memSize ]

mPtr         = 1
pPtr         = 1
div          = ROUND( cyclesNeeed[prgPtr]/20)
_1           = 1
tic          = clock ()

!main --------------------------
DO

 LET ctrGlob = ctrGlob+_1
 IF            !MOD( ctrGlob,div ) THEN gosub update

 GOSUB         idxCmds[ASCII(prg$,pPtr)] , ~
 inc, dec, mPtrDec, mPtrInc, output, input, loopStart, loopEnd

 LET pPtr    = pPtr+_1

UNTIL pPtr   > lenPrg
!------------------------------

GOSUB          update
PRINT          "Finished!"
END


!command-handles ---------------

inc:
LET m[mPtr]       = m[mPtr]+_1
RETURN

dec:
LET m[mPtr]       = m[mPtr]-_1
RETURN

mPtrDec:
LET mPtr          = mPtr - _1
!LET mPtr          = MAX ( mPtr, _1 )
RETURN

mPtrInc:
LET mPtr          = mPtr + _1
!LET mPtr          = MIN( mPtr, memSize)
RETURN

output:
LET out$          = out$  + CHR$( m[mPtr])
LET out2$         = out2$ + INT$( m[mPtr])+","
RETURN

INPUT:
INPUT "Input Number  (0-255)", inp
LET   m[mPtr]     = MIN(MAX(inp, 0), 255)
RETURN

loopStart:
IF  m[mPtr]       = 0
 ARRAY.SEARCH       loopsOpen [], pPtr, lPtr
 LET pPtr         = loopsClose[ lptr ]
ENDIF
RETURN

loopEnd:
IF  m[mPtr]      <> 0
 ARRAY.SEARCH       loopsClose [], pPtr, lPtr
 LET pPtr         = loopsOpen[ lptr ]
ENDIF
RETURN

!------------------------------


!------------------------------
update:
toc           = clock ()
CLS
PRINT "-------------"
PRINT "ctrGlob:         "; INT$(ctrGlob) ;" / ";
PRINT  INT$(ROUND(ctrGlob/cyclesNeeed[prgPtr]*100, 0));" %"
PRINT "speed:           "; round (ctrGlob/(toc-tic) ,3);"  [loops/ms]"
PRINT "memory-dump:"
FOR i         = 1 TO memsize-1
 PRINT          int $( m [i]);",";
NEXT
PRINT           int $(m [i])

PRINT "_______________current output_______________
IF out$= "" THEN print "> nothing <" ELSE print out$
IF out2$="" THEN print "> nothing <" ELSE print out2$
RETURN
!------------------------------


!------------------------------
loopIdx:

DO
 LET ctr++
 posopen         = IS_IN("[", prg$, posopen+1 )
UNTIL              !posopen

DIM                loops      [ctr*2]
DIM                loopsOpen  [ctr-1]
DIM                loopsClose [ctr-1]
p$               = prg$

DO
 posclose        = is_in ("]", p$, posclose +1 )
 ctr2 ++
 posopen         = 0
 DO
  poOld          = posopen
  posopen        = is_in ("[", p$, posopen+1 )
 UNTIL             posopen> posclose | !posopen
 p$              = LEFT$(p$,poOld-1)+"*"+ RIGHT$(p$,LEN(p$)- poOld )
 loops[ctr2*2-1] = poOld
 loops[ctr2*2 ]  = posclose+ poOld* 10 000 000
UNTIL              !posclose

ARRAY.SORT         loops [ ]
FOR i            = 1 TO ctr-1
 loopsOpen  [i]  = loops [i+2]
 loopsClose [i]  = loops [i+1+ctr] - 10 000 000 * loops [i+2]
NEXT

RETURN
!------------------------------


!------------------------------
load_Prg:

DIM                prgs$ [99]
ARRAY.LOAD         prgNames$[]   , ~
"HelloWorld(1)", "HelloWorld(2)", "Fibonacci", "PI"
ARRAY.LOAD         cyclesNeeed[] , 907 , 390, 6026 , 412000


prgs$ [1] ="++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++."

prgs$ [2] =" ++++++++++[>+++++++>++++++++++>+++>+<<<<-] >++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>."

prgs$ [3] = "+.>+.>>>+++++ +++++[<<<[->>+<<]>>[-<+<+>>]<<<[->+<]>>[-<<+>>]<.>>>-]"

prgs$ [4] = " >  +++++ [<+>>>>>>>>++++++++++<<<<<<<-]>+++++[<+++++++++>-]+>>>>>>+[<<+++[>>[-<]<[>]<-]>>[>+>]<[<]>]>[[->>>>+<<<<]>>>+++>-]<[<<<<]<<<<<<<<+[->>>>>>>>>>>>[<+[->>>>+<<<<]>>>>>]<<<<[>>>>>[<<<<+>>>>-]<<<<<-[<<++++++++++>>-]>>>[<<[<+<<+>>>-]<[>+<-]<++<<+>>>>>>-]<<[-]<<-<[->>+<-[>>>]>[[<+>-]>+>>]<<<<<]>[-]>+<<<-[>>+<<-]<]<<<<+>>>>>>>>[-]>[<<<+>>>-]<<++++++++++<[->>+<-[>>>]>[[<+>-]>+>>]<<<<<]>[-]>+>[<<+<+>>>-]<<<<+<+>>[-[-[-[-[-[-[-[-[-<->[-<+<->>]]]]]]]]]]<[+++++[<<<++++++++<++++++++>>>>-]<<<<+<->>>>[>+<<<+++++++++<->>>-]<<<<<[>>+<<-]+<[->-<]>[>>.<<<<[+.[-]]>>-]>[>>.<<-]>[-]>[-]>>>[>>[<<<<<<<<+>>>>>>>>-]<<-]]>>[-]<<<[-]<<<<<<<<]++++++++++. "


RETURN
!------------------------------
