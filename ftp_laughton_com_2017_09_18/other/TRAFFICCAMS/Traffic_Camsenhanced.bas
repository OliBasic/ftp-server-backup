
  rem debug.on
  !debug.echo.on
Init:


 rem check cam files
	
 ind = 1
 
 Do
 
 nam$="cam"+right$(format$("#",ind),1)+".txt"
 
 file.exists lexa,nam$
 if lexa = 0
 sel$= "1:Select a camera"
 text.open w,fvara,nam$
 text.writeln fvara,sel$
 text.close fvara
 endif
 ind = ind+1
 until ind>5
 
 grabfile cam1$,"cam1.txt"
 grabfile cam2$,"cam2.txt"
 grabfile cam3$,"cam3.txt"
 grabfile cam4$,"cam4.txt"
 grabfile cam5$,"cam5.txt"    
 
 
  array.delete choice$[]
  array.load choice$[],"1:Redbridge"~
"2:Millbrook Roundabout"~
"3:Regents Park Road" ~
"4:Mountbatten Way"~
"5:Charlotte Place"~
"6:Stag Gates"~
"7:Burgess Road"~
"8:Chilworth"~
"9:Bursledon Road"~
"10:Bitterne Road"~
"11:Bullar Road"~
"12:Northam Bridge"~
"13:Six Dials"~
"21:Swaythling"~
"22:Thomas Lewis Way"~
"23:Itchen Bridge"~
"25:West Quay"~
"39:Basset Avenue"~
"40:University Road"~
"41:Romsey Road"~
"42:Wilton Road"~
"43:Bevois Valley Road"~
"45:Winchester Road/Hill Lane"~
"47:Town Quay/High Street"~
"48:Polygon"~
"49:Onslow Road/St.Mary's"~
"50:Canute Road/Queens Terrace"~
"51:Cobden Bridge"~
"52:Itchen Bridge"~
"53:West Quay"~
"55:Charlotte Place"~
"55:Select a Camera"
start:

wakelock 3


screena: 
gosub DeScale	
	
screenaa:

gr.orientation 0
touched=0
tvar = 0
gr.render
gr.screen w,h
gr.color 255,255,255,0,1
gr.rect recta,0,40,w/2,120
gr.text.align 2
gr.text.size 50
gr.text.typeface 3
gr.color 255,0,0,0,1
gr.text.draw obja,w/4,100,"TRAFFIC WEBCAMS"
gr.rect rectb,0,121,w/2,160
gr.text.size 20
gr.color 255,255,255,255,1
gr.text.draw objb,w/4,150,"Southampton"
gr.color 255,200,200,200,1
gr.rect rectc,0,161,w/2,240
gr.color 255,0,0,0,1
gr.text.size 35
gr.text.draw objc,w/4,210,"SHOW CAMERAS"
gr.color 255,200,200,200,1
gr.rect rectd,0,241,w/2,320
gr.color 255,0,0,0,1
gr.text.draw objd,w/4,290,"INFORMATION"
gr.color 255,200,200,200,1
gr.rect recte,0,321,w/2,400
gr.color 255,0,0,0,1
gr.text.draw obje,w/4,370,"Reference list of cameras"
gr.color 255,0,0,0,1
gr.rect rectf,0,450,w/2,539
gr.color 255,255,255,255,1
gr.text.draw objf,w/4,500,"EXIT"
gr.color 255,0,0,0,1
gr.rect rectg,w/2+1,39,w,80
gr.color 255,255,255,255,1
gr.text.size 30
gr.text.draw objg,3*w/4,70,"CAMERAS SELECTED"
gr.color 255,255,255,255,1
gr.rect recth,w/2+1,81,w,120
gr.color 255,0,0,0,1
gr.text.size 20
gr.text.draw objh,3*w/4,110,"Tap Buttons to change locations"
gr.color 255,200,200,200,1
gr.text.size 30
gr.rect recti,w/2+5,130,w-5,180
gr.rect rectj,w/2+5,190,w-5,240
gr.rect rectk,w/2+5,250,w-5,300
gr.rect recl,w/2+5,310,w-5,360
gr.rect recm,w/2+5,370,w-5,420
gr.color 255,0,0,0,1
gr.text.size 30
gr.text.draw obji,3*w/4,170,cam1$
gr.text.draw obji,3*w/4,230,cam2$
gr.text.draw obji,3*w/4,290,cam3$
gr.text.draw obji,3*w/4,350,cam4$
gr.text.draw obji,3*w/4,410,cam5$
gr.render

do

gr.touch touched,x,y

until touched

rem CAM1

if (x>w/2&x<w-5)&(y>130&y<180)=1
flag$="1"
gr.front 0
gosub choice
cam1$= choice$[s]
sav$= cam1$
gosub sfile

endif

rem CAM2

if (x>w/2&x<w-5)&(y>190&y<240)=1
flag$="2"
gr.front 0
gosub choice
cam2$= choice$[s]
sav$= cam2$
gosub sfile

endif

rem CAM3

if (x>w/2&x<w-5)&(y>250&y<300)=1
flag$="3"
gr.front 0
gosub choice
cam3$= choice$[s]
sav$= cam3$
gosub sfile

endif

rem CAM4

if (x>w/2&x<w-5)&(y>310&y<360)=1
flag$="4"
gr.front 0
gosub choice
cam4$= choice$[s]
sav$= cam4$
gosub sfile

endif

rem CAM5

if (x>w/2&x<w-5)&(y>370&y<420)=1
flag$="5"
gr.front 0
gosub choice
cam5$= choice$[s]
sav$= cam5$
gosub sfile

endif



rem INFORMATION

if (x>0&x<w/2)&(y>241&y<320)=1
gr.front 0

console.title "TRAFFIC CAMS"
line=0

cls
touched=0

grabfile s$,"TRAFFIC CAMS.txt"
print s$
 
do
 until tvar=1
 tvar=0
 
endif
gr.front 1

rem CAMERA LIST

if (x>0&x<w/2)&(y>321&y<400)=1
gr.front 0

console.title "TRAFFIC CAMS"
line=0

cls
touched=0

grabfile sa$,"cameras.txt"
print sa$
 
do
 until tvar=1
 tvar=0
 
endif

gr.front 1

rem SHOW CAMERAS

IF (x>0&x<w/2)&(y>161&y<240) = 1

split result1$[],cam1$,":"
camn1$=result1$[1]
gosub unda
split result1$[],cam2$,":"
camn2$=result1$[1]
gosub unda
split result1$[],cam3$,":"
camn3$=result1$[1]
gosub unda
split result1$[],cam4$,":"
camn4$=result1$[1]
gosub unda
split result1$[],cam5$,":"
camn5$=result1$[1]
gosub unda

gosub showcam
goto screena
endif


rem CLOSING

if (x>0&x<w/2)&(y>450&y<540) = 1

popup "CLOSING",0,0,0
pause 1000
exit
endif 

cls 
tvar=0
goto screenaa

onconsoletouch:
tvar=1
consoletouch.resume

choice:

select s,choice$[],""
if s=0
s= 32
endif
return

sfile: 

text.open w,myfile, "cam"+flag$+".txt"
text.writeln myfile,sav$
text.close myfile
return

unda: 
undim array[],result1$[]
return

showcam:

htp$="http://hants.gov.uk/cctv/southampton/WCAM"

pa$=htp$+camn1$+".jpg"
pb$=htp$+camn2$+".jpg"
pc$=htp$+camn3$+".jpg"
pd$=htp$+camn4$+".jpg"
pe$=htp$+camn5$+".jpg"
popup "Loading pictures",0,0,0

byte.open r,pa,pa$
if pa=-1
popup "no file" 0,0,0
goto init
endif
gr.text.draw numi,3*w/4,470,camn1$
gr.render
byte.copy pa,"pic1.jpg"
byte.close pa

byte.open r,pb,pb$
gr.modify numi,"text",camn2$
gr.render
byte.copy pb,"pic2.jpg"
byte.close pb

byte.open r,pc,pc$
gr.modify numi,"text",camn3$
gr.render
byte.copy pc,"pic3.jpg"
byte.close pc

byte.open r,pd,pd$ 
gr.modify numi,"text",camn4$
gr.render
byte.copy pd,"pic4.jpg"
byte.close pd

byte.open r,pe,pe$
gr.modify numi,"text",camn5$
gr.render
byte.copy pe,"pic5.jpg"
byte.close pe

display:

gr.cls
gr.close
gosub DeScale

gr.orientation 0
gr.screen w,h

gr.bitmap.load ba,"pic1.jpg"
gr.bitmap.load bb,"pic2.jpg"
gr.bitmap.load bc,"pic3.jpg"
gr.bitmap.load bd,"pic4.jpg"
gr.bitmap.load be,"pic5.jpg"

bitdraw:

gr.bitmap.draw ma,ba,0,40
gr.bitmap.draw mb,bb,w/3,40
gr.bitmap.draw mc,bc,2*w/3,40
gr.bitmap.draw md,bd,0,h/2+15
gr.bitmap.draw me,be,w/3,h/2+15
gr.color 255,0,0,0,1
gr.text.size 30
gr.text.draw objz,11*w/16,350,"TOUCH TO RETURN"
GR.TEXT.SIZE 25
gr.text.draw objza,11*w/16,470,"Touch PICTURE to enlarge "
gr.render

do
gr.touch touched,x,y
until touched

do
gr.touch touched,x,y
until !touched

if (x>0&x<w/3)&(y>40&y<h/2)=1
gr.bitmap.scale nba,ba,2*w/3,h-40
bflag=nba
gosub large
gosub shoobj
goto display
rem pause 3000
endif

if (x>w/3&x<2*w/3)&(y>40&y<h/2)=1
gr.bitmap.scale nba,bb,2*w/3,h-40
bflag=nba
gosub large
gosub shoobj
goto display
rem pause 3000
endif

if (x>2*w/3&x<w)&(y>40&y<h/2)=1
gr.bitmap.scale nba,bc,2*w/3,h-40
bflag=nba
gosub large
gosub shoobj
goto display
rem pause 3000
endif

if (x>0&x<w/3)&(y>h/2&y<h)=1
gr.bitmap.scale nba,bd,2*w/3,h-40
bflag=nba
gosub large
gosub shoobj
goto display
rem pause 3000
endif

if(x>w/3&x<2*w/3)&(y>h/2&y<h)=1
gr.bitmap.scale nba,be,2*w/3,h-40
bflag=nba
gosub large
gosub shoobj
goto display
rem pause 3000
endif

gr.cls
gr.close

return

large:

gosub hidobj
gr.bitmap.draw mg,bflag,0,40
gr.render

do
gr.touch touched,x,y
until touched

do 
gr.touch touched,x,y
until !touched

return



hidobj: 
gr.hide ma
gr.hide mb
gr.hide mc
gr.hide md
gr.hide me
gr.hide objza

return

shoobj:

gr.show ma
gr.show mb
 gr.show mc
 gr.show md
gr.show me
gr.show objza

return

DeScale:

	rem Scaling
	rem developing device
dh= 540
dw= 960
	
	rem Actual width and height
	
gr.open 255,255,255,255,255
pause 500
gr.orientation 0
gr.screen w,h
	
	rem scale factors

scale_width = w/dw
scale_height = h/dh
rem set scale
gr.scale scale_width,scale_height

return

onerror:

popup "Error",0,0,0

exit

OnBackKey:

popup "Press menu key to exit",0,0,0
back.resume

OnMenukey: 
popup "Closing",0,0,0
exit

