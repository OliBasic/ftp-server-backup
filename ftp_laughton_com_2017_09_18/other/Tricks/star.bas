! star from x,y (upper left) to x+a,y+1.25*a (lower right)
! by tony_gr
! Oct.29.2012
! Basic v1.68

FN.DEF star(x,y,a)
 LIST.CREATE n,stella
 LIST.ADD stella, x,y+a*0.5, x+a*0.3125,y+a*0.5, x+a*0.5,y
 LIST.ADD stella, x+a*0.6875,y+a*0.5, x+a,y+a*0.5, x+a*0.75,y+a*0.75
 LIST.ADD stella, x+a,y+1.25*a, x+a*0.5,y+a*0.875, x,y+1.25*a,x+a*0.25,y+a*0.75
 GR.POLY sObj, stella
 FN.RTN stella
FN.END

! main
GR.OPEN 255,0,0,0,0,1
GR.ORIENTATION 1
GR.SCREEN w,h
sx=w/800
sy=h/1232
GR.SCALE sx,sy

! draw 5 stars
FOR i=1 TO 5
 GR.COLOR 255,50*i,255-50*i,155,1
 x=70*(i-1)
 y=100
 a=30+i*10
 z=star(x,y,a) % function call
NEXT i
GR.RENDER

DO
UNTIL 0
