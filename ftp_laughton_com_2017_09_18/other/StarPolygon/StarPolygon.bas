! Star polygon por IAO

GOSUB program_init
GOSUB program_main
END

program_main:
	FOR z = 2 TO F
		B = 2*z*pi/F
		temp_x = (temp_x)+(R*COS(A+B))
		temp_y = (temp_y)+(R*SIN(A+B))
		PosXY[z,1] = temp_x
		PosXY[z,2] = temp_y
		GR.CIRCLE point, temp_x, temp_y, 4
	NEXT
	
	GR.RENDER
	
	FOR z = 1 TO F
		k1 = z
		k2 = z+D
		IF k2 > F
			k2 = k1+D-F
		ENDIF
		GR.LINE line, PosXY[k1,1], PosXY[k1,2], PosXY[k2,1], PosXY[k2,2]
	NEXT
	
	GR.RENDER
	
	DO
		GR.TOUCH tflag, temp_x, temp_y
		PAUSE 50
	UNTIL tflag
	
	FOR z = 1 TO F
		PRINT "Ponto ";z
		PRINT "x:";PosXY[z,1]
		PRINT "Y:";PosXY[z,2]
		PRINT "---"
	NEXT
	
	GR.CLOSE
return

program_init:
	%INPUT "Insert the number of faces:",F
	%INPUT "Insert the density of the star:",D
	%INPUT "Insert the angle of rotation:",A

	DIM PosXY[99,2]
	
	GR.OPEN 255, 0, 0, 0	
	GR.COLOR 255, 255, 255, 255, 1
	GR.SET.STROKE 2
	GR.ORIENTATION 0
	PAUSE 100
	GR.SCREEN sw, sh
	
	pi = atan(1)*4
	
	F = 14
	D = 5
	A = 0
	
	A = A*pi/180			% Angulo de giro em radianos
	R = sw/(3*(F*(1/5)))	% Radio
	
	temp_x = sw/2
	temp_y = sw/25
	
	PosXY[1,1] = temp_x
	PosXY[1,2] = temp_y
	GR.CIRCLE point, temp_x, temp_y, 4
return