REM Start of BASIC! Program
!Title: RuneKaster 5
!Summary: A simple program that displays a rune verse from the Anglo-Saxon Rune Poem in answer to a user-posed question.
!The rune verses are from Bruce Dickins's 1915 book RUNIC AND HEROIC POEMS OF THE OLD TEUTONIC PEOPLES, which is in the public domain in the USA.
!Copyright (c) 2016 by Michael and Vivian Thomas.
!Released under the MIT license, a copy of which can be found in the license.txt file

%scaling
cls
START:
di_height = 780
di_width = 480
gr.open 255, 255, 255, 255
GR.ORIENTATION 1
GR.SCREEN actual_w, actual_h
scale_width = actual_w / di_width
scale_height = actual_h / di_height
GR.SCALE scale_width, scale_height

%opening screen
gr.orientation 1
gr.color 255, 0, 100, 150, 1
gr.rect rct1, 0, 0, 480, 780
gr.color 255, 255, 255, 255, 1
gr.rect rct1, 15, 15,465, 765
gr.color 255, 0, 0, 0, 1
gr.text.size 60
gr.text.draw t1, 75, 250, "Welcome to"
gr.text.draw t2, 55, 400, "RuneKaster 5"
pause 2000

%input
input"Please enter your question or concern.", a$
gr.text.draw t3, 55, 550, "Please Wait ..."
gr.render
pause 3000
gr.close

%answer
cls
print"Your answer may be found in this rune:"
DIM r$[29]
r$[1] = "FEOH: Wealth is a comfort to all men; yet must every man bestow it freely, if he wish to gain honour in the sight of the Lord."
r$[2] = "UR: The aurochs is proud and has great horns, it is a very savage beast and fights with its horns; a great ranger of the moors, it is a creature of mettle."
r$[3] = "THORN: The thorn is exceedingly sharp, an evil thing for any knight to touch, uncommonly severe on all who sit among them."
r$[4] = "OS: The mouth is the source of all language, a pillar of wisdom and a comfort to wise men, a blessing and a joy to every knight."
r$[5] = "RAD: Riding seems easy to every warrior while he is indoors and very courageous to him who traverses the high-roads on the back of a stout horse."
r$[6] = "CEN: The torch is known to every living man by its pale, bright flame; it always burns where princes sit within."
r$[7] = "GYFU: Generosity brings credit and honour, which support one's dignity; it furnished help and subsistence to all broken men who are devoid of aught else."
r$[8] = "WYNN: Bliss he enjoys who knows not suffering, sorrow nor anxiety, and has prosperity and happiness and a good enough house."
r$[9] = "HAEGL: Hail is the whitest of grain; it is whirled from the vault of heaven and is tossed about by gusts of wind and then it melts into water."
r$[10] = "NYD: Trouble is oppressive to the heart; yet often it proves a source of help and salvation to the children of men, to everyone who heeds it betimes."
r$[11] = "IS: Ice is very cold and immeasurably slippery; it glistens as clear as glass and most like to gems; it is a floor wrought by the frost, fair to look upon."
r$[12] = "GER: Summer is a joy to men, when God, the holy King of Heaven, suffers the earth to bring forth fruits for rich and poor alike."
r$[13] = "EOH: The yew is a tree with rough bark, hard and fast in the earth, supported by its roots, a guardian of flame and a joy upon an estate."
r$[14] = "PEORDH: Peorth is a source of recreation and amusement to the great, where warriors sit blithely together in the banqueting-hall."
r$[15] = "EOLH: The EOHL-sedge is mostly to be found in a marsh; it grows in the water and makes a ghastly wound, covering with blood every warrior who touches it."
r$[16] = "SIGEL: The sun is ever a joy in the hopes of seafarers when they journey over the fishes' bath, until the courser of the deep bears them to land."
r$[17] = "TIR: Tiw is a guiding star; well does it keep faith with princes; it is ever on its course over the mists of night and never fails."
r$[18] = "BEORC: The poplar bears no fruit; yet without seed it brings for suckers, for it is generated from its leaves. Splendid are its branches and gloriously adorned its lofty crown which reaches to the skies."
r$[19] = "EH: The horse is a joy to princes in the presence of warriors. A steed in the pride of its hoofs, when rich men on horseback bandy words about it; and it is ever a source of comfort to the restless."
r$[20] = "MANN: The joyous man is dear to his kinsmen; yet every man is doomed to fail his fellow, since the Lord by his decree will the vile carrion to the earth."
r$[21] = "LAGU: The ocean seems interminable to men, if they venture on the rolling bark and the waves of the sea terrify them and the courser of the deep heed not its bridle."
r$[22] = "ING: Ing was first seen by men among the East-Danes, till, followed by his chariot, he departed eastward over the waves. So the Heardingas named the hero."
r$[23] = "ETHEL: An eastate is very dear to every man, if he can enjoy there in his house whatever is right and proper in constant properity."
r$[24] = "DAEG: Day, the glorious light of the Creator, is sent by the Lord; it is beloved of men, a source of hope and happiness to rich and poor, and of service to all."
r$[25] = "AC: The oak fattens the flesh of pigs for the children of men. Often it traverses the gannet's bath, and the ocean proves whether the oak keeps faith in honourable fashion."
r$[26] = "AESC: The ash is exceedingly high and precious to men. With its sturdy trunk it offers a stubborn resistance, though attacked by many a man."
r$[27] = "YR: Yr is a source of joy and honour to every prince and knight; it looks well on a horse and is a reliable equipment for a journey."
r$[28] = "IOR: Iar is a river fish and yet it always feeds on land; it has a fair abode encompassed by water, where it lives in happiness."
r$[29] = "EAR: The grave is horrible to every knight, when the corpse quickly begins to cool and is laid in the bosom of the dark earth. Prosperity declines, happiness passes away and covenants are broken."
i = randomnumber
i = floor(rnd()*29)+1
print r$[i]

%choice repeat or end
print"Touch here if you want to ask another question. Otherwise, please press BACK to exit."
tapped = 0
do
until tapped
goto START
on consoletouch:
tapped = 1
consoletouch.resume

%end
onbackkey:
gr.open
gr.orientation 1
gr.color 255, 0, 100, 150, 1
gr.rect rct2, 0, 0, 480, 780
gr.color 255, 255, 255, 255, 1
gr.rect rct2, 15, 15, 465, 765
gr.color 255, 0, 0, 0, 1
gr.text.size 60
gr.text.draw t4, 100, 390, "GOODBYE"
dialog.message"DISCLAIMER:", "Thank you for playing, but remember that this is only a game. Do not make life decisions based on these results.", ok, "OK"
gr.render
pause 2000
exit

