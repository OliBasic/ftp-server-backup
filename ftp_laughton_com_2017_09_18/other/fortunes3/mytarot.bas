REM Start of BASIC! Program
!Title: My Tarot
!Summary: A simple program that displays a major aracana tarot card interpretation in response to a user-posed question.
!The tarot interpretations are from the 1888 book THE TAROT by S.L. MacGregor Mathers, which is in the public domain in the USA.
!Copyright (c) 2016 by Michael and Vivian Thomas.
!Released under the MIT license, a copy of which can be found in the license.txt file

%scaling
cls
START:
di_height = 780
di_width = 480
gr.open 255, 255, 255, 255
GR.ORIENTATION 1
GR.SCREEN actual_w, actual_h
scale_width = actual_w / di_width
scale_height = actual_h / di_height
GR.SCALE scale_width, scale_height

%opening screen
gr.orientation 1
gr.color 255, 200, 200, 0, 1
gr.rect rct1, 0, 0, 480, 780
gr.color 255, 255, 255, 255, 1
gr.rect rct1, 15, 15, 465, 765
gr.color 255, 0, 0, 0, 1
gr.text.size 60
gr.text.draw t1, 75, 250, "Welcome to"
gr.text.draw t2, 110, 410, "My Tarot"
pause 2000

%input
input"Please enter your question or concern.", a$
gr.text.draw t3, 55, 550, "Please Wait ..."
gr.render
pause 3000
gr.close

%answer
cls
print"Your answer may be found in this card:"
DIM card$[44]
card$[1] = "The Juggler: He represents will, will-power, and dexterity."
card$[2] = "The High Priestess: She represents science, wisdom, knowledge, and education."
card$[3] = "The Empress: She represents action, plan, undertaking movement in a matter, and initiative."
card$[4] = "The Emperor: He represents realisation, effect, and development."
card$[5] = "The Hierophant or Pope: He represents mercy, beneficence, kindness, and goodness."
card$[6] = "The Lovers: They represent wise dispositions, proof, trials surmounted."
card$[7] = "The Chariot: It represents triumph, victory, overcoming obstacles."
card$[8] = "Themis or Justice: This represents equilibrium, balance, and justice."
card$[9] = "The Hermit: He represents prudence, caution, deliberation."
card$[10] = "The Wheel of Fortune: This represents good fortune, success, unexpected luck."
card$[11] = "Strength or Fortitude: This represents power, might, force, strength, fortitude."
card$[12] = "The Hanged Man: He represents self-sacrifice, sacrifice, devotion, and (being) bound."
card$[13] = "Death: He represents death, change, transformation, and alteration for the worse."
card$[14] = "Temperance: She represents combination, conformation, uniting."
card$[15] = "The Devil: He represents fatality for good."
card$[16] = "The Lightning-struck Tower: This represents ruin, disruption, over-throw, loss and bankruptcy."
card$[17] = "The Star: This represents hope, expectation, and bright promises."
card$[18] = "The Moon: This represents twilight, deception, and error."
card$[19] = "The Sun: This represents happiness, and (being) content."
card$[20] = "The Last Judgement: This represents renewal, result and determination of a matter."
card$[21] = "The Foolish Man: He represents folly, expiation, and wavering."
card$[22] = "The Universe: This represents completion, and good reward."
card$[23] = "The Juggler Reversed: The meaning is now will applied to evil ends, weakness of will, cunning, and knavishness."
card$[24] = "The High Priestess Reversed: The meaning is now conceit, ignorance, unskillfulness, superficial knowledge."
card$[25] = "The Empress Reversed: The meaning is now inaction, frittering away of power, want of concentration, vacillation."
card$[26] = "The Emperor Reversed: The meaning is now stoppage, check, immature, unripe."
card$[27] = "The Hierophant or Pope Reversed: The meaning is now over-kindness, weakness, foolish exercise of generosity."
card$[28] = "The Lovers Reversed: The meaning is now unwise plans, failure when put to the test."
card$[29] = "The Chariot Reversed: The meaning is now overthrown, conquered by obstacles at the last moment,"
card$[30] = "Themis or Justice Reversed: The meaning is now bigotry, want of balance, abuse of justice, over-severity, inequality, bias."
card$[31] = "The Hermit Reversed: The meaning is now over-prudence, timorousness, fear."
card$[32] = "The Wheel of Fortune Reversed: The meaning is now ill-fortune, failure, unexpected ill-luck."
card$[33] = "Strength or Fortitude Reversed: The meaning is now abuse of power, over-bearingness, want of fortitude."
card$[34] = "The Hanged Man Reversed: The meaning is now selfishness, unbound, partial sacrifice."
card$[35] = "Death Reversed: The meaning is now death just escaped, partial change, alteration for the better."
card$[36] = "Temperance Reversed: The meaning is now ill-advised combinations, disunion, clashing interest, etc.."
card$[37] = "The Devil Reversed: The meaning is now fatality for evil."
card$[38] = "The Lightning-struck Tower Reversed: The meaning is now ruin, disruption, overthrow, loss, bankruptcy in a more or less partial degree."
card$[39] = "The Star Reversed: The meaning is now hopes not fulfilled, expectations diappointed or fulfilled in a minor degree."
card$[40] = "The Moon Reversed: The meaning is now fluctuation, slight deceptions, trifling mistakes."
card$[41] = "The Sun Reversed: The meaning is now happiness, content(ment), joy in a minor degree."
card$[42] = "The Last Judgement Reversed: The meaning is now postponement of result, delay, matter re-opened later."
card$[43] = "The Foolish Man Reversed: The meaning is now hesitation, instability, trouble arising herefrom."
card$[44] = "The Universe Reversed: The meaning is now evil reward or recompense."
i = randomnumber
i = floor(rnd()*44)+1
print card$[i]

%choice repeat or end
print"Touch here if you want to ask another question. Otherwise, please press BACK to exit."
tapped = 0
do
until tapped
goto START
on consoletouch:
tapped = 1
consoletouch.resume

%end
onbackkey:
gr.open
gr.orientation 1
gr.color 255, 200, 200, 0, 1
gr.rect rct2, 0, 0, 480, 780
gr.color 255, 255, 255, 255, 1
gr.rect rect2, 15, 15, 465, 765
gr.color 255, 0, 0, 0, 1
gr.text.size 60
gr.text.draw t4, 100, 390, "GOODBYE"
dialog.message"DISCLAIMER:", "Thank you for playing, but remember that this is only a game. Do not make life decisions based on these results.", ok, "OK"
gr.render
pause 2000
exit

