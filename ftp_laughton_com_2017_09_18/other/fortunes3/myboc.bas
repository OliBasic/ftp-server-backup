REM Start of BASIC! Program
!Title: My Book of Changes
!Summary: A simple program that displays an I-Ching hexagram interpretation in response to a user-posed question.
!The interpretations are from the 1899 James Legge translation, which is in the public domain in the USA.
!Copyright (c) 2016 by Michael and Vivian Thomas.
!Released under the MIT license, a copy of which can be found in the license.txt file.

%scaling
cls
START:
di_height = 780
di_width = 480
gr.open 255, 255, 255, 255
GR.ORIENTATION 1
GR.SCREEN actual_w, actual_h
scale_width = actual_w / di_width
scale_height = actual_h / di_height
GR.SCALE scale_width, scale_height

%opening screen
gr.color 255, 255, 0, 0, 2
gr.rect rct1, 0, 0, 480, 780
gr.color 255, 255, 255, 255,1
gr.rect rct1, 15, 15, 465, 765
gr.color 255, 0, 0, 0, 1
gr.text.size 50
gr.text.draw t1, 130, 160, "Welcome"
gr.text.draw t2, 180, 250, "- To -"
gr.text.draw t3, 180, 340, "- My -"
gr.text.draw t4, 180, 430, "Book"
gr.text.draw t5, 180, 520, "- Of -"
gr.text.draw t6, 130, 610, "Changes"
pause 2000

%input
input "Please enter your question or concern.", a$

%timer
gr.color 255, 255, 255, 255, 1
gr.rect rct1, 15, 15, 465, 765
gr.render
gr.color 255, 0, 0, 0, 1
gr.text.size 60
gr.text.draw t6, 50, 390, "Please Wait..."
gr.render
pause 3000

%answer
cls
gr.close
print"Your answer may be found in this hexagram:"
DIM h$[64]
h$[1] = "K'HIEN: K'hien (represents) what is great and originating, penetrating, advantageous, correct and firm."
h$[2] = "KHWAN: Khwan (represents) what is great and originating, penetrating, advantageous, correct and having the firmness of a mare. When the superior man (here intended) has to make any movement, if he take the initiative, he will go astray; if he follow, he will find his (proper) lord. The advantageousness will be seen in his getting friends in the south-west and losing friends in the north-east. If he rest in corectness, there will be good fortune."
h$[3] = "KUN: Kun (indicates that in the case which it presupposes) there will be great progress and success, and the advantage will come from being correct and firm. (But) any movement in advance should not be (lightly) undertaken. There will be advantage in appointing feudal princes."
h$[4] = "MANG: Mang (indicates that in the case which it presupposes) there will be progress and success. I do not (go and) see the youthful and inexperienced, but he comes and seeks me. When he shows (the security that marks) the first recourse to divination, I instruct him. If he apply a second and third time, that is troublesome; and I do not instruct the troublesome. There will be advantage to being firm and correct."
h$[5] = "HSU: Hsu intimates that, with the sincerity which is declared in it, there will be brilliant success. With firmness there will be good fortune; and it will be advantageous to cross the great stream."
h$[6] = "SUNG: Sung intimates how, though there is sincerity in one's contention, he will yet meet with opposition and obstruction; but if he cherish an apprehensive caution, there will be good fortune, while, if he must prosecute the contention to the (bitter) end, there will be evil. It will be advantageous to see the great man; it will not be advantageous to cross the great stream."
h$[7] = "SZE: Sze indicates how, in the case which it spposes, with firmness and correctness, and (a leader of) age and experience, there will be good fortune and no error."
h$[8] = "PI: Pi indicates that (under the conditions which it supposes) there is good fortune. But let (the principal party intended in it) re-examine himself, (as if) by divination, whether his virtue be great, unintermitting, and firm. Those who have not rest will then come to him; and with those who are (too) late in coming it will be ill."
h$[9] = "HSIAO KHU: Hsiao Khu indicates that (under its ?conditions) there will be progress and success. (We see) dense clouds, but no rain coming from our borders in the west."
h$[10] = "LI: (Li suggests the idea of) one treading on the tail of a tiger, which does not bite him. There will be progress and success."
h$[11] = "THAI: In Thai (we see) the little gone and the great come. (It indicates that) there will be good fortune, with progress and success."
h$[12] = "PHI: In Phi there is the want of good understanding between the (different classes of) men, and its indication is unfavourable to the firm and correct course of the superior man. We see in it the great gone and the little come."
h$[13] = "THUNG ZAN: Thung Zan ( or 'Union of men') appears here (as we find it) in the (remote districts of the) country, indicating progress and success. It will be advantageous to cross the great stream. It will be advantageous to maintain the firm correctness of the superior man."
h$[14] = "TA YU: Ta Yu indicates that, (under the circumstances which it implies), there will be great progress and success."
h$[15] = "KHIEN: Khien indicates progress and success. The superior man, (being humble as it implies), will have a (good) issue (to his undertakings)."
h$[16] = "YU: Yu indicates that, (in the state which it implies), feudal princes may be set up, and the hosts put in motion, with advantage."
h$[17] = "SUI: Sui indicates that (under its conditions) there will be great progress and success. But it will be advantageous to be firm and correct. There will (then) be no error."
h$[18] = "KU: Ku indicates great progress and success (to him who deals properly with the condition represented by it). There will be advantage in (efforts like that of) crossing the great stream. (He should weigh well, however, the events of) three days before the turning point, and those (to be done) three days after it."
h$[19] = "LIN: Lin (indicates that under the conditions supposed in it) there will be great progress and success, while it will be advantageous to be firmly correct. In the eighth month there will be evil."
h$[20] = "KWAN: Kwan shows (how he whom it represents should be like) the worshipper who has washed his hands, but not (yet) presented his offerings;-=with sincerity and an appearance of dignity (commanding reverent regard).
h$[21] = "SHIH HO: Shih Ho indicates successful progress (in the condition of things which it supposes). It will be advantageous to use legal constraints."
h$[22] = "PI (also spelled PEE): Pi indicates that there should be free course (in what it denotes). There will be little advantage (however) if it be allowed to advance (and take the lead)."
h$[23] = "PO: Po is the symbol of falling or of  causing to fall, and may be applied, both in the natural and political world, to the process of decay, or that of overthrow."
h$[24] = "FU: Fu indicates that there will be free course and progress (in what it denotes). (The subject of it) fins no one to distress him in his exits and entrances; friends come to him, and no error is committed. He will return and repeat his (proper) course. In seven days comes his return. There will be advantage in whatever direction movement is made."
h$[25] = "WU WANG: Wu Wang indicates great progress and success, while there will be advantage in being firm and correct. If (its subject and his action) be not correct, he will fall into errors, and it will not be advantageous for him to move in any direction."
h$[26] = "TA KHU: Under the conditions of Ta Khu it will be advantageous to be firm and correct. (If its subject do not seek to) enjoy his revenues in his own family (without seeking service at court), there will be good fortune. It will be advantageous for him to cross the great stream."
h$[27] = "I (also spelled IH): I indicates that with firm correctness there will be good fortune (in what is denoted by it). We must look at what we are seeking to nourish, and by the exercise of our thoughts seek for the proper aliment."
h$[28] = "TA KWO: Ta Kwo suggests to us a beam that is weak. There will be advantage in moving (under its conditions) in any direction whatever; there will be success."
h$[29] = "KHAN: Khan, here repeated, shows the possession of sincerity, through which the mind is penetrating. Action (in accordance with this) will be of high value."
h$[30] = "LI: Li indicates that, (in regard to what it denotes), it will be advantageous to be firm and correct, and that thus there will be free course and success. Let (its subject) also nourish (a docility like that of) the cow, and there will be good fortune."
h$[31] = "HSIEN: Hsien indicates that, (on the fulfilment of the conditions implied in it), there will be free course and success. Its advantageousness will depend on the being firm and correct, (as) in marrying a young lady. There will be good fortune."
h$[32] = "HANG: Hang indicates successful progress and no error (in what it denotes). But the advantage will come from being firm and correct; and movement in any direction whatever will be advantageous."
h$[33] = "THUN: Thun indicates successful progress (in its circumstances). To a small extent it will (still) be advantageous to be firm and correct."
h$[34] = "TA KWANG: Ta Kwang indicates that (under the conditions which it symbolises) it will be advantageous to be firm and correct."
h$[35] = "TZIN: In Tzin we see a prince who secures the tranquility (of the people) presented on that account with numerous horses (by the king), and three times in a day received at interviews."
h$[36] = "MING I: Ming I indicates that (in the circumstances which it denotes) it will be advantageous to realise the difficulty (of the position), and maintain firm correctness."
h$[37] = "KIA ZAN: For (the realisation of what is taught in) Kia Zan, (or for the regulation of the family), what is most advantageous is that the wife be firm and correct."
h$[38] = "KHWEI: Khwei indicates that, (notwithstanding the condition of things which it denotes), in small matters there will (still) be good success."
h$[39] = "KIEN: In (the state indicated by) Kien advantage will be found in the south-west, and the contrary in the north-east. It will be advantageous (also) to meet with the great man. (In these circumstances), with firmness and correctness, there will be good fortune."
h$[40] = "KIEH: In (the state indicated by) Kieh advantage will be found in the south-west. If no (further) operations be called for, there will be good fortune in coming back (to the old conditions). If some operations be called for, there will be good fortune in the early conducting of them."
h$[41] = "SUN: In (what is denoted by) Sun, if there be sincerity (for him who employs it), there will be great good fortune:--freedom from error; firmness and correctness that can be maintained; and advantage in every movement that shall be made. In what shall this (sincerity in the exercise of Sun) be employed? (Even) in sacrifice two baskets of grain, (though there be nothing else), may be presented."
h$[42] = "YI: Yi indicates that (in the state which it denotes) there will be advantage in every movement which shall be undertaken, that it will be advantageous (even) to cross the great stream."
h$[43] = "KWAI: Kwai requires (in him who would fulfil its meaning) the exhibition (of the culprit's guilt) in the royal court, and a sincere and earnest appeal (for sympathy and support), with a consciousness of the peril (in cutting off the criminal). He should (also) make announcement in his own city, and show that it will not be well to have recourse at once to arms. (In this way) there will be advantage in whatever he shall go forward to."
h$[44] = "KAU: Kau shows a female who is bold and strong. It will not be good to marry (such) a female."
h$[45] = "TZHUI: In (the state denoted by) Tzhui, the king will repair to his ancestral temple. It will be advantageous (also) to meet with the great man; and then there will be progress and success, though the advantage must come through firm correctness. The use of great victims will conduce to good fortune; and in whatever direction movement is made, it will be advantageous."
h$[46] = "SHANG: Shang indicates that (under its conditions) there will be great progress and success. Seeking by (the qualities implied in it) to meet with the great man, its subject need have no anxiety. Advance to the south will be fortunate."
h$[47] = "KHWAN (also spelled KHWEN): In (the condition denoted by) Khwan there may (yet be) progress and success. For the firm and correct, the (really) great man, there will be good fortune. He will fall into no error. If he make speeches, his words cannot be made good."
h$[48] = "TZING: (Looking at) Tzing, (we think of) how (the site of) a town may be changed, while (the fashion of) its wells undergoes no change. (The water of a well) never disappears and never receives (any Great) increase, and those who come and those who go can draw and enjoy the benefit. If (the drawing) have nearly been accomplished, but, before the rope has quite reached the water, the bucket is broken, this is evil."
h$[49] = "KO: (What takes place as indicated by) Ko is believed in only after it has been accomplished. There will be great progress and success. Advantage will come from being firm and correct. (In that case) occasion for repentance will disappear."
h$[50] = "TING: Ting gives the intimation of great progress and success."
h$[51] = "KAN (also spelled KHEN): Kan gives the intimation of ease and development. When (the time of) movement (which it indicates) comes, (the subject of the hexagram) will be found looking out with apprehension, and yet smiling and talking cheerfully. When the movement (like a crash of thunder) terrifies all within a hundred li, he will be (like the sincere worshipper) who is not (startled into) letting go his ladle and (cup of) sacrificial spirits."
h$[52] = "KAN (also spelled KEN): When one's resting is like that of the back, and he loses all consciousness of self; when he walks in his courtyard, and does not see any (of the persons) in it,--there will be no error."
h$[53] = "KIEN: Kien suggest to us the marriage of a young lady, and the god fortune (attending it). There will be advantage in being firm and correct."
h$[54] = "KWEI MEI: Kwei Mei indicates that (under the conditions which it denotes) action will be evil, and in no wise advantageous."
h$[55] = "FANG: Fang intimates progress and development. When a king has reached the point (which the name denotes) there is no occasion to be anxious (through fear of a change). Let him be as the sun at noon."
h$[56] = "LU: Lu intimates that (in the condition which it denotes) there may be some little attainment and progress. If the stranger or traveller be firm and correct as he ought to be, there will be good fortune."
h$[57] = "SUN: Sun intimates that (under the conditions which it denotes) there will be some little attainment and progress. There will be advantage in movement onward in whatever direction. It will be advantageous (also) to see the great man."
h$[58] = "TUI: Tui intimates that (under its conditions) there will be progress and attainment. (But) it will be advantageous to be firm and correct."
h$[59] = "HWAN: Hwan intimates that (under its conditions) there will be progress and success. The k9ing goes to his ancestral temple; and it will be advantageous to cross the great stream. It will be advantageous to be firm and correct."
h$[60] = "KIEH: Kieh intimates that (under its conditions) there will be progress and attainment. (But) if the regulations (which it prescribes) be severe and difficult, they cannot be permanent."
h$[61] = "KUNG FU: Kung Fu (moves even) pigs and fish, and leads to good fortune. There will be advantage in crossing the great stream. There will be advantage in being firm and correct."
h$[62] = "HSIAO KWO: Hsiao Kwo indicates that (in the circumstances which it implies) there will be progress and attainment. But it will be advantageous to be firm and correct. (What the name denotes) may be done in small affairs, but not in great affairs. (It is like) the notes that come down from a bird on the wing;--to descend is better than to ascend. There will (in this way) be great good fortune."
h$[63] = "KI TZI: Ki Tzi intimates progress and success in small matters. There will be advantage in being firm and correct. There has been good fortune in the beginning; there may be disorder in the end."
h$[64] = "WEI TZI: Wei Tzi intimates progress and success (in the circumstances which it implies.) (We see) a young fox that has nearly crossed (the stream), when its tail gets immersed. There will be no advantage in any way."
i = randomnumber
i = floor(rnd()*64)+1
print h$[i]

%choice repeat or end
print"Touch here if you want to ask another question. Otherwise, please press BACK to exit."
tapped = 0
do
until tapped
goto START
on consoletouch:
tapped = 1
consoletouch.resume
on backkey:
gr.open
gr.orientation 1
gr.color 255, 255, 0, 0, 1
gr.rect rct2, 0, 0, 480, 780
gr.color 255, 255, 255, 255, 1
gr.rect rct3, 15, 15, 465, 765
gr.color 255, 0, 0, 0, 1
gr.text.size 60
gr.text.draw t8, 100, 390, "GOODBYE"
dialog.message"DISCLAIMER:", "Thank you for playing, but remember that this is only a game. Do not make life decisions based on these results.", ok, "OK"
gr.render
pause 2000
exit
