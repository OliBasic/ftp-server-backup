DEVICE               dev$
pos2               = is_IN ("Model", dev$)
pos3               = is_IN ("Device", dev$)
pos4               = is_IN ("OS", dev$)
outFile$           =            MID$(dev$, 9     , pos2-10)     + "_"
outFile$           = outFile$ + MID$(dev$, pos2+8, pos3-pos2-9) + "_"
outFile$           = outFile$ + MID$(dev$, pos4+5, LEN(dev$)-pos2+8) + "_.txt"
PRINT                outFile$

PRINT                "opening ftp...."
FTP.OPEN             "ftp.laughton.com" ,21,"basic","basic"
FTP.CD               "other/systemBinPrograms/data"
FTP.DIR              dirFtp
LIST.SIZE            dirFtp, sz
FOR i              = 1 TO sz
 LIST.GET            dirFtp,i, tmp$
 IF tmp$           = outFile$   THEN flagPresent=1
NEXT


IF !flagPresent

 CLS
 PRINT                dev$
 PRINT                "---------"
 SYSTEM.OPEN
 SYSTEM.WRITE         "ls /system/bin/"
 DO
  tic               = CLOCK()
  DO
   SYSTEM.READ.READY  rReady
  UNTIL               rReady | CLOCK()-tic > 400
  SYSTEM.READ.LINE    tmp$
  PRINT               tmp$
 UNTIL                !rReady
 SYSTEM.CLOSE
 CONSOLE.SAVE         outFile$
 PRINT                "------------"
 PRINT                "uploading list...
 FTP.PUT              outFile$, outFile$
 PRINT                "...ready"
 FILE.DELETE          nn,outFile$

ELSE

 PRINT                outFile$; " ...is already present"

ENDIF

PRINT                 "...thanks & good bye"
FTP.CLOSE

END

