flagOri            = 0
refW               = 790
refH               = 1255
IF !flagOri          THEN SWAP refW, refH
GR.OPEN              255, 0 , 0 , 0 ,1,flagOri
GR.SCREEN            curW, curH
scaW               = curW / refW
scaH               = curH / refH
GR.SCALE             scaW , scaH
PAUSE                500



! the canvas and infotext ------
cvx                = refw * 0.95
cvy                = refh * 0.8
ARRAY.LOAD           cvC[], 20, 20 , 70

GR.BITMAP.CREATE     canvas, cvx, cvy

GOSUB init

! graphs propertys ------------
nch       = 9
DIM         y[ nch ] , yo[ nch ]
ARRAY.LOAD  facY[], 50, 50, 50, 30, 20, 30, cvy/200, cvy/200, cvy/200
ARRAY.LOAD  offY[], cvy/2,cvy/2,cvy/2,cvy/4,cvy/4,cvy/4, cvy, cvy, cvy
ARRAY.LOAD  stroke [], 3, 2, 1, 2, 2, 3, 4, 1, 4
ARRAY.LOAD  c[],~
200 , 200 , 200 , ~
000 , 255 , 255 , ~
255 , 255 , 000 , ~
255 , 255 , 255 , ~
000 , 000 , 255 , ~
127 , 090 , 040 , ~
255 , 000 , 000 , ~
008 , 175 , 000 , ~
030 , 255 , 000


! stripe-width (=moving speed) ------
dx                  = 2


! divers -----------------------------

WAKELOCK 1

ARRAY.LOAD            vib1 [], 0, 50
ARRAY.LOAD            vib2 [], 0, 15, 75, 15

dtdesLoop           = 60

kFiltToc            = 0.02
tocFilt             = dtdesLoop

LIST.CREATE           n, storage

ngridHori           = 8
dygrid              = cvy/ ngridHori
gridTime            = 5000 - dtdesLoop/2

tic                 = CLOCK()
ticIni              = tic
tgrid               = tic+ gridTime

!main --------------------------------
DO

 LET  tmoni          = tic - ticIni
 LET  ctr            = ctr + 1
 LET  shift          = ROUND( dx *MAX( toc ,dtdesloop)/dtdesloop )
 !PRINT ctr,tmoni, shift, toc


 !update data -------------
 LET y [1]           = offY[1] - sin (tmoni/10000) * facY[1]
 LET y [2]           = offY[2] - sin (tmoni/2100) * facY[2]
 LET y [3]           = offY[3] - sin (tmoni/700)  * facY[3]
 LET y [4]           = offY[4] - sin (tmoni/1500) * facY[4]
 LET y [5]           = offY[5] - sin (tmoni/3000) * facY[5]
 LET y [6]           = offY[6] - sin (tmoni/200)  * facY[6]
 LET y [7]           = offY[7] - dtdesLoop        * facY[7]
 LET y [8]           = offY[8] - toc              * facY[8]
 LET y [9]           = offY[9] - tocFilt          * facY[9]
 LIST.ADD              storage, tmoni,ctr,y[1],y[2],y[3],y[4],y[5],y[6],y[7],y[8],y[9]


 !move & 'wipe-out' stripe -------
 GR.BITMAP.DRAW        dummy, canvas, -shift , 0
 GR.COLOR              255, cvC [1], cvC [2], cvC [3],1
 GR.RECT               dummy, cvx-shift , 0, cvx, cvy


 !apply  horizontal grid ---------
 GR.COLOR              255, cvC [1]+30, cvC [2]+30, cvC [3]+30,1
 GR.SET.STROKE         2
 FOR i               = 0 TO ngridHori
  GR.LINE              dummy , cvx-shift , i*dygrid+1, cvx , i*dygrid+1
 NEXT
 !apply vertical grid ------------
 IF                    tic>= tgrid  THEN gosub tgrid


 !apply graphs -----------------
 FOR i               = 1 TO nch
  LET k1             = (i-1)*3
  GR.COLOR             255, c[k1+1], c[k1+2], c[k1+3], 1
  GR.SET.STROKE        stroke [i]
  GR.LINE              dummy, cvx-shift , yo[i], cvx, y[i]
  LET yo[i]          = y[i]
 NEXT

 !update info-text ---------------
 GR.MODIFY             tt,"text" ,~
 "ctr :"              + INT$(ctr) +"    "+ ~
 "average loopTime :" + INT$(ROUND(( tocFilt ))) +" [ms]" +"    "+ ~
 "average fps :"      + STR$(ROUND((1000/tocFilt),1)) +" [1/s]"


 !render ------------------
 IF                    !BACKGROUND()  THEN GR.RENDER

 !check for user-input ------
 LET touOld          = tou
 GR.TOUCH              tou , toux, touy
 IF                    tou & !touOld THEN GOSUB touched
 IF                    tou &  touOld THEN GOSUB holded
 IF                   !tou &  touOld THEN GOSUB released


 !clocking -----------------
 LET toc             = CLOCK()-tic
 LET tocFilt         = tocFilt + (toc-tocFilt) * kFiltToc


 !refresh/scheduler/time-stamp------
 IF !MOD(ctr, 150)    THEN GOSUB init
 PAUSE                 MAX(dtdesLoop-toc, 1)
 LET tic             = CLOCK()

UNTIL0
! -----------------------------------------

END


! ---------------------------------------
init:

GR.BITMAP.DRAWINTO.END

IF !ctr THEN pcx= (refw-cvx)/2 ELSE GR.GET.POSITION  canvas, pcx, pcy
GR.CLS
GR.BITMAP.DRAW        _canvas, canvas, pcx , (refh-cvy)/2

! info-text --------
GR.COLOR              255, 255, 255, 255, 1
GR.TEXT.SIZE          22
GR.TEXT.ALIGN         1
GR.TEXT.DRAW          tt, (refw-cvx)/2, refh-(refh-cvy)/2+25 ,"--"

GR.BITMAP.DRAWINTO.START canvas

IF !ctr               THEN  GR.COLOR 255, cvC [1], cvC [2], cvC [3],1
IF !ctr               THEN  GR.RECT dummy, 0, 0, cvx, cvy

GR.TEXT.ALIGN         3
GR.TEXT.SIZE          18

RETURN
! ---------------------------------------


! ---------------------------------------
tgrid:
LET tgrid            = tic + gridTime
GR.LINE                dummy, cvx, 0, cvx, cvy
GR.COLOR               230, 255, 255, 255,1
GR.TEXT.DRAW           dummy, cvx,cvy-15, INT$(ROUND(tmoni/1000))
RETURN
! --------------------------------------

! input handles -------------------
touched:
VIBRATE                vib1[],-1
GR.GET.POSITION        canvas, cx, cy
LET x0               = cx-toux
LET y0               = cy-touy
RETURN

holded:
GR.MODIFY              canvas, "x", x0+toux %, "y", y0+touy
RETURN

released:
VIBRATE                vib2[],-1
RETURN
!--------------------------
