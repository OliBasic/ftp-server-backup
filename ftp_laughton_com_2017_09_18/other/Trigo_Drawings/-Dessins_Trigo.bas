REM Dessins Mathématiques...   @Cassiope34  031415 !!

Fn.def clr( cl$ )
  gr.color 255, val(word$(cl$,1)), val(word$(cl$,2)), val(word$(cl$,3)), 0
Fn.end

gr.open 0,0,0,0,0,0
gr.screen scrx,scry
svx =1280
svy =800
sx  =scrx/svx
sy  =scry/svy
gr.scale sx,sy

ctx =svx/2
cty =svy/2

DIM b[10,2], clr$[10]
b[1,1] =12*pi()  % valeurs de fin de boucle
b[2,1] =2*pi()
b[3,1] =4*pi()
b[4,1] =4*pi()
b[5,1] =2*pi()
b[6,1] =4*pi()
b[7,1] =4*pi()
b[8,1] =160
b[9,1] =40*pi()
b[10,1]=12*pi()
b[1,2] =0.05     % valeurs de pas.  'step'
b[2,2] =0.015
b[3,2] =0.04
b[4,2] =0.02
b[5,2] =0.02
b[6,2] =0.004
b[7,2] =0.004
b[8,2] =0.06
b[9,2] =0.04
b[10,2]=0.04

clr$[1] ="255 0 0"
clr$[2] ="0 255 0"
clr$[3] ="0 0 255"
clr$[4] ="255 255 0"
clr$[5] ="0 255 255"
clr$[6] ="255 0 255"
clr$[7] ="0 128 128"
clr$[8] ="128 128 0"
clr$[9] ="0 128 0"
n =1
List.create N,ld

gr.text.size 38
gr.color 255,241,231,160,1
gr.text.draw mess, 20, 50, int$(n)

array.load tmp[], 40,10,80,60,10,60
list.add.array ld,tmp[]
gr.poly dess, ld

gr.set.stroke 2
call clr( clr$[n] )
gr.paint.get pain_t
gosub datas

DO
  do
    gr.touch touched,x,y
    if !background() then gr.render
  until touched
  
  n =n+1-9*(n=9)
  gr.modify mess, "text", int$(n)
  call clr( clr$[n] )
  gr.paint.get pain_t
  gosub datas
  if tch then gr.modify dess, "list", ld
UNTIL quit

gr.close
end

datas:
tch =0
list.clear ld
gr.modify dess, "paint", pain_t
for teta=0 to b[n,1] step b[n,2]
 if n=1
  rad   =cos(teta)*sin(teta/3)*sin(teta/2)*80*(3+cos(teta*6.333))
  px    =ctx+rad*cos(teta)*2
  py    =cty+rad*sin(teta)*2
  
 elseif n=2
  rad   =-1*(0.5*sin(5*teta))*(0.5*cos(4*teta))*1000
  angle =teta+sin(rad/100)
  px    =ctx+rad*cos(angle)*1.8
  py    =cty+rad*sin(angle)*1.8+100
 
 elseif n=3
  rad   =(1.05+sin(teta*4.5))*100
  angle =teta-cos(teta*10)/10
  px    =ctx+rad*cos(angle)*1.5
  py    =cty+rad*sin(angle)*1.5
 
 elseif n=4
  px    =ctx+sin(teta*5)*300
  py    =cty+cos(teta*5.5)*300
  
 elseif n=5
  px    =ctx+sin(teta*5)*cos(teta*6)*300
  py    =cty+cos(teta*5.5)*sin(teta*6.5)*300
  
 elseif n=6
  px    =exp(sin(teta*10))*230+200
  t     =exp(cos(teta*9.5))*180+110
  py    =t+sin(px/20)*20
  px    =px+sin(t/20)*20+100
  
 elseif n=7
  xt    =ctx+sin(teta*10)*230
  yt    =cty+cos(teta*9.5)*200
  nthet =xt/30+yt/30
  px    =xt+cos(nthet)*20
  py    =yt+sin(nthet)*20
  
 elseif n=8
  xt    =ctx+sin(teta)*teta*2
  yt    =cty+cos(teta)*teta*2
  nthet =xt/30+yt/30
  othet =xt/30-yt/30
  px    =xt+cos(nthet)*15+cos(othet)*15
  py    =yt+sin(nthet)*15+sin(othet)*15
  
 elseif n=9
  rad  =(1.4+sin(teta*3.05))*110
  xt   =ctx+sin(teta)*rad
  yt   =cty+cos(teta)*rad
  py   =yt+sin(xt/20)*15
  px   =xt+cos(yt/20)*15
 
 endif
 list.add ld, px, py
 if !tch
   gr.modify dess, "list", ld
   gr.render
   gr.touch tch,x,y
   if tch then popup " Please wait... ",0,-500,0
 endif
 
next
return
