! 3 bodies are a problem
!
fn.def intro()
  t$="The 3 Body Problem is to try to figure out the future evolution of a system "
  t$=t$+"of 3 bodies under the influence of gravity, given the mass of the bodies "
  t$=t$+"and their initial positions and momenta. There is no general solution given by algebraic "
  t$=t$+"expressions and integrals, and no periodic one, except in special cases.  "
  t$=t$+"This program allows you to simulate the 3 body problem after setting the "
  t$=t$+"initial conditions.\n\n"
  t$=t$+"You can select the option of 2 bodies; it is relatively easy to get a  "
  t$=t$+"stable periodic system with 2 bodies.  You will be prompted to enter the "
  t$=t$+"initial parameters for each body.  Then the fun begins.  There are zoom "
  t$=t$+"controls in the lower right corner, and tapping anywhere on the screen "
  t$=t$+"allows you to stop or restart the program." 
  Text.input x$,t$,"Program Description"
fn.end
!
! set color from color number
fn.def setcolor(ncolor,style)
  a=shift(band(ncolor,hex("ff000000")),24)
  r=shift(band(ncolor,hex("00ff0000")),16)
  g=shift(band(ncolor,hex("0000ff00")),8)
  b=band(ncolor,hex("000000ff"))
  gr.color a,r,g,b,style
fn.end
!
! taken from mougino
FN.DEF URL_DECODE$(e$) %' (internal) URL-decode the string
  IF e$="" THEN FN.RTN ""
  c = 1
  DO
    a$=MID$(e$, c, 1)
    IF a$="+" THEN
      r$+=" "
    ELSEIF a$="%" THEN
      r$+=CHR$(HEX(MID$(e$, c+1, 2)))
      c+=2
    ELSE
      r$+=a$
    END IF
    c++
  UNTIL c=LEN(e$)+1
  FN.RTN r$
FN.END
!
!
fn.def num$(t$)
! guarantees numeric string
  t$=trim$(t$)
  if t$="" then fn.rtn "0"
  ret$=""
  Array.load stage$[],"+-0123456789",".0123456789","0123456789"
  st=1
  for i=1 to len(t$) 
    p$=mid$(t$,i,1)
    if is_in(p$,stage$[st])
      ret$=ret$+p$
      if st=1 then st=2
      if st=2 & p$="." then st=3
    else
      f_n.break
    endif  
  next i
  if ret$="" then ret$="0"
  fn.rtn ret$  
fn.end
!
!
! distance between 2 points
fn.def distance(x1,y1,z1,x2,y2,z2)
 d = SQR((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2))
 if d = 0 then d = 10e-10
 fn.rtn d
fn.end
!
fn.def makebutton(color, caption$, left, top, right, bottom)
! returns group object
    setcolor(color, 1)
    Gr.rect face,left+2,top+2,right-2,bottom-2
    Gr.color 255,0,0,0
    Gr.text.draw dc,left+7,bottom-6,caption$
    Gr.color 255,255,255,255
    Gr.line ltop,left+1,top+1,right-1,top+1
    Gr.line lleft,left+1,top+1,left+1,bottom-1
    Gr.color 255,40,40,75
    Gr.line lright,right-1,top+1,right-1,bottom-1
    Gr.line lbottom,left+1,bottom-1,right-1,bottom-1
    setcolor(band(color,hex("ff555500")),1)
    Gr.rect face1,left+2,top+2,right-2,bottom-2
    Gr.hide face1
    Gr.color 255,0,0,0
    Gr.text.draw dc1,left+7,bottom-6,caption$
    Gr.hide dc1
    Gr.color 255,40,40,75
    Gr.line ltop1,left+1,top+1,right-1,top+1
    Gr.hide ltop1
    Gr.line lleft1,left+1,top+1,left+1,bottom-1
    Gr.hide lleft1
    Gr.color 255,255,255,255
    Gr.line lright1,right-1,top+1,right-1,bottom-1
    Gr.hide lright1
    Gr.line lbottom1,left+1,bottom-1,right-1,bottom-1
    Gr.hide lbottom1
    Gr.group mybut,face,dc,ltop,lleft,lright,lbottom,face1,dc1,ltop1,lleft1,lright1,lbottom1
  fn.rtn mybut
fn.end
!
!
fn.def bmessage(left,lbtext,rbtext,tstr$)
   if left
     obj = lbtext
   else
     obj = rbtext
   endif  
   Gr.modify obj,"text",tstr$
fn.end
!
!
! merge bodies on collision
fn.def collision(i,j,bodies[],nbods)
   tone 220,300,0
   m=bodies[i,1]+bodies[j,1]
   for k=1 to 3
     bodies[i,k]=(bodies[i,k]+bodies[j,k])/2
     bodies[i,k+3]=(bodies[i,1]*bodies[i,k+3]+bodies[j,1]*bodies[j,k+3])/m
   next k
   bodies[i,1]=m
   bodies[i,8]=bor(bodies[i,8],bodies[j,8])
   if j=2 & nbods>2
       for k = 1 to 11
         bodies[j,k]=bodies[j+1,k]
       next k
      setcolor(bodies[j,8],1)
     Gr.paint.get pnt
     Gr.modify bodies[j,12],"paint",pnt
   endif
   Gr.hide bodies[nbods,12]
   nbods = nbods-1
   setcolor(bodies[i,8],1)
   Gr.paint.get pnt
   Gr.modify bodies[i,12],"paint",pnt
fn.end
!
fn.def reorder(low,high,bodies[],nbods,base)
  Gr.getDL dls[],1
  dls[base]=bodies[low,12]
    for i=1 to 3
      if i<>low & i<>high
        middle=i
        f_n.break
      endif
    next i
    if nbods=2
       dls[base+1]=bodies[high,12]
       dls[base+2]=bodies[middle,12]
    else
      dls[base+1]=bodies[middle,12]
      dls[base+2]=bodies[high,12]
    endif
      Gr.newDL dls[]
fn.end
!
! initialize bodies
fn.def ini_bodies(bodies[],radius,swid,shgt,sdep,nbods)
! colors
  red=hex("efff0000")
  green=hex("ef00ff00")
  blue=hex("ef0000ff")
  bodies[1,8]=red
  bodies[2,8]=green
  bodies[3,8]=blue
  for i=1 to 3
! masses
    bodies[i,1]=round(10+rnd()*89,1)
!   positions
    bodies[i,2]=round((rnd()-1/2)*(swid/2),1)
    bodies[i,3]=round((rnd()-1/2)*(shgt/2),1)
    bodies[i,4]=round((rnd()-1/2)*(sdep/4),1)
!   velocities
    bodies[i,5]=round((rnd()-1/2)*(radius/6),1)
    bodies[i,6]=round((rnd()-1/2)*(radius/6),1)
    bodies[i,7]=round((rnd()-1/2)*(radius/6),1)   
! object
  setcolor(bodies[i,8],1)
  if bodies[i,12]=0
    gr.circle bodies[i,12],bodies[i,2]+swid/2,bodies[i,3]+shgt/2,radius
  else
    gr.modify bodies[i,12],"x",bodies[i,2]+swid/2,"y",bodies[i,3]+shgt/2,"radius",radius
   Gr.paint.get pnt
   Gr.modify bodies[i,12],"paint",pnt
  endif
  Gr.hide bodies[i,12]
  next i
  gr.render
fn.end
!
fn.def draw_body(k,radius,scale,sdep,ylim,xoffset,yoffset,zoffset,bodies[])
   d2eye = distance(bodies[k,2],bodies[k,3],bodies[k,4],0,0,zoffset)
   disprad=radius*scale*zoffset/(zoffset-bodies[k,4])
    if disprad<1 then disprad = 1
   zx = zoffset-bodies[k,4]
   alpha=255
   if bodies[k,4]<0 then alpha=80+min(175*abs(zoffset/zx),175)
   dispx=(abs(zoffset/zx))*(bodies[k,2]*scale)+xoffset
   dispy=(abs(zoffset/zx))*(bodies[k,3]*scale)+yoffset
   Gr.modify bodies[k,12],"x",dispx,"y",dispy,"radius",disprad,"alpha",alpha
   bodies[k,9]=dispx
   bodies[k,10]=dispy
   bodies[k,11]=disprad
   if bodies[k,4]>zoffset*2/scale | dispy>ylim then Gr.hide bodies[k,12]
   if bodies[k,4]<=zoffset*2/scale & dispy<=ylim then Gr.show bodies[k,12]
fn.end
!
fn.def pform(i,dstr$[],bodies[])
 frm$ = "<html><head><style type='text/css'>body {background-color:CornSilk}</style>"
  frm$ = frm$ + "<title>" + dstr$[i] + " Object Parameters</title></head>"
  frm$ = frm$ + "<script type='text/javascript'> function doDataLink(data) {"
  frm$ = frm$ + "var ret=''; var x=document.getElementById('frm1');"
  frm$ = frm$ + "var del = String.fromCharCode(127);"
  frm$ = frm$ + "for (var i=0;i<x.length;i++) {"
  frm$ = frm$ + " if (x.elements[i].id == 1) {"
  frm$ = frm$ + " ret = ret + x.elements[i].value + del ;"
  frm$ = frm$ + "      }"
  frm$ = frm$ + "   }"
  frm$ = frm$ + " ret = ret + data; Android.dataLink(ret);}</script>"
  frm$ = frm$ + "<body><form id='frm1' method='get' action='FORM'>" 
  frm$ = frm$ + "<h2>"+dstr$[i]+" Object Parameters</h2>"
  frm$ = frm$+"<p>Mass <input size='6' type='text' id=1 value='"+str$(bodies[i,1])+"'>"
  frm$ = frm$+"<p>X <input size='6' type='text' id=1 value='"+str$(bodies[i,2])+"'>"
  frm$ = frm$+" Y <input size='6' type='text' id=1 value='"+str$(bodies[i,3])+"'>"
  frm$ = frm$+" Z <input size='6' type='text' id=1 value='"+str$(bodies[i,4])+"'>"
  frm$ = frm$+"<p>Vx <input size='6' type='text' id=1 value='"+str$(bodies[i,5])+"'>"
  frm$ = frm$+" Vy <input size='6' type='text' id=1 value='"+str$(bodies[i,6])+"'>"
  frm$ = frm$+" Vz <input size='6' type='text' id=1 value='"+str$(bodies[i,7])+"'>"
  frm$ = frm$ + "<p><input type='button' value='" + "OK" + "'"
  frm$ = frm$ + " OnClick=doDataLink('" + "OK" +"')>"
  frm$ = frm$ + "</form></body></html>"
 Html.open
 Html.load.string frm$
 ret$ = ""
 do
   html.get.datalink data$
   if left$(data$,4)="DAT:" then ret$ = ret$ + mid$(data$,5)
 until data$ <> ""
 Html.close
 for j = 1 to 7
   bodies[i,j]=val(num$(url_decode$(word$(ret$,j,chr$(127)))))
 next j
fn.end
!
!
  nmax=3
  nbods=nmax
  call intro()
  Gr.Open 0,0,0,0,0,0
  ydelta=30
  Gr.screen swid,shgt
  sdep=max(swid,shgt)
  ylim=(shgt-ydelta)
  xlim=swid
  zlim=sdep
  yoffset=ylim/2
  xoffset=xlim/2
  zoffset=sdep/2
  cx=xoffset
  cy=yoffset
  axsize=2*ylim/3
  radius=max(swid,shgt)/60
  Grav=6.673*radius
  dim bodies[nbods,12]
  for i=1 to nmax
    bodies[i,12]=0
  next i
  base=0
  Array.load dstr$[],"1st","2nd","3rd"
  scale=0.5
  gosub ini_screen
  games=1
  while games
    scale=0.5
    gosub axes
    Dialog.message "Number of bodies: ","Select One" ,which,"Two","Three"
    nbods=which+1
    ini_bodies(bodies[],radius,swid,shgt,sdep,nbods)
    if base=0 then base=bodies[1,12]
    for i=1 to nbods
      bmessage(1,lbtext,rbtext,"Tap to position "+dstr$[i]+" object!")
      Gr.render
      tstat=0
      while !tstat
        Gr.touch tstat,mx,my
      repeat
      bodies[i,2]=round(mx-xoffset,2)
      bodies[i,3]=round(my-yoffset,2)
      draw_body(i,radius,scale,sdep,ylim,xoffset,yoffset,zoffset,bodies[])
      Gr.render
      pform(i,dstr$[],bodies[])
      draw_body(i,radius,scale,sdep,ylim,xoffset,yoffset,zoffset,bodies[])
      Gr.render
    next i
!
    pause 30
    alive=1
    count=10
    t0=int(clock()/1000)
    t1=t0
    while alive
      if count>=10
       bmessage(1,lbtext,rbtext,"Tap screen for options menu.")
        count=0
      endif
      count=count+1
      gosub move_bodies
      buthit=0
      Gr.touch touched,mx,my
      if touched
        Gr.modify mcursor,"x",mx,"y",my
        for i=1 to 2
          if gr_collision(mcursor,buttons[i,5])
            buthit=1
            Gr.show.toggle buttons[i,6]
            Gr.render
            if i=1 then 
              fac=2
            else 
              fac=1/2
            endif
            scale=scale*fac
            gosub axes
            pause 250
            Gr.show.toggle buttons[i,6]
            Gr.render
          endif
        next i
        if !buthit
          Dialog.message "Options:","",nop,"Continue","Exit","Restart"
          sw.begin nop
          sw.case 2
            exit
          sw.break  
          sw.case 3
            alive=0
            for i=1 to nbods
              GR.hide bodies[i,12]
            next i
            sw.break
          sw.case 1 
          sw.default
            sw.break
        sw.end
      endif
     endif
     t2=int(clock()/1000)
     if t2>t1
       t1=t2
       ms$="Elapsed: "+str$(t2-t0)
       bmessage(0,lbtext,rbtext,ms$)
      Gr.render
     endif
    pause 10
    Gr.render
    repeat
  repeat
  end
!
ini_screen:
  setcolor(hex("ff00ff00"),0)
  Gr.rect wframe,0,0,xlim-1,ylim-1
  Gr.line hax,cx-axsize*scale,cy,cx+axsize*scale,cy
  Gr.line vax,cx,cy-axsize*scale,cx,min(cy+axsize*scale,ylim)
  setcolor(hex("ffffff00"),1)
  Gr.text.size 20
  Gr.text.draw lbtext,20,shgt-10," "
  Gr.text.draw rbtext,cx,shgt-10," "
  setcolor(0,0)
  Gr.circle mcursor,0,0,2
  G R.render
  gosub ini_buttons
return
!
ini_buttons:
  Array.load captions$[],"+","-"
  Gr.color 255,255,255,0
  Gr.text.draw dc,swid-165,shgt-5,"Zoom"
  dim buttons[2,7]
  for i=1 to 2
    buttons[i,1]=swid-100+35*(i-1)
    buttons[i,2]=shgt-26
    buttons[i,3]=swid-100+26+35*(i-1)
    buttons[i,4]=shgt-1
    Gr.color 255,255,255,127
    Gr.rect buttons[i,5],buttons[i,1],buttons[i,2],buttons[i,3],buttons[i,4]
    buttons[i,6]=makebutton(hex("ffffff00"),captions$[i],buttons[i,1],buttons[i,2],buttons[i,3],buttons[i,4])
  next i
return
!
axes:
  Gr.modify hax,"x1",cx-axsize*scale,"y1",cy,"x2",cx+axsize*scale,"y2",cy
  Gr.modify vax,"x1",cx,"y1",cy-axsize*scale,"x2",cx,"y2",min(cy+axsize*scale,ylim)
return
!
move_bodies:
  high=1
  low=1
  for i=1 to nbods
    bodies[i,2]=bodies[i,2]+bodies[i,5]
    bodies[i,3]=bodies[i,3]+bodies[i,6]
    bodies[i,4]=bodies[i,4]+bodies[i,7]
    if bodies[i,4]<bodies[low,4] then low=i
    if bodies[i,4]>=bodies[high,4] then high=i
    for j=i+1 to nbods
      if j>nbods then f_n.break
      d2p=distance(bodies[j,2],bodies[j,3],bodies[j,4],bodies[i,2],bodies[i,3],bodies[i,4])
      if d2p<radius
        collision(i,j,bodies[],&nbods)
      endif
      accel=Grav*bodies[i,1]/(d2p*d2p)
      ax = accel*(bodies[i,2]-bodies[j,2])/d2p
      ay = accel*(bodies[i,3]-bodies[j,3])/d2p
      az = accel*(bodies[i,4]-bodies[j,4])/d2p
      bodies[j,5]=bodies[j,5]+ax
      bodies[j,6]=bodies[j,6]+ay
      bodies[j,7]=bodies[j,7]+az
      accel=Grav*bodies[j,1]/(d2p*d2p)
      ax = accel*(bodies[j,2]-bodies[i,2])/d2p
      ay = accel*(bodies[j,3]-bodies[i,3])/d2p
      az = accel*(bodies[j,4]-bodies[i,4])/d2p
      bodies[i,5]=bodies[i,5]+ax
      bodies[i,6]=bodies[i,6]+ay
      bodies[i,7]=bodies[i,7]+az
    next j
  next i
  if nbods>1 then reorder(low,high,bodies[],nbods,base)
  gosub draw_bodies
  Gr.render
return
!
! draw all bodies
draw_bodies:
  for i=1 to nbods
      draw_body(i,radius,scale,sdep,ylim,xoffset,yoffset,zoffset,bodies[])
  next i
return
!
