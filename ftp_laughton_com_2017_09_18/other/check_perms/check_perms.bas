CLS
?"CHECK ABSENCE OF PERMISSIONS - Click on a line."
?"1. DEVICE command"
?"2. GPS.* commands"
?"3. VIBRATE command"
?"4. WAKELOCK command"
?"5. GR.CAMERA.* commands"
?"6. STT.* commands"
?"7. TTS.* commands"
?"8. EMAIL.SEND command"
?"9. FTP.* commands"
?"10. GRABURL command"
?"11. BT.* commands"
?"12. SMS.SEND command"
?"13. SMS.RCV.* commands"
?"14. PHONE.CALL command"
?"15. PHONE.DIAL command"
?"16. PHONE.RCV.* commands"
?"17. PHONE.INFO command"
?"18. MYPHONENUMBER command"
?"19. WIFI.INFO command"
?"20. HTTP.POST command"
?"21. SOCKET.* commands"
?"22. HTML.* commands (only with \"http\" protocol)"
?"23. VERSION$() command"

DO: PAUSE 1: UNTIL 0
ONCONSOLETOUCH:
CONSOLE.LINE.TOUCHED demo
IF --demo=0 THEN CONSOLETOUCH.RESUME
CLS
GOTO demo, dev, gps, vibr, waklok, camera, stt, tts, email, ~
     ftp, grurl, bt, smssnd, smsrcv, phoncall, phondial, ~
     phonrcv, phonnfo, mypphonnb, wifinf, post, sockt, htm, ver
END

dev:
?"1. Device info:"
DEVICE d$
?d$
PAUSE 2000
END

gps:
?"2. GPS status:"
GPS.OPEN
GPS.STATUS s$
?s$
PAUSE 2000
END

vibr:
?"3. Vibrating..."
ARRAY.LOAD v[], 0, 500
VIBRATE v[], -1
PAUSE 1000
VIBRATE v[], -1
END

waklok:
?"4. Setting wakelock to 'screen bright'..."
WAKELOCK 3
PAUSE 2000
?"Unsetting wakelock..."
WAKELOCK 5
PAUSE 1000
END

camera:
POPUP "5. Camera autoshoot demo"
GR.OPEN
GR.CAMERA.AUTOSHOOT bmp
GR.BITMAP.DRAW ptr, bmp, 0, 0
PAUSE 2000
END

stt:
?"6. Speech to text demo"
STT.LISTEN
STT.RESULTS list
DEBUG.ON
DEBUG.DUMP.LIST list
PAUSE 2000
END

tts:
?"7. Text to speech demo"
TTS.INIT
POPUP "RFO Basic"
TTS.SPEAK "RFO Basic"
PAUSE 2000
END

email:
POPUP "8. E-mail sEND demo\nThis should work even w/o INTERNET permission"
EMAIL.SEND "nobody@nowhere.com", "Nothing", "..."
END

ftp:
?"9. FTP demo"
FTP.OPEN "ftp.laughton.com", 21, "basic", "basic"
?"Opened ftp.laughton.com"
FTP.DIR list
FTP.CLOSE
DEBUG.ON
DEBUG.DUMP.LIST list
PAUSE 2000
END

grurl:
POPUP "10. Graburl demo"
GRABURL t$, "http://rfo-basic.com"
?t$
PAUSE 2000
END

bt:
POPUP "11. Bluetooth demo"
BT.OPEN
BT.CONNECT
POPUP "Successfully Connected!\nDisconnecting..."
BT.DISCONNECT
BT.CLOSE
END

smssnd:
?"12. SMS send demo"
SMS.SEND "0628332504", "test"
PAUSE 2000
END

smsrcv:
?"13. SMS receive demo"
SMS.RCV.INIT
SMS.RCV.NEXT s$
IF s$="@" THEN ?"No SMS intercepted" ELSE ?"SMS intercepted:\n"+s$
PAUSE 2000
END

phoncall:
POPUP "14. Phone call demo"
PHONE.CALL "123" % voicemail
PAUSE 2000
END

phondial:
POPUP "15. Phone dial demo\n(no permission needed)"
PHONE.DIAL "123" % voicemail
PAUSE 2000
END

phonrcv:
?"16. Phone receive demo"
PHONE.RCV.INIT
?"You have 10 seconds to open a call and look at your screen for the status.\nReady? Go!"
PAUSE 10000
PHONE.RCV.NEXT state, c$
IF state THEN POPUP "Call in progress with "+c$ ELSE POPUP "No call in progress"
PAUSE 2000
END

phonnfo:
?"17. Phone info demo"
PHONE.INFO b
DEBUG.ON
DEBUG.DUMP.BUNDLE b
PAUSE 2000
END

mypphonnb:
?"18. My phone number:"
MYPHONENUMBER s$
?s$
PAUSE 2000
END

wifinf:
?"19. Wifi info demo"
WIFI.INFO ssid$, bssid$, mac$, ip$, mbps
?"SSID: "+ssid$
?"BSSID: "+bssid$
?"MAC: "+mac$
?"IP: "+ip$
?"Speed: ";mbps;" Mbps"
PAUSE 2000
END

post:
?"20. Http Post demo"
LIST.CREATE s, args
LIST.ADD args, "key", "val"
HTTP.POST "http://httpbin.org/response-headers", args, res$
?"Google search for 'rfobasic':"
?LEFT$(res$, 100)
PAUSE 2000
END

sockt:
?"21. Socket test"
SOCKET.CLIENT.CONNECT "time.nist.gov", 13, 0
PAUSE 1000
FOR i=1 TO 2
  SOCKET.CLIENT.READ.LINE line$
  ?line$
  PAUSE 500
NEXT
SOCKET.CLIENT.CLOSE
PAUSE 1500
END

htm:
HTML.OPEN
POPUP "22. No permission should be needed to load a local html"
HTML.LOAD.URL "htmldemo1.html"
POPUP "Now click any button or type Back to load a remote html..."
DO: HTML.GET.DATALINK data$: UNTIL data$ <> ""
HTML.LOAD.URL "http://laughton.com"
PAUSE 4000
END

ver:
?"23. Version$() demo"
?"(no permission should be needed)"
v$=VERSION$()
?"version: "+v$
PAUSE 2000
END
