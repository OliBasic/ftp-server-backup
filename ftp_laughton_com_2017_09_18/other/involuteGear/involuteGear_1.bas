GOSUB         userfun
GR.OPEN       255, 0, 0, 0, 0, -1
refw        = 800
refh        = 1280
GR.SCREEN     curw, curh
scaw        = curw/refw
scah        = curh/refh
GR.SCALE      scaw, scah
centx       = curw/2
centy       = curh/2

!draw coordinate-crosshair -----------------
GR.COLOR      255, 255, 255, 255, 0
GR.LINE       nn , 0, centy, curw, centy
GR.LINE       nn , centx, 0, centx, curh


!!
infos/params ----------------------------
link:
http://www.ahoefler.de/getriebelehre/evolventenverzahnung/evolventenverzahnung.php

alpha            eingriffswinkel (= 20º)
kkrad            kopf -kreisradius
r               waelz -kreisradius (=teilkreisradius) 
d                
rb              grund -kreisradius rb = r * cos(alpha)
fkrad            fuss -kreisradius
di               center-bore diameter
z               % zaehnezahl
m               % modul
p               % teilung

p             = PI * d / z = 2 * PI * r / z 
m             = d / z
d             = m * z

hf            = 1.15 *m  Fusshoehe
hk            = 1.0 *m  kopfhoehe
----------------------------------------
!!


!given ----------------------
alpha         = 20      % normally fix !!!!

mm            = 35
zz            = 15


!calc  ----------------------
dd            = mm*zz
rr            = dd/2
rb            = rr * COS( TORADIANS(alpha) )
dphi          = 360/zz

fkrad         = rr - 1.15 * mm
kkrad         = rr + 1.0 * mm
di            = dd/4


!draw base geonetry ----------------
GR.SET.STROKE   2
GR.COLOR        255, 255, 0, 0, 0
GR.CIRCLE       nn, centx, centy, rr
GR.SET.STROKE   1
GR.COLOR        255, 255, 255, 0, 0
GR.CIRCLE       nn, centx, centy, rb
GR.COLOR        155, 255, 0, 255, 0
GR.CIRCLE       nn, centx, centy, fkrad
GR.COLOR        255, 000, 255, 255, 0
GR.CIRCLE       nn, centx, centy, kkrad
GR.RENDER


!create needed container ----------
LIST.CREATE     n, evolvR
LIST.CREATE     n, evolvL
LIST.CREATE     n, tooth
LIST.CREATE     n, gear



!create master-involute ------------------
phioffs       = 90
phiO          = toradians ( phioffs )
dint          = PI()/100
LIST.ADD        evolvR , 0, -fkrad

DO
 x            =    rb *COS(t +phiO ) + rb *t*SIN(t +phiO )
 y            = -( rb *SIN(t +phiO ) - rb *t*COS(t +phiO ))
 LIST.ADD       evolvR , x, y
 t           += dint
UNTIL ABS(y) >= kkrad*0.99


!create master-tooth, put it into 'gear' -----------
LIST.ADD.LIST     evolvL, evolvR
CALL              mirror_x_Poly( evolvL )
CALL              rotate_Poly( evolvL , dphi*0.525 )
CALL              flip_Poly(evolvL)
LIST.ADD.LIST     tooth, evolvR
LIST.ADD.LIST     tooth, evolvL
LIST.ADD.LIST     gear, tooth


!draw gear/master tooth -----------------------
GR.SET.STROKE     4
GR.COLOR          255, 000, 255, 0, 0
GR.ROTATE.START   0, centx, centy, rotg1
GR.POLY           nn,  gear, centx, centy
GR.ROTATE.END
GR.RENDER
!PAUSE 1000


!complete gear ------------------------
FOR i=1 TO zz-1
 CALL              rotate_Poly( tooth , dphi )
 LIST.ADD.LIST     gear, tooth
 GR.RENDER
NEXT

! draw inner  bore -------------------
GR.CIRCLE         nn, centx,centy, di/2


!infos----------------------
LIST.SIZE         gear, szGear
PRINT             szGear
dyy             = 35
GR.COLOR          255, 255, 255, 255, 1
GR.TEXT.SIZE      22
GR.TEXT.TYPEFACE  2
GR.TEXT.DRAW   nn, 10, 2*dyy, "nTooth      :" + INT$(zz) +"[#]"
GR.TEXT.DRAW   nn, 10, 3*dyy, "Modul       :" + INT$(mm) +"[mm]"
GR.TEXT.DRAW   nn, 10, 4*dyy, "red Diameter:" + STR$(ROUND(rr,1))+"[mm]
GR.TEXT.DRAW   nn, 10, 5*dyy, "nDataPoints :" + INT$(szGear)+"[#]

GR.RENDER



!copy the gear ----------------
LIST.CREATE        n, gear2
LIST.ADD.LIST      gear2, gear
CALL               rotate_Poly( gear2 , dphi/2 )

!...and draw it --------------
GR.COLOR           255, 000, 000, 255, 0
GR.ROTATE.START    0, centx, centy+dd, rotg2
GR.POLY            g2, gear2, centx, centy+dd
GR.ROTATE.END
GR.CIRCLE         nn, centx,centy+dd, di/2
GR.SET.STROKE      1
GR.COLOR           255, 255, 255, 255, 0
GR.LINE             nn , 0, centy+dd, curw, centy+dd
GR.RENDER


DO
 
 gr.modify rotg1,"angle", rotAngle1++
 gr.modify rotg2,"angle", rotAngle2--
 gr.render
 !pause 20

UNTIL0

END


!-------------------------------------
userfun:

FN.DEF                 mirror_x_Poly(poly)
 LIST.SIZE             poly,sz
 FOR i               = 1 TO sz STEP 2
  LIST.GET             poly, i  , x
  LIST.REPLACE         poly, i  , -x
 NEXT
FN.END

FN.DEF                 rotate_Poly(poly, phi)
 sinphi              = SIN(TORADIANS(phi))
 cosphi              = COS(TORADIANS(phi))
 LIST.SIZE             poly,sz
 FOR i               = 1 TO sz STEP 2
  LIST.GET             poly, i  , x
  LIST.GET             poly, i+1, y
  LIST.REPLACE         poly, i  ,  x*cosphi + y*sinphi
  LIST.REPLACE         poly, i+1, -x*sinphi + y*cosphi
 NEXT
FN.END

FN.DEF                 flip_Poly(poly)
 LIST.SIZE             poly,sz
 LIST.TOARRAY          poly, tmp []
 FOR i               = 1 TO sz STEP 2
  LIST.REPLACE         poly, i   , tmp [sz-(i-1)-1]
  LIST.REPLACE         poly, i+1 , tmp [sz-(i-1)  ]
 NEXT
FN.END

RETURN
!-------------------------------------
