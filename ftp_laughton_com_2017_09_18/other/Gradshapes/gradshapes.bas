REM  gradient shapes
REM -Hotwrench-
REM Draws a shape then draws a smaller/larger shape with slightly different color
REM on top of last shape drawn etc. till for-to loop ends
REM try to keep color-size with a ratio of 255 or you'll have stripes
REM such as the large red oval and circle

gr.open 255,0,0,0,0
gr.screen w, h
x1=w/800
y1=h/480 
gr.scale x1,y1 
gr.bitmap.create ball,100,100
gr.bitmap.drawinto.start ball
gr.set.stroke 3
 rad=25
for t= 1 to rad
 gr.color 255,255-t*2,255-t*2,255-t*2,0  %white
 gr.circle tbd,35,35,t
next 
gr.bitmap.drawinto.end

gr.bitmap.draw ball1,ball,380,40  %draw white ball

gr.set.stroke 4
rad=200
for t=1to rad
 gr.color 255,t*250,0,0,0  %red
 gr.circle tbd,600,240,t
next

for t=1 to rad
 gr.color 255,t*10,0,0,0  %red
 gr.oval ovl,0,0,2*t,t*2
next

for t = 1 to 50
 gr.color 255,0,0,5*t,0 %blue 
 gr.rect r1,50-t/10,420+t,150-t/10,470-t  %button
 gr.rect r2,200+t,420+t,250-t,470-t  %square
next 
gr.render

 gr.set.stroke 3
 rad=25
for t= rad to 1 step-1
 gr.color 255,0,0,250*t,0  %blue
 gr.circle tbd,405,340+t*2,t  %droplet
REM gr.render  %remove rem to animate, nice cone effect
next 

gr.render

touch:
gr.touch touched,x,y
if touched then end
goto touch
