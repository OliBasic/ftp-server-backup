REM Keypad on the fly
GR.OPEN 255,0,0,0

! **************************************************
! MAIN LOOP
! **************************************************
gosub customParams
gosub buildBmp
gosub dispKeypad
! listen loop.
do
gosub listenKeypad
until keypaddone

gosub tidyKeypad
! Text result is in keypadtxt$ ************************************
debug.print keypadtxt$


end

! **************************************************
! END OF MAIN LOOP
! **************************************************


customParams:
! **************************************************
! Customizable parameter section
! **************************************************
! keypad position
keypadx=500
keypady=100
! keypad shape
! 1 for round/oval keys, 0 for square
keypadcirc=1
! height of all text. influence size of readout area.
Fontheight=30
keypadborder=5
keywidth=50
keyheight=50
keypadroh=(3*keypadborder)+fontheight

! colour setup. bg, button, text, highlight. rgb/style
undim col$[]
ARRAY.LOAD cols$[], "ffffff02","44444400","22222202","00ffff02"
kpadalpha=255
keypadent$="√"
keypaddel$="<"
hlbrod=15 % Highlight box rate of decay
return
! **************************************************
! end of custom parameters
! **************************************************


! **************************************************
! init parameters
! **************************************************
buildBMP:
UNDIM padkeys[]
undim kpcol[4,5]
DIM padkeys[12,4],kpcol[4,5]

FOR i=1 TO 4
 m=0
 FOR o=1 TO 7 STEP 2
  m=m+1
  kpcol[i,m]=HEX(MID$(cols$[i],o,2))
 NEXT
NEXT
UNDIM cols$[]

m=0
FOR i=0 TO 3
 FOR o=0 TO 2
  m=m+1
  padkeys[m,1]=(keywidth*o)+((o+1)*keypadborder)
  padkeys[m,3]= padkeys[m,1]+keywidth
  padkeys[m,2]=(keyheight*i)+((i+1)*keypadborder)+keypadroh
  padkeys[m,4]= padkeys[m,2]+keyheight

 NEXT
NEXT

keypadx2=padkeys[m,3]+keypadborder
keypady2=padkeys[m,4]+keypadborder
kphlx=padkeys[1,1]
kphly=padkeys[1,2]

IF keypadused THEN
 GR.BITMAP.DELETE TheKeypadBMP
ELSE
 keypadused=1
ENDIF
GR.BITMAP.CREATE TheKeypadBMP, keypadx2,keypady2
GR.BITMAP.DRAWINTO.START TheKeypadBMP

! draw bg
! *********************
GR.COLOR 255, kpcol[1,1], kpcol[1,2], kpcol[1,3], kpcol[1,4]
GR.RECT box, 1,1,keypadx2,keypady2

! draw readout area
! *********************
kpadtxtx=padkeys[3,3]-keypadborder
kpadtxty= (keypadborder*2)+fontheight
GR.COLOR kpadalpha, kpcol[3,1], kpcol[3,2], kpcol[3,3], 0
GR.RECT box, keypadborder,keypadborder,padkeys[3,3],kpadtxty+keypadborder

! draw keys
! *********************
GR.COLOR 255, kpcol[2,1], kpcol[2,2], kpcol[2,3], kpcol[2,4]
FOR i=1 TO 12
 IF keypadcirc THEN
  GR.OVAL box, padkeys[i,1], padkeys[i,2], padkeys[i,3], padkeys[i,4]
 ELSE
  GR.RECT box, padkeys[i,1], padkeys[i,2], padkeys[i,3], padkeys[i,4]
 ENDIF
NEXT

! draw key digits
! *********************
GR.COLOR 255, kpcol[3,1], kpcol[3,2], kpcol[3,3], kpcol[3,4]
GR.TEXT.ALIGN 2
GR.TEXT.SIZE fontheight
GR.TEXT.BOLD 1
FOR i=1 TO 12
 x=padkeys[i,1]+(keywidth/2)
 y=padkeys[i,2]+(keyheight/2) +(fontheight/3)
 SW.BEGIN i
  SW.CASE 10
   keypadlabel$=keypaddel$
   SW.BREAK
  SW.CASE 11
   keypadlabel$="0"
   SW.BREAK
  SW.CASE 12
   keypadlabel$=keypadent$
   SW.BREAK
  SW.DEFAULT
   keypadlabel$=LEFT$(STR$(i),1)
 SW.END
 GR.TEXT.DRAW box,x,y,keypadlabel$
NEXT

GR.BITMAP.DRAWINTO.END
return
! *********************
! Keypad constructed
! *********************


! **************************************************
! Draw keypad on screen
! **************************************************
dispKeypad:
GR.COLOR kpadalpha,0,0,0,0
GR.BITMAP.DRAW Keypado, TheKeypadBMP, keypadx, keypady

! prepare highlight box, snd hide it
GR.COLOR 255,kpcol[4,1], kpcol[4,2], kpcol[4,3], kpcol[4,4]
hlba=0
IF keypadcirc THEN
 GR.OVAL hlbox, padkeys[1,1], padkeys[1,2], padkeys[1,3], padkeys[1,4]
ELSE
 GR.RECT hlbox, padkeys[1,1], padkeys[1,2], padkeys[1,3], padkeys[1,4]
ENDIF
GR.HIDE hlbox
! prepare readout
GR.COLOR 255, kpcol[3,1], kpcol[3,2], kpcol[3,3], kpcol[3,4]
GR.TEXT.ALIGN 3
GR.TEXT.SIZE fontheight
GR.TEXT.BOLD 0
keypadtxt$=""
GR.TEXT.DRAW keypadro, kpadtxtx+keypadx, kpadtxty+keypady, keypadtxt$

GR.RENDER
return
! **************************************************
! end drawing to screen
! **************************************************



! **************************************************
! listen for input
! **************************************************
listenKeypad:
 GR.TOUCH tch, x,y
 keypadin=-1
 keypaddone=0
 IF tch THEN
  DO
   GR.TOUCH tch,m,m
  UNTIL !tch

  FOR i=1 TO 12
   IF (x>=padkeys[i,1]+keypadx) & (x<=padkeys[i,3]+keypadx) & (y>=padkeys[i,2]+keypady) & (y<=padkeys[i,4]+keypady) THEN
   
    keypadin=i

    ! show and move highlight box ********************
    hlba=255
    GR.SHOW hlbox
    GR.MODIFY hlbox, "left", padkeys[keypadin,1]+keypadx
    GR.MODIFY hlbox, "top", padkeys[keypadin,2]+keypady
    GR.MODIFY hlbox, "right", padkeys[keypadin,3]+keypadx
    GR.MODIFY hlbox, "bottom", padkeys[keypadin,4]+keypady
       IF i=11 THEN keypadin=0
   ENDIF
  NEXT
 ENDIF

 IF keypadin>=0 THEN
  IF ((keypadin<10) & (keypadin>=0)) THEN keypadtxt$=keypadtxt$+LEFT$(STR$(keypadin),1)
  IF ((LEN(keypadtxt$)>0) & (keypadin=10)) THEN
   IF (LEN(keypadtxt$)=1) THEN
    keypadtxt$=""
   ELSE
    keypadtxt$=LEFT$(keypadtxt$, LEN(keypadtxt$)-1)
   ENDIF
  ENDIF
  IF (keypadin=12) THEN keypaddone=1
  gr.text.width m, keypadtxt$
  if m>(3*keywidth) then keypadtxt$=LEFT$(keypadtxt$, LEN(keypadtxt$)-1)
  GR.MODIFY keypadro, "text", keypadtxt$
 ENDIF

 IF (hlba>=0) THEN
  GR.COLOR hlba,0,255,255,2
  GR.PAINT.GET pa
  GR.MODIFY hlbox,"paint",pa
  hlba=hlba-hlbrod
  IF hlba<0 THEN GR.HIDE hlbox
 ENDIF

 GR.RENDER
return
! **************************************************
! end of one listen cycle
! **************************************************


! **************************************************
! tidy up
! **************************************************
tidyKeypad:
gr.hide hlbox
gr.hide keypado
return

! **************************************************
! Thats all, folks.

! **************************************************

