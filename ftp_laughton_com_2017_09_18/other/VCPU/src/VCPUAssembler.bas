fn.def tbytes(binary)
byte.write.byte binary, 0
byte.write.byte binary, 0
byte.write.byte binary, 0 
fn.end

fn.def ltbytes(binary)
byte.write.byte binary, 0
byte.write.byte binary, 0
fn.end 

FN.DEF wreg(p$, p2)
 IF p$="ax" THEN byte.write.byte p2, 1
 IF p$="bx" THEN byte.write.byte p2, 2
 IF p$="cx" THEN byte.write.byte p2, 3
 IF p$="dx" THEN byte.write.byte p2, 4
FN.END

FN.DEF eVAL(p$)
 IF p$="ax" THEN fn.rtn 1
 IF p$="bx" THEN fn.rtn 2
 IF p$="cx" THEN fn.rtn 3
 IF p$="dx" THEN fn.rtn 4
FN.END

FN.DEF jVAL(p$, b)
 IF STARTS_WITH("!", p$) THEN
  BUNDLE.CONTAIN b, p$, a else end
  IF a THEN BUNDLE.GET b, p$, nu
  FN.RTN nu
 ELSE
  FN.RTN VAL(p$)
 FN.END

 FN.DEF mVAL(p1$, p2$)
  FN.RTN SHIFT(eVAL(p1$), 4)+eVAL(p2$)
 FN.END

 REM Start of BASIC! Program
 PRINT "VCPU Assembler V3 on BASIC! "; VERSION$()
 BUNDLE.CREATE q
 ip=64
 setsrc:
 INPUT "Source file", src$
 PRINT "Source file set: "; src$
 INPUT "Binary file", bin$
 PRINT "Binary file set: "; bin$
 TEXT.OPEN r, source, src$
 BYTE.OPEN w, binary, bin$
 PRINT "GEN Symtbl"
 b:
 TEXT.READLN source, srln$
 IF srln$="EOF" THEN goto c
 ip=ip+2
 ARRAY.DELETE l$[]
 SPLIT l$[], srln$, " "
 IF STARTS_WITH("!", l$[1]) THEN BUNDLE.PUT q, l$[1], ip
 GOTO b
 c:
 TEXT.CLOSE source
 TEXT.OPEN r, source, src$
 PRINT "CMPL "; src$
 a:
 TEXT.READLN source, srln$
 PRINT "Line: "; srln$
 IF srln$="EOF" THEN end
 IF STARTS_WITH("#", srln$) THEN goto a
 ip=ip+2
 ARRAY.DELETE l$[]
 SPLIT l$[], srln$, " "
 CALL tbytes(binary)
 SW.BEGIN l$[1]
  SW.CASE "movax"
   BYTE.WRITE.BYTE binary, 1
call tbytes(binary)
   BYTE.WRITE.BYTE binary, eVAL(l$[2])
   SW.BREAK
  SW.CASE "movbx"
   BYTE.WRITE.BYTE binary, 2
call tbytes(binary)
   BYTE.WRITE.BYTE binary, eVAL(l$[2])
   SW.BREAK
  SW.CASE "movcx"
   BYTE.WRITE.BYTE binary, 3
call tbytes(binary)
   BYTE.WRITE.BYTE binary, eVAL(l$[2])
   SW.BREAK
  SW.CASE "movdx"
   BYTE.WRITE.BYTE binary, 4
call tbytes(binary)
   BYTE.WRITE.BYTE binary, eVAL(l$[2])
   SW.BREAK
  SW.CASE "movmax"
   BYTE.WRITE.BYTE binary, 5
call tbytes(binary)
   BYTE.WRITE.BYTE binary, eVAL(l$[2])
   SW.BREAK
  SW.CASE "movmbx"
   BYTE.WRITE.BYTE binary, 6
call tbytes(binary)
   BYTE.WRITE.BYTE binary, eVAL(l$[2])
   SW.BREAK
  SW.CASE "movmcx"
   BYTE.WRITE.BYTE binary, 7
call tbytes(binary)
   BYTE.WRITE.BYTE binary, eVAL(l$[2])
   SW.BREAK
  SW.CASE "movmdx"
   BYTE.WRITE.BYTE binary, 8
call tbytes(binary)
   BYTE.WRITE.BYTE binary, eVAL(l$[2])
   SW.BREAK
  SW.CASE "movrax"
   BYTE.WRITE.BYTE binary, 9
call tbytes(binary)
   BYTE.WRITE.BYTE binary, eVAL(l$[2])
   SW.BREAK
  SW.CASE "movrbx"
   BYTE.WRITE.BYTE binary, 10
call tbytes(binary)
   BYTE.WRITE.BYTE binary, eVAL(l$[2])
   SW.BREAK
  SW.CASE "movrcx"
   BYTE.WRITE.BYTE binary, 11
call tbytes(binary)
   BYTE.WRITE.BYTE binary, eVAL(l$[2])
   SW.BREAK
  SW.CASE "movrdx"
   BYTE.WRITE.BYTE binary, 12
call tbytes(binary)
   BYTE.WRITE.BYTE binary, eVAL(l$[2])
   SW.BREAK
  SW.CASE "add"
   BYTE.WRITE.BYTE binary, 13
call ltbytes()
   BYTE.WRITE.BYTE binary, mVAL(l$[2], l$[3])
   SW.BREAK
  SW.CASE "sub"
   BYTE.WRITE.BYTE binary, 14
call ltbytes(binary)
   BYTE.WRITE.BYTE binary, mVAL(l$[2], l$[3])
   SW.BREAK
  SW.CASE "mul"
   BYTE.WRITE.BYTE binary, 15
call ltbytes(binary)
   BYTE.WRITE.BYTE binary, mVAL(l$[2], l$[3])
   SW.BREAK
  SW.CASE "div"
   BYTE.WRITE.BYTE binary, 16
call ltbytes(binary)
   BYTE.WRITE.BYTE binary, mVAL(l$[2], l$[3])
   SW.BREAK
  SW.CASE "and"
   BYTE.WRITE.BYTE binary, 17
call ltbytes(binary)
   BYTE.WRITE.BYTE binary, mVAL(l$[2], l$[3])
   SW.BREAK
  SW.CASE "or"
   BYTE.WRITE.BYTE binary, 18
call ltbytes(binary)
   BYTE.WRITE.BYTE binary, mVAL(l$[2], l$[3])
   SW.BREAK
  SW.CASE "not"
   BYTE.WRITE.BYTE binary, 19
call tbytes(binary)
   BYTE.WRITE.BYTE binary, eVAL(l$[2])
   SW.BREAK
  SW.CASE "cmp"
   BYTE.WRITE.BYTE binary, 20
call ltbytes(binary)
   BYTE.WRITE.BYTE binary, mVAL(l$[2], l$[3])
   SW.BREAK
  SW.CASE "jmp"
   BYTE.WRITE.BYTE binary, 21
call tbytes(binary)
   BYTE.WRITE.BYTE binary, jVAL(l$[2], q)
   SW.BREAK
  SW.CASE "jz"
   BYTE.WRITE.BYTE binary, 22
call tbytes(binary)
   BYTE.WRITE.BYTE binary, jVAL(l$[2], q)
   SW.BREAK
  SW.CASE "jnz"
   BYTE.WRITE.BYTE binary, 23
call tbytes(binary)
   BYTE.WRITE.BYTE binary, jVAL(l$[2], q)
   SW.BREAK
  SW.CASE "je"
   BYTE.WRITE.BYTE binary, 24
call tbytes(binary)
   BYTE.WRITE.BYTE binary, jVAL(l$[2], q)
   SW.BREAK
  SW.CASE "jne"
   BYTE.WRITE.BYTE binary, 25
call tbytes(binary)
   BYTE.WRITE.BYTE binary, jVAL(l$[2], q)
   SW.BREAK
  SW.CASE "jl"
   BYTE.WRITE.BYTE binary, 26
call tbytes(binary)
   BYTE.WRITE.BYTE binary, jVAL(l$[2], q)
   SW.BREAK
  SW.CASE "jg"
   BYTE.WRITE.BYTE binary, 27
call tbytes(binary)
   BYTE.WRITE.BYTE binary, jVAL(l$[2], q)
   SW.BREAK
  SW.CASE "int"
   BYTE.WRITE.BYTE binary, 28
call tbytes(binary)
   BYTE.WRITE.BYTE binary, VAL(l$[2])
   SW.BREAK
  SW.CASE "push"
   BYTE.WRITE.BYTE binary, 29
call tbytes(binary)
   BYTE.WRITE.BYTE binary, eVAL(l$[2])
   SW.BREAK
  SW.CASE "pushm"
   BYTE.WRITE.BYTE binary, 30
call tbytes(binary)
   BYTE.WRITE.BYTE binary, VAL(l$[2])
   SW.BREAK
  SW.CASE "pop"
   BYTE.WRITE.BYTE binary, 31
call tbytes(binary)
   BYTE.WRITE.BYTE binary, eVAL(l$[2])
   SW.BREAK
  SW.DEFAULT
   IF STARTS_WITH("!", l$[1]) THEN sw.break
   PRINT "Invalid command: "; l$[1]
   END
   SW.BREAK
 SW.END
 GOTO a
