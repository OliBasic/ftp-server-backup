REM Start of BASIC! Program
rem demos for virtual reality VR goggles
rem by cterp 


starcount=80
DIM speed[starcount]
DIM birect[starcount]
DIM radius[starcount]
DIM o[starcount]
DIM o2[starcount]
DIM newx[starcount]
DIM newx2[starcount]
DIM newy[starcount]
DIM oldx[starcount]
DIM oldx2[starcount]
DIM oldy[starcount]


fn.def r()
fn.rtn floor(rnd()*100)+80
fn.end

pi=ATAN(1)*4
GR.OPEN 255, 0, 0, 0, 0
GR.ORIENTATION 0
GR.SCREEN mx, my


do

LIST.CREATE S,CLIST

list.clear clist
LIST.ADD CLIST,"squares","stars","letters","mesh","objects:"+int$(starcount),"quit"
dialog.select c,clist,"choose VR demo"
bkhit=0
gr.cls
IF C=1
gosub squares
ELSE IF C=2
gosub starfield
ELSE IF C=3
gosub alpha
else if c=4
gosub mesh
else if c=5
do
input "enter # of objects (3-200):",n,starcount,c
until n>=3 & n<=200
starcount=n
undim speed[], birect[],radius[],o[],o2[],newx[],newx2[], newy[],oldx[],oldx2[],oldy[]
DIM speed[starcount]
DIM birect[starcount]
DIM radius[starcount]
DIM o[starcount]
DIM o2[starcount]
DIM newx[starcount]
DIM newx2[starcount]
DIM newy[starcount]
DIM oldx[starcount]
DIM oldx2[starcount]
DIM oldy[starcount]


ENDIF

until c=6

exit


alpha:

GR.COLOR 255, 255, 255, 255 ,1
GR.SET.STROKE 3 

LET midx=mx/4 
LET midx2=3*mx/4
LET midy=my/2
LET maxrad=midx 
LET dd=40

WAKELOCK 3


FOR I=1 TO starcount 
 LET speed[I]=(RND() + 0.1)/100
 LET birect[I]=(RND()*2*pi)
 LET radius[I]=RND()
 let newx[i]=COS(birect[I])*newrad +midx
 let newx2[i]=COS((1+radius[i]/dd)*birect[I])*newrad +midx2
let newy[i]=SIN(birect[I])*newrad+midy
GR.COLOR 255,r(),r(),r(),255

GR.SET.STROKE 2*rnd()+1

!GR.POINT o[i],newx[i],newy[i]
c$=chr$(ascii("a")+rnd()*26)
gr.text.size rnd()*30+5
gr.text.draw o[i],newx[i],newy[i],c$
gr.text.draw o2[i],newx2[i],newy[i],c$
!GR.POINT o2[i],newx2[i],newy[i]

 let oldx[i]=newx[i]:let oldx2[i]=newx2[i]:let oldy[i]=newy[i]

NEXT
GR.RENDER




DO


 FOR I=1 TO starcount 

  IF radius[I]>=1
   LET speed[I]=(RND() + 0.1)/100
   LET birect[I]=RND()*(2*pi)
   LET radius[I]=RND()/300
  ELSE
   LET radius[I]+=speed[I]:LET newrad=radius[I]^2*maxrad
  ENDIF

 let newx[i]=COS(birect[I])*newrad +midx
 let newx2[i]=COS((1+radius[i]/dd)*birect[I])*newrad +midx2
 let newy[i]=SIN(birect[I])*newrad+midy

  GR.MOVE o[i],newx[i]-oldx[i],newy[i]-oldy[i]
  GR.MOVE o2[i],newx2[i]-oldx2[i],newy[i]-oldy[i]
 let oldx[i]=newx[i]
 let oldx2[i]=newx2[i]
 let oldy[i]=newy[i]
  GR.MODIFY o[i],"alpha",radius[I]*255
  GR.MODIFY o2[i],"alpha",radius[I]*255
 NEXT

 GR.RENDER 


UNTIL bkhit

return






starfield:
GR.COLOR 255, 255, 255, 255 ,1
GR.SET.STROKE 3 

LET midx=mx/4 
LET midx2=3*mx/4
LET midy=my/2
LET maxrad=midx 
LET dd=40

WAKELOCK 3


FOR I=1 TO starcount 
 LET speed[I]=(RND() + 0.3)/100
 LET birect[I]=(RND()*2*pi)
 LET radius[I]=RND()
 let newx[i]=COS(birect[I])*newrad +midx
 let newx2[i]=COS((1+radius[i]/dd)*birect[I])*newrad +midx2
let newy[i]=SIN(birect[I])*newrad+midy
GR.COLOR 255,r(),r(),r(),255
GR.SET.STROKE 1*rnd()+3
GR.POINT o[i],newx[i],newy[i]
GR.POINT o2[i],newx2[i],newy[i]
 let oldx[i]=newx[i]:let oldx2[i]=newx2[i]:let oldy[i]=newy[i]
NEXT
GR.RENDER


DO

 FOR I=1 TO starcount 

  IF radius[I]>=1
   LET speed[I]=(RND() + 0.3)/100
   LET birect[I]=RND()*(2*pi)
   LET radius[I]=RND()/300
  ELSE
   LET radius[I]+=speed[I]:LET newrad=radius[I]^2*maxrad 
  ENDIF

 let newx[i]=COS(birect[I])*newrad +midx
 let newx2[i]=COS((1+radius[i]/dd)*birect[I])*newrad +midx2

 let newy[i]=SIN(birect[I])*newrad+midy

  GR.MOVE o[i],newx[i]-oldx[i],newy[i]-oldy[i]
  GR.MOVE o2[i],newx2[i]-oldx2[i],newy[i]-oldy[i]
 let oldx[i]=newx[i]
 let oldx2[i]=newx2[i]
 let oldy[i]=newy[i]
  GR.MODIFY o[i],"alpha",radius[I]*255
  GR.MODIFY o2[i],"alpha",radius[I]*255
 NEXT
 GR.RENDER 
UNTIL bkhit

return



mesh:
GR.COLOR 255, 255, 255, 255 ,1
GR.SET.STROKE 3 

LET midx=mx/4 
LET midx2=3*mx/4
LET midy=my/2
LET maxrad=midx 
LET dd=40

WAKELOCK 3


FOR I=2 TO starcount 
 LET speed[I]=(RND() + 0.3)/100
 LET birect[I]=(RND()*2*pi)
 LET radius[I]=RND()
 let newx[i]=COS(birect[I])*newrad +midx
 let newx2[i]=COS((1+radius[i]/dd)*birect[I])*newrad +midx2
let newy[i]=SIN(birect[I])*newrad+midy
GR.COLOR 255,r(),r(),r(),255
GR.SET.STROKE 1*rnd()+3
GR.line o[i],newx[i],newy[i],newx[i-1],newy[i-1]
GR.line o2[i],newx2[i],newy[i],newx2[i-1],newy[i-1]

 let oldx[i]=newx[i]:let oldx2[i]=newx2[i]:let oldy[i]=newy[i]
NEXT
GR.RENDER


DO
gr.cls
 FOR I=1 TO starcount 

  IF radius[I]>=1
   LET speed[I]=(RND() + 0.3)/100
   LET birect[I]=RND()*(2*pi)
   LET radius[I]=RND()/300
  ELSE
   LET radius[I]+=speed[I]:LET newrad=radius[I]^2*maxrad 
  ENDIF

 let newx[i]=COS(birect[I])*newrad +midx
 let newx2[i]=COS((1+radius[i]/dd)*birect[I])*newrad +midx2

 let newy[i]=SIN(birect[I])*newrad+midy

if i>1
GR.line o[i],newx[i],newy[i],newx[i-1],newy[i-1]
GR.line o2[i],newx2[i],newy[i],newx2[i-1],newy[i-1]
endif
  
 let oldx[i]=newx[i]
 let oldx2[i]=newx2[i]
 let oldy[i]=newy[i]
if i>1
  GR.MODIFY o[i],"alpha",radius[I]*255
  GR.MODIFY o2[i],"alpha",radius[I]*255
endif
 NEXT
 GR.RENDER 
UNTIL bkhit

return





squares:
gr.screen w,h

GR.COLOR 255, 255, 255, 255 ,1
GR.SET.STROKE 3 
LET midx=mx/4 
LET midx2=3*mx/4
LET midy=my/2
LET maxrad=midx 
LET dd=40

WAKELOCK 3


FOR I=1 TO starcount 
 LET speed[I]=(RND() + 0.1)/100
 LET birect[I]=(RND()*2*pi)
 LET radius[I]=RND()
 let newx[i]=COS(birect[I])*newrad +midx
 let newx2[i]=COS((1+radius[i]/dd)*birect[I])*newrad +midx2
let newy[i]=SIN(birect[I])*newrad+midy
GR.COLOR 255,r(),r(),r(),255
GR.SET.STROKE 40*rnd()+5
GR.POINT o[i],newx[i],newy[i]
GR.POINT o2[i],newx2[i],newy[i]
let oldx[i]=newx[i]:let oldx2[i]=newx2[i]:let oldy[i]=newy[i]
NEXT
GR.RENDER


SENSORS.OPEN 1
cx=w/2
cy=h/2

DO


 FOR I=1 TO starcount 



! The main program loop

 !Read the acclerometer
 SENSORS.READ 1,y,x,z

 !Amplify the magnitudes
 x*=cy/12
 y*=cy/12

 
 ! Calculate the gravity vector
 m=SQR(x*x+y*y)
 IF m=0 THEN m=0.0001
 angle=-(ACOS(x/m)-pi/2)
 IF y<0 THEN angle=-angle



  IF radius[I]>=1
   LET speed[I]=(RND()+0.1)/100
   LET birect[I]=RND()*(2*pi)
   LET radius[I]=RND()/300
  ELSE
   LET radius[I]+=speed[I]:LET newrad=radius[I]^2*maxrad
 let birect[i]+=angle/20
  ENDIF

 let newx[i]=COS(birect[I])*newrad +midx
 let newx2[i]=COS((1+radius[i]/dd)*birect[I])*newrad +midx2
 let newy[i]=SIN(birect[I])*newrad+midy

  GR.MOVE o[i],newx[i]-oldx[i],newy[i]-oldy[i]
  GR.MOVE o2[i],newx2[i]-oldx2[i],newy[i]-oldy[i]
 let oldx[i]=newx[i]
 let oldx2[i]=newx2[i]
 let oldy[i]=newy[i]
  GR.MODIFY o[i],"alpha",radius[I]*255
  GR.MODIFY o2[i],"alpha",radius[I]*255

 NEXT

 GR.RENDER 


UNTIL bkhit
return

ongrtouch:
bkhit=1
gr.ongrtouch.resume



!OnError:
GR.CLOSE
END
