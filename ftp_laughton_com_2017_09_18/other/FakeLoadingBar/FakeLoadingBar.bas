% FAKE LOADING BAR v2.0

% --- METHODS/FUNCTIONS DECLARATIONS ---

% THIS FUNCTION CREATES THE LOADING BAR
Fn.Def CreateLoadbarContents(loadTexturePointer1, loadTexturePointer2,~
	barWidth, barHeight)
	
	% CREATE BOTH BITMAPS
	Gr.Bitmap.Create loadTexturePointer1, barWidth, barHeight
	Gr.Bitmap.Create loadTexturePointer2, barWidth, barHeight
	
	% DRAW THE LOADING BAR TEXTURE
	Gr.Bitmap.Drawinto.Start loadTexturePointer1
		% SET THE COLOR OF THE LOADING BAR
		Gr.Color 255, 0, 255, 155, 1
		
		% DRAW A RECTANGLE INSIDE THIS BITMAP
		Gr.Rect temporaryPointer, 0, 0, barWidth, barHeight
	Gr.Bitmap.Drawinto.End
	
	% DRAW THE HIGHLIGHTED LOADING BAR TEXTURE
	Gr.Bitmap.Drawinto.Start loadTexturePointer2
		% SET THE COLOR TO BRIGHT WHITE
		Gr.Color 255, 255, 255, 255, 1
		
		% DRAW A RECTANGLE INSIDE THIS BITMAP
		Gr.Rect temporaryPointer, 0, 0, barWidth, barHeight
	Gr.Bitmap.Drawinto.End
Fn.End

% THIS FUNCTION CREATES ALL THE COLUMNS OF CHARACTERS
Fn.Def CreateColumnContents(columnQuantity, columnWidth, maxCharsPerColumn,~
	columnSpeedList, randomCharList, columnTextureList, columnHeightList,~
	minColumnSpeed, maxColumnSpeed)
	% SET BASIC CONSTANTS
	TRUE = 1
	FALSE = 0
	LEFT = 1
	CENTER = 2
	RIGHT = 3
		
	% SET TEXT APPEARANCE AND POSITION
	Call SetTextProperties(center, columnWidth)
	CHAR_X = columnWidth / 2
	CHAR_Y = columnWidth

	% GET THE SIZE OF THE RANDOM CHARACTERS LIST
	List.Size randomCharList, randomCharListSize
	
	for i = 1 to columnQuantity
		% SET THE DRAWING COLOR
		Call SetRandomGreen(true, false)
	
		% DEFINES A RANDOM SIZE IN CHARACTERS TO HOLD
		charQuantity = Ceil(Rnd() * maxCharsPerColumn) + 3
		columnHeight = columnWidth * charQuantity
		List.Add columnHeightList, columnHeight
		
		% SET THE COLUMN SPEED
		currentColumnSpeed = Rnd() * (maxColumnSpeed - minColumnSpeed) + minColumnSpeed
		List.Add columnSpeedList, currentColumnSpeed
	
		% CREATE A NEW BITMAP RECTANGLE TO HOLD THE CHARACTERS
		Gr.Bitmap.Create temporaryPointer, columnWidth,~
			columnHeight
		
		% ADD THE NEW COLUMN TO THE LIST
		List.Add columnTextureList, temporaryPointer
		
		% DRAW THE CHARACTERS INSIDE THE RECTANGLE
		Gr.Bitmap.Drawinto.Start temporaryPointer
			for j = 1 to charQuantity
				% PICK A RANDOM INDEX
				temporaryIndex = Ceil(Rnd() * randomCharListSize)
				
				% GET THE CHAR AT THE RANDOM INDEX 
				List.Get randomCharList, temporaryIndex, temporaryChar$
				
				% DRAW THE CHAR
				Gr.Text.Draw temporaryPointer, CHAR_X, CHAR_Y * j, temporaryChar$
			next
		Gr.Bitmap.Drawinto.End	
	next
Fn.End

% SET THE TEXT PROPERTIES: ALIGNMENT AND SIZE
Fn.Def SetTextProperties(textAlign, textSize)
		Gr.Text.Align textAlign
		Gr.Text.Size textSize
Fn.End

% SET A RANDOM RED TO THE DRAWING COLOR
Fn.Def SetRandomRed(isFilled, haveBorder)
	% PROVIDES A RANDOM NUMBER IN THE RANGE OF 0 TO 100
	randomNumber = Ceil(Rnd() * 100)
	
	% SET THE COLOR BASED ON THE NUMBER AND PROPERTIES RECEIVED
	Gr.Color 255, 55 + randomNumber, 0, 0, isFilled + haveBorder
Fn.End

% SET A RANDOM BLUE TO THE DRAWING COLOR
Fn.Def SetRandomBlue(isFilled, haveBorder)
	% PROVIDES A RANDOM NUMBER IN THE RANGE OF 0 TO 100
	randomNumber = Ceil(Rnd() * 100)
	
	% SET THE COLOR BASED ON THE NUMBER AND PROPERTIES RECEIVED
	Gr.Color 255, 55 + randomNumber, 0, 0, isFilled + haveBorder
Fn.End

% SET A RANDOM GREEN TO THE DRAWING COLOR
Fn.Def SetRandomGreen(isFilled, haveBorder)
	% PROVIDES A RANDOM NUMBER IN THE RANGE OF 0 TO 100
	randomNumber = Ceil(Rnd() * 100)
	
	% SET THE COLOR BASED ON THE NUMBER AND PROPERTIES RECEIVED
	Gr.Color 255, 0, 55 + randomNumber, 0, isFilled + haveBorder
Fn.End

% DRAW THE TITLE TEXT IN THE PROVIDED POSITION
Fn.Def InitiateTitleText(windowWidth, tittleStringList, titleTextPointer,~
	textPositionX, textPositionY)
	% SET BASIC CONSTANTS
	TRUE = 1
	FALSE = 0
	LEFT = 1
	CENTER = 2
	RIGHT = 3
	
	% SET TEXT COLOR AND PROPERTIES
	Gr.Color 255, 255, 255, 255, 1
	Call SetTextProperties(center, windowWidth / 22)
	
	% DRAW THE TEXT IN THE PROVIDED POSITION
	Gr.Text.Draw titleTextPointer, textPositionX, textPositionY, ""
Fn.End

% DRAW THE COLUMNS AT RANDOM GENERATED X POSITIONS
Fn.Def InitiateColumns(columnTextureList, columnPointerList, ~
	columnHeightList, maxDrawPositionX, columnPositionListY)
	% GET THE SIZE OF THE LIST OF TEXTURES
	List.Size columnTextureList, columnListSize
	
	for i = 1 to columnListSize
		% GENERATE RANDOM X POSITION
		textPositionX = Floor(Rnd() * maxDrawPositionX)
		
		% SET THE INITIAL Y POSITION
		List.Get columnHeightList, i, columnHeight
		textPositionY = -columnHeight
		List.Add columnPositionListY, -columnHeight
	
		% GET THE POINTER VALUE OF THIS TEXTURE TO BE DRAW
		List.Get columnTextureList, i, currentColumn
		
		% DRAW THE CURRENT COLUMN AT THE RANDOM X
		Gr.Bitmap.Draw temporaryPointer, currentColumn, textPositionX, textPositionY
		
		% ADD THE COLUMN TO THE COLUMNS LIST
		List.Add columnPointerList, temporaryPointer
	next
Fn.End

% DRAW THE LOADING BAR IN ITS INITIAL STATE
Fn.Def InitiateLoadingBar(loadingBarPointer, loadTexturePointer1, windowWidth,~
	windowHeight, barWidth, barHeight)
	% CALCULATE THE INITIAL POSITION OF THE BAR
	barPositionX = (windowWidth - barWidth) / 2
	barPositionY = (windowHeight / 2) - (barHeight / 2)
	
	% DRAW THE BAR ON THE PROVIDED POSITION
	Gr.Bitmap.Draw loadingBarPointer, loadTexturePointer1, barPositionX, barPositionY
Fn.End

% UPDATE TITLE TEXT CONTENTS
Fn.Def UpdateTitleText(titleStringList, titleTextPointer, elapsedTime,~
	titleFrameTime, maxFrameTime, isLoadingComplete)
		% UPDATE THE TITLE FRAME TIME
		titleFrameTime += elapsedTime
		
		% CHECK IF THE LOADING ISN'T COMPLETE
		if !isLoadingComplete
		% CHECK IF ITS ALREADY TIME TO CHANGE THE TEXT
			if titleFrameTime >= maxFrameTime
				titleFrameTime = 0
			
				% GET THE NUMBER OF CONTENTS IN THE LIST
				List.Size titleStringList, titleListSize
			
				% GET A RANDOM TEXT IN THE LIST TO REPLACE THE TITLE
				randomIndex = Ceil(rnd() * titleListSize)
				List.Get titleStringList, randomIndex, currentTitleString$
			
				% CHANGE THE TITLE TEXT TO THE NEW RANDOM ONE
				Gr.Modify titleTextPointer, "text", currentTitleString$
			endif
		else
			% CHANGE THE TEXT TO MATCH THE COMPLETE LOADING BAR
			Gr.Modify titleTextPointer, "text", "Loading complete!"
		endif
Fn.End

% UPDATE THE COLUMNS POSITION BASED ON IT'S SPEED
Fn.Def UpdateColumns(columnPointerList, columnSpeedList, columnHeightList,~
	columnPositionListY, elapsedTime, windowHeight)
	% GET THE NUMBER OF COLUMNS TO BE UPDATED
	List.Size columnPointerList, columnListSize
	
	for i = 1 to columnListSize
		% GET CURRENT COLUMN AND ITS SPEED
		List.Get columnPointerList, i, currentColumn
		List.Get columnSpeedList, i, currentColumnSpeed
		
		% GET THE OLD POSITION Y AND THE HEIGHT OF THE COLUMN
		List.Get columnPositionListY, i, oldColumnPositionY
		List.Get columnHeightList, i, columnHeight
		
		% CHECK IF THE POSITION WASN'T OFF THE SCREEN AND DEAL APPROPRIATELY
		if oldColumnPositionY < windowHeight
			List.Replace columnPositionListY, i,~
				oldColumnPositionY + Ceil(elapsedTime * currentColumnSpeed)
		else
			List.Replace columnPositionListY, i, -columnHeight
		endif
		
		% MODIFY THE REAL Y PROPERTY OF THE COLUMN
		List.Get columnPositionListY, i, currentColumnPositionY
		Gr.Modify currentColumn, "y", currentColumnPositionY
	next
Fn.End

% UPDATE THE LOADING BAR SIZE BASED ON THE TIME PASSED AND PERCENT
Fn.Def UpdateLoadingBar(loadingBarPointer, loadTexturePointer1, loadTexturePointer2,~
	barWidth, barHeight, elapsedTime, barFrameTime, barMaxFrameTime,~
	currentLoadedPercent, loadingTime, loadingMaxTime, currentLoadingTexture,~
	isLoadingComplete)
	% BASIC CONSTANTS
	TRUE = 1
	FALSE = 0
	
	% CHECK IF THE LOAD IS COMPLETE AND DEAL WITH IT
	if currentLoadedPercent < 1
		% UPDATE THE CURRENT LOADED PERCENT
		loadingTime += elapsedTime
		currentLoadedPercent = (1 / loadingMaxTime) * loadingTime

		% CREATE TEMPORARY TEXTURE WITH THE SIZE BASED ON CURRENT PERCENT LOADED
		Gr.Bitmap.Scale temporaryTexture, loadTexturePointer1,~
		Ceil(barWidth * currentLoadedPercent), barHeight
		
		% CHANGE THE LOADING BAR TEXTURE TO THE TEMPORARY ONE
		Gr.Modify loadingBarPointer, "bitmap", temporaryTexture
		
	else
		% UPDATE THE FRAME TIME AND SET THE LOADING COMPLETE FLAG
		barFrameTime += elapsedTime
		isLoadingComplete = true
		
		% CHECK IF ITS TIME TO HIGHLIGHT THE LOADING BAR
		if barFrameTime >= barMaxFrameTime
			barFrameTime = 0			

			if currentLoadingTexture = loadTexturePointer1
				currentLoadingTexture = loadTexturePointer2
				Gr.Modify loadingBarPointer, "bitmap", currentLoadingTexture
			else
				currentLoadingTexture = loadTexturePointer1
				Gr.Modify loadingBarPointer, "bitmap", currentLoadingTexture
			endif
		endif
	endif
Fn.End

% --- FIELD DECLARATIONS ---

% OPEN GRAPHIC WINDOW AND GET WINDOW SIZE
Gr.Open 255, 0, 0, 0
Gr.Screen WINDOW_WIDTH, WINDOW_HEIGHT

% BASIC CONSTANTS
TRUE = 1
FALSE = 0
LEFT = 1
CENTER = 2
RIGHT = 3

% PROGRAM FIELDS
elapsedTime = 1
currentLoadedPercent = 0
TOTAL_LOADING_TIME = 15000

% TEXT CHANGE SUPPORT
titleTextPointer = 0
titleFrameTime = 0
MAX_TITLE_FRAME_TIME = 500
List.Create S, titleStringList
List.Add titleStringList,~
	"Loading graphics...",~
	"Loading animations...",~
	"Loading sounds...",~
	"Loading solid oxigen...",~
	"Loading monkeys...",~
	"Look, a flying bear!"
	
% COLUMNS OF CHAR GOING DOWN SUPPORT
List.Create S, randomCharList
List.Add randomCharList,~
	"0", "1", "2", "3", "4", "5", "6",~
	"7", "8", "9", "#", "%", "@", "X"
	
% COLUMNS CREATION SUPPORT
TOTAL_NUM_COLUMNS = 30
COLUMN_WIDTH = WINDOW_WIDTH / TOTAL_NUM_COLUMNS
MAX_CHAR_PER_COLUMN = Floor(WINDOW_HEIGHT / COLUMN_WIDTH)
MIN_COLUMN_SPEED = WINDOW_HEIGHT / 3000 % PER MILLISECOND
MAX_COLUMN_SPEED = WINDOW_HEIGHT / 1000 % PER MILLISECOND
List.Create N, columnTextureList
List.Create N, columnPointerList
List.Create N, columnPositionListY
List.Create N, columnHeightList
List.Create N, columnSpeedList

% LOADING BAR SUPPORT
loadingBarPointer = 0
currentLoadingTexture = loadTexturePointer1
loadTexturePointer1 = 0
loadTexturePointer2 = 0
LOAD_BAR_WIDTH = WINDOW_WIDTH * 10 / 11
LOAD_BAR_HEIGHT = WINDOW_HEIGHT * 2 / 100
loadingTime = 0
isLoadingComplete = false

% LOADING BAR HIGHLIGHTING SUPPORT
barFrameTime = 0
barMaxFrameTime = 50

% --- LOAD/CREATE CONTENT ---

Call CreateColumnContents(TOTAL_NUM_COLUMNS, COLUMN_WIDTH, MAX_CHAR_PER_COLUMN,~
	columnSpeedList, randomCharList, columnTextureList, columnHeightList,~
	MIN_COLUMN_SPEED, MAX_COLUMN_SPEED)
	
Call CreateLoadbarContents(&loadTexturePointer1, &loadTexturePointer2, LOAD_BAR_WIDTH,~
	LOAD_BAR_HEIGHT)

% --- MAIN SUBSECTION ---

GoSub Main
End

Main:
	% START THE COLUMNS AND ITS CONTENTS
	Call InitiateColumns(columnTextureList, columnPointerList,~
		columnHeightList, WINDOW_WIDTH - COLUMN_WIDTH, columnPositionListY)
		
	% START THE TITLE TEXT
	Call InitiateTitleText(WINDOW_WIDTH, titleStringList, &titleTextPointer,~
		WINDOW_WIDTH / 2, WINDOW_HEIGHT / 3)
		
	% START THE LOADING BAR
	Call InitiateLoadingBar(&loadingBarPointer, loadTexturePointer1, WINDOW_WIDTH,~
		WINDOW_HEIGHT, LOAD_BAR_WIDTH, LOAD_BAR_HEIGHT)
	do
		% STARTS THE CLOCK TO TRACK THE ELAPSED TIME
		gameClock = Clock()
		
		% UPDATE THE COLUMNS POSITION
		Call UpdateColumns(columnPointerList, columnSpeedList, columnHeightList,~
		columnPositionListY, elapsedTime, WINDOW_HEIGHT)
		
		% UPDATE THE TITLE TEXT
		Call UpdateTitleText(titleStringList, titleTextPointer, elapsedTime,~
			&titleFrameTime, MAX_TITLE_FRAME_TIME, &isLoadingComplete)

		% UPDATE THE LOADING BAR
		Call UpdateLoadingBar(loadingBarPointer, loadTexturePointer1, loadTexturePointer2,~
		LOAD_BAR_WIDTH, LOAD_BAR_HEIGHT, &elapsedTime, &barFrameTime,~
		barMaxFrameTime, &currentLoadedPercent, &loadingTime, TOTAL_LOADING_TIME,~
		&currentLoadingTexture, &isLoadingComplete)

		% RENDER THE CURRENT FRAME
		Gr.Render
		
		% CALCULATE THE TIME ELAPSED AFTER THIS FRAME
		elapsedTime = Clock() - gameClock
	until false
Return