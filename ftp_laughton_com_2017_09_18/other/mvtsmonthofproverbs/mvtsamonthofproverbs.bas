REM Start of BASIC! Program
%Title: MVTs A Month of Proverbs
%Summary: A simple program for Android that displays one of 31 of our favorite proverbs.
%Copyright (c) 2016 by Michael and Vivian Thomas
%Released under the MIT license, a copy of which can be found in the license.txt file.

%scaling
di_height = 780
di_width = 480
GR.OPEN 255, 255, 255, 255
GR.ORIENTATION 1
GR.SCREEN actual_w, actual_h
scale_width = actual_w / di_width
scale_height = actual_h / di_height
GR.SCALE scale_width, scale_height

%opening screen
GR.COLOR 255, 255, 0, 0, 1
GR.RECT rct1, 0, 0, 480, 780
GR.COLOR 255, 255, 200, 0, 1
GR.RECT rct2, 15, 15, 465, 765
GR.COLOR 255, 0, 0, 0, 1
GR.RECT rct2, 40, 100, 440, 680
GR.COLOR 255, 255, 225, 0, 1
GR.RECT rct4, 420, 105, 435, 675
GR.RECT rct5, 45, 655, 435, 675
GR.COLOR 255, 0, 0, 0, 1
GR.TEXT.SIZE 30
GR.TEXT.DRAW t1, 70, 65, "A   Month   of   Proverbs"
GR.TEXT.SIZE 50
GR.COLOR 255, 255, 200, 0, 1
GR.TEXT.DRAW t2, 160, 175, "H O L Y"
GR.TEXT.DRAW t3, 150, 250, "B I B L E"
GR.TEXT.SIZE 25
GR.TEXT.DRAW t4, 100, 625, "KING JAMES VERSION"
GR.RENDER

%open bible
PAUSE 3000
GR.COLOR 255, 255, 200, 0, 1
GR.RECT rct6, 15, 15, 465, 765
GR.COLOR 255, 0, 0, 0, 1
GR.RECT rct7, 90, 600, 390, 700
GR.COLOR 255, 255, 255, 255, 1
GR.RECT rct8, 96, 600, 384, 694
GR.COLOR 255, 255, 200, 0, 1
GR.RECT rct9, 96, 600, 100, 694
GR.COLOR 255, 255, 200, 0, 1
GR.RECT rct10, 378, 600, 384, 694
GR.COLOR 255, 255, 200, 0, 1
GR.RECT rct11, 96, 688, 384, 694
GR.COLOR 255, 0, 0, 0, 1
GR.RECT rct12, 239, 600, 240, 700

%text area with bible verse
GR.COLOR 255, 255, 255, 255, 1
GR.RECT rct13, 15, 15, 465, 550
GR.COLOR 255, 200, 0, 0, 1
GR.RECT rct14, 0, 550, 480, 560
GR.TEXT.SIZE 30
GR.COLOR 255, 0, 0, 0, 1
GR.TEXT.DRAW t5, 117, 740, "Press BACK to exit"
GR.RENDER
GR.TEXT.SIZE 35
GR.COLOR 255, 0, 0, 0, 1
i = randomNumber
i = FLOOR(RND()*31)+1
IF i = 1 THEN
 GR.TEXT.DRAW t6, 20, 150, "Fear of the Lord is the": GR.TEXT.DRAW t7, 20, 200, "beginning of knowledge; but": GR.TEXT.DRAW t8, 20, 250, "fools despise wisdom and": GR.TEXT.DRAW t9, 20, 300, "instruction..": GR.TEXT.DRAW t10, 20, 350, "Proverbs 1:7"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 2 THEN
 GR.TEXT.DRAW t6, 20, 150, "For the upright shall": GR.TEXT.DRAW t7, 20, 200, "dwell in the land, and the": GR.TEXT.DRAW t8, 20, 250, "perfect shall remain in it.": GR.TEXT.DRAW t9, 20, 300, "Proverbs 2:21"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 3 THEN
 GR.TEXT.DRAW t6, 20, 150, "Her (wisdom's) ways": GR.TEXT.DRAW t7, 20, 200, "are ways of pleasantness,": GR.TEXT.DRAW t8, 20, 250, "and all her paths are peace.": GR.TEXT.DRAW t9, 20, 300, "Proverbs 3:17"
 GR.RENDER:DO:UNTIL 0

ELSEIF i = 4 THEN
 GR.TEXT.DRAW t6, 20, 150, "Forsake her (wisdom) not,": GR.TEXT.DRAW t7, 20, 200, "and she shall preserve": GR.TEXT.DRAW t8, 20, 250, "thee; love her, and she": GR.TEXT.DRAW t9, 20, 300, "shall keep thee.": GR.TEXT.DRAW t10, 20, 350, "Proverbs 4:6"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 5 THEN
 GR.TEXT.DRAW t6, 20, 150, "For the ways of man are be-": GR.TEXT.DRAW t7, 20, 200, "fore the eyes of the LORD,": GR.TEXT.DRAW t8, 20, 250, "and he pondereth all his": GR.TEXT.DRAW t9, 20, 300, "goings.": GR.TEXT.DRAW t10, 20, 350, "Proverbs 5:21"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 6 THEN
 GR.TEXT.DRAW t6, 20, 150, "How long wilt thou sleep,": GR.TEXT.DRAW t7, 20, 200, "O sluggard? when wilt thou": GR.TEXT.DRAW t8, 20, 250, "arise out of thy sleep?": GR.TEXT.DRAW t9, 20, 300, "Proverbs 6:9"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 7 THEN
 GR.TEXT.DRAW t6, 20, 150, "Say unto wisdom, Thou art": GR.TEXT.DRAW t7, 20, 200, "my sister; and call under-": GR.TEXT.DRAW t8, 20, 250, "standing thy kinswoman:": GR.TEXT.DRAW t9, 20, 300, "Proverbs 7:4"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 8 THEN
 GR.TEXT.DRAW t6, 20, 150, "For wisdom is better than": GR.TEXT.DRAW t7, 20, 200, "rubies; and all the things": GR.TEXT.DRAW t8, 20, 250, "that may be desired are not": GR.TEXT.DRAW t9, 20, 300, "to be compared to it.": GR.TEXT.DRAW t10, 20, 350, "Proverbs 8:11"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 9 THEN
 GR.TEXT.DRAW t6, 20, 150, "The fear of the LORD is the": GR.TEXT.DRAW t7, 20, 200, "beginning of wisdom; and": GR.TEXT.DRAW t8, 20, 250, "the knowledge of the holy": GR.TEXT.DRAW t9, 20, 300, "is understanding.": GR.TEXT.DRAW t10, 20, 350, "Proverbs 9:10"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 10 THEN
 GR.TEXT.DRAW t6, 20, 150, "The mouth of the just": GR.TEXT.DRAW t7, 20, 200, "bringeth forth wisdom; but": GR.TEXT.DRAW t8, 20, 250, "the froward tongue shall be": GR.TEXT.DRAW t9, 20, 300, "cut out.": GR.TEXT.DRAW t10, 20, 350, "Proverbs 10:31"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 11 THEN
 GR.TEXT.DRAW t6, 20, 150, "When pride cometh, then": GR.TEXT.DRAW t7, 20, 200, "cometh shame: but with the": GR.TEXT.DRAW t8, 20, 250, "lowly is wisdom.": GR.TEXT.DRAW t9, 20, 300, "Proverbs 11:2"
 GR.RENDER: DO : UNTIL 0

ELSEIF i = 12 THEN
 GR.TEXT.DRAW t6, 20, 150, "In the way of righteousness": GR.Text.DRAW t7, 20, 200, "is life; and in the pathway": GR.TEXT.DRAW t8, 20, 250, "thereof there is no death.": GR.TEXT.DRAW t9, 20, 300, "Proverbs 12:28"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 13 THEN
 GR.TEXT.DRAW t6, 20, 150, "Whoso despiseth the word": GR.TEXT.DRAW t7, 20, 200, "shall be destroyed; but he": GR.TEXT.DRAW t8, 20, 250, "that feareth the command-": GR.TEXT.DRAW t9, 20, 300, "ment shall be rewarded.": GR.TEXT.DRAW t10, 20, 350, "Proverbs 13:13"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 14 THEN
 GR.TEXT.DRAW t6, 20, 150, "The simple inherit folly;": GR.TEXT.DRAW t7, 20, 200, "but the prudent are crowned": GR.TEXT.DRAW t8, 20, 250, "with knowledge.":  GR.TEXT.DRAW t9, 20, 300, "Proverbs 14:18" 
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 15 THEN
 GR.TEXT.DRAW t6, 20, 150, "A wise son maketh a glad": GR.TEXT.DRAW t7, 20, 200, "father; but a foolish man": GR.TEXT.DRAW t8, 20, 250, "despiseth his mother.": GR.TEXT.DRAW t9, 20, 300, "Proverbs 15:20"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 16 THEN
 GR.TEXT.DRAW t6, 20, 150, "Everyone that is proud in": GR.TEXT.DRAW t7, 20, 200, "heart is an abomination to": GR.TEXT.DRAW t8, 20, 250, "the LORD; though hand join": GR.TEXT.DRAW t9, 20, 300, "in hand, he shall not be": GR.TEXT.DRAW t10, 20, 350, "unpunished.": GR.TEXT.DRAW t11, 20, 400, "Proverbs 16:5"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 17 THEN
 GR.TEXT.DRAW t6, 20, 150, "Even a fool, when he": GR.TEXT.DRAW t7, 20, 200, "holdeth his peace, is": GR.TEXT.DRAW t8, 20, 250, "counted wise; and he that": GR.TEXT.DRAW t9, 20, 300, "shutteth his lips is": GR.TEXT.DRAW t10, 20, 350, "esteemed a man of under-": GR.TEXT.DRAW t11, 20, 400, "standing.": GR.TEXT.DRAW t12, 20, 450, "Proverbs 17:28"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 18 THEN
 GR.TEXT.DRAW t6, 20, 150, "A brother offended is": GR.TEXT.DRAW t7, 20, 200, "harder to be won than a": GR.TEXT.DRAW t8, 20, 250, "strong city; and their con-": GR.TEXT.DRAW t9, 20, 300, "tentions are like the": GR.TEXT.DRAW t10, 20, 350, "bars of a castle.": GR.TEXT.DRAW t11, 20, 400, "Proverbs 18:19"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 19 THEN
 GR.TEXT.DRAW t6, 20, 150, "He that hath pity upon the": GR.TEXT.DRAW t7, 20, 200, "poor lendeth unto the LORD;": GR.TEXT.DRAW t8, 20, 250, "and that which he has given": GR.TEXT.DRAW t9, 20, 300, "will he pay him again.": GR.TEXT.DRAW t10, 20, 350, "Proverbs 19:17"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 20 THEN
 GR.TEXT.DRAW t6, 20, 150, "He that goeth about as a": GR.TEXT.DRAW t7, 20, 200, "talebearer revealeth se-": GR.TEXT.DRAW t8, 20, 250, "crets; therefore meddle not": GR.TEXT.DRAW t9, 20, 300, "with him that flattereth": GR.TEXT.DRAW t10, 20, 350, "with his lips.": GR.TEXT.DRAW t11, 20, 400, "Proverbs 20:17"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 21 THEN
 GR.TEXT.DRAW t6, 20, 150, "The man that wandereth out": GR.TEXT.DRAW t7, 20, 200, "of the way of understanding": GR.TEXT.DRAW t8, 20, 250, "shall remain in the congre-": GR.TEXT.DRAW t9, 20, 300, "gation of the dead.": GR.TEXT.DRAW t10, 20, 350, "Proverbs 21:16"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 22 THEN
 GR.TEXT.DRAW t6, 20, 150, "A good name is rather to be": GR.TEXT.DRAW t7, 20, 200, "chosen than great riches;"  : GR.TEXT.DRAW t8, 20, 250, "and loving favour rather": GR.TEXT.DRAW t9, 20, 300, "than silver and gold.": GR.TEXT.DRAW t10, 20, 350, "Proverbs 22:1"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 23 THEN
 GR.TEXT.DRAW t6, 20, 150, "Hearken unto thy father": GR.TEXT.DRAW t7, 20, 200, "that begat thee, and de-": GR.TEXT.DRAW t8, 20, 250, "spise not thy mother when": GR.TEXT.DRAW t9, 20, 300, "she is old.": GR.TEXT.DRAW t10, 20, 350, "Proverbs 23:22"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 24 THEN
 GR.TEXT.DRAW t6, 20, 150, "For a just man falleth": GR.TEXT.DRAW t7, 20, 200, "seven times, and riseth up": GR.TEXT.DRAW t8, 20, 250, "again; but the wicked shall": GR.TEXT.DRAW t9, 20, 300, "fall into mischief.": GR.TEXT.DRAW t10, 20, 350, "Proverbs 24:16"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 25 THEN
 GR.TEXT.DRAW t6, 20, 150, "If thine enemy be hungry,": GR.TEXT.DRAW t7, 20, 200, "give him bread to eat; and": GR.TEXT.DRAW t8, 20, 250, "if he be thirsty, give him": GR.TEXT.DRAW t9, 20, 300, "water to drink:": GR.TEXT.DRAW t10, 20, 350, "Proverbs 25:21"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 26 THEN
 GR.TEXT.DRAW t6, 20, 150, "Answer not a fool according": GR.TEXT.DRAW t7, 20, 200, "to his folly, lest thou": GR.TEXT.DRAW t8, 20, 250, "also be like unto him.": GR.TEXT.DRAW t9, 20, 300, "Proverbs 26:4"
 GR.RENDER: DO: UNTIL 0 

ELSEIF i = 27 THEN
 GR.TEXT.DRAW t6, 20, 150, "Ointment and perfume": GR.TEXT.DRAW t7, 20, 200, "rejoice the heart; so doth": GR.TEXT.DRAW t8, 20, 250, "the sweetness of a man's": GR.TEXT.DRAW t9, 20, 300, "friend by hearty counsel.": GR.TEXT.DRAW t10, 20, 350, "Proverbs 27:9"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 28 THEN
 GR.TEXT.DRAW t6, 20, 150, "He that turneth away his": GR.TEXT.DRAW t7, 20, 200, "ear from hearing the law,": GR.TEXT.DRAW t8, 20, 250, "even his prayer shall be": GR.TEXT.DRAW t9, 20, 300, "abomination.": GR.TEXT.DRAW t10, 20, 350, "Proverbs 28:9"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 29 THEN
 GR.TEXT.DRAW t6, 20, 150, "When the wicked are multi-": GR.TEXT.DRAW t7, 20, 200, "plied, transgression": GR.TEXT.DRAW t8, 20, 250, "increaseth; but the righ-": GR.TEXT.DRAW t9, 20, 300, "teous shall see their fall.": GR.TEXT.DRAW t10, 20, 350, "Proverbs 29:16"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 30 THEN
 GR.TEXT.DRAW t6, 20, 150, "Every word of God is pure;": GR.TEXT.DRAW t7, 20, 200, "he is a shield unto them": GR.TEXT.DRAW t8, 20, 250, "that put their trust": GR.TEXT.DRAW t9, 20, 300, "in him.": GR.TEXT.DRAW t10, 20, 350, "Proverbs 30:5"
 GR.RENDER: DO: UNTIL 0

ELSEIF i = 31 THEN
 GR.TEXT.DRAW t6, 20, 150, "Give strong drink unto him": GR.TEXT.DRAW t7, 20, 200, "that is ready to perish,": GR.TEXT.DRAW t8, 20, 250, "and wine unto those that be": GR.TEXT.DRAW t9, 20, 300, "of heavy hearts.": GR.TEXT.DRAW t10, 20, 350, "Proverbs 31:6"
 GR.RENDER: DO: UNTIL 0

ENDIF

DO
UNTIL 0
ON BACKKEY:
EXIT
