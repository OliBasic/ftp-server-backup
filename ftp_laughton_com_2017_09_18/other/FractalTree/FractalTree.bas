
! OPEN GRAPHIC INTERFACE
gr.open 255, 0, 0, 0
gr.orientation 0
gr.color 255, 0, 255, 0, 0
gr.set.stroke 2
gr.screen sw, sh

! PI
pi = 3.1415

! INITIAL POSITION
x = sw/2
y = sh-(sh/40)

! SHAPE VALUES
scale_A = 0.75		% SCALE for every new left ramification
scale_B = 0.75		% SCALE for every new right ramification
ang_A = 35*pi/180	% ANGLE for the left ramifications
ang_B = 35*pi/180	% ANGLE for the right ramifications
leng = sh/5			% Trunk lenght
sweep = 0			% Don't change this

! NUMBER OF RAMIFICATIONS
level = 1
max_level = 10		% Changes here only!

! STACKS
DIM lv_x[max_level]
DIM lv_y[max_level]

gosub build_tree
PAUSE 30000
END

build_tree:
	gr.render

	lv_x[level] = x
	lv_y[level] = y
	gosub draw_branch
	
	IF level < max_level
		level += 1
		leng = leng*scale_A
		sweep -= ang_A
		gosub build_tree
		
		leng = leng/scale_A*scale_B
		sweep += ang_A+ang_B
		gosub build_tree
		
		leng = leng/scale_B
		sweep -= ang_B
		level -= 1
	ENDIF
	x = lv_x[level]
	y = lv_y[level]
return
	
draw_branch:
	xn = x + leng*sin(sweep)
	yn = y - leng*cos(sweep)
	gr.line branch, x, y, xn, yn
	x = xn
	y = yn
return