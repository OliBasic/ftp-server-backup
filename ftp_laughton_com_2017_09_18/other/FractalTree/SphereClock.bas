! SphereClock

GR.OPEN 255, 0, 0, 0
GR.ORIENTATION 0
GR.SCREEN ScreenW, ScreenH

GR.COLOR 120, 255, 255, 255, 0
GR.SET.STROKE 6

Unit = ScreenH/16
CenterX = ScreenW/2
CenterY = ScreenH/2

CalcH = 360/12
CalcM = 360/60
CalcS = 360/60

GR.ARC arcHour, CenterX-(2*Unit), CenterY-(2*Unit), CenterX+(2*Unit), CenterY+(2*Unit), 270, 0, 0
GR.ARC arcMin, CenterX-(4*Unit), CenterY-(4*Unit), CenterX+(4*Unit), CenterY+(4*Unit), 270, 0, 0
GR.ARC arcSec, CenterX-(6*Unit), CenterY-(6*Unit), CenterX+(6*Unit), CenterY+(6*Unit), 270, 0, 0

GR.COLOR 120, 255, 255, 255, 0
GR.TEXT.ALIGN 2
GR.TEXT.SIZE Unit
GR.SET.STROKE 2

GR.LINE line, CenterX, CenterY-(1*Unit), CenterX, CenterY-(2*Unit)

FOR i = 0 TO 11
	nvarTemp = i
	IF i < 1 THEN nvarTemp = 12
	svarTemp$ = FORMAT$("##  ",nvarTemp)

	GR.ROTATE.START i*CalcH, CenterX, CenterY
		GR.COLOR 120, 255, 255, 255, 0
		GR.LINE line, CenterX, CenterY-(2*Unit), CenterX, 2*Unit
		
		GR.COLOR 120, 255, 255, 255, 1
		GR.TEXT.DRAW txt, CenterX, 1.5*Unit, svarTemp$
	GR.ROTATE.END
NEXT

GR.COLOR 90, 255, 255, 255, 1
GR.SET.STROKE 1
FOR i = 0 TO 59
	GR.ROTATE.START i*6, CenterX, CenterY
		GR.LINE line, CenterX, CenterY-(4*Unit), CenterX, 2*Unit
	GR.ROTATE.END
NEXT

DO
	TIME ,,, Hr$, Mn$, Sc$

	hAng = VAL(Hr$)*CalcH
	mAng = VAL(Mn$)*CalcM
	sAng = VAL(Sc$)*CalcS

	IF hAng > 360 THEN hAng -= 360

	GR.MODIFY arcHour, "sweep_angle", hAng
	GR.MODIFY arcMin, "sweep_angle", mAng
	GR.MODIFY arcSec, "sweep_angle", sAng
	GR.RENDER
UNTIL 0