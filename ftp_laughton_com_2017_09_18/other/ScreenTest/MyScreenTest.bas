REM Start of BASIC! Program

GR.OPEN 255, 0, 255, 0
GR.ORIENTATION 0
GR.SET.ANTIALIAS 0
GR.SCREEN sw, sh

IF sw > sh
msr = 2*(sh/30)
ELSE
msr = 2*(sw/30)
ENDIF
PRINT sw;"x";sh;" - ";msr

GR.COLOR 255, 0, 0, 0, 1
GR.BITMAP.CREATE square, msr, msr
GR.BITMAP.DRAWINTO.START square
GR.RECT rect, 0, 0, msr, msr
GR.COLOR 255, 255, 0, 0, 0
GR.LINE line, msr-1, msr-1, 0, msr-1
GR.LINE line, msr-1, msr-1, msr-1, 0
GR.COLOR 255, 0, 0, 255, 0
GR.LINE line, 0, 0, msr, 0
GR.LINE line, 0, 0, 0, msr
GR.BITMAP.DRAWINTO.END

GR.COLOR 255, 0, 0, 0, 0
GR.BITMAP.CREATE screen, sw, sh
GR.BITMAP.DRAWINTO.START screen
FOR i = 0 TO (sw-1)
GR.LINE line, i, 0, i, (sh-1)
NEXT
GR.BITMAP.DRAWINTO.END

GR.BITMAP.CREATE screen2, sw, sh
GR.BITMAP.DRAWINTO.START screen2
FOR i = 0 TO sw/msr
GR.BITMAP.DRAW rect, square, i*msr, 0
NEXT
FOR i = 0 TO sh/msr-1
GR.BITMAP.DRAW rect, square, 0, i*msr
NEXT
GR.BITMAP.DRAWINTO.END

! FIRST TEST
GR.BITMAP.DRAW temp_screen, screen, 0, 0
GR.RENDER
PAUSE 5000
GR.HIDE temp_screen

! SECOND TEST
GR.BITMAP.DRAW temp_screen2, screen2, 0, 0
GR.RENDER
PAUSE 10000
GR.HIDE temp_screen2

! THIRD TEST
GR.COLOR 255, 50, 50, 50, 0
FOR i = 0 TO (sh-1)
GR.LINE line, 0, i, (sw-1), i
NEXT
GR.RENDER
PAUSE 5000

! FOURTH TEST
GR.COLOR 255, 0, 0, 255, 1
GR.RECT rect, 0, 0, sw, sh
GR.COLOR 255, 0, 0, 0, 1
GR.RECT rect, 0, 0, sw-1, sh-1
GR.RENDER
PAUSE 5000

! FIFTH TEST
GR.CLS
GR.COLOR 255, 0, 0, 0, 1
GR.RECT rect, 0, 0, sw, sh
GR.COLOR 255, 255, 255, 255, 0
GR.LINE line, 0, 0, sw, 0
GR.LINE line, sw, 0, sw, sh
GR.LINE line, 0, sh, sw, sh
GR.LINE line, 0, 0, 0, sh
GR.RENDER

DO
UNTIL 0
